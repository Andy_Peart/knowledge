select
    'DET'||'|'||
    ol.line_id||'|'||
    ol.sku_id||'|'||
    sk.ean||'|'||
    sk.description||'|'||
    ol.qty_shipped||'|'||
    'Eaches'||'|'||
    case when ol.qty_shipped is null
    then
    'Scratch'
    else
    '||' end ||'|'||
    '|||'||'|'||
    'Y' ||'|'||
    'Y' ||'|'
from
    order_line ol,
    sku sk
where ol.sku_id = sk.sku_id
and ol.order_id ='C7000060024'; 