--create or replace store_to_manifest
declare
CURSOR cur_store_num is

select user_def_type_1 , order_id from order_header where status = 'Shipped' 
and carrier_id = 'FLEET' 
and to_char(shipped_date,'YY/MM/DD') = to_char(sysdate,'YY/MM/DD');

type
CUR_store_TAB_TYPE IS TABLE OF CUR_store_num%ROWTYPE INDEX BY binary_INTEGER;
REC_store CUR_store_num%rowtype; 
TAB_store CUR_store_TAB_TYPE;
v_ix integer := 1;

BEGIN
    OPEN CUR_store_num;
    FETCH CUR_STORE_NUM INTO REC_STORE;
LOOP
    EXIT WHEN CUR_STORE_NUM%NOTFOUND;
    TAB_STORE(v_ix) := REC_STORE;
        update shipping_manifest set shipping_manifest.user_def_type_1 = rec_store.user_def_type_1
        where shipping_manifest.order_id = rec_store.order_id
        and shipping_manifest.user_def_type_1 is null;
    COMMIT;				
    v_ix := v_ix + 1;
    FETCH CUR_STORE_NUM INTO REC_STORE; 
  END LOOP; 
  CLOSE CUR_Store_num; 
END;
/



