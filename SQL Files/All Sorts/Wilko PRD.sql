select "WEEK" , 
    "DAY", 
    "INBOUND",
    "ORDERS OUT", 
    "UNITS OUT",
    "PARCELS OUT",
    round(SUM("UNITS OUT"/"ORDERS OUT"),2) avg_units,
    round(SUM("PARCELS OUT"/"ORDERS OUT"),2)avg_parcel,
    round (HBSPAL.Vol / HBS.loc ,2) HBSPER,
    round (TOPPAL.Vol / TOP.loc ,2) TOPPER,
    round (MIDMPAL.Vol / MIDM.loc ,2) MIDMPER,
    round (MIDPAL.Vol / MID.loc,2) MIDPER,
    round (BULKPAL.Vol / BULK.loc ,2) BULKPER,
    round (ROTHPAL.Vol / ROTH.loc,2) ROTHPER,
    round (TOTPAL.Vol / TOT.loc,2) TOTPER,
    "ORDERS",
    "LINES",
    "PICKED_ORDERS",
    "SHORT_PICKS",
    "TO_QC_DECANT",
    "PI COUNTS",
    "ADJUST UP",
    "ADJUST DOWN",
    to_char(sysdate-1,'DD/MM/YYYY')
from
(select nvl(sum(update_qty),0) "ADJUST DOWN"
    from inventory_transaction
    where update_qty > 0
    and code = 'Adjustment'
    and from_loc_id = 'SUSPENSE'
    and to_char(dstamp,'dd/mm/yyyy hh24:mi')> to_char(systimestamp -2,'DD/MM/YYYY')|| '19:30'
    and to_char(dstamp,'dd/mm/yyyy hh24:mi')< to_char(systimestamp -1,'DD/MM/YYYY')|| '19:30'),
(select nvl(sum(update_qty),0) "ADJUST UP"
    from inventory_transaction
    where update_qty < 0
    and code = 'Adjustment'
    and from_loc_id = 'SUSPENSE'
    and to_char(dstamp,'dd/mm/yyyy hh24:mi')> to_char(systimestamp -2,'DD/MM/YYYY')|| '19:30'
    and to_char(dstamp,'dd/mm/yyyy hh24:mi')< to_char(systimestamp -1,'DD/MM/YYYY')|| '19:30'),
(select count(from_loc_id) "PI COUNTS"
    from inventory_transaction
    where code ='Stock Check'
    and list_id is not null
    and to_char(dstamp,'dd/mm/yyyy')= to_char(sysdate -1,'DD/MM/YYYY')),
(select to_char(to_date(sysdate -1),'WW')  "WEEK"
    from dual),
(select to_char( sysdate -1,'DAY' )  "DAY"
    from dual),
(select sum(update_qty) "INBOUND"
    from inventory_transaction
    where code = 'Receipt'
    and to_char(dstamp,'dd/mm/yyyy hh24:mi')> to_char(systimestamp -2,'DD/MM/YYYY')|| '19:30'
    and to_char(dstamp,'dd/mm/yyyy hh24:mi')< to_char(systimestamp -1,'DD/MM/YYYY')|| '19:30'),
(select count(distinct(order_id)) "ORDERS OUT"
    from shipping_manifest 
    where 
    to_char(shipped_dstamp,'dd/mm/yyyy hh24:mi')> to_char(systimestamp -2,'DD/MM/YYYY')|| '19:30'
    and to_char(shipped_dstamp,'dd/mm/yyyy hh24:mi')< to_char(systimestamp -1,'DD/MM/YYYY')|| '19:30'),
(select sum(qty_shipped) "UNITS OUT"
    from shipping_manifest where    
     to_char(shipped_dstamp,'dd/mm/yyyy hh24:mi')> to_char(systimestamp -2,'DD/MM/YYYY')|| '19:30'
    and to_char(shipped_dstamp,'dd/mm/yyyy hh24:mi')< to_char(systimestamp -1,'DD/MM/YYYY')|| '19:30'),
(select count(distinct(container_id)) "PARCELS OUT"
    from shipping_manifest where     
     to_char(shipped_dstamp,'dd/mm/yyyy hh24:mi')> to_char(systimestamp -2,'DD/MM/YYYY')|| '19:30'
    and to_char(shipped_dstamp,'dd/mm/yyyy hh24:mi')< to_char(systimestamp -1,'DD/MM/YYYY')|| '19:30'),
(select count(distinct(location_id)) Vol from inventory where zone_1 = 'HBS' ) HBSPAL,
(select count(location_id) loc FROM location where zone_1 = 'HBS' ) HBS,
(select count(distinct(location_id)) Vol from inventory where zone_1 = 'ZONE4' ) TOPPAL,
(select count(location_id) loc FROM location where zone_1 = 'ZONE4' ) TOP,
(select count(distinct(location_id)) Vol from inventory where zone_1 = 'ZONE3' ) MIDMPAL,
(select count(location_id) loc FROM location where zone_1 = 'ZONE3' ) MIDM,
(select count(distinct(location_id)) Vol from inventory where zone_1 = 'ZONE2' ) MIDPAL,
(select count(location_id) loc FROM location where zone_1 = 'ZONE2' ) MID,
(select count(distinct(location_id)) Vol from inventory where zone_1 = 'ZONE5' ) BULKPAL,
(select count(location_id) loc FROM location where zone_1 = 'ZONE5' ) BULK,
(select count(distinct(location_id)) Vol from inventory where zone_1 = 'ZONE6' ) ROTHPAL,
(select count(location_id) loc FROM location where zone_1 = 'ZONE6' ) ROTH,
(select count(distinct(location_id)) Vol from inventory where zone_1 in ('HBS','ZONE1','ZONE2','ZONE3','ZONE4','ZONE5', 'ZONE6') ) TOTPAL,
(select count(location_id) loc FROM location where zone_1 in ('HBS','ZONE1','ZONE2','ZONE3','ZONE4','ZONE5', 'ZONE6') ) TOT,
(select count(order_id) "ORDERS"
    from order_header where status in ('Allocated','Released')),
(select sum(qty_ordered) "LINES"
    from order_line where order_id in 
    (select order_id from order_header where status in ('Allocated','Released'))),
  (select count(distinct(reference_id)) "PICKED_ORDERS"
    from inventory_transaction where code = 'Pick'
    and to_loc_id like 'CONT%'
    and to_char(dstamp,'dd/mm/yyyy hh24:mi')> to_char(systimestamp -2,'DD/MM/YYYY')|| '19:30'
    and to_char(dstamp,'dd/mm/yyyy hh24:mi')< to_char(systimestamp -1,'DD/MM/YYYY')|| '19:30'),
    (select count(distinct(reference_id))"SHORT_PICKS"
    from inventory_transaction where code = 'Pick'
    and to_loc_id like 'CONT%'
    and reason_id = 'EX_NE'
    and to_char(dstamp,'dd/mm/yyyy hh24:mi')> to_char(systimestamp -2,'DD/MM/YYYY')|| '19:30'
    and to_char(dstamp,'dd/mm/yyyy hh24:mi')< to_char(systimestamp -1,'DD/MM/YYYY')|| '19:30'),
    (select count(distinct(reference_id))"TO_QC_DECANT"
    from inventory_transaction where code = 'Pick' 
    and to_loc_id like 'QCDEC%'
    and REFERENCE_ID IN (select distinct(reference_id)
    from inventory_transaction where code = 'Pick' 
    and to_loc_id like 'CONT%'
    and reason_id = 'EX_NE'
    and to_char(dstamp,'dd/mm/yyyy hh24:mi')> to_char(systimestamp -2,'DD/MM/YYYY')|| '19:30'
    and to_char(dstamp,'dd/mm/yyyy hh24:mi')< to_char(systimestamp -1,'DD/MM/YYYY')|| '19:30'))
group by
    "WEEK" , 
    "DAY", 
    "INBOUND",
    "ORDERS OUT", 
    "UNITS OUT",
    "PARCELS OUT",
    round (HBSPAL.Vol / HBS.loc,2),
    round (HBSPAL.Vol / HBS.loc,2),
    round (TOPPAL.Vol / TOP.loc ,2),
    round (MIDMPAL.Vol / MIDM.loc,2),
    round (MIDPAL.Vol / MID.loc,2),
    round (BULKPAL.Vol / BULK.loc,2),
    round (ROTHPAL.Vol / ROTH.loc,2),
    round (TOTPAL.Vol / TOT.loc,2) ,
    "ORDERS",
    "LINES",
    "PICKED_ORDERS",
    "SHORT_PICKS",
    "TO_QC_DECANT",
    "PI COUNTS",
    "ADJUST UP",
    "ADJUST DOWN",
    to_char(sysdate-1,'DD/MM/YYYY')