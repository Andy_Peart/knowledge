-- Top Section --
select "WEEK" , 
    "DAY", 
    "INBOUND",
    "ORDERS OUT", 
    "UNITS OUT",
    "PARCELS OUT",
    round(SUM("UNITS OUT"/"ORDERS OUT"),2) avg_units,
    round(SUM("PARCELS OUT"/"ORDERS OUT"),2)avg_parcel,
    round (HBSPAL.Vol / HBS.loc * 100,2) HBSPER,
    round (TOPPAL.Vol / TOP.loc * 100,2) TOPPER,
    round (MIDMPAL.Vol / MIDM.loc * 100,2) MIDMPER,
    round (MIDPAL.Vol / MID.loc * 100,2) MIDPER,
    round (BULKPAL.Vol / BULK.loc * 100,2) BULKPER,
    round (ROTHPAL.Vol / ROTH.loc * 100,2) ROTHPER,
    round (TOTPAL.Vol / TOT.loc * 100,2) TOTPER
from
(select to_char(to_date(sysdate -1),'WW')  "WEEK"
    from dual),
(select to_char( sysdate -1,'DAY' )  "DAY"
    from dual),
(select sum(update_qty) "INBOUND"
    from inventory_transaction
    where code = 'Receipt'
    and to_char(dstamp,'DD/MM/YYYY') = to_char(sysdate -1,'DD/MM/YYYY')),
(select count(distinct(order_id)) "ORDERS OUT"
    from shipping_manifest where to_char(shipped_dstamp,'DD/MM/YYYY') = to_char(sysdate -1,'DD/MM/YYYY')),
(select sum(qty_shipped) "UNITS OUT"
    from shipping_manifest where to_char(shipped_dstamp,'DD/MM/YYYY') = to_char(sysdate -1,'DD/MM/YYYY')),
(select count(distinct(container_id)) "PARCELS OUT"
    from shipping_manifest where to_char(shipped_dstamp,'DD/MM/YYYY') = to_char(sysdate -1,'DD/MM/YYYY')),
(select count(distinct(location_id)) Vol from inventory where zone_1 = 'HBS' ) HBSPAL,
(select count(location_id) loc FROM location where zone_1 = 'HBS' ) HBS,
(select count(distinct(location_id)) Vol from inventory where zone_1 = 'ZONE4' ) TOPPAL,
(select count(location_id) loc FROM location where zone_1 = 'ZONE4' ) TOP,
(select count(distinct(location_id)) Vol from inventory where zone_1 = 'ZONE3' ) MIDMPAL,
(select count(location_id) loc FROM location where zone_1 = 'ZONE3' ) MIDM,
(select count(distinct(location_id)) Vol from inventory where zone_1 = 'ZONE2' ) MIDPAL,
(select count(location_id) loc FROM location where zone_1 = 'ZONE2' ) MID,
(select count(distinct(location_id)) Vol from inventory where zone_1 = 'ZONE5' ) BULKPAL,
(select count(location_id) loc FROM location where zone_1 = 'ZONE5' ) BULK,
(select count(distinct(location_id)) Vol from inventory where zone_1 = 'ZONE6' ) ROTHPAL,
(select count(location_id) loc FROM location where zone_1 = 'ZONE6' ) ROTH,
(select count(distinct(location_id)) Vol from inventory where zone_1 in ('HBS','ZONE1','ZONE2','ZONE3','ZONE4','ZONE5', 'ZONE6') ) TOTPAL,
(select count(location_id) loc FROM location where zone_1 in ('HBS','ZONE1','ZONE2','ZONE3','ZONE4','ZONE5', 'ZONE6') ) TOT
group by
    "WEEK" , 
    "DAY", 
    "INBOUND",
    "ORDERS OUT", 
    "UNITS OUT",
    "PARCELS OUT",
    round (HBSPAL.Vol / HBS.loc * 100,2),
    round (HBSPAL.Vol / HBS.loc * 100,2),
    round (TOPPAL.Vol / TOP.loc * 100,2),
    round (MIDMPAL.Vol / MIDM.loc * 100,2),
    round (MIDPAL.Vol / MID.loc * 100,2),
    round (BULKPAL.Vol / BULK.loc * 100,2),
    round (ROTHPAL.Vol / ROTH.loc * 100,2),
    round (TOTPAL.Vol / TOT.loc * 100,2) ;
    
-- VOLUMES --    
select  round (HBSPAL.Vol / HBS.loc * 100,2) HBSPER,
        round (TOPPAL.Vol / TOP.loc * 100,2) TOPPER,
        round (MIDMPAL.Vol / MIDM.loc * 100,2) MIDMPER,
        round (MIDPAL.Vol / MID.loc * 100,2) MIDPER,
        round (BULKPAL.Vol / BULK.loc * 100,2) BULKPER,
        round (ROTHPAL.Vol / ROTH.loc * 100,2) ROTHPER,
        round (TOTPAL.Vol / TOT.loc * 100,2) TOTPER
from
(select count(distinct(location_id)) Vol from inventory where zone_1 = 'HBS' ) HBSPAL,
(select count(location_id) loc FROM location where zone_1 = 'HBS' ) HBS,
(select count(distinct(location_id)) Vol from inventory where zone_1 = 'ZONE4' ) TOPPAL,
(select count(location_id) loc FROM location where zone_1 = 'ZONE4' ) TOP,
(select count(distinct(location_id)) Vol from inventory where zone_1 = 'ZONE3' ) MIDMPAL,
(select count(location_id) loc FROM location where zone_1 = 'ZONE3' ) MIDM,
(select count(distinct(location_id)) Vol from inventory where zone_1 = 'ZONE2' ) MIDPAL,
(select count(location_id) loc FROM location where zone_1 = 'ZONE2' ) MID,
(select count(distinct(location_id)) Vol from inventory where zone_1 = 'ZONE5' ) BULKPAL,
(select count(location_id) loc FROM location where zone_1 = 'ZONE5' ) BULK,
(select count(distinct(location_id)) Vol from inventory where zone_1 = 'ZONE6' ) ROTHPAL,
(select count(location_id) loc FROM location where zone_1 = 'ZONE6' ) ROTH,
(select count(distinct(location_id)) Vol from inventory where zone_1 in ('HBS','ZONE1','ZONE2','ZONE3','ZONE4','ZONE5', 'ZONE6') ) TOTPAL,
(select count(location_id) loc FROM location where zone_1 in ('HBS','ZONE1','ZONE2','ZONE3','ZONE4','ZONE5', 'ZONE6') ) TOT;

-- TODAYS VOLUME --
select "ORDERS","LINES"
from
(select count(order_id) "ORDERS"
    from order_header where status in ('Allocated','Released')),
(select sum(qty_ordered) "LINES"
    from order_line where order_id in 
    (select order_id from order_header where status in ('Allocated','Released')));

