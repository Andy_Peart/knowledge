select count(distinct(order_id)),count(distinct(container_id)),sum(qty_shipped),carrier_id||' '||service_level
from 
shipping_manifest
where to_char(shipped_dstamp,'dd/mm/yyyy HH24:mi') > '12/10/2018 00:00'
and to_char(shipped_dstamp,'dd/mm/yyyy HH24:mi') < '12/10/2018 23:59'
group by
carrier_id||' '||service_level
order by
carrier_id||' '||service_level