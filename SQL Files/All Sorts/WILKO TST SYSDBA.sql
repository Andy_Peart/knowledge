create or replace function  clipper.AllOW_AUTOMATION (p_order_id in dcsdba.order_line.order_id%TYPE)return number;

BEGIN
    update dcsdba.order_line set dcsdba.order_line.user_def_chk_4 = 'N'
        where dcsdba.order_line.order_id = p_order_id;
    commit;
End;