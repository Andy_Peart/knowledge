select order_id from order_header
where carrier_id != 'TUFFNELLS'
and
order_id in (select order_id from order_container where length(CARRIER_CONSIGNMENT_ID) = 9)
and trunc(shipped_date) = '25-SEP-2018'
union all
select order_id from order_header
where carrier_id != 'YODEL'
and
order_id in (select order_id from order_container where CARRIER_CONSIGNMENT_ID like 'JD%')
and trunc(shipped_date) = '25-SEP-2018'
union all
select order_id from order_header
where carrier_id != 'HERMES'
and
order_id in (select order_id from order_container where length(CARRIER_CONSIGNMENT_ID) = 16)
and trunc(shipped_date) = '25-SEP-2018';


