-- PLT-X-013-)ODES
SELECT
  Rslt.Creation_Date AS "Day Date",
  Rslt.Creation_Time      AS "Date_Hour",
  Rslt.Source_Table       AS "Source_Table",
  SUM(Rslt.Order_Id)      AS "Orders"
FROM
  (SELECT 'Orders' AS Source_Table,
  TRUNC(Orh.Creation_Date)   AS Creation_Date,
  TO_CHAR(Orh.Creation_Date,'hh24') AS Creation_Time,
  COUNT(Orh.Order_Id)AS Order_Id
FROM ORDER_HEADER Orh
WHERE Orh.Creation_Date between  '01-AUG-18' and '02-AUG-18'
GROUP BY trunc(orh.Creation_Date) ,
TO_CHAR(Orh.Creation_Date,'hh24')
UNION ALL
SELECT
'Shipped' AS Source_Table,
TRUNC(Shm.Shipped_Dstamp)
AS  Creation_Date,
TO_CHAR(Shm.Shipped_Dstamp,'hh24') AS Creation_Time,
COUNT(DISTINCT Shm.Order_Id) AS Order_Id
FROM
Shipping_Manifest Shm
WHERE Shm.Shipped_Dstamp >=  '01-AUG-18'
AND Shm.Shipped_Dstamp < '02-AUG-18'
GROUP BY
TRUNC(Shipped_Dstamp) ,
TO_CHAR(Shm.Shipped_Dstamp,'hh24')
) Rslt
GROUP BY
Rslt.Creation_Date,
Rslt.Creation_Time,
Rslt.Source_Table
ORDER BY
Rslt.Creation_Date,
Rslt.Creation_Time,
Rslt.Source_Table