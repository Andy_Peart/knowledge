select count(i.key),i.user_id,u.name ,to_char(i.dstamp,'HH24') from inventory_transaction i, application_user u
where code = 'Pick'
and i.user_id = u.user_id
and to_loc_id like 'CONT%'
and to_char(dstamp,'DD/MM/YYYY') = '14/09/2018'
group by i.User_id,u.name,to_char(dstamp,'HH24')
order by user_id,to_char(dstamp,'HH24');

select count(key),user_id,to_char(dstamp,'HH24') from inventory_transaction
where code = 'Repack'
and to_loc_id like 'REPA%'
and to_char(dstamp,'DD/MM/YYYY') = '14/09/2018'
group by User_id,to_char(dstamp,'HH24')
order by user_id,to_char(dstamp,'HH24');

select count(distinct(container_id)),user_id,to_char(dstamp,'HH24') from inventory_transaction
where code = 'Repack'
and pallet_id like '9%'
and to_char(dstamp,'DD/MM/YYYY') = '14/09/2018'
group by User_id,to_char(dstamp,'HH24')
order by user_id,to_char(dstamp,'HH24');

select count(distinct(pallet_id)),user_id,to_char(dstamp,'HH24') from inventory_transaction
where code = 'Vehicle Load'
and pallet_id like '9%'
and to_char(dstamp,'DD/MM/YYYY') = '14/09/2018'
group by User_id,to_char(dstamp,'HH24') 
order by user_id,to_char(dstamp,'HH24');

select sum(UPDATE_QTY),user_id,to_char(dstamp,'HH24'),from_loc_id from inventory_transaction
where code = 'Putaway'
and to_char(dstamp,'DD/MM/YYYY') = '14/09/2018'
group by User_id,to_char(dstamp,'HH24'),from_loc_id 
order by user_id,to_char(dstamp,'HH24');

update inventory set origin_id = null where origin_id is not null and LOCK_STATUS = 'UnLocked';

update move_task set origin_id = null where origin_id is not null; and LOCK_STATUS = 'UnLocked';



