DECLARE

l_count         NUMBER(5)       := 0;
v_commit_limit  NUMBER(5)       := 100;
v_code  		NUMBER;
v_errm  		VARCHAR2(64);

CURSOR CUR_GROUND IS
	SELECT location_id from location where work_zone in
	('PICK01','PICK05','PICK06','V-PICK');
CURSOR CUR_MID IS
	SELECT location_id from location where work_zone in
	('PICK02','PICK03');
CURSOR CUR_TOP IS
	SELECT location_id from location where work_zone in
	('PICK04');

CUR_GROUND_ROW CUR_GROUND%rowtype;
CUR_MID_ROW CUR_MID%rowtype;
CUR_TOP_ROW CUR_TOP%rowtype;

BEGIN
	OPEN CUR_GROUND;
	l_count := 1;
	LOOP
		FETCH CUR_GROUND INTO CUR_GROUND_ROW;
		IF CUR_GROUND%notfound
		THEN /*EXIT WHEN NO DATA FOUND*/
			dbms_output.put_line('NO GROUND FLOOR LOCATIONS');
			EXIT;
		END IF;
		dbms_output.put_line(cur_ground_row.location_id);
		update location set user_def_type_7 = 'GROUND' 	/*SET LOCATIONS AS GROUND*/
			where location_id = cur_ground_row.location_id;
		IF l_count = v_commit_limit THEN /*COMMIT COUNTER*/
			dbms_output.put_line('commit');
			l_count := 1;
			COMMIT;
		END IF;
		l_count := l_count + 1;
	END LOOP;
	COMMIT;
	CLOSE CUR_GROUND;
	OPEN CUR_MID;
	l_count := 1;
	LOOP
		FETCH CUR_MID INTO CUR_MID_ROW;
		IF CUR_MID%notfound
		THEN /*EXIT WHEN NO DATA FOUND*/
			dbms_output.put_line('NO MID FLOOR LOCATIONS');
			EXIT;
		END IF;
		dbms_output.put_line(cur_mid_row.location_id);
		update location set user_def_type_7 = 'MID' /*SET LOCATIONS AS MID*/
			where location_id = cur_mid_row.location_id;
		IF l_count = v_commit_limit THEN /*COMMIT COUNTER*/
			dbms_output.put_line('commit');
			l_count := 1;
			COMMIT;
		END IF;
		l_count := l_count + 1;
	END LOOP;
	COMMIT;	
	CLOSE CUR_MID;
	OPEN CUR_TOP;
	l_count := 1;
	LOOP
		FETCH CUR_TOP INTO CUR_TOP_ROW;
		IF CUR_top%notfound
		THEN /*EXIT WHEN NO DATA FOUND*/
			dbms_output.put_line('NO TOP FLOOR LOCATIONS');
			EXIT;
		END IF;
		dbms_output.put_line(cur_top_row.location_id);
		update location set user_def_type_7 = 'TOP' /*SET LOCATIONS AS TOP*/
			where location_id = cur_top_row.location_id;
		IF l_count = v_commit_limit THEN /*COMMIT COUNTER*/
			dbms_output.put_line('commit');
			l_count := 1;
			COMMIT;
		END IF;
		l_count := l_count + 1;
	END LOOP;
	COMMIT;	
	CLOSE CUR_TOP;
	EXCEPTION /* WHEN ERROR OCCURS OUTPUT ERROR CODE AND CYCLE THROUGH CURSORS TO CHECK THEY ARE ALL SHUT*/
    WHEN OTHERS THEN
    v_code := SQLCODE;
    v_errm := SUBSTR(SQLERRM, 1, 64);
    DBMS_OUTPUT.PUT_LINE (v_code || ' ' || v_errm);
	IF
		CUR_GROUND%isopen
	then
		close CUR_GROUND;
	END IF;
	IF
		CUR_MID%isopen
	then
		close CUR_MID;
	END IF;
	IF
		CUR_TOP%isopen
	then
		close CUR_TOP;
	END IF;
	COMMIT; /*FINAL COMMIT TO ENSURE NO DB LOCKS*/
END;