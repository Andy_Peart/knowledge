select ';'||order_id, order_date from order_header where status in ('In Progress','Picked','Packed')
and order_id not in (select task_id from move_task) and DISALLOW_SHORT_SHIP is not null;

select ';'||order_id, order_date from order_header where status in ('In Progress','Picked','Packed')
and order_id not in (select task_id from move_task);

update order_header set DISALLOW_SHORT_SHIP = null where order_id in (select order_id from order_header where status in ('In Progress','Picked','Packed')
and order_id not in (select task_id from move_task));

select order_id from order_container where pallet_id like '9%'
group by order_id
having (count(distinct(pallet_id ))) > 1;

update move_task set status = 'Released' where task_id = '001052091067';

select order_id from order_line
group by order_id
having (count(distinct(trunc(CREATION_DATE))))>1;

select * from order_header where order_id like 'ANDYRE%';

select lock_status from location where location_id = 'QCDECANT';

update INTERFACE_supplier_sku set merge_status = 'Pending', qc_status = 'Released';

update order_header set user_def_chk_4 = 'N' where order_id in 
(select order_id from shipping_manifest where order_id in 
(select order_id from shipping_manifest where location_id ='123')
and location_id != '123');

(select distinct(order_id) from shipping_manifest where order_id in 
(select order_id from shipping_manifest where location_id ='123')
and location_id != '123');

select * from order_line;

update order_header set user_def_chk_4 = 'N' where order_id in 
(select order_id from shipping_manifest where location_id = '123' and uploaded_filename = 'DUMMY');

update shipping_manifest set uploaded = 'N' , uploaded_filename = null
where location_id = '814' and uploaded = 'Y' and uploaded_filename = 'DUMMY';


select order_header.order_id
from order_header, order_line
where
order_header.order_id = order_line.order_id
and
order_line.sku_id in (select sku_id from sku s where s.sku_id = sku_id and ugly = 'N') ;


(select 1
from
order_header, order_line
where
order_header.order_id = order_line.order_id
and
order_line.sku_id in (select sku_id from sku s where s.sku_id = sku_id and ugly = ''));


update move_task set to_pallet_id = 'CP3284CP3284' where to_pallet_id = 'CP2735' and list_id = 'ALL0171253';

update order_header set shipped_date = null,user_def_chk_4 = 'N' where order_id = '001002201800';