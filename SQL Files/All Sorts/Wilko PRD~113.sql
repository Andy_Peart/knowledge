select "TOT UNIT SCRATCHED","TOT UN UNITS"
from
(select sum(OLD_QTY_TO_MOVE- QTY_TO_MOVE) "TOT UNIT SCRATCHED" 
from MOVE_DESCREPANCY
where task_id in (select order_id from order_header where status = 'Shipped' and to_timestamp(to_char(UPLOADED_DSTAMP,'dd-mon-rr hh24:mi')) between
    					$P{from} and $P{to})
and REASON_CODE = 'EX_NE'
or task_id in (SELECT order_id from order_header 
							WHERE 
							status = 'Cancelled'
							AND to_timestamp(to_char(UPLOADED_DSTAMP,'dd-mon-rr hh24:mi')) between
								$P{from} and $P{to}
                            AND STATUS_REASON_CODE = '29'    
							OR
							status = 'Shipped'
							AND to_timestamp(to_char(UPLOADED_DSTAMP,'dd-mon-rr hh24:mi')) between
								$P{from} and $P{to}
							AND order_id not in (select order_id from shipping_manifest where 
													Shipped = 'Y'))),
(SELECT NVL(SUM(GS.Qty_Ordered- (ol.Qty_Tasked+OL.Qty_Shipped)),0) "TOT UN UNITS"
	FROM Generation_Shortage GS, Order_Line OL,Order_header OH
	WHERE 
	OL.Order_ID = GS.Task_ID
	AND OL.Order_ID = oh.order_id
	AND Oh.Order_ID = GS.Task_ID
	AND OL.Client_ID = GS.Client_ID
	AND OL.Line_ID = GS.Line_ID
	AND gs.SKU_ID = OL.SKU_ID
    and gs.task_id = ol.order_id
	AND GS.Task_ID IN (SELECT DISTINCT Oh.Order_ID 
						FROM Order_Line OH 
						WHERE status = 'Shipped')
	AND to_timestamp(to_char(oh.SHIPPED_DATE,'dd-mon-rr hh24:mi')) between
	$P{from} and $P{to}
    and task_id not in 
(select task_id 
from MOVE_DESCREPANCY
where task_id in (select order_id from order_header where status = 'Shipped' and to_timestamp(to_char(UPLOADED_DSTAMP,'dd-mon-rr hh24:mi')) between
    					$P{from} and $P{to})
and REASON_CODE = 'EX_NE'
or task_id in (SELECT order_id from order_header 
							WHERE 
							status = 'Cancelled'
							AND to_timestamp(to_char(UPLOADED_DSTAMP,'dd-mon-rr hh24:mi')) between
								$P{from} and $P{to}
                            AND STATUS_REASON_CODE = '29'    
							OR
							status = 'Shipped'
							AND to_timestamp(to_char(UPLOADED_DSTAMP,'dd-mon-rr hh24:mi')) between
								$P{from} and $P{to}
							AND order_id not in (select order_id from shipping_manifest where 
													Shipped = 'Y'))))
                                                 
                                                    