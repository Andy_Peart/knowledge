select "Allocation Shortage Order","Allocation Shortage Units","Cancelled Orders","Cancelled Units",
    "Allocation Shortage Order"+"Cancelled Orders", "Allocation Shortage Units"+"Cancelled Units"
from
(SELECT  
	count(distinct(GS.Task_ID)) "Allocation Shortage Order"
    --sum(GS.Qty_Ordered- (ol.Qty_Tasked+OL.Qty_Shipped))
FROM Generation_Shortage GS, Order_Line OL, SKU S,Order_header OH
WHERE OL.Order_ID = GS.Task_ID
and OL.Order_ID = oh.order_id
and Oh.Order_ID = GS.Task_ID
AND OL.Client_ID = GS.Client_ID
AND OL.Line_ID = GS.Line_ID
AND S.SKU_ID = OL.SKU_ID
AND S.Client_ID = OL.Client_ID
AND GS.Task_ID IN (SELECT DISTINCT Oh.Order_ID 
		   FROM Order_Line OH 
		   WHERE status = 'Shipped')
AND to_timestamp(to_char(oh.SHIPPED_DATE,'dd-mon-rr hh24:mi')) between
	'20-nov-18 07:30' and '21-nov-18 07:30'),
(SELECT  
	--count(distinct(GS.Task_ID)) "Unavailable Orders" 
    sum(GS.Qty_Ordered- (ol.Qty_Tasked+OL.Qty_Shipped))"Allocation Shortage Units"
FROM Generation_Shortage GS, Order_Line OL, SKU S,Order_header OH
WHERE OL.Order_ID = GS.Task_ID
and OL.Order_ID = oh.order_id
and Oh.Order_ID = GS.Task_ID
AND OL.Client_ID = GS.Client_ID
AND OL.Line_ID = GS.Line_ID
AND S.SKU_ID = OL.SKU_ID
AND S.Client_ID = OL.Client_ID
AND GS.Task_ID IN (SELECT DISTINCT Oh.Order_ID 
		   FROM Order_Line OH 
		   WHERE status = 'Shipped')
AND to_timestamp(to_char(oh.SHIPPED_DATE,'dd-mon-rr hh24:mi')) between
	'20-nov-18 07:30' and '21-nov-18 07:30'),
(select count(order_id) "Cancelled Orders"
    from order_header 
    where status = 'Cancelled'
	and status_reason_code = '30'
	and to_timestamp(to_char(UPLOADED_DSTAMP,'dd-mon-rr hh24:mi')) between
		'20-nov-18 07:30' and '21-nov-18 07:30'),
(select sum(QTY_ORDERED) "Cancelled Units"
    from order_line 
    where order_id in (select order_id from order_header
    where
    status = 'Cancelled'
	and status_reason_code = '30'
	and to_timestamp(to_char(UPLOADED_DSTAMP,'dd-mon-rr hh24:mi')) between
		'20-nov-18 07:30' and '21-nov-18 07:30'))
