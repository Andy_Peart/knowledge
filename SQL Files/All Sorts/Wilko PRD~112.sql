SELECT SUM(OL.Qty_Ordered- (ol.Qty_Tasked+OL.Qty_Shipped))
	from Order_Line ol
	WHERE
	Qty_ordered != qty_shipped
	AND order_id in (select order_id
						from Order_header
						where
						status = 'Shipped'
						AND to_timestamp(to_char(UPLOADED_DSTAMP,'dd-mon-rr hh24:mi')) between
						'20-nov-18 07:30' and '21-nov-18 07:30')
	AND order_id not in (SELECT order_id from order_header 
							WHERE 
							status = 'Cancelled'
							AND to_timestamp(to_char(UPLOADED_DSTAMP,'dd-mon-rr hh24:mi')) between
								'20-nov-18 07:30' and '21-nov-18 07:30'
							OR
							status = 'Shipped'
							AND to_timestamp(to_char(UPLOADED_DSTAMP,'dd-mon-rr hh24:mi')) between
								'20-nov-18 07:30' and '21-nov-18 07:30'
							AND order_id not in (select order_id from shipping_manifest where 
													Shipped = 'Y'))
	AND ol.order_id not in (SELECT gs.task_id
							FROM Generation_Shortage GS, Order_Line OL, SKU S,Order_header OH
							WHERE 
							OL.Order_ID = GS.Task_ID
							AND OL.Order_ID = oh.order_id
							AND Oh.Order_ID = GS.Task_ID
							AND OL.Client_ID = GS.Client_ID
							AND OL.Line_ID = GS.Line_ID
							AND S.SKU_ID = OL.SKU_ID
							AND S.Client_ID = OL.Client_ID
							AND GS.Task_ID IN (SELECT DISTINCT Oh.Order_ID 
												FROM Order_Line OH 
												WHERE status = 'Shipped')
							AND to_timestamp(to_char(oh.SHIPPED_DATE,'dd-mon-rr hh24:mi')) between
							'20-nov-18 07:30' and '21-nov-18 07:30')