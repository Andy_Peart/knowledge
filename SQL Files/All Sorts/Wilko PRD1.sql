select trunc(creation_date),count(distinct(order_ID)), sum(qty_ordered) from order_line
group by trunc(creation_date)
order by trunc(creation_date);

update order_header set DISALLOW_SHORT_SHIP = null where order_id in (
select s.order_id from shipping_manifest s where s.shipped = 'N'
and location_id = '001');

select * from order_container where order_id like 'WCSTEST%';

select * from package_logging_data where dstamp like '31-AUG-%';


select * from order_header where V_GFS_CARRIER_PRE = 'Y' and status = 'Released';

describe order_header;

update order_header set load_sequence = null where order_id in ('001033935713',
'001034111494',
'001034073079',
'001034070632',
'001034437459');

select order_id, trunc(order_date) from order_header where order_type like 'ISC%FLT' and V_GFS_CARRIER_PRE = 'Y' and status = 'Released';


