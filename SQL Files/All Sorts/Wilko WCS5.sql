select list_id from move_task where task_id = '001075299958';

SELECT
  v.sku_id,
  v.gfs_label_image_base_64,
  v.order_id,
  TO_CHAR(oh.CREATION_DATE, 'DD/MM/YYYY') as CREATION_DATE,
  TO_CHAR(sysdate, 'DD/MM/YYYY') as SHIPPED_DATE,
  oh.name,
  oh.CONTACT_PHONE,
  i.QTY_TASKED,
  oh.contact,
  s.description,
  mt.list_id
FROM order_header oh,
  v_order_ugly_label v,
  order_line i,
  sku s,
  move_task mt
WHERE oh.Client_id  = 'WILKINSON'
and mt.list_id = 'PNS0080509'
AND v.order_id =   mt.task_id 
and oh.order_id =   mt.task_id 
and i.order_id =   mt.task_id
and v.sku_id =  mt.sku_id 
and v.sku_id = i.sku_id
and i.sku_id = s.sku_id
order by mt.sequence