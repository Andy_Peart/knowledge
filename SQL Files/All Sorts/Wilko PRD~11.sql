update order_header set DISALLOW_SHORT_SHIP = null where order_id in (
select s.order_id
from shipping_manifest s
where s.shipped = 'N'
and s.order_id not in (select task_id from move_task)
and location_id like '123'
);


select distinct(';'||order_id),status from order_header where order_id in
(select order_id from shipping_manifest where location_id = '001')
and status != 'Shipped';

select distinct(task_id),pallet_id from move_task where task_id in (select order_id from shipping_manifest where location_id = '001');

-- Orders On a trailer --
select distinct(s.location_id) "location to ship", trunc(o.order_date) "ORDER DATE", ';'||o.order_id,o.order_type,o.status
from shipping_manifest s ,order_header o 
where s.shipped = 'N'
and s.order_id = o.order_id
and s.order_id not in (select task_id from move_task)
and location_id like 'S-%'
order by trunc(o.order_date);

select distinct(it.reference_id),sum(it.update_qty), ol.qty_ordered,ol.sku_id,ol.line_id,oc.container_id
from inventory_transaction it, order_line ol, order_container oc
where it.reference_id = ol.order_id
and it.sku_id = ol.sku_id
and oc.container_id = it.container_id
and code = 'Repack'
and station_id not like 'RDT%'
and reference_id = '001033780255'
group by it.reference_id, ol.sku_id,ol.qty_ordered,ol.line_id,oc.container_id;

update move_task set TO_CONTAINER_ID = null, TO_PALLET_ID = null, TROLLEY_SLOT_ID = null, V_TROLLEY_ID_ASSIGN = 'N' where list_id in ('30M0049542',
'20M0049426',
'23M0052565',
'12M0052561',
'123M0052208',
'14M0051669',
'234M0050440');


describe move_task;
