select "orders_picked", "orders_shorted", "orders_qcdecant" from
(select count(distinct(reference_id)) "orders_picked"
    from inventory_transaction where code = 'Pick'
    and client_id = 'LOVECR'
    and to_loc_id like 'CONT%'
    and to_char(dstamp,'dd/mm/yyyy')=$P{date}),
(select count(distinct(reference_id))"orders_shorted"
    from inventory_transaction where code = 'Pick'
    and client_id = 'LOVECR'
    and to_loc_id like 'CONT%'
    and reason_id = 'EX_NE'
    and to_char(dstamp,'DD/MM/YYYY') = $P{date}),
(select count(distinct(reference_id))"orders_qcdecant"
    from inventory_transaction where code = 'Pick' 
    and to_loc_id like 'QCDEC%'
    and client_id = 'LOVECR'
    and REFERENCE_ID IN (select distinct(reference_id)
    from inventory_transaction where code = 'Pick' 
    and to_loc_id like 'CONT%'
    and reason_id = 'EX_NE'
    and to_char(dstamp,'DD/MM/YYYY') = $P{date})
    and to_char(dstamp,'DD/MM/YYYY') = $P{date})


