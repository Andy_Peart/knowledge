select distinct(o.order_id),o.status,trunc(o.order_date), m.from_loc_id , m.task_type, m.pallet_id, m.list_id, o.order_type
from
order_header o, move_task m
where
m.task_id = o.order_id
order by trunc(o.order_date);

select distinct(';'||o.order_id),o.status,trunc(o.order_date), o.order_type
from
order_header o
where
o.order_id not in (select task_id from move_task)
and
o.status not in ('Shipped','Cancelled', 'Released')
order by trunc(o.order_date), o.order_type;


select count(distinct(ol.order_id)), sum(ol.qty_ordered), trunc(oh.order_date) from order_line ol, order_header oh
where
oh.order_id = ol.order_id
group by trunc(oh.order_date);

update move_task set status = 'Complete' where task_type = 'B'
and task_id in ( select o.order_id from order_header o where o.status = 'Packed'
                   and o.order_id in
(select reference_id from inventory_transaction where code = 'Repack' and FROM_LOC_ID = 'QCDECANT')
and o.order_type like '%FLT');





select distinct(o.order_id),m.task_type, trunc(o.order_date),sum(qty_to_move) /*m.pallet_id*/, o.order_type 
from order_header o, move_task m where o.status not in ('Shipped','Complete')
and o.order_id = m.task_id
and o.order_id in
(select reference_id from inventory_transaction where code = 'Repack' and FROM_LOC_ID = 'QCDECANT')
group by o.order_id,m.task_type, trunc(o.order_date),o.order_type
order by trunc(o.order_date);


