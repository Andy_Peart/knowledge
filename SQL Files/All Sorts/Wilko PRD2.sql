create or replace PROCEDURE MARSH_HEADER (p_pallet in move_task.pallet_id%TYPE)                           

IS

v_key           move_task.KEY%TYPE; -- set .nextval
v_first_key     move_task.first_KEY%TYPE; --set .nextval
v_from          move_task.from_loc_id%TYPE;
v_old_from      move_task.old_from_loc_id%TYPE;
v_to            move_task.to_loc_id%TYPE;
v_old_to        move_task.old_to_loc_id%TYPE;
v_final         move_task.final_loc_id%TYPE;
v_dstamp        move_task.dstamp%TYPE; --set as sysdate -10
v_consol        move_task.consol_link%TYPE;
v_zone          move_task.work_zone%TYPE;
v_group         move_task.work_group%TYPE;
v_consign       move_task.consignment%TYPE;
v_session       move_task.session_type%TYPE;
v_repack        move_task.repack%TYPE;
v_config        move_task.pallet_config%TYPE;
v_bond          move_task.ce_under_bond%TYPE;
v_print         move_task.print_label%TYPE;
v_stage         move_task.stage_route_sequence%TYPE;



BEGIN
  select MOVE_TASK_PK_SEQ.nextval, MOVE_TASK_PK_SEQ.nextval, from_loc_id, old_from_loc_id, to_loc_id, old_to_loc_id, final_loc_id, consol_link , work_zone ,consignment ,session_type,repack
          ,ce_under_bond,print_label,stage_route_sequence,pallet_config
    into v_key, v_first_key, v_from , v_old_from , v_to, v_old_to, v_final, v_consol, v_zone,v_consign,v_session,v_repack,v_bond ,v_print ,v_stage ,v_config 
    from move_task where ROWNUM = 1 and pallet_id = p_pallet;
    v_dstamp := sysdate -10;

  insert into move_task (KEY,FIRST_KEY,TASK_TYPE,TASK_ID,CLIENT_ID,SKU_ID,DESCRIPTION,QTY_TO_MOVE,OLD_QTY_TO_MOVE,SITE_ID,FROM_LOC_ID,OLD_FROM_LOC_ID,TO_LOC_ID,OLD_TO_LOC_ID,FINAL_LOC_ID,SEQUENCE,STATUS,
                          DSTAMP,START_DSTAMP,FINISH_DSTAMP,ORIGINAL_DSTAMP,PRIORITY,CONSOL_LINK,WORK_ZONE,CONSIGNMENT,PALLET_ID,PALLET_CONFIG,SESSION_TYPE,REPACK,SUMMARY_RECORD,KIT_RATIO,
                          CE_UNDER_BOND,PRINT_LABEL,STAGE_ROUTE_SEQUENCE) 
                          VALUES 
                          (v_key, v_first_key,'T','PALLET','WILKINSON','PALLET','Single container pallet','1','1','OLLE', v_from,v_old_from,v_to,v_old_to ,v_final,'0','Released',v_dstamp,v_dstamp,v_dstamp,v_dstamp,
                          '50',v_consol, v_zone,v_consign ,p_pallet,v_config,v_session,v_repack,'Y','1',v_bond ,v_print,v_stage);
  commit;


END;
/
BEGIN
    marsh_header('5098812');
end;
/
update move_task set TO_LOC_ID = 'REPACK6',FINAL_LOC_ID = 'REPACK6' where pallet_id = '5098812';

