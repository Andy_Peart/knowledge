SELECT "IN FULL","IN PART" /*,"UNITS UNAVAIL","UNIT SCRATCH","TOT ORD UNAVAIL","TOT ORD SCRATCH"*/
from
/* All orders cancelled in Full or Zero Shipped in full*/
(select count(order_id) "IN FULL"
	from order_header where status = 'Cancelled'
	and to_timestamp(to_char(UPLOADED_DSTAMP,'dd-mon-rr hh24:mi')) between
		'06-nov-18 02:00' and '09-nov-18 23:00'
	or
	status = 'Shipped'
	and to_timestamp(to_char(SHIPPED_DATE,'dd-mon-rr hh24:mi')) between
		'06-nov-18 02:00' and '09-nov-18 23:00'
	and order_id not in (select order_id from shipping_manifest)),
/* Orders With Short Shipped Line */
(select count(order_id) "IN PART" 
	from order_header
	where status = 'Shipped'
	and to_timestamp(to_char(SHIPPED_DATE,'dd-mon-rr hh24:mi')) between
		'06-nov-18 02:00' and '09-nov-18 23:00'
	and order_id in
			(select order_id from order_line where shipped_date is not null
				and QTY_ORDERED != QTY_SHIPPED)
				and order_id not in (select count(order_id) "IN FULL"
	from order_header where status = 'Cancelled'
	and to_timestamp(to_char(UPLOADED_DSTAMP,'dd-mon-rr hh24:mi')) between
		'06-nov-18 02:00' and '09-nov-18 23:00'
	or
	status = 'Shipped'
	and to_timestamp(to_char(SHIPPED_DATE,'dd-mon-rr hh24:mi')) between
		'06-nov-18 02:00' and '09-nov-18 23:00'
	and order_id not in (select order_id from shipping_manifest)));
    
    select * from order_header where order_id= '001075810754';
    
select * from move_task where task_id = '001075810754';

select * from PALLET_CONFIG where config_id like 'CASE%';

select distinct(status) from order_container;