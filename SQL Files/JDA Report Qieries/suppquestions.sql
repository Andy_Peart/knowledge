/******************************************************************************/
/*                                                                            */
/*   Logistics and Internet Systems Ltd                                       */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics and Internet Systems Ltd  */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Internet   */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Internet Systems Limited, 2001               */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*                                 LIS                                        */
/*                                                                            */
/*     FILE NAME  :     suppquestions.sql                                     */
/*                                                                            */
/*     DESCRIPTION:     List Supplier Questions.                              */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   21/03/01 DGP  DCS    NEW5300  initial version                            */
/*   29/08/02 RMW  DCS    NEW6186  Multi-client addresses                     */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/******************************************************************************/

/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132

/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Supplier Performance - Supplier Questions' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2 -

/* Column Headings and Formats */
COLUMN ClientID HEADING 'Client' FORMAT A10
COLUMN SupplierID HEADING 'Supplier' FORMAT A15
COLUMN QuestionID HEADING 'Question ID' FORMAT 99999999
COLUMN Question HEADING 'Questions' FORMAT A70 WRAP
COLUMN MaxMarks HEADING 'MaxMarks' FORMAT 999
COLUMN Weighting HEADING 'Weighting' FORMAT 999

BREAK ON ClientID SKIP PAGE ON Supplier_ID SKIP 1

/* SQL Select statement */
SELECT 	Client_ID ClientID, 
	Supplier_ID SupplierID,
	Question_ID QuestionID,
	Text Question,
	Max_Marks MaxMarks,
	Weighting Weighting
	FROM Supplier_Questions
	ORDER BY Client_ID ASC, Supplier_ID DESC, Question_ID ASC
;

SET TERMOUT ON

