/******************************************************************************/
/*                                                                            */
/*  Copyright (c) 2004                                                        */
/*  RedPrairie Corporation                                                    */
/*  All Rights Reserved                                                       */
/*                                                                            */
/*  The information in this file contains trade secrets and confidential      */
/*  information which is the property of RedPrairie Corporation.              */
/*                                                                            */
/*  All trademarks, trade names, copyrights and other intellectual property   */
/*  rights created, developed, embodied in or arising in connection with      */
/*  this software shall remain the sole property of RedPrairie Corporation.   */
/*                                                                            */
/*  Copyright (c) RedPrairie Corporation, 2004                                */
/*  ALL RIGHTS RESERVED                                                       */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :  suppdelmeasure.sql                                       */
/*                                                                            */
/*     DESCRIPTION:  List Supplier Performance by Supplier and date range.    */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/******************************************************************************/

/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132

/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Supplier Performance - Scheduled Against Actual Date' -
RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
LEFT 'Date Range From &4 To &5' RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 1 -
LEFT 'For Supplier: &3   Name: ' sSupplier SKIP 2 -

/* Column Headings and Formats */
COLUMN SupplierName NEW_VALUE sSupplier NOPRINT

COLUMN SiteID HEADING 'Site ID' FORMAT A10
COLUMN PreAdviceID HEADING 'Pre Advice ID' FORMAT A15
COLUMN ExpectedDStamp HEADING 'Expected Date/Time'  FORMAT A20
COLUMN ActualDStamp HEADING 'Actual Date/Time'  FORMAT A20
COLUMN QtyDue HEADING 'Expected Qty'  FORMAT 999.99
COLUMN QtyReceived HEADING 'Received Qty'  FORMAT 999.99


/* SQL Select statement */

SELECT  ITL.Site_ID SiteID,
        AD.Name SupplierName,
	BDD.Reference_ID PreAdviceID,
	TO_CHAR (SP.Scheduled_DStamp, 'DD-MON-YYYY HH24:MI') ExpectedDStamp,
	TO_CHAR (SP.Actual_DStamp, 'DD-MON-YYYY HH24:MI') ActualDStamp,
	PAL.Qty_Due QtyDue,
	ITL.Update_Qty QtyReceived
FROM Supplier_Performance SP, Booking_In_Diary_Details BDD, Address AD,
	 Inventory_Transaction ITL, Pre_Advice_Line PAL
WHERE SP.Added_DStamp >= TO_DATE ('&4', 'DD-MON-YYYY')
AND SP.Added_DStamp < TO_DATE ('&5', 'DD-MON-YYYY') + 1
AND SP.Actual_DStamp is NOT NULL
AND BDD.BookRef_ID = SP.BookRef_ID
AND ITL.Reference_ID = BDD.Reference_ID
AND TO_CHAR (ITL.DStamp, 'DD-MON-YYYY') = TO_CHAR (SP.Actual_DStamp, 'DD-MON-YYYY')
AND ITL.Supplier_ID = SP.Supplier_ID
AND PAL.Pre_Advice_ID = ITL.Reference_ID
AND PAL.Line_ID = ITL.Line_ID
AND AD.Address_ID = SP.Supplier_ID
AND SP.Supplier_ID = '&3'
AND SP.Client_ID = '&2'
ORDER BY ITL.Site_ID ASC , BDD.Reference_ID ASC
;

SET TERMOUT ON

