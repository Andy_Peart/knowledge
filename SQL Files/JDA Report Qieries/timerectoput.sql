/******************************************************************************/
/*                                                                            */
/*   Logistics and Internet Systems Ltd                                       */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics & Internet Systems Ltd    */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Internet   */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Internet Systems Limited, 2003               */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*                                  LIS                                       */
/*                                                                            */
/*     FILE NAME  :     timerectoput.sql                                      */
/*                                                                            */
/*     DESCRIPTION:     List the time from receipt to putaway by date range.  */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   20/08/03 RMW  DCS    NEW6806  Additional reports from projects           */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   06/07/05 JH   DCS    PDR9627  Report problems                            */
/*   08/08/05 DM   DCS    NEW7858  Extend SKU to 30 characters                */
/******************************************************************************/

/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132


/* Top Title */
TTITLE CENTER _SITENAME SKIP 2 -
LEFT 'Time from Receipt to Putaway by Date Range &5 to &6' -
RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2

COLUMN SiteClient HEADING 'Site/|Client' FORMAT A10 WRAP
COLUMN Owner_ID HEADING 'Owner' FORMAT A10 WRAP
COLUMN SKU_ID HEADING 'SKU' FORMAT A30 WRAP
COLUMN Description HEADING 'Description' FORMAT A40 WRAP
COLUMN Tag_ID HEADING 'Tag ID' FORMAT A20 WRAP
COLUMN TimeGap HEADING 'Time Delay' FORMAT A12 WRAP

BREAK ON Site_ID SKIP PAGE ON Client_ID SKIP 1 ON Owner_ID ON SKU_ID ON Description

SELECT	RPAD (ITL.Site_ID, 10) || ITL.Client_ID SiteClient,
	ITL.Owner_ID,
	ITL.SKU_ID,
	S.Description,
	ITL.Tag_ID,
	LPAD (LibDate.ConvertSecsToHMS (LibDate.IntervalSecond (MAX (ITL2.DStamp) - MIN (ITL.DStamp))), 12) TimeGap
FROM Inventory_Transaction ITL, Inventory_Transaction ITL2, SKU S
WHERE ITL.Tag_ID = ITL2.Tag_ID
AND ITL.Site_ID = ITL2.Site_ID
AND ITL.Code = 'Receipt'
AND ITL2.Code = 'Putaway'
AND ITL.Client_ID = S.Client_ID
AND ITL.SKU_ID = S.SKU_ID
AND (('&2' IS NULL) OR (ITL.Site_ID = '&2'))
AND (('&3' IS NULL) OR (ITL.Client_ID = '&3'))
AND (('&4' IS NULL) OR (ITL.Owner_ID = '&4'))
AND (ITL.DStamp BETWEEN TO_DATE('&5', 'DD-MON-YYYY')
AND TO_DATE('&6', 'DD-MON-YYYY') + 1)
GROUP BY ITL.Site_ID, ITL.Client_ID, ITL.Owner_ID, ITL.SKU_ID, S.Description, ITL.Tag_ID
ORDER BY ITL.Site_ID, ITL.Client_ID, ITL.Owner_ID, ITL.SKU_ID, S.Description, ITL.Tag_ID
;

SET TERMOUT ON
