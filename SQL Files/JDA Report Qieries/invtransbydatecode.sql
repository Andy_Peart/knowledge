/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics and Industrial Systems Ltd*/
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LEXLOGISTiX.                                 */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1995             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/
 
/******************************************************************************/
/*                                                                            */
/*                             LEXLOGISTiX                                    */
/*                                                                            */
/*     FILE NAME  :     invtransbydatecode.sql                                */
/*                                                                            */
/*     DESCRIPTION:     List inventory transactions by date range             */
/*                      and transaction code.                                 */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   30/03/95 DJS  DCS    N/A      initial version                            */
/*   25/04/97 MJT  DCS    PDR5924  Modified to handle european group & decimal*/
/*                                 separators.                                */
/*   07/08/97 MJT  N/A    N/A      Changed number format to display 0         */
/*   06/02/98 JH   DCS    PDR5915  Reports need to be multi-sited             */
/*   27/07/98 MJT  DCS    NEW3188  Third Party Warehousing                    */
/*   18/04/99 JH   DCS    NEW3920  Extra/wider database columns		      */
/*   16/06/99 MJT  DCS    PDR7593  Some sql*plus reports are wrapping         */
/*   26/06/01 MJT  DCS    NEW5486  Client ID added                            */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   06/07/05 JH   DCS    PDR9627  Report problems                            */
/*   04/08/05 DM   DCS    NEW7858  Extend SKU to 30 characters                */
/******************************************************************************/
 
/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF 
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132
 
/* Top Title */
TTITLE CENTER _SITENAME SKIP 2 -
LEFT 'Inventory Transaction By Date And Code' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2 - 
LEFT 'Date Range From &5 To &6 For Code &7' -
SKIP 2 'Date: ' dDate SKIP 2 

/* Column Headings and Formats */
COLUMN Site_Client HEADING 'Site/|Client/|Owner' FORMAT A10 WORD_WRAP
COLUMN DStamp NEW_VALUE dDate  NOPRINT
COLUMN Code_Reason HEADING 'Code/|Reason' FORMAT A15 WORD_WRAP
COLUMN Locations HEADING 'From/To|Locations' FORMAT A10 WORD_WRAP
COLUMN SKU_Tag HEADING 'SKU/|Tag' FORMAT A30 WORD_WRAP
COLUMN Reference HEADING 'Reference/Line' FORMAT A20
COLUMN Station_ID HEADING 'Station/|User' FORMAT A20
COLUMN Update_Quant HEADING 'Update Qty' FORMAT A19

BREAK ON Site_Client SKIP PAGE ON DStamp SKIP 1

/* SQL Select statement */
SELECT	RPAD(Site_ID, 10) || RPAD(Client_ID, 10) || Owner_ID Site_Client,
	DStamp,
	RPAD (Code, 15) || Reason_ID Code_Reason,
	RPAD (NVL (From_Loc_ID, ' '), 10) || To_Loc_ID Locations,
	RPAD (SKU_ID, 30) || Tag_ID SKU_Tag,
	RPAD (NVL (Reference_ID, ' '), 20) || Line_ID Reference,
	RPAD (Station_ID, 20) || User_ID Station_ID,
	TO_CHAR(Update_Qty,'999G999G990D999999') Update_Quant
FROM Inventory_Transaction
WHERE DStamp >= TO_DATE('&5', 'DD-MON-YYYY') 
AND DStamp < TO_DATE('&6', 'DD-MON-YYYY') + 1
AND (('&2' IS NULL) OR (Site_ID = '&2'))
AND (('&4' IS NULL) OR (Owner_ID = '&4'))
AND Code = '&7'
AND ((('&3' IS NULL) AND ('&8' IS NULL))
OR  (Client_ID = '&3')
OR  (('&3' IS NULL) AND (Client_ID IS NULL OR Client_ID IN
	(SELECT Client_ID 
	 FROM Client_Group_Clients
	 WHERE Client_Group = '&8'))))  
AND Summary_Record = 'Y'
ORDER BY Site_ID, Client_ID, Owner_ID, DStamp, Code
; 

SET TERMOUT ON







