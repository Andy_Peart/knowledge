/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics and Industrial Systems Ltd*/
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1998             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/
 
/******************************************************************************/
/*                                                                            */
/*                                 LIS                                        */
/*                                                                            */
/*     FILE NAME  :     invinsuspbyqty.sql                                    */
/*                                                                            */
/*     DESCRIPTION:     List inventory in suspense by largest quantity first. */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   21/01/98 RMW  DCS    N/A      initial version                            */
/*   14/04/99 RMD  DCS    PDR7494  Remove Site Argument                       */
/*   18/04/99 JH   DCS    NEW3920  Extra/wider database columns		      */
/*   16/06/99 MJT  DCS    PDR7593  Some sql*plus reports are wrapping         */
/*   25/06/01 MJT  DCS    NEW5486  Client ID added                            */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   04/08/05 DM   DCS    NEW7858  Extend SKU to 30 characters                */
/*   29/12/05 JH   DCS    BUG2001  Problem with SL_SITE function access       */
/******************************************************************************/
 
/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132

/* Top Title */
TTITLE CENTER _SITENAME SKIP 2 - 
LEFT 'Inventory in Suspense (Ordered by Quantity)' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -                               
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2

/* Avoid duplicate SKUs */
BREAK ON Site_Client_Owner SKIP PAGE ON sDescription SKIP 1 ON Tag_Pallet_Container_ID 

/* Column Headings and Formats */
COLUMN Site_Client_Owner HEADING 'Site/|Client/|Owner' FORMAT A10 WORD_WRAP
COLUMN sDescription HEADING 'SKU/|Description|' FORMAT A39 WORD_WRAP
COLUMN Config_Batch HEADING 'Pack Config/|Batch|' FORMAT A15 WORD_WRAP
COLUMN Origin_Condition HEADING 'Origin/|Condition|' FORMAT A10 WRAP
COLUMN Receipt_Expiry_DStamp HEADING 'Receipt/|Expiry|Dates' FORMAT A11 WORD_WRAP
COLUMN Tag_Pallet_Container_ID HEADING 'Tag/|Pallet/|Container' FORMAT A20 WORD_WRAP
COLUMN Qty_On_Hand HEADING 'Qty On Hand||' FORMAT '999G999G990D999999'
 
/* SQL Select statement */
SELECT	RPAD(NVL(I.Site_ID, '-'), 10) || RPAD(I.Client_ID, 10) || 
	I.Owner_ID Site_Client_Owner, 
	RPAD (I.SKU_ID, 39) || I.Description sDescription,
	RPAD (NVL (I.Config_ID, '-'), 15) || Batch_ID Config_Batch,
	TO_CHAR (I.Receipt_DStamp, 'DD-MON-YYYY') || 
	TO_CHAR (I.Expiry_DStamp, 'DD-MON-YYYY') Receipt_Expiry_DStamp,
	RPAD (NVL (I.Origin_ID, '-'), 10) || I.Condition_ID Origin_Condition,
	RPAD (NVL (Tag_ID, '-'), 20) || RPAD (NVL (I.Pallet_ID, '-'), 20) || 
	I.Container_ID Tag_Pallet_Container_ID,
	I.Qty_On_Hand
FROM Inventory I, Location L
WHERE LibSession.GlobalOptionEnabled('SL_SITE') = 1
AND I.Location_ID = L.Location_ID
AND I.Site_ID = L.Site_ID
AND L.Loc_Type = 'Suspense'
AND (('&5' IS NULL) OR (L.Site_ID = '&5'))
AND (('&2' IS NULL) OR (I.Owner_ID = '&2'))
AND ((('&3' IS NULL) AND ('&4' IS NULL))
OR  (I.Client_ID = '&3')
OR  (('&3' IS NULL) AND I.Client_ID IN (SELECT Client_ID 
				        FROM Client_Group_Clients
					WHERE Client_Group = '&4')))  
UNION
SELECT	RPAD(NVL(I.Site_ID, '-'), 10) || RPAD(I.Client_ID, 10) || 
	I.Owner_ID Site_Client_Owner, 
	RPAD (I.SKU_ID, 39) || I.Description sDescription,
	RPAD (NVL (I.Config_ID, '-'), 15) || Batch_ID Config_Batch,
	TO_CHAR (I.Receipt_DStamp, 'DD-MON-YYYY') || 
	TO_CHAR (I.Expiry_DStamp, 'DD-MON-YYYY') Receipt_Expiry_DStamp,
	RPAD (NVL (I.Origin_ID, '-'), 10) || I.Condition_ID Origin_Condition,
	RPAD (NVL( Tag_ID, '-'), 20) || RPAD (NVL (I.Pallet_ID, '-'), 20) || 
	I.Container_ID Tag_Pallet_Container_ID,
	I.Qty_On_Hand
FROM Inventory I
WHERE LibSession.GlobalOptionEnabled('SL_SITE') = 0
AND I.Location_ID = (SELECT Location_ID
		     FROM Location 
		     WHERE Loc_Type = 'Suspense'
		     AND ROWNUM = 1)
AND I.Site_ID IS NULL
AND (('&2' IS NULL) OR (I.Owner_ID = '&2'))
AND ((('&3' IS NULL) AND ('&4' IS NULL))
OR  (I.Client_ID = '&3')
OR  (('&3' IS NULL) AND I.Client_ID IN (SELECT Client_ID 
				        FROM Client_Group_Clients
					WHERE Client_Group = '&4')))  
ORDER BY 1, 7 DESC, 2, 6;
 
SET TERMOUT ON





