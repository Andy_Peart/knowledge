/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics and Industrial Systems Ltd*/
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LEXLOGISTiX.                                 */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1995             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/
 
/******************************************************************************/
/*                                                                            */
/*                             LEXLOGISTiX                                    */
/*                                                                            */
/*     FILE NAME  :     palletlabel.sql                                       */
/*                                                                            */
/*     DESCRIPTION:     Output the field(s) required for the pallet label     */
/*                      formatter.                                            */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   10/04/95 RMW  DCS    N/A      initial version                            */
/*   16/06/99 MJT  DCS    PDR7593  Some sql*plus reports are wrapping         */
/*   27/06/01 MJT  DCS    NEW5486  Client ID added                            */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/******************************************************************************/
 
/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK OFF
SET VERIFY OFF
SET TAB OFF 
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set headings off, no trailing spaces etc. */
SET PAGESIZE 0
SET NEWPAGE 0
SET HEADING OFF
SET TRIM ON
 
COLUMN Pallet_ID FOLD_AFTER
COLUMN Customer_ID FOLD_AFTER
COLUMN Ship_Dock FOLD_AFTER
COLUMN Consignment FOLD_AFTER
COLUMN Name FOLD_AFTER
COLUMN Address1 FOLD_AFTER
COLUMN Address2 FOLD_AFTER
COLUMN Town FOLD_AFTER
COLUMN County FOLD_AFTER
COLUMN PostCode FOLD_AFTER

/* SQL Select statement */
SELECT	OC.Pallet_ID,
	OH.Customer_ID,
	OH.Ship_Dock,
	OH.Consignment,
	OH.Name,
	OH.Address1,
	OH.Address2,
	OH.Town,
	OH.County,
	OH.PostCode
FROM Order_Container OC, Order_Header OH
WHERE OC.Order_ID = OH.Order_ID
AND OC.Client_ID = OH.Client_ID
AND Pallet_ID = '&2'
AND ROWNUM = 1; 

SET TERMOUT ON





