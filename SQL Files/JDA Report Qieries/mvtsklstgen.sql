/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics & Industrial Systems Ltd  */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1995             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/
 
/******************************************************************************/
/*                                                                            */
/*                                 LIS                                        */
/*                                                                            */
/*     FILE NAME  :     mvtsklstgen.sql                                       */
/*                                                                            */
/*     DESCRIPTION:     Print move task list by list ID and passed in orderby */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   28/05/03 CJD  DCS    NEW6650  Automatic list generation by criteria      */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   19/10/06 JH   DCS    ENH3120  Increase cons/wg to 20 chars               */
/******************************************************************************/

/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 60
SET NEWPAGE 0
SET LINESIZE 132
 
/* Set up Variables */
COLUMN LIST_ID NEW_VALUE VLIST_ID NOPRINT 

/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Move Task By List' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page:' FORMAT 999 SQL.PNO SKIP 2 - 
LEFT 'List ' FORMAT A10 VLIST_ID SKIP 2
 
/* Column Headings and Formats */
COLUMN Site_Client HEADING 'Site/|Client' FORMAT A10 WORD_WRAP
COLUMN Owner_Fromloc HEADING 'Owner/|From|Location' FORMAT A10 WORD_WRAP
COLUMN Qty_Tag HEADING 'Qty/|Tag' FORMAT A20 WRAP
COLUMN sDescription HEADING 'SKU |Description' FORMAT A30 WORD_WRAP
COLUMN To_Final_Loc_ID HEADING 'To|Location/|Final|Location' FORMAT A10
COLUMN Task_ID HEADING 'Task' FORMAT A20 WRAP
COLUMN Work_Group HEADING 'Work Group/|Consignment' FORMAT A20 WRAP
COLUMN Container_Pallet_ID HEADING 'Container/|Pallet' FORMAT A20 WRAP
 
/* leave a blank line between lines */
BREAK ON Site_Client SKIP PAGE ON Owner_ID SKIP PAGE ON ROW SKIP 1

/* SQL Select statement */
SELECT	RPAD (Site_ID, 10) || Client_ID Site_Client,
	RPAD (Owner_ID, 10) || From_Loc_ID Owner_Fromloc,
	List_ID,
	RPAD (SKU_ID, 30) || Description sDescription,
	RPAD(To_Loc_ID, 10) || Final_Loc_ID To_Final_Loc_ID,
	LPAD(TO_CHAR(Qty_To_Move,'999G999G990D999999'), 20) || Tag_ID Qty_Tag,
	Task_ID,
	RPAD (NVL (Work_Group, ' '), 20) || Consignment Work_Group,
	RPAD (NVL (Container_ID, ' '), 20) || Pallet_ID Container_Pallet_ID
FROM Move_Task
WHERE List_ID = '&2'
&3;
 
SET TERMOUT ON


