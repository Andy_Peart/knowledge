/******************************************************************************/
/*                                                                            */
/*   Logistics and Internet Systems Ltd                                       */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics & Internet Systems Ltd    */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Internet   */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Internet Systems Limited, 2003               */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*                                  LIS                                       */
/*                                                                            */
/*     FILE NAME  :     picklists_left.sql                                    */
/*                                                                            */
/*     DESCRIPTION:     Summary of Order Pick Lists that are oustanding       */
/*			giving an indication of when they were printed        */
/*			and the number of hours oustanding                    */
/*                      Useful for Paper based sites                          */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   20/08/03 RMW  DCS    NEW6806  Additional reports from projects           */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   04/05/04 DGP  dcs    PDR9336  Change sysdate to current_timestamp        */
/******************************************************************************/

/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132


/* Top Title */
TTITLE CENTER _SITENAME SKIP 2 -
LEFT 'Pick Lists not Completed' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2

COLUMN Site_ID HEADING 'Site' FORMAT A10
COLUMN Client_ID HEADING 'Client' FORMAT A10
COLUMN Owner_ID HEADING 'Owner' FORMAT A10
COLUMN List_ID HEADING 'List ID'
COLUMN Task_ID HEADING 'Task ID' FORMAT A20
COLUMN Status HEADING 'Task|Status' FORMAT A11
COLUMN Tasks HEADING 'Tasks'
COLUMN CreateTime HEADING 'Creation Date|and Hour' FORMAT A14
COLUMN TimeOver HEADING 'HH:MI:SS Since|Allocation' FORMAT A14

BREAK ON Site_ID SKIP 1 ON Client_ID SKIP 1 ON Owner_ID SKIP 1 ON List_ID SKIP 1 ON Order_ID ON Status

SELECT	MT.Site_ID,
	MT.Client_ID,
	MT.Owner_ID,
	MT.List_ID,
	MT.Task_ID,
	MT.Status,
	COUNT (MT.Key) Tasks,
	TO_CHAR (MT.DStamp, 'DD-MON-YYYY HH24') CreateTime,
	LibDate.ConvertSecsToHMS (LibDate.IntervalSecond (CURRENT_TIMESTAMP - MT.DStamp)) TimeOver
FROM Move_Task MT, Order_Header OH
WHERE MT.Task_ID = OH.Order_ID
AND MT.Site_ID = OH.From_site_ID
AND MT.Client_ID = OH.Client_ID
AND MT.Owner_ID = OH.Owner_ID
AND (('&2' IS NULL) OR (MT.Site_ID = '&2'))
AND (('&3' IS NULL) OR (MT.Client_ID = '&3'))
AND (('&4' IS NULL) OR (MT.Owner_ID = '&4'))
AND (MT.Work_Group LIKE '&5' OR ('&5' IS NULL AND MT.Work_Group IS NULL))
AND (MT.Consignment LIKE '&6' OR ('&6' IS NULL AND MT.Consignment IS NULL))
AND MT.List_ID IS NOT NULL
GROUP BY MT.Site_ID,
	 MT.Client_ID,
	 MT.Owner_ID,
	 MT.List_ID,
	 MT.Task_ID,
	 TO_CHAR (MT.DStamp, 'DD-MON-YYYY HH24'),
	 LibDate.ConvertSecsToHMS (LibDate.IntervalSecond (CURRENT_TIMESTAMP - MT.DStamp)),
	 MT.Status
ORDER BY MT.Site_ID,
	 MT.Client_ID,
	 MT.Owner_ID,
	 MT.List_ID,
	 MT.Task_ID,
	 TO_CHAR (MT.DStamp, 'DD-MON-YYYY HH24'),
	 LibDate.ConvertSecsToHMS (LibDate.IntervalSecond (CURRENT_TIMESTAMP - MT.DStamp)),
	 MT.Status;

SET TERMOUT ON
