/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics and Industrial Systems Ltd*/
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1999             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/
 
/******************************************************************************/
/*                                                                            */
/*                                 LIS                                        */
/*                                                                            */
/*     FILE NAME  :     shippartshort.sql                                     */
/*                                                                            */
/*     DESCRIPTION:     List the shipments made for which there were          */
/*                      allocation shortages.                                 */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   28/06/01 MJT  DCS    NEW5486  Client ID added                            */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   05/08/05 DM   DCS    NEW7858  Extend SKU to 30 chraacters                */
/******************************************************************************/

/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF 

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132

/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Shipped Orders with Allocation Shortages By Date Range' -
RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2
 
/* Column Headings and Formats */
COLUMN Client_ID HEADING 'Client' FORMAT A10
COLUMN Order_ID HEADING 'Order ID ' FORMAT A20
COLUMN SKU_ID HEADING 'SKU' FORMAT A30
COLUMN Description HEADING 'Description' FORMAT A40
COLUMN QtyOrdered HEADING 'Ordered' FORMAT 9999
COLUMN QtyTasked HEADING 'Tasked' FORMAT 9999
COLUMN QtyShipped HEADING 'Shipped' FORMAT 9999

BREAK ON Client_ID ON Order_ID SKIP 1 


SELECT  GS.Client_ID Client_ID,
	GS.Task_ID Order_ID, 
	--RPAD (OL.SKU_ID, 40) || S.Description SKUIDd,
	ol.sku_id sku_id,
	s.description description,
	GS.Qty_Ordered QtyOrdered, 
	GS.Qty_Tasked QtyTasked,
	OL.Qty_Shipped QtyShipped
FROM Generation_Shortage GS, Order_Line OL, SKU S
WHERE OL.Order_ID = GS.Task_ID
AND OL.Client_ID = GS.Client_ID
AND OL.Line_ID = GS.Line_ID
AND S.SKU_ID = OL.SKU_ID
AND S.Client_ID = OL.Client_ID
AND GS.TASK_ID IN
	(SELECT DISTINCT OL.Order_ID 
	 FROM Order_Line OL 
	 WHERE NVL (OL.Qty_Shipped, 0) > 0
	 AND OL.Order_ID = GS.Task_ID
	 AND OL.Client_ID = GS.Client_ID)
ORDER BY GS.Client_ID, S.SKU_ID
/

SET TERMOUT ON



