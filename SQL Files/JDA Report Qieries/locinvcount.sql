/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics & Industrial Systems Ltd  */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1998             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/
/******************************************************************************/
/*                                                                            */
/*                                  LIS                                       */
/*                                                                            */
/*     FILE NAME  :     locinvcount.sql                                       */
/*                                                                            */
/*     DESCRIPTION:     List the locations (and SKUs in the location) having  */
/*                      more than X different SKUs in the location.           */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   28/04/98 RMW  DCS    N/A      initial version                            */
/*   27/07/98 MJT  DCS    NEW3188  Third Party Warehousing                    */
/*   16/06/99 MJT  DCS    PDR7593  Some sql*plus reports are wrapping         */
/*   26/06/01 MJT  DCS    NEW5486  Client ID added                            */
/*   29/08/03 DL   DCS    PDR9074  Various reporting issues                   */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   25/06/04 RMC  DCS    PDR9092  Change reports for VAS and beam            */
/*   04/08/05 DM   DCS    NEW7858  Extend SKU to 30 characters                */
/*   18/09/06 JH   DCS    BUG3091  Make work with same loc id in diff sites   */
/******************************************************************************/

/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF 
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132
 
/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Storage locations containing more than &5 SKUs' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2

BREAK ON Site_Client SKIP PAGE ON Owner_ID SKIP PAGE ON Location_ID SKIP 1 ON Loc_Type ON Lock_Status ON Volume ON Used_Volume
 
/* Column Headings and Formats */
COLUMN Site_Client HEADING 'Site/|Client' FORMAT A10 WORD_WRAP
COLUMN Owner_ID HEADING 'Owner' FORMAT A10
COLUMN Location_ID HEADING 'Location|Beam ID' FORMAT A15
COLUMN Loc_Type HEADING 'Location Type|Beam Position' FORMAT A15
COLUMN Lock_Status HEADING 'Lock|Status' FORMAT A9
COLUMN Volume_Used_Volume HEADING 'Volume/|Used Volume' FORMAT A17
COLUMN SKU_ID HEADING 'SKU ID' FORMAT A30
COLUMN Qty_On_Hand HEADING 'Qty On Hand' FORMAT A19

/* SQL Select statement */
SELECT	/*+ FIRST_ROWS */ RPAD(L.Site_ID, 10) || I.Client_ID Site_Client,
	I.Owner_ID,
	RPAD(L.Location_ID, 15) || RPAD(L.Beam_ID, 15) Location_ID,
	RPAD(L.Loc_Type, 15) || RPAD(L.Beam_Position, 15) Loc_Type,
	L.Lock_Status, 
	TO_CHAR(L.Volume, '9G999G999D999999') || 
	TO_CHAR((L.Current_Volume + NVL (L.Alloc_Volume, 0.0)), '9G999G999D999999') Volume_Used_Volume,
	I.SKU_ID, 
	TO_CHAR(I.Qty_On_Hand, '999G999G990D999999') Qty_On_Hand
FROM Inventory I, Location L
WHERE I.Location_ID = L.Location_ID
AND ((I.Site_ID IS NULL AND L.Site_ID IS NULL) OR (I.Site_ID = L.Site_ID))
AND (('&2' IS NULL) OR (I.Site_ID = '&2'))
AND (('&4' IS NULL) OR (I.Owner_ID = '&4'))
AND L.Loc_Type IN ('Bin', 'Bulk', 'Tag-FIFO', 'Tag-LIFO', 'Tag-Operator')
AND L.Location_ID IN (SELECT /*+ FIRST_ROWS */ Location_ID 
		      FROM Inventory I
		      WHERE I.Site_ID = L.Site_ID
		      AND I.Location_ID = L.Location_ID
		      AND (('&4' IS NULL) OR (Owner_ID = '&4')) 
		      AND ((('&3' IS NULL) AND ('&6' IS NULL))
		      OR  (Client_ID = '&3')
		      OR  (('&3' IS NULL) AND Client_ID IN
			(SELECT Client_ID 
			FROM Client_Group_Clients
			WHERE Client_Group = '&6')))  
		      GROUP BY Location_ID
		      HAVING COUNT (DISTINCT(SKU_ID)) > &5)
ORDER BY L.Site_ID, I.Client_ID, I.Owner_ID, L.Location_ID, L.Loc_Type, L.Lock_Status, L.Volume, L.Current_Volume + NVL (L.Alloc_Volume, 0.0), I.SKU_ID;
 
SET TERMOUT ON

