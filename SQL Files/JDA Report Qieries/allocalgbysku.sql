/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics & Industrial Systems Ltd  */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1995             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*                                 LIS                                        */
/*                                                                            */
/*     FILE NAME  :     allocalgbysku.sql                                     */
/*                                                                            */
/*     DESCRIPTION:     List the sku allocation algorithms.		      */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   08/06/95 RMW  DCS    N/A      initial version                            */
/*   11/11/98 JH   DCS    NEW3173  ABC/pareto analysis                        */
/*   16/06/99 MJT  DCS    PDR7593  Some sql*plus reports are wrapping         */
/*   21/02/00 RMW  DCS    NEW4070  Crossdock enhancements                     */
/*   21/06/01 MJT  DCS    NEW5486  Client ID added                            */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   03/08/05 DM   DCS    NEW7858  Increase SKU to 30 charactres              */
/******************************************************************************/

/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132

/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'SKU Allocation Algorithms By SKU Range' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2 -
LEFT 'SKU Range From &4 To &5' SKIP 2

BREAK ON Site_Client SKIP PAGE ON SKU_Priority SKIP 1

/* Column Headings and Formats */
COLUMN Site_Client HEADING 'Site/|Client' FORMAT A10 WORD_WRAP
COLUMN SKU_Priority HEADING 'SKU/|Priority' FORMAT A30 WORD_WRAP
COLUMN Algorithm HEADING 'Algorithm' FORMAT A60
COLUMN Zone_1 HEADING 'Zone' FORMAT A10
COLUMN FinalLocZone HEADING 'Final|Locn|Zone' FORMAT A5
COLUMN SplitInventory HEADING 'Split|Inv' FORMAT A5
COLUMN Min_Due_Pick_Mins HEADING 'Minimum|Pick Time' FORMAT 9999

/* SQL Select statement for the sku algorithms */
SELECT	RPAD(SA.Site_ID, 10) || SA.Client_ID Site_Client,
	RPAD(SA.SKU_ID, 18) || SA.Priority SKU_Priority,
	L.Text Algorithm,
	SA.Zone_1,
	NVL(SA.Final_Loc_Zone, 'N') FinalLocZone,
	NVL(SA.Split_Inventory, 'N') SplitInventory,
	SA.Min_Due_Pick_Mins
FROM Language_Text L, SKU_Allocation SA
WHERE L.Label = SA.Algorithm
AND L.Language = 'EN_GB'
AND SA.SKU_ID BETWEEN '&4' AND '&5'
AND (('&2' IS NULL) OR (SA.Site_ID = '&2'))
AND ((('&3' IS NULL) AND ('&6' IS NULL))
OR  (SA.Client_ID = '&3')
OR  (('&3' IS NULL) AND SA.Client_ID IN
	(SELECT Client_ID 
	 FROM Client_Group_Clients
	 WHERE Client_Group = '&6')))  
ORDER BY 1, 2, 3, 4;

SET TERMOUT ON

