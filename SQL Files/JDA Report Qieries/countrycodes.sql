/******************************************************************************/
/*                                                                            */
/*   Logistics & Industrial Systems Ltd                                       */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics & Industrial Systems Ltd  */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics & Industrial   */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics & Industrial Systems Limited, 2001               */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*                                  LIS                                       */
/*                                                                            */
/*     FILE NAME  :     country.sql                                           */
/*                                                                            */
/*     DESCRIPTION:     Output ISO3 country codes and country name            */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   12/02/01 RMW  DCS    N/A      initial version                            */
/*   11/12/03 DGP  DCS    NEW6997  DutyMaster Customs Interface Reports       */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   03/03/05 DGP  DCS    NEW7632  CE EU type column not translated           */
/******************************************************************************/

/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132
SET TAB OFF

/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Countries - ISO Codes and Names' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2

/* Column Headings and Formats */
COLUMN ISO3_ID HEADING 'ISO-3 Code' FORMAT A10
COLUMN ISO2_ID HEADING 'ISO-2 Code' FORMAT A10
COLUMN CountryName HEADING 'Country Name' FORMAT A60
COLUMN CE_EU_Type HEADING 'Customs EU Type' FORMAT A15


/* SQL Select statement */
SELECT	ISO3_ID, ISO2_ID,
	LibLanguage.GetTranslation ('WLK' || ISO3_ID) CountryName,
	CE_EU_Type
FROM Country
ORDER BY ISO3_ID
;

SET TERMOUT ON
