/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics & Industrial Systems Ltd  */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LEXLOGISTiX.                                 */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1996             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*                                  LIS                                       */
/*                                                                            */
/*     FILE NAME  :     preadvbydate.sql                                       */
/*                                                                            */
/*     DESCRIPTION:     List the pre-advice data by due date.                 */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   28/12/95 RMW  DCS    N/A      initial version                            */
/*   25/04/97 MJT  DCS    PDR5924  Modified to handle european group & decimal*/
/*                                 separators.                                */
/*   07/08/97 MJT  N/A    N/A      Changed number format to display 0         */
/*   06/02/98 JH   DCS    PDR5915  Reports need to be multi-sited             */
/*   28/07/98 MJT  DCS    NEW3188  Third Party Warehousing                    */
/*   16/06/99 MJT  DCS    PDR7593  Some sql*plus reports are wrapping         */
/*   12/12/00 RMC  DCS    NEW3998  Remove BookRef ID for Dock Scheduler       */
/*   27/06/01 MJT  DCS    NEW5486  Client ID added                            */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   05/08/05 DM   CLOCIT NEW7858  Extend SKU to 30 characters                */
/******************************************************************************/

/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132

/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Pre-Advices By Date Range' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2 - 
LEFT 'Date From &5 To &6' SKIP 2
 
BREAK ON Site_Client SKIP PAGE ON Pre_Advice_ID SKIP 1
 
/* Column Headings and Formats */
COLUMN Site_Client HEADING 'Site/|Client' FORMAT A10 WORD_WRAP
COLUMN Owner_Status HEADING 'Owner/|Status' FORMAT A11 WORD_WRAP
COLUMN cTaskID HEADING 'Pre-Advice/|Line'  FORMAT A20
COLUMN sDescription HEADING 'SKU/|Description' FORMAT A32 WRAP
COLUMN Config_ID HEADING 'Pack Config/|Cond Code' FORMAT A15
COLUMN Quant_Due HEADING 'Qty Due' FORMAT A19
COLUMN cQtyReceived HEADING 'Qty Received' FORMAT A19

SELECT	RPAD (PAH.Site_ID, 10) || PAH.Client_ID Site_Client,
	RPAD (PAH.Owner_ID, 11) || PAH.Status Owner_Status,
	RPAD (PAH.Pre_Advice_ID, 20) || PAL.Line_id cTaskID,
	RPAD (PAL. Sku_ID, 32) || S.Description sDescription,
	RPAD (NVL (PAL.Config_ID, ' '), 15) || PAL.Condition_ID Config_ID,
	TO_CHAR(PAL.Qty_Due,'999G999G990D999999') Quant_Due,
	TO_CHAR(NVL (PAL.Qty_Received, 0),'999G999G990D999999') cQtyReceived
FROM SKU S, Pre_Advice_Line PAL, Pre_Advice_Header PAH
WHERE PAL.Pre_Advice_id = PAH.Pre_Advice_ID
AND PAL.Client_ID = PAH.Client_ID
AND PAL.SKU_ID = S.SKU_ID
AND PAL.Client_ID = S.Client_ID
AND PAH.Due_DStamp >= TO_DATE('&5', 'DD-MON-YYYY') 
AND PAH.Due_DStamp < TO_DATE('&6', 'DD-MON-YYYY') + 1
AND (('&2' IS NULL) OR (PAH.Site_ID = '&2'))
AND (('&4' IS NULL) OR (PAH.Owner_ID = '&4'))
AND ((('&3' IS NULL) AND ('&7' IS NULL))
OR  (PAH.Client_ID = '&3')
OR  (('&3' IS NULL) AND PAH.Client_ID IN
(SELECT Client_ID 
FROM Client_Group_Clients
WHERE Client_Group = '&7')))  
ORDER BY PAH.Site_ID, PAH.Client_ID, PAH.Owner_ID, PAH.Due_DStamp,
PAH.Pre_Advice_ID, PAL.Line_ID
;





