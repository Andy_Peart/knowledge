/******************************************************************************/
/*                                                                            */
/*   Logistics and Internet Systems Ltd                                       */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics & Internet Systems Ltd    */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Internet   */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Internet Systems Limited, 2003               */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*                                  LIS                                       */
/*                                                                            */
/*     FILE NAME  :     in_and_out.sql                                        */
/*                                                                            */
/*     DESCRIPTION:     Summary of number of Tasks, Eachs, Pallets,           */
/*			Containers and Tags Received and Shipped 	      */
/*                      by date range                                         */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   20/08/03 RMW  DCS    NEW6806  Additional reports from projects           */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/******************************************************************************/

/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132


BREAK ON REPORT

COLUMN GroupDate NOPRINT
COLUMN Site_ID NOPRINT
COLUMN Client_ID NOPRINT
COLUMN Owner_ID NOPRINT
COLUMN Site_Client_Owner HEADING 'Site/|Client/|Owner' FORMAT A10 WORD_WRAP
COLUMN D HEADING 'Date'
COLUMN E HEADING 'Receipts'
COLUMN F HEADING 'Shipments'
COLUMN G HEADING 'Eaches|Received' format 99999999
COLUMN H HEADING 'Eaches|Shipped' format 99999999
COLUMN I HEADING 'Pallet|Received'
COLUMN J HEADING 'Pallet|Shipped'
COLUMN K HEADING 'Container|Received'
COLUMN L HEADING 'Container|Shipped'
COLUMN M HEADING 'Tags|Received'
COLUMN N HEADING 'Tags|Shipped'

BREAK ON Site_Client_Owner SKIP PAGE

COMPUTE SUM LABEL 'Total' OF E ON ITL.Site_ID
COMPUTE SUM LABEL 'Total' OF F ON ITL.Site_ID
COMPUTE SUM LABEL 'Total' OF G ON ITL.Site_ID
COMPUTE SUM LABEL 'Total' OF H ON ITL.Site_ID
COMPUTE SUM LABEL 'Total' OF I ON ITL.Site_ID
COMPUTE SUM LABEL 'Total' OF J ON ITL.Site_ID
COMPUTE SUM LABEL 'Total' OF K ON ITL.Site_ID
COMPUTE SUM LABEL 'Total' OF L ON ITL.Site_ID
COMPUTE SUM LABEL 'Total' OF M ON ITL.Site_ID
COMPUTE SUM LABEL 'Total' OF N ON ITL.Site_ID

/* Top Title */
TTITLE CENTER _SITENAME SKIP 2 -
LEFT "Inbound and Outbound Summary between &4 and &5" -
RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2


SELECT	ITL.Site_ID,
	ITL.Client_ID,
	ITL.Owner_ID,
	RPAD (Site_ID, 10) || RPAD(Client_ID, 10) || Owner_ID Site_Client_Owner,
	TO_CHAR (ITL.DStamp, 'DD-MON-YYYY') D,
	NVL (Q1.E1,0) E,
	NVL (Q2.E2,0) F,
	NVL (Q1.F1,0) G,
	NVL (Q2.F2,0) H,
	NVL (Q1.G1,0) I,
	NVL (Q2.G2,0) J,
	NVL (Q1.H1,0) K,
	NVL (Q2.H2,0) L,
	NVL (Q1.I1,0) M,
	NVL (Q2.I2,0) N,
	TO_CHAR (ITL.DStamp, 'YYYYMMDD') GROUPDATE
FROM Inventory_Transaction ITL,
       (SELECT ITL2.Site_ID A1,
	ITL2.Client_ID B1,
	ITL2.Owner_ID C1,
	TO_CHAR (ITL2.DStamp, 'DD-MON-YYYY') D1,
	COUNT (ITL2.Key) E1,
	SUM (NVL (ITL2.Update_Qty, 0)) F1,
	COUNT (DISTINCT (ITL2.Pallet_ID)) G1,
	COUNT (DISTINCT (ITL2.Container_ID)) H1,
	COUNT (DISTINCT (ITL2.Tag_ID)) I1
	FROM Inventory_Transaction ITL2
	WHERE ITL2.Code = 'Receipt'
	AND (ITL2.Site_ID = '&2' OR '&2' IS NULL)
	AND (ITL2.Client_ID = '&3' OR '&3' IS NULL)
	AND ITL2.DStamp >= TO_DATE('&4', 'DD-MON-YYYY')
	AND ITL2.DStamp < TO_DATE('&5', 'DD-MON-YYYY') + 1
	GROUP BY ITL2.Site_ID,
		 ITL2.Client_ID,
		 ITL2.Owner_ID,
		 TO_CHAR (ITL2.DStamp, 'DD-MON-YYYY')) Q1,
       (SELECT	ITL3.Site_ID A2,
		ITL3.Client_ID B2,
		ITL3.Owner_ID C2,
		TO_CHAR (ITL3.DStamp, 'DD-MON-YYYY') D2,
		COUNT (ITL3.Key) E2,
		SUM (NVL (ITL3.Update_Qty, 0)) F2,
		COUNT (DISTINCT (ITL3.Pallet_ID)) G2,
		COUNT (DISTINCT (ITL3.Container_ID)) H2,
		COUNT (DISTINCT (ITL3.Tag_ID)) I2
	FROM Inventory_Transaction ITL3
	WHERE ITL3.Code = 'Shipment'
	AND (ITL3.Site_ID = '&2' OR '&2' IS NULL)
	AND (ITL3.Client_ID = '&3' OR '&3' IS NULL)
	AND ITL3.DStamp >= TO_DATE('&4', 'DD-MON-YYYY')
	AND ITL3.DStamp < TO_DATE('&5', 'DD-MON-YYYY') + 1
	GROUP BY ITL3.Site_ID,
		 ITL3.Client_ID,
		 ITL3.Owner_ID,
		 TO_CHAR (ITL3.DStamp, 'DD-MON-YYYY')) Q2
WHERE ITL.Code IN ('Receipt', 'Shipment')
AND (ITL.Site_ID = '&2' OR '&2' IS NULL)
AND (ITL.Client_ID = '&3' OR '&3' IS NULL)
AND ITL.Site_ID = Q1.A1 (+)
AND ITL.Site_ID = Q2.A2 (+)
AND ITL.Client_ID = Q1.B1 (+)
AND ITL.Client_ID = Q2.B2 (+)
AND ITL.Owner_ID = Q1.C1 (+)
AND ITL.Owner_ID = Q2.C2 (+)
AND ITL.DStamp >= TO_DATE('&4', 'DD-MON-YYYY')
AND ITL.DStamp < TO_DATE('&5', 'DD-MON-YYYY') + 1
AND TO_CHAR (ITL.Dstamp, 'DD-MON-YYYY') = Q1.D1 (+)
AND TO_CHAR (ITL.Dstamp, 'DD-MON-YYYY') = Q2.D2 (+)
GROUP BY ITL.Site_ID, 
	 ITL.Client_ID, 
	 ITL.Owner_ID,
	 RPAD (Site_ID, 10) || RPAD (Client_ID, 10) || Owner_ID,
	 TO_CHAR (ITL.DStamp, 'DD-MON-YYYY'),
	 Q1.E1,
	 Q2.E2,
	 Q1.F1,
	 Q2.F2,
	 Q1.G1,
	 Q2.G2,
	 Q1.H1,
	 Q2.H2,
	 Q1.I1,
	 Q2.I2,
	 TO_CHAR (ITL.DStamp, 'YYYYMMDD')
ORDER BY 4, 16;

SET TERMOUT ON
