/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics & Industrial Systems Ltd  */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1995             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/
 
/******************************************************************************/
/*                                                                            */
/*                                 LIS                                        */
/*                                                                            */
/*     FILE NAME  :     picktasklabel.sql
/*                                                                            */
/*     DESCRIPTION:     Output the field(s) required for the pick label       */
/*                      formatter.                                            */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   20/07/05 CL   DCS    N/A      initial version                            */
/*   04/08/05 DM   DCS    NEW7858  Increase SKU to 30 characters              */
/*   19/10/06 JH   DCS    ENH3120  Increase cons/wg to 20 chars               */
/******************************************************************************/
 
/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132
 
/* Column Headings and Formats */
COLUMN  sorter  NOPRINT
COLUMN  sorter2 NOPRINT
COLUMN  sorter3 NOPRINT
COLUMN Print_Label_ID HEADING LABEL_ID FORMAT '9999999999'
COLUMN Task_ID HEADING ORDER_ID FORMAT A20
COLUMN Site_ID  FORMAT A15
COLUMN SKUDes HEADING 'SKU/|Description' FORMAT A30
COLUMN Description FORMAT A30
COLUMN Tag_ID FORMAT A15
COLUMN Consignment FORMAT A20
COLUMN Qty_To_Move HEADING QUANTITY FORMAT '999G990D999999'
 
/* SQL Select statement */
SELECT  Distinct MT.CLIENT_ID||MT.TASK_ID||lpad(MT.LINE_ID, 6) sorter,
	MT.CLIENT_ID||NVL(MT.SEQUENCE,0) sorter2,
	MT.CLIENT_ID||MT.PRINT_LABEL_ID sorter3,
	MT.Print_Label_ID,
	MT.Task_ID,
	MT.Site_ID,
	RPAD(MT.SKU_ID, 30) || S.Description SKUDes,
	MT.Tag_ID,
	MT.Consignment,
	MT.Qty_To_Move
FROM Move_Task MT, SKU S
WHERE MT.Site_ID = '&&3'
AND MT.Client_ID = '&&2'
AND ((MT.Work_Zone = '&&4' AND (MT.Work_Group = '&&5' OR MT.Consignment = '&&6'))
OR (MT.Work_Zone = '&&4' AND '&&5' is NULL AND '&&6' IS NULL)
OR (MT.Print_Label_ID >= '&&7' AND MT.Print_Label_ID <= '&&8'))
AND S.SKU_ID = MT.SKU_ID
AND S.Client_ID = MT.Client_ID
AND MT.Task_Type = 'O'
ORDER BY '&&9'
; 
 
SET TERMOUT ON
 
