/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics & Industrial Systems Ltd  */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1996             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/
 
/******************************************************************************/
/*                                                                            */
/*                                  LIS                                       */
/*                                                                            */
/*     FILE NAME  :     putalgglobal.sql                                      */
/*                                                                            */
/*     DESCRIPTION:     List the global putaway algorithms.                   */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   08/11/96 RMW  N/A    N/A      initial version                            */
/*   06/02/98 JH   DCS    PDR5915  Reports need to be multi-sited             */
/*   12/12/98 JH   DCS    NEW3173  ABC/pareto analysis			      */
/*   18/04/99 JH   DCS    NEW3920  Extra/wider database columns		      */
/*   16/06/99 MJT  DCS    PDR7593  Some sql*plus reports are wrapping         */
/*   02/02/00 RMC  DCS    PDR8095  Incorrect formatting on Zone/Subzone fields*/
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/******************************************************************************/
 
/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON 
SET VERIFY OFF
SET TAB OFF
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132
 
/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Global Putaway Algorithms' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2
 
/* Column Headings and Formats */
COLUMN Site_ID HEADING 'Site' FORMAT A10
COLUMN Priority HEADING 'Pri' FORMAT 999
COLUMN Text HEADING 'Algorithm' FORMAT A40 WRAP
COLUMN OwnerMix HEADING 'Owner/Mix|Origin/Mix' FORMAT A10 WRAP
COLUMN ConditionMix HEADING 'Cond/|Mix' FORMAT A5 WRAP
COLUMN FromLocZone HEADING 'From|Loc|Zone' FORMAT A4
COLUMN Zones HEADING 'Zone/|SubZne1/|SubZne2' FORMAT A10 WRAP
COLUMN InVolume HEADING 'In|Vol' FORMAT A4
COLUMN PartPallet HEADING 'Part|Pall' FORMAT A4
COLUMN PalletSearch HEADING 'Pall|Srch' FORMAT A4
COLUMN SKUSearch HEADING 'SKU|Srch' FORMAT A4
COLUMN TopUp HEADING 'Top|Up' FORMAT A4
COLUMN SKUMix HEADING 'SKU|Mix' FORMAT A4
COLUMN BatchMix HEADING 'Btch|Mix' FORMAT A4
COLUMN Mix_Days HEADING 'Mix|Days' FORMAT 9999

BREAK ON Site_ID SKIP PAGE
 
/* SQL Select statement */
SELECT Site_ID,
	Priority,
	LT.Text,
	RPAD(NVL(Owner_ID, ' '), 10) || RPAD(NVL(Owner_Mix, 'N'), 10) || RPAD(NVL(Origin_ID, ' '), 10) || NVL(Origin_Mix, 'N') OwnerMix,
	RPAD(NVL(Condition_ID, ' '), 5) || NVL(Condition_Mix, 'N') ConditionMix,
	NVL(From_Loc_Zone, 'N') FromLocZone,
	RPAD(Zone_1, 10) || RPAD(Subzone_1, 10) || Subzone_2 Zones,
	NVL(In_Volume, 'N') InVolume,
	NVL(Part_Pallet, 'N') PartPallet,
	NVL(Pallet_Search, 'N') PalletSearch,
	NVL(SKU_Search, 'N') SKUSearch,
	NVL(Top_Up, 'N') TopUP,
	NVL(SKU_Mix, 'N') SKUMix,
	NVL(Batch_Mix, 'N') BatchMix,
	Mix_Days
FROM Global_Putaway GP, Language_Text LT
WHERE GP.Algorithm = LT.Label
AND LT.Language = 'EN_GB'
AND (('&2' IS NULL) OR (GP.Site_ID = '&2'))
ORDER BY 1, 2;
 
SET TERMOUT ON
 
