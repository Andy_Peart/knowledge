/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics and Industrial Systems Ltd*/
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1996             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/
 
/******************************************************************************/
/*                                                                            */
/*                                 LIS                                        */
/*                                                                            */
/*     FILE NAME  :     customstagmerge.sql                                   */
/*                                                                            */
/*     DESCRIPTION:     List Stock Check tasks within X days of doing the     */
/*                      Stock Check and have not yet gone up to DutyMaster.   */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   14/02/06 DGP  DCS    ENH2044  Initial version                            */
/******************************************************************************/
 
/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132

/* Top Title */
TTITLE CENTER _SITENAME SKIP 2 - 
LEFT 'Customs Stock Check Tasks done in the past &4 Days that have not been sent upto DutyMaster' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -                               
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2

/* Column Headings and Formats */
COLUMN Site_ID HEADING 'Site ID' FORMAT A10 WORD_WRAP
COLUMN Client_ID HEADING 'Client ID' FORMAT A10 WORD_WRAP
COLUMN From_Loc_ID HEADING 'Location' FORMAT A10 WORD_WRAP
COLUMN sDescription HEADING 'SKU/|Description' FORMAT A30 WORD_WRAP
COLUMN Tag_ID HEADING 'Tag ID' FORMAT A20
COLUMN CE_Rotation_ID HEADING 'Rotation ID' FORMAT A11
 
/* Compute sums */
 
/* SQL Select statement */
SELECT	ITL.Site_ID, ITL.Client_ID, ITL.From_Loc_ID,
	RPAD (S.SKU_ID, 30) || S.Description sDescription,
	ITL.Tag_ID, ITL.CE_Rotation_ID
FROM Inventory_Transaction ITL, SKU S
WHERE CURRENT_TIMESTAMP - &4 >= ITL.DStamp
AND (('&2' IS NULL) OR (ITL.Site_ID = '&2'))
AND ((('&3' IS NULL) AND ('&5' IS NULL))
OR  (ITL.Client_ID = '&3')
OR  (('&3' IS NULL) AND (ITL.Client_ID IS NULL OR ITL.Client_ID IN
        (SELECT Client_ID
         FROM Client_Group_Clients
         WHERE Client_Group = '&5'))))
AND S.Client_ID = ITL.Client_ID 
AND S.SKU_ID = ITL.SKU_ID 
AND NVL(S.CE_Customs_Excise, 'N') = 'Y' 
AND ITL.Code = 'Stock Check' 
AND NVL(ITL.Uploaded_Customs, 'N') = 'N' 
ORDER BY ITL.Tag_ID, ITL.Site_ID, ITL.Client_ID, ITL.SKU_ID;
 
SET TERMOUT ON
 
