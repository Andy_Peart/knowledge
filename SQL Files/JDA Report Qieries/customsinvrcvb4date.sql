/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics and Industrial Systems Ltd*/
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1996             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/
 
/******************************************************************************/
/*                                                                            */
/*                                 LIS                                        */
/*                                                                            */
/*     FILE NAME  :     customsinvrcvb4date.sql                               */
/*                                                                            */
/*     DESCRIPTION:     Customs & Excise - List inventory recived before a    */
/*                      specified date.                                       */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   11/12/03 DGP  DCS    NEW6997  Initial Version.                           */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   03/08/05 DM   DCS    NEW7858  Increase SKU to 30 charactres              */
/******************************************************************************/
 
/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132

/* Top Title */
TTITLE CENTER _SITENAME SKIP 2 - 
LEFT 'Customs and Excise - Inventory Received Before &5' RIGHT _DATESTAMP SKIP 1 -        
RIGHT _TIMESTAMP SKIP 1 -                               
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2

/* Avoid duplicate SKUs */
BREAK ON Site_Client SKIP PAGE ON Owner_ID ON sDescription SKIP 1 ON Location_ID

/* Column Headings and Formats */
COLUMN Site_Client HEADING 'Site/|Client' FORMAT A10 WORD_WRAP
COLUMN Owner_Location Heading 'Owner/|Location' FORMAT A10 WORD_WRAP
COLUMN sDescription HEADING 'SKU/|Description' FORMAT A30 WORD_WRAP
COLUMN Config_ID HEADING 'Pack Config/|Cond Code' FORMAT A15 WRAP
COLUMN Receipt_Expiry_DStamp HEADING 'Receipt/|Expiry|Dates' FORMAT A11 WRAP
COLUMN Tag_Batch HEADING 'Tag/|Batch' FORMAT A20 WORD_WRAP
COLUMN Qty_On_Hand HEADING 'Qty On Hand' FORMAT '999G999G990D999999'
COLUMN Qty_Allocated HEADING 'Qty Allocated' FORMAT '999G999G990D999999'
COLUMN Rotation_Consignment HEADING 'Rotation ID/|Consignment ID' FORMAT A12 WORD_WRAP
COLUMN Receipt_UCR HEADING 'Receipt Type/|Unique Consign Ref' FORMAT A2 WORD_WRAP
COLUMN Originator HEADING 'Originator/|Originator Ref' FORMAT A15 WORD_WRAP
COLUMN Customs_Country HEADING 'Country of Originator/|Country Whence Consigned' FORMAT A2 WORD_WRAP
COLUMN Under_Bond_Date HEADING 'Under Bond/|Document Date' FORMAT A1 WORD_WRAP
 
/* Compute sums */
COMPUTE SUM OF Qty_On_Hand Qty_Allocated ON sDescription
 
/* SQL Select statement */
SELECT	RPAD (I.Site_ID, 10) || I.Client_ID Site_Client,
	RPAD(I.Owner_ID, 10) || I.Location_ID Owner_Location, 
	RPAD (I.SKU_ID, 30) || I.Description sDescription,
	RPAD (NVL (I.Config_ID, ' '), 15) || I.Condition_ID Config_ID,
	TO_CHAR (I.Receipt_DStamp, 'DD-MON-YYYY') || 
	TO_CHAR (I.Expiry_DStamp, 'DD-MON-YYYY') Receipt_Expiry_DStamp,
	RPAD(NVL(I.Tag_ID, ' '), 20) || I.Batch_ID Tag_Batch,
	I.Qty_On_Hand,
	I.Qty_Allocated,
	RPAD (I.CE_Rotation_ID, 12) || 
	I.CE_Consignment_ID Rotation_Consignment,
	RPAD (I.CE_Receipt_Type, 2) || I.CE_UCR Receipt_UCR,
	RPAD (NVL (I.CE_Originator, ' '), 15) || 
	I.CE_Originator_Reference Originator,
	RPAD (NVL (I.CE_COO, ' '), 2) || I.CE_CWC Customs_Country,
	NVL (I.CE_Under_Bond, ' ') || 
	TO_CHAR (I.CE_Document_DStamp, 'DD-MON-YYYY') Under_Bond_Date
FROM Inventory I
WHERE I.Receipt_DStamp < TO_DATE('&5', 'DD-MON-YYYY')
AND (('&2' IS NULL) OR (I.Site_ID = '&2'))
AND (('&4' IS NULL) OR (I.Owner_ID = '&4'))
AND ((('&3' IS NULL) AND ('&6' IS NULL))
OR  (I.Client_ID = '&3')
OR  (('&3' IS NULL) AND I.Client_ID IN
	(SELECT Client_ID 
	 FROM Client_Group_Clients
	 WHERE Client_Group = '&6')))  
ORDER BY I.Site_ID, I.Client_ID, I.Owner_ID, I.SKU_ID, I.Location_ID, I.Receipt_DStamp;
 
SET TERMOUT ON
