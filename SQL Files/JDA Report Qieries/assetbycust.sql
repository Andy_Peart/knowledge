/******************************************************************************/
/*                                                                            */
/*   Logistics and Internet Systems Ltd                                       */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics and Internet Systems Ltd  */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Internet   */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Internet Systems Limited, 2001               */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*                                 LIS                                        */
/*                                                                            */
/*     FILE NAME  :     suppquestions.sql                                     */
/*                                                                            */
/*     DESCRIPTION:     List shipped assets for a customer                    */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   27/02/07 RMD  DCS    ENH3356  Initial Version                            */
/******************************************************************************/

/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132

/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Shipped Assets - by Customer' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2 -
LEFT 'Customer: &3' SKIP 2

/* Column Headings and Formats */
COLUMN AssetID HEADING 'Asset ID' FORMAT A20
COLUMN PalletConfig HEADING 'Pallet Type' FORMAT A15
COLUMN ShippedDate HEADING 'Shipped Date/Time' FORMAT A18

/* BREAK ON ClientID SKIP PAGE ON Supplier_ID SKIP 1 */

/* SQL Select statement */
SELECT 	Asset_ID AssetID, 
	Pallet_Config PalletConfig,
	TO_CHAR (Shipped_DStamp, 'DD-MON-YYYY HH24:MI') ShippedDate
	FROM Asset_Details
	WHERE Client_ID = '&2'
	AND Customer_ID = '&3'
	AND Status = 'Shipped'
	ORDER BY Shipped_DStamp ASC, Pallet_Config, Asset_ID;

SET TERMOUT ON

