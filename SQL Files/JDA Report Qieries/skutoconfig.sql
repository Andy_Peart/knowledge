/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics & Industrial Systems Ltd  */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LEXLOGISTiX.                                 */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1995             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/
 
/******************************************************************************/
/*                                                                            */
/*                             LEXLOGISTiX                                    */
/*                                                                            */
/*     FILE NAME  :     skutoconfig.sql                                       */
/*                                                                            */
/*     DESCRIPTION:     List the SKUs linked to a number of configurations.   */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   17/03/95 RMW  DCS    N/A      initial version                            */
/*   16/06/99 MJT  DCS    PDR7593  Some sql*plus reports are wrapping         */
/*   28/06/01 MJT  DCS    NEW5486  Client ID added                            */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   08/08/05 DM   DCS    NEW7858  Extend SKU to 30 characters                */
/******************************************************************************/
 
/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132
 
/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'SKUs By Number Of Configurations' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2 - 
LEFT 'Max Configurations &3' SKIP 2

/* Column Headings and Formats */
COLUMN Client_ID HEADING 'Client' FORMAT A10
COLUMN SKU_ID HEADING 'SKU' FORMAT A30
COLUMN Description HEADING 'Description' FORMAT A40
COLUMN Product_Group HEADING 'Product|Group' FORMAT A10
 
/* SQL Select statement */
SELECT	S.Client_ID,
	S.SKU_ID, 
	COUNT(SSC.SKU_ID) "Links", 
	S.Description, 
	S.Product_Group
FROM SKU_SKU_Config SSC, SKU S
WHERE SSC.SKU_ID (+) = S.SKU_ID
AND SSC.Client_ID (+) = S.Client_ID
AND ((('&2' IS NULL) AND ('&4' IS NULL))
OR  (S.Client_ID = '&2')
OR  (('&2' IS NULL) AND S.Client_ID IN
(SELECT Client_ID
FROM Client_Group_Clients
WHERE Client_Group = '&4')))
GROUP BY S.Client_ID, S.SKU_ID, S.Description, S.Product_Group
HAVING COUNT(SSC.SKU_ID) = &3
ORDER BY S.Client_ID, COUNT(SSC.SKU_ID)
; 
 
SET TERMOUT ON
 
