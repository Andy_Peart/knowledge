/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics and Industrial Systems Ltd*/
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LEXLOGISTiX.                                 */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1995             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/
 
/******************************************************************************/
/*                                                                            */
/*                             LEXLOGISTiX                                    */
/*                                                                            */
/*     FILE NAME  :     invbyskubymanufd.sql                                  */
/*                                                                            */
/*     DESCRIPTION:     List inventory by SKU ID and manufacture date         */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   28/04/98 DS   DCS    N/A      initial version                            */
/*   16/06/99 MJT  DCS    PDR7593  Some sql*plus reports are wrapping         */
/*   25/06/01 MJT  DCS    NEW5486  Client ID added                            */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   04/08/05 DM   DCS    NEW7858  Extens SKU to 30 characters                */
/******************************************************************************/
 
/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132

/* Top Title */
TTITLE CENTER _SITENAME SKIP 2 - 
LEFT 'Inventory By SKU and Manufacture Date Range' RIGHT _DATESTAMP SKIP 1 - 
RIGHT _TIMESTAMP SKIP 1 -                               
LEFT 'Date Range From &6 To &7 For SKU &5' -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2

/* Avoid duplicate SKUs */
BREAK ON Site_Client SKIP PAGE ON Owner_ID SKIP PAGE ON sDescription 

/* Column Headings and Formats */
COLUMN Site_Client HEADING 'Site/|Client' FORMAT A10 WORD_WRAP
COLUMN Owner_Location Heading 'Owner/|Location' FORMAT A10 WORD_WRAP
COLUMN sDescription HEADING 'SKU/|Description' FORMAT A40 WORD_WRAP
COLUMN Config_ID HEADING 'Pack Config/|Cond Code' FORMAT A15 WORD_WRAP
COLUMN Manuf_DStamp HEADING 'Manufacture|Date' FORMAT A11
COLUMN Tag_Batch HEADING 'Tag/|Batch' FORMAT A20 WORD_WRAP
COLUMN Quant_On_Hand_Allocated HEADING 'Qty On Hand/|Allocated' FORMAT A19 WRAP
 
/* Compute sums */
COMPUTE SUM OF Qty_On_Hand Qty_Allocated ON sDescription
 
/* SQL Select statement */
SELECT	RPAD (I.Site_ID, 10) || I.Client_ID Site_Client,
	RPAD(I.Owner_ID, 10) || I.Location_ID Owner_Location, 
	RPAD (I.SKU_ID, 40) || I.Description sDescription,
	RPAD (NVL (I.Config_ID, ' '), 15) || I.Condition_ID Config_ID,
	TO_CHAR (I.Manuf_DStamp, 'DD-MON-YYYY HH24:MI:SS') Manuf_DStamp,
	RPAD(NVL(I.Tag_ID, ' '), 20) || I.Batch_ID Tag_Batch,
	LPAD (TO_CHAR (Qty_On_Hand,'999G999G990D999999'), 19) ||
	TO_CHAR (Qty_Allocated,'999G999G990D999999') Quant_On_Hand_Allocated
FROM Inventory I
WHERE I.SKU_ID = '&5'
AND (('&2' IS NULL) OR (I.Site_ID = '&2'))
AND (('&4' IS NULL) OR (I.Owner_ID = '&4'))
AND Manuf_DStamp >= TO_DATE('&6', 'DD-MON-YYYY')
AND Manuf_DStamp < TO_DATE('&7', 'DD-MON-YYYY') + 1
AND ((('&3' IS NULL) AND ('&8' IS NULL))
OR  (I.Client_ID = '&3')
OR  (('&3' IS NULL) AND I.Client_ID IN
	(SELECT Client_ID 
	 FROM Client_Group_Clients
	 WHERE Client_Group = '&8')))  
ORDER BY I.Site_ID, I.Client_ID, I.Owner_ID, I.Manuf_DStamp; 
 
SET TERMOUT ON
 





