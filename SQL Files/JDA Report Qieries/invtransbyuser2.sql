/******************************************************************************/
/*                                                                            */
/*   Logistics and Internet Systems Ltd                                       */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics & Internet Systems Ltd    */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Internet   */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Internet Systems Limited, 2003               */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*                                  LIS                                       */
/*                                                                            */
/*     FILE NAME  :     invtransbyuser2.sql                                   */
/*                                                                            */
/*     DESCRIPTION:     List inventory transactions by user, by date range.   */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   20/08/03 RMW  DCS    NEW6806  Additional reports from projects           */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   23/06/05 JH   DCS    PDR9627  Problems with inv trans report             */
/*   13/09/05 JRL  DCS    PDR9861  Zero exception picking problem	      */
/*   02/02/07 JH   DCS    DSP1547  Consolidated pick counts                   */
/******************************************************************************/

/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132

/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Total Tasks by User and Date Range' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 1 - 
LEFT "Date Range From &4 To &5" SKIP 3

/* Column Headings and Formats */
COLUMN User_ID HEADING 'User' FORMAT A10 WORD_WRAP
COLUMN Receipt HEADING 'Receipt' FORMAT '999G990'
COLUMN Eaches HEADING 'Eaches' FORMAT '99G990'
COLUMN Putaway HEADING 'Putaway' FORMAT '999G990'
COLUMN Pick HEADING 'Pick' FORMAT '999G990'
COLUMN Replenish HEADING 'Replen' FORMAT '99G990'
COLUMN Relocate HEADING 'Relocate' FORMAT '999G990'
COLUMN Stock_Check HEADING 'Stk Chk' FORMAT '99G990'
COLUMN Total HEADING 'Total' FORMAT '99G990'
COLUMN TotEach HEADING 'Tot Eaches' FORMAT '9G999G990'

COMPUTE SUM OF Receipt Putaway Pick Replenish Relocate Stock_Check Total ON REPORT

/* SQL Select statement */
SELECT	ITL.User_ID,
	SUM (DECODE (ITL.Code, 'Receipt', 1, 0)) Receipt,
	SUM (DECODE (ITL.Code, 'Receipt', ITL.Update_Qty, 0)) Eaches,
	SUM (DECODE (ITL.Code, 'Putaway', 1, 0)) Putaway,
	SUM (DECODE (ITL.Code, 'Putaway', ITL.Update_Qty, 0)) Eaches,
	SUM (DECODE (ITL.Code, 'Pick', 1, 'Consol', 1, 0)) Pick,
	SUM (DECODE (ITL.Code, 'Pick', ITL.Update_Qty, 'Consol', ITL.Update_Qty, 0)) Eaches,
	SUM (DECODE (ITL.Code, 'Replenish',1, 0)) Replenish,
	SUM (DECODE (ITL.Code, 'Replenish', ITL.Update_Qty, 0)) Eaches,
	SUM (DECODE (ITL.Code, 'Relocate',1, 0)) Relocate,
	SUM (DECODE (ITL.Code, 'Relocate', ITL.Update_Qty, 0)) Eaches,
	SUM (DECODE (ITL.Code, 'Stock Check', 1, 0)) Stock_Check,
	SUM (DECODE (ITL.Code, 'Stock Check', ITL.Update_Qty, 0)) Eaches,
	SUM (DECODE (ITL.Code, 'Receipt', 1,
			       'Putaway', 1,
			       'Pick', 1, 'Consol', 1,
			       'Replenish', 1,
			       'Relocate', 1,
			       'Stock Check', 1, 0)) Total,
	SUM (DECODE (ITL.Code, 'Receipt', ITL.Update_Qty,
			       'Putaway', ITL.Update_Qty,
			       'Pick', ITL.Update_Qty, 'Consol', ITL.Update_Qty,
			       'Replenish', ITL.Update_Qty,
			       'Relocate', ITL.Update_Qty,
			       'Stock Check', ITL.Update_Qty, 0)) TotEach
FROM Inventory_Transaction ITL, Application_User AU
WHERE ITL.DStamp BETWEEN TO_DATE('&4', 'DD-MON-YYYY') 
AND TO_DATE('&5', 'DD-MON-YYYY') + (1 - (1/86400))
AND (ITL.Site_ID = '&2' OR '&2' IS NULL)
AND (ITL.User_ID = '&3' OR '&3' IS NULL)
AND ITL.User_ID = AU.User_ID
AND ITL.Summary_Record = 'Y'
AND ((ITL.Code = 'Pick' and ITL.Update_Qty > 0) 
OR (ITL.Code = 'Consol' and ITL.Update_Qty > 0) 
OR (ITL.Code <> 'Pick' and ITL.Code <> 'Consol'))
GROUP BY ITL.User_ID
ORDER BY ITL.User_ID
; 

/* The complex where statement is to get over the fact that Oracle stores */
/* dates and times as part of the date field. Depending on how it was stored */
/* it might have the seconds after midnight, or might be set as midnight */
/* The expression 1 will default to midnight  at the beginning of the day */
/* specified. The 2 expression adds 1 day minus 1 second, so effectively */
/* causing the between to be between day1 0 seconds after midnight and */
/* day2 1 second before midnight! */

SET TERMOUT ON
