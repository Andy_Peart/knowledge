/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics and Industrial Systems Ltd*/
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LEXLOGISTiX.                                 */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1998             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/
 
/******************************************************************************/
/*                                                                            */
/*                             LEXLOGISTiX                                    */
/*                                                                            */
/*     FILE NAME  :     abcdynpfmismatch.sql                                  */
/*                                                                            */
/*     DESCRIPTION:     List abc dynamic pick faces which are mismatched      */
/*                      by site/owner.                                        */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   11/11/98 JH   DCS    NEW3173  ABC/pareto analysis			      */
/*   16/06/99 MJT  DCS    PDR7593  Some sql*plus reports are wrapping         */
/*   20/06/01 MJT  DCS    NEW5486  Client ID added                            */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   03/08/05 DM   DCS    NEW7858  Increase SKU ID to 30 characters           */
/******************************************************************************/
 
/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132

/* Top Title */
TTITLE CENTER _SITENAME SKIP 2 - 
LEFT 'ABC Dynamic Pick Face Mismatch' RIGHT _DATESTAMP SKIP 1 -        
RIGHT _TIMESTAMP SKIP 1 -                               
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2

/* Avoid duplicates */
BREAK ON Site_ID SKIP PAGE ON Client_ID SKIP PAGE ON Owner_ID SKIP 1 ON SKU_ID

/* Column Headings and Formats */
COLUMN Site_ID HEADING 'Site' FORMAT A10 WRAP
COLUMN Client_ID HEADING 'Client' FORMAT A10 WRAP
COLUMN Owner_ID HEADING 'Owner' FORMAT A10 WRAP
COLUMN SKU_ID HEADING 'SKU' FORMAT A30 WRAP
COLUMN Location_ID HEADING 'Location' FORMAT A10 WRAP
COLUMN Description HEADING 'Description' FORMAT A40 WRAP
COLUMN ActualFreq HEADING 'Actual|Frequency' FORMAT A9 WRAP
COLUMN RequiredFreq HEADING 'Required|Frequency' FORMAT A9 WRAP

/* SQL Select statement */
SELECT PF.Site_ID,
	PF.Client_ID,
	PF.Owner_ID,
	PF.SKU_ID,
	PF.Location_ID,
	S.Description,
	PF.ABC_Frequency ActualFreq,
	SR.ABC_Frequency RequiredFreq
FROM Pick_Face PF, SKU_Ranking SR, SKU S
WHERE ((PF.Site_ID IS NULL AND SR.Site_ID IS NULL) OR (PF.Site_ID = SR.Site_ID))
AND ((PF.Owner_ID IS NULL AND SR.Owner_ID IS NULL) OR (PF.Owner_ID = SR.Owner_ID))
AND (('&2' IS NULL) OR (PF.Site_ID = '&2'))
AND (('&4' IS NULL) OR (PF.Owner_ID = '&4'))
AND PF.ABC_Frequency <> SR.ABC_Frequency
AND PF.Face_Type = 'D'
AND NVL(PF.ABC_Disable, 'N') = 'N'
AND NVL(S.ABC_Disable, 'N') = 'N'
AND PF.SKU_ID = SR.SKU_ID
AND PF.Client_ID = SR.Client_ID
AND SR.SKU_ID = S.SKU_ID
AND SR.Client_ID = S.Client_ID
AND ((('&3' IS NULL) AND ('&5' IS NULL))
OR  (PF.Client_ID = '&3')
OR  (('&3' IS NULL) AND PF.Client_ID IN
	(SELECT Client_ID 
	 FROM Client_Group_Clients
	 WHERE Client_Group = '&5')))  
ORDER BY PF.Site_ID, PF.Client_ID, PF.Owner_ID, PF.SKU_ID, PF.Location_ID;
 
SET TERMOUT ON
