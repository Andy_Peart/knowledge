/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics & Industrial Systems Ltd  */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 2001             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/
 
/******************************************************************************/
/*                                                                            */
/*                                 LIS                                        */
/*                                                                            */
/*     FILE NAME  :     locovervolbyloc.sql                                   */
/*                                                                            */
/*     DESCRIPTION:     List locations exceeding their set volume by location */
/*                      range.                                                */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   25/06/04 RMC  DCS    PDR9092  Change reports for VAS and beam            */
/******************************************************************************/

/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF 
SET TAB OFF
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132
 
/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Locations Exceeding their Location Volume By Range' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2 - 
LEFT 'Location Range From &3 To &4' SKIP 2

BREAK ON Site_ID SKIP PAGE
 
/* Column Headings and Formats */
COLUMN Site_ID HEADING 'Site' FORMAT A10
COLUMN Location_ID_Type HEADING 'Location/|Location Type|Beam ID' FORMAT A15 WRAP
COLUMN Check_String HEADING 'Check String|Beam Position' FORMAT A13
COLUMN Zone_1 HEADING 'Zone' FORMAT A10
COLUMN Storage_Class HEADING 'Storage|Class' FORMAT A10
COLUMN Lock_Status HEADING 'Lock|Status' FORMAT A9
COLUMN cVolWgt HEADING 'Volume/|Weight' FORMAT A17 WRAP
COLUMN cCurVolWgt HEADING 'Current Volume/|Current Weight' FORMAT A17 WRAP
COLUMN In_Stage HEADING 'In/Out|Stage' FORMAT A10
COLUMN Pick_Seq HEADING 'Pick|Sequence' FORMAT '999G999D99'
 
/* SQL Select statement */
SELECT	Site_ID,
        RPAD (Location_ID, 15) || RPAD (Loc_Type, 15) || RPAD (Beam_ID, 10)  Location_ID_Type,
        RPAD (NVL(Check_String,' '), 13) || RPAD (' ', 13) || RPAD(TO_CHAR(Beam_Position, 999999999), 13) Check_String,
	Zone_1,
	Storage_Class,
	Lock_Status,
	LPAD (TO_CHAR (Volume, '9G999G990D999999'), 17) ||
	LPAD (TO_CHAR (NVL (Weight, 0), '9G999G990D999999'), 17) cVolWgt,
	LPAD (TO_CHAR ((NVL (Current_Volume, 0) + NVL (Alloc_Volume, 0)), '9G999G990D999999'), 17) ||
	LPAD (TO_CHAR ((NVL (Current_Weight, 0) + NVL (Alloc_Weight, 0)), '9G999G990D999999'), 17) cCurVolWgt,
	RPAD (NVL (In_Stage, ' '), 10) || Out_Stage In_Stage,
	TO_CHAR (Pick_Sequence, '999G999D99') Pick_Seq
FROM Location
WHERE (Current_Volume + NVL (Alloc_Volume, 0)) > Volume
AND Location_ID BETWEEN '&3' AND '&4'
AND (('&2' IS NULL) OR (Site_ID = '&2'))
ORDER BY Site_ID, Location_ID
; 
 
SET TERMOUT ON
 
