/******************************************************************************/
/*                                                                            */
/*   Logistics & Industrial Systems Ltd                                       */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics & Industrial Systems Ltd  */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics & Industrial   */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics & Industrial Systems Limited, 1998               */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*                                  LIS                                       */
/*                                                                            */
/*     FILE NAME  :     address.sql                                           */
/*                                                                            */
/*     DESCRIPTION:     Output address file details.                          */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   01/03/95 RMW  DCS    N/A      initial version                            */
/*   18/04/98 RMW  DCS    NEW3196  PeopleSoft Interface (Customer_ID length)  */
/*   04/10/98 RMW  DCS    NEW3555  Add country to address data                */
/*   16/06/99 MJT  DCS    PDR7593  Some sql*plus reports are wrapping         */
/*   26/08/02 RMW  DCS    NEW6186  Multi-client addresses                     */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/******************************************************************************/

/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132
SET TAB OFF

/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Customer/Supplier Addresses' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2

/* Column Headings and Formats */
COLUMN Address_Type HEADING 'Type' FORMAT A10
COLUMN Client_Address HEADING 'Client/|Address ID' FORMAT A15
COLUMN Name HEADING 'Name' FORMAT A30
COLUMN cAddress HEADING 'Address' FORMAT A35 WRAP
COLUMN cContact HEADING 'Contact (Telephone, Fax, E-Mail)' FORMAT A35 WRAP

BREAK ON Address_Type SKIP 2 ON Client_ID SKIP 1

/* SQL Select statement */
SELECT	A.Address_Type,
 	RPAD (A.Client_ID, 15) || A.Address_ID Client_Address,
	A.Name,
	RPAD (NVL (A.Address1, ' '), 35) ||
	RPAD (NVL (A.Address2, ' '), 35) ||
	RPAD (NVL (A.Town, ' '), 35) ||
	RPAD (NVL (A.County, ' '), 35) ||
	RPAD (NVL (A.Postcode, ' '), 35) ||
	LibLanguage.GetTranslation(Country) cAddress,
	RPAD (NVL (A.Contact, ' '), 35) ||
	RPAD (NVL (A.Contact_Phone, ' '), 35) ||
	RPAD (NVL (A.Contact_Fax, ' '), 35) ||
	RPAD (NVL (A.Contact_EMail, ' '), 35) cContact
FROM Address A
WHERE (('&2' IS NULL) OR (A.Client_ID = '&2'))
ORDER BY A.Client_ID, A.Address_Type, A.Address_ID
;

SET TERMOUT ON

