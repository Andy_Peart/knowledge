/******************************************************************************/
/*                                                                            */
/*   Logistics and Internet Systems Ltd                                       */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics & Internet Systems Ltd    */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Internet   */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Internet Systems Limited, 2003               */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*                                  LIS                                       */
/*                                                                            */
/*     FILE NAME  :     skunomove.sql                                         */
/*                                                                            */
/*     DESCRIPTION:     Skus with no movement by date range.                  */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   20/08/03 RMW  DCS    NEW6806  Additional reports from projects           */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   08/08/05 DM   DCS    NEW7858  Extend SKU to 30 characters                */
/******************************************************************************/

/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132


/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Skus With No Picks By Date Range' -
RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2 -
LEFT 'Date Range From &5 To &6' SKIP 3


/* Column Headings and Formats */
COLUMN SiteClient HEADING 'Site/|Client' FORMAT A10 WRAP
COLUMN SKU_ID HEADING 'SKU ID' FORMAT A30
COLUMN Owner_ID HEADING 'Owner' FORMAT A10
COLUMN cQTY HEADING 'Qty On Hand' FORMAT A19 JUS LEFT
COLUMN Receipt_DStamp HEADING 'Receipt Dstamp' FORMAT A20
COLUMN Tag_ID HEADING 'Tag ID' FORMAT A20
COLUMN Location_ID HEADING 'Location' FORMAT A10

BREAK ON Site SKIP PAGE ON Client_ID SKIP 1 ON Owner_ID ON SKU_ID

/* SQL Select statement */
SELECT  RPAD (I.Site_ID, 10) || I.Client_ID SiteClient,
        I.Owner_ID,
        I.SKU_ID,
        I.Tag_ID,
        I.Location_ID,
        TO_CHAR (I.Receipt_DStamp, 'DD-MON-YYYY HH24:MI:SS') Receipt_DStamp,
        LPAD (TO_CHAR (I.Qty_On_Hand, '999G999G990D999999'), 19) cQTY
FROM Inventory I
WHERE (I.SKU_ID, I.Client_ID, I.Site_ID) IN 
	(
	SELECT I2.SKU_ID, I2.Client_ID, I2.Site_ID
	FROM Inventory I2
	WHERE (('&2' IS NULL) OR (I2.Site_ID = '&2'))
	AND (('&3' IS NULL) OR (I2.Client_ID = '&3'))
	AND (('&4' IS NULL) OR (I2.Owner_ID = '&4'))
	MINUS 
	SELECT ITL.Sku_ID, ITL.Client_ID, ITL.Site_ID
	FROM Inventory_Transaction ITL
	WHERE ITL.DStamp BETWEEN TO_DATE('&5', 'DD-MON-YYYY') 
	AND TO_DATE ('&6', 'DD-MON-YYYY') + (1 - (1/86400))
	AND ITL.Code = 'Pick'
	AND (('&2' IS NULL) OR (ITL.Site_ID = '&2'))
	AND (('&3' IS NULL) OR (ITL.Client_ID = '&3'))
	AND (('&4' IS NULL) OR (ITL.Owner_ID = '&4'))
	)
ORDER BY Site_ID,
         Client_ID,
         Owner_ID,
         SKU_ID,
         Receipt_DStamp;

SET TERMOUT ON
