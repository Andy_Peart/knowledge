/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics & Industrial Systems Ltd  */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LEXLOGISTiX.                                 */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1995             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/
 
/******************************************************************************/
/*                                                                            */
/*                             LEXLOGISTiX                                    */
/*                                                                            */
/*     FILE NAME  :     applicatuser.sql                                      */
/*                                                                            */
/*     DESCRIPTION:     List all the users on the system                      */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   15/02/95 RMW  DCS    N/A      initial version                            */
/*   06/02/98 JH   DCS    PDR5915  Reports need to be multi-sited             */
/*   16/06/99 MJT  DCS    PDR7593  Some sql*plus reports are wrapping         */
/*   30/06/99 RMW  DCS    NEW4036  Last login for users                       */
/*   22/06/01 MJT  DCS    NEW5486  Client ID added                            */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/******************************************************************************/
 
/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF 

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132

/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'User Report' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2
 
/* Column Headings and Formats */
COLUMN Site_Client HEADING 'Site/|Client|Vis Group' FORMAT A10 WORD_WRAP
COLUMN Group_Owner HEADING 'Group/|Owner' FORMAT A10
COLUMN User_ID HEADING 'User' FORMAT A20
COLUMN Notes HEADING 'Notes' FORMAT A40 WRAP
COLUMN Language HEADING 'Language' FORMAT A8
COLUMN Shift HEADING 'Shift' FORMAT A10
COLUMN PasswordDStamp HEADING 'Password|Changed' FORMAT A11
COLUMN LastLoginDStamp HEADING 'Last|Login' FORMAT A11

BREAK ON Site_Client SKIP PAGE ON Owner_ID SKIP 1 ON Group_ID

/* SQL Select statement */
SELECT	RPAD(Site_ID, 10) || Client_Vis_Group Site_Client, 
	RPAD(Group_ID, 10) || Owner_ID Group_Owner,
	User_ID,
	Language,
	Shift,
	TO_CHAR (Password_DStamp, 'DD-MON-YYYY') PasswordDStamp,
	TO_CHAR (Last_Login_DStamp, 'DD-MON-YYYY') LastLoginDStamp,
	Notes
FROM Application_User
WHERE (('&2' IS NULL) OR (Site_ID = '&2'))
ORDER BY Site_ID, Client_Vis_Group, Owner_ID, Group_ID, User_ID
; 

SET TERMOUT ON

