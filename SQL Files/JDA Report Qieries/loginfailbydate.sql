/******************************************************************************/
/*                                                                            */
/*   Logistics and Internet Systems Ltd                                       */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics & Internet Systems Ltd    */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Internet   */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Internet Systems Limited, 2002               */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*                                 LIS                                        */
/*                                                                            */
/*     FILE NAME  :     loginfailbydate.sql                                   */
/*                                                                            */
/*     DESCRIPTION:     List all login failures by date range.                */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   16/09/02 RMW  DCS    NEW6343  Login failure logging                      */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/******************************************************************************/

/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132

/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Login Failures by Date Range' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP -
LEFT 'Date Range From &3 To &4' SKIP 2

BREAK ON Site_ID SKIP 2 ON Station_ID SKIP 1

/* Column Headings and Formats */
COLUMN Site_ID HEADING 'Site' FORMAT A10
COLUMN User_ID HEADING 'User' FORMAT A20
COLUMN Station_ID HEADING 'Workstation' FORMAT A20
COLUMN LoginDate HEADING 'Date' FORMAT A11
COLUMN LoginTime HEADING 'Time' FORMAT A8
COLUMN Type HEADING 'Type' FORMAT A6

/* SQL Select statement */
SELECT	WS.Site_ID,
	LF.Station_ID,
	LF.User_ID,
	DECODE (LF.MDI_Type, 'C', 'PC', 'E', 'PC', 'R', 'RDT', 'V', 'Voice', 'W', 'Web', NULL) Type,
	TO_CHAR (LF.DStamp, 'DD-MON-YYYY') LoginDate,
        TO_CHAR (LF.DStamp, 'HH24:MI:SS') LoginTime
FROM Login_Failure LF, Workstation WS
WHERE WS.Station_ID = LF.Station_ID
AND (('&2' IS NULL) OR (WS.Site_ID = '&2'))
AND LF.DStamp >= TO_DATE('&3', 'DD-MON-YYYY') 
AND LF.DStamp < TO_DATE('&4', 'DD-MON-YYYY') + 1
ORDER BY WS.Site_ID, Type, LF.Station_ID, LF.DStamp
;

SET TERMOUT ON

