/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics and Industrial Systems Ltd*/
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LEXLOGISTiX.                                 */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1995             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/
 
/******************************************************************************/
/*                                                                            */
/*                             LEXLOGISTiX                                    */
/*                                                                            */
/*     FILE NAME  :     noconfigskus.sql                                      */
/*                                                                            */
/*     DESCRIPTION:     List inventory with SKU's  with no pack configs.      */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   06/03/95 DJS  DCS    N/A      initial version                            */
/*   14/04/97 MJT  DCS    PDR5916  Working version.                           */
/*   06/02/98 JH   DCS    PDR5915  Reports need to be multi-sited             */
/*   27/07/98 MJT  DCS    NEW3188  Third Party Warehousing                    */
/*   18/04/99 JH   DCS    NEW3920  Extra/wider database columns		      */
/*   16/06/99 MJT  DCS    PDR7593  Some sql*plus reports are wrapping         */
/*   27/06/01 MJT  DCS    NEW5486  Client ID added                            */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   04/08/05 DM   DCS    NEW7858  Increase SKU to 30 characters              */
/******************************************************************************/
 
/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132
 
/* Top Title */
TTITLE CENTER _SITENAME SKIP 2 -
LEFT 'Inventory Not Linked To A Pack Config' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2

/* Avoid duplicate values */
BREAK ON Site_ID SKIP PAGE ON Client_ID SKIP PAGE ON Owner_ID SKIP PAGE ON Location_ID SKIP 1 ON SKU_ID
 
/* Column Headings and Formats */
COLUMN Site_Client HEADING 'Site/|Client' FORMAT A10
COLUMN Owner_ID HEADING 'Owner' FORMAT A10
COLUMN Tag_ID HEADING 'Tag' FORMAT A20
COLUMN Location_ID HEADING 'Location' FORMAT A10
COLUMN SKU_ID HEADING 'SKU' FORMAT A30
COLUMN Description HEADING 'Description' FORMAT A30 WORD_WRAP
COLUMN Condition_ID HEADING 'Condition' FORMAT A9
 
/* SQL Select statement */
SELECT	RPAD(I.Site_ID, 10) || I.Client_ID Site_Client, 
	I.Owner_ID, I.Tag_ID, I.Location_ID, I.SKU_ID, I.Description, I.Condition_ID
FROM INVENTORY I
WHERE I.Config_ID NOT IN (SELECT Config_ID 
			  FROM SKU_SKU_Config
			  WHERE I.SKU_ID=SKU_ID
			  AND I.Client_ID=Client_ID
			  )
AND (('&2' IS NULL) OR (I.Site_ID = '&2'))
AND (('&4' IS NULL) OR (I.Owner_ID = '&4'))
AND ((('&3' IS NULL) AND ('&5' IS NULL))
OR  (I.Client_ID = '&3')
OR  (('&3' IS NULL) AND I.Client_ID IN
(SELECT Client_ID 
FROM Client_Group_Clients
WHERE Client_Group = '&5')))  
ORDER BY I.Site_ID, I.Client_ID, I.Owner_ID, I.Location_ID, I.SKU_ID
; 
 
SET TERMOUT ON



