/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics & Industrial Systems Ltd  */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1997             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/
 
/******************************************************************************/
/*                                                                            */
/*                                 LIS                                        */
/*                                                                            */
/*     FILE NAME  :     invtranbytagid.sql                                    */
/*                                                                            */
/*     DESCRIPTION:     List DStamp, Code, SKU_ID, Update_Qty, User_ID,       */
/*                      Station_ID, From_Loc_ID, To_Loc_ID for all            */
/*                      Inventory Transactions with a given Tag_ID in DStamp, */
/*                      Key order.                                            */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   03/04/97 MJT  DCS    2694     initial version                            */
/*   25/04/97 MJT  N/A    PDR5924  Modified to handle european group & decimal*/
/*                                 separators.                                */
/*   07/08/97 MJT  N/A    N/A      Changed number format to display 0         */
/*   06/02/98 JH   DCS    PDR5915  Reports need to be multi-sited             */
/*   09/03/98 JH   DCS    PDR6685  Report shows months instead of minutes     */
/*   27/07/98 MJT  DCS    NEW3188  Third Party Warehousing                    */
/*   16/06/99 MJT  DCS    PDR7593  Some sql*plus reports are wrapping         */
/*   26/06/01 MJT  DCS    NEW5486  Client ID added                            */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   06/07/05 JH   DCS    PDR9627  Report problems                            */
/*   04/08/05 DM   DCS    NEW7858  Extend SKU to 30 characters                */
/******************************************************************************/
 
/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132
  
/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Inventory Transactions For Tag' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2 - 
LEFT 'Tag &5' SKIP 2
 
/* Column Headings and Formats */
COLUMN Site_Client HEADING 'Site/|Client' FORMAT A10 WORD_WRAP
COLUMN Owner_ID HEADING 'Owner' FORMAT A10 
COLUMN DateTime HEADING 'Date/|Time' FORMAT A11
COLUMN Code HEADING 'Code' FORMAT A15
COLUMN SKU_ID HEADING 'SKU' FORMAT A30
COLUMN Update_Quant HEADING 'Update Qty' FORMAT A19
COLUMN User_Station HEADING 'User/|Station' FORMAT A20
COLUMN From_Loc_To_Loc HEADING 'From|Location/|To|Location' FORMAT A10 WORD_WRAP
 
/* SQL Select statement */
SELECT	RPAD (Site_ID, 10) || Client_ID Site_Client,
	Owner_ID,
	TO_CHAR(DStamp,'DD-MON-YYYY') ||
        TO_CHAR(DStamp,'HH24:MI:SS') DateTime,
	Code,
	SKU_ID,
 	TO_CHAR(Update_Qty,'999G999G990D999999') Update_Quant,
	RPAD(From_Loc_ID, 10) || To_Loc_ID From_Loc_To_Loc,
	RPAD(User_ID, 20) || Station_ID User_Station
FROM Inventory_Transaction
WHERE Tag_ID = '&5'
AND (('&2' IS NULL) OR (Site_ID = '&2'))
AND (('&4' IS NULL) OR (Owner_ID = '&4'))
AND ((('&3' IS NULL) AND ('&6' IS NULL))
OR  (Client_ID = '&3')
OR  (('&3' IS NULL) AND (Client_ID IS NULL OR Client_ID IN
	(SELECT Client_ID 
	 FROM Client_Group_Clients
	 WHERE Client_Group = '&6'))))  
AND Summary_Record = 'Y'
ORDER BY Site_ID, Client_ID, Owner_ID, DStamp, Key
;

SET TERMOUT ON
 





