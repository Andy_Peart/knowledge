/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics & Industrial Systems Ltd  */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1995             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/
 
/******************************************************************************/
/*                                                                            */
/*                                 LIS                                        */
/*                                                                            */
/*     FILE NAME  :     allocshortbywg.sql                                    */
/*                                                                            */
/*     DESCRIPTION:     List allocation shortages by Work Group               */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   31/10/95 CT   DCS    N/A      initial version                            */
/*   25/04/97 MJT  N/A    PDR5924  Modified to handle european group & decimal*/
/*                                 separators.                                */
/*   07/08/97 MJT  N/A    N/A      Changed number format to display 0         */
/*   06/02/98 JH   DCS    PDR5915  Reports need to be multi-sited             */
/*   24/07/98 MJT  DCS    NEW3188  Third Party Warehousing                    */
/*   16/06/99 MJT  DCS    PDR7593  Some sql*plus reports are wrapping         */
/*   22/06/01 MJT  DCS    NEW5486  Client ID added                            */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   19/10/06 JH   DCS    ENH3120  Increase cons/wg to 20 chars               */
/******************************************************************************/
 
/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF

SET TERMOUT ON

CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132

TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Allocation Shortages By Work Group' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page ' FORMAT 999 SQL.PNO SKIP 2 -
LEFT 'Work Group &5' SKIP 2

BREAK ON Site_Client SKIP PAGE ON Owner_Work SKIP 1

COLUMN Site_Client HEADING 'Site/|Client' FORMAT A10 WORD_WRAP
COLUMN Owner_Work HEADING 'Owner/|Work Group' FORMAT A20 WORD_WRAP
COLUMN ConTas HEADING 'Consignment/|Task' FORMAT A20 WRAP
COLUMN LineIDent HEADING 'Line' FORMAT '999G990'
COLUMN sDescription  HEADING 'SKU/|Description' FORMAT A30 WORD_WRAP
COLUMN Batch_ID HEADING 'Batch' FORMAT A15
COLUMN Shortage HEADING 'Shortage' FORMAT '999G999G990D999999'

SELECT	 RPAD(GS.Site_ID, 10) || GS.Client_ID Site_Client,
	 RPAD(GS.Owner_ID, 20) || GS.Work_Group Owner_Work,
	 RPAD(GS.Consignment, 20) || GS.Task_ID ConTas,
	 TO_CHAR(GS.Line_ID,'999G990') LineIDent,
	 RPAD(GS.Sku_ID,30) || S.Description sDescription,
	 TO_CHAR(GS.Qty_Ordered - GS.Qty_Tasked,'999G999G990D999999') Shortage,
	 GS.Batch_ID
FROM	 Generation_Shortage GS, SKU S
WHERE 	 GS.SKU_ID = S.SKU_ID
	 AND GS.Client_ID = S.Client_ID
	 AND Work_Group = '&5'
	 AND (('&2' IS NULL) OR (GS.Site_ID = '&2'))
	 AND (('&4' IS NULL) OR (GS.Owner_ID = '&4'))
AND ((('&3' IS NULL) AND ('&6' IS NULL))
OR  (GS.Client_ID = '&3')
OR  (('&3' IS NULL) AND GS.Client_ID IN
	(SELECT Client_ID 
	 FROM Client_Group_Clients
	 WHERE Client_Group = '&6')))  
ORDER BY GS.Site_ID, GS.Client_ID, GS.Owner_ID, GS.Work_Group, GS.Task_ID
;


SET TERMOUT ON 







