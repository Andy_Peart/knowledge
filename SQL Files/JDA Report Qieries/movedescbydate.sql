/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics & Industrial Systems Ltd  */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LEXLOGISTiX.                                 */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1995             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/
 
/******************************************************************************/
/*                                                                            */
/*                                 LIS                                        */
/*                                                                            */
/*     FILE NAME  :     movedescbydate.sql                                    */
/*                                                                            */
/*     DESCRIPTION:     List move descrepancies by date range.                */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   15/02/95 RMW  DCS    N/A      initial version                            */
/*   19/05/97 MJT  DCS    PDR5924  Modified to handle european group & decimal*/
/*                                 separators.                                */
/*   07/08/97 MJT  N/A    N/A      Changed number format to display 0         */
/*   06/02/98 JH   DCS    PDR5915  Reports need to be multi-sited             */
/*   18/04/99 JH   DCS    NEW3920  Extra/wider database columns		      */
/*   16/06/99 MJT  DCS    PDR7593  Some sql*plus reports are wrapping         */
/*   26/06/01 MJT  DCS    NEW5486  Client ID added                            */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   04/08/05 DM   DCS    NEW7858  Extend SKU to 30 characters                */
/*   19/10/06 JH   DCS    ENH3120  Increase cons/wg to 20 chars               */
/******************************************************************************/
 
/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF 
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132
 
/* Set up Variables */
 
/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Move Discrepancy Report By Date Range (Picks)' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2 - 
LEFT 'Date Range From &4 To &5' SKIP 2

/* Column Headings and Formats */
COLUMN Site_Client HEADING 'Site/|Client' FORMAT A10 WORD_WRAP
COLUMN DSTAMP NEW_VALUE dDate NOPRINT
COLUMN cTaskID HEADING 'Task/|Line' FORMAT A20
COLUMN SKU_ID HEADING 'SKU/|Tag/|List' FORMAT A30
COLUMN cLOCATION HEADING 'From/|To/|Final' FORMAT A10 JUS L
COLUMN cQTY HEADING 'Required Qty/|Actual Qty' FORMAT A19 JUS L
COLUMN cWORKGROUP HEADING 'Work Group/|Consignment' FORMAT A20 JUS L 
COLUMN cUSER HEADING 'User/|Station/|Reason' FORMAT A10 WORD_WRAP

BREAK ON Site_Client SKIP PAGE ON DSTAMP SKIP 1
 
/* SQL Select statement */
SELECT	RPAD(Site_ID, 10) || Client_ID Site_Client,
	DStamp,
	RPAD (Task_ID, 20) || Line_ID cTaskID,
	RPAD (SKU_ID, 30) || RPAD (Tag_ID, 30) || List_ID SKU_ID,
	RPAD (From_Loc_ID, 10) || RPAD (To_Loc_ID, 10) || 
	RPAD (Final_Loc_ID, 10) cLOCATION,
	LPAD(TO_CHAR(Old_Qty_To_Move,'999G999G990D999999'), 19) ||
	LPAD(TO_CHAR(Qty_To_Move,'999G999G990D999999'), 19) cQTY,
	RPAD(NVL (Work_Group, ' '), 20) || RPAD(NVL(Consignment, ' '), 20) cWORKGROUP,
	RPAD(User_ID, 20) || RPAD(Station_ID, 20) || Reason_Code cUSER
FROM Move_Descrepancy
WHERE DStamp >= TO_DATE('&4', 'DD-MON-YYYY') 
AND DStamp < TO_DATE('&5', 'DD-MON-YYYY') + 1
AND (('&2' IS NULL) OR (Site_ID = '&2'))
AND Task_Type IN ('C', 'O')
AND ((('&3' IS NULL) AND ('&6' IS NULL))
OR  (Client_ID = '&3')
OR  (('&3' IS NULL) AND Client_ID IN
(SELECT Client_ID 
FROM Client_Group_Clients
WHERE Client_Group = '&6')))  
ORDER BY Site_ID, Client_ID, DStamp; 

SET TERMOUT ON

