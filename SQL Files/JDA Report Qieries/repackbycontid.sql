/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics and Industrial Systems Ltd*/
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1997             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/
 
/******************************************************************************/
/*                                                                            */
/*                                 LIS                                        */
/*                                                                            */
/*     FILE NAME  :     repackbycontid.sql                                    */
/*                                                                            */
/*     DESCRIPTION:     Output the contents of a container by container ID.   */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   10/10/97 RMW  DCS    N/A      initial version                            */
/*   28/07/98 MJT  DCS    NEW3188  Third Party Warehousing                    */
/*   18/04/99 JH   DCS    NEW3920  Extra/wider database columns		      */
/*   16/06/99 MJT  DCS    PDR7593  Some sql*plus reports are wrapping         */
/*   28/06/01 MJT  DCS    NEW5486  Client ID added                            */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/******************************************************************************/
 
/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF 
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132
 
/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Container Contents by Container' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2 - 
LEFT 'Container &2' SKIP 2

BREAK ON Client_ID ON Owner_ID ON SKU_Description

/* Column Headings and Formats */
COLUMN Client_ID HEADING 'Client' FORMAT A10
COLUMN Owner_ID HEADING 'Owner' FORMAT A10
COLUMN SKU_Description HEADING 'SKU/|Description' FORMAT A40 WORD_WRAP
COLUMN Qty_On_Hand HEADING 'Qty On Hand' FORMAT '999G999G990D999999'
COLUMN Tag_ID HEADING 'Tag' FORMAT A20
COLUMN Batch_ID HEADING 'Batch' FORMAT A15
COLUMN Condition_ID HEADING 'Condition' FORMAT A9

/* SQL Select statement */
SELECT	I.Client_ID,
	I.Owner_ID,
	RPAD(I.SKU_ID, 40) || I.Description SKU_Description,
	I.Qty_On_Hand,
	I.Tag_ID,
	I.Batch_ID,
	I.Condition_ID
FROM Inventory I
WHERE I.Container_ID = '&2'
ORDER BY I.Client_ID, I.Owner_ID, I.SKU_ID, I.Description, I.Tag_ID
;

SET TERMOUT ON

