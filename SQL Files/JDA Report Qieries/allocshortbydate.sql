/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics and Industrial Systems Ltd*/
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1997             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/
 
/******************************************************************************/
/*                                                                            */
/*                                  LIS                                       */
/*                                                                            */
/*     FILE NAME  :     allocshortbydate.sql                                  */
/*                                                                            */
/*     DESCRIPTION:     Content of allocation shortage log by date range.     */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   30/03/95 RMW  DCS    N/A      initial version                            */
/*   25/04/97 MJT  N/A    PDR5924  Modified to handle european group & decimal*/
/*                                 separators.                                */
/*   07/08/97 MJT  N/A    N/A      Changed number format to display 0         */
/*   06/02/98 JH   DCS    PDR5915  Reports need to be multi-sited             */
/*   24/07/98 MJT  DCS    NEW3188  Third Party Warehousing                    */
/*   16/06/99 MJT  DCS    PDR7593  Some sql*plus reports are wrapping         */
/*   22/06/01 MJT  DCS    NEW5486  Client ID added                            */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   03/08/05 DM   DCS    NEW7858  Increase SKU to 30 charactres              */
/*   19/10/06 JH   DCS    ENH3120  Increase cons/wg to 20 chars               */
/******************************************************************************/
 
/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF 
SET TERMOUT ON

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132
 
/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Allocation Shortage By Date Range' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2 - 
LEFT 'Date Range From &5 To &6' SKIP 2 

BREAK ON Site_Client SKIP PAGE

/* Column Headings and Formats */
COLUMN Site_Client HEADING 'Site/|Client' FORMAT A10 WORD_WRAP
COLUMN Task HEADING 'Task/|Description' FORMAT A25 WORD_WRAP
COLUMN LineIDent HEADING 'Line' FORMAT '999G990'
COLUMN SKUBat HEADING 'SKU/|Batch' FORMAT A30
COLUMN Shortage HEADING 'Shortage' FORMAT '999G999G990D999999'
COLUMN Order_Groups HEADING 'Work Group/|Consignment' FORMAT A20 WRAP
COLUMN Owner_DStamp HEADING 'Owner/|Date' FORMAT A11 WORD_WRAP

SELECT	RPAD (GS.Site_ID, 10) || GS.CLient_ID Site_Client,
	RPAD (GS.Owner_ID, 11) || TO_CHAR (GS.DStamp, 'DD-MON-YYYY') Owner_DStamp,
	RPAD (GS.Task_ID, 20) || S.Description Task,
	TO_CHAR (GS.Line_ID,'999G990') LineIDent,
	RPAD(GS.SKU_ID, 30) || GS.Batch_ID SKUBat,
	TO_CHAR (GS.Qty_Ordered - GS.Qty_Tasked,'999G999G990D999999') Shortage,
	RPAD (NVL (GS.Work_Group, ' '), 20) || GS.Consignment Order_Groups
FROM SKU S, Generation_Shortage GS
WHERE GS.SKU_ID = S.SKU_ID
AND GS.Client_ID = S.Client_ID 
AND GS.DStamp >= TO_DATE('&5', 'DD-MON-YYYY') 
AND GS.DStamp < TO_DATE('&6', 'DD-MON-YYYY') + 1
AND (('&2' IS NULL) OR (GS.Site_ID = '&2'))
AND (('&4' IS NULL) OR (GS.Owner_ID = '&4'))
AND ((('&3' IS NULL) AND ('&7' IS NULL))
OR  (GS.Client_ID = '&3')
OR  (('&3' IS NULL) AND GS.Client_ID IN
	(SELECT Client_ID 
	 FROM Client_Group_Clients
	 WHERE Client_Group = '&7')))  
ORDER BY GS.Site_ID, GS.Client_ID, GS.Owner_ID, GS.DStamp
; 

SET TERMOUT ON














