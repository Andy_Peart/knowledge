/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics and Industrial Systems Ltd*/
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1999             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/
 
/******************************************************************************/
/*                                                                            */
/*                                 LIS                                        */
/*                                                                            */
/*     FILE NAME  :     movetasksleft.sql                                     */
/*                                                                            */
/*     DESCRIPTION:     List the outstanding move tasks where at least part   */
/*                      of the order has been shipped.                        */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   06/09/99 RMW  DCS    N/A      initial version                            */
/*   27/06/01 MJT  DCS    NEW5486  Client ID added                            */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/******************************************************************************/

/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF 

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132

/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Move Tasks - for Orders having at least One Line Shipped' -
RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2
 
/* Skip to a new page on chaging site */
BREAK ON Site_Client SKIP PAGE ON Owner_ID SKIP 1

/* Column Headings and Formats */
COLUMN Site_Client HEADING 'Site/|Client' FORMAT A10 WORD_WRAP
COLUMN Owner_ID HEADING 'Owner' FORMAT A10
COLUMN Task_ID HEADING 'Order ID' FORMAT A20
COLUMN Name HEADING 'Name' FORMAT A30
COLUMN Order_Date HEADING 'Order Date ' FORMAT A11
COLUMN Picks_Left HEADING 'Picks Left' FORMAT 9999999999
COLUMN Zone HEADING 'Zone' FORMAT A10
COLUMN Start_Date HEADING 'Start Date' FORMAT A15

SELECT	RPAD(MT.Site_ID, 10) || MT.Client_ID Site_Client, 
	MT.Owner_ID,
	MT.Task_ID,
	OH.Name,
	OH.Order_Date Order_date,
	COUNT (MT.Key) Picks_Left,
	MT.Work_Zone Zone,
	MIN (TO_CHAR (MT.DStamp, 'DD-MON-YY HH24:MI')) Start_Date
FROM Move_Task MT, Order_Header OH
WHERE MT.Task_ID IN (SELECT DISTINCT SM.Order_ID 
		     FROM Shipping_Manifest SM
		     WHERE SM.Shipped = 'Y'
		     AND SM.Client_ID = MT.Client_ID)
AND OH.Order_ID = MT.Task_ID
AND OH.Client_ID = MT.Client_ID
AND (('&2' IS NULL) OR (MT.Site_ID = '&2'))
AND (('&4' IS NULL) OR (MT.Owner_ID = '&4'))
AND ((('&3' IS NULL) AND ('&5' IS NULL))
OR  (MT.Client_ID = '&3')
OR  (('&3' IS NULL) AND MT.Client_ID IN
(SELECT Client_ID 
FROM Client_Group_Clients
WHERE Client_Group = '&5')))  
GROUP BY MT.Site_ID,
	 MT.Client_ID, 
	 MT.Owner_ID, 
	 OH.Order_Date, 
	 MT.Task_ID, 
	 OH.Name, 
	 MT.Work_Zone
ORDER BY MT.Site_ID,
	 MT.Client_ID, 
	 MT.Owner_ID, 
	 OH.Order_Date DESC, 
	 OH.Name, 
	 MT.Task_ID
/

SET TERMOUT ON

