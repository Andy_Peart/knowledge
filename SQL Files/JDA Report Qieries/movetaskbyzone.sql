/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics & Industrial Systems Ltd  */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LEXLOGISTiX.                                 */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1995             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/
 
/******************************************************************************/
/*                                                                            */
/*                             LEXLOGISTiX                                    */
/*                                                                            */
/*     FILE NAME  :     movetaskbyzone.sql                                    */
/*                                                                            */
/*     DESCRIPTION:     Print move tasks by zone ID.                          */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   06/03/95 RMW  DCS    N/A      initial version                            */
/*   25/04/97 MJT  DCS    PDR5924  Modified to handle european group & decimal*/
/*                                 separators.                                */
/*   7/08/97  MJT  DCS    NEW2813  Modified to include new container id,      */
/*                                 pallet id fields in inventory              */
/*   06/02/98 JH   DCS    PDR5915  Reports need to be multi-sited             */
/*   27/07/98 MJT  DCS    NEW3188  Third Party Warehousing                    */
/*   18/04/99 JH   DCS    NEW3920  Extra/wider database columns		      */
/*   16/06/99 MJT  DCS    PDR7593  Some sql*plus reports are wrapping         */
/*   27/06/01 MJT  DCS    NEW5486  Client ID added                            */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   04/08/05 DM   DCS    NEW7858  Extend SKU to 30 characters                */
/*   19/10/06 JH   DCS    ENH3120  Increase cons/wg to 20 chars               */
/******************************************************************************/

/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 60
SET NEWPAGE 0
SET LINESIZE 132
 
/* Set up Variables */
COLUMN Work_Zone NEW_VALUE VWORK_ZONE NOPRINT 
 
/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Move Tasks By Work Zone' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2 - 
LEFT 'Work Zone ' FORMAT A10 VWORK_ZONE SKIP 2
 
/* Column Headings and Formats */
COLUMN Site_Client HEADING 'Site/|Client/|Owner' FORMAT A10 WORD_WRAP
COLUMN Qty_Tag HEADING 'Qty/|Tag' FORMAT A20 WRAP
COLUMN sDescription HEADING 'Status/|SKU/|Description' FORMAT A30 WORD_WRAP
COLUMN From_To_Loc_ID HEADING 'From Locn/|To Locn' FORMAT A10 WORD_WRAP
COLUMN tFinal_Loc_ID HEADING 'Task/|Final Location' FORMAT A20
COLUMN Work_Group HEADING 'Work Group/|Consignment' FORMAT A20 WRAP
COLUMN Container_Pallet_ID HEADING 'Container/|Pallet' FORMAT A10 WRAP
 
/* leave a blank line between lines */
BREAK ON Site_Client ON Owner_ID SKIP PAGE ON ROW SKIP 1

COMPUTE COUNT SUM OF Qty_To_Move ON REPORT

/* SQL Select statement */
SELECT	RPAD (Site_ID, 10) || RPAD(Client_ID, 10) || Owner_ID Site_Client,
	Work_Zone,
	RPAD(Status, 30) || RPAD (SKU_ID, 30) || Description sDescription,
	RPAD (NVL (From_Loc_ID, ' '), 10) || To_Loc_ID From_To_Loc_ID,
	RPAD (Task_ID, 20) || Final_Loc_ID tFinal_Loc_ID,
	LPAD(TO_CHAR(Qty_To_Move,'999G999G990D999999'), 20) || Tag_ID Qty_Tag,
	RPAD (NVL (Work_Group, ' '), 20) || Consignment Work_Group,
	RPAD (NVL (Container_ID, ' '), 20) || Pallet_ID Container_Pallet_ID
FROM Move_Task
WHERE Work_Zone LIKE '&5'
AND (('&2' IS NULL) OR (Site_ID = '&2'))
AND (('&4' IS NULL) OR (Owner_ID = '&4'))
AND ((('&3' IS NULL) AND ('&6' IS NULL))
OR  (Client_ID = '&3')
OR  (('&3' IS NULL) AND Client_ID IN
(SELECT Client_ID 
FROM Client_Group_Clients
WHERE Client_Group = '&6')))  
ORDER BY Site_ID, Client_ID, Owner_ID, Sequence, SKU_ID; 
 
SET TERMOUT ON

