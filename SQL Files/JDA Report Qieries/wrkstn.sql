/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics & Industrial Systems Ltd  */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LEXLOGISTiX.                                 */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1995             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/
 
/******************************************************************************/
/*                                                                            */
/*                             LEXLOGISTiX                                    */
/*                                                                            */
/*     FILE NAME  :     wrkstn.sql                                            */
/*                                                                            */
/*     DESCRIPTION:     List all the workstations on the system (RDT and PC). */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   15/02/95 RMW  DCS    N/A      initial version                            */
/*   06/02/98 JH   DCS    PDR5915  Reports need to be multi-sited             */
/*   16/06/99 MJT  DCS    PDR7593  Some sql*plus reports are wrapping         */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/******************************************************************************/
 
/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF 

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132

/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Workstations' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2
 
BREAK ON Site_ID SKIP PAGE ON Station_ID SKIP 1

/* Column Headings and Formats */
COLUMN Site_ID HEADING 'Site' FORMAT A10
COLUMN Station_ID HEADING 'Workstation' FORMAT A20
COLUMN RecDock_ID HEADING 'Receive|Dock' FORMAT A10
COLUMN Notes HEADING 'Notes' FORMAT A25 WORD_WRAP
COLUMN Tag_Command HEADING 'Tag Command' FORMAT A25 WORD_WRAP
COLUMN Rpt_Command HEADING 'Report Command' FORMAT A25 WORD_WRAP

/* SQL Select statement */
SELECT	Site_ID,
	Station_ID,
	RecDock_ID,
	Notes,
	Tag_Command,
	Rpt_Command
FROM WorkStation
WHERE (('&2' IS NULL) OR (Site_ID = '&2'))
ORDER BY Site_ID, Station_ID 
; 

SET TERMOUT ON

