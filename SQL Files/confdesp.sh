#!/bin/sh

# this is a temporary scrip to produce the confdesp output during go-live
# clipper needs to decide on a more permanent solution once the site is stable
# this script is unsupported by JDA and
# Author : Oliver Dearlove

# temporary test
dt=`date +%y%m%d%H%M%S`
logfile=$DCS_TMPDIR/confdesp.sh
. /home/wmswcs/dcs/.dcs_profile

# do a process count to see if the script may already be running - if it is exit
isrunning=`ps -ef|grep -v tail | grep confdesp.sh|grep -v grep|grep -v $$|grep -v view|wc -l`
if [ "$isrunning" -gt "0" ]
then
	echo "script may already be running - exiting! "
	exit 0
fi

echo "logging " >> $logfile

# go to outwork directory
cd $DCS_COMMSDIR/outwork

# setup the filename 
date=`date +"20%y%m%d"`
time=`date|awk '{print $4}'|sed 's/\://g'`
file=WILKINSONdes$date$time.csv

# setup a tempory file used to check when to exit the loop
filecheck=filecheck.$$

echo "logging 2" >> $logfile

# if we are good to run then clear the temporary table and populate it with all orders where user def check 4 = N
sqlplus -s $ORACLE_USR << !
        set feedback off
        set heading off
        set pagesize 9999

delete from uc_desp_conf;

insert into uc_desp_conf
(select order_id from order_header oh where
    oh.status ='Shipped' and
    oh.client_id = 'WILKINSON'and
    oh.user_def_chk_4 = 'N');

commit;

!

# go into an endless loop to process all records.  Each of the select is taken from the old jasper reports and sends data to the output file
while [ 1 ]; do

echo "in loop " >> $logfile

sqlplus -s $ORACLE_USR << ! >> $file
        set feedback off
        set heading off
        set pagesize 999
	set linesize 500

select 'BEG'||'|'||
    to_char(oh.creation_date,'YYYYMMDD') ||'|'||
    'Ollerton' ||'|'||
    'Wilkinson' ||'|'||
    '|'||'|'||'|'||'|'||'|'||'|'||'|'||'|'||
    oh.name ||'|'||
    oh.address1 ||'|'||
    oh.address2 ||'|'||
    oh.town ||'|'||
    oh.County ||'|'||
    'UK' ||'|'||
    oh.postcode ||'|'||
    '|'||
    oh.order_id ||'|'||
    oh.order_id ||'|'||
    '|'||
    'Default'||'|'||
    nvl(to_char(oh.ship_by_date,'YYYYMMDD'), (to_char(current_timestamp,'YYYYMMDD')))||'|'||
    nvl(to_char(oh.shipped_date,'YYYYMMDD'), (to_char(current_timestamp,'YYYYMMDD')))||'|'||
    nvl(to_char(oh.shipped_date,'HH24MISS'), (to_char(current_timestamp,'HH24MISS')))||'|'||
    nvl(to_char(oh.deliver_by_date,'YYYYMMDD'), (to_char(current_timestamp,'YYYYMMDD')))||'|'||
    nvl(to_char(oh.deliver_by_date,'YYYYMMDD'), (to_char(current_timestamp,'YYYYMMDD')))||'|'||
    'By'||'|'||
    '000000'||'|'||
    '000000'||'|'||
    '|'||
    '|'||
    '|'||
    oh.order_type ||'|'||
    oh.order_type ||'|'||
    '0' ||'|'||
    '1' ||'|'||
    to_char(oh.creation_date,'YYYYMMDD') ||'|'||
    to_char(oh.creation_date,'HH24MISS') ||'|'||
    '|'||
    sum(ol.qty_shipped)||'|'||   ----- add maths for each order line individualy then sum
    oh.num_lines "result"
from
    order_header oh,
    order_line ol
where
    oh.order_id = ol.order_id and
    oh.order_id in (select order_id from uc_desp_conf where rownum=1)
group by
    oh.order_id,to_char(oh.creation_date,'YYYYMMDD'),oh.name ,
    oh.name ,
    oh.address1 ,
    oh.address2 ,
    oh.town ,
    oh.County ,
    'UK' ,
    oh.postcode ,
    '|',
    oh.order_id ,
    oh.order_id ,
    'DEFAULT',
    to_char(ship_by_date , 'YYYYMMDD'),
    to_char(oh.shipped_date,'YYYYMMDD'),
    to_char(oh.shipped_date,'HH24MISS'),
    to_char(oh.deliver_by_date,'YYYYMMDD'),
    to_char(oh.deliver_by_date,'YYYYMMDD'),
    'By',
    '000000',
    '000000',
    oh.order_type ,
    oh.order_type ,
    '0' ,
    '1' ,
    to_char(oh.creation_date,'YYYYMMDD') ,
    to_char(oh.creation_date,'HH24MISS') ,
    oh.num_lines;

select
    'DET'||'|'||
    ol.line_id||'|'||
    ol.sku_id||'|'||
    sk.ean||'|'||
    sk.description||'|'||
    nvl(ol.qty_shipped,0)||'|'||
    'Eaches'||'|'||
    case when ol.qty_shipped is null
    then
    'Scratch'
    else
    '||' end ||'|'||
    to_char(sysdate,'YYYMMDD')||'|'||  'Y' ||'|'||
    'Y' ||'|'||
    sk.description "result"
from
    order_line ol,
    sku sk
where ol.sku_id = sk.sku_id
and ol.order_id in (select order_id from uc_desp_conf where rownum = 1);

select
    'DEL'||'|'||
    'TRK'||'|'||
    nvl(sm.CARRIER_CONTAINER_ID,oc.config_id||oc.pallet_id) ||'|'||
    sm.sku_id ||'|'||
    sm.QTY_shipped ||'||'||
    sm.line_id "result"
from
    shipping_manifest sm,order_container oc
where oc.order_id=sm.order_id
and oc.container_id = sm.container_id
and sm.order_id in (select order_id from uc_desp_conf where rownum = 1);
update order_header set user_def_chk_4 = 'Y'
where order_id in (select order_id from uc_desp_conf where rownum = 1);
delete from uc_desp_conf where rownum = 1;
commit;
!


# now check out many records are left in the temporary table
sqlplus -s $ORACLE_USR << ! >| $filecheck
        set feedback off
        set heading off
        set pagesize 0

select count(*) from uc_desp_conf;

!

# if rows = 0 then exit the loop
y=`more filecheck.$$|awk '{print $1}'`

echo "converting y" >> $logfile
y=`echo $y|awk '{print $4}'`

echo "value of y = "$y >> $logfile
if [ "$y" = "0" ]
then
	break
fi
echo "in loop end"  >> $logfile

done

echo "out of loop" >> $logfile

# remove empty lines from the output file
sed -i '/^$/d' $file

# remove the temporary file we created
rm $filecheck

# copy to outarchive
cp $file ../outarchive/$file.a.$$

# move the file to the outtray
mv $file ../outtray/.

echo "about to exit" >> $logfile
exit 1
