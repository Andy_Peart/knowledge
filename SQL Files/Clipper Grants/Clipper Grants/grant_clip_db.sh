#!/bin/sh
################################################################################
#         Clipper_grantaccess                                                  #
#                                                                              #
#DATE       BY                Change No   VERSION DESCRIPTION                  # 
#========== ================= =========== ======= =============================#
#18/02/2019 Andy Peart   	  --              1.0 Initial version              #
################################################################################

. $HOME/dcs/.dcs_profile


cd $CLIP_GRANTDIR

pwd
#################################################################################
# HASH OUT ANY SECTIONS THAT ARE NOT REQIURED                                   #
#################################################################################
echo "SCRIPT Started to CREATE clipper User"

sqlplus -s $DCS_SYSDBA <<EOF
SET SERVEROUTPUT ON;
@CREATE_CLIPPER.sql
exit;
EOF

echo "CLIPPER Created"

##################################################################################

echo ""
echo "SCRIPT Started to action grants"
echo ""
###################################################################################

# Grant Clipper user all tables #

echo "Granting DCSDBA table access to Clipper User"

sqlplus -s $ORACLE_USR <<EOF
SET SERVEROUTPUT ON;
@DCSDBA_TABLE_GRANT.sql
exit;
EOF

echo "Clipper now has access to DCSDBA Tables"
echo ""
##################################################################################

# Grant Clipper user all Objects #

echo "Granting DCSDBA OBJECTS access to Clipper User"

sqlplus -s $ORACLE_USR <<EOF
SET SERVEROUTPUT ON;
@DCSDBA_OBJECT_GRANT.sql
exit;
EOF

echo "Clipper now has access to DCSDBA Objects"
echo ""
##################################################################################

# Grant Clipper user all Views #

echo "Granting DCSDBA Views access to Clipper User"

sqlplus -s $ORACLE_USR <<EOF
SET SERVEROUTPUT ON;
@DCSDBA_VIEW_GRANT.sql
exit;
EOF

echo "Clipper now has access to DCSDBA VIEWS"
echo ""
##################################################################################

# Grant Clipper user all Sequences #

echo "Granting DCSDBA sequence access to Clipper User"

sqlplus -s $ORACLE_USR <<EOF
SET SERVEROUTPUT ON;
@DCSDBA_SEQ_GRANT.sql
exit;
EOF

echo "Clipper now has access to DCSDBA sequences"
echo ""

###################################################################################

# Grant DCSDBA user all tables #

echo "Granting clipper table access to DCS User"

sqlplus -s $CLIP_USR <<EOF
SET SERVEROUTPUT ON;
@CLIPPER_TABLE_GRANT.sql
exit;
EOF

echo "Clipper now has access to DCSDBA Tables"
echo ""
##################################################################################

# Grant DCSDBA user all Objects #

echo "Granting clipper OBJECTS access to DCS User"

sqlplus -s $CLIP_USR <<EOF
SET SERVEROUTPUT ON;
@CLIPPER_OBJECT_GRANT.sql
exit;
EOF

echo "Clipper now has access to DCSDBA Objects"
echo ""
##################################################################################

# Grant DCSDBA user all Views #

echo "Granting clipper Views access to DCS User"

sqlplus -s $CLIP_USR <<EOF
SET SERVEROUTPUT ON;
@CLIPPER_VIEW_GRANT.sql
exit;
EOF

echo "Clipper now has access to DCSDBA VIEWS"
echo ""
##################################################################################

# Grant DCSDBA user all Sequences #

echo "Granting clipper sequence access to DCS User"

sqlplus -s $CLIP_USR <<EOF
SET SERVEROUTPUT ON;
@CLIPPER_SEQ_GRANT.sql
exit;
EOF

echo "Clipper now has access to DCSDBA sequences"
echo ""

##################################################################################

echo "Script Complete"