/******************************************************************************/
/*                                                                            */
/* NAME:         CREATE Clipper User and Schema                               */
/*                                                                            */
/* DESCRIPTION:  Grants CLIPPER User full table access on DCSDBA              */
/*                                                                            */
/*                                                                            */
/* Date       By  Proj    Ref      Description                                 /
/* ---------- --- ------- -------- -------------------------                  */
/* 18/02/2019 AMP Internal         Initial Write                              */
/******************************************************************************/


create tablespace clipper_tablespace
datafile 'clipper_tablespace.dat'
size 10M autoextend on;
 
create temporary tablespace clipper_tablespace_temp
tempfile 'clipper_tablespace_temp.dat'
size 5M autoextend on;
 
create user CLIPPER 
identified by CLIPPER
default tablespace clipper_tablespace
temporary tablespace clipper_tablespace_temp;
 
grant create session to clipper;
grant create table to clipper;
grant unlimited tablespace to clipper;
GRANT GRANT ANY OBJECT PRIVILEGE TO CLIPPER;
GRANT GRANT ANY PRIVILEGE TO CLIPPER;
GRANT GRANT ANY ROLE TO CLIPPER;