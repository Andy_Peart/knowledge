/******************************************************************************/
/*                                                                            */
/* NAME:         Table Grant Script                                           */
/*                                                                            */
/* DESCRIPTION:  Grants CLIPPER User full table access on DCSDBA              */
/*                                                                            */
/*                                                                            */
/* Date       By  Proj    Ref      Description                                 /
/* ---------- --- ------- -------- -------------------------                  */
/* 18/02/2019 AMP Internal         Initial Write                              */
/******************************************************************************/
Declare
	v_code NUMBER;
	v_errm VARCHAR2(64);
	/*SELECT all table Names from Schema into a Cursor*/
	cursor table_to_drop is
		SELECT
			Table_Name
		FROM
			User_Tables
		WHERE
			Table_Name NOT LIKE 'BIN\$%'
		ORDER BY
			Table_Name
		;

	drop_table table_to_drop%rowtype;
begin
	/*Open the Cursor*/
	open table_to_drop;
	/*Loop Through each Table*/
	LOOP
		fetch table_to_drop
		into
			drop_table
		;

		dbms_output.put_line(grant_table.table_name);
		/*IF no tables left EXIT*/
		IF table_to_dropt%notfound then
			dbms_output.put_line('No more table to drop');
			EXIT;
		END IF;
		/*REVOKE Clipper Privelages*/
		execute immediate ('drop table '
		||drop_table.table_name;
		dbms_output.put_line(drop_table.table_name
		||' DROPPPED');
		END LOOP;
	close table_to_drop;
	/* ERROR Handling */
EXCEPTION
WHEN OTHERS THEN
	v_code := SQLCODE;
	v_errm := SUBSTR(SQLERRM, 1, 64);
	DBMS_OUTPUT.PUT_LINE (v_code
	|| ' '
	|| v_errm);
END;
/
