#!AFORWARDSLASHbinAFORWARDSLASHsh
################################################################################
#                                                                              #
#  Copyright (c) 2014 JDA Software Group, Inc.                                 #
#  All rights reserved - Company Confidential                                  #
#                                                                              #
################################################################################

################################################################################
#                                                                              #
#  FILE NAME  :  instnew.sh                                                    #
#                                                                              #
#  DESCRIPTION:  Script for creating windows nt/2000 oracle service.           #
#                Run this script before trying to run the crdbs.sql scripts.   #
#                You can use the crdbsall.sql script to run all of the others. #
#                                                                              #
#  DATE     BY   PROJ   ID       DESCRIPTION                                   #
#  ======== ==== ====== ======== =============                                 #
#  25/05/04 JH   DCS    NEW7386  Porting to oracle 10g                         #
#  08/11/07 JH   DCS    DSP1441  Porting to oracle 11g                         #
################################################################################

(
cd ${ORACLE_HOME}AFORWARDSLASHbin

if [ -x "oradim.exe" ]
then
	ORADIM="oradim.exe"

elif [ -x "oradim80.exe" ]
then
	ORADIM="oradim80.exe"

elif [ -x "oradim73.exe" ]
then
	ORADIM="oradim73.exe"

else
	ORADIM=""
fi

if [ "$ORADIM" = "" ]
then
	echo ""
	echo "Could not find oradim executable !"
	echo ""
	exit 1
fi

echo ""
echo "Removing XXX oracle service (if it exists)..."
$ORADIM -delete \
	-sid XXX
echo "done"

echo ""
echo "Sleeping..."
sleep 60
echo "done"

if [ "$ORACLEVERSION" -ge "10" ]
then
	rm -f ${ORACLE_HOME}AFORWARDSLASHdatabaseAFORWARDSLASHPWD${ORACLE_SID}.ORA
	rm -f ${ORACLE_HOME}AFORWARDSLASHdatabaseAFORWARDSLASHhc_${ORACLE_SID}.dat
	SRVCSTART="-srvcstart system"
	SHUTMODE="-shutmode immediate"
else
	SRVCSTART=""
	SHUTMODE=""
fi

echo ""
echo "Adding XXX oracle service..."
$ORADIM -new \
	-sid XXX \
	-intpwd changemenow \
	-maxusers 5 \
	-startmode auto \
	$SRVCSTART \
	$SHUTMODE \
	-pfile "VVV/database/initXXX.ora"
echo "done"
echo ""
)

exit 0
