/******************************************************************************/
/*                                                                            */
/*  Copyright (c) 2014 JDA Software Group, Inc.                               */
/*  All rights reserved - Company Confidential                                */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :     lockobjs.sql                                          */
/*                                                                            */
/*     DESCRIPTION:     Lists current locking statements in the database.     */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/******************************************************************************/

clear breaks
clear columns

set pagesize 66
set linesize 132

column username format a10 heading 'Username' trunc
column program format a15 heading 'Program' trunc
column process format a9 heading 'Process'
column serial# format a10 heading 'Serial#'
column sid format a3 heading 'Sid'
column sql_hash_value format a15 heading 'Hash Value'
column sql_text format a64 heading 'Sql Text'
break on sql_hash_value skip 1 duplicate

select	b.username, 
	b.program,
	b.process process,
	to_char(b.serial#) serial#,
	to_char(b.sid) sid,
	to_char(b.sql_hash_value) sql_hash_value,
	a.sql_text sql_text
from v$sqltext a, v$session b
where a.address = b.sql_address
and a.hash_value = b.sql_hash_value
and b.lockwait is not null
order by a.address, a.piece;
