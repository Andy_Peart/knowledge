
set echo off
set heading off
set pagesize 1000
set feedback off

col operation format a78

select lpad(' ', 2 * level) ||
	operation || ' ' || decode(options, null, null, '('||options||') ') ||
	decode(object_name, null, null, 'OF '''||object_name||'''') ||
	decode(object_type, null, null, ' ('||object_type||')') || ' ' ||
	decode(cost, null, null, 'COST '''||cost||'''') operation
from plan_table
connect by prior id = parent_id
start with id = 1
order by timestamp;

