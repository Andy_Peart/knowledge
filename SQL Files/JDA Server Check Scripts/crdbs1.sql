--#############################################################################
--                                                                              
--  Copyright (c) 2014 JDA Software Group, Inc.                                 
--  All rights reserved - Company Confidential                                  
--                                                                              
--#############################################################################

--#############################################################################
--                                                                             
--  FILE NAME  : crdbs1.sql                                                    
--                                                                             
--  DESCRIPTION: This script creates the database, i.e. the system related
--		tablespaces, redo log files and control files. 
--									      
--              When sizing a database you should use the tools in the 
--		$DCS/tools/sizing directory.  Note that these tools assume 
--		that every record is fully populated with data (maximum 
--		density).		      
--                                                                             
--              Replace all X's by SID of database.     	              
--		Replace all Y's by paths.	 		              
--                                                                             
--		Do not use environment variables or ? for these, if you are   
--		planning to use windows based products for controlling or     
--		monitoring the database.				      
--                                                                             
--  DATE     BY   PROJ   ID       DESCRIPTION                                  
--  ======== ==== ====== ======== =============                                
--  29/05/98 JH   DCS    NEW3375  Windows NT porting to Oracle 8               
--  11/06/98 JH   DCS    NEW3383  Change default oracle passwords              
--  22/07/98 JH   DCS    PDR6913  Add support for 8 bit characters             
--  19/08/99 JH   DCS    NEW4123  Oracle 8.1.5 port on windows nt	      
--  11/02/02 JH   DCS    NEW5856  Porting to Oracle 9i
--  14/01/03 JH   DCS    NEW6474  Time zone support
--  03/03/04 JH   DCS    PDR8932  Time zone problems
--  30/03/04 JH   DCS    NEW6981  Multi byte character support                 
--  25/05/04 JH   DCS    NEW7386  Porting to oracle 10g                     
--  28/06/06 JH   DCS    ENH2780  Increase size of maxlogfiles
--  13/07/06 JH   DCS    ENH2806  Add more options to builddbdirs            
--  25/07/06 JH   DCS    ENH2846  Database creation/upgrade problems
--#############################################################################

connect internal/changemenow as sysdba

spool crdbs1_XXX.log

--
-- Display options
--

set echo on
set termout on

--
-- Start the instance for XXX database
--

WNTORACLE10GOFFstartup nomount pfile=$ORACLE_HOME/dbs/initXXX.ora

--
-- Create the <dbname> database
-- SYSTEM tablespace configuration guidelines:
--
-- 	General-Purpose ORACLE RDBMS 5Mb
--	Additional dictionary for applications 10-50Mb 
--
-- Redo Log File configuration guidelines:
--
-- 	Use 3+ redo log files to relieve ``cannot allocate new log...'' waits
-- 	Use ~100Kb per redo log file per connection to reduce checkpoints
--
-- Character set:
--
--      The default now is to create the database using the unicode
--	character set AL32UTF8.  If you choose a different character set
--	then licencing may not work and all the licence codes provided
--	with the application will need to be reset.
--
-- Time zones:
--
-- 	Should use "set time_zone = '0:00'" when you create the
--	database - this is an oracle and our recommendation!
--

create database "XXX"
	noarchivelog
	maxinstances  1
	maxlogmembers 5 
	maxlogfiles   20
	maxdatafiles  250
	maxloghistory 1000
	character set "DCSDBCHARSET"
	set time_zone = '0:00'
	datafile 'YYY/oradata/XXX/system01.dbf' size 500M
	extent management local
ORACLE10GON	sysaux datafile 'YYY/oradata/XXX/sysaux01.dbf' size 300M
--ORACLE10GON	default tablespace data
	default temporary tablespace temp 
		tempfile 'YYY/oradata/XXX/temp01.dbf' size 100M
	undo tablespace undo 
		datafile 'YYY/oradata/XXX/undo01.dbf' size 100M
		autoextend on next 10M 
		maxsize 200M
--		maxsize unlimited
	logfile group 1 
		(
		'YYYODD/oradata/XXX/redo01.log'
		) size 5M,
		group 2 
		(
		'YYYEVEN/oradata/XXX/redo02.log'
		) size 5M,
		group 3 
		(
		'YYYODD/oradata/XXX/redo03.log'
		) size 5M,
		group 4 
		(
		'YYYEVEN/oradata/XXX/redo04.log'
		) size 5M,
		group 5 
		(
		'YYYODD/oradata/XXX/redo05.log'
		) size 5M,
		group 6 
		(
		'YYYEVEN/oradata/XXX/redo06.log'
		) size 5M;

disconnect

--
-- End of crdbs1_XXX.sql
--

spool off

