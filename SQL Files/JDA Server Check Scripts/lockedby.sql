/******************************************************************************/
/*                                                                            */
/*  Copyright (c) 2014 JDA Software Group, Inc.                               */
/*  All rights reserved - Company Confidential                                */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :     lockedby.sql                                          */
/*                                                                            */
/*     DESCRIPTION:     Lists sql statements causing locks.                   */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   02/03/99 JH   DCS    NEW3829  New oracle admin scripts		      */
/*   20/10/99 JH   DCS    PDR7903  Changes required for Oracle 8              */
/******************************************************************************/
clear breaks

clear columns

set pagesize 66

set linesize 132

ttitle 'SQL Statements Causing Locks'
column username format a10 heading 'Username' trunc

column program format a10 heading 'Program' trunc

column process format a10 heading 'Process'

column sid format 99999999 heading 'SID'

column serial# format 99999999 heading 'Serial#'

column id1 format 99999999 heading 'ID'

column sql_text format a64 heading 'SQL' wrap

BREAK ON username ON program ON process ON sid SKIP 1 ON serial# ON id1

SELECT /*+ RULE */
    a.username,
    a.program,
    a.process,
    a.sid,
    a.serial#,
    b.id1,
    c.sql_text
FROM
    v$session   a,
    v$lock      b,
    v$sqltext   c
WHERE
    b.id1 IN(
        SELECT DISTINCT
            e.id1
        FROM
            v$session   d,
            v$lock      e
        WHERE
            d.lockwait = e.kaddr
    )
    AND a.sid = b.sid
        AND c.hash_value = a.sql_hash_value
            AND b.request = 0
ORDER BY
    c.address,
    c.piece;

