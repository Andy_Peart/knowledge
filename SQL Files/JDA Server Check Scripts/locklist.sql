/******************************************************************************/
/*                                                                            */
/*  Copyright (c) 2014 JDA Software Group, Inc.                               */
/*  All rights reserved - Company Confidential                                */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :     locklist.sql                                          */
/*                                                                            */
/*     DESCRIPTION:     Lists currently held locks in the database.           */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   20/10/99 JH   DCS    PDR7903  Changes required for Oracle 8              */
/*   27/06/11 JH   DCS    DSP4415  Add module/action to query                 */
/******************************************************************************/
CLEAR BREAKS

CLEAR COLUMNS

SET PAGESIZE 66

SET LINESIZE 132

COLUMN username FORMAT a10 HEADING 'Username' TRUNC

COLUMN program FORMAT a15 HEADING 'Program' TRUNC

COLUMN process FORMAT a9 HEADING 'Process'

COLUMN sid FORMAT a3 HEADING 'Sid'

COLUMN serial# FORMAT a10 HEADING 'Serial#'

COLUMN sql_hash_value FORMAT a15 HEADING 'Hash Value'

COLUMN lock_type FORMAT a26 HEADING 'Lock Type'

COLUMN mode_held FORMAT a13 HEADING 'Mode Held'

COLUMN mode_requested FORMAT a13 HEADING 'Mode Req'

COLUMN lock_id1 FORMAT a8 HEADING 'Id1'

COLUMN lock_id2 FORMAT a8 HEADING 'Id2'

COLUMN module FORMAT a48 HEADING 'Module'

COLUMN action FORMAT a32 HEADING 'Action'

COLUMN event FORMAT a40 HEADING 'Event'

COLUMN last_call_et HEADING 'LastCallET'

COLUMN seconds_in_wait HEADING 'SecsInWait'

COLUMN logon_time FORMAT a19 HEADING 'LogonTime'

BREAK ON lock_id1 SKIP 1 DUPLICATE

SELECT /*+ RULE */
    b.username,
    b.program,
    b.process process,
    TO_CHAR(a.sid)sid,
    TO_CHAR(b.serial#)serial#,
    b.module,
    b.action,
    TO_CHAR(b.sql_hash_value)sql_hash_value,
    DECODE(a.type,'MR','Media Recovery','RT','Redo Thread','UN','User Name','TX','Transaction','TM','DML','UL','PL/SQL User Lock'
    ,'DX','Distributed Transaction','CF','Control File','IS','Instance State','FS','File Set','IR','Instance Recovery','ST','Disk Space Transaction'
    ,'TS','Temp Segment','IV','Library Cache Invalidation','LS','Log Start or Switch','RW','Row Wait','SQ','Sequence Number','TE'
    ,'Extend Table','TT','Temp Table',a.type)lock_type,
    DECODE(a.lmode,0,'None',1,'Null',2,'Row-S (SS)',3,'Row-X (SX)',4,'Share',5,'S/Row-X (SSX)',6,'Exclusive',TO_CHAR(a.lmode))mode_held
    ,
    DECODE(a.request,0,'None',1,'Null',2,'Row-S (SS)',3,'Row-X (SX)',4,'Share',5,'S/Row-X (SSX)',6,'Exclusive',TO_CHAR(a.request)
    )mode_requested,
    TO_CHAR(a.id1)lock_id1,
    TO_CHAR(a.id2)lock_id2,
    b.event,
    b.last_call_et,
    b.seconds_in_wait,
    TO_CHAR(b.logon_time,'DD-MM-YYYY HH24:MI:SS')logon_time
FROM
    v$lock      a,
    v$session   b
WHERE
    (a.sid = b.sid
     AND a.request != 0)
    OR(a.sid = b.sid
       AND a.request = 0
       AND a.lmode != 4
       AND(a.id1,
           a.id2)IN(
        SELECT
            c.id1,
            c.id2
        FROM
            v$lock c
        WHERE
            c.request != 0
            AND a.id1 = c.id1
            AND a.id2 = c.id2
    ))
ORDER BY
    a.id1,
    a.id2,
    a.request;
    
 alter system kill session '261,15413';
  alter system kill session '460,54942';