/******************************************************************************/
/*                                                                            */
/*  Copyright (c) 2014 JDA Software Group, Inc.                               */
/*  All rights reserved - Company Confidential                                */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :     rollback.sql                                          */
/*                                                                            */
/*     DESCRIPTION:     Display rollback segment information.                 */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/******************************************************************************/

clear breaks
clear columns

set pagesize 66
set linesize 132

column name format a10 heading 'Name' trunc
column rstatus format a10 heading 'Status' trunc
column optsize format 999,999,990 heading 'Optimal (B)'
column hwmsize format 999,999,990 heading 'Highest (B)'
column rssize format 999,999,990 heading 'Current (B)'
column writes format 999,999,990 heading 'Writes'
column xacts format 999,990 heading 'Trans''s'
column shrinks format 999,990 heading 'Shrinks'
column aveshrink format 999,999,990 heading 'Shrink (B)'
column aveactive format 999,999,990 heading 'Active (B)'

select rn.name,
	initcap(rs.status) rstatus,
	rs.optsize,
	rs.hwmsize,
	rs.rssize,
	rs.writes,
	rs.xacts,
	rs.shrinks,
	rs.aveshrink,
	rs.aveactive
from v$rollstat rs, v$rollname rn
where rs.usn = rn.usn
order by 1;

