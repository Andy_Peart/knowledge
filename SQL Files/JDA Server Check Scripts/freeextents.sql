/******************************************************************************/
/*                                                                            */
/*  Copyright (c) 2014 JDA Software Group, Inc.                               */
/*  All rights reserved - Company Confidential                                */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :     freeextents.sql                                       */
/*                                                                            */
/*     DESCRIPTION:     Show information about free extents, specifically     */
/*                      how fragmented the tablespaces are.                   */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/******************************************************************************/

clear breaks
clear columns

set pagesize 66
set linesize 80
set verify off

column tablespace_name format a15 heading 'Tablespace' trunc
column file_id format 999 heading 'File ID'
column pieces format 999 heading 'Pieces'
column maximum format 999,990 heading 'Maximum'
column minimum format 999,990 heading 'Minimum'
column average format 999,990 heading 'Average'
column total format 999,990 heading 'Total'

select tablespace_name, 	
	file_id,
	count(*) pieces,
	max(blocks) maximum,
	min(blocks) minimum,
	avg(blocks) average,
	sum(blocks) total
from dba_free_space
group by tablespace_name, file_id
order by tablespace_name, file_id;

