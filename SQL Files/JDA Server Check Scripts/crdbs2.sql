--#############################################################################
--                                                                              
--  Copyright (c) 2014 JDA Software Group, Inc.                                 
--  All rights reserved - Company Confidential                                  
--                                                                              
--#############################################################################

--#############################################################################
--                                                                             
--  FILE NAME  : crdbs2.sql                                                    
--                                                                             
--  DESCRIPTION: This script takes care of all commands necessary to create    
--		an OFA compliant database after the create database command   
--		has succeeded.	The tablespaces created by this script are:   
--                                                                             
--               data (medium and large if uncommented)
--		 archive (medium and large if uncommented)
--               audit (medium and large if uncommented)
--               snapshot (medium and large if uncommented)
--		 indexes (medium and large if uncommented)
--               tblsync                                                       
--               media
--               report
--               xdb
--               mist (if environment variable set)
--               moca (if environment variable set)
--               ems (if environment variable set)
--               intg (if environment variable set)
--                                                                             
--              Replace all X's by SID of database.     	              
--		Replace all Y's by paths.	 		              
--                                                                             
--		Do not use environment variables or ? for these, if you are   
--		planning to use windows based products for controlling or     
--		monitoring the database.				      
--                                                                             
--  DATE     BY   PROJ   ID       DESCRIPTION                                  
--  ======== ==== ====== ======== =============                                
--  29/05/98 JH   DCS    NEW3375  Windows NT porting to Oracle 8               
--  11/06/98 JH   DCS    NEW3383  Change default oracle passwords              
--  21/04/99 JH   DCS    NEW3924  Table auditing functionality                 
--  19/08/99 JH   DCS    NEW4123  Oracle 8.1.5 port on windows nt              
--  11/02/02 JH   DCS    NEW5856  Porting to Oracle 9i
--  19/02/02 JH   DCS    NEW5891  Table space sizing
--  30/08/02 JH   DCS    NEW6243  Synchronise tables functionality
--  08/11/05 JH   DCS    NEW7942  Enhancements to SKU Image Processing
--  03/05/06 JH   DCS    ENH2620  Database security changes for customer
--  25/07/06 JH   DCS    ENH2846  Database creation/upgrade problems         
--  08/11/07 JH   DCS    DSP1441  Porting to oracle 11g                      
--  12/09/09 RMW  DCS    DSP2571  Extend tablespace for Integrator Mappings  
--  30/10/12 JH   DCS    DSP3420  Table data snapshot functionality
--#############################################################################

spool crdbs2_XXX.log

connect internal/changemenow as sysdba

--
-- Display options
--

set echo on
set termout on

--
-- Create tablespace(s) for data segments
--

create tablespace data 
	datafile 'YYY/oradata/XXX/data01.dbf' size 250M
        extent management local uniform size 128K
        segment space management auto;

--
-- Uncomment and configure if using different size tablespaces for data
-- (also uncomment relevant tablespace quota bit in crdbs4.sql script)
--
--create tablespace datamedium
--	datafile 'YYY/oradata/XXX/datamedium01.dbf' size 250M
--      extent management local uniform size 4M
--      segment space management auto;

--
-- Uncomment and configure if using different size tablespaces for data
-- (also uncomment relevant tablespace quota bit in crdbs4.sql script)
--
--create tablespace datalarge
--	datafile 'YYY/oradata/XXX/datalarge01.dbf' size 250M
--      extent management local uniform size 128M
--      segment space management auto;

--
-- Create tablespace(s) for archive data segments
--

create tablespace archive
	datafile 'YYY/oradata/XXX/archive01.dbf' size 30M
        extent management local uniform size 128K
        segment space management auto;

--
-- Uncomment and configure if using different size tablespaces for data
-- (also uncomment relevant tablespace quota bit in crdbs4.sql script)
--
--create tablespace archivemedium
--	datafile 'YYY/oradata/XXX/archivemedium01.dbf' size 30M
--      extent management local uniform size 4M
--      segment space management auto;

--
-- Uncomment and configure if using different size tablespaces for data
-- (also uncomment relevant tablespace quota bit in crdbs4.sql script)
--
--create tablespace archivelarge
--	datafile 'YYY/oradata/XXX/archivelarge01.dbf' size 30M
--      extent management local uniform size 128M
--      segment space management auto;

--
-- Create tablespace(s) for auditing data segments
--

create tablespace auditing
	datafile 'YYY/oradata/XXX/audit01.dbf' size 30M
        extent management local uniform size 128K
        segment space management auto;

--
-- Uncomment and configure if using different size tablespaces for data
-- (also uncomment relevant tablespace quota bit in crdbs4.sql script)
--
--create tablespace auditingmedium
--	datafile 'YYY/oradata/XXX/auditmedium01.dbf' size 30M
--      extent management local uniform size 4M
--      segment space management auto;

--
-- Uncomment and configure if using different size tablespaces for data
-- (also uncomment relevant tablespace quota bit in crdbs4.sql script)
--
--create tablespace auditinglarge
--	datafile 'YYY/oradata/XXX/auditlarge01.dbf' size 30M
--      extent management local uniform size 128M
--      segment space management auto;

--
-- Create tablespace(s) for snapshot data segments
--

create tablespace snapshot
	datafile 'YYY/oradata/XXX/snapshot01.dbf' size 30M
        extent management local uniform size 128K
        segment space management auto;

--
-- Uncomment and configure if using different size tablespaces for data
-- (also uncomment relevant tablespace quota bit in crdbs4.sql script)
--
--create tablespace snapshotmedium
--	datafile 'YYY/oradata/XXX/snapshotmedium01.dbf' size 30M
--      extent management local uniform size 4M
--      segment space management auto;

--
-- Uncomment and configure if using different size tablespaces for data
-- (also uncomment relevant tablespace quota bit in crdbs4.sql script)
--
--create tablespace snapshotlarge
--	datafile 'YYY/oradata/XXX/snapshotlarge01.dbf' size 30M
--      extent management local uniform size 128M
--      segment space management auto;

--
-- Create tablespace(s) for index segments
--

create tablespace indexes 
	datafile 'YYY/oradata/XXX/indexes01.dbf' size 250M
        extent management local uniform size 128K
        segment space management auto;

--
-- Uncomment and configure if using different size tablespaces for data
-- (also uncomment relevant tablespace quota bit in crdbs4.sql script)
--
--create tablespace indexesmedium
--	datafile 'YYY/oradata/XXX/indexesmedium01.dbf' size 250M
--      extent management local uniform size 4M
--      segment space management auto;

--
-- Uncomment and configure if using different size tablespaces for data
-- (also uncomment relevant tablespace quota bit in crdbs4.sql script)
--
--create tablespace indexeslarge
--	datafile 'YYY/oradata/XXX/indexeslarge01.dbf' size 250M
--      extent management local uniform size 128M
--      segment space management auto;

--
-- Create tablespace for table synchronisation segments
--

create tablespace tblsync
        datafile 'YYY/oradata/XXX/tblsync01.dbf' size 100M
        extent management local uniform size 40K
        segment space management auto;

--
-- Create tablespace for media (sku images etc) segments
--

create tablespace media
        datafile 'YYY/oradata/XXX/media01.dbf' size 30M
        extent management local uniform size 1M
        segment space management auto;

--
-- Create tablespace for jasper report archiving segments
--

create tablespace report
        datafile 'YYY/oradata/XXX/report01.dbf' size 30M
        extent management local uniform size 1M
        segment space management auto;

--
-- Create tablespace for package logging segments
--

create tablespace logging
        datafile 'YYY/oradata/XXX/logging01.dbf' size 30M
        extent management local uniform size 1M
        segment space management auto;

--
-- Create tablespace for oracle 11g network access (utl_smtp etc) segments.
-- If configuring ACLs for http/smtp access this should be increased to 300M.
--

ORACLE11GONcreate tablespace xdb
ORACLE11GON        datafile 'YYY/oradata/XXX/xdb01.dbf' size 100M
ORACLE11GON        extent management local uniform size 128K
ORACLE11GON        segment space management auto;

MISTUSERON--
MISTUSERON-- Create tablespace(s) for MicroStrategy (mist) segments
MISTUSERON--
MISTUSERON
MISTUSERONcreate tablespace mist 
MISTUSERON	datafile 'YYY/oradata/XXX/mist01.dbf' size 50M
MISTUSERON      extent management local uniform size 128K
MISTUSERON      segment space management auto;

MOCAUSERON--
MOCAUSERON-- Create tablespace(s) for MOCA (moca) segments
MOCAUSERON--
MOCAUSERON
MOCAUSERONcreate tablespace moca 
MOCAUSERON	datafile 'YYY/oradata/XXX/moca01.dbf' size 250M
MOCAUSERON      extent management local uniform size 128K
MOCAUSERON      segment space management auto;

EMSUSERON--
EMSUSERON-- Create tablespace(s) for EMS (ems) segments
EMSUSERON--
EMSUSERON
EMSUSERONcreate tablespace ems 
EMSUSERON      datafile 'YYY/oradata/XXX/ems01.dbf' size 100M
EMSUSERON      extent management local uniform size 128K
EMSUSERON      segment space management auto;

INTGUSERON--
INTGUSERON-- Create tablespace(s) for INTG (integrator) segments
INTGUSERON--
INTGUSERON
INTGUSERONcreate tablespace intg 
INTGUSERON	datafile 'YYY/oradata/XXX/intg01.dbf' size 400M
INTGUSERON      extent management local uniform size 128K
INTGUSERON      segment space management auto;

disconnect

--
-- End of crdbs2_XXX.sql
--

spool off

