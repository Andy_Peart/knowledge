--#############################################################################
--                                                                              
--  Copyright (c) 2014 JDA Software Group, Inc.                                 
--  All rights reserved - Company Confidential                                  
--                                                                              
--#############################################################################

--#############################################################################
--                                                                             
--  FILE NAME  : crdbs3.sql                                                    
--                                                                             
--  DESCRIPTION: Run procedural option, deadlock monitoring, shared pool,      
--		and product user profile scripts into the database.	      
--                                                                             
--  DATE     BY   PROJ   ID       DESCRIPTION                                  
--  ======== ==== ====== ======== =============                                
--  29/05/98 JH   DCS    NEW3375  Windows NT porting to Oracle 8               
--  11/06/98 JH   DCS    NEW3383  Change default oracle passwords              
--  20/10/00 RMC  DCS    NEW5096  Include UTL_RAW packages		      
--  24/08/01 RMC  DCS    NEW5548  Include DBMS_LOB packages		      
--  11/02/02 JH   DCS    NEW5856  Porting to Oracle 9i
--  11/08/04 JH   DCS    NEW7453  Allow unicode password
--  25/07/06 JH   DCS    ENH2846  Database creation/upgrade problems
--#############################################################################

spool crdbs3_XXX.log

connect internal/changemenow as sysdba

--
-- Display options
--

set echo on
set termout on

--
-- Create database objects
--

@$ORACLE_HOME/rdbms/admin/catalog.sql
@$ORACLE_HOME/rdbms/admin/catblock.sql
@$ORACLE_HOME/rdbms/admin/catproc.sql
@$ORACLE_HOME/rdbms/admin/catoctk.sql
@$ORACLE_HOME/rdbms/admin/catobtk.sql
@$ORACLE_HOME/rdbms/admin/dbmspool.sql
@$ORACLE_HOME/rdbms/admin/utlxplan.sql

--
-- To use oracle smtp/http functionality with oracle 11g 
-- you need to run these scripts manually following the 
-- instructions in the readme.txt files in the 
-- $DCS/server/source/ansi-c/emaildae/lis and
-- $DCS/server/source/ansi-c/traxcomss/lis directories.
--

--ORACLE11GON@$ORACLE_HOME/rdbms/admin/catqm.sql <password> XDB TEMP
--ORACLE11GON@$ORACLE_HOME/rdbms/admin/catxdbj.sql

--
-- Sql*plus help system
--

connect system/manager

@$ORACLE_HOME/sqlplus/admin/pupbld.sql
@$ORACLE_HOME/sqlplus/admin/help/hlpbld.sql helpus.sql
@$ORACLE_HOME/sqlplus/admin/help/helpus.sql

disconnect

--
-- End of crdbs3_XXX.sql
--

spool off

