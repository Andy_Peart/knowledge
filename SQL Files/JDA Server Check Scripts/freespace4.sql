/******************************************************************************/
/*                                                                            */
/*  Copyright (c) 2014 JDA Software Group, Inc.                               */
/*  All rights reserved - Company Confidential                                */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :     freespace4.sql                                        */
/*                                                                            */
/*     DESCRIPTION:     Displays free space information for all tablespaces,  */
/*			by tablespace.					      */
/*                                                                            */
/*                      See notes below for source of script and a summary    */
/*			of the output generated - probably the best freespace */
/*			script we have now - nice one Tom!                    */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/******************************************************************************/

clear col
clear break
clear comp

set doc on
set echo off
set verify off
set linesize 132
set pagesize 23
set feedback off

/*

Reporting on Database Free Space:
- from Oracle Magazine July/August 2006 
- article by Tom Kyte (AskTom)

I would like a report in SQL*Plus that shows the free space by tablespace. Do you have a working query that provides that? 

I do-I've had one for a long time. Basically, what I have to do is generate a query that reports free space by tablespace (aggregating DBA_FREE_SPACE to the tablespace level) and join that to a query that reports the space allocated to each tablespace (including temporary tablespaces). 

The problem is that DBA_FREE_SPACE reports space at the extent level within a tablespace and DBA_DATA_FILES/DBA_TEMP_FILES reports allocated space by file within a tablespace. I need to aggregate the data in each of these views to the tablespace level before I combine them. Inline views are very useful for doing this, and I'll make use of them in the query in Listing 2. Additionally, to accommodate tablespaces that are completely full (and hence would have no entries in DBA_FREE_SPACE), I'll use an outer join to put the answer together. 

The query in Listing 2 supplies the following output:

Tablespace Name: The name of the tablespace. A leading asterisk (*) in the tablespace name indicates that the tablespace is a locally managed one, whereas a leading blank means that it is an old-fashioned dictionary-managed tablespace. If the second character is a, that tells you that the tablespace is using Automatic Segment Space Management (ASSM) managed storage, whereas if the second character is M, that tells you that the tablespace is manually managed (pctused, freelists, and so on are used to control space utilization). 

Kbytes: Allocated space of the tablespace; the sum of kilobytes consumed by all datafiles associated with the tablespace. 

Used: Space in the tablespace that is used by some segment. 

Free: Space in the tablespace not allocated to any segment. 

% Used: Ratio of free to allocated space. 

Largest: The size of the largest contiguous set of blocks available (useful mostly with dictionary-managed tablespaces). If this number in a dictionary-managed tablespace is smaller than the next extent for some object, that object could fail with an "out of space" message, even if the FREE column says there is lots of free space. 

MaxPoss Kbytes: The autoextend maximum size. (Note: This can be smaller than the allocated size! You can set the maximum size to be less than the current size of a file.) 

% Max Used: How much of the maximum autoextend size has been used so far. 

*/

column          dummy noprint
column          pct_used        format 999.9                    heading "%|Used"
column          name            format a19                      heading "Tablespace Name"
column          Kbytes          format 999,999,999              heading "Kbytes"
column          used            format 999,999,999              heading "Used"
column          free            format 999,999,999              heading "Free"
column          largest         format 999,999,999              heading "Largest"
column          max_size        format 999,999,999              heading "MaxPoss|Kbytes"
column          pct_max_used    format 999.                     heading "%|Max|Used"
break           on report
compute sum of kbytes on report
compute sum of free on report
compute sum of used on report

select (select decode(extent_management,'LOCAL','*',' ') ||
               decode(segment_space_management,'AUTO','a ','m ')
          from dba_tablespaces where tablespace_name = b.tablespace_name) ||
nvl(b.tablespace_name,
             nvl(a.tablespace_name,'UNKOWN')) name,
       kbytes_alloc kbytes,
       kbytes_alloc-nvl(kbytes_free,0) used,
       nvl(kbytes_free,0) free,
       ((kbytes_alloc-nvl(kbytes_free,0))/
                          kbytes_alloc)*100 pct_used,
       nvl(largest,0) largest,
       nvl(kbytes_max,kbytes_alloc) Max_Size,
       decode( kbytes_max, 0, 0, (kbytes_alloc/kbytes_max)*100) pct_max_used
from ( select sum(bytes)/1024 Kbytes_free,
              max(bytes)/1024 largest,
              tablespace_name
       from  sys.dba_free_space
       group by tablespace_name ) a,
     ( select sum(bytes)/1024 Kbytes_alloc,
              sum(maxbytes)/1024 Kbytes_max,
              tablespace_name
       from sys.dba_data_files
       group by tablespace_name
       union all
      select sum(bytes)/1024 Kbytes_alloc,
              sum(maxbytes)/1024 Kbytes_max,
              tablespace_name
       from sys.dba_temp_files
       group by tablespace_name )b
where a.tablespace_name (+) = b.tablespace_name
order by 1;

