/******************************************************************************/
/*                                                                            */
/*  Copyright (c) 2016 JDA Software Group, Inc.                               */
/*  All rights reserved - Company Confidential                                */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :     blockedbywait.sql                                     */
/*                                                                            */
/*     DESCRIPTION:     Lists sessions currently blocked by a wait event      */
/*                      along with the session that is causing the wait.      */
/*                                                                            */
/*	DATE	ID	DESCRIPTION                                           */
/*	======= ===     =============                                         */
/*	28-JUL-16 DISP-11129	Initial                                       */	
/******************************************************************************/

clear breaks
clear columns

set pagesize 999
set linesize 200

column sid format 9999999 heading 'Sid'
column serial# format 9999999 heading 'Serial#'
column blocking_session format 9999999 heading 'Blocker'
column program format a25 heading 'Program' trunc
column wait_class format a20 heading 'Wait Class' trunc
column status format a8 heading 'Status'
column process format a9 heading 'Process'
column module format a25 heading 'Module'
column last_call_et format 9999999 heading 'LastCall'
column event format a25 heading 'Event'

select	distinct sid, serial#, blocking_session, program, wait_class,
			status, process, module, last_call_et, event
from	v$session
where	sid in (
		select	blocking_session
		from v$session
		where blocking_session is not null
		)
or	blocking_session is not null
order by nvl (blocking_session, sid), blocking_session nulls first;

