/******************************************************************************/
/*                                                                            */
/*  Copyright (c) 2014 JDA Software Group, Inc.                               */
/*  All rights reserved - Company Confidential                                */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :     sessionclient.sql                                     */
/*                                                                            */
/*     DESCRIPTION:     Lists session information for clients.                */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   02/03/99 JH   DCS    NEW3829  New oracle admin scripts		      */
/******************************************************************************/

clear breaks
clear columns

set pages 66
set lines 80

column program heading 'Program' format a12 trunc
column process heading 'Process' format 9999999999
column name heading 'Name' format a40 trunc
column value heading 'Value' format 9999999999

break on program on process skip page

select a.program, a.process, c.name, b.value
from v$session a, v$sesstat b, v$statname c
where a.sid = b.sid
and b.statistic# = c.statistic#
and b.value != 0
and a.username='DCSUSR'
order by 1, 2, 3;
