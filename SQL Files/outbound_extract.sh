#!/bin/bash

. $HOME/dcs/.dcs_profile

cd $DCS_COMMSDIR/outwork/.

# setup the filename 
date=`date +"%m%d"`
time=`date|awk '{print $4}'|sed 's/\://g'`
file=VITESSE_OUTBOUND_$date$time.csv

#Check if there is any data to export
sqlplus -s $ORACLE_USR > KEYS << EOF
SET SERVEROUTPUT ON
SET FEEDBACK OFF

declare
	key_count number;
begin
	select
		case
			when
				(
					select
						nvl(max(key),0)
					from
						inventory_transaction
					where
						pallet_id in
						(
							select
								pallet_id
							from
								inventory_transaction
							where
								key in
								(
									select
										key
									from
										inventory_transaction
									where
										dstamp >= sysdate - (11/1440)
								)
								and code      = 'Pick'
								and to_loc_id ='QCDECANT'
								and dstamp   >= sysdate - (600000/86400000)
								and pallet_id not      in
								(
									select
										pallet_id
									from
										inventory_transaction
									where
										key >
										(
											select
												key
											from
												inventory_transaction
											where
												dstamp >= sysdate - (11/1440)
										)
										and client_id           = 'PLT'
										and pallet_id is not null
										and code                = 'Pick'
										and to_loc_id           ='QCDECANT'
										and dstamp              > sysdate - (550000/86400000)
										and dstamp             <= sysdate - (559999/86400000)
								)
						)
						and to_loc_id = 'CONTAINER'
				)
				> 0
				then 2
			when
				(
					select
						nvl(max(key),0)
					from
						inventory_transaction
					where
						key in
						(
							select
								key
							from
								inventory_transaction
							where
								dstamp >= sysdate - (11/1440)
						)
						and dstamp >= sysdate - (10/1440)
						and code    = 'Repack'
				)
				> 0
				then 2
			when
				(
					select
						nvl(max(key),0)
					from
						inventory_transaction
					where
						key >
						(
							select
								key
							from
								inventory_transaction
							where
								dstamp >= sysdate - (11/1440)
						)
						and code = 'Pick'
						and to_loc_id in
						(
							select
								location_id
							from
								location
							where
								loc_type = 'Repack'
						)
						and dstamp > sysdate - (600000/86400000)
						and pallet_id not   in
						(
							select
								pallet_id
							from
								inventory_transaction
							where
								key in
								(
									select
										key
									from
										inventory_transaction
									where
										dstamp >= sysdate - (11/1440)
								)
								and code      = 'Pick'
								and to_loc_id ='QCDECANT'
								and dstamp    > sysdate - (550000/86400000)
								and dstamp   <= sysdate - (559999/86400000)
						)
				)
				> 0
				then 2
				else 0
		end as Key
	into
		key_count
	from
		Dual
	;
	
	DBMS_OUTPUT.PUT_LINE(key_count);
end;
/
exit
EOF

keycount=`cat KEYS`

#If there is any data to extract then run an extract

if ((keycount > 0)); then
sqlplus -s $ORACLE_USR << ! >> $file
        set feedback off
        set heading off
        set pagesize 999
        set linesize 32767
        select
        	code
          ,','
          , list_id
          ,','
          , site_id
          ,','
          , from_loc_id
          ,','
          , sku_id
          ,','
          , user_id
          ,','
          , dstamp
          ,','
          , update_qty
          ,','
          , original_qty
          ,','
          , user_def_type_2
          ,','
          , user_def_type_3
          ,','
          , user_def_type_4
          ,','
          , user_def_type_5
          ,','
          , reference_id
        from
        	inventory_transaction
        where
        	key in
        	(
        		select
        			key
        		from
        			inventory_transaction
        		where
        			dstamp >= sysdate - (11/1440)
        	)
        	and pallet_id in
        	(
        		select
        			Pallet_id
        		from
        			inventory_transaction
        		where
        			key in
        			(
        				select
        					key
        				from
        					inventory_transaction
        				where
        					dstamp >= sysdate - (11/1440)
        			)
        			and code      = 'Pick'
        			and to_loc_id ='QCDECANT'
        			and dstamp   >= sysdate - (600000/86400000)
        			and pallet_id not      in
        			(
        				select
        					pallet_id
        				from
        					inventory_transaction
        				where
        					key in
        					(
        						select
        							key
        						from
        							inventory_transaction
        						where
        							dstamp >= sysdate - (11/1440)
        					)
        					and code      = 'Pick'
        					and to_loc_id ='QCDECANT'
        					and dstamp    > sysdate - (550000/86400000)
        					and dstamp   <= sysdate - (559999/86400000)
        			)
        	)
        	and to_loc_id = 'CONTAINER'
        union
        select
        	code
          ,','
          , list_id
          ,','
          , site_id
          ,','
          , from_loc_id
          ,','
          , sku_id
          ,','
          , user_id
          ,','
          , dstamp
          ,','
          , update_qty
          ,','
          , original_qty
          ,','
          , user_def_type_2
          ,','
          , user_def_type_3
          ,','
          , user_def_type_4
          ,','
          , user_def_type_5
          ,','
          , reference_id
        from
        	inventory_transaction
        where
        	key in
        	(
        		select
        			key
        		from
        			inventory_transaction
        		where
        			dstamp >= sysdate - (11/1440)
        	)
        	and pallet_id in
        	(
        		select
        			Pallet_id
        		from
        			inventory_transaction
        		where
        			key in
        			(
        				select
        					key
        				from
        					inventory_transaction
        				where
        					dstamp >= sysdate - (11/1440)
        			)
        			and code = 'Pick'
        			and to_loc_id in
        			(
        				select
        					location_id
        				from
        					location
        				where
        					loc_type = 'Repack'
        			)
        			and dstamp >= sysdate - (600000/86400000)
        			and pallet_id not    in
        			(
        				select
        					pallet_id
        				from
        					inventory_transaction
        				where
        					key in
        					(
        						select
        							key
        						from
        							inventory_transaction
        						where
        							dstamp >= sysdate - (11/1440)
        					)
        					and code = 'Pick'
        					and to_loc_id in
        					(
        						select
        							location_id
        						from
        							location
        						where
        							loc_type = 'Repack'
        					)
        					and dstamp  > sysdate - (550000/86400000)
        					and dstamp <= sysdate - (559999/86400000)
        			)
        	)
        	and to_loc_id = 'CONTAINER'
        UNION
        select
        	code
          ,','
          , list_id
          ,','
          , site_id
          ,','
          , from_loc_id
          ,','
          , sku_id
          ,','
          , user_id
          ,','
          , dstamp
          ,','
          , update_qty
          ,','
          , original_qty
          ,','
          , user_def_type_2
          ,','
          , user_def_type_3
          ,','
          , user_def_type_4
          ,','
          , user_def_type_5
          ,','
          , reference_id
        from
        	inventory_transaction
        where
        	key in
        	(
        		select
        			key
        		from
        			inventory_transaction
        		where
        			dstamp >= sysdate - (11/1440)
        	)
        	and dstamp >= sysdate - (10/1440)
        	and code    = 'Repack'
        ;
!

# remove empty lines from the output file
sed -i '/^$/d' $file

# copy to outarchive
cp $file ../outarchive/.

# move the file to the outtray
mv $file ../outtray/.
fi