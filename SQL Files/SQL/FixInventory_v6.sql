DECLARE
  l_count            NUMBER(5)                := 0;
  l_mt_count         NUMBER(5)                := 0;
  l_sm_count         NUMBER(5)                := 0;
  l_count_prob_tasks NUMBER(5)                := 0;
  c_client client.client_id%type              := 'WILKINSON';
  c_inv_commit_limit NUMBER(5)                := 100;
  c_rel_commit_limit NUMBER(5)                := 100;
  c_container_suffix move_task.pallet_id%type := 'CX';
  c_pallet_suffix move_task.pallet_id%type    := 'PX';
  c_problem location.location_id%type         := 'PROBLEM';
  c_problem_identifier NUMBER(5)              := 99;
  /*
  Search for Inventory without tasks
  */
  CURSOR InvNoTasksManifests
  IS
    SELECT *
    FROM inventory
    WHERE container_id IS NOT NULL
    AND client_id       = c_client
    AND location_id    <> c_problem
    AND nvl(condition_id, 'A')   <> c_problem
    AND container_id NOT LIKE '%X'
    AND NOT EXISTS
		(SELECT 1
		FROM move_task
		WHERE client_id  = inventory.client_id
		AND sku_id       = inventory.sku_id
		AND container_id = inventory.container_id
		)
	AND NOT EXISTS
		(SELECT 1
		FROM shipping_manifest
		WHERE client_id  = inventory.client_id
		AND sku_id       = inventory.sku_id
		AND container_id = inventory.container_id
		)
	AND NOT EXISTS
		(SELECT 1
		FROM order_container
		WHERE client_id  = inventory.client_id
		AND sku_id       = inventory.sku_id
		AND container_id = inventory.container_id
		) 
  AND exists
		(select location_id 
		from location 
		where loc_type not in ('Tag-FIFO', 'Bin', 'Tag-LIFO', 'Suspense', 'Trailer')
		and location_id = inventory.location_id
		and site_id = inventory.site_id);
  /*
  Search for Inventory with c_problem_identifier
  */
  CURSOR ProblemInvSearch
  IS
    SELECT *
    FROM inventory
    WHERE user_def_num_4 = c_problem_identifier
	AND location_id    <> c_problem
    AND client_id        = c_client
    AND NOT EXISTS
      (SELECT 1
      FROM interface_inventory_move
      WHERE pallet_id = inventory.pallet_id
      AND sku_id      = inventory.sku_id
      AND client_id   = INVENTORY.CLIENT_ID
      ) ;

  /*
  CursorRow declarations
  */
  InvNoTasksManifestsRow InvNoTasksManifests%rowtype;
  ProblemInvRow ProblemInvSearch%rowtype;
  /*
  Main Block
  */
BEGIN
  
  /*
  Search for Inventory with no Tasks or Manifests
  */  
  OPEN InvNoTasksManifests;
  LOOP
    FETCH InvNoTasksManifests INTO InvNoTasksManifestsRow;
    IF InvNoTasksManifests%NOTFOUND THEN
      dbms_output.put_line('InvNoTasksManifests Not Found');
      EXIT;
    END IF;
    /*
    Loop Counter
    */
    l_count := l_count + 1;
    /*
    If move_task count = 0 and shipping_manifest = 0
    then update the container with a suffix
    */
    UPDATE inventory
    SET container_id 	  = null,
      pallet_id 		    = null,
      USER_DEF_NOTE_1 	= 'Previous Container:' || container_id || ' Pallet:' || pallet_id,
      condition_id 		  = c_problem, 
      qty_allocated 	  = 0
    WHERE KEY        = InvNoTasksManifestsRow.key
    AND client_id    = c_client;
    dbms_output.put_line('key:' || InvNoTasksManifestsRow.key || ', ' || InvNoTasksManifestsRow.container_id || c_container_suffix);
    /*
    If loop counter hits a max commit limit, run commit
    */
    IF l_count = c_inv_commit_limit THEN
      dbms_output.put_line('commit');
      COMMIT;
    END IF;
  END LOOP;
  COMMIT;
  CLOSE InvNoTasksManifests;
EXCEPTION
WHEN OTHERS THEN
  dbms_output.put_line('exit script');
  /*We hit issues where the container / pallet is too large already:
  ORA-12899: value too large for column "DCSDBA"."INVENTORY"."CONTAINER_ID" (actual: 22, maximum: 20)
  so we can just handle this with committing what we can
  */  
  COMMIT;
END;
