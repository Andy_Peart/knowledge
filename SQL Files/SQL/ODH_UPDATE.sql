DECLARE
	l_count        NUMBER(5) := 0;
	v_rollback_limit NUMBER(5) := 100;
	v_code         NUMBER;
	v_errm         VARCHAR2(64);
	T_count        NUMBER(8) := 0;
	
	CURSOR CUR_ORD is
		select
			order_id
		  , order_type
		from
			order_header
		where
			status             = 'Released'
			and User_def_chk_3 = 'N'
		;
	
	ODH CUR_ORD%rowtype
Begin
	open CUR_SKU;
	LOOP
		FETCH CUR_ORD
		into
			ODH
		;
		
		/* IF NO DATA then Exit the procedure */
		IF CUR_ORD%notfound THEN
			dbms_output.put_line('Completed');
			EXIT;
		END IF;
		/*Intit loop counter then increment*/
		l_count           = l_count + 1;
		IF odh.order_type = 'ISC2FLT' THEN
			UPDATE
				order_header
			SET user_def_type_8 = null
			  , user_def_num_1  = null
			  , user_def_num_2  = null
			where
				ODH.order_id = order_id
			;
		
		else
			UPDATE
				order_header
			SET v_gfs_carrier_pre       = 'X'
			  , v_gfs_error             = null
			  , v_gfs_error_description = null
			  , V_GFS_CONTRACT_NO       = null
			  , V_GFS_ERROR_CODE        = null
			  , V_GFS_ERROR_MESSAGE     = null
			  , user_def_type_8         = null
			  , user_def_num_1          = null
			  , user_def_num_2          = null
			where
				ODH.order_id = order_id
			;
		
		END IF;
		/*Check how many Rows have been updated*/
		IF l_count = v_rollback_limit THEN
			dbms_output.put_line('rollback');
					l_count           = 0;
			rollback;
		END IF;
				T_count           = T_count + 1;
	END LOOP;
	rollback;
		DBMS_OUTPUT.PUT_LINE (T_COUNT
	|| ' Orders Updated');
	/*Close open Cursor*/
	CLOSE CUR_ORD;
EXCEPTION
WHEN OTHERS THEN
EXCEPTION
WHEN OTHERS THEN
	v_code := SQLCODE;
	v_errm := SUBSTR(SQLERRM, 1, 64);
	DBMS_OUTPUT.PUT_LINE (v_code
	|| ' '
	|| v_errm);
END;