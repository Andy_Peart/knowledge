/******************************************************************************/
/*                                                                            */
/* NAME:         Update EAN on SKU Record                                     */
/*                                                                            */
/* DESCRIPTION:  Read Supplier Sku un update Ske Record                       */
/*                                                                            */
/*                                                                            */
/* Date       By  Proj    Ref      Description                                 /
/* ---------- --- ------- -------- -------------------------                  */
/* 30/09/2018 AMP WILKINSON        Update EAN on SKU                          */
/******************************************************************************/

DECLARE
l_count			NUMBER(5)				:= 0;
v_commit_limit 	NUMBER(5)               := 100;
v_client		VARCHAR(20)				:= 'WILKINSON'

CURSOR CUR_SKU is
	Select S.SKU_ID, SS.SUPLIER_SKU  
	From SKU S, SUPPLIER_SKU ss 
	where S.SKU_ID = S.EAN
	and s.SKU_id = SS.SKU_ID
	and s.sku_id = substr(ss.supplier_sku,0,7);
	
CUR_SKU_ROW := cur_sku%rowtype
	
Begin
	open CUR_SKU;
	LOOP
		FETCH CUR_SKU into CUR_SKU_ROW;
		/* IF NO DATA then Exit the procedure */
		IF CUR_SKU%notfound THEN
			dbms_output.put_line('No SKU to UPDATE');
			EXIT;
		END IF;
		
		/*Intit loop counter then increment*/
		l_count = l_count + 1;
		
		UPDATE SKU set EAN = CUR_SKU_ROW.SUPPLIER_SKU 
			where SKU_ID = CUR_SKU_ROW.SKU_ID
			and client_id = v_client;
		
		/*Check how many Rows have been updated*/
		IF l_count = v_commit_limit THEN
			dbms_output.put_line('commit');
			COMMIT;
		END IF;
	END LOOP;
	COMMIT;
	/*Close open Cursor*/
	CLOSE CUR_SKU;
	EXCEPTION
	WHEN OTHERS THEN
	dbms_output.put_line('Additional Unknown Error');
	/* Additional Commit as Belt and Braces */
	COMMIT;
END;
