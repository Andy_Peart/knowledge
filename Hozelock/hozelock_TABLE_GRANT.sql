/******************************************************************************/
/*                                                                            */
/* NAME:         Table Grant Script                                           */
/*                                                                            */
/* DESCRIPTION:  Grants dcsdba User full table access on hozelock             */
/*                                                                            */
/*                                                                            */
/* Date       By  Proj    Ref      Description                                 /
/* ---------- --- ------- -------- -------------------------                  */
/* 18/02/2019 AMP Internal         Initial Write                              */
/******************************************************************************/
Declare
	v_code NUMBER;
	v_errm VARCHAR2(64);
	/*SELECT all table Names from Schema into a Cursor*/
	cursor table_to_grant is
		SELECT
			Table_Name
		FROM
			User_Tables
		WHERE
			Table_Name NOT LIKE 'BIN\$%'
		ORDER BY
			Table_Name
		;
	
	Grant_table table_to_grant%rowtype;
begin
	/*Open the Cursor*/
	open table_to_grant;
	/*Loop Through each Table*/
	LOOP
		fetch table_to_grant
		into
			Grant_table
		;
		
		dbms_output.put_line(grant_table.table_name);
		/*IF no tables left EXIT*/
		IF table_to_grant%notfound then
			dbms_output.put_line('No more table to Grant');
			EXIT;
		END IF;
		/*REVOKE dcsdba Privelages*/
		execute immediate ('REVOKE ALL ON '
		||grant_table.table_name
		||' FROM dcsdba');
		dbms_output.put_line(grant_table.table_name
		||' REVOKED');
		/*GRANT dcsdba Privelages*/
		execute immediate ('GRANT
		SELECT
		,
		INSERT
		,
		UPDATE
		,
		DELETE
		ON
		'
		||grant_table.table_name
		||' to dcsdba');
		dbms_output.put_line(grant_table.table_name
		||' Granted');
		/* END LOOP when all tables granted */
	END LOOP;
	close table_to_grant;
	/* ERROR Handling */
EXCEPTION
WHEN OTHERS THEN
	v_code := SQLCODE;
	v_errm := SUBSTR(SQLERRM, 1, 64);
	DBMS_OUTPUT.PUT_LINE (v_code
	|| ' '
	|| v_errm);
END;
/