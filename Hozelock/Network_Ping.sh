#!/bin/bash
###################################### My Rewrite ###############################################

# Setup pre script install
# Personally I would set up a folder for this to run. Putting data files in the bin directory is really bad practice.
#
# make dir /home/dcsdba/Ping_Check
# make dir /home/dcsdba/Ping_check/outwork
# place IP list file in /home/dcsdba/Ping_check/outwork



. $HOME/dcs/.dcs_profile

OUTWORKDIR=$HOME/Ping_Check/outwork
OUTDIR=$HOME/Ping_Check
OUTFILETEMP=IP_Failure.tmp
OUTFILE=IP_Failure.txt
INFILE=names.txt

cd $OUTWORKDIR

date >> $OUTFILETMP
while read IP; do
    echo -n "$IP is " >> $OUTFILETMP
    if ping "$IP" -c1  > /dev/null 2>&1; then
        echo "up"
    else
        echo "down" >> $OUTFILETMP
    fi
done < $INFILE

cp $OUTFILETMP $OUTDIR/$OUTFILE
