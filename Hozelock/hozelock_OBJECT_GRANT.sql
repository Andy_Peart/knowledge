/******************************************************************************/
/*                                                                            */
/* NAME:         OBJECT Grant Script                                          */
/*                                                                            */
/* DESCRIPTION:  Grants dcsdba User full table access on hozelock             */
/*                                                                            */
/*                                                                            */
/* Date       By  Proj    Ref      Description                                 /
/* ---------- --- ------- -------- -------------------------                  */
/* 18/02/2019 AMP Internal         Initial Write                              */
/******************************************************************************/
Declare
	v_code NUMBER;
	v_errm VARCHAR2(64);
	/*SELECT all OBJECTS from Schema into a Cursor*/
	cursor object_to_grant is
		SELECT DISTINCT
			Object_Name
		FROM
			User_Objects
		WHERE
			(
				UPPER(Object_Type)    = 'PACKAGE'
				OR UPPER(Object_Type) = 'TYPE'
				OR UPPER(Object_Type) = 'FUNCTION'
				OR UPPER(Object_Type) = 'PROCEDURE'
			)
		;
	
	Grant_Object object_to_grant%rowtype;
begin
	/*Open the Cursor*/
	open object_to_grant;
	/*Loop Through each OBJECT*/
	LOOP
		fetch object_to_grant
		into
			Grant_Object
		;
		
		dbms_output.put_line(Grant_Object.object_name);
		/*IF no objects left EXIT*/
		IF object_to_grant%notfound then
			dbms_output.put_line('No more table to Grant');
			EXIT;
		END IF;
		/*REVOKE dcsdba Privelages*/
		execute immediate ('REVOKE ALL ON '
		||Grant_Object.object_name
		||' FROM dcsdba');
		dbms_output.put_line(Grant_Object.object_name
		||' REVOKED');
		/*GRANT dcsdba Privelages*/
		execute immediate ('GRANT
		EXECUTE
		ON
		'
		||Grant_Object.object_name
		||' to dcsdba');
		dbms_output.put_line(Grant_Object.object_name
		||' Granted');
		/* END LOOP when all objects granted */
	END LOOP;
	close object_to_grant;
	/* ERROR Handling */
EXCEPTION
WHEN OTHERS THEN
	v_code := SQLCODE;
	v_errm := SUBSTR(SQLERRM, 1, 64);
	DBMS_OUTPUT.PUT_LINE (v_code
	|| ' '
	|| v_errm);
END;
/