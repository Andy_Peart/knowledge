/******************************************************************************/
/*                                                                            */
/* NAME:         OBJECT View Script                                       */
/*                                                                            */
/* DESCRIPTION:  Grants dcsdba User full VIEW access on hozelock          */
/*                                                                            */
/*                                                                            */
/* Date       By  Proj    Ref      Description                                */
/* ---------- --- ------- -------- -------------------------                  */
/* 18/02/2019 AMP Internal         Initial Write                              */
/******************************************************************************/
Declare
	v_code NUMBER;
	v_errm VARCHAR2(64);
	/*SELECT all OBJECTS from Schema into a Cursor*/
	cursor object_to_grant is
		SELECT
			View_Name
		FROM
			User_Views
		ORDER BY
			View_Name
		;
	
	Grant_Object object_to_grant%rowtype;
begin
	/*Open the Cursor*/
	open object_to_grant;
	/*Loop Through each OBJECT*/
	LOOP
		fetch object_to_grant
		into
			Grant_Object
		;
		
		dbms_output.put_line(Grant_Object.view_name);
		/*IF no objects left EXIT*/
		IF object_to_grant%notfound then
			dbms_output.put_line('Nothing Left to Grant');
			EXIT;
		END IF;
		/*REVOKE dcsdba Privelages*/
		execute immediate ('REVOKE ALL ON '
		||Grant_Object.view_name
		||' FROM dcsdba');
		dbms_output.put_line(Grant_Object.view_name
		||' REVOKED');
		/*GRANT dcsdba Privelages*/
		execute immediate ('GRANT
		SELECT
		ON
		'
		||Grant_Object.view_name
		||' to dcsdba');
		dbms_output.put_line(Grant_Object.view_name
		||' Granted');
		/* END LOOP when all objects granted */
	END LOOP;
	close object_to_grant;
	/* ERROR Handling */
EXCEPTION
WHEN OTHERS THEN
	v_code := SQLCODE;
	v_errm := SUBSTR(SQLERRM, 1, 64);
	DBMS_OUTPUT.PUT_LINE (v_code
	|| ' '
	|| v_errm);
END;
/