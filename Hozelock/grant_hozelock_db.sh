#!/bin/sh
################################################################################
#         hozelock_grantaccess                                                  #
#                                                                              #
#DATE       BY                Change No   VERSION DESCRIPTION                  # 
#========== ================= =========== ======= =============================#
#18/02/2019 Andy Peart   	  --              1.0 Initial version              #
################################################################################

. $HOME/dcs/.dcs_profile


##################################################################################

echo ""
echo "SCRIPT Started to action grants"
echo ""
###################################################################################

# Grant hozelock user all tables #

echo "Granting DCSDBA table access to hozelock User"

sqlplus -s $ORACLE_USR <<EOF
SET SERVEROUTPUT ON;
@DCSDBA_TABLE_GRANT.sql
exit;
EOF

echo "hozelock now has access to DCSDBA Tables"
echo ""
##################################################################################

# Grant hozelock user all Objects #

echo "Granting DCSDBA OBJECTS access to hozelock User"

sqlplus -s $ORACLE_USR <<EOF
SET SERVEROUTPUT ON;
@DCSDBA_OBJECT_GRANT.sql
exit;
EOF

echo "hozelock now has access to DCSDBA Objects"
echo ""
##################################################################################

# Grant hozelock user all Views #

echo "Granting DCSDBA Views access to hozelock User"

sqlplus -s $ORACLE_USR <<EOF
SET SERVEROUTPUT ON;
@DCSDBA_VIEW_GRANT.sql
exit;
EOF

echo "hozelock now has access to DCSDBA VIEWS"
echo ""
##################################################################################

# Grant hozelock user all Sequences #

echo "Granting DCSDBA sequence access to hozelock User"

sqlplus -s $ORACLE_USR <<EOF
SET SERVEROUTPUT ON;
@DCSDBA_SEQ_GRANT.sql
exit;
EOF

echo "hozelock now has access to DCSDBA sequences"
echo ""

###################################################################################

# Grant DCSDBA user all tables #

echo "Granting hozelock table access to DCS User"

sqlplus -s hozelock/hozelock@$ORACLE_TNSNAME <<EOF
SET SERVEROUTPUT ON;
@hozelock_TABLE_GRANT.sql
exit;
EOF

echo "hozelock now has access to DCSDBA Tables"
echo ""
##################################################################################

# Grant DCSDBA user all Objects #

echo "Granting hozelock OBJECTS access to DCS User"

sqlplus -s hozelock/hozelock@$ORACLE_TNSNAME <<EOF
SET SERVEROUTPUT ON;
@hozelock_OBJECT_GRANT.sql
exit;
EOF

echo "hozelock now has access to DCSDBA Objects"
echo ""
##################################################################################

# Grant DCSDBA user all Views #

echo "Granting hozelock Views access to DCS User"

sqlplus -s hozelock/hozelock@$ORACLE_TNSNAME <<EOF
SET SERVEROUTPUT ON;
@hozelock_VIEW_GRANT.sql
exit;
EOF

echo "hozelock now has access to DCSDBA VIEWS"
echo ""
##################################################################################

# Grant DCSDBA user all Sequences #

echo "Granting hozelock sequence access to DCS User"

sqlplus -s hozelock/hozelock@$ORACLE_TNSNAME <<EOF
SET SERVEROUTPUT ON;
@hozelock_SEQ_GRANT.sql
exit;
EOF

echo "hozelock now has access to DCSDBA sequences"
echo ""

##################################################################################

echo "Script Complete"