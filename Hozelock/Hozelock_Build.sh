#!/bin/bash

################################################################################
#         Hozelock Build Schema                                                #
#                                                                              #
#DATE       BY                Change No   VERSION DESCRIPTION                  # 
#========== ================= =========== ======= =============================#
#04/03/2020 Andy Peart   	  --              1.0 Initial version              #
################################################################################

. $HOME/dcs/.dcs_profile

echo "SCRIPT Started to CREATE HOZELOCK User"

sqlplus -s $DCS_SYSDBA <<EOF
SET SERVEROUTPUT ON;
create tablespace hozelock_tablespace
datafile 'hozelock_tablespace.dat'
size 10M autoextend on;
 
create temporary tablespace hozelock_tablespace_temp
tempfile 'hozelock_tablespace_temp.dat'
size 5M autoextend on;
 
create user hozelock 
identified by hozelock
default tablespace hozelock_tablespace
temporary tablespace hozelock_tablespace_temp;
 
grant create session to hozelock;
grant create table to hozelock;
grant unlimited tablespace to hozelock;
GRANT GRANT ANY OBJECT PRIVILEGE TO hozelock;
GRANT GRANT ANY PRIVILEGE TO hozelock;
GRANT GRANT ANY ROLE TO hozelock;
exit;
EOF

echo "hozelock Created"