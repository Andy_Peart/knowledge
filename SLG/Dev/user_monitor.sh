#!/bin/sh

####################################################################################################################################
# Script name: user_monitor.sh
# Creator: Andy Peart
# Date: 27/02/2019
# Purpose: Script to Populate max users to a table in the ecom Schema
# ***************************************CHANGES************************************************************************************
# 1.0 - 02/10/2020 - Andy Peart - Initial version
####################################################################################################################################

sqlplus -s $ORACLE_USR << !

declare

cursor c_users is
select count(user_id) as users,site_id,sysdate as dstamp from dcsdba.application_session, dual where user_id is not null and site_id is not null group by site_id, sysdate;

r_users c_users%rowtype;

record_count integer;
stored_user integer;
current_date DATE;
max_store_date DATE;
min_store_date DATE;

begin

/* setup all date data to be used for cleardown and populate data */

select trunc(sysdate) into current_date from dual;
select trunc(max(dstamp)) into max_store_date from ecom.max_users;
select trunc(min(dstamp)) into min_store_date from ecom.max_users;

/* first run a cleardown of all records > 400 days old */

IF min_store_date < current_date -400 then
  delete from ecom.max_users where dstamp < (sysdate -400);
  commit;
END IF;

/* Insert records if its a new day for new day */

IF current_date > max_store_date then

DBMS_OuTPUT.PUT_LINE ('CUR DATE > MAX DATE');
  open c_users;
  loop
    fetch c_users into r_users;
    IF c_users%notfound THEN
        EXIT;
    END IF;
    insert into ecom.max_users VALUES (r_users.users,r_users.site_id,r_users.dstamp);
    commit;
  END LOOP;
END IF;

/* Update the records in the max user table for the active day */

IF current_date = max_store_date then
  open c_users;
  loop
    fetch c_users into r_users;
    IF c_users%notfound THEN
      EXIT;
    END IF;

/* see if a record for the site already exists */

    select count(mu.site_id) into record_count from ecom.max_users mu where mu.site_id = r_users.site_id and to_char(mu.dstamp,'DD-MON-YY') = to_char(r_users.dstamp,'DD-MON-YY');

/* If there is a record then See what the current stored user count is  */

    IF record_count != 0 THEN
      select 1 into stored_user from ecom.max_users mu where mu.site_id = r_users.site_id and to_char(mu.dstamp,'DD-MON-YY') = to_char(r_users.dstamp,'DD-MON-YY');

/* if the current user count is greater than the stored user count update the count */

      IF r_users.users > stored_user THEN
        update ecom.max_users SET users = r_users.users where ecom.max_users.site_id = r_users.site_id and to_char(ecom.max_users.dstamp,'DD-MON-YY') = to_char(r_users.dstamp,'DD-MON-YY');
        commit;
      END IF;

/* if there is no record for the site for the day insert the record */

    ELSE
      insert into ecom.max_users VALUES (r_users.users,r_users.site_id,r_users.dstamp);
      commit;
    END IF;
  END LOOP;
END IF;
close c_users;
commit;
end;

/
!
exit
