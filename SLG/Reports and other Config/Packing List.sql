----PACKING_LIST
select
	sum(sm.qty_picked) as "PICKED"
  , sum(sm.qty_picked * nvl(s.each_weight,0)) "NET"
  , trunc(sum(sm.qty_picked/s.user_def_num_1),0)"CASES"
  , 'PALLET '||sm.Pallet_id "PALL"
  , sm.sku_id "SKU"
  , s.description "Description"
  , sm.batch_id "LOT"
  , oh.purchase_order "PO"
  ,'SLG'||trunc(oh.order_id,7)"SHIPMENT ID"
  , oh.name "Cust"
  , oh.address1 "ADD_1"
  , oh.address2 "ADD_2"
  , oh.town "ADD_3"
  , oh.county "ADD_4"
  , oh.postcode "ADD_5"
  , oh.country "ADD_6"
  ,to_char(Deliver_by_date,'DD/MM/RRRR')
from
	shipping_manifest sm
  , sku               s
  , order_header      oh
where
	sm.sku_id        = s.sku_id
	and sm.order_id  =  $P{order} 
	and oh.order_id  = sm.order_id
	and sm.client_id = s.client_id
	and sm.client_id = 'SLG'
group by
	'PALLET '||sm.Pallet_id
  , sm.sku_id
  , s.description
  , sm.batch_id
  , s.each_weight
  , s.user_def_num_1
  , oh.purchase_order
  ,'SLG'
		||trunc(oh.order_id,7)
  , oh.name 
  , oh.address1 
  , oh.address2 
  , oh.town
  , oh.county
  , oh.postcode
  , oh.country
  ,to_char(Deliver_by_date,'DD/MM/RRRR')
order by
	'PALLET '||sm.Pallet_id