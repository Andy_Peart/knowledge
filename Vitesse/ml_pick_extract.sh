#!/bin/bash

####################################################################################################################################
# Script name: ml_pick_extract.sh
# Creator: Gurmit Karyal - ModernLogic
# Date: 16/05/2019
# Purpose: Script creates csv files from pick itls
# ***************************************CHANGES************************************************************************************
# 1.0 - 16/05/2019 - Gurmit Karyal - Initial version
####################################################################################################################################

# set preextract value
PRE_UPLOADED="G"
ITLVISIBLE_UPLOADED="1"
UPLOADED="X"

# find the records that need to be extracted and set them with the pre-extract value
sqlplus -s $ORACLE_USR << !
update inventory_transaction set uploaded_customs = '$PRE_UPLOADED' 
where client_id = 'PLT' 
and key in
(
/* fist picks that have had an exception not in a container*/
select distinct key
from inventory_transaction
where not exists
(select 1 from
move_task
where list_id = inventory_transaction.list_id
and client_id = inventory_transaction.client_id)
and code = 'Pick'
and dstamp >= current_timestamp -1
and reason_id like 'EX%'
and container_id is null
and client_id = 'PLT'
and update_qty < original_qty
--and list_id in ('S3-P8450439')
and list_id is not null
and uploaded_customs = 'Y'
union
/* picks on a list */
select distinct key
from inventory_transaction
where not exists
(select 1 from
move_task
where list_id = inventory_transaction.list_id
and client_id = inventory_transaction.client_id)
and code = 'Pick'
and to_loc_id = 'CONTAINER'
and dstamp >= current_timestamp -1
and container_id is not null
--and list_id in ('S3-P8450439')
and list_id is not null
and client_id = 'PLT'
and uploaded_customs = 'Y'
)
;
commit;
exit;
!

# compose the csv file for the extract
EXTENSION="$(date +%y%m%d%H%M%S)"
CSVFILE="VITESSE_PICK_${EXTENSION}.csv"
OUTWORK="$DCS_COMMSDIR/outwork"

sqlplus -s $ORACLE_USR << ! >> $OUTWORK/$CSVFILE
SET ECHO OFF HEADING OFF LINESIZE 32767 TRIMSPOOL ON PAGESIZE 0 FEEDBACK OFF TRIMOUT ON
SELECT code ,
  ',' ,
  list_id ,
  ',' ,
  site_id ,
  ',' ,
  from_loc_id ,
  ',' ,
  sku_id ,
  ',' ,
  user_id ,
  ',' ,
  dstamp ,
  ',' ,
  update_qty ,
  ',' ,
  original_qty ,
  ',' ,
  user_def_type_2 ,
  ',' ,
  user_def_type_3 ,
  ',' ,
  user_def_type_4 ,
  ',' ,
  user_def_type_5 ,
  ',' ,
  reference_id
FROM inventory_transaction
WHERE uploaded_customs = '$PRE_UPLOADED' 
AND client_id = 'PLT';
exit;
!

# remove empty files where needed
if [[ ! -s "$OUTWORK/$CSVFILE" ]]
then
  echo "$OUTWORK/$CSVFILE is empty. removed" 1>&2
  rm $OUTWORK/$CSVFILE
  exit 1
fi  

# set the data to be uploaded
sqlplus -s $ORACLE_USR << !
update inventory_transaction
set old_user_def_num_4 = '$ITLVISIBLE_UPLOADED', 
uploaded_customs = '$UPLOADED'
where uploaded_customs = '$PRE_UPLOADED'
and client_id = 'PLT'
;
commit;
exit;
!

echo $OUTWORK/$CSVFILE

# move to outtray
OUTTRAY="$DCS_COMMSDIR/outtray"
echo "moving $OUTWORK/$CSVFILE $OUTTRAY"
mv $OUTWORK/$CSVFILE $OUTTRAY 


exit 0
