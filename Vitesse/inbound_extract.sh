#!/bin/bash

. $HOME/dcs/.dcs_profile

cd $DCS_COMMSDIR/outwork/.

# compose the csv file for the extract
EXTENSION="$(date +%y%m%d%H%M%S)"
CSVFILE="VITESSE_INBOUND_${EXTENSION}.csv"
OUTWORK="$DCS_COMMSDIR/outwork"

# set preextract value
PRE_UPLOADED="A"
UPLOADED="X"
ITLVISIBLE_UPLOADED="1" # this is so that the itl can see the data in the front end. there is no functionality otherwise

# find the records that need to be extracted and set them with the pre-extract value Receipt
sqlplus -s $ORACLE_USR << !
update inventory_transaction
set UPLOADED_CUSTOMS = 'A'
where uploaded_customs = 'N'
and client_id = 'PLT'
and dstamp < to_date(to_char(current_timestamp, 'yyyymmddhh24') || '00', 'yyyymmddhh24mi')
and code in ('Receipt');
commit;
exit;
!


# find the records that need to be extracted and set them with the pre-extract value Putaway
sqlplus -s $ORACLE_USR << !
update inventory_transaction
set UPLOADED_CUSTOMS = 'A'
where uploaded_customs = 'Y'
and client_id = 'PLT'
and dstamp < to_date(to_char(current_timestamp, 'yyyymmddhh24') || '00', 'yyyymmddhh24mi')
and code in ('Putaway');
commit;
exit;
!



# setup the filename 
date=`date +"%m%d"`
time=`date|awk '{print $4}'|sed 's/\://g'`
file=VITESSE_INBOUND_$date$time.csv

#Check if there is any data to export
sqlplus -s $ORACLE_USR > KEYS << EOF
SET SERVEROUTPUT ON
SET FEEDBACK OFF

declare
	key_count number;
begin
	select
		count(key)  
	into
		key_count
	from
	 inventory_transaction where UPLOADED_CUSTOMS = 'A'
	;
	
	DBMS_OUTPUT.PUT_LINE(key_count);
end;
/
exit
EOF

keycount=`cat KEYS`

#If there is any data to extract then run an extract

if ((keycount > 0)); then
sqlplus -s $ORACLE_USR << ! >> $CSVFILE
        set feedback off
        set heading off
        set pagesize 999
        set linesize 32767
        select
        	i.CODE
          ,','
          , i.TAG_ID
          ,','
          , i.site_id
          ,','
          , i.from_loc_id
          ,','
          , i.to_loc_id
          ,','
          , i.final_loc_id
          ,','
          , i.sku_id
          ,','
          , i.user_id
          ,','
          , i.dstamp
          ,','
		  , i.reference_id
		  ,','
		  , i.pallet_id
		  ,','
          , i.update_qty
          ,','
          , i.original_qty
          ,','
          , s.user_def_type_1
          ,','
          , s.user_def_type_2
          ,','
          , s.user_def_type_3
          ,','
          , s.user_def_type_4
        from
        	inventory_transaction i
          , sku                   s
        where
        	UPLOADED_CUSTOMS = 'A'
			and i.sku_id = s.sku_id
        ;
		
		
		update inventory_transaction
set old_user_def_num_4 = '1',
UPLOADED_CUSTOMS = '1'
where UPLOADED_CUSTOMS = 'A'
and client_id = 'PLT';
commit;
!



if [[ ! -s "$OUTWORK/$CSVFILE" ]]
then
  echo "$OUTWORK/$CSVFILE is empty. removed" 1>&2
  rm $OUTWORK/$CSVFILE
  exit 1
fi  


echo $OUTWORK/$CSVFILE

# Copy file to out archive
OUTARCHIVE = "$DCS_COMMSDIR/outarchive"
echo "Copying $OUTWORK/$CSVFILE $OUTARCHIVE"
cp $OUTWORK/$CSVFILE $OUTARCHIVE/$CSVFILE

# move to outtray
OUTTRAY="$DCS_COMMSDIR/outtray"
echo "moving $OUTWORK/$CSVFILE $OUTTRAY"
mv $OUTWORK/$CSVFILE $OUTTRAY 

fi

exit 0


