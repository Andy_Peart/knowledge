create or replace TRIGGER XCEVA_COR01_TAG_PRINT
AFTER insert
on dcsdba.INVENTORY
FOR EACH ROW
when ( NEW.LOCATION_ID = 'CONREC' and NEW.SITE_ID = 'COR01'
    and NEW.receipt_type = 'B')
BEGIN
  XCEVA_COR01_MERGE.RUN_TASK_INSERT(:NEW.client_id,:new.site_id,'AUTOMATIC','INTERFACE','10.209.2.201',:new.tag_id,
                                    :NEW.location_id,:new.sku_id,:new.config_id,:new.qty_on_hand,:new.receipt_id, to_char(systimestamp,'DD/MM/YYYY'),to_char(systimestamp,'HH24:MI:SS'),:new.condition_id,:new.batch_id,:new.qc_status,to_char(:new.expiry_dstamp,'DD/MM/YYYY'),to_char(:new.expiry_dstamp,'HH24:MI:SS'));
END;