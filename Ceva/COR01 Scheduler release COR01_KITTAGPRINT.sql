BEGIN

BEGIN
    DBMS_SCHEDULER.create_program(
        program_name => 'COR01_KITTAGPRINT_P',
        program_action => 'begin update run_task set status = ''Pending'',pid = ''1'' ,print_label = ''Y'' where pid is null and command like ''"jasper_DCS_tag_label" "%CONREC%'' and dstamp > sysdate -0.5 ; commit; end;',
        program_type => 'PLSQL_BLOCK',
        number_of_arguments => 0,
        comments => 'Tag print set pid for COR01',
        enabled => FALSE);
		
    DBMS_SCHEDULER.ENABLE(name=>'COR01_KITTAGPRINT_P');

    insert into scheduler_program values ('COR01_KITTAGPRINT_P','PLSQL_BLOCK','begin update run_task set status = ''Pending'',pid = ''1'' ,print_label = ''Y'' where pid is null and command like ''"jasper_DCS_tag_label" "%CONREC%'' and dstamp > sysdate -0.5 ; commit; end;',null,'Y','Tag print set pid for COR01','RELEASE',systimestamp,'RELEASE',systimestamp);

COMMIT;
	
	DBMS_SCHEDULER.CREATE_SCHEDULE (
	 schedule_name   => 'COR01_KITTAGPRINT_S',
	 repeat_interval  => 'freq=secondly; interval=30',
	 comments     => 'COR01 Tag Runtask run every 30 seconds');

	insert into scheduler_schedule values ('COR01_KITTAGPRINT_S',null,systimestamp,null,'freq=secondly; interval=30','COR01 Tag Runtask run every 30 seconds','RELEASE',systimestamp,'RELEASE',systimestamp);

COMMIT;

	DBMS_SCHEDULER.CREATE_JOB (
	  job_name     => 'COR01_KITTAGPRINT_J',
	  program_name   => 'COR01_KITTAGPRINT_P',
	  schedule_name   => 'COR01_KITTAGPRINT_S');
	  
	  DBMS_SCHEDULER.ENABLE(name=>'COR01_KITTAGPRINT_J');
	
	insert into scheduler_job values ('COR01_KITTAGPRINT_J',null,'COR01_KITTAGPRINT_P','Y','N',null,'COR01_KITTAGPRINT_S',null,null,null,null,null,'Tag print set pid for COR01','RELEASE',sysdate,'RELEASE',sysdate);

COMMIT;

END;
/
