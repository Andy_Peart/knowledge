/* added this comment to test issue linking */
SET SERVEROUTPUT ON;
Declare
p_last_station_id dcsdba.workstation.station_id%TYPE :=&startport; -- This will be the last workstation_id currently in the workstation table +1.
p_group_id dcsdba.workstation.group_id%TYPE := &wokstationgroup;
p_site_id dcsdba.workstation.site_id%TYPE := &site;
p_workstation_notes dcsdba.workstation.notes%TYPE := 'RDT Reserved for Client:'||&client;
p_created_by dcsdba.sia_process.created_by%TYPE := &your_name; -- Please change to your name for auditing purposes. 

arg_rdt_qty number := &numstations; -- This is the number of new RDT's required
p_rdt_counter number := 0; -- Keep this at 0!


Begin

WHILE p_rdt_counter < arg_rdt_qty
LOOP

dbms_output.put_line('Creating workstations... ' || p_last_station_id);

insert into dcsdba.workstation 
(station_id, group_id, disabled, site_id, rdt, max_pallets, max_containers, notes, truck_id, ptysvr_port_no, default_language, screen_columns, screen_rows, fixed_position)
VALUES
(p_last_station_id, p_group_id, 'N', p_site_id, 'Y', 1, 5, p_workstation_notes , p_last_station_id, p_last_station_id,
'EN_GB', 20, 16, 'N');

insert into dcsdba.sia_process
(PROCESS_NAME, GROUP_NAME, ACTIVE, FLAGS, EXECUTABLE_NAME, ARGUMENTS, NICE, TSET, PRIORITY, SITE_ID, CREATION_DATE, CREATED_BY, NOTES)
VALUES
('pty' || p_last_station_id, 'pty', 'Y', 'revive', 'ptysvr', '-n -t -p ' || p_last_station_id || ' crtrdt -j ' || p_last_station_id || ' -n -a -r 16 -c 20', 0, 'N', 999, p_site_id, sysdate, p_created_by, 
p_workstation_notes);

insert into dcsdba.sia_process
(PROCESS_NAME, GROUP_NAME, ACTIVE, FLAGS, EXECUTABLE_NAME, ARGUMENTS, NICE, TSET, PRIORITY, SITE_ID, CREATION_DATE, CREATED_BY, NOTES)
VALUES
('rdt' || p_last_station_id, 'rdt', 'Y', 'revive', 'ritch', '-j ' || p_last_station_id || ' -s -w ' || p_last_station_id, 0, 'N', 999, p_site_id, sysdate, p_created_by, 
p_workstation_notes);

p_rdt_counter := p_rdt_counter + 1;
p_last_station_id := p_last_station_id +1;

END LOOP;

END;
