declare

			cursor xx
			is
			select distinct site_id, client_id, program_name from rdt_screen_mp;

			Result    integer;
			Message    varchar2(200);
			l_ProgramName    RDT_Screen_MP.Program_Name%type;
			l_ClientID       RDT_Screen_MP.Client_ID%type;
			l_SiteID         RDT_Screen_MP.Site_ID%type;
			p_Values varchar2(200);
			xxrow xx%rowtype;

		begin

				OPEN xx;

				while true
				loop
					   FETCH xx INTO xxrow;

					   EXIT WHEN xx%NOTFOUND;

					p_Values := LibMQSData.EncodeString('PROGRAM_NAME', xxrow.program_name);
					p_Values := p_Values || LibMQSData.EncodeString('CLIENT_ID',xxrow.client_id);
					p_Values := p_Values || LibMQSData.EncodeString ('SITE_ID',xxrow.site_id);

					LibMQSRDT_Screen_CMP.CompileProgram(Result,Message,p_Values);

					dbms_output.put_line('Compiled program_name = [' || xxrow.program_name || ']');

					commit;

				 end loop;
				CLOSE xx;

		end;
/