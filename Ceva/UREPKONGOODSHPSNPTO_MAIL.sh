#!/bin/sh

sleep 10

s_order_id $2

sqlplus -s  $ORACLE_USR<<eof
DECLARE 
result VARCHAR2(20) := '1'; 
lReportName report.name%TYPE := 'UREPKONGOODSHPSNPTO';
v_receipts varchar2(100);
v_order UPPER('$s_order_id');
begin 

select text_data into v_receipts from system_profile where parent_profile_id = '-ROOT-_USER_REPORTS_DISTRIBUTION_'||lReportName;

LibPrint.AdvancedPrintDoc(p_Result=>result,p_ReportName => lReportName, P_PRINTCOMMAND => '', p_Language=>'EN_GB',p_TimeZoneName=>'Europe/London',
p_ExportType=>'',
p_Parameters=>'"site_id" "UK-LGP-01" "client_id" "KON902" "order_id" "'||v_order||'"',
P_USERID => 'WEBSERVICEU', P_STATIONID =>'SCHEDULER', P_SITEID=>'UK-LGP-01',P_CLIENTID=>'KON902'/*,P_EMAILRECIPIENTS=>v_receipts*/); 
commit;
end;
/
exit
eof



22 feb