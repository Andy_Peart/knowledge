SET SERVEROUTPUT ON;
	declare
		cursor xx
		is
		  select distinct 'order_mp' program_name, client_id, site_id from order_mp
		  union
		  select distinct 'pre_advice_mp' program_name, client_id, site_id from pre_advice_mp
		  union
		  select distinct 'sku_mp' program_name, client_id, site_id from sku_mp
		  union
		  select distinct 'upi_receipt_mp' program_name, client_id, site_id from upi_receipt_mp
		  union
		  select distinct 'sku_config_mp' program_name, client_id, site_id from sku_config_mp
		  union
		  select distinct 'delivery_mp' program_name, client_id, site_id from delivery_mp
		  union
		  select distinct 'address_mp' program_name, client_id, site_id from address_mp
		  ;

		Result    integer;  
		l_ProgramName    RDT_Screen_MP.Program_Name%type;
		l_ClientID       RDT_Screen_MP.Client_ID%type;
		l_SiteID         RDT_Screen_MP.Site_ID%type; 
		xxrow xx%rowtype;
			
	begin
			OPEN xx;

			while true
			loop
				   FETCH xx INTO xxrow;

				   EXIT WHEN xx%NOTFOUND;
			  
				  if xxrow.program_name = 'order_mp' 
				  then 
					  LibOrderMergeRule.VerifyHeaderProgramProc (Result, xxrow.client_id, xxrow.site_id);
					  LibOrderMergeRule.VerifyLineProgramProc (Result, xxrow.client_id, xxrow.site_id);
					  
				  elsif xxrow.program_name = 'pre_advice_mp' 
				  then      
					  LibPreAdviceMergeRule.VerifyLineProgramProc (Result, xxrow.client_id, xxrow.site_id);
					  LibPreAdviceMergeRule.VerifyHeaderProgramProc(Result, xxrow.client_id, xxrow.site_id);
					  
				  elsif xxrow.program_name = 'sku_mp' 
				  then  
					  LibSkuMergeRule.VerifyHeaderProgramProc (Result, xxrow.client_id, xxrow.site_id);
					  
				  elsif xxrow.program_name = 'upi_receipt_mp' 
				  then                 
					  LibUPIReceiptMergeRule.VerifyHeaderProgramProc(Result, xxrow.client_id, xxrow.site_id);
					  LibUPIReceiptMergeRule.VerifyLineProgramProc (Result, xxrow.client_id, xxrow.site_id);
										
				  elsif xxrow.program_name = 'sku_config_mp' 
				  then
					  LibSKUConfigMergeRule.VerifyHeaderProgramProc(Result, xxrow.client_id, xxrow.site_id);
										
				  elsif xxrow.program_name = 'delivery_mp' 
				  then
					  LibDeliveryMergeRule.VerifyHeaderProgramProc(Result, xxrow.client_id, xxrow.site_id);
										
				  elsif xxrow.program_name = 'address_mp' 
				  then
					  LibAddressMergeRule.VerifyHeaderProgramProc (Result, xxrow.client_id, xxrow.site_id);
				  end if;              
			  
				  dbms_output.put('program [' || xxrow.program_name || '] ' || rpad('      ',15-(length(xxrow.program_name))) || '');
				  dbms_output.put(' site/client [' || xxrow.client_id || ']/[' || xxrow.site_id || ']' || rpad('      ',15-(length(xxrow.client_id || xxrow.site_id))));             
				  dbms_output.put_line(' Result = [' || Result || ']'  );             
				 
				   commit;
				  
			 end loop;
		CLOSE xx;
	end;
/