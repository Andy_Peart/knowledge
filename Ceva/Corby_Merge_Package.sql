create or replace package XCEVA_COR01_MERGE AS

function PALNOTES_AS_PAHNOTES (P_pre_advice_id VARCHAR2,p_ref varchar2) RETURN VARCHAR2;

procedure PALNOTES_AS_PAHNOTES_PROC (p_pre_advice_id VARCHAR2,p_ref varchar2);

PROCEDURE RUN_TASK_INSERT
	(arg_client IN dcsdba.inventory.client_id%type,
	arg_site IN dcsdba.inventory.site_id%TYPE,
    arg_user_id IN dcsdba.application_user.user_id%TYPE,
    arg_station_id IN dcsdba.workstation.station_id%TYPE,
    arg_print varchar2,
	arg_tag IN dcsdba.inventory.tag_id%type,
	arg_loc in dcsdba.inventory.location_id%type,
	arg_sku IN dcsdba.inventory.sku_id%type,
	arc_config IN dcsdba.inventory.config_id%type,
	arg_qty in dcsdba.inventory.qty_on_hand%type,
	arg_reciept IN dcsdba.inventory.receipt_id%type,
	arg_date varchar2,
	arg_time varchar2,
	arg_cond IN dcsdba.inventory.condition_id%type,
	arg_batch IN dcsdba.inventory.batch_id%type,
	arg_status IN dcsdba.inventory.qc_status%type,
	arg_expiry varchar2,
    arg_expire_time varchar2);

END XCEVA_COR01_MERGE;

/


-----------------------------------------------------------------------------------------------------------------------------------

create or replace package body XCEVA_COR01_MERGE as
-----------------------------------------------------------------------------------------------------------------------------------
function PALNOTES_AS_PAHNOTES (P_pre_advice_id VARCHAR2,p_ref varchar2) 
RETURN VARCHAR2
IS
BEGIN
    PALNOTES_AS_PAHNOTES_PROC (P_pre_advice_id,p_ref);
    return 'X';
END;
-----------------------------------------------------------------------------------------------------------------------------------
procedure PALNOTES_AS_PAHNOTES_PROC (p_pre_advice_id VARCHAR2,p_ref varchar2)
IS
PRAGMA  autonomous_transaction;
BEGIN
    update dcsdba.pre_advice_line set notes = p_ref  
    where CLIENT_id in (select CLIENT_ID from DCSDBA.CLIENT_GROUP_CLIENTS where client_group = 'CORMU-DV')
    and pre_advice_id = p_pre_advice_id;
    COMMIT;
END;
-----------------------------------------------------------------------------------------------------------------------------------
PROCEDURE RUN_TASK_INSERT
	(arg_client IN dcsdba.inventory.client_id%type,
	arg_site IN dcsdba.inventory.site_id%TYPE,
    arg_user_id IN dcsdba.application_user.user_id%TYPE,
    arg_station_id IN dcsdba.workstation.station_id%TYPE,
    arg_print varchar2,
	arg_tag IN dcsdba.inventory.tag_id%type,
	arg_loc in dcsdba.inventory.location_id%type,
	arg_sku IN dcsdba.inventory.sku_id%type,
	arc_config IN dcsdba.inventory.config_id%type,
	arg_qty in dcsdba.inventory.qty_on_hand%type,
	arg_reciept IN dcsdba.inventory.receipt_id%type,
	arg_date varchar2,
	arg_time varchar2,
	arg_cond IN dcsdba.inventory.condition_id%type,
	arg_batch IN dcsdba.inventory.batch_id%type,
	arg_status IN dcsdba.inventory.qc_status%type,
	arg_expiry varchar2,
    arg_expire_time varchar2)
IS
pragma autonomous_transaction;
    success_flag BOOLEAN;
    v_desc dcsdba.sku.description%type;
    v_key DCSDBA.RUN_TASK.key%type;
BEGIN
select DCSDBA.RUN_TASK_PK_SEQ.NextVal into v_key from dual;
select description into v_desc from dcsdba.sku where sku_id = arg_sku and client_id = arg_client;

success_flag := true;

IF (arg_site IS NULL OR arg_user_id IS NULL OR arg_station_id IS NULL OR arg_tag IS NULL) THEN
success_flag := false;
ELSE

insert into dcsdba.run_task

(KEY,
SITE_ID,
STATION_ID,
USER_ID,
STATUS,
COMMAND,
PID,
OLD_DSTAMP,
DSTAMP,
LANGUAGE,
NAME,
TIME_ZONE_NAME,
NLS_CALENDAR,
PRINT_LABEL,
JAVA_REPORT,
RUN_LIGHT,
SERVER_INSTANCE,
PRIORITY,
ARCHIVE,
ARCHIVE_IGNORE_SCREEN,
ARCHIVE_RESTRICT_USER,
CLIENT_ID,
EMAIL_RECIPIENTS,
EMAIL_ATTACHMENT,
EMAIL_SUBJECT,
EMAIL_MESSAGE,
MASTER_KEY,
USE_DB_TIME_ZONE)
(select v_key,
arg_site,
arg_station_id,
arg_user_id,
'Pending',
'"jasper_DCS_tag_label" "'||arg_print||'" "P" "1" "tag_id" "'||arg_tag||'" "location_id" "'||arg_loc||'" "client_id" "'||arg_client||'" "sku_id" "'||arg_sku||'" "description" "'||v_desc||'" "config_id" "'||arc_config||'" "quantity" "'||arg_qty||'" "receipt_id" "'||arg_reciept||'" "receipt_dstamp" "'||arg_date||
	'" "receipt_time" "'||arg_time||'" "condition_id" "'||arg_cond||'" "batch_id" "'||arg_batch||'" "qc_status" "'||arg_status||'" "expiry_dstamp" "'||arg_expiry||'" "expiry_time" "'||arg_expire_time||'" "supplier_id" "" "notes" "" "site_id" "'||arg_site||'" "pallet_id" "" "owner_id" "AOWNER" "origin_id" "" "manuf_dstamp" "
	" "manuf_time" "" "user_def_type_1" "" "user_def_type_2" "" "user_def_type_3" "" "user_def_type_4" "" "user_def_type_5" "" "user_def_type_6" "" "user_def_type_7" "" "user_def_type_8" "" "user_def_chk_1" "N" "user_def_chk_2" "N" "user_def_chk_3" "N" "user_def_chk_4" "N" 
	"user_def_dstamp_1" "" "user_def_time_1" "" "user_def_dstamp_2" "" "user_def_time_2" "" "user_def_dstamp_3" "" "user_def_time_3" "" "user_def_dstamp_4" "" "user_def_time_4" "" "user_def_num_1" "" "user_def_num_2" "" "user_def_num_3" "" "user_def_num_4" "" "user_def_note_1" "" "user_def_note_2" ""',
'',
sysdate,
sysdate,
'EN_GB',
'UREPKITTAGLABEL',
'Europe/Amsterdam',
'Gregorian',
'N',
'Y',
'Y',
'',
'',
'',
'',
'',
arg_client,
'',
'',
'',
'',
v_key,
'N' from dual);
commit;

END IF;

EXCEPTION

WHEN OTHERS THEN
   success_flag := false;
   DCSDBA.LIBError.WriteErrorLog ('xceva_run_task_insert', 'Error' , arg_user_id || arg_station_id ||'-' || SQLCODE || '-' || SQLERRM);
END;

-----------------------------------------------------------------------------------------------------------------------------------
END XCEVA_COR01_MERGE;