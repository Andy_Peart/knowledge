set serveroutput on;
/******************************************************************************/
/*                                                                            */
/* NAME:         Anonomyse live orders                     */
/*                                                                            */
/* DESCRIPTION:  Script will anonomyse orders as part of gdpr requests        */
/*                                                                            */
/*                                                                            */
/* Date       By  Proj    Ref      Description                                 /
/* ---------- --- ------- -------- -------------------------                  */
/* 29/10/2018 AMP Clipper          Remove name address 1 phone and email      */
/******************************************************************************/
DECLARE
	v_code NUMBER;
	v_errm VARCHAR2(64);
	cursor ORD_CURS is
		select
			order_id
		  , name
		  , address1
		  , contact
		  , contact_phone
		  , contact_email
		from
			order_header
		where
			contact_email in ('iode327@gmail.com'
							, 'bferrenz@comcast.net'
							, 'damienyam@yahoo.com.sg'
							, 'bethmarshall@outlook.com'
							, 'clwofischer@googlemail.com'
							, 'elke.escobar-design@web.de'
							, 'hai630@aol.com'
							, 'kathrin-wolf@t-online.de'
							, 'w.madda@gmx.de'
							, 'jane.erickson26@gmail.com'
							, 'c.lang0510@gmail.com'
							, 'badevey@hotmail.com'
							, 'bjfromnv@gmail.com'
							, 'Barbara.5337@t-online.de'
							, 'crochetarcade@gmail.com'
							, 'rshekman@gmail.com'
							, 'saroeuncheam2@gmail.com'
							, 'ramonawhyte@gmail.com'
							, 'dtvbr59@gmail.com'
							, 'info@itsasalfordthing.co.uk'
							, 'dbjuhlmannsiek@t-online.de'
							, 'v.knappert@gmx.de'
							, 'dtvbr59@gmail.com'
							, 'yviburz@t-online.de'
							, 'ep@roe.me'
							, 'cowgirlup_07_1989@yahoo.com'
							, 'beyenburgerin.brigitte@yahoo.de'
							, 'lenamerv@gmail.com'
							, 'katiemm2000@yahoo.co.uk'
							, 'do.hartmann@icloud.com'
							, 'roisind6598@gmail.com'
							, 'jemimahreid@gmail.com'
							, 'dianeavnon@gmail.com'
							, 'dianeavnon@gmail.com')
		union all
		select
			order_id
		  , name
		  , address1
		  , contact
		  , contact_phone
		  , contact_email
		from
			order_header_archive
		where
			contact_email in ('iode327@gmail.com'
							, 'bferrenz@comcast.net'
							, 'damienyam@yahoo.com.sg'
							, 'bethmarshall@outlook.com'
							, 'clwofischer@googlemail.com'
							, 'elke.escobar-design@web.de'
							, 'hai630@aol.com'
							, 'kathrin-wolf@t-online.de'
							, 'w.madda@gmx.de'
							, 'jane.erickson26@gmail.com'
							, 'c.lang0510@gmail.com'
							, 'badevey@hotmail.com'
							, 'bjfromnv@gmail.com'
							, 'Barbara.5337@t-online.de'
							, 'crochetarcade@gmail.com'
							, 'rshekman@gmail.com'
							, 'saroeuncheam2@gmail.com'
							, 'ramonawhyte@gmail.com'
							, 'dtvbr59@gmail.com'
							, 'info@itsasalfordthing.co.uk'
							, 'dbjuhlmannsiek@t-online.de'
							, 'v.knappert@gmx.de'
							, 'dtvbr59@gmail.com'
							, 'yviburz@t-online.de'
							, 'ep@roe.me'
							, 'cowgirlup_07_1989@yahoo.com'
							, 'beyenburgerin.brigitte@yahoo.de'
							, 'lenamerv@gmail.com'
							, 'katiemm2000@yahoo.co.uk'
							, 'do.hartmann@icloud.com'
							, 'roisind6598@gmail.com'
							, 'jemimahreid@gmail.com'
							, 'dianeavnon@gmail.com'
							, 'dianeavnon@gmail.com')
		;
	
	ORD_ROW ORD_CURS%rowtype;
Begin
	open ORD_CURS;
	loop
		fetch ORD_CURS
		into
			ORD_ROW
		;
		
		IF ORD_CURS%notfound THEN
			/*EXIT WHEN NO DATA FOUND*/
			dbms_output.put_line('NO More Orders To Update');
			EXIT;
		END IF;
		update
			order_header
		set name          = 'anonymised'
		  , address1      = 'anonymised'
		  , contact_email = 'anonymised@anonymised'
		  , contact_phone = '999999'
		  , contact       = '999999'
		where
			order_id = ord_row.order_id
		;
		
		update
			order_header_archive
		set name          = 'anonymised'
		  , address1      = 'anonymised'
		  , contact_email = 'anonymised@anonymised'
		  , contact_phone = '999999'
		  , contact       = '999999'
		where
			order_id = ord_row.order_id
		;
		
		dbms_output.put_line (ord_row.order_id
		|| ' '
		|| ord_row.name
		|| ' '
		|| ord_row.contact_email
		|| ' has been anonymised');
	end loop;
	COMMIT;
	dbms_output.put_line ('COMMIT complete');
	close ORD_CURS;
	dbms_output.put_line ('
	Cursor closed');
EXCEPTION
	/* WHEN ERROR OCCURS OUTPUT ERROR CODE AND CYCLE THROUGH CURSORS TO CHECK THEY ARE ALL SHUT*/
WHEN OTHERS THEN
	v_code := SQLCODE;
	v_errm := SUBSTR(SQLERRM, 1, 64);
	DBMS_OUTPUT.PUT_LINE (v_code
	|| ' '
	|| v_errm);
	close ORD_CURS;
	dbms_output.put_line ('Cursor closed');
end;