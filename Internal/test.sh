#!/bin/bash

if [ -e filenames.txt ]; then
	echo  "ok"
	rm filenames.txt
else
	echo "no file"
fi
if ls -1 -d test*.csv 1>/dev/null  2>&1; then
	ls -1 -d test*.csv >filenames.txt
else
	echo "error out"
	exit 1
fi

while read i; do
	echo "$(tail -n +2 $i)" >$i
	while IFS="," read f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 f29 f30 f31 f32 f33 f34 f35 f36 f37 f38 f39 f40 f41 f42 f43 f44 f45 f46 f47 f48 f49 f50 f51 f52 f53 f54 f55 f56 f57 f58 f59 f60 f61 f62 f63 f64 f65 f66 f67 f68 f69 f70 f71 f72 f73 f74 f75 f76 f77 f78 f79 f80 f81 f82 f83 f84 f85 f86 f87 f88 f89; do
		echo  "order  is          : $f3"
		echo     "line  is           : $f1"
		echo     "third field   is   : $f2"
		echo     "third field   is   : $f87"
		echo "NEW RECORD"

		sqlplus -s $ORACLE_USR <<!
insert into interface_order_header values (
if_oh_pk_seq.nextval,
'SENZ',
$f3,$f4,'','Released','Released','50''ASSEMBLY',$f4,'','','','ROTH001','','SENZ',$f8,$f9,'','',$f11,$f12,$f13,$f14,$f15,$f16,
$f17,$f18,$f19,$f20,$f21,$f22,$f23,'N',$f25,'',$f26,'N','N',$f27,$f28,$f29,$f30,$f31,$f32,$f33,$f34,$f35,$f36,$f37,$f38,’’,
’’,’’,’’,’’,’’,’’,’’,$f41,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,
’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,
’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’Y’,’’,’’,
’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,’’,'SENZV’,'A’,'Pending’,'’,sysdate);

commit;

!
	done <$i
	echo "NEW FILE"
	mv $i $i.old
done <filenames.txt
