--------------------------------------------------------
--  DDL for Package TQ_FUNCTIONS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "TORQUE"."TQ_FUNCTIONS" AS 

/***********************************************************************************************************************/ 

/*                                                                                                                     */ 

/*  NAME:               TQ_FUNCTIONS                                                                                   */ 

/*                                                                                                                     */ 

/*  DESCRIPTION:        To Store All Torque Functions Not Related Directly To WMS Process                              */ 

/*                                                                                                                     */ 

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */ 

/*  =======     ===========     ==     ===================================================================             */ 

/*    1         2019-09-05      KS     Initial Version                                                                 */ 

/*    2         2019-09-27      KS     Added TRIGGER_SHELL_SCRIPT Function To Package                                  */ 

/*    3         2019-11-22      KS     Added REPACK_CONTAINER Function To Package                                      */ 

/*    4         2020-01-29      KS     Added REMOVE_LIST_DUPLICATES Function To Package                                */

/*    4         2020-01-30      KS     Amended REMOVE_LIST_DUPLICATES Function                                         */

/*    5         2020-08-05      CH     Added  for repacking for MW                                                     */

/*    6         2020-12-16      CH     Added tq_is_order_complete                                                      */

/*                                                                                                                     */ 

/***********************************************************************************************************************/  

/**********************GRANTS**************************

grant execute on TQ_FUNCTIONS to DCSDBA; 
grant execute on TQ_FUNCTIONS to DCSRPT; 

******************************************************/

/******************REQUIRED GRANTS*********************

grant select on run_task_pk_seq to TORQUE;
grant select on move_task to TORQUE;

grant insert on run_task to TORQUE;

grant update on move_task to TORQUE;
grant update on shipping_manifest to TORQUE;
grant update on inventory to TORQUE;

grant execute on liborderrepack to TORQUE;
grant execute on liberror to TORQUE;

******************************************************/
    FUNCTION tq_sequence (
        p_client_id    VARCHAR2,
        p_type         VARCHAR2,
        p_update       NUMBER DEFAULT 1,
        p_fullstring   NUMBER DEFAULT 1
    ) RETURN VARCHAR2;

-- 

    FUNCTION trigger_shell_script (
        p_script_path   VARCHAR2,
        p_parameters    VARCHAR2,
        p_name          VARCHAR2,
        p_user_id       VARCHAR2,
        p_station_id    VARCHAR2
    ) RETURN NUMBER;

--

    FUNCTION repack_container (
        p_site_id        VARCHAR2,
        p_client_id      VARCHAR2,
        p_container_id   VARCHAR2,
        p_order_id       VARCHAR2,
        p_to_pallet_id   VARCHAR2,
        p_autoship       NUMBER
    ) RETURN NUMBER;

--

    FUNCTION remove_list_duplicates (
        p_string     VARCHAR2,
        p_splitter   VARCHAR2
    ) RETURN VARCHAR2;
    
--

     function tq_bulk_relocate(
        p_SiteId varchar2, 
        p_ClientId varchar2, 
        p_FromLoc varchar2, 
        p_ToLoc varchar2, 
        p_UserId varchar2, 
        p_Workstation varchar2, 
        p_EmptyOnly char default 'N',
        p_Sku varchar2 default '*'
) return varchar2;

--

function TQ_PALLET_REPACK
(
    p_Tracking varchar2,
    p_To varchar2,
    p_Client varchar2,
    p_User varchar2,
    p_Station varchar2
) return varchar2;

---------

procedure TQ_ADD_ITL_PROC
(
is_success out number,
in_code varchar2,
in_site_id varchar2 default null,
in_from_site_id varchar2 default null,
in_to_site_id varchar2 default null,
in_from_loc_id varchar2 default null,
in_to_loc_id varchar2 default null,
in_final_loc_id varchar2 default null,
in_ship_dock varchar2 default null,
in_dock_door_id varchar2 default null,
in_owner_id varchar2 default 'TORQUE',
in_client_id varchar2 default null,
in_sku_id varchar2 default null,
in_config_id varchar2 default null,
in_tag_id varchar2 default null,
in_container_id varchar2 default null,
in_pallet_id varchar2 default null,
in_batch_id varchar2 default null,
in_qc_status varchar2 default null,
in_expiry_dstamp timestamp with local time zone default null,
in_manuf_dstamp  timestamp with local time zone default null,
in_origin_id varchar2 default null,
in_condition_id varchar2 default null,
in_spec_code varchar2  default null,
in_lock_status varchar2 default null,
in_list_id varchar2 default null,
in_dstamp timestamp with local time zone default sysdate,
in_work_group varchar2 default null,
in_consignment varchar2 default null,
in_supplier_id varchar2 default null,
in_reference_id varchar2 default null,
in_line_id number default null,
in_reason_id varchar2 default null,
in_station_id varchar2,
in_user_id varchar2,
in_group_id varchar2 default null,
in_shift varchar2 default null,
in_update_qty number default 0,
in_original_qty number default null,
in_uploaded varchar2 default 'N',
in_uploaded_ws2pc_id varchar2 default null,
in_uploaded_filename varchar2 default null,
in_uploaded_dstamp timestamp with local time zone default null,
in_uploaded_ab varchar2 default null,
in_uploaded_tm varchar2 default null,
in_uploaded_vview varchar2 default null,
in_session_type varchar2 default null,
in_summary_record varchar2 default null,
in_elapsed_time number default null,
in_estimated_time number default null,
in_sap_idoc_type varchar2 default null,
in_sap_tid varchar2 default null,
in_task_category number default null,
in_sampling_type varchar2 default null,
in_job_id varchar2 default null,
in_manning number default null,
in_job_unit varchar2 default null,
in_complete_dstamp timestamp with local time zone default null,
in_grn number default null,
in_notes varchar2 default null,
in_user_def_type_1 varchar2 default null,
in_user_def_type_2 varchar2 default null,
in_user_def_type_3 varchar2 default null,
in_user_def_type_4 varchar2 default null,
in_user_def_type_5 varchar2 default null,
in_user_def_type_6 varchar2 default null,
in_user_def_type_7 varchar2 default null,
in_user_def_type_8 varchar2 default null,
in_user_def_chk_1 varchar2 default null,
in_user_def_chk_2 varchar2 default null,
in_user_def_chk_3 varchar2 default null,
in_user_def_chk_4 varchar2 default null,
in_user_def_date_1 timestamp with local time zone default null,
in_user_def_date_2 timestamp with local time zone default null,
in_user_def_date_3 timestamp with local time zone default null,
in_user_def_date_4 timestamp with local time zone default null,
in_user_def_num_1 number default null,
in_user_def_num_2 number default null,
in_user_def_num_3 number default null,
in_user_def_num_4 number default null,
in_user_def_note_1 varchar2 default null,
in_user_def_note_2 varchar2 default null,
in_old_user_def_type_1 varchar2 default null,
in_old_user_def_type_2 varchar2 default null,
in_old_user_def_type_3 varchar2 default null,
in_old_user_def_type_4 varchar2 default null,
in_old_user_def_type_5 varchar2 default null,
in_old_user_def_type_6 varchar2 default null,
in_old_user_def_type_7 varchar2 default null,
in_old_user_def_type_8 varchar2 default null,
in_old_user_def_chk_1 varchar2 default null,
in_old_user_def_chk_2 varchar2 default null,
in_old_user_def_chk_3 varchar2 default null,
in_old_user_def_chk_4 varchar2 default null,
in_old_user_def_date_1 timestamp with local time zone default null,
in_old_user_def_date_2 timestamp with local time zone default null,
in_old_user_def_date_3 timestamp with local time zone default null,
in_old_user_def_date_4 timestamp with local time zone default null,
in_old_user_def_num_1 number default null,
in_old_user_def_num_2 number default null,
in_old_user_def_num_3 number default null,
in_old_user_def_num_4 number default null,
in_old_user_def_note_1 varchar2 default null,
in_old_user_def_note_2 varchar2 default null,
in_ce_orig_rotation_id varchar2 default null,
in_ce_rotation_id varchar2 default null,
in_ce_consignment_id varchar2 default null,
in_ce_receipt_type varchar2 default null,
in_ce_originator varchar2 default null,
in_ce_originator_reference varchar2 default null,
in_ce_coo varchar2 default null,
in_ce_cwc varchar2 default null,
in_ce_ucr varchar2 default null,
in_ce_under_bond varchar2 default null,
in_ce_document_dstamp timestamp with local time zone default null,
in_ce_colli_count number default null,
in_ce_colli_count_expected number default null,
in_ce_seals_ok varchar2 default null,
in_uploaded_customs varchar2 default null,
in_uploaded_labor varchar2 default null,
in_print_label_id number default null,
in_lock_code varchar2 default null,
in_asn_id varchar2 default null,
in_customer_id varchar2 default null,
in_ce_duty_stamp varchar2 default null,
in_pallet_grouped varchar2 default null,
in_consol_link number default null,
in_job_site_id varchar2 default null,
in_job_client_id varchar2 default null,
in_tracking_level varchar2 default null,
in_extra_notes varchar2 default null,
in_stage_route_id varchar2 default null,
in_stage_route_sequence number default null,
in_pf_consol_link number default null,
in_master_pah_id varchar2 default null,
in_master_pal_id number default null,
in_archived varchar2 default null,
in_shipment_number number default null,
in_customer_shipment_number number default null,
in_pallet_config varchar2 default null,
in_container_type varchar2 default null,
in_ce_avail_status varchar2 default null,
in_from_status varchar2 default null,
in_to_status varchar2 default null,
in_kit_plan_id varchar2 default null,
in_plan_sequence number default null,
in_master_order_id varchar2 default null,
in_master_order_line_id number default null,
in_ce_invoice_number varchar2 default null,
in_rdt_user_mode varchar2 default null,
in_labor_assignment number default null,
in_grid_pick varchar2 default null,
in_labor_grid_sequence number default null,
in_kit_ce_consignment_id varchar2 default null
);

------

function TQ_PALLET_REPACK_V2
(
    p_From varchar2,
    p_To varchar2,
    p_Client varchar2,
    p_TrailerLoad char default 'N',
    p_User varchar2,
    p_Station varchar2
) return varchar2;

---

function tq_is_order_complete
(
    p_Client varchar2,
    p_Order varchar2
) return number;

---

END tq_functions;

/

  GRANT EXECUTE ON "TORQUE"."TQ_FUNCTIONS" TO "DCSRPT";
  GRANT EXECUTE ON "TORQUE"."TQ_FUNCTIONS" TO "DCSDBA";
