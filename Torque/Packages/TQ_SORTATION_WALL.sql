--------------------------------------------------------
--  DDL for Package TQ_SORTATION_WALL
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "TORQUE"."TQ_SORTATION_WALL" AS 


/***********************************************************************************************************************/ 

/*                                                                                                                     */ 

/*  NAME:               TQ_SORTATION_WALL                                                                              */ 

/*                                                                                                                     */ 

/*  DESCRIPTION:        To Handle the Sortation Wall Custom Functionality                                              */ 

/*                                                                                                                     */ 

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */ 

/*  =======     ===========     ==     ===================================================================             */ 

/*    1         2019-07-12      KS     Initial Version                                                                 */ 

/*    2         2019-07-22		KS	   Modified To Support 2 Stage SORT	                                               */

/*    3         2019-07-22		KS     Added PROCESS_STOCK_DIRECT Function	                                           */

/*    4         2019-09-27      KS     Reworked SORTATION_WALL package and renamed to TQ_SORTATION_WALL                */

/*    5         2019-10-01      KS     Reworked Search_Location Function                                               */

/*    6         2019-10-08      KS     Modified PROCESS_STOCK Function                                                 */

/*    7         2019-11-11      KS     Modified PROCESS_STOCK_OUT to ensure TO_LOCATION IS CORRECT                     */

/*    8         2020-02-05      KS     Modified PROCESS_STOCK AND PROCESS_STOCK_OUT to no longer require client/site   */

/*                                                                                                                     */ 

/***********************************************************************************************************************/ 

/**********************GRANTS**************************

grant execute on TQ_SORTATION_WALL to DCSDBA; 

******************************************************/

/******************REQUIRED GRANTS*********************

grant select on move_task to TORQUE;
grant select on location to TORQUE;

grant update on move_task to TORQUE;
grant update on order_header to TORQUE;

grant insert on move_task to TORQUE;

grant execute on liberror to TORQUE;


/********************DEPENDENCIES***********************

Packages:
    RDTBUFFER

******************************************************/

-- 
    FUNCTION search_location (
        p_site_id       IN   VARCHAR2,
        p_client_id     IN   VARCHAR2,
        p_user_id       IN   VARCHAR2,
        p_station_id    IN   VARCHAR2,
        p_from_loc_id   IN   VARCHAR2,
        p_sku_id        IN   VARCHAR2,
        p_sort_mode     IN   NUMBER DEFAULT 1
    ) RETURN VARCHAR2; 

-- 

    FUNCTION process_stock (
        p_site_id      IN   VARCHAR2,
        p_client_id    IN   VARCHAR2,
        p_user_id      IN   VARCHAR2,
        p_station_id   IN   VARCHAR2
    ) RETURN NUMBER; 

-- 

    FUNCTION sort_validation (
        p_site_id       VARCHAR2,
        p_client_id     VARCHAR2,
        p_station_id    VARCHAR2,
        p_user_id       VARCHAR2,
        p_location_id   VARCHAR2
    ) RETURN NUMBER;

-- 

    FUNCTION process_stock_out (
        p_site_id      VARCHAR2,
        p_client_id    VARCHAR2,
        p_station_id   VARCHAR2,
        p_user_id      VARCHAR2
    ) RETURN NUMBER;

-- 

    FUNCTION process_stock_direct (
        p_site_id     VARCHAR2,
        p_client_id   VARCHAR2,
        p_order_id    VARCHAR2
    ) RETURN NUMBER;

END tq_sortation_wall;

/

  GRANT EXECUTE ON "TORQUE"."TQ_SORTATION_WALL" TO "DCSDBA";
