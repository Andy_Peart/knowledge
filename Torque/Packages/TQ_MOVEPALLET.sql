--------------------------------------------------------
--  DDL for Function TQ_MOVEPALLET
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "TORQUE"."TQ_MOVEPALLET" (
    i_container in varchar2,
    i_new_pallet in varchar2,
    i_client in varchar2,
    i_user in varchar2
)
return varchar2
as
return_value varchar2(100); --return message
orig_pallet varchar2(30);   --pallet that will be switched
orig_container varchar2(30);--container to reference
order_carrier varchar2(30); --orders' carrier
new_consol_link int(30);    --new consol link
new_pallet_count int(30);   --check if new pallet already exists
orig_marshall_hdr int(30);  --check if original marshall header is redundant or not
container_count int(10);    --see how many containers on new pallet
multi_cont int(10);
ship_dock_count int(10);

v_from_loc_id varchar2(30);
v_to_loc_id varchar2(30);
v_final_loc_id varchar2(30);

vp_from_loc_id varchar2(200);
vp_to_loc_id varchar2(200);
vp_final_loc_id varchar2(200);

start_time timestamp;
finish_time timestamp;
execution_time number(10);

/* Below 4 to help with exceptions */
inventory_count int(30);    --used with pallet count to make sure inventory matches move task
pallet_count int(30);       --used with inventory count to make sure inventory matches move task
outstanding_order int(10);  --used to see if order still has outstanding pick tasks
valid_container int(10);    --make sure entered container is a valid order/container

/* Don't comment out! */
pragma autonomous_transaction;

/* Will loop through this sql */    
cursor get_pallet
is
    select pallet_id, container_id, from_loc_id, to_loc_id, final_loc_id
    from dcsdba.move_task
    where (container_id = upper(i_container) or task_id = upper(i_container))
    and client_id = upper(i_client)
    group by pallet_id, container_id, from_loc_id, to_loc_id, final_loc_id;

/* Exceptions */
no_inventory_match exception;
no_cont_entry exception;
no_pallet_entry exception;
no_client_entry exception;
order_not_ready exception;
incorrect_carrier exception;
incorrect_container exception;
multi_container exception;
same_pallet exception;

begin
    start_time := current_timestamp;
    /* If there are multiple containers/pallets associated with the task, raise exception */
    select count(distinct container_id)
    into multi_cont
    from dcsdba.move_task
    where task_id = upper(i_container)
    and client_id = upper(i_client)
    and task_type = 'O'
    and status = 'Consol';
    
    if multi_cont > 1 then raise multi_container;
    end if;    

    select count(*)
    into valid_container
    from dcsdba.move_task
    where client_id = upper(i_client)
    and (container_id = upper(i_container) or task_id = upper(i_container));

    if valid_container = 0 then raise incorrect_container;
    end if;

    /* Get Carrier */
    if upper(i_client) = 'TR' then
    select carrier_id
    into order_carrier
    from dcsdba.order_header -- Use Order Container instead for quicker query
    where client_id = upper(i_client)
    and (order_id = upper(i_container));
    end if;

    /* Remove spaces from carrier id (Mainly to make UK Mail UKMail and not have pallets with spaces in) */
    if (replace(order_carrier,' ','') not like '%' || upper(regexp_replace(i_new_pallet, '[0-9]')) || '%' or order_carrier is null)
    and upper(i_client) = 'TR' then raise incorrect_carrier;
    end if;

    select count(*)
    into inventory_count
    from dcsdba.inventory
    where upper(container_id) = upper(i_container)
    and client_id = upper(i_client);

    select count(*)
    into pallet_count
    from dcsdba.move_task
    where upper(container_id) = upper(i_container)
    and task_type = 'O'
    and client_id = upper(i_client);

    /* Check that the inventory is present also in the move task */
    if inventory_count <> pallet_count then raise no_inventory_match;
    end if;

    if i_client is null then raise no_client_entry;
    end if;

    if i_new_pallet is null then raise no_pallet_entry;
    end if;

    if i_container is null then raise no_cont_entry;
    end if;

    /* Check if new Pallet id is already created*/
    select count(*)
    into new_pallet_count
    from dcsdba.move_task
    where pallet_id = upper(i_new_pallet)
    and client_id = upper(i_client)
    and task_type = 'T';
    
    open get_pallet;
    loop
        /* Use all cursor */
        fetch get_pallet into orig_pallet, orig_container, v_from_loc_id, v_to_loc_id, v_final_loc_id;
            if get_pallet%NotFound then exit;
            end if;
        
        /* Can't scan to same pallet a second time */
        if orig_pallet = upper(i_new_pallet) then raise same_pallet;
        end if;
        
        /* Make sure there are no outstanding Picks for current Pallet*/
        if upper(i_client) = 'TR' then
        select count(*)
        into outstanding_order
        from dcsdba.move_task
        where task_id = upper(i_container)
        and task_type = 'O'
        and status in ('Released','Hold','In Progress','Error');
        end if;
        
        if new_pallet_count > 0 then
          select listagg(from_loc_id,'|')within group(order by pallet_id)
          , listagg(to_loc_id,'|')within group(order by pallet_id)
          , listagg(final_loc_id,'|')within group(order by pallet_id)
            into vp_from_loc_id, vp_to_loc_id, vp_final_loc_id
            from dcsdba.move_task
            where client_id = upper(i_client)
            and pallet_id = upper(i_new_pallet)
            and task_type = 'T'
            group by pallet_id;
        end if;
        
        if new_pallet_count > 0 then
            select count(*)
            into ship_dock_count
            from dcsdba.move_task
            where client_id = upper(i_client)
            and pallet_id = upper(i_new_pallet)
            and from_loc_id in 
                (select regexp_substr(vp_from_loc_id,'[^|]+', 1, level) from dual
                connect by regexp_substr(vp_from_loc_id, '[^|]+', 1, level) is not null)            
            and from_loc_id = v_from_loc_id
            and to_loc_id in
                (select regexp_substr(vp_to_loc_id,'[^|]+', 1, level) from dual
                connect by regexp_substr(vp_to_loc_id, '[^|]+', 1, level) is not null) 
            and to_loc_id = v_to_loc_id
            and final_loc_id in
                (select regexp_substr(vp_final_loc_id,'[^|]+', 1, level) from dual
                connect by regexp_substr(vp_final_loc_id, '[^|]+', 1, level) is not null)
            and final_loc_id = v_final_loc_id
            and task_type = 'T';
        end if;

        if outstanding_order > 0 and upper(i_client) = 'TR' then raise order_not_ready;
        end if;

        /* If it is a new pallet, then just update the pallet id in move task/inventory/order container tables */
        if new_pallet_count = 0 or ship_dock_count = 0 then

            insert into dcsdba.move_task
            (key
            , first_key
            , task_type
            , task_id
            , client_id
            , sku_id
            , description
            , qty_to_move
            , old_qty_to_move
            , site_id
            , from_loc_id
            , old_from_loc_id
            , to_loc_id
            , old_to_loc_id
            , final_loc_id
            , sequence
            , status
            , dstamp
            , start_dstamp
            , finish_dstamp
            , original_dstamp
            , priority
            , consol_link
            , work_zone
            , work_group
            , pallet_id
            , pallet_config
            , session_type
            , summary_record
            , repack
            , kit_ratio
            , ce_under_bond
            , uploaded_labor
            , print_label
            , catch_weight
            , stage_route_sequence
            , labelling
            , first_pick
            , plan_sequence
            , move_whole
            , shipping_unit
            )
            (
            select dcsdba.move_task_pk_seq.nextval
            , dcsdba.move_task_pk_seq.nextval
            , 'T'
            , 'PALLET'
            , upper(i_client)
            , 'PALLET'
            , 'Multi container pallet'
            , '1'
            , '1'
            , site_id
            , from_loc_id
            , old_from_loc_id
            , to_loc_id
            , old_to_loc_id
            , final_loc_id
            , sequence
            , 'Released'
            , sysdate
            , sysdate
            , sysdate
            , original_dstamp
            , priority
            , dcsdba.consol_move_task_link_seq.nextval
            , work_zone
            , work_group
            , upper(i_new_pallet)
            , 'MD'
            , session_type
            , summary_record
            , repack
            , kit_ratio
            , ce_under_bond
            , uploaded_labor
            , print_label
            , catch_weight
            , stage_route_sequence
            , labelling
            , first_pick
            , plan_sequence
            , move_whole
            , shipping_unit
            from dcsdba.move_task
            where client_id = upper(i_client)
            and pallet_id = orig_pallet
            and (container_id = upper(i_container) or task_id = upper(i_container))
            /* Ensure just one line is inserted */
            and rownum = 1);
            
            commit;
            
        end if;
            
        /*
         Update the consol links to match the consol link of the destination's pallet,
         Update the pallet id in move task/inventory/order container
        */
        
        /* Grab consol link of new pallet header */
        select consol_link
        into new_consol_link
        from dcsdba.move_task
        where client_id = upper(i_client)
        and pallet_id = upper(i_new_pallet)
        and from_loc_id = v_from_loc_id
        and to_loc_id = v_to_loc_id
        and final_loc_id = v_final_loc_id
        and task_type = 'T'
        and rownum = 1;
            
        /* If is last consol task, then delete Marshall header as it will not have any children after last update */
        select count(distinct task_id)
        into orig_marshall_hdr
        from dcsdba.move_task
        where client_id = upper(i_client)
        and pallet_id = orig_pallet
        and task_type = 'O'
        and status = 'Consol';

        if orig_marshall_hdr <= 1 then   
            delete from dcsdba.move_task
            where client_id = upper(i_client)
            and task_type = 'T'
            and upper(pallet_id) = upper(orig_pallet);
        end if;
            
        /* Switch children to new pallet header for move task/inventory/order container */
        update dcsdba.move_task
        set pallet_id = upper(i_new_pallet), consol_link = new_consol_link
        where client_id = upper(i_client)
        and task_type = 'O'
        and pallet_id = orig_pallet
        and container_id = orig_container;

        update dcsdba.inventory
        set pallet_id = upper(i_new_pallet)
        where container_id = orig_container
        and pallet_id = orig_pallet
        and client_id = upper(i_client);

        update dcsdba.order_container
        set pallet_id = upper(i_new_pallet)
        where container_id = orig_container
        and pallet_id = orig_pallet
        and client_id = upper(i_client);
        
        finish_time := current_timestamp;
        
        insert into torque.fn_tq_movepallet
        (user_id
        , start_dstamp
        , finish_dstamp
        , from_pal
        , to_pal
        , container_id
        )values(
        i_user
        , start_time
        , finish_time
        , orig_pallet
        , upper(i_new_pallet)
        , upper(i_container)
        );
    
        commit;

    end loop;
    
    close get_pallet;

    /* Used to see how many containers currently in new Pallet */
    select count(distinct container_id)
    into container_count
    from dcsdba.move_task
    where pallet_id = upper(i_new_pallet)
    and client_id = upper(i_client);

    /* Outputs */
    return_value := i_container || ' now in Pallet: ' || i_new_pallet || '. Currently ' || container_count || ' Cartons in new Pallet.';
    return return_value;

    exception
        when no_inventory_match then
        return_value := 'Inventory doesnt match move tasks';
        return return_value;

        when no_cont_entry then
        return_value := 'No Container Entered';
        return return_value;

        when no_pallet_entry then
        return_value := 'No Pallet Entered';
        return return_value;

        when no_client_entry then
        return_value := 'No Client Entered';
        return return_value;

        when order_not_ready then
        return_value := 'Incomplete Order';
        return return_value;

        when incorrect_carrier then
        return_value := 'Not a ' || regexp_replace(i_new_pallet, '[0-9]') || ' order.';
        return return_value;

        when incorrect_container then
        return_value := 'Not a valid order.';
        return return_value;
        
        when multi_container then
        return_value := 'Order has multiple containers';
        return return_value;
        
        when same_pallet then
        return_value := 'Can''t scan into same pallet';
        return return_value;

end;

/

  GRANT EXECUTE ON "TORQUE"."TQ_MOVEPALLET" TO "DCSDBA";
