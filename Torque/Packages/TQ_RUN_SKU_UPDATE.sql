--------------------------------------------------------
--  DDL for Procedure TQ_RUN_SKU_UPDATE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "TORQUE"."TQ_RUN_SKU_UPDATE" is

l_count           NUMBER(5)                := 0;
l_limit             NUMBER(5)               := 100;

cursor c_sku_data
    is
    select * from torque.tq_sku_coo_update where uploaded is null;

r_sku_data c_sku_data%rowtype;

begin
    open c_sku_data;
    loop
        fetch c_sku_data into r_sku_data;
        if c_sku_data%notfound then
        exit;
        end if;
        l_count := l_count+1;

        update dcsdba.sku set ce_coo = r_sku_data.ce_coo,commodity_code = r_sku_data.commodity_code,commodity_desc = r_sku_data.commodity_desc , user_def_note_1= r_sku_data.user_def_note_1 where sku_id = r_sku_data.sku_id and client_id = r_sku_data.client_id;
        update torque.tq_sku_coo_update set uploaded = 'Y' where sku_id = r_sku_data.sku_id and client_id = r_sku_data.client_id;

        if l_count = l_limit then
        commit;
        l_count := 0;
        end if;
    end loop;
    close c_sku_data;
    commit;
end tq_run_sku_update;

/

  GRANT EXECUTE ON "TORQUE"."TQ_RUN_SKU_UPDATE" TO "DCSDBA" WITH GRANT OPTION;
