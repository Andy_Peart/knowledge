--------------------------------------------------------
--  DDL for Function WORKING_DAYS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "TORQUE"."WORKING_DAYS" (p_start_date DATE, p_end_date DATE)
   RETURN NUMBER
IS

/*
    Source: Rod West (DevShed)
    Author: Nabil Yousfi
    Description: Calculates the working days between 2 dates
                 Also caters for bank holidays in the 'bank_holidays' table
*/
    v_holidays     NUMBER;
    v_start_date   DATE   := TRUNC (p_start_date);
    v_end_date     DATE   := TRUNC (p_end_date);
BEGIN
    
    IF v_end_date >= v_start_date    THEN       
        SELECT COUNT (*)         
        INTO v_holidays         
        FROM bank_holidays        
        WHERE holiday BETWEEN v_start_date AND v_end_date        
        AND holiday NOT IN (    SELECT hol.holiday
                                FROM bank_holidays hol 
                                WHERE MOD(TO_CHAR(hol.holiday, 'J'), 7) + 1 IN (6, 7));        
                                
        RETURN   GREATEST (NEXT_DAY (v_start_date, 'MON') - v_start_date - 2, 0)              
                    +   (  (  NEXT_DAY (v_end_date, 'MON') - NEXT_DAY (v_start_date, 'MON') ) / 7 ) * 5 - GREATEST (NEXT_DAY (v_end_date, 'MON') - v_end_date - 3, 0) - v_holidays;
        ELSE
             RETURN NULL;    
        END IF; 
        
END working_days;

/

  GRANT EXECUTE ON "TORQUE"."WORKING_DAYS" TO "DCSDBA";
