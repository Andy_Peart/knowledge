--------------------------------------------------------
--  File created - Thursday-March-18-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Procedure TQ_PARCEL_PACK_DO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "TORQUE"."TQ_PARCEL_PACK_DO" (
    p_Client varchar2,
    p_TASK varchar2,
    p_PARCEL varchar2,
    p_user_id varchar2,
    p_sku VARCHAR2,
    p_key dcsdba.move_task.key%type
)  as
PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
    Update dcsdba.move_task set 
    status = 'Complete',
    to_pallet_id = p_client||p_PARCEL,
    to_container_id = p_client||p_PARCEL,
    user_id = p_user_id 
where
    TASK_TYPE = 'B'
    and task_id = P_TASK
    and CLIENT_ID = P_CLIENT
    and sku_id = p_sku
    and key = p_key;
    COMMIT;

END;

/
