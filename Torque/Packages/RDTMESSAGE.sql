--------------------------------------------------------
--  DDL for Function RDTMESSAGE
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "TORQUE"."RDTMESSAGE" 

  ( 

  p_stationid IN VARCHAR2, 

  p_msg IN VARCHAR2 

  ) 

  RETURN VARCHAR2 AS 

   

  v_cols INTEGER; 

  v_rows INTEGER; 



  v_msg VARCHAR2(1000); 

  v_linemsg v_msg%TYPE; 

  v_msgbuffer v_linemsg%type; 

  v_lines v_rows%TYPE; 



BEGIN 

  v_msg := TRIM(p_msg); 



  SELECT NVL(SCREEN_ROWS, 16), NVL(SCREEN_COLUMNS, 20) INTO v_rows, v_cols FROM dcsdba.WORKSTATION WHERE STATION_ID = p_stationid; 



  v_lines := 0; 



  WHILE (LENGTH(v_msg) > 0 AND v_lines < (v_rows - 2)) LOOP   

    IF LENGTH(v_msg) > v_cols THEN 

      IF SUBSTR(v_msg, v_cols + 1, 1) = ' ' THEN 

        -- Following character is a space 

        v_linemsg := SUBSTR(v_msg, 1, v_cols); 

--        DBMS_OUTPUT.PUT_LINE('Hit branch 1'); 

      ELSE 

        IF INSTR(SUBSTR(v_msg, 1, v_cols), ' ', -1) > 0 THEN 

          -- Spaces exist 

          v_linemsg := SUBSTR(v_msg, 1, INSTR(SUBSTR(v_msg, 1, v_cols), ' ', -1)); 

--          DBMS_OUTPUT.PUT_LINE('Hit branch 2'); 

        ELSE 

          -- No spaces 

          v_linemsg := SUBSTR(v_msg, 1, v_cols); 

--          DBMS_OUTPUT.PUT_LINE('Hit branch 3'); 

        END IF; 

      END IF; 

    ELSE 

      -- Remaining message is shorter than screen columns 

      v_linemsg := v_msg; 

--      DBMS_OUTPUT.PUT_LINE('Hit branch 4'); 

    END IF; 



    v_msg := TRIM(SUBSTR(v_msg, LENGTH(v_linemsg) + 1, LENGTH(v_msg) - LENGTH(v_linemsg))); 



    v_linemsg := RPAD(TRIM(v_linemsg), v_cols, ' '); 

    v_msgbuffer := v_msgbuffer || v_linemsg; 

    v_lines := v_lines + 1; 

  END LOOP; 



--  DBMS_OUTPUT.PUT_LINE(NULL); 

--  DBMS_OUTPUT.PUT_LINE(v_msgbuffer); 



  RETURN v_msgbuffer; 

END RDTMESSAGE;

/

  GRANT EXECUTE ON "TORQUE"."RDTMESSAGE" TO "DCSDBA";
