--------------------------------------------------------
--  File created - Tuesday-February-23-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package TQ_SORTATION_WALL
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "TORQUE"."TQ_SORTATION_WALL" AS 


/***********************************************************************************************************************/ 

/*                                                                                                                     */ 

/*  NAME:               TQ_SORTATION_WALL                                                                              */ 

/*                                                                                                                     */ 

/*  DESCRIPTION:        To Handle the Sortation Wall Custom Functionality                                              */ 

/*                                                                                                                     */ 

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */ 

/*  =======     ===========     ==     ===================================================================             */ 

/*    1         2019-07-12      KS     Initial Version                                                                 */ 

/*    2         2019-07-22		KS	   Modified To Support 2 Stage SORT	                                               */

/*    3         2019-07-22		KS     Added PROCESS_STOCK_DIRECT Function	                                           */

/*    4         2019-09-27      KS     Reworked SORTATION_WALL package and renamed to TQ_SORTATION_WALL                */

/*    5         2019-10-01      KS     Reworked Search_Location Function                                               */

/*    6         2019-10-08      KS     Modified PROCESS_STOCK Function                                                 */

/*    7         2019-11-11      KS     Modified PROCESS_STOCK_OUT to ensure TO_LOCATION IS CORRECT                     */

/*    8         2020-02-05      KS     Modified PROCESS_STOCK AND PROCESS_STOCK_OUT to no longer require client/site   */

/*                                                                                                                     */ 

/***********************************************************************************************************************/ 

/**********************GRANTS**************************

grant execute on TQ_SORTATION_WALL to DCSDBA; 

******************************************************/

/******************REQUIRED GRANTS*********************

grant select on move_task to TORQUE;
grant select on location to TORQUE;

grant update on move_task to TORQUE;
grant update on order_header to TORQUE;

grant insert on move_task to TORQUE;

grant execute on liberror to TORQUE;


/********************DEPENDENCIES***********************

Packages:
    RDTBUFFER

******************************************************/

-- 
    FUNCTION search_location (
        p_site_id       IN   VARCHAR2,
        p_client_id     IN   VARCHAR2,
        p_user_id       IN   VARCHAR2,
        p_station_id    IN   VARCHAR2,
        p_from_loc_id   IN   VARCHAR2,
        p_sku_id        IN   VARCHAR2,
        p_sort_mode     IN   NUMBER DEFAULT 1
    ) RETURN VARCHAR2; 

-- 

    FUNCTION process_stock (
        p_site_id      IN   VARCHAR2,
        p_client_id    IN   VARCHAR2,
        p_user_id      IN   VARCHAR2,
        p_station_id   IN   VARCHAR2
    ) RETURN NUMBER; 

-- 

    FUNCTION sort_validation (
        p_site_id       VARCHAR2,
        p_client_id     VARCHAR2,
        p_station_id    VARCHAR2,
        p_user_id       VARCHAR2,
        p_location_id   VARCHAR2
    ) RETURN NUMBER;

-- 

    FUNCTION process_stock_out (
        p_site_id      VARCHAR2,
        p_client_id    VARCHAR2,
        p_station_id   VARCHAR2,
        p_user_id      VARCHAR2
    ) RETURN NUMBER;

-- 

    FUNCTION process_stock_direct (
        p_site_id     VARCHAR2,
        p_client_id   VARCHAR2,
        p_order_id    VARCHAR2
    ) RETURN NUMBER;

END tq_sortation_wall;

/

  GRANT EXECUTE ON "TORQUE"."TQ_SORTATION_WALL" TO "DCSDBA";
--------------------------------------------------------
--  DDL for Package TQ_REPORTS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "TORQUE"."TQ_REPORTS" AS 

-- =============================================
-- Author:      Christian Haslam
-- Create date: 13/08/2020
-- Description: Package for Objects that are used in report selection in the WMS
--
--
-- Change History:
--      CH | 16/12/2020: Added function that allows us to create users in Bulk
-- Grants:
--      grant execute on TORQUE.TQ_REPORTS to DCSRPT
--      grant execute on TORQUE.TQ_REPORTS to DCSDBA
-- =============================================


-- =============================================
-- FUNCTIONS
-- =============================================


-- =============================================
-- PROCEDURES
-- =============================================
procedure TQ_UPDATE_SKU_CE_COO (op_Rows out number, p_Client varchar2, p_Sku varchar2, p_COO varchar2, p_Delimeter char default ',');
procedure TQ_BULK_CREATE_USERS(op_UsersCreated out number, p_UserToCopy varchar2, p_NewUsers varchar2, p_NewName varchar2 default null, p_Password varchar2, p_PassShouldChange char);

END TQ_REPORTS;

/

  GRANT EXECUTE ON "TORQUE"."TQ_REPORTS" TO "DCSDBA";
  GRANT EXECUTE ON "TORQUE"."TQ_REPORTS" TO "DCSRPT";
--------------------------------------------------------
--  DDL for Package TQ_INTERFACE
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "TORQUE"."TQ_INTERFACE" AS 
/***********************************************************************************************************************/

/*                                                                                                                     */

/*  NAME:               TQ_INTERFACE                                                                                   */

/*                                                                                                                     */

/*  DESCRIPTION:        To Store Provide Easy Access to the JDA API For Use With SSIS And Courier                      */

/*                                                                                                                     */

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */

/*  =======     ===========     ==     ===================================================================             */

/*    1         2019-10-18      KS     Initial Version                                                                 */

/*    2         2019-11-05      KS     SKU Interface Function Modified                                                 */

/*    3         2020-01-28      KS     Interface Functions Modified                                                    */

/*    4         2020-02-10      KS     Interface Receipt Function Created                                              */

/*    4         2020-02-20      KS     Interface SKU added SKU_EXT table Functionality                                 */

/*    5         2020-02-27      KS     Added INTERFACE_ORDER_HEADER & INTERFACE_ORDER_LINE Functions                   */

/*    6         2020-02-28      KS     Amended Order_Line and Pre_Advice_line to Generate Line ID                      */

/*    7         2020-03-02      KS     Standardised Pre-Advice And Receipt Interface Parameters                        */

/*    8         2020-03-10      KS     INTERFACE_LOCATION Function Added                                               */

/*    9         2020-04-14      KS     JSON_ERROR Function Added                                                       */

/*                                                                                                                     */

/***********************************************************************************************************************/

/**********************GRANTS**************************

grant execute on TQ_INTERFACE to DCSDBA;  

******************************************************/

/******************REQUIRED GRANTS*********************

grant select on pre_advice_line to TORQUE;
grant select on IF_PAH_PK_SEQ to TORQUE;
grant select on IF_PAL_PK_SEQ to TORQUE;
grant select on IF_SUPSKU_PK_SEQ TO TORQUE;
grant select on IF_SSC_PK_SEQ TO TORQUE;
grant select on IF_SKU_PK_SEQ TO TORQUE;
grant select on IF_REC_PK_SEQ to TORQUE;
grant select on interface_receipt to TORQUE;
grant select on interface_pre_advice_line to TORQUE;
grant select on interface_sku to TORQUE;
grant select on IF_OH_PK_SEQ TO TORQUE;
grant select on IF_OL_PK_SEQ TO TORQUE;
grant select on IF_L_PK_SEQ to TORQUE;
grant select on interface_order_header to TORQUE;
grant select on interface_order_line to TORQUE;
grant select on interface_location to TORQUE;
grant select on language_text TO TORQUE;

grant insert on interface_update_columns to TORQUE;
grant insert on interface_sku to TORQUE;
grant insert on interface_pre_advice_line to TORQUE;
grant insert on interface_pre_advice_header to TORQUE;
grant insert on interface_sku_sku_config to TORQUE;
grant insert on interface_supplier_sku to TORQUE;
grant insert on interface_receipt to TORQUE;
grant insert on interface_order_header to TORQUE;
grant insert on interface_order_line to TORQUE;
grant insert on interface_location to TORQUE;

grant update on sku to TORQUE;
grant update on pre_advice_line to TORQUE;

grant delete on interface_update_columns to TORQUE;
grant delete on interface_sku to TORQUE;
grant delete on interface_order_header to TORQUE;
grant delete on interface_order_line to TORQUE;

grant execute on libsession to TORQUE;
grant execute on libmergesku to TORQUE;
grant execute on libmergeskuskuconfig to TORQUE;
grant execute on libmergesuppliersku to TORQUE;
grant execute on libmergepreadvice to TORQUE;
grant execute on libmergereceipt to TORQUE;
grant execute on libmergeorder to TORQUE;
grant execute on libmergelocation to TORQUE;
grant execute on liberror to TORQUE;

******************************************************/
    FUNCTION interface_sku (
        in_merge_action              VARCHAR2 DEFAULT 'U',
        in_client_id                 VARCHAR2,
        in_sku_id                    VARCHAR2,
        in_ean                       VARCHAR2 DEFAULT NULL,
        in_upc                       VARCHAR2 DEFAULT NULL,
        in_description               VARCHAR2,
        in_product_group             VARCHAR2 DEFAULT NULL,
        in_each_height               NUMBER DEFAULT NULL,
        in_each_weight               NUMBER DEFAULT NULL,
        in_each_volume               NUMBER DEFAULT NULL,
        in_each_value                NUMBER DEFAULT NULL,
        in_each_quantity             NUMBER DEFAULT NULL,
        in_qc_status                 VARCHAR2 DEFAULT NULL,
        in_shelf_life                NUMBER DEFAULT NULL,
        in_qc_frequency              NUMBER DEFAULT NULL,
        in_split_lowest              VARCHAR2 DEFAULT NULL,
        in_condition_reqd            VARCHAR2 DEFAULT NULL,
        in_expiry_reqd               VARCHAR2 DEFAULT NULL,
        in_origin_reqd               VARCHAR2 DEFAULT NULL,
        in_serial_at_pack            VARCHAR2 DEFAULT NULL,
        in_serial_at_pick            VARCHAR2 DEFAULT NULL,
        in_serial_at_receipt         VARCHAR2 DEFAULT NULL,
        in_serial_range              VARCHAR2 DEFAULT NULL,
        in_serial_format             VARCHAR2 DEFAULT NULL,
        in_serial_valid_merge        VARCHAR2 DEFAULT NULL,
        in_serial_no_reuse           VARCHAR2 DEFAULT NULL,
        in_serial_per_each           NUMBER DEFAULT NULL,
        in_pick_sequence             NUMBER DEFAULT NULL,
        in_pick_count_qty            NUMBER DEFAULT NULL,
        in_count_frequency           NUMBER DEFAULT NULL,
        in_oain_wiin_enabled         VARCHAR2 DEFAULT NULL,
        in_kit_sku                   VARCHAR2 DEFAULT NULL,
        in_kit_split                 VARCHAR2 DEFAULT NULL,
        in_kit_trigger_qty           NUMBER DEFAULT NULL,
        in_kit_qty_due               NUMBER DEFAULT NULL,
        in_kitting_loc_id            VARCHAR2 DEFAULT NULL,
        in_allocation_group          VARCHAR2 DEFAULT NULL,
        in_putaway_group             VARCHAR2 DEFAULT NULL,
        in_abc_disable               VARCHAR2 DEFAULT NULL,
        in_handling_class            VARCHAR2 DEFAULT NULL,
        in_obsolete_product          VARCHAR2 DEFAULT NULL,
        in_new_product               VARCHAR2 DEFAULT NULL,
        in_disallow_upload           VARCHAR2 DEFAULT NULL,
        in_disallow_cross_dock       VARCHAR2 DEFAULT NULL,
        in_manuf_dstamp_reqd         VARCHAR2 DEFAULT NULL,
        in_manuf_dstamp_flt          VARCHAR2 DEFAULT NULL,
        in_min_shelf_life            NUMBER DEFAULT NULL,
        in_colour                    VARCHAR2 DEFAULT NULL,
        in_sku_size                  VARCHAR2 DEFAULT NULL,
        in_hazmat                    VARCHAR2 DEFAULT NULL,
        in_hazmat_id                 VARCHAR2 DEFAULT NULL,
        in_ship_shelf_life           NUMBER DEFAULT NULL,
        in_nmfc_number               NUMBER DEFAULT NULL,
        in_incub_rule                VARCHAR2 DEFAULT NULL,
        in_incub_hours               NUMBER DEFAULT NULL,
        in_each_width                NUMBER DEFAULT NULL,
        in_each_depth                NUMBER DEFAULT NULL,
        in_reorder_trigger_qty       NUMBER DEFAULT NULL,
        in_low_trigger_qty           NUMBER DEFAULT NULL,
        in_disallow_merge_rules      VARCHAR2 DEFAULT NULL,
        in_pack_despatch_repack      VARCHAR2 DEFAULT NULL,
        in_spec_id                   VARCHAR2 DEFAULT NULL,
        in_user_def_type_1           VARCHAR2 DEFAULT NULL,
        in_user_def_type_2           VARCHAR2 DEFAULT NULL,
        in_user_def_type_3           VARCHAR2 DEFAULT NULL,
        in_user_def_type_4           VARCHAR2 DEFAULT NULL,
        in_user_def_type_5           VARCHAR2 DEFAULT NULL,
        in_user_def_type_6           VARCHAR2 DEFAULT NULL,
        in_user_def_type_7           VARCHAR2 DEFAULT NULL,
        in_user_def_type_8           VARCHAR2 DEFAULT NULL,
        in_user_def_chk_1            VARCHAR2 DEFAULT NULL,
        in_user_def_chk_2            VARCHAR2 DEFAULT NULL,
        in_user_def_chk_3            VARCHAR2 DEFAULT NULL,
        in_user_def_chk_4            VARCHAR2 DEFAULT NULL,
        in_user_def_date_1           VARCHAR2 DEFAULT NULL,
        in_user_def_date_2           VARCHAR2 DEFAULT NULL,
        in_user_def_date_3           VARCHAR2 DEFAULT NULL,
        in_user_def_date_4           VARCHAR2 DEFAULT NULL,
        in_user_def_num_1            NUMBER DEFAULT NULL,
        in_user_def_num_2            NUMBER DEFAULT NULL,
        in_user_def_num_3            NUMBER DEFAULT NULL,
        in_user_def_num_4            NUMBER DEFAULT NULL,
        in_user_def_note_1           VARCHAR2 DEFAULT NULL,
        in_user_def_note_2           VARCHAR2 DEFAULT NULL,
        in_beam_units                NUMBER DEFAULT NULL,
        in_ce_warehouse_type         VARCHAR2 DEFAULT NULL,
        in_ce_customs_excise         VARCHAR2 DEFAULT NULL,
        in_ce_standard_cost          NUMBER DEFAULT NULL,
        in_ce_standard_currency      VARCHAR2 DEFAULT NULL,
        in_disallow_clustering       VARCHAR2 DEFAULT NULL,
        in_max_stack                 NUMBER DEFAULT NULL,
        in_stack_description         VARCHAR2 DEFAULT NULL,
        in_stack_limitation          NUMBER DEFAULT NULL,
        in_ce_duty_stamp             VARCHAR2 DEFAULT NULL,
        in_capture_weight            VARCHAR2 DEFAULT NULL,
        in_weigh_at_receipt          VARCHAR2 DEFAULT NULL,
        in_upper_weight_tolerance    NUMBER DEFAULT NULL,
        in_lower_weight_tolerance    NUMBER DEFAULT NULL,
        in_serial_at_loading         VARCHAR2 DEFAULT NULL,
        in_serial_at_kitting         VARCHAR2 DEFAULT NULL,
        in_serial_at_unkitting       VARCHAR2 DEFAULT NULL,
        in_ce_commodity_code         VARCHAR2 DEFAULT NULL,
        in_ce_coo                    VARCHAR2 DEFAULT NULL,
        in_ce_cwc                    VARCHAR2 DEFAULT NULL,
        in_ce_vat_code               VARCHAR2 DEFAULT NULL,
        in_ce_product_type           VARCHAR2 DEFAULT NULL,
        in_commodity_code            VARCHAR2 DEFAULT NULL,
        in_commodity_desc            VARCHAR2 DEFAULT NULL,
        in_family_group              VARCHAR2 DEFAULT NULL,
        in_breakpack                 VARCHAR2 DEFAULT NULL,
        in_clearable                 VARCHAR2 DEFAULT NULL,
        in_stage_route_id            VARCHAR2 DEFAULT NULL,
        in_serial_dynamic_range      VARCHAR2 DEFAULT NULL,
        in_serial_max_range          NUMBER DEFAULT NULL,
        in_manufacture_at_repack     VARCHAR2 DEFAULT NULL,
        in_expiry_at_repack          VARCHAR2 DEFAULT NULL,
        in_udf_at_repack             VARCHAR2 DEFAULT NULL,
        in_repack_by_piece           VARCHAR2 DEFAULT NULL,
        in_packed_height             NUMBER DEFAULT NULL,
        in_packed_width              NUMBER DEFAULT NULL,
        in_packed_depth              NUMBER DEFAULT NULL,
        in_packed_volume             NUMBER DEFAULT NULL,
        in_packed_weight             NUMBER DEFAULT NULL,
        in_hanging_garment           VARCHAR2 DEFAULT NULL,
        in_conveyable                VARCHAR2 DEFAULT NULL,
        in_fragile                   VARCHAR2 DEFAULT NULL,
        in_gender                    VARCHAR2 DEFAULT NULL,
        in_high_security             VARCHAR2 DEFAULT NULL,
        in_ugly                      VARCHAR2 DEFAULT NULL,
        in_collatable                VARCHAR2 DEFAULT NULL,
        in_ecommerce                 VARCHAR2 DEFAULT NULL,
        in_promotion                 VARCHAR2 DEFAULT NULL,
        in_foldable                  VARCHAR2 DEFAULT NULL,
        in_style                     VARCHAR2 DEFAULT NULL,
        in_business_unit_code        VARCHAR2 DEFAULT NULL,
        in_two_man_lift              VARCHAR2 DEFAULT NULL,
        in_awkward                   VARCHAR2 DEFAULT NULL,
        in_decatalogued              VARCHAR2 DEFAULT NULL,
        in_stock_check_rule_id       VARCHAR2 DEFAULT NULL,
        in_unkitting_inherit         VARCHAR2 DEFAULT NULL,
        in_serial_at_stock_check     VARCHAR2 DEFAULT NULL,
        in_serial_at_stock_adjust    VARCHAR2 DEFAULT NULL,
        in_kit_ship_components       VARCHAR2 DEFAULT NULL,
        in_unallocatable             VARCHAR2 DEFAULT NULL,
        in_batch_at_kitting          VARCHAR2 DEFAULT NULL,
        in_collective_mode           VARCHAR2 DEFAULT NULL,
        in_collective_sequence       NUMBER DEFAULT NULL,
        in_vmi_allow_allocation      VARCHAR2 DEFAULT NULL,
        in_vmi_allow_replenish       VARCHAR2 DEFAULT NULL,
        in_vmi_allow_manual          VARCHAR2 DEFAULT NULL,
        in_vmi_allow_interfaced      VARCHAR2 DEFAULT NULL,
        in_vmi_overstock_qty         NUMBER DEFAULT NULL,
        in_vmi_aging_days            NUMBER DEFAULT NULL,
        in_scrap_on_return           VARCHAR2 DEFAULT NULL,
        in_harmonised_product_code   VARCHAR2 DEFAULT NULL,
        in_tag_merge                 VARCHAR2 DEFAULT NULL,
        in_carrier_pallet_mixing     VARCHAR2 DEFAULT NULL,
        in_special_container_type    VARCHAR2 DEFAULT NULL,
        in_disallow_over_picking     VARCHAR2 DEFAULT NULL,
        in_no_alloc_backorder        VARCHAR2 DEFAULT NULL,
        in_return_min_shelf_life     NUMBER DEFAULT NULL,
        in_weigh_at_grid_pick        VARCHAR2 DEFAULT NULL,
        in_ce_excise_product_code    VARCHAR2 DEFAULT NULL,
        in_ce_degree_plato           NUMBER DEFAULT NULL,
        in_ce_designation_origin     VARCHAR2 DEFAULT NULL,
        in_ce_density                NUMBER DEFAULT NULL,
        in_ce_brand_name             VARCHAR2 DEFAULT NULL,
        in_ce_alcoholic_strength     NUMBER DEFAULT NULL,
        in_ce_fiscal_mark            VARCHAR2 DEFAULT NULL,
        in_ce_size_of_producer       VARCHAR2 DEFAULT NULL,
        in_ce_commercial_desc        VARCHAR2 DEFAULT NULL,
        in_serial_no_outbound        VARCHAR2 DEFAULT NULL,
        in_full_pallet_at_receipt    VARCHAR2 DEFAULT NULL,
        in_always_full_pallet        VARCHAR2 DEFAULT NULL,
        in_sub_within_product_grp    VARCHAR2 DEFAULT NULL,
        in_serial_check_string       VARCHAR2 DEFAULT NULL,
        in_carrier_product_type      VARCHAR2 DEFAULT NULL,
        in_max_pack_configs          NUMBER DEFAULT NULL,
        in_parcel_packing_by_piece   VARCHAR2 DEFAULT NULL,
        in_time_zone_name            VARCHAR2 DEFAULT NULL,
        in_nls_calendar              VARCHAR2 DEFAULT NULL,
        in_client_group              VARCHAR2 DEFAULT NULL,
        in_update_columns            VARCHAR2 DEFAULT NULL,
        in_user_id                   VARCHAR2 DEFAULT 'API',
        in_station_id                VARCHAR2 DEFAULT 'API',
        in_user_def_type_9           VARCHAR2 DEFAULT NULL,
        in_user_def_type_10          VARCHAR2 DEFAULT NULL,
        in_user_def_type_11          VARCHAR2 DEFAULT NULL,
        in_user_def_type_12          VARCHAR2 DEFAULT NULL,
        in_user_def_type_13          VARCHAR2 DEFAULT NULL,
        in_user_def_type_14          VARCHAR2 DEFAULT NULL,
        in_user_def_type_15          VARCHAR2 DEFAULT NULL,
        in_user_def_type_16          VARCHAR2 DEFAULT NULL,
        in_user_def_type_17          VARCHAR2 DEFAULT NULL,
        in_user_def_type_18          VARCHAR2 DEFAULT NULL,
        in_user_def_type_19          VARCHAR2 DEFAULT NULL,
        in_user_def_type_20          VARCHAR2 DEFAULT NULL,
        in_user_def_note_3           VARCHAR2 DEFAULT NULL,
        in_user_def_note_4           VARCHAR2 DEFAULT NULL,
        in_user_def_note_5           VARCHAR2 DEFAULT NULL,
        in_user_def_note_6           VARCHAR2 DEFAULT NULL,
        in_sku_aux2                  VARCHAR2 DEFAULT NULL,
        in_sku_knit_wove             VARCHAR2 DEFAULT NULL,
        in_sku_fabric1               VARCHAR2 DEFAULT NULL,
        in_sku_fabric2               VARCHAR2 DEFAULT NULL,
        in_sku_supplier              VARCHAR2 DEFAULT NULL,
        in_record_error_in_table     VARCHAR2 DEFAULT NULL
    ) RETURN VARCHAR2;
-- 

    FUNCTION interface_sku_sku_config (
        in_update_columns          VARCHAR2 DEFAULT NULL,
        in_merge_action            VARCHAR2 DEFAULT 'U',
        in_client_id               VARCHAR2,
        in_sku_id                  VARCHAR2,
        in_config_id               VARCHAR2,
        in_main_config_id          VARCHAR2 DEFAULT NULL,
        in_min_full_pallet_perc    INTEGER DEFAULT NULL,
        in_max_full_pallet_perc    INTEGER DEFAULT NULL,
        in_disabled                VARCHAR2 DEFAULT NULL,
        in_nls_calendar              VARCHAR2 DEFAULT NULL,
        in_timezone_name           VARCHAR2 DEFAULT NULL,
        in_user_id                 VARCHAR2 DEFAULT 'API-INT',
        in_station_id              VARCHAR2 DEFAULT 'API-INT',
        in_record_error_in_table   VARCHAR2 DEFAULT NULL
    ) RETURN VARCHAR2;
--

    FUNCTION interface_supplier_sku (
        in_update_columns            VARCHAR2 DEFAULT NULL,
        in_merge_action              VARCHAR2 DEFAULT 'U',
        in_client_id                 VARCHAR2,
        in_sku_id                    VARCHAR2,
        in_supplier_sku_id           VARCHAR2,
        in_supplier_id               VARCHAR2 DEFAULT NULL,
        in_notes                     VARCHAR2 DEFAULT NULL,
        in_vm_sku                    VARCHAR2 DEFAULT NULL,
        in_nls_calendar              VARCHAR2 DEFAULT NULL,
        in_timezone_name             VARCHAR2 DEFAULT NULL,
        in_qc_status                 VARCHAR2 DEFAULT NULL,
        in_delivery_lead_time_days   NUMBER DEFAULT NULL,
        in_reorder_preference        NUMBER DEFAULT NULL,
        in_user_id                   VARCHAR2 DEFAULT 'API-INT',
        in_station_id                VARCHAR2 DEFAULT 'API-INT',
        in_record_error_in_table     VARCHAR2 DEFAULT NULL
    ) RETURN VARCHAR2;
--

    FUNCTION interface_pre_advice_header (
        in_update_columns          VARCHAR2 DEFAULT NULL,
        in_merge_action            VARCHAR2 DEFAULT 'U',
        in_client_id               VARCHAR2,
        in_pre_advice_id           VARCHAR2,
        in_pre_advice_type         VARCHAR2 DEFAULT NULL,
        in_site_id                 VARCHAR2 DEFAULT NULL,
        in_owner_id                VARCHAR2 DEFAULT NULL,
        in_supplier_id             VARCHAR2 DEFAULT NULL,
        in_status                  VARCHAR2 DEFAULT NULL,
        in_bookref_id              VARCHAR2 DEFAULT NULL,
        in_due_dstamp              VARCHAR2 DEFAULT NULL,
        in_contact                 VARCHAR2 DEFAULT NULL,
        in_contact_phone           VARCHAR2 DEFAULT NULL,
        in_contact_fax             VARCHAR2 DEFAULT NULL,
        in_contact_email           VARCHAR2 DEFAULT NULL,
        in_name                    VARCHAR2 DEFAULT NULL,
        in_address1                VARCHAR2 DEFAULT NULL,
        in_address2                VARCHAR2 DEFAULT NULL,
        in_town                    VARCHAR2 DEFAULT NULL,
        in_county                  VARCHAR2 DEFAULT NULL,
        in_postcode                VARCHAR2 DEFAULT NULL,
        in_country                 VARCHAR2 DEFAULT NULL,
        in_vat_number              VARCHAR2 DEFAULT NULL,
        in_return_flag             VARCHAR2 DEFAULT NULL,
        in_sampling_type           VARCHAR2 DEFAULT NULL,
        in_returned_order_id       VARCHAR2 DEFAULT NULL,
        in_email_confirm           VARCHAR2 DEFAULT NULL,
        in_collection_reqd         VARCHAR2 DEFAULT NULL,
        in_consignment             VARCHAR2 DEFAULT NULL,
        in_load_sequence           NUMBER DEFAULT NULL,
        in_notes                   VARCHAR2 DEFAULT NULL,
        in_disallow_merge_rules    VARCHAR2 DEFAULT NULL,
        in_user_def_type_1         VARCHAR2 DEFAULT NULL,
        in_user_def_type_2         VARCHAR2 DEFAULT NULL,
        in_user_def_type_3         VARCHAR2 DEFAULT NULL,
        in_user_def_type_4         VARCHAR2 DEFAULT NULL,
        in_user_def_type_5         VARCHAR2 DEFAULT NULL,
        in_user_def_type_6         VARCHAR2 DEFAULT NULL,
        in_user_def_type_7         VARCHAR2 DEFAULT NULL,
        in_user_def_type_8         VARCHAR2 DEFAULT NULL,
        in_user_def_chk_1          VARCHAR2 DEFAULT NULL,
        in_user_def_chk_2          VARCHAR2 DEFAULT NULL,
        in_user_def_chk_3          VARCHAR2 DEFAULT NULL,
        in_user_def_chk_4          VARCHAR2 DEFAULT NULL,
        in_user_def_date_1         VARCHAR2 DEFAULT NULL,
        in_user_def_date_2         VARCHAR2 DEFAULT NULL,
        in_user_def_date_3         VARCHAR2 DEFAULT NULL,
        in_user_def_date_4         VARCHAR2 DEFAULT NULL,
        in_user_def_num_1          NUMBER DEFAULT NULL,
        in_user_def_num_2          NUMBER DEFAULT NULL,
        in_user_def_num_3          NUMBER DEFAULT NULL,
        in_user_def_num_4          NUMBER DEFAULT NULL,
        in_user_def_note_1         VARCHAR2 DEFAULT NULL,
        in_user_def_note_2         VARCHAR2 DEFAULT NULL,
        in_time_zone_name          VARCHAR2 DEFAULT NULL,
        in_disallow_replens        VARCHAR2 DEFAULT NULL,
        in_client_group            VARCHAR2 DEFAULT NULL,
        in_status_reason_code      VARCHAR2 DEFAULT NULL,
        in_priority                VARCHAR2 DEFAULT NULL,
        in_supplier_reference      VARCHAR2 DEFAULT NULL,
        in_carrier_name            VARCHAR2 DEFAULT NULL,
        in_carrier_reference       VARCHAR2 DEFAULT NULL,
        in_tod                     VARCHAR2 DEFAULT NULL,
        in_tod_place               VARCHAR2 DEFAULT NULL,
        in_mode_of_transport       VARCHAR2 DEFAULT NULL,
        in_yard_container_type     VARCHAR2 DEFAULT NULL,
        in_yard_container_id       VARCHAR2 DEFAULT NULL,
        in_ce_consignment_id       VARCHAR2 DEFAULT NULL,
        in_contact_mobile          VARCHAR2 DEFAULT NULL,
        in_master_pre_advice       VARCHAR2 DEFAULT NULL,
        in_ce_invoice_number       VARCHAR2 DEFAULT NULL,
        in_user_id                 VARCHAR2 DEFAULT 'API-INT',
        in_station_id              VARCHAR2 DEFAULT 'API-INT',
        in_nls_calendar VARCHAR2 DEFAULT NULL,
        in_record_error_in_table   VARCHAR2 DEFAULT NULL
    ) RETURN VARCHAR2;
--

    FUNCTION interface_pre_advice_line (
        in_update_columns          VARCHAR2 DEFAULT NULL,
        in_merge_action            VARCHAR2 DEFAULT 'U',
        in_client_id               VARCHAR2,
        in_pre_advice_id           VARCHAR2,
        in_line_id                 NUMBER DEFAULT NULL,
        in_host_pre_advice_id      VARCHAR2 DEFAULT NULL,
        in_host_line_id            VARCHAR2 DEFAULT NULL,
        in_sku_id                  VARCHAR2,
        in_config_id               VARCHAR2 DEFAULT NULL,
        in_batch_id                VARCHAR2 DEFAULT NULL,
        in_expiry_dstamp           VARCHAR2 DEFAULT NULL,
        in_manuf_dstamp            VARCHAR2 DEFAULT NULL,
        in_pallet_config           VARCHAR2 DEFAULT NULL,
        in_origin_id               VARCHAR2 DEFAULT NULL,
        in_condition_id            VARCHAR2 DEFAULT NULL,
        in_tag_id                  VARCHAR2 DEFAULT NULL,
        in_lock_code               VARCHAR2 DEFAULT NULL,
        in_spec_code               VARCHAR2 DEFAULT NULL,
        in_qty_due                 NUMBER DEFAULT NULL,
        in_notes                   VARCHAR2 DEFAULT NULL,
        in_disallow_merge_rules    VARCHAR2 DEFAULT NULL,
        in_user_def_type_1         VARCHAR2 DEFAULT NULL,
        in_user_def_type_2         VARCHAR2 DEFAULT NULL,
        in_user_def_type_3         VARCHAR2 DEFAULT NULL,
        in_user_def_type_4         VARCHAR2 DEFAULT NULL,
        in_user_def_type_5         VARCHAR2 DEFAULT NULL,
        in_user_def_type_6         VARCHAR2 DEFAULT NULL,
        in_user_def_type_7         VARCHAR2 DEFAULT NULL,
        in_user_def_type_8         VARCHAR2 DEFAULT NULL,
        in_user_def_chk_1          VARCHAR2 DEFAULT NULL,
        in_user_def_chk_2          VARCHAR2 DEFAULT NULL,
        in_user_def_chk_3          VARCHAR2 DEFAULT NULL,
        in_user_def_chk_4          VARCHAR2 DEFAULT NULL,
        in_user_def_date_1         VARCHAR2 DEFAULT NULL,
        in_user_def_date_2         VARCHAR2 DEFAULT NULL,
        in_user_def_date_3         VARCHAR2 DEFAULT NULL,
        in_user_def_date_4         VARCHAR2 DEFAULT NULL,
        in_user_def_num_1          NUMBER DEFAULT NULL,
        in_user_def_num_2          NUMBER DEFAULT NULL,
        in_user_def_num_3          NUMBER DEFAULT NULL,
        in_user_def_num_4          NUMBER DEFAULT NULL,
        in_user_def_note_1         VARCHAR2 DEFAULT NULL,
        in_user_def_note_2         VARCHAR2 DEFAULT NULL,
        in_time_zone_name          VARCHAR2 DEFAULT NULL,
        in_client_group            VARCHAR2 DEFAULT NULL,
        in_tracking_level          VARCHAR2 DEFAULT NULL,
        in_qty_due_tolerance       NUMBER DEFAULT NULL,
        in_ce_coo                  VARCHAR2 DEFAULT NULL,
        in_owner_id                VARCHAR2 DEFAULT NULL,
        in_ce_consignment_id       VARCHAR2 DEFAULT NULL,
        in_ce_under_bond           VARCHAR2 DEFAULT NULL,
        in_ce_link                 VARCHAR2 DEFAULT NULL,
        in_product_price           NUMBER DEFAULT NULL,
        in_product_currency        VARCHAR2 DEFAULT NULL,
        in_ce_invoice_number       VARCHAR2 DEFAULT NULL,
        in_serial_valid_merge      VARCHAR2 DEFAULT NULL,
        in_sampling_type           VARCHAR2 DEFAULT NULL,
        in_expected_net_weight     NUMBER DEFAULT NULL,
        in_expected_gross_weight   NUMBER DEFAULT NULL,
        in_find_line_id            VARCHAR2 DEFAULT 'Y',
        in_user_id                 VARCHAR2 DEFAULT 'API-INT',
        in_station_id              VARCHAR2 DEFAULT 'API-INT',
        in_nls_calendar VARCHAR2 DEFAULT NULL,
        in_record_error_in_table   VARCHAR2 DEFAULT NULL
    ) RETURN VARCHAR2;

--

    FUNCTION interface_receipt (
        in_update_columns          VARCHAR2 DEFAULT NULL,
        in_merge_action            VARCHAR2 DEFAULT 'A',
        in_site_id                 VARCHAR2,
        in_location_id             VARCHAR2,
        in_owner_id                VARCHAR2,
        in_client_id               VARCHAR2,
        in_sku_id                  VARCHAR2,
        in_config_id               VARCHAR2 DEFAULT NULL,
        in_tag_id                  VARCHAR2 DEFAULT NULL,
        in_qty_on_hand             NUMBER,
        in_batch_id                VARCHAR2 DEFAULT NULL,
        in_expiry_dstamp           VARCHAR2 DEFAULT NULL,
        in_manuf_dstamp            VARCHAR2 DEFAULT NULL,
        in_receipt_id              VARCHAR2 DEFAULT NULL,
        in_supplier_id             VARCHAR2 DEFAULT NULL,
        in_origin_id               VARCHAR2 DEFAULT NULL,
        in_condition_id            VARCHAR2 DEFAULT NULL,
        in_lock_code               VARCHAR2 DEFAULT NULL,
        in_tag_copies              INTEGER DEFAULT NULL,
        in_receipt_dstamp          VARCHAR2 DEFAULT NULL,
        in_pallet_id               VARCHAR2 DEFAULT NULL,
        in_pallet_config           VARCHAR2 DEFAULT 'MD',
        in_pallet_volume           NUMBER DEFAULT NULL,
        in_pallet_height           NUMBER DEFAULT NULL,
        in_pallet_depth            NUMBER DEFAULT NULL,
        in_pallet_width            NUMBER DEFAULT NULL,
        in_pallet_weight           NUMBER DEFAULT NULL,
        in_pallet_grouped          VARCHAR2 DEFAULT NULL,
        in_notes                   VARCHAR2 DEFAULT NULL,
        in_sampling_type           VARCHAR2 DEFAULT NULL,
        in_user_def_type_1         VARCHAR2 DEFAULT NULL,
        in_user_def_type_2         VARCHAR2 DEFAULT NULL,
        in_user_def_type_3         VARCHAR2 DEFAULT NULL,
        in_user_def_type_4         VARCHAR2 DEFAULT NULL,
        in_user_def_type_5         VARCHAR2 DEFAULT NULL,
        in_user_def_type_6         VARCHAR2 DEFAULT NULL,
        in_user_def_type_7         VARCHAR2 DEFAULT NULL,
        in_user_def_type_8         VARCHAR2 DEFAULT NULL,
        in_user_def_chk_1          VARCHAR2 DEFAULT NULL,
        in_user_def_chk_2          VARCHAR2 DEFAULT NULL,
        in_user_def_chk_3          VARCHAR2 DEFAULT NULL,
        in_user_def_chk_4          VARCHAR2 DEFAULT NULL,
        in_user_def_date_1         VARCHAR2 DEFAULT NULL,
        in_user_def_date_2         VARCHAR2 DEFAULT NULL,
        in_user_def_date_3         VARCHAR2 DEFAULT NULL,
        in_user_def_date_4         VARCHAR2 DEFAULT NULL,
        in_user_def_num_1          NUMBER DEFAULT NULL,
        in_user_def_num_2          NUMBER DEFAULT NULL,
        in_user_def_num_3          NUMBER DEFAULT NULL,
        in_user_def_num_4          NUMBER DEFAULT NULL,
        in_user_def_note_1         VARCHAR2 DEFAULT NULL,
        in_user_def_note_2         VARCHAR2 DEFAULT NULL,
        in_time_zone_name          VARCHAR2 DEFAULT NULL,
        in_client_group            VARCHAR2 DEFAULT NULL,
        in_tracking_level          VARCHAR2 DEFAULT NULL,
        in_ce_consignment_id       VARCHAR2 DEFAULT NULL,
        in_ce_receipt_type         VARCHAR2 DEFAULT NULL,
        in_ce_originator           VARCHAR2 DEFAULT NULL,
        in_ce_originator_ref       VARCHAR2 DEFAULT NULL,
        in_ce_coo                  VARCHAR2 DEFAULT NULL,
        in_ce_cwc                  VARCHAR2 DEFAULT NULL,
        in_ce_ucr                  VARCHAR2 DEFAULT NULL,
        in_ce_under_bond           VARCHAR2 DEFAULT NULL,
        in_ce_document_dstamp      VARCHAR2 DEFAULT NULL,
        in_spec_code               VARCHAR2 DEFAULT NULL,
        in_ce_duty_stamp           VARCHAR2 DEFAULT NULL,
        in_user_id                 VARCHAR2 DEFAULT 'API-INT',
        in_station_id              VARCHAR2 DEFAULT 'API-INT',
        in_nls_calendar              VARCHAR2 DEFAULT NULL,
        in_record_error_in_table   VARCHAR2 DEFAULT NULL,
        in_update_pre_advice       VARCHAR2 DEFAULT 'N',
        in_line_id                 NUMBER DEFAULT NULL
    ) RETURN VARCHAR2;

--

    FUNCTION interface_order_header (
        in_update_columns            VARCHAR2 DEFAULT NULL,
        in_merge_action              VARCHAR2 DEFAULT 'U',
        in_order_id                  VARCHAR2,
        in_order_type                VARCHAR2 DEFAULT NULL,
        in_status                    VARCHAR2 DEFAULT NULL,
        in_priority                  VARCHAR2 DEFAULT NULL,
        in_ship_dock                 VARCHAR2 DEFAULT NULL,
        in_work_group                VARCHAR2 DEFAULT NULL,
        in_consignment               VARCHAR2 DEFAULT NULL,
        in_delivery_point            VARCHAR2 DEFAULT NULL,
        in_load_sequence             INTEGER DEFAULT NULL,
        in_from_site_id              VARCHAR2 DEFAULT NULL,
        in_to_site_id                VARCHAR2 DEFAULT NULL,
        in_customer_id               VARCHAR2 DEFAULT NULL,
        in_order_date                VARCHAR2 DEFAULT NULL,
        in_ship_by_date              VARCHAR2 DEFAULT NULL,
        in_purchase_order            VARCHAR2 DEFAULT NULL,
        in_contact                   VARCHAR2 DEFAULT NULL,
        in_contact_phone             VARCHAR2 DEFAULT NULL,
        in_contact_fax               VARCHAR2 DEFAULT NULL,
        in_contact_email             VARCHAR2 DEFAULT NULL,
        in_name                      VARCHAR2 DEFAULT NULL,
        in_address1                  VARCHAR2 DEFAULT NULL,
        in_address2                  VARCHAR2 DEFAULT NULL,
        in_town                      VARCHAR2 DEFAULT NULL,
        in_county                    VARCHAR2 DEFAULT NULL,
        in_postcode                  VARCHAR2 DEFAULT NULL,
        in_country                   VARCHAR2 DEFAULT NULL,
        in_instructions              VARCHAR2 DEFAULT NULL,
        in_repack                    VARCHAR2 DEFAULT NULL,
        in_owner_id                  VARCHAR2,
        in_carrier_id                VARCHAR2 DEFAULT NULL,
        in_dispatch_method           VARCHAR2 DEFAULT NULL,
        in_service_level             VARCHAR2 DEFAULT NULL,
        in_inv_address_id            VARCHAR2 DEFAULT NULL,
        in_inv_contact               VARCHAR2 DEFAULT NULL,
        in_inv_contact_phone         VARCHAR2 DEFAULT NULL,
        in_inv_contact_fax           VARCHAR2 DEFAULT NULL,
        in_inv_contact_email         VARCHAR2 DEFAULT NULL,
        in_inv_name                  VARCHAR2 DEFAULT NULL,
        in_inv_address1              VARCHAR2 DEFAULT NULL,
        in_inv_address2              VARCHAR2 DEFAULT NULL,
        in_inv_town                  VARCHAR2 DEFAULT NULL,
        in_inv_county                VARCHAR2 DEFAULT NULL,
        in_inv_postcode              VARCHAR2 DEFAULT NULL,
        in_inv_country               VARCHAR2 DEFAULT NULL,
        in_deliver_by_date           VARCHAR2 DEFAULT NULL,
        in_fastest_carrier           VARCHAR2 DEFAULT NULL,
        in_cheapest_carrier          VARCHAR2 DEFAULT NULL,
        in_site_replen               VARCHAR2 DEFAULT NULL,
        in_cid_number                VARCHAR2 DEFAULT NULL,
        in_sid_number                VARCHAR2 DEFAULT NULL,
        in_location_number           NUMBER DEFAULT NULL,
        in_freight_charges           VARCHAR2 DEFAULT NULL,
        in_client_id                 VARCHAR2,
        in_export                    VARCHAR2 DEFAULT NULL,
        in_disallow_merge_rules      VARCHAR2 DEFAULT NULL,
        in_user_def_type_1           VARCHAR2 DEFAULT NULL,
        in_user_def_type_2           VARCHAR2 DEFAULT NULL,
        in_user_def_type_3           VARCHAR2 DEFAULT NULL,
        in_user_def_type_4           VARCHAR2 DEFAULT NULL,
        in_user_def_type_5           VARCHAR2 DEFAULT NULL,
        in_user_def_type_6           VARCHAR2 DEFAULT NULL,
        in_user_def_type_7           VARCHAR2 DEFAULT NULL,
        in_user_def_type_8           VARCHAR2 DEFAULT NULL,
        in_user_def_chk_1            VARCHAR2 DEFAULT NULL,
        in_user_def_chk_2            VARCHAR2 DEFAULT NULL,
        in_user_def_chk_3            VARCHAR2 DEFAULT NULL,
        in_user_def_chk_4            VARCHAR2 DEFAULT NULL,
        in_user_def_date_1           VARCHAR2 DEFAULT NULL,
        in_user_def_date_2           VARCHAR2 DEFAULT NULL,
        in_user_def_date_3           VARCHAR2 DEFAULT NULL,
        in_user_def_date_4           VARCHAR2 DEFAULT NULL,
        in_user_def_num_1            NUMBER DEFAULT NULL,
        in_user_def_num_2            NUMBER DEFAULT NULL,
        in_user_def_num_3            NUMBER DEFAULT NULL,
        in_user_def_num_4            NUMBER DEFAULT NULL,
        in_user_def_note_1           VARCHAR2 DEFAULT NULL,
        in_user_def_note_2           VARCHAR2 DEFAULT NULL,
        in_soh_id                    VARCHAR2 DEFAULT NULL,
        in_move_task_status          VARCHAR2 DEFAULT NULL,
        in_time_zone_name            VARCHAR2 DEFAULT NULL,
        in_repack_loc_id             VARCHAR2 DEFAULT NULL,
        in_ce_reason_code            VARCHAR2 DEFAULT NULL,
        in_ce_reason_notes           VARCHAR2 DEFAULT NULL,
        in_ce_order_type             VARCHAR2 DEFAULT NULL,
        in_ce_customs_customer       VARCHAR2 DEFAULT NULL,
        in_ce_excise_customer        VARCHAR2 DEFAULT NULL,
        in_ce_instructions           VARCHAR2 DEFAULT NULL,
        in_client_group              VARCHAR2 DEFAULT NULL,
        in_delivered_dstamp          VARCHAR2 DEFAULT NULL,
        in_signatory                 VARCHAR2 DEFAULT NULL,
        in_route_id                  VARCHAR2 DEFAULT NULL,
        in_cross_dock_to_site        VARCHAR2 DEFAULT NULL,
        in_web_service_alloc_immed   VARCHAR2 DEFAULT NULL,
        in_web_service_alloc_clean   VARCHAR2 DEFAULT NULL,
        in_disallow_short_ship       VARCHAR2 DEFAULT NULL,
        in_work_order_type           VARCHAR2 DEFAULT NULL,
        in_status_reason_code        VARCHAR2 DEFAULT NULL,
        in_hub_address_id            VARCHAR2 DEFAULT NULL,
        in_hub_contact               VARCHAR2 DEFAULT NULL,
        in_hub_contact_phone         VARCHAR2 DEFAULT NULL,
        in_hub_contact_fax           VARCHAR2 DEFAULT NULL,
        in_hub_contact_email         VARCHAR2 DEFAULT NULL,
        in_hub_name                  VARCHAR2 DEFAULT NULL,
        in_hub_address1              VARCHAR2 DEFAULT NULL,
        in_hub_address2              VARCHAR2 DEFAULT NULL,
        in_hub_town                  VARCHAR2 DEFAULT NULL,
        in_hub_county                VARCHAR2 DEFAULT NULL,
        in_hub_postcode              VARCHAR2 DEFAULT NULL,
        in_hub_country               VARCHAR2 DEFAULT NULL,
        in_stage_route_id            VARCHAR2 DEFAULT NULL,
        in_single_order_sortation    VARCHAR2 DEFAULT NULL,
        in_hub_carrier_id            VARCHAR2 DEFAULT NULL,
        in_hub_service_level         VARCHAR2 DEFAULT NULL,
        in_force_single_carrier      VARCHAR2 DEFAULT NULL,
        in_expected_value            NUMBER DEFAULT NULL,
        in_expected_volume           NUMBER DEFAULT NULL,
        in_expected_weight           NUMBER DEFAULT NULL,
        in_tod                       VARCHAR2 DEFAULT NULL,
        in_tod_place                 VARCHAR2 DEFAULT NULL,
        in_language                  VARCHAR2 DEFAULT NULL,
        in_seller_name               VARCHAR2 DEFAULT NULL,
        in_seller_phone              VARCHAR2 DEFAULT NULL,
        in_documentation_text_1      VARCHAR2 DEFAULT NULL,
        in_documentation_text_2      VARCHAR2 DEFAULT NULL,
        in_documentation_text_3      VARCHAR2 DEFAULT NULL,
        in_cod                       VARCHAR2 DEFAULT NULL,
        in_cod_value                 NUMBER DEFAULT NULL,
        in_cod_currency              VARCHAR2 DEFAULT NULL,
        in_cod_type                  VARCHAR2 DEFAULT NULL,
        in_vat_number                VARCHAR2 DEFAULT NULL,
        in_inv_vat_number            VARCHAR2 DEFAULT NULL,
        in_hub_vat_number            VARCHAR2 DEFAULT NULL,
        in_inv_reference             VARCHAR2 DEFAULT NULL,
        in_inv_dstamp                VARCHAR2 DEFAULT NULL,
        in_inv_currency              VARCHAR2 DEFAULT NULL,
        in_print_invoice             VARCHAR2 DEFAULT NULL,
        in_letter_of_credit          VARCHAR2 DEFAULT NULL,
        in_payment_terms             VARCHAR2 DEFAULT NULL,
        in_subtotal_1                NUMBER DEFAULT NULL,
        in_subtotal_2                NUMBER DEFAULT NULL,
        in_subtotal_3                NUMBER DEFAULT NULL,
        in_subtotal_4                NUMBER DEFAULT NULL,
        in_freight_cost              NUMBER DEFAULT NULL,
        in_freight_terms             VARCHAR2 DEFAULT NULL,
        in_insurance_cost            NUMBER DEFAULT NULL,
        in_misc_charges              NUMBER DEFAULT NULL,
        in_discount                  NUMBER DEFAULT NULL,
        in_other_fee                 NUMBER DEFAULT NULL,
        in_inv_total_1               NUMBER DEFAULT NULL,
        in_inv_total_2               NUMBER DEFAULT NULL,
        in_inv_total_3               NUMBER DEFAULT NULL,
        in_inv_total_4               NUMBER DEFAULT NULL,
        in_tax_rate_1                NUMBER DEFAULT NULL,
        in_tax_basis_1               NUMBER DEFAULT NULL,
        in_tax_amount_1              NUMBER DEFAULT NULL,
        in_tax_rate_2                NUMBER DEFAULT NULL,
        in_tax_basis_2               NUMBER DEFAULT NULL,
        in_tax_amount_2              NUMBER DEFAULT NULL,
        in_tax_rate_3                NUMBER DEFAULT NULL,
        in_tax_basis_3               NUMBER DEFAULT NULL,
        in_tax_amount_3              NUMBER DEFAULT NULL,
        in_tax_rate_4                NUMBER DEFAULT NULL,
        in_tax_basis_4               NUMBER DEFAULT NULL,
        in_tax_amount_4              NUMBER DEFAULT NULL,
        in_tax_rate_5                NUMBER DEFAULT NULL,
        in_tax_basis_5               NUMBER DEFAULT NULL,
        in_tax_amount_5              NUMBER DEFAULT NULL,
        in_order_reference           VARCHAR2 DEFAULT NULL,
        in_start_by_date             VARCHAR2 DEFAULT NULL,
        in_metapack_carrier_pre      VARCHAR2 DEFAULT NULL,
        in_collective_mode           VARCHAR2 DEFAULT NULL,
        in_contact_mobile            VARCHAR2 DEFAULT NULL,
        in_inv_contact_mobile        VARCHAR2 DEFAULT NULL,
        in_hub_contact_mobile        VARCHAR2 DEFAULT NULL,
        in_shipment_group            VARCHAR2 DEFAULT NULL,
        in_freight_currency          VARCHAR2 DEFAULT NULL,
        in_ncts                      VARCHAR2 DEFAULT NULL,
        in_nls_calendar              VARCHAR2 DEFAULT NULL,
        in_mpack_consignment         VARCHAR2 DEFAULT NULL,
        in_mpack_nominated_dstamp    VARCHAR2 DEFAULT NULL,
        in_gln                       VARCHAR2 DEFAULT NULL,
        in_hub_gln                   VARCHAR2 DEFAULT NULL,
        in_inv_gln                   VARCHAR2 DEFAULT NULL,
        in_allocation_priority       INTEGER DEFAULT NULL,
        in_allow_pallet_pick         VARCHAR2 DEFAULT NULL,
        in_split_shipping_units      VARCHAR2 DEFAULT NULL,
        in_vol_pck_sscc_label        VARCHAR2 DEFAULT NULL,
        in_collective_sequence       INTEGER DEFAULT NULL,
        in_trax_use_hub_addr         VARCHAR2 DEFAULT NULL,
        in_direct_to_store           VARCHAR2 DEFAULT NULL,
        in_vol_ctr_label_format      VARCHAR2 DEFAULT NULL,
        in_retailer_id               VARCHAR2 DEFAULT NULL,
        in_carrier_bags              VARCHAR2 DEFAULT NULL,
        in_user_id                   VARCHAR2 DEFAULT 'API-INT',
        in_station_id                VARCHAR2 DEFAULT 'API-INT',
        in_record_error_in_table     VARCHAR2 DEFAULT NULL
    ) RETURN VARCHAR2;

--

    FUNCTION interface_order_line (
        in_update_columns             VARCHAR2 DEFAULT NULL,
        in_merge_action               VARCHAR2 DEFAULT 'U',
        in_order_id                   VARCHAR2,
        in_line_id                    INTEGER DEFAULT NULL,
        in_sku_id                     VARCHAR2 DEFAULT NULL,
        in_config_id                  VARCHAR2 DEFAULT NULL,
        in_tracking_level             VARCHAR2 DEFAULT NULL,
        in_batch_id                   VARCHAR2 DEFAULT NULL,
        in_batch_mixing               VARCHAR2 DEFAULT NULL,
        in_condition_id               VARCHAR2 DEFAULT NULL,
        in_lock_code                  VARCHAR2 DEFAULT NULL,
        in_qty_ordered                NUMBER,
        in_allocate                   VARCHAR2 DEFAULT NULL,
        in_back_ordered               VARCHAR2 DEFAULT NULL,
        in_deallocate                 VARCHAR2 DEFAULT NULL,
        in_kit_split                  VARCHAR2 DEFAULT NULL,
        in_origin_id                  VARCHAR2 DEFAULT NULL,
        in_notes                      VARCHAR2 DEFAULT NULL,
        in_customer_sku_id            VARCHAR2 DEFAULT NULL,
        in_shelf_life_days            INTEGER DEFAULT NULL,
        in_shelf_life_percent         INTEGER DEFAULT NULL,
        in_client_id                  VARCHAR2,
        in_disallow_merge_rules       VARCHAR2 DEFAULT NULL,
        in_user_def_type_1            VARCHAR2 DEFAULT NULL,
        in_user_def_type_2            VARCHAR2 DEFAULT NULL,
        in_user_def_type_3            VARCHAR2 DEFAULT NULL,
        in_user_def_type_4            VARCHAR2 DEFAULT NULL,
        in_user_def_type_5            VARCHAR2 DEFAULT NULL,
        in_user_def_type_6            VARCHAR2 DEFAULT NULL,
        in_user_def_type_7            VARCHAR2 DEFAULT NULL,
        in_user_def_type_8            VARCHAR2 DEFAULT NULL,
        in_user_def_chk_1             VARCHAR2 DEFAULT NULL,
        in_user_def_chk_2             VARCHAR2 DEFAULT NULL,
        in_user_def_chk_3             VARCHAR2 DEFAULT NULL,
        in_user_def_chk_4             VARCHAR2 DEFAULT NULL,
        in_user_def_date_1            VARCHAR2 DEFAULT NULL,
        in_user_def_date_2            VARCHAR2 DEFAULT NULL,
        in_user_def_date_3            VARCHAR2 DEFAULT NULL,
        in_user_def_date_4            VARCHAR2 DEFAULT NULL,
        in_user_def_num_1             NUMBER DEFAULT NULL,
        in_user_def_num_2             NUMBER DEFAULT NULL,
        in_user_def_num_3             NUMBER DEFAULT NULL,
        in_user_def_num_4             NUMBER DEFAULT NULL,
        in_user_def_note_1            VARCHAR2 DEFAULT NULL,
        in_user_def_note_2            VARCHAR2 DEFAULT NULL,
        in_soh_id                     VARCHAR2 DEFAULT NULL,
        in_line_value                 NUMBER DEFAULT NULL,
        in_time_zone_name             VARCHAR2 DEFAULT NULL,
        in_spec_code                  VARCHAR2 DEFAULT NULL,
        in_rule_id                    VARCHAR2 DEFAULT NULL,
        in_client_group               VARCHAR2 DEFAULT NULL,
        in_task_per_each              VARCHAR2 DEFAULT NULL,
        in_use_pick_to_grid           VARCHAR2 DEFAULT NULL,
        in_ignore_weight_capture      VARCHAR2 DEFAULT NULL,
        in_host_order_id              VARCHAR2 DEFAULT NULL,
        in_host_line_id               VARCHAR2 DEFAULT NULL,
        in_stage_route_id             VARCHAR2 DEFAULT NULL,
        in_min_qty_ordered            NUMBER DEFAULT NULL,
        in_max_qty_ordered            NUMBER DEFAULT NULL,
        in_expected_value             NUMBER DEFAULT NULL,
        in_expected_volume            NUMBER DEFAULT NULL,
        in_expected_weight            NUMBER DEFAULT NULL,
        in_customer_sku_desc1         VARCHAR2 DEFAULT NULL,
        in_customer_sku_desc2         VARCHAR2 DEFAULT NULL,
        in_purchase_order             VARCHAR2 DEFAULT NULL,
        in_product_price              NUMBER DEFAULT NULL,
        in_product_currency           VARCHAR2 DEFAULT NULL,
        in_documentation_unit         VARCHAR2 DEFAULT NULL,
        in_extended_price             NUMBER DEFAULT NULL,
        in_tax_1                      NUMBER DEFAULT NULL,
        in_tax_2                      NUMBER DEFAULT NULL,
        in_documentation_text_1       VARCHAR2 DEFAULT NULL,
        in_serial_number              VARCHAR2 DEFAULT NULL,
        in_owner_id                   VARCHAR2 DEFAULT NULL,
        in_collective_mode            VARCHAR2 DEFAULT NULL,
        in_ce_receipt_type            VARCHAR2 DEFAULT NULL,
        in_ce_coo                     VARCHAR2 DEFAULT NULL,
        in_kit_plan_id                VARCHAR2 DEFAULT NULL,
        in_location_id                VARCHAR2 DEFAULT NULL,
        in_nls_calendar               VARCHAR2 DEFAULT NULL,
        in_collective_sequence        NUMBER DEFAULT NULL,
        in_unallocatable              VARCHAR2 DEFAULT NULL,
        in_full_tracking_level_only   VARCHAR2 DEFAULT NULL,
        in_min_full_pallet_perc       INTEGER DEFAULT NULL,
        in_max_full_pallet_perc       INTEGER DEFAULT NULL,
        in_substitute_grade           VARCHAR2 DEFAULT NULL,
        in_disallow_substitution      VARCHAR2 DEFAULT NULL,
        in_find_line_id               VARCHAR2 DEFAULT 'Y',
        in_user_id                    VARCHAR2 DEFAULT 'API-INT',
        in_station_id                 VARCHAR2 DEFAULT 'API-INT',
        in_record_error_in_table      VARCHAR2 DEFAULT NULL
    ) RETURN VARCHAR2;

--

    FUNCTION interface_location (
        in_update_columns          VARCHAR2 DEFAULT NULL,
        in_merge_action            VARCHAR2 DEFAULT 'U',
        in_time_zone_name          VARCHAR2 DEFAULT NULL,
        in_site_id                 VARCHAR2,
        in_location_id             VARCHAR2,
        in_loc_type                VARCHAR2 DEFAULT NULL,
        in_lock_status             VARCHAR2 DEFAULT NULL,
        in_check_string            VARCHAR2 DEFAULT NULL,
        in_zone_1                  VARCHAR2 DEFAULT NULL,
        in_subzone_1               VARCHAR2 DEFAULT NULL,
        in_subzone_2               VARCHAR2 DEFAULT NULL,
        in_work_zone               VARCHAR2 DEFAULT NULL,
        in_storage_class           VARCHAR2 DEFAULT NULL,
        in_in_stage                VARCHAR2 DEFAULT NULL,
        in_out_stage               VARCHAR2 DEFAULT NULL,
        in_pick_sequence           NUMBER DEFAULT NULL,
        in_put_sequence            NUMBER DEFAULT NULL,
        in_check_sequence          NUMBER DEFAULT NULL,
        in_volume                  NUMBER DEFAULT NULL,
        in_height                  NUMBER DEFAULT NULL,
        in_depth                   NUMBER DEFAULT NULL,
        in_width                   NUMBER DEFAULT NULL,
        in_weight                  NUMBER DEFAULT NULL,
        in_disallow_alloc          VARCHAR2 DEFAULT NULL,
        in_allow_pall_cnt          VARCHAR2 DEFAULT NULL,
        in_abc_frequency           VARCHAR2 DEFAULT NULL,
        in_abc_storage             VARCHAR2 DEFAULT NULL,
        in_x_position              NUMBER DEFAULT NULL,
        in_y_position              NUMBER DEFAULT NULL,
        in_z_position              NUMBER DEFAULT NULL,
        in_point_id1               VARCHAR2 DEFAULT NULL,
        in_point_id2               VARCHAR2 DEFAULT NULL,
        in_ab_chargeable           VARCHAR2 DEFAULT NULL,
        in_beam_id                 VARCHAR2 DEFAULT NULL,
        in_beam_position           INTEGER DEFAULT NULL,
        in_beam_units              INTEGER DEFAULT NULL,
        in_qc_hold_reloc           VARCHAR2 DEFAULT NULL,
        in_qc_rel_reloc            VARCHAR2 DEFAULT NULL,
        in_user_def_type_1         VARCHAR2 DEFAULT NULL,
        in_user_def_type_2         VARCHAR2 DEFAULT NULL,
        in_user_def_type_3         VARCHAR2 DEFAULT NULL,
        in_user_def_type_4         VARCHAR2 DEFAULT NULL,
        in_user_def_type_5         VARCHAR2 DEFAULT NULL,
        in_user_def_type_6         VARCHAR2 DEFAULT NULL,
        in_user_def_type_7         VARCHAR2 DEFAULT NULL,
        in_user_def_type_8         VARCHAR2 DEFAULT NULL,
        in_user_def_chk_1          VARCHAR2 DEFAULT NULL,
        in_user_def_chk_2          VARCHAR2 DEFAULT NULL,
        in_user_def_chk_3          VARCHAR2 DEFAULT NULL,
        in_user_def_chk_4          VARCHAR2 DEFAULT NULL,
        in_user_def_date_1         VARCHAR2 DEFAULT NULL,
        in_user_def_date_2         VARCHAR2 DEFAULT NULL,
        in_user_def_date_3         VARCHAR2 DEFAULT NULL,
        in_user_def_date_4         VARCHAR2 DEFAULT NULL,
        in_user_def_num_1          NUMBER DEFAULT NULL,
        in_user_def_num_2          NUMBER DEFAULT NULL,
        in_user_def_num_3          NUMBER DEFAULT NULL,
        in_user_def_num_4          NUMBER DEFAULT NULL,
        in_user_def_note_1         VARCHAR2 DEFAULT NULL,
        in_user_def_note_2         VARCHAR2 DEFAULT NULL,
        in_fan_x                   INTEGER DEFAULT NULL,
        in_fan_y                   INTEGER DEFAULT NULL,
        in_fan_z                   INTEGER DEFAULT NULL,
        in_count_needed            VARCHAR2 DEFAULT NULL,
        in_aisle                   VARCHAR2 DEFAULT NULL,
        in_bay                     VARCHAR2 DEFAULT NULL,
        in_levels                  VARCHAR2 DEFAULT NULL,
        in_position                VARCHAR2 DEFAULT NULL,
        in_create_on_sort_loc      VARCHAR2 DEFAULT NULL,
        in_sortation_location      VARCHAR2 DEFAULT NULL,
        in_sortation_putaway       VARCHAR2 DEFAULT NULL,
        in_no_put_empty_pallet     VARCHAR2 DEFAULT NULL,
        in_stack                   VARCHAR2 DEFAULT NULL,
        in_no_new_empty_pallet     VARCHAR2 DEFAULT NULL,
        in_total_qty_stockcheck    VARCHAR2 DEFAULT NULL,
        in_aisle_side              VARCHAR2 DEFAULT NULL,
        in_is_sortation_location   VARCHAR2 DEFAULT NULL,
        in_orientation             INTEGER DEFAULT NULL,
        in_rail_id                 VARCHAR2 DEFAULT NULL,
        in_locality                VARCHAR2 DEFAULT NULL,
        in_ignore_volume_weight    VARCHAR2 DEFAULT NULL,
        in_exclude_stock_checks    VARCHAR2 DEFAULT NULL,
        in_move_task_status        VARCHAR2 DEFAULT NULL,
        in_automation_id           VARCHAR2 DEFAULT NULL,
        in_nls_calendar            VARCHAR2 DEFAULT NULL,
        in_tag_merge               VARCHAR2 DEFAULT NULL,
        in_max_units_on_loc        INTEGER DEFAULT NULL,
        in_move_whole_endpoint     VARCHAR2 DEFAULT NULL,
        in_allow_pick_steal        VARCHAR2 DEFAULT NULL,
        in_user_id                 VARCHAR2 DEFAULT 'API-INT',
        in_station_id              VARCHAR2 DEFAULT 'API-INT',
        in_record_error_in_table   VARCHAR2 DEFAULT NULL
    ) RETURN VARCHAR2;

--

    FUNCTION JSON_ERROR(
        p_label VARCHAR2
    ) RETURN VARCHAR2;
    
--
    function interface_relocation_task
    (
        in_update_columns varchar2 default null,
        in_merge_action varchar2 default 'A',
        in_to_loc_id varchar2,
        in_tag_id varchar2,
        in_sku_id varchar2,
        in_pallet_id varchar2 default null,
        in_from_loc_id varchar2,
        in_client_id varchar2,
        in_site_id varchar2,
        in_quantity number,
        in_move_if_allocated varchar2 default 'N',
        in_move_task_status varchar2 default 'Released',
        in_find_location varchar2 default 'N',
        in_disallow_tag_swap varchar2 default null,
        in_timezone_name varchar2 default null,
        in_user_id varchar2 default 'API-INT',
        in_station_id varchar2 default 'API-INT',
        in_record_error_in_table varchar2 default null
    )
    return varchar2;


--
END tq_interface;

/

  GRANT EXECUTE ON "TORQUE"."TQ_INTERFACE" TO "DCSDBA";
--------------------------------------------------------
--  DDL for Package TQ_HOUSEKEEPING
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "TORQUE"."TQ_HOUSEKEEPING" AS 

/***********************************************************************************************************************/ 

/*                                                                                                                     */ 

/*  NAME:               TQ_HOUSEKEEPING                                                                                */ 

/*                                                                                                                     */ 

/*  DESCRIPTION:        To Store All Custom Functionality Related To System Housekeeping                               */ 

/*                                                                                                                     */ 

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */ 

/*  =======     ===========     ==     ===================================================================             */ 

/*    1         2019-07-29      KS     Initial Version                                                                 */ 

/*    2         2019-07-30      KS     Added Account Seurity                                                           */ 

/*    3         2019-08-07      KS     Modified Account Seurity                                                        */ 

/*    4         2019-08-12      KS     Improved Loop Logic                                                             */ 

/*    5         2019-08-21      KS     Added Order Type to GDPR Compliance                                             */ 

/*    6         2019-09-19      KS     Added Custom ITL Archive Purging Procedure                                      */ 

/*                                                                                                                     */ 

/***********************************************************************************************************************/  

/**********************GRANTS**************************

grant execute on TQ_HOUSEKEEPING to DCSDBA; 

******************************************************/

/******************REQUIRED GRANTS*********************

grant select on pre_advice_header to TORQUE;
grant select on order_header to TORQUE;
grant select on order_header_archive to TORQUE; 
grant select on shipping_manifest to TORQUE;  
grant select on inventory_transaction_archive to TORQUE; 
grant select on purge_setup to TORQUE; 
grant select on application_user to TORQUE; 

grant update on shipping_manifest to TORQUE;
grant update on pre_advice_header to TORQUE;
grant update on order_header to TORQUE; 
grant update on order_header_archive to TORQUE; 
grant update on application_user to TORQUE; 

grant delete on inventory_transaction_archive to TORQUE; 

grant execute on liberror to TORQUE;

******************************************************/

    PROCEDURE tq_update_uploaded; 

-- 

    PROCEDURE tq_gdpr_compliance; 

--

    PROCEDURE tq_account_security;
    
-- 
    
    PROCEDURE tq_purge_itl_archive(
        p_dstamp IN VARCHAR2 DEFAULT 'PURGE',
        p_commit_every IN NUMBER DEFAULT 5000
    );

END tq_housekeeping;

/

  GRANT EXECUTE ON "TORQUE"."TQ_HOUSEKEEPING" TO "DCSDBA";
--------------------------------------------------------
--  DDL for Package TQ_FUNCTIONS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "TORQUE"."TQ_FUNCTIONS" AS 

/***********************************************************************************************************************/ 

/*                                                                                                                     */ 

/*  NAME:               TQ_FUNCTIONS                                                                                   */ 

/*                                                                                                                     */ 

/*  DESCRIPTION:        To Store All Torque Functions Not Related Directly To WMS Process                              */ 

/*                                                                                                                     */ 

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */ 

/*  =======     ===========     ==     ===================================================================             */ 

/*    1         2019-09-05      KS     Initial Version                                                                 */ 

/*    2         2019-09-27      KS     Added TRIGGER_SHELL_SCRIPT Function To Package                                  */ 

/*    3         2019-11-22      KS     Added REPACK_CONTAINER Function To Package                                      */ 

/*    4         2020-01-29      KS     Added REMOVE_LIST_DUPLICATES Function To Package                                */

/*    4         2020-01-30      KS     Amended REMOVE_LIST_DUPLICATES Function                                         */

/*    5         2020-08-05      CH     Added  for repacking for MW                                                     */

/*    6         2020-12-16      CH     Added tq_is_order_complete                                                      */

/*                                                                                                                     */ 

/***********************************************************************************************************************/  

/**********************GRANTS**************************

grant execute on TQ_FUNCTIONS to DCSDBA; 
grant execute on TQ_FUNCTIONS to DCSRPT; 

******************************************************/

/******************REQUIRED GRANTS*********************

grant select on run_task_pk_seq to TORQUE;
grant select on move_task to TORQUE;

grant insert on run_task to TORQUE;

grant update on move_task to TORQUE;
grant update on shipping_manifest to TORQUE;
grant update on inventory to TORQUE;

grant execute on liborderrepack to TORQUE;
grant execute on liberror to TORQUE;

******************************************************/
    FUNCTION tq_sequence (
        p_client_id    VARCHAR2,
        p_type         VARCHAR2,
        p_update       NUMBER DEFAULT 1,
        p_fullstring   NUMBER DEFAULT 1
    ) RETURN VARCHAR2;

-- 

    FUNCTION trigger_shell_script (
        p_script_path   VARCHAR2,
        p_parameters    VARCHAR2,
        p_name          VARCHAR2,
        p_user_id       VARCHAR2,
        p_station_id    VARCHAR2
    ) RETURN NUMBER;

--

    FUNCTION repack_container (
        p_site_id        VARCHAR2,
        p_client_id      VARCHAR2,
        p_container_id   VARCHAR2,
        p_order_id       VARCHAR2,
        p_to_pallet_id   VARCHAR2,
        p_autoship       NUMBER
    ) RETURN NUMBER;

--

    FUNCTION remove_list_duplicates (
        p_string     VARCHAR2,
        p_splitter   VARCHAR2
    ) RETURN VARCHAR2;
    
--

     function tq_bulk_relocate(
        p_SiteId varchar2, 
        p_ClientId varchar2, 
        p_FromLoc varchar2, 
        p_ToLoc varchar2, 
        p_UserId varchar2, 
        p_Workstation varchar2, 
        p_EmptyOnly char default 'N',
        p_Sku varchar2 default '*'
) return varchar2;

--

function TQ_PALLET_REPACK
(
    p_Tracking varchar2,
    p_To varchar2,
    p_Client varchar2,
    p_User varchar2,
    p_Station varchar2
) return varchar2;

---------

procedure TQ_ADD_ITL_PROC
(
is_success out number,
in_code varchar2,
in_site_id varchar2 default null,
in_from_site_id varchar2 default null,
in_to_site_id varchar2 default null,
in_from_loc_id varchar2 default null,
in_to_loc_id varchar2 default null,
in_final_loc_id varchar2 default null,
in_ship_dock varchar2 default null,
in_dock_door_id varchar2 default null,
in_owner_id varchar2 default 'TORQUE',
in_client_id varchar2 default null,
in_sku_id varchar2 default null,
in_config_id varchar2 default null,
in_tag_id varchar2 default null,
in_container_id varchar2 default null,
in_pallet_id varchar2 default null,
in_batch_id varchar2 default null,
in_qc_status varchar2 default null,
in_expiry_dstamp timestamp with local time zone default null,
in_manuf_dstamp  timestamp with local time zone default null,
in_origin_id varchar2 default null,
in_condition_id varchar2 default null,
in_spec_code varchar2  default null,
in_lock_status varchar2 default null,
in_list_id varchar2 default null,
in_dstamp timestamp with local time zone default sysdate,
in_work_group varchar2 default null,
in_consignment varchar2 default null,
in_supplier_id varchar2 default null,
in_reference_id varchar2 default null,
in_line_id number default null,
in_reason_id varchar2 default null,
in_station_id varchar2,
in_user_id varchar2,
in_group_id varchar2 default null,
in_shift varchar2 default null,
in_update_qty number default 0,
in_original_qty number default null,
in_uploaded varchar2 default 'N',
in_uploaded_ws2pc_id varchar2 default null,
in_uploaded_filename varchar2 default null,
in_uploaded_dstamp timestamp with local time zone default null,
in_uploaded_ab varchar2 default null,
in_uploaded_tm varchar2 default null,
in_uploaded_vview varchar2 default null,
in_session_type varchar2 default null,
in_summary_record varchar2 default null,
in_elapsed_time number default null,
in_estimated_time number default null,
in_sap_idoc_type varchar2 default null,
in_sap_tid varchar2 default null,
in_task_category number default null,
in_sampling_type varchar2 default null,
in_job_id varchar2 default null,
in_manning number default null,
in_job_unit varchar2 default null,
in_complete_dstamp timestamp with local time zone default null,
in_grn number default null,
in_notes varchar2 default null,
in_user_def_type_1 varchar2 default null,
in_user_def_type_2 varchar2 default null,
in_user_def_type_3 varchar2 default null,
in_user_def_type_4 varchar2 default null,
in_user_def_type_5 varchar2 default null,
in_user_def_type_6 varchar2 default null,
in_user_def_type_7 varchar2 default null,
in_user_def_type_8 varchar2 default null,
in_user_def_chk_1 varchar2 default null,
in_user_def_chk_2 varchar2 default null,
in_user_def_chk_3 varchar2 default null,
in_user_def_chk_4 varchar2 default null,
in_user_def_date_1 timestamp with local time zone default null,
in_user_def_date_2 timestamp with local time zone default null,
in_user_def_date_3 timestamp with local time zone default null,
in_user_def_date_4 timestamp with local time zone default null,
in_user_def_num_1 number default null,
in_user_def_num_2 number default null,
in_user_def_num_3 number default null,
in_user_def_num_4 number default null,
in_user_def_note_1 varchar2 default null,
in_user_def_note_2 varchar2 default null,
in_old_user_def_type_1 varchar2 default null,
in_old_user_def_type_2 varchar2 default null,
in_old_user_def_type_3 varchar2 default null,
in_old_user_def_type_4 varchar2 default null,
in_old_user_def_type_5 varchar2 default null,
in_old_user_def_type_6 varchar2 default null,
in_old_user_def_type_7 varchar2 default null,
in_old_user_def_type_8 varchar2 default null,
in_old_user_def_chk_1 varchar2 default null,
in_old_user_def_chk_2 varchar2 default null,
in_old_user_def_chk_3 varchar2 default null,
in_old_user_def_chk_4 varchar2 default null,
in_old_user_def_date_1 timestamp with local time zone default null,
in_old_user_def_date_2 timestamp with local time zone default null,
in_old_user_def_date_3 timestamp with local time zone default null,
in_old_user_def_date_4 timestamp with local time zone default null,
in_old_user_def_num_1 number default null,
in_old_user_def_num_2 number default null,
in_old_user_def_num_3 number default null,
in_old_user_def_num_4 number default null,
in_old_user_def_note_1 varchar2 default null,
in_old_user_def_note_2 varchar2 default null,
in_ce_orig_rotation_id varchar2 default null,
in_ce_rotation_id varchar2 default null,
in_ce_consignment_id varchar2 default null,
in_ce_receipt_type varchar2 default null,
in_ce_originator varchar2 default null,
in_ce_originator_reference varchar2 default null,
in_ce_coo varchar2 default null,
in_ce_cwc varchar2 default null,
in_ce_ucr varchar2 default null,
in_ce_under_bond varchar2 default null,
in_ce_document_dstamp timestamp with local time zone default null,
in_ce_colli_count number default null,
in_ce_colli_count_expected number default null,
in_ce_seals_ok varchar2 default null,
in_uploaded_customs varchar2 default null,
in_uploaded_labor varchar2 default null,
in_print_label_id number default null,
in_lock_code varchar2 default null,
in_asn_id varchar2 default null,
in_customer_id varchar2 default null,
in_ce_duty_stamp varchar2 default null,
in_pallet_grouped varchar2 default null,
in_consol_link number default null,
in_job_site_id varchar2 default null,
in_job_client_id varchar2 default null,
in_tracking_level varchar2 default null,
in_extra_notes varchar2 default null,
in_stage_route_id varchar2 default null,
in_stage_route_sequence number default null,
in_pf_consol_link number default null,
in_master_pah_id varchar2 default null,
in_master_pal_id number default null,
in_archived varchar2 default null,
in_shipment_number number default null,
in_customer_shipment_number number default null,
in_pallet_config varchar2 default null,
in_container_type varchar2 default null,
in_ce_avail_status varchar2 default null,
in_from_status varchar2 default null,
in_to_status varchar2 default null,
in_kit_plan_id varchar2 default null,
in_plan_sequence number default null,
in_master_order_id varchar2 default null,
in_master_order_line_id number default null,
in_ce_invoice_number varchar2 default null,
in_rdt_user_mode varchar2 default null,
in_labor_assignment number default null,
in_grid_pick varchar2 default null,
in_labor_grid_sequence number default null,
in_kit_ce_consignment_id varchar2 default null
);

------

function TQ_PALLET_REPACK_V2
(
    p_From varchar2,
    p_To varchar2,
    p_Client varchar2,
    p_TrailerLoad char default 'N',
    p_User varchar2,
    p_Station varchar2
) return varchar2;

---

function tq_is_order_complete
(
    p_Client varchar2,
    p_Order varchar2
) return number;

---

END tq_functions;

/

  GRANT EXECUTE ON "TORQUE"."TQ_FUNCTIONS" TO "DCSRPT";
  GRANT EXECUTE ON "TORQUE"."TQ_FUNCTIONS" TO "DCSDBA";
--------------------------------------------------------
--  DDL for Package TQ_BULK_PICK
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "TORQUE"."TQ_BULK_PICK" AS 


/***********************************************************************************************************************/ 

/*                                                                                                                     */ 

/*  NAME:               TQ_BULK_PICK                                                                                   */ 

/*                                                                                                                     */ 

/*  DESCRIPTION:        To Handle the Bulk Picking / Sortation Functionality                                           */ 

/*                                                                                                                     */ 

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */ 

/*  =======     ===========     ==     ===================================================================             */ 

/*    1         2019-07-12      KS     Initial Version                                                                 */ 

/*    2         2019-07-22		KS	   Modified To Support 2 Stage SORT	                                               */

/*    3         2019-07-22		KS     Added PROCESS_STOCK_DIRECT Function	                                           */

/*    4         2019-09-27      KS     Reworked SORTATION_WALL package and renamed to TQ_SORTATION_WALL                */

/*    5         2019-10-01      KS     Reworked Search_Location Function                                               */

/*    6         2019-10-08      KS     Modified PROCESS_STOCK Function                                                 */

/*    7         2019-11-11      KS     Modified PROCESS_STOCK_OUT to ensure TO_LOCATION IS CORRECT                     */

/*    8         2020-02-05      KS     Modified PROCESS_STOCK AND PROCESS_STOCK_OUT to no longer require client/site   */

/*    9         2020-02-26      KS     Renamed TQ_SORTATION_WALL to TQ_BULK_PICK to make changes for new Clients       */

/*    10        2020-02-26      KS     Added SET_STAGE_ROUTE Function and removed 2 stage sort from SEARCH_LOCATION    */

/*                                                                                                                     */ 

/***********************************************************************************************************************/ 

/**********************GRANTS**************************

grant execute on TQ_BULK_PICK to DCSDBA; 

******************************************************/

/******************REQUIRED GRANTS*********************

grant select on move_task to TORQUE;
grant select on location to TORQUE;

grant update on move_task to TORQUE;
grant update on order_header to TORQUE;

grant insert on move_task to TORQUE;

grant execute on liberror to TORQUE;


/********************DEPENDENCIES***********************

Packages:
    RDTBUFFER

******************************************************/

-- 
    FUNCTION search_location (
        p_site_id       IN   VARCHAR2,
        p_client_id     IN   VARCHAR2,
        p_user_id       IN   VARCHAR2,
        p_station_id    IN   VARCHAR2,
        p_from_loc_id   IN   VARCHAR2,
        p_sku_id        IN   VARCHAR2
    ) RETURN VARCHAR2; 

-- 

    FUNCTION process_stock (
        p_user_id      IN   VARCHAR2,
        p_station_id   IN   VARCHAR2
    ) RETURN NUMBER; 

-- 

    FUNCTION sort_validation (
        p_site_id       VARCHAR2,
        p_client_id     VARCHAR2,
        p_station_id    VARCHAR2,
        p_user_id       VARCHAR2,
        p_location_id   VARCHAR2
    ) RETURN NUMBER;

-- 

    FUNCTION process_stock_out (
        p_station_id   VARCHAR2,
        p_user_id      VARCHAR2
    ) RETURN NUMBER;

-- 

    FUNCTION process_stock_direct (
        p_site_id     VARCHAR2,
        p_client_id   VARCHAR2,
        p_order_id    VARCHAR2
    ) RETURN NUMBER;

--

    FUNCTION set_bulk_pick (
        p_client_id       VARCHAR2,
        p_consignment     VARCHAR2,
        p_ship_location   VARCHAR2,
        p_stage_route_id  VARCHAR2 DEFAULT 'BULK-PICK'
    ) RETURN NUMBER;

END tq_bulk_pick;

/

  GRANT EXECUTE ON "TORQUE"."TQ_BULK_PICK" TO "DCSDBA";
--------------------------------------------------------
--  DDL for Package RDTBUFFER
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "TORQUE"."RDTBUFFER" AS 

/***********************************************************************************************************************/ 

/*                                                                                                                     */ 

/*  NAME:               RDTBUFFER                                                                                      */ 

/*                                                                                                                     */ 

/*  DESCRIPTION:        To Assist With Storing Temporary Data Into A Table Using RDT Rul                               */ 

/*                                                                                                                     */ 

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */ 

/*  =======     ===========     ==     ===================================================================             */ 

/*    1         2019-05-14      KS     Initial Version                                                                 */ 

/*    2         2019-09-30      KS     Added PurgeBufferData Procedure                                                 */ 

/*                                                                                                                     */ 

/***********************************************************************************************************************/  

/**********************GRANTS**************************

grant execute on RDTBUFFER to DCSDBA; 

******************************************************/

/******************REQUIRED GRANTS*********************

******************************************************/


-- 
    FUNCTION setbufferdata (
        p_clientid    IN   VARCHAR2,
        p_userid      IN   VARCHAR2,
        p_stationid   IN    VARCHAR2,
        p_field       IN   VARCHAR2 := NULL,
        p_data        IN   VARCHAR2 := NULL
    ) RETURN INTEGER; 

-- 

    FUNCTION readbufferdata (
        p_clientid    IN    VARCHAR2,
        p_userid      IN   VARCHAR2,
        p_stationid   IN   VARCHAR2,
        p_field       IN   VARCHAR2 := NULL
    ) RETURN VARCHAR2; 

-- 

    FUNCTION deletebufferdata (
        p_clientid    IN   VARCHAR2,
        p_userid      IN   VARCHAR2,
        p_stationid   IN   VARCHAR2,
        p_field       IN   VARCHAR2 := NULL
    ) RETURN INTEGER; 

--

    PROCEDURE purgebufferdata (
        p_age        IN   NUMBER DEFAULT 7
        );

--

    FUNCTION clearallbuffer (
        p_userid      IN   VARCHAR2,
        p_stationid   IN    VARCHAR2
    ) RETURN INTEGER; 

END rdtbuffer;

/

  GRANT EXECUTE ON "TORQUE"."RDTBUFFER" TO "DCSDBA";
--------------------------------------------------------
--  DDL for Package JDA_PICKING
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "TORQUE"."JDA_PICKING" AS 

/***********************************************************************************************************************/ 

/*                                                                                                                     */ 

/*  NAME:               JDA_PICKING                                                                                    */ 

/*                                                                                                                     */ 

/*  DESCRIPTION:        To Store All Custom Functionality Related To Picking                                           */ 

/*                                                                                                                     */ 

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */ 

/*  =======     ===========     ==     ===================================================================             */ 

/*    1         2019-07-23      KS     Initial Version                                                                 */ 

/*    2         2019-10-15      KS     Added SET_REPACK Function                                                       */ 

/*    3         2019-10-24      KS     Added SET_BULK_PICK Function                                                    */ 

/*    4         2019-11-22      KS     Added COMPLETE_KITTING Function                                                 */ 

/*    5         2020-08-10      CH     Added COLL_ORDERTABLE type to return order list                                 */ 

/*    6         2020-08-10      CH     Added TQ_ORDER_GROUPING Function                                                */ 

/*    7         2020-08-12      CH     Added Specific Grouping Rule For MW                                             */ 

/*                                                                                                                     */  

/***********************************************************************************************************************/  

/**********************GRANTS**************************

grant execute on JDA_PICKING to DCSDBA;   
grant execute on JDA_PICKING to DCSRPT;

******************************************************/

/******************REQUIRED GRANTS*********************

grant select on location to TORQUE;
grant select on move_task to TORQUE;
grant select on sku to TORQUE;
grant select on move_task to TORQUE;
grant select on order_line to TORQUE;

grant update on move_task to TORQUE;
grant update on order_header to TORQUE;

grant execute on libsession to TORQUE;
grant execute on libkitting to TORQUE;
grant execute on liberror to TORQUE;

******************************************************/

    FUNCTION re_sort_tasks (
        p_client_id   IN   VARCHAR2,
        p_order_id    IN   VARCHAR2,
        p_sort_by     IN   VARCHAR2
    ) RETURN VARCHAR2; 

-- 

    FUNCTION complete_kitting (
        p_site_id        VARCHAR2,
        p_client_id      VARCHAR2,
        p_order_id       VARCHAR2,
        p_kit_location   VARCHAR2,
        p_user_id        VARCHAR2,
        p_station_id     VARCHAR2
    ) RETURN NUMBER;

--

    FUNCTION set_bulk_pick (
        p_client_id       VARCHAR2,
        p_consignment     VARCHAR2,
        p_ship_location   VARCHAR2
    ) RETURN NUMBER;


--

    FUNCTION set_repack (
        p_client_id       IN   VARCHAR2,
        p_consignment     IN   VARCHAR2,
        p_ship_location   IN   VARCHAR2,
        p_pack_location   IN   VARCHAR2
    ) RETURN NUMBER;
    
--

    function TQ_ORDER_GROUPING
    (
        p_Client varchar2,
        p_MinUnits number,
        p_MaxUnits number,
        p_ConsigsToBeCreated number,
        p_OrdersPerConsig number,
        p_MinOrdersReqPerConsig number,
        p_OrderTypes varchar2,
        p_WorkZoneCount number default 0,
        p_WorkZones varchar2 default '',
        p_CustomWhereSQL varchar2 default '',
        p_CustomHavingSQL varchar2 default '',
        p_UpdateTable varchar2 default '',
        p_UpdateSQL varchar2 default '',
        p_OrderByOpt number default 0
    )
    return coll_Orders;

--

    function TQ_MW_SORTER_BATCH_GROUPING (
        p_Phase2MinUnits int,
        p_Phase2MaxUnits int,
        p_Phase2Orders int,
        p_StandardMinUnits int,
        p_StandardMaxUnits int,
        p_StandardOrders int,
        p_LargeOrders int,
        p_UglyOrders int,
        p_IncludeSingles varchar2,
        p_PriorityOnly char default 'N' 
    ) return coll_Orders;

--

END jda_picking;

/

  GRANT EXECUTE ON "TORQUE"."JDA_PICKING" TO "DCSRPT";
  GRANT EXECUTE ON "TORQUE"."JDA_PICKING" TO "DCSDBA";
--------------------------------------------------------
--  DDL for Package JDA_INBOUND
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "TORQUE"."JDA_INBOUND" AS 

/***********************************************************************************************************************/ 

/*                                                                                                                     */ 

/*  NAME:               JDA_INBOUND                                                                                    */ 

/*                                                                                                                     */ 

/*  DESCRIPTION:        To Store All Custom Functionality Related To Inbound                                           */ 

/*                                                                                                                     */ 

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */ 

/*  =======     ===========     ==     ===================================================================             */ 

/*    1         2019-08-05      KS     Initial Version                                                                 */ 

/*    2         2019-08-15      KS     Added PUTAWAY_RECREATE Function                                                 */ 

/*    3         2019-08-22      KS     Added FIND_TAG Function                                                         */ 

/*    4         2019-09-20      KS     Updated PUTAWAY_RECREATE Function                                               */ 

/*    5         2019-09-23      KS     Added FIND_RETURN_LOCATION Function                                             */

/*    6         2019-09-24      KS     Updated FIND_RETURN_LOCATION + PUTAWAY_RECREATE Function                        */

/*    7         2019-09-25      KS     Added RETURN_API Function                                                       */

/*    7         2019-10-08      KS     Modified RETURN_API Function                                                    */

/*    8         2020-04-14      KS     Modified RETURN_API Function To Support Scrapping                               */

/*    9         2020-12-15      CH     Modified RETURN_API Function To Update ITLs UDT1 with COO                       */

/*                                                                                                                     */ 

/***********************************************************************************************************************/ 

/**********************GRANTS**************************

grant execute on JDA_INBOUND to DCSDBA; 

******************************************************/

/******************REQUIRED GRANTS*********************

grant select on sku to TORQUE;
grant select on supplier_sku to TORQUE;
grant select on move_task_pk_seq to TORQUE;
grant select on move_task to TORQUE;
grant select on inventory to TORQUE;
grant select on location to TORQUE;   
grant select on inventory_transaction to TORQUE;
grant select on language_text to TORQUE;      
grant select on sampling_route_action to TORQUE;

grant update on supplier_sku to TORQUE;
grant update on inventory to TORQUE;
grant update on inventory_transaction to TORQUE;
grant update on order_line to TORQUE;
grant update on order_line_archive to TORQUE;

grant insert on supplier_sku to TORQUE;
grant insert on move_task to TORQUE;  

grant delete on supplier_sku to TORQUE;
grant delete on move_task to TORQUE;

grant execute on liberror to TORQUE;  
grant execute on libdatacheck to TORQUE;
grant execute on libmovetask to TORQUE;
grant execute on libsession to TORQUE;
grant execute on libmergereceipt to TORQUE;
grant execute on LIBSTOCKADJUST to TORQUE;

******************************************************/

    FUNCTION capture_supplier_sku (
        p_client_id         VARCHAR2,
        p_sku_id            VARCHAR2,
        p_supplier_sku_id   VARCHAR2,
        p_single_record     NUMBER
    ) RETURN NUMBER; 

-- 

    FUNCTION putaway_recreate (
        p_site_id        VARCHAR2,
        p_client_id      VARCHAR2,
        p_tag_id         VARCHAR2,
        p_from_loc_id    VARCHAR2,
        p_final_loc_id   VARCHAR2 DEFAULT NULL,
        p_qty            NUMBER DEFAULT 0
    ) RETURN NUMBER; 

-- 

    FUNCTION find_tag (
        p_site_id       VARCHAR2,
        p_client_id     VARCHAR2,
        p_sku_id        VARCHAR2,
        p_location_id   VARCHAR2
    ) RETURN VARCHAR2;

--

    FUNCTION find_return_location (
        p_station_id    VARCHAR2,
        p_user_id       VARCHAR2,
        p_site_id       VARCHAR2,
        p_client_id     VARCHAR2,
        p_zone_1        VARCHAR2,
        p_tag_id        VARCHAR2,
        p_sku_id        VARCHAR2,
        p_location_id   VARCHAR2,
        p_level_ex      VARCHAR2
    ) RETURN NUMBER;

--

    FUNCTION return_api (
        p_site_id         VARCHAR2,
        p_client_id       VARCHAR2,
        p_owner_id        VARCHAR2,
        p_line_id         NUMBER,
        p_sku_id          VARCHAR2,
        p_qty             NUMBER,
        p_location_id     VARCHAR2,
        p_order_id        VARCHAR2,
        p_condition_id    VARCHAR2,
        p_notes           VARCHAR2,
        p_reason_id       VARCHAR2,
        p_user_id         VARCHAR2 DEFAULT 'COURIERAPI',
        p_station_id      VARCHAR2 DEFAULT 'COURIERAPI',
        p_config_id       VARCHAR2 DEFAULT 'P1',
        p_coo             VARCHAR2 DEFAULT NULL,
        p_pallet_config   VARCHAR2 DEFAULT 'MD'
    ) RETURN VARCHAR2;

--

END jda_inbound;

/

  GRANT EXECUTE ON "TORQUE"."JDA_INBOUND" TO "DCSDBA";
--------------------------------------------------------
--  DDL for Package Body TQ_INTERFACE
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "TORQUE"."TQ_INTERFACE" AS 
/***********************************************************************************************************************/

/*                                                                                                                     */

/*  NAME:               TQ_INTERFACE.INTERFACE_SKU                                                                     */

/*                                                                                                                     */

/*  DESCRIPTION:        This Function Uses The Interface SKU API to Add SKUS to the System                             */

/*                                                                                                                     */

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */

/*  =======     ===========     ==     ===================================================================             */

/*    1         2019-10-18      KS     Initial Version                                                                 */

/*    2         2019-11-05      KS     Modified To Delete Older Interface Records For Failed SKUs                      */

/*    3         2020-01-28      KS     Support For Custom User_def_type Fields + Optional Log Error Parameter          */

/*    4         2020-02-20      KS     Support For SKU_EXT Table additional Fields                                     */

/*                                                                                                                     */

/***********************************************************************************************************************/

/**********************GRANTS**************************

grant select on IF_SKU_PK_SEQ TO TORQUE;
grant select on interface_sku to TORQUE;

grant insert on interface_update_columns to TORQUE;
grant insert on interface_sku to TORQUE;

grant update on sku to TORQUE;

grant delete on interface_update_columns to TORQUE;
grant delete on interface_sku to TORQUE;

grant execute on libsession to TORQUE;
grant execute on libmergesku to TORQUE;
grant execute on liberror to TORQUE;


******************************************************/
FUNCTION interface_sku (
  in_merge_action VARCHAR2 DEFAULT 'U', 
  in_client_id VARCHAR2, 
  in_sku_id VARCHAR2, 
  in_ean VARCHAR2 DEFAULT NULL, 
  in_upc VARCHAR2 DEFAULT NULL, 
  in_description VARCHAR2, 
  in_product_group VARCHAR2 DEFAULT NULL, 
  in_each_height NUMBER DEFAULT NULL, 
  in_each_weight NUMBER DEFAULT NULL, 
  in_each_volume NUMBER DEFAULT NULL, 
  in_each_value NUMBER DEFAULT NULL, 
  in_each_quantity NUMBER DEFAULT NULL, 
  in_qc_status VARCHAR2 DEFAULT NULL, 
  in_shelf_life NUMBER DEFAULT NULL, 
  in_qc_frequency NUMBER DEFAULT NULL, 
  in_split_lowest VARCHAR2 DEFAULT NULL, 
  in_condition_reqd VARCHAR2 DEFAULT NULL, 
  in_expiry_reqd VARCHAR2 DEFAULT NULL, 
  in_origin_reqd VARCHAR2 DEFAULT NULL, 
  in_serial_at_pack VARCHAR2 DEFAULT NULL, 
  in_serial_at_pick VARCHAR2 DEFAULT NULL, 
  in_serial_at_receipt VARCHAR2 DEFAULT NULL, 
  in_serial_range VARCHAR2 DEFAULT NULL, 
  in_serial_format VARCHAR2 DEFAULT NULL, 
  in_serial_valid_merge VARCHAR2 DEFAULT NULL, 
  in_serial_no_reuse VARCHAR2 DEFAULT NULL, 
  in_serial_per_each NUMBER DEFAULT NULL, 
  in_pick_sequence NUMBER DEFAULT NULL, 
  in_pick_count_qty NUMBER DEFAULT NULL, 
  in_count_frequency NUMBER DEFAULT NULL, 
  in_oain_wiin_enabled VARCHAR2 DEFAULT NULL, 
  in_kit_sku VARCHAR2 DEFAULT NULL, 
  in_kit_split VARCHAR2 DEFAULT NULL, 
  in_kit_trigger_qty NUMBER DEFAULT NULL, 
  in_kit_qty_due NUMBER DEFAULT NULL, 
  in_kitting_loc_id VARCHAR2 DEFAULT NULL, 
  in_allocation_group VARCHAR2 DEFAULT NULL, 
  in_putaway_group VARCHAR2 DEFAULT NULL, 
  in_abc_disable VARCHAR2 DEFAULT NULL, 
  in_handling_class VARCHAR2 DEFAULT NULL, 
  in_obsolete_product VARCHAR2 DEFAULT NULL, 
  in_new_product VARCHAR2 DEFAULT NULL, 
  in_disallow_upload VARCHAR2 DEFAULT NULL, 
  in_disallow_cross_dock VARCHAR2 DEFAULT NULL, 
  in_manuf_dstamp_reqd VARCHAR2 DEFAULT NULL, 
  in_manuf_dstamp_flt VARCHAR2 DEFAULT NULL, 
  in_min_shelf_life NUMBER DEFAULT NULL, 
  in_colour VARCHAR2 DEFAULT NULL, 
  in_sku_size VARCHAR2 DEFAULT NULL, 
  in_hazmat VARCHAR2 DEFAULT NULL, 
  in_hazmat_id VARCHAR2 DEFAULT NULL, 
  in_ship_shelf_life NUMBER DEFAULT NULL, 
  in_nmfc_number NUMBER DEFAULT NULL, 
  in_incub_rule VARCHAR2 DEFAULT NULL, 
  in_incub_hours NUMBER DEFAULT NULL, 
  in_each_width NUMBER DEFAULT NULL, 
  in_each_depth NUMBER DEFAULT NULL, 
  in_reorder_trigger_qty NUMBER DEFAULT NULL, 
  in_low_trigger_qty NUMBER DEFAULT NULL, 
  in_disallow_merge_rules VARCHAR2 DEFAULT NULL, 
  in_pack_despatch_repack VARCHAR2 DEFAULT NULL, 
  in_spec_id VARCHAR2 DEFAULT NULL, 
  in_user_def_type_1 VARCHAR2 DEFAULT NULL, 
  in_user_def_type_2 VARCHAR2 DEFAULT NULL, 
  in_user_def_type_3 VARCHAR2 DEFAULT NULL, 
  in_user_def_type_4 VARCHAR2 DEFAULT NULL, 
  in_user_def_type_5 VARCHAR2 DEFAULT NULL, 
  in_user_def_type_6 VARCHAR2 DEFAULT NULL, 
  in_user_def_type_7 VARCHAR2 DEFAULT NULL, 
  in_user_def_type_8 VARCHAR2 DEFAULT NULL, 
  in_user_def_chk_1 VARCHAR2 DEFAULT NULL, 
  in_user_def_chk_2 VARCHAR2 DEFAULT NULL, 
  in_user_def_chk_3 VARCHAR2 DEFAULT NULL, 
  in_user_def_chk_4 VARCHAR2 DEFAULT NULL, 
  in_user_def_date_1 VARCHAR2 DEFAULT NULL, 
  in_user_def_date_2 VARCHAR2 DEFAULT NULL, 
  in_user_def_date_3 VARCHAR2 DEFAULT NULL, 
  in_user_def_date_4 VARCHAR2 DEFAULT NULL, 
  in_user_def_num_1 NUMBER DEFAULT NULL, 
  in_user_def_num_2 NUMBER DEFAULT NULL, 
  in_user_def_num_3 NUMBER DEFAULT NULL, 
  in_user_def_num_4 NUMBER DEFAULT NULL, 
  in_user_def_note_1 VARCHAR2 DEFAULT NULL, 
  in_user_def_note_2 VARCHAR2 DEFAULT NULL, 
  in_beam_units NUMBER DEFAULT NULL, 
  in_ce_warehouse_type VARCHAR2 DEFAULT NULL, 
  in_ce_customs_excise VARCHAR2 DEFAULT NULL, 
  in_ce_standard_cost NUMBER DEFAULT NULL, 
  in_ce_standard_currency VARCHAR2 DEFAULT NULL, 
  in_disallow_clustering VARCHAR2 DEFAULT NULL, 
  in_max_stack NUMBER DEFAULT NULL, 
  in_stack_description VARCHAR2 DEFAULT NULL, 
  in_stack_limitation NUMBER DEFAULT NULL, 
  in_ce_duty_stamp VARCHAR2 DEFAULT NULL, 
  in_capture_weight VARCHAR2 DEFAULT NULL, 
  in_weigh_at_receipt VARCHAR2 DEFAULT NULL, 
  in_upper_weight_tolerance NUMBER DEFAULT NULL, 
  in_lower_weight_tolerance NUMBER DEFAULT NULL, 
  in_serial_at_loading VARCHAR2 DEFAULT NULL, 
  in_serial_at_kitting VARCHAR2 DEFAULT NULL, 
  in_serial_at_unkitting VARCHAR2 DEFAULT NULL, 
  in_ce_commodity_code VARCHAR2 DEFAULT NULL, 
  in_ce_coo VARCHAR2 DEFAULT NULL, 
  in_ce_cwc VARCHAR2 DEFAULT NULL, 
  in_ce_vat_code VARCHAR2 DEFAULT NULL, 
  in_ce_product_type VARCHAR2 DEFAULT NULL, 
  in_commodity_code VARCHAR2 DEFAULT NULL, 
  in_commodity_desc VARCHAR2 DEFAULT NULL, 
  in_family_group VARCHAR2 DEFAULT NULL, 
  in_breakpack VARCHAR2 DEFAULT NULL, 
  in_clearable VARCHAR2 DEFAULT NULL, 
  in_stage_route_id VARCHAR2 DEFAULT NULL, 
  in_serial_dynamic_range VARCHAR2 DEFAULT NULL, 
  in_serial_max_range NUMBER DEFAULT NULL, 
  in_manufacture_at_repack VARCHAR2 DEFAULT NULL, 
  in_expiry_at_repack VARCHAR2 DEFAULT NULL, 
  in_udf_at_repack VARCHAR2 DEFAULT NULL, 
  in_repack_by_piece VARCHAR2 DEFAULT NULL, 
  in_packed_height NUMBER DEFAULT NULL, 
  in_packed_width NUMBER DEFAULT NULL, 
  in_packed_depth NUMBER DEFAULT NULL, 
  in_packed_volume NUMBER DEFAULT NULL, 
  in_packed_weight NUMBER DEFAULT NULL, 
  in_hanging_garment VARCHAR2 DEFAULT NULL, 
  in_conveyable VARCHAR2 DEFAULT NULL, 
  in_fragile VARCHAR2 DEFAULT NULL, 
  in_gender VARCHAR2 DEFAULT NULL, 
  in_high_security VARCHAR2 DEFAULT NULL, 
  in_ugly VARCHAR2 DEFAULT NULL, 
  in_collatable VARCHAR2 DEFAULT NULL, 
  in_ecommerce VARCHAR2 DEFAULT NULL, 
  in_promotion VARCHAR2 DEFAULT NULL, 
  in_foldable VARCHAR2 DEFAULT NULL, 
  in_style VARCHAR2 DEFAULT NULL, 
  in_business_unit_code VARCHAR2 DEFAULT NULL, 
  in_two_man_lift VARCHAR2 DEFAULT NULL, 
  in_awkward VARCHAR2 DEFAULT NULL, 
  in_decatalogued VARCHAR2 DEFAULT NULL, 
  in_stock_check_rule_id VARCHAR2 DEFAULT NULL, 
  in_unkitting_inherit VARCHAR2 DEFAULT NULL, 
  in_serial_at_stock_check VARCHAR2 DEFAULT NULL, 
  in_serial_at_stock_adjust VARCHAR2 DEFAULT NULL, 
  in_kit_ship_components VARCHAR2 DEFAULT NULL, 
  in_unallocatable VARCHAR2 DEFAULT NULL, 
  in_batch_at_kitting VARCHAR2 DEFAULT NULL, 
  in_collective_mode VARCHAR2 DEFAULT NULL, 
  in_collective_sequence NUMBER DEFAULT NULL, 
  in_vmi_allow_allocation VARCHAR2 DEFAULT NULL, 
  in_vmi_allow_replenish VARCHAR2 DEFAULT NULL, 
  in_vmi_allow_manual VARCHAR2 DEFAULT NULL, 
  in_vmi_allow_interfaced VARCHAR2 DEFAULT NULL, 
  in_vmi_overstock_qty NUMBER DEFAULT NULL, 
  in_vmi_aging_days NUMBER DEFAULT NULL, 
  in_scrap_on_return VARCHAR2 DEFAULT NULL, 
  in_harmonised_product_code VARCHAR2 DEFAULT NULL, 
  in_tag_merge VARCHAR2 DEFAULT NULL, 
  in_carrier_pallet_mixing VARCHAR2 DEFAULT NULL, 
  in_special_container_type VARCHAR2 DEFAULT NULL, 
  in_disallow_over_picking VARCHAR2 DEFAULT NULL, 
  in_no_alloc_backorder VARCHAR2 DEFAULT NULL, 
  in_return_min_shelf_life NUMBER DEFAULT NULL, 
  in_weigh_at_grid_pick VARCHAR2 DEFAULT NULL, 
  in_ce_excise_product_code VARCHAR2 DEFAULT NULL, 
  in_ce_degree_plato NUMBER DEFAULT NULL, 
  in_ce_designation_origin VARCHAR2 DEFAULT NULL, 
  in_ce_density NUMBER DEFAULT NULL, 
  in_ce_brand_name VARCHAR2 DEFAULT NULL, 
  in_ce_alcoholic_strength NUMBER DEFAULT NULL, 
  in_ce_fiscal_mark VARCHAR2 DEFAULT NULL, 
  in_ce_size_of_producer VARCHAR2 DEFAULT NULL, 
  in_ce_commercial_desc VARCHAR2 DEFAULT NULL, 
  in_serial_no_outbound VARCHAR2 DEFAULT NULL, 
  in_full_pallet_at_receipt VARCHAR2 DEFAULT NULL, 
  in_always_full_pallet VARCHAR2 DEFAULT NULL, 
  in_sub_within_product_grp VARCHAR2 DEFAULT NULL, 
  in_serial_check_string VARCHAR2 DEFAULT NULL, 
  in_carrier_product_type VARCHAR2 DEFAULT NULL, 
  in_max_pack_configs NUMBER DEFAULT NULL, 
  in_parcel_packing_by_piece VARCHAR2 DEFAULT NULL, 
  in_time_zone_name VARCHAR2 DEFAULT NULL, 
  in_nls_calendar VARCHAR2 DEFAULT NULL, 
  in_client_group VARCHAR2 DEFAULT NULL, 
  in_update_columns VARCHAR2 DEFAULT NULL, 
  in_user_id VARCHAR2 DEFAULT 'API', 
  in_station_id VARCHAR2 DEFAULT 'API',
  in_user_def_type_9 VARCHAR2 DEFAULT NULL,
  in_user_def_type_10 VARCHAR2 DEFAULT NULL,
  in_user_def_type_11 VARCHAR2 DEFAULT NULL,
  in_user_def_type_12 VARCHAR2 DEFAULT NULL,
  in_user_def_type_13 VARCHAR2 DEFAULT NULL,
  in_user_def_type_14 VARCHAR2 DEFAULT NULL,
  in_user_def_type_15 VARCHAR2 DEFAULT NULL,
  in_user_def_type_16 VARCHAR2 DEFAULT NULL,
  in_user_def_type_17 VARCHAR2 DEFAULT NULL,
  in_user_def_type_18 VARCHAR2 DEFAULT NULL,
  in_user_def_type_19 VARCHAR2 DEFAULT NULL,
  in_user_def_type_20 VARCHAR2 DEFAULT NULL,
  in_user_def_note_3 VARCHAR2 DEFAULT NULL,
  in_user_def_note_4 VARCHAR2 DEFAULT NULL,
  in_user_def_note_5 VARCHAR2 DEFAULT NULL,
  in_user_def_note_6 VARCHAR2 DEFAULT NULL,
  in_sku_aux2 VARCHAR2 DEFAULT NULL,
  in_sku_knit_wove VARCHAR2 DEFAULT NULL,
  in_sku_fabric1 VARCHAR2 DEFAULT NULL,
  in_sku_fabric2 VARCHAR2 DEFAULT NULL,
  in_sku_supplier VARCHAR2 DEFAULT NULL,
  in_record_error_in_table VARCHAR2 DEFAULT NULL
) RETURN VARCHAR2 IS PRAGMA autonomous_transaction;
v_retval NUMBER;
v_error VARCHAR2(40);
v_interface_key NUMBER;
v_user_def_date_1 TIMESTAMP := NULL;
v_user_def_date_2 TIMESTAMP := NULL;
v_user_def_date_3 TIMESTAMP := NULL;
v_user_def_date_4 TIMESTAMP := NULL;

v_update_chk NUMBER;

BEGIN 

dcsdba.libsession.initialisesession(
  in_user_id, 'INT-API', in_station_id, 
  'INT-API'
);
IF in_user_def_date_1 IS NOT NULL THEN v_user_def_date_1 := to_timestamp(
  in_user_def_date_1, 'YYYYMMDDHH24MISS'
);
END IF;
IF in_user_def_date_2 IS NOT NULL THEN v_user_def_date_2 := to_timestamp(
  in_user_def_date_2, 'YYYYMMDDHH24MISS'
);
END IF;
IF in_user_def_date_3 IS NOT NULL THEN v_user_def_date_3 := to_timestamp(
  in_user_def_date_3, 'YYYYMMDDHH24MISS'
);
END IF;
IF in_user_def_date_4 IS NOT NULL THEN v_user_def_date_4 := to_timestamp(
  in_user_def_date_4, 'YYYYMMDDHH24MISS'
);
END IF;
v_retval := dcsdba.libmergesku.directsku(
  p_mergeerror => v_error, 
  p_toupdatecols => in_update_columns, 
  p_mergeaction => in_merge_action, 
  p_clientid => in_client_id, 
  p_skuid => in_sku_id, 
  p_ean => in_ean, 
  p_upc => in_upc, 
  p_description => in_description, 
  p_productgroup => in_product_group, 
  p_eachheight => in_each_height, 
  p_eachweight => in_each_weight, 
  p_eachvolume => in_each_volume, 
  p_eachvalue => in_each_value, 
  p_qcstatus => in_qc_status, 
  p_shelflife => in_shelf_life, 
  p_qcfrequency => in_qc_frequency, 
  p_splitlowest => in_split_lowest, 
  p_conditionreqd => in_condition_reqd, 
  p_expiryreqd => in_expiry_reqd, 
  p_originreqd => in_origin_reqd, 
  p_serialatpack => in_serial_at_pack, 
  p_serialatpick => in_serial_at_pick, 
  p_serialatreceipt => in_serial_at_receipt, 
  p_serialrange => in_serial_range, 
  p_serialformat => in_serial_format, 
  p_serialvalidmerge => in_serial_valid_merge, 
  p_serialnoreuse => in_serial_no_reuse, 
  p_picksequence => in_pick_sequence, 
  p_pickcountqty => in_pick_count_qty, 
  p_countfrequency => in_count_frequency, 
  p_kitsku => in_kit_sku, 
  p_kitsplit => in_kit_split, 
  p_kittinglocid => in_kitting_loc_id, 
  p_allocationgroup => in_allocation_group, 
  p_putawaygroup => in_putaway_group, 
  p_abcdisable => in_abc_disable, 
  p_handlingclass => in_handling_class, 
  p_obsoleteproduct => in_obsolete_product, 
  p_newproduct => in_new_product, 
  p_disallowupload => in_disallow_upload, 
  p_disallowcrossdock => in_disallow_cross_dock, 
  p_manufdstampreqd => in_manuf_dstamp_reqd, 
  p_manufdstampdflt => in_manuf_dstamp_flt, 
  p_minshelflife => in_min_shelf_life, 
  p_colour => in_colour, 
  p_skusize => in_sku_size, 
  p_hazmat => in_hazmat, 
  p_hazmatid => in_hazmat_id, 
  p_shipshelflife => in_ship_shelf_life, 
  p_nmfcnumber => in_nmfc_number, 
  p_incubrule => in_incub_rule, 
  p_incubhours => in_incub_hours, 
  p_eachwidth => in_each_width, 
  p_eachdepth => in_each_depth, 
  p_reordertriggerqty => in_reorder_trigger_qty, 
  p_lowtriggerqty => in_low_trigger_qty, 
  p_disallowmergerules => in_disallow_merge_rules, 
  p_packdespatchrepack => in_pack_despatch_repack, 
  p_specid => in_spec_id, 
  p_userdeftype1 => in_user_def_type_1, 
  p_userdeftype2 => in_user_def_type_2, 
  p_userdeftype3 => in_user_def_type_3, 
  p_userdeftype4 => in_user_def_type_4, 
  p_userdeftype5 => in_user_def_type_5, 
  p_userdeftype6 => in_user_def_type_6, 
  p_userdeftype7 => in_user_def_type_7, 
  p_userdeftype8 => in_user_def_type_8, 
  p_userdefchk1 => in_user_def_chk_1, 
  p_userdefchk2 => in_user_def_chk_2, 
  p_userdefchk3 => in_user_def_chk_3, 
  p_userdefchk4 => in_user_def_chk_4, 
  p_userdefdate1 => v_user_def_date_1, 
  p_userdefdate2 => v_user_def_date_2, 
  p_userdefdate3 => v_user_def_date_3, 
  p_userdefdate4 => v_user_def_date_4, 
  p_userdefnum1 => in_user_def_num_1, 
  p_userdefnum2 => in_user_def_num_2, 
  p_userdefnum3 => in_user_def_num_3, 
  p_userdefnum4 => in_user_def_num_4, 
  p_userdefnote1 => in_user_def_note_1, 
  p_userdefnote2 => in_user_def_note_2, 
  p_timezonename => in_time_zone_name, 
  p_beamunits => in_beam_units, 
  p_cewarehousetype => in_ce_warehouse_type, 
  p_cecustomsexcise => in_ce_customs_excise, 
  p_cestandardcost => in_ce_standard_cost, 
  p_cestandardcurrency => in_ce_standard_currency, 
  p_disallowclustering => in_disallow_clustering, 
  p_clientgroup => in_client_group, 
  p_maxstack => in_max_stack, 
  p_stackdescription => in_stack_description, 
  p_stacklimitation => in_stack_limitation, 
  p_cedutystamp => in_ce_duty_stamp, 
  p_captureweight => in_capture_weight, 
  p_weighatreceipt => in_weigh_at_receipt, 
  p_upperweighttolerance => in_upper_weight_tolerance, 
  p_lowerweighttolerance => in_lower_weight_tolerance, 
  p_serialatloading => in_serial_at_loading, 
  p_serialatkitting => in_serial_at_kitting, 
  p_serialatunkitting => in_serial_at_unkitting, 
  p_cecommoditycode => in_ce_commodity_code, 
  p_cecoo => in_ce_coo, 
  p_cecwc => in_ce_cwc, 
  p_cevatcode => in_ce_vat_code, 
  p_ceproducttype => in_ce_product_type, 
  p_commoditycode => in_commodity_code, 
  p_commoditydesc => in_commodity_desc, 
  p_familygroup => in_family_group, 
  p_breakpack => in_breakpack, 
  p_clearable => in_clearable, 
  p_stagerouteid => in_stage_route_id, 
  p_serialmaxrange => in_serial_max_range, 
  p_serialdynamicrange => in_serial_dynamic_range, 
  p_expiryatrepack => in_expiry_at_repack, 
  p_udfatrepack => in_udf_at_repack, 
  p_manufactureatrepack => in_manufacture_at_repack, 
  p_repackbypiece => in_repack_by_piece, 
  p_eachquantity => in_each_quantity, 
  p_packedheight => in_packed_height, 
  p_packedwidth => in_packed_width, 
  p_packeddepth => in_packed_depth, 
  p_packedvolume => in_packed_volume, 
  p_packedweight => in_packed_weight, 
  p_awkward => in_awkward, 
  p_twomanlift => in_two_man_lift, 
  p_decatalogued => in_decatalogued, 
  p_stockcheckruleid => in_stock_check_rule_id, 
  p_unkittinginherit => in_unkitting_inherit, 
  p_serialatstockcheck => in_serial_at_stock_check, 
  p_serialatstockadjust => in_serial_at_stock_adjust, 
  p_kitshipcomponents => in_kit_ship_components, 
  p_unallocatable => in_unallocatable, 
  p_batchatkitting => in_batch_at_kitting, 
  p_serialpereach => in_serial_per_each, 
  p_vmiallowallocation => in_vmi_allow_allocation, 
  p_vmiallowinterfaced => in_vmi_allow_interfaced, 
  p_vmiallowmanual => in_vmi_allow_manual, 
  p_vmiallowreplenish => in_vmi_allow_replenish, 
  p_vmiagingdays => in_vmi_aging_days, 
  p_vmioverstockqty => in_vmi_overstock_qty, 
  p_scraponreturn => in_scrap_on_return, 
  p_harmonisedproductcode => in_harmonised_product_code, 
  p_hanginggarment => in_hanging_garment, 
  p_conveyable => in_conveyable, 
  p_fragile => in_fragile, 
  p_gender => in_gender, 
  p_highsecurity => in_high_security, 
  p_ugly => in_ugly, 
  p_collatable => in_collatable, 
  p_ecommerce => in_ecommerce, 
  p_promotion => in_promotion, 
  p_foldable => in_foldable, 
  p_style => in_style, 
  p_businessunitcode => in_business_unit_code, 
  p_tagmerge => in_tag_merge, 
  p_carrierpalletmixing => in_carrier_pallet_mixing, 
  p_specialcontainertype => in_special_container_type, 
  p_disallowrdtoverpicking => in_disallow_over_picking, 
  p_noallocbackorder => in_no_alloc_backorder, 
  p_returnminshelflife => in_return_min_shelf_life, 
  p_weighatgridpick => in_weigh_at_grid_pick, 
  p_ceexciseproductcode => in_ce_excise_product_code, 
  p_cedegreeplato => in_ce_degree_plato, 
  p_cedesignationorigin => in_ce_designation_origin, 
  p_cedensity => in_ce_density, 
  p_cebrandname => in_ce_brand_name, 
  p_cealcoholicstrength => in_ce_alcoholic_strength, 
  p_cefiscalmark => in_ce_fiscal_mark, 
  p_cesizeofproducer => in_ce_size_of_producer, 
  p_cecommercialdesc => in_ce_commercial_desc, 
  p_serialnooutbound => in_serial_no_outbound, 
  p_fullpalletatreceipt => in_full_pallet_at_receipt, 
  p_alwaysfullpallet => in_always_full_pallet, 
  p_subwithinproductgrp => in_sub_within_product_grp, 
  p_serialcheckstring => in_serial_check_string, 
  p_carrierproducttype => in_carrier_product_type, 
  p_maxpackconfigs => in_max_pack_configs, 
  p_parcelpackingbypiece => in_parcel_packing_by_piece
);

IF 
v_error is not null 
AND in_user_def_type_9 IS NULL
AND in_user_def_type_10 IS NULL 
AND in_user_def_type_11 IS NULL 
AND in_user_def_type_12 IS NULL 
AND in_user_def_type_13 IS NULL 
AND in_user_def_type_14 IS NULL 
AND in_user_def_type_15 IS NULL 
AND in_user_def_type_16 IS NULL 
AND in_user_def_type_17 IS NULL 
AND in_user_def_type_18 IS NULL 
AND in_user_def_type_19 IS NULL 
AND in_user_def_type_20 IS NULL 
AND in_user_def_note_3 IS NULL
AND in_user_def_note_4 IS NULL
AND in_user_def_note_5 IS NULL
AND in_user_def_note_6 IS NULL
AND in_sku_aux2 IS NULL
AND in_sku_knit_wove IS NULL
AND in_sku_fabric1 IS NULL
AND in_sku_fabric2 IS NULL
AND in_sku_supplier IS NULL
AND in_record_error_in_table = 'Y'
then
v_interface_key := DCSDBA.IF_SKU_PK_SEQ.nextval;
delete from dcsdba.interface_update_columns where key in (select key from dcsdba.interface_sku where client_id = IN_client_id AND sku_id = IN_SKU_ID AND merge_status = 'Error');
delete from dcsdba.interface_sku where client_id = IN_client_id AND sku_id = IN_SKU_ID AND merge_status = 'Error';
insert into dcsdba.interface_update_columns(key,table_name,update_columns)VALUES(v_interface_key,'interface_sku',nvl(in_update_columns,':sku_id::ean::description::product_group::allocation_group::putaway_group::each_height::each_weight::each_volume::qc_status::expiry_reqd::shelf_life::qc_frequency::split_lowest::condition_reqd::origin_reqd::serial_at_pick::serial_at_pack::pick_count_qty::count_frequency::kit_sku::kit_split::kitting_loc_id::abc_disable::handling_class::obsolete_product::new_product::disallow_upload::manuf_dstamp_reqd::manuf_dstamp_dflt::min_shelf_life::colour::sku_size::hazmat::hazmat_id::disallow_cross_dock::upc::ship_shelf_life::nmfc_number::each_value::client_id::incub_rule::incub_hours::each_width::each_depth::reorder_trigger_qty::low_trigger_qty::user_def_type_1::user_def_type_2::user_def_type_3::user_def_type_4::user_def_type_5::user_def_type_6::user_def_type_7::user_def_type_8::user_def_chk_1::user_def_chk_2::user_def_chk_3::user_def_chk_4::user_def_date_1::user_def_date_2::user_def_date_3::user_def_date_4::user_def_num_1::user_def_num_2::user_def_num_3::user_def_num_4::user_def_note_1::user_def_note_2::disallow_merge_rules::pack_despatch_repack::serial_at_receipt::serial_valid_merge::serial_no_reuse::serial_format::serial_range::spec_id::time_zone_name::beam_units::pick_sequence::ce_warehouse_type::ce_customs_excise::ce_standard_cost::ce_standard_currency::disallow_clustering::client_group::max_stack::stack_description::stack_limitation::ce_duty_stamp::capture_weight::weigh_at_receipt::upper_weight_tolerance::lower_weight_tolerance::serial_at_loading::serial_at_kitting::serial_at_unkitting::ce_commodity_code::ce_coo::ce_cwc::ce_vat_code::ce_product_type::commodity_code::commodity_desc::family_group::breakpack::clearable::stage_route_id::serial_max_range::serial_dynamic_range::expiry_at_repack::udf_at_repack::manufacture_at_repack::repack_by_piece::each_quantity::collective_mode::packed_height::packed_width::packed_depth::packed_volume::packed_weight::awkward::two_man_lift::decatalogued::stock_check_rule_id::unkitting_inherit::serial_at_stock_check::serial_at_stock_adjust::kit_ship_components::unallocatable::batch_at_kitting::serial_per_each::vmi_allow_allocation::vmi_allow_replenish::vmi_allow_interfaced::vmi_allow_manual::vmi_aging_days::vmi_overstock_qty::scrap_on_return::nls_calendar::harmonised_product_code::hanging_garment::conveyable::fragile::gender::high_security::ugly::collatable::ecommerce::promotion::foldable::style::business_unit_code::tag_merge::carrier_pallet_mixing::no_alloc_back_order::special_container_type::disallow_rdt_over_picking::return_min_shelf_life::collective_sequence::weigh_at_grid_pick::ce_excise_product_code::ce_degree_plato::ce_designation_origin::ce_density::ce_brand_name::ce_alcoholic_strength::ce_fiscal_mark::ce_size_of_producer::ce_commercial_desc::serial_no_outbound::full_pallet_at_receipt::always_full_pallet::sub_within_product_grp::serial_check_string::carrier_product_type::max_pack_configs:'));
insert into dcsdba.interface_sku(Key, Merge_Dstamp, Merge_Status, Merge_Error, Merge_Action,Abc_Disable,Allocation_Group,Always_Full_Pallet,Awkward,Batch_At_Kitting,Beam_Units,Breakpack,Business_Unit_Code,Capture_Weight,Carrier_Pallet_Mixing,Carrier_Product_Type,Ce_Alcoholic_Strength,Ce_Brand_Name,Ce_Commercial_Desc,Ce_Commodity_Code,Ce_Coo,Ce_Customs_Excise,Ce_Cwc,Ce_Degree_Plato,Ce_Density,Ce_Designation_Origin,Ce_Duty_Stamp,Ce_Excise_Product_Code,Ce_Fiscal_Mark,Ce_Product_Type,Ce_Size_Of_Producer,Ce_Standard_Cost,Ce_Standard_Currency,Ce_Vat_Code,Ce_Warehouse_Type,Clearable,Client_Group,Client_Id,Collatable,Colour,Commodity_Code,Commodity_Desc,Condition_Reqd,Conveyable,Count_Frequency,Decatalogued,Description,Disallow_Clustering,Disallow_Cross_Dock,Disallow_Merge_Rules,Disallow_Rdt_Over_Picking,Disallow_Upload,Each_Depth,Each_Height,Each_Quantity,Each_Value,Each_Volume,Each_Weight,Each_Width,Ean,Ecommerce,Expiry_At_Repack,Expiry_Reqd,Family_Group,Foldable,Fragile,Full_Pallet_At_Receipt,Gender,Handling_Class,Hanging_Garment,Harmonised_Product_Code,Hazmat,Hazmat_Id,High_Security,Incub_Hours,Incub_Rule,Kit_Ship_Components,Kit_Sku,Kit_Split,Kitting_Loc_Id,Lower_Weight_Tolerance,Low_Trigger_Qty,Manufacture_At_Repack,Manuf_Dstamp_Dflt,Manuf_Dstamp_Reqd,Max_Pack_Configs,Max_Stack,Min_Shelf_Life,New_Product,Nmfc_Number,No_Alloc_Back_Order,Obsolete_Product,Origin_Reqd,Pack_Despatch_Repack,Packed_Depth,Packed_Height,Packed_Volume,Packed_Weight,Packed_Width,Parcel_Packing_By_Piece,Pick_Count_Qty,Pick_Sequence,Product_Group,Promotion,Putaway_Group,Qc_Frequency,Qc_Status,Reorder_Trigger_Qty,Repack_By_Piece,Return_Min_Shelf_Life,Scrap_On_Return,Serial_At_Kitting,Serial_At_Loading,Serial_At_Pack,Serial_At_Pick,Serial_At_Receipt,Serial_At_Stock_Adjust,Serial_At_Stock_Check,Serial_At_Unkitting,Serial_Check_String,Serial_Dynamic_Range,Serial_Format,Serial_Max_Range,Serial_No_Outbound,Serial_No_Reuse,Serial_Per_Each,Serial_Range,Serial_Valid_Merge,Shelf_Life,Ship_Shelf_Life,Sku_Id,Sku_Size,Special_Container_Type,Spec_Id,Split_Lowest,Stack_Description,Stack_Limitation,Stage_Route_Id,Stock_Check_Rule_Id,Style,Sub_Within_Product_Grp,Tag_Merge,Time_Zone_Name,Two_Man_Lift,Udf_At_Repack,Ugly,Unallocatable,Unkitting_Inherit,Upc,Upper_Weight_Tolerance,User_Def_Chk_1,User_Def_Chk_2,User_Def_Chk_3,User_Def_Chk_4,User_Def_Date_1,User_Def_Date_2,User_Def_Date_3,User_Def_Date_4,User_Def_Note_1,User_Def_Note_2,User_Def_Num_1,User_Def_Num_2,User_Def_Num_3,User_Def_Num_4,User_Def_Type_1,User_Def_Type_2,User_Def_Type_3,User_Def_Type_4,User_Def_Type_5,User_Def_Type_6,User_Def_Type_7,User_Def_Type_8,Vmi_Aging_Days,Vmi_Allow_Allocation,Vmi_Allow_Interfaced,Vmi_Allow_Manual,Vmi_Allow_Replenish,Vmi_Overstock_Qty,Weigh_At_Grid_Pick,Weigh_At_Receipt)
VALUES(v_interface_key, sysdate, 'Error', v_error, IN_Merge_Action, IN_Abc_Disable, IN_Allocation_Group, IN_Always_Full_Pallet, IN_Awkward, IN_Batch_At_Kitting, IN_Beam_Units, IN_Breakpack, IN_Business_Unit_Code, IN_Capture_Weight, IN_Carrier_Pallet_Mixing, IN_Carrier_Product_Type, IN_Ce_Alcoholic_Strength, IN_Ce_Brand_Name, IN_Ce_Commercial_Desc, IN_Ce_Commodity_Code, IN_Ce_Coo, IN_Ce_Customs_Excise, IN_Ce_Cwc, IN_Ce_Degree_Plato, IN_Ce_Density, IN_Ce_Designation_Origin, IN_Ce_Duty_Stamp, IN_Ce_Excise_Product_Code, IN_Ce_Fiscal_Mark, IN_Ce_Product_Type, IN_Ce_Size_Of_Producer, IN_Ce_Standard_Cost, IN_Ce_Standard_Currency, IN_Ce_Vat_Code, IN_Ce_Warehouse_Type, IN_Clearable, IN_Client_Group, IN_Client_Id, IN_Collatable, IN_Colour, IN_Commodity_Code, IN_Commodity_Desc, IN_Condition_Reqd, IN_Conveyable, IN_Count_Frequency, IN_Decatalogued, IN_Description, IN_Disallow_Clustering, IN_Disallow_Cross_Dock, IN_Disallow_Merge_Rules, in_disallow_over_picking, IN_Disallow_Upload, IN_Each_Depth, IN_Each_Height, IN_Each_Quantity, IN_Each_Value, IN_Each_Volume, IN_Each_Weight, IN_Each_Width, IN_Ean, IN_Ecommerce, IN_Expiry_At_Repack, IN_Expiry_Reqd, IN_Family_Group, IN_Foldable, IN_Fragile, IN_Full_Pallet_At_Receipt, IN_Gender, IN_Handling_Class, IN_Hanging_Garment, IN_Harmonised_Product_Code, IN_Hazmat, IN_Hazmat_Id, IN_High_Security, IN_Incub_Hours, IN_Incub_Rule, IN_Kit_Ship_Components, IN_Kit_Sku, IN_Kit_Split, IN_Kitting_Loc_Id, IN_Lower_Weight_Tolerance, IN_Low_Trigger_Qty, IN_Manufacture_At_Repack, in_Manuf_Dstamp_flt, IN_Manuf_Dstamp_Reqd, IN_Max_Pack_Configs, IN_Max_Stack, IN_Min_Shelf_Life, IN_New_Product, IN_Nmfc_Number, IN_No_Alloc_BackOrder, IN_Obsolete_Product, IN_Origin_Reqd, IN_Pack_Despatch_Repack, IN_Packed_Depth, IN_Packed_Height, IN_Packed_Volume, IN_Packed_Weight, IN_Packed_Width, IN_Parcel_Packing_By_Piece, IN_Pick_Count_Qty, IN_Pick_Sequence, IN_Product_Group, IN_Promotion, IN_Putaway_Group, IN_Qc_Frequency, IN_Qc_Status, IN_Reorder_Trigger_Qty, IN_Repack_By_Piece, IN_Return_Min_Shelf_Life, IN_Scrap_On_Return, IN_Serial_At_Kitting, IN_Serial_At_Loading, IN_Serial_At_Pack, IN_Serial_At_Pick, IN_Serial_At_Receipt, IN_Serial_At_Stock_Adjust, IN_Serial_At_Stock_Check, IN_Serial_At_Unkitting, IN_Serial_Check_String, IN_Serial_Dynamic_Range, IN_Serial_Format, IN_Serial_Max_Range, IN_Serial_No_Outbound, IN_Serial_No_Reuse, IN_Serial_Per_Each, IN_Serial_Range, IN_Serial_Valid_Merge, IN_Shelf_Life, IN_Ship_Shelf_Life, IN_Sku_Id, IN_Sku_Size, IN_Special_Container_Type, IN_Spec_Id, IN_Split_Lowest, IN_Stack_Description, IN_Stack_Limitation, IN_Stage_Route_Id, IN_Stock_Check_Rule_Id, IN_Style, IN_Sub_Within_Product_Grp, IN_Tag_Merge, IN_Time_Zone_Name, IN_Two_Man_Lift, IN_Udf_At_Repack, IN_Ugly, IN_Unallocatable, IN_Unkitting_Inherit, IN_Upc, IN_Upper_Weight_Tolerance, IN_User_Def_Chk_1, IN_User_Def_Chk_2, IN_User_Def_Chk_3, IN_User_Def_Chk_4, v_User_Def_Date_1, v_User_Def_Date_2, v_User_Def_Date_3, v_User_Def_Date_4, IN_User_Def_Note_1, IN_User_Def_Note_2, IN_User_Def_Num_1, IN_User_Def_Num_2, IN_User_Def_Num_3, IN_User_Def_Num_4, IN_User_Def_Type_1, IN_User_Def_Type_2, IN_User_Def_Type_3, IN_User_Def_Type_4, IN_User_Def_Type_5, IN_User_Def_Type_6, IN_User_Def_Type_7, IN_User_Def_Type_8, IN_Vmi_Aging_Days, IN_Vmi_Allow_Allocation, IN_Vmi_Allow_Interfaced, IN_Vmi_Allow_Manual, IN_Vmi_Allow_Replenish, IN_Vmi_Overstock_Qty, IN_Weigh_At_Grid_Pick, IN_Weigh_At_Receipt);
ELSIF v_error is null then
delete from dcsdba.interface_update_columns where key in (select key from dcsdba.interface_sku where client_id = IN_client_id AND sku_id = IN_SKU_ID AND merge_status = 'Error');
delete from dcsdba.interface_sku where client_id = IN_client_id AND sku_id = IN_SKU_ID AND merge_status = 'Error';

update dcsdba.sku set 
user_def_type_9 = in_user_def_type_9,
user_def_type_10 = in_user_def_type_10,
user_def_type_11 = in_user_def_type_11,
user_def_type_12 = in_user_def_type_12,
user_def_type_13 = in_user_def_type_13,
user_def_type_14 = in_user_def_type_14,
user_def_type_15 = in_user_def_type_15,
user_def_type_16 = in_user_def_type_16,
user_def_type_17 = in_user_def_type_17,
user_def_type_18 = in_user_def_type_18,
user_def_type_19 = in_user_def_type_19,
user_def_type_20 = in_user_def_type_20
where client_id = upper(in_client_id) AND sku_id = upper(in_sku_id);

    IF
    in_user_def_note_3 IS NOT NULL
    OR in_user_def_note_4 IS NOT NULL
    OR in_user_def_note_5 IS NOT NULL
    OR in_user_def_note_6 IS NOT NULL
    OR in_sku_aux2 IS NOT NULL
    OR in_sku_knit_wove IS NOT NULL
    OR in_sku_fabric1 IS NOT NULL
    OR in_sku_fabric2 IS NOT NULL
    OR in_sku_supplier IS NOT NULL 
    Then
        update torque.sku_ext set user_def_note_3 = in_user_def_note_3, user_def_note_4 = in_user_def_note_4, user_def_note_5 = in_user_def_note_5, user_def_note_6 = in_user_def_note_6, sku_aux2 = in_sku_aux2,
        sku_knit_wove = in_sku_knit_wove, sku_fabric1 = in_sku_fabric1, sku_fabric2 = in_sku_fabric2, sku_supplier = in_sku_supplier WHERE client_id = upper(in_client_id) AND sku_id = upper(in_sku_id);
        v_update_chk := SQL%rowcount;
        IF v_update_chk = 0 then
            insert into torque.sku_ext(client_id,sku_id,user_def_note_3,user_def_note_4,user_def_note_5,user_def_note_6,sku_aux2,sku_knit_wove,sku_fabric1,sku_fabric2,sku_supplier)
            VALUES(upper(in_client_id),upper(in_sku_id),in_user_def_note_3,in_user_def_note_4,in_user_def_note_5,in_user_def_note_6,in_sku_aux2,in_sku_knit_wove,in_sku_fabric1,in_sku_fabric2,in_sku_supplier);
        END IF;
    ELSE
        delete from torque.sku_ext WHERE client_id = upper(in_client_id) AND sku_id = upper(in_sku_id);
    END IF;


END IF;

COMMIT;
RETURN v_error;

END INTERFACE_SKU;
/***********************************************************************************************************************/

/*                                                                                                                     */

/*  NAME:               TQ_INTERFACE.INTERFACE_SKU_SKU_CONFIG                                                          */

/*                                                                                                                     */

/*  DESCRIPTION:        This Function Uses The Interface SKU_SKU_CONFIG to Link Pack Configs                           */

/*                                                                                                                     */

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */

/*  =======     ===========     ==     ===================================================================             */

/*    1         2019-10-18      KS     Initial Version                                                                 */

/*    2         2020-01-28      KS     Optional Log Error Parameter                                                    */

/*                                                                                                                     */

/***********************************************************************************************************************/

/**********************GRANTS**************************

grant select on IF_SSC_PK_SEQ TO TORQUE;

grant insert on interface_update_columns to TORQUE;
grant insert on interface_sku_sku_config to TORQUE;

grant execute on libsession to TORQUE;
grant execute on libmergeskuskuconfig to TORQUE;
grant execute on liberror to TORQUE;

******************************************************/
FUNCTION interface_sku_sku_config (
  in_update_columns VARCHAR2 DEFAULT NULL, 
  in_merge_action VARCHAR2 DEFAULT 'U', 
  in_client_id VARCHAR2, 
  in_sku_id VARCHAR2, 
  in_config_id VARCHAR2, 
  in_main_config_id VARCHAR2 DEFAULT NULL, 
  in_min_full_pallet_perc INTEGER DEFAULT NULL, 
  in_max_full_pallet_perc INTEGER DEFAULT NULL, 
  in_disabled VARCHAR2 DEFAULT NULL, 
  in_nls_calendar VARCHAR2 DEFAULT NULL,
  in_timezone_name VARCHAR2 DEFAULT NULL, 
  in_user_id VARCHAR2 DEFAULT 'API-INT', 
  in_station_id VARCHAR2 DEFAULT 'API-INT',
  in_record_error_in_table VARCHAR2 DEFAULT NULL
) RETURN VARCHAR2 IS PRAGMA autonomous_transaction;
v_retval NUMBER;
v_error VARCHAR2(20);
v_interface_key NUMBER;
BEGIN dcsdba.libsession.initialisesession(
  in_user_id, 'INT-API', in_station_id, 
  'INT-API'
);
v_retval := dcsdba.libmergeskuskuconfig.directskuskuconfig(
  v_error, in_update_columns, in_merge_action, 
  in_client_id, in_sku_id, in_config_id, 
  in_main_config_id, in_min_full_pallet_perc, 
  in_max_full_pallet_perc, in_disabled, 
  in_timezone_name
);
IF v_error is not null AND in_record_error_in_table = 'Y' then
v_interface_key := DCSDBA.IF_SUPSKU_PK_SEQ.nextval;
insert into dcsdba.interface_update_columns(key,table_name,update_columns)VALUES(v_interface_key,'interface_sku_sku_config',nvl(in_update_columns,':sku_id::config_id::main_config_id::client_id::min_full_pallet_perc::max_full_pallet_perc::time_zone_name::nls_calendar:'));
insert into dcsdba.interface_sku_sku_config(Key, Merge_Dstamp, Merge_Status, merge_error, Merge_Action, client_id, config_id, disabled, main_config_id, max_full_pallet_perc, Min_Full_Pallet_Perc, sku_id, time_zone_name)
VALUES(v_interface_key, sysdate, 'Error', v_error, IN_Merge_Action, in_client_id, in_config_id, in_disabled, in_main_config_id, in_max_full_pallet_perc, in_min_full_pallet_perc, in_sku_id, in_timezone_name);
END IF;
COMMIT;
RETURN v_error;

END INTERFACE_SKU_SKU_CONFIG;
/***********************************************************************************************************************/

/*                                                                                                                     */

/*  NAME:               TQ_INTERFACE.INTERFACE_SUPPLIER_SKU                                                            */

/*                                                                                                                     */

/*  DESCRIPTION:        This Function Uses The Interface SUPPLIER_SKU to add supplier SKUs                             */

/*                                                                                                                     */

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */

/*  =======     ===========     ==     ===================================================================             */

/*    1         2019-10-18      KS     Initial Version                                                                 */

/*    2         2020-01-28      KS     Optional Log Error Parameter                                                    */

/*                                                                                                                     */

/***********************************************************************************************************************/

/**********************GRANTS**************************

grant select on IF_SUPSKU_PK_SEQ TO TORQUE;

grant insert on interface_update_columns to TORQUE;
grant insert on interface_supplier_sku to TORQUE;

grant execute on libsession to TORQUE;
grant execute on libmergesuppliersku to TORQUE;
grant execute on liberror to TORQUE;

******************************************************/
FUNCTION interface_supplier_sku (
  in_update_columns VARCHAR2 DEFAULT NULL, 
  in_merge_action VARCHAR2 DEFAULT 'U', 
  in_client_id VARCHAR2, in_sku_id VARCHAR2, 
  in_supplier_sku_id VARCHAR2, in_supplier_id VARCHAR2 DEFAULT NULL, 
  in_notes VARCHAR2 DEFAULT NULL, in_vm_sku VARCHAR2 DEFAULT NULL, 
  in_nls_calendar VARCHAR2 DEFAULT NULL,
  in_timezone_name VARCHAR2 DEFAULT NULL, 
  in_qc_status VARCHAR2 DEFAULT NULL, 
  in_delivery_lead_time_days NUMBER DEFAULT NULL, 
  in_reorder_preference NUMBER DEFAULT NULL, 
  in_user_id VARCHAR2 DEFAULT 'API-INT', 
  in_station_id VARCHAR2 DEFAULT 'API-INT',
  in_record_error_in_table VARCHAR2 DEFAULT NULL
) RETURN VARCHAR2 IS PRAGMA autonomous_transaction;
v_retval NUMBER;
v_error VARCHAR2(20);
v_interface_key NUMBER;
BEGIN dcsdba.libsession.initialisesession(
  in_user_id, 'INT-API', in_station_id, 
  'INT-API'
);
v_retval := dcsdba.libmergesuppliersku.directsuppliersku(
  v_error, in_update_columns, in_merge_action, 
  in_client_id, in_sku_id, in_supplier_sku_id, 
  in_supplier_id, in_notes, in_vm_sku, 
  in_timezone_name, in_qc_status, 
  in_delivery_lead_time_days, in_reorder_preference
);

IF v_error is not null AND in_record_error_in_table = 'Y' then
v_interface_key := DCSDBA.IF_SUPSKU_PK_SEQ.nextval;
insert into dcsdba.interface_update_columns(key,table_name,update_columns)VALUES(v_interface_key,'interface_supplier_sku',nvl(in_update_columns,':sku_id::supplier_sku_id::supplier_id::notes::client_id::vm_sku::time_zone_name::qc_status::collective_mode::nls_calendar::delivery_lead_time_days::reorder_preference::collective_sequence:'));
insert into dcsdba.interface_supplier_sku(Key, Merge_Dstamp, Merge_Status, merge_error, Merge_Action, Sku_Id, Supplier_Sku_Id, Supplier_Id, Notes, Client_Id, Vm_Sku, Time_Zone_Name, Qc_Status, Delivery_Lead_Time_Days, Reorder_Preference)
VALUES(v_interface_key, sysdate, 'Error', v_error, IN_Merge_Action,IN_Sku_Id, IN_Supplier_Sku_Id, IN_Supplier_Id, IN_Notes, IN_Client_Id, IN_Vm_Sku, IN_TimeZone_Name, IN_Qc_Status, IN_Delivery_Lead_Time_Days, IN_Reorder_Preference);
END IF;
COMMIT;
RETURN v_error;

END interface_supplier_sku;
/***********************************************************************************************************************/

/*                                                                                                                     */

/*  NAME:               TQ_INTERFACE.INTERFACE_PRE_ADVICE_HEADER                                                       */

/*                                                                                                                     */

/*  DESCRIPTION:        This Function Uses The Interface PRE_ADVICE_HEADER                                             */

/*                                                                                                                     */

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */

/*  =======     ===========     ==     ===================================================================             */

/*    1         2019-10-18      KS     Initial Version                                                                 */

/*    2         2020-01-28      KS     Optional Log Error Parameter                                                    */

/*    3         2020-03-02      KS     Standardised Parameters                                                         */

/*                                                                                                                     */

/***********************************************************************************************************************/

/**********************GRANTS**************************

grant select on IF_PAH_PK_SEQ to TORQUE;

grant insert on interface_update_columns to TORQUE;
grant insert on interface_pre_advice_header to TORQUE;

grant execute on libsession to TORQUE;
grant execute on libmergepreadvice to TORQUE;
grant execute on liberror to TORQUE;

******************************************************/
FUNCTION Interface_pre_advice_header (
  in_update_columns VARCHAR2 DEFAULT NULL, 
  in_merge_action VARCHAR2 DEFAULT 'U', 
  in_client_id VARCHAR2, 
  in_pre_advice_id VARCHAR2, 
  in_pre_advice_type VARCHAR2 DEFAULT NULL, 
  in_site_id VARCHAR2 DEFAULT NULL, 
  in_owner_id VARCHAR2 DEFAULT NULL, 
  in_supplier_id VARCHAR2 DEFAULT NULL, 
  in_status VARCHAR2 DEFAULT NULL, 
  in_bookref_id VARCHAR2 DEFAULT NULL, 
  in_due_dstamp VARCHAR2 DEFAULT NULL, 
  in_contact VARCHAR2 DEFAULT NULL, 
  in_contact_phone VARCHAR2 DEFAULT NULL, 
  in_contact_fax VARCHAR2 DEFAULT NULL, 
  in_contact_email VARCHAR2 DEFAULT NULL, 
  in_name VARCHAR2 DEFAULT NULL, 
  in_address1 VARCHAR2 DEFAULT NULL, 
  in_address2 VARCHAR2 DEFAULT NULL, 
  in_town VARCHAR2 DEFAULT NULL, 
  in_county VARCHAR2 DEFAULT NULL, 
  in_postcode VARCHAR2 DEFAULT NULL, 
  in_country VARCHAR2 DEFAULT NULL, 
  in_vat_number VARCHAR2 DEFAULT NULL, 
  in_return_flag VARCHAR2 DEFAULT NULL, 
  in_sampling_type VARCHAR2 DEFAULT NULL, 
  in_returned_order_id VARCHAR2 DEFAULT NULL, 
  in_email_confirm VARCHAR2 DEFAULT NULL, 
  in_collection_reqd VARCHAR2 DEFAULT NULL, 
  in_consignment VARCHAR2 DEFAULT NULL, 
  in_load_sequence NUMBER DEFAULT NULL, 
  in_notes VARCHAR2 DEFAULT NULL, 
  in_disallow_merge_rules VARCHAR2 DEFAULT NULL, 
  in_user_def_type_1 VARCHAR2 DEFAULT NULL, 
  in_user_def_type_2 VARCHAR2 DEFAULT NULL, 
  in_user_def_type_3 VARCHAR2 DEFAULT NULL, 
  in_user_def_type_4 VARCHAR2 DEFAULT NULL, 
  in_user_def_type_5 VARCHAR2 DEFAULT NULL, 
  in_user_def_type_6 VARCHAR2 DEFAULT NULL, 
  in_user_def_type_7 VARCHAR2 DEFAULT NULL, 
  in_user_def_type_8 VARCHAR2 DEFAULT NULL, 
  in_user_def_chk_1 VARCHAR2 DEFAULT NULL, 
  in_user_def_chk_2 VARCHAR2 DEFAULT NULL, 
  in_user_def_chk_3 VARCHAR2 DEFAULT NULL, 
  in_user_def_chk_4 VARCHAR2 DEFAULT NULL, 
  in_user_def_date_1 VARCHAR2 DEFAULT NULL, 
  in_user_def_date_2 VARCHAR2 DEFAULT NULL, 
  in_user_def_date_3 VARCHAR2 DEFAULT NULL, 
  in_user_def_date_4 VARCHAR2 DEFAULT NULL, 
  in_user_def_num_1 NUMBER DEFAULT NULL, 
  in_user_def_num_2 NUMBER DEFAULT NULL, 
  in_user_def_num_3 NUMBER DEFAULT NULL, 
  in_user_def_num_4 NUMBER DEFAULT NULL, 
  in_user_def_note_1 VARCHAR2 DEFAULT NULL, 
  in_user_def_note_2 VARCHAR2 DEFAULT NULL, 
  in_time_zone_name VARCHAR2 DEFAULT NULL, 
  in_disallow_replens VARCHAR2 DEFAULT NULL, 
  in_client_group VARCHAR2 DEFAULT NULL, 
  in_status_reason_code VARCHAR2 DEFAULT NULL, 
  in_priority VARCHAR2 DEFAULT NULL, 
  in_supplier_reference VARCHAR2 DEFAULT NULL, 
  in_carrier_name VARCHAR2 DEFAULT NULL, 
  in_carrier_reference VARCHAR2 DEFAULT NULL, 
  in_tod VARCHAR2 DEFAULT NULL, 
  in_tod_place VARCHAR2 DEFAULT NULL, 
  in_mode_of_transport VARCHAR2 DEFAULT NULL, 
  in_yard_container_type VARCHAR2 DEFAULT NULL, 
  in_yard_container_id VARCHAR2 DEFAULT NULL, 
  in_ce_consignment_id VARCHAR2 DEFAULT NULL, 
  in_contact_mobile VARCHAR2 DEFAULT NULL, 
  in_master_pre_advice VARCHAR2 DEFAULT NULL, 
  in_ce_invoice_number VARCHAR2 DEFAULT NULL, 
  in_user_id VARCHAR2 DEFAULT 'API-INT', 
  in_station_id VARCHAR2 DEFAULT 'API-INT',
  in_nls_calendar VARCHAR2 DEFAULT NULL,
  in_record_error_in_table VARCHAR2 DEFAULT NULL
) RETURN VARCHAR2 IS PRAGMA autonomous_transaction;
v_retval NUMBER;
v_error VARCHAR2(20);
v_due_dstamp TIMESTAMP;
v_user_def_date_1 TIMESTAMP;
v_user_def_date_2 TIMESTAMP;
v_user_def_date_3 TIMESTAMP;
v_user_def_date_4 TIMESTAMP;
v_interface_key NUMBER;
BEGIN 
dcsdba.libsession.initialisesession(
  in_user_id, 'INT-API', in_station_id, 
  'INT-API'
);
IF in_due_dstamp IS NOT NULL THEN v_due_dstamp := To_timestamp(
  in_due_dstamp, 'YYYYMMDDHH24MISS'
);
END IF;
IF in_user_def_date_1 IS NOT NULL THEN v_user_def_date_1 := To_timestamp(
  in_user_def_date_1, 'YYYYMMDDHH24MISS'
);
END IF;
IF in_user_def_date_2 IS NOT NULL THEN v_user_def_date_2 := To_timestamp(
  in_user_def_date_2, 'YYYYMMDDHH24MISS'
);
END IF;
IF in_user_def_date_3 IS NOT NULL THEN v_user_def_date_3 := To_timestamp(
  in_user_def_date_3, 'YYYYMMDDHH24MISS'
);
END IF;
IF in_user_def_date_4 IS NOT NULL THEN v_user_def_date_4 := To_timestamp(
  in_user_def_date_4, 'YYYYMMDDHH24MISS'
);
END IF;
v_retval := dcsdba.libmergepreadvice.Directpreadviceheader(
  p_mergeerror => v_error, 
  p_toupdatecols => in_update_columns, 
  p_mergeaction => in_merge_action, 
  p_clientid => in_client_id, 
  p_preadviceid => in_pre_advice_id, 
  p_preadvicetype => in_pre_advice_type, 
  p_siteid => in_site_id, 
  p_ownerid => in_owner_id, 
  p_supplierid => in_supplier_id, 
  p_status => in_status, 
  p_bookrefid => in_bookref_id, 
  p_duedstamp => v_due_dstamp, 
  p_contact => in_contact, 
  p_contactphone => in_contact_phone, 
  p_contactfax => in_contact_fax, 
  p_contactemail => in_contact_email, 
  p_name => in_name, 
  p_address1 => in_address1, 
  p_address2 => in_address2, 
  p_town => in_town, 
  p_county => in_county, 
  p_postcode => in_postcode, 
  p_country => in_country, 
  p_vatnumber => in_vat_number, 
  p_returnflag => in_return_flag, 
  p_samplingtype => in_sampling_type, 
  p_returnedorderid => in_returned_order_id, 
  p_emailconfirm => in_email_confirm, 
  p_collectionreqd => in_collection_reqd, 
  p_consignment => in_consignment, 
  p_loadsequence => in_load_sequence, 
  p_notes => in_notes, 
  p_disallowmergerules => in_disallow_merge_rules, 
  p_userdeftype1 => in_user_def_type_1, 
  p_userdeftype2 => in_user_def_type_2, 
  p_userdeftype3 => in_user_def_type_3, 
  p_userdeftype4 => in_user_def_type_4, 
  p_userdeftype5 => in_user_def_type_5, 
  p_userdeftype6 => in_user_def_type_6, 
  p_userdeftype7 => in_user_def_type_7, 
  p_userdeftype8 => in_user_def_type_8, 
  p_userdefchk1 => in_user_def_chk_1, 
  p_userdefchk2 => in_user_def_chk_2, 
  p_userdefchk3 => in_user_def_chk_3, 
  p_userdefchk4 => in_user_def_chk_4, 
  p_userdefdate1 => v_user_def_date_1, 
  p_userdefdate2 => v_user_def_date_2, 
  p_userdefdate3 => v_user_def_date_3, 
  p_userdefdate4 => v_user_def_date_4, 
  p_userdefnum1 => in_user_def_num_1, 
  p_userdefnum2 => in_user_def_num_2, 
  p_userdefnum3 => in_user_def_num_3, 
  p_userdefnum4 => in_user_def_num_4, 
  p_userdefnote1 => in_user_def_note_1, 
  p_userdefnote2 => in_user_def_note_2, 
  p_timezonename => in_time_zone_name, 
  p_disallowreplens => in_disallow_replens, 
  p_clientgroup => in_client_group, 
  p_statusreasoncode => in_status_reason_code, 
  p_priority => in_priority, 
  p_supplierreference => in_supplier_reference, 
  p_carriername => in_carrier_name, 
  p_carrierreference => in_carrier_reference, 
  p_tod => in_tod, 
  p_todplace => in_tod_place, 
  p_modeoftransport => in_mode_of_transport, 
  p_yardcontainertype => in_yard_container_type, 
  p_yardcontainerid => in_yard_container_id, 
  p_ceconsignmentid => in_ce_consignment_id, 
  p_contactmobile => in_contact_mobile, 
  p_masterpreadvice => in_master_pre_advice, 
  p_ceinvoicenumber => in_ce_invoice_number
);
COMMIT;

IF v_error is not null AND in_record_error_in_table = 'Y' then
v_interface_key := DCSDBA.IF_PAH_PK_SEQ.nextval;
insert into dcsdba.interface_update_columns(key,table_name,update_columns)VALUES(v_interface_key,'interface_pre_advice_header',nvl(in_update_columns,':pre_advice_id::site_id::supplier_id::status::bookref_id::due_dstamp::notes::owner_id::contact::contact_phone::contact_fax::contact_email::name::address1::address2::town::county::postcode::country::pre_advice_type::sampling_type::return_flag::returned_order_id::collection_reqd::consignment::load_sequence::email_confirm::client_id::disallow_merge_rules::user_def_type_1::user_def_type_2::user_def_type_3::user_def_type_4::user_def_type_5::user_def_type_6::user_def_type_7::user_def_type_8::user_def_chk_1::user_def_chk_2::user_def_chk_3::user_def_chk_4::user_def_date_1::user_def_date_2::user_def_date_3::user_def_date_4::user_def_num_1::user_def_num_2::user_def_num_3::user_def_num_4::user_def_note_1::user_def_note_2::time_zone_name::disallow_replens::client_group::supplier_reference::carrier_name::carrier_reference::tod::tod_place::mode_of_transport::vat_number::yard_container_type::yard_container_id::ce_consignment_id::collective_mode::contact_mobile::master_pre_advice::ce_invoice_number::nls_calendar::oap_rma:'));
insert into dcsdba.interface_pre_advice_header(Key, Merge_Dstamp, Merge_Status, merge_error, Merge_Action, Address1, Address2, Bookref_Id, Carrier_Name, Carrier_Reference, Ce_Consignment_Id, Ce_Invoice_Number, Client_Group, Client_Id, Collection_Reqd, Consignment, Contact, Contact_Email, Contact_Fax, Contact_Mobile, Contact_Phone, Country, County, Disallow_Merge_Rules, Disallow_Replens, Due_Dstamp, Email_Confirm, Load_Sequence, Master_Pre_Advice, Mode_Of_Transport, Name, Notes, Owner_Id, Postcode, Pre_Advice_Id, Pre_Advice_Type, Priority, Returned_Order_Id, Return_Flag, Sampling_Type, Site_Id, Status, Status_Reason_Code, Supplier_Id, Supplier_Reference, Time_Zone_Name, Tod, Tod_Place, Town, User_Def_Chk_1, User_Def_Chk_2, User_Def_Chk_3, User_Def_Chk_4, User_Def_Date_1, User_Def_Date_2, User_Def_Date_3, User_Def_Date_4, User_Def_Note_1, User_Def_Note_2, User_Def_Num_1, User_Def_Num_2, User_Def_Num_3, User_Def_Num_4, User_Def_Type_1, User_Def_Type_2, User_Def_Type_3, User_Def_Type_4, User_Def_Type_5, User_Def_Type_6, User_Def_Type_7, User_Def_Type_8, Vat_Number, Yard_Container_Id, Yard_Container_Type)
VALUES(v_interface_key, sysdate, 'Error', v_error, IN_Merge_Action, IN_Address1, IN_Address2, IN_Bookref_Id, IN_Carrier_Name, IN_Carrier_Reference, IN_Ce_Consignment_Id, IN_Ce_Invoice_Number, IN_Client_Group, IN_Client_Id, IN_Collection_Reqd, IN_Consignment, IN_Contact, IN_Contact_Email, IN_Contact_Fax, IN_Contact_Mobile, IN_Contact_Phone, IN_Country, IN_County, IN_Disallow_Merge_Rules, IN_Disallow_Replens, v_Due_Dstamp, IN_Email_Confirm, IN_Load_Sequence, IN_Master_Pre_Advice, IN_Mode_Of_Transport, IN_Name, IN_Notes, IN_Owner_Id, IN_Postcode, IN_Pre_Advice_Id, IN_Pre_Advice_Type, IN_Priority, IN_Returned_Order_Id, IN_Return_Flag, IN_Sampling_Type, IN_Site_Id, IN_Status, IN_Status_Reason_Code, IN_Supplier_Id, IN_Supplier_Reference, IN_Time_Zone_Name, IN_Tod, IN_Tod_Place, IN_Town, IN_User_Def_Chk_1, IN_User_Def_Chk_2, IN_User_Def_Chk_3, IN_User_Def_Chk_4, v_User_Def_Date_1, v_User_Def_Date_2, v_User_Def_Date_3, v_User_Def_Date_4, IN_User_Def_Note_1, IN_User_Def_Note_2, IN_User_Def_Num_1, IN_User_Def_Num_2, IN_User_Def_Num_3, IN_User_Def_Num_4, IN_User_Def_Type_1, IN_User_Def_Type_2, IN_User_Def_Type_3, IN_User_Def_Type_4, IN_User_Def_Type_5, IN_User_Def_Type_6, IN_User_Def_Type_7, IN_User_Def_Type_8, IN_Vat_Number, IN_Yard_Container_Id, IN_Yard_Container_Type);
commit;
END IF;

RETURN v_error;
END interface_pre_advice_header;
/***********************************************************************************************************************/

/*                                                                                                                     */

/*  NAME:               TQ_INTERFACE.INTERFACE_PRE_ADVICE_LINE                                                         */

/*                                                                                                                     */

/*  DESCRIPTION:        This Function Uses The Interface PRE_ADVICE_LINE                                               */

/*                                                                                                                     */

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */

/*  =======     ===========     ==     ===================================================================             */

/*    1         2019-10-18      KS     Initial Version                                                                 */

/*    2         2020-01-28      KS     Optional Log Error Parameter                                                    */

/*    3         2020-03-02      KS     Standardised Parameters                                                         */

/*                                                                                                                     */

/***********************************************************************************************************************/

/**********************GRANTS**************************

grant select on pre_advice_line to TORQUE;
grant select on IF_PAL_PK_SEQ to TORQUE;
grant select on interface_pre_advice_line to TORQUE;

grant insert on interface_update_columns to TORQUE;
grant insert on interface_pre_advice_line to TORQUE;

grant execute on libsession to TORQUE;
grant execute on libmergepreadvice to TORQUE;
grant execute on liberror to TORQUE;

******************************************************/
FUNCTION Interface_pre_advice_line (
  in_update_columns VARCHAR2 DEFAULT NULL, 
  in_merge_action VARCHAR2 DEFAULT 'U', 
  in_client_id VARCHAR2, 
  in_pre_advice_id VARCHAR2, 
  in_line_id NUMBER DEFAULT NULL, 
  in_host_pre_advice_id VARCHAR2 DEFAULT NULL, 
  in_host_line_id VARCHAR2 DEFAULT NULL, 
  in_sku_id VARCHAR2, 
  in_config_id VARCHAR2 DEFAULT NULL, 
  in_batch_id VARCHAR2 DEFAULT NULL, 
  in_expiry_dstamp VARCHAR2 DEFAULT NULL, 
  in_manuf_dstamp VARCHAR2 DEFAULT NULL, 
  in_pallet_config VARCHAR2 DEFAULT NULL, 
  in_origin_id VARCHAR2 DEFAULT NULL, 
  in_condition_id VARCHAR2 DEFAULT NULL, 
  in_tag_id VARCHAR2 DEFAULT NULL, 
  in_lock_code VARCHAR2 DEFAULT NULL, 
  in_spec_code VARCHAR2 DEFAULT NULL, 
  in_qty_due NUMBER DEFAULT NULL, 
  in_notes VARCHAR2 DEFAULT NULL, 
  in_disallow_merge_rules VARCHAR2 DEFAULT NULL, 
  in_user_def_type_1 VARCHAR2 DEFAULT NULL, 
  in_user_def_type_2 VARCHAR2 DEFAULT NULL, 
  in_user_def_type_3 VARCHAR2 DEFAULT NULL, 
  in_user_def_type_4 VARCHAR2 DEFAULT NULL, 
  in_user_def_type_5 VARCHAR2 DEFAULT NULL, 
  in_user_def_type_6 VARCHAR2 DEFAULT NULL, 
  in_user_def_type_7 VARCHAR2 DEFAULT NULL, 
  in_user_def_type_8 VARCHAR2 DEFAULT NULL, 
  in_user_def_chk_1 VARCHAR2 DEFAULT NULL, 
  in_user_def_chk_2 VARCHAR2 DEFAULT NULL, 
  in_user_def_chk_3 VARCHAR2 DEFAULT NULL, 
  in_user_def_chk_4 VARCHAR2 DEFAULT NULL, 
  in_user_def_date_1 VARCHAR2 DEFAULT NULL, 
  in_user_def_date_2 VARCHAR2 DEFAULT NULL, 
  in_user_def_date_3 VARCHAR2 DEFAULT NULL, 
  in_user_def_date_4 VARCHAR2 DEFAULT NULL, 
  in_user_def_num_1 NUMBER DEFAULT NULL, 
  in_user_def_num_2 NUMBER DEFAULT NULL, 
  in_user_def_num_3 NUMBER DEFAULT NULL, 
  in_user_def_num_4 NUMBER DEFAULT NULL, 
  in_user_def_note_1 VARCHAR2 DEFAULT NULL, 
  in_user_def_note_2 VARCHAR2 DEFAULT NULL, 
  in_time_zone_name VARCHAR2 DEFAULT NULL, 
  in_client_group VARCHAR2 DEFAULT NULL, 
  in_tracking_level VARCHAR2 DEFAULT NULL, 
  in_qty_due_tolerance NUMBER DEFAULT NULL, 
  in_ce_coo VARCHAR2 DEFAULT NULL, 
  in_owner_id VARCHAR2 DEFAULT NULL, 
  in_ce_consignment_id VARCHAR2 DEFAULT NULL, 
  in_ce_under_bond VARCHAR2 DEFAULT NULL, 
  in_ce_link VARCHAR2 DEFAULT NULL, 
  in_product_price NUMBER DEFAULT NULL, 
  in_product_currency VARCHAR2 DEFAULT NULL, 
  in_ce_invoice_number VARCHAR2 DEFAULT NULL, 
  in_serial_valid_merge VARCHAR2 DEFAULT NULL, 
  in_sampling_type VARCHAR2 DEFAULT NULL, 
  in_expected_net_weight NUMBER DEFAULT NULL, 
  in_expected_gross_weight NUMBER DEFAULT NULL, 
  in_find_line_id VARCHAR2 DEFAULT 'Y',
  in_user_id VARCHAR2 DEFAULT 'API-INT', 
  in_station_id VARCHAR2 DEFAULT 'API-INT',
  in_nls_calendar VARCHAR2 DEFAULT NULL,
  in_record_error_in_table VARCHAR2 DEFAULT NULL
) RETURN VARCHAR2 IS PRAGMA autonomous_transaction;
v_retval NUMBER;
v_error VARCHAR2(20);
v_expiry_dstamp TIMESTAMP;
v_manuf_dstamp TIMESTAMP;
v_user_def_date_1 TIMESTAMP;
v_user_def_date_2 TIMESTAMP;
v_user_def_date_3 TIMESTAMP;
v_user_def_date_4 TIMESTAMP;
v_line_id NUMBER;
v_interface_key NUMBER;
BEGIN 

dcsdba.libsession.initialisesession(
  in_user_id, 'INT-API', in_station_id, 
  'INT-API'
);

IF in_expiry_dstamp IS NOT NULL THEN v_expiry_dstamp := To_timestamp(
  in_expiry_dstamp, 'YYYYMMDDHH24MISS'
);
END IF;
IF in_manuf_dstamp IS NOT NULL THEN v_manuf_dstamp := To_timestamp(
  in_manuf_dstamp, 'YYYYMMDDHH24MISS'
);
END IF;
IF in_user_def_date_1 IS NOT NULL THEN v_user_def_date_1 := To_timestamp(
  in_user_def_date_1, 'YYYYMMDDHH24MISS'
);
END IF;
IF in_user_def_date_2 IS NOT NULL THEN v_user_def_date_2 := To_timestamp(
  in_user_def_date_2, 'YYYYMMDDHH24MISS'
);
END IF;
IF in_user_def_date_3 IS NOT NULL THEN v_user_def_date_3 := To_timestamp(
  in_user_def_date_3, 'YYYYMMDDHH24MISS'
);
END IF;
IF in_user_def_date_4 IS NOT NULL THEN v_user_def_date_4 := To_timestamp(
  in_user_def_date_4, 'YYYYMMDDHH24MISS'
);
END IF;

v_line_id := in_line_id;

--Find Line ID from SKU If Exists
IF v_line_id is null AND in_find_line_id = 'Y' then
SELECT MIN(line_id) into v_line_id  from(
SELECT MIN(line_id) as line_id  from dcsdba.pre_advice_line where client_id = in_client_id and pre_advice_id = in_pre_advice_id AND sku_id = in_sku_id
UNION
SELECT MIN(line_id)  from dcsdba.interface_pre_advice_line where client_id = in_client_id and pre_advice_id = in_pre_advice_id AND sku_id = in_sku_id);
END IF;

--Calculate Next Available Line ID
IF v_line_id is null AND in_find_line_id = 'Y' then
SELECT MAX(line_id) into v_line_id  from(
SELECT nvl(MAX(line_id),0)+1 as line_id  from dcsdba.pre_advice_line where client_id = in_client_id and pre_advice_id = in_pre_advice_id
UNION
SELECT nvl(MAX(line_id),0)+1  from dcsdba.interface_pre_advice_line where client_id = in_client_id and pre_advice_id = in_pre_advice_id);
END IF;

v_retval := dcsdba.libmergepreadvice.directpreadviceline(
  p_mergeerror => v_error, 
  p_ToUpdateCols => in_update_columns, 
  p_MergeAction => in_merge_action, 
  p_ClientId => in_client_id, 
  p_PreAdviceId => in_pre_advice_id, 
  p_LineId => v_line_id, 
  p_HostPreAdviceId => in_host_pre_advice_id, 
  p_HostLineId => in_host_line_id, 
  p_SkuId => in_sku_id, 
  p_ConfigId => in_config_id, 
  p_BatchId => in_batch_id, 
  p_ExpiryDstamp => v_expiry_dstamp, 
  p_ManufDstamp => v_manuf_dstamp, 
  p_PalletConfig => in_pallet_config, 
  p_OriginId => in_origin_id, 
  p_ConditionId => in_condition_id, 
  p_TagId => in_tag_id, 
  p_LockCode => in_lock_code, 
  p_SpecCode => in_spec_code, 
  p_QtyDue => in_qty_due, 
  p_Notes => in_notes, 
  p_DisallowMergeRules => in_disallow_merge_rules, 
  p_UserDefType1 => in_user_def_type_1, 
  p_UserDefType2 => in_user_def_type_2, 
  p_UserDefType3 => in_user_def_type_3, 
  p_UserDefType4 => in_user_def_type_4, 
  p_UserDefType5 => in_user_def_type_5, 
  p_UserDefType6 => in_user_def_type_6, 
  p_UserDefType7 => in_user_def_type_7, 
  p_UserDefType8 => in_user_def_type_8, 
  p_UserDefChk1 => in_user_def_chk_1, 
  p_UserDefChk2 => in_user_def_chk_2, 
  p_UserDefChk3 => in_user_def_chk_3, 
  p_UserDefChk4 => in_user_def_chk_4, 
  p_UserDefDate1 => v_user_def_date_1, 
  p_UserDefDate2 => v_user_def_date_2, 
  p_UserDefDate3 => v_user_def_date_3, 
  p_UserDefDate4 => v_user_def_date_4, 
  p_UserDefNum1 => in_user_def_num_1, 
  p_UserDefNum2 => in_user_def_num_2, 
  p_UserDefNum3 => in_user_def_num_3, 
  p_UserDefNum4 => in_user_def_num_4, 
  p_UserDefNote1 => in_user_def_note_1, 
  p_UserDefNote2 => in_user_def_note_2, 
  p_TimeZoneName => in_time_zone_name, 
  p_ClientGroup => in_client_group, 
  p_TrackingLevel => in_tracking_level, 
  p_QtyDueTolerance => in_qty_due_tolerance, 
  p_CeCoo => in_ce_coo, 
  p_OwnerId => in_owner_id, 
  p_CeConsignmentId => in_ce_consignment_id, 
  p_CeUnderBond => in_ce_under_bond, 
  p_CeLink => in_ce_link, 
  p_ProductPrice => in_product_price, 
  p_ProductCurrency => in_product_currency, 
  p_CeInvoiceNumber => in_ce_invoice_number, 
  p_SerialValidMerge => in_serial_valid_merge, 
  p_SamplingType => in_sampling_type, 
  p_ExpectedNetWeight => in_expected_net_weight, 
  p_ExpectedGrossWeight => in_expected_gross_weight
);
COMMIT;

IF v_error is not null AND in_record_error_in_table = 'Y' then
v_interface_key := DCSDBA.IF_PAL_PK_SEQ.nextval;
insert into dcsdba.interface_update_columns(key,table_name,update_columns)VALUES(v_interface_key,'interface_pre_advice_line',nvl(in_update_columns,':pre_advice_id::line_id::sku_id::config_id::batch_id::expiry_dstamp::pallet_config::condition_id::qty_due::lock_code::tag_id::origin_id::notes::manuf_dstamp::client_id::disallow_merge_rules::user_def_type_1::user_def_type_2::user_def_type_3::user_def_type_4::user_def_type_5::user_def_type_6::user_def_type_7::user_def_type_8::user_def_chk_1::user_def_chk_2::user_def_chk_3::user_def_chk_4::user_def_date_1::user_def_date_2::user_def_date_3::user_def_date_4::user_def_num_1::user_def_num_2::user_def_num_3::user_def_num_4::user_def_note_1::user_def_note_2::time_zone_name::spec_code::client_group::tracking_level::host_pre_advice_id::host_line_id::qty_due_tolerance::ce_coo::owner_id::nls_calendar:'));
insert into dcsdba.interface_pre_advice_line(Key, Merge_Dstamp, Merge_Status,Merge_Error, Merge_Action, Batch_Id, Ce_Consignment_Id, Ce_Coo, Ce_Invoice_Number, Ce_Link, Ce_Under_Bond, Client_Group, Client_Id, Condition_Id, Config_Id, Disallow_Merge_Rules, Expected_Gross_Weight, Expected_Net_Weight, Expiry_Dstamp, Host_Line_Id, Host_Pre_Advice_Id, Line_Id, Lock_Code, Manuf_Dstamp, Notes, Origin_Id, Owner_Id, Pallet_Config, Pre_Advice_Id, Product_Currency, Product_Price, Qty_Due, Qty_Due_Tolerance, Sampling_Type, Serial_Valid_Merge, Sku_Id, Spec_Code, Tag_Id, Time_Zone_Name, Tracking_Level, User_Def_Chk_1, User_Def_Chk_2, User_Def_Chk_3, User_Def_Chk_4, User_Def_Date_1, User_Def_Date_2, User_Def_Date_3, User_Def_Date_4, User_Def_Note_1, User_Def_Note_2, User_Def_Num_1, User_Def_Num_2, User_Def_Num_3, User_Def_Num_4, User_Def_Type_1, User_Def_Type_2, User_Def_Type_3, User_Def_Type_4, User_Def_Type_5, User_Def_Type_6, User_Def_Type_7, User_Def_Type_8)
VALUES(v_interface_key, sysdate, 'Error', v_error, IN_Merge_Action, IN_Batch_Id, IN_Ce_Consignment_Id, IN_Ce_Coo, IN_Ce_Invoice_Number, IN_Ce_Link, IN_Ce_Under_Bond, IN_Client_Group, IN_Client_Id, IN_Condition_Id, IN_Config_Id, IN_Disallow_Merge_Rules, IN_Expected_Gross_Weight, IN_Expected_Net_Weight, v_Expiry_Dstamp, IN_Host_Line_Id, IN_Host_Pre_Advice_Id, v_Line_Id, IN_Lock_Code, v_Manuf_Dstamp, IN_Notes, IN_Origin_Id, IN_Owner_Id, IN_Pallet_Config, IN_Pre_Advice_Id, IN_Product_Currency, IN_Product_Price, IN_Qty_Due, IN_Qty_Due_Tolerance, IN_Sampling_Type, IN_Serial_Valid_Merge, IN_Sku_Id, IN_Spec_Code, IN_Tag_Id, IN_Time_Zone_Name, IN_Tracking_Level, IN_User_Def_Chk_1, IN_User_Def_Chk_2, IN_User_Def_Chk_3, IN_User_Def_Chk_4, v_User_Def_Date_1, v_User_Def_Date_2, v_User_Def_Date_3, v_User_Def_Date_4, IN_User_Def_Note_1, IN_User_Def_Note_2, IN_User_Def_Num_1, IN_User_Def_Num_2, IN_User_Def_Num_3, IN_User_Def_Num_4, IN_User_Def_Type_1, IN_User_Def_Type_2, IN_User_Def_Type_3, IN_User_Def_Type_4, IN_User_Def_Type_5, IN_User_Def_Type_6, IN_User_Def_Type_7, IN_User_Def_Type_8);
commit;
END IF;

RETURN v_error;

END interface_pre_advice_line;





/***********************************************************************************************************************/

/*                                                                                                                     */

/*  NAME:               TQ_INTERFACE.INTERFACE_RECEIPT					                                               */

/*                                                                                                                     */

/*  DESCRIPTION:        This Function Uses The Interface PRE_ADVICE_LINE                                               */

/*                                                                                                                     */

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */

/*  =======     ===========     ==     ===================================================================             */

/*    1         2020-02-10      KS     Initial Version                                                                 */

/*    2         2020-03-02      KS     Standardised Parameters                                                         */

/*                                                                                                                     */

/***********************************************************************************************************************/

/**********************GRANTS**************************

grant select on IF_REC_PK_SEQ to TORQUE;
grant select on interface_receipt to TORQUE;
grant select on pre_advice_line to TORQUE;

grant insert on interface_update_columns to TORQUE;
grant insert on interface_receipt to TORQUE;

grant update on pre_advice_line to TORQUE;

grant execute on libsession to TORQUE;
grant execute on libmergereceipt to TORQUE;
grant execute on liberror to TORQUE;

******************************************************/
FUNCTION Interface_receipt (
	in_update_columns   VARCHAR2 DEFAULT NULL,
	in_merge_action   VARCHAR2 DEFAULT 'A', 
	in_site_id VARCHAR2, 
	in_location_id   VARCHAR2, 
	in_owner_id   VARCHAR2, 
	in_client_id   VARCHAR2, 
	in_sku_id   VARCHAR2, 
	in_config_id   VARCHAR2 DEFAULT NULL, 
	in_tag_id   VARCHAR2 DEFAULT NULL, 
	in_qty_on_hand  NUMBER, 
	in_batch_id   VARCHAR2 DEFAULT NULL, 
	in_expiry_dstamp   VARCHAR2 DEFAULT NULL, 
	in_manuf_dstamp   VARCHAR2 DEFAULT NULL, 
	in_receipt_id   VARCHAR2 DEFAULT NULL, 
	in_supplier_id   VARCHAR2 DEFAULT NULL, 
	in_origin_id   VARCHAR2 DEFAULT NULL, 
	in_condition_id   VARCHAR2 DEFAULT NULL, 
	in_lock_code   VARCHAR2 DEFAULT NULL, 
	in_tag_copies   INTEGER DEFAULT NULL,
	in_receipt_dstamp VARCHAR2 DEFAULT NULL, 
	in_pallet_id   VARCHAR2 DEFAULT NULL, 
	in_pallet_config   VARCHAR2 DEFAULT 'MD', 
	in_pallet_volume   NUMBER DEFAULT NULL, 
	in_pallet_height   NUMBER DEFAULT NULL, 
	in_pallet_depth   NUMBER DEFAULT NULL, 
	in_pallet_width   NUMBER DEFAULT NULL, 
	in_pallet_weight   NUMBER DEFAULT NULL, 
	in_pallet_grouped   VARCHAR2 DEFAULT NULL, 
	in_notes   VARCHAR2 DEFAULT NULL, 
	in_sampling_type   VARCHAR2 DEFAULT NULL, 
	in_user_def_type_1   VARCHAR2 DEFAULT NULL, 
	in_user_def_type_2   VARCHAR2 DEFAULT NULL, 
	in_user_def_type_3   VARCHAR2 DEFAULT NULL, 
	in_user_def_type_4   VARCHAR2 DEFAULT NULL, 
	in_user_def_type_5   VARCHAR2 DEFAULT NULL, 
	in_user_def_type_6   VARCHAR2 DEFAULT NULL, 
	in_user_def_type_7   VARCHAR2 DEFAULT NULL, 
	in_user_def_type_8   VARCHAR2 DEFAULT NULL, 
	in_user_def_chk_1   VARCHAR2 DEFAULT NULL, 
	in_user_def_chk_2   VARCHAR2 DEFAULT NULL, 
	in_user_def_chk_3   VARCHAR2 DEFAULT NULL, 
	in_user_def_chk_4   VARCHAR2 DEFAULT NULL, 
	in_user_def_date_1   VARCHAR2 DEFAULT NULL, 
	in_user_def_date_2   VARCHAR2 DEFAULT NULL, 
	in_user_def_date_3   VARCHAR2 DEFAULT NULL, 
	in_user_def_date_4   VARCHAR2 DEFAULT NULL, 
	in_user_def_num_1   NUMBER DEFAULT NULL, 
	in_user_def_num_2   NUMBER DEFAULT NULL, 
	in_user_def_num_3   NUMBER DEFAULT NULL, 
	in_user_def_num_4   NUMBER DEFAULT NULL, 
	in_user_def_note_1   VARCHAR2 DEFAULT NULL, 
	in_user_def_note_2   VARCHAR2 DEFAULT NULL, 
	in_time_zone_name   VARCHAR2 DEFAULT NULL, 
	in_client_group   VARCHAR2 DEFAULT NULL, 
	in_tracking_level   VARCHAR2 DEFAULT NULL, 
	in_ce_consignment_id   VARCHAR2 DEFAULT NULL, 
	in_ce_receipt_type   VARCHAR2 DEFAULT NULL, 
	in_ce_originator   VARCHAR2 DEFAULT NULL, 
	in_ce_originator_ref   VARCHAR2 DEFAULT NULL, 
	in_ce_coo   VARCHAR2 DEFAULT NULL, 
	in_ce_cwc   VARCHAR2 DEFAULT NULL, 
	in_ce_ucr   VARCHAR2 DEFAULT NULL, 
	in_ce_under_bond   VARCHAR2 DEFAULT NULL, 
	in_ce_document_dstamp   VARCHAR2 DEFAULT NULL, 
	in_spec_code   VARCHAR2 DEFAULT NULL, 
	in_ce_duty_stamp   VARCHAR2 DEFAULT NULL, 
    in_user_id VARCHAR2 DEFAULT 'API-INT', 
    in_station_id VARCHAR2 DEFAULT 'API-INT',
    in_nls_calendar VARCHAR2 DEFAULT NULL,
    in_record_error_in_table VARCHAR2 DEFAULT NULL,
	in_update_pre_advice VARCHAR2 DEFAULT 'N',
	in_line_id NUMBER DEFAULT NULL
) RETURN VARCHAR2 IS 

PRAGMA autonomous_transaction;

v_retval NUMBER;
v_error VARCHAR2(20);
v_receipt_dstamp TIMESTAMP;
v_ce_document_dstamp TIMESTAMP;
v_expiry_dstamp TIMESTAMP;
v_manuf_dstamp TIMESTAMP;
v_user_def_date_1 TIMESTAMP;
v_user_def_date_2 TIMESTAMP;
v_user_def_date_3 TIMESTAMP;
v_user_def_date_4 TIMESTAMP;
v_line_id NUMBER;
v_interface_key NUMBER;
v_check_line NUMBER;

BEGIN 

dcsdba.libsession.initialisesession(
  in_user_id, 'INT-API', in_station_id, 
  'INT-API'
);

IF in_receipt_dstamp IS NOT NULL THEN v_receipt_dstamp := To_timestamp(
  in_receipt_dstamp, 'YYYYMMDDHH24MISS'
);
END IF;
IF in_ce_document_dstamp IS NOT NULL THEN v_ce_document_dstamp := To_timestamp(
  in_ce_document_dstamp, 'YYYYMMDDHH24MISS'
);
END IF;
IF in_expiry_dstamp IS NOT NULL THEN v_expiry_dstamp := To_timestamp(
  in_expiry_dstamp, 'YYYYMMDDHH24MISS'
);
END IF;
IF in_manuf_dstamp IS NOT NULL THEN v_manuf_dstamp := To_timestamp(
  in_manuf_dstamp, 'YYYYMMDDHH24MISS'
);
END IF;
IF in_user_def_date_1 IS NOT NULL THEN v_user_def_date_1 := To_timestamp(
  in_user_def_date_1, 'YYYYMMDDHH24MISS'
);
END IF;
IF in_user_def_date_2 IS NOT NULL THEN v_user_def_date_2 := To_timestamp(
  in_user_def_date_2, 'YYYYMMDDHH24MISS'
);
END IF;
IF in_user_def_date_3 IS NOT NULL THEN v_user_def_date_3 := To_timestamp(
  in_user_def_date_3, 'YYYYMMDDHH24MISS'
);
END IF;
IF in_user_def_date_4 IS NOT NULL THEN v_user_def_date_4 := To_timestamp(
  in_user_def_date_4, 'YYYYMMDDHH24MISS'
);
END IF;

v_line_id := in_line_id;

/* Identify Line ID From Client/Pre-Advice/SKU IF NULL */
IF in_update_pre_advice = 'Y' AND in_line_id is null then
	BEGIN
		
		select min(line_id) into v_line_id from dcsdba.pre_advice_line where client_id = upper(in_client_id) AND pre_advice_id= upper(in_receipt_id) AND sku_id = upper(in_sku_id);

		EXCEPTION when others then
			v_error := 'TQF0001';
	END;
END IF;

/*Check Line Exists for Client/PreAdvice/SKU/Line */
IF in_update_pre_advice = 'Y' then
    BEGIN
    select COUNT(*) into v_check_line from dcsdba.pre_advice_line where client_id = upper(in_client_id) AND pre_advice_id= upper(in_receipt_id) AND sku_id = upper(in_sku_id) AND line_id = v_line_id;
        IF v_check_line != 1 then
            v_error := 'TQF0001';
        END IF;
    EXCEPTION when others then
			v_error := 'TQF0001';
    END;
END IF;

IF v_error is null then
v_retval := dcsdba.libmergereceipt.directreceipt(
 p_MergeError  => v_error,
 p_ToUpdateCols  => in_update_columns ,
 p_MergeAction  => in_merge_action ,
 p_SiteId  => in_site_id ,
 p_LocationId  => in_location_id ,
 p_OwnerId  => in_owner_id ,
 p_ClientId  => in_client_id ,
 p_SkuId  => in_sku_id ,
 p_ConfigId  => in_config_id ,
 p_TagId  => in_tag_id ,
 p_QtyOnHand  => in_qty_on_hand ,
 p_BatchId  => in_batch_id ,
 p_ExpiryDstamp  => v_expiry_dstamp ,
 p_ManufDstamp  => v_manuf_dstamp ,
 p_ReceiptId  => in_receipt_id ,
 p_SupplierId  => in_supplier_id ,
 p_OriginId  => in_origin_id ,
 p_ConditionId  => in_condition_id ,
 p_LockCode  => in_lock_code ,
 p_TagCopies  => in_tag_copies ,
 p_ReceiptDstamp  => v_receipt_dstamp ,
 p_PalletId  => in_pallet_id ,
 p_PalletConfig  => in_pallet_config ,
 p_PalletVolume  => in_pallet_volume ,
 p_PalletHeight  => in_pallet_height ,
 p_PalletDepth  => in_pallet_depth ,
 p_PalletWidth  => in_pallet_width ,
 p_PalletWeight  => in_pallet_weight ,
 p_PalletGrouped  => in_pallet_grouped ,
 p_Notes  => in_notes ,
 p_SamplingType  => in_sampling_type ,
 p_UserDefType1  => in_user_def_type_1 ,
 p_UserDefType2  => in_user_def_type_2 ,
 p_UserDefType3  => in_user_def_type_3 ,
 p_UserDefType4  => in_user_def_type_4 ,
 p_UserDefType5  => in_user_def_type_5 ,
 p_UserDefType6  => in_user_def_type_6 ,
 p_UserDefType7  => in_user_def_type_7 ,
 p_UserDefType8  => in_user_def_type_8 ,
 p_UserDefChk1  => in_user_def_chk_1 ,
 p_UserDefChk2  => in_user_def_chk_2 ,
 p_UserDefChk3  => in_user_def_chk_3 ,
 p_UserDefChk4  => in_user_def_chk_4 ,
 p_UserDefDate1  => v_user_def_date_1 ,
 p_UserDefDate2  => v_user_def_date_2,
 p_UserDefDate3  => v_user_def_date_3,
 p_UserDefDate4  => v_user_def_date_4,
 p_UserDefNum1  => in_user_def_num_1 ,
 p_UserDefNum2  => in_user_def_num_2 ,
 p_UserDefNum3  => in_user_def_num_3 ,
 p_UserDefNum4  => in_user_def_num_4 ,
 p_UserDefNote1  => in_user_def_note_1 ,
 p_UserDefNote2  => in_user_def_note_2 ,
 p_TimeZoneName  => in_time_zone_name ,
 p_ClientGroup  => in_client_group ,
 p_TrackingLevel  => in_tracking_level ,
 p_CeConsignmentId  => in_ce_consignment_id ,
 p_CeReceiptType  => in_ce_receipt_type ,
 p_CeOriginator  => in_ce_originator ,
 p_CeOriginatorRef  => in_ce_originator_ref ,
 p_CeCoo  => in_ce_coo ,
 p_CeCwc  => in_ce_cwc ,
 p_CeUcr  => in_ce_ucr ,
 p_CeUnderBond  => in_ce_under_bond ,
 p_CeDocumentDstamp  => v_ce_document_dstamp ,
 p_SpecCode  => in_spec_code ,
 p_CeDutyStamp  => in_ce_duty_stamp
);
COMMIT;
END IF;

IF in_update_pre_advice = 'Y' AND v_error is null then

update dcsdba.pre_advice_line set qty_received = (nvl(qty_received,0) + in_qty_on_hand) where client_id = upper(in_client_id) AND pre_advice_id = upper(in_receipt_id) AND sku_id = upper(in_sku_id) AND line_id = v_line_id;
commit;

END IF;

IF v_error is not null AND in_record_error_in_table = 'Y' AND in_update_pre_advice = 'N' then
v_interface_key := DCSDBA.IF_REC_PK_SEQ.nextval;
insert into dcsdba.interface_update_columns(key,table_name,update_columns)VALUES(v_interface_key,'interface_receipt',nvl(in_update_columns,'KEY:SITE_ID:LOCATION_ID:OWNER_ID:CLIENT_ID:SKU_ID:CONFIG_ID:TAG_ID:QTY_ON_HAND:BATCH_ID:EXPIRY_DSTAMP:MANUF_DSTAMP:RECEIPT_ID:LINE_ID:SUPPLIER_ID:ORIGIN_ID:CONDITION_ID:LOCK_CODE:TAG_COPIES:RECEIPT_DSTAMP:PALLET_ID:PALLET_CONFIG:PALLET_VOLUME:PALLET_HEIGHT:PALLET_DEPTH:PALLET_WIDTH:PALLET_WEIGHT:PALLET_GROUPED:NOTES:SAMPLING_TYPE:USER_DEF_TYPE_1:USER_DEF_TYPE_2:USER_DEF_TYPE_3:USER_DEF_TYPE_4:USER_DEF_TYPE_5:USER_DEF_TYPE_6:USER_DEF_TYPE_7:USER_DEF_TYPE_8:USER_DEF_CHK_1:USER_DEF_CHK_2:USER_DEF_CHK_3:USER_DEF_CHK_4:USER_DEF_DATE_1:USER_DEF_DATE_2:USER_DEF_DATE_3:USER_DEF_DATE_4:USER_DEF_NUM_1:USER_DEF_NUM_2:USER_DEF_NUM_3:USER_DEF_NUM_4:USER_DEF_NOTE_1:USER_DEF_NOTE_2:TRACKING_LEVEL:CE_CONSIGNMENT_ID:CE_RECEIPT_TYPE:CE_ORIGINATOR:CE_ORIGINATOR_REF:CE_COO:CE_CWC:CE_UCR:CE_UNDER_BOND:CE_DOCUMENT_DSTAMP:CE_DUTY_STAMP:SPEC_CODE:SESSION_TIME_ZONE_NAME:TIME_ZONE_NAME:NLS_CALENDAR:CLIENT_GROUP:MERGE_ACTION:MERGE_STATUS:MERGE_ERROR:MERGE_DSTAMP'));
insert into dcsdba.interface_receipt(KEY,	MERGE_DSTAMP,	MERGE_STATUS,	MERGE_ERROR,	MERGE_ACTION,	SITE_ID,	LOCATION_ID,	OWNER_ID,	CLIENT_ID,	SKU_ID,	CONFIG_ID,	TAG_ID,	QTY_ON_HAND,	BATCH_ID,	EXPIRY_DSTAMP,	MANUF_DSTAMP,	RECEIPT_ID,	LINE_ID,	SUPPLIER_ID,	ORIGIN_ID,	CONDITION_ID,	LOCK_CODE,	TAG_COPIES,	RECEIPT_DSTAMP,	PALLET_ID,	PALLET_CONFIG,	PALLET_VOLUME,	PALLET_HEIGHT,	PALLET_DEPTH,	PALLET_WIDTH,	PALLET_WEIGHT,	PALLET_GROUPED,	NOTES,	SAMPLING_TYPE,	USER_DEF_TYPE_1,	USER_DEF_TYPE_2,	USER_DEF_TYPE_3,	USER_DEF_TYPE_4,	USER_DEF_TYPE_5,	USER_DEF_TYPE_6,	USER_DEF_TYPE_7,	USER_DEF_TYPE_8,	USER_DEF_CHK_1,	USER_DEF_CHK_2,	USER_DEF_CHK_3,	USER_DEF_CHK_4,	USER_DEF_DATE_1,	USER_DEF_DATE_2,	USER_DEF_DATE_3,	USER_DEF_DATE_4,	USER_DEF_NUM_1,	USER_DEF_NUM_2,	USER_DEF_NUM_3,	USER_DEF_NUM_4,	USER_DEF_NOTE_1,	USER_DEF_NOTE_2,	TRACKING_LEVEL,	CE_CONSIGNMENT_ID,	CE_RECEIPT_TYPE,	CE_ORIGINATOR,	CE_ORIGINATOR_REF,	CE_COO,	CE_CWC,	CE_UCR,	CE_UNDER_BOND,	CE_DOCUMENT_DSTAMP,	CE_DUTY_STAMP,	SPEC_CODE,	CLIENT_GROUP)
VALUES(v_interface_key,	sysdate,	'Error',	v_error,	IN_MERGE_ACTION,	IN_SITE_ID,	IN_LOCATION_ID,	IN_OWNER_ID,	IN_CLIENT_ID,	IN_SKU_ID,	IN_CONFIG_ID,	IN_TAG_ID,	IN_QTY_ON_HAND,	IN_BATCH_ID,	v_EXPIRY_DSTAMP,	v_MANUF_DSTAMP,	IN_RECEIPT_ID,	v_LINE_ID,	IN_SUPPLIER_ID,	IN_ORIGIN_ID,	IN_CONDITION_ID,	IN_LOCK_CODE,	IN_TAG_COPIES,	v_RECEIPT_DSTAMP,	IN_PALLET_ID,	IN_PALLET_CONFIG,	IN_PALLET_VOLUME,	IN_PALLET_HEIGHT,	IN_PALLET_DEPTH,	IN_PALLET_WIDTH,	IN_PALLET_WEIGHT,	IN_PALLET_GROUPED,	IN_NOTES,	IN_SAMPLING_TYPE,	IN_USER_DEF_TYPE_1,	IN_USER_DEF_TYPE_2,	IN_USER_DEF_TYPE_3,	IN_USER_DEF_TYPE_4,	IN_USER_DEF_TYPE_5,	IN_USER_DEF_TYPE_6,	IN_USER_DEF_TYPE_7,	IN_USER_DEF_TYPE_8,	IN_USER_DEF_CHK_1,	IN_USER_DEF_CHK_2,	IN_USER_DEF_CHK_3,	IN_USER_DEF_CHK_4,	v_USER_DEF_DATE_1,	v_USER_DEF_DATE_2,	v_USER_DEF_DATE_3,	v_USER_DEF_DATE_4,	IN_USER_DEF_NUM_1,	IN_USER_DEF_NUM_2,	IN_USER_DEF_NUM_3,	IN_USER_DEF_NUM_4,	IN_USER_DEF_NOTE_1,	IN_USER_DEF_NOTE_2,	IN_TRACKING_LEVEL,	IN_CE_CONSIGNMENT_ID,	IN_CE_RECEIPT_TYPE,	IN_CE_ORIGINATOR,	IN_CE_ORIGINATOR_REF,	IN_CE_COO,	IN_CE_CWC,	IN_CE_UCR,	IN_CE_UNDER_BOND,	v_CE_DOCUMENT_DSTAMP,	IN_CE_DUTY_STAMP,	IN_SPEC_CODE,	IN_CLIENT_GROUP);
commit;
END IF;

RETURN v_error;


END Interface_receipt;


/***********************************************************************************************************************/

/*                                                                                                                     */

/*  NAME:               TQ_INTERFACE.INTERFACE_ORDER_HEADER                                                            */

/*                                                                                                                     */

/*  DESCRIPTION:        This Function Uses The Interface Order Header API to Add Order Headers to the System           */

/*                                                                                                                     */

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */

/*  =======     ===========     ==     ===================================================================             */

/*    1         2020-02-27      KS     Initial Version                                                                 */

/***********************************************************************************************************************/

/**********************GRANTS**************************

grant select on IF_OH_PK_SEQ TO TORQUE;
grant select on interface_order_header to TORQUE;

grant insert on interface_update_columns to TORQUE;
grant insert on interface_order_header to TORQUE;

grant delete on interface_update_columns to TORQUE;
grant delete on interface_order_header to TORQUE;

grant execute on libsession to TORQUE;
grant execute on libmergeorder to TORQUE;
grant execute on liberror to TORQUE;


******************************************************/

FUNCTION INTERFACE_ORDER_HEADER (
IN_UPDATE_COLUMNS VARCHAR2 DEFAULT NULL,
IN_MERGE_ACTION VARCHAR2 DEFAULT 'U',
IN_ORDER_ID VARCHAR2 ,
IN_ORDER_TYPE VARCHAR2 DEFAULT NULL,
IN_STATUS VARCHAR2 DEFAULT NULL,
IN_PRIORITY VARCHAR2 DEFAULT NULL,
IN_SHIP_DOCK VARCHAR2 DEFAULT NULL,
IN_WORK_GROUP VARCHAR2 DEFAULT NULL,
IN_CONSIGNMENT VARCHAR2 DEFAULT NULL,
IN_DELIVERY_POINT VARCHAR2 DEFAULT NULL,
IN_LOAD_SEQUENCE INTEGER DEFAULT NULL,
IN_FROM_SITE_ID VARCHAR2 DEFAULT NULL,
IN_TO_SITE_ID VARCHAR2 DEFAULT NULL,
IN_CUSTOMER_ID VARCHAR2 DEFAULT NULL,
IN_ORDER_DATE VARCHAR2 DEFAULT NULL,
IN_SHIP_BY_DATE VARCHAR2 DEFAULT NULL,
IN_PURCHASE_ORDER VARCHAR2 DEFAULT NULL,
IN_CONTACT VARCHAR2 DEFAULT NULL,
IN_CONTACT_PHONE VARCHAR2 DEFAULT NULL,
IN_CONTACT_FAX VARCHAR2 DEFAULT NULL,
IN_CONTACT_EMAIL VARCHAR2 DEFAULT NULL,
IN_NAME VARCHAR2 DEFAULT NULL,
IN_ADDRESS1 VARCHAR2 DEFAULT NULL,
IN_ADDRESS2 VARCHAR2 DEFAULT NULL,
IN_TOWN VARCHAR2 DEFAULT NULL,
IN_COUNTY VARCHAR2 DEFAULT NULL,
IN_POSTCODE VARCHAR2 DEFAULT NULL,
IN_COUNTRY VARCHAR2 DEFAULT NULL,
IN_INSTRUCTIONS VARCHAR2 DEFAULT NULL,
IN_REPACK VARCHAR2 DEFAULT NULL,
IN_OWNER_ID VARCHAR2 ,
IN_CARRIER_ID VARCHAR2 DEFAULT NULL,
IN_DISPATCH_METHOD VARCHAR2 DEFAULT NULL,
IN_SERVICE_LEVEL VARCHAR2 DEFAULT NULL,
IN_INV_ADDRESS_ID VARCHAR2 DEFAULT NULL,
IN_INV_CONTACT VARCHAR2 DEFAULT NULL,
IN_INV_CONTACT_PHONE VARCHAR2 DEFAULT NULL,
IN_INV_CONTACT_FAX VARCHAR2 DEFAULT NULL,
IN_INV_CONTACT_EMAIL VARCHAR2 DEFAULT NULL,
IN_INV_NAME VARCHAR2 DEFAULT NULL,
IN_INV_ADDRESS1 VARCHAR2 DEFAULT NULL,
IN_INV_ADDRESS2 VARCHAR2 DEFAULT NULL,
IN_INV_TOWN VARCHAR2 DEFAULT NULL,
IN_INV_COUNTY VARCHAR2 DEFAULT NULL,
IN_INV_POSTCODE VARCHAR2 DEFAULT NULL,
IN_INV_COUNTRY VARCHAR2 DEFAULT NULL,
IN_DELIVER_BY_DATE VARCHAR2 DEFAULT NULL,
IN_FASTEST_CARRIER VARCHAR2 DEFAULT NULL,
IN_CHEAPEST_CARRIER VARCHAR2 DEFAULT NULL,
IN_SITE_REPLEN VARCHAR2 DEFAULT NULL,
IN_CID_NUMBER VARCHAR2 DEFAULT NULL,
IN_SID_NUMBER VARCHAR2 DEFAULT NULL,
IN_LOCATION_NUMBER NUMBER DEFAULT NULL,
IN_FREIGHT_CHARGES VARCHAR2 DEFAULT NULL,
IN_CLIENT_ID VARCHAR2 ,
IN_EXPORT VARCHAR2 DEFAULT NULL,
IN_DISALLOW_MERGE_RULES VARCHAR2 DEFAULT NULL,
IN_USER_DEF_TYPE_1 VARCHAR2 DEFAULT NULL,
IN_USER_DEF_TYPE_2 VARCHAR2 DEFAULT NULL,
IN_USER_DEF_TYPE_3 VARCHAR2 DEFAULT NULL,
IN_USER_DEF_TYPE_4 VARCHAR2 DEFAULT NULL,
IN_USER_DEF_TYPE_5 VARCHAR2 DEFAULT NULL,
IN_USER_DEF_TYPE_6 VARCHAR2 DEFAULT NULL,
IN_USER_DEF_TYPE_7 VARCHAR2 DEFAULT NULL,
IN_USER_DEF_TYPE_8 VARCHAR2 DEFAULT NULL,
IN_USER_DEF_CHK_1 VARCHAR2 DEFAULT NULL,
IN_USER_DEF_CHK_2 VARCHAR2 DEFAULT NULL,
IN_USER_DEF_CHK_3 VARCHAR2 DEFAULT NULL,
IN_USER_DEF_CHK_4 VARCHAR2 DEFAULT NULL,
IN_USER_DEF_DATE_1 VARCHAR2 DEFAULT NULL,
IN_USER_DEF_DATE_2 VARCHAR2 DEFAULT NULL,
IN_USER_DEF_DATE_3 VARCHAR2 DEFAULT NULL,
IN_USER_DEF_DATE_4 VARCHAR2 DEFAULT NULL,
IN_USER_DEF_NUM_1 NUMBER DEFAULT NULL,
IN_USER_DEF_NUM_2 NUMBER DEFAULT NULL,
IN_USER_DEF_NUM_3 NUMBER DEFAULT NULL,
IN_USER_DEF_NUM_4 NUMBER DEFAULT NULL,
IN_USER_DEF_NOTE_1 VARCHAR2 DEFAULT NULL,
IN_USER_DEF_NOTE_2 VARCHAR2 DEFAULT NULL,
IN_SOH_ID VARCHAR2 DEFAULT NULL,
IN_MOVE_TASK_STATUS VARCHAR2 DEFAULT NULL,
IN_TIME_ZONE_NAME VARCHAR2 DEFAULT NULL,
IN_REPACK_LOC_ID VARCHAR2 DEFAULT NULL,
IN_CE_REASON_CODE VARCHAR2 DEFAULT NULL,
IN_CE_REASON_NOTES VARCHAR2 DEFAULT NULL,
IN_CE_ORDER_TYPE VARCHAR2 DEFAULT NULL,
IN_CE_CUSTOMS_CUSTOMER VARCHAR2 DEFAULT NULL,
IN_CE_EXCISE_CUSTOMER VARCHAR2 DEFAULT NULL,
IN_CE_INSTRUCTIONS VARCHAR2 DEFAULT NULL,
IN_CLIENT_GROUP VARCHAR2 DEFAULT NULL,
IN_DELIVERED_DSTAMP VARCHAR2 DEFAULT NULL,
IN_SIGNATORY VARCHAR2 DEFAULT NULL,
IN_ROUTE_ID VARCHAR2 DEFAULT NULL,
IN_CROSS_DOCK_TO_SITE VARCHAR2 DEFAULT NULL,
IN_WEB_SERVICE_ALLOC_IMMED VARCHAR2 DEFAULT NULL,
IN_WEB_SERVICE_ALLOC_CLEAN VARCHAR2 DEFAULT NULL,
IN_DISALLOW_SHORT_SHIP VARCHAR2 DEFAULT NULL,
IN_WORK_ORDER_TYPE VARCHAR2 DEFAULT NULL,
IN_STATUS_REASON_CODE VARCHAR2 DEFAULT NULL,
IN_HUB_ADDRESS_ID VARCHAR2 DEFAULT NULL,
IN_HUB_CONTACT VARCHAR2 DEFAULT NULL,
IN_HUB_CONTACT_PHONE VARCHAR2 DEFAULT NULL,
IN_HUB_CONTACT_FAX VARCHAR2 DEFAULT NULL,
IN_HUB_CONTACT_EMAIL VARCHAR2 DEFAULT NULL,
IN_HUB_NAME VARCHAR2 DEFAULT NULL,
IN_HUB_ADDRESS1 VARCHAR2 DEFAULT NULL,
IN_HUB_ADDRESS2 VARCHAR2 DEFAULT NULL,
IN_HUB_TOWN VARCHAR2 DEFAULT NULL,
IN_HUB_COUNTY VARCHAR2 DEFAULT NULL,
IN_HUB_POSTCODE VARCHAR2 DEFAULT NULL,
IN_HUB_COUNTRY VARCHAR2 DEFAULT NULL,
IN_STAGE_ROUTE_ID VARCHAR2 DEFAULT NULL,
IN_SINGLE_ORDER_SORTATION VARCHAR2 DEFAULT NULL,
IN_HUB_CARRIER_ID VARCHAR2 DEFAULT NULL,
IN_HUB_SERVICE_LEVEL VARCHAR2 DEFAULT NULL,
IN_FORCE_SINGLE_CARRIER VARCHAR2 DEFAULT NULL,
IN_EXPECTED_VALUE NUMBER DEFAULT NULL,
IN_EXPECTED_VOLUME NUMBER DEFAULT NULL,
IN_EXPECTED_WEIGHT NUMBER DEFAULT NULL,
IN_TOD VARCHAR2 DEFAULT NULL,
IN_TOD_PLACE VARCHAR2 DEFAULT NULL,
IN_LANGUAGE VARCHAR2 DEFAULT NULL,
IN_SELLER_NAME VARCHAR2 DEFAULT NULL,
IN_SELLER_PHONE VARCHAR2 DEFAULT NULL,
IN_DOCUMENTATION_TEXT_1 VARCHAR2 DEFAULT NULL,
IN_DOCUMENTATION_TEXT_2 VARCHAR2 DEFAULT NULL,
IN_DOCUMENTATION_TEXT_3 VARCHAR2 DEFAULT NULL,
IN_COD VARCHAR2 DEFAULT NULL,
IN_COD_VALUE NUMBER DEFAULT NULL,
IN_COD_CURRENCY VARCHAR2 DEFAULT NULL,
IN_COD_TYPE VARCHAR2 DEFAULT NULL,
IN_VAT_NUMBER VARCHAR2 DEFAULT NULL,
IN_INV_VAT_NUMBER VARCHAR2 DEFAULT NULL,
IN_HUB_VAT_NUMBER VARCHAR2 DEFAULT NULL,
IN_INV_REFERENCE VARCHAR2 DEFAULT NULL,
IN_INV_DSTAMP VARCHAR2 DEFAULT NULL,
IN_INV_CURRENCY VARCHAR2 DEFAULT NULL,
IN_PRINT_INVOICE VARCHAR2 DEFAULT NULL,
IN_LETTER_OF_CREDIT VARCHAR2 DEFAULT NULL,
IN_PAYMENT_TERMS VARCHAR2 DEFAULT NULL,
IN_SUBTOTAL_1 NUMBER DEFAULT NULL,
IN_SUBTOTAL_2 NUMBER DEFAULT NULL,
IN_SUBTOTAL_3 NUMBER DEFAULT NULL,
IN_SUBTOTAL_4 NUMBER DEFAULT NULL,
IN_FREIGHT_COST NUMBER DEFAULT NULL,
IN_FREIGHT_TERMS VARCHAR2 DEFAULT NULL,
IN_INSURANCE_COST NUMBER DEFAULT NULL,
IN_MISC_CHARGES NUMBER DEFAULT NULL,
IN_DISCOUNT NUMBER DEFAULT NULL,
IN_OTHER_FEE NUMBER DEFAULT NULL,
IN_INV_TOTAL_1 NUMBER DEFAULT NULL,
IN_INV_TOTAL_2 NUMBER DEFAULT NULL,
IN_INV_TOTAL_3 NUMBER DEFAULT NULL,
IN_INV_TOTAL_4 NUMBER DEFAULT NULL,
IN_TAX_RATE_1 NUMBER DEFAULT NULL,
IN_TAX_BASIS_1 NUMBER DEFAULT NULL,
IN_TAX_AMOUNT_1 NUMBER DEFAULT NULL,
IN_TAX_RATE_2 NUMBER DEFAULT NULL,
IN_TAX_BASIS_2 NUMBER DEFAULT NULL,
IN_TAX_AMOUNT_2 NUMBER DEFAULT NULL,
IN_TAX_RATE_3 NUMBER DEFAULT NULL,
IN_TAX_BASIS_3 NUMBER DEFAULT NULL,
IN_TAX_AMOUNT_3 NUMBER DEFAULT NULL,
IN_TAX_RATE_4 NUMBER DEFAULT NULL,
IN_TAX_BASIS_4 NUMBER DEFAULT NULL,
IN_TAX_AMOUNT_4 NUMBER DEFAULT NULL,
IN_TAX_RATE_5 NUMBER DEFAULT NULL,
IN_TAX_BASIS_5 NUMBER DEFAULT NULL,
IN_TAX_AMOUNT_5 NUMBER DEFAULT NULL,
IN_ORDER_REFERENCE VARCHAR2 DEFAULT NULL,
IN_START_BY_DATE VARCHAR2 DEFAULT NULL,
IN_METAPACK_CARRIER_PRE VARCHAR2 DEFAULT NULL,
IN_COLLECTIVE_MODE VARCHAR2 DEFAULT NULL,
IN_CONTACT_MOBILE VARCHAR2 DEFAULT NULL,
IN_INV_CONTACT_MOBILE VARCHAR2 DEFAULT NULL,
IN_HUB_CONTACT_MOBILE VARCHAR2 DEFAULT NULL,
IN_SHIPMENT_GROUP VARCHAR2 DEFAULT NULL,
IN_FREIGHT_CURRENCY VARCHAR2 DEFAULT NULL,
IN_NCTS VARCHAR2 DEFAULT NULL,
IN_NLS_CALENDAR VARCHAR2 DEFAULT NULL,
IN_MPACK_CONSIGNMENT VARCHAR2 DEFAULT NULL,
IN_MPACK_NOMINATED_DSTAMP VARCHAR2 DEFAULT NULL,
IN_GLN VARCHAR2 DEFAULT NULL,
IN_HUB_GLN VARCHAR2 DEFAULT NULL,
IN_INV_GLN VARCHAR2 DEFAULT NULL,
IN_ALLOCATION_PRIORITY INTEGER DEFAULT NULL,
IN_ALLOW_PALLET_PICK VARCHAR2 DEFAULT NULL,
IN_SPLIT_SHIPPING_UNITS VARCHAR2 DEFAULT NULL,
IN_VOL_PCK_SSCC_LABEL VARCHAR2 DEFAULT NULL,
IN_COLLECTIVE_SEQUENCE INTEGER DEFAULT NULL,
IN_TRAX_USE_HUB_ADDR VARCHAR2 DEFAULT NULL,
IN_DIRECT_TO_STORE VARCHAR2 DEFAULT NULL,
IN_VOL_CTR_LABEL_FORMAT VARCHAR2 DEFAULT NULL,
IN_RETAILER_ID VARCHAR2 DEFAULT NULL,
IN_CARRIER_BAGS VARCHAR2 DEFAULT NULL,
in_user_id VARCHAR2 DEFAULT 'API-INT', 
in_station_id VARCHAR2 DEFAULT 'API-INT',
in_record_error_in_table VARCHAR2 DEFAULT NULL
) RETURN VARCHAR2 IS 

PRAGMA autonomous_transaction;
v_retval NUMBER;
v_error VARCHAR2(20);
v_interface_key NUMBER;
v_user_def_date_1 DATE := NULL;
v_user_def_date_2 DATE := NULL;
v_user_def_date_3 DATE := NULL;
v_user_def_date_4 DATE := NULL;
v_order_date DATE := NULL;
v_ship_by_date DATE := NULL;
v_deliver_by_date DATE := NULL;
v_delivered_dstamp DATE := NULL;
v_inv_dstamp DATE := NULL;
v_start_by_date DATE := NULL;
v_mpack_nominated_dstamp DATE := NULL;

BEGIN dcsdba.libsession.initialisesession(
  in_user_id, 'INT-API', in_station_id, 
  'INT-API'
);

IF in_user_def_date_1 IS NOT NULL THEN 
v_user_def_date_1 := to_date(in_user_def_date_1, 'YYYYMMDDHH24MISS');
END IF;
IF in_user_def_date_2 IS NOT NULL THEN 
v_user_def_date_2 := to_date(in_user_def_date_2, 'YYYYMMDDHH24MISS');
END IF;
IF in_user_def_date_3 IS NOT NULL THEN 
v_user_def_date_3 := to_date(in_user_def_date_3, 'YYYYMMDDHH24MISS');
END IF;
IF in_user_def_date_4 IS NOT NULL THEN 
v_user_def_date_4 := to_date(in_user_def_date_4, 'YYYYMMDDHH24MISS');
END IF;
IF in_order_date IS NOT NULL THEN 
v_order_date := to_date(in_order_date, 'YYYYMMDDHH24MISS');
END IF;
IF in_ship_by_date IS NOT NULL THEN 
v_ship_by_date := to_date(in_ship_by_date, 'YYYYMMDDHH24MISS');
END IF;
IF in_deliver_by_date IS NOT NULL THEN 
v_deliver_by_date := to_date(in_deliver_by_date, 'YYYYMMDDHH24MISS');
END IF;
IF in_delivered_dstamp IS NOT NULL THEN 
v_delivered_dstamp := to_date(in_delivered_dstamp, 'YYYYMMDDHH24MISS');
END IF;
IF in_inv_dstamp IS NOT NULL THEN 
v_inv_dstamp := to_date(in_inv_dstamp, 'YYYYMMDDHH24MISS');
END IF;
IF in_start_by_date IS NOT NULL THEN 
v_start_by_date := to_date(in_start_by_date, 'YYYYMMDDHH24MISS');
END IF;
IF in_mpack_nominated_dstamp IS NOT NULL THEN 
v_mpack_nominated_dstamp := to_date(in_mpack_nominated_dstamp, 'YYYYMMDDHH24MISS');
END IF;

v_retval := dcsdba.libmergeorder.directorderheader(
p_mergeerror => v_error,
p_ToUpdateCols => IN_UPDATE_COLUMNS,
p_mergeaction => IN_MERGE_ACTION,
p_orderid => IN_ORDER_ID,
p_ordertype => IN_ORDER_TYPE,
p_status => IN_STATUS,
p_priority => IN_PRIORITY,
p_shipdock => IN_SHIP_DOCK,
p_workgroup => IN_WORK_GROUP,
p_consignment => IN_CONSIGNMENT,
p_deliverypoint => IN_DELIVERY_POINT,
p_loadsequence => IN_LOAD_SEQUENCE,
p_fromsiteid => IN_FROM_SITE_ID,
p_tositeid => IN_TO_SITE_ID,
p_customerid => IN_CUSTOMER_ID,
p_orderdate => V_ORDER_DATE,
p_shipbydate => V_SHIP_BY_DATE,
p_purchaseorder => IN_PURCHASE_ORDER,
p_contact => IN_CONTACT,
p_contactphone => IN_CONTACT_PHONE,
p_contactfax => IN_CONTACT_FAX,
p_contactemail => IN_CONTACT_EMAIL,
p_name => IN_NAME,
p_address1 => IN_ADDRESS1,
p_address2 => IN_ADDRESS2,
p_town => IN_TOWN,
p_county => IN_COUNTY,
p_postcode => IN_POSTCODE,
p_country => IN_COUNTRY,
p_instructions => IN_INSTRUCTIONS,
p_repack => IN_REPACK,
p_ownerid => IN_OWNER_ID,
p_carrierid => IN_CARRIER_ID,
p_dispatchmethod => IN_DISPATCH_METHOD,
p_servicelevel => IN_SERVICE_LEVEL,
p_invaddressid => IN_INV_ADDRESS_ID,
p_invcontact => IN_INV_CONTACT,
p_invcontactphone => IN_INV_CONTACT_PHONE,
p_invcontactfax => IN_INV_CONTACT_FAX,
p_invcontactemail => IN_INV_CONTACT_EMAIL,
p_invname => IN_INV_NAME,
p_invaddress1 => IN_INV_ADDRESS1,
p_invaddress2 => IN_INV_ADDRESS2,
p_invtown => IN_INV_TOWN,
p_invcounty => IN_INV_COUNTY,
p_invpostcode => IN_INV_POSTCODE,
p_invcountry => IN_INV_COUNTRY,
p_deliverbydate => V_DELIVER_BY_DATE,
p_fastestcarrier => IN_FASTEST_CARRIER,
p_cheapestcarrier => IN_CHEAPEST_CARRIER,
p_sitereplen => IN_SITE_REPLEN,
p_cidnumber => IN_CID_NUMBER,
p_sidnumber => IN_SID_NUMBER,
p_locationnumber => IN_LOCATION_NUMBER,
p_freightcharges => IN_FREIGHT_CHARGES,
p_clientid => IN_CLIENT_ID,
p_export => IN_EXPORT,
p_disallowmergerules => IN_DISALLOW_MERGE_RULES,
p_userdeftype1 => IN_USER_DEF_TYPE_1,
p_userdeftype2 => IN_USER_DEF_TYPE_2,
p_userdeftype3 => IN_USER_DEF_TYPE_3,
p_userdeftype4 => IN_USER_DEF_TYPE_4,
p_userdeftype5 => IN_USER_DEF_TYPE_5,
p_userdeftype6 => IN_USER_DEF_TYPE_6,
p_userdeftype7 => IN_USER_DEF_TYPE_7,
p_userdeftype8 => IN_USER_DEF_TYPE_8,
p_userdefchk1 => IN_USER_DEF_CHK_1,
p_userdefchk2 => IN_USER_DEF_CHK_2,
p_userdefchk3 => IN_USER_DEF_CHK_3,
p_userdefchk4 => IN_USER_DEF_CHK_4,
p_userdefdate1 => V_USER_DEF_DATE_1,
p_userdefdate2 => V_USER_DEF_DATE_2,
p_userdefdate3 => V_USER_DEF_DATE_3,
p_userdefdate4 => V_USER_DEF_DATE_4,
p_userdefnum1 => IN_USER_DEF_NUM_1,
p_userdefnum2 => IN_USER_DEF_NUM_2,
p_userdefnum3 => IN_USER_DEF_NUM_3,
p_userdefnum4 => IN_USER_DEF_NUM_4,
p_userdefnote1 => IN_USER_DEF_NOTE_1,
p_userdefnote2 => IN_USER_DEF_NOTE_2,
p_sohid => IN_SOH_ID,
p_movetaskstatus => IN_MOVE_TASK_STATUS,
p_timezonename => IN_TIME_ZONE_NAME,
p_repacklocid => IN_REPACK_LOC_ID,
p_cereasoncode => IN_CE_REASON_CODE,
p_cereasonnotes => IN_CE_REASON_NOTES,
p_ceordertype => IN_CE_ORDER_TYPE,
p_cecustomscustomer => IN_CE_CUSTOMS_CUSTOMER,
p_ceexcisecustomer => IN_CE_EXCISE_CUSTOMER,
p_ceinstructions => IN_CE_INSTRUCTIONS,
p_clientgroup => IN_CLIENT_GROUP,
p_delivereddstamp => V_DELIVERED_DSTAMP,
p_signatory => IN_SIGNATORY,
p_routeid => IN_ROUTE_ID,
p_crossdocktosite => IN_CROSS_DOCK_TO_SITE,
p_webserviceallocimmed => IN_WEB_SERVICE_ALLOC_IMMED,
p_webserviceallocclean => IN_WEB_SERVICE_ALLOC_CLEAN,
p_disallowshortship => IN_DISALLOW_SHORT_SHIP,
p_workordertype => IN_WORK_ORDER_TYPE,
p_statusreasoncode => IN_STATUS_REASON_CODE,
p_hubaddressid => IN_HUB_ADDRESS_ID,
p_hubcontact => IN_HUB_CONTACT,
p_hubcontactphone => IN_HUB_CONTACT_PHONE,
p_hubcontactfax => IN_HUB_CONTACT_FAX,
p_hubcontactemail => IN_HUB_CONTACT_EMAIL,
p_hubname => IN_HUB_NAME,
p_hubaddress1 => IN_HUB_ADDRESS1,
p_hubaddress2 => IN_HUB_ADDRESS2,
p_hubtown => IN_HUB_TOWN,
p_hubcounty => IN_HUB_COUNTY,
p_hubpostcode => IN_HUB_POSTCODE,
p_hubcountry => IN_HUB_COUNTRY,
p_stagerouteid => IN_STAGE_ROUTE_ID,
p_singleordersortation => IN_SINGLE_ORDER_SORTATION,
p_hubcarrierid => IN_HUB_CARRIER_ID,
p_hubservicelevel => IN_HUB_SERVICE_LEVEL,
p_forcesinglecarrier => IN_FORCE_SINGLE_CARRIER,
p_expectedvalue => IN_EXPECTED_VALUE,
p_expectedvolume => IN_EXPECTED_VOLUME,
p_expectedweight => IN_EXPECTED_WEIGHT,
p_tod => IN_TOD,
p_todplace => IN_TOD_PLACE,
p_language => IN_LANGUAGE,
p_sellername => IN_SELLER_NAME,
p_sellerphone => IN_SELLER_PHONE,
p_documentationtext1 => IN_DOCUMENTATION_TEXT_1,
p_documentationtext2 => IN_DOCUMENTATION_TEXT_2,
p_documentationtext3 => IN_DOCUMENTATION_TEXT_3,
p_cod => IN_COD,
p_codvalue => IN_COD_VALUE,
p_codcurrency => IN_COD_CURRENCY,
p_codtype => IN_COD_TYPE,
p_vatnumber => IN_VAT_NUMBER,
p_invvatnumber => IN_INV_VAT_NUMBER,
p_hubvatnumber => IN_HUB_VAT_NUMBER,
p_invreference => IN_INV_REFERENCE,
p_invdstamp => V_INV_DSTAMP,
p_invcurrency => IN_INV_CURRENCY,
p_printinvoice => IN_PRINT_INVOICE,
p_letterofcredit => IN_LETTER_OF_CREDIT,
p_paymentterms => IN_PAYMENT_TERMS,
p_subtotal1 => IN_SUBTOTAL_1,
p_subtotal2 => IN_SUBTOTAL_2,
p_subtotal3 => IN_SUBTOTAL_3,
p_subtotal4 => IN_SUBTOTAL_4,
p_freightcost => IN_FREIGHT_COST,
p_freightterms => IN_FREIGHT_TERMS,
p_insurancecost => IN_INSURANCE_COST,
p_misccharges => IN_MISC_CHARGES,
p_discount => IN_DISCOUNT,
p_otherfee => IN_OTHER_FEE,
p_invtotal1 => IN_INV_TOTAL_1,
p_invtotal2 => IN_INV_TOTAL_2,
p_invtotal3 => IN_INV_TOTAL_3,
p_invtotal4 => IN_INV_TOTAL_4,
p_taxrate1 => IN_TAX_RATE_1,
p_taxbasis1 => IN_TAX_BASIS_1,
p_taxamount1 => IN_TAX_AMOUNT_1,
p_taxrate2 => IN_TAX_RATE_2,
p_taxbasis2 => IN_TAX_BASIS_2,
p_taxamount2 => IN_TAX_AMOUNT_2,
p_taxrate3 => IN_TAX_RATE_3,
p_taxbasis3 => IN_TAX_BASIS_3,
p_taxamount3 => IN_TAX_AMOUNT_3,
p_taxrate4 => IN_TAX_RATE_4,
p_taxbasis4 => IN_TAX_BASIS_4,
p_taxamount4 => IN_TAX_AMOUNT_4,
p_taxrate5 => IN_TAX_RATE_5,
p_taxbasis5 => IN_TAX_BASIS_5,
p_taxamount5 => IN_TAX_AMOUNT_5,
p_orderreference => IN_ORDER_REFERENCE,
p_startbydate => V_START_BY_DATE,
p_metapackcarrierpre => IN_METAPACK_CARRIER_PRE,
p_contactmobile => IN_CONTACT_MOBILE,
p_invcontactmobile => IN_INV_CONTACT_MOBILE,
p_hubcontactmobile => IN_HUB_CONTACT_MOBILE,
p_shipmentgroup => IN_SHIPMENT_GROUP,
p_freightcurrency => IN_FREIGHT_CURRENCY,
p_ncts => IN_NCTS,
p_mpackconsignment => IN_MPACK_CONSIGNMENT,
p_mpacknominateddstamp => V_MPACK_NOMINATED_DSTAMP,
p_gln => IN_GLN,
p_hubgln => IN_HUB_GLN,
p_invgln => IN_INV_GLN,
p_allocationpriority => IN_ALLOCATION_PRIORITY,
p_allowpalletpick => IN_ALLOW_PALLET_PICK,
p_splitshippingunits => IN_SPLIT_SHIPPING_UNITS,
p_volpcksscclabel => IN_VOL_PCK_SSCC_LABEL,
p_traxusehubaddr => IN_TRAX_USE_HUB_ADDR,
p_directtostore => IN_DIRECT_TO_STORE,
p_volctrlabelformat => IN_VOL_CTR_LABEL_FORMAT,
p_retailerid => IN_RETAILER_ID,
p_carrierbags => IN_CARRIER_BAGS
);

IF v_error is not null AND in_record_error_in_table = 'Y' then
v_interface_key := DCSDBA.IF_OH_PK_SEQ.nextval;
insert into dcsdba.interface_update_columns(key,table_name,update_columns)VALUES(v_interface_key,'interface_order_header',nvl(in_update_columns,':key::client_id::order_id::order_type::work_order_type::status::move_task_status::priority::ship_dock::work_group::consignment::delivery_point::load_sequence::from_site_id::to_site_id::owner_id::customer_id::order_date::ship_by_date::deliver_by_date::purchase_order::contact::contact_phone::contact_mobile::contact_fax::contact_email::name::address1::address2::town::county::postcode::country::instructions::repack::carrier_id::dispatch_method::service_level::fastest_carrier::cheapest_carrier::inv_address_id::inv_contact::inv_contact_phone::inv_contact_mobile::inv_contact_fax::inv_contact_email::inv_name::inv_address1::inv_address2::inv_town::inv_county::inv_postcode::inv_country::psft_dmnd_srce::psft_order_id::site_replen::cid_number::sid_number::location_number::freight_charges::disallow_merge_rules::export::soh_id::repack_loc_id::user_def_type_1::user_def_type_2::user_def_type_3::user_def_type_4::user_def_type_5::user_def_type_6::user_def_type_7::user_def_type_8::user_def_chk_1::user_def_chk_2::user_def_chk_3::user_def_chk_4::user_def_date_1::user_def_date_2::user_def_date_3::user_def_date_4::user_def_num_1::user_def_num_2::user_def_num_3::user_def_num_4::user_def_note_1::user_def_note_2::ce_reason_code::ce_reason_notes::ce_order_type::ce_customs_customer::ce_excise_customer::ce_instructions::delivered_dstamp::signatory::route_id::cross_dock_to_site::web_service_alloc_immed::web_service_alloc_clean::disallow_short_ship::hub_address_id::hub_contact::hub_contact_phone::hub_contact_mobile::hub_contact_fax::hub_contact_email::hub_name::hub_address1::hub_address2::hub_town::hub_county::hub_postcode::hub_country::hub_carrier_id::hub_service_level::status_reason_code::stage_route_id::single_order_sortation::force_single_carrier::expected_volume::expected_weight::expected_value::tod::tod_place::language::seller_name::seller_phone::documentation_text_1::documentation_text_2::documentation_text_3::cod::cod_value::cod_currency::cod_type::vat_number::inv_vat_number::hub_vat_number::print_invoice::inv_reference::inv_dstamp::inv_currency::letter_of_credit::payment_terms::subtotal_1::subtotal_2::subtotal_3::subtotal_4::freight_cost::freight_terms::insurance_cost::misc_charges::discount::other_fee::inv_total_1::inv_total_2::inv_total_3::inv_total_4::tax_rate_1::tax_basis_1::tax_amount_1::tax_rate_2::tax_basis_2::tax_amount_2::tax_rate_3::tax_basis_3::tax_amount_3::tax_rate_4::tax_basis_4::tax_amount_4::tax_rate_5::tax_basis_5::tax_amount_5::order_reference::collective_mode::collective_sequence::start_by_date::metapack_carrier_pre::shipment_group::freight_currency::ncts::gln::hub_gln::inv_gln::allow_pallet_pick::split_shipping_units::vol_pck_sscc_label::allocation_priority::trax_use_hub_addr::mpack_consignment::mpack_nominated_dstamp::direct_to_store::vol_ctr_label_format::retailer_id::carrier_bags::session_time_zone_name::time_zone_name::nls_calendar::client_group::merge_action::merge_status::merge_error::merge_dstamp:'));
insert into dcsdba.interface_order_header(Key, Merge_Dstamp, Merge_Status, merge_error, merge_action,order_id,order_type,status,priority,ship_dock,work_group,consignment,delivery_point,load_sequence,from_site_id,to_site_id,customer_id,order_date,ship_by_date,purchase_order,contact,contact_phone,contact_fax,contact_email,name,address1,address2,town,county,postcode,country,instructions,repack,owner_id,carrier_id,dispatch_method,service_level,inv_address_id,inv_contact,inv_contact_phone,inv_contact_fax,inv_contact_email,inv_name,inv_address1,inv_address2,inv_town,inv_county,inv_postcode,inv_country,deliver_by_date,fastest_carrier,cheapest_carrier,site_replen,cid_number,sid_number,location_number,freight_charges,client_id,export,disallow_merge_rules,user_def_type_1,user_def_type_2,user_def_type_3,user_def_type_4,user_def_type_5,user_def_type_6,user_def_type_7,user_def_type_8,user_def_chk_1,user_def_chk_2,user_def_chk_3,user_def_chk_4,user_def_date_1,user_def_date_2,user_def_date_3,user_def_date_4,user_def_num_1,user_def_num_2,user_def_num_3,user_def_num_4,user_def_note_1,user_def_note_2,soh_id,move_task_status,time_zone_name,repack_loc_id,ce_reason_code,ce_reason_notes,ce_order_type,ce_customs_customer,ce_excise_customer,ce_instructions,client_group,delivered_dstamp,signatory,route_id,cross_dock_to_site,web_service_alloc_immed,web_service_alloc_clean,disallow_short_ship,work_order_type,status_reason_code,hub_address_id,hub_contact,hub_contact_phone,hub_contact_fax,hub_contact_email,hub_name,hub_address1,hub_address2,hub_town,hub_county,hub_postcode,hub_country,stage_route_id,single_order_sortation,hub_carrier_id,hub_service_level,force_single_carrier,expected_value,expected_volume,expected_weight,tod,tod_place,language,seller_name,seller_phone,documentation_text_1,documentation_text_2,documentation_text_3,cod,cod_value,cod_currency,cod_type,vat_number,inv_vat_number,hub_vat_number,inv_reference,inv_dstamp,inv_currency,print_invoice,letter_of_credit,payment_terms,subtotal_1,subtotal_2,subtotal_3,subtotal_4,freight_cost,freight_terms,insurance_cost,misc_charges,discount,other_fee,inv_total_1,inv_total_2,inv_total_3,inv_total_4,tax_rate_1,tax_basis_1,tax_amount_1,tax_rate_2,tax_basis_2,tax_amount_2,tax_rate_3,tax_basis_3,tax_amount_3,tax_rate_4,tax_basis_4,tax_amount_4,tax_rate_5,tax_basis_5,tax_amount_5,order_reference,start_by_date,metapack_carrier_pre,contact_mobile,inv_contact_mobile,hub_contact_mobile,shipment_group,freight_currency,ncts,mpack_consignment,mpack_nominated_dstamp,gln,hub_gln,inv_gln,allocation_priority,allow_pallet_pick,split_shipping_units,vol_pck_sscc_label,trax_use_hub_addr,direct_to_store,vol_ctr_label_format,retailer_id,carrier_bags)
VALUES(v_interface_key, sysdate, 'Error', v_error, IN_MERGE_ACTION,IN_ORDER_ID,IN_ORDER_TYPE,IN_STATUS,IN_PRIORITY,IN_SHIP_DOCK,IN_WORK_GROUP,IN_CONSIGNMENT,IN_DELIVERY_POINT,IN_LOAD_SEQUENCE,IN_FROM_SITE_ID,IN_TO_SITE_ID,IN_CUSTOMER_ID,V_ORDER_DATE,V_SHIP_BY_DATE,IN_PURCHASE_ORDER,IN_CONTACT,IN_CONTACT_PHONE,IN_CONTACT_FAX,IN_CONTACT_EMAIL,IN_NAME,IN_ADDRESS1,IN_ADDRESS2,IN_TOWN,IN_COUNTY,IN_POSTCODE,IN_COUNTRY,IN_INSTRUCTIONS,IN_REPACK,IN_OWNER_ID,IN_CARRIER_ID,IN_DISPATCH_METHOD,IN_SERVICE_LEVEL,IN_INV_ADDRESS_ID,IN_INV_CONTACT,IN_INV_CONTACT_PHONE,IN_INV_CONTACT_FAX,IN_INV_CONTACT_EMAIL,IN_INV_NAME,IN_INV_ADDRESS1,IN_INV_ADDRESS2,IN_INV_TOWN,IN_INV_COUNTY,IN_INV_POSTCODE,IN_INV_COUNTRY,V_DELIVER_BY_DATE,IN_FASTEST_CARRIER,IN_CHEAPEST_CARRIER,IN_SITE_REPLEN,IN_CID_NUMBER,IN_SID_NUMBER,IN_LOCATION_NUMBER,IN_FREIGHT_CHARGES,IN_CLIENT_ID,IN_EXPORT,IN_DISALLOW_MERGE_RULES,IN_USER_DEF_TYPE_1,IN_USER_DEF_TYPE_2,IN_USER_DEF_TYPE_3,IN_USER_DEF_TYPE_4,IN_USER_DEF_TYPE_5,IN_USER_DEF_TYPE_6,IN_USER_DEF_TYPE_7,IN_USER_DEF_TYPE_8,IN_USER_DEF_CHK_1,IN_USER_DEF_CHK_2,IN_USER_DEF_CHK_3,IN_USER_DEF_CHK_4,V_USER_DEF_DATE_1,V_USER_DEF_DATE_2,V_USER_DEF_DATE_3,V_USER_DEF_DATE_4,IN_USER_DEF_NUM_1,IN_USER_DEF_NUM_2,IN_USER_DEF_NUM_3,IN_USER_DEF_NUM_4,IN_USER_DEF_NOTE_1,IN_USER_DEF_NOTE_2,IN_SOH_ID,IN_MOVE_TASK_STATUS,IN_TIME_ZONE_NAME,IN_REPACK_LOC_ID,IN_CE_REASON_CODE,IN_CE_REASON_NOTES,IN_CE_ORDER_TYPE,IN_CE_CUSTOMS_CUSTOMER,IN_CE_EXCISE_CUSTOMER,IN_CE_INSTRUCTIONS,IN_CLIENT_GROUP,V_DELIVERED_DSTAMP,IN_SIGNATORY,IN_ROUTE_ID,IN_CROSS_DOCK_TO_SITE,IN_WEB_SERVICE_ALLOC_IMMED,IN_WEB_SERVICE_ALLOC_CLEAN,IN_DISALLOW_SHORT_SHIP,IN_WORK_ORDER_TYPE,IN_STATUS_REASON_CODE,IN_HUB_ADDRESS_ID,IN_HUB_CONTACT,IN_HUB_CONTACT_PHONE,IN_HUB_CONTACT_FAX,IN_HUB_CONTACT_EMAIL,IN_HUB_NAME,IN_HUB_ADDRESS1,IN_HUB_ADDRESS2,IN_HUB_TOWN,IN_HUB_COUNTY,IN_HUB_POSTCODE,IN_HUB_COUNTRY,IN_STAGE_ROUTE_ID,IN_SINGLE_ORDER_SORTATION,IN_HUB_CARRIER_ID,IN_HUB_SERVICE_LEVEL,IN_FORCE_SINGLE_CARRIER,IN_EXPECTED_VALUE,IN_EXPECTED_VOLUME,IN_EXPECTED_WEIGHT,IN_TOD,IN_TOD_PLACE,IN_LANGUAGE,IN_SELLER_NAME,IN_SELLER_PHONE,IN_DOCUMENTATION_TEXT_1,IN_DOCUMENTATION_TEXT_2,IN_DOCUMENTATION_TEXT_3,IN_COD,IN_COD_VALUE,IN_COD_CURRENCY,IN_COD_TYPE,IN_VAT_NUMBER,IN_INV_VAT_NUMBER,IN_HUB_VAT_NUMBER,IN_INV_REFERENCE,V_INV_DSTAMP,IN_INV_CURRENCY,IN_PRINT_INVOICE,IN_LETTER_OF_CREDIT,IN_PAYMENT_TERMS,IN_SUBTOTAL_1,IN_SUBTOTAL_2,IN_SUBTOTAL_3,IN_SUBTOTAL_4,IN_FREIGHT_COST,IN_FREIGHT_TERMS,IN_INSURANCE_COST,IN_MISC_CHARGES,IN_DISCOUNT,IN_OTHER_FEE,IN_INV_TOTAL_1,IN_INV_TOTAL_2,IN_INV_TOTAL_3,IN_INV_TOTAL_4,IN_TAX_RATE_1,IN_TAX_BASIS_1,IN_TAX_AMOUNT_1,IN_TAX_RATE_2,IN_TAX_BASIS_2,IN_TAX_AMOUNT_2,IN_TAX_RATE_3,IN_TAX_BASIS_3,IN_TAX_AMOUNT_3,IN_TAX_RATE_4,IN_TAX_BASIS_4,IN_TAX_AMOUNT_4,IN_TAX_RATE_5,IN_TAX_BASIS_5,IN_TAX_AMOUNT_5,IN_ORDER_REFERENCE,V_START_BY_DATE,IN_METAPACK_CARRIER_PRE,IN_CONTACT_MOBILE,IN_INV_CONTACT_MOBILE,IN_HUB_CONTACT_MOBILE,IN_SHIPMENT_GROUP,IN_FREIGHT_CURRENCY,IN_NCTS,IN_MPACK_CONSIGNMENT,V_MPACK_NOMINATED_DSTAMP,IN_GLN,IN_HUB_GLN,IN_INV_GLN,IN_ALLOCATION_PRIORITY,IN_ALLOW_PALLET_PICK,IN_SPLIT_SHIPPING_UNITS,IN_VOL_PCK_SSCC_LABEL,IN_TRAX_USE_HUB_ADDR,IN_DIRECT_TO_STORE,IN_VOL_CTR_LABEL_FORMAT,IN_RETAILER_ID,IN_CARRIER_BAGS);
END IF;

COMMIT;
RETURN v_error;

END INTERFACE_ORDER_HEADER;

/***********************************************************************************************************************/

/*                                                                                                                     */

/*  NAME:               TQ_INTERFACE.INTERFACE_ORDER_LINE                                                              */

/*                                                                                                                     */

/*  DESCRIPTION:        This Function Uses The Interface Order LINE API to Add Order Headers to the System             */

/*                                                                                                                     */

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */

/*  =======     ===========     ==     ===================================================================             */

/*    1         2020-02-27      KS     Initial Version                                                                 */

/*    2         2020-02-28      KS     Amended to add LineID Calulation if IN_LINE_ID is null                          */

/***********************************************************************************************************************/

/**********************GRANTS**************************

grant select on IF_OL_PK_SEQ TO TORQUE;
grant select on order_line to TORQUE;
grant select on interface_order_line to TORQUE;

grant insert on interface_update_columns to TORQUE;
grant insert on interface_order_line to TORQUE;

grant delete on interface_update_columns to TORQUE;
grant delete on interface_order_line to TORQUE;

grant execute on libsession to TORQUE;
grant execute on libmergeorder to TORQUE;
grant execute on liberror to TORQUE;


******************************************************/

FUNCTION INTERFACE_ORDER_LINE (
IN_UPDATE_COLUMNS VARCHAR2 DEFAULT NULL,
IN_MERGE_ACTION VARCHAR2 DEFAULT 'U',
IN_ORDER_ID VARCHAR2 ,
IN_LINE_ID INTEGER DEFAULT NULL,
IN_SKU_ID VARCHAR2 DEFAULT NULL,
IN_CONFIG_ID VARCHAR2 DEFAULT NULL,
IN_TRACKING_LEVEL VARCHAR2 DEFAULT NULL,
IN_BATCH_ID VARCHAR2 DEFAULT NULL,
IN_BATCH_MIXING VARCHAR2 DEFAULT NULL,
IN_CONDITION_ID VARCHAR2 DEFAULT NULL,
IN_LOCK_CODE VARCHAR2 DEFAULT NULL,
IN_QTY_ORDERED NUMBER ,
IN_ALLOCATE VARCHAR2 DEFAULT NULL,
IN_BACK_ORDERED VARCHAR2 DEFAULT NULL,
IN_DEALLOCATE VARCHAR2 DEFAULT NULL,
IN_KIT_SPLIT VARCHAR2 DEFAULT NULL,
IN_ORIGIN_ID VARCHAR2 DEFAULT NULL,
IN_NOTES VARCHAR2 DEFAULT NULL,
IN_CUSTOMER_SKU_ID VARCHAR2 DEFAULT NULL,
IN_SHELF_LIFE_DAYS INTEGER DEFAULT NULL,
IN_SHELF_LIFE_PERCENT INTEGER DEFAULT NULL,
IN_CLIENT_ID VARCHAR2 ,
IN_DISALLOW_MERGE_RULES VARCHAR2 DEFAULT NULL,
IN_USER_DEF_TYPE_1 VARCHAR2 DEFAULT NULL,
IN_USER_DEF_TYPE_2 VARCHAR2 DEFAULT NULL,
IN_USER_DEF_TYPE_3 VARCHAR2 DEFAULT NULL,
IN_USER_DEF_TYPE_4 VARCHAR2 DEFAULT NULL,
IN_USER_DEF_TYPE_5 VARCHAR2 DEFAULT NULL,
IN_USER_DEF_TYPE_6 VARCHAR2 DEFAULT NULL,
IN_USER_DEF_TYPE_7 VARCHAR2 DEFAULT NULL,
IN_USER_DEF_TYPE_8 VARCHAR2 DEFAULT NULL,
IN_USER_DEF_CHK_1 VARCHAR2 DEFAULT NULL,
IN_USER_DEF_CHK_2 VARCHAR2 DEFAULT NULL,
IN_USER_DEF_CHK_3 VARCHAR2 DEFAULT NULL,
IN_USER_DEF_CHK_4 VARCHAR2 DEFAULT NULL,
IN_USER_DEF_DATE_1 VARCHAR2 DEFAULT NULL,
IN_USER_DEF_DATE_2 VARCHAR2 DEFAULT NULL,
IN_USER_DEF_DATE_3 VARCHAR2 DEFAULT NULL,
IN_USER_DEF_DATE_4 VARCHAR2 DEFAULT NULL,
IN_USER_DEF_NUM_1 NUMBER DEFAULT NULL,
IN_USER_DEF_NUM_2 NUMBER DEFAULT NULL,
IN_USER_DEF_NUM_3 NUMBER DEFAULT NULL,
IN_USER_DEF_NUM_4 NUMBER DEFAULT NULL,
IN_USER_DEF_NOTE_1 VARCHAR2 DEFAULT NULL,
IN_USER_DEF_NOTE_2 VARCHAR2 DEFAULT NULL,
IN_SOH_ID VARCHAR2 DEFAULT NULL,
IN_LINE_VALUE NUMBER DEFAULT NULL,
IN_TIME_ZONE_NAME VARCHAR2 DEFAULT NULL,
IN_SPEC_CODE VARCHAR2 DEFAULT NULL,
IN_RULE_ID VARCHAR2 DEFAULT NULL,
IN_CLIENT_GROUP VARCHAR2 DEFAULT NULL,
IN_TASK_PER_EACH VARCHAR2 DEFAULT NULL,
IN_USE_PICK_TO_GRID VARCHAR2 DEFAULT NULL,
IN_IGNORE_WEIGHT_CAPTURE VARCHAR2 DEFAULT NULL,
IN_HOST_ORDER_ID VARCHAR2 DEFAULT NULL,
IN_HOST_LINE_ID VARCHAR2 DEFAULT NULL,
IN_STAGE_ROUTE_ID VARCHAR2 DEFAULT NULL,
IN_MIN_QTY_ORDERED NUMBER DEFAULT NULL,
IN_MAX_QTY_ORDERED NUMBER DEFAULT NULL,
IN_EXPECTED_VALUE NUMBER DEFAULT NULL,
IN_EXPECTED_VOLUME NUMBER DEFAULT NULL,
IN_EXPECTED_WEIGHT NUMBER DEFAULT NULL,
IN_CUSTOMER_SKU_DESC1 VARCHAR2 DEFAULT NULL,
IN_CUSTOMER_SKU_DESC2 VARCHAR2 DEFAULT NULL,
IN_PURCHASE_ORDER VARCHAR2 DEFAULT NULL,
IN_PRODUCT_PRICE NUMBER DEFAULT NULL,
IN_PRODUCT_CURRENCY VARCHAR2 DEFAULT NULL,
IN_DOCUMENTATION_UNIT VARCHAR2 DEFAULT NULL,
IN_EXTENDED_PRICE NUMBER DEFAULT NULL,
IN_TAX_1 NUMBER DEFAULT NULL,
IN_TAX_2 NUMBER DEFAULT NULL,
IN_DOCUMENTATION_TEXT_1 VARCHAR2 DEFAULT NULL,
IN_SERIAL_NUMBER VARCHAR2 DEFAULT NULL,
IN_OWNER_ID VARCHAR2 DEFAULT NULL,
IN_COLLECTIVE_MODE VARCHAR2 DEFAULT NULL,
IN_CE_RECEIPT_TYPE VARCHAR2 DEFAULT NULL,
IN_CE_COO VARCHAR2 DEFAULT NULL,
IN_KIT_PLAN_ID VARCHAR2 DEFAULT NULL,
IN_LOCATION_ID VARCHAR2 DEFAULT NULL,
IN_NLS_CALENDAR VARCHAR2 DEFAULT NULL,
IN_COLLECTIVE_SEQUENCE NUMBER DEFAULT NULL,
IN_UNALLOCATABLE VARCHAR2 DEFAULT NULL,
IN_FULL_TRACKING_LEVEL_ONLY VARCHAR2 DEFAULT NULL,
IN_MIN_FULL_PALLET_PERC INTEGER DEFAULT NULL,
IN_MAX_FULL_PALLET_PERC INTEGER DEFAULT NULL,
IN_SUBSTITUTE_GRADE VARCHAR2 DEFAULT NULL,
IN_DISALLOW_SUBSTITUTION VARCHAR2 DEFAULT NULL,
in_find_line_id VARCHAR2 DEFAULT 'Y',
in_user_id VARCHAR2 DEFAULT 'API-INT', 
in_station_id VARCHAR2 DEFAULT 'API-INT',
in_record_error_in_table VARCHAR2 DEFAULT NULL
) RETURN VARCHAR2 IS 

PRAGMA autonomous_transaction;
v_retval NUMBER;
v_error VARCHAR2(20);
v_interface_key NUMBER;
v_line_id NUMBER;

v_user_def_date_1 DATE := NULL;
v_user_def_date_2 DATE := NULL;
v_user_def_date_3 DATE := NULL;
v_user_def_date_4 DATE := NULL;

BEGIN dcsdba.libsession.initialisesession(
  in_user_id, 'INT-API', in_station_id, 
  'INT-API'
);

IF in_user_def_date_1 IS NOT NULL THEN 
v_user_def_date_1 := to_date(in_user_def_date_1, 'YYYYMMDDHH24MISS');
END IF;
IF in_user_def_date_2 IS NOT NULL THEN 
v_user_def_date_2 := to_date(in_user_def_date_2, 'YYYYMMDDHH24MISS');
END IF;
IF in_user_def_date_3 IS NOT NULL THEN 
v_user_def_date_3 := to_date(in_user_def_date_3, 'YYYYMMDDHH24MISS');
END IF;
IF in_user_def_date_4 IS NOT NULL THEN 
v_user_def_date_4 := to_date(in_user_def_date_4, 'YYYYMMDDHH24MISS');
END IF;

v_line_id := in_line_id;

--Find Line ID from SKU If Exists
IF v_line_id is null AND in_find_line_id = 'Y' then
SELECT MIN(line_id) into v_line_id  from(
SELECT MIN(line_id) as line_id  from dcsdba.order_line where client_id = in_client_id and order_id = in_order_id AND sku_id = in_sku_id
UNION
SELECT MIN(line_id)  from dcsdba.interface_order_line where client_id = in_client_id and order_id = in_order_id AND sku_id = in_sku_id);
END IF;

--Calculate Next Available Line ID
IF v_line_id is null AND in_find_line_id = 'Y' then
SELECT MAX(line_id) into v_line_id  from(
SELECT nvl(MAX(line_id),0)+1 as line_id  from dcsdba.order_line where client_id = in_client_id and order_id = in_order_id
UNION
SELECT nvl(MAX(line_id),0)+1  from dcsdba.interface_order_line where client_id = in_client_id and order_id = in_order_id);
END IF;

v_retval := dcsdba.libmergeorder.directorderline(
p_mergeerror => v_error,
p_ToUpdateCols  => IN_UPDATE_COLUMNS,
p_mergeaction => IN_MERGE_ACTION,
p_orderid => IN_ORDER_ID,
p_lineid => v_line_id,
p_skuid => IN_SKU_ID,
p_configid => IN_CONFIG_ID,
p_trackinglevel => IN_TRACKING_LEVEL,
p_batchid => IN_BATCH_ID,
p_batchmixing => IN_BATCH_MIXING,
p_conditionid => IN_CONDITION_ID,
p_lockcode => IN_LOCK_CODE,
p_qtyordered => IN_QTY_ORDERED,
p_allocate => IN_ALLOCATE,
p_backordered => IN_BACK_ORDERED,
p_deallocate => IN_DEALLOCATE,
p_kitsplit => IN_KIT_SPLIT,
p_originid => IN_ORIGIN_ID,
p_notes => IN_NOTES,
p_customerskuid => IN_CUSTOMER_SKU_ID,
p_shelflifedays => IN_SHELF_LIFE_DAYS,
p_shelflifepercent => IN_SHELF_LIFE_PERCENT,
p_clientid => IN_CLIENT_ID,
p_disallowmergerules => IN_DISALLOW_MERGE_RULES,
p_userdeftype1 => IN_USER_DEF_TYPE_1,
p_userdeftype2 => IN_USER_DEF_TYPE_2,
p_userdeftype3 => IN_USER_DEF_TYPE_3,
p_userdeftype4 => IN_USER_DEF_TYPE_4,
p_userdeftype5 => IN_USER_DEF_TYPE_5,
p_userdeftype6 => IN_USER_DEF_TYPE_6,
p_userdeftype7 => IN_USER_DEF_TYPE_7,
p_userdeftype8 => IN_USER_DEF_TYPE_8,
p_userdefchk1 => IN_USER_DEF_CHK_1,
p_userdefchk2 => IN_USER_DEF_CHK_2,
p_userdefchk3 => IN_USER_DEF_CHK_3,
p_userdefchk4 => IN_USER_DEF_CHK_4,
p_userdefdate1 => V_USER_DEF_DATE_1,
p_userdefdate2 => V_USER_DEF_DATE_2,
p_userdefdate3 => V_USER_DEF_DATE_3,
p_userdefdate4 => V_USER_DEF_DATE_4,
p_userdefnum1 => IN_USER_DEF_NUM_1,
p_userdefnum2 => IN_USER_DEF_NUM_2,
p_userdefnum3 => IN_USER_DEF_NUM_3,
p_userdefnum4 => IN_USER_DEF_NUM_4,
p_userdefnote1 => IN_USER_DEF_NOTE_1,
p_userdefnote2 => IN_USER_DEF_NOTE_2,
p_sohid => IN_SOH_ID,
p_linevalue => IN_LINE_VALUE,
p_timezonename => IN_TIME_ZONE_NAME,
p_speccode => IN_SPEC_CODE,
p_ruleid => IN_RULE_ID,
p_clientgroup => IN_CLIENT_GROUP,
p_taskpereach => IN_TASK_PER_EACH,
p_usepicktogrid => IN_USE_PICK_TO_GRID,
p_ignoreweightcapture => IN_IGNORE_WEIGHT_CAPTURE,
p_hostorderid => IN_HOST_ORDER_ID,
p_hostlineid => IN_HOST_LINE_ID,
p_stagerouteid => IN_STAGE_ROUTE_ID,
p_minqtyordered => IN_MIN_QTY_ORDERED,
p_maxqtyordered => IN_MAX_QTY_ORDERED,
p_expectedvalue => IN_EXPECTED_VALUE,
p_expectedvolume => IN_EXPECTED_VOLUME,
p_expectedweight => IN_EXPECTED_WEIGHT,
p_customerskudesc1 => IN_CUSTOMER_SKU_DESC1,
p_customerskudesc2 => IN_CUSTOMER_SKU_DESC2,
p_purchaseorder => IN_PURCHASE_ORDER,
p_productprice => IN_PRODUCT_PRICE,
p_productcurrency => IN_PRODUCT_CURRENCY,
p_documentationunit => IN_DOCUMENTATION_UNIT,
p_extendedprice => IN_EXTENDED_PRICE,
p_tax1 => IN_TAX_1,
p_tax2 => IN_TAX_2,
p_documentationtext1 => IN_DOCUMENTATION_TEXT_1,
p_serialnumber => IN_SERIAL_NUMBER,
p_ownerid => IN_OWNER_ID,
p_cereceipttype => IN_CE_RECEIPT_TYPE,
p_cecoo => IN_CE_COO,
p_kitplanid => IN_KIT_PLAN_ID,
p_locationid => IN_LOCATION_ID,
p_unallocatable => IN_UNALLOCATABLE,
p_fulltrackinglevelonly => IN_FULL_TRACKING_LEVEL_ONLY,
p_minfullpalletperc => IN_MIN_FULL_PALLET_PERC,
p_maxfullpalletperc => IN_MAX_FULL_PALLET_PERC,
p_substitutegrade => IN_SUBSTITUTE_GRADE,
p_disallowsubstitution => IN_DISALLOW_SUBSTITUTION
);

IF v_error is not null AND in_record_error_in_table = 'Y' then
v_interface_key := DCSDBA.IF_OL_PK_SEQ.nextval;
insert into dcsdba.interface_update_columns(key,table_name,update_columns)VALUES(v_interface_key,'interface_order_line',nvl(in_update_columns,':key::client_id::order_id::line_id::host_order_id::host_line_id::sku_id::customer_sku_id::config_id::tracking_level::batch_id::batch_mixing::shelf_life_days::shelf_life_percent::origin_id::condition_id::lock_code::spec_code::qty_ordered::allocate::back_ordered::kit_split::deallocate::notes::psft_int_line::psft_schd_line::psft_dmnd_line::sap_pick_req::disallow_merge_rules::line_value::rule_id::soh_id::user_def_type_1::user_def_type_2::user_def_type_3::user_def_type_4::user_def_type_5::user_def_type_6::user_def_type_7::user_def_type_8::user_def_chk_1::user_def_chk_2::user_def_chk_3::user_def_chk_4::user_def_date_1::user_def_date_2::user_def_date_3::user_def_date_4::user_def_num_1::user_def_num_2::user_def_num_3::user_def_num_4::user_def_note_1::user_def_note_2::task_per_each::use_pick_to_grid::ignore_weight_capture::stage_route_id::min_qty_ordered::max_qty_ordered::expected_volume::expected_weight::expected_value::customer_sku_desc1::customer_sku_desc2::purchase_order::product_price::product_currency::documentation_unit::extended_price::tax_1::tax_2::documentation_text_1::serial_number::owner_id::collective_mode::collective_sequence::ce_receipt_type::ce_coo::kit_plan_id::location_id::unallocatable::min_full_pallet_perc::max_full_pallet_perc::full_tracking_level_only::substitute_grade::disallow_substitution::session_time_zone_name::time_zone_name::nls_calendar::client_group::merge_action::merge_status::merge_error::merge_dstamp::final_order_line:'));
insert into dcsdba.interface_order_line(key,merge_dstamp,merge_status,merge_error,merge_action,order_id,line_id,sku_id,config_id,tracking_level,batch_id,batch_mixing,condition_id,lock_code,qty_ordered,allocate,back_ordered,deallocate,kit_split,origin_id,notes,customer_sku_id,shelf_life_days,shelf_life_percent,client_id,disallow_merge_rules,user_def_type_1,user_def_type_2,user_def_type_3,user_def_type_4,user_def_type_5,user_def_type_6,user_def_type_7,user_def_type_8,user_def_chk_1,user_def_chk_2,user_def_chk_3,user_def_chk_4,user_def_date_1,user_def_date_2,user_def_date_3,user_def_date_4,user_def_num_1,user_def_num_2,user_def_num_3,user_def_num_4,user_def_note_1,user_def_note_2,soh_id,line_value,time_zone_name,spec_code,rule_id,client_group,task_per_each,use_pick_to_grid,ignore_weight_capture,host_order_id,host_line_id,stage_route_id,min_qty_ordered,max_qty_ordered,expected_value,expected_volume,expected_weight,customer_sku_desc1,customer_sku_desc2,purchase_order,product_price,product_currency,documentation_unit,extended_price,tax_1,tax_2,documentation_text_1,serial_number,owner_id,ce_receipt_type,ce_coo,kit_plan_id,location_id,unallocatable,full_tracking_level_only,min_full_pallet_perc,max_full_pallet_perc,substitute_grade,disallow_substitution)
VALUES(v_interface_key, sysdate, 'Error', v_error, IN_MERGE_ACTION,IN_ORDER_ID,v_LINE_ID,IN_SKU_ID,IN_CONFIG_ID,IN_TRACKING_LEVEL,IN_BATCH_ID,IN_BATCH_MIXING,IN_CONDITION_ID,IN_LOCK_CODE,IN_QTY_ORDERED,IN_ALLOCATE,IN_BACK_ORDERED,IN_DEALLOCATE,IN_KIT_SPLIT,IN_ORIGIN_ID,IN_NOTES,IN_CUSTOMER_SKU_ID,IN_SHELF_LIFE_DAYS,IN_SHELF_LIFE_PERCENT,IN_CLIENT_ID,IN_DISALLOW_MERGE_RULES,IN_USER_DEF_TYPE_1,IN_USER_DEF_TYPE_2,IN_USER_DEF_TYPE_3,IN_USER_DEF_TYPE_4,IN_USER_DEF_TYPE_5,IN_USER_DEF_TYPE_6,IN_USER_DEF_TYPE_7,IN_USER_DEF_TYPE_8,IN_USER_DEF_CHK_1,IN_USER_DEF_CHK_2,IN_USER_DEF_CHK_3,IN_USER_DEF_CHK_4,IN_USER_DEF_DATE_1,IN_USER_DEF_DATE_2,IN_USER_DEF_DATE_3,IN_USER_DEF_DATE_4,IN_USER_DEF_NUM_1,IN_USER_DEF_NUM_2,IN_USER_DEF_NUM_3,IN_USER_DEF_NUM_4,IN_USER_DEF_NOTE_1,IN_USER_DEF_NOTE_2,IN_SOH_ID,IN_LINE_VALUE,IN_TIME_ZONE_NAME,IN_SPEC_CODE,IN_RULE_ID,IN_CLIENT_GROUP,IN_TASK_PER_EACH,IN_USE_PICK_TO_GRID,IN_IGNORE_WEIGHT_CAPTURE,IN_HOST_ORDER_ID,IN_HOST_LINE_ID,IN_STAGE_ROUTE_ID,IN_MIN_QTY_ORDERED,IN_MAX_QTY_ORDERED,IN_EXPECTED_VALUE,IN_EXPECTED_VOLUME,IN_EXPECTED_WEIGHT,IN_CUSTOMER_SKU_DESC1,IN_CUSTOMER_SKU_DESC2,IN_PURCHASE_ORDER,IN_PRODUCT_PRICE,IN_PRODUCT_CURRENCY,IN_DOCUMENTATION_UNIT,IN_EXTENDED_PRICE,IN_TAX_1,IN_TAX_2,IN_DOCUMENTATION_TEXT_1,IN_SERIAL_NUMBER,IN_OWNER_ID,IN_CE_RECEIPT_TYPE,IN_CE_COO,IN_KIT_PLAN_ID,IN_LOCATION_ID,IN_UNALLOCATABLE,IN_FULL_TRACKING_LEVEL_ONLY,IN_MIN_FULL_PALLET_PERC,IN_MAX_FULL_PALLET_PERC,IN_SUBSTITUTE_GRADE,IN_DISALLOW_SUBSTITUTION);
END IF;

COMMIT;
RETURN v_error;

END INTERFACE_ORDER_LINE;


/***********************************************************************************************************************/

/*                                                                                                                     */

/*  NAME:               TQ_INTERFACE.INTERFACE_LOCATION                                                                */

/*                                                                                                                     */

/*  DESCRIPTION:        This Function Uses The Interface Location API to Add Locations to the System                   */

/*                                                                                                                     */

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */

/*  =======     ===========     ==     ===================================================================             */

/*    1         2020-03-10      KS     Initial Version                                                                 */

/***********************************************************************************************************************/

/**********************GRANTS**************************

grant select on IF_L_PK_SEQ TO TORQUE;
grant select on location to TORQUE;
grant select on interface_location to TORQUE;

grant insert on interface_update_columns to TORQUE;
grant insert on interface_location to TORQUE;

grant delete on interface_update_columns to TORQUE;
grant delete on interface_location to TORQUE;

grant execute on libsession to TORQUE;
grant execute on libmergelocation to TORQUE;
grant execute on liberror to TORQUE;


******************************************************/


FUNCTION INTERFACE_LOCATION (
    in_update_columns VARCHAR2 DEFAULT NULL,
    in_merge_action VARCHAR2 DEFAULT 'U',
    in_time_zone_name VARCHAR2 DEFAULT NULL,
    in_site_id VARCHAR2,
    in_location_id VARCHAR2,
    in_loc_type VARCHAR2 DEFAULT NULL,
    in_lock_status VARCHAR2 DEFAULT NULL,
    in_check_string VARCHAR2 DEFAULT NULL,
    in_zone_1 VARCHAR2 DEFAULT NULL,
    in_subzone_1 VARCHAR2 DEFAULT NULL,
    in_subzone_2 VARCHAR2 DEFAULT NULL,
    in_work_zone VARCHAR2 DEFAULT NULL,
    in_storage_class VARCHAR2 DEFAULT NULL,
    in_in_stage VARCHAR2 DEFAULT NULL,
    in_out_stage VARCHAR2 DEFAULT NULL,
    in_pick_sequence NUMBER DEFAULT NULL,
    in_put_sequence NUMBER DEFAULT NULL,
    in_check_sequence NUMBER DEFAULT NULL,
    in_volume NUMBER DEFAULT NULL,
    in_height NUMBER DEFAULT NULL,
    in_depth NUMBER DEFAULT NULL,
    in_width NUMBER DEFAULT NULL,
    in_weight NUMBER DEFAULT NULL,
    in_disallow_alloc VARCHAR2 DEFAULT NULL,
    in_allow_pall_cnt VARCHAR2 DEFAULT NULL,
    in_abc_frequency VARCHAR2 DEFAULT NULL,
    in_abc_storage VARCHAR2 DEFAULT NULL,
    in_x_position NUMBER DEFAULT NULL,
    in_y_position NUMBER DEFAULT NULL,
    in_z_position NUMBER DEFAULT NULL,
    in_point_id1 VARCHAR2 DEFAULT NULL,
    in_point_id2 VARCHAR2 DEFAULT NULL,
    in_ab_chargeable VARCHAR2 DEFAULT NULL,
    in_beam_id VARCHAR2 DEFAULT NULL,
    in_beam_position INTEGER DEFAULT NULL,
    in_beam_units INTEGER DEFAULT NULL,
    in_qc_hold_reloc VARCHAR2 DEFAULT NULL,
    in_qc_rel_reloc VARCHAR2 DEFAULT NULL,
    in_user_def_type_1 VARCHAR2 DEFAULT NULL,
    in_user_def_type_2 VARCHAR2 DEFAULT NULL,
    in_user_def_type_3 VARCHAR2 DEFAULT NULL,
    in_user_def_type_4 VARCHAR2 DEFAULT NULL,
    in_user_def_type_5 VARCHAR2 DEFAULT NULL,
    in_user_def_type_6 VARCHAR2 DEFAULT NULL,
    in_user_def_type_7 VARCHAR2 DEFAULT NULL,
    in_user_def_type_8 VARCHAR2 DEFAULT NULL,
    in_user_def_chk_1 VARCHAR2 DEFAULT NULL,
    in_user_def_chk_2 VARCHAR2 DEFAULT NULL,
    in_user_def_chk_3 VARCHAR2 DEFAULT NULL,
    in_user_def_chk_4 VARCHAR2 DEFAULT NULL,
    in_user_def_date_1 VARCHAR2 DEFAULT NULL,
    in_user_def_date_2 VARCHAR2 DEFAULT NULL,
    in_user_def_date_3 VARCHAR2 DEFAULT NULL,
    in_user_def_date_4 VARCHAR2 DEFAULT NULL,
    in_user_def_num_1 NUMBER DEFAULT NULL,
    in_user_def_num_2 NUMBER DEFAULT NULL,
    in_user_def_num_3 NUMBER DEFAULT NULL,
    in_user_def_num_4 NUMBER DEFAULT NULL,
    in_user_def_note_1 VARCHAR2 DEFAULT NULL,
    in_user_def_note_2 VARCHAR2 DEFAULT NULL,
    in_fan_x INTEGER DEFAULT NULL,
    in_fan_y INTEGER DEFAULT NULL,
    in_fan_z INTEGER DEFAULT NULL,
    in_count_needed VARCHAR2 DEFAULT NULL,
    in_aisle VARCHAR2 DEFAULT NULL,
    in_bay VARCHAR2 DEFAULT NULL,
    in_levels VARCHAR2 DEFAULT NULL,
    in_position VARCHAR2 DEFAULT NULL,
    in_create_on_sort_loc VARCHAR2 DEFAULT NULL,
    in_sortation_location VARCHAR2 DEFAULT NULL,
    in_sortation_putaway VARCHAR2 DEFAULT NULL,
    in_no_put_empty_pallet VARCHAR2 DEFAULT NULL,
    in_stack VARCHAR2 DEFAULT NULL,
    in_no_new_empty_pallet VARCHAR2 DEFAULT NULL,
    in_total_qty_stockcheck VARCHAR2 DEFAULT NULL,
    in_aisle_side VARCHAR2 DEFAULT NULL,
    in_is_sortation_location VARCHAR2 DEFAULT NULL,
    in_orientation INTEGER DEFAULT NULL,
    in_rail_id VARCHAR2 DEFAULT NULL,
    in_locality VARCHAR2 DEFAULT NULL,
    in_ignore_volume_weight VARCHAR2 DEFAULT NULL,
    in_exclude_stock_checks VARCHAR2 DEFAULT NULL,
    in_move_task_status VARCHAR2 DEFAULT NULL,
    in_automation_id VARCHAR2 DEFAULT NULL,
    in_nls_calendar VARCHAR2 DEFAULT NULL,
    in_tag_merge VARCHAR2 DEFAULT NULL,
    in_max_units_on_loc INTEGER DEFAULT NULL,
    in_move_whole_endpoint VARCHAR2 DEFAULT NULL,
    in_allow_pick_steal VARCHAR2 DEFAULT NULL, 
    in_user_id VARCHAR2 DEFAULT 'API-INT', 
    in_station_id VARCHAR2 DEFAULT 'API-INT',
    in_record_error_in_table VARCHAR2 DEFAULT NULL
) RETURN VARCHAR2 IS PRAGMA autonomous_transaction;
v_retval NUMBER;
v_error VARCHAR2(20);
v_interface_key NUMBER;
v_user_def_date_1 TIMESTAMP := NULL;
v_user_def_date_2 TIMESTAMP := NULL;
v_user_def_date_3 TIMESTAMP := NULL;
v_user_def_date_4 TIMESTAMP := NULL;
BEGIN dcsdba.libsession.initialisesession(
  in_user_id, 'INT-API', in_station_id, 
  'INT-API'
);

IF in_user_def_date_1 IS NOT NULL THEN v_user_def_date_1 := to_timestamp(
  in_user_def_date_1, 'YYYYMMDDHH24MISS'
);
END IF;
IF in_user_def_date_2 IS NOT NULL THEN v_user_def_date_2 := to_timestamp(
  in_user_def_date_2, 'YYYYMMDDHH24MISS'
);
END IF;
IF in_user_def_date_3 IS NOT NULL THEN v_user_def_date_3 := to_timestamp(
  in_user_def_date_3, 'YYYYMMDDHH24MISS'
);
END IF;
IF in_user_def_date_4 IS NOT NULL THEN v_user_def_date_4 := to_timestamp(
  in_user_def_date_4, 'YYYYMMDDHH24MISS'
);
END IF;

v_retval := dcsdba.libmergelocation.directlocation(
p_mergeError => v_error,
p_toupdatecols => in_update_columns,
p_mergeaction => in_merge_action,
p_timezonename => in_time_zone_name,
p_siteid => in_site_id,
p_locationid => in_location_id,
p_loctype => in_loc_type,
p_lockstatus => in_lock_status,
p_checkstring => in_check_string,
p_zone1 => in_zone_1,
p_subzone1 => in_subzone_1,
p_subzone2 => in_subzone_2,
p_workzone => in_work_zone,
p_storageclass => in_storage_class,
p_instage => in_in_stage,
p_outstage => in_out_stage,
p_picksequence => in_pick_sequence,
p_putsequence => in_put_sequence,
p_checksequence => in_check_sequence,
p_volume => in_volume,
p_height => in_height,
p_depth => in_depth,
p_width => in_width,
p_weight => in_weight,
p_disallowalloc => in_disallow_alloc,
p_allowpallcnt => in_allow_pall_cnt,
p_abcfrequency => in_abc_frequency,
p_abcstorage => in_abc_storage,
p_xposition => in_x_position,
p_yposition => in_y_position,
p_zposition => in_z_position,
p_pointid1 => in_point_id1,
p_pointid2 => in_point_id2,
p_abchargeable => in_ab_chargeable,
p_beamid => in_beam_id,
p_beamposition => in_beam_position,
p_beamunits => in_beam_units,
p_qcholdreloc => in_qc_hold_reloc,
p_qcrelreloc => in_qc_rel_reloc,
p_userdeftype1 => in_user_def_type_1,
p_userdeftype2 => in_user_def_type_2,
p_userdeftype3 => in_user_def_type_3,
p_userdeftype4 => in_user_def_type_4,
p_userdeftype5 => in_user_def_type_5,
p_userdeftype6 => in_user_def_type_6,
p_userdeftype7 => in_user_def_type_7,
p_userdeftype8 => in_user_def_type_8,
p_userdefchk1 => in_user_def_chk_1,
p_userdefchk2 => in_user_def_chk_2,
p_userdefchk3 => in_user_def_chk_3,
p_userdefchk4 => in_user_def_chk_4,
p_userdefdate1 => v_user_def_date_1,
p_userdefdate2 => v_user_def_date_2,
p_userdefdate3 => v_user_def_date_3,
p_userdefdate4 => v_user_def_date_4,
p_userdefnum1 => in_user_def_num_1,
p_userdefnum2 => in_user_def_num_2,
p_userdefnum3 => in_user_def_num_3,
p_userdefnum4 => in_user_def_num_4,
p_userdefnote1 => in_user_def_note_1,
p_userdefnote2 => in_user_def_note_2,
p_fanx => in_fan_x,
p_fany => in_fan_y,
p_fanz => in_fan_z,
p_countneeded => in_count_needed,
p_aisle => in_aisle,
p_bay => in_bay,
p_levels => in_levels,
p_position => in_position,
p_createonsortloc => in_create_on_sort_loc,
p_sortationlocation => in_sortation_location,
p_sortationputaway => in_sortation_putaway,
p_noputemptypallet => in_no_put_empty_pallet,
p_stack => in_stack,
p_nonewemptypallet => in_no_new_empty_pallet,
p_totalqtystockcheck => in_total_qty_stockcheck,
p_aisleside => in_aisle_side,
p_issortationlocation => in_is_sortation_location,
p_orientation => in_orientation,
p_railid => in_rail_id,
p_locality => in_locality,
p_ignorevolumeweight => in_ignore_volume_weight,
p_excludestockchecks => in_exclude_stock_checks,
p_movetaskstatus => in_move_task_status,
p_automationid => in_automation_id,
p_tagmerge => in_tag_merge,
p_maxunitsonloc => in_max_units_on_loc,
p_movewholeendpoint => in_move_whole_endpoint,
p_allowpicksteal => in_allow_pick_steal
);

IF v_error is not null AND in_record_error_in_table = 'Y' then
    v_interface_key := DCSDBA.IF_L_PK_SEQ.nextval;
    insert into dcsdba.interface_update_columns(key,table_name,update_columns)VALUES(v_interface_key,'interface_location',nvl(in_update_columns,':KEY::SITE_ID::LOCATION_ID::CHECK_STRING::ZONE_1::SUBZONE_1::SUBZONE_2::WORK_ZONE::STORAGE_CLASS::LOC_TYPE::LOCK_STATUS::IN_STAGE::OUT_STAGE::PICK_SEQUENCE::PUT_SEQUENCE::CHECK_SEQUENCE::VOLUME::HEIGHT::DEPTH::WIDTH::WEIGHT::DISALLOW_ALLOC::ALLOW_PALL_CNT::ABC_FREQUENCY::ABC_STORAGE::X_POSITION::Y_POSITION::Z_POSITION::ORIENTATION::POINT_ID1::POINT_ID2::AB_CHARGEABLE::BEAM_ID::BEAM_POSITION::BEAM_UNITS::QC_HOLD_RELOC::QC_REL_RELOC::USER_DEF_TYPE_1::USER_DEF_TYPE_2::USER_DEF_TYPE_3::USER_DEF_TYPE_4::USER_DEF_TYPE_5::USER_DEF_TYPE_6::USER_DEF_TYPE_7::USER_DEF_TYPE_8::USER_DEF_CHK_1::USER_DEF_CHK_2::USER_DEF_CHK_3::USER_DEF_CHK_4::USER_DEF_DATE_1::USER_DEF_DATE_2::USER_DEF_DATE_3::USER_DEF_DATE_4::USER_DEF_NUM_1::USER_DEF_NUM_2::USER_DEF_NUM_3::USER_DEF_NUM_4::USER_DEF_NOTE_1::USER_DEF_NOTE_2::FAN_X::FAN_Y::FAN_Z::COUNT_NEEDED::AISLE::BAY::LEVELS::POSITION::CREATE_ON_SORT_LOC::SORTATION_LOCATION::IS_SORTATION_LOCATION::SORTATION_PUTAWAY::NO_PUT_EMPTY_PALLET::NO_NEW_EMPTY_PALLET::STACK::TOTAL_QTY_STOCKCHECK::AISLE_SIDE::RAIL_ID::LOCALITY::IGNORE_VOLUME_WEIGHT::EXCLUDE_STOCK_CHECKS::EXCLUDE_STOCK_ADJUSTMENT::MOVE_TASK_STATUS::MAX_UNITS_ON_LOC::AUTOMATION_ID::TAG_MERGE::MOVE_WHOLE_ENDPOINT::ALLOW_PICK_STEAL::SESSION_TIME_ZONE_NAME::TIME_ZONE_NAME::NLS_CALENDAR:'));
    insert into dcsdba.interface_location(key,merge_dstamp,merge_status,merge_error,merge_action,site_id,location_id,check_string,zone_1,subzone_1,subzone_2,work_zone,storage_class,loc_type,lock_status,in_stage,out_stage,pick_sequence,put_sequence,check_sequence,volume,height,depth,width,weight,disallow_alloc,allow_pall_cnt,abc_frequency,abc_storage,x_position,y_position,z_position,orientation,point_id1,point_id2,ab_chargeable,beam_id,beam_position,beam_units,qc_hold_reloc,qc_rel_reloc,user_def_type_1,user_def_type_2,user_def_type_3,user_def_type_4,user_def_type_5,user_def_type_6,user_def_type_7,user_def_type_8,user_def_chk_1,user_def_chk_2,user_def_chk_3,user_def_chk_4,user_def_date_1,user_def_date_2,user_def_date_3,user_def_date_4,user_def_num_1,user_def_num_2,user_def_num_3,user_def_num_4,user_def_note_1,user_def_note_2,fan_x,fan_y,fan_z,count_needed,aisle,bay,levels,position,create_on_sort_loc,sortation_location,is_sortation_location,sortation_putaway,no_put_empty_pallet,no_new_empty_pallet,stack,total_qty_stockcheck,aisle_side,rail_id,locality,ignore_volume_weight,exclude_stock_checks,move_task_status,max_units_on_loc,automation_id,tag_merge,move_whole_endpoint,allow_pick_steal)
    VALUES(v_interface_key, sysdate, 'Error', v_error, IN_Merge_Action, in_site_id,in_location_id,in_check_string,in_zone_1,in_subzone_1,in_subzone_2,in_work_zone,in_storage_class,in_loc_type,in_lock_status,in_in_stage,in_out_stage,in_pick_sequence,in_put_sequence,in_check_sequence,in_volume,in_height,in_depth,in_width,in_weight,in_disallow_alloc,in_allow_pall_cnt,in_abc_frequency,in_abc_storage,in_x_position,in_y_position,in_z_position,in_orientation,in_point_id1,in_point_id2,in_ab_chargeable,in_beam_id,in_beam_position,in_beam_units,in_qc_hold_reloc,in_qc_rel_reloc,in_user_def_type_1,in_user_def_type_2,in_user_def_type_3,in_user_def_type_4,in_user_def_type_5,in_user_def_type_6,in_user_def_type_7,in_user_def_type_8,in_user_def_chk_1,in_user_def_chk_2,in_user_def_chk_3,in_user_def_chk_4,v_user_def_date_1,v_user_def_date_2,v_user_def_date_3,v_user_def_date_4,in_user_def_num_1,in_user_def_num_2,in_user_def_num_3,in_user_def_num_4,in_user_def_note_1,in_user_def_note_2,in_fan_x,in_fan_y,in_fan_z,in_count_needed,in_aisle,in_bay,in_levels,in_position,in_create_on_sort_loc,in_sortation_location,in_is_sortation_location,in_sortation_putaway,in_no_put_empty_pallet,in_no_new_empty_pallet,in_stack,in_total_qty_stockcheck,in_aisle_side,in_rail_id,in_locality,in_ignore_volume_weight,in_exclude_stock_checks,in_move_task_status,in_max_units_on_loc,in_automation_id,in_tag_merge,in_move_whole_endpoint,in_allow_pick_steal);
    END IF;
COMMIT;
RETURN v_error;

END INTERFACE_LOCATION;

/***********************************************************************************************************************/

/*                                                                                                                     */

/*  NAME:               TQ_INTERFACE.JSON_ERROR                                                                        */

/*                                                                                                                     */

/*  DESCRIPTION:        This Function Takes Output From API Functions and Returns A JSON String                         */

/*                                                                                                                     */

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */

/*  =======     ===========     ==     ===================================================================             */

/*    1         2020-04-14      KS     Initial Version                                                                 */

/***********************************************************************************************************************/

/**********************GRANTS**************************

grant select on language_text TO TORQUE;

grant execute on liberror to TORQUE;


******************************************************/

FUNCTION JSON_ERROR(
p_label VARCHAR2
)RETURN VARCHAR2 IS
v_text VARCHAR2(250);
BEGIN

IF p_label is null then
    RETURN '{ "Success": 1, "MergeError": "", "ErrorMessage": ""}';
ELSE
    BEGIN
        select text into v_text from dcsdba.language_text where language = 'EN_GB' AND label = upper(p_label) AND rownum = 1;
        EXCEPTION WHEN NO_DATA_FOUND THEN
            RETURN '{ "Success": 0, "MergeError": "'||p_label||'", "ErrorMessage": ""}';
    END;
    
    RETURN '{ "Success": 0, "MergeError": "'||p_label||'", "ErrorMessage": "'||v_text||'"}';
    
END IF;
EXCEPTION WHEN OTHERS THEN
            dcsdba.liberror.writeerrorlog('TQ_INTERFACE.JSON_ERROR', sqlcode, sqlerrm);
            RETURN '{ "Success": 0, "MergeError": "'||p_label||'", "ErrorMessage": "SQL Exception Occured: Please See Error Table For More Details"}';
END JSON_ERROR;

/**********************GRANTS**************************

grant select on dcsdba.IF_RLC_PK_SEQ TO TORQUE;

grant insert on dcsdba.interface_update_columns to TORQUE;
grant insert on dcsdba.interface_inventory_move to TORQUE;

grant execute on dcsdba.libsession to TORQUE;
grant execute on dcsdba.libmergeinvmove to TORQUE;

******************************************************/
function interface_relocation_task
(
  in_update_columns varchar2 default null,
  in_merge_action varchar2 default 'A',
  in_to_loc_id varchar2,
  in_tag_id varchar2,
  in_sku_id varchar2,
  in_pallet_id varchar2 default null,
  in_from_loc_id varchar2,
  in_client_id varchar2,
  in_site_id varchar2,
  in_quantity number,
  in_move_if_allocated varchar2 default 'N',
  in_move_task_status varchar2 default 'Released',
  in_find_location varchar2 default 'N',
  in_disallow_tag_swap varchar2 default null,
  in_timezone_name varchar2 default null,
  in_user_id varchar2 default 'API-INT',
  in_station_id varchar2 default 'API-INT',
  in_record_error_in_table varchar2 default null
)
return varchar2 is pragma autonomous_transaction;
v_retval number;
v_error varchar2(20);
v_interface_key number(10);

begin
    dcsdba.libsession.initialisesession(in_user_id, 'API-INT', in_station_id, 'API-INT');

    v_retval := dcsdba.libmergeinvmove.directinventorymove
    (
        p_MergeError => v_error, 
        p_ToUpdateCols => in_update_columns, 
        p_MergeAction => in_merge_action, 
        p_ToLocId => in_to_loc_id, 
        p_TagId => in_tag_id, 
        p_SkuId => in_sku_id, 
        p_PalletId => in_pallet_id, 
        p_FromLocId  => in_from_loc_id, 
        p_ClientId  => in_client_id,
        p_SiteId => in_site_id, 
        p_Quantity => in_quantity, 
        p_MoveIfAllocated => in_move_if_allocated, 
        p_MoveTaskStatus => in_move_task_status, 
        p_FindLocation => in_find_location, 
        p_DisallowTagSwap => in_disallow_tag_swap, 
        p_TimeZoneName => in_timezone_name
    );

    if v_error is not null and in_record_error_in_table = 'Y' then
        v_interface_key := dcsdba.IF_RLC_PK_SEQ.nextval;

        insert into dcsdba.interface_update_columns(key,table_name,update_columns)values(v_interface_key,'interface_inventory_move',nvl(in_update_columns,':site_id::client_id::from_loc_id::to_loc_id::sku_id::tag_id::quantity:'));

        insert into dcsdba.interface_inventory_move
        (
            key, merge_dstamp, merge_status, merge_error, merge_action, to_loc_id, site_id, tag_id, client_id, pallet_id, sku_id, from_loc_id
            , quantity, move_if_allocated, move_task_status, find_location, disallow_tag_swap, time_zone_name
        )
        values
        (
            v_interface_key, sysdate, 'Error', v_error, in_merge_action, in_to_loc_id, in_site_id, in_tag_id, in_client_id, in_pallet_id, in_sku_id
            , in_from_loc_id, in_quantity, in_move_if_allocated, in_move_task_status, in_find_location, in_disallow_tag_swap, in_timezone_name
        );

    end if;
    commit;
    return v_error;

end interface_relocation_task;

END tq_interface;

/

  GRANT EXECUTE ON "TORQUE"."TQ_INTERFACE" TO "DCSDBA";
--------------------------------------------------------
--  DDL for Package Body TQ_HOUSEKEEPING
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "TORQUE"."TQ_HOUSEKEEPING" AS

/***********************************************************************************************************************/ 

/*                                                                                                                     */ 

/*  NAME:               TQ_HOUSEKEEPING.TQ_UPDATE_UPLOADED                                                             */ 

/*                                                                                                                     */ 

/*  DESCRIPTION:        This Procedure Updates Orphaned Records Which Do Not Have Uploaded = Y                         */ 

/*                                                                                                                     */ 

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */ 

/*  =======     ===========     ==     ===================================================================             */ 

/*    1         2019-07-30      KS     Initial Version                                                                 */ 

/*    2         2019-08-07      KS     Removed Count + Improved Exit                                                   */ 

/*                                                                                                                     */  

/***********************************************************************************************************************/ 

/**********************GRANTS**************************

grant select on pre_advice_header to TORQUE;
grant select on order_header to TORQUE;
grant select on shipping_manifest to TORQUE;  

grant update on shipping_manifest to TORQUE;
grant update on pre_advice_header to TORQUE;
grant update on order_header to TORQUE;   

grant execute on liberror to TORQUE;

******************************************************/

    PROCEDURE tq_update_uploaded IS
        v_loops NUMBER := 0;
    BEGIN

	--Loop Through Order Header Records 5000 at a time, setting the Uploaded Flag to 'Y'
        LOOP
            UPDATE 
	/* TORQUE.TQ_UPDATE_UPLOADED - Order Header Uploaded UPDATE */ dcsdba.order_header
            SET
                uploaded = 'Y'
            WHERE
                status IN (
                    'Shipped',
                    'Cancelled'
                )
                AND creation_date < trunc(SYSDATE) - 90
                AND uploaded = 'N'
                AND ROWNUM <= 5000;

            EXIT WHEN SQL%notfound;
            COMMIT;
        END LOOP;

	--Loop Through Pre Advice Header Records 5000 at a time, setting the Uploaded Flag to 'Y'

        FOR x IN 1..v_loops LOOP
            UPDATE 
	/* TORQUE.TQ_UPDATE_UPLOADED - Pre Advice Header Uploaded UPDATE */ dcsdba.pre_advice_header
            SET
                uploaded = 'Y'
            WHERE
                status IN (
                    'Complete'
                )
                AND creation_date < trunc(SYSDATE) - 90
                AND uploaded = 'N'
                AND ROWNUM <= 5000;

    EXIT WHEN SQL%notfound;
            COMMIT;
        END LOOP;

	--Loop Through Shipping Manifest Records 5000 at a time, setting the Uploaded Flag to 'Y'

        FOR x IN 1..v_loops LOOP
            UPDATE 
	/* TORQUE.TQ_UPDATE_UPLOADED - Shipping Manifest Uploaded UPDATE */ dcsdba.shipping_manifest
            SET
                uploaded = 'Y'
            WHERE
                shipped_dstamp < trunc(SYSDATE) - 90
                AND uploaded = 'N'
                AND ROWNUM <= 5000;

            EXIT WHEN SQL%notfound;
            COMMIT;
        END LOOP;

    EXCEPTION
        WHEN OTHERS THEN
            dcsdba.liberror.writeerrorlog('TQ_HOUSEKEEPING.TQ_UPDATE_UPLOADED', sqlcode, sqlerrm);
    END tq_update_uploaded;

/***********************************************************************************************************************/ 

/*                                                                                                                     */ 

/*  NAME:               TQ_HOUSEKEEPING.TQ_GDPR_COMPLIANCE                                                             */ 

/*                                                                                                                     */ 

/*  DESCRIPTION:        This Procedure Removes Personal Details From Archived Orders                                   */ 

/*                                                                                                                     */ 

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */ 

/*  =======     ===========     ==     ===================================================================             */ 

/*    1         2019-07-26      KS     Initial Version                                                                 */ 

/*    2         2019-08-12      KS     Removed Count + Improved Exit                                                   */ 

/*    3         2019-08-21      KS     Added Order Type JACPY for DX                                                   */ 

/*                                                                                                                     */  

/***********************************************************************************************************************/ 

/**********************GRANTS**************************

grant select on order_header_archive to TORQUE; 

grant update on order_header_archive to TORQUE; 

grant execute on liberror to TORQUE;

******************************************************/

    PROCEDURE tq_gdpr_compliance IS
    BEGIN

	--Loop Through Order Header Archive Records, Removing All Personal Details 5000 Records At A Time
        LOOP
            UPDATE 
	/* TORQUE.TQ_GDPR_COMPLIANCE - Order Header Archive UPDATE */ dcsdba.order_header_archive
            SET
                inv_contact = NULL,
                inv_contact_phone = NULL,
                inv_contact_mobile = NULL,
                inv_contact_fax = NULL,
                inv_contact_email = NULL,
                inv_name = NULL,
                inv_address1 = NULL,
                inv_address2 = NULL,
                inv_town = NULL,
                inv_postcode = NULL,
                inv_county = NULL,
                contact = NULL,
                contact_phone = NULL,
                contact_mobile = NULL,
                contact_fax = NULL,
                contact_email = NULL,
                name = 'GDPR',
                address1 = NULL,
                address2 = NULL,
                town = NULL,
                county = NULL,
                postcode = NULL
            WHERE
                order_type IN (
                    'SPECIAL',
                    'STANDARD',
                    'GIFTMSG',
                    'GIFTWRAP',
                    'WEB',
                    'HHWEB',
                    'HOWEB',
                    'NIWEB',
                    'JACPY'
                )
                AND name <> 'GDPR'
                AND ROWNUM <= 5000;

            EXIT WHEN SQL%notfound;
            COMMIT;
        END LOOP;
    EXCEPTION
        WHEN OTHERS THEN
            dcsdba.liberror.writeerrorlog('TQ_HOUSEKEEPING.TQ_GDPR_COMPLIANCE', sqlcode, sqlerrm);
    END tq_gdpr_compliance;

/***********************************************************************************************************************/ 

/*                                                                                                                     */ 

/*  NAME:               TQ_HOUSEKEEPING.TQ_ACCOUNT_SECURITY                                                            */ 

/*                                                                                                                     */ 

/*  DESCRIPTION:        This Procedure Ensures Application User Settings Are Set Correctly                             */ 

/*                                                                                                                     */ 

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */ 

/*  =======     ===========     ==     ===================================================================             */ 

/*    1         2019-07-30      KS     Initial Version                                                                 */ 

/*    2         2019-08-07      KS     Added Account Delete Days                                                       */ 

/*                                                                                                                     */  

/***********************************************************************************************************************/ 

/**********************GRANTS**************************

grant select on application_user to TORQUE; 

grant update on application_user to TORQUE; 

grant execute on liberror to TORQUE;

******************************************************/

    PROCEDURE tq_account_security IS
    BEGIN
        UPDATE dcsdba.application_user
        SET
            disable_days = 90
        WHERE
            ( disable_days IS NULL
              OR disable_days > 90 )
            AND user_id != 'SUPPORT';

        UPDATE dcsdba.application_user
        SET
            delete_days = 180
        WHERE
            ( delete_days IS NULL
              OR delete_days > 180 )
            AND user_id != 'SUPPORT';

        COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            dcsdba.liberror.writeerrorlog('TQ_HOUSEKEEPING.TQ_ACCOUNT_SECURITY', sqlcode, sqlerrm);
    END tq_account_security;

/***********************************************************************************************************************/ 

/*                                                                                                                     */ 

/*  NAME:               TQ_HOUSEKEEPING.TQ_PURGE_ITL_ARCHIVE                                                           */ 

/*                                                                                                                     */ 

/*  DESCRIPTION:        This Procedure More Efficiently Purges The Inventory Transaction Archive Table (Global Only)   */ 

/*                                                                                                                     */ 

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */ 

/*  =======     ===========     ==     ===================================================================             */ 

/*    1         2019-09-19      KS     Initial Version                                                                 */ 

/*                                                                                                                     */  

/***********************************************************************************************************************/ 

/**********************GRANTS**************************

grant select on inventory_transaction_archive to TORQUE; 
grant select on purge_setup to TORQUE; 

grant delete on inventory_transaction_archive to TORQUE; 

******************************************************/

    PROCEDURE tq_purge_itl_archive (
        p_dstamp         IN   VARCHAR2 DEFAULT 'PURGE',
        p_commit_every   IN   NUMBER DEFAULT 5000
    ) IS
        TYPE t_row_ids IS
            TABLE OF ROWID;
        l_row_ids   t_row_ids;
        v_dstamp    VARCHAR2(10);
    BEGIN
        IF p_dstamp = 'PURGE' THEN
            SELECT
                TO_CHAR(SYSDATE - SUM(purge_days), 'DD/MM/YYYY') AS purge_date
            INTO v_dstamp
            FROM
                dcsdba.purge_setup
            WHERE
                purge_id IN (
                    'INVENTORY_TRANSACTION_ARCHIVE',
                    'INVENTORY_TRANSACTION'
                )
                AND site_id IS NULL
                AND client_id IS NULL;

        ELSE
            v_dstamp := p_dstamp;
        END IF;

--For each RowID in l_row_ids collection

        FOR i IN (
            SELECT
                KEY,
                ROWNUM
            FROM
                dcsdba.inventory_transaction_archive ita
            WHERE
                dstamp between TO_DATE(v_dstamp, 'DD/MM/YYYY')-28 AND TO_DATE(v_dstamp, 'DD/MM/YYYY')
        ) LOOP
            DELETE FROM dcsdba.inventory_transaction_archive
            WHERE
                KEY = i.KEY;

--Commit Every X Records

            IF MOD(i.rownum, p_commit_every) = 0 THEN
                COMMIT;
            END IF;
        END LOOP;

--Final Commit for remaining Records

        COMMIT;


--Rollback and log Exceptions
    EXCEPTION
        WHEN OTHERS THEN
            ROLLBACK;
            dcsdba.liberror.writeerrorlog('TQ_HOUSEKEEPING.TQ_PURGE_ITL_ARCHIVE', sqlcode, sqlerrm);
    END tq_purge_itl_archive;

END tq_housekeeping;

/

  GRANT EXECUTE ON "TORQUE"."TQ_HOUSEKEEPING" TO "DCSDBA";
--------------------------------------------------------
--  DDL for Package Body TQ_BULK_PICK
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "TORQUE"."TQ_BULK_PICK" AS

/***********************************************************************************************************************/ 

/*                                                                                                                     */ 

/*  NAME:               TQ_BULK_PICK.SEARCH_LOCATION                                                                   */ 

/*                                                                                                                     */ 

/*  DESCRIPTION:        To Identify An Order Task For A SKU/Location and Sort It                                       */ 

/*                                                                                                                     */ 

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */ 

/*  =======     ===========     ==     ===================================================================             */ 

/*    1         2019-07-12      KS     Initial Version                                                                 */ 

/*    2			2019-07-22		KS	   Modified To Support 2 Stage SORT	                                               */

/*    3			2019-09-27		KS	   Modified To Choose Order ID by lowest string                                    */

/*    4			2019-10-01		KS	   Added Support Mode Parameter To Swap Between 1 And 2 Stage Sort                 */

/*    5 		2020-02-26		KS	   Removed 2 Stage Sort Logic - Another Package should be used if this is needed   */

/*                                                                                                                     */  

/***********************************************************************************************************************/  

/**********************GRANTS**************************

grant select on move_task to TORQUE;
grant select on location to TORQUE;

grant update on move_task to TORQUE;

grant execute on liberror to TORQUE;

******************************************************/

    FUNCTION search_location (
        p_site_id       IN   VARCHAR2,
        p_client_id     IN   VARCHAR2,
        p_user_id       IN   VARCHAR2,
        p_station_id    IN   VARCHAR2,
        p_from_loc_id   IN   VARCHAR2,
        p_sku_id        IN   VARCHAR2
    ) RETURN VARCHAR2 AS

        PRAGMA autonomous_transaction;
        v_location_id   VARCHAR2(40);
        v_key           NUMBER;
        v_sort_type     VARCHAR(50);
        v_from_value    VARCHAR2(5);
        v_temp          NUMBER;
    BEGIN 

		--Clear Existing Buffer For Station
        v_temp := rdtbuffer.deletebufferdata(NULL, NULL, p_station_id, 'SORT_LOCATION');
        v_temp := rdtbuffer.deletebufferdata(NULL, NULL, p_station_id, 'SORT_KEY');
        v_temp := rdtbuffer.deletebufferdata(NULL, NULL, p_station_id, 'SORT_SKU');


		--Identify Order For Stock And Search For Existing Sortation Wall Location For That Order -- Also Store The Move Task Key For The Identified Order Task
        BEGIN
        
        --Find Which BULK Set Is Being Used
        v_from_value := substr(p_from_loc_id,LENGTH(p_client_id)+5);
            WITH q AS (
                SELECT
                    *
                FROM
                    (
                        SELECT
                            mt.key,
                            mt.site_id,
                            mt.client_id,
                            mt.task_id,
                            mt.tag_id,
                            mt.from_loc_id,
                            mt.to_loc_id,
                            mt.sku_id,
                            mt.description,
                            mt.qty_to_move,
                            ROW_NUMBER() OVER(
                                ORDER BY
                                    task_id
                            ) AS sequ
                        FROM
                            dcsdba.move_task mt
                        WHERE
                            mt.from_loc_id = p_from_loc_id
                            AND mt.site_id = p_site_id
                            AND mt.client_id = p_client_id
                            AND mt.sku_id = p_sku_id
                            AND mt.task_type = 'O'
                            AND mt.status IN (
                                'Hold',
                                'Released'
                            )
                    )
                WHERE
                    sequ = 1
            )
            SELECT
                nvl(mt.from_loc_id, mt2.to_loc_id) AS location_id,
                q.key
            INTO
                v_location_id,
                v_key
            FROM
                q                  left
                JOIN dcsdba.move_task   mt ON ( q.task_id = mt.task_id
                                              AND q.site_id = mt.site_id
                                              AND q.client_id = mt.client_id
                                              AND mt.from_loc_id LIKE p_client_id
                                                                      || 'TROL'
                                                                      || v_from_value
                                                                      || '_-__' )
                LEFT JOIN dcsdba.move_task   mt2 ON ( q.task_id = mt2.task_id
                                                    AND q.site_id = mt2.site_id
                                                    AND q.client_id = mt2.client_id
                                                    AND mt2.to_loc_id LIKE p_client_id
                                                                           || 'TROL'
                                                                           || v_from_value
                                                                           || '_-__' )
            WHERE
                ROWNUM = 1;

        EXCEPTION
            WHEN no_data_found THEN
                NULL;
        END;

		--IF No Existing Location can be found then identify the next sortation Location within the sortation wall following the putaway sequence

        IF v_location_id IS NULL THEN
            BEGIN
                SELECT
                    location_id
                INTO v_location_id
                FROM
                    (
                        SELECT
                            location_id,
                            ROW_NUMBER() OVER(
                                ORDER BY
                                    put_sequence
                            ) AS sequ
                        FROM
                            (
                                SELECT
                                    l.location_id,
                                    nvl(l.user_def_num_2,1) AS max_orders,
                                    l.put_sequence,
                                    COUNT(DISTINCT mt.task_id) AS tasks,
                                    COUNT(DISTINCT mt2.task_id) AS tasks_to
                                FROM
                                    dcsdba.location    l
                                    LEFT JOIN dcsdba.move_task   mt ON ( l.location_id = mt.from_loc_id
                                                                       AND l.site_id = mt.site_id )
                                    LEFT JOIN dcsdba.move_task   mt2 ON ( l.location_id = mt2.to_loc_id
                                                                        AND l.site_id = mt2.site_id )
                                WHERE
                                    l.site_id = p_site_id
                                    AND l.lock_status = 'UnLocked'
                                    AND ( l.location_id LIKE p_client_id
                                                             || 'TROL'
                                                             || v_from_value
                                                             || '_-__' )
                                GROUP BY
                                    l.location_id,
                                    nvl(l.user_def_num_2,1),
                                    l.put_sequence
                            )
                        WHERE
                            ( tasks + tasks_to ) < nvl(max_orders, 1)
                    )
                WHERE
                    sequ = 1;

            EXCEPTION
                WHEN no_data_found THEN
                    NULL;
            END;
        END IF;

		--IF a location has been found above then

        IF v_location_id IS NOT NULL THEN

		--Update Move Task with the Found Sortation Location
            UPDATE dcsdba.move_task
            SET
                to_loc_id = v_location_id
            WHERE
                key = v_key;

		--Store the Move Task Key Found in V_RDT_BUFFER

            v_temp := torque.rdtbuffer.setbufferdata(NULL, p_station_id, p_station_id, 'SORT_KEY', v_key);

            COMMIT;
        END IF;

        RETURN v_location_id;
    EXCEPTION
        WHEN OTHERS THEN
            dcsdba.liberror.writeerrorlog('TQ_BULK_PICK.SEARCH_LOCATION', sqlcode, sqlerrm);
    END search_location;

/***********************************************************************************************************************/ 

/*                                                                                                                     */ 

/*  NAME:               TQ_BULK_PICK.PROCESS_STOCK                                                                     */ 

/*                                                                                                                     */ 

/*  DESCRIPTION:        Process Order Tasks Which Has Been Sorted                                                      */ 

/*                                                                                                                     */ 

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */ 

/*  =======     ===========     ==     ===================================================================             */ 

/*    1         2019-07-15      KS     Initial Version                                                                 */ 

/*    2         2019-10-08      KS     Added Logic to store Sorted Location Into Order Header HUB_COUNTY               */ 

/*    3         2020-02-05      KS     Modified To Remove the need for Site/Client Parameters                          */

/*                                                                                                                     */  

/***********************************************************************************************************************/  

/**********************GRANTS**************************

grant select on move_task to TORQUE;

grant update on move_task to TORQUE;
grant update on order_header to TORQUE;

grant insert on move_task to TORQUE;

grant execute on liberror to TORQUE;

******************************************************/

    FUNCTION process_stock (
        p_user_id      IN   VARCHAR2,
        p_station_id   IN   VARCHAR2
    ) RETURN NUMBER AS

        PRAGMA autonomous_transaction;
        v_key             NUMBER;
        v_client_id       VARCHAR2(40);
        v_order_id        VARCHAR2(40);
        v_sort_location   VARCHAR2(40);
        v_qty_to_move     NUMBER;
        v_new_key         NUMBER;
        v_temp            NUMBER;
    BEGIN
        --Read The Move Task Key Captured During the SEARCH_LOCATION Function
        v_key := torque.rdtbuffer.readbufferdata(NULL, p_station_id, p_station_id, 'SORT_KEY');
        BEGIN

            --Find the QTY_TO_MOVE for the Move Task Identified
            SELECT
                client_id,
                task_id,
                qty_to_move,
                to_loc_id
            INTO
                v_client_id,
                v_order_id,
                v_qty_to_move,
                v_sort_location
            FROM
                dcsdba.move_task
            WHERE
                key = v_key;

        EXCEPTION
            WHEN OTHERS THEN
                ROLLBACK;
                RETURN 0;
        END;

        --If QTY_TO_MOVE is 1 then stamp User + Station to the Move Task and set it to Complete

        IF v_qty_to_move = 1 THEN
            UPDATE dcsdba.move_task
            SET
                status = 'Complete',
                user_id = p_user_id,
                station_id = p_station_id
            WHERE
                key = v_key;

            COMMIT;
        -- If QTY_TO_MOVE is Greater than 1 then Insert a New Move Task with the same Details as the original but with QTY_TO_MOVE + OLD_QTY_TO_MOVE Being 1 Less
        -- Then set the QTY_TO_MOVE + OLD_QTY_TO_MOVE to 1 on the original Move task, stamp it with the User + Station + set the Reason Code To EX_SP and set it to Complete.
        ELSIF v_qty_to_move > 1 THEN
            FOR x IN (
                SELECT
                    *
                FROM
                    dcsdba.move_task
                WHERE
                    key = v_key
            ) LOOP
                v_new_key := dcsdba.move_task_pk_seq.nextval;
                INSERT INTO dcsdba.move_task (
                    key,
                    first_key,
                    task_id,
                    task_type,
                    line_id,
                    client_id,
                    sku_id,
                    config_id,
                    description,
                    tag_id,
                    old_tag_id,
                    customer_id,
                    origin_id,
                    condition_id,
                    qty_to_move,
                    old_qty_to_move,
                    site_id,
                    from_loc_id,
                    old_from_loc_id,
                    to_loc_id,
                    old_to_loc_id,
                    owner_id,
                    sequence,
                    status,
                    list_id,
                    dstamp,
                    start_dstamp,
                    finish_dstamp,
                    original_dstamp,
                    priority,
                    work_zone,
                    consignment,
                    bol_id,
                    session_type,
                    summary_record,
                    repack,
                    kit_ratio,
                    ce_under_bond,
                    uploaded_labor,
                    print_label_id,
                    print_label,
                    repack_qc_done,
                    catch_weight,
                    moved_lock_status,
                    stage_route_sequence,
                    labelling,
                    first_pick,
                    plan_sequence,
                    ce_rotation_id,
                    move_whole,
                    shipping_unit,
                    final_loc_id
                ) VALUES (
                    v_new_key,
                    v_new_key,
                    x.task_id,
                    x.task_type,
                    x.line_id,
                    x.client_id,
                    x.sku_id,
                    x.config_id,
                    x.description,
                    x.tag_id,
                    x.old_tag_id,
                    x.customer_id,
                    x.origin_id,
                    x.condition_id,
                    x.qty_to_move - 1,
                    x.old_qty_to_move - 1,
                    x.site_id,
                    x.from_loc_id,
                    x.old_from_loc_id,
                    x.client_id || 'SORTXXX',
                    x.client_id || 'SORTXXX',
                    x.owner_id,
                    x.sequence,
                    x.status,
                    x.list_id,
                    x.dstamp,
                    x.start_dstamp,
                    x.finish_dstamp,
                    x.original_dstamp,
                    x.priority,
                    x.work_zone,
                    x.consignment,
                    x.bol_id,
                    x.session_type,
                    x.summary_record,
                    x.repack,
                    x.kit_ratio,
                    x.ce_under_bond,
                    x.uploaded_labor,
                    x.print_label_id,
                    x.print_label,
                    x.repack_qc_done,
                    x.catch_weight,
                    x.moved_lock_status,
                    x.stage_route_sequence,
                    x.labelling,
                    x.first_pick,
                    x.plan_sequence,
                    x.ce_rotation_id,
                    x.move_whole,
                    x.shipping_unit,
                    x.final_loc_id
                );

            END LOOP;

            --As Above Set QTY_TO_MOVE + OLD_QTY_TO_MOVE to 1, set the Reason Code To EX_SP, Stamp the User + Station and set the status to Complete

            UPDATE dcsdba.move_task
            SET
                status = 'Complete',
                user_id = p_user_id,
                station_id = p_station_id,
                qty_to_move = 1,
                old_qty_to_move = 1,
                reason_code = 'EX_SP'
            WHERE
                key = v_key;

            COMMIT;
        END IF;

        --UPDATE ORDER_HEADER WITH SORTATION SLOT For Use on the Despatch Notes

        UPDATE dcsdba.order_header
        SET
            hub_county = v_sort_location
        WHERE
            client_id = v_client_id
            AND order_id = v_order_id;

        COMMIT;

        --Clear All Stored Temporary Buffer Data
        v_temp := rdtbuffer.deletebufferdata(NULL, NULL, p_station_id, 'SORT_LOCATION');
        v_temp := rdtbuffer.deletebufferdata(NULL, NULL, p_station_id, 'SORT_KEY');
        v_temp := rdtbuffer.deletebufferdata(NULL, NULL, p_station_id, 'SORT_SKU');
        RETURN 1;
    EXCEPTION
        WHEN OTHERS THEN
            dcsdba.liberror.writeerrorlog('TQ_BULK_PICK.PROCESS_STOCK', sqlcode, sqlerrm);
    END process_stock;

/***********************************************************************************************************************/ 

/*                                                                                                                     */ 

/*  NAME:               TQ_BULK_PICK.SORT_VALIDATION                                                                   */ 

/*                                                                                                                     */ 

/*  DESCRIPTION:        Identifies Order/Sort Location and Whether the Order Is Fully Sorted                           */ 

/*                                                                                                                     */ 

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */ 

/*  =======     ===========     ==     ===================================================================             */ 

/*    1         2019-07-16      KS     Initial Version                                                                 */ 

/*    2         2019-07-27      KS     Reworked To Allow Validation Of Full Trolley                                    */ 

/*                                                                                                                     */  

/***********************************************************************************************************************/  

/**********************GRANTS**************************

grant select on move_task to TORQUE;

grant execute on liberror to TORQUE;

******************************************************/

    FUNCTION sort_validation (
        p_site_id       VARCHAR2,
        p_client_id     VARCHAR2,
        p_station_id    VARCHAR2,
        p_user_id       VARCHAR2,
        p_location_id   VARCHAR2
    ) RETURN NUMBER AS

        PRAGMA autonomous_transaction;
        exception_invalid_reference EXCEPTION;
        exception_not_in_sort EXCEPTION;
        v_location_id   VARCHAR2(30);
        v_count         NUMBER;
        v_valid_check   NUMBER := 0;
        v_temp          NUMBER;
    BEGIN

        --Clear Existing Buffer For Station
        v_temp := rdtbuffer.deletebufferdata(NULL, NULL, p_station_id, 'FROM_SORT');

        --Prep the Location Variables with the Location/Order Parameters in Upper Case
        v_location_id := upper(p_location_id);

        --IF The Location ID is not Valid the Raise Exception
        SELECT
            COUNT(*)
        INTO v_valid_check
        FROM
            dcsdba.move_task mt
        WHERE
            client_id = p_client_id
            AND ( from_loc_id LIKE v_location_id || '-__'
                  OR from_loc_id = v_location_id );

        IF ( v_location_id NOT LIKE p_client_id || 'TROL%' AND v_location_id not like '%-%' ) OR v_valid_check
        = 0 THEN
            RAISE exception_invalid_reference;
        END IF;

        --Count The Number Of Move Tasks For ALL orders in sortation location which are not from the Sortation Location

        SELECT
            COUNT(*)
        INTO v_count
        FROM
            dcsdba.move_task
        WHERE
            task_id IN (
                SELECT
                    task_id
                FROM
                    dcsdba.move_task mt
                WHERE
                    client_id = p_client_id
                    AND ( from_loc_id LIKE v_location_id || '-__'
                          OR from_loc_id = v_location_id )
            )
            AND from_loc_id NOT LIKE v_location_id || '-__'
            AND from_loc_id != v_location_id
            AND site_id = p_site_id
            AND client_id = p_client_id;

        --If Any Move Tasks Are Found Which Are Not From The Sortation Location then the order is not fully sorted, so raise an exception

        IF v_count > 0 THEN
            RAISE exception_not_in_sort;
        END IF;

        --Set Temporary Buffer Data
        v_temp := rdtbuffer.setbufferdata(NULL, p_station_id, p_station_id, 'FROM_SORT', v_location_id);
        RETURN 1;
    EXCEPTION
        WHEN exception_invalid_reference THEN
                --dcsdba.liberror.writeerrorlog('IS_SORTATION_OUT', sqlcode, 'Order Not Ready To Pack - Please Sort And Try Again');
            RETURN 0;
        WHEN exception_not_in_sort THEN
                --dcsdba.liberror.writeerrorlog('IS_SORTATION_OUT', sqlcode, 'Move Tasks Found Outside Sortation Location');
            RETURN 0;
        WHEN OTHERS THEN
            dcsdba.liberror.writeerrorlog('TQ_BULK_PICK.SORT_VALIDATION', sqlcode, sqlerrm);
    END sort_validation;

/***********************************************************************************************************************/ 

/*                                                                                                                     */ 

/*  NAME:               TQ_BULK_PICK.PROCESS_STOCK_OUT                                                                 */ 

/*                                                                                                                     */ 

/*  DESCRIPTION:        Process The Order Tasks Which Are Have Been Fully Sorted                                       */ 

/*                                                                                                                     */ 

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */ 

/*  =======     ===========     ==     ===================================================================             */ 

/*    1         2019-07-17      KS     Initial Version                                                                 */ 

/*    2         2019-09-27      KS     Reworked To Accept A Full Trolley As Location                                   */ 

/*    3         2019-11-11      KS     Modified PROCESS_STOCK_OUT to ensure TO_LOCATION IS CORRECT                     */

/*    4         2020-02-05      KS     Modified To Remove the need for Client Parameters                          */

/*                                                                                                                     */  

/***********************************************************************************************************************/  

/**********************GRANTS**************************

grant update on move_task to TORQUE;

grant execute on liberror to TORQUE;

******************************************************/

    FUNCTION process_stock_out (
        p_station_id   VARCHAR2,
        p_user_id      VARCHAR2
    ) RETURN NUMBER AS
        PRAGMA autonomous_transaction;
        v_location_id   VARCHAR2(50);
        v_temp          NUMBER;
    BEGIN
        --Read the Location + Order Values Stored In The Buffer For This Sortation Process
        v_location_id := rdtbuffer.readbufferdata(NULL, p_station_id, p_station_id, 'FROM_SORT');

        --Stamp the User + Station On To All Move Tasks For The Order Coming From The Sort Location then set all of these to Complete
        UPDATE dcsdba.move_task
        SET
            status = 'Complete',
            to_loc_id = final_loc_id,
            user_id = p_user_id,
            station_id = p_station_id
        WHERE
            ( from_loc_id = v_location_id
              OR from_loc_id LIKE v_location_id || '-__' )
            AND (client_id = substr(v_location_id, 1, length(v_location_id)-7)
            OR client_id = substr(v_location_id, 1, length(v_location_id)-10))
            AND task_type = 'O'
            AND status IN (
                'Hold',
                'Released'
            );

        COMMIT;

        --Clear All Stored Temporary Buffer Data
        v_temp := rdtbuffer.deletebufferdata(NULL, NULL, p_station_id, 'FROM_SORT');
        RETURN 1;
    EXCEPTION
        WHEN OTHERS THEN
            dcsdba.liberror.writeerrorlog('TQ_BULK_PICK.PROCESS_STOCK_OUT', sqlcode, sqlerrm);
            RETURN 0;
    END process_stock_out;

/***********************************************************************************************************************/ 

/*                                                                                                                     */ 

/*  NAME:               TQ_BULK_PICK.PROCESS_STOCK_DIRECT                                                              */ 

/*                                                                                                                     */ 

/*  DESCRIPTION:        Process The Order Tasks Directly - For Use With An External API                                */ 

/*                                                                                                                     */ 

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */ 

/*  =======     ===========     ==     ===================================================================             */ 

/*    1         2019-07-22      KS     Initial Version                                                                 */ 

/*    2         2019-11-11      KS     Modified to ensure TO_LOCATION IS CORRECT                                       */

/*                                                                                                                     */  

/***********************************************************************************************************************/  

/**********************GRANTS**************************

grant select on move_task to TORQUE;

grant update on move_task to TORQUE;

grant execute on liberror to TORQUE;

******************************************************/

    FUNCTION process_stock_direct (
        p_site_id     VARCHAR2,
        p_client_id   VARCHAR2,
        p_order_id    VARCHAR2
    ) RETURN NUMBER AS
        PRAGMA autonomous_transaction;
        v_loc_check NUMBER;
        exception_location EXCEPTION;
    BEGIN

        --Count Locations Order Exists In
        SELECT
            COUNT(DISTINCT from_loc_id)
        INTO v_loc_check
        FROM
            dcsdba.move_task
        WHERE
            site_id = p_site_id
            AND client_id = p_client_id
            AND task_id = p_order_id;

        IF v_loc_check != 1 THEN
            RAISE exception_location;
        END IF;

        --Complete the Move Tasks For Specified Order
        UPDATE dcsdba.move_task
        SET
            status = 'Complete',
            user_id = 'TORQUE_API',
            to_loc_id = final_loc_id,
            station_id = 'TORQUE_API'
        WHERE
            task_id = p_order_id
            AND from_loc_id LIKE p_client_id || 'TROL%'
            AND client_id = p_client_id
            AND site_id = p_site_id
            AND task_type = 'O'
            AND status IN (
                'Hold',
                'Released'
            );

        COMMIT;
        RETURN 1;
    EXCEPTION
        WHEN exception_location THEN
            --Order Exists In Multiple Locations Or Does Not Exist
            RETURN 0;
        WHEN OTHERS THEN
            dcsdba.liberror.writeerrorlog('TQ_BULK_PICK.PROCESS_STOCK_OUT', sqlcode, sqlerrm);
            RETURN 0;
    END process_stock_direct;

/***********************************************************************************************************************/ 

/*                                                                                                                     */ 

/*  NAME:               TQ_BULK_PICK.SET_BULK_PICK                                                                     */ 

/*                                                                                                                     */ 

/*  DESCRIPTION:        Modifies Order Header and Move Task Data To Use Bulk Picking Functionality                     */

/*                                                                                                                     */ 

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */ 

/*  =======     ===========     ==     ===================================================================             */ 

/*    1         2019-10-24      KS     Initial Version                                                                 */ 

/*    2         2020-02-26      KS     Added Parameter for Stage Route ID                                              */ 

/*                                                                                                                     */  

/***********************************************************************************************************************/  

/**********************GRANTS**************************

grant select on move_task to TORQUE;

grant update on order_header to TORQUE;
grant update on move_task to TORQUE;

grant execute on liberror to TORQUE;

******************************************************/

    FUNCTION set_bulk_pick (
        p_client_id        VARCHAR2,
        p_consignment      VARCHAR2,
        p_ship_location    VARCHAR2,
        p_stage_route_id   VARCHAR2 DEFAULT 'BULK-PICK'
    ) RETURN NUMBER AS
        PRAGMA autonomous_transaction;
    BEGIN
        FOR x IN (
            SELECT
                key
            FROM
                dcsdba.move_task
            WHERE
                client_id = upper(p_client_id)
                AND consignment = upper(p_consignment)
                AND task_type = 'O'
                AND status IN (
                    'Released',
                    'Hold'
                )
                AND to_loc_id = upper(p_ship_location)
        ) LOOP UPDATE dcsdba.move_task
        SET
            to_loc_id = p_client_id || 'BULK'
        WHERE
            key = x.key;

        END LOOP;

        UPDATE dcsdba.order_header
        SET
            stage_route_id = upper(p_stage_route_id)
        WHERE
            client_id = upper(p_client_id)
            AND consignment = upper(p_consignment)
            AND stage_route_id IS NULL
            AND ship_dock = upper(p_ship_location);

        COMMIT;
        RETURN 1;
    EXCEPTION
        WHEN OTHERS THEN
            ROLLBACK;
            dcsdba.liberror.writeerrorlog('TQ_BULK_PICK.SET_BULK_PICK', sqlcode, sqlerrm);
            RETURN 0;
    END set_bulk_pick;

END tq_bulk_pick;

/

  GRANT EXECUTE ON "TORQUE"."TQ_BULK_PICK" TO "DCSDBA";
--------------------------------------------------------
--  DDL for Package Body TQ_SORTATION_WALL
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "TORQUE"."TQ_SORTATION_WALL" AS

/***********************************************************************************************************************/ 

/*                                                                                                                     */ 

/*  NAME:               TQ_SORTATION_WALL.SEARCH_LOCATION                                                              */ 

/*                                                                                                                     */ 

/*  DESCRIPTION:        To Identify An Order Task For A SKU/Location and Sort It                                       */ 

/*                                                                                                                     */ 

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */ 

/*  =======     ===========     ==     ===================================================================             */ 

/*    1         2019-07-12      KS     Initial Version                                                                 */ 

/*    2			2019-07-22		KS	   Modified To Support 2 Stage SORT	                                               */

/*    3			2019-09-27		KS	   Modified To Choose Order ID by lowest string                                    */

/*    4			2019-10-01		KS	   Added Support Mode Parameter To Swap Between 1 And 2 Stage Sort                 */

/*                                                                                                                     */  

/***********************************************************************************************************************/  

/**********************GRANTS**************************

grant select on move_task to TORQUE;
grant select on location to TORQUE;

grant update on move_task to TORQUE;

grant execute on liberror to TORQUE;

******************************************************/

    FUNCTION search_location (
        p_site_id       IN   VARCHAR2,
        p_client_id     IN   VARCHAR2,
        p_user_id       IN   VARCHAR2,
        p_station_id    IN   VARCHAR2,
        p_from_loc_id   IN   VARCHAR2,
        p_sku_id        IN   VARCHAR2,
        p_sort_mode     IN   NUMBER DEFAULT 1
    ) RETURN VARCHAR2 AS

        PRAGMA autonomous_transaction;
        v_location_id   VARCHAR2(40);
        v_key           NUMBER;
        v_sort_type     VARCHAR(50);
        v_temp          NUMBER;
    BEGIN 

		--Clear Existing Buffer For Station
        v_temp := rdtbuffer.deletebufferdata(NULL, NULL, p_station_id, 'SORT_LOCATION');
        v_temp := rdtbuffer.deletebufferdata(NULL, NULL, p_station_id, 'SORT_KEY');
        v_temp := rdtbuffer.deletebufferdata(NULL, NULL, p_station_id, 'SORT_SKU');

        --Used to identify whether sorting from Bulk to Sort OR Sort to Trolley
        IF p_from_loc_id LIKE '%BULK%' THEN
            v_sort_type := 'SORT';
        ELSIF p_from_loc_id LIKE '%SORT%' THEN
            v_sort_type := 'TROL';
        END IF;

		--Identify Order For Stock And Search For Existing Sortation Wall Location For That Order -- Also Store The Move Task Key For The Identified Order Task

        BEGIN
            WITH q AS (
                SELECT
                    *
                FROM
                    (
                        SELECT
                            mt.key,
                            mt.site_id,
                            mt.client_id,
                            mt.task_id,
                            mt.tag_id,
                            mt.from_loc_id,
                            mt.to_loc_id,
                            mt.sku_id,
                            mt.description,
                            mt.qty_to_move,
                            ROW_NUMBER() OVER(
                                ORDER BY
                                    task_id
                            ) AS sequ
                        FROM
                            dcsdba.move_task mt
                        WHERE
                            mt.from_loc_id = p_from_loc_id
                            AND mt.site_id = p_site_id
                            AND mt.client_id = p_client_id
                            AND mt.sku_id = p_sku_id
                            AND mt.task_type = 'O'
                            AND mt.status IN (
                                'Hold',
                                'Released'
                            )
                    )
                WHERE
                    sequ = 1
            )
            SELECT
                nvl(mt.from_loc_id, mt2.to_loc_id) AS location_id,
                q.key
            INTO
                v_location_id,
                v_key
            FROM
                q                  left
                JOIN dcsdba.move_task   mt ON ( q.task_id = mt.task_id
                                              AND q.site_id = mt.site_id
                                              AND q.client_id = mt.client_id
                                                 --AND mt.from_loc_id LIKE p_client_id || 'SORT%' 
                                              AND ( ( ( p_sort_mode = 1
                                                        AND ( ( mt.from_loc_id LIKE p_client_id
                                                                                    || 'SORT'
                                                                                    || substr(p_from_loc_id, - 2)
                                                                                    || '_'
                                                                AND v_sort_type = 'SORT' )
                                                              OR ( mt.from_loc_id LIKE p_client_id
                                                                                       || 'TROL'
                                                                                       || substr(p_from_loc_id, - 3)
                                                                                       || '-__'
                                                                   AND v_sort_type = 'TROL' ) ) ) )
                                                    OR ( p_sort_mode = 2
                                                         AND mt.from_loc_id LIKE p_client_id
                                                                                 || 'TROL'
                                                                                 || substr(p_from_loc_id, - 2)
                                                                                 || '_-__' ) ) )
                LEFT JOIN dcsdba.move_task   mt2 ON ( q.task_id = mt2.task_id
                                                    AND q.site_id = mt2.site_id
                                                    AND q.client_id = mt2.client_id
                                                    --AND mt2.to_loc_id LIKE p_client_id || 'SORT%'
                                                    --AND mt2.status = 'Complete'
                                                    --AND mt2.to_loc_id != p_client_id || 'SORTXXX' 
                                                    AND ( ( ( p_sort_mode = 1
                                                              AND ( ( mt2.to_loc_id LIKE p_client_id
                                                                                         || 'SORT'
                                                                                         || substr(p_from_loc_id, - 2)
                                                                                         || '_'
                                                                      AND v_sort_type = 'SORT' )
                                                                    OR ( mt2.to_loc_id LIKE p_client_id
                                                                                            || 'TROL'
                                                                                            || substr(p_from_loc_id, - 3)
                                                                                            || '-__'
                                                                         AND v_sort_type = 'TROL' ) ) ) )
                                                          OR ( p_sort_mode = 2
                                                               AND mt2.to_loc_id LIKE p_client_id
                                                                                      || 'TROL'
                                                                                      || substr(p_from_loc_id, - 2)
                                                                                      || '_-__' ) ) )
            WHERE
                ROWNUM = 1;

        EXCEPTION
            WHEN no_data_found THEN
                NULL;
        END;

		--IF No Existing Location can be found then identify the next sortation Location within the sortation wall following the putaway sequence

        IF v_location_id IS NULL THEN
            BEGIN
                SELECT
                    location_id
                INTO v_location_id
                FROM
                    (
                        SELECT
                            location_id,
                            ROW_NUMBER() OVER(
                                ORDER BY
                                    put_sequence
                            ) AS sequ
                        FROM
                            (
                                SELECT
                                    l.location_id,
                                    l.user_def_num_2 AS max_orders,
                                    l.put_sequence,
                                    COUNT(DISTINCT mt.task_id) AS tasks,
                                    COUNT(DISTINCT mt2.task_id) AS tasks_to
                                FROM
                                    dcsdba.location    l
                                    LEFT JOIN dcsdba.move_task   mt ON ( l.location_id = mt.from_loc_id
                                                                       AND l.site_id = mt.site_id )
                                    LEFT JOIN dcsdba.move_task   mt2 ON ( l.location_id = mt2.to_loc_id
                                                                        AND l.site_id = mt2.site_id )
                                    --inner join dcsdba.order_header oh ON (mt.task_id = oh.order_id AND mt.client_id = oh.client_id)
                                WHERE
                                    l.site_id = p_site_id
									--AND l.location_id LIKE p_client_id || 'SORT%'
									--AND l.location_id != p_client_id || 'SORTXXX'
                                    AND l.lock_status = 'UnLocked'
                                    AND ( ( p_sort_mode = 1
                                            AND ( ( l.location_id LIKE p_client_id
                                                                       || 'SORT'
                                                                       || substr(p_from_loc_id, - 2)
                                                                       || '_'
                                                    AND v_sort_type = 'SORT' )
                                                  OR ( l.location_id LIKE p_client_id
                                                                          || 'TROL'
                                                                          || substr(p_from_loc_id, - 3)
                                                                          || '-__'
                                                       AND v_sort_type = 'TROL' ) ) )
                                          OR ( p_sort_mode = 2
                                               AND l.location_id LIKE p_client_id
                                                                      || 'TROL'
                                                                      || substr(p_from_loc_id, - 2)
                                                                      || '_-__' ) 
                                          )
                                GROUP BY
                                    l.location_id,
                                    l.user_def_num_2,
                                    l.put_sequence
                            )
                        WHERE
                            ( tasks + tasks_to ) < nvl(max_orders, 1)
                    )
                WHERE
                    sequ = 1;

            EXCEPTION
                WHEN no_data_found THEN
                    NULL;
            END;
        END IF;

		--IF a location has been found above then

        IF v_location_id IS NOT NULL THEN

		--Update Move Task with the Found Sortation Location
            UPDATE dcsdba.move_task
            SET
                to_loc_id = v_location_id
            WHERE
                key = v_key;

		--Store the Move Task Key Found in V_RDT_BUFFER

            v_temp := torque.rdtbuffer.setbufferdata(NULL, p_station_id, p_station_id, 'SORT_KEY', v_key);

            COMMIT;
        END IF;

        RETURN v_location_id;
    EXCEPTION
        WHEN OTHERS THEN
            dcsdba.liberror.writeerrorlog('TQ_SORTATION_WALL.SEARCH_LOCATION', sqlcode, sqlerrm);
    END search_location;

/***********************************************************************************************************************/ 

/*                                                                                                                     */ 

/*  NAME:               TQ_SORTATION_WALL.PROCESS_STOCK                                                                */ 

/*                                                                                                                     */ 

/*  DESCRIPTION:        Process Order Tasks Which Has Been Sorted                                                      */ 

/*                                                                                                                     */ 

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */ 

/*  =======     ===========     ==     ===================================================================             */ 

/*    1         2019-07-15      KS     Initial Version                                                                 */ 

/*    2         2019-10-08      KS     Added Logic to store Sorted Location Into Order Header HUB_COUNTY               */ 

/*    3         2020-02-05      KS     Modified To Remove the need for Site/Client Parameters                          */

/*                                                                                                                     */  

/***********************************************************************************************************************/  

/**********************GRANTS**************************

grant select on move_task to TORQUE;

grant update on move_task to TORQUE;
grant update on order_header to TORQUE;

grant insert on move_task to TORQUE;

grant execute on liberror to TORQUE;

******************************************************/

    FUNCTION process_stock (
        p_site_id      IN   VARCHAR2, --Now Redundant
        p_client_id    IN   VARCHAR2, --Now Redundant
        p_user_id      IN   VARCHAR2,
        p_station_id   IN   VARCHAR2
    ) RETURN NUMBER AS

        PRAGMA autonomous_transaction;
        v_key             NUMBER;
        v_client_id       VARCHAR2(40);
        v_order_id        VARCHAR2(40);
        v_sort_location   VARCHAR2(40);
        v_qty_to_move     NUMBER;
        v_new_key         NUMBER;
        v_temp            NUMBER;
    BEGIN
        --Read The Move Task Key Captured During the SEARCH_LOCATION Function
        v_key := torque.rdtbuffer.readbufferdata(NULL, p_station_id, p_station_id, 'SORT_KEY');
        BEGIN

            --Find the QTY_TO_MOVE for the Move Task Identified
            SELECT
                client_id,
                task_id,
                qty_to_move,
                to_loc_id
            INTO
                v_client_id,
                v_order_id,
                v_qty_to_move,
                v_sort_location
            FROM
                dcsdba.move_task
            WHERE
                key = v_key;

        EXCEPTION
            WHEN OTHERS THEN
                ROLLBACK;
                RETURN 0;
        END;

        --If QTY_TO_MOVE is 1 then stamp User + Station to the Move Task and set it to Complete

        IF v_qty_to_move = 1 THEN
            UPDATE dcsdba.move_task
            SET
                status = 'Complete',
                user_id = p_user_id,
                station_id = p_station_id
            WHERE
                key = v_key;

            COMMIT;
        -- If QTY_TO_MOVE is Greater than 1 then Insert a New Move Task with the same Details as the original but with QTY_TO_MOVE + OLD_QTY_TO_MOVE Being 1 Less
        -- Then set the QTY_TO_MOVE + OLD_QTY_TO_MOVE to 1 on the original Move task, stamp it with the User + Station + set the Reason Code To EX_SP and set it to Complete.
        ELSIF v_qty_to_move > 1 THEN
            FOR x IN (
                SELECT
                    *
                FROM
                    dcsdba.move_task
                WHERE
                    key = v_key
            ) LOOP
                v_new_key := dcsdba.move_task_pk_seq.nextval;
                INSERT INTO dcsdba.move_task (
                    key,
                    first_key,
                    task_id,
                    task_type,
                    line_id,
                    client_id,
                    sku_id,
                    config_id,
                    description,
                    tag_id,
                    old_tag_id,
                    customer_id,
                    origin_id,
                    condition_id,
                    qty_to_move,
                    old_qty_to_move,
                    site_id,
                    from_loc_id,
                    old_from_loc_id,
                    to_loc_id,
                    old_to_loc_id,
                    owner_id,
                    sequence,
                    status,
                    list_id,
                    dstamp,
                    start_dstamp,
                    finish_dstamp,
                    original_dstamp,
                    priority,
                    work_zone,
                    consignment,
                    bol_id,
                    session_type,
                    summary_record,
                    repack,
                    kit_ratio,
                    ce_under_bond,
                    uploaded_labor,
                    print_label_id,
                    print_label,
                    repack_qc_done,
                    catch_weight,
                    moved_lock_status,
                    stage_route_sequence,
                    labelling,
                    first_pick,
                    plan_sequence,
                    ce_rotation_id,
                    move_whole,
                    shipping_unit,
                    final_loc_id
                ) VALUES (
                    v_new_key,
                    v_new_key,
                    x.task_id,
                    x.task_type,
                    x.line_id,
                    x.client_id,
                    x.sku_id,
                    x.config_id,
                    x.description,
                    x.tag_id,
                    x.old_tag_id,
                    x.customer_id,
                    x.origin_id,
                    x.condition_id,
                    x.qty_to_move - 1,
                    x.old_qty_to_move - 1,
                    x.site_id,
                    x.from_loc_id,
                    x.old_from_loc_id,
                    x.client_id || 'SORTXXX',
                    x.client_id || 'SORTXXX',
                    x.owner_id,
                    x.sequence,
                    x.status,
                    x.list_id,
                    x.dstamp,
                    x.start_dstamp,
                    x.finish_dstamp,
                    x.original_dstamp,
                    x.priority,
                    x.work_zone,
                    x.consignment,
                    x.bol_id,
                    x.session_type,
                    x.summary_record,
                    x.repack,
                    x.kit_ratio,
                    x.ce_under_bond,
                    x.uploaded_labor,
                    x.print_label_id,
                    x.print_label,
                    x.repack_qc_done,
                    x.catch_weight,
                    x.moved_lock_status,
                    x.stage_route_sequence,
                    x.labelling,
                    x.first_pick,
                    x.plan_sequence,
                    x.ce_rotation_id,
                    x.move_whole,
                    x.shipping_unit,
                    x.final_loc_id
                );

            END LOOP;

            --As Above Set QTY_TO_MOVE + OLD_QTY_TO_MOVE to 1, set the Reason Code To EX_SP, Stamp the User + Station and set the status to Complete

            UPDATE dcsdba.move_task
            SET
                status = 'Complete',
                user_id = p_user_id,
                station_id = p_station_id,
                qty_to_move = 1,
                old_qty_to_move = 1,
                reason_code = 'EX_SP'
            WHERE
                key = v_key;

            COMMIT;
        END IF;

        --UPDATE ORDER_HEADER WITH SORTATION SLOT

        UPDATE dcsdba.order_header
        SET
            hub_county = v_sort_location
        WHERE
            client_id = v_client_id
            AND order_id = v_order_id;

        COMMIT;

        --Clear All Stored Temporary Buffer Data
        v_temp := rdtbuffer.deletebufferdata(NULL, NULL, p_station_id, 'SORT_LOCATION');
        v_temp := rdtbuffer.deletebufferdata(NULL, NULL, p_station_id, 'SORT_KEY');
        v_temp := rdtbuffer.deletebufferdata(NULL, NULL, p_station_id, 'SORT_SKU');
        RETURN 1;
    EXCEPTION
        WHEN OTHERS THEN
            dcsdba.liberror.writeerrorlog('TQ_SORTATION_WALL.PROCESS_STOCK', sqlcode, sqlerrm);
    END process_stock;

/***********************************************************************************************************************/ 

/*                                                                                                                     */ 

/*  NAME:               TQ_SORTATION_WALL.SORT_VALIDATION                                                              */ 

/*                                                                                                                     */ 

/*  DESCRIPTION:        Identifies Order/Sort Location and Whether the Order Is Fully Sorted                           */ 

/*                                                                                                                     */ 

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */ 

/*  =======     ===========     ==     ===================================================================             */ 

/*    1         2019-07-16      KS     Initial Version                                                                 */ 

/*    2         2019-07-27      KS     Reworked To Allow Validation Of Full Trolley                                    */ 

/*                                                                                                                     */  

/***********************************************************************************************************************/  

/**********************GRANTS**************************

grant select on move_task to TORQUE;

grant execute on liberror to TORQUE;

******************************************************/

    FUNCTION sort_validation (
        p_site_id       VARCHAR2,
        p_client_id     VARCHAR2,
        p_station_id    VARCHAR2,
        p_user_id       VARCHAR2,
        p_location_id   VARCHAR2
    ) RETURN NUMBER AS

        PRAGMA autonomous_transaction;
        exception_invalid_reference EXCEPTION;
        exception_not_in_sort EXCEPTION;
        v_location_id   VARCHAR2(30);
        v_count         NUMBER;
        v_valid_check   NUMBER := 0;
        v_temp          NUMBER;
    BEGIN

        --Clear Existing Buffer For Station
        v_temp := rdtbuffer.deletebufferdata(NULL, NULL, p_station_id, 'FROM_SORT');

        --Prep the Location Variables with the Location/Order Parameters in Upper Case
        v_location_id := upper(p_location_id);

        --IF The Location ID is not Valid the Raise Exception
        SELECT
            COUNT(*)
        INTO v_valid_check
        FROM
            dcsdba.move_task mt
        WHERE
            client_id = p_client_id
            AND ( from_loc_id LIKE v_location_id || '-__'
                  OR from_loc_id = v_location_id );

        IF ( v_location_id NOT LIKE p_client_id || 'SORT___' AND v_location_id NOT LIKE p_client_id || 'TROL___' ) OR v_valid_check
        = 0 THEN
            RAISE exception_invalid_reference;
        END IF;
        
        --Count The Number Of Move Tasks For ALL orders in sortation location which are not from the Sortation Location

        SELECT
            COUNT(*)
        INTO v_count
        FROM
            dcsdba.move_task
        WHERE
            task_id IN (
                SELECT
                    task_id
                FROM
                    dcsdba.move_task mt
                WHERE
                    client_id = p_client_id
                    AND ( from_loc_id LIKE v_location_id || '-__'
                          OR from_loc_id = v_location_id )
            )
            AND from_loc_id NOT LIKE v_location_id || '-__'
            AND from_loc_id != v_location_id
            AND site_id = p_site_id
            AND client_id = p_client_id;

        --If Any Move Tasks Are Found Which Are Not From The Sortation Location then the order is not fully sorted, so raise an exception

        IF v_count > 0 THEN
            RAISE exception_not_in_sort;
        END IF;

        --Set Temporary Buffer Data
        v_temp := rdtbuffer.setbufferdata(NULL, p_station_id, p_station_id, 'FROM_SORT', v_location_id);
        RETURN 1;
    EXCEPTION
        WHEN exception_invalid_reference THEN
                --dcsdba.liberror.writeerrorlog('IS_SORTATION_OUT', sqlcode, 'Order Not Ready To Pack - Please Sort And Try Again');
            RETURN 0;
        WHEN exception_not_in_sort THEN
                --dcsdba.liberror.writeerrorlog('IS_SORTATION_OUT', sqlcode, 'Move Tasks Found Outside Sortation Location');
            RETURN 0;
        WHEN OTHERS THEN
            dcsdba.liberror.writeerrorlog('TQ_SORTATION_WALL.SORT_VALIDATION', sqlcode, sqlerrm);
    END sort_validation;

/***********************************************************************************************************************/ 

/*                                                                                                                     */ 

/*  NAME:               TQ_SORTATION_WALL.PROCESS_STOCK_OUT                                                            */ 

/*                                                                                                                     */ 

/*  DESCRIPTION:        Process The Order Tasks Which Are Have Been Fully Sorted                                       */ 

/*                                                                                                                     */ 

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */ 

/*  =======     ===========     ==     ===================================================================             */ 

/*    1         2019-07-17      KS     Initial Version                                                                 */ 

/*    2         2019-09-27      KS     Reworked To Accept A Full Trolley As Location                                   */ 

/*    3         2019-11-11      KS     Modified PROCESS_STOCK_OUT to ensure TO_LOCATION IS CORRECT                     */

/*    4         2020-02-05      KS     Modified To Remove the need for Site/Client Parameters                          */

/*                                                                                                                     */  

/***********************************************************************************************************************/  

/**********************GRANTS**************************

grant update on move_task to TORQUE;

grant execute on liberror to TORQUE;

******************************************************/

    FUNCTION process_stock_out (
        p_site_id      VARCHAR2, --Now Redundant
        p_client_id    VARCHAR2, --Now Redundant
        p_station_id   VARCHAR2,
        p_user_id      VARCHAR2
    ) RETURN NUMBER AS
        PRAGMA autonomous_transaction;
        v_location_id   VARCHAR2(50);
        v_temp          NUMBER;
    BEGIN
        --Read the Location + Order Values Stored In The Buffer For This Sortation Process
        v_location_id := rdtbuffer.readbufferdata(NULL, p_station_id, p_station_id, 'FROM_SORT');

        --Stamp the User + Station On To All Move Tasks For The Order Coming From The Sort Location then set all of these to Complete
        UPDATE dcsdba.move_task
        SET
            status = 'Complete',
            to_loc_id = final_loc_id,
            user_id = p_user_id,
            station_id = p_station_id
        WHERE
            ( from_loc_id = v_location_id
              OR from_loc_id LIKE v_location_id || '-__' )
            AND client_id = substr(v_location_id,1,2)
            AND task_type = 'O'
            AND status IN (
                'Hold',
                'Released'
            );

        COMMIT;

        --Clear All Stored Temporary Buffer Data
        v_temp := rdtbuffer.deletebufferdata(NULL, NULL, p_station_id, 'FROM_SORT');
        RETURN 1;
    EXCEPTION
        WHEN OTHERS THEN
            dcsdba.liberror.writeerrorlog('TQ_SORTATION_WALL.PROCESS_STOCK_OUT', sqlcode, sqlerrm);
            RETURN 0;
    END process_stock_out;

/***********************************************************************************************************************/ 

/*                                                                                                                     */ 

/*  NAME:               TQ_SORTATION_WALL.PROCESS_STOCK_DIRECT                                                         */ 

/*                                                                                                                     */ 

/*  DESCRIPTION:        Process The Order Tasks Directly - For Use With An External API                                */ 

/*                                                                                                                     */ 

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */ 

/*  =======     ===========     ==     ===================================================================             */ 

/*    1         2019-07-22      KS     Initial Version                                                                 */ 

/*    2         2019-11-11      KS     Modified to ensure TO_LOCATION IS CORRECT                     */

/*                                                                                                                     */  

/***********************************************************************************************************************/  

/**********************GRANTS**************************

grant select on move_task to TORQUE;

grant update on move_task to TORQUE;

grant execute on liberror to TORQUE;

******************************************************/

    FUNCTION process_stock_direct (
        p_site_id     VARCHAR2,
        p_client_id   VARCHAR2,
        p_order_id    VARCHAR2
    ) RETURN NUMBER AS
        PRAGMA autonomous_transaction;
        v_loc_check NUMBER;
        exception_location EXCEPTION;
    BEGIN

        --Count Locations Order Exists In
        SELECT
            COUNT(DISTINCT from_loc_id)
        INTO v_loc_check
        FROM
            dcsdba.move_task
        WHERE
            site_id = p_site_id
            AND client_id = p_client_id
            AND task_id = p_order_id;

        IF v_loc_check != 1 THEN
            RAISE exception_location;
        END IF;

        --Complete the Move Tasks For Specified Order
        UPDATE dcsdba.move_task
        SET
            status = 'Complete',
            user_id = 'TORQUE_API',
            to_loc_id = final_loc_id,
            station_id = 'TORQUE_API'
        WHERE
            task_id = p_order_id
            AND from_loc_id like p_client_id || 'TROL%'
            AND client_id = p_client_id
            AND site_id = p_site_id
            AND task_type = 'O'
            AND status IN (
                'Hold',
                'Released'
            );

        COMMIT;
        RETURN 1;
    EXCEPTION
        WHEN exception_location THEN
            --Order Exists In Multiple Locations Or Does Not Exist
            RETURN 0;
        WHEN OTHERS THEN
            dcsdba.liberror.writeerrorlog('TQ_SORTATION_WALL.PROCESS_STOCK_OUT', sqlcode, sqlerrm);
            RETURN 0;
    END process_stock_direct;

END tq_sortation_wall;

/

  GRANT EXECUTE ON "TORQUE"."TQ_SORTATION_WALL" TO "DCSDBA";
--------------------------------------------------------
--  DDL for Package Body TQ_REPORTS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "TORQUE"."TQ_REPORTS" AS
-- =============================================
-- PROCEDURES
-- =============================================
-- TQ_UPDATE_SKU_CE_COO
-- =============================================
-- Author:      Christian Haslam
-- Create date: 13/08/2020
-- Description: Updates a sku's country of origin
--
-- Parameters:
--      p_Client    - Client id. Not Nullable
--      p_Sku       - Sku that will be updated
--      p_COO       - ISO3 Country code that will be stored against the sku; If NIL is passed, this will act as null
--      p_Delimeter - Allows a different delimeter to split the string passed other than a comma
--
-- Returns:
--      op_Rows     - Stores number of rows affected in the output variable
--
-- Change History:
--      CH | 14/08/2020: Checks for ISO3 code in the COUNTRY table so only updates valid arguments
--
-- Grants:
--      grant update on dcsdba.SKU to TORQUE
--      grant select on dcsdba.COUNTRY to TORQUE
--
-- Dependencies:
--      TORQUE - (IT) - Update Country of Origin for Skus <WMS Report>
-- =============================================
procedure TQ_UPDATE_SKU_CE_COO (op_Rows out number, p_Client varchar2, p_Sku varchar2, p_COO varchar2, p_Delimeter char default ',')
is
    -- ensures passed values are all uppercase
        v_Client varchar2(10):= upper(p_Client);
        v_Sku varchar2(650):= upper(p_Sku); -- each sku is a max of 30 therefore limited to a maximum of 21 skus at full length (plus 20 delimeter characters)
        v_COO varchar2(3):= upper(p_COO);
    begin
    -- set country variable to null if 'NIL' is passed to the procedure
        if v_COO = 'NIL'
            then v_COO:= null;
        end if;

    -- perform the update
        update dcsdba.SKU
        set ce_coo = v_COO
        where client_id = v_Client
        and sku_id in
        (
            select regexp_substr(v_Sku, '[^'||p_Delimeter||']+', 1, level) from dual
            connect by regexp_substr(v_Sku, '[^'||p_Delimeter||']+', 1, level) is not null
        )
        and
        (
            v_COO in
            (
                select iso3_id from dcsdba.COUNTRY
            )
            or v_COO is null
        ) 
        and coalesce(ce_coo, '777') != coalesce(v_COO, '777'); -- only update changes

    -- capture rows affected in the output parameter
        op_Rows:= SQL%ROWCOUNT;

    COMMIT;

end TQ_UPDATE_SKU_CE_COO;
-- =============================================
-- TQ_BULK_CREATE_USERS
-- =============================================
-- Author:      Christian Haslam
-- Create date: 16/12/2020
-- Description: Copy an account to create new accounts in bulk
--
-- Parameters:
--      p_UserToCopy        - User that the new accounts will be copying
--      p_NewUsers          - Comma separated list of new users to create
--      p_NewName           - Name that should be associated with the accounts (will increment with each loop when multiple specified)
--      p_Password          - Password that will be set for the new users--
--      p_PassShouldChange  - Y | N - States whether the password should be static or not--      
--
-- Returns:
--      op_UsersCreated  - returns the amount of users created
--
-- Change History:
--
-- Grants:
--      grant select, insert on dcsdba.INTERFACE_APPLICATION_USER to TORQUE
--      grant select on dcsdba.IF_AU_PK_Seq.nextval to TORQUE
--
-- Dependencies:
--      TORQUE - (IT) - Add Users in Bulk <WMS Report>
-- =============================================
procedure TQ_BULK_CREATE_USERS(op_UsersCreated out number, p_UserToCopy varchar2, p_NewUsers varchar2, p_NewName varchar2 default null, p_Password varchar2, p_PassShouldChange char)
is
v_ClientVisGroup varchar2(10);
v_SiteGroup varchar2(10);
v_GroupId varchar2(10);
v_LoopCount number(2):= 0;
v_PassChange varchar2(10);
v_TimeZone varchar2(10);
v_NewName varchar2(30);
begin
    select group_id, site_group, client_vis_group, time_zone_name into v_GroupId, v_SiteGroup, v_ClientVisGroup, v_TimeZone
    from dcsdba.APPLICATION_USER
    where user_id = p_UserToCopy; 
    
    op_UsersCreated:= 0;

    for x in
    (
        select regexp_substr(p_NewUsers, '[^,]+', 1, level) users from dual
        connect by regexp_substr(p_NewUsers, '[^,]+', 1, level) is not null
    )
    loop
        if p_NewName is null
            then v_NewName:= x.users || ' - Created in Bulk';
        else
            v_NewName:= p_NewName;
        end if;
    
        insert into dcsdba.INTERFACE_APPLICATION_USER
        (
            key
            , merge_status
            , merge_action
            , group_id
            , site_group
            , client_vis_group
            , name
            , notes
            , password
            , user_id
            , password_days
            , disable_days
            , delete_days
            , expired
            , time_zone_name
        )
        values
        (
            dcsdba.IF_AU_PK_Seq.nextval
            , 'Pending'
            , 'A'
            , v_GroupId
            , v_SiteGroup
            , v_ClientVisGroup
            , v_NewName
            , v_NewName
            , p_Password
            , x.users
            , v_PassChange
            , 90
            , 180
            , p_PassShouldChange
            , v_TimeZone
        );
        
        v_LoopCount:= v_LoopCount + 1;
        op_UsersCreated:= op_UsersCreated + SQL%ROWCOUNT;
    end loop;
    
    commit;

end TQ_BULK_CREATE_USERS;
-- =============================================



-- =============================================
-- FUNCTIONS
-- =============================================







END TQ_REPORTS;

/

  GRANT EXECUTE ON "TORQUE"."TQ_REPORTS" TO "DCSDBA";
  GRANT EXECUTE ON "TORQUE"."TQ_REPORTS" TO "DCSRPT";
--------------------------------------------------------
--  DDL for Package Body TQ_FUNCTIONS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "TORQUE"."TQ_FUNCTIONS" AS

/***********************************************************************************************************************/ 

/*                                                                                                                     */ 

/*  NAME:               TQ_FUNCTIONS.TQ_SEQUENCE                                                                       */ 

/*                                                                                                                     */ 

/*  DESCRIPTION:        This Function Allows Easy Control of the Torque Schema Sequence Table                          */ 

/*                                                                                                                     */ 

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */ 

/*  =======     ===========     ==     ===================================================================             */ 

/*    1         2019-09-05      KS     Initial Version                                                                 */ 


/*                                                                                                                     */  

/***********************************************************************************************************************/

    FUNCTION tq_sequence (
        p_client_id    VARCHAR2,
        p_type         VARCHAR2,
        p_update       NUMBER DEFAULT 1,
        p_fullstring   NUMBER DEFAULT 1
    ) RETURN VARCHAR2 IS

        PRAGMA autonomous_transaction;
        v_return    VARCHAR2(20);
        v_value     NUMBER;
        v_prefix    VARCHAR(10);
        v_padding   NUMBER;
    BEGIN
        SELECT
            position,
            prefix,
            padding
        INTO
            v_value,
            v_prefix,
            v_padding
        FROM
            torque.file_sequences
        WHERE
            client_id = p_client_id
            AND type = p_type;

        UPDATE torque.file_sequences
        SET
            position = position + p_update
        WHERE
            client_id = p_client_id
            AND type = p_type;

        COMMIT;
        IF p_fullstring = 1 THEN
            v_return := v_prefix
                        || lpad(v_value, v_padding, '0');
        ELSE
            v_return := v_value;
        END IF;

        RETURN v_return;
    EXCEPTION
        WHEN OTHERS THEN
            ROLLBACK;
            RETURN '0';
    END tq_sequence;

/***********************************************************************************************************************/ 

/*                                                                                                                     */ 

/*  NAME:               TQ_FUNCTIONS.TRIGGER_SHELL_SCRIPT                                                              */ 

/*                                                                                                                     */ 

/*  DESCRIPTION:        This Function Can Call A Shell Script On The Server                                            */ 

/*                                                                                                                     */ 

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */ 

/*  =======     ===========     ==     ===================================================================             */ 

/*    1         2019-08-15      KS     Initial Version                                                                 */ 

/*                                                                                                                     */  

/***********************************************************************************************************************/

/**********************GRANTS**************************

grant select on run_task_pk_seq to TORQUE;

grant insert on run_task to TORQUE;

grant execute on liberror to TORQUE;

******************************************************/

    FUNCTION trigger_shell_script (
        p_script_path   VARCHAR2,
        p_parameters    VARCHAR2,
        p_name          VARCHAR2,
        p_user_id       VARCHAR2,
        p_station_id    VARCHAR2
    ) RETURN NUMBER AS
        PRAGMA autonomous_transaction;
        v_run_task_key NUMBER;
    BEGIN

--Example Usage
--select TORQUE.TQ_TRIGGER_SHELL_SCRIPT('$DCS_USERRPTDIR/print_consignment_delivery.sh','"GB" "BB" "BB-2019-08-09-3" "PRT057" "KSHACK"','UREPDELNOTEBYCONS','KYLE','STATION') from dual
--select TORQUE.TQ_TRIGGER_SHELL_SCRIPT('$DCS_USERRPTDIR/print_consignment_delivery.sh','"GB" "BB" "BB-2019-08-09-3" "PRT057" "KSHACK"','TESTKYLE1234','KYLE','STATION') from dual
        v_run_task_key := dcsdba.run_task_pk_seq.nextval;
        INSERT INTO dcsdba.run_task (
            key,
            site_id,
            station_id,
            user_id,
            status,
            command,
            pid,
            old_dstamp,
            dstamp,
            language,
            name,
            time_zone_name,
            nls_calendar,
            print_label,
            java_report,
            run_light,
            server_instance,
            priority,
            archive,
            archive_ignore_screen,
            archive_restrict_user,
            client_id,
            email_recipients,
            email_attachment,
            email_subject,
            email_message,
            master_key,
            use_db_time_zone
        ) VALUES (
            v_run_task_key,
            'SHELL',
            p_user_id,
            p_station_id,
            'Pending',
            p_script_path
            || ' '
            || p_parameters,
            - 1,
            systimestamp,
            systimestamp,
            'EN_GB',
            p_name,
            'GB',
            'Gregorian',
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            v_run_task_key,
            'N'
        );

        COMMIT;
        RETURN 1;
    EXCEPTION
        WHEN OTHERS THEN
            ROLLBACK;
            dcsdba.liberror.writeerrorlog('TQ_FUNCTONS.TRIGGER_SHELL_SCRIPT', sqlcode, sqlerrm);
            RETURN 0;
    END trigger_shell_script;

/***********************************************************************************************************************/ 

/*                                                                                                                     */ 

/*  NAME:               TQ_FUNCTIONS.REPACK_CONTAINER                                                                  */ 

/*                                                                                                                     */ 

/*  DESCRIPTION:        This Function Repacks Containers Onto New Pallet ID's                                          */ 

/*                                                                                                                     */ 

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */ 

/*  =======     ===========     ==     ===================================================================             */ 

/*    1         2019-11-22      KS     Initial Version                                                                 */ 

/*                                                                                                                     */  

/***********************************************************************************************************************/

/**********************GRANTS**************************

grant select on move_task to TORQUE;

grant update on move_task to TORQUE;
grant update on shipping_manifest to TORQUE;
grant update on inventory to TORQUE;

grant execute on liborderrepack to TORQUE;
grant execute on liberror to TORQUE;

******************************************************/

    FUNCTION repack_container (
        p_site_id        VARCHAR2,
        p_client_id      VARCHAR2,
        p_container_id   VARCHAR2,
        p_order_id       VARCHAR2,
        p_to_pallet_id   VARCHAR2,
        p_autoship       NUMBER
    ) RETURN NUMBER IS

        PRAGMA autonomous_transaction;
        v_result           NUMBER;
        v_pallet_id        VARCHAR2(50);
        v_from_loc_id      VARCHAR2(50);
        v_to_loc_id        VARCHAR2(50);
        v_final_loc_id     VARCHAR2(50);
        v_work_zone        VARCHAR2(50);
        v_work_group       VARCHAR2(50);
        v_container_id     VARCHAR2(50);
        result             VARCHAR2(50);
        v_pallet_cleanup   NUMBER;
        v_new_pallet_chk   NUMBER;
        v_key              NUMBER;
        invalidcontainer EXCEPTION;
    BEGIN
        IF p_container_id IS NULL THEN
            SELECT DISTINCT
                container_id
            INTO v_container_id
            FROM
                dcsdba.move_task
            WHERE
                client_id = p_client_id
                AND site_id = p_site_id
                AND task_id = p_order_id
                AND task_type = 'O';

        ELSE
            v_container_id := upper(p_container_id);
        END IF;

        IF v_container_id IS NULL THEN
            RAISE invalidcontainer;
        END IF;
        SELECT DISTINCT
            mt.pallet_id,
            from_loc_id,
            to_loc_id,
            final_loc_id,
            work_zone,
            work_group
        INTO
            v_pallet_id,
            v_from_loc_id,
            v_to_loc_id,
            v_final_loc_id,
            v_work_zone,
            v_work_group
        FROM
            dcsdba.move_task mt
        WHERE
            mt.site_id = upper(p_site_id)
            AND mt.client_id = upper(p_client_id)
            AND mt.container_id = v_container_id
            AND mt.task_type = 'O'
            AND status NOT IN (
                'Error',
                'Hold'
            );

        IF v_from_loc_id = 'CONTAINER' THEN
            IF p_order_id IS NOT NULL THEN
                UPDATE dcsdba.order_header
                SET
                    instructions = substr(instructions || ' - Processed Through Bagger But At Wrong State', 1, 180)
                WHERE
                    client_id = upper(p_client_id)
                    AND order_id = upper(p_order_id);

                COMMIT;
            END IF;

            RETURN 0;
        END IF;

        IF v_pallet_id IS NULL THEN
            RAISE invalidcontainer;
        END IF;
        SELECT
            COUNT(*)
        INTO v_new_pallet_chk
        FROM
            dcsdba.move_task
        WHERE
            client_id = upper(p_client_id)
            AND site_id = upper(p_site_id)
            AND pallet_id = upper(p_to_pallet_id)
            AND task_type = 'T';

        IF v_new_pallet_chk = 0 THEN
            v_key := dcsdba.move_task_pk_seq.nextval;
            INSERT INTO dcsdba.move_task (
                key,
                first_key,
                task_type,
                task_id,
                client_id,
                sku_id,
                description,
                qty_to_move,
                old_qty_to_move,
                site_id,
                from_loc_id,
                old_from_loc_id,
                to_loc_id,
                old_to_loc_id,
                final_loc_id,
                sequence,
                status,
                dstamp,
                start_dstamp,
                finish_dstamp,
                original_dstamp,
                priority,
                consol_link,
                work_zone,
                work_group,
                pallet_id,
                pallet_config,
                session_type,
                summary_record,
                repack,
                kit_ratio,
                ce_under_bond,
                uploaded_labor,
                print_label,
                catch_weight,
                stage_route_sequence,
                labelling,
                first_pick,
                plan_sequence,
                move_whole,
                shipping_unit
            ) VALUES (
                v_key,
                v_key,
                'T',
                'PALLET',
                upper(p_client_id),
                'PALLET',
                'Multi container pallet',
                '1',
                '1',
                upper(p_site_id),
                v_from_loc_id,
                v_from_loc_id,
                v_to_loc_id,
                v_to_loc_id,
                v_final_loc_id,
                1,
                'Released',
                sysdate,
                sysdate,
                sysdate,
                sysdate,
                50,
                dcsdba.consol_move_task_link_seq.nextval,
                v_work_zone,
                v_work_group,
                upper(p_to_pallet_id),
                'MD',
                'M',
                'Y',
                'N',
                1,
                'N',
                'N',
                'N',
                0,
                0,
                'N',
                'N',
                1,
                'N',
                'N'
            );

        END IF;

        dcsdba.liborderrepack.addcontainertopallet(p_result => v_result, p_clientid => upper(p_client_id), p_palletid => NULL, p_containerid
        => v_container_id, p_newpalletid => upper(p_to_pallet_id),
                              p_pallettype => 'MD', p_siteid => upper(p_site_id));

        UPDATE dcsdba.inventory
        SET
            pallet_id = upper(p_to_pallet_id)
        WHERE
            container_id = v_container_id
            AND client_id = upper(p_client_id)
            AND site_id = upper(site_id)
            AND pallet_id = upper(v_pallet_id);

        UPDATE dcsdba.shipping_manifest
        SET
            pallet_id = upper(p_to_pallet_id)
        WHERE
            container_id = v_container_id
            AND client_id = upper(p_client_id)
            AND site_id = upper(site_id)
            AND pallet_id = upper(v_pallet_id);

        SELECT
            COUNT(*)
        INTO v_pallet_cleanup
        FROM
            dcsdba.move_task
        WHERE
            client_id = upper(p_client_id)
            AND site_id = upper(p_site_id)
            AND pallet_id = v_pallet_id
            AND task_type = 'O';

        IF v_pallet_cleanup = 0 THEN
            DELETE FROM dcsdba.move_task v_pallet_cleanup
            WHERE
                client_id = upper(p_client_id)
                AND site_id = upper(p_site_id)
                AND pallet_id = v_pallet_id
                AND task_type = 'T';

        END IF;

        IF p_autoship = 1 AND upper(p_to_pallet_id) != upper(v_pallet_id) THEN
            UPDATE dcsdba.move_task
            SET
                status = 'Complete'
            WHERE
                task_type = 'T'
                AND pallet_id = upper(p_to_pallet_id)
                AND client_id = upper(p_client_id)
                AND site_id = upper(p_site_id)
                AND status = 'Released';

        END IF;

        COMMIT;
        RETURN v_result;
    EXCEPTION
        WHEN OTHERS THEN
            ROLLBACK;
            RETURN 0;
    END repack_container;

/***********************************************************************************************************************/ 

/*                                                                                                                     */ 

/*  NAME:               TQ_FUNCTIONS.REMOVE_LIST_DUPLICATES                                                            */ 

/*                                                                                                                     */ 

/*  DESCRIPTION:        This Function Removes Duplicates From ListAgg Generated Strings                                */ 

/*                                                                                                                     */ 

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */ 

/*  =======     ===========     ==     ===================================================================             */ 

/*    1         2020-01-29      KS     Initial Version                                                                 */ 

/*    2         2020-01-30      KS     Modified So Input Should Always Be Comma to Prevent Issue                       */

/*                                                                                                                     */  

/***********************************************************************************************************************/

/**********************GRANTS**************************

******************************************************/

    FUNCTION remove_list_duplicates (
        p_string     VARCHAR2,
        p_splitter   VARCHAR2
    ) RETURN VARCHAR2 IS
        v_return VARCHAR2(4000);
    BEGIN
        WITH data AS (
            SELECT DISTINCT
                TRIM(regexp_substr(str, '[^,]+', 1, level)) str
            FROM
                (
                    SELECT
                        p_string str
                    FROM
                        dual
                )
            CONNECT BY
                regexp_substr(str, '[^,]+', 1, level) IS NOT NULL
        )
        SELECT
            LISTAGG(str, p_splitter) WITHIN GROUP(
                ORDER BY
                    str
            )
        INTO v_return
        FROM
            data;

        RETURN v_return;
    EXCEPTION
        WHEN OTHERS THEN
            RETURN NULL;
    END remove_list_duplicates;
    
/***********************************************************************************************************************/ 

/*                                                                                                                     */ 

/*  NAME:               TQ_FUNCTIONS.TQ_BULK_RELOCATE                                                            */ 

/*                                                                                                                     */ 

/*  DESCRIPTION:        This Function is an enhancement to the original bin to bin relocation function                 */ 

/*                                                                                                                     */ 

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */ 

/*  =======     ===========     ==     ===================================================================             */ 

/*    1         2020-08-05      CH     Initial Version                                                                 */

/*                                                                                                                     */  

/***********************************************************************************************************************/

/**********************GRANTS**************************

grant select on dcsdba.INVENTORY to TORQUE
grant select on dcsdba.LOCATION to TORQUE
grant select on dcsdba.CLIENT to TORQUE

******************************************************/
    
function tq_bulk_relocate
(
    p_SiteId varchar2, p_ClientId varchar2, p_FromLoc varchar2, p_ToLoc varchar2, p_UserId varchar2, p_Workstation varchar2, p_EmptyOnly char default 'N', p_Sku varchar2 default '*'
) return varchar2
is
    -- re-assignments so just have to do upper once and not again for the rest of the function
    v_Client varchar2(10):= upper(p_ClientId);
    v_FromLoc varchar2(20):= upper(p_FromLoc);
    v_ToLoc varchar2(20):= upper(p_ToLoc);
    v_EmptyOnly char(1):= upper(p_EmptyOnly);
    v_Sku varchar2(30):= upper(p_Sku);

    -- variables to store values
    v_Return varchar2(150); -- used to return data to rdt screen
    v_ClientIdCheck varchar2(10);
    v_ToLocLocationType varchar2(15);
    v_ToLocLockStatus varchar2(15);
    v_ToLocQty number(5);
    v_FromLocLocationType varchar2(15);
    v_FromLocLockStatus varchar2(15);
    v_FromLocQty number(1);
    v_RelocTasks number(4):= 0;
    v_RelocUnits number(5):= 0;
    v_TQAPIReturnVal varchar2(20);

    -- cursor to loop through
    cursor cur_InventoryFromLocDets is
        select i.location_id, i.sku_id, i.tag_id, i.site_id, i.client_id, i.qty_on_hand, l.loc_type, l.lock_status, i.pallet_id
        from dcsdba.INVENTORY i
        inner join dcsdba.LOCATION l on l.location_id = i.location_id and l.site_id = i.site_id
        where i.client_id = v_Client
        and i.location_id = v_FromLoc
        and
        (
            i.sku_id = v_Sku
            or
            v_Sku = '*'
        );

    -- exception declarations
    ex_ClientInvalid exception;
    ex_ToLocNonExistent exception;
    ex_ToLocTypeInvalid exception;
    ex_ToLocLockStatus exception;
    ex_ToLocNotEmpty exception;
    ex_FromLocTypeInvalid exception;
    ex_FromLocLockStatus exception;
    ex_FromLocEmpty exception;
    ex_FromLocNonExistent exception;
    ex_FunctionError exception;

begin
    -- block to validate that client id exists and raise exception if invalid and stop execution of program
    begin
        select client_id into v_ClientIdCheck
        from dcsdba.CLIENT
        where client_id = v_Client;

        exception when NO_DATA_FOUND then raise ex_ClientInvalid;
    end;

    -- block to validate to_location
    begin
        -- only join to inventory table if to_location needs to be empty
        if v_EmptyOnly = 'Y' then
            select l.loc_type, l.lock_status, coalesce(sum(i.qty_on_hand), 0) into v_ToLocLocationType, v_ToLocLockStatus, v_ToLocQty
            from dcsdba.LOCATION l
            left join dcsdba.INVENTORY i on i.location_id = l.location_id and i.site_id = l.site_id
            where l.location_id = v_ToLoc
            and l.site_id = p_SiteId
            group by l.loc_type, l.lock_status;
        else
            select loc_type, lock_status into v_ToLocLocationType, v_ToLocLockStatus
            from dcsdba.LOCATION
            where location_id = v_ToLoc
            and site_id = p_SiteId;
        end if;

        -- exception handling regarding to_location
        if v_ToLocLocationType not in ('Receive Dock','Tag-FIFO','Tag-LIFO') then raise ex_ToLocTypeInvalid;
        end if;

        if v_ToLocLockStatus in ('Locked','InLocked') then raise ex_ToLocLockStatus;
        end if;

        if v_EmptyOnly = 'Y' and v_ToLocQty > 0 then raise ex_ToLocNotEmpty;
        end if;

        exception when NO_DATA_FOUND then raise ex_ToLocNonExistent;
    end;

    -- loop through inventory lines and use TQ API function to insert records
    begin
        for v_line in cur_InventoryFromLocDets
        loop
            v_FromLocLocationType:= v_line.loc_type;
            v_FromLocLockStatus:= v_line.lock_status;
            -- raise any from_loc exceptions and exit loop before making any inserts
            if v_FromLocLocationType not in ('Receive Dock','Tag-FIFO','Tag-LIFO') then raise ex_FromLocTypeInvalid;
            end if;

            if v_FromLocLockStatus in ('Locked','OutLocked') then raise ex_FromLocLockStatus;
            end if;

            -- insert the relocation tasks via the TQ API function
            select TQ_INTERFACE.interface_relocation_task
            (
                in_to_loc_id => v_ToLoc,
                in_tag_id => v_line.tag_id,
                in_sku_id => v_line.sku_id,
                in_pallet_id => v_line.pallet_id,
                in_from_loc_id => v_FromLoc,
                in_client_id => v_Client,
                in_site_id => p_SiteId,
                in_quantity => v_line.qty_on_hand,
                in_move_task_status => 'Complete',
                in_user_id => p_UserId,
                in_station_id => p_Workstation,
                in_move_if_allocated => 'Y',
                in_record_error_in_table => 'Y'
            ) into v_TQAPIReturnVal from dual;

            -- if function returns an error, raise exception and exit program
            if v_TQAPIReturnVal is not null then raise ex_FunctionError;
            end if;

            v_RelocUnits:= v_RelocUnits + v_line.qty_on_hand; -- sum up the units
            v_RelocTasks:= v_RelocTasks + 1; -- count lines affected
        end loop;

        -- raise exception to let user know if from location didnt exist or was empty
        if v_RelocTasks = 0 then 
            select count(*) into v_FromLocQty
            from dcsdba.LOCATION
            where location_id = v_FromLoc
            and site_id = p_SiteId;

            if v_FromLocQty = 0 then raise ex_FromLocNonExistent;
            else raise ex_FromLocEmpty;
            end if;            
        end if;
    end;

    -- return info to the screen
    v_Return:= rpad('Lines Moved: '||to_char(v_RelocTasks), 20, ' ')||rpad('Units Moved: '||to_char(v_RelocUnits), 20, ' ')||rpad('From Loc: '||v_FromLoc, 20, ' ')||rpad('To Loc:   '||v_ToLoc, 20, ' ');
    return v_Return;

    exception
        when ex_ClientInvalid then v_Return:= 'Invalid Client id:  '||v_Client; return v_Return;
        when ex_ToLocTypeInvalid then v_Return:= rpad('Invalid To Location Type:', 40, ' ')||v_ToLoc||' - '||v_ToLocLocationType; return v_Return;
        when ex_ToLocLockStatus then v_Return:= rpad('Invalid To Location Status:', 40, ' ')||v_ToLoc||' - '||v_ToLocLockStatus; return v_Return;
        when ex_ToLocNonExistent then v_Return:= 'To Location Does NotExist: '||v_ToLoc; return v_Return;
        when ex_ToLocNotEmpty then v_Return:= rpad('To Location Is Not  Empty: '||v_ToLoc, 40, ' ')||'Contact IT If This  Needs Changing'; return v_Return;
        when ex_FromLocTypeInvalid then v_Return:= rpad('Invalid From', 20, ' ')||rpad('Location Type:', 20, ' ')||v_FromLoc||' - '||v_FromLocLocationType; return v_Return;
        when ex_FromLocLockStatus then v_Return:= rpad('Invalid From', 20, ' ')||rpad('Location Status:', 20, ' ')||v_FromLoc||' - '||v_FromLocLockStatus; return v_Return;
        when ex_FromLocEmpty then v_Return:= 'Nothing To Relocate From: '||v_FromLoc; return v_Return;
        when ex_FromLocNonExistent then v_Return:= 'From Location Does  '||rpad('Not Exist:', 20, ' ')||v_FromLoc; return v_Return;
        when ex_FunctionError then v_Return:= 'Relocate Failed. AskADMIN To Check      Interface Errors forINV RELOCATION TASKS'; return v_Return;
        -- catch all exception
        when others then v_Return:= 'Unknown Error...    Please Report.'; return v_Return;
end TQ_BULK_RELOCATE;


-- =============================================
--  TQ_FUNCTIONS.TQ_PALLET_REPACK
-- =============================================
--  Author:      Christian Haslam
--  Create date: 11/08/2020
--  Description: Takes in an order id and attempts to switch over the pallets and container id to new destination pallet and order id respectively
--
--  Parameters:
--      p_Order     - tracking number that gets converted to Order
--      p_To        - destination pallet id
--      p_Client    - client id
--      p_User      - BUFFER_USER_ID
--      p_Station   - RDT Number
--
--  Returns:
--      v_Return    - Return info to the RDT
--
--  Change History:
--      24/08/2020: Added in a check to see if the order id was entered instead of the signatory
--      24/08/2020: If TNT/UK Mail is the TO_PALLET destination, then use a substring to get WMS signatory since the barcode doesnt match WMS records exactly
--      25/08/2020: Checks for a message in the hub contact fax and sends this to RDT screen without performing the repack
--
--  Grants:
--      grant execute on DCSDBA.LIBREPACK to TORQUE
--
--      grant select on dcsdba.MOVE_TASK to TORQUE
--      grant select on dcsdba.ORDER_HEADER to TORQUE
--      grant select on dcsdba.CARRIERS to TORQUE
--
--  Dependencies:
--
--  RDT screen visual:
-- ==============================
-- *                 (2834)     *
-- *               BoxCntEnt    *
-- *                            *
-- *    Order: ______________   *
-- *    ___________             *
-- *    To: ________________    *
-- *    ____                    *
-- *    Client: ____________    *
-- *                            *
-- ==============================
--     
--  Mappings:
--     DATA_RULE_INFO_1 => Client
--     DATA_RULE_INFO_2 => Order
--     DATA_RULE_INFO_3 => To
--
-- =============================================
function TQ_PALLET_REPACK
(
    p_Tracking varchar2,
    p_To varchar2,
    p_Client varchar2,
    p_User varchar2,
    p_Station varchar2
) return varchar2
is PRAGMA AUTONOMOUS_TRANSACTION; -- required to perform DML statements within a function

    -- re-assignments so just have to do upper once and not again for the rest of the function
        v_Tracking varchar2(60):= upper(p_Tracking);
        v_To varchar2(20):= upper(p_To);
        v_Client varchar2(10):= upper(p_Client);

    -- variables to store values
        v_Return varchar2(150); -- used to return data to rdt screen    

    -- counters
        v_Count number(5);
        v_Success number(1);

    -- checks
        v_Order varchar2(20);
        v_Status varchar2(20);
        v_Carrier varchar2(20);
        v_CarrierPrefix varchar2(5);
        v_AdminMessage varchar2(30);

    -- cursor to loop through the orders move tasks
        cursor cur_tasks is
            select key, pallet_id, container_id, sku_id, description, tag_id, site_id, from_loc_id, final_loc_id, line_id, list_id, work_group, customer_id, consignment, owner_id, pallet_config, pallet_volume, pallet_height, pallet_width, pallet_weight, pallet_depth, qty_to_move, container_config
            from dcsdba.MOVE_TASK
            where client_id = v_Client
            and task_id = v_Order
            and task_type = 'O';

    -- exception declarations
        ex_InvalidOrder exception;
        ex_OrderShipped exception;
        ex_NoCarrierSet exception;
        ex_OrderIncomplete exception;
        ex_NoMoveTasks exception;
        ex_InvalidToPallet exception;

begin
-- pre checks
    begin
    -- check if order entered rather than signatory
        begin
            select count(*) into v_Count
            from dcsdba.ORDER_HEADER
            where client_id = v_Client
            and order_id = v_Tracking
            and
            (
                status in ('Complete','In Progress','Picked')
                or
                (
                    status = 'Shipped'
                    and shipped_date > sysdate - 2 -- for performance
                )
            );
        end;
    -- if an order, get the details by using that
        if v_Count > 0 then
            begin
                select order_id, status, carrier_id, hub_contact_fax into v_Order, v_Status, v_Carrier, v_AdminMessage
                from dcsdba.ORDER_HEADER
                where client_id = v_Client
                and order_id = v_Tracking
                and
                (
                    status in ('Complete','In Progress','Picked')
                    or
                    (
                        status = 'Shipped'
                        and shipped_date > sysdate - 2 -- for performance
                    )
                );

                -- if v_Status = 'Shipped' then raise ex_OrderShipped;
                -- end if;

                if v_Carrier is null then raise ex_NoCarrierSet;
                end if;

                exception when NO_DATA_FOUND then raise ex_InvalidOrder;
            end;
    
        else
    -- get order details via the signatory
    -- handle tracking differently if scanning TNT or UKMAIL
            if substr(v_To, 1, 3) = 'TNT' then v_Tracking:= substr(v_Tracking, 15, 11); end if;
            if substr(v_To, 1, 3) = 'UKM' then v_Tracking:= substr(v_Tracking, 7, 14); end if;    
            if substr(v_To, 1, 4) = 'ARMX' then v_Tracking:= substr(v_Tracking, 19, 12); end if;    
            begin
                select order_id, status, carrier_id, hub_contact_fax into v_Order, v_Status, v_Carrier, v_AdminMessage
                from dcsdba.ORDER_HEADER
                where client_id = v_Client
                and signatory = v_Tracking
                and
                (
                    status in ('Complete','In Progress','Picked')
                    or
                    (
                        status = 'Shipped'
                        and shipped_date > sysdate - 2 -- for performance
                    )
                );

                -- if v_Status = 'Shipped' then raise ex_OrderShipped;
                -- end if;

                if v_Carrier is null then raise ex_NoCarrierSet;
                end if;

                exception when NO_DATA_FOUND then raise ex_InvalidOrder;
            end;
        end if;
    -- check for admin message and return out of the function if it is populated
        begin
            if v_AdminMessage is not null then
                v_Return:= v_AdminMessage;
                return v_Return;
            end if;
        end;
    -- check there are move tasks for the order
        begin
            select count(*) into v_Count
            from dcsdba.MOVE_TASK
            where client_id = v_Client
            and task_id = v_Order;

            if v_Count = 0 then raise ex_NoMoveTasks;
            end if;
        end;
    -- check full order's move tasks are all in a pallet
        begin
            select count(*) into v_Count
            from dcsdba.MOVE_TASK
            where client_id = v_Client
            and task_id = v_Order
            and status != 'Consol';

            if v_Count > 0 then raise ex_OrderIncomplete;
            end if;
        end;
    -- check valid TO_PALLET destination
        begin
            select consignment_prefix into v_CarrierPrefix
            from dcsdba.CARRIERS
            where client_id = v_Client
            and carrier_id = v_Carrier;

            if v_CarrierPrefix != substr(v_To, 1, length(v_CarrierPrefix)) then raise ex_InvalidToPallet;
            end if;
        end;
        
    end;

-- perform the function
    begin
    -- set counter value to 1 so it can be mulitplied by either 0 or 1 from success variable to determine if all results are successful or not
        v_Count:= 1;
    -- loop through the cursor
        for task in cur_tasks
        loop
            v_Success:= dcsdba.LibRepack.COMPLETEMOVETASKREPACK
                        (
                        P_KEY => task.key,
                        P_TOPALLETID => v_To,
                        P_TOCONTAINERID => v_Order,
                        P_FROMPALLETID => task.pallet_id,
                        P_FROMCONTAINERID => task.container_id,
                        P_CLIENTID => v_Client,
                        P_SKUID => task.sku_id,
                        P_DESCRIPTION => task.description,
                        P_TAGID => task.tag_id,
                        P_SITEID => task.site_id,
                        P_REPACKLOCID => task.from_loc_id,
                        P_FINALLOCID => task.final_loc_id,
                        P_TASKID => v_Order,
                        P_LINEID => task.line_id,
                        P_LISTID => task.list_id,
                        P_WORKGROUP => task.work_group,
                        P_CUSTOMERID => task.customer_id,
                        P_CONSIGNMENT => task.consignment,
                        P_OWNERID => task.owner_id,
                        P_PALLETCONFIG => task.pallet_config,
                        P_PALLETVOLUME => task.pallet_volume,
                        P_PALLETHEIGHT => task.pallet_height,
                        P_PALLETWIDTH => task.pallet_width,
                        P_PALLETWEIGHT => task.pallet_weight,
                        P_PALLETDEPTH => task.pallet_depth,
                        P_QTYTOMOVE => task.qty_to_move,
                        P_USERID => p_User,
                        P_STATIONID => p_Station,
                        P_TOCONTAINERCONFIG => task.container_config
                        );
            v_Count:= v_Count * v_Success;
        end loop;

        -- if any failures, roll back the transaction and return out of the function
            if v_Count = 0 then
                ROLLBACK;
                v_Return:= 'Pallet switch failedContact Supervisor';
                return v_Return;
            end if;
    end;

    COMMIT;

    v_Return:= rpad(v_Order, 20, ' ')||rpad(rpad('.', length(v_Order), '.'), 20, ' ')||'... => '||v_To;

    return v_Return;

-- exception handing
    exception
        when ex_InvalidOrder then v_Return:= 'Cannot Find Order   For Tracking Number:'||v_Tracking; return v_Return;
        when ex_OrderShipped then v_Return:= rpad('Order Is Already    Shipped', 40, ' ')||'Cannot Repack Order:'||v_Order; return v_Return;
        when ex_NoCarrierSet then v_Return:= 'No Carrier Set For  '||rpad('Order:', 20, ' ')||v_Order; return v_Return;
        when ex_NoMoveTasks then v_Return:= rpad('No Move Tasks Exist For Order: ', 40, ' ')||v_Order; return v_Return;
        when ex_OrderIncomplete then v_Return:= 'Order Not Complete'; return v_Return;
        when ex_InvalidToPallet then v_Return:= 'Cannot Repack Order:'||rpad(v_Order, 20, ' ')||'Must Be Scanned To: '||v_CarrierPrefix||'* Pallet'; return v_Return;

    -- catch all
        when others then v_Return:= 'Unknown Error...    Contact IT'; return v_Return;
end TQ_PALLET_REPACK;

-----------------------------------------
-----------------------------------------
procedure TQ_ADD_ITL_PROC
(
is_success out number,
in_code varchar2,
in_site_id varchar2 default null,
in_from_site_id varchar2 default null,
in_to_site_id varchar2 default null,
in_from_loc_id varchar2 default null,
in_to_loc_id varchar2 default null,
in_final_loc_id varchar2 default null,
in_ship_dock varchar2 default null,
in_dock_door_id varchar2 default null,
in_owner_id varchar2 default 'TORQUE',
in_client_id varchar2 default null,
in_sku_id varchar2 default null,
in_config_id varchar2 default null,
in_tag_id varchar2 default null,
in_container_id varchar2 default null,
in_pallet_id varchar2 default null,
in_batch_id varchar2 default null,
in_qc_status varchar2 default null,
in_expiry_dstamp timestamp with local time zone default null,
in_manuf_dstamp  timestamp with local time zone default null,
in_origin_id varchar2 default null,
in_condition_id varchar2 default null,
in_spec_code varchar2  default null,
in_lock_status varchar2 default null,
in_list_id varchar2 default null,
in_dstamp timestamp with local time zone default sysdate,
in_work_group varchar2 default null,
in_consignment varchar2 default null,
in_supplier_id varchar2 default null,
in_reference_id varchar2 default null,
in_line_id number default null,
in_reason_id varchar2 default null,
in_station_id varchar2,
in_user_id varchar2,
in_group_id varchar2 default null,
in_shift varchar2 default null,
in_update_qty number default 0,
in_original_qty number default null,
in_uploaded varchar2 default 'N',
in_uploaded_ws2pc_id varchar2 default null,
in_uploaded_filename varchar2 default null,
in_uploaded_dstamp timestamp with local time zone default null,
in_uploaded_ab varchar2 default null,
in_uploaded_tm varchar2 default null,
in_uploaded_vview varchar2 default null,
in_session_type varchar2 default null,
in_summary_record varchar2 default null,
in_elapsed_time number default null,
in_estimated_time number default null,
in_sap_idoc_type varchar2 default null,
in_sap_tid varchar2 default null,
in_task_category number default null,
in_sampling_type varchar2 default null,
in_job_id varchar2 default null,
in_manning number default null,
in_job_unit varchar2 default null,
in_complete_dstamp timestamp with local time zone default null,
in_grn number default null,
in_notes varchar2 default null,
in_user_def_type_1 varchar2 default null,
in_user_def_type_2 varchar2 default null,
in_user_def_type_3 varchar2 default null,
in_user_def_type_4 varchar2 default null,
in_user_def_type_5 varchar2 default null,
in_user_def_type_6 varchar2 default null,
in_user_def_type_7 varchar2 default null,
in_user_def_type_8 varchar2 default null,
in_user_def_chk_1 varchar2 default null,
in_user_def_chk_2 varchar2 default null,
in_user_def_chk_3 varchar2 default null,
in_user_def_chk_4 varchar2 default null,
in_user_def_date_1 timestamp with local time zone default null,
in_user_def_date_2 timestamp with local time zone default null,
in_user_def_date_3 timestamp with local time zone default null,
in_user_def_date_4 timestamp with local time zone default null,
in_user_def_num_1 number default null,
in_user_def_num_2 number default null,
in_user_def_num_3 number default null,
in_user_def_num_4 number default null,
in_user_def_note_1 varchar2 default null,
in_user_def_note_2 varchar2 default null,
in_old_user_def_type_1 varchar2 default null,
in_old_user_def_type_2 varchar2 default null,
in_old_user_def_type_3 varchar2 default null,
in_old_user_def_type_4 varchar2 default null,
in_old_user_def_type_5 varchar2 default null,
in_old_user_def_type_6 varchar2 default null,
in_old_user_def_type_7 varchar2 default null,
in_old_user_def_type_8 varchar2 default null,
in_old_user_def_chk_1 varchar2 default null,
in_old_user_def_chk_2 varchar2 default null,
in_old_user_def_chk_3 varchar2 default null,
in_old_user_def_chk_4 varchar2 default null,
in_old_user_def_date_1 timestamp with local time zone default null,
in_old_user_def_date_2 timestamp with local time zone default null,
in_old_user_def_date_3 timestamp with local time zone default null,
in_old_user_def_date_4 timestamp with local time zone default null,
in_old_user_def_num_1 number default null,
in_old_user_def_num_2 number default null,
in_old_user_def_num_3 number default null,
in_old_user_def_num_4 number default null,
in_old_user_def_note_1 varchar2 default null,
in_old_user_def_note_2 varchar2 default null,
in_ce_orig_rotation_id varchar2 default null,
in_ce_rotation_id varchar2 default null,
in_ce_consignment_id varchar2 default null,
in_ce_receipt_type varchar2 default null,
in_ce_originator varchar2 default null,
in_ce_originator_reference varchar2 default null,
in_ce_coo varchar2 default null,
in_ce_cwc varchar2 default null,
in_ce_ucr varchar2 default null,
in_ce_under_bond varchar2 default null,
in_ce_document_dstamp timestamp with local time zone default null,
in_ce_colli_count number default null,
in_ce_colli_count_expected number default null,
in_ce_seals_ok varchar2 default null,
in_uploaded_customs varchar2 default null,
in_uploaded_labor varchar2 default null,
in_print_label_id number default null,
in_lock_code varchar2 default null,
in_asn_id varchar2 default null,
in_customer_id varchar2 default null,
in_ce_duty_stamp varchar2 default null,
in_pallet_grouped varchar2 default null,
in_consol_link number default null,
in_job_site_id varchar2 default null,
in_job_client_id varchar2 default null,
in_tracking_level varchar2 default null,
in_extra_notes varchar2 default null,
in_stage_route_id varchar2 default null,
in_stage_route_sequence number default null,
in_pf_consol_link number default null,
in_master_pah_id varchar2 default null,
in_master_pal_id number default null,
in_archived varchar2 default null,
in_shipment_number number default null,
in_customer_shipment_number number default null,
in_pallet_config varchar2 default null,
in_container_type varchar2 default null,
in_ce_avail_status varchar2 default null,
in_from_status varchar2 default null,
in_to_status varchar2 default null,
in_kit_plan_id varchar2 default null,
in_plan_sequence number default null,
in_master_order_id varchar2 default null,
in_master_order_line_id number default null,
in_ce_invoice_number varchar2 default null,
in_rdt_user_mode varchar2 default null,
in_labor_assignment number default null,
in_grid_pick varchar2 default null,
in_labor_grid_sequence number default null,
in_kit_ce_consignment_id varchar2 default null
)
is 	
    v_key number;
begin
	select dcsdba.INVENTORY_TRANSACTION_PK_SEQ.nextval into v_key from dual;

    insert into dcsdba.INVENTORY_TRANSACTION
    (
        key,
        code,
        site_id,
        from_site_id,
        to_site_id,
        from_loc_id,
        to_loc_id,
        final_loc_id,
        ship_dock,
        dock_door_id,
        owner_id,
        client_id,
        sku_id,
        config_id,
        tag_id,
        container_id,
        pallet_id,
        batch_id,
        qc_status,
        expiry_dstamp,
        manuf_dstamp,
        origin_id,
        condition_id,
        spec_code,
        lock_status,
        list_id,
        dstamp,
        work_group,
        consignment,
        supplier_id,
        reference_id,
        line_id,
        reason_id,
        station_id,
        user_id,
        group_id,
        shift,
        update_qty,
        original_qty,
        uploaded,
        uploaded_ws2pc_id,
        uploaded_filename,
        uploaded_dstamp,
        uploaded_ab,
        uploaded_tm,
        uploaded_vview,
        session_type,
        summary_record,
        elapsed_time,
        estimated_time,
        sap_idoc_type,
        sap_tid,
        task_category,
        sampling_type,
        job_id,
        manning,
        job_unit,
        complete_dstamp,
        grn,
        notes,
        user_def_type_1,
        user_def_type_2,
        user_def_type_3,
        user_def_type_4,
        user_def_type_5,
        user_def_type_6,
        user_def_type_7,
        user_def_type_8,
        user_def_chk_1,
        user_def_chk_2,
        user_def_chk_3,
        user_def_chk_4,
        user_def_date_1,
        user_def_date_2,
        user_def_date_3,
        user_def_date_4,
        user_def_num_1,
        user_def_num_2,
        user_def_num_3,
        user_def_num_4,
        user_def_note_1,
        user_def_note_2,
        old_user_def_type_1,
        old_user_def_type_2,
        old_user_def_type_3,
        old_user_def_type_4,
        old_user_def_type_5,
        old_user_def_type_6,
        old_user_def_type_7,
        old_user_def_type_8,
        old_user_def_chk_1,
        old_user_def_chk_2,
        old_user_def_chk_3,
        old_user_def_chk_4,
        old_user_def_date_1,
        old_user_def_date_2,
        old_user_def_date_3,
        old_user_def_date_4,
        old_user_def_num_1,
        old_user_def_num_2,
        old_user_def_num_3,
        old_user_def_num_4,
        old_user_def_note_1,
        old_user_def_note_2,
        ce_orig_rotation_id,
        ce_rotation_id,
        ce_consignment_id,
        ce_receipt_type,
        ce_originator,
        ce_originator_reference,
        ce_coo,
        ce_cwc,
        ce_ucr,
        ce_under_bond,
        ce_document_dstamp,
        ce_colli_count,
        ce_colli_count_expected,
        ce_seals_ok,
        uploaded_customs,
        uploaded_labor,
        print_label_id,
        lock_code,
        asn_id,
        customer_id,
        ce_duty_stamp,
        pallet_grouped,
        consol_link,
        job_site_id,
        job_client_id,
        tracking_level,
        extra_notes,
        stage_route_id,
        stage_route_sequence,
        pf_consol_link,
        master_pah_id,
        master_pal_id,
        archived,
        shipment_number,
        customer_shipment_number,
        pallet_config,
        container_type,
        ce_avail_status,
        from_status,
        to_status,
        kit_plan_id,
        plan_sequence,
        master_order_id,
        master_order_line_id,
        ce_invoice_number,
        rdt_user_mode,
        labor_assignment,
        grid_pick,
        labor_grid_sequence,
        kit_ce_consignment_id
    )values(
		v_key,
		in_code,
		in_site_id,
		in_from_site_id,
		in_to_site_id,
		in_from_loc_id,
		in_to_loc_id,
		in_final_loc_id,
		in_ship_dock,
		in_dock_door_id,
		in_owner_id,
		in_client_id,
		in_sku_id,
		in_config_id,
		in_tag_id,
		in_container_id,
		in_pallet_id,
		in_batch_id,
		in_qc_status,
		in_expiry_dstamp,
		in_manuf_dstamp,
		in_origin_id,
		in_condition_id,
		in_spec_code,
		in_lock_status,
		in_list_id,
		in_dstamp,
		in_work_group,
		in_consignment,
		in_supplier_id,
		in_reference_id,
		in_line_id,
		in_reason_id,
		in_station_id,
		in_user_id,
		in_group_id,
		in_shift,
		in_update_qty,
		in_original_qty,
		in_uploaded,
		in_uploaded_ws2pc_id,
		in_uploaded_filename,
		in_uploaded_dstamp,
		in_uploaded_ab,
		in_uploaded_tm,
		in_uploaded_vview,
		in_session_type,
		in_summary_record,
		in_elapsed_time,
		in_estimated_time,
		in_sap_idoc_type,
		in_sap_tid,
		in_task_category,
		in_sampling_type,
		in_job_id,
		in_manning,
		in_job_unit,
		in_complete_dstamp,
		in_grn,
		in_notes,
		in_user_def_type_1,
		in_user_def_type_2,
		in_user_def_type_3,
		in_user_def_type_4,
		in_user_def_type_5,
		in_user_def_type_6,
		in_user_def_type_7,
		in_user_def_type_8,
		in_user_def_chk_1,
		in_user_def_chk_2,
		in_user_def_chk_3,
		in_user_def_chk_4,
		in_user_def_date_1,
		in_user_def_date_2,
		in_user_def_date_3,
		in_user_def_date_4,
		in_user_def_num_1,
		in_user_def_num_2,
		in_user_def_num_3,
		in_user_def_num_4,
		in_user_def_note_1,
		in_user_def_note_2,
		in_old_user_def_type_1,
		in_old_user_def_type_2,
		in_old_user_def_type_3,
		in_old_user_def_type_4,
		in_old_user_def_type_5,
		in_old_user_def_type_6,
		in_old_user_def_type_7,
		in_old_user_def_type_8,
		in_old_user_def_chk_1,
		in_old_user_def_chk_2,
		in_old_user_def_chk_3,
		in_old_user_def_chk_4,
		in_old_user_def_date_1,
		in_old_user_def_date_2,
		in_old_user_def_date_3,
		in_old_user_def_date_4,
		in_old_user_def_num_1,
		in_old_user_def_num_2,
		in_old_user_def_num_3,
		in_old_user_def_num_4,
		in_old_user_def_note_1,
		in_old_user_def_note_2,
		in_ce_orig_rotation_id,
		in_ce_rotation_id,
		in_ce_consignment_id,
		in_ce_receipt_type,
		in_ce_originator,
		in_ce_originator_reference,
		in_ce_coo,
		in_ce_cwc,
		in_ce_ucr,
		in_ce_under_bond,
		in_ce_document_dstamp,
		in_ce_colli_count,
		in_ce_colli_count_expected,
		in_ce_seals_ok,
		in_uploaded_customs,
		in_uploaded_labor,
		in_print_label_id,
		in_lock_code,
		in_asn_id,
		in_customer_id,
		in_ce_duty_stamp,
		in_pallet_grouped,
		in_consol_link,
		in_job_site_id,
		in_job_client_id,
		in_tracking_level,
		in_extra_notes,
		in_stage_route_id,
		in_stage_route_sequence,
		in_pf_consol_link,
		in_master_pah_id,
		in_master_pal_id,
		in_archived,
		in_shipment_number,
		in_customer_shipment_number,
		in_pallet_config,
		in_container_type,
		in_ce_avail_status,
		in_from_status,
		in_to_status,
		in_kit_plan_id,
		in_plan_sequence,
		in_master_order_id,
		in_master_order_line_id,
		in_ce_invoice_number,
		in_rdt_user_mode,
		in_labor_assignment,
		in_grid_pick,
		in_labor_grid_sequence,
		in_kit_ce_consignment_id
    );

    is_success:= SQL%ROWCOUNT;

    if is_success = 0 then rollback;
        else commit;
	end if;

end TQ_ADD_ITL_PROC;

-- =============================================
--  TQ_FUNCTIONS.TQ_PALLET_REPACK_V2
-- =============================================
--  Author:      Christian Haslam
--  Create date: 26/08/2020
--  Description: Takes in an tracking id or order id and tags the locations scanned against hub values in the order header
--
--  Parameters:
--      p_From      - tracking number that gets converted to Order (or just plain order id)
--      p_To        - destination pallet id
--      p_Client    - client id
--      p_TrailerLoad - If N, checks order by order. If Y, checks everything on pallet supplied
--      p_User      - BUFFER_USER_ID
--      p_Station   - RDT Number
--
--  Returns:
--      v_Return    - Return info to the RDT
--
--  Change History:
--      29/10/2020: Added either --SUCCESS-- or --FAILURE-- to the opening return line
--      28/11/2020: Moved the carrier selection rule above the statement to check about the carrier
--
--  Grants:
--      grant update on DCSDBA.ORDER_HEADER to TORQUE
--
--      grant select on dcsdba.ORDER_HEADER to TORQUE
--      grant select on dcsdba.CARRIERS to TORQUE
--
--      grant execute on dcsdba.LibCarrierSelection to TORQUE
--
--  Dependencies:
--
--  RDT screen visual:
-- ==============================
-- *                 (2834)     *
-- *               BoxCntEnt    *
-- *                            *
-- *    From: _______________   *
-- *    ___________             *
-- *    To: ________________    *
-- *    ____                    *
-- *    Client: ____________    *
-- *    TrlLoad: N              *
-- *                            *
-- ==============================
--
--  Mappings:
--     DATA_RULE_INFO_1 => Client
--     DATA_RULE_INFO_2 => From
--     DATA_RULE_INFO_3 => To
--     DATA_RULE_INFO_4 => Trailer packing Y | N
--
-- =============================================
function TQ_PALLET_REPACK_V2
(
    p_From varchar2,
    p_To varchar2,
    p_Client varchar2,
    p_TrailerLoad char default 'N',
    p_User varchar2,
    p_Station varchar2
) return varchar2
is PRAGMA AUTONOMOUS_TRANSACTION; -- required to perform DML statements within a function

    -- re-assignments so just have to do upper once and not again for the rest of the function
        v_From varchar2(60):= upper(p_From);
        v_To varchar2(20):= upper(p_To);
        v_Client varchar2(10):= upper(p_Client);
        v_IsTrailerload char:= upper(p_TrailerLoad);

    -- variables to store values
        v_Return varchar2(150); -- used to return data to rdt screen

    -- counters
        v_Count number(5);
        v_Success number(1);

    -- checks
        v_Order varchar2(20);
        v_Status varchar2(20);
        v_Carrier varchar2(20);
        v_Consignment varchar2(20);
        v_CarrierPrefix varchar2(5);
        v_AdminMessage varchar2(30);

    -- cursor
        cursor cur_TrlOrders (p_Client varchar2, p_FromPallet varchar2) is
            select order_id, carrier_id, consignment
            from dcsdba.ORDER_HEADER
            where client_id = p_Client
            and hub_address2 = p_FromPallet
            and
            (
                status not in ('Shipped','Cancelled')
                or
                (
                    status = 'Shipped'
                    and shipped_date > sysdate - 2 -- for performance
                )
            );


    -- exception declarations
        ex_InvalidOrder exception;
        ex_OrderStatusError exception;
        ex_NoCarrierSet exception;
        ex_OrderIncomplete exception;
        ex_InvalidToPallet exception;
        ex_TrlFromPallEmpty exception;

begin
-- pre checks
    begin
    -- If trailer load selection is N, perform the update at order header detail
        if v_IsTrailerload = 'N' then
        -- check if order entered rather than signatory
            begin
                select count(*) into v_Count
                from dcsdba.ORDER_HEADER
                where client_id = v_Client
                and order_id = v_From
                and
                (
                    status not in ('Shipped','Cancelled')
                    or
                    (
                        status = 'Shipped'
                        and shipped_date > sysdate - 2 -- for performance
                    )
                );
            end;
        -- if an order, get the details by using that
            if v_Count > 0 then
                begin
                    select order_id, status, hub_contact_fax, consignment into v_Order, v_Status, v_AdminMessage, v_Consignment
                    from dcsdba.ORDER_HEADER
                    where client_id = v_Client
                    and order_id = v_From
                    and
                    (
                        status not in ('Shipped','Cancelled')
                        or
                        (
                            status = 'Shipped'
                            and shipped_date > sysdate - 2 -- for performance
                        )
                    );

                    if v_Status not in ('Complete','Picked','In Progress','Shipped') then raise ex_OrderStatusError;
                    end if;

                    -- execute carrier selection rule
                    dcsdba.LibCarrierSelection.SelectOrderCarrier(v_Order, v_Client);
                    
                    select carrier_id into v_Carrier from dcsdba.ORDER_HEADER where order_id = v_Order and client_id = v_Client;                    

                    if v_Carrier is null then raise ex_NoCarrierSet;
                    end if;

                    exception when NO_DATA_FOUND then raise ex_InvalidOrder;
                end;

            else
        -- get order details via the signatory
        -- handle tracking differently if scanning TNT/UKMAIL/ARAMEX
                if substr(v_To, 1, 3) = 'TNT' then v_From:= substr(v_From, 15, 11); end if;
                if substr(v_To, 1, 3) = 'UKM' then v_From:= substr(v_From, 7, 14); end if;
                if substr(v_To, 1, 4) = 'ARMX' then v_From:= substr(v_From, 19, 12); end if;
                begin
                    select order_id, status, hub_contact_fax, consignment into v_Order, v_Status, v_AdminMessage, v_Consignment
                    from dcsdba.ORDER_HEADER
                    where client_id = v_Client
                    and signatory = v_From
                    and
                    (
                        status not in ('Shipped','Cancelled')
                        or
                        (
                            status = 'Shipped'
                            and shipped_date > sysdate - 2 -- for performance
                        )
                    );                    

                    if v_Status not in ('Complete','Picked','In Progress','Shipped') then raise ex_OrderStatusError;
                    end if;
                    
                    -- execute carrier selection rule
                    dcsdba.LibCarrierSelection.SelectOrderCarrier(v_Order, v_Client);

                    select carrier_id into v_Carrier from dcsdba.ORDER_HEADER where order_id = v_Order and client_id = v_Client;                    

                    if v_Carrier is null then raise ex_NoCarrierSet;
                    end if;

                    exception when NO_DATA_FOUND then raise ex_InvalidOrder;
                end;
            end if;
        -- check for admin message and return out of the function if it is populated
            begin
                if v_AdminMessage is not null then
                    v_Return:= v_AdminMessage;
                    return v_Return;
                end if;
            end;
        -- check valid TO_PALLET destination
            begin
                select consignment_prefix into v_CarrierPrefix
                from dcsdba.CARRIERS
                where client_id = v_Client
                and carrier_id = v_Carrier;

                if v_CarrierPrefix != substr(v_To, 1, length(v_CarrierPrefix)) then raise ex_InvalidToPallet;
                end if;
            end;
        -- perform the function
            begin
            -- update ORDER_HEADER hub details
                update dcsdba.ORDER_HEADER
                set hub_address2 = v_To
                where client_id = v_Client
                and order_id = v_Order;

                v_Count := SQL%ROWCOUNT;

                TQ_ADD_ITL_PROC
                (
                    is_success => v_Success,
                    in_code => 'Repack',
                    in_user_id => p_User,
                    in_station_id => p_Station,
                    in_to_loc_id => v_To,
                    in_reference_id => v_Order,
                    in_client_id => v_Client,
                    in_update_qty => 1,
                    in_notes => v_Carrier,
                    in_extra_notes => 'ORDER REPACK',
                    in_consignment => v_Consignment,
                    in_line_id => 1
                );

                if v_Count * v_Success = 0 then v_Return:= '--FAILURE--         Something went wrongContact Supervisor.';
                    rollback;
                    return v_Return;
                end if;

                commit;

                v_Return:= '--SUCCESS--         '||rpad(v_Order, 20, ' ')||rpad(rpad('.', length(v_Order), '.'), 20, ' ')||'... => '||v_To;

                return v_Return;
            end;
        elsif v_IsTrailerload = 'Y' then
        -- check that TO_PALLET matches FROM_PALLET
            if substr(v_From, 1, 3) <> substr(v_To, 1, 3) then 
                v_Return:= '--FAILURE--         Invalid Trailer id Must be '||substr(v_From, 1, 3)||'* to pallet';
                return v_Return;
            end if;
        -- check the from pallet exists
            select count(*) into v_Count
            from dcsdba.ORDER_HEADER
            where client_id = v_Client
            and hub_address2 = v_From
            and 
            (
                status not in ('Shipped','Cancelled')
                or
                (
                    status = 'Shipped'
                    and shipped_date > sysdate - 2 -- for performance
                )
            );

            if v_Count = 0 then raise ex_TrlFromPallEmpty;
            end if;

        -- use the from input for the pallet
            update dcsdba.ORDER_HEADER
            set hub_name = v_To
            where client_id = v_Client
            and hub_address2 = v_From
            and 
            (
                status not in ('Shipped','Cancelled')
                or
                (
                    status = 'Shipped'
                    and shipped_date > sysdate - 2 -- for performance
                )
            );

            v_Count := SQL%ROWCOUNT;

            for ord in cur_TrlOrders (v_Client, v_From)
            loop

                TQ_ADD_ITL_PROC
                (
                    is_success => v_Success,
                    in_code => 'Repack',
                    in_user_id => p_User,
                    in_station_id => p_Station,
                    in_from_loc_id => v_From,
                    in_to_loc_id => v_To,
                    in_reference_id => ord.order_id,
                    in_client_id => v_Client,
                    in_update_qty => 1,
                    in_notes => ord.carrier_id,
                    in_consignment => ord.consignment,
                    in_extra_notes => 'TRAILER REPACK',
                    in_line_id => 2
                );

            v_Count:= v_Count * v_Success;

            end loop;

            if v_Count * v_Success = 0 then v_Return:= '--FAILURE--         Something went wrongContact Supervisor.';
                rollback;
                return v_Return;
            end if;

            commit;

            v_Return:= '--SUCCESS--         '||rpad(v_From, 20, ' ')||rpad(rpad('.', length(v_From), '.'), 20, ' ')||'... => '||v_To;

            return v_Return;
        else v_Return:= '--FAILURE--         Invalid IsTrailer   Entered';
        end if;
        return v_Return;
    end;

-- exception handing
    exception
        when ex_InvalidOrder then v_Return:= '--FAILURE--         Cannot Find Order   For Tracking Number:'||v_From; return v_Return;
        when ex_OrderStatusError then v_Return:= '--FAILURE--         '||rpad('Order Is '||v_Status, 20, ' ')||'Cannot Repack Order:'||v_Order; return v_Return;
        when ex_NoCarrierSet then v_Return:= '--FAILURE--         No Carrier Set For  '||rpad('Order:', 20, ' ')||v_Order; return v_Return;
        when ex_InvalidToPallet then v_Return:= '--FAILURE--         Cannot Repack Order:'||rpad(v_Order, 20, ' ')||'Must Be Scanned To: '||v_CarrierPrefix||'* Pallet'; return v_Return;
        when ex_TrlFromPallEmpty then v_Return:= '--FAILURE--         Invalid From Pallet: ' || v_From; return v_Return;

    -- catch all
        when others then v_Return:= '--FAILURE--         Unknown Error...    Contact IT'; return v_Return;
end TQ_PALLET_REPACK_V2;

-- =============================================
--  TQ_FUNCTIONS.TQ_IS_ORDER_COMPLETE
-- =============================================
--  Author:      Christian Haslam
--  Create date: 15/12/2020
--  Description: Takes in a Client and an order determines whether it has been picked or not (Single Unit Orders only)
--
--  Parameters:
--      p_Client
--      p_Order
--
--  Returns:
--      v_Return    - 0 for failure or 1 for success
--
--  Change History:
--
--  Grants:
--      grant select on DCSDBA.ORDER_LINE to TORQUE
--      grant select on DCSDBA.ORDER_CONTAINER to TORQUE

  function tq_is_order_complete
(
    p_Client varchar2,
    p_Order varchar2
) return number 
as
v_Return number(1);
begin
    select count(*) into v_Return
    from dual
    where exists
    (
    select 1 ex
    from dcsdba.ORDER_LINE
    where client_id = upper(p_Client)
    and order_id = upper(p_Order)
    and coalesce(qty_picked , 0) > 0
    union all
    select 1 ex
    from dcsdba.ORDER_CONTAINER
    where order_id = upper(p_Order)
    and client_id = upper(p_Client)
    );
    return v_Return;
end tq_is_order_complete;

--------------------------------------------

END tq_functions;

/

  GRANT EXECUTE ON "TORQUE"."TQ_FUNCTIONS" TO "DCSRPT";
  GRANT EXECUTE ON "TORQUE"."TQ_FUNCTIONS" TO "DCSDBA";
--------------------------------------------------------
--  DDL for Package Body RDTBUFFER
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "TORQUE"."RDTBUFFER" AS 

/***********************************************************************************************************************/ 

/*                                                                                                                     */ 

/*  NAME:               RDTBUFFER.SetBufferData                                                                        */ 

/*                                                                                                                     */ 

/*  DESCRIPTION:        These Fucntions/Procedures Set Data In The V RDT BUFFER                                        */ 

/*                                                                                                                     */ 

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */ 

/*  =======     ===========     ==     ===================================================================             */ 

/*    1         2019-05-14      KS     Initial Version                                                                 */ 

/*                                                                                                                     */  

/***********************************************************************************************************************/ 

--Declare P_Insert_Buffer_Data Procedure 

    PROCEDURE p_setbufferdata (
        p_result      OUT   INTEGER,
        p_clientid    IN    VARCHAR2,
        p_userid       IN    VARCHAR2,
        p_stationid   IN    VARCHAR2,
        p_field       IN    VARCHAR2 := NULL,
        p_data        IN    VARCHAR2 := NULL
    );

    PROCEDURE p_setbufferdata( 

            p_Result      OUT   INTEGER,
        p_clientid    IN    VARCHAR2,
        p_userid      IN    VARCHAR2,
        p_stationid   IN    VARCHAR2,
        p_field       IN    VARCHAR2 := NULL,
        p_data        IN    VARCHAR2 := NULL
    ) IS
        PRAGMA autonomous_transaction;
        v_hit INTEGER;
    BEGIN
        SELECT COUNT(*)
        INTO v_hit
        FROM
            torque.v_rdt_buffer
        WHERE 

            nvl(client_id, 'XXX') = nvl(p_clientid,'XXX')
            AND nvl(user_id, 'XXX') = nvl(p_userid, 'XXX')
            AND nvl(station_id, 'XXX') = nvl(p_stationid, 'XXX')
            AND nvl(field_name, 'XXX') = nvl(p_field, 'XXX');

        IF v_hit > 0 then
            UPDATE torque.v_rdt_buffer
            SET
                field_data = p_data
            WHERE
                nvl(client_id, 'XXX') = nvl(p_clientid, 'XXX')
                AND nvl(user_id, 'XXX') = nvl(p_userid, 'XXX')
                AND nvl(station_id, 'XXX') = nvl(p_stationid, 'XXX')
                AND nvl(field_name, 'XXX') = nvl(p_field, 'XXX');

            p_result := 1;
        ELSE
            INSERT INTO torque.v_rdt_buffer (
                client_id,
                user_id,
                station_id,
                field_name,
                field_data
            ) VALUES (
                p_clientid,
                p_userid,
                p_stationid,
                p_field,
                p_data
            );

            p_result := 1;
        END IF;

        COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            p_result := 0;
    END p_setbufferdata; 





--Declare P_Delete_Buffer_Data Procedure 

PROCEDURE p_deletebufferdata (
    p_result      OUT   INTEGER,
    p_clientid    IN    VARCHAR2,
    p_userid      IN    VARCHAR2,
    p_stationid   IN    VARCHAR2,
    p_field       IN    VARCHAR2 := NULL
);
    PROCEDURE p_deletebufferdata (
        p_result      OUT   INTEGER,
        p_clientid    IN    VARCHAR2,
        p_userid      IN    VARCHAR2,
        p_stationid   IN    VARCHAR2,
        p_field       IN    VARCHAR2 := NULL
    ) IS
        PRAGMA autonomous_transaction;
    BEGIN
        IF p_userid IS NOT NULL OR p_stationid IS NOT NULL THEN
            DELETE FROM v_rdt_buffer
            WHERE
                ( client_id = p_clientid
                  OR p_clientid IS NULL )
                AND ( field_name = p_field
                      OR p_field IS NULL )
                AND ( user_id = p_userid
                      OR p_userid IS NULL )
                AND ( station_id = p_stationid
                      OR p_stationid IS NULL )
                AND ( p_stationid IS NOT NULL
                      OR p_userid IS NOT NULL );

            p_result := 1;
            COMMIT;
        ELSE
            p_result := 0;
        END IF;
    EXCEPTION
        WHEN OTHERS THEN
            p_result := 0;
    END P_DeleteBufferData;     

--Public Function SetBufferData 

    FUNCTION setbufferdata (
        p_clientid    IN   VARCHAR2,
        p_userid      IN   VARCHAR2,
        p_stationid   IN   VARCHAR2,
        p_field       IN   VARCHAR2 := NULL,
        p_data        IN   VARCHAR2 := NULL
    ) RETURN INTEGER AS
        v_result INTEGER;
    BEGIN
        p_setbufferdata(v_result, p_clientid, p_userid, p_stationid, p_field, p_data);
        RETURN v_result;
    END setbufferdata;        









--Public Function ReadBufferData 

    FUNCTION readbufferdata (
        p_clientid    IN   VARCHAR2,
        p_userid      IN   VARCHAR2,
        p_stationid   IN   VARCHAR2,
        p_field       IN   VARCHAR2 := NULL
    ) RETURN VARCHAR2 AS
        v_result VARCHAR2(4000);
    BEGIN
        SELECT
            field_data
        INTO v_result
        FROM
            torque.v_rdt_buffer
        WHERE
            nvl(client_id, 'XXX') = nvl(p_clientid, 'XXX')
            AND nvl(user_id, 'XXX') = nvl(p_userid, 'XXX')
            AND nvl(station_id, 'XXX') = nvl(p_stationid, 'XXX')
            AND nvl(field_name, 'XXX') = nvl(p_field, 'XXX');

        RETURN v_result;
    EXCEPTION
        WHEN no_data_found THEN
            RETURN NULL;
    END readbufferdata;     







--Public Function DeleteBufferData 

    FUNCTION deletebufferdata (
        p_clientid    IN   VARCHAR2,
        p_userid      IN   VARCHAR2,
        p_stationid   IN   VARCHAR2,
        p_field       IN   VARCHAR2 := NULL
    ) RETURN INTEGER AS
        v_result INTEGER;
    BEGIN
        p_deletebufferdata(v_result, p_clientid, p_userid, p_stationid, p_field);
        RETURN v_result;
    END deletebufferdata;

    PROCEDURE purgebufferdata (
        p_age IN NUMBER DEFAULT 7
    ) IS
    BEGIN
        DELETE FROM v_rdt_buffer
        WHERE
            dstamp < trunc(SYSDATE) - p_age;

        COMMIT;
    END purgebufferdata;
    
    
    
        FUNCTION clearallbuffer (
        p_userid      IN   VARCHAR2,
        p_stationid   IN    VARCHAR2
    ) RETURN INTEGER as
    BEGIN
    delete from torque.v_rdt_buffer where user_id = p_userid and station_id = p_stationid;
    return 1;
    end;
    

END rdtbuffer;

/

  GRANT EXECUTE ON "TORQUE"."RDTBUFFER" TO "DCSDBA";
--------------------------------------------------------
--  DDL for Package Body JDA_PICKING
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "TORQUE"."JDA_PICKING" AS

/***********************************************************************************************************************/ 

/*                                                                                                                     */ 

/*  NAME:               JDA_PICKING.RE_SORT_TASKS                                                                      */ 

/*                                                                                                                     */ 

/*  DESCRIPTION:        This Report Uses an Oracle PL/SQL Function to Change The Sequence                              */

/*                      On Move Tasks, To Effect Pick Walk                                                             */ 

/*                                                                                                                     */ 

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */ 

/*  =======     ===========     ==     ===================================================================             */ 

/*    1         2019-07-23      KS     Initial Version                                                                 */ 

/*                                                                                                                     */  

/***********************************************************************************************************************/  

/**********************GRANTS**************************

grant select on location to TORQUE;
grant select on move_task to TORQUE;
grant select on sku to TORQUE;

grant update on move_task to TORQUE;

grant execute on liberror to TORQUE;

******************************************************/

    FUNCTION re_sort_tasks (
        p_client_id   IN   VARCHAR2,
        p_order_id    IN   VARCHAR2,
        p_sort_by     IN   VARCHAR2
    ) RETURN VARCHAR2 AS

        PRAGMA autonomous_transaction;
        exception_field_not_exist EXCEPTION;
        exception_no_tasks_exist EXCEPTION;
        exception_invalid_sort_on EXCEPTION;
        v_sort_check    NUMBER;
        v_order_check   NUMBER;
        v_sort_string   VARCHAR2(50);
    BEGIN

--Validate Tasks Exist For Order
        SELECT
            COUNT(*)
        INTO v_order_check
        FROM
            dcsdba.move_task
        WHERE
            client_id = p_client_id
            AND task_id = p_order_id;

        IF v_order_check = 0 THEN
            RAISE exception_no_tasks_exist;
        END IF;

--Validate Sort By Is Valid
        IF upper(p_sort_by) = 'SKU - UDT1' THEN
            FOR x IN (
                SELECT
                    move_task.key,
                    move_task.sku_id,
                    move_task.sequence    AS mt_seq,
                    sku.user_def_type_1   AS ordering,
                    ROW_NUMBER() OVER(
                        ORDER BY
                            sku.user_def_type_1, location.pick_sequence
                    ) AS task_sequ
                FROM
                    dcsdba.move_task
                    INNER JOIN dcsdba.sku ON ( move_task.client_id = sku.client_id
                                               AND move_task.sku_id = sku.sku_id )
                    INNER JOIN dcsdba.location ON ( move_task.from_loc_id = location.location_id
                                                    AND move_task.site_id = location.site_id )
                WHERE
                    move_task.task_id = p_order_id
                    AND move_task.client_id = p_client_id
                ORDER BY
                    ordering,
                    location.pick_sequence
            ) LOOP UPDATE dcsdba.move_task
            SET
                sequence = x.task_sequ
            WHERE
                key = x.key
                AND client_id = p_client_id
                AND task_id = p_order_id;

            END LOOP;

            COMMIT;
            -- SKU.UDT1 + SKU.COLOUR
            ELSIF upper(p_sort_by) = 'SKU - UDT1 COL' THEN
            FOR x IN (
                SELECT
                    move_task.key,
                    move_task.sku_id,
                    move_task.sequence    AS mt_seq,
                    sku.user_def_type_1 || sku.colour   AS ordering,
                    ROW_NUMBER() OVER(
                        ORDER BY
                            sku.user_def_type_1, location.pick_sequence
                    ) AS task_sequ
                FROM
                    dcsdba.move_task
                    INNER JOIN dcsdba.sku ON ( move_task.client_id = sku.client_id
                                               AND move_task.sku_id = sku.sku_id )
                    INNER JOIN dcsdba.location ON ( move_task.from_loc_id = location.location_id
                                                    AND move_task.site_id = location.site_id )
                WHERE
                    move_task.task_id = p_order_id
                    AND move_task.client_id = p_client_id
                ORDER BY
                    ordering,
                    location.pick_sequence
            ) LOOP UPDATE dcsdba.move_task
            SET
                sequence = x.task_sequ
            WHERE
                key = x.key
                AND client_id = p_client_id
                AND task_id = p_order_id;

            END LOOP;

            COMMIT;
        ELSIF upper(p_sort_by) = 'SKU - PRODUCT_GROUP' THEN
            FOR x IN (
                SELECT
                    move_task.key,
                    move_task.sku_id,
                    move_task.sequence    AS mt_seq,
                    sku.user_def_type_1   AS ordering,
                    ROW_NUMBER() OVER(
                        ORDER BY
                            sku.product_group, location.pick_sequence
                    ) AS task_sequ
                FROM
                    dcsdba.move_task
                    INNER JOIN dcsdba.sku ON ( move_task.client_id = sku.client_id
                                               AND move_task.sku_id = sku.sku_id )
                    INNER JOIN dcsdba.location ON ( move_task.from_loc_id = location.location_id
                                                    AND move_task.site_id = location.site_id )
                WHERE
                    move_task.task_id = p_order_id
                    AND move_task.client_id = p_client_id
                ORDER BY
                    ordering,
                    location.pick_sequence
            ) LOOP UPDATE dcsdba.move_task
            SET
                sequence = x.task_sequ
            WHERE
                key = x.key
                AND client_id = p_client_id
                AND task_id = p_order_id;

            END LOOP;

            COMMIT;
        ELSIF upper(p_sort_by) = 'MT - SKU_ID' THEN
            FOR x IN (
                SELECT
                    move_task.key,
                    move_task.sku_id,
                    move_task.sequence   AS mt_seq,
                    move_task.sku_id     AS ordering,
                    ROW_NUMBER() OVER(
                        ORDER BY
                            move_task.sku_id, location.pick_sequence
                    ) AS task_sequ
                FROM
                    dcsdba.move_task
                    INNER JOIN dcsdba.location ON ( move_task.from_loc_id = location.location_id
                                                    AND move_task.site_id = location.site_id )
                WHERE
                    move_task.task_id = p_order_id
                    AND move_task.client_id = p_client_id
                ORDER BY
                    ordering,
                    location.pick_sequence
            ) LOOP UPDATE dcsdba.move_task
            SET
                sequence = x.task_sequ
            WHERE
                key = x.key
                AND client_id = p_client_id
                AND task_id = p_order_id;

            END LOOP;

            COMMIT;
        ELSIF upper(p_sort_by) = 'LOC - PICK_SEQUENCE' THEN
            FOR x IN (
                SELECT
                    move_task.key,
                    move_task.sku_id,
                    move_task.sequence       AS mt_seq,
                    location.pick_sequence   AS ordering,
                    location.pick_sequence   AS task_sequ
                FROM
                    dcsdba.move_task
                    INNER JOIN dcsdba.location ON ( move_task.from_loc_id = location.location_id
                                                    AND move_task.site_id = location.site_id )
                WHERE
                    move_task.task_id = p_order_id
                    AND move_task.client_id = p_client_id
                ORDER BY
                    ordering
            ) LOOP UPDATE dcsdba.move_task
            SET
                sequence = x.task_sequ
            WHERE
                key = x.key
                AND client_id = p_client_id
                AND task_id = p_order_id;

            END LOOP;

            COMMIT;
        ELSE
            RAISE exception_invalid_sort_on;
        END IF;

        RETURN 'Success - Tasks Pick Sequence Has Been Re-Sorted Using ' || p_sort_by;
    EXCEPTION
        WHEN exception_invalid_sort_on THEN
            RETURN 'Not Valid Data to Sort On';
        WHEN exception_no_tasks_exist THEN
            RETURN 'No Move Tasks Exist for the Order/Client Provided';
        WHEN OTHERS THEN
            dcsdba.liberror.writeerrorlog('JDA_PICKING.RE_SORT_TASKS', sqlcode, sqlerrm);
            RETURN 'Unknown Issue - Please Raise A Helpdesk Ticket';
    END re_sort_tasks;

/***********************************************************************************************************************/ 

/*                                                                                                                     */ 

/*  NAME:               JDA_PICKING.SET_REPACK                                                                         */ 

/*                                                                                                                     */ 

/*  DESCRIPTION:        Modifies Order Header and Move Task Data To Use Core Repacking Functionality                   */

/*                                                                                                                     */ 

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */ 

/*  =======     ===========     ==     ===================================================================             */ 

/*    1         2019-10-15      KS     Initial Version                                                                 */ 

/*                                                                                                                     */  

/***********************************************************************************************************************/  

/**********************GRANTS**************************

grant select on move_task to TORQUE;

grant update on order_header to TORQUE;
grant update on move_task to TORQUE;

grant execute on liberror to TORQUE;

******************************************************/

    FUNCTION set_repack (
        p_client_id       VARCHAR2,
        p_consignment     VARCHAR2,
        p_ship_location   VARCHAR2,
        p_pack_location   VARCHAR2
    ) RETURN NUMBER AS
        PRAGMA autonomous_transaction;
        v_client_check NUMBER := 1;
    BEGIN
        FOR x IN (
            SELECT
                key
            FROM
                dcsdba.move_task
            WHERE
                client_id = upper(p_client_id)
                AND consignment = upper(p_consignment)
                AND task_type = 'O'
                AND status IN (
                    'Released',
                    'Hold'
                )
                AND to_loc_id = upper(p_ship_location)
        ) LOOP UPDATE dcsdba.move_task
        SET
            to_loc_id = upper(p_pack_location)
        WHERE
            key = x.key;

        END LOOP;

        UPDATE dcsdba.order_header
        SET
            repack = 'Y',
            repack_loc_id = upper(p_pack_location)
        WHERE
            client_id = upper(p_client_id)
            AND consignment = upper(p_consignment)
            AND repack_loc_id IS NULL
            AND ship_dock = upper(p_ship_location);

        COMMIT;
        RETURN 1;
    EXCEPTION
        WHEN OTHERS THEN
            ROLLBACK;
            dcsdba.liberror.writeerrorlog('JDA_PICKING.SET_REPACK', sqlcode, sqlerrm);
            RETURN 0;
    END set_repack;

/***********************************************************************************************************************/ 

/*                                                                                                                     */ 

/*  NAME:               JDA_PICKING.SET_BULK_PICK                                                                      */ 

/*                                                                                                                     */ 

/*  DESCRIPTION:        Modifies Order Header and Move Task Data To Use Bulk Picking Functionality                     */

/*                                                                                                                     */ 

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */ 

/*  =======     ===========     ==     ===================================================================             */ 

/*    1         2019-10-24      KS     Initial Version                                                                 */ 

/*                                                                                                                     */  

/***********************************************************************************************************************/  

/**********************GRANTS**************************

grant select on move_task to TORQUE;

grant update on order_header to TORQUE;
grant update on move_task to TORQUE;

grant execute on liberror to TORQUE;

******************************************************/

    FUNCTION set_bulk_pick (
        p_client_id       VARCHAR2,
        p_consignment     VARCHAR2,
        p_ship_location   VARCHAR2
    ) RETURN NUMBER AS
        PRAGMA autonomous_transaction;
        v_client_check NUMBER := 1;
    BEGIN
        FOR x IN (
            SELECT
                key
            FROM
                dcsdba.move_task
            WHERE
                client_id = upper(p_client_id)
                AND consignment = upper(p_consignment)
                AND task_type = 'O'
                AND status IN (
                    'Released',
                    'Hold'
                )
                AND to_loc_id = upper(p_ship_location)
        ) LOOP UPDATE dcsdba.move_task
        SET
            to_loc_id = p_client_id || 'BULK'
        WHERE
            key = x.key;

        END LOOP;

        UPDATE dcsdba.order_header
        SET
            stage_route_id = 'BULK-PICK'
        WHERE
            client_id = upper(p_client_id)
            AND consignment = upper(p_consignment)
            AND stage_route_id IS NULL
            AND ship_dock = upper(p_ship_location);

        COMMIT;
        RETURN 1;
    EXCEPTION
        WHEN OTHERS THEN
            ROLLBACK;
            dcsdba.liberror.writeerrorlog('JDA_PICKING.SET_BULK_PICK', sqlcode, sqlerrm);
            RETURN 0;
    END set_bulk_pick;




/***********************************************************************************************************************/ 

/*                                                                                                                     */ 

/*  NAME:               JDA_PICKING.COMPLETE_KITTING                                                                   */ 

/*                                                                                                                     */ 

/*  DESCRIPTION:        Completes and Marshalls All Kitting Tasks For Client/Order/Location                            */

/*                                                                                                                     */ 

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */ 

/*  =======     ===========     ==     ===================================================================             */ 

/*    1         2019-11-22      KS     Initial Version                                                                 */ 

/*                                                                                                                     */  

/***********************************************************************************************************************/  

/**********************GRANTS**************************

grant select on move_task to TORQUE;
grant select on order_line to TORQUE;

grant update on move_task to TORQUE;

grant execute on libsession to TORQUE;
grant execute on libkitting to TORQUE;
grant execute on liberror to TORQUE;

******************************************************/

    FUNCTION complete_kitting (
        p_site_id        VARCHAR2,
        p_client_id      VARCHAR2,
        p_order_id       VARCHAR2,
        p_kit_location   VARCHAR2,
        p_user_id        VARCHAR2,
        p_station_id     VARCHAR2
    ) RETURN NUMBER IS
        PRAGMA autonomous_transaction;
        v_result INTEGER;
    BEGIN
        dcsdba.libsession.initialisesession(p_user_id, 'INT-API', p_station_id, 'INT-API');
        FOR x IN (
            SELECT
                mt.qty_to_move,
                ol.client_id,
                ol.sku_id,
                mt.tag_id,
                mt.pallet_config,
                mt.config_id,
                ol.order_id,
                ol.line_id,
                ol.owner_id
            FROM
                dcsdba.order_line   ol
                INNER JOIN dcsdba.move_task    mt ON ( ol.client_id = mt.client_id
                                                    AND ol.order_id = mt.task_id
                                                    AND ol.line_id = mt.line_id )
            WHERE
                ol.client_id = p_client_id
                AND ol.order_id = p_order_id
                AND mt.task_type = 'K'
                AND mt.from_loc_id = p_kit_location
                AND mt.site_id = p_site_id
                AND mt.status = 'Released'
        ) LOOP
            dcsdba.libkitting.buildcompletekits(p_result => v_result, p_numkits => x.qty_to_move, --MOVE TASK QTY
             p_clientid => x.client_id, p_kitskuid => x.sku_id, p_tagid => x.tag_id,
                              p_tagcopies => 0, p_palletconfig => x.pallet_config, p_packconfig => x.config_id, p_batchid => NULL
                              , p_expirydate => NULL,
                              p_taskid => x.order_id, p_lineid => x.line_id, p_kitlocation => p_kit_location, p_siteid => p_site_id
                              , p_ownerid => x.owner_id);

            COMMIT;
        END LOOP;

        UPDATE dcsdba.move_task
        SET
            status = 'Complete',
            user_id = p_user_id,
            station_id = p_station_id
        WHERE
            task_id = p_order_id
            AND client_id = p_client_id
            AND from_loc_id = p_kit_location
            AND task_type = 'O'
            AND status = 'Released';

        COMMIT;
        IF v_result is null then v_result := 0; end if;
        RETURN v_result;
            EXCEPTION
        WHEN OTHERS THEN
            ROLLBACK;
            dcsdba.liberror.writeerrorlog('JDA_PICKING.COMPLETE_KITTING', sqlcode, sqlerrm);
            RETURN 0;
    END complete_kitting;
   
-- =============================================
--  JDA_PICKING.TQ_ORDER_GROUPING
-- =============================================
--  Author:      Christian Haslam
--  Create date: 10/08/2020
--  Description: Returns a list of fully allocated/unconsigned orders that fit into the given criteria
--
--  Parameters:
--      * OPTIONAL
--      p_Client                - client id
--      p_MinUnits              - minimum amount of units required on an order for it to be grouped
--      p_MaxUnits              - maximum amount of units required on an order for it to be grouped
--      p_ConsigsToBeCreated    - maximum amount of consignments to be created upon running the order grouping rule
--      p_OrdersPerConsig       - maximum amount of orders per consignment (should match order group building in wms)
--      p_MinOrdersReqPerConsig - minimum amount of orders that a consignment requires for the consignment to be created
--      p_OrderTypes            - order types of the orders
--      *p_WorkZoneCount        - maximum amount of work zones that the order is allocated across
--      *p_WorkZones            - work zones that the order could be allocated within
--      *p_CustomWhereSQL       - custom SQL so that can run specific rules for edge case scenarios
--      *p_CustomHavingSQL      - same as above but for aggregation SQL
--      *p_UpdateTable          - table will be updated
--      *p_UpdateSQL            - columns and values that are to be updated
--      *P_OrderByOpt           - option of how to order the orders
--
--  Returns:
--      v_Orders    - List of Orders
--
--  Change History:
--      02/10/2020   - adding in updateTable and updateSQL parameters
--      24/11/2020   - add ability to order by order date or by pick sequence
--		22/02/2021   - corrected way to join on move tasks to ensure multiple move tasks don't sku the totals
--
--  Grants:
--      grant select on dcsdba.ORDER_HEADER to TORQUE;
--      grant select on dcsdba.ORDER_LINE to TORQUE;
function TQ_ORDER_GROUPING
(
    p_Client varchar2,
    p_MinUnits number,
    p_MaxUnits number,
    p_ConsigsToBeCreated number,
    p_OrdersPerConsig number,
    p_MinOrdersReqPerConsig number,
    p_OrderTypes varchar2,
    p_WorkZoneCount number default 0,
    p_WorkZones varchar2 default '',
    p_CustomWhereSQL varchar2 default '',
    p_CustomHavingSQL varchar2 default '',
    p_UpdateTable varchar2 default '',
    p_UpdateSQL varchar2 default '',
    p_OrderByOpt number default 0
)
return coll_Orders -- custom type defined in JDA_PICKING package to return a collection of orders
-- run below line if coll_Orders type doesn't exist
-- create or replace type coll_Orders is table of varchar2(30);
is pragma AUTONOMOUS_TRANSACTION;
v_Orders coll_Orders;
v_Remainder number(3);
v_OrderToIgnoreFrom number(4);
v_MaxOrders number(5):= p_ConsigsToBeCreated * p_OrdersPerConsig;
v_WorkZoneSQL varchar2(750):= p_WorkZones;
v_SQL varchar2(2500);
v_UpdateSQL varchar2(1000);
v_OrderSQL varchar2(250);

begin
    if length(p_UpdateTable) > 0 then  
        v_UpdateSQL:=   '   update dcsdba.' || p_UpdateTable || ' 
                            set '|| p_UpdateSQL || '
                            where client_id = '''||p_Client||'''
                            and order_id = ''';
    end if;

    if p_WorkZoneCount > 0
        then v_WorkZoneSQL:= '  and oh.order_id in 
                                (
                                    select task_id from dcsdba.MOVE_TASK where client_id = '''||p_Client||''' and task_type = ''O'' group by task_id having count(distinct work_zone) <= '||p_WorkZoneCount||'
                                )
                                and oh.order_id not in
                                (
                                    select task_id from dcsdba.MOVE_TASK where client_id = '''||p_Client||''' and task_type = ''O'' and work_zone not in ('||p_WorkZones||')
                                ) ';
    end if;

    -- order by options, can be an expanding list
    if p_OrderByOpt = 0
      then v_OrderSQL:= ' order by coalesce(oh.order_date, oh.creation_date) ';
   end if;

   if p_OrderByOpt = 1
      then v_OrderSQL:= ' order by coalesce(max(mt.sequence), 999999) ';
   end if;

    v_SQL := '  select oh.order_id
                from dcsdba.ORDER_HEADER oh
                inner join dcsdba.ORDER_LINE ol on ol.order_id = oh.order_id and ol.client_id = oh.client_id
                left join 
				(
					select task_id
					, max(sequence) sequence
					from dcsdba.MOVE_TASK
					where client_id = '''||p_Client||'''
					group by task_id
				) mt on mt.task_id = oh.order_id
                where oh.consignment is null
                and oh.order_type in ('||p_OrderTypes||')                
                and oh.status in (''Allocated'') '
                ||p_CustomWhereSQL||
                ' and oh.client_id = '''||p_Client||''' '
                ||v_WorkZoneSQL||
                ' group by oh.order_id
                , oh.order_date
                , oh.creation_date
                having sum(ol.qty_ordered) - sum(ol.qty_tasked) = 0 
                and sum(ol.qty_ordered) between '||p_MinUnits||' and '||p_MaxUnits||' '
                ||p_CustomHavingSQL|| ' '
                ||v_OrderSQL||
                ' fetch first '||v_MaxOrders||' row only';

    execute immediate v_SQL bulk collect into v_Orders;

    -- if orders amount is same as max orders then return out of the function
    if v_Orders.count = v_MaxOrders then
        -- update orders if values passed in
        if length(p_UpdateTable) > 0 then
            for i in 1..v_Orders.count
            loop
                execute immediate v_UpdateSQL || v_Orders(i) || '''';
            end loop;
            commit;
        end if;

        return v_Orders;
    end if;

    -- check the size of the last consignments worth of orders
    v_Remainder:= mod(v_Orders.count, p_OrdersPerConsig);

    -- return out of the function if remainder is higher than min orders per consignment specified
    if v_Remainder >= p_MinOrdersReqPerConsig then
        -- update orders if values passed in
        if length(p_UpdateTable) > 0 then
            for i in 1..v_Orders.count
            loop
                execute immediate v_UpdateSQL || v_Orders(i) || '''';
            end loop;
            commit;
        end if;

        return v_Orders;

    else
        v_OrderToIgnoreFrom:= v_Orders.count - v_Remainder + 1;
        -- Remove the last consignments worth of orders from the collection if remainder is lower than min orders per consignment specified
        v_Orders.delete(v_OrderToIgnoreFrom, v_Orders.count);
        -- update orders if values passed in
        if length(p_UpdateTable) > 0 then
            for i in 1..v_Orders.count
            loop
                execute immediate v_UpdateSQL || v_Orders(i) || '''';
            end loop;
            commit;
        end if;
        return v_Orders;
    end if;
end TQ_ORDER_GROUPING;


/***********************************************************************************************************************/ 

/*                                                                                                                     */ 

/*  NAME:               JDA_PICKING.TQ_MW_SORTER_BATCH_GROUPING                                                        */ 

/*                                                                                                                     */ 

/*  DESCRIPTION:        MW Specific grouping rules for a super batch                                                   */

/*                                                                                                                     */ 

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */ 

/*  =======     ===========     ==     ===================================================================             */ 

/*    1         2020-08-12      CH     Initial Version                                                                 */ 

/*                                                                                                                     */  

/***********************************************************************************************************************/  

/**********************GRANTS**************************

grant select on dcsdba.ORDER_HEADER to TORQUE;
grant select on dcsdba.ORDER_LINE to TORQUE;
grant select on dcsdba.SKU to TORQUE;

******************************************************/

/**********************DEPENDENCIES********************

-- TORQUE.MW_WEB_PRIORITIES
CREATE TABLE TORQUE.MW_WEB_PRIORITIES
(	
    "PRIORITY_CODE" NUMBER NOT NULL, 
    "PRIORITY_RANK" NUMBER NOT NULL, 
    CONSTRAINT "MW_WEB_PRIORITIES_PK" PRIMARY KEY ("PRIORITY_CODE")  
)

******************************************************/
function TQ_MW_SORTER_BATCH_GROUPING (
    p_Phase2MinUnits int,
    p_Phase2MaxUnits int,
    p_Phase2Orders int,
    p_StandardMinUnits int,
    p_StandardMaxUnits int,
    p_StandardOrders int,
    p_LargeOrders int,
    p_UglyOrders int,
    p_IncludeSingles varchar2,
    p_PriorityOnly char default 'N' 
)
return coll_Orders is
    v_OrderIds coll_Orders;
    v_LargeUnits int := p_StandardMaxUnits + 1;
    v_SingleBlocker int := 1;
    v_Priority int := 1;
begin

    if p_IncludeSingles != 'Y' then
        v_SingleBlocker := 2;
    end if;
    
    if p_PriorityOnly = 'Y' then
        v_Priority := 2;
    else 
        v_Priority := 9999;
    end if;
    
    with t1 as
    (
        select oh.order_id
        , oh.priority
        , oh.order_date
        , min(coalesce(s.ugly, 'N')) minUgly
        , max(coalesce(s.ugly, 'N')) maxUgly
        , sum(ol.qty_ordered) unitsOrdered
        , coalesce(tq.priority_rank,999) priority_rank
        from dcsdba.ORDER_HEADER oh
        inner join dcsdba.ORDER_LINE ol on ol.order_id = oh.order_id and ol.client_id = oh.client_id
        inner join dcsdba.SKU s on s.sku_id = ol.sku_id and s.client_id = ol.client_id
        inner join torque.MW_WEB_PRIORITIES tq on tq.priority_code = oh.priority -- inner join means priority must exist
        where oh.client_id = 'MOUNTAIN'                
        and oh.status = 'Allocated'
        and oh.from_site_id = 'BD2'
        and oh.consignment is null
        and oh.order_type = 'WEB'
        and tq.priority_rank <= v_Priority
        group by oh.order_id
        , oh.priority
        , oh.order_date
        , tq.priority_rank
        having sum(ol.qty_ordered) >= v_SingleBlocker -- doesn't look for single orders if 'N' specified
        and min(coalesce(s.ugly, 'N')) = 'N' -- Guarantees at least one non-ugly is a part of the order
        and sum(ol.qty_ordered) = sum(coalesce(ol.qty_tasked,0)) -- No shortages
    )
    select order_id
    bulk collect into v_OrderIds
    from
    (
        -- Phase 2 Orders
            select order_id
            from
            (
                select order_id
                from t1    
                where unitsOrdered between p_Phase2MinUnits and p_Phase2MaxUnits
                and minUgly = maxUgly
                order by priority_rank, order_date
                fetch first p_Phase2Orders rows only
            )            
        -- End Phase 2 Orders
            union all
        -- Single chute orders; All Non Ugly;
            select order_id
            from
            (
                select order_id
                from t1    
                where unitsOrdered between p_StandardMinUnits and p_StandardMaxUnits
                and minUgly = maxUgly
                order by priority_rank, order_date
                fetch first p_StandardOrders rows only
            )
        -- End Single chute orders; All Non Ugly;
            union all
        -- Orders more than 10 units; All Non-Ugly
            select order_id
            from
            (
                select order_id
                from t1    
                where unitsOrdered > v_LargeUnits
                and minUgly = maxUgly
                order by priority_rank, order_date
                fetch first p_LargeOrders rows only
            )
        -- End Orders more than 10 units; All Non-Ugly
            union all
        -- All single unit orders
            select order_id
            from t1    
            where unitsOrdered = 1
            and minUgly = 'N'
        -- End All single unit orders
            union all
        -- Orders of mixed uglies and non-uglies
            select order_id
            from
            (
                select order_id
                from t1    
                where minUgly <> maxUgly
                order by priority_rank, order_date
                fetch first p_UglyOrders rows only
            )
        -- End Orders of mixed uglies and non-uglies
    );

    return v_OrderIds;

end TQ_MW_SORTER_BATCH_GROUPING;


END jda_picking;

/

  GRANT EXECUTE ON "TORQUE"."JDA_PICKING" TO "DCSRPT";
  GRANT EXECUTE ON "TORQUE"."JDA_PICKING" TO "DCSDBA";
--------------------------------------------------------
--  DDL for Package Body JDA_INBOUND
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "TORQUE"."JDA_INBOUND" AS

/***********************************************************************************************************************/ 

/*                                                                                                                     */ 

/*  NAME:               JDA_INBOUND.CAPTURE_SUPPLIER_SKU                                                               */ 

/*                                                                                                                     */ 

/*  DESCRIPTION:        This Function Updates And Maintains Supplier SKU Records For A SKU                             */ 

/*                                                                                                                     */ 

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */ 

/*  =======     ===========     ==     ===================================================================             */ 

/*    1         2019-08-05      KS     Initial Version                                                                 */ 

/*                                                                                                                     */  

/***********************************************************************************************************************/  

/**********************GRANTS**************************

grant select on sku to TORQUE;
grant select on supplier_sku to TORQUE;

grant update on supplier_sku to TORQUE;

grant delete on supplier_sku to TORQUE;

grant insert on supplier_sku to TORQUE;

grant execute on liberror to TORQUE;

******************************************************/

    FUNCTION capture_supplier_sku (
        p_client_id         VARCHAR2,
        p_sku_id            VARCHAR2,
        p_supplier_sku_id   VARCHAR2,
        p_single_record     NUMBER
    ) RETURN NUMBER IS
        PRAGMA autonomous_transaction;
        v_supplier_sku_count   NUMBER;
        v_sku_count            NUMBER;
        exception_sku_id_invalid EXCEPTION;
    BEGIN

    --Check Supplier SKU Given Is Not NULL
        IF p_supplier_sku_id IS NOT NULL THEN

    --Check if the CLIENT/SKU Is Valid
            SELECT
                COUNT(*)
            INTO v_sku_count
            FROM
                dcsdba.sku
            WHERE
                client_id = p_client_id
                AND sku_id = p_sku_id;


    --IF SKU is invalid then raise Exception
            IF v_sku_count = 0 THEN
                RAISE exception_sku_id_invalid;
            END IF;

	--Check if the Supplier SKU Already Exists for the Client/SKU
            SELECT
                COUNT(*)
            INTO v_supplier_sku_count
            FROM
                dcsdba.supplier_sku
            WHERE
                client_id = p_client_id
                AND sku_id = p_sku_id
                AND supplier_sku_id = p_supplier_sku_id;

	--If the Single Record Flag is Set And The Supplier SKU doesnt already exist then delete all current supplier SKUs

            IF p_single_record = 1 AND v_supplier_sku_count = 0 THEN
                DELETE FROM dcsdba.supplier_sku
                WHERE
                    sku_id = p_sku_id
                    AND client_id = p_client_id;

            END IF;

	--If the supplier SKU does not already exist then insert it into the table

            IF v_supplier_sku_count = 0 THEN
                INSERT INTO dcsdba.supplier_sku (
                    client_id,
                    sku_id,
                    supplier_sku_id,
                    vm_sku
                ) VALUES (
                    p_client_id,
                    p_sku_id,
                    p_supplier_sku_id,
                    'N'
                );

            END IF;

            COMMIT;
            RETURN 1;
        ELSE
            RETURN 1;
        END IF;
    EXCEPTION
        WHEN exception_sku_id_invalid THEN
            ROLLBACK;
            RETURN 0;
        WHEN OTHERS THEN
            ROLLBACK;
            dcsdba.liberror.writeerrorlog('JDA_INBOUND.CAPTURE_SUPPLIER_SKU', sqlcode, sqlerrm);
            RETURN 0;
    END capture_supplier_sku;

/***********************************************************************************************************************/ 

/*                                                                                                                     */ 

/*  NAME:               JDA_INBOUND.PUTAWAY_RECREATE                                                                   */ 

/*                                                                                                                     */ 

/*  DESCRIPTION:        Checks If A Putaway Task Exists For A Tag And Creates It If Not                                */ 

/*                                                                                                                     */ 

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */ 

/*  =======     ===========     ==     ===================================================================             */ 

/*    1         2019-08-15      KS     Initial Version                                                                 */ 

/*    2         2019-08-22      KS     Amended to only create putaways for tasks on Receipt Docks / Sampling Locs      */ 

/*    3         2019-09-20      KS     Added functionality to specify a final location + QTY for putaway               */ 

/*    4         2019-09-24      KS     Added Code To Verify Volumetrics/Weights When Specifying final Location         */ 

/*                                                                                                                     */  

/***********************************************************************************************************************/  

/**********************GRANTS**************************

grant select on move_task_pk_seq to TORQUE;
grant select on move_task to TORQUE;
grant select on inventory to TORQUE;
grant select on location to TORQUE;
grant select on sku to TORQUE;     

grant insert on move_task to TORQUE;                

grant execute on liberror to TORQUE;  
grant execute on libmovetask to TORQUE;

******************************************************/

    FUNCTION putaway_recreate (
        p_site_id        VARCHAR2,
        p_client_id      VARCHAR2,
        p_tag_id         VARCHAR2,
        p_from_loc_id    VARCHAR2,
        p_final_loc_id   VARCHAR2 DEFAULT NULL,
        p_qty            NUMBER DEFAULT 0
    ) RETURN NUMBER AS

        PRAGMA autonomous_transaction;
        v_valid_count          INTEGER;
        v_existing_count       INTEGER;
        v_mt_key               NUMBER;
        v_sku_id               VARCHAR2(50);
        v_config_id            VARCHAR2(20);
        v_description          VARCHAR2(150);
        v_condition_id         VARCHAR2(20);
        v_origin_id            VARCHAR2(20);
        v_owner_id             VARCHAR2(20);
        v_work_zone            VARCHAR2(30);
        v_qty                  NUMBER;
        v_to_loc_id            VARCHAR2(30);
        v_dc_ret               NUMBER;
        v_alloc_volume         NUMBER;
        v_current_volume       NUMBER;
        v_alloc_weight         NUMBER;
        v_current_weight       NUMBER;
        v_old_alloc_volume     NUMBER;
        v_old_current_volume   NUMBER;
        v_old_alloc_weight     NUMBER;
        v_old_current_weight   NUMBER;
    BEGIN
    --Calculate the Next To Location (For Staging Purposes etc.)
        IF upper(p_final_loc_id) IS NOT NULL THEN
            v_to_loc_id := dcsdba.libmovetask.getmovetasknextlocation(upper(p_site_id), upper(p_from_loc_id), upper(p_final_loc_id
            ), upper(p_client_id), NULL,
                   NULL, NULL, 'P', NULL, NULL,
                   NULL, NULL, NULL, NULL, NULL);
        END IF;


--Check if Putaway Task Exists For Tag

        SELECT
            COUNT(*)
        INTO v_existing_count
        FROM
            dcsdba.move_task
        WHERE
            task_type = 'P'
            AND site_id = upper(p_site_id)
            AND client_id = upper(p_client_id)
            AND tag_id = upper(p_tag_id)
            AND from_loc_id = upper(p_from_loc_id)
            AND pallet_id IS NULL
            AND container_id IS NULL;

--No Existing Tasks Have Been Found, So One Is Required If Inventory Is Valid

        IF v_existing_count = 0 THEN

    --Validate the Inventory Exists For The Tag and that it is on a receipt dock / sampling location
            SELECT
                COUNT(*)
            INTO v_valid_count
            FROM
                dcsdba.inventory   i
                INNER JOIN dcsdba.location    l ON ( i.site_id = l.site_id
                                                  AND i.location_id = l.location_id )
            WHERE
                i.site_id = upper(p_site_id)
                AND i.client_id = upper(p_client_id)
                AND i.tag_id = upper(p_tag_id)
                AND i.location_id = upper(p_from_loc_id)
                AND i.pallet_id IS NULL
                AND i.container_id IS NULL
                AND i.qty_allocated = 0
                AND l.loc_type IN (
                    'Receive Dock',
                    'Sampling'
                );

--Inventory Is Valid So We Need To Create Putaway Task

            IF v_valid_count > 0 THEN

--Fetch The Next Move Task Key From The Sequence
                v_mt_key := dcsdba.move_task_pk_seq.nextval;

--Find Values Required For Putaway Task
                SELECT
                    i.sku_id,
                    i.config_id,
                    sku.description,
                    i.condition_id,
                    i.origin_id,
                    i.owner_id,
                    i.qty_on_hand,
                    l.work_zone
                INTO
                    v_sku_id,
                    v_config_id,
                    v_description,
                    v_condition_id,
                    v_origin_id,
                    v_owner_id,
                    v_qty,
                    v_work_zone
                FROM
                    dcsdba.inventory   i
                    INNER JOIN dcsdba.sku ON ( i.client_id = sku.client_id
                                               AND i.sku_id = sku.sku_id )
                    INNER JOIN dcsdba.location    l ON ( i.site_id = l.site_id
                                                      AND i.location_id = l.location_id )
                WHERE
                    i.tag_id = upper(p_tag_id)
                    AND i.location_id = upper(p_from_loc_id)
                    AND i.site_id = upper(p_site_id)
                    AND i.client_id = upper(p_client_id);

    --Set QTY To Parameter If Given

                IF p_qty > 0 AND p_qty <= v_qty THEN
                    v_qty := p_qty;
                END IF;

--Insert Putaway Task Into Move Task Table
                INSERT INTO dcsdba.move_task (
                    key,
                    first_key,
                    task_type,
                    task_id,
                    line_id,
                    client_id,
                    sku_id,
                    config_id,
                    description,
                    tag_id,
                    old_tag_id,
                    customer_id,
                    origin_id,
                    condition_id,
                    qty_to_move,
                    old_qty_to_move,
                    site_id,
                    from_loc_id,
                    old_from_loc_id,
                    to_loc_id,
                    old_to_loc_id,
                    final_loc_id,
                    owner_id,
                    sequence,
                    status,
                    list_id,
                    dstamp,
                    start_dstamp,
                    finish_dstamp,
                    original_dstamp,
                    priority,
                    consol_link,
                    face_type,
                    face_key,
                    work_zone,
                    work_group,
                    consignment,
                    bol_id,
                    reason_code,
                    container_id,
                    to_container_id,
                    pallet_id,
                    to_pallet_id,
                    to_pallet_config,
                    to_pallet_volume,
                    to_pallet_height,
                    to_pallet_depth,
                    to_pallet_width,
                    to_pallet_weight,
                    pallet_grouped,
                    pallet_config,
                    pallet_volume,
                    pallet_height,
                    pallet_depth,
                    pallet_width,
                    pallet_weight,
                    user_id,
                    station_id,
                    session_type,
                    summary_record,
                    repack,
                    kit_sku_id,
                    kit_line_id,
                    kit_ratio,
                    kit_link,
                    status_link,
                    due_type,
                    due_task_id,
                    due_line_id,
                    trailer_position,
                    consolidated_task,
                    disallow_tag_swap,
                    ce_under_bond,
                    increment_time,
                    estimated_time,
                    uploaded_labor,
                    print_label_id,
                    print_label,
                    old_status,
                    repack_qc_done,
                    old_task_id,
                    catch_weight,
                    moved_lock_status,
                    pick_realloc_flag,
                    stage_route_id,
                    stage_route_sequence,
                    labelling,
                    pf_consol_link,
                    inv_key,
                    first_pick,
                    serial_number,
                    label_exceptioned,
                    shipment_group,
                    deconsolidate,
                    kit_plan_id,
                    plan_sequence,
                    to_container_config,
                    container_config,
                    ce_rotation_id,
                    ce_avail_status,
                    rdt_user_mode,
                    consol_run_num,
                    labor_assignment,
                    list_to_pallet_id,
                    list_to_container_id,
                    labor_grid_sequence,
                    trolley_slot_id,
                    processing,
                    move_whole,
                    user_def_match_blank_1,
                    user_def_match_blank_2,
                    user_def_match_blank_3,
                    user_def_match_blank_4,
                    user_def_match_blank_5,
                    user_def_match_blank_6,
                    user_def_match_blank_7,
                    user_def_match_blank_8,
                    user_def_match_chk_1,
                    user_def_match_chk_2,
                    user_def_match_chk_3,
                    user_def_match_chk_4,
                    logging_level,
                    last_held_user,
                    last_released_user,
                    last_held_workstation,
                    last_released_workstation,
                    last_held_reason_code,
                    last_released_reason_code,
                    last_held_date,
                    last_released_date,
                    spec_code,
                    full_pallet_cluster,
                    shipping_unit
                ) VALUES (
                    v_mt_key,
                    v_mt_key,
                    'P',
                    'PUTAWAY',
                    NULL,
                    upper(p_client_id),
                    v_sku_id,
                    v_config_id,
                    v_description,
                    upper(p_tag_id),
                    upper(p_tag_id),
                    NULL,
                    v_origin_id,
                    v_condition_id,
                    v_qty,
                    v_qty,
                    upper(p_site_id),
                    upper(p_from_loc_id),
                    upper(p_from_loc_id),
                    (
                        CASE
                            WHEN v_to_loc_id IS NOT NULL
                                 AND p_final_loc_id IS NOT NULL THEN
                                upper(v_to_loc_id)
                            ELSE
                                NULL
                        END
                    ),
                    (
                        CASE
                            WHEN v_to_loc_id IS NOT NULL
                                 AND p_final_loc_id IS NOT NULL THEN
                                upper(v_to_loc_id)
                            ELSE
                                NULL
                        END
                    ),
                    (
                        CASE
                            WHEN v_to_loc_id IS NOT NULL
                                 AND p_final_loc_id IS NOT NULL THEN
                                upper(p_final_loc_id)
                            ELSE
                                NULL
                        END
                    ),
                    v_owner_id,
                    10,
                    'Released',
                    NULL,
                    systimestamp,
                    systimestamp,
                    systimestamp,
                    systimestamp,
                    50,
                    NULL,
                    NULL,
                    NULL,
                    v_work_zone,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    'M',
                    'Y',
                    'N',
                    NULL,
                    NULL,
                    1,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    'N',
                    NULL,
                    NULL,
                    'N',
                    NULL,
                    'N',
                    NULL,
                    'N',
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    0,
                    'N',
                    NULL,
                    NULL,
                    'N',
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    'XX/D00000001',
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    NULL,
                    'N'
                );

        --If To Location Is Populated Then DataCheck It To Correct Volumes/Weights Etc.

                IF v_to_loc_id IS NOT NULL THEN
                    dcsdba.libdatacheck.calculatelocationvalues(v_dc_ret, 1, 0, p_site_id, upper(v_to_loc_id),
                                        v_alloc_volume, v_current_volume, v_alloc_weight, v_current_weight, v_old_alloc_volume,
                                        v_old_current_volume, v_old_alloc_weight, v_old_current_weight);
                END IF;
        
        --If Final Location Is Populated And Different to the To Location the Also DataCheck The Final Location

                IF p_final_loc_id IS NOT NULL AND upper(p_final_loc_id) != upper(v_to_loc_id) THEN
                    dcsdba.libdatacheck.calculatelocationvalues(v_dc_ret, 1, 0, p_site_id, upper(p_final_loc_id),
                                        v_alloc_volume, v_current_volume, v_alloc_weight, v_current_weight, v_old_alloc_volume,
                                        v_old_current_volume, v_old_alloc_weight, v_old_current_weight);
                END IF;

                COMMIT;
                RETURN 1;
            ELSE
--No Inventory Found For Tag - Return 1
                RETURN 1;
            END IF;
--Putaway Task Has Been Found So No Need To Create One - Return 1

        ELSE
            RETURN 1;
        END IF;

    EXCEPTION
        WHEN OTHERS THEN
            ROLLBACK;
            dcsdba.liberror.writeerrorlog('JDA_INBOUND.PUTAWAY_RECREATE', sqlcode, sqlerrm);
            RETURN 0;
    END putaway_recreate;

/***********************************************************************************************************************/ 

/*                                                                                                                     */ 

/*  NAME:               JDA_INBOUND.FIND_TAG                                                                           */ 

/*                                                                                                                     */ 

/*  DESCRIPTION:        Returns a Tag ID Using Site, Client, SKU And Location                                          */ 

/*                                                                                                                     */ 

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */ 

/*  =======     ===========     ==     ===================================================================             */ 

/*    1         2019-08-22      KS     Initial Version                                                                 */ 

/*                                                                                                                     */  

/***********************************************************************************************************************/  

/**********************GRANTS**************************

grant select on inventory to TORQUE;
grant select on location to TORQUE;
grant select on sku to TORQUE;

******************************************************/

    FUNCTION find_tag (
        p_site_id       VARCHAR2,
        p_client_id     VARCHAR2,
        p_sku_id        VARCHAR2,
        p_location_id   VARCHAR2
    ) RETURN VARCHAR2 IS
        PRAGMA autonomous_transaction;
        v_tag_id VARCHAR2(50);
    BEGIN
        SELECT
            MIN(tag_id) AS tag_id
        INTO v_tag_id
        FROM
            dcsdba.inventory i
            INNER JOIN dcsdba.sku ON ( i.client_id = sku.client_id
                                       AND i.sku_id = sku.sku_id )
        WHERE
            i.location_id = p_location_id
            AND ( i.sku_id = upper(p_sku_id)
                  OR sku.ean = upper(p_sku_id)
                  OR sku.upc = upper(p_sku_id) )
            AND i.site_id = upper(p_site_id)
            AND i.client_id = upper(p_client_id);

        RETURN v_tag_id;
    EXCEPTION
        WHEN no_data_found THEN
            RETURN NULL;
        WHEN OTHERS THEN
            dcsdba.liberror.writeerrorlog('JDA_INBOUND.FIND_TAG', sqlcode, sqlerrm);
            RETURN NULL;
    END find_tag;

/***********************************************************************************************************************/ 

/*                                                                                                                     */ 

/*  NAME:               JDA_INBOUND.FIND_RETURN_LOCATION                                                               */ 

/*                                                                                                                     */ 

/*  DESCRIPTION:        Finds A Putaway Location For Given Inventory                                                   */ 

/*                                                                                                                     */ 

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */ 

/*  =======     ===========     ==     ===================================================================             */ 

/*    1         2019-09-23      KS     Initial Version                                                                 */ 

/*    2         2019-09-24      KS     Removed Functionality To Datacheck Final Location as Redundant                  */ 

/*                                                                                                                     */  

/***********************************************************************************************************************/  

/**********************GRANTS**************************

grant select on move_task to TORQUE;
grant select on location to TORQUE;
grant select on inventory_transaction to TORQUE;

grant delete on move_task to TORQUE;

grant execute on liberror to TORQUE;  
grant execute on libdatacheck to TORQUE;

******************************************************/

    FUNCTION find_return_location (
        p_station_id    VARCHAR2,
        p_user_id       VARCHAR2,
        p_site_id       VARCHAR2,
        p_client_id     VARCHAR2,
        p_zone_1        VARCHAR2,
        p_tag_id        VARCHAR2,
        p_sku_id        VARCHAR2,
        p_location_id   VARCHAR2,
        p_level_ex      VARCHAR2
    ) RETURN NUMBER IS

        PRAGMA autonomous_transaction;
        v_to_location          VARCHAR2(30);
        v_location             VARCHAR2(30);
        v_available            NUMBER := 0;
        v_put_sequence         NUMBER := 0;
        v_ret                  NUMBER;
        tq_fatal_exception EXCEPTION;
        v_dc_ret               NUMBER;
        v_alloc_volume         NUMBER;
        v_current_volume       NUMBER;
        v_alloc_weight         NUMBER;
        v_current_weight       NUMBER;
        v_old_alloc_volume     NUMBER;
        v_old_current_volume   NUMBER;
        v_old_alloc_weight     NUMBER;
        v_old_current_weight   NUMBER;
    BEGIN

--Delete Existing Move Tasks For Tag And Datacheck Any Locations Which Where Destinations For Tasks
        FOR x IN (
            SELECT
                *
            FROM
                dcsdba.move_task
            WHERE
                tag_id = p_tag_id
                AND from_loc_id = p_location_id
                AND site_id = p_site_id
                AND client_id = p_client_id
                AND sku_id = p_sku_id
                AND task_type = 'P'
                AND status IN (
                    'Released',
                    'Hold'
                )
        ) LOOP
    
        --Delete the existing Putaway move tasks for Inventory
            DELETE FROM dcsdba.move_task
            WHERE
                key = x.key;

        --If To Location Is Populated Then DataCheck It To Correct Volumes/Weights Etc.

            IF x.to_loc_id IS NOT NULL THEN
                dcsdba.libdatacheck.calculatelocationvalues(v_dc_ret, 1, 0, x.site_id, x.to_loc_id,
                                    v_alloc_volume, v_current_volume, v_alloc_weight, v_current_weight, v_old_alloc_volume,
                                    v_old_current_volume, v_old_alloc_weight, v_old_current_weight);

            END IF;

        --If Final Location Is Populated And Different to the To Location the Also DataCheck The Final Location

            IF x.final_loc_id IS NOT NULL AND x.to_loc_id != x.final_loc_id THEN
                dcsdba.libdatacheck.calculatelocationvalues(v_dc_ret, 1, 0, x.site_id, x.final_loc_id,
                                    v_alloc_volume, v_current_volume, v_alloc_weight, v_current_weight, v_old_alloc_volume,
                                    v_old_current_volume, v_old_alloc_weight, v_old_current_weight);
            END IF;

            COMMIT;
        END LOOP;

--select location_id,put_sequence, volume, current_volume from location where location_id = '01IS01A' AND site_id = 'WFD';

        BEGIN
            SELECT
                to_loc_id,
                available_space,
                put_sequence
            INTO
                v_location,
                v_available,
                v_put_sequence
            FROM
                (
                    SELECT
                        itl.to_loc_id,
                        volume - ( current_volume + alloc_volume ) AS available_space,
                        l.put_sequence,
                        ROW_NUMBER() OVER(
                            ORDER BY
                                dstamp DESC
                        ) AS rows1
                    FROM
                        dcsdba.inventory_transaction   itl
                        INNER JOIN dcsdba.location                l ON ( itl.to_loc_id = l.location_id
                                                          AND itl.site_id = l.site_id )
                    WHERE
                        l.lock_status IN (
                            'UnLocked',
                            'OutLocked'
                        )
                        AND l.zone_1 = upper(p_zone_1)
                        AND itl.code = 'Putaway'
                        AND itl.from_loc_id = upper(p_location_id)
                        AND itl.site_id = upper(p_site_id)
                        AND itl.client_id = upper(p_client_id)
                        AND sysdate < itl.dstamp + ( 1 / 96 )
                        AND itl.user_id = upper(p_user_id)
                        AND itl.station_id = upper(p_station_id)
                        AND ( substr(l.location_id, - 1) NOT IN (
                            SELECT
                                regexp_substr(upper(p_level_ex), '[^,]+', 1, level)
                            FROM
                                dual
                            CONNECT BY
                                regexp_substr(upper(p_level_ex), '[^,]+', 1, level) IS NOT NULL
                        )
                              OR p_level_ex IS NULL )
                )
            WHERE
                rows1 = 1;

        EXCEPTION
            WHEN no_data_found THEN
                NULL;
            WHEN OTHERS THEN
                RAISE tq_fatal_exception;
        END;

        dbms_output.put_line(v_location);
        IF v_location IS NOT NULL AND nvl(v_available, 0) <= 0 THEN
            BEGIN
                SELECT
                    location_id,
                    volume - ( current_volume + alloc_volume ) AS available_space,
                    put_sequence
                INTO
                    v_location,
                    v_available,
                    v_put_sequence
                FROM
                    (
                        SELECT
                            l.location_id,
                            l.volume,
                            l.current_volume,
                            l.alloc_volume,
                            l.put_sequence,
                            ROW_NUMBER() OVER(
                                ORDER BY
                                    put_sequence ASC
                            ) AS rows1
                        FROM
                            dcsdba.location l
                        WHERE
                            l.site_id = p_site_id
                            AND ( l.volume - l.current_volume - l.alloc_volume ) > 0
                            AND put_sequence >= to_number(v_put_sequence)
                            AND ( substr(l.location_id, - 1) NOT IN (
                                SELECT
                                    regexp_substr(p_level_ex, '[^,]+', 1, level)
                                FROM
                                    dual
                                CONNECT BY
                                    regexp_substr(p_level_ex, '[^,]+', 1, level) IS NOT NULL
                            )
                                  OR p_level_ex IS NULL )
                            AND l.zone_1 = p_zone_1
                            AND l.lock_status IN (
                                'UnLocked',
                                'OutLocked'
                            )
                        ORDER BY
                            put_sequence
                    )
                WHERE
                    rows1 = 1;

            EXCEPTION
                WHEN no_data_found THEN
                    NULL;
                WHEN OTHERS THEN
                    RAISE tq_fatal_exception;
            END;
        END IF;

        SELECT
            jda_inbound.putaway_recreate(p_site_id, p_client_id, p_tag_id, p_location_id, v_location,
                                         1)
        INTO v_ret
        FROM
            dual;

        --NO LONGER REQUIRED AS PUTAWAY_RECREATE NOW HANDLES LOCATION DATACHECKS        
--        IF v_location is not null then
--            dcsdba.libdatacheck.calculatelocationvalues(v_dc_ret, 1, 0, p_site_id, v_location, v_alloc_volume, v_current_volume, v_alloc_weight
--    
--            , v_current_weight, v_old_alloc_volume, v_old_current_volume, v_old_alloc_weight, v_old_current_weight);
--        END IF;

        dbms_output.put_line(v_ret);
        COMMIT;
        RETURN 1;
    EXCEPTION
        WHEN OTHERS THEN
            dcsdba.liberror.writeerrorlog('JDA_INBOUND.FIND_RETURN_LOCATION', sqlcode, sqlerrm);
            ROLLBACK;
            RETURN 0;
    END find_return_location;

/***********************************************************************************************************************/ 

/*                                                                                                                     */ 

/*  NAME:               JDA_INBOUND.RETURN_API                                                                         */ 

/*                                                                                                                     */ 

/*  DESCRIPTION:        Processes A Return For An Order Line                                                           */ 

/*                                                                                                                     */ 

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */ 

/*  =======     ===========     ==     ===================================================================             */ 

/*    1         2019-09-25      KS     Initial Version                                                                 */ 

/*    2         2019-10-08      KS     Moved XRET Value from USER_DEF_TYPE_1 to USER_DEF_TYPE_3 + Fixed Upload         */ 

/*    3         2020-04-14      KS     Modified To Better Match RDT Process (Scap Flag )                               */ 

/*    4         2020-11-11      CH     Retrieves the Owner id from the initial pick (if non-archive)                   */ 

/*    5         2020-12-09      CH     Defaults all Crew returns to CC2 as per SD/Customer request                     */ 

/*    6         2020-12-15      CH     Updates user def type 1 to COO that is passed in                                */ 

/*                                                                                                                     */  

/***********************************************************************************************************************/  

/**********************GRANTS**************************

grant select on language_text to TORQUE;
grant select on location to TORQUE;
grant select on inventory_transaction to TORQUE;
grant select on sampling_route_action to TORQUE;
grant select on inventory to TORQUE;

grant update on inventory to TORQUE;
grant update on inventory_transaction to TORQUE;
grant update on order_line to TORQUE;
grant update on order_line_archive to TORQUE;

grant delete on move_task to TORQUE;

grant execute on liberror to TORQUE;  
grant execute on libsession to TORQUE;
grant execute on libmergereceipt to TORQUE;
grant execute on LIBSTOCKADJUST to TORQUE;

******************************************************/

    FUNCTION return_api (
        p_site_id         VARCHAR2,
        p_client_id       VARCHAR2,
        p_owner_id        VARCHAR2,
        p_line_id         NUMBER,
        p_sku_id          VARCHAR2,
        p_qty             NUMBER,
        p_location_id     VARCHAR2,
        p_order_id        VARCHAR2,
        p_condition_id    VARCHAR2,
        p_notes           VARCHAR2,
        p_reason_id       VARCHAR2,
        p_user_id         VARCHAR2 DEFAULT 'COURIERAPI',
        p_station_id      VARCHAR2 DEFAULT 'COURIERAPI',
        p_config_id       VARCHAR2 DEFAULT 'P1',
        p_coo             VARCHAR2 DEFAULT NULL,
        p_pallet_config   VARCHAR2 DEFAULT 'MD'
    ) RETURN VARCHAR2 IS

        PRAGMA autonomous_transaction;
        v_zone_1             VARCHAR2(30);
        v_retval             NUMBER;
        v_merge_error        VARCHAR2(10);
        v_merge_error_text   VARCHAR(100);
        v_transaction_id     VARCHAR(30);
        v_status             NUMBER;
        v_success            NUMBER;
        v_update_success     NUMBER;
        v_update_chk1        NUMBER;
        v_update_chk2        NUMBER;
        v_owner              VARCHAR2(20);
        exception_merge_error EXCEPTION;
        exception_unknown_error EXCEPTION;
        exception_invalid_return EXCEPTION;
        exception_failed_ol_update EXCEPTION;
        exception_multi_ol_lines EXCEPTION;
        exception_invalid_location EXCEPTION;
        exception_failed_itl_update EXCEPTION;
        v_sampling_action VARCHAR2(20);
        v_adj_retval    NUMBER;
        v_inv_key NUMBER;
        
    BEGIN
    
    --Check that the Location Being Returned To Exists and Store Zone In Variable
        BEGIN
            SELECT
                zone_1
            INTO v_zone_1
            FROM
                dcsdba.location
            WHERE
                site_id = p_site_id
                AND location_id = p_location_id;

        EXCEPTION
            WHEN no_data_found THEN
                RAISE exception_invalid_location;
        END;

        --Set Up Transaction Session
        dcsdba.libsession.initialisesession(p_user_id, 'COURIERAPI', p_station_id, 'COURIERAPI');
        
        --Process A Receipt Of The Inventory Being Returned (Inventory Is Receipted to Container Due To Issues With Receipts Into Sampling Locations - This Is Corrected Before Commiting)
        v_transaction_id := 'XRET-' || to_char(systimestamp, 'YYMMDDhh24miss');
        
        --Get owner id from original pick; defaults to TORQUE if not found in non-archive itl table
        select coalesce(owner_id, 'TORQUE') into v_owner
        from dual
        left join
        (
        select case when client_id = 'CC' then 'CC2' else owner_id end owner_id
        from dcsdba.INVENTORY_TRANSACTION 
        where reference_id = p_order_id
        and line_id = p_line_id
        and client_id = p_client_id
        and code = 'Pick' 
        and coalesce(update_qty, 0) > 0
        and rownum = 1
        ) x on 1 = 1;
        
        v_retval := dcsdba.libmergereceipt.directreceipt(p_mergeerror => v_merge_error, p_toupdatecols => NULL, p_mergeaction => 'A'
        , p_siteid => p_site_id, p_locationid => 'CONTAINER',
                       p_ownerid => v_owner, p_clientid => p_client_id, p_skuid => p_sku_id, p_configid => p_config_id, p_qtyonhand
                       => p_qty,
                       p_receiptid => p_order_id, p_conditionid => p_condition_id, p_palletconfig => p_pallet_config, p_notes => p_notes
                       , p_userdeftype3 => v_transaction_id);

        IF v_retval = 1 THEN
        DELETE from dcsdba.move_task where from_loc_id = 'CONTAINER' AND task_type = 'P' AND task_id = 'PUTAWAY' AND sku_id = p_sku_id AND site_id  = p_site_id AND client_id = p_client_id;
            
            --Check The Sampling Action For The Return Location
            BEGIN
                select sampling_action into v_sampling_action
                from dcsdba.sampling_route_action
                where site_id = p_site_id
                AND sampling_route = upper(p_location_id)
                AND sampling_action in ('SCRAP')
                AND rownum = 1;
                EXCEPTION
                WHEN no_data_found THEN
                    null;
             END;
            
            --Update Inventory Transaction From Receipt To Return And Also Correct Location Data On the ITL
            UPDATE dcsdba.inventory_transaction
            SET
                code = 'Return',
                line_id = p_line_id,
                reason_id = p_reason_id,
                from_loc_id = p_location_id,
                to_loc_id = p_location_id,
                final_loc_id = NULL,
                uploaded = 'N',
                sampling_type = p_location_id,
                user_def_type_1 = p_coo
                where client_id = p_client_id
                AND site_id = p_site_id
                AND user_def_type_3 = v_transaction_id
                AND sku_id = p_sku_id
                AND reference_id = p_order_id
                AND code = 'Receipt'
                AND from_loc_id = 'CONTAINER';
                
            v_update_chk1 := SQL%rowcount;
            
            --Find The Key Of The Inventory Which Has Been Returned (Used To Adjust Off If Required)
            select key into v_inv_key from dcsdba.inventory
            WHERE
                client_id = p_client_id
                AND site_id = p_site_id
                AND user_def_type_3 = v_transaction_id
                AND sku_id = p_sku_id
                AND location_id = 'CONTAINER'
                AND receipt_id = p_order_id
                AND rownum = 1;

            --Update The Inventory Record with the correct Location, Zone And Sampling Type
            UPDATE dcsdba.inventory
            SET
                location_id = p_location_id,
                zone_1 = v_zone_1,
                sampling_type = p_location_id
            WHERE
                key = v_inv_key;
            v_update_chk2 := SQL%rowcount;
            
            -- If the Sampling Action For This Location is to SCRAP Then Write Off The Inventory
            IF v_sampling_action = 'SCRAP' then
            
            --Write Off Inventory
            DCSDBA.LIBSTOCKADJUST.ADJUSTEXISTINGINVENTORY
            (
            RESULT => v_adj_retval,
            KEYFIELD => v_inv_key,
            NEWQTYONHAND => 0,
            REASONID => 'SCRAP',
            NEWCONFIGID => p_config_id
            );
            END IF;

            --If The Inventory Transaction OR Inventory Record Failed to update then Raise Exception And Rollback
            IF v_update_chk1 = 0 OR v_update_chk2 = 0 THEN
                RAISE exception_failed_itl_update;
            END IF;
            --Update The QTY_Returned Value on the Order Line
            UPDATE dcsdba.order_line
            SET
                qty_returned = nvl(qty_returned, 0) + p_qty
            WHERE
                order_id = p_order_id
                AND client_id = p_client_id
                AND sku_id = p_sku_id
                AND line_id = p_line_id
                AND nvl(qty_returned, 0) + p_qty <= qty_ordered;

            v_update_success := SQL%rowcount;
            --If Updated Order Line Sucessfully Then Commit And Return Success
            IF v_update_success = 1 THEN
                COMMIT;
                RETURN '{ "Success": 1, "MergeError": "", "ErrorMessage": ""}';
            --If Failed to Update Order Line Then Attempt To Update Line In Archive
            ELSIF v_update_success = 0 THEN
                UPDATE dcsdba.order_line_archive
                SET
                    qty_returned = nvl(qty_returned, 0) + p_qty
                WHERE
                    order_id = p_order_id
                    AND client_id = p_client_id
                    AND sku_id = p_sku_id
                    AND line_id = p_line_id
                    AND nvl(qty_returned, 0) + p_qty <= qty_ordered;

                v_update_success := SQL%rowcount;
                --If Updated Order Line Archive Sucessfully Then Commit And Return Success
                IF v_update_success = 1 THEN
                    COMMIT;
                    RETURN '{ "Success": 1, "MergeError": "", "ErrorMessage": ""}';
                --Raise Exceptions Based On Failure Types
                ELSIF v_update_success = 0 THEN
                    RAISE exception_failed_ol_update;
                ELSE
                    RAISE exception_multi_ol_lines;
                END IF;

            ELSE
                RAISE exception_multi_ol_lines;
            END IF;

        ELSIF v_retval = 0 AND v_merge_error IS NOT NULL THEN
            RAISE exception_merge_error;
        END IF;

        ROLLBACK;
        RAISE exception_unknown_error;
    EXCEPTION
        WHEN exception_failed_itl_update THEN
            ROLLBACK;
            RETURN '{ "Success": 0, "MergeError": "", "ErrorMessage": "Internal Error Occured"}';
        WHEN exception_failed_ol_update THEN
            ROLLBACK;
            RETURN '{ "Success": 0, "MergeError": "", "ErrorMessage": "No Valid Order Lines Found"}';
        WHEN exception_multi_ol_lines THEN
            ROLLBACK;
            RETURN '{ "Success": 0, "MergeError": "", "ErrorMessage": "Multiple Order Lines Found"}';
        WHEN exception_merge_error THEN
            ROLLBACK;
            SELECT
                text
            INTO v_merge_error_text
            FROM
                dcsdba.language_text
            WHERE
                label = v_merge_error
                AND language = 'EN_GB';

            RETURN '{ "Success": 0, "MergeError": "'
                   || v_merge_error
                   || '", "ErrorMessage": "'
                   || v_merge_error_text
                   || '"}';
        WHEN exception_unknown_error THEN
            ROLLBACK;
            RETURN '{ "Success": 0, "MergeError": "", "ErrorMessage": "Unknown Exception"}';
        WHEN exception_invalid_location THEN
            ROLLBACK;
            RETURN '{ "Success": 0, "MergeError": "", "ErrorMessage": "Location ID Is Invalid!"}';
        WHEN OTHERS THEN
            ROLLBACK;
            dcsdba.liberror.writeerrorlog('JDA_INBOUND.RETURN_API', sqlcode, sqlerrm);
            RETURN '{ "Success": 0, "MergeError": "", "ErrorMessage": "SQL Exception Occured: Please See Error Table For More Details"}'
            ;
    END return_api;

END jda_inbound;

/

  GRANT EXECUTE ON "TORQUE"."JDA_INBOUND" TO "DCSDBA";
