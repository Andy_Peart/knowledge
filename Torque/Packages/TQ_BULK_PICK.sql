--------------------------------------------------------
--  DDL for Package TQ_BULK_PICK
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "TORQUE"."TQ_BULK_PICK" AS 


/***********************************************************************************************************************/ 

/*                                                                                                                     */ 

/*  NAME:               TQ_BULK_PICK                                                                                   */ 

/*                                                                                                                     */ 

/*  DESCRIPTION:        To Handle the Bulk Picking / Sortation Functionality                                           */ 

/*                                                                                                                     */ 

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */ 

/*  =======     ===========     ==     ===================================================================             */ 

/*    1         2019-07-12      KS     Initial Version                                                                 */ 

/*    2         2019-07-22		KS	   Modified To Support 2 Stage SORT	                                               */

/*    3         2019-07-22		KS     Added PROCESS_STOCK_DIRECT Function	                                           */

/*    4         2019-09-27      KS     Reworked SORTATION_WALL package and renamed to TQ_SORTATION_WALL                */

/*    5         2019-10-01      KS     Reworked Search_Location Function                                               */

/*    6         2019-10-08      KS     Modified PROCESS_STOCK Function                                                 */

/*    7         2019-11-11      KS     Modified PROCESS_STOCK_OUT to ensure TO_LOCATION IS CORRECT                     */

/*    8         2020-02-05      KS     Modified PROCESS_STOCK AND PROCESS_STOCK_OUT to no longer require client/site   */

/*    9         2020-02-26      KS     Renamed TQ_SORTATION_WALL to TQ_BULK_PICK to make changes for new Clients       */

/*    10        2020-02-26      KS     Added SET_STAGE_ROUTE Function and removed 2 stage sort from SEARCH_LOCATION    */

/*                                                                                                                     */ 

/***********************************************************************************************************************/ 

/**********************GRANTS**************************

grant execute on TQ_BULK_PICK to DCSDBA; 

******************************************************/

/******************REQUIRED GRANTS*********************

grant select on move_task to TORQUE;
grant select on location to TORQUE;

grant update on move_task to TORQUE;
grant update on order_header to TORQUE;

grant insert on move_task to TORQUE;

grant execute on liberror to TORQUE;


/********************DEPENDENCIES***********************

Packages:
    RDTBUFFER

******************************************************/

-- 
    FUNCTION search_location (
        p_site_id       IN   VARCHAR2,
        p_client_id     IN   VARCHAR2,
        p_user_id       IN   VARCHAR2,
        p_station_id    IN   VARCHAR2,
        p_from_loc_id   IN   VARCHAR2,
        p_sku_id        IN   VARCHAR2
    ) RETURN VARCHAR2; 

-- 

    FUNCTION process_stock (
        p_user_id      IN   VARCHAR2,
        p_station_id   IN   VARCHAR2
    ) RETURN NUMBER; 

-- 

    FUNCTION sort_validation (
        p_site_id       VARCHAR2,
        p_client_id     VARCHAR2,
        p_station_id    VARCHAR2,
        p_user_id       VARCHAR2,
        p_location_id   VARCHAR2
    ) RETURN NUMBER;

-- 

    FUNCTION process_stock_out (
        p_station_id   VARCHAR2,
        p_user_id      VARCHAR2
    ) RETURN NUMBER;

-- 

    FUNCTION process_stock_direct (
        p_site_id     VARCHAR2,
        p_client_id   VARCHAR2,
        p_order_id    VARCHAR2
    ) RETURN NUMBER;

--

    FUNCTION set_bulk_pick (
        p_client_id       VARCHAR2,
        p_consignment     VARCHAR2,
        p_ship_location   VARCHAR2,
        p_stage_route_id  VARCHAR2 DEFAULT 'BULK-PICK'
    ) RETURN NUMBER;

END tq_bulk_pick;

/

  GRANT EXECUTE ON "TORQUE"."TQ_BULK_PICK" TO "DCSDBA";
