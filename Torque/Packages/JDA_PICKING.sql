--------------------------------------------------------
--  DDL for Package JDA_PICKING
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "TORQUE"."JDA_PICKING" AS 

/***********************************************************************************************************************/ 

/*                                                                                                                     */ 

/*  NAME:               JDA_PICKING                                                                                    */ 

/*                                                                                                                     */ 

/*  DESCRIPTION:        To Store All Custom Functionality Related To Picking                                           */ 

/*                                                                                                                     */ 

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */ 

/*  =======     ===========     ==     ===================================================================             */ 

/*    1         2019-07-23      KS     Initial Version                                                                 */ 

/*    2         2019-10-15      KS     Added SET_REPACK Function                                                       */ 

/*    3         2019-10-24      KS     Added SET_BULK_PICK Function                                                    */ 

/*    4         2019-11-22      KS     Added COMPLETE_KITTING Function                                                 */ 

/*    5         2020-08-10      CH     Added COLL_ORDERTABLE type to return order list                                 */ 

/*    6         2020-08-10      CH     Added TQ_ORDER_GROUPING Function                                                */ 

/*    7         2020-08-12      CH     Added Specific Grouping Rule For MW                                             */ 

/*                                                                                                                     */  

/***********************************************************************************************************************/  

/**********************GRANTS**************************

grant execute on JDA_PICKING to DCSDBA;   
grant execute on JDA_PICKING to DCSRPT;

******************************************************/

/******************REQUIRED GRANTS*********************

grant select on location to TORQUE;
grant select on move_task to TORQUE;
grant select on sku to TORQUE;
grant select on move_task to TORQUE;
grant select on order_line to TORQUE;

grant update on move_task to TORQUE;
grant update on order_header to TORQUE;

grant execute on libsession to TORQUE;
grant execute on libkitting to TORQUE;
grant execute on liberror to TORQUE;

******************************************************/

    FUNCTION re_sort_tasks (
        p_client_id   IN   VARCHAR2,
        p_order_id    IN   VARCHAR2,
        p_sort_by     IN   VARCHAR2
    ) RETURN VARCHAR2; 

-- 

    FUNCTION complete_kitting (
        p_site_id        VARCHAR2,
        p_client_id      VARCHAR2,
        p_order_id       VARCHAR2,
        p_kit_location   VARCHAR2,
        p_user_id        VARCHAR2,
        p_station_id     VARCHAR2
    ) RETURN NUMBER;

--

    FUNCTION set_bulk_pick (
        p_client_id       VARCHAR2,
        p_consignment     VARCHAR2,
        p_ship_location   VARCHAR2
    ) RETURN NUMBER;


--

    FUNCTION set_repack (
        p_client_id       IN   VARCHAR2,
        p_consignment     IN   VARCHAR2,
        p_ship_location   IN   VARCHAR2,
        p_pack_location   IN   VARCHAR2
    ) RETURN NUMBER;
    
--

    function TQ_ORDER_GROUPING
    (
        p_Client varchar2,
        p_MinUnits number,
        p_MaxUnits number,
        p_ConsigsToBeCreated number,
        p_OrdersPerConsig number,
        p_MinOrdersReqPerConsig number,
        p_OrderTypes varchar2,
        p_WorkZoneCount number default 0,
        p_WorkZones varchar2 default '',
        p_CustomWhereSQL varchar2 default '',
        p_CustomHavingSQL varchar2 default '',
        p_UpdateTable varchar2 default '',
        p_UpdateSQL varchar2 default '',
        p_OrderByOpt number default 0
    )
    return coll_Orders;

--

    function TQ_MW_SORTER_BATCH_GROUPING (
        p_Phase2MinUnits int,
        p_Phase2MaxUnits int,
        p_Phase2Orders int,
        p_StandardMinUnits int,
        p_StandardMaxUnits int,
        p_StandardOrders int,
        p_LargeOrders int,
        p_UglyOrders int,
        p_IncludeSingles varchar2,
        p_PriorityOnly char default 'N' 
    ) return coll_Orders;

--

END jda_picking;

/

  GRANT EXECUTE ON "TORQUE"."JDA_PICKING" TO "DCSRPT";
  GRANT EXECUTE ON "TORQUE"."JDA_PICKING" TO "DCSDBA";
