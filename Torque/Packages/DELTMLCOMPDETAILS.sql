--------------------------------------------------------
--  DDL for Procedure DELTMLCOMPDETAILS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "TORQUE"."DELTMLCOMPDETAILS" AS 
BEGIN
  delete from torque.tml_comp_details where date_run < trunc(sysdate)-5;
END DELTMLCOMPDETAILS;

/
