--------------------------------------------------------
--  DDL for Package TQ_REPORTS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "TORQUE"."TQ_REPORTS" AS 

-- =============================================
-- Author:      Christian Haslam
-- Create date: 13/08/2020
-- Description: Package for Objects that are used in report selection in the WMS
--
--
-- Change History:
--      CH | 16/12/2020: Added function that allows us to create users in Bulk
-- Grants:
--      grant execute on TORQUE.TQ_REPORTS to DCSRPT
--      grant execute on TORQUE.TQ_REPORTS to DCSDBA
-- =============================================


-- =============================================
-- FUNCTIONS
-- =============================================


-- =============================================
-- PROCEDURES
-- =============================================
procedure TQ_UPDATE_SKU_CE_COO (op_Rows out number, p_Client varchar2, p_Sku varchar2, p_COO varchar2, p_Delimeter char default ',');
procedure TQ_BULK_CREATE_USERS(op_UsersCreated out number, p_UserToCopy varchar2, p_NewUsers varchar2, p_NewName varchar2 default null, p_Password varchar2, p_PassShouldChange char);

END TQ_REPORTS;

/

  GRANT EXECUTE ON "TORQUE"."TQ_REPORTS" TO "DCSDBA";
  GRANT EXECUTE ON "TORQUE"."TQ_REPORTS" TO "DCSRPT";
