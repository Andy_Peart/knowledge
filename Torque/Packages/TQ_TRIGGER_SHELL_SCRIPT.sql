--------------------------------------------------------
--  DDL for Function TQ_TRIGGER_SHELL_SCRIPT
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "TORQUE"."TQ_TRIGGER_SHELL_SCRIPT" (
    p_script_path   VARCHAR2,
    p_parameters    VARCHAR2,
    p_name          VARCHAR2,
    p_user_id       VARCHAR2,
    p_station_id    VARCHAR2
) RETURN NUMBER AS
    PRAGMA autonomous_transaction;
    v_run_task_key NUMBER;
BEGIN

--GRANT ALL ON RUN_TASK_PK_SEQ TO TORQUE;
--GRANT INSERT ON RUN_TASK TO TORQUE;
--grant execute on TQ_TRIGGER_SHELL_SCRIPT to DCSDBA;
--grant execute on TQ_TRIGGER_SHELL_SCRIPT to DCSRPT;

--Example Usage
--select TORQUE.TQ_TRIGGER_SHELL_SCRIPT('$DCS_USERRPTDIR/print_consignment_delivery.sh','"GB" "BB" "BB-2019-08-09-3" "PRT057" "KSHACK"','UREPDELNOTEBYCONS','KYLE','STATION') from dual
--select TORQUE.TQ_TRIGGER_SHELL_SCRIPT('$DCS_USERRPTDIR/print_consignment_delivery.sh','"GB" "BB" "BB-2019-08-09-3" "PRT057" "KSHACK"','TESTKYLE1234','KYLE','STATION') from dual
    
    v_run_task_key := dcsdba.run_task_pk_seq.nextval;
    
    INSERT INTO dcsdba.run_task (
        key,
        site_id,
        station_id,
        user_id,
        status,
        command,
        pid,
        old_dstamp,
        dstamp,
        language,
        name,
        time_zone_name,
        nls_calendar,
        print_label,
        java_report,
        run_light,
        server_instance,
        priority,
        archive,
        archive_ignore_screen,
        archive_restrict_user,
        client_id,
        email_recipients,
        email_attachment,EMAIL_SUBJECT,
        email_message,
        master_key,
        use_db_time_zone
    ) VALUES (
        v_run_task_key,
        'SHELL',
        p_station_id,
        p_user_id,
        'Pending',
        p_script_path
        || ' '
        || p_parameters,
        - 1,
        systimestamp,
        systimestamp,
        'EN_GB',
        p_name,
        'GB',
        'Gregorian',
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        v_run_task_key,
        'N'
    );

    COMMIT;
    RETURN 1;
    
EXCEPTION
    WHEN OTHERS THEN
        ROLLBACK;
        dcsdba.liberror.writeerrorlog('TQ_TRIGGER_SHELL_SCRIPT', sqlcode, sqlerrm);
        RETURN 0;
END;

/

  GRANT EXECUTE ON "TORQUE"."TQ_TRIGGER_SHELL_SCRIPT" TO "DCSRPT";
  GRANT EXECUTE ON "TORQUE"."TQ_TRIGGER_SHELL_SCRIPT" TO "DCSDBA";
