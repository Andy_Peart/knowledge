--------------------------------------------------------
--  DDL for Package RDTBUFFER
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "TORQUE"."RDTBUFFER" AS 

/***********************************************************************************************************************/ 

/*                                                                                                                     */ 

/*  NAME:               RDTBUFFER                                                                                      */ 

/*                                                                                                                     */ 

/*  DESCRIPTION:        To Assist With Storing Temporary Data Into A Table Using RDT Rul                               */ 

/*                                                                                                                     */ 

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */ 

/*  =======     ===========     ==     ===================================================================             */ 

/*    1         2019-05-14      KS     Initial Version                                                                 */ 

/*    2         2019-09-30      KS     Added PurgeBufferData Procedure                                                 */ 

/*                                                                                                                     */ 

/***********************************************************************************************************************/  

/**********************GRANTS**************************

grant execute on RDTBUFFER to DCSDBA; 

******************************************************/

/******************REQUIRED GRANTS*********************

******************************************************/


-- 
    FUNCTION setbufferdata (
        p_clientid    IN   VARCHAR2,
        p_userid      IN   VARCHAR2,
        p_stationid   IN    VARCHAR2,
        p_field       IN   VARCHAR2 := NULL,
        p_data        IN   VARCHAR2 := NULL
    ) RETURN INTEGER; 

-- 

    FUNCTION readbufferdata (
        p_clientid    IN    VARCHAR2,
        p_userid      IN   VARCHAR2,
        p_stationid   IN   VARCHAR2,
        p_field       IN   VARCHAR2 := NULL
    ) RETURN VARCHAR2; 

-- 

    FUNCTION deletebufferdata (
        p_clientid    IN   VARCHAR2,
        p_userid      IN   VARCHAR2,
        p_stationid   IN   VARCHAR2,
        p_field       IN   VARCHAR2 := NULL
    ) RETURN INTEGER; 

--

    PROCEDURE purgebufferdata (
        p_age        IN   NUMBER DEFAULT 7
        );

-- 

END rdtbuffer;

/

  GRANT EXECUTE ON "TORQUE"."RDTBUFFER" TO "DCSDBA";
