--------------------------------------------------------
--  DDL for Function FUNC_STD_TROL_SINGLE_ZONE
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "TORQUE"."FUNC_STD_TROL_SINGLE_ZONE" (
    p_client_id varchar2,
    p_cons_per_run int, 
    p_ords_per_con int,
    p_min_ord_per_con int,
    p_min_units int,
    p_max_units int,
    p_carriers varchar2,
    p_zone varchar2 DEFAULT NULL
    )    
return tbl_order_id
is

/**********************************************************

Temporary Function For Site Testing

***********************************************************/



    /* Initializing Order table */
    v_order_ids tbl_order_id := tbl_order_id();
    /* Total orders affected by query */
    v_num_orders int;
    /* Amount of 'full' batches available to consign */
    v_num_full_cons int;
    /* Remainder left over to consign from a full batch e.g For 24 orders, this variable would show 4 when the 'v_num_ords_per_cons' variable is set to 20 */
    v_num_remainder int;
    /* Max orders allowed for the consignment */
    v_num_max_ords int := p_cons_per_run * p_ords_per_con;
    /* Number of orders per consignment ; ENSURE THIS MATCHES THE ORDER GROUP BUILDING SCREEN */
    v_num_ords_per_cons int := p_ords_per_con;
    /* Minumum orders to consign */
    v_min_ords int := p_min_ord_per_con;
begin        
    /* Get the number of orders affected by the rule */
    select count(*) into v_num_orders
    from
    (select order_id from
        (select oh.order_id, sum(ol.qty_ordered) qo, sum(ol.qty_tasked) qt
        from dcsdba.order_header oh
        inner join dcsdba.order_line ol on ol.order_id = oh.order_id and ol.client_id = oh.client_id
        where oh.client_id = p_client_id
        and oh.order_type = 'WEB'
        and oh.status = 'Allocated'
        and oh.carrier_id in    (select regexp_substr(p_carriers,'[^,]+',1,level) from dual
                                connect by regexp_substr(p_carriers,'[^,]+',1,level) is not null)
        and oh.consignment is null
        and oh.order_id in (with q as(
select 
oh.client_id, 
oh.order_id,
l.zone_1
from 
dcsdba.order_header oh
inner join dcsdba.order_line ol on (oh.client_id = ol.client_id AND oh.order_id = ol.order_id)
inner join dcsdba.move_task mt on (ol.client_id = mt.client_id AND ol.order_id = mt.task_id AND ol.line_id = mt.line_id)
inner join dcsdba.location l on (mt.from_loc_id = l.location_id AND mt.site_id = l.site_id)
where oh.client_id = p_client_id
AND oh.status = 'Allocated'
AND oh.consignment is null
group by
oh.client_id, 
oh.order_id,
l.zone_1
order by
oh.order_id
),
q2 as(
select client_id,order_id, COUNT(DISTINCT zone_1)zones 
from q 
group by 
client_id,
order_id 
HAVING COUNT(DISTINCT zone_1) = 1)

select distinct q.order_id from q inner join q2 on (q.client_id = q2.client_id AND q.order_id = q2.order_id)
WHERE q.zone_1 = p_zone)
        group by  oh.order_id)
    where qo = qt
    and qo between p_min_units and p_max_units);

    /* Capping max orders to pre-determined max limit */
    if v_num_orders >= v_num_max_ords then
        v_num_full_cons := v_num_max_ords / v_num_ords_per_cons;
        v_num_remainder := 0;
    else
        /* Get the full consignment count, plus the remainder if not >= than the max order number */
        v_num_full_cons := floor(v_num_orders/v_num_ords_per_cons);
        v_num_remainder := mod(v_num_orders, v_num_ords_per_cons);
        /* Rounding up or down based on the pre-determined order limit */
        if v_num_remainder = 0 or v_num_remainder >= v_min_ords then 
        v_num_full_cons := v_num_full_cons + 1;
        end if;
    end if;

    v_order_ids.extend();
    select typ_order_id(order_id)
    bulk collect into v_order_ids
        from 
            (select oh.order_id, sum(ol.qty_ordered) qo, sum(ol.qty_tasked) qt
            from dcsdba.order_header oh
            inner join dcsdba.order_line ol on ol.order_id = oh.order_id and ol.client_id = oh.client_id
            where oh.client_id = p_client_id
            and oh.order_type = 'WEB'
            and oh.status = 'Allocated'
            and oh.carrier_id in    (select regexp_substr(p_carriers,'[^,]+',1,level) from dual
                                    connect by regexp_substr(p_carriers,'[^,]+',1,level) is not null)
            and oh.consignment is null
            and oh.order_id in (with q as(
select 
oh.client_id, 
oh.order_id,
l.zone_1
from 
dcsdba.order_header oh
inner join dcsdba.order_line ol on (oh.client_id = ol.client_id AND oh.order_id = ol.order_id)
inner join dcsdba.move_task mt on (ol.client_id = mt.client_id AND ol.order_id = mt.task_id AND ol.line_id = mt.line_id)
inner join dcsdba.location l on (mt.from_loc_id = l.location_id AND mt.site_id = l.site_id)
where oh.client_id = p_client_id
AND oh.status = 'Allocated'
AND oh.consignment is null
group by
oh.client_id, 
oh.order_id,
l.zone_1
order by
oh.order_id
),
q2 as(
select client_id,order_id, COUNT(DISTINCT zone_1)zones 
from q 
group by 
client_id,
order_id 
HAVING COUNT(DISTINCT zone_1) = 1)

select distinct q.order_id from q inner join q2 on (q.client_id = q2.client_id AND q.order_id = q2.order_id)
WHERE q.zone_1 = p_zone)
            group by  oh.order_id, oh.creation_date order by oh.creation_date)
        where qo = qt
        and qo between p_min_units and p_max_units
        /* The rownum is gathered from the if block */
        and rownum <= v_num_full_cons * v_num_ords_per_cons;
    return  v_order_ids;
end;

/

  GRANT EXECUTE ON "TORQUE"."FUNC_STD_TROL_SINGLE_ZONE" TO "DCSDBA";
