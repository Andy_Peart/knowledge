--------------------------------------------------------
--  File created - Thursday-March-18-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Function TQ_PARCEL_PACK
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "TORQUE"."TQ_PARCEL_PACK" 
(
    p_Client varchar2,
    p_TASK varchar2,
    p_PARCEL varchar2,
    p_user_id varchar2,
    p_sku VARCHAR2
) return varchar2 as
v_key dcsdba.move_task.key%type;

BEGIN

select min(key) into v_key
from dcsdba.move_task where 
    TASK_TYPE = 'B'
    and task_id = P_TASK
    and CLIENT_ID = P_CLIENT
    and sku_id = p_sku;
    
   TQ_PARCEL_PACK_DO
(
    p_Client ,
    p_TASK ,
    p_PARCEL ,
    p_user_id ,
    p_sku,
    v_key );
 
return '1';

END;

/

  GRANT EXECUTE ON "TORQUE"."TQ_PARCEL_PACK" TO "DCSDBA" WITH GRANT OPTION;
