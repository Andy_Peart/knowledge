--------------------------------------------------------
--  DDL for Package JDA_INBOUND
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "TORQUE"."JDA_INBOUND" AS 

/***********************************************************************************************************************/ 

/*                                                                                                                     */ 

/*  NAME:               JDA_INBOUND                                                                                    */ 

/*                                                                                                                     */ 

/*  DESCRIPTION:        To Store All Custom Functionality Related To Inbound                                           */ 

/*                                                                                                                     */ 

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */ 

/*  =======     ===========     ==     ===================================================================             */ 

/*    1         2019-08-05      KS     Initial Version                                                                 */ 

/*    2         2019-08-15      KS     Added PUTAWAY_RECREATE Function                                                 */ 

/*    3         2019-08-22      KS     Added FIND_TAG Function                                                         */ 

/*    4         2019-09-20      KS     Updated PUTAWAY_RECREATE Function                                               */ 

/*    5         2019-09-23      KS     Added FIND_RETURN_LOCATION Function                                             */

/*    6         2019-09-24      KS     Updated FIND_RETURN_LOCATION + PUTAWAY_RECREATE Function                        */

/*    7         2019-09-25      KS     Added RETURN_API Function                                                       */

/*    7         2019-10-08      KS     Modified RETURN_API Function                                                    */

/*    8         2020-04-14      KS     Modified RETURN_API Function To Support Scrapping                               */

/*    9         2020-12-15      CH     Modified RETURN_API Function To Update ITLs UDT1 with COO                       */

/*                                                                                                                     */ 

/***********************************************************************************************************************/ 

/**********************GRANTS**************************

grant execute on JDA_INBOUND to DCSDBA; 

******************************************************/

/******************REQUIRED GRANTS*********************

grant select on sku to TORQUE;
grant select on supplier_sku to TORQUE;
grant select on move_task_pk_seq to TORQUE;
grant select on move_task to TORQUE;
grant select on inventory to TORQUE;
grant select on location to TORQUE;   
grant select on inventory_transaction to TORQUE;
grant select on language_text to TORQUE;      
grant select on sampling_route_action to TORQUE;

grant update on supplier_sku to TORQUE;
grant update on inventory to TORQUE;
grant update on inventory_transaction to TORQUE;
grant update on order_line to TORQUE;
grant update on order_line_archive to TORQUE;

grant insert on supplier_sku to TORQUE;
grant insert on move_task to TORQUE;  

grant delete on supplier_sku to TORQUE;
grant delete on move_task to TORQUE;

grant execute on liberror to TORQUE;  
grant execute on libdatacheck to TORQUE;
grant execute on libmovetask to TORQUE;
grant execute on libsession to TORQUE;
grant execute on libmergereceipt to TORQUE;
grant execute on LIBSTOCKADJUST to TORQUE;

******************************************************/

    FUNCTION capture_supplier_sku (
        p_client_id         VARCHAR2,
        p_sku_id            VARCHAR2,
        p_supplier_sku_id   VARCHAR2,
        p_single_record     NUMBER
    ) RETURN NUMBER; 

-- 

    FUNCTION putaway_recreate (
        p_site_id        VARCHAR2,
        p_client_id      VARCHAR2,
        p_tag_id         VARCHAR2,
        p_from_loc_id    VARCHAR2,
        p_final_loc_id   VARCHAR2 DEFAULT NULL,
        p_qty            NUMBER DEFAULT 0
    ) RETURN NUMBER; 

-- 

    FUNCTION find_tag (
        p_site_id       VARCHAR2,
        p_client_id     VARCHAR2,
        p_sku_id        VARCHAR2,
        p_location_id   VARCHAR2
    ) RETURN VARCHAR2;

--

    FUNCTION find_return_location (
        p_station_id    VARCHAR2,
        p_user_id       VARCHAR2,
        p_site_id       VARCHAR2,
        p_client_id     VARCHAR2,
        p_zone_1        VARCHAR2,
        p_tag_id        VARCHAR2,
        p_sku_id        VARCHAR2,
        p_location_id   VARCHAR2,
        p_level_ex      VARCHAR2
    ) RETURN NUMBER;

--

    FUNCTION return_api (
        p_site_id         VARCHAR2,
        p_client_id       VARCHAR2,
        p_owner_id        VARCHAR2,
        p_line_id         NUMBER,
        p_sku_id          VARCHAR2,
        p_qty             NUMBER,
        p_location_id     VARCHAR2,
        p_order_id        VARCHAR2,
        p_condition_id    VARCHAR2,
        p_notes           VARCHAR2,
        p_reason_id       VARCHAR2,
        p_user_id         VARCHAR2 DEFAULT 'COURIERAPI',
        p_station_id      VARCHAR2 DEFAULT 'COURIERAPI',
        p_config_id       VARCHAR2 DEFAULT 'P1',
        p_coo             VARCHAR2 DEFAULT NULL,
        p_pallet_config   VARCHAR2 DEFAULT 'MD'
    ) RETURN VARCHAR2;

--

END jda_inbound;

/

  GRANT EXECUTE ON "TORQUE"."JDA_INBOUND" TO "DCSDBA";
