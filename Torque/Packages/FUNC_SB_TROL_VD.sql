--------------------------------------------------------
--  DDL for Function FUNC_SB_TROL_VD
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "TORQUE"."FUNC_SB_TROL_VD" (
    p_cons_per_run int, 
    p_ords_per_con int,
    p_min_ord_per_con int,
    p_min_units int,
    p_max_units int,    
    p_num_work_zones int,
    p_work_zones varchar2
    )    
return tbl_order_id
is
    /* Initializing Order table */
    v_order_ids tbl_order_id := tbl_order_id();
    /* Total orders affected by query */
    v_num_orders int;
    /* Amount of 'full' batches available to consign */
    v_num_full_cons int;
    /* Remainder left over to consign from a full batch e.g For 24 orders, this variable would show 4 when the 'v_num_ords_per_cons' variable is set to 20 */
    v_num_remainder int;
    /* Max orders allowed for the consignment */
    v_num_max_ords int := p_cons_per_run * p_ords_per_con;
    /* Number of orders per consignment ; ENSURE THIS MATCHES THE ORDER GROUP BUILDING SCREEN */
    v_num_ords_per_cons int := p_ords_per_con;
    /* Minumum orders to consign */
    v_min_ords int := p_min_ord_per_con;
begin        
    /* Get the number of orders affected by the rule */
    select count(*) into v_num_orders
    from
    (select order_id from
        (select oh.order_id, sum(ol.qty_ordered) qo, sum(ol.qty_tasked) qt
        from dcsdba.order_header oh
        inner join dcsdba.order_line ol on ol.order_id = oh.order_id and ol.client_id = oh.client_id
        where oh.client_id = 'SB'
        and oh.order_id in (select task_id from dcsdba.MOVE_TASK where client_id = 'SB' and task_type = 'O' group by task_id having count(distinct work_zone) = p_num_work_zones)
        and oh.order_id not in (select distinct task_id from dcsdba.MOVE_TASK where client_id = 'SB' and task_type = 'O' and work_zone not in (select regexp_substr(p_work_zones,'[^,]+',1,level) from dual
                                                                                                                                                connect by regexp_substr(p_work_zones,'[^,]+',1,level) is not null))
        and oh.order_type in ('WEB') 
        and oh.status = 'Allocated'
        and oh.dispatch_method = 'STANDARDCCV'
        and oh.consignment is null
        group by  oh.order_id)
    where qo = qt
    and qo between p_min_units and p_max_units);

    /* Capping max orders to pre-determined max limit */
    if v_num_orders >= v_num_max_ords then
        v_num_full_cons := v_num_max_ords / v_num_ords_per_cons;
        v_num_remainder := 0;
    else
        /* Get the full consignment count, plus the remainder if not >= than the max order number */
        v_num_full_cons := floor(v_num_orders/v_num_ords_per_cons);
        v_num_remainder := mod(v_num_orders, v_num_ords_per_cons);
        /* Rounding up or down based on the pre-determined order limit */
        if v_num_remainder = 0 or v_num_remainder >= v_min_ords then 
        v_num_full_cons := v_num_full_cons + 1;
        end if;
    end if;

    v_order_ids.extend();
    select typ_order_id(order_id)
    bulk collect into v_order_ids
        from 
            (select oh.order_id, sum(ol.qty_ordered) qo, sum(ol.qty_tasked) qt, oh.creation_date
            from dcsdba.order_header oh
            inner join dcsdba.order_line ol on ol.order_id = oh.order_id and ol.client_id = oh.client_id
            where oh.client_id = 'SB'
            and oh.order_id in (select task_id from dcsdba.MOVE_TASK where client_id = 'SB' and task_type = 'O' group by task_id having count(distinct work_zone) = p_num_work_zones)
            and oh.order_id not in (select distinct task_id from dcsdba.MOVE_TASK where client_id = 'SB' and task_type = 'O' and work_zone not in (select regexp_substr(p_work_zones,'[^,]+',1,level) from dual
                                                                                                                                                connect by regexp_substr(p_work_zones,'[^,]+',1,level) is not null))
            and oh.order_type in ('WEB') 
            and oh.status = 'Allocated'
            and oh.consignment is null
            and oh.dispatch_method = 'STANDARDCCV'
            group by  oh.order_id, oh.creation_date
            order by creation_date)
        where qo = qt
        and qo between p_min_units and p_max_units
        /* The rownum is gathered from the if block */
        and rownum <= v_num_full_cons * v_num_ords_per_cons
        order by creation_date
        ;
    return  v_order_ids;
end;

/

  GRANT EXECUTE ON "TORQUE"."FUNC_SB_TROL_VD" TO "DCSDBA";
