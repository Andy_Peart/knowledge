--------------------------------------------------------
--  DDL for Procedure TMP_RESOURCE_LIMIT_LOG
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "TORQUE"."TMP_RESOURCE_LIMIT_LOG" 
IS
BEGIN
insert into TORQUE.RP_RESOURCE_LIMIT_LOG(SELECT rl.*, systimestamp FROM v$resource_limit rl WHERE resource_name IN ('processes', 'sessions'));
commit;
END;

/

  GRANT EXECUTE ON "TORQUE"."TMP_RESOURCE_LIMIT_LOG" TO "DCSDBA";
