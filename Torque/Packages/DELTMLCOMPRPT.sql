--------------------------------------------------------
--  DDL for Procedure DELTMLCOMPRPT
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "TORQUE"."DELTMLCOMPRPT" AS 
begin
  delete from TORQUE.TML_COMPRPT where date_run < trunc(sysdate)-30;
END DELTMLCOMPRPT;

/
