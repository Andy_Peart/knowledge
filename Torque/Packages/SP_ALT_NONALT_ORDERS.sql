--------------------------------------------------------
--  DDL for Function SP_ALT_NONALT_ORDERS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "TORQUE"."SP_ALT_NONALT_ORDERS" (
    p_alteration NUMBER,
    p_min_units NUMBER DEFAULT 1,
    p_max_units NUMBER DEFAULT 99999
) RETURN tbl_order_id IS
/* Initializing Order table */
    v_order_ids tbl_order_id := tbl_order_id();
BEGIN
    IF p_alteration = 1 THEN
        SELECT
            typ_order_id(oh.order_id)
        BULK COLLECT
        INTO v_order_ids
        FROM
            dcsdba.order_header   oh
            INNER JOIN dcsdba.order_line     ol ON ( oh.client_id = ol.client_id
                                                 AND oh.order_id = ol.order_id )
            LEFT JOIN dcsdba.move_task      mt ON ( oh.client_id = mt.client_id
                                               AND oh.order_id = mt.task_id
                                               AND ol.line_id = mt.line_id )
        WHERE
            oh.client_id = 'SP'
            AND oh.consignment IS NULL
            AND oh.status IN (
                'Released',
                'Allocated'
            )
            AND mt.task_type = 'O'
            AND (ol.sku_id != mt.sku_id )
            group by oh.order_id
            HAVING SUM(ol.qty_ordered) between p_min_units AND p_max_units;
    ELSE
        SELECT
            typ_order_id(oh.order_id)
        BULK COLLECT
        INTO v_order_ids
        FROM
            dcsdba.order_header oh
            inner join dcsdba.order_line ol on (oh.client_id = ol.client_id AND oh.order_id = ol.order_id)
        WHERE
            oh.client_id = 'SP' 
AND oh.consignment IS NULL
            AND oh.status IN (
                'Released',
                'Allocated'
            )
            AND oh.order_id NOT IN (
                SELECT DISTINCT
                    oh.order_id
                FROM
                    dcsdba.order_header   oh
                    INNER JOIN dcsdba.order_line     ol ON ( oh.client_id = ol.client_id
                                                         AND oh.order_id = ol.order_id)
left JOIN dcsdba.move_task      mt ON ( oh.client_id = mt.
                                                         client_id
                                                       AND oh.order_id = mt.task_id
                                                       AND ol.line_id = mt.line_id )
                WHERE
                    oh.client_id = 'SP'
                    AND oh.consignment IS NULL
                    AND oh.status IN (
                        'Released',
                        'Allocated'
                    )
                    AND mt.task_type = 'O'
                    AND ( ol.sku_id != mt.sku_id )
            )
            group by oh.order_id
            HAVING SUM(ol.qty_ordered) between p_min_units AND p_max_units;


    END IF;

RETURN v_order_ids;    
    
END;

/

  GRANT EXECUTE ON "TORQUE"."SP_ALT_NONALT_ORDERS" TO "DCSDBA";
