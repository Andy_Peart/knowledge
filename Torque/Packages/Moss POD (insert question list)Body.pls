--------------------------------------------------------
--  File created - Thursday-March-18-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package Body TQ_MOSS_POD
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "TORQUE"."TQ_MOSS_POD" AS

/*-----------------------------------------------------------------------------------------------*/
/*                                                                                                                              */
/*  Name :- TQ_MOSS_POD                                                                              */
/*                                                                                                                              */
/* Descritpion :- Functions required to force input of POD details              */
/*                                                                                                                              */
/* Version      Date                DEV     Description                                              */
/*=======    ========       ===      ============================ */
/* 1                 09/02/21         AMP      Initial Write                                              */
/*                                                                                                                              */
/*-----------------------------------------------------------------------------------------------*/

FUNCTION CLEAR_QUESTION_LOG (
p_station_id VARCHAR2) RETURN VARCHAR2  is

BEGIN

CLEAR_QUESTION_LOG_DO (
p_station_id);
RETURN '1';

END CLEAR_QUESTION_LOG;

PROCEDURE CLEAR_QUESTION_LOG_DO (
p_station_id VARCHAR2) is

PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN

delete from dcsdba.questions_log where station_Id = p_station_id;
COMMIT;

END CLEAR_QUESTION_LOG_DO;

------------------------------------------------------------------------------------------------------------------------

PROCEDURE CLEAR_QUESTION_LIST_DO(
p_station_id VARCHAR2)
IS
PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN 

delete from dcsdba.question_list_screen where station_Id = p_station_id;
COMMIT;

END CLEAR_QUESTION_LIST_DO;


FUNCTION CLEAR_QUESTION_LIST(
p_station_id VARCHAR2)
RETURN VARCHAR2 IS

BEGIN 
 CLEAR_QUESTION_LIST_DO(p_station_id);
RETURN 1;

END CLEAR_QUESTION_LIST;


------------------------------------------------------------------------------------------------------------------------

FUNCTION  DO_ADD_PO(
p_client_id VARCHAR2,
p_site_id VARCHAR2,
p_station_id VARCHAR2,
p_user VARCHAR2,
p_pre_advice VARCHAR2)
RETURN number IS

BEGIN
   torque.TQ_MOSS_POD. ADD_PO_TO_LOG_PROC(
p_client_id ,
p_site_id ,
p_station_id ,
p_user ,
p_pre_advice );

return 1;

END  DO_ADD_PO;


PROCEDURE ADD_PO_TO_LOG_PROC(
p_client_id VARCHAR2,
p_site_id VARCHAR2,
p_station_id VARCHAR2,
p_user VARCHAR2,
p_pre_advice VARCHAR2)
IS
PRAGMA AUTONOMOUS_TRANSACTION;
v_key dcsdba.questions_log.key%type;
v_client_id  varchar2(50) := p_client_id;
v_site_id varchar2(50) := p_site_id;
v_station_id varchar2(50) := p_station_id;
v_user varchar2(50) := P_USER;
v_pre_advice varchar2(50) := p_pre_advice;

BEGIN

select dcsdba.questions_log_pk_seq.nextval
into v_key
from dual;

Insert into dcsdba.questions_log 
            values (v_key,v_site_id,V_CLIENT_id ,v_station_id,v_user,'PREADVICE','description text',v_pre_advice,systimestamp);

     COMMIT;
           update dcsdba.pre_advice_header set status = 'Released' where client_id = p_client_id and pre_advice_id = p_pre_advice;
          commit;

END ADD_PO_TO_LOG_PROC;

---------------------------------------------------------------------------------------------------------------------

FUNCTION LINK_QUESTION_LIST(
p_client_id VARCHAR2,
p_site_id VARCHAR2,
p_station_id VARCHAR2,
p_list VARCHAR2)
RETURN VARCHAR2 IS

BEGIN

LINK_QUESTION_LIST_DO(p_client_id ,p_site_id ,p_station_id ,p_list );

RETURN '1';

END LINK_QUESTION_LIST;

PROCEDURE LINK_QUESTION_LIST_DO(
p_client_id VARCHAR2,
p_site_id VARCHAR2,
p_station_id VARCHAR2,
p_list VARCHAR2)
IS
PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN

insert into dcsdba.question_list_screen values ('recpadv.frm',p_station_id,p_site_id,p_client_id,	p_list,null);
COMMIT;
END LINK_QUESTION_LIST_DO;

-----------------------------------------------------------------------------------------------------------------

FUNCTION COMPLETE_BOOKING_LOG (
p_client_id VARCHAR2,
p_station_id VARCHAR2,
p_user VARCHAR2)

RETURN VARCHAR2 is

BEGIN

COMPLETE_BOOKING_LOG_DO (p_client_id ,p_station_id ,p_user );

RETURN 1;

END COMPLETE_BOOKING_LOG ;


PROCEDURE COMPLETE_BOOKING_LOG_DO (
p_client_id VARCHAR2,
p_station_id VARCHAR2,
p_user VARCHAR2)
 is
PRAGMA AUTONOMOUS_TRANSACTION;

V_INVOICE VARCHAR2(50);
V_VALUE VARCHAR2(50);
V_UNITS VARCHAR2(50);
V_PRE_ADVICE VARCHAR2(20);
V_KEY DCSDBA.BOOKING_IN_DIARY_LOG.KEY%type;
V_STATION varchar2(50) := P_STATION_ID;
V_USER varchar2(50) := P_USER;
V_CLIENT_ID varchar2(50) := P_CLIENT_ID;
V_VAT NUMBER;

BEGIN

SELECT ANSWER 
    INTO V_INVOICE 
    FROM DCSDBA.QUESTIONS_LOG
    WHERE
    CLIENT_ID = P_CLIENT_ID AND
    STATION_ID = P_STATION_ID AND
    USER_ID = P_USER AND
    QUESTION_ID = 'INV NO';

SELECT ANSWER 
    INTO V_VALUE 
    FROM DCSDBA.QUESTIONS_LOG
    WHERE
    CLIENT_ID = P_CLIENT_ID AND
    STATION_ID = P_STATION_ID AND
    USER_ID = P_USER AND
    QUESTION_ID = 'INV VAL';

SELECT ANSWER 
    INTO V_UNITS 
    FROM DCSDBA.QUESTIONS_LOG
    WHERE
    CLIENT_ID = P_CLIENT_ID AND
    STATION_ID = P_STATION_ID AND
    USER_ID = P_USER AND
    QUESTION_ID = 'TOT UNITS';

SELECT ANSWER 
    INTO V_PRE_ADVICE
    FROM DCSDBA.QUESTIONS_LOG
    WHERE
    CLIENT_ID = P_CLIENT_ID AND
    STATION_ID = P_STATION_ID AND
    USER_ID = P_USER AND
    QUESTION_ID = 'PREADVICE';

SELECT NUMERIC_DATA 
    INTO V_VAT
    FROM DCSDBA.SYSTEM_PROFILE
    WHERE PROFILE_ID = '-ROOT-_USER_DATA RULE CONSTANTS_VAT';



SELECT dcsdba.BOOKING_LOG_PK_SEQ.nextval
    into V_KEY
    FROM DUAL;

insert into dcsdba.booking_in_diary_log 
values (
V_KEY,
'Add',
V_INVOICE,
'WFD',
systimestamp,
'Complete',
'A1',
'1',
V_VAT,
V_VALUE,
V_UNITS,
'0',
V_PRE_ADVICE,
'Pre-Advised',
v_client_ID,
null,
null,
null,
null,
V_STATION,
V_USER,
systimestamp,
null,
'N',
null,
null,
null);

COMMIT;


END COMPLETE_BOOKING_LOG_DO ;

END TQ_MOSS_POD;

/

  GRANT EXECUTE ON "TORQUE"."TQ_MOSS_POD" TO "DCSDBA";
