--------------------------------------------------------
--  DDL for Function TQ_DECIDE_SORT
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "TORQUE"."TQ_DECIDE_SORT" (
	p_client_id VARCHAR2,
	p_bulk_location VARCHAR2,
    p_ship_location   VARCHAR2,
	p_consignment VARCHAR2
	)
return varchar2
as
v_check_invalid_consignment NUMBER;

begin

select count(*) into v_check_invalid_consignment from 
dcsdba.order_header where client_id = p_client_id AND consignment = p_consignment AND (status not in ('Released','Allocated','Hold') OR hub_address2 is not null OR hub_county is not null OR stage_route_id is not null);


IF v_check_invalid_consignment = 0 then
    for x in
    (
    with orders as(
    select 
    client_id, order_id, row_number() over (order by order_id) as ordering from dcsdba.order_header where client_id = p_client_id AND consignment = p_consignment),
    
    sort_locs as (
    select 
    l.location_id,
    row_number() over (order by put_sequence) as sort_location
    from dcsdba.location l
    inner join (select rownum rn from dual connect by level < 9999) a
    on (l.user_def_num_2 >= a.rn)
    where l.location_id like
    p_client_id
    || 'SORT'
    || substr(p_bulk_location, - 2)
    || '_'
    AND l.location_id <> 'ISSORTXXX')
--    ,
--    trol_locs as (
--        select 
--    l.location_id,
--    row_number() over (order by put_sequence) as sort_location
--    from dcsdba.location l
--    inner join (select rownum rn from dual connect by level < 9999) a
--    on (l.user_def_num_2 >= a.rn)
--    where l.location_id like
--    p_client_id
--    || 'TROL'
--    || substr(p_bulk_location, - 2)
--    || '_-__')
    
    select 
    orders.client_id,
    orders.order_id,
    sort_locs.location_id
    --,
    --trol_locs.location_id as trolley
    from orders inner join sort_locs on (orders.ordering = sort_locs.sort_location)
    --inner join trol_locs on (orders.ordering = trol_locs.sort_location)
    )
    
    LOOP
    
    update dcsdba.order_header set 
    --hub_county = x.trolley, 
    hub_address2 = x.location_id, stage_route_id = 'BULK-PICK' where client_id = x.client_id AND order_id = x.order_id;
    update dcsdba.move_task set to_loc_id = x.location_id where client_id = x.client_id AND task_id = x.order_id AND to_loc_id = p_ship_location AND task_type = 'O'AND status IN ('Released','Hold');
    
    END LOOP;
    commit;
END if;
return 'Success';
    EXCEPTION
        WHEN OTHERS THEN
            ROLLBACK;
            dcsdba.liberror.writeerrorlog('JDA_PICKING.DECIDE_SORT', sqlcode, sqlerrm);
            RETURN 'Fail';
end;

/
