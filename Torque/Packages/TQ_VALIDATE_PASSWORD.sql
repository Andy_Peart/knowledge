--------------------------------------------------------
--  DDL for Function TQ_VALIDATE_PASSWORD
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "TORQUE"."TQ_VALIDATE_PASSWORD" (
p_user_id VARCHAR2,
p_password VARCHAR2
) RETURN INTEGER IS
v_encpassword VARCHAR2(100) := ' ';
v_return INTEGER;
BEGIN

--grant execute on libcryption to torque;
--grant select on application_user to torque;

--grant execute on TQ_validate_password to DCSDBA;

dcsdba.LIBCRYPTION.ENCRYPT(v_encpassword,p_password,upper(p_user_id));
select COUNT(*) into v_return from dcsdba.application_user where user_id = upper(p_user_id) AND password = v_encpassword;
return v_return;
EXCEPTION 
WHEN others then
return 0;
END;

/

  GRANT EXECUTE ON "TORQUE"."TQ_VALIDATE_PASSWORD" TO "DCSDBA";
