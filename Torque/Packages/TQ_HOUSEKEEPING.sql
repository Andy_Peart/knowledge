--------------------------------------------------------
--  DDL for Package TQ_HOUSEKEEPING
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "TORQUE"."TQ_HOUSEKEEPING" AS 

/***********************************************************************************************************************/ 

/*                                                                                                                     */ 

/*  NAME:               TQ_HOUSEKEEPING                                                                                */ 

/*                                                                                                                     */ 

/*  DESCRIPTION:        To Store All Custom Functionality Related To System Housekeeping                               */ 

/*                                                                                                                     */ 

/*  RELEASE        DATE         BY     DESCRIPTION                                                                     */ 

/*  =======     ===========     ==     ===================================================================             */ 

/*    1         2019-07-29      KS     Initial Version                                                                 */ 

/*    2         2019-07-30      KS     Added Account Seurity                                                           */ 

/*    3         2019-08-07      KS     Modified Account Seurity                                                        */ 

/*    4         2019-08-12      KS     Improved Loop Logic                                                             */ 

/*    5         2019-08-21      KS     Added Order Type to GDPR Compliance                                             */ 

/*    6         2019-09-19      KS     Added Custom ITL Archive Purging Procedure                                      */ 

/*                                                                                                                     */ 

/***********************************************************************************************************************/  

/**********************GRANTS**************************

grant execute on TQ_HOUSEKEEPING to DCSDBA; 

******************************************************/

/******************REQUIRED GRANTS*********************

grant select on pre_advice_header to TORQUE;
grant select on order_header to TORQUE;
grant select on order_header_archive to TORQUE; 
grant select on shipping_manifest to TORQUE;  
grant select on inventory_transaction_archive to TORQUE; 
grant select on purge_setup to TORQUE; 
grant select on application_user to TORQUE; 

grant update on shipping_manifest to TORQUE;
grant update on pre_advice_header to TORQUE;
grant update on order_header to TORQUE; 
grant update on order_header_archive to TORQUE; 
grant update on application_user to TORQUE; 

grant delete on inventory_transaction_archive to TORQUE; 

grant execute on liberror to TORQUE;

******************************************************/

    PROCEDURE tq_update_uploaded; 

-- 

    PROCEDURE tq_gdpr_compliance; 

--

    PROCEDURE tq_account_security;
    
-- 
    
    PROCEDURE tq_purge_itl_archive(
        p_dstamp IN VARCHAR2 DEFAULT 'PURGE',
        p_commit_every IN NUMBER DEFAULT 5000
    );

END tq_housekeeping;

/

  GRANT EXECUTE ON "TORQUE"."TQ_HOUSEKEEPING" TO "DCSDBA";
