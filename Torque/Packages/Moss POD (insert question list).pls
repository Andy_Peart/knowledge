--------------------------------------------------------
--  File created - Thursday-March-18-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package TQ_MOSS_POD
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "TORQUE"."TQ_MOSS_POD" AS
/*-----------------------------------------------------------------------------------------------*/
/*                                                                                                                              */
/*  Name :- TQ_MOSS_POD                                                                              */
/*                                                                                                                              */
/* Descritpion :- Functions required to force input of POD details              */
/*                                                                                                                              */
/* Version      Date                DEV     Description                                              */
/*=======    ========       ===      ============================ */
/* 1                 09/02/21         AMP      Initial Write                                              */
/*                                                                                                                              */
/*-----------------------------------------------------------------------------------------------*/



FUNCTION  DO_ADD_PO(
p_client_id VARCHAR2,
p_site_id VARCHAR2,
p_station_id VARCHAR2,
p_user VARCHAR2,
p_pre_advice VARCHAR2)
RETURN number;

PROCEDURE ADD_PO_TO_LOG_PROC(
p_client_id VARCHAR2,
p_site_id VARCHAR2,
p_station_id VARCHAR2,
p_user VARCHAR2,
p_pre_advice VARCHAR2);

FUNCTION CLEAR_QUESTION_LOG (
p_station_id VARCHAR2)
RETURN VARCHAR2;

PROCEDURE CLEAR_QUESTION_LOG_DO (
p_station_id VARCHAR2);

FUNCTION CLEAR_QUESTION_LIST(
p_station_id VARCHAR2)
RETURN VARCHAR2;

FUNCTION LINK_QUESTION_LIST(
p_client_id VARCHAR2,
p_site_id VARCHAR2,
p_station_id VARCHAR2,
p_list VARCHAR2)
RETURN VARCHAR2;

PROCEDURE LINK_QUESTION_LIST_DO(
p_client_id VARCHAR2,
p_site_id VARCHAR2,
p_station_id VARCHAR2,
p_list VARCHAR2);

FUNCTION COMPLETE_BOOKING_LOG (
p_client_id VARCHAR2,
p_station_id VARCHAR2,
p_user VARCHAR2)
RETURN VARCHAR2;

PROCEDURE COMPLETE_BOOKING_LOG_DO (
p_client_id VARCHAR2,
p_station_id VARCHAR2,
p_user VARCHAR2);


END TQ_MOSS_POD;

/

  GRANT EXECUTE ON "TORQUE"."TQ_MOSS_POD" TO "DCSDBA";
