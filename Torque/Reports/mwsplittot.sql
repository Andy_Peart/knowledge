set pagesize 68
set linesize 120
set verify off

ttitle 'Mountain Warehouse Receipts and Putaways' skip 2

column A heading 'Date' format a8
column B heading 'Receipted' format 99999999
column C heading 'Putaway' format 99999999
column D heading 'Total' format 99999999

break on report

compute sum label 'Total' of B C on report


select 
to_char (A1, 'DD/MM/YY') A,
nvl(B1,0) B,
nvl(B2,0) C
from 
(select 
trunc(dstamp) A1,
sum (update_qty) B1
from inventory_transaction
where client_id = 'MOUNTAIN'
and code = 'Receipt'
and dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by trunc(dstamp)),
(select 
trunc(dstamp) A2,
sum (update_qty) B2
from inventory_transaction
where client_id = 'MOUNTAIN'
and station_id not like 'Auto%'
and code = 'Relocate'
and from_loc_id like 'Z%'
and to_loc_id not like 'Z%'
and dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by trunc(dstamp))
where A1=A2(+)
order by A
/
