SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 120

column A heading 'Cust' format a40
column AA heading 'Grp' format a6
column B heading 'Type' format a12
column BB heading 'Type' format a15
column C heading 'Qty' format 999999

/* This is set so that the week start date is the previous Sunday */
/* except that on Sundays and Mondays it goes back to the start of the previous week */

variable v_start  varchar2(20)
variable v_end  varchar2(20)
declare
   v_startdate  date;
begin
   v_startdate := sysdate-2;
   while trim(to_char(v_startdate,'day')) not in ('sunday')
      loop
      v_startdate := v_startdate-1;
      end loop;
:v_start:=to_char(v_startdate,'DD/MM/YYYY');
:v_end:=to_char(v_startdate+7,'DD/MM/YYYY');
end;
/


--break on A skip 2 on AA dup skip 1 on report

--compute sum label 'Total' of C on A
--compute sum label 'Total' of C on AA
--compute sum label 'Total' of C on report

--set termout off
--spool TR057.csv

select 'TR57 - Shipped by week for W/C  - '||:v_start from Dual;

select
distinct 'OT '||nvl(oh.order_type,'Blank') BB,
'END'
from  shipping_manifest sm,order_header oh
where sm.client_id='TR'
AND sm.CLIENT_ID=oh.CLIENT_ID
AND sm.CUSTOMER_ID=oh.CUSTOMER_ID
AND sm.CONSIGNMENT=oh.CONSIGNMENT
AND sm.ORDER_ID=oh.ORDER_ID
and trunc(sm.SHIPPED_DSTAMP)>=to_date(:v_start,'DD/MM/YYYY')
and trunc(sm.SHIPPED_DSTAMP)<to_date(:v_end,'DD/MM/YYYY')
--and trunc(sm.SHIPPED_DSTAMP)>trunc(sysdate-8)
--and trunc(sm.SHIPPED_DSTAMP)<trunc(sysdate)
order by BB
/

select
sm.CUSTOMER_ID||' - '||oh.name A,
sku.product_group AA,
nvl(oh.order_type,'Blank') B,
nvl(sum(sm.qty_shipped),0) C
FROM  shipping_manifest sm,order_header oh,sku
WHERE  sm.CLIENT_ID='TR'
AND sm.CLIENT_ID=oh.CLIENT_ID
AND sm.CUSTOMER_ID=oh.CUSTOMER_ID
AND sm.CONSIGNMENT=oh.CONSIGNMENT
AND sm.ORDER_ID=oh.ORDER_ID
AND sm.CLIENT_ID=SKU.CLIENT_ID
and sm.sku_id=sku.sku_id
and trunc(sm.SHIPPED_DSTAMP)>=to_date(:v_start,'DD/MM/YYYY')
and trunc(sm.SHIPPED_DSTAMP)<to_date(:v_end,'DD/MM/YYYY')
--and trunc(sm.SHIPPED_DSTAMP)>trunc(sysdate-8)
--and trunc(sm.SHIPPED_DSTAMP)<trunc(sysdate)
group by
sm.customer_id,
oh.name,
sku.product_group,
oh.ORDER_TYPE
order by
sm.customer_id,
sku.product_group,
oh.ORDER_TYPE
/


select 'END' from DUAL;


--spool off
--set termout on


