SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 80



column A format A12
column B format A10
column C format A10
column D format A8
column E format A10
column F format 99999
column G format 99999
column M noprint

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



--set termout off
--spool SBPICKS.csv


select 'Units Shipped by Retail Store from &&2 to &&3 - for client ID SB' from DUAL;
select '  ' from DUAL;
Select 'Date,Order Type,Stock Type,Store ID,Shipments,Qty Shipped,,' from DUAL;
select '  ' from DUAL;


break on A on report

compute sum label 'TOTALS' of F G on report


select
to_char(itl.Dstamp, 'DD/MM/YY') A,
to_char(itl.Dstamp, 'YYMM') M,
oh.order_type B,
'STOCK' C,
''''||itl.customer_id||'''' D,
count(*) F,
sum(itl.update_qty) G,
' '
from inventory_transaction itl,order_header oh
where itl.client_id='SB'
and itl.code='Shipment'
and itl.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and itl.reference_id like '%T%'
and oh.client_id='SB'
and itl.reference_id=oh.order_id
group by
to_char(itl.Dstamp, 'DD/MM/YY'),
to_char(itl.Dstamp, 'YYMM'),
oh.order_type,
--'Stock Type',
itl.customer_id
order by M,A,B,C
/


--spool off
--set termout on
