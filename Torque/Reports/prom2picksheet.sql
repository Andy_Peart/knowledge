SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET LINESIZE 200
SET TRIMSPOOL ON
SET HEADING OFF



select '                        PROMINENT Pick Sheet' from DUAL;
select '                        --------------------' from DUAL;
select ' ' from DUAL;
select
DECODE('&&3','1','SEQUENCE 1 - Long, reg, short - Large to small',
             '2','SEQUENCE 2 - Short, reg, long - Small to large',
             '3','SEQUENCE 3 - Long,  short, reg -  Large to small',
             '4','SEQUENCE 4 - Large to small -  Long,  short, reg',
             '5','SEQUENCE 5 - Long,  short, reg -  Small to large','SEQUENCE &&3 - INVALID - Valid Range 1 to 5') 
from DUAL;
select ' ' from DUAL;


column AA noprint
column R heading 'Order' format a16
column A heading 'SKU' format a16
column A1 heading 'Description' format a28
column A2 heading 'PORD' format a20
column B heading ' ' format a4
column C heading 'Location' format a8
column D heading 'Pick' format 9999
column D2 heading 'Stck' format 9999
column D4 heading 'Ordered' format 9999999
column E heading 'Picked' format A12
column F heading 'Size' format A4
column J heading 'PO' format A11
column M format A8 Noprint
column G heading 'X' format A1




select 'Order            SKU              Description                  PORD                Size   Ordered  Pick  From    Location   Picked' from DUAL;
select '-----            ---              -----------                  ----                ----   -------  ----  ----    --------   ------' from DUAL;
select ' ' from DUAL;

break on row skip 1 on report

compute sum label 'Total' of D D2 D4 on report


select
ol.order_id R,
ol.sku_id A,
sku.description A1,
RRR A2,
--UPPER(ol.user_def_note_2) J,
sku_size F,
DECODE('&&3','1',(DECODE(substr(sku_size,-1,1),'L','1','R','2','S','3','4')||100-substr(sku_size,1,2)),
             '2',(DECODE(substr(sku_size,-1,1),'L','3','R','2','S','1','4')||substr(sku_size,1,2)),
             '3',(DECODE(substr(sku_size,-1,1),'L',1,'R','3','S','2','4')||100-substr(sku_size,1,2)),
             '4',(100-substr(sku_size,1,2)||DECODE(substr(sku_size,-1,1),'L','1','R','3','S','2','4')),
             '5',(DECODE(substr(sku_size,-1,1),'L',1,'R','3','S','2','4')||substr(sku_size,1,2)),'XYZ') M,
ol.qty_ordered D4,
sum(mt.qty_to_move) D,
QQQ D2,
'   ' B,
mt.from_loc_id C,
'___________' E
from move_task mt,order_line ol,(select
sku_id SSS,
location_id LLL,
max(receipt_id) RRR,
sum(qty_on_hand) QQQ
from inventory
where client_id='PR'
--and receipt_id like 'PORD%'
group by
sku_id,
location_id),sku
where mt.client_id='PR'
and ol.order_id like '&&2%'
and ol.client_id=mt.client_id
and ol.order_id=mt.task_id
and ol.sku_id=mt.sku_id
and ol.line_id=mt.line_id
and ol.sku_id=SSS
and LLL=mt.from_loc_id (+)
and ol.sku_id=sku.sku_id
and sku.client_id='PR'
group by
ol.order_id,
ol.sku_id,
sku.description,
RRR,
UPPER(ol.user_def_note_2),
sku_size,
DECODE('&&3','1',(DECODE(substr(sku_size,-1,1),'L','1','R','2','S','3','4')||100-substr(sku_size,1,2)),
             '2',(DECODE(substr(sku_size,-1,1),'L','3','R','2','S','1','4')||substr(sku_size,1,2)),
             '3',(DECODE(substr(sku_size,-1,1),'L',1,'R','3','S','2','4')||100-substr(sku_size,1,2)),
             '4',(100-substr(sku_size,1,2)||DECODE(substr(sku_size,-1,1),'L','1','R','3','S','2','4')),
             '5',(DECODE(substr(sku_size,-1,1),'L',1,'R','3','S','2','4')||substr(sku_size,1,2)),'XYZ'),
ol.qty_ordered,
QQQ,
mt.from_loc_id
order by M
/
