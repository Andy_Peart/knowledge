set feedback off
set verify off
set newpage 0


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

set pagesize 66
set linesize 200
set trimspool on
 

column A heading 'Order ID' format A20
column B heading 'Order Type' format A12
column C heading 'Location' format A12
column D heading 'SKU' format A18
column E heading 'Description' format A40
column F heading 'Qty' format 999999
column G heading 'Instructions' format A80
column H heading 'Tick' format A6
column R heading 'Delivery Address' format A36
column S heading 'Invoice Address' format A36


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

set und '-'
set heading on
set newpage 0

ttitle  LEFT 'PICK SHEET in product order for Order &&2 and client Daxbourne as at ' report_date RIGHT 'Page:' FORMAT 999 SQL.PNO skip 2

select 
rpad(customer_id,36,' ')||
rpad(name,36,' ')||
rpad(address1,36,' ')||
rpad(address2,36,' ')||
rpad(town,36,' ')||
rpad(county,36,' ')||
rpad(postcode,36,' ') R,
rpad(inv_name,36,' ')||
rpad(inv_address1,36,' ')||
rpad(inv_address2,36,' ')||
rpad(inv_town,36,' ')||
rpad(inv_county,36,' ')||
rpad(inv_postcode,36,' ')||
rpad(inv_country,36,' ') S,
user_def_note_2 G
from order_header
where client_id='DX'
and order_id='&&2'
/



--break on report

--compute sum label 'Total' of D on report


set pagesize 0


select 'Order ID             Order Type   Location     SKU/Description                              Qty Tick  ' from DUAL;
select '--------             ----------   --------     ---------------                              --- ----  ' from Dual;
select '' from Dual;

break on A dup skip 2 on G 

select
mt.task_id A,
oh.order_type B,
mt.from_loc_id C,
rpad(mt.sku_id,40,' ')||
sku.description E,
mt.qty_to_move F,
'______'  H
from order_header oh,order_line ol,sku,move_task mt
where oh.client_id='DX'
and oh.order_id=ol.order_id
and oh.order_id=mt.task_id
--and oh.status<>'Shipped'
and mt.task_id='&&2'
and ol.sku_id=sku.sku_id
and ol.sku_id=mt.sku_id
order by
mt.task_id,
mt.sku_id
/
