SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','
SET WRAP OFF


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
set linesize 500
SET TRIMSPOOL ON

break on report
compute sum of Q1 Q2 on report

--spool mwrelto12.csv

Select 'Relocates into Zone 12 from &&2 to &&3' from DUAL;

select 'Sku,Descrip[tion,LPG,From Location,Cartons,Units' from DUAL;

select
''''||it.sku_id||'''' SK,
sku.description DK,
sku.user_def_type_8 LPG,
it.from_loc_id FL,
count(*) Q1,
sum(it.update_qty) Q2
from inventory_transaction it,location L,sku
where it.client_id = 'MOUNTAIN'
and it.code = 'Relocate'
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.to_loc_id=L.location_id
and it.site_id=L.site_id
and L.work_zone='12'
and it.sku_id=sku.sku_id
and sku.client_id = 'MOUNTAIN'
group by
it.sku_id,
sku.description,
sku.user_def_type_8,
it.from_loc_id
order by SK
/

--spool off