SET FEEDBACK OFF                 
set pagesize 66
set linesize 122
set verify off
set wrap off
set newpage 0
set colsep ' '
set und on
set heading on

clear columns
clear breaks
clear computes






set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

update SKU
set user_def_type_7 = '&&2'
where sku_id = 'LOAD1'
and client_id = 'MOUNTAIN';
/

set heading off
set pagesize 0

Select '' from DUAL;
Select 'END of REPORT' from DUAL;



