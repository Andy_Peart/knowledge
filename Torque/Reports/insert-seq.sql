create table sequence_file
	(control_id number
        ,next_seq number)
/
commit;

Insert into sequence_file (Control_id,Next_seq)
Values (1,1);
Insert into sequence_file (Control_id,Next_seq)
Values (2,1);
Insert into sequence_file (Control_id,Next_seq)
Values (3,1);
Insert into sequence_file (Control_id,Next_seq)
Values (4,1);
Insert into sequence_file (Control_id,Next_seq)
Values (5,1);
Insert into sequence_file (Control_id,Next_seq)
Values (6,1);

commit;
