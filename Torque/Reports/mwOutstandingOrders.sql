
set pagesize 0
set linesize 120
set verify off

select 'Order, Status, Creation Date' from DUAL;

select order_id ||','||
status ||','||
creation_date
from order_header
where client_id = 'MOUNTAIN'
and order_type = 'WEB'
and status in ('Allocated', 'Released', 'In Progress')
order by status;
