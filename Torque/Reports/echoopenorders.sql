SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET COLSEP ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET HEADING OFF
SET PAGESIZE 0
SET LINESIZE 400
SET TRIMSPOOL ON

column A heading 'Order Ref' format A20
column C heading 'Customer' format A12
column E heading 'Date' format A12
column B heading 'Units Shipped' format 9999999

break on A dup skip 1 
--compute sum label 'Cust Total' of B on C
--compute sum label 'Full Total' of B on report

--select 'Prominent Shipments by Customer' from DUAL;
--select to_char(SYSDATE, 'DD/MM/YY    HH:MI:SS') from DUAL;


COLUMN NEWSEQ NEW_VALUE SEQNO


select 'S://redprairie/apps/home/dcsdba/reports/live/ECHO_OPEN_ORDERS_'||to_char(SYSDATE, '_YYYYMMDDHHMISS')||'.csv' NEWSEQ from DUAL;
--select 'live/ECHO_OPEN_ORDERS_'||to_char(SYSDATE, '_YYYYMMDDHHMISS')||'.csv' NEWSEQ from DUAL;

spool &SEQNO


select 'Order Ref,Status,Name,Type,Line ID,SKU,Description,Ordered,Allocated,Picked' from DUAL;

select
oh.order_id A,
oh.status,
oh.name,
oh.order_type,
--oh.num_lines,
ol.line_id,
ol.sku_id,
sku.description,
nvl(ol.qty_ordered,0),
nvl(ol.qty_tasked,0)+nvl(ol.qty_picked,0),
nvl(ol.qty_picked,0)
from order_header oh,order_line ol,sku
where oh.client_id='ECHO'
and oh.status not in ('Cancelled','Shipped')
and oh.order_type<>'RSR'
and ol.client_id='ECHO'
and ol.order_id=oh.order_id
and sku.client_id='ECHO'
and ol.sku_id=sku.sku_id
order by
oh.order_id,ol.line_id
/

spool off


select 'S://redprairie/apps/home/dcsdba/reports/live/RSR_OPEN_ORDERS_'||to_char(SYSDATE, '_YYYYMMDDHHMISS')||'.csv' NEWSEQ from DUAL;
--select 'live/RSR_OPEN_ORDERS_'||to_char(SYSDATE, '_YYYYMMDDHHMISS')||'.csv' NEWSEQ from DUAL;

spool &SEQNO


select 'Order Ref,Status,Name,Type,Line ID,SKU,Description,Ordered,Allocated,Picked' from DUAL;

select
oh.order_id A,
oh.status,
oh.name,
oh.order_type,
--oh.num_lines,
ol.line_id,
ol.sku_id,
sku.description,
nvl(ol.qty_ordered,0),
nvl(ol.qty_tasked,0)+nvl(ol.qty_picked,0),
nvl(ol.qty_picked,0)
from order_header oh,order_line ol,sku
where oh.client_id='ECHO'
and oh.status not in ('Cancelled','Shipped')
and oh.order_type='RSR'
and ol.client_id='ECHO'
and ol.order_id=oh.order_id
and sku.client_id='ECHO'
and ol.sku_id=sku.sku_id
order by
oh.order_id,ol.line_id
/

spool off
