SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

Select 'SB08 C Mail,Order Returns' from DUAL;
select 'Order No,Date,Customer Sent,Customer Name,SKU,Description,Qty,Condition,Exch/Ref,Notes,RTN Code' from DUAL;


column A format A10
column A2 format A10
column B format A10
column C format A32
column D format A10
column E format A40
column F format 999
column G format A10
column H format A10
column I format A40
column J format A10



set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



--ttitle  LEFT 'TML Retail - Open Orders Report ' RIGHT 'Printed ' report_date skip 2


--break on A skip 1

--compute sum label 'Grand Total' of QQQ on report
--compute sum label 'Total Units' of QQQ on HH

select
oh.ORDER_ID A,
to_char(it.Dstamp,'DD/MM/YY') A2,
oh.SHIP_DOCK B,
oh.NAME C,
it.SKU_ID D,
'"' || sku.description || '"' E,
it.UPDATE_QTY F,
it.CONDITION_ID G,
it.USER_DEF_NOTE_1 H,
it.NOTES I,
it.USER_DEF_NOTE_2 J
from order_header oh,inventory_transaction it,sku
where oh.CLIENT_ID='SB'
and it.reference_id like 'C%'
and it.reference_id=oh.order_id
and it.sku_id=sku.sku_id
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.CODE='Receipt'
order by
A,D
/
