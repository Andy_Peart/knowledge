SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'ALL,FULL,SHORT' from dual
union all
select "Tot unship" || ',' || "Tot full" || ',' || "Tot Short"
from (
with total as (
select count(order_id) as "tot"
from order_header
where status <> 'Shipped'
and client_id = 'SE'
), shorts as (
select count(order_id) as "error"
from order_header
where status <> 'Shipped'
and client_id = 'SE'
and order_id in (
select order_id
from order_line
where client_id = 'SE'
and qty_ordered - nvl(qty_tasked,0)-nvl(qty_picked,0) <> 0
group by order_id
)
)
select total."tot"              as "Tot unship"
, total."tot" - shorts."error"  as "Tot full"
, shorts."error"                as "Tot Short"
from total, shorts
)
/
--spool off