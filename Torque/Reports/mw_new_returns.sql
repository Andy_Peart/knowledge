SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 132


column A heading 'Reference' format A12
column B heading 'SKU ID' format A18
column C heading 'Qty' format 999999
column D heading 'Reason' format A32
column E heading 'Required' format A12
column F heading 'Condition' format A12
column G heading 'Date' format A14



--set termout off
--spool returns.csv

--Select 'MOUNTAIN Returns - &&2 to &&3' from DUAL;


select 'Reference,SKU,Qty,Reason,Required,Condition,Date,,' from DUAL;


select
Reference_id A,
''''||SKU_ID||'''' B,
Update_qty C,
User_def_note_1 D,
Notes E,
User_def_type_8 F,
to_char(Dstamp,'DD/MM/YYYY') G,
' '
from inventory_transaction
where client_id='MOUNTAIN'
and code='Receipt'
and user_def_note_2='Y'
and Dstamp>sysdate-1
order by
--to_char(Dstamp,'YYYY/MM/DD'),
Reference_id
/


--spool off
--set termout on
