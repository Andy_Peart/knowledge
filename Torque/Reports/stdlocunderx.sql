SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON

--spool stdlocunderx.csv

select 'SKU ID,Description,Colour,Location,Total Qty,Allocated Qty,Available Qty,Owner,Work Zone' from DUAL;



select
A||','||
B||','||
C||','||
D||','||
Q1||','||
Q2||','||
Q3||','||
Q4||','||
Q5
from (
    SELECT
    i.sku_id A,
    s.description B,
    s.colour C,
    i.location_id D,
    i.qty_on_hand AS Q1,
    i.qty_allocated AS Q2,
    i.qty_on_hand-NVL(i.qty_allocated, 0) AS Q3,
    i.owner_id as Q4,
    l.work_zone as Q5
    FROM inventory i, location l, sku s,(
        select sku_id SK, SUM(qty_on_hand) QQ
        FROM inventory
        WHERE client_id = '&&2'
        AND location_id NOT IN ('SUSPENSE', 'CONTAINER', 'UTOUT', 'UTFAULTY', 'UTIN', 'UTRET', 'UTWEB')
        group by sku_id
    )
    WHERE i.client_id = '&&2'
    AND s.client_id = '&&2'
    AND i.sku_id = s.sku_id
    AND i.location_id NOT IN ('SUSPENSE', 'CONTAINER', 'UTOUT', 'UTFAULTY', 'UTIN', 'UTRET', 'UTWEB')
    and i.sku_id=SK
    and l.location_id = i.location_id
    and i.site_id = l.site_id
    and QQ<&&3
    ORDER BY i.sku_id
)
/

--spool off
