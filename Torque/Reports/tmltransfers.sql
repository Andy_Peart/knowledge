SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','
SET UND OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 320
SET TRIMSPOOL ON

/* Column Headings and Formats */
COLUMN A heading "Client ID" FORMAT a6
COLUMN A1 heading "Client ID" FORMAT a6
COLUMN B heading "Supplier" FORMAT a10
COLUMN C heading "Pre Advice ID" FORMAT a16
COLUMN D heading "Line ID" FORMAT 999999
COLUMN E heading "Creation Date" FORMAT a14
COLUMN F heading "SKU" FORMAT a18
COLUMN G heading "Season" FORMAT a20
COLUMN H heading "Style" FORMAT a10
COLUMN J heading "Qual" FORMAT a14
COLUMN K heading "Size" FORMAT a8
COLUMN L heading "Qty Rec" FORMAT 999999
COLUMN L2 heading "Qty Shp" FORMAT 999999
COLUMN L3 heading "Diff" FORMAT 999999
COLUMN N heading "Colour" FORMAT a14



set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


break on report

compute sum label 'Totals' of L L2 L3 on report


--spool TMLdiff.csv

Select 'Date,From,To,Order ID,SKU ID,Received,Shipped,Diff,Product,Size,Colour' from DUAL;


select
E,A,A1,C,F,
sum(L) L,
sum(L2) L2,
sum(L3) L3,
G,
ZZZ,
CCC
from (SELECT
trunc(PAH.Last_Update_Date) E,
pah.client_id A,
ol.client_id A1,
ol.Order_ID C,
''''||substr(ol.SKU_ID,1,12)||'''' F,
nvl(PAL.qty_received,0) L,
nvl(ol.qty_shipped,0) L2,
nvl(PAL.qty_received,0)-nvl(ol.qty_shipped,0) L3,
U2||'-'||U1||'-'||U4 G,
ZZZ,
CCC
FROM Pre_Advice_Header PAH, Pre_Advice_Line PAL, (select 
sku_id SSS,
user_def_type_2 U2,
user_def_type_1 U1,
user_def_type_4 U4,
sku_size ZZZ,
colour CCC
from sku where client_id='TR'),order_line ol
WHERE PAH.Status='Complete' 
and PAH.client_id in ('T','TR')
and PAH.Last_Update_Date>sysdate-1
and PAH.supplier_id in ('WH1','WH2')
and PAH.Pre_Advice_ID = PAL.Pre_Advice_ID
and PAL.client_id in ('T','TR')
AND PAL.SKU_ID = SSS
and ol.client_id in ('T','TR')
and PAH.Pre_Advice_ID=ol.Order_ID
and PAL.SKU_ID=ol.SKU_ID
and PAL.LINE_ID=ol.LINE_ID)
group by
E,A,A1,C,F,
G,
ZZZ,
CCC
order by E,C,F
/

--spool off
