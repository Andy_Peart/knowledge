SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','
SET WRAP OFF


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 220

select 'Report data from ' || '&&2' || ' to ' || '&&3' from dual
union all
select 'Order No, Department, Category, Ident no, Description, Size, Colour, Retail Price, Cost Price, Sales Units, Sales Value, Stock Units, Stock Value' from dual
union all
select  order_no || ',' || dept || ',' || categ || ',' || sku_id || ',' || description || ',' || sku_size || ',' || colour || ',' || retail_price || ',' || cost_price || ',' || sales_units || ',' || sales_value || ',' || stock_units
from (
select 
'"=""' || it.reference_id || '"""' order_no, 
sku.user_def_type_2 dept, sku.user_def_type_3 categ, 
'"=""' || sku.sku_id || '"""' sku_id,
sku.description, sku.sku_size, sku.colour,
sku.user_def_num_1 retail_price, sku.user_def_num_2 cost_price, sum(it.update_qty) sales_units, sum(it.update_qty*sku.user_def_num_1) sales_value,
inv.stock_units
from inventory_transaction it inner join sku on sku.client_id = it.client_id and it.sku_id = sku.sku_id
left join 
(
select sku_id, sum(nvl(qty_on_hand,0))-sum(nvl(qty_allocated,0)) stock_units
from inventory
where client_id = 'HP'
group by sku_id
) inv
on inv.sku_id = it.sku_id and inv.sku_id = sku.sku_id
where it.client_id = 'HP'
and it.code = 'Shipment'
and it.update_qty>0
and it.dstamp between to_date('&&2','DD-MON-YYYY') and to_date('&&3','DD-MON-YYYY')
group by 
it.reference_id, sku.user_def_type_2, sku.user_def_type_3, sku.sku_id, sku.description, 
sku.sku_size, sku.colour, sku.user_def_num_1, sku.user_def_num_2, inv.stock_units
order by 1
)
/