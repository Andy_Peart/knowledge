SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON

select 'Order Header and Lines of incompleted orders Data Extract - for client ID &&2 from &&3 to &&4 - as at  ' 
    || to_char(sysdate,'dd/mm/yy  hh24:mi:ss') from dual;

with containers as (
  select reference_id, count(*) containers
  from (
    select reference_id, container_id, count(*)
    from inventory_transaction
    where client_id = '&&2'
    and dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+5
    and code = 'Shipment'
    group by reference_id, container_id
    order by 1,2
  )
  group by reference_id
  union all
  select reference_id, count(*) containers
  from (
    select reference_id, container_id, count(*)
    from inventory_transaction_archive
    where client_id = '&&2'
    and dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+5
    and code = 'Shipment'
    group by reference_id, container_id
    order by 1,2
  )
  group by reference_id
),
T_arch_all as (
	select oh.creation_date, oh.client_id
	||','|| oh.work_group
	||','|| oh.consignment
	||','|| to_char(oh.creation_date, 'DD/MM/YYYY')
	||','|| to_char(oh.creation_date, 'HH24:MI')
	||','|| to_char(oh.last_update_date, 'DD/MM/YYYY')  
	||','|| to_char(oh.last_update_date, 'HH24:MI') 
	||','|| to_char(oh.shipped_date, 'DD/MM/YYYY')  
	||','|| to_char(oh.shipped_date, 'HH24:MI') 
	||','|| case  when oh.client_id = 'SS' and oh.order_type = 'RETAIL' then oh.postcode 
              when oh.client_id = 'PR' then oh.customer_id || oh.name
              else oh.customer_id end
	||','|| oh.order_id
	||','|| oh.order_type
	||','|| oh.country 
	||','|| oh.dispatch_method
	||','|| to_char(oh.priority)
	||','|| oh.ship_dock
	||','|| oh.status
	||','|| to_char(1) 
	||','|| to_char(count(ol.line_id)) 
	||','|| to_char(sum(ol.qty_ordered))
	||','|| to_char(sum(nvl(ol.qty_shipped,0)))
	||','|| case when oh.status = 'Shipped' then to_char(sum(nvl(ol.qty_ordered,0)-nvl(ol.qty_shipped,0)))  else '0'  end 
	||','|| to_char(sum(nvl(ol.qty_tasked,0))) 
  ||','|| C.containers 
  ||','|| oh.carrier_id
  ||','|| oh.signatory
	||','|| cl.user_def_type_1
	||','|| case when oh.consignment_grouping_id like '%TROLLEY%' then 'TROLLEY' when oh.consignment_grouping_id like '%SINGLE%' then 'Single' end
	||','|| to_char(oh.creation_date, 'HH24')
	||','|| to_char(oh.shipped_date, 'HH24') 
	||','|| max(oh.freight_cost)
	||','|| sum(ol.line_value)
	arch_columns
	from order_header_archive oh inner join order_line_archive ol on oh.order_id=ol.order_id and oh.client_id=ol.client_id
  left join containers c on c.reference_id = oh.order_id
	inner join client cl on cl.client_id = oh.client_id
	where oh.client_id = '&&2'
	and oh.status <> 'Cancelled'
	and oh.creation_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1  
	group by oh.client_id
	, oh.work_group
	, oh.consignment
	, to_char(oh.creation_date, 'DD/MM/YYYY')
	, to_char(oh.creation_date, 'HH24:MI')
	, to_char(oh.last_update_date, 'DD/MM/YYYY')  
	, to_char(oh.last_update_date, 'HH24:MI') 
	, to_char(oh.shipped_date, 'DD/MM/YYYY')  
	, to_char(oh.shipped_date, 'HH24:MI') 
	, oh.customer_id
	, oh.order_id
	, oh.order_type
	, oh.dispatch_method
	, to_char(oh.priority)
	, oh.ship_dock
	, oh.status
	, to_char(1)
	, oh.creation_date
  , c.containers
  , oh.carrier_id
  , oh.signatory
  , oh.country
	, cl.user_def_type_1
	, oh.consignment_grouping_id
	, to_char(oh.creation_date, 'HH24')
	, to_char(oh.shipped_date, 'HH24')
	, oh.name
    , oh.postcode 
	order by oh.creation_date
),
T_arch_incomplete as (
	select oh.creation_date, oh.client_id
	||','|| oh.work_group
	||','|| oh.consignment
	||','|| to_char(oh.creation_date, 'DD/MM/YYYY')
	||','|| to_char(oh.creation_date, 'HH24:MI')
	||','|| to_char(oh.last_update_date, 'DD/MM/YYYY')  
	||','|| to_char(oh.last_update_date, 'HH24:MI') 
	||','|| to_char(oh.shipped_date, 'DD/MM/YYYY')  
	||','|| to_char(oh.shipped_date, 'HH24:MI') 
	||','|| case  when oh.client_id = 'SS' and oh.order_type = 'RETAIL' then oh.postcode 
              when oh.client_id = 'PR' then oh.customer_id || oh.name
              else oh.customer_id end
	||','|| oh.order_id
	||','|| oh.order_type
	||','|| oh.country
	||','|| oh.dispatch_method
	||','|| to_char(oh.priority)
	||','|| oh.ship_dock
	||','|| oh.status
	||','|| to_char(1) 
	||','|| to_char(count(ol.line_id)) 
	||','|| to_char(sum(ol.qty_ordered))
	||','|| to_char(sum(nvl(ol.qty_shipped,0)))
	||','|| case when oh.status = 'Shipped' then to_char(sum(nvl(ol.qty_ordered,0)-nvl(ol.qty_shipped,0)))  else '0'  end 
	||','|| to_char(sum(nvl(ol.qty_tasked,0)))
  ||','|| C.containers 
  ||','|| oh.carrier_id
  ||','|| oh.signatory
	||','|| cl.user_def_type_1
	||','|| case when oh.consignment_grouping_id like '%TROLLEY%' then 'TROLLEY' when oh.consignment_grouping_id like '%SINGLE%' then 'Single' end
	||','|| to_char(oh.creation_date, 'HH24')
	||','|| to_char(oh.shipped_date, 'HH24')
	||','|| max(oh.freight_cost)
	||','|| sum(ol.line_value)	arch_columns
	from order_header_archive oh inner join order_line_archive ol on oh.order_id=ol.order_id and oh.client_id=ol.client_id
  left join containers c on c.reference_id = oh.order_id
	inner join client cl on cl.client_id = oh.client_id
	where oh.client_id = '&&2'
	and oh.status <> 'Cancelled'
	and (oh.creation_date < to_date('&&3', 'DD-MON-YYYY') OR oh.creation_date  > to_date('&&4', 'DD-MON-YYYY')+1)
	and oh.status in ('Allocated','In Progress','Picked','Released')
	group by oh.client_id
	, oh.work_group
	, oh.consignment
	, to_char(oh.creation_date, 'DD/MM/YYYY')
	, to_char(oh.creation_date, 'HH24:MI')
	, to_char(oh.last_update_date, 'DD/MM/YYYY')  
	, to_char(oh.last_update_date, 'HH24:MI') 
	, to_char(oh.shipped_date, 'DD/MM/YYYY')  
	, to_char(oh.shipped_date, 'HH24:MI') 
	, oh.customer_id
	, oh.order_id
	, oh.order_type
	, oh.dispatch_method
	, to_char(oh.priority)
	, oh.ship_dock
	, oh.status
	, to_char(1)
	, oh.creation_date
  , c.containers
  , oh.carrier_id
  , oh.signatory
  , oh.country
	, cl.user_def_type_1
	, oh.consignment_grouping_id
	, to_char(oh.creation_date, 'HH24')
	, to_char(oh.shipped_date, 'HH24')
	, oh.name
    , oh.postcode 
	order by oh.creation_date
),
T_current_all as (
	select oh.creation_date, oh.client_id
	||','|| oh.work_group
	||','|| oh.consignment
	||','|| to_char(oh.creation_date, 'DD/MM/YYYY')
	||','|| to_char(oh.creation_date, 'HH24:MI')
	||','|| to_char(oh.last_update_date, 'DD/MM/YYYY')  
	||','|| to_char(oh.last_update_date, 'HH24:MI') 
	||','|| to_char(oh.shipped_date, 'DD/MM/YYYY')  
	||','|| to_char(oh.shipped_date, 'HH24:MI') 
	||','|| case  when oh.client_id = 'SS' and oh.order_type = 'RETAIL' then oh.postcode 
              when oh.client_id = 'PR' then oh.customer_id || oh.name
              else oh.customer_id end
	||','|| oh.order_id
	||','|| oh.order_type
	||','|| oh.country
	||','|| oh.dispatch_method
	||','|| to_char(oh.priority)
	||','|| oh.ship_dock
	||','|| oh.status
	||','|| to_char(1) 
	||','|| to_char(count(ol.line_id)) 
	||','|| to_char(sum(ol.qty_ordered))
	||','|| to_char(sum(nvl(ol.qty_shipped,0)))
	||','|| case when oh.status = 'Shipped' then to_char(sum(nvl(ol.qty_ordered,0)-nvl(ol.qty_shipped,0)))  else '0'  end 
	||','|| to_char(sum(nvl(ol.qty_tasked,0))) 
  ||','|| C.containers 
  ||','|| oh.carrier_id
  ||','|| oh.signatory
	||','|| cl.user_def_type_1
	||','|| case when oh.consignment_grouping_id like '%TROLLEY%' then 'TROLLEY' when oh.consignment_grouping_id like '%SINGLE%' then 'Single' end
	||','|| to_char(oh.creation_date, 'HH24')
	||','|| to_char(oh.shipped_date, 'HH24')
	||','|| max(oh.freight_cost)
	||','|| sum(ol.line_value)	current_columns
	from order_header oh inner join order_line ol on oh.order_id=ol.order_id and oh.client_id=ol.client_id
  left join containers c on c.reference_id = oh.order_id
	inner join client cl on cl.client_id = oh.client_id
	where oh.client_id = '&&2'
	and oh.status <> 'Cancelled'
	and oh.creation_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
	group by oh.client_id
	, oh.work_group
	, oh.consignment
	, to_char(oh.creation_date, 'DD/MM/YYYY')
	, to_char(oh.creation_date, 'HH24:MI')
	, to_char(oh.last_update_date, 'DD/MM/YYYY')  
	, to_char(oh.last_update_date, 'HH24:MI') 
	, to_char(oh.shipped_date, 'DD/MM/YYYY')  
	, to_char(oh.shipped_date, 'HH24:MI') 
	, oh.customer_id
	, oh.order_id
	, oh.order_type
	, oh.dispatch_method
	, to_char(oh.priority)
	, oh.ship_dock
	, oh.status
	, to_char(1)
	, oh.creation_date
  , c.containers
  , oh.carrier_id
  , oh.signatory
  , oh.country
	, cl.user_def_type_1
	, oh.consignment_grouping_id
	, to_char(oh.creation_date, 'HH24')
	, to_char(oh.shipped_date, 'HH24')
	, oh.name
    , oh.postcode 
	ORDER BY oh.creation_date
),
T_current_incomplete as (
	select oh.creation_date, oh.client_id
	||','|| oh.work_group
	||','|| oh.consignment
	||','|| to_char(oh.creation_date, 'DD/MM/YYYY')
	||','|| to_char(oh.creation_date, 'HH24:MI')
	||','|| to_char(oh.last_update_date, 'DD/MM/YYYY')  
	||','|| to_char(oh.last_update_date, 'HH24:MI') 
	||','|| to_char(oh.shipped_date, 'DD/MM/YYYY')  
	||','|| to_char(oh.shipped_date, 'HH24:MI') 
	||','|| case  when oh.client_id = 'SS' and oh.order_type = 'RETAIL' then oh.postcode 
              when oh.client_id = 'PR' then oh.customer_id || oh.name
              else oh.customer_id end
	||','|| oh.order_id
	||','|| oh.order_type
	||','|| oh.country
	||','|| oh.dispatch_method
	||','|| to_char(oh.priority)
	||','|| oh.ship_dock
	||','|| oh.status
	||','|| to_char(1) 
	||','|| to_char(count(ol.line_id)) 
	||','|| to_char(sum(ol.qty_ordered))
	||','|| to_char(sum(nvl(ol.qty_shipped,0)))
	||','|| case when oh.status = 'Shipped' then to_char(sum(nvl(ol.qty_ordered,0)-nvl(ol.qty_shipped,0)))  else '0'  end 
	||','|| to_char(sum(nvl(ol.qty_tasked,0)))
  ||','|| C.containers 
  ||','|| oh.carrier_id
  ||','|| oh.signatory
	||','|| cl.user_def_type_1 
	||','|| case when oh.consignment_grouping_id like '%TROLLEY%' then 'TROLLEY' when oh.consignment_grouping_id like '%SINGLE%' then 'Single' end
	||','|| to_char(oh.creation_date, 'HH24')
	||','|| to_char(oh.shipped_date, 'HH24')
	||','|| max(oh.freight_cost)
	||','|| sum(ol.line_value)	current_columns
	from order_header oh inner join order_line ol on oh.order_id=ol.order_id and oh.client_id=ol.client_id
  left join containers c on c.reference_id = oh.order_id
	inner join client cl on cl.client_id = oh.client_id
	where oh.client_id = '&&2'
	and oh.status <> 'Cancelled'
	and (oh.creation_date < to_date('&&3', 'DD-MON-YYYY') OR oh.creation_date  > to_date('&&4', 'DD-MON-YYYY')+1)
	and oh.status in ('Allocated','In Progress','Picked','Released')
	group by oh.client_id
	, oh.work_group
	, oh.consignment
	, to_char(oh.creation_date, 'DD/MM/YYYY')
	, to_char(oh.creation_date, 'HH24:MI')
	, to_char(oh.last_update_date, 'DD/MM/YYYY')  
	, to_char(oh.last_update_date, 'HH24:MI') 
	, to_char(oh.shipped_date, 'DD/MM/YYYY')  
	, to_char(oh.shipped_date, 'HH24:MI') 
	, oh.customer_id
	, oh.order_id
	, oh.order_type
	, oh.dispatch_method
	, to_char(oh.priority)
	, oh.ship_dock
	, oh.status
	, to_char(1)
	, oh.creation_date
  , c.containers
  , oh.carrier_id
  , oh.signatory
  , oh.country
	, cl.user_def_type_1
	, oh.consignment_grouping_id
	, to_char(oh.creation_date, 'HH24')
	, to_char(oh.shipped_date, 'HH24')
	, oh.name
    , oh.postcode 
	ORDER BY oh.creation_date
)
select 
'client, work group, consignment, creation date, creation time, last update date, last update time, shipped date, shipped time, customer id, order, order type, country, dispatch method, priority, ship dock, status, orders, lines, order qty, shipped qty, short, allocated qty,containers,carrier,signatory,site,type,creation hour,shipped hour,Delivery Cost,Cost of Goods'
from dual
union all
SELECT X FROM (
  SELECT a1.X
  FROM (
    SELECT CREATION_DATE Y, ARCH_COLUMNS X FROM T_ARCH_ALL
    union all
    SELECT CREATION_DATE Y, ARCH_COLUMNS X FROM T_ARCH_INCOMPLETE
    union all
    SELECT CREATION_DATE Y, CURRENT_COLUMNS X FROM T_CURRENT_ALL
    union all
    SELECT CREATION_DATE Y, CURRENT_COLUMNS X FROM T_CURRENT_INCOMPLETE
  ) a1
  ORDER BY A1.Y
)
/
