SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 130

column A heading 'SKU ID' format A18
column B heading 'Colour' format A18
column C heading 'Size' format A8
column D heading 'Description' format A40
column E heading 'Location' format A12
column F heading 'Stock' format 99999999
column G heading 'Last Count' format A12


--set termout off
--spool Under10s.csv


select 'Location,SKU ID,Colour,Size,Description,Available,Last Count,,' from DUAL;
 
--set TERMOUT OFF
--column curdate NEW_VALUE report_date
--select to_char(SYSDATE, 'DAY, DD MONTH, YYYY') curdate from DUAL;
--set TERMOUT ON



--compute sum label 'Totals' of B C D E F on report


select
i.location_id E,
''''||sku.sku_id||'''' A,
sku.colour B,
sku.sku_size C,
sku.description D,
i.qty_on_hand - i.qty_allocated F,
to_char(i.count_dstamp,'DD/MM/YYYY') G,
' '
from inventory i,sku,
(select
sku_id BBB,
sum(qty_on_hand - qty_allocated) CCC
from inventory
where client_id='MOUNTAIN'
and location_id not in ('SUSPENSE','DESPATCH')
group by
sku_id)
where i.client_id='MOUNTAIN'
and CCC<10
and i.location_id not in ('SUSPENSE','DESPATCH')
and i.sku_id=BBB
and sku.sku_id=BBB
and sku.client_id='MOUNTAIN'
order by
i.location_id,
sku.sku_id
/


--spool off
--set termout on
