SET FEEDBACK OFF
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

select 'Order Status, Order Number, SKU, Description, Product Type, Colour, Size, Qty Ordered, Allocated, Order Date, Name, Email, Delivery, Country'
from dual
union all
select * from (SELECT
oh.status ||','||
oh.order_id ||','||
ol.sku_id ||','||
sku.description ||','||
sku.product_group ||','||
sku.colour ||','||
sku.sku_size ||','||
ol.qty_ordered ||','||
nvl(QQ3,0) ||','||
trunc(oh.order_date) ||','||
oh.contact ||','||
oh.contact_email ||','||
oh.priority ||','||
oh.country  
FROM order_header oh, order_line ol, sku,(select
sku_id SS,
sum(qty_on_hand-qty_allocated) QQ
from inventory
where client_id='WL'
and location_id='BOND'
group by
sku_id),(select
sku_id SS2,
sum(qty_on_hand-qty_allocated) QQ2
from inventory iv,location ll
where iv.client_id='WL'
and iv.location_id<>'BOND'
and iv.location_id=ll.location_id
and ll.loc_type='Receive Dock'
group by
sku_id),(select
sku_id SS3,
sum(qty_allocated) QQ3
from inventory
where client_id='WL'
group by
sku_id),(select
sku_id SS4,
sum(qty_on_hand-qty_allocated) QQ4
from inventory iv,location ll
where iv.client_id='WL'
and iv.location_id=ll.location_id
and ll.loc_type='Ship Dock'
group by
sku_id),(select
sku_id SS5,
sum(qty_on_hand-qty_allocated) QQ5
from inventory iv,location ll
where iv.client_id='WL'
and iv.location_id=ll.location_id
and iv.site_id=ll.site_id
and (ll.loc_type='Suspense'
or (ll.loc_type='Tag-FIFO'
and iv.lock_status in ('Locked','OutLocked')))
group by
sku_id),(select
pl.sku_id S2,
max(ph.due_dstamp) D2
from pre_advice_header ph,pre_advice_line pl
where ph.client_id='WL'
and ph.client_id=pl.client_id
and ph.pre_advice_id=pl.pre_advice_id
and nvl(pl.qty_received,0)=0
group by
pl.sku_id)
WHERE oh.order_id=ol.order_id
AND (oh.order_type='&&2' OR '&&2' IS NULL)
AND oh.status NOT IN ('Cancelled','Shipped')
--AND oh.consignment = 'SHORT'
AND oh.client_id = 'WL'
AND ol.client_id = 'WL'
AND nvl(ol.qty_ordered,0)-nvl(ol.qty_tasked,0)-nvl(ol.qty_picked,0)<>0
and ol.sku_id=sku.sku_id
and sku.client_id='WL'
and ol.sku_id=SS (+)
and ol.sku_id=SS2 (+)
and ol.sku_id=SS3 (+)
and ol.sku_id=SS4 (+)
and ol.sku_id=SS5 (+)
and ol.sku_id=S2 (+)
GROUP BY
oh.status,
oh.order_id,
ol.sku_id,
sku.description,
sku.product_group,
sku.colour,
sku.sku_size,
ol.qty_ordered,
QQ3,
oh.order_date,
oh.contact,
oh.contact_email,
oh.priority,
oh.country
ORDER BY ol.sku_id)
/
