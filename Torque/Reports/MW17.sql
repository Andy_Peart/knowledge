SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','
SET WRAP OFF


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 900

column A heading 'Order' format A8
column B heading 'Receipt Date' format A12
column C heading 'Qty' format 99999
column D heading 'Order Line' format 99999
column F heading 'SKU ID' format A18
column G heading 'Description' format A40
column H heading 'Colour' format A12
column J heading 'Size' format A8
column JJ heading 'Notes' format A50
column K heading 'Check' format A40
column L heading 'Condition' format A8
column M heading 'Customer' format A8
column Q heading 'Name' format A24
column R heading 'Customer Ref' format A12
column S heading 'Order Type' format A8
column T heading 'T2T' format A12
column U heading 'User ID' format A12


select 'Order No, Receipt Date,Qty,Order Line,Sku,Description,Colour,Size,Notes,Check SKU on order,Condition,Customer ID,Name,Customer Ref,Order Type,T2T,User ID' from DUAL;

--break on D dup

--compute sum label 'Order Total' of U V W on D


select
it.reference_id A,
to_char(it.Dstamp,'DD-Mon-YYYY') B,
it.update_qty C,
nvl(ol.line_id,0) D,
''''||it.sku_id||'''' F,
sku.description G,
sku.colour H,
sku.sku_size J,
it.notes JJ,
DECODE (ol.Client_id,'MOUNTAIN','','Please Check (SKU is not on this Order)') K,
it.condition_id L,
oh.customer_id M,
oh.name Q,
oh.purchase_order R,
oh.order_type S,
sku.user_def_type_1 T,
it.user_id U
from inventory_transaction it,order_header oh,order_line ol,sku
where it.client_id='MOUNTAIN'
and it.code='Receipt'
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.reference_id not like 'MOR%'
and it.reference_id=oh.order_id
and oh.client_id='MOUNTAIN'
and it.reference_id=ol.order_id (+)
and it.sku_id=ol.sku_id (+)
and it.sku_id=sku.sku_id
order by
A
/




