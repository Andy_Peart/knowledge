SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 999
SET TRIMSPOOL ON

column A heading 'Date' format A12
column A1 heading 'Order ID' format A24
column A2 heading 'SKU ID' format A20
column A3 heading 'Description' format A40
column F heading 'Stock' format 999999
column B heading 'Shortage' format 999999
column G heading 'Variance' format 999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

COLUMN NEWSEQ NEW_VALUE SEQNO

--break on report
--compute sum label 'Total' of B C D G on report

break on A1 dup skip 1 on report
compute sum label 'Total' of B C D G on A1


select 'BERWIN Shortages - '||to_char(SYSDATE, 'DD/MM/YY  HH24:MI:SS') from DUAL;

select 'Order No,SKU,Description,Stock,Ordered,Allocated,Shortage,Variance' from DUAL;


select
A1,A2,A3,F,B,C,D,J G
from (
    select distinct oh.order_id A1, ol.line_id, ol.sku_id A2, sku.description A3, ol.qty_ordered B,
    nvl(ol.qty_tasked,0) C, ol.qty_ordered-nvl(ol.qty_tasked,0) D, nvl(Q1,0) F,nvl(Q2,0) J
    from order_header oh,order_line ol,sku,(
        select sku_id SSS, sum(qty_on_hand) Q1, sum(qty_on_hand-qty_allocated) Q2
        from inventory
        where client_id='BW'
        group by sku_id
        )
    where oh.client_id='BW'
        and (oh.order_id='&&2' or oh.consignment = '&&3')
        and ol.order_id=oh.order_id
        and ol.client_id=oh.client_id
        and ol.sku_id=sku.sku_id
        and sku.client_id='BW'
        and ol.sku_id=SSS (+)
    )
order by
A1,A2
/