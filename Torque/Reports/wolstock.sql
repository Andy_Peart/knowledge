SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 

column A heading 'Category' format A11
column B heading 'Qty on Hand' format 9999999
column C heading 'Qty Allocated' format 9999999
column D heading 'Qty Available' format 9999999
column F heading 'Number of SKUs' format 9999999

set heading off
set pagesize 0

select 'Stock Holding Report for client WOL on ',to_char(SYSDATE, 'DD/MM/YY    HH:MI:SS') from DUAL;


select '' from DUAL;

select '                On Hand     Allocated     Available   Number of SKUs' from DUAL;


select
'Stock' A,
B,
C,
B-C D,
FF
from (select
nvl(sum(i.qty_on_hand),0) B,
nvl(sum(i.qty_allocated),0) C,
count(distinct sku_id) FF
from inventory i
where i.client_id like 'WOL')
/
