SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON

select 'Order ID,Order date,Order Time,Creation date,Creation Time,Carrier,Consignment,Status,Qty ordered,Alt qty ordered,Jeans,Formals,TShirt' from dual
union all
select order_id || ',' || order_date || ',' || order_time || ',' || creation_date || ',' || creation_time || ',' || carrier_id || ',' || consignment  || ',' || 
status || ',' || qty_ordered || ',' || qty_alt  || ',' || qty_jeans || ',' || qty_formal || ',' || TShirt_Kit
from
(
    select to_char(oh.order_date,'DD-MM-YYYY') order_date,
    to_char(oh.order_date,'HH24:MI:SS') order_time, 
    to_char(oh.creation_date,'DD-MM-YYYY') creation_date,
    to_char(oh.creation_date,'HH24:MI:SS') creation_time, 
    oh.order_id, oh.status, sum(ol.qty_ordered) qty_ordered, 
    sum(case  when ol.user_def_note_2 like '%Finished to Order' then qty_ordered else 0 end) qty_alt, oh.carrier_id, oh.consignment,
    sum(case when ol.sku_id like 'JE-DNM%' then qty_ordered else 0 end) qty_jeans,
    sum(case when ol.sku_id like 'FO%' then qty_ordered else 0 end) qty_formal,
    sum(case when ol.sku_id like 'TS-CSS-KIT%' then qty_ordered else 0 end) TShirt_Kit
    from order_header oh inner join order_line ol on oh.order_id = ol.order_id and oh.client_id = ol.client_id
    where oh.client_id = 'SP'
    and oh.status not in ('Shipped','Cancelled')
    group by oh.order_date, oh.creation_date, oh.order_id, oh.status, oh.carrier_id, oh.consignment
    order by 2,1
)
/