/******************************************************************************/
/*                                                                            */
/*                            Elite                                           */
/*                                                                            */
/*     FILE NAME  :   ckatepick.sql                                           */
/*                                                                            */
/*     DESCRIPTION:    Datastream for Curvy Kate Pick Note                    */
/*                                                                            */
/*   DATE     BY   PROJ     ID         DESCRIPTION                            */
/*   ======== ==== ======   ========   =============                          */
/*   05/07/10 RW   CK                  Pick Note for Curvy Kate               */
/*                                                                            */
/******************************************************************************/

/* Input Parameters are as follows */
-- 1)  Order ID
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES


SET PAGESIZE 0
SET VERIFY OFF
SET FEEDBACK OFF
SET TAB OFF

 
-- Set the line length to the maximum possible, but trim 
-- trailing spaces for both screen output and spool files
SET LINESIZE 320
SET WRAP ON
SET TRIMOUT ON
SET TRIMSPOOL ON

/* Define the format of the output dates */
DEFINE	OutputDateFormat = 'DD-MON-YYYY HH24:MI:SS'
COLUMN	sorter	NOPRINT

select distinct rtrim (oh.order_id) sorter,
'DSTREAM_CKATE_PICK_HDR' ||'|'||
rtrim (oh.order_id) ||'|'||
--rtrim (ad.name) ||'|'||
rtrim (oh.name) ||'|'||
rtrim (trunc(oh.order_date)) ||'|'||
rtrim (oh.num_lines)
from order_header oh
where oh.client_id = 'CK'
and (oh.order_id = '&&2'
or oh.consignment='&&2')
--and oh.customer_id = ad.address_id
--and oh.user_def_type_2 = ad.address_id
union all
select distinct rtrim (REF) sorter,
'DSTREAM_CKATE_PICK_LINE' ||'|'||
rtrim (SSS) ||'|'||
rtrim (s.description) ||'|'||
rtrim (s.colour) ||'|'||
rtrim (s.ean) ||'|'||
rtrim (s.sku_size) ||'|'||
rtrim (LLL) ||'|'||
rtrim (QQQ) 
from (select
it.reference_id REF,
it.sku_id SSS,
it.from_loc_id LLL,
sum(DECODE(it.code,'Allocate',update_qty,0-update_qty)) QQQ 
from inventory_transaction it,order_header oh
where it.client_id = 'CK'
and oh.client_id = 'CK'
and it.reference_id = oh.order_id
and (oh.order_id = '&&2'
or oh.consignment='&&2')
and it.code in ('Allocate')
--and it.code in ('Deallocate','Allocate')
group by 
it.reference_id,
it.sku_id,
it.from_loc_id),sku s
where SSS = s.sku_id
and QQQ>0
order by 1,2;

exit;


