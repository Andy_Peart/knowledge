SET FEEDBACK OFF
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON



/* Clear previous settings*/
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

column A heading 'Category' format A16
column B heading 'Quantity' format 9999999

select to_char(SYSDATE,'DD/MM/YY HH:MI:SS') GVRELOCATECOMBINED_REPORT from DUAL;

SET PAGESIZE 0

select '' from DUAL
select '' from DUAL
select 'PUTAWAY' from DUAL

(select
'QTY OF SKU G RELOCATED' A,
NVL(sum (i.update_qty),0) B
from inventory_transaction i, location l, sku s 
where i.client_id = 'GV'
and s.sku_id like ('G%')
and s.sku_id = i.sku_id (+)
and i.from_loc_id ='AQL'
and i.to_loc_id = l.location_id
and l.zone_1 ='GIVE'
and i.code ='Relocate'
and l.site_id ='GIVE'
and i.dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1)
/


select '' from DUAL



select ''QTY OF SKU C RELOCATED'||chr(13)||'QTY OF SKU U RELOCATED'||chr(13)||'QTY OF SKU U and C RELOCATED'||chr(13),
X||chr(13)||Y||chr(13)||(X+Y)
from
(select
NVL(sum (i.update_qty),0) X
from inventory_transaction i, location l, sku s 
where i.client_id = 'GV'
and s.sku_id like ('C%')
and s.sku_id = i.sku_id (+)
and i.from_loc_id ='AQL'
and i.to_loc_id = l.location_id
and l.zone_1 ='GIVE'
and i.code ='Relocate'
and l.site_id ='GIVE'
and i.dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1),

(select
nvl(sum (i.update_qty),0) Y
from inventory_transaction i, location l, sku s 
where i.client_id = 'GV'
and s.sku_id like ('U%')
and s.sku_id = i.sku_id (+)
and i.from_loc_id ='AQL'
and i.to_loc_id = l.location_id
and l.zone_1 ='GIVE'
and i.code ='Relocate'
and l.site_id ='GIVE'
and i.dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1)
/
