set feedback off
set pagesize 70
set linesize 150
set verify off
set colsep ' '



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

 

column A heading 'Location' format A11
column B heading 'SKU' format A20
column C heading 'Description' format A40
column C2 heading 'Colour' format A12
column D heading 'Qty on Hand' format 999999
column D1 heading 'Qty Allocated' format 999999
column E heading '  Moved' format A14

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

set und '-'
set heading on
set newpage 0

ttitle  LEFT 'Stock Units by Location Range from &&3 to &&4 - for Client &&2 as at ' report_date RIGHT 'Page:' FORMAT 999 SQL.PNO skip 4

break on report

compute sum label 'Total' of D on report

break on row skip 1

select
i.sku_id B,
Sku.description C,
substr(Sku.colour,1,12) C2,
i.location_id A,
sum(i.qty_on_hand) D,
sum(i.qty_allocated) D1,
'  ____________' E
from inventory i inner join sku on i.sku_id=sku.sku_id and i.client_id=sku.client_id
where i.client_id like '&&2'
and location_id between '&&3' and '&&4'
and i.sku_id like '&&5%'
group by
i.sku_id,
Sku.description,
substr(Sku.colour,1,12),
i.location_id 
order by i.location_id,i.sku_id
/

ttitle  LEFT ' '
set heading off
break on row skip 0
set newpage 1

select
'Location Count =',
count (*) D
from (select distinct location_id from inventory
where client_id like '&&2'
and location_id between '&&3' and '&&4'
and sku_id like '&&5%')
/


select
'     SKU Count =',
count (*) D
from (select distinct sku_id from inventory
where client_id like '&&2'
and location_id between '&&3' and '&&4' 
and sku_id like '&&5%')
/

select
'    Unit Count =',
nvl(sum(qty_on_hand),0) D
from inventory
where client_id like '&&2'
and location_id between '&&3' and '&&4' 
and sku_id like '&&5%'
/

