SET FEEDBACK OFF             
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 300
SET TRIMSPOOL ON


--spool mwlastpicked.csv

--select to_char(SYSDATE, 'DD/MM/YY  HH:MI:SS') curdate from DUAL;

select 'Sku Code,Description,LPG,Qty,Earliest Date Received,Latest Date Received,Date Last Picked,Locations' from dual;



select
''''||PA||''','||
DD||','||
LPG||','||
QQ||','||
trunc(RD1)||','||
trunc(RD2)||','||
PB||','||
LLL||','||
' ' from (select
i.sku_id PA,
i.description DD,
nvl(sku.user_def_type_8,'ZZZZ') LPG,
min(Receipt_dstamp) RD1,
max(Receipt_dstamp) RD2,
sum(i.qty_on_hand) QQ,
nvl(BB,'01-jan-01') PB,
LISTAGG(i.location_id||'('||i.qty_on_hand||')', ':') WITHIN GROUP (ORDER BY i.location_id) LLL
from inventory i,location L,sku,(select
PP,
max(BB) BB
from (select
i.Sku_id PP,
max(trunc(i.dstamp)) BB
from inventory_transaction i
where i.client_id = 'MOUNTAIN'
and i.code = 'Pick'
and elapsed_time>0
group by i.sku_id
union
select
i.Sku_id PP,
max(trunc(i.dstamp)) BB
from inventory_transaction_archive i
where i.client_id = 'MOUNTAIN'
and i.code = 'Pick'
and elapsed_time>0
group by i.sku_id)
group by PP)
where i.client_id = 'MOUNTAIN'
and i.Sku_id=PP (+)
and i.location_id=L.location_id
and i.site_id=L.site_id
and L.loc_type='Tag-FIFO'
and i.sku_id=sku.sku_id
and sku.client_id = 'MOUNTAIN'
group by i.sku_id,i.description,nvl(sku.user_def_type_8,'ZZZZ'),nvl(BB,'01-jan-01'))
order by 1
/

--select to_char(SYSDATE, 'DD/MM/YY  HH:MI:SS') curdate from DUAL;

--spool off
