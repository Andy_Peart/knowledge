SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','
SET WRAP OFF


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON

column F1 heading 'Location' format A20
column F2 heading 'SKU' format A10
column F3 heading 'Units' format A10
column F4 heading 'GRN' format A10
column F5 heading 'Receipt date' format A20
column F6 heading 'Tag' format A10
column F7 heading 'GRN Status' format A15
column F8 heading 'Desciption' format A30
column F9 heading 'Color' format A10
column F10 heading 'Type' format A10


--set termout off
--spool bk_comp.csv



select 'Location, SKU, Units, GRN, Receipt date, Tag, GRN Status, Description, Color, Type' from DUAL
union all
select F1 || ',' || F2 || ',' || F3 || ',' || F4 || ',' || F5 || ',' || F6 || ',' || F7 || ',' || F8 || ',' || F9 || ',' || F10
from (
SELECT 
  INV.location_id F1, 
  INV.sku_id F2,
  INV.qty_on_hand F3, 
  INV.receipt_id F4, 
  to_char(INV.receipt_dstamp,'DD-MM-YYYY') as F5, 
  INV.tag_id F6,
  PH.status F7,
  sku.description F8,
  substr(SKU.colour,6,10) F9,
  SKU.product_group F10
FROM inventory INV inner join pre_advice_header PH on PH.pre_advice_id = INV.receipt_id
inner join SKU on SKU.sku_id = INV.sku_id
WHERE SKU.client_id = 'TR'
and INV.qty_on_hand >= 5 and INV.qty_on_hand < 17
and INV.receipt_id not like 'F%'
and INV.client_id = 'TR'
AND INV.location_id IN
  (SELECT loc
  FROM
    (SELECT location_id AS LOC,
      COUNT(SKU_id)          AS CNT
    FROM Inventory
    WHERE client_id = 'TR'
    AND qty_on_hand      >5
    AND location_id NOT IN ('CONTAINER', 'PACK1', 'SUSPENSE', 'BR04', 'SLICK','MNGIFTBOX','WMGIFTBOX', 'QCCUTUP', 'WH1', 'WH2','I1', 'H760','MARSHALB')
    AND location_id NOT LIKE 'HOLD%'
    AND location_id NOT LIKE 'HANG%'
    AND location_id NOT LIKE 'ZZ%'
    AND location_id NOT LIKE 'GL%'
    AND location_id NOT LIKE 'GM%'
    AND location_id NOT LIKE 'GN%'
    AND location_id NOT LIKE 'GO%'
    AND location_id NOT LIKE 'GP%'
    AND location_id NOT LIKE 'T%'
    AND location_id NOT LIKE 'CUF%'
    AND location_id NOT LIKE 'HI%'
    AND location_id NOT LIKE 'CF___'
    AND location_id NOT LIKE 'WMY%'
    AND location_id NOT LIKE 'WED%'
    AND location_id NOT LIKE 'WBM%'
    AND location_id NOT LIKE 'WKA%'
    AND location_id NOT LIKE 'WVO%'
	AND location_id NOT LIKE 'WYO%'	
	AND location_id NOT LIKE 'WCO%'
	AND location_id NOT LIKE 'WFA%'	
	AND location_id NOT LIKE 'WIN%'	
	AND location_id NOT LIKE 'WJA%'	
	AND location_id NOT LIKE 'WJE%'	
	AND location_id NOT LIKE 'WPR%'	
    AND location_id NOT LIKE 'WRM%'
	AND location_id NOT LIKE 'WSM%'	
	AND location_id NOT LIKE 'WST%'	
	AND location_id NOT LIKE 'WSU%'	
    AND location_id NOT LIKE 'A___'
    GROUP BY location_id
    HAVING COUNT(*)>1
    AND COUNT(   *)<5
    ORDER BY location_id
    )
  )
  order by INV.client_id, INV.location_id
)
/