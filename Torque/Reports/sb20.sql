SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 120

select 'SKU,Description,Qty +/-,Date/Time,Reason,Reference ID,,' from DUAL;

column A format A10
column B format A40
column C format 99999
column CC noprint
column D format A24
column E format A12
column F format A16

column M format A20 noprint

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



--ttitle  LEFT 'TML Retail - Open Orders Report ' RIGHT 'Printed ' report_date skip 2


--break on CC skip 1

--compute sum label 'Grand Total' of QQQ on report
--compute sum label 'Total Units' of QQQ on HH

select
it.REFERENCE_ID M,
it.sku_id A,
'"' || sku.description || '"' B,
it.UPDATE_QTY C,
to_char(it.DSTAMP,'DD/MM/YYYY HH24:MI:SS') D,
it.REASON_ID E,
it.REFERENCE_ID F,
' '
from inventory_transaction it,sku
where it.CLIENT_ID='SB'
and it.DSTAMP>sysdate-8
--and it.REFERENCE_ID like 'P%'
and it.CODE='Adjustment'
and it.sku_id=sku.sku_id
order by
M,A
/
