SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET COLSEP ','

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 900
SET TRIMSPOOL ON
SET HEADING OFF 

COLUMN Z HEADING 'Status' FORMAT A12
column A heading 'Order' format A20
COLUMN B HEADING 'Line' FORMAT 9999
COLUMN C HEADING 'SKU' FORMAT A30
COLUMN F HEADING 'EAN' FORMAT A30
COLUMN G HEADING 'Style' FORMAT A20
COLUMN H HEADING 'Size' FORMAT A12
column D heading 'Qty1' format 99999
column E heading 'Qty2' format 99999

--spool prnew.csv

select 'Prominent Consignment &&2 Shipment Detail' from DUAL;
--select to_char(SYSDATE, 'DD/MM/YY    HH:MI:SS') from DUAL;

select 'Status,Order ID,Line ID,SKU ID,EAN,Style,Size,Qty Ordered,Qty Shipped' from DUAL;

break on A dup skip 1

--compute sum label 'Total' of D on report

select
oh.status Z,
oh.order_id A,
OL.LINE_ID B,
DECODE(SKU.USER_DEF_TYPE_2,'TML',OL.USER_DEF_TYPE_4||SKU.USER_DEF_TYPE_8||SUBSTR(SKU.COLOUR,5,20)||'-'||SKU.SKU_SIZE,OL.SKU_ID) C,
OL.SKU_ID F,
SKU.USER_DEF_TYPE_5 G,
sku.sku_size H,
nvl(ol.user_def_type_8,ol.qty_ordered) E,
NVL(OL.QTY_PICKED,0) D
FROM ORDER_HEADER OH, ORDER_LINE OL LEFT JOIN SKU ON OL.SKU_ID=SKU.SKU_ID
WHERE OH.CLIENT_ID in ('PR','PS')
AND OH.ORDER_ID=OL.ORDER_ID
AND OL.CLIENT_ID in ('PR','PS')
AND SKU.CLIENT_ID in ('PR','PS')
AND oh.order_id=ol.order_id
and oh.consignment = '&&2'
order by
OH.ORDER_ID,
SKU.USER_DEF_TYPE_5,
sku.sku_size
/

--spool off