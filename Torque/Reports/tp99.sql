SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 100
SET TRIMSPOOL ON

column A3  format 999990.99


--spool tp99.csv

select 'TP TOTALS,&&2,to,&&3' from DUAL;
select 'Date,Code,Number,Qty,Average' from DUAL;

select
trunc(it.dstamp) AA,
--it.code B,
DECODE(nvl(OT,substr(it.tag_id,1,3)),'RET','RETURNS','WEB','WEB PICKS','RETAIL','RETAIL PICKS','RECEIPTS') B,
count(*) C,
sum(it.update_qty) A2,
sum(it.update_qty)/count(*) A3
from inventory_transaction it,(select
order_id OD,
order_type OT
from order_header
where client_id='TP')
where it.client_id = 'TP'
and it.code in ('Pick','Receipt')
--and trunc(it.Dstamp)>trunc(sysdate)-20
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.reference_id=OD (+)
group by
trunc(it.dstamp),
DECODE(nvl(OT,substr(it.tag_id,1,3)),'RET','RETURNS','WEB','WEB PICKS','RETAIL','RETAIL PICKS','RECEIPTS')
order by
trunc(it.dstamp),
DECODE(nvl(OT,substr(it.tag_id,1,3)),'RET','RETURNS','WEB','WEB PICKS','RETAIL','RETAIL PICKS','RECEIPTS')
/

break on AA skip 1 on report

compute sum label 'Total' of C A2 on report

select ' ' from DUAL;
select ' ' from DUAL;

select
'ALL DAYS' AA,
DECODE(nvl(OT,substr(it.tag_id,1,3)),'RET','RETURNS','WEB','WEB PICKS','RETAIL','RETAIL PICKS','RECEIPTS') B,
count(*) C,
sum(it.update_qty) A2,
sum(it.update_qty)/count(*) A3
from inventory_transaction it,(select
order_id OD,
order_type OT
from order_header
where client_id='TP')
where it.client_id = 'TP'
and it.code in ('Pick','Receipt')
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.reference_id=OD (+)
group by
DECODE(nvl(OT,substr(it.tag_id,1,3)),'RET','RETURNS','WEB','WEB PICKS','RETAIL','RETAIL PICKS','RECEIPTS')
order by
DECODE(nvl(OT,substr(it.tag_id,1,3)),'RET','RETURNS','WEB','WEB PICKS','RETAIL','RETAIL PICKS','RECEIPTS')
/

--spool off

