SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 50
set newpage 0
set verify off
set colsep ','



column A heading 'W/C' format A12
column C heading 'Branch' format A14
column B format 999999
column D format 99999999
column E format 9999.99
column F format A10 noprint
column G format 999999
column H format 9999.99
column J format A10

break on C dup on report

compute sum label 'Store Total' of D on C
compute sum label 'Grand Total' of D on report

--spool newtots4.csv

select 'Period &&2 to &&3' from DUAL;

select 'Store,Date,Order ID,Units,' from DUAL;


select
oh.customer_id C,
to_char(oh.creation_date,'YYYYMMDD') F,
to_char(oh.creation_date,'DD/MM/YY') A,
oh.order_id J,
nvl(sum (ol.qty_shipped),0) D
from order_header oh,order_line ol
where oh.client_id = '&&4'
and oh.creation_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')
and oh.order_id=ol.order_id
and ol.client_id='&&4'
and oh.customer_id<>'WWW'
group by
oh.customer_id,
to_char(oh.creation_date,'YYYYMMDD'),
to_char(oh.creation_date,'DD/MM/YY'),
oh.order_id
order by C,F
/


--spool off
