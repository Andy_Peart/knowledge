SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON

with t1 as (
    select sku_id, count(*) no_of_locations
    from (
        select distinct sku_id, location_id
        from inventory
        where client_id = '&&2'
        and location_id not in (
            'CONTAINER',
            'DESPATCH',
            'MAIL ORDER',
            'SUSPENSE'
        )
    )
    group by sku_id
)
select 'SKU,Units in locations,Number of locations' from dual
union all
select sku_id || ',' || qty || ',' || no_of_locs 
from (
select A.sku_id, A.qty, nvl(T1.no_of_locations,0) no_of_locs
from (
    select sku.sku_id, sum(nvl(inv.qty_on_hand,0)) qty
    from sku 
	left join inventory inv on sku.sku_id = inv.sku_id and sku.client_id = inv.client_id
    where sku.client_id = '&&2'
    and location_id not in ('CONTAINER','DESPATCH', 'MAIL ORDER','SUSPENSE') 
    --and upper(sku.sku_id) = lower(sku.sku_id)
    group by sku.sku_id    
) A left join T1 on A.sku_id = T1.sku_id
order by 1
)
/