SET FEEDBACK OFF                
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 400
SET TRIMSPOOL ON

column M heading ' ' noprint
--break on M skip 2

--spool huwstock.csv

select 'HUW STOCK LEVELS' from DUAL;
select 'Sku,Description,Condition,On Hand,Allocated,Available' from DUAL;

select
i.sku_id SS,
sku.description,
i.condition_id,
sum(i.qty_on_hand) AA,
sum(i.qty_allocated) BB,
sum(i.qty_on_hand-i.qty_allocated) CC
from inventory i,sku
where i.client_id = 'HU'
and sku.client_id = 'HU'
and i.sku_id=sku.sku_id 
group by 
i.sku_id,
sku.description,
i.condition_id
order by
i.sku_id
/

--spool off

