SET FEEDBACK OFF                 
set verify off
set wrap off
set colsep ','

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 132
set trimspool on

--BREAK ON AAA skip 1 dup

--spool mwfifolifo.csv

select ',MOUNTAIN stock in FIFO/LIFO locations' from DUAL;
select 'SKU,Description,Qty,Location,LPG,Work Zone' from DUAL;

select
''''||i.SKU_ID||''','||
i.description||','||
sum(i.qty_on_hand)||','||
''''||i.location_id||''','||
sku.user_def_type_8||','||
L.work_zone
from inventory i,location L,sku
where i.client_id='MOUNTAIN'
and i.site_id=L.site_id
and i.location_id=L.location_id
and L.loc_type in ('Tag-FIFO','Tag-LIFO')
and sku.client_id='MOUNTAIN'
and i.sku_id=sku.sku_id
group by
i.SKU_ID,
i.description,
i.location_id,
sku.user_def_type_8,
L.work_zone
order by 1
/

--spool off


