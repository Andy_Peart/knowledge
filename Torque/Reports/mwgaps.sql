SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','
SET WRAP OFF


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON


select 'Date, User ID, Order, Zone, Work_group, End time of current job, Start time of next job, Difference' from dual
union all
select B.date_picked  || ',' ||  B.user_id || ',' || B.reference_id || ',' || B.pick_zone  || ',' ||  B.work_group || ',' || B.end_time || ',' || A.start_time  || ',' || 
substr( to_char(  to_timestamp (A.start_time,'HH24:MI') - TO_TIMESTAMP (B.end_time,'HH24:MI') ), 12,8)
from 
(
select date_picked, row_number() over (order by date_picked, user_id, end_time) as rn, user_id, reference_id, pick_zone, work_group, end_time
from
(
select to_char(it.dstamp,'YYYY-MM-DD') as date_picked, it.user_id, it.reference_id, substr(it.from_loc_id,1,1) as pick_zone, it.work_group,  max(to_char(it.dstamp,'HH24:MI')) end_time
from inventory_transaction it
where it.client_id = 'MOUNTAIN'
and it.dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1
and it.code = 'Pick'
and it.from_loc_id <> 'CONTAINER'
and it.update_qty > 0
and it.reference_id like 'MOR%'
and it.from_loc_id not like '0%'
and it.from_loc_id not like '1%'
and it.from_loc_id not like '2%'
and it.from_loc_id not like '3%'
and it.from_loc_id not like '4%'
and it.from_loc_id not like '5%'
and it.from_loc_id not like '6%'
and it.from_loc_id not like '7%'
and it.from_loc_id not like '8%'
and it.from_loc_id not like '9%'
and it.from_loc_id not like 'G%'
group by to_char(it.dstamp,'YYYY-MM-DD'), it.user_id, it.reference_id, substr(it.from_loc_id,1,1), it.work_group
order by 1,2,5
)
) B
left join 
(
select row_number() over (order by date_picked, user_id, start_time) as rn, user_id, reference_id, pick_zone, work_group, start_time
from
(
select to_char(it.dstamp,'YYYY-MM-DD') as date_picked,  it.user_id, it.reference_id, substr(it.from_loc_id,1,1) as pick_zone,  it.work_group,  min(to_char(it.dstamp,'HH24:MI')) start_time
from inventory_transaction it
where it.client_id = 'MOUNTAIN'
and it.dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1
and it.code = 'Pick'
and it.from_loc_id <> 'CONTAINER'
and it.update_qty > 0
and it.reference_id like 'MOR%'
and it.from_loc_id not like '0%'
and it.from_loc_id not like '1%'
and it.from_loc_id not like '2%'
and it.from_loc_id not like '3%'
and it.from_loc_id not like '4%'
and it.from_loc_id not like '5%'
and it.from_loc_id not like '6%'
and it.from_loc_id not like '7%'
and it.from_loc_id not like '8%'
and it.from_loc_id not like '9%'
and it.from_loc_id not like 'G%'
group by to_char(it.dstamp,'YYYY-MM-DD'), it.user_id, it.reference_id, substr(it.from_loc_id,1,1), it.work_group
order by 1,2,5
)
) A
on B.rn = A.rn -1
where substr( to_char(  to_timestamp (A.start_time,'HH24:MI') - TO_TIMESTAMP (B.end_time,'HH24:MI') ), 1,1) = '+'
/