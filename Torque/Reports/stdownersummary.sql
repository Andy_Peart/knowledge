SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240
SET TRIMSPOOL ON

column E format A12


break on E dup on report
compute sum label 'Owner Totals' of F G H I I2 J2 J K on E
compute sum label 'Run Totals' of F G H I I2 J2 J K on report

select 'Stock Summary Report - for client ID &&2 - as at  ' 
    || to_char(sysdate,'dd/mm/yy  hh24:mi:ss') from dual;

--SET TERMOUT OFF
--spool sos.csv

select 'Product Code,Description,Colour,Size,Owner ID,Total Stock,Intake,QC Hold,Suspense,Locked,Picked,Allocated,Available'
from dual;

select
i.sku_id A,
sku.description B,
sku.colour C,
sku.sku_size D,
i.owner_id E,
sum(i.qty_on_hand) F,
nvl(J,0) G,
nvl(K,0) H,
nvl(L,0) I,
nvl(L2,0) I2,
nvl(M,0) J2,
nvl(sum(i.qty_allocated),0) J,
nvl(sum(i.qty_on_hand-i.qty_allocated)-nvl(J,0)-nvl(K,0)-nvl(L,0)-nvl(L2,0)-nvl(M,0),0) K
from inventory i,sku, 
(select distinct jc.sku_id JA,jc.owner_id OA,sum(jc.qty_on_hand) J 
from inventory jc, location lc
where jc.client_id='&&2'
and jc.location_id=lc.location_id
and jc.site_id=lc.site_id
and lc.loc_type in ('Receive Dock')
group by jc.sku_id,jc.owner_id),
(select distinct jc.sku_id JB,jc.owner_id OB,sum(jc.qty_on_hand) K from inventory jc
where jc.client_id='&&2'
and jc.condition_id='QCHOLD'
group by jc.sku_id,jc.owner_id),
(select distinct jc.sku_id JC,jc.owner_id OC,sum(jc.qty_on_hand) L from inventory jc
where jc.client_id='&&2'
and jc.location_id='SUSPENSE'
group by jc.sku_id,jc.owner_id),
(select distinct jc.sku_id JD,jc.owner_id OD,sum(jc.qty_on_hand) M 
from inventory jc, location lc
where jc.client_id='&&2'
and jc.location_id=lc.location_id
and jc.site_id=lc.site_id
and lc.loc_type in ('ShipDock','Stage')
group by jc.sku_id,jc.owner_id),(select
iv.sku_id JC2,
iv.owner_id OD2,
sum(qty_on_hand-qty_allocated) L2
from inventory iv,location ll
where iv.client_id='&&2'
and iv.location_id=ll.location_id
and iv.site_id=ll.site_id
and iv.lock_status in ('Locked','OutLocked')
group by
iv.sku_id,
iv.owner_id)
where i.client_id='&&2'
and sku.client_id='&&2'
and i.Sku_id=sku.sku_id
and i.sku_id = JA (+)
and i.sku_id = JB (+)
and i.sku_id = JC (+)
and i.sku_id = JD (+)
and i.sku_id = JC2 (+)
and i.owner_id=OA (+)
and i.owner_id=OB (+)
and i.owner_id=OC (+)
and i.owner_id=OD (+)
and i.owner_id=OD2 (+)
group by 
i.sku_id,
sku.description,
sku.colour,
sku.sku_size,
i.owner_id,
nvl(J,0),nvl(K,0),nvl(L,0),nvl(L2,0),nvl(M,0)
order by i.owner_id,i.sku_id
/

--spool off
--SET TERMOUT ON
