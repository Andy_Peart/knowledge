SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Status,Order #,Sku id,Currently Allocated,Currently Ordered,Originally Ordered,,Total Qty on Hand,Total Qty Allocated' from dual
union all
select "Status" || ',' || "Order #" || ',' || "Sku" || ',' || "CurAl" || ',' || "CurOr" || ',' || "OriOrd" || ',,' || "TotHan" || ',' || "TotAl"
from (
select oh.status        as "Status"
, oh.order_id           as "Order #"
, ol.sku_id             as "Sku"
, nvl(ol.qty_tasked,0)  as "CurAl"
, ol.qty_ordered        as "CurOr"
, ol.user_def_type_8    as "OriOrd"
, null
, sum(i.qty_on_hand)    as "TotHan"
, sum(i.qty_allocated)  as "TotAl"
from order_header oh
inner join order_line ol on ol.client_id = oh.client_id and ol.order_id = oh.order_id
left join inventory i on i.client_id = ol.client_id and i.sku_id = ol.sku_id
left join location l on l.site_id = i.site_id and l.location_id = i.location_id
where oh.client_id in ('PR','PS')
and upper(oh.user_Def_type_5) = 'Z'
and oh.status not in ('Shipped','Cancelled')
--and oh.order_id = '186683-P73350'
and l.loc_type in ('Tag-FIFO','Tag-LIFO')
and nvl(l.disallow_alloc,'N') = 'N'
and i.lock_status = 'UnLocked'
group by oh.status
,oh.order_id
, ol.sku_id
, ol.user_def_type_8
, ol.qty_ordered
, ol.qty_tasked
--having sum(i.qty_on_hand)<>sum(i.qty_allocated)
order by 1
)
/
--spool off