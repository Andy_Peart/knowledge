SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
with t1 as (
select oh.order_id                              as "Order"
, trunc(oh.order_date)                          as "Order Date"
, ol.line_id                                    as "Line"
, ol.sku_id                                     as "Sku"
, count(ol.sku_id)over(partition by oh.order_id)as "Count"
, s.sku_size                                    as "Size"
, s.style                                       as "Style"
, s.colour                                      as "Colour"
, oh.ship_dock                                  as "SD"
from order_header oh
inner join order_line ol on ol.client_id = oh.client_id and ol.order_id = oh.order_id
inner join sku s on s.client_id = ol.client_id and s.sku_id = ol.sku_id
where oh.client_id = 'UT'
and oh.order_id in  (
                    select reference_id
                    from inventory_transaction
                    where client_id = 'UT'
                    and code = 'Return'
                    )
group by oh.order_id
, trunc(oh.order_date)
, ol.line_id
, ol.sku_id
, s.sku_size
, s.style
, s.colour
, oh.ship_dock
), t2 as (
select reference_id                             as "Order"
, sku_id                                        as "Sku"
, count(sku_id)over(partition by reference_id)  as "Count"
, line_id                                       as "Line"
, trunc(dstamp)                                 as "Date"
, reason_id                                     as "Reason"
, 'Returned'                                    as "Ret"
from inventory_transaction
where client_id = 'UT'
and code = 'Return'
group by reference_id
, sku_id
, line_id
, trunc(dstamp)
, reason_id
)
select 'ShipDock, Order ID,Order Date,Sku,Size,Style,Colour,Returned Date,Return Reason,% of Order Returned,Returned/Not Returned' from dual
union all
select "SD" || ',' || "Order" || ',' || "Order Date" || ',' || "Sku" || ',' || "Size" || ',' || "Style" || ',' || "Colour" || ',' || "Date" || ',' || "Reason" || ',' || "%" || ',' || "Ret"
from (
select t1."SD"
, t1."Order"
, t1."Order Date"
, t1."Sku"
, t1."Size"
, t1."Style"
, t1."Colour"
, t2."Date"
, t2."Reason"
, case when nvl(t2."Count",0) = 0 then '' else to_char(t2."Count"/t1."Count", '99.99')*100 || '%' end as "%"
, t2."Ret"
from t1
left join t2 on t2."Order" = t1."Order" and t2."Sku" = t1."Sku" and t2."Line" = t1."Line"
order by 1
)
/
--spool off