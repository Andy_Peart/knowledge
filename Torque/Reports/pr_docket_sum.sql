set pagesize 64
set linesize 2000
set verify off
clear columns
clear breaks
clear computes

set heading on
set feedback off

column A heading 'Dockets' format a20
column B heading 'Receipts' format 9999999

set heading off

select 'Prom Dockets Receipts from &&2 to &&3' from DUAL;

set heading on

break on report

SELECT RECEIPT_ID AS DOCKET, SUM(QTY_ON_HAND) AS TOTAL FROM INVENTORY
WHERE CLIENT_ID='PR' AND ZONE_1 = 'P' 
AND TRUNC(RECEIPT_DSTAMP)>=TO_DATE('&&2','dd-mon-yyyy')
AND TRUNC(RECEIPT_DSTAMP)<=TO_DATE('&&3','dd-mon-yyyy')
GROUP BY RECEIPT_ID
ORDER BY RECEIPT_ID

/