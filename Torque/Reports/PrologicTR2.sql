SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep '~'


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

select
'90003~90003~GAR-HOLD~TAKE ON~Road-1~FOB~~~~~' ||'~'||
substr(SS,1,12) ||'~~'||
QQ ||'~'||
'~~~~~~~~~~~~' ||'~'||
nvl(sku.each_value,0) ||'~'
--'Price' ||'~'
from sku,
(select
sku_id SS,
sum(qty_on_hand) QQ
from inventory
where client_id='TR'
and location_id<>'SUSPENSE'
and condition_id='HOLD'
group by sku_id)
where SS=sku.sku_id
and sku.client_id='TR'
order by SS
/
