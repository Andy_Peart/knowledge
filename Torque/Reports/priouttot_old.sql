set pagesize 68
set linesize 120
set verify off

ttitle '&&4 Warehouse Despatch Quantity' skip 2

column A heading 'Date' format a8
column F heading 'Mode' format a12
column B heading 'Qty Despatched' format 99999999
column M format a4 noprint


break on report

compute sum label 'Total' of B on report

select
to_char(Dstamp, 'YYMM') M,
to_char(dstamp, 'DD/MM/YY') A,
DECODE(substr(reference_id,1,2),'31','WHOLESALE','RETAIL') F,
sum (update_qty) B
from inventory_transaction
where client_id = '&&4'
and code = 'Shipment'
and dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1
group by
to_char(Dstamp, 'YYMM'),
to_char(dstamp, 'DD/MM/YY'),
substr(reference_id,1,2)
order by
M,A
/
