SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 99
SET NEWPAGE 0
SET LINESIZE 200
SET TRIMSPOOL ON
 
column AA heading 'Type' format A36
column QQQ heading 'Units' format 999999


ttitle  LEFT 'PROMINENT Receipts from '&&2' to '&&3'  .. ' skip 2

break on report
compute sum label 'Total' of QQQ on report




select
'HANGING STOCK  (NOT Ratio Packs) ' AA,
nvl(sum(itl.update_qty),0) QQQ
from inventory_transaction itl,sku
where itl.client_id='PR'
and itl.station_id not like 'Auto%'
and itl.site_id='PROM'
and itl.code like 'Receipt%'
and itl.user_def_type_8 is not null
and itl.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and sku.client_id='PR'
and itl.sku_id=sku.sku_id
and sku.user_def_type_1='H'
and (sku.user_def_type_9=1 or sku.user_def_type_9 is null)
union
select
'HANGING STOCK      (Ratio Packs) ' AA,
nvl(sum(itl.update_qty*sku.user_def_type_9),0) QQQ
from inventory_transaction itl,sku
where itl.client_id='PR'
and itl.site_id='PROM'
and itl.station_id not like 'Auto%'
and itl.code like 'Receipt%'
and itl.user_def_type_8 is not null
and itl.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and sku.client_id='PR'
and itl.sku_id=sku.sku_id
and sku.user_def_type_1='H'
and sku.user_def_type_9>1
union
select
'  BOXED STOCK  (NOT Ratio Packs) '  AA,
nvl(sum(itl.update_qty),0) QQQ
from inventory_transaction itl,sku
where itl.client_id='PR'
and itl.site_id='PROM'
and itl.station_id not like 'Auto%'
and itl.code like 'Receipt%'
and itl.user_def_type_8 is not null
and itl.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and sku.client_id='PR'
and itl.sku_id=sku.sku_id
and (sku.user_def_type_1<>'H' or sku.user_def_type_1 is null)
and (sku.user_def_type_9=1 or sku.user_def_type_9 is null)
union
select
'  BOXED STOCK      (Ratio Packs) ' AA,
nvl(sum(itl.update_qty*sku.user_def_type_9),0) QQQ
from inventory_transaction itl,sku
where itl.client_id='PR'
and itl.site_id='PROM'
and itl.station_id not like 'Auto%'
and itl.code like 'Receipt%'
and itl.user_def_type_8 is not null
and itl.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and sku.client_id='PR'
and itl.sku_id=sku.sku_id
and (sku.user_def_type_1<>'H' or sku.user_def_type_1 is null)
and sku.user_def_type_9>1
order by AA
/
