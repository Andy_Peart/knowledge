SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 120

select 'MOUNTAIN Order Totals' from DUAL;
select ' ' from DUAL;

column A heading 'Status' format A12
column B heading 'Creation Date' format A16
column CC heading 'Number' format 999999
column DD heading 'Units' format 999999
column F heading 'Order' format A8

break on report

compute sum label 'Total' of CC on report



select 'Status,Number' from DUAL;

select
status A,
count(*) CC
from order_header
where client_id='MOUNTAIN'
and work_group='WWW'
and status in ('Released','Allocated','In Progress','Complete')
group by
status
/

select ' ' from DUAL;
select 'Creation Date,Number' from DUAL;

select
to_char(creation_date,'DD/MM/YYYY') B,
count(*) CC
from order_header
where client_id='MOUNTAIN'
and work_group='WWW'
and status in ('Released','Allocated','In Progress','Complete')
group by
to_char(creation_date,'DD/MM/YYYY')
order by
to_char(creation_date,'DD/MM/YYYY')
/


select ' ' from DUAL;
select 'Consignment = SHORTAGE' from DUAL;
select ' ' from DUAL;
select 'Order,Creation Date,Status,Lines,Name,,' from DUAL;

select
oh.Order_id  F,
to_char(oh.creation_date,'DD/MM/YYYY') B,
oh.status A,
count(*) CC,
name,
' ',
' '
from order_header oh,order_line ol
where oh.client_id='MOUNTAIN'
and oh.work_group='WWW'
and oh.status in ('Released','Allocated','In Progress','Complete')
and oh.order_id=ol.order_id
and oh.consignment='SHORTAGE'
group by
oh.Order_id,
to_char(oh.creation_date,'DD/MM/YYYY'),
oh.status,
name
order by
oh.Order_id
/
