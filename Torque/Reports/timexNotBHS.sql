SET FEEDBACK OFF           
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ' '
SET HEADING ON


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 66
SET NEWPAGE 0
SET LINESIZE 100

break on report

compute sum label 'Total' of D on report


ttitle 'TIMEX Stock (NOT BHS)' skip 2

column A heading 'Date' format a12
column B heading 'SKU' format a12
column C heading 'Description' format a32
column D heading 'Qty' format 99999999

--select 'Date,Sku,Description,Qty,' from Dual;

select
it.sku_id B,
sku.description C,
sum (it.qty_on_hand) D
from inventory it,sku
where it.client_id = 'TIMEX'
and it.sku_id=sku.sku_id
and sku.client_id='TIMEX'
and nvl(sku.product_group,' ')<>'BHS'
group by 
it.sku_id,
sku.description
order by 
it.sku_id,
sku.description
/
SET HEADING OFF
SET PAGESIZE 0

select '' From DUAL;
select 'End of Report' From DUAL;
