SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','
SET WRAP OFF


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON


SELECT 'Date, Time, Code, Location, SKU, Qty' from DUAL
union all
SELECT tdate || ',' || ttime || ',' || Code || ',' || FROM_LOC_ID || ',' || SKU_ID || ',' || UPDATE_QTY
from (
SELECT TO_CHAR(dstamp, 'DD-MM-YYYY') as tdate, TO_CHAR(dstamp, 'HH24:MI') as ttime, Code, FROM_LOC_ID, SKU_ID, UPDATE_QTY
FROM INVENTORY_TRANSACTION
WHERE CLIENT_ID = 'MOUNTAIN'
AND CODE = 'Stock Check'
AND FROM_LOC_ID = 'TEMP'
order by 1,2,5
)
/