SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 480
SET TRIMSPOOL ON

column A heading 'SKU' format 9999999
column B heading 'Desc' format a30
column C heading 'Unit Price' format 999999
column D heading 'Qty' format 999999
column D heading 'Stock Value' format 999999

select 'SKU,Desc,Unit Price,Qty,Stock Value' from DUAL;

break on report

--compute sum label 'Total' of C D  on report
select "SKU","Desc","Unit Price","Qty",("Qty"*"Unit Price") Stock_Value 
from(select i.sku_id "SKU",s.description "Desc",s.user_def_num_1 "Unit Price",sum(i.qty_on_hand) "Qty" 
from inventory i, sku s
where i.client_id = 'SB'
and i.sku_id = s.sku_id
and location_id not in('SUSPENSE','992','QCHOLD','REJECT','REJECTD','SBSALE','SBIN','TAGS')
-- RW 26/11/10 Darryl asked for Voucher and Catalogues removing from blocked and TAGS added
--and location_id not in('SUSPENSE','992','QCHOLD','REJECT','REJECTD','SBSALE','SBIN','VOUCHER','CATALOGUES')
and location_ID not like ('SBIN%')
group by i.sku_id, s.description,s.user_def_num_1)
/