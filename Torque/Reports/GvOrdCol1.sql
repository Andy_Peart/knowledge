SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET WRAP OFF
SET colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 90
SET NEWPAGE 1
set linesize 500
set trimspool on


column A heading '  ' format a18
column B heading '  ' format a18
column M heading 'Ordered' format 99999999
column R heading 'Shipped' format 99999999
column T heading 'Shortage' format 99999999
column W heading 'Fulfilment %' format 99999.99

--select 'GIVe Orders - Released - Allocated - In Progress - Hold - Awaiting - Complete --- as at '||to_char(SYSDATE, 'DD/MM/YY  HH:MI') from DUAL;

--select 'Order ID,Qty Ordered,Qty Shipped,Qty Short,Creation Date,' from DUAL;

break on report

compute sum label 'Totals' of M R T on report

select
'Consignment &&2' A,
DECODE(Substr(ol.sku_id,1,1),'C','Consumables','Others') B,
sum(nvl(ol.qty_ordered,0)) M,
sum(nvl(ol.qty_shipped,0)) R,
sum(nvl(ol.qty_ordered,0)-nvl(ol.qty_shipped,0)) T,
sum(nvl(ol.qty_shipped,0))/sum(nvl(ol.qty_ordered,0))*100 W
from order_header oh,order_line ol
where oh.client_id = 'GV'
and ol.client_id=oh.Client_id
and oh.order_id = ol.order_id
and consignment like '&&2%'
and oh.order_id like '%T%'
group by
DECODE(Substr(ol.sku_id,1,1),'C','Consumables','Others')
union
select
'Home Shopping' A,
'  ' B,
sum(nvl(ol.qty_ordered,0)) M,
sum(nvl(ol.qty_shipped,0)) R,
sum(nvl(ol.qty_ordered,0)-nvl(ol.qty_shipped,0)) T,
sum(nvl(ol.qty_shipped,0))/sum(nvl(ol.qty_ordered,0))*100 W
from order_header oh,order_line ol
where oh.client_id = 'GV'
and ol.client_id=oh.Client_id
and oh.order_id = ol.order_id
and oh.creation_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and oh.order_id not like '%T%'
/
