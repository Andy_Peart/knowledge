
SET FEEDBACK ON                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 600
set trimspool on


--spool SBDUSA.csv

select 'ITL'||','
||'E'||','
||'Receipt'||','
||decode(sign(i.original_qty), 1, '+', -1, '-', '+')||','
||nvl(i.original_qty,0)||','
||decode(sign(i.update_qty), 1, '+', -1, '-', '+')||','
||i.update_qty||','
||to_char(i.dstamp, 'DD-MM-YYYY')||','
||i.client_id||','
||i.sku_id||','
||i.from_loc_id||','
||i.to_loc_id||','
||i.final_loc_id||','
||i.tag_id||','
||i.reference_id||to_char(sysdate, 'DDHH24')||','
||i.line_id||','
||i.condition_id||','
||i.notes||','
||i.reason_id||','
||i.batch_id||','
||i.expiry_dstamp||','
||i.user_id||','
||i.shift||','
||i.station_id||','
||i.site_id||','
||i.container_id||','
||i.pallet_id||','
||i.list_id||','
||i.owner_id||','
||i.origin_id||','
||i.work_group||','
||i.consignment||','
||i.manuf_dstamp||','
||i.lock_status||','
||i.qc_status||','
||i.session_type||','
||i.summary_record||','
||i.elapsed_time||','
||i.supplier_id||','
||i.reference_id||','
||'Receipt'||','
||i.user_def_type_3||','
||i.user_def_type_4||','
||i.user_def_type_5||','
||i.user_def_type_6||','
||i.user_def_type_7||','
||i.user_def_type_8||','
||i.user_def_chk_1||','
||i.user_def_chk_2||','
||i.user_def_chk_3||','
||i.user_def_chk_4||','
||i.user_def_date_1||','
||i.user_def_date_2||','
||i.user_def_date_3||','
||i.user_def_date_4||','
||i.user_def_num_1||','
||i.user_def_num_2||','
||i.user_def_num_3||','
||i.user_def_num_4||','
||i.user_def_note_1||','
||i.user_def_note_2||','
||i.from_site_id||','
||i.to_site_id||','
||''||','
||i.job_id||','
||i.job_unit||','
||i.manning||','
||i.spec_code||','
||i.config_id||','
||i.estimated_time||','
||i.task_category||','
||i.sampling_type||','
||to_char(i.complete_dstamp, 'DD-MM-YYYY')||','
||i.grn||','
||i.group_id||','
||'N'||','
||i.uploaded_vview||','
||i.uploaded_ab||','
||i.sap_idoc_type||','
||i.sap_tid||','
||i.ce_orig_rotation_id||','
||i.ce_rotation_id||','
||i.ce_consignment_id||','
||i.ce_receipt_type||','
||i.ce_originator||','
||i.ce_originator_reference||','
||i.ce_coo||','
||i.ce_cwc||','
||i.ce_ucr||','
||i.ce_under_bond||','
||''||','
||i.uploaded_customs||','
||i.uploaded_labor||','
||i.asn_id||','
||i.customer_id||','
||i.print_label_id||','
||i.lock_code||','
||i.ship_dock||','
||''||','
||'N'||','
||'N'
from inventory_transaction i
where i.client_id = 'SB'
and trunc(i.Dstamp)>trunc(sysdate-1)
and i.code = 'Receipt'
and i.reference_id like 'U%'
/


--spool off
