SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

select 'Report for &&2 to &&3' from dual
union all
select 'Countries, Desp Method, Orders, Units' from dual
union all
select country_group || ',' || dispatch_method || ',' ||  orders || ',' || units
from (
select country_group, dispatch_method, count(*) orders, sum(shipped) as units
from (
select oh.order_id, oh.country, oh.dispatch_method, sum(nvl(ol.qty_shipped,0)) shipped,
case 
  when country in ('AUS') then 'Australia'
  when country in ('AUT','BEL','CYP','CZE','DNK','EST','FIN','FRA','DEU','GRC','HUN','IRL','IMN','ITA','JEY','LVA','LTU','LUX','MLT','MCO','NLD','POL','PRT','ROU','SMR','SVK','SVN','ESP','SWE','GUF','GLP','GGY','MTQ','REU','VAT') then 'Europe'
  when country in ('NZL') then 'New Zealand'
  when country in ('USA') then 'United Stated'
  when country in ('GBR') then 'United Kingdom'
  else 'ROW'
end country_group
from order_header oh INNER JOIN order_line ol on oh.order_id = ol.order_id
where oh.client_id = 'SB'
and (oh.order_id like 'C%'  OR oh.order_id like 'E%')
AND oh.creation_date between to_date('&&2') and to_date('&&3')+1
group by oh.order_id, oh.country, oh.dispatch_method
union all
select oh.order_id, oh.country, oh.dispatch_method, sum(nvl(ol.qty_shipped,0)) shipped,
case 
  when country in ('AUS') then 'Australia'
  when country in ('AUT','BEL','CYP','CZE','DNK','EST','FIN','FRA','DEU','GRC','HUN','IRL','IMN','ITA','JEY','LVA','LTU','LUX','MLT','MCO','NLD','POL','PRT','ROU','SMR','SVK','SVN','ESP','SWE','GUF','GLP','GGY','MTQ','REU','VAT') then 'Europe'
  when country in ('NZL') then 'New Zealand'
  when country in ('USA') then 'United Stated'
  when country in ('GBR') then 'United Kingdom'
  else 'ROW'
end country_group
from order_header_archive oh INNER JOIN order_line_archive ol on oh.order_id = ol.order_id
where oh.client_id = 'SB'
and (oh.order_id like 'C%'  OR oh.order_id like 'E%')
AND oh.creation_date between to_date('&&2') and to_date('&&3')+1
group by oh.order_id, oh.country, oh.dispatch_method
)
group by country_group, dispatch_method
order by 1,2
)
/