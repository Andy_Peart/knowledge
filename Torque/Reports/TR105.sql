SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

column D heading 'Total' format 99,999,999



--select 'Order ID,Ordered,Picked' from DUAL;
 
set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DAY, DD MONTH, YYYY') curdate from DUAL;
set TERMOUT ON



--compute sum label 'Total' of D DD on report

--TR105 - TM Lewin Retail Picking summary for  orders with a creation date in March 2008

select 'TR105 - TM Lewin Retail Order Picking summary' from DUAL;
select 'for orders with a creation date in '||trim(to_char(sysdate-30,'Month'))||to_char(sysdate-30,', YYYY') from DUAL;
select '' from DUAL;
select '' from DUAL;

break on report skip 2

select
'Qty Ordered     =   '||sum(ol.qty_ordered) D
from order_header oh,order_line ol
where oh.client_id='TR'
and to_char(oh.creation_date,'MonYYYY')=to_char(sysdate-30,'MonYYYY')
and oh.status in ('Shipped')
and oh.client_id=ol.client_id
and oh.order_id=ol.order_id
/


select
'Pickable Qty    =   '||sum(update_qty) D
from inventory_transaction it
where code='Allocate'
and it.client_id='TR'
and reference_id in
(select
order_id
from order_header oh
where oh.client_id='TR'
and to_char(oh.creation_date,'MonYYYY')=to_char(sysdate-30,'MonYYYY')
and oh.status in ('Shipped'))
and from_loc_id<>'CONTAINER'
/



select
'Qty Picked      =   '||sum(ol.qty_picked) D
from order_header oh,order_line ol
where oh.client_id='TR'
and to_char(oh.creation_date,'MonYYYY')=to_char(sysdate-30,'MonYYYY')
and oh.status in ('Shipped')
and oh.client_id=ol.client_id
and oh.order_id=ol.order_id
/

select
'Qty not picked  =   '||sum(ol.qty_ordered-nvl(ol.qty_picked,0)) D
from order_header oh,order_line ol
where oh.client_id='TR'
and to_char(oh.creation_date,'MonYYYY')=to_char(sysdate-30,'MonYYYY')
and oh.status in ('Shipped')
and oh.client_id=ol.client_id
and oh.order_id=ol.order_id
/
