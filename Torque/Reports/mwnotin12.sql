SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON

--spool mwnotin12.csv

select 'Sku Id,Location,Work Zone,Qty,Web Order,Consignment,Web Stock' from DUAL;

select
SKU||','||
LOC||','||
ZZ||','||
QWEBA||','||
TSK||','||
CNS||','||
WQ
from (select
mt.sku_id SKU,
mt.from_loc_id LOC,
mt.work_zone ZZ,
mt.task_id TSK,
mt.consignment CNS,
--LISTAGG(mt.task_id, ':') WITHIN GROUP (ORDER BY mt.sku_id) AS TSK,
sum(mt.qty_to_move) QWEBA,
nvl(QQ,0) WQ
from move_task mt,location L,(select
sku_id SKU2,
sum(qty_on_hand-qty_allocated) QQ
from inventory
where client_id='MOUNTAIN'
and zone_1='WEB'
group by sku_id)
where mt.client_id='MOUNTAIN'
and mt.work_zone not like '12%'
and mt.work_group='WWW'
and mt.sku_id like '5%'
and mt.from_loc_id=L.location_id
and L.loc_type='Tag-FIFO'
and mt.sku_id=SKU2 (+)
group by
mt.sku_id,
mt.from_loc_id,
mt.work_zone,
mt.task_id,
mt.consignment,
nvl(QQ,0))
order by 1
/

--spool off
