SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET COLSEP ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 200
SET TRIMSPOOL ON
SET HEADING OFF 

column A heading 'Status' format A60
column B heading 'Order' format A60
column C heading 'Line' format A60
column D heading 'SKU' format A60
column E heading 'Style' format A20
column F heading 'Qty Ordered' format 9999999
column G heading 'Qty Picked' format 9999999
column H heading 'Qty Short' format 9999999
column I heading 'Description' format A100
column J heading 'Description 2' format A100
column K heading 'Original Qty' format A100

select 'Prominent Consignment &&2 Shipment Detail' from DUAL;

select 'Status,Order ID,Line ID,SKU ID,TML Style,Qty Ordered,Qty Shipped,Qty Short,Description,Description 2,Original Qty' from DUAL;

break on 'Order' skip 1;

select status||','||order_id||','||line_id||','||"Sku"||','||"Style"||','||qty_ordered||','||"Picked"||','||"Short"||','||description||','||user_def_type_2||','||"Orig"
from
(
select oh.status
, oh.order_id
, ol.line_id
, decode(s.user_def_type_2,'TML',ol.user_def_type_4||s.user_def_type_8||substr(s.colour,5,20)||'-'||s.sku_size,ol.sku_id) as "Sku"
, decode(s.user_def_type_2,'TML',s.user_def_type_5,'') as "Style"
, ol.qty_ordered
, nvl(ol.qty_picked,0) as "Picked"
, ol.qty_ordered - nvl(ol.qty_picked,0) as "Short"
, s.description
, s.user_def_type_2
, nvl(ol.user_def_type_8,0) as "Orig"
from order_header oh
inner join order_line ol on ol.client_id = oh.client_id and ol.order_id = oh.order_id
inner join sku s on s.client_id = ol.client_id and s.sku_id = ol.sku_id
where oh.client_id in ('PR','PS')
and oh.consignment like '&&2%'
order by oh.order_id
, ol.line_id
)
/

--spool off
