SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON



--break on report

--compute sum label 'Total' of B C on report

--spool mwoutstatsumm.csv


select 'MOUNTAIN Warehouse STATIONERY Summary from &&2 to &&3 ' from DUAL;

select 'Date,Receipts,Returns,Picks,Adj Up,Adj Down' from DUAL;

select
D||','||
B1||','||
B2||','||
B3||','||
B4||','||
B5
from (select
distinct
trunc(Dstamp) D,
nvl(B1,0) B1,
nvl(B2,0) B2,
nvl(B3,0) B3,
nvl(B4,0) B4,
nvl(B5,0) B5
from inventory_transaction,(select
trunc(Dstamp) D1,
sum(update_qty) B1
from inventory_transaction
where client_id='MOUNTAIN'
and code like 'Receipt%'
and upper(sku_id)<>lower(sku_id)
and Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and notes is null
group by
trunc(Dstamp)),(select
trunc(Dstamp) D2,
sum(update_qty) B2
from inventory_transaction
where client_id='MOUNTAIN'
and code like 'Receipt%'
and upper(sku_id)<>lower(sku_id)
and Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and notes is not null
group by
trunc(Dstamp)),(select
trunc(Dstamp) D3,
sum(update_qty) B3
from inventory_transaction
where client_id='MOUNTAIN'
and code like 'Pick'
and elapsed_time>0
and upper(sku_id)<>lower(sku_id)
and Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by
trunc(Dstamp)),(select
trunc(Dstamp) D4,
sum(update_qty) B4
from inventory_transaction
where client_id='MOUNTAIN'
and code='Adjustment'
and update_qty>0
and upper(sku_id)<>lower(sku_id)
and Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by
trunc(Dstamp)),(select
trunc(Dstamp) D5,
sum(update_qty) B5
from inventory_transaction
where client_id='MOUNTAIN'
and code='Adjustment'
and update_qty<0
and upper(sku_id)<>lower(sku_id)
and Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by
trunc(Dstamp))
where client_id='MOUNTAIN'
and code in ('Receipt','Pick')
and upper(sku_id)<>lower(sku_id)
and Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and trunc(Dstamp)=D1(+)
and trunc(Dstamp)=D2(+)
and trunc(Dstamp)=D3(+)
and trunc(Dstamp)=D4(+)
and trunc(Dstamp)=D5(+)
order by
D)
/


--spool off
