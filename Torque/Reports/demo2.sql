SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240
SET TRIMSPOOL ON



select '&&2  &&3 X&&4 X  &&5' from DUAL;

select
user_id,
count(*)
from inventory_transaction
where client_id='&&2'
and Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&5', 'DD-MON-YYYY')+1
and user_id like '&&4%'
group by user_id
/
