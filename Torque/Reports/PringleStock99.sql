SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 

column A heading 'Category' format A11
column B heading 'Qty' format 9999999

select to_char(SYSDATE, 'DD/MM/YY    HH:MI:SS') Stock_Report from DUAL;



select
'Stock' A,
sum(i.qty_on_hand) B
from inventory i
where i.location_id not in ('QC0','QC1','QC2','QCHOLD','WP1','SUSPENSE')
and i.client_id like '&&2'
and i.location_id like 'D%'
/

SET PAGESIZE 0

select
'Allocated' A,
sum(i.qty_allocated) B
from inventory i
where i.location_id not in ('QC0','QC1','QC2','QCHOLD','WP1','SUSPENSE')
and i.client_id like '&&2'
and i.location_id like 'D%'
/

select
'Available' A,
sum(i.qty_on_hand-i.qty_allocated) B
from inventory i
where i.location_id not in ('QC0','QC1','QC2','QCHOLD','WP1','SUSPENSE')
and i.client_id like '&&2'
and i.location_id like 'D%'
/

select 'in QC0' A,
nvl(sum(jc.qty_on_hand),0) B 
from inventory jc
where jc.location_id='QC0'
and jc.client_id like '&&2'
and jc.location_id like 'D%'
/

select 'in QC1' A,
nvl(sum(jc.qty_on_hand),0) B 
from inventory jc
where jc.location_id='QC1'
and jc.client_id like '&&2'
and jc.location_id like 'D%'
/

select 'in QC2' A,
nvl(sum(jc.qty_on_hand),0) B 
from inventory jc
where jc.location_id='QC2'
and jc.client_id like '&&2'
and jc.location_id like 'D%'
/

select 'in QCHOLD' A,
nvl(sum(jc.qty_on_hand),0) B 
from inventory jc
where jc.location_id='QCHOLD'
and jc.client_id like '&&2'
and jc.location_id like 'D%'
/

select 'in WP1' A,
nvl(sum(jc.qty_on_hand),0) B
from inventory jc
where jc.location_id='WP1'
and jc.client_id like '&&2'
and jc.location_id like 'D%'
/

select
'Full Total' A,
sum(i.qty_on_hand) B
from inventory i
where i.client_id like '&&2'
and i.location_id like 'D%'
/
