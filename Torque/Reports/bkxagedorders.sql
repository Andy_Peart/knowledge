SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240



set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


--ttitle  LEFT 'Wots NOT picked for client ID &&2 as at ' report_date RIGHT 'Page:' FORMAT 999 SQL.PNO skip 2

--break on F skip 1 on A skip 1
--compute sum label 'Total' of E on A

select 'Created,Age,Status,Order #,Store,,Type,Ordered,Picked,Tasked,Shipped,Consignment,Opened' from DUAL;


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DAY, DD MONTH, YYYY') curdate from DUAL;
set TERMOUT ON

--ttitle  LEFT 'TML Retail - Open Orders over 3 days old - to check as at    ' report_date skip 2


break on a.name skip 1 

--compute sum of QQQ on CCC


select
to_char(oh.creation_date, 'DD/MM/YY') ||','||
trunc(5*(trunc(sysdate)-trunc(oh.creation_date))/7)  ||','||
oh.status  ||','||
oh.order_id  ||','||
oh.customer_id  ||','||
a.name  ||','||
oh.order_type  ||','||
OO  ||','||
PP  ||','||
KK  ||','||
SS  ||','||
oh.consignment  ||','||
--to_char(a.user_def_date_1, 'DD/MM/YY') MM
CASE WHEN (trunc(a.user_def_date_1)-trunc(sysdate)>0) THEN 'Not Open Yet'
  	ELSE 'Trading'
	END 
from order_header oh,address a,
(select
oh.order_id  ID,
nvl(sum(ol.qty_ordered),0) OO,
nvl(sum(ol.qty_picked),0) PP,
nvl(sum(ol.qty_tasked),0) KK,
nvl(sum(ol.qty_shipped),0) SS
from order_header oh,order_line ol
where oh.client_id='TR'
and ol.client_id='TR'
and oh.order_id=ol.order_id
group by
oh.order_id)
where oh.order_id=ID
and trunc(sysdate)-trunc(oh.creation_date)>5
and ((trunc(a.user_def_date_1)-trunc(sysdate)<1) or (a.user_def_date_1 is null))
and oh.status not in ('Cancelled','Hold','Shipped')
--and (oh.consignment is null and oh.order_type<>'RATIO' and oh.status<>'Released')
and oh.client_id='TR'
and a.client_id='TR'
--and a.user_def_date_1 is null
and oh.customer_id=a.address_id
order by
oh.creation_date,
oh.customer_id
/
