SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON

select 'Date,SO Number,Item,Description,Qty,R E,Return Reason,Notes' from dual;

select A1 || ',' || A || ',' || B || ',' || C || ',' || D || ',' || E || ',' || F || ',' || G from 
(
select 
to_char(itl.dstamp, 'DD-MON-YYYY') as A1
,itl.user_def_type_1 A
,itl.sku_id AS B
,libsku.getuserlangskudesc(itl.client_id,itl.sku_id) AS C
, to_char(sum(itl.update_qty)) AS  D
, CASE nvl(itl.user_def_chk_1,'N')
		WHEN 'Y' THEN 'EXCHANGE'
		WHEN 'N' THEN 'RETURN'
		ELSE 'N/A'
	END as E
, CASE itl.user_def_type_2
		WHEN '1' THEN 'TOO BIG'
		WHEN '2' THEN 'TOO SMALL'
		WHEN '3' THEN 'TOO LONG'
		WHEN '4' THEN 'TOO SHORT'
		WHEN '5' THEN 'ORDERED FOR CHOICE'
		WHEN '6' THEN 'STYLE DOESNT SUIT'
		WHEN '7' THEN 'ARRIVED LATE'
		WHEN '8' THEN 'LOOK DIFFERENT TO WEBSITE'
		WHEN '9' THEN 'INCORRECT ITEM'
		WHEN '10' THEN 'UNWANTED GIFT'
		WHEN '11' THEN 'FAULTY ITEM/PACKAGING'
		WHEN '12' THEN 'CHANGED MY MIND'
		ELSE itl.user_def_type_2 
	END AS F
, replace(itl.user_def_note_1,',','') AS G
FROM
	inventory_transaction itl
WHERE
	itl.client_id        = '&&2'
	and itl.code in ('Receipt')
	AND itl.reference_id = 'R'
  AND itl.dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
group by
to_char(itl.dstamp, 'DD-MON-YYYY'),
replace(itl.user_def_type_2,' ','_')
, itl.user_def_type_1
, itl.sku_id 
, CASE nvl(itl.user_def_chk_1,'N')
		WHEN 'Y' THEN 'EXCHANGE'
		WHEN 'N' THEN 'RETURN'
		ELSE 'N/A'
	END
,CASE itl.user_def_type_2
		WHEN '1' THEN 'TOO BIG'
		WHEN '2' THEN 'TOO SMALL'
		WHEN '3' THEN 'TOO LONG'
		WHEN '4' THEN 'TOO SHORT'
		WHEN '5' THEN 'ORDERED FOR CHOICE'
		WHEN '6' THEN 'STYLE DOESNT SUIT'
		WHEN '7' THEN 'ARRIVED LATE'
		WHEN '8' THEN 'LOOK DIFFERENT TO WEBSITE'
		WHEN '9' THEN 'INCORRECT ITEM'
		WHEN '10' THEN 'UNWANTED GIFT'
		WHEN '11' THEN 'FAULTY ITEM/PACKAGING'
		WHEN '12' THEN 'CHANGED MY MIND'
		ELSE itl.user_def_type_2 
	END
, itl.user_def_note_1
, libsku.getuserlangskudesc(itl.client_id,itl.sku_id)
order by 1
)
/