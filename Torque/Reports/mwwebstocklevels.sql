SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240
SET TRIMSPOOL ON

column R heading 'Type' format a18
column W heading 'Desc' format a40
column S heading 'Stk1' format 999999
column T heading 'Stk2' format 999999
column RL format 999999
column U heading 'Loc' format a10
column V heading 'DR' format a12

break on R 

--spool test.csv


select 'SKU,Description,Stock in W Locations,Other Stock,Other Locations,Reserve,Date Received,Ordered,' from DUAL;

select
R,
W,
S,
T,
U,
RL,
V,
nvl(QQ,0),
' '
from (select
distinct
i.sku_id  R,
sku.description W,
nvl(B,0) S,
nvl(B1,0) T,
nvl(LL,' ') U,
nvl(sku.user_def_num_3,0) RL,
RD V
from inventory i,sku,(select
iv.sku_id A,
sum(iv.qty_on_hand-iv.qty_allocated) B
from inventory iv
where iv.client_id = 'MOUNTAIN'
and iv.location_id like 'W%'
group by iv.sku_id),(select
iv.sku_id A1,
iv.location_id LL,
trunc(iv.receipt_Dstamp) RD,
sum(iv.qty_on_hand-iv.qty_allocated) B1
from inventory iv
where iv.client_id = 'MOUNTAIN'
and (iv.location_id like 'F%'
or iv.location_id like 'G%')
group by iv.sku_id,iv.location_id,trunc(iv.receipt_Dstamp))
where i.client_id='MOUNTAIN'
and i.sku_id=A(+)
and i.sku_id=A1(+)
and i.sku_id=sku.sku_id
and sku.client_id='MOUNTAIN'),(select
ol.sku_id SS,
sum(ol.qty_ordered) QQ
from order_header oh, order_line ol
where oh.client_id='MOUNTAIN'
and oh.Order_date>sysdate-21
and Oh.order_id not like 'MOR%'
and oh.order_id=ol.order_id
and ol.client_id='MOUNTAIN'
group by
ol.sku_id)
where S<20
and T>0
and R=SS (+)
order by R,V
/

--spool off
