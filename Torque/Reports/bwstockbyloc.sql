set feedback off
set pagesize 0
set linesize 120
set verify off
set colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

 

column A heading 'Location' format A11
column B heading 'SKU' format A30
column C heading 'Description' format A40
column D heading 'Qty' format 999999
--column E heading '  Moved' format A14

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

set und '-'
set heading on
set newpage 0
set trimspool on


spool BWSTOCK.csv

--ttitle  LEFT 'Stock Units by Location Range from &&2 to &&3 - for BERWIN as at ' report_date RIGHT 'Page:' FORMAT 999 SQL.PNO skip 4

--break on report

--compute sum label 'Total' of D on report

--break on row skip 1

select 'SKU,Description,Location,Condition,Qty' from DUAL;

select
i.sku_id||','||
Sku.description||','||
i.location_id||','||
i.condition_id ||','||
sum(i.qty_on_hand)
from inventory i,sku
where i.client_id like 'BW'
and location_id between '&&2' and '&&3'
and i.sku_id=sku.sku_id
group by i.sku_id, Sku.description, i.location_id, i.condition_id
order by i.location_id,i.sku_id
/

spool off
