SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Store,Order id,Shipped Date,Shipped Time,Units Shipped' from dual
union all
select "Store"||','||"Order"||','||"Shipdate"||','||"Shiptime"||','||"Units Ship"
from
(
select oh.work_group    as "Store"
, oh.order_id   as "Order"
, to_char(oh.shipped_date, 'DD/MM/YYYY')    as "Shipdate"
, to_char(oh.shipped_date, 'HH24:MI:SS')    as "Shiptime"
, sum(ol.qty_shipped)   as "Units Ship"
from order_header oh
inner join order_line ol on ol.client_id = oh.client_id and ol.order_id = oh.order_id
inner join address a on a.client_id = oh.client_id and a.address_id = oh.work_group
where oh.client_id = 'MOUNTAIN'
and oh.status = 'Shipped'
and oh.shipped_date between to_date('&&2') and to_date('&&3')+1
and a.user_def_note_1 = 'TNT-MANIFEST'
group by oh.work_group
, oh.order_id
, to_char(oh.shipped_date, 'DD/MM/YYYY')
, to_char(oh.shipped_date, 'HH24:MI:SS')
order by work_group
)

/
--spool off