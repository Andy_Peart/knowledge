SET FEEDBACK OFF                 
set verify off
set wrap off
set colsep ','

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 500
set trimspool on

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


break on report
compute sum label 'Totals' of A2 A3 A4 A5 on report

--spool sostock.csv


select 'SKU,Description,Qty On Hand,Qty Unallocatable,Qty Allocated,Qty Available' from DUAL;

select
distinct
''''||AA||'''' A1,
AB ,
nvl(BB,0) A2,
nvl(DD,0) A3,
nvl(CC,0) A4,
nvl(BB,0)-nvl(DD,0)-nvl(CC,0) A5
from (select
jc.sku_id AA,
jc.description AB,
sum(jc.qty_on_hand) BB,
sum(jc.qty_allocated) CC
from inventory jc
where jc.client_id='SO'
group by
jc.sku_id,jc.description),(select
jc.sku_id AAA,
nvl(sum(jc.qty_on_hand),0)-nvl(sum(jc.qty_allocated),0) DD
from inventory jc,location lc
where jc.client_id='SO'
and jc.location_id=lc.location_id
and jc.site_id=lc.site_id
and (lc.disallow_alloc='Y' or jc.lock_status in ('Locked','OutLocked'))
group by
jc.sku_id)
where AA=AAA (+)
--and nvl(BB,0)<>0
order by 1
/

--spool off
