SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 90
set newpage 0
set verify off
set colsep ','



column A heading 'W/C' format A12
column C heading 'Branch' format A8
column B format 999999
column D format 999999
column E format 9999.99
column F format 999 noprint
column G format 999999
column H format 9999.99
column J format 999999

--break on report

--compute sum label 'Total' of B D G J on report

--spool newtots2.csv

select 'W/C,Stores,Orders,Shipments,Units,Ave. Units Shipped per Order,Ave. Units per Shipment,' from DUAL;

select
to_char(trunc(to_date('&&2', 'DD-MON-YYYY'))+(AAA*7),'DD-MON-YYYY') A,
AAA F,
count(*) G,
sum(JJJ) J,
sum(BBB) B,
sum(DDD) D,
sum(DDD)/sum(JJJ) H,
sum(DDD)/sum(BBB) E,
' '
from (select
trunc(((trunc(Dstamp))-(trunc(to_date('&&2', 'DD-MON-YYYY'))))/7) AAA,
customer_id CCC,
count(*) BBB,
count(distinct reference_id) JJJ,
sum (update_qty) DDD
from inventory_transaction
where client_id = 'MOUNTAIN'
and code = 'Shipment'
and customer_id<>'WWW'
and dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and nvl(update_qty,0)>0
and upper(sku_id)=lower(sku_id)
group by
trunc(((trunc(Dstamp))-(trunc(to_date('&&2', 'DD-MON-YYYY'))))/7),
customer_id)
group by AAA
order by F
/

select
'  Totals' A,
count(*) G,
sum(JJJ) J,
sum(BBB) B,
sum(DDD) D,
sum(DDD)/sum(JJJ) H,
sum(DDD)/sum(BBB) E,
' '
from (select
count(*) BBB,
count(distinct reference_id) JJJ,
sum (update_qty) DDD
from inventory_transaction
where client_id = 'MOUNTAIN'
and code = 'Shipment'
and customer_id<>'WWW'
and dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and nvl(update_qty,0)>0
and upper(sku_id)=lower(sku_id))
/


--spool off
