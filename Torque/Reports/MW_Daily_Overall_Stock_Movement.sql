SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','
SET UND OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 320

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(dstamp, 'DD/MM/YYYY  HH:MI') curdate from DUAL;
set TERMOUT ON


--select ',Customer,Date,SKU,Reference,,Qty,' from DUAL;

--break on report

--compute sum label 'Total'  of G  on report
select 'DATE', 'SKU_RECORDS', 'TOTAL_QTY', 'RECORD_TYPE'
from dual;

select unique DSTAMP, sku_records, total_qty, 'Last Nights Stock Level' Record_Type from mw_ism_record , (select max(dstamp) zzz from mw_ism_record)
where dstamp = zzz
union all
select unique trunc(dstamp), count(sku_id), sum(update_qty), 'Adjustments' 
from inventory_transaction
where trunc(dstamp) = trunc(sysdate)
and client_id = 'MOUNTAIN'
and code = 'Adjustment'
group by trunc(dstamp)
union all
select unique trunc(dstamp), count(sku_id), sum(update_qty), 'Receipts' 
from inventory_transaction
where trunc(dstamp) = trunc(sysdate)
and client_id = 'MOUNTAIN'
and code = 'Receipt'
group by trunc(dstamp)
union all
select unique trunc(dstamp), count(sku_id), sum(update_qty), 'Shipments' 
from inventory_transaction
where trunc(dstamp) = trunc(sysdate)
and client_id = 'MOUNTAIN'
and code = 'Shipment'
group by trunc(dstamp)
union all
select ZZ, 0, A+BB+CC-DD E, 'Calculated Balance'
from 
(select ZZ, A, nvl(B, 0)BB, nvl(C,0)CC, nvl(D,0)DD
from 
(select trunc(sysdate) ZZ, total_qty A
from mw_ism_record, (select max(dstamp) Z from mw_ism_record)
where dstamp = Z)
, 
(select sum(update_qty) B
from inventory_transaction
where client_id = 'MOUNTAIN'
and trunc(dstamp) = trunc(sysdate)
and code = 'Adjustment')
,
(select sum(update_qty) C
from inventory_transaction
where client_id = 'MOUNTAIN'
and trunc(dstamp) = trunc(sysdate)
and code = 'Receipt')
,
(select sum(update_qty) D
from inventory_transaction
where client_id = 'MOUNTAIN'
and trunc(dstamp) = trunc(sysdate)
and code = 'Shipment')
)
union all
select unique trunc(sysdate) DSTAMP, count(sku_id)SKU_RECORDS, sum(qty_on_hand)TOTAL_QTY, 'Current System Stock Level' from inventory
where client_id = 'MOUNTAIN'
--and location_id <> 'DESPATCH'
--and location_id <> 'SUSPENSE'
--and sku_id <> 'TEST'
;

insert into MW_ISM_RECORD
select unique trunc(sysdate) DSTAMP, count(sku_id)SKU_RECORDS, sum(qty_on_hand)TOTAL_QTY from inventory
where client_id = 'MOUNTAIN'
--and location_id <> 'DESPATCH'
--and location_id <> 'SUSPENSE'
--and sku_id <> 'TEST'
;

commit;
 
