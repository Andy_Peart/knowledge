SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 600
SET TRIMSPOOL ON

break on D dup skip 1

compute sum label 'Totals' of F G on D

--spool mwvload.csv

select 'Order ID,Store,Date,Trailer ID,Pallet,Lines,Qty' from DUAL;

select
reference_id A,
work_group B,
trunc(Dstamp) C,
to_loc_id D,
pallet_id E,
count(*) F,
sum(update_qty) G
from inventory_transaction
where client_id='MOUNTAIN'
and reference_id='&&2'
and code like 'Vehicle Load'
group by
reference_id,
work_group,
trunc(Dstamp),
to_loc_id,
pallet_id 
order by 
D,E
/

--spool off
