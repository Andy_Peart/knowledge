SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

Select 'Order No,Status,Order Date,Creation Date,Consignment,Name,Notes,SKU,Description,SKU Size,Colour,Qty Ordered,Qty Allocated,Qty not in Warehouse,Qty on Hand,Qty Allocated to Other Orders' from DUAL;


column A format A10
column B format A12
column C format A12
column CC format A12
column CCC format A12
column D format A32
column E format A16
column F format A10
column G format A30
column H format A10
column I format A10
column J format 99999
column K format 99999
column L format 99999
column LL format 99999
column LLL format 99999
column M format 9 noprint

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



--ttitle  LEFT 'TML Retail - Open Orders Report ' RIGHT 'Printed ' report_date skip 2


--break on CC skip 1

--compute sum label 'Grand Total' of QQQ on report
--compute sum label 'Total Units' of QQQ on HH

select
oh.ORDER_ID A,
oh.STATUS B,
DECODE (oh.status,'In Progress','1',
		  'Allocated','2','3') M,
to_char(oh.ORDER_DATE,'DD-MM-YYYY') C,
to_char(oh.creation_DATE,'DD-MM-YYYY') CC,
oh.Consignment CCC,
oh.NAME D,
oh.USER_DEF_TYPE_7 E,
ol.SKU_ID F,
'"' || sku.description || '"' G,
SKU.SKU_SIZE H,
SKU.COLOUR I,
ol.QTY_ORDERED J,
nvl(ol.qty_tasked,0) K,
ol.QTY_ORDERED-nvl(ol.qty_tasked,0) L,
nvl(FFF,0) LLL, 
nvl(BBB,0) LL 
from order_header oh,order_line ol,sku,
(select
sku_id SS,
sum(qty_tasked) BBB
from order_line
where client_id='SB'
group by
sku_id),
(select
sku_id SSS,
sum(nvl(qty_on_hand,0)) FFF 
from inventory
where client_id='SB'
and condition_id='GOOD'
group by
sku_id)
where oh.CLIENT_ID='SB'
and oh.order_ID like 'C%'
and oh.order_ID=ol.order_id
and oh.status in ('Released','Allocated','In Progress')
and ol.QTY_ORDERED-nvl(ol.qty_tasked,0)>0
and ol.sku_id=sku.sku_id
and ol.sku_id=SS (+)
and ol.sku_id=SSS (+)
order by
M,A,F
/

