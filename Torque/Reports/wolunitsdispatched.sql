set feedback off
set pagesize 68
set linesize 240
set verify off

clear columns
clear breaks
clear computes

column D heading 'Date' format a16
column A heading 'Reference' format A20
column C heading 'Orders' format 999999
column B heading 'Units' format 999999
column M noprint


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

set heading on

ttitle  LEFT 'Orders/Units Dispatched from '&&2' to '&&3' - for client ID WOL as at ' report_date skip 2

break on D skip 1 on report

compute sum label 'Day Total' of B C on D
compute sum label 'Total' of B C on report



select
trunc(Dstamp) D,
reference_id A,
count(distinct reference_id) C,
sum(update_qty) B
from inventory_transaction
where client_id='WOL'
and code='Shipment'
and Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by
trunc(Dstamp),
reference_id
order by
trunc(Dstamp),
reference_id
/
