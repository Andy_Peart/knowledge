SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON



SELECT 'PO,Supplier,Name,Creation Date,Due Date,Booked Date,Started,Completed,Qty Due,Qty Received,Difference'
  FROM DUAL
UNION ALL
SELECT *
  FROM (SELECT PH.PRE_ADVICE_ID || ',' || PH.SUPPLIER_ID || ',' ||
               NVL(NMR, ' ') || ',' ||
               TO_CHAR(PH.CREATION_DATE, 'DD/MM/YYYY HH24:MI:SS') || ',' ||
               TO_CHAR(PH.DUE_DSTAMP, 'DD/MM/YYYY HH24:MI:SS') || ',' ||
               'Booked Date' || ',' ||
               TO_CHAR(PH.ACTUAL_DSTAMP, 'DD/MM/YYYY HH24:MI:SS') || ',' ||
               TO_CHAR(PH.FINISH_DSTAMP, 'DD/MM/YYYY HH24:MI:SS') || ',' ||
               SUM(PL.QTY_DUE) || ',' || SUM(NVL(PL.QTY_RECEIVED, 0)) || ',' ||
               SUM(NVL(PL.QTY_RECEIVED, 0) - PL.QTY_DUE)
          FROM PRE_ADVICE_HEADER PH,
               PRE_ADVICE_LINE PL,
               (SELECT ADDRESS_ID ADR, NAME NMR
                  FROM ADDRESS
                 WHERE CLIENT_ID = 'CC')
         WHERE PH.CLIENT_ID = 'CC'
           AND PH.STATUS = 'Complete'
           AND PH.FINISH_DSTAMP > TRUNC(SYSDATE - 7)
           AND PH.CLIENT_ID = PL.CLIENT_ID
           AND PH.PRE_ADVICE_ID = PL.PRE_ADVICE_ID
           AND PH.SUPPLIER_ID = ADR(+)
         GROUP BY PH.PRE_ADVICE_ID,
                  PH.SUPPLIER_ID,
                  NVL(NMR, ' '),
                  TO_CHAR(PH.CREATION_DATE, 'DD/MM/YYYY HH24:MI:SS'),
                  TO_CHAR(PH.DUE_DSTAMP, 'DD/MM/YYYY HH24:MI:SS'),
                  'Booked Date',
                  TO_CHAR(PH.ACTUAL_DSTAMP, 'DD/MM/YYYY HH24:MI:SS'),
                  TO_CHAR(PH.FINISH_DSTAMP, 'DD/MM/YYYY HH24:MI:SS')
         ORDER BY 1)
UNION ALL
SELECT *
  FROM (SELECT ',,,,,,,Totals,' || SUM(PL.QTY_DUE) || ',' ||
               SUM(NVL(PL.QTY_RECEIVED, 0)) || ',' ||
               SUM(NVL(PL.QTY_RECEIVED, 0) - PL.QTY_DUE)
          FROM PRE_ADVICE_HEADER PH, PRE_ADVICE_LINE PL
         WHERE PH.PRE_ADVICE_ID = PL.PRE_ADVICE_ID
           AND PH.CLIENT_ID = 'CC'
           AND PH.STATUS = 'Complete'
           AND PH.FINISH_DSTAMP > TRUNC(SYSDATE - 7))		 
/
