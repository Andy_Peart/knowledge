SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 52


column A heading 'SKU' format A12
column B heading 'Location' format A12
column C heading 'Qty' format 9999999
column D heading 'Date Received' format A16


select 'SKU,Location,Qty,Date Received,,' from DUAL;

break on report

compute sum label 'Total' of C on report

select
sku_id A,
Location_id B,
sum(qty_on_hand) C,
to_char(receipt_Dstamp,'DD-Mon-YYYY') D
from inventory
where client_id='PR'
and location_id not like 'QC%'
and location_id not like 'SUSP%'
and location_id<>'T'
and location_id<>'TR'
group by
sku_id,
Location_id,
to_char(receipt_Dstamp,'DD-Mon-YYYY') 
order by B
/


select
sku_id A,
Location_id B,
sum(qty_on_hand) C,
to_char(receipt_Dstamp,'DD-Mon-YYYY') D
from inventory
where client_id='PR'
and location_id like 'QC%'
group by
sku_id,
Location_id,
to_char(receipt_Dstamp,'DD-Mon-YYYY') 
order by B
/
