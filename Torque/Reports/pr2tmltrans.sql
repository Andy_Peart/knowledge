/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :     pr2tmltrans.sql                         		      */
/*                                                                            */
/*     DESCRIPTION:                                                           */
/*     CSV version compares shipments from pr to received in t or tr          */
/*                                                                            */
/*   DATE     BY   DESCRIPTION						                          */
/*   ======== ==== ===========					                              */
/*   04/11/04 BS   initial version     				                          */
/*   15/09/06 MS   modified to include ean			                          */
/*   19/02/07 BK   modified to change text                                    */ 
/*   12/03/07 BK   modified to include SKU description                        */
/*   14/05/07 BK   rehashed to show groups and products                       */
/*   19/09/08 BK   New version to write to CSV file                           */
/*   20/12/12 YB   Created new report using existing as basis				  */
/******************************************************************************/
SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','
SET WRAP OFF


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500

/* Column Headings and Formats */
COLUMN A heading "From" FORMAT a5
COLUMN B heading "Pre Advice" FORMAT a10
COLUMN C HEADING "SKU ID" FORMAT a14
COLUMN D HEADING "Ordered" FORMAT 9999
COLUMN E heading "Shipped" FORMAT 9999
COLUMN F heading "TO" FORMAT a4
COLUMN G heading "Pre Advice" FORMAT a10
COLUMN H HEADING "SKU" FORMAT a14
COLUMN I heading "Expected" FORMAT 9999
COLUMN J heading "Received" FORMAT 9999

break on report

compute sum label 'Total' of D E I J on report

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

SPOOL PR2TML_TRANSFER_.CSV

/* Top Title */
select 'From, Pre Advice, SKU, Ordered, Shipped, To, Pre Advice, SKU, Expected, Received' from DUAL;

WITH T1 AS (
SELECT OL.CLIENT_ID A, 
OL.ORDER_ID B, 
OL.SKU_ID C, 
SUM(OL.QTY_ORDERED) D, 
SUM(OL.QTY_SHIPPED) E
FROM ORDER_LINE OL
WHERE OL.CLIENT_ID in ('PR','PS')
AND OL.ORDER_ID = '&&2'
GROUP BY
OL.CLIENT_ID, 
OL.ORDER_ID, 
OL.SKU_ID),
T2 AS (
SELECT PAL.CLIENT_ID F, 
PAL.PRE_ADVICE_ID G, 
PAL.SKU_ID H, 
SUM(PAL.QTY_DUE) I, 
SUM(PAL.QTY_RECEIVED) J
FROM PRE_ADVICE_LINE PAL
WHERE PAL.CLIENT_ID LIKE 'T%'
AND PAL.PRE_ADVICE_ID = '&&2'
GROUP BY
PAL.CLIENT_ID, 
PAL.PRE_ADVICE_ID, 
PAL.SKU_ID)
SELECT * FROM T1 FULL OUTER JOIN T2
ON T1.C=T2.H
/

SPOOL OFF
