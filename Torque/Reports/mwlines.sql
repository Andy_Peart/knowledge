SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON

--spool mwlines.csv

select 'Creation Date, Due Date, Pre Advice Id,Sku,Colour,Size,Description,Qty Due,Qty Received,Overdue' From DUAL;

select
trunc(ph.creation_date),
trunc(ph.due_dstamp),
pl.pre_advice_id,
pl.sku_id,
sku.colour,
sku.sku_size,
sku.description,
pl.qty_due,
nvl(pl.qty_received,0),
pl.qty_due-nvl(pl.qty_received,0)
from pre_advice_header ph,pre_advice_line pl,sku
where ph.client_id='MOUNTAIN'
and ph.status='Released'
and ph.due_dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and pl.client_id=ph.client_id
and pl.pre_advice_id=ph.pre_advice_id
and pl.qty_due-nvl(pl.qty_received,0)>0
and pl.client_id=sku.client_id
and pl.sku_id=sku.sku_id
order by 1,2
/

--spool off

