SET FEEDBACK OFF             
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 300
SET TRIMSPOOL ON

--spool mwrezonenew.csv

--select to_char(SYSDATE, 'DD/MM/YY  HH:MI:SS') curdate from DUAL;

select 'Due Date,LPG,Cartons Expected,Units Due,Carton Average,Est Pallets Due,Current Stock,Current Cartons,Total Cartons,Est Pallet spaces' from dual;

select
replace(XXX,' ','')
from (select
DTE||','||
LPG||','||
QQ||','||
QU||','||
to_char(QU/QQ,'9999')||','||
to_char(QQ/18,'9999')||','||
QS||','||
to_char(QS/(QU/QQ),'999999')||','||
to_char(QQ+(QS/(QU/QQ)),'999999')||','||
to_char((QQ+(QS/(QU/QQ)))/18,'999999')||','||
' ' XXX
from (select
trunc(mo.thedate) DTE,
--container CT,
LPG,
sum(mo.cartons) QQ,
sum(QU) QU,
QS
from mountainorders mo,(select
--trunc(ph.due_dstamp) DTE,
pl.pre_advice_id PID,
sku.user_def_type_8 LPG,
sum(pl.qty_due) QU
from pre_advice_header ph,pre_advice_line pl,sku
where ph.client_id='MOUNTAIN'
--and ph.due_dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and ph.pre_advice_id=pl.pre_advice_id
and pl.client_id='MOUNTAIN'
and sku.client_id = 'MOUNTAIN'
and sku.sku_id=pl.sku_id
group by
--trunc(ph.due_dstamp),
pl.pre_advice_id,
sku.user_def_type_8),(select
sku.user_def_type_8 LPG2,
sum(qty_on_hand) QS
from inventory iv,sku
where iv.client_id='MOUNTAIN'
and iv.sku_id=sku.sku_id
and sku.client_id='MOUNTAIN'
group by
sku.user_def_type_8)
where substr(mo.cw_order,1,11)=substr(PID,1,11)
and mo.thedate between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and LPG=LPG2
group by
trunc(mo.thedate),
--container,
LPG,
QS
order by
DTE,
LPG))
/

--spool off

