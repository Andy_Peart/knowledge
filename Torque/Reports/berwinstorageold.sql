SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON


column A heading 'SKU' format a16
column B heading 'Date' format a12
column C heading 'Type' format a8
column H heading 'Description' format a12
column D heading 'Units' format 999999
column G heading 'Days' format 999
column F heading '   Rate' format a7
column E heading 'Charge' format 999999.99

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


--ttitle 'Berwin and Berwin Storage Charges for &&2 to &&3'

break on A dup on report

compute sum label 'Nett Charge' of E on A
compute sum label 'Total Charge' of E on report

--spool storage4.csv

select 'SKU,Date,Type,Units,Description,Days,Rate,Charge' from DUAL;


select
AAA A,
to_char(BBB,'DD/MM/YY') B,
CCC C,
DDD D,
HHH H,
to_date('&&3', 'DD-MON-YYYY')-BBB+DECODE(CCC,'OUT',0,1) G,
DECODE(HHH,'TROUSERS',' 0.04/7',' 0.05/7') F,
(to_date('&&3', 'DD-MON-YYYY')-BBB+DECODE(CCC,'OUT',0,1))*DDD*(DECODE(HHH,'TROUSERS','0.04','0.05')/7)*DECODE(CCC,'OUT',-1,1) E
from (select
it.sku_id AAA,
trunc(it.Dstamp) BBB,
DECODE(it.code,'Receipt','IN','Shipment','OUT','IN') CCC,
sum(it.update_qty) DDD,
sku.description HHH
from inventory_transaction it,sku
where it.client_id='BW'
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and (it.code in ('Receipt','Shipment') or (it.code='Adjustment' and it.reason_id='RECRV'))
and sku.client_id=it.client_id
and sku.sku_id=it.sku_id
--and it.sku_id in ('PORD131288','PORD1-018607-0')
group by
it.sku_id,
trunc(it.Dstamp),
DECODE(it.code,'Receipt','IN','Shipment','OUT','IN'),
sku.description)
where DDD>0
union
select
the_sku A,
to_char(the_date,'DD/MM/YY') B,
'B/FWD' C,
the_qty D,
sku.description H,
to_date('&&3', 'DD-MON-YYYY')-the_date+1 G,
DECODE(sku.description,'TROUSERS',' 0.04/7',' 0.05/7') F,
(to_date('&&3', 'DD-MON-YYYY')-the_date+1)*the_qty*(DECODE(sku.description,'TROUSERS','0.04','0.05')/7) E
from berwin_snapshot,sku
where the_date=to_date('&&2','DD-MON-YYYY')
--and the_sku in ('PORD131288','PORD1-018607-0')
and sku.client_id='BW'
and sku.sku_id=the_sku
order by A,B,C
/

--spool off
