SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET WRAP OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
set linesize 200

--ttitle 'Mountain Warehouse Overpicks for Period &&2 to &&3' skip 2


Select 'Order ID,Created Date,and Time,Consignment,Status,Ordered,Tasked,Shipped,Lines,Shipped Date,and Time,Turn Round Time (D:H:M),Country,,' from DUAL;


select
oh.order_id||','||
to_char(oh.creation_date,'DD/MM/YY,HH24:MI:SS')||','||
oh.consignment||','||
oh.status||','||
nvl(AAA,0)||','||
nvl(BBB,0)||','||
nvl(CCC,0)||','||
oh.num_lines||','||
nvl(to_char(oh.shipped_date,'DD/MM/YY,HH24:MI:SS'),',')||','||
trunc(cast(oh.shipped_date AS DATE)-cast(oh.creation_date AS DATE))||':'||
trunc( mod( (cast(oh.shipped_date AS DATE)-cast(oh.creation_date AS DATE))*24, 24 ) )||':'||
trunc( mod( (cast(oh.shipped_date AS DATE)-cast(oh.creation_date AS DATE))*24*60, 60 ) )||','||
nvl(country.tandata_id,oh.country)||','||
' '
from order_header oh,country,(select
oh.order_id OO,
sum(ol.qty_ordered) AAA,
sum(ol.qty_tasked) BBB,
sum(ol.qty_shipped) CCC
from order_header oh,order_line ol
where oh.client_id = 'T'
and oh.ship_dock='PACK1'
and oh.creation_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and to_number(to_char(oh.creation_date,'HH24'))<'&&4'
and oh.order_id=ol.order_id
and ol.client_id = 'T'
group by
oh.order_id)
where oh.client_id = 'T'
and oh.ship_dock='PACK1'
and oh.creation_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and to_number(to_char(oh.creation_date,'HH24'))<'&&4'
and oh.country=country.iso3_id (+)
and oh.order_id=OO
order by
oh.order_id
/
