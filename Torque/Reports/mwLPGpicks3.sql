SET FEEDBACK OFF             
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 300
SET TRIMSPOOL ON

--spool mwLPGpicks3.csv

select 'Date,Stores,Lines,Units' from DUAL
union all
select dstamp ||','|| ords ||','|| lines ||','|| units from
(
    select dstamp, ords, lines, units from
(
select
trunc(it.dstamp) dstamp
, count(*) lines
, sum(it.update_qty) units
, count(distinct it.reference_id) ords
from inventory_transaction it,sku
where it.Dstamp between to_date('&&2 &&4', 'DD-MON-YYYY hh24:mi:ss') and to_date('&&3 &&5', 'DD-MON-YYYY hh24:mi:ss')
and it.client_id='MOUNTAIN'
and it.code like 'Pick'
and (it.to_loc_id = 'CONTAINER'
    or
    it.to_loc_id = 'DESPATCH' and it.container_id is null)
and it.sku_id=sku.sku_id
and it.client_id=sku.client_id
and substr(work_group,2,2) <> 'WW'
and substr(it.reference_id,1,3) <> 'MSO'
and substr(work_group,1,3) not in ('AAC','AAU','NQC','NQT')
and it.update_qty <> 0
group by trunc(it.dstamp)
union all
select
trunc(it.dstamp) dstamp
, count(*) lines
, sum(it.update_qty) units
, count(distinct it.reference_id) ords
from inventory_transaction_archive it,sku
where it.Dstamp between to_date('&&2 &&4', 'DD-MON-YYYY hh24:mi:ss') and to_date('&&3 &&5', 'DD-MON-YYYY hh24:mi:ss')
and it.client_id='MOUNTAIN'
and it.code like 'Pick'
and (it.to_loc_id = 'CONTAINER'
    or
    it.to_loc_id = 'DESPATCH' and it.container_id is null)
and it.sku_id=sku.sku_id
and it.client_id=sku.client_id
and substr(work_group,2,2) <> 'WW'
and substr(it.reference_id,1,3) <> 'MSO'
and substr(work_group,1,3) not in ('AAC','AAU','NQC','NQT')
and it.update_qty <> 0
group by trunc(it.dstamp)
)
order by dstamp
)
/

--spool off
