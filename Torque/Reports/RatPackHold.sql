set pagesize 2000
set linesize 2000
set verify off

clear columns
clear breaks
clear computes

select 
sku_id SKU,
qty_on_hand Qty,
location_id Location,
receipt_id Receipt
from inventory
where client_id='TR'
and condition_id='HOLD'
and move_dstamp <sysdate - '&2'
/
