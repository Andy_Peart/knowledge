SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 200
set trimspool on
set newpage 0
set verify off


select 'Run Time : '||to_char(sysdate,'DD/MM/YY HH:MI:SS') from DUAL
union all
select
'Total ND 888 and 824 Orders Created in 24 hours up to 2.30pm Today '||count(*)
from order_header oh
where oh.client_id='MOUNTAIN'
and oh.creation_date>trunc(sysdate)-1+0.60417
and oh.creation_date<trunc(sysdate)+0.60417
and oh.work_group='WWW'
and oh.priority in ('888','824')
union all
select
'ND 888 and 824 Orders Still Outstanding at 2.30pm '||count(*)
from order_header oh
where oh.client_id='MOUNTAIN'
and oh.creation_date>trunc(sysdate)-1+0.60417
and oh.creation_date<trunc(sysdate)+0.60417
and oh.work_group='WWW'
and oh.consignment not like 'S%'
and oh.priority in ('888','824')
and oh.order_id not in (select
reference_id
from inventory_transaction
where client_id='MOUNTAIN'
and Dstamp>trunc(sysdate)-1
and Dstamp<trunc(sysdate)+0.60417
and code='Order Status'
and notes like '%> Complete')
union all
select
'ND 888 and 824 Orders Still Outstanding NOW '||count(*)
from order_header oh
where oh.client_id='MOUNTAIN'
and oh.creation_date>trunc(sysdate)-1+0.60417
and oh.creation_date<trunc(sysdate)+0.60417
and oh.work_group='WWW'
and oh.consignment not like 'S%'
and oh.priority in ('888','824')
and oh.order_id not in (select
reference_id
from inventory_transaction
where client_id='MOUNTAIN'
and Dstamp>trunc(sysdate)-1
and code='Order Status'
and notes like '%> Complete')
union all
select ' ' from DUAL
union all
select
'Total STD Orders Created YESTERDAY '||count(*)
from order_header oh
where oh.client_id='MOUNTAIN'
and oh.creation_date>trunc(sysdate)-1
and oh.creation_date<trunc(sysdate)
and oh.work_group='WWW'
and oh.priority not in ('888','824')
union all
select
'STD Orders Still Outstanding at 2.30pm '||count(*)
from order_header oh
where oh.client_id='MOUNTAIN'
and oh.creation_date>trunc(sysdate)-1
and oh.creation_date<trunc(sysdate)
and oh.work_group='WWW'
and oh.consignment not like 'S%'
and oh.priority not in ('888','824')
and oh.order_id not in (select
reference_id
from inventory_transaction
where client_id='MOUNTAIN'
and Dstamp>trunc(sysdate)-1
and Dstamp<trunc(sysdate)+0.60417
and code='Order Status'
and notes like '%> Complete')
union all
select
'STD Orders Still Outstanding NOW '||count(*)
from order_header oh
where oh.client_id='MOUNTAIN'
and oh.creation_date>trunc(sysdate)-1
and oh.creation_date<trunc(sysdate)
and oh.work_group='WWW'
and oh.consignment not like 'S%'
and oh.priority not in ('888','824')
and oh.order_id not in (select
reference_id
from inventory_transaction
where client_id='MOUNTAIN'
and Dstamp>trunc(sysdate)-1
and code='Order Status'
and notes like '%> Complete')
/

