SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','
SET UND OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 320
SET TRIMSPOOL ON




column A1 format A18
column A format A18
column B format A40
column Z format A12
column C format 9999999
column D format 9999999
column E format 9999999
column F format 9999999
column G format 9999999
column H format 9999999
column J format 9999999
column K format 9999999
column L format 9999999
column N format 9999999
column P format 9999999
column Q format 9999999
column V format 9999999
column W format 9999999


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



break on report

compute sum label 'Total'  of C D E J K G N P Q F V W on report

--set termout off
--spool dxrep3allnew.csv

select 'Full Comparison_Report - '||to_char(SYSDATE, 'DD/MM/YY  HH24:MI:SS') from DUAL;
select 
distinct 
'Report Date: '||to_char(SYSDATE, 'DD/MM/YY  HH:MI')||',Last Update: '||to_char(the_date,'DD/MM/YY HH:MI')
from dx_skufile;

select 'Category,Product,Description,Dax_qty,TORQUE_qty,Difference,        ,TORQUE_Pick,TORQUE_Alloc,TORQUE_Hold,TORQUE_TV,TORQUE_Transit,TORQUE_REJ1,TORQUE_Susp,TORQUE_Free,TORQUE_True_Diff' from DUAL;
--                                     DXQTY     QQQ                           Q4         Q5           Q2        TV        TRT          QTN        Q1      
select * from (select
nvl(product_group,'') A1,
KKK A,
nvl(description,'Not on SKU table') B,
nvl(DXQTY,0) C,
--nvl(QQQ,0) D,
--nvl(DXQTY,0)-nvl(QQQ,0) E,
nvl(QQQ,0)-nvl(TRT,0) D,
nvl(DXQTY,0)-nvl(QQQ,0)+nvl(TRT,0) E,
' ',
nvl(Q4,0) J,
nvl(Q5,0) K,
nvl(Q2,0) G,
nvl(TV,0) N,
nvl(TRT,0) P,
nvl(QTN,0) Q,
nvl(Q1,0) F, 
nvl(QQQ,0)-nvl(Q4,0)-nvl(Q5,0)-nvl(Q2,0)-nvl(TV,0)-nvl(TRT,0)-nvl(QTN,0) V,
nvl(DXQTY,0)-(nvl(QQQ,0)-nvl(Q4,0)-nvl(Q5,0)-nvl(Q2,0)-nvl(TRT,0)+nvl(Q1,0)) W
--nvl(DXQTY,0)-nvl(QQQ,0)+nvl(Q4,0)+nvl(Q5,0)+nvl(TV,0)+nvl(QTN,0) w
from (select distinct KKK
from (select sku_id KKK
from sku
where client_id='DX'
and sku_id not like '*%'
union
select the_sku KKK
from dx_skufile)),(Select
sku_id SSS,
sum(qty_on_hand) QQQ
from inventory
where client_id='DX'
group by sku_id),(Select 
sku_id S5,
sum(qty_allocated) Q5
from inventory
where client_id='DX'
group by sku_id),(Select 
sku_id S1,
sum(qty_on_hand) Q1
from inventory
where client_id='DX'
and location_id='SUSPENSE'
group by sku_id),(Select 
sku_id STV,
sum(qty_on_hand) TV
from inventory
where client_id='DX'
and zone_1 in ('QVC','IDEAL')
and (condition_id<>'HOLD' or condition_id is null)
group by sku_id),(Select 
sku_id STR,
sum(qty_on_hand) TRT
from inventory
where client_id='DX'
and location_id in  ('DXBRET','DXBREJ')
and (condition_id<>'HOLD' or condition_id is null)
group by sku_id),(Select 
sku_id SQT,
sum(qty_on_hand) QTN
from inventory
where client_id='DX'
and location_id in  ('DXBREJ1')
and (condition_id<>'HOLD' or condition_id is null)
group by sku_id),(Select
ol.sku_id S4,
sum(nvl(ol.qty_picked,0)-nvl(ol.qty_shipped,0)) Q4
from order_line ol,order_header oh
where ol.client_id='DX'
and ol.order_id=oh.order_id
and oh.client_id='DX'
and oh.status not in ('Cancelled','Shipped')
group by ol.sku_id),(Select 
sku_id S2,
sum(qty_on_hand) Q2
from inventory
where client_id='DX'
and condition_id='HOLD'
group by sku_id),(Select 
sku_id S3,
sum(qty_on_hand) Q3
from inventory
where client_id='DX'
and condition_id='RETURN'
group by sku_id),(select 
THE_SKU,
sum(the_qty) DXQTY 
from (select
the_warehouse,
the_sku,
max(the_qty) the_qty
from dx_skufile
group by
the_warehouse,
the_sku)
group by the_sku),(select
sku.sku_id skusku,
product_group,
description
from sku
where client_id='DX')
where KKK=the_sku (+)
and KKK=SSS (+)
and KKK=S5 (+)
and KKK=S1 (+)
and KKK=STV (+)
and KKK=STR (+)
and KKK=SQT (+)
and KKK=S4 (+)
and KKK=S2 (+)
and KKK=S3 (+)
and KKK=skusku (+)
and (QQQ<>0 or DXQTY<>0))
order by
A
/


 
--spool off
--set termout on
