SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 80
set trimspool on

--SET TERMOUT OFF
SPOOL RSRshipments.csv

select 'Order Reference,Customer Name,SKU,Description,Qty Shipped' from DUAL;

select
it.reference_id||','||
oh.name||','||
it.sku_id||','||
sku.description||','||
it.update_qty
from inventory_transaction it,sku,order_header oh
where it.client_id='ECHO'
and it.code='Shipment'
and trunc(it.Dstamp)=trunc(sysdate)
and it.sku_id=sku.sku_id
and sku.client_id='ECHO'
and it.reference_id=oh.order_id
and oh.client_id='ECHO'
and oh.order_type='RSR'
order by 1
/


spool off

