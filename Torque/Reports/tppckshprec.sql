SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 300
SET NEWPAGE 1
SET LINESIZE 900
SET TRIMSPOOL ON
 

column A heading 'Product Group' format A16
column A1 heading 'Order Type' format A16
column B heading 'Orders Picked' format 99999
column C heading 'Units Picked' format 99999
column D heading 'Orders Shipped' format 99999
column E heading 'Units Shipped' format 99999
column F heading 'Units Receipted' format 99999
   
with T1 as
(
select distinct
case 
when sku.product_group = 'BATHTIME' then 'TIPPITOES'
when sku.product_group = 'HIGHCHAIRS' then 'TIPPITOES'
when sku.product_group = 'OUT AND AB' then 'TIPPITOES'
when sku.product_group = 'STROLLERS' then 'TIPPITOES'
when sku.product_group = 'TOILET TRA' then 'TIPPITOES'
when sku.product_group = 'TIPPITOES' then 'TIPPITOES'
when sku.product_group  = 'TOC' then 'CLSHOES'
when sku.product_group  is null then '(No Group)'
else sku.product_group||''
end PG
from sku 
where sku.client_id = 'TP'
),
T2 as (
  select 
    case 
    when sku.product_group = 'BATHTIME' then 'TIPPITOES'
    when sku.product_group = 'HIGHCHAIRS' then 'TIPPITOES'
    when sku.product_group = 'OUT AND AB' then 'TIPPITOES'
    when sku.product_group = 'STROLLERS' then 'TIPPITOES'
    when sku.product_group = 'TOILET TRA' then 'TIPPITOES'
    when sku.product_group = 'TIPPITOES' then 'TIPPITOES'
    when sku.product_group  = 'TOC' then 'CLSHOES'
    when sku.product_group  is null then '(No Group)'
    else sku.product_group||''
    end PG,
  oh.order_type,
  count(it.reference_id) as orders_p, sum(it.update_qty) as units_p
  from inventory_transaction it inner join sku on it.sku_id = sku.sku_id and it.client_id = sku.client_id inner join order_header oh on it.reference_id = oh.order_id and it.client_id = oh.client_id
  where it.client_id = 'TP'
  and it.dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
  and it.code in ('Pick','UnPick')
  and nvl(it.from_loc_id,'XXXXXX') <> 'CONTAINER'
  group by 
    case 
    when sku.product_group = 'BATHTIME' then 'TIPPITOES'
    when sku.product_group = 'HIGHCHAIRS' then 'TIPPITOES'
    when sku.product_group = 'OUT AND AB' then 'TIPPITOES'
    when sku.product_group = 'STROLLERS' then 'TIPPITOES'
    when sku.product_group = 'TOILET TRA' then 'TIPPITOES'
    when sku.product_group = 'TIPPITOES' then 'TIPPITOES'
    when sku.product_group  = 'TOC' then 'CLSHOES'
    when sku.product_group  is null then '(No Group)'
    else sku.product_group||''
end, order_type
),
T3 as (
  select 
    case 
    when sku.product_group = 'BATHTIME' then 'TIPPITOES'
    when sku.product_group = 'HIGHCHAIRS' then 'TIPPITOES'
    when sku.product_group = 'OUT AND AB' then 'TIPPITOES'
    when sku.product_group = 'STROLLERS' then 'TIPPITOES'
    when sku.product_group = 'TOILET TRA' then 'TIPPITOES'
    when sku.product_group = 'TIPPITOES' then 'TIPPITOES'
    when sku.product_group  = 'TOC' then 'CLSHOES'
    when sku.product_group  is null then '(No Group)'
    else sku.product_group||''
    end PG, oh.order_type,
  count(it.reference_id) as orders_s, sum(it.update_qty) as units_s
  from inventory_transaction it inner join sku on it.sku_id = sku.sku_id and it.client_id = sku.client_id inner join order_header oh on it.reference_id = oh.order_id and it.client_id = oh.client_id  
  where it.client_id = 'TP'
  and it.dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
  and it.code in ('Shipment','UnShipment')
  group by 
    case 
    when sku.product_group = 'BATHTIME' then 'TIPPITOES'
    when sku.product_group = 'HIGHCHAIRS' then 'TIPPITOES'
    when sku.product_group = 'OUT AND AB' then 'TIPPITOES'
    when sku.product_group = 'STROLLERS' then 'TIPPITOES'
    when sku.product_group = 'TOILET TRA' then 'TIPPITOES'
    when sku.product_group = 'TIPPITOES' then 'TIPPITOES'
    when sku.product_group  = 'TOC' then 'CLSHOES'
    when sku.product_group  is null then '(No Group)'
    else sku.product_group||''
end, oh.order_type
),
T4 as (
  select 
    case 
    when sku.product_group = 'BATHTIME' then 'TIPPITOES'
    when sku.product_group = 'HIGHCHAIRS' then 'TIPPITOES'
    when sku.product_group = 'OUT AND AB' then 'TIPPITOES'
    when sku.product_group = 'STROLLERS' then 'TIPPITOES'
    when sku.product_group = 'TOILET TRA' then 'TIPPITOES'
    when sku.product_group = 'TIPPITOES' then 'TIPPITOES'
    when sku.product_group  = 'TOC' then 'CLSHOES'
    when sku.product_group  is null then '(No Group)'
    else sku.product_group||''
    end PG,
  sum(it.update_qty) as units_rec
  from inventory_transaction it inner join sku on it.sku_id = sku.sku_id and it.client_id = sku.client_id 
  where it.client_id = 'TP'
  and it.dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
  and it.code in ('Receipt')
  group by 
    case 
    when sku.product_group = 'BATHTIME' then 'TIPPITOES'
    when sku.product_group = 'HIGHCHAIRS' then 'TIPPITOES'
    when sku.product_group = 'OUT AND AB' then 'TIPPITOES'
    when sku.product_group = 'STROLLERS' then 'TIPPITOES'
    when sku.product_group = 'TOILET TRA' then 'TIPPITOES'
    when sku.product_group = 'TIPPITOES' then 'TIPPITOES'
    when sku.product_group  = 'TOC' then 'CLSHOES'
    when sku.product_group  is null then '(No Group)'
    else sku.product_group||''
end
)
select
T1.PG A, 
T2.order_type A1, 
T2.orders_p B, 
T2.units_p C, 
T3.orders_s D, 
T3.units_s E, 
T4.units_rec F
from T1 left join T2 on T1.PG = T2.PG left join T3 on T1.PG = T3.PG and T2.order_type = T3.order_type left join T4 on T4.PG = T1.PG
where T2.orders_p is not null
or T2.units_p is not null
or T3.orders_s is not null
or T3.units_s is not null
or T4.units_rec is not null
order by 1,2
/