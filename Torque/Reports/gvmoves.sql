SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON



COLUMN NEWSEQ NEW_VALUE SEQNO
COLUMN FOLD NEW_VALUE FOLDER

update inventory_transaction
set uploaded='M'
where rowid in (select
it.rowid
from inventory_transaction it,location l1,location l2
where it.client_id = 'GV'
and it.code='Relocate' 
and it.uploaded = 'N'
--and it.dstamp > sysdate-1
and it.from_loc_id=l1.location_id
and it.to_loc_id=l2.location_id
and l1.zone_1 in ('INTERIM','GIVE','OUTLET','FAULTY','RETURN')
and l2.zone_1 in ('INTERIM','GIVE','OUTLET','FAULTY')
and DECODE(l1.zone_1,'INTERIM',it.from_loc_id,l1.zone_1) <> DECODE(l2.zone_1,'INTERIM',it.to_loc_id,l2.zone_1))
/


select
DECODE(count(*),0,'TEMP/','LIVE/') FOLD
from inventory_transaction it
where it.client_id = 'GV'
and it.code='Relocate'
and it.uploaded = 'M'
/



select 'S://redprairie/apps/home/dcsdba/reports/'||'&FOLDER'||'GIVEM'||substr(to_char(10000000+next_seq),2,7)||'.csv' NEWSEQ from sequence_file
--select '&FOLDER'||'GIVEM'||substr(to_char(10000000+next_seq),2,7)||'.csv' NEWSEQ from sequence_file
where control_id=7
/


set termout off
spool &SEQNO


select 'ITL,E,Movement'||','
||decode(sign(A1), 1, '+', -1, '-', '+')||','
--||A1||','
||''||','
||decode(sign(A2), 1, '+', -1, '-', '+')||','
||A2||','
||AA||','
||'GV'||','
||SS||','
||DECODE(substr(FF,1,3),'AQL','AQL',FF)||','
||DECODE(substr(TT,1,3),'AQL','AQL',TT)||','
||''||','
||''||','
||RR||','
||LI||','
||'GOOD'||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||'GIVE'||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||SUP||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||U8||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||',,'
from (select
to_char(it.dstamp, 'YYYYMMDDHH24MISS') AA,
it.sku_id SS,
it.line_id LI,
it.reference_id RR,
it.supplier_id SUP,
it.user_def_type_8 U8,
sum(it.original_qty) A1,
sum(it.update_qty) A2,
DECODE(l1.zone_1,'INTERIM',it.from_loc_id,l1.zone_1) FF,
DECODE(l2.zone_1,'INTERIM',it.to_loc_id,l2.zone_1) TT
from inventory_transaction it,location l1,location l2
where it.client_id = 'GV'
and it.code='Relocate'
and it.uploaded = 'M'
and it.from_loc_id=l1.location_id
and it.to_loc_id=l2.location_id
and l1.zone_1 in ('INTERIM','GIVE','OUTLET','FAULTY','RETURN')
and l2.zone_1 in ('INTERIM','GIVE','OUTLET','FAULTY')
and DECODE(l1.zone_1,'INTERIM',it.from_loc_id,l1.zone_1) <> DECODE(l2.zone_1,'INTERIM',it.to_loc_id,l2.zone_1)
and '&FOLDER' like 'LIVE%'
group by
to_char(it.dstamp, 'YYYYMMDDHH24MISS'),
it.sku_id,
it.line_id,
it.reference_id,
it.supplier_id,
it.user_def_type_8,
DECODE(l1.zone_1,'INTERIM',it.from_loc_id,l1.zone_1),
DECODE(l2.zone_1,'INTERIM',it.to_loc_id,l2.zone_1))
order by
AA
/



spool off
set termout on
set feedback on



update inventory_transaction
set uploaded='Y'
where client_id = 'GV'
and code='Relocate'
and uploaded = 'M'
and '&FOLDER' like 'LIVE%'
/



Update sequence_file
set next_seq=next_seq+1
where control_id=7 and '&FOLDER' like 'LIVE%'
/

commit;


