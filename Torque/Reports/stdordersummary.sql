SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240
SET TRIMSPOOL ON

--spool ordersummary2.csv

select 'Order Summary Report - for client ID &&2 from &&3 to &&4 - as at  ' 
    || to_char(sysdate,'dd/mm/yy  hh24:mi:ss') from dual;

select 'Order Id,Customer Id,Order Type,Qty Ordered,Qty Allocated,Qty Shipped,Shipped Date' from dual;

select
B,
C,
D,
Q1,
Q1-Q2,
Q3,
E from (select
oh.order_id B,
oh.customer_id C,
oh.order_type D,
sum(ol.qty_ordered) Q1,
nvl(QQ,0) Q2,
sum(ol.qty_shipped) Q3,
trunc(oh.shipped_date) E
from order_header oh,order_line ol,(select
task_id TT,
sum(qty_ordered-qty_tasked) QQ
from generation_shortage 
where client_id='&&2'
group by task_id)
where oh.client_id='&&2' 
and oh.creation_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and oh.order_type like '&&5%'
and ol.client_id=oh.client_id
and ol.order_id=oh.order_id
and oh.order_id=TT (+)
group by 
oh.order_id,
oh.customer_id,
oh.order_type,
nvl(QQ,0),
oh.shipped_date)
order by 1
/

--spool off
