SET FEEDBACK OFF                 
set pagesize 68
set linesize 160
set verify off

clear columns
clear breaks
clear computes

column C heading 'Client ID' format a10
column D heading 'Date' format a16
column A heading 'Type' format a20
column B heading 'Received' format 99999999
column M format a4 noprint

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'Total Units Received between &&2 and &&3 - for client DAXBOURNE as at ' report_date skip 2

break on report

compute sum label 'Totals' of B on report


select
trunc(it.Dstamp) D,
it.reference_id A,
sum(it.update_qty) B
from inventory_transaction it
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='DX'
and it.station_id not like 'Auto%'
and it.code like 'Receipt%'
group by
trunc(it.Dstamp),
it.reference_id 
order by D,A
/

set pagesize 0

select
'Number of Orders = '||count(distinct it.reference_id) B
from inventory_transaction it
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='DX'
and it.station_id not like 'Auto%'
and it.code like 'Receipt%'
/
