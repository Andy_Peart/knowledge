SET FEEDBACK OFF                 
set pagesize 68
set linesize 132
set verify off
set wrap off
set newpage 0

clear columns
clear breaks
clear computes


column C heading 'Shipments' format 999999
column D heading 'Units' format 999999
column F heading 'Date' format a12

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'Shipments by Date from &&2 to &&3 - for client ID &&4 as at ' report_date skip 2


select
to_char(Dstamp, 'DD/MM/YY') F,
count(*) C,
sum(update_qty) D
from inventory_transaction
where Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and client_id='&&4'
and update_qty<>0
and code='Shipment'
group by
to_char(Dstamp, 'DD/MM/YY')
order by
to_char(Dstamp, 'DD/MM/YY')
/

