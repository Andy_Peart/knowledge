SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 120
SET TRIMSPOOL ON
 
column A format A18
column B format 999999



--spool wol.csv

--select 'SKU,Stock' from DUAL;

select
sku.sku_id||','||nvl(BB,0)
from sku,(select
sku_id SS,
nvl(i.qty_on_hand,0)-nvl(i.qty_allocated,0) BB
from inventory i
where i.client_id='WOL')
where sku.client_id ='WOL'
and sku.sku_id=SS(+)
order by 1
/

--spool off
