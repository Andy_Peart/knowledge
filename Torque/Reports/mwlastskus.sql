SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Sku,Description,ZoneLocation,Qty on Hand' from dual
union all
select "Sku" || ',' || "Desc" || ',' || "Zone" || ',' || "Loc" || ',' || "Qty" from
(
with t1 as (
select count(distinct i.location_id)    as "Count"
, sum(i.qty_on_hand)                    as "Qty"
, i.sku_id                              as "Sku"
, s.description                         as "Desc"
, i.client_id                           as "Client"
from inventory i
inner join location l on l.site_id = i.site_id and l.location_id = i.location_id
inner join sku s on s.sku_id = i.sku_id and s.client_id = i.client_id
where i.client_id = 'MOUNTAIN'
and l.loc_type in ('Tag-FIFO','Tag-LIFO','Receive Dock')
group by i.sku_id
, s.description
, i.client_id
having count(distinct i.location_id) = 1
and sum(i.qty_on_hand) <= 20
), t2 as (
select i.sku_id as "Sku"
, l.work_zone   as "Zone"
, i.client_id   as "Client"
, i.location_id as "Loc"  
from inventory i
inner join location l on l.site_id = i.site_id and l.location_id = i.location_id
where i.client_id = 'MOUNTAIN'
and l.loc_type in ('Tag-FIFO','Tag-LIFO','Receive Dock')
)
select '''' || t1."Sku" || '''' as "Sku"
, t1."Desc"                     
, t2."Zone"
, t2."Loc"
, t1."Qty"
from t1
inner join t2 on t2."Client" = t1."Client" and t2."Sku" = t1."Sku"
group by t1."Sku"
, t1."Desc"
, t2."Loc"
, t2."Zone"
, t1."Qty"
order by 4,5
)
/
--spool off