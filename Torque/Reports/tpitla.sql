SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON


select 'ITL,E,Adjustment'||','
--||decode(sign(A1), 1, '+', -1, '-', '+')||','
--||A1||','
||''||','
||''||','
||decode(sign(A2), 1, '+', -1, '-', '+')||','
||A2||','
||AA||'000000,'
||'TP'||','
||SS||','
||''||','
||''||','
||'Unspecified'||','
||''||','
||RR||','
||''||','
--||CI||','
||''||','
||''||','
--||RI||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||SI||','
||''||','
--||PI||','
||''||','
||''||','
||OI||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||SUP||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
from (select
to_char(it.dstamp, 'YYYYMMDD') AA,
it.client_id CT,
it.sku_id SS,
it.reference_id RR,
--it.condition_id CI,
--it.reason_id RI,
it.site_id SI,
--it.pallet_id PI,
it.owner_id OI,
it.supplier_id SUP,
sum(it.original_qty) A1,
sum(it.update_qty) A2
from inventory_transaction it
where it.client_id = 'TP'
and it.code='Adjustment' 
and it.reason_id<>'RECRV'
and nvl(it.tag_id,'XX') not like 'RET%'
--and trunc(it.Dstamp)>trunc(sysdate)-1
and (it.uploaded = 'N'
or it.uploaded is null)
group by
to_char(it.dstamp, 'YYYYMMDD'),
it.client_id,
it.sku_id,
it.reference_id,
--it.condition_id,
--it.reason_id,
it.site_id,
--it.pallet_id,
it.owner_id,
it.supplier_id)
where A2<>0
order by
RR,SS
/

--SET FEEDBACK ON

update inventory_transaction
set uploaded='Y'
where client_id = 'TP'
and code='Adjustment' 
and reason_id<>'RECRV'
and nvl(tag_id,'XX') not like 'RET%'
and (uploaded = 'N'
or uploaded is null)
/

commit;
--rollback;

