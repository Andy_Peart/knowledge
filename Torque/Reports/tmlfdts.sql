SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','
SET WRAP OFF


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON

column F1 heading 'Order ID' format A10
column F2 heading 'Status' format A15
column F3 heading 'Ship dock' format A10
column F4 heading 'Consignment' format A18
column F5 heading 'Order Type' format A15
column F6 heading 'Order Date' format A15
column F7 heading 'Order Time' format A15
column F8 heading 'Creation Date' format A15
column F9 heading 'Creation Time' format A15
column F10 heading 'Purchase Order' format A20
column F11 heading 'Store No' format A10


--set termout off
--spool bk_comp.csv



select 'Order ID, Status, Ship dock, Consignment, Order Type, Order Date, Order Time, Creation Date, Creation Time, Purchase Order, Store No' from DUAL
union all
select
F1 ||','|| 
F2 ||','|| 
F3 ||','||
F4 ||','||
F5 ||','||
F6 ||','||
F7 ||','||
F8 ||','||
F9 ||','||
F10 ||','||
F11
from (
select 
oh.order_id as F1, 
oh.status as F2, 
oh.ship_dock as F3, 
oh.consignment as F4,
oh.order_type as F5, 
to_char(oh.order_date,'DD-MM-YYYY') as F6, 
to_char(oh.order_date,'HH24:MM:SS') as F7, 
to_char(oh.creation_date, 'DD-MM-YYYY') as F8, 
to_char(oh.creation_date, 'HH24:MM:SS') as F9,
oh.purchase_order as F10, oh.user_def_type_5 AS F11
from order_header oh inner join address ad on oh.user_def_type_5 = ad.address_id
where oh.order_id like 'H%'
and oh.client_id = 'T'
and ad.client_id = 'T'
and ad.user_def_chk_1 = 'Y'
and oh.order_type = 'STOREDEL'
and oh.creation_date between to_date('&&2', 'DD/MM/YY') and to_date('&&2', 'DD/MM/YY')+1
order by oh.user_def_type_5)
/
