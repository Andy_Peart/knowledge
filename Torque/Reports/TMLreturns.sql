SET FEEDBACK OFF                
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 400
SET TRIMSPOOL ON

column A heading 'Store No.' format a12
column B heading 'Name' format a30
column C heading 'PA ID' format a12
column D heading 'Status' format a12
column E heading 'Date' format a12
column F heading 'SKU' format a18
column FF heading 'CIMS' format a28
column G heading 'Qty Due' format 999999
column H heading 'Qty Rec' format 999999
column J heading 'Variance' format 999999
column K heading 'Date2' format a12

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

--break on report

--compute sum label 'Total' of C D  on report

--spool tmlreturns.csv

select'Store No,Store Name,Transfer ID,Pre-Advice Status,Pre-Advice Creation Date,SKU,CIMS Code,Qty Due,Qty Recd,Variance,Putaway Date' from DUAL;

select
ph.supplier_id A,
ph.name B,
ph.pre_advice_id C,
ph.status D,
to_char(ph.creation_date,'DD/MM/YY') E,
pl.sku_id||'''' F,
sku.user_def_type_2||'-'||sku.user_def_type_1||'-'||sku.user_def_type_4||'-'||sku.sku_size FF,
pl.qty_due G,
nvl(HHH,0) H,
nvl(HHH,0)-pl.qty_due J,
KKK K
from pre_advice_header ph,pre_advice_line pl,sku,(select
reference_id CCC,
sku_id FFF,
max(to_char(dstamp,'DD/MM/YY')) KKK,
sum(update_qty) HHH
from inventory_transaction
where client_id = 'TR'
and Dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1 
--and trunc(dstamp)=to_date('&&2','DD-MON-YYYY')
and code = 'Adjustment'
and (reference_id like 'T%' or reference_id like 'H%')
group by
--reason_id,
reference_id,
sku_id)
--to_char(dstamp,'DD/MM/YY'))
where ph.client_id = 'TR'
and ph.pre_advice_id=pl.pre_advice_id
--and ph.pre_advice_id='T248/10090'
and pl.client_id = 'TR'
and pl.pre_advice_id like 'T%'
and pl.pre_advice_id=CCC (+)
and pl.sku_id=FFF (+)
and sku.client_id = 'TR'
and sku.sku_id=pl.sku_id
and ph.pre_advice_id in (select
distinct reference_id
from inventory_transaction
where client_id = 'TR'
and (reference_id like 'T%' or reference_id like 'H%')
and Dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1 
--and trunc(dstamp)=to_date('&&2','DD-MON-YYYY')
and code = 'Adjustment')
order by
A,C,F
/

--spool off
