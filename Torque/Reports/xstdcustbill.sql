SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON

select 'Client ID,From Site ID,Creation Date,Creation Time,Consinment code,Reference,Order Status,Customer,Line ID,SKU,Description,Qty Ordered,Qty Tasked,Qty Picked,Qty Shipped,Qty Returned,Return Notes,Returned Date,Shipped Date,Line Value,User Defined Type 1,User Defined Type 2,User Defined Type 3,User Defined Type 4,User Defined Type 5,User Defined Type 6,User Defined Type 7,User Defined Type 8' from dual
union all
select client_id || ',' || from_site_id || ',' || date_created || ',' || time_created || ',' || consignment || ',' || 
reference_id || ',' || status || ',' || customer_id || ',' || line_id || ',' || 
sku_id || ',' || descrip || ',' || DD || ',' || CC || ',' || BB || ',' || AA || ',' || qty_returned || ',' || notes || ',' || RETDATE  || ',' || date_shipped || 
line_value || ',' || 
user_def_type_1 || ',' || user_def_type_2 || ',' || user_def_type_3 || ',' || user_def_type_4 || ',' || 
user_def_type_5 || ',' || user_def_type_6 || ',' || user_def_type_7 || ',' || user_def_type_8
from (
select oh.client_id
, oh.from_site_id
, to_char(oh.creation_date,'DD-MON-YYYY') as date_created
, to_char(oh.creation_date,'HH24:MM:SS') as time_created
, oh.consignment
, it.reference_id
, oh.status
, oh.customer_id
, ol.line_id
, ol.sku_id
, libsku.getuserlangskudesc(oh.client_id,ol.sku_id) descrip
, sum(nvl(ol.qty_ordered,'0')) AS "DD"
, sum(nvl(ol.qty_tasked,'0')) AS "CC"
, sum(nvl(ol.qty_picked,'0')) AS "BB"
, sum(nvl(ol.qty_shipped,'0')) as "AA"
, sum(nvl(ol.qty_returned,0)) qty_returned
, x.notes
, to_char(x.dstamp,'DD-MON-YYYY') as RETDATE
, to_char(oh.shipped_date,'DD-MON-YYYY') as date_shipped
, ol.line_value
, oh.user_def_type_1
, oh.user_def_type_2
, oh.user_def_type_3
, oh.user_def_type_4
, oh.user_def_type_5
, oh.user_def_type_6
, oh.user_def_type_7
, oh.user_def_type_8
from inventory_transaction it
left join order_header oh on it.reference_id = oh.order_id and it.client_id = oh.client_id
inner join order_line ol on oh.order_id = ol.order_id and oh.client_id = ol.client_id and it.sku_id = ol.sku_id and it.line_id = ol.line_id
inner join location loc on loc.location_id = it.from_loc_id and loc.site_id = oh.from_site_id
left join
(
select xitl.reference_id, xitl.sku_id, xitl.line_id, xitl.reason_id, r.notes, xitl.dstamp
from inventory_transaction xitl
left join return_reason r on xitl.reason_id = r.reason_id
where xitl.dstamp > trunc(sysdate)-90
and xitl.client_id = '&&2'
and xitl.code = 'Return'
) x on it.REFERENCE_ID = x.reference_id and it.SKU_ID = x.sku_id and it.LINE_ID = x.line_id
where oh.client_id = '&&2'
and it.code = 'Pick'
and it.from_loc_id <> 'CONTAINER'
and oh.creation_date between to_date('&&3') AND to_date('&&4')+1
GROUP BY oh.client_id
, oh.from_site_id
, to_char(oh.creation_date,'DD-MON-YYYY')
, to_char(oh.creation_date,'HH24:MM:SS')
, oh.consignment
, it.reference_id
, oh.status
, oh.customer_id
, ol.line_id
, ol.sku_id
, libsku.getuserlangskudesc(oh.client_id,ol.sku_id)
, x.notes
, to_char(x.dstamp,'DD-MON-YYYY')
, to_char(oh.shipped_date,'DD-MON-YYYY')
, ol.line_value
, oh.user_def_type_1
, oh.user_def_type_2
, oh.user_def_type_3
, oh.user_def_type_4
, oh.user_def_type_5
, oh.user_def_type_6
, oh.user_def_type_7
, oh.user_def_type_8
order by 3,4
)
/