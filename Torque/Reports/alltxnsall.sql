SET FEEDBACK OFF                 
set verify off
set wrap off
set colsep ','

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 900
set trimspool on


--spool alltxnsall.csv


select 'Run Date / Time : '||to_char(sysdate,'DD/MM/YYYY HH24:MI:SS') from DUAL
union all
select 'Detail for Week Commencing '||trunc(sysdate-77-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)) from DUAL
union all
Select',,,,First Week,,,,,Second Week,,,,,Third Week,,,,,Fourth Week,,,,,Fifth Week,,,,,Sixth Week' from DUAL
union all
Select 'Site,Client,Orders Received,Units Ordered,Units Allocated,Shortfall,Units Shipped,% Fulfilled,Orders Shipped,Units Allocated,Shortfall,Units Shipped,% Fulfilled,Orders Shipped,Units Allocated,Shortfall,Units Shipped,% Fulfilled,Orders Shipped,Units Allocated,Shortfall,Units Shipped,% Fulfilled,Orders Shipped,Units Allocated,Shortfall,Units Shipped,% Fulfilled,Orders Shipped,Units Allocated,Shortfall,Units Shipped,% Fulfilled,Orders Shipped,Still NOT Shipped,Oldest Order NOT Shipped' from DUAL
union all
select * from (select
upper(BB)||','||
upper(AA)||','||
nvl(D1,0)||','||
nvl(D2,0)||','||
--******************* FIRST WEEK ****************************
--'AVL'||','||
--Units Allocated below
nvl(L3,0)||','||
nvl(D2-nvl(L3,0),0)||','||
--Units Shipped Below
nvl(C3,0)||','||
ltrim(to_char((C3/D2)*100,'999.00'))||','||
--Orders Shipped Below
nvl(D3,0)||','||
--**************** SECOND WEEK *******************************
--'AVL'||','||
--Units Allocated below
nvl(L31,0)||','||
nvl(D2-nvl(L31,0),0)||','||
--Units Shipped Below
nvl(C31,0)||','||
ltrim(to_char((C31/D2)*100,'999.00'))||','||
--Orders Shipped Below
nvl(D31,0)||','||
--***************** THIRD WEEK******************************
--'AVL'||','||
--Units Allocated below
nvl(L32,0)||','||
nvl(D2-nvl(L32,0),0)||','||
--Units Shipped Below
nvl(C32,0)||','||
ltrim(to_char((C32/D2)*100,'999.00'))||','||
--Orders Shipped Below
nvl(D32,0)||','||
--***************** FOURTH WEEK******************************
--'AVL'||','||
--Units Allocated below
nvl(L33,0)||','||
nvl(D2-nvl(L33,0),0)||','||
--Units Shipped Below
nvl(C33,0)||','||
ltrim(to_char((C33/D2)*100,'999.00'))||','||
--Orders Shipped Below
nvl(D33,0)||','||
--***************** FIFTH WEEK ******************************
--'AVL'||','||
--Units Allocated below
nvl(L34,0)||','||
nvl(D2-nvl(L34,0),0)||','||
--Units Shipped Below
nvl(C34,0)||','||
ltrim(to_char((C34/D2)*100,'999.00'))||','||
--Orders Shipped Below
nvl(D34,0)||','||
--****************** SIXTH WEEK *****************************
--'AVL'||','||
--Units Allocated below
nvl(L35,0)||','||
nvl(D2-nvl(L35,0),0)||','||
--Units Shipped Below
nvl(C35,0)||','||
ltrim(to_char((C35/D2)*100,'999.00'))||','||
--Orders Shipped Below
nvl(D35,0)||','||
--***************** FINALE  ******************************
nvl(D36,0)||','||
E10
--***************** OLDEST ORDER  ******************************
from (select
nvl(cl.name,cl.client_id) AA,
ST BB,
min(trunc(creation_date)) E10
from order_header oh,client cl,(select
distinct
client_id CL,
site_id ST
from inventory)
where status not in ('Cancelled','Shipped')
and oh.client_id=cl.client_id
and oh.client_id=CL
group by
nvl(cl.name,cl.client_id),ST),
--****************ORDERS RECEIVED AND UNITS *******************************
(select
nvl(cl.name,cl.client_id) A2,
oh.from_site_id B2,
count(distinct oh.order_id) D1,
sum(DECODE(substr(oh.order_id,1,3),'MOR',(DECODE(substr(ol.user_def_note_1,1,8),'Original',substr(ol.user_def_note_1,31,4),ol.qty_ordered)),ol.qty_ordered)) D2
from order_header oh,client cl,order_line ol
where oh.status not in ('Cancelled')
and oh.creation_date between trunc(sysdate)-77-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-70-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
and oh.order_id=ol.order_id
and ol.client_id=oh.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--***********************SHIPMENTS AND ALLOCATIONS ************************ 1
(select
nvl(cl.name,cl.client_id) A20,
oh.from_site_id B20,
sum(nvl(QQ,0)) C3,
sum(nvl(QA,0)) L3
from order_header oh,client cl,order_line ol,(select
reference_id REF,
sku_id SKU,
line_id LIN,
sum(DECODE(code,'Shipment',nvl(update_qty,0),'Unshipment',nvl(update_qty,0)*-1,0)) QQ,
sum(DECODE(code,'Allocate',nvl(update_qty,0),'Deallocate',nvl(update_qty,0)*-1,0)) QA
from inventory_transaction
where code in ('Shipment','Unshipment','Allocate','Deallocate')
and Dstamp between trunc(sysdate)-77-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-70-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
group by
reference_id,
sku_id,
line_id)
where oh.status not in ('Cancelled')
and oh.creation_date between trunc(sysdate)-77-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-70-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
and oh.order_id=ol.order_id
and ol.client_id=oh.client_id
and REF=oh.order_id
and SKU=ol.sku_id
and LIN=ol.line_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 2
(select
nvl(cl.name,cl.client_id) A21,
oh.from_site_id B21,
sum(nvl(QQ,0)) C31,
sum(nvl(QA,0)) L31
from order_header oh,client cl,order_line ol,(select
reference_id REF,
sku_id SKU,
line_id LIN,
sum(DECODE(code,'Shipment',nvl(update_qty,0),'Unshipment',nvl(update_qty,0)*-1,0)) QQ,
sum(DECODE(code,'Allocate',nvl(update_qty,0),'Deallocate',nvl(update_qty,0)*-1,0)) QA
from inventory_transaction
where code in ('Shipment','Unshipment','Allocate','Deallocate')
and Dstamp between trunc(sysdate)-77-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-63-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
group by
reference_id,
sku_id,
line_id)
where oh.status not in ('Cancelled')
and oh.creation_date between trunc(sysdate)-77-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-70-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
and oh.order_id=ol.order_id
and ol.client_id=oh.client_id
and REF=oh.order_id
and SKU=ol.sku_id
and LIN=ol.line_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 3
(select
nvl(cl.name,cl.client_id) A22,
oh.from_site_id B22,
sum(nvl(QQ,0)) C32,
sum(nvl(QA,0)) L32
from order_header oh,client cl,order_line ol,(select
reference_id REF,
sku_id SKU,
line_id LIN,
sum(DECODE(code,'Shipment',nvl(update_qty,0),'Unshipment',nvl(update_qty,0)*-1,0)) QQ,
sum(DECODE(code,'Allocate',nvl(update_qty,0),'Deallocate',nvl(update_qty,0)*-1,0)) QA
from inventory_transaction
where code in ('Shipment','Unshipment','Allocate','Deallocate')
and Dstamp between trunc(sysdate)-77-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-56-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
group by
reference_id,
sku_id,
line_id)
where oh.status not in ('Cancelled')
and oh.creation_date between trunc(sysdate)-77-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-70-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
and oh.order_id=ol.order_id
and ol.client_id=oh.client_id
and REF=oh.order_id
and SKU=ol.sku_id
and LIN=ol.line_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 4
(select
nvl(cl.name,cl.client_id) A23,
oh.from_site_id B23,
sum(nvl(QQ,0)) C33,
sum(nvl(QA,0)) L33
from order_header oh,client cl,order_line ol,(select
reference_id REF,
sku_id SKU,
line_id LIN,
sum(DECODE(code,'Shipment',nvl(update_qty,0),'Unshipment',nvl(update_qty,0)*-1,0)) QQ,
sum(DECODE(code,'Allocate',nvl(update_qty,0),'Deallocate',nvl(update_qty,0)*-1,0)) QA
from inventory_transaction
where code in ('Shipment','Unshipment','Allocate','Deallocate')
and Dstamp between trunc(sysdate)-77-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
group by
reference_id,
sku_id,
line_id)
where oh.status not in ('Cancelled')
and oh.creation_date between trunc(sysdate)-77-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-70-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
and oh.order_id=ol.order_id
and ol.client_id=oh.client_id
and REF=oh.order_id
and SKU=ol.sku_id
and LIN=ol.line_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 5
(select
nvl(cl.name,cl.client_id) A24,
oh.from_site_id B24,
sum(nvl(QQ,0)) C34,
sum(nvl(QA,0)) L34
from order_header oh,client cl,order_line ol,(select
reference_id REF,
sku_id SKU,
line_id LIN,
sum(DECODE(code,'Shipment',nvl(update_qty,0),'Unshipment',nvl(update_qty,0)*-1,0)) QQ,
sum(DECODE(code,'Allocate',nvl(update_qty,0),'Deallocate',nvl(update_qty,0)*-1,0)) QA
from inventory_transaction
where code in ('Shipment','Unshipment','Allocate','Deallocate')
and Dstamp between trunc(sysdate)-77-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
group by
reference_id,
sku_id,
line_id)
where oh.status not in ('Cancelled')
and oh.creation_date between trunc(sysdate)-77-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-70-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
and oh.order_id=ol.order_id
and ol.client_id=oh.client_id
and REF=oh.order_id
and SKU=ol.sku_id
and LIN=ol.line_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 6
(select
nvl(cl.name,cl.client_id) A25,
oh.from_site_id B25,
sum(nvl(QQ,0)) C35,
sum(nvl(QA,0)) L35
from order_header oh,client cl,order_line ol,(select
reference_id REF,
sku_id SKU,
line_id LIN,
sum(DECODE(code,'Shipment',nvl(update_qty,0),'Unshipment',nvl(update_qty,0)*-1,0)) QQ,
sum(DECODE(code,'Allocate',nvl(update_qty,0),'Deallocate',nvl(update_qty,0)*-1,0)) QA
from inventory_transaction
where code in ('Shipment','Unshipment','Allocate','Deallocate')
and Dstamp>trunc(sysdate)-77-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
--and trunc(sysdate)-35-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
group by
reference_id,
sku_id,
line_id)
where oh.status not in ('Cancelled')
and oh.creation_date between trunc(sysdate)-77-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-70-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
and oh.order_id=ol.order_id
and ol.client_id=oh.client_id
and REF=oh.order_id
and SKU=ol.sku_id
and LIN=ol.line_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--**************SHIPPED ORDERS********************************* 1
(select
nvl(cl.name,cl.client_id) A3,
oh.from_site_id B3,
count(distinct oh.order_id) D3
from order_header oh,client cl
where status='Shipped'
and oh.creation_date between trunc(sysdate)-77-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-70-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.shipped_date between trunc(sysdate)-77-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-70-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 2
(select
nvl(cl.name,cl.client_id) A31,
oh.from_site_id B31,
count(distinct oh.order_id) D31
from order_header oh,client cl
where status='Shipped'
and oh.creation_date between trunc(sysdate)-77-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-70-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.shipped_date between trunc(sysdate)-77-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-63-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 3
(select
nvl(cl.name,cl.client_id) A32,
oh.from_site_id B32,
count(distinct oh.order_id) D32
from order_header oh,client cl
where status='Shipped'
and oh.creation_date between trunc(sysdate)-77-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-70-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.shipped_date between trunc(sysdate)-77-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-56-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 4
(select
nvl(cl.name,cl.client_id) A33,
oh.from_site_id B33,
count(distinct oh.order_id) D33
from order_header oh,client cl
where status='Shipped'
and oh.creation_date between trunc(sysdate)-77-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-70-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.shipped_date between trunc(sysdate)-77-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 5
(select
nvl(cl.name,cl.client_id) A34,
oh.from_site_id B34,
count(distinct oh.order_id) D34
from order_header oh,client cl
where status='Shipped'
and oh.creation_date between trunc(sysdate)-77-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-70-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.shipped_date between trunc(sysdate)-77-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 6
(select
nvl(cl.name,cl.client_id) A35,
oh.from_site_id B35,
count(distinct oh.order_id) D35
from order_header oh,client cl
where status='Shipped'
and oh.creation_date between trunc(sysdate)-77-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-70-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.shipped_date>trunc(sysdate)-77-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
--and trunc(sysdate)-35-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--****************OPEN ORDERS*******************************
(select
nvl(cl.name,cl.client_id) A36,
oh.from_site_id B36,
count(distinct oh.order_id) D36
from order_header oh,client cl
where status not in ('Shipped','Cancelled')
and oh.creation_date between trunc(sysdate)-77-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-70-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id)
where AA=A2 (+)
and BB=B2 (+)
and AA=A20 (+)
and BB=B20 (+)
and AA=A21 (+)
and BB=B21 (+)
and AA=A22 (+)
and BB=B22 (+)
and AA=A23 (+)
and BB=B23 (+)
and AA=A24 (+)
and BB=B24 (+)
and AA=A25 (+)
and BB=B25 (+)
and AA=A3 (+)
and BB=B3 (+)
and AA=A31 (+)
and BB=B31 (+)
and AA=A32 (+)
and BB=B32 (+)
and AA=A33 (+)
and BB=B33 (+)
and AA=A34 (+)
and BB=B34 (+)
and AA=A35 (+)
and BB=B35 (+)
and AA=A36 (+)
and BB=B36 (+)
order by upper(BB),upper(AA))
union all
select 'Detail for Week Commencing '||trunc(sysdate-70-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)) from DUAL
union all
Select',,,,First Week,,,,,Second Week,,,,,Third Week,,,,,Fourth Week,,,,,Fifth Week,,,,,Sixth Week' from DUAL
union all
Select 'Site,Client,Orders Received,Units Ordered,Units Allocated,Shortfall,Units Shipped,% Fulfilled,Orders Shipped,Units Allocated,Shortfall,Units Shipped,% Fulfilled,Orders Shipped,Units Allocated,Shortfall,Units Shipped,% Fulfilled,Orders Shipped,Units Allocated,Shortfall,Units Shipped,% Fulfilled,Orders Shipped,Units Allocated,Shortfall,Units Shipped,% Fulfilled,Orders Shipped,Units Allocated,Shortfall,Units Shipped,% Fulfilled,Orders Shipped,Still NOT Shipped,Oldest Order NOT Shipped' from DUAL
union all
select * from (select
upper(BB)||','||
upper(AA)||','||
nvl(D1,0)||','||
nvl(D2,0)||','||
--******************* FIRST WEEK ****************************
--'AVL'||','||
--Units Allocated below
nvl(L3,0)||','||
nvl(D2-nvl(L3,0),0)||','||
--Units Shipped Below
nvl(C3,0)||','||
ltrim(to_char((C3/D2)*100,'999.00'))||','||
--Orders Shipped Below
nvl(D3,0)||','||
--**************** SECOND WEEK *******************************
--'AVL'||','||
--Units Allocated below
nvl(L31,0)||','||
nvl(D2-nvl(L31,0),0)||','||
--Units Shipped Below
nvl(C31,0)||','||
ltrim(to_char((C31/D2)*100,'999.00'))||','||
--Orders Shipped Below
nvl(D31,0)||','||
--***************** THIRD WEEK******************************
--'AVL'||','||
--Units Allocated below
nvl(L32,0)||','||
nvl(D2-nvl(L32,0),0)||','||
--Units Shipped Below
nvl(C32,0)||','||
ltrim(to_char((C32/D2)*100,'999.00'))||','||
--Orders Shipped Below
nvl(D32,0)||','||
--***************** FOURTH WEEK******************************
--'AVL'||','||
--Units Allocated below
nvl(L33,0)||','||
nvl(D2-nvl(L33,0),0)||','||
--Units Shipped Below
nvl(C33,0)||','||
ltrim(to_char((C33/D2)*100,'999.00'))||','||
--Orders Shipped Below
nvl(D33,0)||','||
--***************** FIFTH WEEK ******************************
--'AVL'||','||
--Units Allocated below
nvl(L34,0)||','||
nvl(D2-nvl(L34,0),0)||','||
--Units Shipped Below
nvl(C34,0)||','||
ltrim(to_char((C34/D2)*100,'999.00'))||','||
--Orders Shipped Below
nvl(D34,0)||','||
--****************** SIXTH WEEK *****************************
--'AVL'||','||
--Units Allocated below
nvl(L35,0)||','||
nvl(D2-nvl(L35,0),0)||','||
--Units Shipped Below
nvl(C35,0)||','||
ltrim(to_char((C35/D2)*100,'999.00'))||','||
--Orders Shipped Below
nvl(D35,0)||','||
--***************** FINALE  ******************************
nvl(D36,0)||','||
E10
--***************** OLDEST ORDER  ******************************
from (select
nvl(cl.name,cl.client_id) AA,
ST BB,
min(trunc(creation_date)) E10
from order_header oh,client cl,(select
distinct
client_id CL,
site_id ST
from inventory)
where status not in ('Cancelled','Shipped')
and oh.client_id=cl.client_id
and oh.client_id=CL
group by
nvl(cl.name,cl.client_id),ST),
--****************ORDERS RECEIVED AND UNITS *******************************
(select
nvl(cl.name,cl.client_id) A2,
oh.from_site_id B2,
count(distinct oh.order_id) D1,
sum(DECODE(substr(oh.order_id,1,3),'MOR',(DECODE(substr(ol.user_def_note_1,1,8),'Original',substr(ol.user_def_note_1,31,4),ol.qty_ordered)),ol.qty_ordered)) D2
from order_header oh,client cl,order_line ol
where oh.status not in ('Cancelled')
and oh.creation_date between trunc(sysdate)-70-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-63-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
and oh.order_id=ol.order_id
and ol.client_id=oh.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--***********************SHIPMENTS AND ALLOCATIONS ************************ 1
(select
nvl(cl.name,cl.client_id) A20,
oh.from_site_id B20,
sum(nvl(QQ,0)) C3,
sum(nvl(QA,0)) L3
from order_header oh,client cl,order_line ol,(select
reference_id REF,
sku_id SKU,
line_id LIN,
sum(DECODE(code,'Shipment',nvl(update_qty,0),'Unshipment',nvl(update_qty,0)*-1,0)) QQ,
sum(DECODE(code,'Allocate',nvl(update_qty,0),'Deallocate',nvl(update_qty,0)*-1,0)) QA
from inventory_transaction
where code in ('Shipment','Unshipment','Allocate','Deallocate')
and Dstamp between trunc(sysdate)-70-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-63-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
group by
reference_id,
sku_id,
line_id)
where oh.status not in ('Cancelled')
and oh.creation_date between trunc(sysdate)-70-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-63-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
and oh.order_id=ol.order_id
and ol.client_id=oh.client_id
and REF=oh.order_id
and SKU=ol.sku_id
and LIN=ol.line_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 2
(select
nvl(cl.name,cl.client_id) A21,
oh.from_site_id B21,
sum(nvl(QQ,0)) C31,
sum(nvl(QA,0)) L31
from order_header oh,client cl,order_line ol,(select
reference_id REF,
sku_id SKU,
line_id LIN,
sum(DECODE(code,'Shipment',nvl(update_qty,0),'Unshipment',nvl(update_qty,0)*-1,0)) QQ,
sum(DECODE(code,'Allocate',nvl(update_qty,0),'Deallocate',nvl(update_qty,0)*-1,0)) QA
from inventory_transaction
where code in ('Shipment','Unshipment','Allocate','Deallocate')
and Dstamp between trunc(sysdate)-70-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-56-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
group by
reference_id,
sku_id,
line_id)
where oh.status not in ('Cancelled')
and oh.creation_date between trunc(sysdate)-70-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-63-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
and oh.order_id=ol.order_id
and ol.client_id=oh.client_id
and REF=oh.order_id
and SKU=ol.sku_id
and LIN=ol.line_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 3
(select
nvl(cl.name,cl.client_id) A22,
oh.from_site_id B22,
sum(nvl(QQ,0)) C32,
sum(nvl(QA,0)) L32
from order_header oh,client cl,order_line ol,(select
reference_id REF,
sku_id SKU,
line_id LIN,
sum(DECODE(code,'Shipment',nvl(update_qty,0),'Unshipment',nvl(update_qty,0)*-1,0)) QQ,
sum(DECODE(code,'Allocate',nvl(update_qty,0),'Deallocate',nvl(update_qty,0)*-1,0)) QA
from inventory_transaction
where code in ('Shipment','Unshipment','Allocate','Deallocate')
and Dstamp between trunc(sysdate)-70-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
group by
reference_id,
sku_id,
line_id)
where oh.status not in ('Cancelled')
and oh.creation_date between trunc(sysdate)-70-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-63-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
and oh.order_id=ol.order_id
and ol.client_id=oh.client_id
and REF=oh.order_id
and SKU=ol.sku_id
and LIN=ol.line_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 4
(select
nvl(cl.name,cl.client_id) A23,
oh.from_site_id B23,
sum(nvl(QQ,0)) C33,
sum(nvl(QA,0)) L33
from order_header oh,client cl,order_line ol,(select
reference_id REF,
sku_id SKU,
line_id LIN,
sum(DECODE(code,'Shipment',nvl(update_qty,0),'Unshipment',nvl(update_qty,0)*-1,0)) QQ,
sum(DECODE(code,'Allocate',nvl(update_qty,0),'Deallocate',nvl(update_qty,0)*-1,0)) QA
from inventory_transaction
where code in ('Shipment','Unshipment','Allocate','Deallocate')
and Dstamp between trunc(sysdate)-70-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
group by
reference_id,
sku_id,
line_id)
where oh.status not in ('Cancelled')
and oh.creation_date between trunc(sysdate)-70-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-63-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
and oh.order_id=ol.order_id
and ol.client_id=oh.client_id
and REF=oh.order_id
and SKU=ol.sku_id
and LIN=ol.line_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 5
(select
nvl(cl.name,cl.client_id) A24,
oh.from_site_id B24,
sum(nvl(QQ,0)) C34,
sum(nvl(QA,0)) L34
from order_header oh,client cl,order_line ol,(select
reference_id REF,
sku_id SKU,
line_id LIN,
sum(DECODE(code,'Shipment',nvl(update_qty,0),'Unshipment',nvl(update_qty,0)*-1,0)) QQ,
sum(DECODE(code,'Allocate',nvl(update_qty,0),'Deallocate',nvl(update_qty,0)*-1,0)) QA
from inventory_transaction
where code in ('Shipment','Unshipment','Allocate','Deallocate')
and Dstamp between trunc(sysdate)-70-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-35-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
group by
reference_id,
sku_id,
line_id)
where oh.status not in ('Cancelled')
and oh.creation_date between trunc(sysdate)-70-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-63-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
and oh.order_id=ol.order_id
and ol.client_id=oh.client_id
and REF=oh.order_id
and SKU=ol.sku_id
and LIN=ol.line_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 6
(select
nvl(cl.name,cl.client_id) A25,
oh.from_site_id B25,
sum(nvl(QQ,0)) C35,
sum(nvl(QA,0)) L35
from order_header oh,client cl,order_line ol,(select
reference_id REF,
sku_id SKU,
line_id LIN,
sum(DECODE(code,'Shipment',nvl(update_qty,0),'Unshipment',nvl(update_qty,0)*-1,0)) QQ,
sum(DECODE(code,'Allocate',nvl(update_qty,0),'Deallocate',nvl(update_qty,0)*-1,0)) QA
from inventory_transaction
where code in ('Shipment','Unshipment','Allocate','Deallocate')
and Dstamp>trunc(sysdate)-70-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
--and trunc(sysdate)-28-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
group by
reference_id,
sku_id,
line_id)
where oh.status not in ('Cancelled')
and oh.creation_date between trunc(sysdate)-70-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-63-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
and oh.order_id=ol.order_id
and ol.client_id=oh.client_id
and REF=oh.order_id
and SKU=ol.sku_id
and LIN=ol.line_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--**************SHIPPED ORDERS********************************* 1
(select
nvl(cl.name,cl.client_id) A3,
oh.from_site_id B3,
count(distinct oh.order_id) D3
from order_header oh,client cl
where status='Shipped'
and oh.creation_date between trunc(sysdate)-70-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-63-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.shipped_date between trunc(sysdate)-70-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-63-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 2
(select
nvl(cl.name,cl.client_id) A31,
oh.from_site_id B31,
count(distinct oh.order_id) D31
from order_header oh,client cl
where status='Shipped'
and oh.creation_date between trunc(sysdate)-70-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-63-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.shipped_date between trunc(sysdate)-70-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-56-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 3
(select
nvl(cl.name,cl.client_id) A32,
oh.from_site_id B32,
count(distinct oh.order_id) D32
from order_header oh,client cl
where status='Shipped'
and oh.creation_date between trunc(sysdate)-70-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-63-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.shipped_date between trunc(sysdate)-70-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 4
(select
nvl(cl.name,cl.client_id) A33,
oh.from_site_id B33,
count(distinct oh.order_id) D33
from order_header oh,client cl
where status='Shipped'
and oh.creation_date between trunc(sysdate)-70-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-63-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.shipped_date between trunc(sysdate)-70-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 5
(select
nvl(cl.name,cl.client_id) A34,
oh.from_site_id B34,
count(distinct oh.order_id) D34
from order_header oh,client cl
where status='Shipped'
and oh.creation_date between trunc(sysdate)-70-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-63-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.shipped_date between trunc(sysdate)-70-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-35-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 6
(select
nvl(cl.name,cl.client_id) A35,
oh.from_site_id B35,
count(distinct oh.order_id) D35
from order_header oh,client cl
where status='Shipped'
and oh.creation_date between trunc(sysdate)-70-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-63-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.shipped_date>trunc(sysdate)-70-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
--and trunc(sysdate)-28-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--****************OPEN ORDERS*******************************
(select
nvl(cl.name,cl.client_id) A36,
oh.from_site_id B36,
count(distinct oh.order_id) D36
from order_header oh,client cl
where status not in ('Shipped','Cancelled')
and oh.creation_date between trunc(sysdate)-70-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-63-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id)
where AA=A2 (+)
and BB=B2 (+)
and AA=A20 (+)
and BB=B20 (+)
and AA=A21 (+)
and BB=B21 (+)
and AA=A22 (+)
and BB=B22 (+)
and AA=A23 (+)
and BB=B23 (+)
and AA=A24 (+)
and BB=B24 (+)
and AA=A25 (+)
and BB=B25 (+)
and AA=A3 (+)
and BB=B3 (+)
and AA=A31 (+)
and BB=B31 (+)
and AA=A32 (+)
and BB=B32 (+)
and AA=A33 (+)
and BB=B33 (+)
and AA=A34 (+)
and BB=B34 (+)
and AA=A35 (+)
and BB=B35 (+)
and AA=A36 (+)
and BB=B36 (+)
order by upper(BB),upper(AA))
union all
select 'Detail for Week Commencing '||trunc(sysdate-63-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)) from DUAL
union all
Select',,,,First Week,,,,,Second Week,,,,,Third Week,,,,,Fourth Week,,,,,Fifth Week,,,,,Sixth Week' from DUAL
union all
Select 'Site,Client,Orders Received,Units Ordered,Units Allocated,Shortfall,Units Shipped,% Fulfilled,Orders Shipped,Units Allocated,Shortfall,Units Shipped,% Fulfilled,Orders Shipped,Units Allocated,Shortfall,Units Shipped,% Fulfilled,Orders Shipped,Units Allocated,Shortfall,Units Shipped,% Fulfilled,Orders Shipped,Units Allocated,Shortfall,Units Shipped,% Fulfilled,Orders Shipped,Units Allocated,Shortfall,Units Shipped,% Fulfilled,Orders Shipped,Still NOT Shipped,Oldest Order NOT Shipped' from DUAL
union all
select * from (select
upper(BB)||','||
upper(AA)||','||
nvl(D1,0)||','||
nvl(D2,0)||','||
--******************* FIRST WEEK ****************************
--'AVL'||','||
--Units Allocated below
nvl(L3,0)||','||
nvl(D2-nvl(L3,0),0)||','||
--Units Shipped Below
nvl(C3,0)||','||
ltrim(to_char((C3/D2)*100,'999.00'))||','||
--Orders Shipped Below
nvl(D3,0)||','||
--**************** SECOND WEEK *******************************
--'AVL'||','||
--Units Allocated below
nvl(L31,0)||','||
nvl(D2-nvl(L31,0),0)||','||
--Units Shipped Below
nvl(C31,0)||','||
ltrim(to_char((C31/D2)*100,'999.00'))||','||
--Orders Shipped Below
nvl(D31,0)||','||
--***************** THIRD WEEK******************************
--'AVL'||','||
--Units Allocated below
nvl(L32,0)||','||
nvl(D2-nvl(L32,0),0)||','||
--Units Shipped Below
nvl(C32,0)||','||
ltrim(to_char((C32/D2)*100,'999.00'))||','||
--Orders Shipped Below
nvl(D32,0)||','||
--***************** FOURTH WEEK******************************
--'AVL'||','||
--Units Allocated below
nvl(L33,0)||','||
nvl(D2-nvl(L33,0),0)||','||
--Units Shipped Below
nvl(C33,0)||','||
ltrim(to_char((C33/D2)*100,'999.00'))||','||
--Orders Shipped Below
nvl(D33,0)||','||
--***************** FIFTH WEEK ******************************
--'AVL'||','||
--Units Allocated below
nvl(L34,0)||','||
nvl(D2-nvl(L34,0),0)||','||
--Units Shipped Below
nvl(C34,0)||','||
ltrim(to_char((C34/D2)*100,'999.00'))||','||
--Orders Shipped Below
nvl(D34,0)||','||
--****************** SIXTH WEEK *****************************
--'AVL'||','||
--Units Allocated below
nvl(L35,0)||','||
nvl(D2-nvl(L35,0),0)||','||
--Units Shipped Below
nvl(C35,0)||','||
ltrim(to_char((C35/D2)*100,'999.00'))||','||
--Orders Shipped Below
nvl(D35,0)||','||
--***************** FINALE  ******************************
nvl(D36,0)||','||
E10
--***************** OLDEST ORDER  ******************************
from (select
nvl(cl.name,cl.client_id) AA,
ST BB,
min(trunc(creation_date)) E10
from order_header oh,client cl,(select
distinct
client_id CL,
site_id ST
from inventory)
where status not in ('Cancelled','Shipped')
and oh.client_id=cl.client_id
and oh.client_id=CL
group by
nvl(cl.name,cl.client_id),ST),
--****************ORDERS RECEIVED AND UNITS *******************************
(select
nvl(cl.name,cl.client_id) A2,
oh.from_site_id B2,
count(distinct oh.order_id) D1,
sum(DECODE(substr(oh.order_id,1,3),'MOR',(DECODE(substr(ol.user_def_note_1,1,8),'Original',substr(ol.user_def_note_1,31,4),ol.qty_ordered)),ol.qty_ordered)) D2
from order_header oh,client cl,order_line ol
where oh.status not in ('Cancelled')
and oh.creation_date between trunc(sysdate)-63-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-56-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
and oh.order_id=ol.order_id
and ol.client_id=oh.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--***********************SHIPMENTS AND ALLOCATIONS ************************ 1
(select
nvl(cl.name,cl.client_id) A20,
oh.from_site_id B20,
sum(nvl(QQ,0)) C3,
sum(nvl(QA,0)) L3
from order_header oh,client cl,order_line ol,(select
reference_id REF,
sku_id SKU,
line_id LIN,
sum(DECODE(code,'Shipment',nvl(update_qty,0),'Unshipment',nvl(update_qty,0)*-1,0)) QQ,
sum(DECODE(code,'Allocate',nvl(update_qty,0),'Deallocate',nvl(update_qty,0)*-1,0)) QA
from inventory_transaction
where code in ('Shipment','Unshipment','Allocate','Deallocate')
and Dstamp between trunc(sysdate)-63-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-56-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
group by
reference_id,
sku_id,
line_id)
where oh.status not in ('Cancelled')
and oh.creation_date between trunc(sysdate)-63-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-56-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
and oh.order_id=ol.order_id
and ol.client_id=oh.client_id
and ol.order_id=REF(+)
and ol.sku_id=SKU(+)
and ol.line_id=LIN(+)
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 2
(select
nvl(cl.name,cl.client_id) A21,
oh.from_site_id B21,
sum(nvl(QQ,0)) C31,
sum(nvl(QA,0)) L31
from order_header oh,client cl,order_line ol,(select
reference_id REF,
sku_id SKU,
line_id LIN,
sum(DECODE(code,'Shipment',nvl(update_qty,0),'Unshipment',nvl(update_qty,0)*-1,0)) QQ,
sum(DECODE(code,'Allocate',nvl(update_qty,0),'Deallocate',nvl(update_qty,0)*-1,0)) QA
from inventory_transaction
where code in ('Shipment','Unshipment','Allocate','Deallocate')
and Dstamp between trunc(sysdate)-63-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
group by
reference_id,
sku_id,
line_id)
where oh.status not in ('Cancelled')
and oh.creation_date between trunc(sysdate)-63-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-56-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
and oh.order_id=ol.order_id
and ol.client_id=oh.client_id
and ol.order_id=REF(+)
and ol.sku_id=SKU(+)
and ol.line_id=LIN(+)
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 3
(select
nvl(cl.name,cl.client_id) A22,
oh.from_site_id B22,
sum(nvl(QQ,0)) C32,
sum(nvl(QA,0)) L32
from order_header oh,client cl,order_line ol,(select
reference_id REF,
sku_id SKU,
line_id LIN,
sum(DECODE(code,'Shipment',nvl(update_qty,0),'Unshipment',nvl(update_qty,0)*-1,0)) QQ,
sum(DECODE(code,'Allocate',nvl(update_qty,0),'Deallocate',nvl(update_qty,0)*-1,0)) QA
from inventory_transaction
where code in ('Shipment','Unshipment','Allocate','Deallocate')
and Dstamp between trunc(sysdate)-63-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
group by
reference_id,
sku_id,
line_id)
where oh.status not in ('Cancelled')
and oh.creation_date between trunc(sysdate)-63-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-56-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
and oh.order_id=ol.order_id
and ol.client_id=oh.client_id
and ol.order_id=REF(+)
and ol.sku_id=SKU(+)
and ol.line_id=LIN(+)
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 4
(select
nvl(cl.name,cl.client_id) A23,
oh.from_site_id B23,
sum(nvl(QQ,0)) C33,
sum(nvl(QA,0)) L33
from order_header oh,client cl,order_line ol,(select
reference_id REF,
sku_id SKU,
line_id LIN,
sum(DECODE(code,'Shipment',nvl(update_qty,0),'Unshipment',nvl(update_qty,0)*-1,0)) QQ,
sum(DECODE(code,'Allocate',nvl(update_qty,0),'Deallocate',nvl(update_qty,0)*-1,0)) QA
from inventory_transaction
where code in ('Shipment','Unshipment','Allocate','Deallocate')
and Dstamp between trunc(sysdate)-63-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-35-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
group by
reference_id,
sku_id,
line_id)
where oh.status not in ('Cancelled')
and oh.creation_date between trunc(sysdate)-63-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-56-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
and oh.order_id=ol.order_id
and ol.client_id=oh.client_id
and ol.order_id=REF(+)
and ol.sku_id=SKU(+)
and ol.line_id=LIN(+)
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 5
(select
nvl(cl.name,cl.client_id) A24,
oh.from_site_id B24,
sum(nvl(QQ,0)) C34,
sum(nvl(QA,0)) L34
from order_header oh,client cl,order_line ol,(select
reference_id REF,
sku_id SKU,
line_id LIN,
sum(DECODE(code,'Shipment',nvl(update_qty,0),'Unshipment',nvl(update_qty,0)*-1,0)) QQ,
sum(DECODE(code,'Allocate',nvl(update_qty,0),'Deallocate',nvl(update_qty,0)*-1,0)) QA
from inventory_transaction
where code in ('Shipment','Unshipment','Allocate','Deallocate')
and Dstamp between trunc(sysdate)-63-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-28-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
group by
reference_id,
sku_id,
line_id)
where oh.status not in ('Cancelled')
and oh.creation_date between trunc(sysdate)-63-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-56-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
and oh.order_id=ol.order_id
and ol.client_id=oh.client_id
and ol.order_id=REF(+)
and ol.sku_id=SKU(+)
and ol.line_id=LIN(+)
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 6
(select
nvl(cl.name,cl.client_id) A25,
oh.from_site_id B25,
sum(nvl(QQ,0)) C35,
sum(nvl(QA,0)) L35
from order_header oh,client cl,order_line ol,(select
reference_id REF,
sku_id SKU,
line_id LIN,
sum(DECODE(code,'Shipment',nvl(update_qty,0),'Unshipment',nvl(update_qty,0)*-1,0)) QQ,
sum(DECODE(code,'Allocate',nvl(update_qty,0),'Deallocate',nvl(update_qty,0)*-1,0)) QA
from inventory_transaction
where code in ('Shipment','Unshipment','Allocate','Deallocate')
and Dstamp>trunc(sysdate)-63-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
--and trunc(sysdate)-21-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
group by
reference_id,
sku_id,
line_id)
where oh.status not in ('Cancelled')
and oh.creation_date between trunc(sysdate)-63-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-56-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
and oh.order_id=ol.order_id
and ol.client_id=oh.client_id
and ol.order_id=REF(+)
and ol.sku_id=SKU(+)
and ol.line_id=LIN(+)
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--**************SHIPPED ORDERS********************************* 1
(select
nvl(cl.name,cl.client_id) A3,
oh.from_site_id B3,
count(distinct oh.order_id) D3
from order_header oh,client cl
where status='Shipped'
and oh.creation_date between trunc(sysdate)-63-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-56-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.shipped_date between trunc(sysdate)-63-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-56-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 2
(select
nvl(cl.name,cl.client_id) A31,
oh.from_site_id B31,
count(distinct oh.order_id) D31
from order_header oh,client cl
where status='Shipped'
and oh.creation_date between trunc(sysdate)-63-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-56-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.shipped_date between trunc(sysdate)-63-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 3
(select
nvl(cl.name,cl.client_id) A32,
oh.from_site_id B32,
count(distinct oh.order_id) D32
from order_header oh,client cl
where status='Shipped'
and oh.creation_date between trunc(sysdate)-63-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-56-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.shipped_date between trunc(sysdate)-63-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 4
(select
nvl(cl.name,cl.client_id) A33,
oh.from_site_id B33,
count(distinct oh.order_id) D33
from order_header oh,client cl
where status='Shipped'
and oh.creation_date between trunc(sysdate)-63-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-56-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.shipped_date between trunc(sysdate)-63-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-35-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 5
(select
nvl(cl.name,cl.client_id) A34,
oh.from_site_id B34,
count(distinct oh.order_id) D34
from order_header oh,client cl
where status='Shipped'
and oh.creation_date between trunc(sysdate)-63-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-56-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.shipped_date between trunc(sysdate)-63-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-28-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 6
(select
nvl(cl.name,cl.client_id) A35,
oh.from_site_id B35,
count(distinct oh.order_id) D35
from order_header oh,client cl
where status='Shipped'
and oh.creation_date between trunc(sysdate)-63-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-56-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.shipped_date>trunc(sysdate)-63-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
--and trunc(sysdate)-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--****************OPEN ORDERS*******************************
(select
nvl(cl.name,cl.client_id) A36,
oh.from_site_id B36,
count(distinct oh.order_id) D36
from order_header oh,client cl
where status not in ('Shipped','Cancelled')
and oh.creation_date between trunc(sysdate)-63-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-56-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id)
where AA=A2 (+)
and BB=B2 (+)
and AA=A20 (+)
and BB=B20 (+)
and AA=A21 (+)
and BB=B21 (+)
and AA=A22 (+)
and BB=B22 (+)
and AA=A23 (+)
and BB=B23 (+)
and AA=A24 (+)
and BB=B24 (+)
and AA=A25 (+)
and BB=B25 (+)
and AA=A3 (+)
and BB=B3 (+)
and AA=A31 (+)
and BB=B31 (+)
and AA=A32 (+)
and BB=B32 (+)
and AA=A33 (+)
and BB=B33 (+)
and AA=A34 (+)
and BB=B34 (+)
and AA=A35 (+)
and BB=B35 (+)
and AA=A36 (+)
and BB=B36 (+)
order by upper(BB),upper(AA))
union all
select 'Detail for Week Commencing '||trunc(sysdate-56-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)) from DUAL
union all
Select',,,,First Week,,,,,Second Week,,,,,Third Week,,,,,Fourth Week,,,,,Fifth Week,,,,,Sixth Week' from DUAL
union all
Select 'Site,Client,Orders Received,Units Ordered,Units Allocated,Shortfall,Units Shipped,% Fulfilled,Orders Shipped,Units Allocated,Shortfall,Units Shipped,% Fulfilled,Orders Shipped,Units Allocated,Shortfall,Units Shipped,% Fulfilled,Orders Shipped,Units Allocated,Shortfall,Units Shipped,% Fulfilled,Orders Shipped,Units Allocated,Shortfall,Units Shipped,% Fulfilled,Orders Shipped,Units Allocated,Shortfall,Units Shipped,% Fulfilled,Orders Shipped,Still NOT Shipped,Oldest Order NOT Shipped' from DUAL
union all
select * from (select
upper(BB)||','||
upper(AA)||','||
nvl(D1,0)||','||
nvl(D2,0)||','||
--******************* FIRST WEEK ****************************
--'AVL'||','||
--Units Allocated below
nvl(L3,0)||','||
nvl(D2-nvl(L3,0),0)||','||
--Units Shipped Below
nvl(C3,0)||','||
ltrim(to_char((C3/D2)*100,'999.00'))||','||
--Orders Shipped Below
nvl(D3,0)||','||
--**************** SECOND WEEK *******************************
--'AVL'||','||
--Units Allocated below
nvl(L31,0)||','||
nvl(D2-nvl(L31,0),0)||','||
--Units Shipped Below
nvl(C31,0)||','||
ltrim(to_char((C31/D2)*100,'999.00'))||','||
--Orders Shipped Below
nvl(D31,0)||','||
--***************** THIRD WEEK******************************
--'AVL'||','||
--Units Allocated below
nvl(L32,0)||','||
nvl(D2-nvl(L32,0),0)||','||
--Units Shipped Below
nvl(C32,0)||','||
ltrim(to_char((C32/D2)*100,'999.00'))||','||
--Orders Shipped Below
nvl(D32,0)||','||
--***************** FOURTH WEEK******************************
--'AVL'||','||
--Units Allocated below
nvl(L33,0)||','||
nvl(D2-nvl(L33,0),0)||','||
--Units Shipped Below
nvl(C33,0)||','||
ltrim(to_char((C33/D2)*100,'999.00'))||','||
--Orders Shipped Below
nvl(D33,0)||','||
--***************** FIFTH WEEK ******************************
--'AVL'||','||
--Units Allocated below
nvl(L34,0)||','||
nvl(D2-nvl(L34,0),0)||','||
--Units Shipped Below
nvl(C34,0)||','||
ltrim(to_char((C34/D2)*100,'999.00'))||','||
--Orders Shipped Below
nvl(D34,0)||','||
--****************** SIXTH WEEK *****************************
--'AVL'||','||
--Units Allocated below
nvl(L35,0)||','||
nvl(D2-nvl(L35,0),0)||','||
--Units Shipped Below
nvl(C35,0)||','||
ltrim(to_char((C35/D2)*100,'999.00'))||','||
--Orders Shipped Below
nvl(D35,0)||','||
--***************** FINALE  ******************************
nvl(D36,0)||','||
E10
--***************** OLDEST ORDER  ******************************
from (select
nvl(cl.name,cl.client_id) AA,
ST BB,
min(trunc(creation_date)) E10
from order_header oh,client cl,(select
distinct
client_id CL,
site_id ST
from inventory)
where status not in ('Cancelled','Shipped')
and oh.client_id=cl.client_id
and oh.client_id=CL
group by
nvl(cl.name,cl.client_id),ST),
--****************ORDERS RECEIVED AND UNITS *******************************
(select
nvl(cl.name,cl.client_id) A2,
oh.from_site_id B2,
count(distinct oh.order_id) D1,
sum(DECODE(substr(oh.order_id,1,3),'MOR',(DECODE(substr(ol.user_def_note_1,1,8),'Original',substr(ol.user_def_note_1,31,4),ol.qty_ordered)),ol.qty_ordered)) D2
from order_header oh,client cl,order_line ol
where oh.status not in ('Cancelled')
and oh.creation_date between trunc(sysdate)-56-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
and oh.order_id=ol.order_id
and ol.client_id=oh.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--***********************SHIPMENTS AND ALLOCATIONS ************************ 1
(select
nvl(cl.name,cl.client_id) A20,
oh.from_site_id B20,
sum(nvl(QQ,0)) C3,
sum(nvl(QA,0)) L3
from order_header oh,client cl,order_line ol,(select
reference_id REF,
sku_id SKU,
line_id LIN,
sum(DECODE(code,'Shipment',nvl(update_qty,0),'Unshipment',nvl(update_qty,0)*-1,0)) QQ,
sum(DECODE(code,'Allocate',nvl(update_qty,0),'Deallocate',nvl(update_qty,0)*-1,0)) QA
from inventory_transaction
where code in ('Shipment','Unshipment','Allocate','Deallocate')
and Dstamp between trunc(sysdate)-56-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
group by
reference_id,
sku_id,
line_id)
where oh.status not in ('Cancelled')
and oh.creation_date between trunc(sysdate)-56-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
and oh.order_id=ol.order_id
and ol.client_id=oh.client_id
and REF=oh.order_id
and SKU=ol.sku_id
and LIN=ol.line_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 2
(select
nvl(cl.name,cl.client_id) A21,
oh.from_site_id B21,
sum(nvl(QQ,0)) C31,
sum(nvl(QA,0)) L31
from order_header oh,client cl,order_line ol,(select
reference_id REF,
sku_id SKU,
line_id LIN,
sum(DECODE(code,'Shipment',nvl(update_qty,0),'Unshipment',nvl(update_qty,0)*-1,0)) QQ,
sum(DECODE(code,'Allocate',nvl(update_qty,0),'Deallocate',nvl(update_qty,0)*-1,0)) QA
from inventory_transaction
where code in ('Shipment','Unshipment','Allocate','Deallocate')
and Dstamp between trunc(sysdate)-56-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
group by
reference_id,
sku_id,
line_id)
where oh.status not in ('Cancelled')
and oh.creation_date between trunc(sysdate)-56-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
and oh.order_id=ol.order_id
and ol.client_id=oh.client_id
and REF=oh.order_id
and SKU=ol.sku_id
and LIN=ol.line_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 3
(select
nvl(cl.name,cl.client_id) A22,
oh.from_site_id B22,
sum(nvl(QQ,0)) C32,
sum(nvl(QA,0)) L32
from order_header oh,client cl,order_line ol,(select
reference_id REF,
sku_id SKU,
line_id LIN,
sum(DECODE(code,'Shipment',nvl(update_qty,0),'Unshipment',nvl(update_qty,0)*-1,0)) QQ,
sum(DECODE(code,'Allocate',nvl(update_qty,0),'Deallocate',nvl(update_qty,0)*-1,0)) QA
from inventory_transaction
where code in ('Shipment','Unshipment','Allocate','Deallocate')
and Dstamp between trunc(sysdate)-56-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-35-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
group by
reference_id,
sku_id,
line_id)
where oh.status not in ('Cancelled')
and oh.creation_date between trunc(sysdate)-56-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
and oh.order_id=ol.order_id
and ol.client_id=oh.client_id
and REF=oh.order_id
and SKU=ol.sku_id
and LIN=ol.line_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 4
(select
nvl(cl.name,cl.client_id) A23,
oh.from_site_id B23,
sum(nvl(QQ,0)) C33,
sum(nvl(QA,0)) L33
from order_header oh,client cl,order_line ol,(select
reference_id REF,
sku_id SKU,
line_id LIN,
sum(DECODE(code,'Shipment',nvl(update_qty,0),'Unshipment',nvl(update_qty,0)*-1,0)) QQ,
sum(DECODE(code,'Allocate',nvl(update_qty,0),'Deallocate',nvl(update_qty,0)*-1,0)) QA
from inventory_transaction
where code in ('Shipment','Unshipment','Allocate','Deallocate')
and Dstamp between trunc(sysdate)-56-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-28-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
group by
reference_id,
sku_id,
line_id)
where oh.status not in ('Cancelled')
and oh.creation_date between trunc(sysdate)-56-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
and oh.order_id=ol.order_id
and ol.client_id=oh.client_id
and REF=oh.order_id
and SKU=ol.sku_id
and LIN=ol.line_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 5
(select
nvl(cl.name,cl.client_id) A24,
oh.from_site_id B24,
sum(nvl(QQ,0)) C34,
sum(nvl(QA,0)) L34
from order_header oh,client cl,order_line ol,(select
reference_id REF,
sku_id SKU,
line_id LIN,
sum(DECODE(code,'Shipment',nvl(update_qty,0),'Unshipment',nvl(update_qty,0)*-1,0)) QQ,
sum(DECODE(code,'Allocate',nvl(update_qty,0),'Deallocate',nvl(update_qty,0)*-1,0)) QA
from inventory_transaction
where code in ('Shipment','Unshipment','Allocate','Deallocate')
and Dstamp between trunc(sysdate)-56-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-21-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
group by
reference_id,
sku_id,
line_id)
where oh.status not in ('Cancelled')
and oh.creation_date between trunc(sysdate)-56-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
and oh.order_id=ol.order_id
and ol.client_id=oh.client_id
and REF=oh.order_id
and SKU=ol.sku_id
and LIN=ol.line_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 6
(select
nvl(cl.name,cl.client_id) A25,
oh.from_site_id B25,
sum(nvl(QQ,0)) C35,
sum(nvl(QA,0)) L35
from order_header oh,client cl,order_line ol,(select
reference_id REF,
sku_id SKU,
line_id LIN,
sum(DECODE(code,'Shipment',nvl(update_qty,0),'Unshipment',nvl(update_qty,0)*-1,0)) QQ,
sum(DECODE(code,'Allocate',nvl(update_qty,0),'Deallocate',nvl(update_qty,0)*-1,0)) QA
from inventory_transaction
where code in ('Shipment','Unshipment','Allocate','Deallocate')
and Dstamp>trunc(sysdate)-56-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
--and trunc(sysdate)-14-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
group by
reference_id,
sku_id,
line_id)
where oh.status not in ('Cancelled')
and oh.creation_date between trunc(sysdate)-56-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
and oh.order_id=ol.order_id
and ol.client_id=oh.client_id
and REF=oh.order_id
and SKU=ol.sku_id
and LIN=ol.line_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--**************SHIPPED ORDERS********************************* 1
(select
nvl(cl.name,cl.client_id) A3,
oh.from_site_id B3,
count(distinct oh.order_id) D3
from order_header oh,client cl
where status='Shipped'
and oh.creation_date between trunc(sysdate)-56-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.shipped_date between trunc(sysdate)-56-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 2
(select
nvl(cl.name,cl.client_id) A31,
oh.from_site_id B31,
count(distinct oh.order_id) D31
from order_header oh,client cl
where status='Shipped'
and oh.creation_date between trunc(sysdate)-56-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.shipped_date between trunc(sysdate)-56-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 3
(select
nvl(cl.name,cl.client_id) A32,
oh.from_site_id B32,
count(distinct oh.order_id) D32
from order_header oh,client cl
where status='Shipped'
and oh.creation_date between trunc(sysdate)-56-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.shipped_date between trunc(sysdate)-56-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-35-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 4
(select
nvl(cl.name,cl.client_id) A33,
oh.from_site_id B33,
count(distinct oh.order_id) D33
from order_header oh,client cl
where status='Shipped'
and oh.creation_date between trunc(sysdate)-56-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.shipped_date between trunc(sysdate)-56-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-28-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 5
(select
nvl(cl.name,cl.client_id) A34,
oh.from_site_id B34,
count(distinct oh.order_id) D34
from order_header oh,client cl
where status='Shipped'
and oh.creation_date between trunc(sysdate)-56-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.shipped_date between trunc(sysdate)-56-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-21-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 6
(select
nvl(cl.name,cl.client_id) A35,
oh.from_site_id B35,
count(distinct oh.order_id) D35
from order_header oh,client cl
where status='Shipped'
and oh.creation_date between trunc(sysdate)-56-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.shipped_date>trunc(sysdate)-56-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
--and trunc(sysdate)-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--****************OPEN ORDERS*******************************
(select
nvl(cl.name,cl.client_id) A36,
oh.from_site_id B36,
count(distinct oh.order_id) D36
from order_header oh,client cl
where status not in ('Shipped','Cancelled')
and oh.creation_date between trunc(sysdate)-56-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id)
where AA=A2 (+)
and BB=B2 (+)
and AA=A20 (+)
and BB=B20 (+)
and AA=A21 (+)
and BB=B21 (+)
and AA=A22 (+)
and BB=B22 (+)
and AA=A23 (+)
and BB=B23 (+)
and AA=A24 (+)
and BB=B24 (+)
and AA=A25 (+)
and BB=B25 (+)
and AA=A3 (+)
and BB=B3 (+)
and AA=A31 (+)
and BB=B31 (+)
and AA=A32 (+)
and BB=B32 (+)
and AA=A33 (+)
and BB=B33 (+)
and AA=A34 (+)
and BB=B34 (+)
and AA=A35 (+)
and BB=B35 (+)
and AA=A36 (+)
and BB=B36 (+)
order by upper(BB),upper(AA))
union all
select 'Detail for Week Commencing '||trunc(sysdate-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)) from DUAL
union all
Select',,,,First Week,,,,,Second Week,,,,,Third Week,,,,,Fourth Week,,,,,Fifth Week,,,,,Sixth Week' from DUAL
union all
Select 'Site,Client,Orders Received,Units Ordered,Units Allocated,Shortfall,Units Shipped,% Fulfilled,Orders Shipped,Units Allocated,Shortfall,Units Shipped,% Fulfilled,Orders Shipped,Units Allocated,Shortfall,Units Shipped,% Fulfilled,Orders Shipped,Units Allocated,Shortfall,Units Shipped,% Fulfilled,Orders Shipped,Units Allocated,Shortfall,Units Shipped,% Fulfilled,Orders Shipped,Units Allocated,Shortfall,Units Shipped,% Fulfilled,Orders Shipped,Still NOT Shipped,Oldest Order NOT Shipped' from DUAL
union all
select * from (select
upper(BB)||','||
upper(AA)||','||
nvl(D1,0)||','||
nvl(D2,0)||','||
--******************* FIRST WEEK ****************************
--'AVL'||','||
--Units Allocated below
nvl(L3,0)||','||
nvl(D2-nvl(L3,0),0)||','||
--Units Shipped Below
nvl(C3,0)||','||
ltrim(to_char((C3/D2)*100,'999.00'))||','||
--Orders Shipped Below
nvl(D3,0)||','||
--**************** SECOND WEEK *******************************
--'AVL'||','||
--Units Allocated below
nvl(L31,0)||','||
nvl(D2-nvl(L31,0),0)||','||
--Units Shipped Below
nvl(C31,0)||','||
ltrim(to_char((C31/D2)*100,'999.00'))||','||
--Orders Shipped Below
nvl(D31,0)||','||
--***************** THIRD WEEK******************************
--'AVL'||','||
--Units Allocated below
nvl(L32,0)||','||
nvl(D2-nvl(L32,0),0)||','||
--Units Shipped Below
nvl(C32,0)||','||
ltrim(to_char((C32/D2)*100,'999.00'))||','||
--Orders Shipped Below
nvl(D32,0)||','||
--***************** FOURTH WEEK******************************
--'AVL'||','||
--Units Allocated below
nvl(L33,0)||','||
nvl(D2-nvl(L33,0),0)||','||
--Units Shipped Below
nvl(C33,0)||','||
ltrim(to_char((C33/D2)*100,'999.00'))||','||
--Orders Shipped Below
nvl(D33,0)||','||
--***************** FIFTH WEEK ******************************
--'AVL'||','||
--Units Allocated below
nvl(L34,0)||','||
nvl(D2-nvl(L34,0),0)||','||
--Units Shipped Below
nvl(C34,0)||','||
ltrim(to_char((C34/D2)*100,'999.00'))||','||
--Orders Shipped Below
nvl(D34,0)||','||
--****************** SIXTH WEEK *****************************
--'AVL'||','||
--Units Allocated below
nvl(L35,0)||','||
nvl(D2-nvl(L35,0),0)||','||
--Units Shipped Below
nvl(C35,0)||','||
ltrim(to_char((C35/D2)*100,'999.00'))||','||
--Orders Shipped Below
nvl(D35,0)||','||
--***************** FINALE  ******************************
nvl(D36,0)||','||
E10
--***************** OLDEST ORDER  ******************************
from (select
nvl(cl.name,cl.client_id) AA,
ST BB,
min(trunc(creation_date)) E10
from order_header oh,client cl,(select
distinct
client_id CL,
site_id ST
from inventory)
where status not in ('Cancelled','Shipped')
and oh.client_id=cl.client_id
and oh.client_id=CL
group by
nvl(cl.name,cl.client_id),ST),
--****************ORDERS RECEIVED AND UNITS *******************************
(select
nvl(cl.name,cl.client_id) A2,
oh.from_site_id B2,
count(distinct oh.order_id) D1,
sum(DECODE(substr(oh.order_id,1,3),'MOR',(DECODE(substr(ol.user_def_note_1,1,8),'Original',substr(ol.user_def_note_1,31,4),ol.qty_ordered)),ol.qty_ordered)) D2
from order_header oh,client cl,order_line ol
where oh.status not in ('Cancelled')
and oh.creation_date between trunc(sysdate)-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
and oh.order_id=ol.order_id
and ol.client_id=oh.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--***********************SHIPMENTS AND ALLOCATIONS ************************ 1
(select
nvl(cl.name,cl.client_id) A20,
oh.from_site_id B20,
sum(nvl(QQ,0)) C3,
sum(nvl(QA,0)) L3
from order_header oh,client cl,order_line ol,(select
reference_id REF,
sku_id SKU,
line_id LIN,
sum(DECODE(code,'Shipment',nvl(update_qty,0),'Unshipment',nvl(update_qty,0)*-1,0)) QQ,
sum(DECODE(code,'Allocate',nvl(update_qty,0),'Deallocate',nvl(update_qty,0)*-1,0)) QA
from inventory_transaction
where code in ('Shipment','Unshipment','Allocate','Deallocate')
and Dstamp between trunc(sysdate)-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
group by
reference_id,
sku_id,
line_id)
where oh.status not in ('Cancelled')
and oh.creation_date between trunc(sysdate)-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
and oh.order_id=ol.order_id
and ol.client_id=oh.client_id
and REF=oh.order_id
and SKU=ol.sku_id
and LIN=ol.line_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 2
(select
nvl(cl.name,cl.client_id) A21,
oh.from_site_id B21,
sum(nvl(QQ,0)) C31,
sum(nvl(QA,0)) L31
from order_header oh,client cl,order_line ol,(select
reference_id REF,
sku_id SKU,
line_id LIN,
sum(DECODE(code,'Shipment',nvl(update_qty,0),'Unshipment',nvl(update_qty,0)*-1,0)) QQ,
sum(DECODE(code,'Allocate',nvl(update_qty,0),'Deallocate',nvl(update_qty,0)*-1,0)) QA
from inventory_transaction
where code in ('Shipment','Unshipment','Allocate','Deallocate')
and Dstamp between trunc(sysdate)-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-35-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
group by
reference_id,
sku_id,
line_id)
where oh.status not in ('Cancelled')
and oh.creation_date between trunc(sysdate)-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
and oh.order_id=ol.order_id
and ol.client_id=oh.client_id
and REF=oh.order_id
and SKU=ol.sku_id
and LIN=ol.line_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 3
(select
nvl(cl.name,cl.client_id) A22,
oh.from_site_id B22,
sum(nvl(QQ,0)) C32,
sum(nvl(QA,0)) L32
from order_header oh,client cl,order_line ol,(select
reference_id REF,
sku_id SKU,
line_id LIN,
sum(DECODE(code,'Shipment',nvl(update_qty,0),'Unshipment',nvl(update_qty,0)*-1,0)) QQ,
sum(DECODE(code,'Allocate',nvl(update_qty,0),'Deallocate',nvl(update_qty,0)*-1,0)) QA
from inventory_transaction
where code in ('Shipment','Unshipment','Allocate','Deallocate')
and Dstamp between trunc(sysdate)-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-28-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
group by
reference_id,
sku_id,
line_id)
where oh.status not in ('Cancelled')
and oh.creation_date between trunc(sysdate)-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
and oh.order_id=ol.order_id
and ol.client_id=oh.client_id
and REF=oh.order_id
and SKU=ol.sku_id
and LIN=ol.line_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 4
(select
nvl(cl.name,cl.client_id) A23,
oh.from_site_id B23,
sum(nvl(QQ,0)) C33,
sum(nvl(QA,0)) L33
from order_header oh,client cl,order_line ol,(select
reference_id REF,
sku_id SKU,
line_id LIN,
sum(DECODE(code,'Shipment',nvl(update_qty,0),'Unshipment',nvl(update_qty,0)*-1,0)) QQ,
sum(DECODE(code,'Allocate',nvl(update_qty,0),'Deallocate',nvl(update_qty,0)*-1,0)) QA
from inventory_transaction
where code in ('Shipment','Unshipment','Allocate','Deallocate')
and Dstamp between trunc(sysdate)-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-21-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
group by
reference_id,
sku_id,
line_id)
where oh.status not in ('Cancelled')
and oh.creation_date between trunc(sysdate)-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
and oh.order_id=ol.order_id
and ol.client_id=oh.client_id
and REF=oh.order_id
and SKU=ol.sku_id
and LIN=ol.line_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 5
(select
nvl(cl.name,cl.client_id) A24,
oh.from_site_id B24,
sum(nvl(QQ,0)) C34,
sum(nvl(QA,0)) L34
from order_header oh,client cl,order_line ol,(select
reference_id REF,
sku_id SKU,
line_id LIN,
sum(DECODE(code,'Shipment',nvl(update_qty,0),'Unshipment',nvl(update_qty,0)*-1,0)) QQ,
sum(DECODE(code,'Allocate',nvl(update_qty,0),'Deallocate',nvl(update_qty,0)*-1,0)) QA
from inventory_transaction
where code in ('Shipment','Unshipment','Allocate','Deallocate')
and Dstamp between trunc(sysdate)-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-14-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
group by
reference_id,
sku_id,
line_id)
where oh.status not in ('Cancelled')
and oh.creation_date between trunc(sysdate)-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
and oh.order_id=ol.order_id
and ol.client_id=oh.client_id
and REF=oh.order_id
and SKU=ol.sku_id
and LIN=ol.line_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 6
(select
nvl(cl.name,cl.client_id) A25,
oh.from_site_id B25,
sum(nvl(QQ,0)) C35,
sum(nvl(QA,0)) L35
from order_header oh,client cl,order_line ol,(select
reference_id REF,
sku_id SKU,
line_id LIN,
sum(DECODE(code,'Shipment',nvl(update_qty,0),'Unshipment',nvl(update_qty,0)*-1,0)) QQ,
sum(DECODE(code,'Allocate',nvl(update_qty,0),'Deallocate',nvl(update_qty,0)*-1,0)) QA
from inventory_transaction
where code in ('Shipment','Unshipment','Allocate','Deallocate')
and Dstamp>trunc(sysdate)-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
--and trunc(sysdate)-7-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
group by
reference_id,
sku_id,
line_id)
where oh.status not in ('Cancelled')
and oh.creation_date between trunc(sysdate)-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
and oh.order_id=ol.order_id
and ol.client_id=oh.client_id
and REF=oh.order_id
and SKU=ol.sku_id
and LIN=ol.line_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--**************SHIPPED ORDERS********************************* 1
(select
nvl(cl.name,cl.client_id) A3,
oh.from_site_id B3,
count(distinct oh.order_id) D3
from order_header oh,client cl
where status='Shipped'
and oh.creation_date between trunc(sysdate)-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.shipped_date between trunc(sysdate)-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 2
(select
nvl(cl.name,cl.client_id) A31,
oh.from_site_id B31,
count(distinct oh.order_id) D31
from order_header oh,client cl
where status='Shipped'
and oh.creation_date between trunc(sysdate)-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.shipped_date between trunc(sysdate)-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-35-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 3
(select
nvl(cl.name,cl.client_id) A32,
oh.from_site_id B32,
count(distinct oh.order_id) D32
from order_header oh,client cl
where status='Shipped'
and oh.creation_date between trunc(sysdate)-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.shipped_date between trunc(sysdate)-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-28-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 4
(select
nvl(cl.name,cl.client_id) A33,
oh.from_site_id B33,
count(distinct oh.order_id) D33
from order_header oh,client cl
where status='Shipped'
and oh.creation_date between trunc(sysdate)-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.shipped_date between trunc(sysdate)-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-21-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 5
(select
nvl(cl.name,cl.client_id) A34,
oh.from_site_id B34,
count(distinct oh.order_id) D34
from order_header oh,client cl
where status='Shipped'
and oh.creation_date between trunc(sysdate)-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.shipped_date between trunc(sysdate)-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-14-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 6
(select
nvl(cl.name,cl.client_id) A35,
oh.from_site_id B35,
count(distinct oh.order_id) D35
from order_header oh,client cl
where status='Shipped'
and oh.creation_date between trunc(sysdate)-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.shipped_date>trunc(sysdate)-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
--and trunc(sysdate)-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--****************OPEN ORDERS*******************************
(select
nvl(cl.name,cl.client_id) A36,
oh.from_site_id B36,
count(distinct oh.order_id) D36
from order_header oh,client cl
where status not in ('Shipped','Cancelled')
and oh.creation_date between trunc(sysdate)-49-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id)
where AA=A2 (+)
and BB=B2 (+)
and AA=A20 (+)
and BB=B20 (+)
and AA=A21 (+)
and BB=B21 (+)
and AA=A22 (+)
and BB=B22 (+)
and AA=A23 (+)
and BB=B23 (+)
and AA=A24 (+)
and BB=B24 (+)
and AA=A25 (+)
and BB=B25 (+)
and AA=A3 (+)
and BB=B3 (+)
and AA=A31 (+)
and BB=B31 (+)
and AA=A32 (+)
and BB=B32 (+)
and AA=A33 (+)
and BB=B33 (+)
and AA=A34 (+)
and BB=B34 (+)
and AA=A35 (+)
and BB=B35 (+)
and AA=A36 (+)
and BB=B36 (+)
order by upper(BB),upper(AA))
union all
select 'Detail for Week Commencing '||trunc(sysdate-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)) from DUAL
union all
Select',,,,First Week,,,,,Second Week,,,,,Third Week,,,,,Fourth Week,,,,,Fifth Week,,,,,Sixth Week' from DUAL
union all
Select 'Site,Client,Orders Received,Units Ordered,Units Allocated,Shortfall,Units Shipped,% Fulfilled,Orders Shipped,Units Allocated,Shortfall,Units Shipped,% Fulfilled,Orders Shipped,Units Allocated,Shortfall,Units Shipped,% Fulfilled,Orders Shipped,Units Allocated,Shortfall,Units Shipped,% Fulfilled,Orders Shipped,Units Allocated,Shortfall,Units Shipped,% Fulfilled,Orders Shipped,Units Allocated,Shortfall,Units Shipped,% Fulfilled,Orders Shipped,Still NOT Shipped,Oldest Order NOT Shipped' from DUAL
union all
select * from (select
upper(BB)||','||
upper(AA)||','||
nvl(D1,0)||','||
nvl(D2,0)||','||
--******************* FIRST WEEK ****************************
--'AVL'||','||
--Units Allocated below
nvl(L3,0)||','||
nvl(D2-nvl(L3,0),0)||','||
--Units Shipped Below
nvl(C3,0)||','||
ltrim(to_char((C3/D2)*100,'999.00'))||','||
--Orders Shipped Below
nvl(D3,0)||','||
--**************** SECOND WEEK *******************************
--'AVL'||','||
--Units Allocated below
nvl(L31,0)||','||
nvl(D2-nvl(L31,0),0)||','||
--Units Shipped Below
nvl(C31,0)||','||
ltrim(to_char((C31/D2)*100,'999.00'))||','||
--Orders Shipped Below
nvl(D31,0)||','||
--***************** THIRD WEEK******************************
--'AVL'||','||
--Units Allocated below
nvl(L32,0)||','||
nvl(D2-nvl(L32,0),0)||','||
--Units Shipped Below
nvl(C32,0)||','||
ltrim(to_char((C32/D2)*100,'999.00'))||','||
--Orders Shipped Below
nvl(D32,0)||','||
--***************** FOURTH WEEK******************************
--'AVL'||','||
--Units Allocated below
nvl(L33,0)||','||
nvl(D2-nvl(L33,0),0)||','||
--Units Shipped Below
nvl(C33,0)||','||
ltrim(to_char((C33/D2)*100,'999.00'))||','||
--Orders Shipped Below
nvl(D33,0)||','||
--***************** FIFTH WEEK ******************************
--'AVL'||','||
--Units Allocated below
nvl(L34,0)||','||
nvl(D2-nvl(L34,0),0)||','||
--Units Shipped Below
nvl(C34,0)||','||
ltrim(to_char((C34/D2)*100,'999.00'))||','||
--Orders Shipped Below
nvl(D34,0)||','||
--****************** SIXTH WEEK *****************************
--'AVL'||','||
--Units Allocated below
nvl(L35,0)||','||
nvl(D2-nvl(L35,0),0)||','||
--Units Shipped Below
nvl(C35,0)||','||
ltrim(to_char((C35/D2)*100,'999.00'))||','||
--Orders Shipped Below
nvl(D35,0)||','||
--***************** FINALE  ******************************
nvl(D36,0)||','||
E10
--***************** OLDEST ORDER  ******************************
from (select
nvl(cl.name,cl.client_id) AA,
ST BB,
min(trunc(creation_date)) E10
from order_header oh,client cl,(select
distinct
client_id CL,
site_id ST
from inventory)
where status not in ('Cancelled','Shipped')
and oh.client_id=cl.client_id
and oh.client_id=CL
group by
nvl(cl.name,cl.client_id),ST),
--****************ORDERS RECEIVED AND UNITS *******************************
(select
nvl(cl.name,cl.client_id) A2,
oh.from_site_id B2,
count(distinct oh.order_id) D1,
sum(DECODE(substr(oh.order_id,1,3),'MOR',(DECODE(substr(ol.user_def_note_1,1,8),'Original',substr(ol.user_def_note_1,31,4),ol.qty_ordered)),ol.qty_ordered)) D2
from order_header oh,client cl,order_line ol
where oh.status not in ('Cancelled')
and oh.creation_date between trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-35-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
and oh.order_id=ol.order_id
and ol.client_id=oh.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--***********************SHIPMENTS AND ALLOCATIONS ************************ 1
(select
nvl(cl.name,cl.client_id) A20,
oh.from_site_id B20,
sum(nvl(QQ,0)) C3,
sum(nvl(QA,0)) L3
from order_header oh,client cl,order_line ol,(select
reference_id REF,
sku_id SKU,
line_id LIN,
sum(DECODE(code,'Shipment',nvl(update_qty,0),'Unshipment',nvl(update_qty,0)*-1,0)) QQ,
sum(DECODE(code,'Allocate',nvl(update_qty,0),'Deallocate',nvl(update_qty,0)*-1,0)) QA
from inventory_transaction
where code in ('Shipment','Unshipment','Allocate','Deallocate')
and Dstamp between trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-35-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
group by
reference_id,
sku_id,
line_id)
where oh.status not in ('Cancelled')
and oh.creation_date between trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-35-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
and oh.order_id=ol.order_id
and ol.client_id=oh.client_id
and REF=oh.order_id
and SKU=ol.sku_id
and LIN=ol.line_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 2
(select
nvl(cl.name,cl.client_id) A21,
oh.from_site_id B21,
sum(nvl(QQ,0)) C31,
sum(nvl(QA,0)) L31
from order_header oh,client cl,order_line ol,(select
reference_id REF,
sku_id SKU,
line_id LIN,
sum(DECODE(code,'Shipment',nvl(update_qty,0),'Unshipment',nvl(update_qty,0)*-1,0)) QQ,
sum(DECODE(code,'Allocate',nvl(update_qty,0),'Deallocate',nvl(update_qty,0)*-1,0)) QA
from inventory_transaction
where code in ('Shipment','Unshipment','Allocate','Deallocate')
and Dstamp between trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-28-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
group by
reference_id,
sku_id,
line_id)
where oh.status not in ('Cancelled')
and oh.creation_date between trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-35-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
and oh.order_id=ol.order_id
and ol.client_id=oh.client_id
and REF=oh.order_id
and SKU=ol.sku_id
and LIN=ol.line_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 3
(select
nvl(cl.name,cl.client_id) A22,
oh.from_site_id B22,
sum(nvl(QQ,0)) C32,
sum(nvl(QA,0)) L32
from order_header oh,client cl,order_line ol,(select
reference_id REF,
sku_id SKU,
line_id LIN,
sum(DECODE(code,'Shipment',nvl(update_qty,0),'Unshipment',nvl(update_qty,0)*-1,0)) QQ,
sum(DECODE(code,'Allocate',nvl(update_qty,0),'Deallocate',nvl(update_qty,0)*-1,0)) QA
from inventory_transaction
where code in ('Shipment','Unshipment','Allocate','Deallocate')
and Dstamp between trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-21-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
group by
reference_id,
sku_id,
line_id)
where oh.status not in ('Cancelled')
and oh.creation_date between trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-35-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
and oh.order_id=ol.order_id
and ol.client_id=oh.client_id
and REF=oh.order_id
and SKU=ol.sku_id
and LIN=ol.line_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 4
(select
nvl(cl.name,cl.client_id) A23,
oh.from_site_id B23,
sum(nvl(QQ,0)) C33,
sum(nvl(QA,0)) L33
from order_header oh,client cl,order_line ol,(select
reference_id REF,
sku_id SKU,
line_id LIN,
sum(DECODE(code,'Shipment',nvl(update_qty,0),'Unshipment',nvl(update_qty,0)*-1,0)) QQ,
sum(DECODE(code,'Allocate',nvl(update_qty,0),'Deallocate',nvl(update_qty,0)*-1,0)) QA
from inventory_transaction
where code in ('Shipment','Unshipment','Allocate','Deallocate')
and Dstamp between trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-14-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
group by
reference_id,
sku_id,
line_id)
where oh.status not in ('Cancelled')
and oh.creation_date between trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-35-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
and oh.order_id=ol.order_id
and ol.client_id=oh.client_id
and REF=oh.order_id
and SKU=ol.sku_id
and LIN=ol.line_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 5
(select
nvl(cl.name,cl.client_id) A24,
oh.from_site_id B24,
sum(nvl(QQ,0)) C34,
sum(nvl(QA,0)) L34
from order_header oh,client cl,order_line ol,(select
reference_id REF,
sku_id SKU,
line_id LIN,
sum(DECODE(code,'Shipment',nvl(update_qty,0),'Unshipment',nvl(update_qty,0)*-1,0)) QQ,
sum(DECODE(code,'Allocate',nvl(update_qty,0),'Deallocate',nvl(update_qty,0)*-1,0)) QA
from inventory_transaction
where code in ('Shipment','Unshipment','Allocate','Deallocate')
and Dstamp between trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-7-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
group by
reference_id,
sku_id,
line_id)
where oh.status not in ('Cancelled')
and oh.creation_date between trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-35-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
and oh.order_id=ol.order_id
and ol.client_id=oh.client_id
and REF=oh.order_id
and SKU=ol.sku_id
and LIN=ol.line_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 6
(select
nvl(cl.name,cl.client_id) A25,
oh.from_site_id B25,
sum(nvl(QQ,0)) C35,
sum(nvl(QA,0)) L35
from order_header oh,client cl,order_line ol,(select
reference_id REF,
sku_id SKU,
line_id LIN,
sum(DECODE(code,'Shipment',nvl(update_qty,0),'Unshipment',nvl(update_qty,0)*-1,0)) QQ,
sum(DECODE(code,'Allocate',nvl(update_qty,0),'Deallocate',nvl(update_qty,0)*-1,0)) QA
from inventory_transaction
where code in ('Shipment','Unshipment','Allocate','Deallocate')
and Dstamp>trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
--and trunc(sysdate)-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
group by
reference_id,
sku_id,
line_id)
where oh.status not in ('Cancelled')
and oh.creation_date between trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and trunc(sysdate)-35-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
and oh.order_id=ol.order_id
and ol.client_id=oh.client_id
and REF=oh.order_id
and SKU=ol.sku_id
and LIN=ol.line_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--**************SHIPPED ORDERS********************************* 1
(select
nvl(cl.name,cl.client_id) A3,
oh.from_site_id B3,
count(distinct oh.order_id) D3
from order_header oh,client cl
where status='Shipped'
and oh.creation_date between trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-35-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.shipped_date between trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-35-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 2
(select
nvl(cl.name,cl.client_id) A31,
oh.from_site_id B31,
count(distinct oh.order_id) D31
from order_header oh,client cl
where status='Shipped'
and oh.creation_date between trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-35-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.shipped_date between trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-28-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 3
(select
nvl(cl.name,cl.client_id) A32,
oh.from_site_id B32,
count(distinct oh.order_id) D32
from order_header oh,client cl
where status='Shipped'
and oh.creation_date between trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-35-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.shipped_date between trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-21-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 4
(select
nvl(cl.name,cl.client_id) A33,
oh.from_site_id B33,
count(distinct oh.order_id) D33
from order_header oh,client cl
where status='Shipped'
and oh.creation_date between trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-35-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.shipped_date between trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-14-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 5
(select
nvl(cl.name,cl.client_id) A34,
oh.from_site_id B34,
count(distinct oh.order_id) D34
from order_header oh,client cl
where status='Shipped'
and oh.creation_date between trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-35-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.shipped_date between trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-7-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--*********************************************** 6
(select
nvl(cl.name,cl.client_id) A35,
oh.from_site_id B35,
count(distinct oh.order_id) D35
from order_header oh,client cl
where status='Shipped'
and oh.creation_date between trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-35-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.shipped_date>trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
--and trunc(sysdate)-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),
--****************OPEN ORDERS*******************************
(select
nvl(cl.name,cl.client_id) A36,
oh.from_site_id B36,
count(distinct oh.order_id) D36
from order_header oh,client cl
where status not in ('Shipped','Cancelled')
and oh.creation_date between trunc(sysdate)-42-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7) 
and trunc(sysdate)-35-mod((trunc(sysdate)-to_date('31-dec-2012','dd-mon-yyyy')),7)
and oh.client_id=cl.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id)
where AA=A2 (+)
and BB=B2 (+)
and AA=A20 (+)
and BB=B20 (+)
and AA=A21 (+)
and BB=B21 (+)
and AA=A22 (+)
and BB=B22 (+)
and AA=A23 (+)
and BB=B23 (+)
and AA=A24 (+)
and BB=B24 (+)
and AA=A25 (+)
and BB=B25 (+)
and AA=A3 (+)
and BB=B3 (+)
and AA=A31 (+)
and BB=B31 (+)
and AA=A32 (+)
and BB=B32 (+)
and AA=A33 (+)
and BB=B33 (+)
and AA=A34 (+)
and BB=B34 (+)
and AA=A35 (+)
and BB=B35 (+)
and AA=A36 (+)
and BB=B36 (+)
order by upper(BB),upper(AA))
union all
select 'Finish Date / Time : '||to_char(sysdate,'DD/MM/YYYY HH24:MI:SS') from DUAL
/

--spool off







