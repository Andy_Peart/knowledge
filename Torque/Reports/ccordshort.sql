SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 999
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON


--spool ccordshort.csv

/*                                    */
/* Detail version of the report       */
/*                                    */
/*
T1. order line and sku's jointed together
T2. All inventory summary no condition id
T2ALLINV. Added table to show all inventory summary by condition id for other tables to join
T3. Inventory that is in suspense
T4. Inventory that is in intake
T5. Inventory that is in ship dock
T6. Condition code 3 stock only but is not in ship dock, receive dock, suspense, stage
T7. Condition code 203 stock only but is not in ship dock, receive dock, suspense, stage
T8. Condition code 299 stock only but is not in ship dock, receive dock, suspense, stage
T9. Condition code any other stock only but is not in ship dock, receive dock, suspense, stage
T10. Shows locked stock that is not goods in either location locked or inventory locked
T11. Free Stock
*/
WITH T1 AS
  (SELECT ol.order_id,
    ol.sku_id,
    ol.condition_id,
    ol.QTY_ORDERED,
    NVL(ol.QTY_TASKED,0)                                                                  AS "QTY_TASKED",
    NVL(ol.QTY_PICKED,0)                                                                  AS qty_picked,
    NVL(ol.QTY_SHIPPED,0)                                                                 AS qty_shipped,
    ol.QTY_ORDERED- SUM( NVL(ol.QTY_TASKED,0)+NVL(ol.QTY_PICKED,0)+NVL(ol.QTY_SHIPPED,0)) AS shortfall,
    sku.description                                                                       AS "DESCRIPTION",
    SKU.PRODUCT_GROUP                                                                     AS "PRODUCT_GROUP",
    SKU.COLOUR                                                                            AS "COLOUR",
    sku.sku_size                                                                          AS "SIZE"
  FROM order_line ol
  INNER JOIN sku
  ON ol.sku_id       =sku.sku_id
  AND ol.client_id   =sku.client_id
  WHERE ol.client_id = 'CC'
  AND ((to_char(sysdate,'DAY') not like 'MONDAY%' and OL.CREATION_DATE BETWEEN TRUNC(SYSDATE) AND TRUNC(SYSDATE)+1)
  OR (to_char(sysdate,'DAY') like 'MONDAY%' and OL.CREATION_DATE BETWEEN TRUNC(SYSDATE)-2 AND TRUNC(SYSDATE)+1))
  GROUP BY ol.order_id,
    sku.sku_size,
    SKU.COLOUR,
    ol.QTY_PICKED,
    ol.QTY_ORDERED,
    ol.condition_id,
    sku.description,
    ol.QTY_TASKED,
    SKU.PRODUCT_GROUP,
    ol.sku_id,
    ol.QTY_SHIPPED
  HAVING ol.QTY_ORDERED- SUM( NVL(ol.QTY_TASKED,0)+NVL(ol.QTY_PICKED,0)+NVL(ol.QTY_SHIPPED,0)) > 0
  ),
  /* T2. All inventory Summary */
  T2 AS
  (SELECT inventory.SKU_ID,
    SUM(NVL(inventory.QTY_ON_HAND,0))   AS "QTY_ON_HAND",
    SUM(NVL(inventory.QTY_ALLOCATED,0)) AS "QTY_ALLOCATED"
  FROM inventory
  where inventory.client_id = 'CC'
  and inventory.location_id <> 'SUSPENSE'
	group by inventory.sku_id
  ORDER BY inventory.sku_id
  ),
	  /* T2. All inventory Summary */
	  T2ALLINV AS
  (SELECT inventory.SKU_ID,
    sum(nvl(inventory.qty_on_hand,0))   as "QTY_ON_HAND",
    sum(nvl(inventory.qty_allocated,0)) as "QTY_ALLOCATED",
		inventory.condition_id
  FROM inventory
  where inventory.client_id = 'CC'
	group by inventory.sku_id, inventory.condition_id
  order by inventory.sku_id
  ),
  /* T3. Inventory that is in suspense */
  T3 AS
  (SELECT CLIENT_ID,
    SKU_ID,
    CONDITION_ID,
    SUM(NVL(QTY_ON_HAND,0)) AS "SUSPENSE"
  FROM INVENTORY
  where client_id = 'CC'
  and (location_id = 'SUSPENSE'
	or (lock_status = 'Locked' and lock_code is null))
  GROUP BY CLIENT_ID,
    SKU_ID,
    CONDITION_ID
  ),
  /* T4. Inventory that is in intake */
  T4 AS
  (select inv.client_id,
    inv.sku_id,
    inv.condition_id,
    sum(nvl(inv.qty_on_hand,0)) as "INTAKE"
  from inventory inv
  	left join pre_advice_header pah
  on inv.receipt_id=pah.pre_advice_id
  and pah.client_id=inv.client_id
  	left join pre_advice_line pal
  on pah.pre_advice_id            =pal.pre_advice_id
  and pah.client_id               =pal.client_id
  and inv.sku_id                  =pal.sku_id
  where inv.client_id             = 'CC'
	and nvl(pah.user_def_type_7,0) <> 'MANUAL'
  and ((inv.lock_status           = 'Locked'
  and inv.location_id            <> 'SUSPENSE'
  and inv.lock_code 			 <> 'LOCK')
  or pal.user_def_type_6         is null
  and inv.location_id            <> 'SUSPENSE'
  and inv.lock_code 			 <> 'LOCK')
  group by inv.client_id,
    inv.sku_id,
    inv.condition_id	 
  union all
	select inv.client_id,
    inv.sku_id,
    inv.condition_id,
    sum(nvl(inv.qty_on_hand,0)) as "INTAKE"
  from inventory inv inner join location loc on inv.site_id = loc.site_id and inv.location_id = loc.location_id
	where inv.client_id='CC'
	and loc.loc_type = 'Receive Dock'
	  group by inv.client_id,
    inv.sku_id,
    inv.condition_id
  ),
  /* T5. Inventory that is in ship dock */
  T5 AS
  (SELECT INV.CLIENT_ID,
    INV.SKU_ID,
    INV.CONDITION_ID,
    SUM(NVL(INV.QTY_ON_HAND,0)) AS "SHIPDOCK"
  FROM INVENTORY INV
  INNER JOIN LOCATION LOC
  ON INV.LOCATION_ID  =LOC.LOCATION_ID
  AND LOC.SITE_ID     = 'WIGAN'
  WHERE INV.CLIENT_ID = 'CC'
  AND LOC.LOC_TYPE   IN ('ShipDock','Stage')
  GROUP BY INV.CLIENT_ID,
    INV.SKU_ID,
    INV.CONDITION_ID
  ),
  /* T6. Condition code 3 stock only but is not in ship dock, receive dock, suspense, stage */
  T6 AS
  (SELECT INV.CLIENT_ID,
    INV.SKU_ID,
    INV.CONDITION_ID,
    SUM(NVL(INV.QTY_ON_HAND,0)) AS "3"
  FROM INVENTORY INV
  INNER JOIN LOCATION LOC
  on inv.location_id    =loc.location_id
  AND LOC.SITE_ID       = inv.site_id
  WHERE INV.CLIENT_ID   = 'CC'
  AND LOC.LOC_TYPE = 'Tag-FIFO'
  AND INV.CONDITION_ID  = '3'
  GROUP BY INV.CLIENT_ID,
    INV.SKU_ID,
    INV.CONDITION_ID
  ),
  /* T7. Condition code 203 stock only but is not in ship dock, receive dock, suspense, stage */
  T7 AS
  (SELECT INV.CLIENT_ID,
    INV.SKU_ID,
    INV.CONDITION_ID,
    SUM(NVL(INV.QTY_ON_HAND,0)) AS "203"
  FROM INVENTORY INV
  INNER JOIN LOCATION LOC
  ON INV.LOCATION_ID    =LOC.LOCATION_ID
  AND LOC.SITE_ID       = 'WIGAN'
  WHERE INV.CLIENT_ID   = 'CC'
  AND LOC.LOC_TYPE = ('Tag-FIFO')
  AND INV.CONDITION_ID  = '203'
  GROUP BY INV.CLIENT_ID,
    INV.SKU_ID,
    INV.CONDITION_ID
  ),
  /* T8. Condition code 299 stock only but is not in ship dock, receive dock, suspense, stage */
  T8 AS
  (SELECT INV.CLIENT_ID,
    INV.SKU_ID,
    INV.CONDITION_ID,
    SUM(NVL(INV.QTY_ON_HAND,0)) AS "299"
  FROM INVENTORY INV
  INNER JOIN LOCATION LOC
  ON INV.LOCATION_ID    =LOC.LOCATION_ID
  AND LOC.SITE_ID       = 'WIGAN'
  where inv.client_id   = 'CC'
  AND LOC.LOC_TYPE = 'Tag-FIFO'
  AND INV.CONDITION_ID  = '299'
  GROUP BY INV.CLIENT_ID,
    INV.SKU_ID,
    INV.CONDITION_ID
  ),
  /* T9. Condition code any other stock only but is not in ship dock, receive dock, suspense, stage */
  T9 AS
  (SELECT INV.CLIENT_ID,
    INV.SKU_ID,
    INV.CONDITION_ID,
    SUM(NVL(INV.QTY_ON_HAND,0)) AS "Other"
  FROM INVENTORY INV
  INNER JOIN LOCATION LOC
  ON INV.LOCATION_ID        =LOC.LOCATION_ID
  AND LOC.SITE_ID           = 'WIGAN'
  where inv.client_id       = 'CC'
  AND LOC.LOC_TYPE = 'Tag-FIFO'
  AND INV.CONDITION_ID NOT IN ('3','203','299')
  GROUP BY INV.CLIENT_ID,
    INV.SKU_ID,
    INV.CONDITION_ID
  ),
	/* T10. Locked Stock but not goods in and not in suspense */
	T10 as
	(select inv.client_id,
    inv.sku_id,
    inv.condition_id,
    sum(nvl(inv.qty_on_hand,0))-sum(nvl(inv.qty_allocated,0)) as "Locked Stock", 
	loc.lock_status
	from inventory inv
  inner join location loc
  on inv.location_id    =loc.location_id
  and loc.site_id       = inv.site_id
  where inv.client_id   = 'CC'
  and loc.loc_type =  'Tag-FIFO'
  and loc.lock_status   = 'Locked'
  group by inv.client_id,
    inv.sku_id,
    inv.condition_id,
	loc.lock_status
union all
select inv.client_id,
    inv.sku_id,
    inv.condition_id,
    sum(nvl(inv.qty_on_hand,0))-sum(nvl(inv.qty_allocated,0)) as "Locked Stock",
	loc.lock_status
	from inventory inv
  inner join location loc
  on inv.location_id    =loc.location_id
  and loc.site_id       = inv.site_id
  where inv.client_id   = 'CC'
  and loc.loc_type =  'Tag-FIFO'
  and (inv.lock_status  = 'Locked' and inv.lock_code <> 'LOCK')
  group by inv.client_id,
    inv.sku_id,
    inv.condition_id,
	loc.lock_status),
  /* T11. Free Stock */
  T11 AS
  (SELECT INV.CLIENT_ID,
    INV.SKU_ID,
    INV.CONDITION_ID,
    SUM(NVL(INV.QTY_ON_HAND,0))-SUM(NVL(INV.QTY_ALLOCATED,0)) AS "Free Stock"
  FROM INVENTORY INV
  INNER JOIN LOCATION LOC
  on inv.location_id    =loc.location_id
  AND LOC.SITE_ID       = INV.SITE_ID
  WHERE INV.CLIENT_ID   = 'CC'
  and loc.loc_type =  'Tag-FIFO'
  AND LOC.LOCK_STATUS   = 'UnLocked'
  GROUP BY INV.CLIENT_ID,
    INV.SKU_ID,
    inv.condition_id
  )
SELECT T1.order_id,
  T1.sku_id,
  T1.condition_id,
  T1.QTY_ORDERED,
  T1.QTY_TASKED,
  T1.qty_picked,
  T1.qty_shipped,
  T1.SHORTFALL,
  T1.DESCRIPTION,
  T1.PRODUCT_GROUP,
  T1.COLOUR,
  NVL(T2.QTY_ON_HAND,0)   AS "Total QTY",
  NVL(T2.QTY_ALLOCATED,0) AS "Allocated",
  NVL(T3.SUSPENSE,0)      AS "Suspense",
  NVL(T4.INTAKE,0)        AS "Intake",
  NVL(T5.SHIPDOCK,0)      AS "Pick",
  NVL(T6."3",0)           AS "3 Stock",
  NVL(T7."203",0)         AS "203 Stock",
  NVL(T8."299",0)         AS "299 Stock",
  nvl(t9."Other",0)       as "Other Stock",
  NVL(T10."Locked Stock",0) AS "Locked Stock",
  NVL(T11."Free Stock",0) AS "Free Stock"
FROM T1
  /* T1. order line and sku's jointed together */
LEFT JOIN T2
  /* T2. All inventory  */
on t1.sku_id=t2.sku_id --AND T1.CONDITION_ID=T2.CONDITION_ID
/* T2ALLINV All inventory by condition table */
LEFT JOIN T2ALLINV ON T1.SKU_ID=T2ALLINV.SKU_ID AND T1.CONDITION_ID=T2ALLINV.CONDITION_ID
LEFT JOIN T3
  /* T3. Inventory that is in suspense  */
ON T2ALLINV.SKU_ID       =T3.SKU_ID
AND T2ALLINV.condition_id=t3.condition_id
LEFT JOIN T4
  /* T4. Inventory that is in intake  */
ON T2ALLINV.SKU_ID       =T4.SKU_ID
AND T2ALLINV.condition_id=t4.condition_id
LEFT JOIN T5
  /*  T5. Inventory that is in ship dock  */
ON T2ALLINV.SKU_ID       =T5.SKU_ID
AND T2ALLINV.condition_id=T5.condition_id
LEFT JOIN T6
  /* T6. Condition code 3 stock only but is not in ship dock, receive dock, suspense, stage */
ON T2.SKU_ID=T6.SKU_ID
LEFT JOIN T7
  /* T7. Condition code 203 stock only but is not in ship dock, receive dock, suspense, stage */
ON T2.SKU_ID=T7.SKU_ID
LEFT JOIN T8
  /* T8. Condition code 299 stock only but is not in ship dock, receive dock, suspense, stage */
ON T2.SKU_ID=T8.SKU_ID
LEFT JOIN T9
  /* T9. Condition code any other stock only but is not in ship dock, receive dock, suspense, stage  */
on t2allinv.sku_id       =t9.sku_id
left join t10
	/* T10. Locked Stock but not goods in and not in suspense */
on t2allinv.sku_id				=t10.sku_id
left join t11
  /* T11. Free Stock */
on t2allinv.sku_id       =t11.sku_id 
ORDER BY ORDER_ID
/


--spool off

