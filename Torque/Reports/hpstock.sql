SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 

column A heading 'Category' format A11
column B heading '&&2 Qty' format 9999999
column C heading 'VH Qty' format 9999999
column D heading 'ALL' format 9999999

set heading off
set pagesize 100

select 'Stock Holding Report for Hope on ',to_char(SYSDATE, 'DD/MM/YY    HH:MI:SS') from DUAL;

set heading on




select
'Stock' A,
sum(i.qty_on_hand) B
from inventory i
where i.location_id not in ('IN','IN2','IN3','QCHOLD','WP1','SUSPENSE','CATALOGUES')
and i.client_id = 'HP'
/

SET PAGESIZE 0

select
'Allocated' A,
sum(i.qty_allocated) B
from inventory i
where i.location_id not in ('IN','IN2','IN3','QCHOLD','WP1','SUSPENSE','CATALOGUES')
and i.client_id = 'HP'
/
select
'Available' A,
sum(i.qty_on_hand-i.qty_allocated) B
from inventory i
where i.location_id not in ('IN','IN2','IN3','QCHOLD','WP1','SUSPENSE','CATALOGUES')
and i.client_id = 'HP'
/
select 
'in SUSPENSE ' A,
nvl(sum(jc.qty_on_hand),0) B
from inventory jc
where jc.location_id='SUSPENSE'
and jc.client_id = 'HP'
/
select 'Rejected' A,
sum(nvl(i.qty_on_hand,0)-nvl(i.qty_allocated,0)) B
from inventory i
where i.location_id not in ('IN','IN2','IN3','QCHOLD','WP1','SUSPENSE','CATALOGUES')
and i.location_id = 'HPREJECT01'
and i.client_id = 'HP'
group by i.client_id
/
select
'Full Total' A,
sum(i.qty_on_hand) B
from inventory i
where i.client_id = 'HP'
/
