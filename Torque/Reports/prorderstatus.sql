SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET COLSEP ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 200
SET TRIMSPOOL ON
SET HEADING OFF 

column X heading 'Cust' format A8
column Z heading 'Status' format A12
column A heading 'Order' format A20
column A1 heading 'HB' format A4
column B heading 'Date1' format A12
column C heading 'Date2' format A12
column D heading 'Qty1' format 9999999
column E heading 'Qty2' format 9999999
column F heading 'Qty3' format 9999999


--spool igor.csv

select 'Prominent Order Status Report' from DUAL;
--select to_char(SYSDATE, 'DD/MM/YY    HH:MI:SS') from DUAL;

select 'Customer,Order ID,H/B,Status,Order Date,Ship By Date,Qty Ordered,Qty Tasked,Qty Picked' from DUAL;


break on C dup skip 1
compute sum label 'Total' of D E F on C


select
oh.name X,
oh.order_id A,
DECODE(oh.user_def_type_3,'H','Hang','B') A1,
oh.status Z,
trunc(oh.order_date) B,
trunc(oh.ship_by_date) C,
sum(ol.qty_ordered) D,
sum(nvl(ol.qty_tasked,0)) E,
sum(nvl(ol.qty_picked,0)) F
from order_header oh, order_line ol
where oh.client_id like 'PR'
and oh.ship_dock='PR'
and oh.ship_by_date>to_date('01-JAN-2011', 'DD-MON-YYYY')
and oh.status in ('Released','Allocated','In Progress','Complete')
and oh.client_id=ol.client_id
and oh.order_id=ol.order_id
group by
oh.name,
oh.order_id,
DECODE(oh.user_def_type_3,'H','Hang','B'),
oh.status,
trunc(oh.order_date),
trunc(oh.ship_by_date)
order by
C,X,A
/

--spool off
