SET FEEDBACK OFF                 
set verify off
set wrap off
set colsep ','

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 2000
set trimspool on

select to_char(imported,'yyyy/mm/dd HH24:MI:SS') date_in, Order_num, Sequence
from yo_total_labels tl join order_header oh on oh.order_id = tl.order_num 
where trunc(imported) between to_date('&&2','DD-MON-YYYY') and to_date('&&3','DD-MON-YYYY')
and sequence like '(J)JD00 022 410%' and oh.status <> 'Cancelled' and tl.deleted ='N' order by imported desc
/
