SET FEEDBACK OFF                 
set verify off
set wrap off
set colsep ','

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 4000
set trimspool on

column A heading 'User ID' format a18
column B heading 'Task Type' format a18
column XX heading 'WWW' format a12
column C heading 'Tasks' format 99999999
column D heading 'Units' format 99999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

--break on report 
--break on A dup 

--spool mwpicks.csv

select 'CODE,CLIENT_ID,SKU_ID,CONTAINER_ID,DSTAMP,WORK_GROUP,CONSIGNMENT,REFERENCE_ID,USER_ID,UPDATE_QTY,ORIGINAL_QTY,COMPLETE_DSTAMP' from DUAL;


select
CODE||','||
CLIENT_ID||','||
SKU_ID||','||
CONTAINER_ID||','||
DSTAMP||','||
WORK_GROUP||','||
CONSIGNMENT||','||
REFERENCE_ID||','||
USER_ID||','||
UPDATE_QTY||','||
ORIGINAL_QTY||','||
COMPLETE_DSTAMP
from inventory_transaction it
where it.client_id='MOUNTAIN'
and it.code='Pick'
and it.consignment like '%YDL%' 
and it.work_group<>'WWW'
and it.to_loc_id in ('DESPATCH')
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
/


--spool off
