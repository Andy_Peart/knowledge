SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 120

select 'MOUNTAIN Zero Quantity Ordered' from DUAL;
select ' ' from DUAL;
select 'Order ID,   Work Group,   Lines at Zero,  Total Lines on Order' from DUAL;  

column B heading 'Order ID' format a14
column A heading 'Work Group' format a12
column C heading 'No of Lines at Zero' format 99999
column D heading 'Total Lines on Order' format 99999

break on report

select oh.order_id B, oh.work_group A,count(ol.line_id) C, oh.num_lines D
from order_header oh, order_line ol
where oh.client_id = 'MOUNTAIN'
and ol.client_id = oh.client_id
and ol.order_id = oh.order_id
and trunc(oh.creation_date) > trunc(sysdate-2)
and ol.qty_ordered = 0
group by oh.work_group, oh.order_id, oh.num_lines having count(ol.line_id) > 20order by oh.work_group
/
