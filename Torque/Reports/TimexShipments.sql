SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON


column A heading 'Type' format a18
column A2 heading 'Reference' format a24
column A3 heading 'SKU' format a14
column A4 heading 'Date' format a14
column B heading 'Units' format 999999
column K heading 'Units' format 999999
column B1 heading 'Rate' format A6
column C heading 'Value' format 999999.99

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON




break on A dup skip 1 on A1 dup skip 1 on report


--compute sum label 'Sub Total' of B C on A2
compute sum label 'Summary' of B C on A1
compute sum label 'Totals' of B C on A
compute sum label 'Overall' of B C on report

spool timexshipments.csv

select 'Type,Reference,SKU,Date Shipped,Units,Value' from DUAL;

select
'Orders Shipped' A,
oh.order_id A2,
' ' A3,
to_char(oh.Shipped_date,'dd/mm/yy') A4,
1 B,
20 C
from order_header oh,order_line ol
where oh.client_id='TIMEX'
and oh.status='Shipped'
and to_char(shipped_date,'mm/yyyy')=to_char(sysdate-1,'mm/yyyy')
and oh.order_id=ol.order_id
and ol.sku_id not in (select
distinct sku_id
from inventory
where client_id='TIMEX'
and qty_on_hand>0)
union
select
'Shipments' A,
reference_id A2,
sku_id A3,
to_char(Dstamp,'dd/mm/yy') A4,
sum(update_qty) B,
sum(update_qty)*0.02 C
from inventory_transaction
where client_id='TIMEX'
and to_char(Dstamp,'mm/yyyy')=to_char(sysdate-1,'mm/yyyy')
and code='Shipment'
and sku_id not in (select
distinct sku_id
from inventory
where client_id='TIMEX'
and qty_on_hand>0)
group by
reference_id,
sku_id,
to_char(Dstamp,'dd/mm/yy')
union
select
A,
A2,
' ' A3,
A4,
B,
CASE WHEN (C<20 AND B>9) THEN 20
     WHEN (B<10) THEN 0
  	ELSE C
	END C
from (select
'Shipments (Part)' A,
reference_id A2,
to_char(Dstamp,'dd/mm/yy') A4,
sum(update_qty) B,
sum(update_qty)*0.14 C
from inventory_transaction
where client_id='TIMEX'
--and to_char(Dstamp,'mm/yyyy')=to_char(sysdate-1,'mm/yyyy')
and code='Shipment'
and sku_id in (select
distinct sku_id
from inventory
where client_id='TIMEX'
and qty_on_hand>0)
group by
reference_id,
to_char(Dstamp,'dd/mm/yy'))
order by
A,A2
/

spool off


