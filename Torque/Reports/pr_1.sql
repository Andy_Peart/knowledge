SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ' '



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 100

column A heading 'Docket (SKU)' format A14
column B heading 'Cartons Due' format A12
column BB heading 'Due' format 999999
column C1 heading 'Qty' format A20
column C2 heading 'Qty' format A20
column C3 heading 'Qty' format A20
column D heading 'Date Received' format A16

break on A skip 1

select 'Prominent - Pre Advice ID &&2' from DUAL;

select 
'Docket (SKU)' A,
'Cartons Due' B,
'   Qty' C1,
'   Qty' C2,
'   Qty' C3
from DUAL;
select ' ' from DUAL;
 

select
sku_id A,
qty_due BB,
'  ',
'|..............|' C1,
'|..............|' C2,
'|..............|' C3
from pre_advice_line
where client_id='PR'
and pre_advice_id like '&&2%'
order by A
/


select ' ' from DUAL;
select ' ' from DUAL;


select 'Additional Dockets / Exception Notes .....................................................' from DUAL;
select ' ' from DUAL;
select 'Additional Dockets / Exception Notes .....................................................' from DUAL;
select ' ' from DUAL;
select 'Additional Dockets / Exception Notes .....................................................' from DUAL;
select ' ' from DUAL;
select 'Additional Dockets / Exception Notes .....................................................' from DUAL;

select ' ' from DUAL;
select ' ' from DUAL;
select 'Unloaded by  |...........................|   |..............................|' from DUAL;
select '                        PRINT                         SIGN' from DUAL;
