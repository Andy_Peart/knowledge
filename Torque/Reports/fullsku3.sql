SET FEEDBACK OFF
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
set colsep ','
set heading off
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 120

/* select '&&2' from DUAL; */

--SET TERMOUT OFF
--spool ss.csv


select 'SKU,Season,Style,Quality,Size,Colour,Description,Product Group,' from dual;


select
Sku_id||','||
user_def_type_2||','||
user_def_type_1||','||
user_def_type_4||','||
sku_size||','||
colour||','||
description||','||
product_group||','||
' '
from sku
where client_id='&&2'
order by
sku_id
/

--spool off
--SET TERMOUT ON
