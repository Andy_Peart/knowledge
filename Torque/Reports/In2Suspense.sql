set pagesize 0
set linesize 120
set verify off
set feedback off

clear columns
clear breaks
clear computes

column F heading 'Tag ID' format A16
column A heading 'SKU' format a20
column B heading 'DESCRIPTION' format a40
column C heading 'Qty' format 999999
column D heading ' ' format a2
column E heading 'From' format a7

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

--SET TERMOUT OFF
--SPOOL INSUSPENSE.csv



select 'Key,Tag ID,Owner ID,Client ID,SKU,Description,Location,Site,Status,Qty,Receipt ID,Move Date,Move Time,' from DUAL;


--ttitle  CENTER 'Stock in SUSPENSE - for client ID &&2 as at ' report_date skip 2

--break on report

--compute sum label 'Total' of C on report

select
iv.key ||','||
iv.tag_id ||','||
iv.owner_id ||','||
iv.client_id ||','||
iv.sku_id ||','||
sku.description ||','||
iv.location_id ||','||
iv.Site_id ||','||
iv.lock_status ||','||
nvl(iv.qty_on_hand,0) ||','||
iv.receipt_id ||','||
to_char(iv.Move_dstamp,'DD/MM/YY') ||','||
to_char(iv.Move_dstamp,'HH:MI:SS') ||','||
'  ' 
from inventory iv,sku
where iv.client_id='&&2'
and iv.location_id='SUSPENSE'
and iv.sku_id=sku.sku_id
order by iv.sku_id
/



--SPOOL OFF
--SET TERMOUT ON



