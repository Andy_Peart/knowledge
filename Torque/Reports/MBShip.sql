SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON


select 'Date,Order,Total Units' from Dual
union all
select  A || ',' || B || ',' || C  from 
(
Select 
 to_char(Dstamp, 'DD-MON-YYYY') as "A"
, Reference_id as "B"
, sum(update_qty) as "C"
from inventory_transaction 
where client_id = 'MB'
AND dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and code = 'Shipment'
group by To_char(Dstamp, 'DD-MON-YYYY'), reference_id
order by A
)

/


