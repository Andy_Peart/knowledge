SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON


column A heading 'SKU' format a36
column B heading 'Date' format a12
column C heading 'Type' format a8
column H heading 'Description' format a36
column D heading 'Units' format 999999
column G heading 'Days' format 999
column F heading '   Rate' format a11
column E heading 'Charge' format 999999.9999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



break on report

--compute sum label 'Nett Charge for above SKU' of E on A
compute sum label 'Total Charge' of E on report

--spool denimstorage.csv

select 'SKU,Type,Date,Move,Units,Description,Days,Rate,Charge' from DUAL;


select
AAA A,
DECODE(ZZZ,'1','BULK','2','PICK','ZZZ') Z,
to_char(BBB,'DD/MM/YY') B,
CCC C,
DDD D,
HHH H,
trunc(sysdate)-1-BBB+DECODE(CCC,'OUT',0,1) G,
--' 0.00143' F,
--(trunc(sysdate)-1-BBB+DECODE(CCC,'OUT',0,1))*DDD*0.00143*DECODE(CCC,'OUT',-1,1) E
' 0.00' F,
(trunc(sysdate)-1-BBB+DECODE(CCC,'OUT',0,1))*DDD*0.00*DECODE(CCC,'OUT',-1,1) E
from (select
it.sku_id AAA,
L.zone_1 ZZZ,
trunc(it.Dstamp) BBB,
DECODE(it.code,'Receipt','IN','Pick','OUT','IN') CCC,
sum(it.update_qty) DDD,
sku.description HHH
from inventory_transaction it,sku,location L
where it.client_id='SP'
and it.station_id not like 'Auto%'
--and to_char(it.Dstamp,'mm/yyyy')=to_char(sysdate-2,'mm/yyyy')
and trunc(it.Dstamp)>=trunc(sysdate)-35
and trunc(it.Dstamp)<>trunc(sysdate)
and sku.client_id=it.client_id
and sku.sku_id=it.sku_id
and L.site_id='SP'
--and (it.code in ('Receipt','Shipment') or (it.code='Adjustment' and it.reason_id in ('RECRV','STK CHECK')))
--and it.from_loc_id=L.location_id
and ((it.code='Pick' and it.from_loc_id=L.location_id)
or (it.code='Receipt' and it.to_loc_id=L.location_id)
or (it.code='Adjustment' and it.reason_id in ('RECRV','STK CHECK','RET') and it.to_loc_id=L.location_id))
group by
it.sku_id,
L.zone_1,
trunc(it.Dstamp),
DECODE(it.code,'Receipt','IN','Pick','OUT','IN'),
sku.description)
where DDD<>0
union
select
the_sku A,
DECODE(the_zone,'1','BULK','2','PICK','ZZZ') Z,
to_char(the_date,'DD/MM/YY') B,
'B/FWD' C,
the_qty D,
sku.description H,
trunc(sysdate)-the_date G,
--' 0.00143' F,
--(trunc(sysdate)-the_date)*the_qty*0.00143 E
' 0.00' F,
(trunc(sysdate)-the_date)*the_qty*0.00 E
from SPORTA_snapshot,sku
--where the_date=to_date(('03'||to_char(sysdate-2,'-mon-yyyy')),'DD-MON-YYYY')
where the_date=trunc(sysdate)-35
and sku.client_id='SP'
and sku.sku_id=the_sku
order by A,B,C
/

--spool off
