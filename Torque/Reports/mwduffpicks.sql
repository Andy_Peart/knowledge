set feedback off
set pagesize 0
set newpage 0
set linesize 240
set verify off
set trimspool on

clear columns
clear breaks
clear computes

column A heading 'Location' format A16
column B heading 'SKU' format A16
column C heading 'Qty' format 999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


--ttitle  LEFT 'Units Picked for Order '&&2' - for client ID MOUNTAIN as at ' report_date skip 2


--spool mwduffpicks.csv

select 'Order Id,Sku Id,Qty Picked,Picked From,Qty in Retail,Where,Received,Picker' from DUAL;

select
reference_id||','||
sku_id||','||
update_qty||','||
from_loc_id||','||
nvl(QQQ,0)||','||
LLL||','||
DDD||','||
user_id E
from inventory_transaction,(select
sku_id SSS,
location_id LLL,
to_char(receipt_Dstamp,'DD/MM/YY HH:MI') DDD,
sum(qty_on_hand) QQQ
from inventory
where client_id='MOUNTAIN'
and zone_1='RETAIL'
group by
sku_id,location_id,
to_char(receipt_Dstamp,'DD/MM/YY HH:MI'))
where client_id='MOUNTAIN'
and code='Pick'
and from_loc_id like 'W%'
and reference_id like 'MOR%'
and trunc(Dstamp)=trunc(sysdate)
and sku_id=SSS (+)
order by reference_id,sku_id
/

--spool off
