SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON


column A heading 'Type' format a14
column A2 heading 'Reference' format a20
column A3 heading 'SKU' format a36
column A4 heading 'Date' format a14
column B heading 'Units' format 999999
column K heading 'Units' format 999999
column B1 heading 'Handling Rate' format A10
column C1 heading 'Handling Charge' format 999999.99
column B2 heading 'Sortation Rate' format A10
column C2 heading 'Sortation Charge' format 999999.99

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON




break on report

compute sum label 'Monthly Total' of B C1 C2 on report

--spool denimpicks.csv

select 'Type,Reference,Date Picked,Units Picked,Pick Rate,Pick Charge,Basic Charge' from DUAL;

select
'PICKS' A,
it.reference_id A2,
--it.sku_id A3,
to_char(it.Dstamp,'dd/mm/yy') A4,
sum(it.update_qty) B,
'0.30' B1,
sum(it.update_qty*0.30) C1,
5 C2
from inventory_transaction it
where it.client_id='SP'
--and to_char(it.Dstamp,'mm/yyyy')=to_char(sysdate-2,'mm/yyyy')
and trunc(it.Dstamp)>=trunc(sysdate)-28
and trunc(it.Dstamp)<>trunc(sysdate)
and it.code='Pick'
group by
it.reference_id,
to_char(it.Dstamp,'dd/mm/yy') 
order by
A4,A2
/


--spool off
