set pagesize 9999
set linesize 2000
set trimspool on
set colsep ','


select key,code,dstamp,client_id,sku_id,condition_id
from inventory_transaction
where code in ('Shipment','Adjustment','Condition Update','Inventory lock','Inventory UnLock')
and trunc(dstamp) = trunc(sysdate)
and client_id in('TR','T')
and (code<>'Shipment' or client_id <>'T' or customer_id like('T%'))
order by code desc
/

