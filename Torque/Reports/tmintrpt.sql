/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :     tmintrpt.sql	                         		      */
/*                                                                            */
/*     DESCRIPTION:                                                           */
/*     Interntational ship report created by Sylwester           			  */
/*                                                                            */
/*   DATE       BY   DESCRIPTION						                      */
/*   ========== ==== ===========					                          */
/*   30/07/2013 SYL   initial version     				                      */
/******************************************************************************/


SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','
SET WRAP OFF


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500

/* Column Headings and Formats */
COLUMN "CUSTOMER_ID" HEADING "Customer ID" FORMAT A12
COLUMN "Store" HEADING "Store" FORMAT A20
COLUMN "Qty_Picked" HEADING "Qty Picked" FORMAT 999999


break on report

--compute sum label 'Total' of TOTAL on report

set TERMOUT OFF
COLUMN CURDATE NEW_VALUE REPORT_DATE
select to_char(SYSDATE, 'DD/MM/YYYY  HH24:MI') curdate from DUAL;
SET TERMOUT ON
SELECT 'Totals for week: '|| to_char(to_date(sysdate),'ww') dates from DUAL;

--SPOOL tmintrpt_.CSV

/* Top Title */

select OH.CUSTOMER_ID as "CUSTOMER_ID", 
OH.name as "Store_Name", 
sum(OL.qty_picked) as "Qty_Picked"
from order_header OH inner join order_line OL on oh.order_id=ol.order_id and oh.client_id=ol.client_id
where 
( (  OH.order_id like 'F%' and OH.customer_id not in ('WH1', 'WH2')  )  or  OH.order_id like 'A%'  ) 
and OH.client_id = 'TR'
and OH.consignment like '%' || to_char(to_date(sysdate),'ww') || '%'
group by OH.CUSTOMER_ID, OH.name
/