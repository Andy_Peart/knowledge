SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','
SET WRAP OFF


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON



select 'Work Group, Order ID, SKU, From Loc, Description, Qrt, Date picked, Time picked, User' from dual
union all
select work_group || ',' || order_no  || ',' || sku_id  || ',' ||  from_loc || ',' || description  || ',' || qty || ',' || date_picked  || ',' || time_picked  || ',' || user_id
from (
select it.WORK_GROUP, it.reference_id order_no, it.sku_id, it.from_loc_id from_loc, sku.description, it.update_qty qty, to_char(it.dstamp, 'DD-MM-YYYY') date_picked,   to_char(it.dstamp, 'HH24:MI') time_picked, it.user_id
from inventory_transaction it inner join sku on it.sku_id = sku.sku_id
where it.client_id = 'MOUNTAIN'
and sku.client_id = 'MOUNTAIN'
and it.code = 'Pick'
and it.elapsed_time >0
and it.update_qty > 0
and it.work_group = '&&2'
and it.reference_id like 'MOR%'
and it.dstamp between to_date('&&3','DD-MON-YYYY') and to_date('&&4','DD-MON-YYYY')+1
order by it.dstamp, it.REFERENCE_ID
)
/