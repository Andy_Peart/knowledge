SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON

break on report
compute sum of Q on report

--spool stdinvbywk.csv

select 'Client Id &&2' from DUAL;
select 'Week,Sku,Description,Qty On Hand' from Dual;



select 
RECDATE,
sku_id,
description,
Q
from (select
to_char(receipt_dstamp,'YYYY WW') RECDATE,
sku_id,
description,
sum(qty_on_hand) Q
from inventory
where client_id='&&2'
--and receipt_dstamp<sysdate-70
group by
to_char(receipt_dstamp,'YYYY WW'),
sku_id,
description
order by 
RECDATE DESC,
sku_id)
where substr(RECDATE,1,1)='2'
/

--spool off

