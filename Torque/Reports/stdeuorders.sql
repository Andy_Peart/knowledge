SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET TRIMSPOOL ON
SET LINESIZE 550

select 'SiteID,ClientID,Country,OrderID,Lines,Units,Order Customer Name,Value,Dispatch Method,Inco Terms,Currency' A1 from dual
union all
select site_id || ',' || client_id || ',' || country || ',' || order_id || ',' || customer_name  || ',' ||  num_lines || ',' || units || ',' || order_value || ',' || dispatch_method || ',' || freight_terms || ',' || inv_currency
from (
select oh.from_site_id site_id, oh.client_id, initcap(replace(ct.tandata_id,'_',' ')) country,  oh.order_id, nvl(oh.contact, oh.name) customer_name, oh.num_lines, oh.order_volume*10000 units, nvl(oh.order_value,0) order_value, oh.dispatch_method,  oh.freight_terms, oh.inv_currency
from order_header oh inner join country ct on oh.country = ct.iso3_id
where nvl(oh.order_type,'X') = 'WEB'
and nvl(ct.ce_eu_type,'X') = 'EU'
and oh.shipped_date between to_date('&&2','DD-MON-YYYY') and to_date('&&3','DD-MON-YYYY')+1
order by 1,2,3
)
/