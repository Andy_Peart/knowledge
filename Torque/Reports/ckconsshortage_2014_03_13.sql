SET FEEDBACK OFF                 
set verify off
set wrap off
set colsep ','

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 300
set trimspool on


--spool ckshortage.csv

select 'Short Order Report for Consignment &&2' from DUAL
union all
Select 'Order Date,Order Id,Status,Sku Id,Description,Colour,Size,Qty Ordered,Qty Allocated,Shortage' from DUAL
union all
select
CID||','||
RID||','||
ST||','''||
SK||''','||
DS||','||
SC||','||
SZ||','||
Q1||','||
Q3||','||
Q2
from (select
CID,
RID,
ST,
SK,
DS,
SC,
SZ,
Q1,
Q1-Q2 Q3,
Q2
from (select
trunc(oh.order_date) CID,
oh.order_id RID,
oh.status ST,
ol.sku_id SK,
sku.description DS,
sku.colour SC,
sku.sku_size SZ,
sum(ol.qty_ordered) Q1,
nvl(QQQ,0) Q2
from order_header oh,order_line ol,(select
task_id TK,
sku_id SK2,
sum(qty_ordered-qty_tasked) QQQ
from generation_shortage
where client_id='CK'
group by task_id,sku_id),sku
where oh.client_id='CK'
and oh.consignment='&&2'
--and trunc(oh.creation_date)>trunc(sysdate)-3
and ol.order_id=oh.order_id
and ol.client_id=oh.client_id
and oh.order_id=TK
and ol.sku_id=SK2
and ol.sku_id=sku.sku_id
and sku.client_id='CK'
group by
trunc(oh.order_date),
oh.order_id,
oh.status,
ol.sku_id,
sku.description,
sku.colour,
sku.sku_size,
nvl(QQQ,0))
order by RID)
union all
select
SS||','||
Q1||','||
Q3||','||
Q2
from (select
'Summary,,,,,,' SS,
sum(Q1) Q1,
sum(Q1)-sum(Q2) Q3,
sum(Q2) Q2
from (select
trunc(oh.order_date) CID,
oh.order_id RID,
oh.status ST,
ol.sku_id SK,
sku.description DS,
sku.colour SC,
sku.sku_size SZ,
sum(ol.qty_ordered) Q1,
nvl(QQQ,0) Q2
from order_header oh,order_line ol,(select
task_id TK,
sku_id SK2,
sum(qty_ordered-qty_tasked) QQQ
from generation_shortage
where client_id='CK'
group by task_id,sku_id),sku
where oh.client_id='CK'
and oh.consignment='&&2'
--and trunc(oh.creation_date)>trunc(sysdate)-3
and ol.order_id=oh.order_id
and ol.client_id=oh.client_id
and oh.order_id=TK
and ol.sku_id=SK2
and ol.sku_id=sku.sku_id
and sku.client_id='CK'
group by
trunc(oh.order_date),
oh.order_id,
oh.status,
ol.sku_id,
sku.description,
sku.colour,
sku.sku_size,
nvl(QQQ,0)))
/


--spool off


