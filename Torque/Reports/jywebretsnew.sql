SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON

select 'Stream,Returned Date, Customer, Order, Shipped Date, Item, EAN, Description, Qty Returned, Value, Return Reason Code, Return Reason Notes' from dual;

select A1 || ',' || A || ',' || B || ',' || C || ',' || D || ',' || E || ',' || F || ',' || G || ',' || H || ',' || J || ',' || K || ',' || L from 
(
select  
case 
  when oh.ship_dock = 'JYWEB' then 'Joy'
  else oh.ship_dock end                         as A1
, to_char(it.dstamp, 'DD-MON-YYYY')             as A
, oh.name                                       as B
, it.reference_id                               as C
, to_char(oh.shipped_date, 'DD-MON-YYYY')       as D
, it.sku_id                                     as E
, sku.ean                                       as F
, sku.DESCRIPTION                               as G
, it.UPDATE_QTY                                 as H
, ltrim(it.reason_id, 'JY')                     as J
, nvl(r.notes, 'No Notes')                      as K
, case 
  when it.sampling_type = 'JYEX' then 'EXCHANGE'
  when it.sampling_type = 'JYGOOD' then 'GOOD RETURN'
  when it.sampling_type = 'JYBAD' then 'BAD RETURN'
  when it.sampling_type IS NULL then 'RETURN'
  else it.sampling_type end as L
from inventory_transaction it
Inner join order_header oh on it.client_id = oh.client_id and it.REFERENCE_ID = oh.order_id
Inner join order_line ol on oh.client_id = ol.client_id and oh.order_id = ol.order_id
Inner join sku on it.client_id = sku.client_id and it.sku_id = sku.sku_id
left join return_reason r on it.reason_id = r.REASON_ID
where it.client_id = 'JY'
AND it.dstamp between to_date('&&2', 'DD-MM-YYYY') and to_date('&&3', 'DD-MM-YYYY')+1
and it.code = 'Return'
group by 
case 
  when oh.ship_dock = 'JYWEB' then 'Joy'
  else oh.ship_dock end                         
, to_char(it.dstamp, 'DD-MON-YYYY')             
, oh.name                                       
, it.reference_id                               
, to_char(oh.shipped_date, 'DD-MON-YYYY')       
, it.sku_id                                     
, sku.ean                                     
, sku.DESCRIPTION                           
, it.UPDATE_QTY                               
, ltrim(it.reason_id, 'JY')                
, nvl(r.notes, 'No Notes')                      
, case 
  when it.sampling_type = 'JYEX' then 'EXCHANGE'
  when it.sampling_type = 'JYGOOD' then 'GOOD RETURN'
  when it.sampling_type = 'JYBAD' then 'BAD RETURN'
  when it.sampling_type IS NULL then 'RETURN'
  else it.sampling_type end 
 order by 1  
)
/
