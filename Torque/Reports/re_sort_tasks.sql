/*******************************************************
*  Description:	This Report Uses an Oracle PL/SQL	   *
*   			Function to Change The Sequence		   *
*				On Move Tasks, To Effect Pick Walk	   *
*													   *
*													   *
*													   *
********************************************************/
select TORQUE.JDA_PICKING.RE_SORT_TASKS('&&2','&&3','&&4') from dual
/