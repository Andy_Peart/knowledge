SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240


column C heading 'Order ID' format A8
column D heading 'Ord' format 999999
column E heading 'Tas' format 999999
column F heading 'Pic' format 999999


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



--ttitle  LEFT 'TML Retail - Duplicate Order Addresses ' RIGHT 'Printed ' report_date skip 2

select 'SKU ID,EAN' from DUAL;


--break on F skip 1

select 
sku_id ||','||
ean
from sku
where client_id='T'
order by sku_id
/
