SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON


select 'SKU','DESCRIPTION','QTY ON HAND', 'ALLOCATED', 'CONDITION','LOCATION' FROM DUAL
union all
SELECT i.sku_id,
sk.description,
TO_CHAR(SUM(i.qty_on_hand)),
TO_CHAR(SUM(i.qty_allocated)),
i.condition_id,
i.location_id
FROM inventory i
INNER JOIN sku sk ON i.sku_id = sk.sku_id AND sk.client_id = I.CLIENT_ID
WHERE i.client_id = 'DX'
GROUP BY
i.sku_id,
sk.description,
i.condition_id,
i.location_id
/
