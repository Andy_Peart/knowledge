SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
with inv as 
(
    select i.sku_id
    , sum(i.qty_on_hand) unitsFromParent
    from INVENTORY i
    where i.client_id = 'GG'
    group by i.sku_id
), kits as
(
    select kit_id
    , min(unitsFromKit) unitsFromKit
    from
    (
        select kl.kit_id
        , kl.sku_id
        , sum(kl.quantity * i.qty_on_hand) unitsFromKit
        from KIT_LINE kl
        inner join INVENTORY i on i.sku_id = kl.sku_id and i.client_id = kl.client_id
        where kl.client_id = 'GG'
        group by kl.kit_id
        , kl.sku_id
    )
    group by kit_id
), grouped as
(
    select sku_id
    , sum(unitsFromParent) unitsAvailable
    from
    (
        select sku_id
        , unitsFromParent
        from inv
        union all
        select kit_id
        , unitsFromKit
        from kits
    )
    group by sku_id
)
select 'Sku,Units Ordered,Units Available,Orders Requesting Sku,Kit Line Qtys' from dual
union all
select sku_id||','||qtyOrdered||','||unitsAvail||','||orders||','||kitSkus
from
(
    select x.sku_id
    , sum(x.qtyOrdered) qtyOrdered
    , x.unitsAvail
    , listagg(x.order_id,',')within group(order by x.order_id) orders
    ,(
        select listagg(kl.sku_id||'('|| floor(sum(i.qty_on_hand)/max(kl.quantity))||')',',')within group(order by kl.sku_id) qtyOnHand
        from KIT_LINE kl
        inner join INVENTORY i on i.sku_id = kl.sku_id and i.client_id = kl.client_id
        where kl.client_id = 'GG'
        and kl.kit_id = x.sku_id
        group by kl.sku_id
    ) kitSkus
    from
    (
        select oh.order_id
        , ol.sku_id
        , sum(ol.qty_ordered) qtyOrdered
        , max(unitsAvailable) unitsAvail
        from ORDER_HEADER oh
        inner join ORDER_LINE ol on ol.order_id = oh.order_id and ol.client_id = oh.client_id
        left join grouped on grouped.sku_id = ol.sku_id
        where oh.client_id = 'GG'
        and oh.status not in ('Shipped','Cancelled')
        group by oh.order_id,ol.sku_id
        having sum(ol.qty_ordered) > max(unitsAvailable)
    ) x
    group by x.sku_id
    , x.unitsAvail
)
/
--spool off