SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 260
Set TRIMSPOOL ON


--column A heading 'Order' format a300

--select 'Order No,Status' from DUAL;



Select
oh.Order_id||','||'DESPATCH'||','||to_char(shipped_date,'YYYYMMDDHH24MISS')
from order_header oh
where oh.client_id='MOUNTAIN'
and oh.status='Shipped'
and oh.shipped_date>sysdate-5
and (oh.user_def_type_3 ='LIFE' or oh.user_def_type_3 = 'PAR')
union
Select
distinct oh.Order_id||','||'ON HOLD'||','||to_char(shipped_date,'YYYYMMDDHH24MISS')
from order_header oh,order_line ol
where oh.client_id='MOUNTAIN'
and oh.order_id=ol.order_id
--and oh.status in ('Released','Allocated','In Progress')
and oh.status in ('Released')
and ol.qty_ordered<>nvl(ol.qty_tasked,0)
and (oh.user_def_type_3 ='LIFE' or oh.user_def_type_3 = 'PAR')
union
Select
distinct oh.Order_id||','||'PROCESSING'||','||to_char(shipped_date,'YYYYMMDDHH24MISS')
from order_header oh,order_line ol
where oh.client_id='MOUNTAIN'
and oh.order_id=ol.order_id
and oh.status in ('Allocated')
and ol.qty_ordered<>nvl(ol.qty_tasked,0)
and (oh.user_def_type_3 ='LIFE' or oh.user_def_type_3 = 'PAR')
union
Select
distinct oh.Order_id||','||'PROCESSING'||','||to_char(shipped_date,'YYYYMMDDHH24MISS')
from order_header oh,order_line ol
where oh.client_id='MOUNTAIN'
and oh.order_id=ol.order_id
and oh.status in ('Complete')
and ol.qty_ordered=nvl(ol.qty_picked,0)
and (oh.user_def_type_3 ='LIFE' or oh.user_def_type_3 = 'PAR')
union
Select
distinct oh.Order_id||','||'PROCESSING'||','||to_char(shipped_date,'YYYYMMDDHH24MISS')
from order_header oh,order_line ol
where oh.client_id='MOUNTAIN'
and oh.order_id=ol.order_id
and oh.status in ('In Progress')
and (ol.qty_ordered<>nvl(ol.qty_tasked,0)
or ol.qty_ordered=nvl(ol.qty_picked,0)+nvl(ol.qty_tasked,0))
and (oh.user_def_type_3 ='LIFE' or oh.user_def_type_3 = 'PAR')
order by 1
/


