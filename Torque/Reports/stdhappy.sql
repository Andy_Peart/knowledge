SET FEEDBACK OFF                 
set verify off
set wrap off
set colsep ' '

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 300
set trimspool on
set heading off

select 'Client '||'&&2' from DUAL;
select
'Number of SKUSs = '|| count(distinct sku_id) 
from inventory
where client_id='&&2'
/
select
'Number of Locations Used = '|| count(distinct location_id) 
from inventory
where client_id='&&2'
/
select
'Total Stock on Hand = '|| sum(qty_on_hand) 
from inventory
where client_id='&&2'
/
select
'Currently Allocated = '||sum(qty_allocated) 
from inventory
where client_id='&&2'
/


