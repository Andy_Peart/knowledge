SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Receipt Date,Sku id,Description,Units,To Location,Condition,User id' from dual
union all
select "Date" ||','|| sku_id ||','|| description ||','|| update_qty ||','|| to_loc_id ||','|| condition_id ||','|| user_id
from
(
select trunc(it.dstamp) as "Date"
, it.sku_id
, s.description
, it.update_qty
, it.to_loc_id
, it.condition_id
, it.user_id
from inventory_transaction it
inner join sku s on s.client_id = it.client_id and s.sku_id = it.sku_id
where it.client_id = 'HS'
and it.code in ('Receipt', 'Return')
and it.condition_id = 'REFURB'
and it.dstamp between to_date('&&2') and to_date('&&3')+1
order by trunc(it.dstamp)
, it.sku_id
)
/
--spool off