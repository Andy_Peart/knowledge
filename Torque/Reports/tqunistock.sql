SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Sku,Description,Units Available,Unit Cost,Total Value' from dual
union all
select Sku ||','|| Description ||','|| Units_Available ||','|| Cost_Value ||','|| Units_Available * Cost_Value
from
(
    select i.sku_id Sku 
    , s.description Description
    , sum(i.qty_on_hand) Units_Available
    , s.user_def_type_5 Cost_Value
    from inventory i
    inner join sku s on s.client_id = i.client_id and s.sku_id = i.sku_id
    where i.client_id = 'TQ'
    group by i.sku_id
    , s.description
    , s.user_def_type_5
)
/
--spool off