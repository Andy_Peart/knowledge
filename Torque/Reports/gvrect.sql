SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON


COLUMN NEWSEQ NEW_VALUE SEQNO
COLUMN FOLD NEW_VALUE FOLDER

update inventory_transaction
set uploaded='T'
where client_id = 'GV'
and code = 'Receipt'
and uploaded = 'N'
and from_loc_id='RETURN'
and reference_id like '%T%'
and reference_id in (select
pre_advice_id from pre_advice_header
where client_id='GV'
and status='Complete')
/



select
DECODE(count(*),0,'TEMP/','LIVE/') FOLD
from inventory_transaction it
where it.client_id = 'GV'
and it.code = 'Receipt'
and it.uploaded = 'T'
and it.from_loc_id='RETURN'
/

select 'S://redprairie/apps/home/dcsdba/reports/'||'&FOLDER'||'GIVERT'||substr(to_char(10000000+next_seq),2,7)||'.csv' NEWSEQ from sequence_file
--select '&FOLDER'||'GIVERT'||substr(to_char(10000000+next_seq),2,7)||'.csv' NEWSEQ from sequence_file
where control_id=1
/


set termout off
spool &SEQNO


select 'ITL,E,Receipt'||','
||decode(sign(A1), 1, '+', -1, '-', '+')||','
--||A1||','
||''||','
||decode(sign(A2), 1, '+', -1, '-', '+')||','
||A2||','
||AA||'000000,'
||'GV'||','
||SS||','
||'RETURN'||','
||'RETURN'||','
||''||','
||''||','
||RR||','
||LI||','
||'GOOD'||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||'GIVE'||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||SUP||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||U8||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||',,'
from (select
to_char(it.dstamp, 'YYYYMMDD') AA,
it.sku_id SS,
DECODE(nvl(pl.user_def_num_2,0),0,it.line_id,pl.user_def_num_2) LI,
it.reference_id RR,
it.supplier_id SUP,
it.user_def_type_8 U8,
sum(it.original_qty) A1,
sum(it.update_qty) A2
from inventory_transaction it,pre_advice_line pl
where it.client_id = 'GV'
and it.code = 'Receipt'
and it.uploaded = 'T'
and it.from_loc_id='RETURN'
and pl.client_id='GV'
and it.reference_id=pl.pre_advice_id
and it.line_id=pl.line_id
and '&FOLDER' like 'LIVE%'
group by
to_char(it.dstamp, 'YYYYMMDD'),
it.sku_id,
DECODE(nvl(pl.user_def_num_2,0),0,it.line_id,pl.user_def_num_2),
it.reference_id,
it.supplier_id,
it.user_def_type_8)
order by
RR,SS
/




spool off
set termout on
set feedback on



update inventory_transaction
set uploaded='Y'
where client_id = 'GV'
and code = 'Receipt'
and uploaded = 'T'
and from_loc_id='RETURN'
and '&FOLDER' like 'LIVE%'
/



Update sequence_file
set next_seq=next_seq+1
where control_id=1 and '&FOLDER' like 'LIVE%'
/

commit;
