set pagesize 68
set linesize 120
set verify off

ttitle '&&4 Warehouse Despatch Quantities &&2 to &&3' skip 2

column A heading 'Date' format a8
column B heading 'Retail' format 99999999
column C heading 'WWW' format 99999999
column D heading 'Total' format 99999999

break on report

compute sum label 'Total' of B C D on report


select 
to_char (A1, 'DD/MM/YY') A,
nvl(B1,0)-nvl(B2,0) B,
nvl(B2,0) C,
nvl(B1,0) D
from 
(select
trunc(it.dstamp) A1,
sum (it.update_qty) B1
from inventory_transaction it,sku
where it.client_id = '&&4'
and it.code = 'Shipment'
--and it.work_group='WWW'
and it.sku_id=sku.sku_id
and sku.client_id='&&4'
and nvl(sku.product_group,'  ')<>'NS'
and it.dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1
group by trunc(it.dstamp)),
(select
trunc(it.dstamp) A2,
sum (it.update_qty) B2
from inventory_transaction it,sku
where it.client_id = '&&4'
and it.code = 'Shipment'
and it.work_group='WWW'
and it.sku_id=sku.sku_id
and sku.client_id='&&4'
and nvl(sku.product_group,'  ')<>'NS'
and it.dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1
group by trunc(it.dstamp))
where A1=A2(+)
order by A1
/

