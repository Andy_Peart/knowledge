SET FEEDBACK OFF           
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 600
SET TRIMSPOOL ON

--spool pbd.csv

select 'Pick Seq,Order,Docket,Sku,Line,Qty Tasked,Location' from Dual;


select
lo.pick_sequence,
oh.order_id,
ol.user_def_type_4,
ol.sku_id||'''',
ol.line_id,
mt.qty_to_move,
mt.from_loc_id
from order_header oh,order_line ol,move_task mt,location lo
where oh.client_id='PR'
and oh.customer_id='TML'
and oh.order_id='&&2'
and oh.order_id=ol.order_id
and ol.client_id='PR'
and mt.task_id=oh.order_id
and ol.line_id=mt.line_id
and nvl(ol.qty_tasked,0)>0
and mt.from_loc_id=lo.location_id
and lo.site_id='PROM'
order by 
lo.pick_sequence,
ol.line_id
/

--spool off
