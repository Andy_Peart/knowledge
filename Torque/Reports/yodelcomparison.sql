SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON




set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

--break on A

--compute sum of CT on A

--spool yodelcomparison.csv

select 'Picks from &&2 to &&3 with wrong labels' from DUAL;

select 'Date,Order ID,Container,Work Group,Label Code,Picker' from DUAL;


select
distinct
K,
R,
A,
B,
E,
U
from (select
distinct
trunc(it.Dstamp) K,
it.container_id A,
it.reference_id R,
it.user_id U,
it.work_group B,
postcode C,
labnum D,
labid E
from inventory_transaction it,address ad,(select
'J'||substr(sequence,4,4)||replace(substr(sequence,9,20),' ','') as labnum,
address_id labid
from yo_total_labels)
where it.client_id='MOUNTAIN'
and it.work_group<>'WWW'
and it.code='Pick'
--and it.container_id like 'JJD%'
and it.to_loc_id='CONTAINER'
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.container_id=labnum
--and it.container_id='JJD0002240183173919'
--and it.work_group=labid
and it.work_group=ad.address_id (+))
where B<>E
order by
A
/

--spool off
