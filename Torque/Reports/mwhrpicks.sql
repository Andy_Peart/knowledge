SET FEEDBACK OFF
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240
SET TRIMSPOOL ON

--SET TERMOUT OFF

--break on A skip 1


--spool mwhrpicks.csv

select 'Period &&2 to &&3' from DUAL;

select 'User Id,Hour,Qty Picked,Mins Worked,KPI' from DUAL;

select
A,
B,
F,
--to_char(E/60,'99990') E,
CASE
WHEN B=11 THEN 30
ELSE 60 END E,
CASE
WHEN B=11 THEN to_char(F/30,'99990.99')
ELSE to_char(F/60,'99990.99') END H
from (select
it.user_id A,
to_char(it.Dstamp,'HH24') B,
sum(it.elapsed_time) E,
sum(it.update_qty) F,
count(*) G
from inventory_transaction it
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='MOUNTAIN'
and it.update_qty<>0
and it.reference_id like 'MOR%'
and it.station_id not like 'Auto%'
and it.code='Pick'
and it.elapsed_time>0
group by it.user_id,to_char(it.Dstamp,'HH24')
order by it.user_id,to_char(it.Dstamp,'HH24'))
/

--spool off
