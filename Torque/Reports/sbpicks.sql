SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 66
SET NEWPAGE 0
SET LINESIZE 60

TTitle 'SB Pick Totals from &&2 to &&3' skip 2

column A heading 'Store' format A12
column CC heading 'Picks' format 999999
column DD heading 'Units' format 999999

select
customer_id A,
count(*) CC,
nvl(sum(update_qty),0) DD
from inventory_transaction
where client_id='SB'
and code='Pick'
and from_loc_id<>'CONTAINER'
and Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and customer_id like'&&4%'
group by
customer_id
order by
customer_id
/

