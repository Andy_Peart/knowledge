SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 2000
SET TRIMSPOOL ON


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


select 'Adjustments for Week Commencing '||to_char(SYSDATE-4, 'DD/MM/YYYY') from DUAL;

select 'Qty,Product,Description,From Zone,To Zone' from DUAL;

select
sum(it.update_qty)||','||
sku.user_def_note_1||','||
sku.description
from inventory_transaction it,sku
where it.client_id='SH'
and it.code like 'Adjustment'
and trunc(it.Dstamp)>trunc(sysdate)-7
and it.sku_id=sku.sku_id
and sku.client_id='SH'
group by
sku.user_def_note_1,
sku.description
order by
sku.user_def_note_1,
sku.description
/

