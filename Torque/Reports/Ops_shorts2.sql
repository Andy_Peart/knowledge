SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

column A heading 'SKU' format A18
column B heading 'Qty' format 99999
column C heading 'Order' format A10
column D heading 'SOI' format A32
column E heading 'Cust ID' format A12
column F heading 'Order Date' format A12
column G heading 'Ship by Date' format A12
column H heading 'PO' format A12
column J heading 'Cust SKU' format A12

--set termout off
--spool OPS-Test.csv

select 'SKU,Qty,Order Number,Special Order Instructions,Customer_Id,Order_Date,Ship_By_Date,Purchase_Order,Customer_Sku_Id' from DUAL;


select
DD ||','||
CC ||','||
AA ||','||
EE ||','||
FF ||','||
GG ||','||
HH ||','||
JJ ||','||
KK ||','||
' '
from (select
oh.order_id as AA,
ol.sku_id as DD,
oh.Instructions EE,
oh.customer_id FF,
to_char(oh.order_date,'DD-MON-YY') GG,
to_char(oh.ship_by_date,'DD-MON-YY') HH,
oh.purchase_order JJ,
'' KK,
nvl(ol.qty_ordered,0)-nvl(ol.qty_shipped,0) as CC
from order_header oh,order_line ol
where oh.client_id='OPS'
and oh.order_id=ol.order_id
and oh.status='Shipped'
and trunc(oh.shipped_date) between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1)
where CC>0
order by AA
/


--spool off
--SET TERMOUT ON
