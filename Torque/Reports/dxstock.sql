SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
set colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 

column A heading 'Location' format A14
column F heading 'Condition' format A14
column B heading 'In Hand' format 999999999
column C heading 'Allocated' format 999999999
column D heading 'Available' format 999999999

set heading on
set pagesize 0
set linesize 100

select 'Stock Holding Report for DX on ',to_char(SYSDATE, 'DD/MM/YY    HH:MI:SS') from DUAL;

select '' from DUAL;

select 'Location       Condition         In Hand  Allocated  Available' from DUAL;

select
A,
F,
B,
C,
DECODE(A,'GOOD',D,0) D
from (select
DECODE(location_id,'DXBIN','Receiving','DXBOUT','Ship Dock','DXBHOLD','QC Hold','DXBREJ','Rejects','DXBREJ1','DXBREJ1','DXBRET','Returns','GOOD') A,
i.condition_id F,
nvl(sum(i.qty_on_hand),0) B,
nvl(sum(i.qty_allocated),0) C,
nvl(sum(i.qty_on_hand-i.qty_allocated),0) D
from inventory i
where i.client_id like 'DX'
--and i.condition_id in ('GOOD')
group by
DECODE(location_id,'DXBIN','Receiving','DXBOUT','Ship Dock','DXBHOLD','QC Hold','DXBREJ','Rejects','DXBREJ1','DXBREJ1','DXBRET','Returns','GOOD'),
i.condition_id)
order by
A
/

