SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 120

column A heading 'Cust' format a40
column AA heading 'Grp' format a6
column B heading 'Type' format a12
column BB heading 'Type' format a15
column C heading 'Qty' format 999999


break on A skip 2 on B dup on report

compute sum label 'Total' of C on A
compute sum label 'Total' of C on B
compute sum label 'Total' of C on report


--set termout off
--spool TR060.csv

select 'TR60 - Shipped - '||to_char(sysdate-1,'DD/MM/YYYY') from Dual;

select 'Shop,Order Type,Order Id,Qty' from DUAL;

select
sm.CUSTOMER_ID||' - '||oh.name A,
nvl(oh.order_type,'Blank') B,
sm.order_id BB,
nvl(sum(sm.qty_shipped),0) C
FROM  shipping_manifest sm,order_header oh,sku
WHERE  sm.CLIENT_ID='TR'
AND sm.CLIENT_ID=oh.CLIENT_ID
AND sm.CUSTOMER_ID=oh.CUSTOMER_ID
AND sm.CONSIGNMENT=oh.CONSIGNMENT
AND sm.ORDER_ID=oh.ORDER_ID
AND sm.CLIENT_ID=SKU.CLIENT_ID
and sm.sku_id=sku.sku_id
and trunc(sm.SHIPPED_DSTAMP)=trunc(sysdate-1)
--and trunc(sm.SHIPPED_DSTAMP)<trunc(sysdate)
group by
sm.customer_id,
oh.name,
oh.ORDER_TYPE,
sm.order_id
order by
sm.customer_id,
oh.name,
oh.ORDER_TYPE,
sm.order_id
/




--spool off
--set termout on

