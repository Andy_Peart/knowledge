SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9999
SET TRIMSPOOL ON

--set feedback on

update inventory_transaction
set user_def_chk_2='Z'
where client_id = 'DX'
--and trunc(Dstamp)<>trunc(sysdate)
and code = 'Receipt'
and (user_def_chk_2 = 'N'
or user_def_chk_2 is null)
and to_loc_id in ('DXBRET','DXBREJ')
and user_id<>'Receive'
and nvl(reference_id,'NULL') not like 'PO%'
and nvl(reference_id,'NULL') not in (select
nvl(order_id,'NOTHING')
from order_header
where client_id='DX')
and nvl(reference_id,'NULL') not in (select
nvl(instructions,'NOTHING')
from order_header
where client_id='DX')
and reference_id not like 'QVC%'
and reference_id not like 'SI%'
and reference_id not like 'PK%'
and reference_id not in
('BARLO',
'BOB',
'BULL',
'CITY',
'DING',
'FENN',
'HHNOT',
'HHSHE',
'MAIER',
'METRO',
'MILL',
'NI',
'PLUS',
'RACK',
'SALON',
'SELF',
'TRAF',
'TS')
/



set feedback off

--spool dxrep8blank.csv

select 'Returns_Report - '||to_char(SYSDATE, 'DD/MM/YY  HH24:MI:SS') from DUAL;

select 'Date,Order Type,Rev.Stream,Notes,Order ID,Customer,Product,Description,Condition ID,Location ID,Qty,Reason,User ID' from DUAL;

select
trunc(i.Dstamp)||','||
'UNKNOWN'||','||
' '||','||
i.user_def_note_2||','||
i.reference_id||','||
' '||','||
i.sku_id||','||
sku.description||','||
i.condition_id||','||
i.to_loc_id||','||
i.update_qty||','||
i.user_def_type_1||','||
i.user_id
from inventory_transaction i,sku
where i.client_id='DX'
and i.code = 'Receipt'
and i.user_def_chk_2 = 'Z'
and i.to_loc_id in ('DXBRET','DXBREJ')
--and trunc(i.Dstamp)>trunc(sysdate-7)
and sku.client_id = 'DX'
and i.sku_id=sku.sku_id
order by
trunc(i.Dstamp),
i.sku_id
/

--spool off

--set feedback on


update inventory_transaction
set user_def_chk_2='Y'
where client_id = 'DX'
--and trunc(Dstamp)>trunc(sysdate-7)
and code = 'Receipt'
and user_def_chk_2 = 'Z'
/


--rollback;
commit;

 
