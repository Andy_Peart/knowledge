-- ================================================
-- TORQUE - (IT) - Add Users in Bulk
-- ================================================
set serveroutput on;
set verify off;
set feedback off;

-- set proper names for passed in variables
define copyuser="&&2"
define newusers="&&3"
define namenewuser="&&4"
define password="&&5"
define passtochange="&&6"

declare    
    v_Users number(2):= 0;
begin
    -- call stored procedure to perform update
        TORQUE.TQ_REPORTS.TQ_BULK_CREATE_USERS
        (
            op_UsersCreated => v_Users,
            p_UserToCopy => '&&copyuser', 
            p_NewUsers => '&&newusers', 
            p_NewName => '&&namenewuser',
            p_Password => '&&password',
			p_PassShouldChange => '&&passtochange'
        );

    -- quick check on rowcount to ensure correct grammar
        if v_Users = 1 then
            DBMS_OUTPUT.PUT_LINE(v_Users||' user created.');
        else
            DBMS_OUTPUT.PUT_LINE(v_Users||' users created.');
        end if;
end;
/