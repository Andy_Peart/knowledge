SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','
SET UND OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 320
SET TRIMSPOOL ON


column A1 format A18
column A format A28
column B format A40
column Z format A12
column C format 9999999
column D format 9999999
column E format 9999999
column F format 9999999
column G format 9999999
column H format 9999999
column J format 9999999
column K format 9999999
column L format 9999999
column N format 9999999
column P format 9999999
column Q format 9999999
column V format 9999999
column W format 9999999


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


COLUMN NEWSEQ NEW_VALUE SEQNO


select 'S://redprairie/apps/home/dcsdba/reports/LIVE/DX.csv' NEWSEQ from DUAL;
--select 'LIVE/DX.csv' NEWSEQ from DUAL;

--set termout off


--break on report

--compute sum label 'Total'  of C D E J K G N P Q F V W on report

set termout on

spool &SEQNO append

--select ',Elite W,Elite QUAR,Elite FATY,Elite DAMG,Elite QVC,Elite IDEAL,Elite Suspense,Elite Transit,Elite Total' from DUAL;

select trunc(sysdate) from DUAL;

select
'Opening Stock' A,
CCC-QUAR-FATY-DAMG-QVC-IDEAL-SUSP-TRT C,
QUAR D,
FATY E,
DAMG F,
QVC H,
IDEAL J,
SUSP K,
TRT L,
CCC N
from (Select
sum(qty_on_hand) CCC
from inventory
where client_id='DX'),(Select
sum(qty_on_hand) QUAR
from inventory
where client_id='DX'
and location_id in ('DXBREJ1')
and condition_id='QUAR'),(Select
sum(qty_on_hand) FATY
from inventory
where client_id='DX'
and location_id in ('DXBREJ1')
and condition_id='FATY'),(Select
NVL(sum(qty_on_hand),0) DAMG
from inventory
where client_id='DX'
and location_id in ('DXBREJ1')
and condition_id='DMGD'),(Select
nvl(sum(qty_on_hand),0) QVC
from inventory
where client_id='DX'
and zone_1 in ('QVC')),(Select
nvl(sum(qty_on_hand),0) IDEAL
from inventory
where client_id='DX'
and zone_1 in ('IDEAL')),(Select
nvl(sum(qty_on_hand),0) SUSP
from inventory
where client_id='DX'
and location_id='SUSPENSE'),(Select
nvl(sum(qty_on_hand),0) TRT
from inventory
where client_id='DX'
and location_id in  ('DXBRET','DXBREJ'))


--select '  ' from DUAL;

Select
'Shipments - '||
DECODE(DECODE(oh.order_type,'HOME',oh.order_type||substr(oh.user_def_type_1,3,2),oh.order_type),'HOMEHH','Hot Hair','HOMENI','Natural Image',
'HOMEPY','Paula Young','WHOLESALEB','WHOLESALE',DECODE(oh.order_type,'HOME',oh.order_type||substr(oh.user_def_type_1,3,2),oh.order_type)) A,
0-sum(update_qty) D
from inventory_transaction it,order_header oh
where it.client_id='DX'
and it.code='Shipment'
and trunc(it.Dstamp)=trunc(sysdate)-1
and oh.client_id='DX'
and it.reference_id=oh.order_id
group by
DECODE(DECODE(oh.order_type,'HOME',oh.order_type||substr(oh.user_def_type_1,3,2),oh.order_type),'HOMEHH','Hot Hair','HOMENI','Natural Image',
'HOMEPY','Paula Young','WHOLESALEB','WHOLESALE',DECODE(oh.order_type,'HOME',oh.order_type||substr(oh.user_def_type_1,3,2),oh.order_type))
order by A
/

Select
'UnShipments' A,
nvl(sum(update_qty),0) D
from inventory_transaction it
where it.client_id='DX'
and it.code='UnShipment'
and trunc(it.Dstamp)=trunc(sysdate)-1
/

select
'Returns' A,
sum(i.update_qty) D
from inventory_transaction i
where i.client_id='DX'
and i.code = 'Receipt'
and i.to_loc_id in ('DXBRET','DXBREJ')
and trunc(i.Dstamp)=trunc(sysdate)-1
/
 
select
'Movements' A,
CCC-QUAR-FATY-DAMG-QVC-IDEAL-SUSP-TRT C,
QUAR D,
FATY E,
DAMG F,
QVC H,
IDEAL J,
SUSP K,
TRT L,
CCC N
from (Select
nvl(sum(update_qty),0) CCC
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)-1
and code='Relocate'),(Select
nvl(sum(update_qty),0) QUAR
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)-1
and final_loc_id in ('DXBREJ1')
and condition_id='QUAR'
and code='Relocate'),(Select
nvl(sum(update_qty),0) FATY
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)-1
and final_loc_id in ('DXBREJ1')
and condition_id='FATY'
and code='Relocate'),(Select
NVL(sum(update_qty),0) DAMG
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)-1
and final_loc_id in ('DXBREJ1')
and condition_id='DMGD'
and code='Relocate'),(Select
nvl(sum(update_qty),0) QVC
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)-1
and condition_id in ('QVC')
and code='Relocate'),(Select
nvl(sum(update_qty),0) IDEAL
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)-1
and condition_id in ('IDEAL')
and code='Relocate'),(Select
nvl(sum(update_qty),0) SUSP
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)-1
and from_loc_id='SUSPENSE'
and code='Relocate'),(Select
nvl(sum(update_qty),0) TRT
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)-1
and final_loc_id in  ('DXBRET','DXBREJ')
and code='Relocate')
/


select
'Receipts' A,
CCC-QUAR-FATY-DAMG-QVC-IDEAL-SUSP-TRT C,
QUAR D,
FATY E,
DAMG F,
QVC H,
IDEAL J,
SUSP K,
TRT L,
CCC N
from (Select
nvl(sum(update_qty),0) CCC
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)-1
and code='Receipt'
and reference_id in (select
pre_advice_id from pre_advice_header
where client_id='DX')),(Select
nvl(sum(update_qty),0) QUAR
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)-1
and final_loc_id in ('DXBREJ1')
and condition_id='QUAR'
and code='Receipt'),(Select
nvl(sum(update_qty),0) FATY
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)-1
and final_loc_id in ('DXBREJ1')
and condition_id='FATY'
and code='Receipt'),(Select
NVL(sum(update_qty),0) DAMG
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)-1
and final_loc_id in ('DXBREJ1')
and condition_id='DMGD'
and code='Receipt'),(Select
nvl(sum(update_qty),0) QVC
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)-1
and condition_id in ('QVC')
and code='Receipt'),(Select
nvl(sum(update_qty),0) IDEAL
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)-1
and condition_id in ('IDEAL')
and code='Receipt'),(Select
nvl(sum(update_qty),0) SUSP
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)-1
and from_loc_id='SUSPENSE'
and code='Receipt'),(Select
nvl(sum(update_qty),0) TRT
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)-1
and final_loc_id in  ('DXBRET','DXBREJ')
and code='Receipt')
/

select
'Receipts (Blind)' A,
CCC-QUAR-FATY-DAMG-QVC-IDEAL-SUSP-TRT C,
QUAR D,
FATY E,
DAMG F,
QVC H,
IDEAL J,
SUSP K,
TRT L,
CCC N
from (Select
nvl(sum(update_qty),0) CCC
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)-1
and code='Receipt'
and reference_id not in (select
pre_advice_id from pre_advice_header
where client_id='DX')),(Select
nvl(sum(update_qty),0) QUAR
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)-1
and final_loc_id in ('DXBREJ1')
and condition_id='QUAR'
and code='Receipt'),(Select
nvl(sum(update_qty),0) FATY
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)-1
and final_loc_id in ('DXBREJ1')
and condition_id='FATY'
and code='Receipt'),(Select
NVL(sum(update_qty),0) DAMG
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)-1
and final_loc_id in ('DXBREJ1')
and condition_id='DMGD'
and code='Receipt'),(Select
nvl(sum(update_qty),0) QVC
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)-1
and condition_id in ('QVC')
and code='Receipt'),(Select
nvl(sum(update_qty),0) IDEAL
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)-1
and condition_id in ('IDEAL')
and code='Receipt'),(Select
nvl(sum(update_qty),0) SUSP
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)-1
and from_loc_id='SUSPENSE'
and code='Receipt'),(Select
nvl(sum(update_qty),0) TRT
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)-1
and final_loc_id in  ('DXBRET','DXBREJ')
and code='Receipt')
/

select
'Adjustments' A,
CCC-QUAR-FATY-DAMG-QVC-IDEAL-SUSP-TRT C,
QUAR D,
FATY E,
DAMG F,
QVC H,
IDEAL J,
SUSP K,
TRT L,
CCC N
from (Select
nvl(sum(update_qty),0) CCC
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)-1
and code='Adjustment'),(Select
nvl(sum(update_qty),0) QUAR
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)-1
and final_loc_id in ('DXBREJ1')
and condition_id='QUAR'
and code='Adjustment'),(Select
nvl(sum(update_qty),0) FATY
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)-1
and final_loc_id in ('DXBREJ1')
and condition_id='FATY'
and code='Adjustment'),(Select
NVL(sum(update_qty),0) DAMG
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)-1
and final_loc_id in ('DXBREJ1')
and condition_id='DMGD'
and code='Adjustment'),(Select
nvl(sum(update_qty),0) QVC
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)-1
and condition_id in ('QVC')
and code='Adjustment'),(Select
nvl(sum(update_qty),0) IDEAL
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)-1
and condition_id in ('IDEAL')
and code='Adjustment'),(Select
nvl(sum(update_qty),0) SUSP
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)-1
and from_loc_id='SUSPENSE'
and code='Adjustment'),(Select
nvl(sum(update_qty),0) TRT
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)-1
and final_loc_id in  ('DXBRET','DXBREJ')
and code='Adjustment')
/

select
'Stock Checks' A,
CCC-QUAR-FATY-DAMG-QVC-IDEAL-SUSP-TRT C,
QUAR D,
FATY E,
DAMG F,
QVC H,
IDEAL J,
SUSP K,
TRT L,
CCC N
from (Select
nvl(sum(update_qty),0) CCC
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)-1
and code='Stock Check'),(Select
nvl(sum(update_qty),0) QUAR
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)-1
and final_loc_id in ('DXBREJ1')
and condition_id='QUAR'
and code='Stock Check'),(Select
nvl(sum(update_qty),0) FATY
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)-1
and final_loc_id in ('DXBREJ1')
and condition_id='FATY'
and code='Stock Check'),(Select
NVL(sum(update_qty),0) DAMG
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)-1
and final_loc_id in ('DXBREJ1')
and condition_id='DMGD'
and code='Stock Check'),(Select
nvl(sum(update_qty),0) QVC
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)-1
and condition_id in ('QVC')
and code='Stock Check'),(Select
nvl(sum(update_qty),0) IDEAL
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)-1
and condition_id in ('IDEAL')
and code='Stock Check'),(Select
nvl(sum(update_qty),0) SUSP
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)-1
and from_loc_id='SUSPENSE'
and code='Stock Check'),(Select
nvl(sum(update_qty),0) TRT
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)-1
and final_loc_id in  ('DXBRET','DXBREJ')
and code='Stock Check')
/

select '  ' from DUAL;

select
'Closing Stock' A,
CCC-QUAR-FATY-DAMG-QVC-IDEAL-SUSP-TRT C,
QUAR D,
FATY E,
DAMG F,
QVC H,
IDEAL J,
SUSP K,
TRT L,
CCC N
from (Select
sum(qty_on_hand) CCC
from inventory
where client_id='DX'),(Select
sum(qty_on_hand) QUAR
from inventory
where client_id='DX'
and location_id in ('DXBREJ1')
and condition_id='QUAR'),(Select
sum(qty_on_hand) FATY
from inventory
where client_id='DX'
and location_id in ('DXBREJ1')
and condition_id='FATY'),(Select
NVL(sum(qty_on_hand),0) DAMG
from inventory
where client_id='DX'
and location_id in ('DXBREJ1')
and condition_id='DMGD'),(Select
nvl(sum(qty_on_hand),0) QVC
from inventory
where client_id='DX'
and zone_1 in ('QVC')),(Select
nvl(sum(qty_on_hand),0) IDEAL
from inventory
where client_id='DX'
and zone_1 in ('IDEAL')),(Select
nvl(sum(qty_on_hand),0) SUSP
from inventory
where client_id='DX'
and location_id='SUSPENSE'),(Select
nvl(sum(qty_on_hand),0) TRT
from inventory
where client_id='DX'
and location_id in  ('DXBRET','DXBREJ'))
/

spool off
