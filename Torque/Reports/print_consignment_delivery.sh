#!/bin/sh

echo "Starting Script"

echo ""

pClientId=$2

pConsignment=$3

pPrinterId=$4

echo "Printer: $pPrinterId"

echo ""

{

sqlplus -s $ORACLE_USR << !
@/home2/dcsdba/reports/print_consignment_delivery.sql $pClientId $pPrinterId $pConsignment "VALID"
exit
/
!
} |
while read line

do

  if [ "$line" ] # Line not NULL

  then

      set $line

	if [ "$4" == "VALID" ]
	
	then
		clientId="$1"
		printerId="$2"
		orderId="$3"
		primarySite="http://torquecourier/Order/PrintByUrl?clientId=$clientId&printerId=$printerId&orderId=$orderId"
		primaryResponse=$(curl --write-out %{http_code} --silent --max-time 60 --output /dev/null "$primarySite")
		#echo $primaryResponse
		echo "Client: $clientId"
		echo "Order: $orderId"
		#echo $primarySite
		echo ""
		
	fi

  fi   

done
