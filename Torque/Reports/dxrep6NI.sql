SET FEEDBACK OFF
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
set colsep ','
set heading off
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240
SET TRIMSPOOL ON

--set feedback on

update order_line
set user_def_chk_4='M'
where rowid in (select
ol.rowid
from order_header oh,order_line ol
where oh.Client_ID='DX'
and oh.status in ('Allocated','Released','In Progress','On Hold')
and nvl(oh.order_type,' ') ='HOME'
and nvl(oh.user_def_type_1,' ') like '%NI%'
--and oh.order_date>sysdate-1
and oh.order_id=ol.order_id
and ol.Client_ID='DX'
and ol.qty_ordered>0
and nvl(ol.user_def_chk_4,' ')<>'Y'
and nvl(ol.qty_tasked,0)+nvl(ol.qty_picked,0)<ol.qty_ordered)
/

--set feedback off

--spool dxrep6.csv

select 'Short_order_report - '||to_char(SYSDATE, 'DD/MM/YY  HH24:MI:SS') from DUAL;

select 'Order Date,Order ID,Dax Ref,Status,Qty Ordered,Qty Allocated,Qty Picked,SKU ID,Description' from dual;


select
to_char(oh.creation_date, 'DD/MM/YY') ||','||
oh.order_id  ||','||
oh.instructions||','||
oh.status  ||','||
nvl(ol.qty_ordered,0) ||','||
nvl(ol.qty_tasked,0) ||','||
nvl(ol.qty_picked,0) ||','||
ol.sku_id  ||','||
sku.description  ||','||
sku.colour  ||','||
sku.sku_size
from order_header oh,order_line ol,sku
where oh.Client_ID='DX'
--and oh.order_date>sysdate-1
and ol.Client_ID='DX'
and oh.order_id=ol.order_id
and ol.user_def_chk_4='M'
and sku.Client_ID='DX'
and ol.sku_id=sku.sku_id
order by
to_char(oh.creation_date, 'DD/MM/YYYY'),
oh.order_id
/
--spool off

--set feedback on

update order_line
set user_def_chk_4='Y'
where client_id = 'DX'
and user_def_chk_4 = 'M'
/

--rollback;
commit;

