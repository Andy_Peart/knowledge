SET FEEDBACK OFF                
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

set pagesize 68
set linesize 120
set verify off

ttitle 'CURVY KATE Receipt Quantities from &&2 to &&3' skip 2

column A heading 'Date' format a12
column F heading 'Mode' format a12
column B heading 'Qty Received' format 99999999
column M format a4 noprint


break on A dup skip 1 on report

compute sum label 'Total' of B on report

select
trunc(dstamp) A,
decode(sku_id,'205163119000','Hangers','205164001000','Hangers','Stock') F,
sum (update_qty) B
from inventory_transaction
where client_id = 'CK'
and station_id not like 'Auto%'
and code like  'Receipt%'
and dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1
group by
trunc(Dstamp),
decode(sku_id,'205163119000','Hangers','205164001000','Hangers','Stock')
order by
A,F
/
