SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 900
SET TRIMSPOOL ON
SET HEADING ON

with T1 as (
select 'Consignment,Order,Date picked,Date created,Time Created,Original Qty,Update Qty,SKU,Work zone,Priority' from dual
union all
select consignment || ',' || reference_id || ',' || date_picked || ',' || date_created || ',' || time_created || ',' || org || ',' || upd || ',' || sku_id || ',' || work_zone || ',' || priority
from (
    select A.consignment, A.reference_id, A.date_picked, B.date_created, B.time_created, A.org, A.upd, A.sku_id, A.work_zone, B.priority
    from (
        select itl.consignment, itl.reference_id, to_char(itl.dstamp,'DD-MM-YYYY') date_picked, itl.sku_id, sum(nvl(itl.original_qty,0)) org, sum(nvl(itl.update_qty,0)) upd, loc.work_zone
        from inventory_transaction itl inner join location loc on itl.from_loc_id = loc.location_id and itl.site_id = loc.site_id
        where itl.client_id = 'MOUNTAIN'
        and itl.code = 'Pick'
        and itl.dstamp between to_date('&&2') and to_date('&&3')+1
        and itl.WORK_GROUP = 'WWW'
        and itl.elapsed_time > 0
        group by itl.consignment, itl.reference_id, to_char(itl.dstamp,'DD-MM-YYYY'), itl.sku_id, loc.work_zone
    ) A
    left join
    (
        select oh.order_id, to_char(oh.creation_date, 'DD-MM-YYYY') date_created, to_char(oh.creation_date, 'HH24:MI:SS') time_created, oh.priority
        from order_header oh
        where oh.client_id = 'MOUNTAIN'
        and order_type = 'WEB'
        and oh.creation_date between to_date('&&2')-5 and to_date('&&3')+1
    ) B
    on A.reference_id = B.order_id
    order by 2,1
)
),
T2 as (
select 'Consignment,Order,Date picked,Date created,Time Created,Qty picked,Priority' from dual
union all
select consignment || ',' || reference_id || ',' || date_picked || ',' || date_created || ',' || time_created || ',' || upd || ',' || priority
from (
    select A.consignment, A.reference_id, A.date_picked, B.date_created, B.time_created, A.upd, B.priority
    from (
        select itl.consignment, itl.reference_id, to_char(itl.dstamp,'DD-MM-YYYY') date_picked, sum(nvl(itl.update_qty,0)) upd
        from inventory_transaction itl inner join location loc on itl.from_loc_id = loc.location_id and itl.site_id = loc.site_id
        where itl.client_id = 'MOUNTAIN'
        and itl.code = 'Pick'
        and itl.dstamp between to_date('&&2') and to_date('&&3')+1
        and itl.WORK_GROUP = 'WWW'
        and itl.elapsed_time > 0
        group by itl.consignment, itl.reference_id, to_char(itl.dstamp,'DD-MM-YYYY')
    ) A
    left join
    (
        select oh.order_id, to_char(oh.creation_date, 'DD-MM-YYYY') date_created, to_char(oh.creation_date, 'HH24:MI:SS') time_created, oh.priority
        from order_header oh
        where oh.client_id = 'MOUNTAIN'
        and order_type = 'WEB'
        and oh.creation_date between to_date('&&2')-5 and to_date('&&3')+1
    ) B
    on A.reference_id = B.order_id
    order by 2,1
)
)
select * from T1
where '&&4' = 'Y'
union all
select * from T2
where '&&4' = 'N'
/