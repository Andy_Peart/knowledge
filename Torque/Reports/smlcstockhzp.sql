set feedback off
set pagesize 68
set linesize 240
set verify off
set colsep ' '


clear columns
clear breaks
clear computes

column CD heading 'Client Id' format A10
column A1 heading 'Work Zone' format A12
column A2 heading 'Qty On Hand' format 999999999
column A3 heading 'Qty Unallocatable' format 999999999
column A4 heading 'Qty Allocated' format 999999999
column A5 heading 'Qty Available' format 999999999



set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

set heading on

break on CD skip 2 dup on report
compute sum label 'Totals' of A2 A3 A4 A5 on report


select 'Stock Holding by Zone as at ' report_date skip 2


select
CD,
A1,
A2,
A3,
A4,
A5
from (select
distinct
CD,
AA A1,
nvl(BB,0) A2,
nvl(DD,0) A3,
nvl(CC,0) A4,
nvl(BB,0)-nvl(DD,0)-nvl(CC,0) A5
from (select
jc.client_id CD,
lc.work_zone AA,
sum(jc.qty_on_hand) BB,
sum(jc.qty_allocated) CC,
count(distinct jc.location_id) PP
from inventory jc,location lc
where jc.client_id in ('IF','S2','GG','VF','EX')
and jc.location_id=lc.location_id
and jc.site_id=lc.site_id
and (jc.client_id='EX'
or (jc.client_id<>'EX' and lc.work_zone not like 'EX%'))
group by
jc.client_id,
lc.work_zone),(select
jc.client_id CD2,
lc.work_zone AAA,
nvl(sum(jc.qty_on_hand),0)-nvl(sum(jc.qty_allocated),0) DD
from inventory jc,location lc
where jc.client_id in ('IF','S2','GG','VF','EX')
and jc.location_id=lc.location_id
and jc.site_id=lc.site_id
and lc.disallow_alloc='Y'
and (jc.client_id='EX'
or (jc.client_id<>'EX' and lc.work_zone not like 'EX%'))group by
jc.client_id,
lc.work_zone)
where AA=AAA (+)
--and (nvl(BB,0)<>0 or location.work_zone like 'S2%')
order by 1,2)
where CD='EX'
or (CD<>'EX'
and A1 not like 'EX%')
/
