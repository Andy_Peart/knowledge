SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON

column A heading 'User' format a20
column B heading 'Batch' format a20
column C heading 'WorkZone' format a10
column D heading 'Location' format a10
column E heading 'TimePicked' format a25
column F heading 'TimeElapsed' format a10
column G heading 'Seconds' format 99999
column H heading 'Seconds' format 99999

break on C dup skip 1 on report

compute avg label 'Average Seconds' of G on C

select 'Date: &&2 to &&3, Pick zone: &&4' from dual;

select 'User,Consignment,Work Zone,Location,Time picked,Time elapsed,Seconds,Units Picked' from DUAL;

select user_id A,
consignment B,
work_zone C,
from_loc_id D,
to_char(time_picked_1,'DD-MM-YYYY HH24:MI:SS') E,
substr(to_char(time_diff),12,8) F,
to_number(substr(to_char(time_diff),18,2))+to_number(substr(to_char(time_diff),15,2))*60 G,
qty H
from (
	select A.user_id, A.consignment, A.work_zone, A.from_loc_id, A.time_picked time_picked_1, B.time_picked time_pick_2, A.rn, B.rn, B.time_picked - A.time_picked time_diff, A.qty
	from (
		select user_id, consignment, loc.work_zone, from_loc_id, dstamp time_picked,
		row_number() over (partition by consignment, work_zone order by user_id, consignment, work_zone, dstamp) rn,case when it.work_group = 'WWW' then 'Web' else 'Retail' end pick_zone,
        sum(nvl(update_qty,0)) qty
		from inventory_transaction it inner join location loc on it.site_id = loc.site_id and it.from_loc_id = loc.location_id
		where it.client_id = 'MOUNTAIN'
		and it.code = 'Pick'
		and it.elapsed_time > 0
		and it.dstamp between to_date('&&2','DD-MON-YYYY') and to_date('&&3','DD-MON-YYYY')+1
        group by user_id, consignment, loc.work_zone, from_loc_id, dstamp, case when it.work_group = 'WWW' then 'Web' else 'Retail' end
		order by 1,2,3,5
	) A
	left join 
	(
		select user_id, consignment, loc.work_zone, from_loc_id, dstamp time_picked,
		row_number() over (partition by consignment, work_zone order by user_id, consignment, work_zone, dstamp)+1 rn,case when it.work_group = 'WWW' then 'Web' else 'Retail' end pick_zone,
        sum(nvl(update_qty,0)) qty
		from inventory_transaction it inner join location loc on it.site_id = loc.site_id and it.from_loc_id = loc.location_id
		where it.client_id = 'MOUNTAIN'
		and it.code = 'Pick'
		and it.elapsed_time > 0
		and it.dstamp between to_date('&&2','DD-MON-YYYY') and to_date('&&3','DD-MON-YYYY')+1
        group by user_id, consignment, loc.work_zone, from_loc_id, dstamp, case when it.work_group = 'WWW' then 'Web' else 'Retail' end
		order by 1,2,3,5
	)
	B on A.user_id = B.user_id and A.work_zone = B.work_zone and A.consignment = B.consignment and A.rn = B.rn
    where A.pick_zone = '&&4'
    and B.pick_zone = '&&4'
	order by 1,2,3,5
)
/