SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 66
SET NEWPAGE 0
SET LINESIZE 600

column AA heading 'Date' format a24
column BB heading 'Name' format a24
column CC heading 'Start' format a24
column DD heading 'Stop' format a24
column EE heading 'Job ID' format a24
column FF heading 'Notes' format a30

break on AA dup skip 2 on BB dup skip 1

compute count label 'Totals' of FF on BB

--spool mwparcelact.csv
select 'Date', 'Name', 'Start', 'Stop', 'Job Id', 'Notes' from dual

select date_ AA, 
user_id BB, 
start_ CC, 
stop_ DD, 
job_id EE, 
notes FF
from (
select it.dstamp, to_char(it.dstamp, 'DD-MON-YYYY') as date_, it.user_id, to_char(it.dstamp, 'HH24:MI;SS') as Start_, to_char(it.dstamp, 'HH24:MI;SS') as Stop_, IT.JOB_ID, it.notes
from inventory_transaction IT
where IT.client_id = 'MOUNTAIN'
and IT.code IN ('Job Start')
and IT.JOB_ID = 'PARCEL'
and it.dstamp between to_date('&&2') and to_date('&&3')+1
order by 1,2
)
/

--spool off