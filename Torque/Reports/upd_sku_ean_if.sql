#script to blank ean on sku records where theres an interface record
update sku
set ean = null
where sku_id in (select sku_id from interface_sku where ean is not null)
/
