/******************************************************************************/
/*                                                                            */
/*                          Torque                                            */
/*                                                                            */
/*     FILE NAME  :   SHdespatch.sql                                          */
/*                                                                            */
/*     DESCRIPTION:   Datastream for Sahara Despatch Note                     */
/*                                                                            */
/*   DATE     BY   PROJ       ID         DESCRIPTION                          */
/*   ======== ==== ======     ========   =============                        */
/*   20/01/12 RW   Sahara                 New retail despatch note            */
/*                                                                            */
/******************************************************************************/

/* Input Parameters are as follows */
-- 1)  GMT
-- 2)  Order_id
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 0
SET VERIFY OFF
SET FEEDBACK OFF
SET TAB OFF

 
-- Set the line length to the maximum possible, but trim 
-- trailing spaces for both screen output and spool files
SET LINESIZE 32767
SET WRAP ON
SET TRIMOUT ON
SET TRIMSPOOL ON


/* Define the format of the output dates */
DEFINE	OutputDateFormat = 'DD-MON-YYYY HH24:MI:SS'
COLUMN	sorter	NOPRINT

SELECT Distinct '0' sorter,
       'DSTREAM_SHDESPC_HDR'          		||'|'||
       rtrim('&&2')                            /* Order_id */
from dual
union all
SELECT distinct '1' sorter,
'DSTREAM_SHDESPC_ADD_HDR' 	             	||'|'||
rtrim (oh.name)					||'|'||
rtrim (oh.customer_id)				||'|'||
rtrim (oh.contact_email)				||'|'||
decode(oh.address1,null,'',oh.address1||'|')||
decode(oh.address2,null,'',oh.address2||'|')||
decode(oh.town,null,'',oh.town||'|')||
decode(oh.county,null,'',oh.county||'|')||
decode(t.text,null,'',t.text||'|')||          /* Country converted using workstation lookup) */
decode(oh.postcode,null,'',oh.postcode||'|')
from order_header oh, country c, language_text t
where oh.order_id = '&&2'
AND oh.CLIENT_ID = 'SH'
and oh.country = c.iso3_id
and language = 'EN_GB'
and label = 'WLK'||c.iso3_id
union all
select distinct '1' sorter,
'DSTREAM_SHDESPC_ORD_HDR'      			||'|'||
rtrim (to_char(sm.shipped_dstamp, 'DD-MON-YYYY'))	||'|'||
rtrim (sm.location_id)        				||'|'||
rtrim (sm.user_id)          				||'|'||
rtrim (sm.site_id)          				||'|'||
rtrim (instructions)
from order_header oh, shipping_manifest sm
where oh.order_id = '&&2'
and oh.order_id = sm.order_id
and oh.client_id = 'SH'
union all
SELECT '1'||sh.container_id SORTER,
'DSTREAM_SHDESPC_LINE'          	 	||'|'||
rtrim (sh.container_id)       			||'|'||
rtrim (s.SKU_id)        			||'|'||
rtrim (s.description)               		||'|'||
rtrim (s.COLOUR)          			||'|'||
rtrim (s.SKU_SIZE)          			||'|'||
rtrim (sum(sh.qty_shipped))         		||'|'||
rtrim (s.user_def_type_5) 			
from sku s, shipping_manifest sh
where sh.order_id = '&&2'
and sh.qty_picked > '0'
and sh.sku_id = s.sku_id
and s.client_id = 'SH'
order by 1,2
/

