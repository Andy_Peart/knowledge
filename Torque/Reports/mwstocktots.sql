SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Stock Holding' from dual
union all
select to_char(sum(qty_on_hand))
from inventory
where client_id = 'MOUNTAIN'
and location_id <> 'SUSPENSE'
union all
select null from dual
union all
select 'Total Skus' from dual
union all
select to_char(count(distinct sku_id))
from inventory
where client_id = 'MOUNTAIN'
and location_id <> 'SUSPENSE'
union all
select null from dual
union all
select 'Sku less than 4' from dual
union all
select to_char(count("Sum")) from 
(
select sum(qty_on_hand) as "Sum"
from inventory
where client_id = 'MOUNTAIN'
and location_id <> 'SUSPENSE'
group by sku_id
having sum(qty_on_hand) <= 4
)
union all
select null from dual
union all
select 'Sku less than 15' from dual
union all
select to_char(count("Sum")) from 
(
select sum(qty_on_hand) as "Sum"
from inventory
where client_id = 'MOUNTAIN'
and location_id <> 'SUSPENSE'
group by sku_id
having sum(qty_on_hand) <= 15
)
/
--spool off