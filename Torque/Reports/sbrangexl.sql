SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON


select 'SKU','FIRST LOCATION','DESCRIPTION','COLOUR','TOTAL QTY', 'RANGE', 'SEASON','NO OF SKU' from dual
union all
select     
i.sku_id ,     
max(i.location_id) as "first location" ,     
s.description ,     
s.colour ,    
TO_CHAR(sum(nvl(i.qty_on_hand, 0))) "Total Qty",     
s.user_def_type_5 as "range",     
s.user_def_type_3 as "season",    
TO_CHAR(count(i.sku_id)) as "sku no of times"    
from inventory i      
inner join sku s on i.client_id = s.client_id and i.sku_id = s.sku_id     
inner join location loc on i.location_id = loc.location_id and i.site_id =loc.site_id     
where i.client_id = 'SB'     
and loc.loc_type = 'Tag-FIFO'     
group by     
i.sku_id,     
s.description,     
s.colour,   
s.user_def_type_5,     
s.user_def_type_3
/
