SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON


column Q format 99999999
column Q2 format 99999999
column Q3 format 99999999
column Q4 format 99999999


break on D dup skip 2 on C dup
compute sum of Q Q2 on C

--spool mwsingles.csv

select 'Store,Creation Date,Order Id,Sku Id,Order Qty,Shipped Qty,Stock' from dual;

select
oh.work_group D,
trunc(oh.creation_date) A,
oh.order_id B,
ol.sku_id C,
ol.qty_ordered Q,
ol.qty_shipped Q2,
nvl(CC,0)
from order_header oh,order_line ol,(select
jc.sku_id SKU,
nvl(sum(jc.qty_on_hand),0)-nvl(sum(jc.qty_allocated),0) CC
from inventory jc,location L
where jc.client_id like 'MOUNTAIN'
and jc.location_id=L.location_id
and L.loc_type='Tag-FIFO'
group by
jc.sku_id)
where oh.client_id='MOUNTAIN'
and oh.work_group like '&&4%'
and oh.order_id like 'MOR%'
and oh.creation_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and ol.client_id='MOUNTAIN'
and ol.order_id=oh.order_id
and ol.qty_ordered=1
and ol.sku_id=SKU (+)
order by D,C,A,B
/

--spool off

