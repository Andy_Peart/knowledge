SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON

break on A dup on report

compute sum label 'Totals' of C D on A
compute sum label 'Run Totals' of C D on report


--spool utnew98.csv

select 'Order Type,User,Orders,Units' from DUAL;

select
DECODE(it.to_loc_id,'UTNET',nvl(oh.user_def_type_1,it.to_loc_id),it.to_loc_id) A,
it.user_id B,
count(distinct it.reference_id) C,
SUM(it.update_qty) D
from inventory_transaction it,order_header oh
where it.client_id = 'UT'
and it.code= 'Pick'
and it.to_loc_id <> 'CONTAINER'
and it.dstamp between TO_DATE('&&2','DD-MON-YYYY') AND TO_DATE('&&3','DD-MON-YYYY') +1
and it.reference_id=oh.order_id
and oh.client_id = 'UT'
GROUP BY
DECODE(it.to_loc_id,'UTNET',nvl(oh.user_def_type_1,it.to_loc_id),it.to_loc_id),
it.USER_ID
ORDER BY
A,B
/

--spool off
