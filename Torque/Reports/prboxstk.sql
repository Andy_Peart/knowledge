SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

 
select 'Location, Docket, Product, Size, Date Received, Qty, Style' from dual;
select loc ||','|| docket ||','|| product ||','|| sku_size ||','||  received ||','|| qty || ',' || style
from (
select inv.location_id loc, inv.user_def_type_4 docket, sku.product_group product, sku.sku_size, to_char(inv.receipt_dstamp,'DD-MM-YYYY') received, sum(inv.qty_on_hand) qty,
inv.user_def_type_7 style
from inventory inv inner join sku on inv.sku_id = sku.sku_id
where inv.client_id in ('PR','PS')
and sku.client_id = 'TR'
and inv.location_id not like 'PRHG%'
and inv.location_id not in ('T','PROMHOLD','REJECT','I1')
and 
(
inv.location_id like '1___'
or
inv.location_id like '4___'
or
inv.location_id like 'TW%'
or
inv.location_id between 'P01' and 'P99'
or
inv.location_id like 'TOP%'
)
group by inv.location_id, inv.user_def_type_4, sku.product_group, sku.sku_size, to_char(inv.receipt_dstamp,'DD-MM-YYYY'), inv.user_def_type_7
order by 1,2,3,4
)
/