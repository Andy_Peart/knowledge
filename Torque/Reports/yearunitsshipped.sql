SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 200
set trimspool on
set newpage 0
set verify off
set colsep ','



break on A dup on report
compute sum label 'WK Total' of B D on A
compute sum label 'Run Total' of B D on report


--spool NEWtots5.csv

select 'Week,Work Group,Txns,Units' from DUAL;

select
trunc(to_date('&&2', 'DD-MON-YYYY'))+(trunc(((trunc(Dstamp))-(trunc(to_date('&&2', 'DD-MON-YYYY'))))/7)*7) A,
work_group C,
count(*) B,
sum (update_qty) D
from inventory_transaction
where client_id = 'MOUNTAIN'
and code = 'Shipment'
and work_group<>'WWW'
--and elapsed_time>0
and dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by
trunc(to_date('&&2', 'DD-MON-YYYY'))+(trunc(((trunc(Dstamp))-(trunc(to_date('&&2', 'DD-MON-YYYY'))))/7)*7),
work_group
union
select
trunc(to_date('&&2', 'DD-MON-YYYY'))+(trunc(((trunc(Dstamp))-(trunc(to_date('&&2', 'DD-MON-YYYY'))))/7)*7) A,
work_group C,
count(*) B,
sum (update_qty) D
from inventory_transaction_archive
where client_id = 'MOUNTAIN'
and code = 'Shipment'
and work_group<>'WWW'
--and elapsed_time>0
and dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by
trunc(to_date('&&2', 'DD-MON-YYYY'))+(trunc(((trunc(Dstamp))-(trunc(to_date('&&2', 'DD-MON-YYYY'))))/7)*7),
work_group
order by A,C
/

--spool off
