SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 500
SET NEWPAGE 1
SET LINESIZE 500
SET TRIMSPOOL ON

column A heading 'Consignment' format A20
column B heading 'Sku' format A20

/*Input SQL here*/
select consignment
, ol.sku_id
from order_header oh
inner join order_line ol on ol.client_id = oh.client_id and ol.order_id = oh.order_id
where ol.sku_id = '&&2'
and oh.client_id = 'WF'
and oh.consignment like 'CLWF%'
group by consignment
, ol.sku_id
/
--spool off