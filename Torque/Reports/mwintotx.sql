SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 80
SET TRIMSPOOL ON


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



select '&&4 Intake Quantity from &&2 to &&3' from DUAL;

column A heading 'Date' format a12
column C heading 'Notes' format a18
column B heading 'Qty' format 99999999

break on A dup on report
compute sum label 'Total' of B on A
compute sum label 'Full Total' of B on report


select
to_char (dstamp, 'DD/MM/YY') A,
Notes C,
sum (update_qty) B
from inventory_transaction
where client_id = '&&4'
and station_id not like 'Auto%'
and code like 'Receipt%'
and dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1
group by
to_char(dstamp, 'DD/MM/YY'),
Notes
order by A, C
/


CLEAR BREAKS
CLEAR COMPUTES

break on report
compute sum label 'Full Total' of B on report

select
'Period' A,
Notes C,
sum (update_qty) B
from inventory_transaction
where client_id = '&&4'
and station_id not like 'Auto%'
and code like 'Receipt%'
and dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1
group by
Notes
order by C
/
