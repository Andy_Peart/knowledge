SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

 

with T1 as 
(
select 'Complete -> Released' as txn_code, to_char(dstamp, 'DD-MM-YYYY') as txn_date, it.reference_id, it.client_id, it.user_id
from inventory_transaction it
where it.client_id in ('TR','PR','MOUNTAIN','SB')
and it.dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.code = 'PreAdv Status'
and it.notes = 'Complete --> Released'
),
T2 as 
(
select 'Receipt Reversal' as txn_code, to_char(dstamp, 'DD-MM-YYYY') as txn_date, it.reference_id, it.client_id, it.user_id
from inventory_transaction it
where it.client_id in ('TR','PR','MOUNTAIN','SB')
and it.dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.code = 'Receipt Reverse'
)
select 'Code, Date, Pre Advice, Client, User ID, Bond, Style, Qty' from dual
union all
select txn_code  || ',' || txn_date   || ',' ||  reference_id   || ',' ||  client_id   || ',' ||  user_id   || ',' ||  bond_flag   || ',' ||  style_name   || ',' ||  qty
from (
select T1.txn_code, T1.txn_date, T1.reference_id, T1.client_id, T1.user_id, PL.user_def_type_5 as bond_flag, user_def_type_7 as style_name, sum(PL.qty_due) qty
from T1 inner join pre_advice_line PL on T1.reference_id = PL.pre_advice_id and T1.client_id = PL.client_id
group by T1.txn_code, T1.txn_date, T1.reference_id, T1.client_id, T1.user_id, T1.user_id, PL.user_def_type_5, user_def_type_7
union all
select T2.txn_code, T2.txn_date, T2.reference_id, T2.client_id, T2.user_id, PL.user_def_type_5 as bond_flag, user_def_type_7 as style_name, sum(PL.qty_due) qty
from T2 inner join pre_advice_line PL on T2.reference_id = PL.pre_advice_id and T2.client_id = PL.client_id
group by T2.txn_code, T2.txn_date, T2.reference_id, T2.client_id, T2.user_id, T2.user_id, PL.user_def_type_5, user_def_type_7
order by 2
)
/