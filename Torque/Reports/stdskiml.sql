SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 9999
--SET NEWPAGE 0
SET LINESIZE 900
SET TRIMSPOOL ON

  select
            i.sku_id as Sku_Id ,
            i.description  as Description,
            i.location_id  as Location_Id,
            sum(i.qty_on_hand) as Qty_On_Hand
   from inventory i,location ll,
                                                      (
                                                        select sku_id as SK 
                                                        from
                                                        (
                                                          select sku_id,count(*) as CT from
                                                              (
                                                                  select iv.sku_id as sku_id,
                                                                  ll.loc_type,
                                                                  iv.location_id,
                                                                  sum(iv.qty_on_hand)
                                                                  from inventory iv,location ll
                                                                  where iv.client_id='&&2'
                                                                  and iv.location_id=ll.location_id
                                                                  and ll.loc_type like 'Tag-FIFO'
                                                                  group by iv.sku_id,ll.loc_type,iv.location_id
                                                               )
                                                          group by sku_id
                                                       )
                                                       where CT>1
                                                     )
    where i.sku_id=SK
    and i.client_id= '&&2'
    and i.location_id=ll.location_id
    and ll.loc_type like 'Tag-FIFO'
    group by i.sku_id,i.location_id,i.description, Qty_On_Hand
  order by i.sku_id,i.location_id
    /