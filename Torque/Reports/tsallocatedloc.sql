SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 800
SET TRIMSPOOL ON


column A format A10
column B format 999999999
column C format 999999999
column M format a4 noprint



break on REPORT on C skip 1



break on report
compute sum label 'Totals' of B C on report

select 'Pickable Locations Allocated' from DUAL;
select '' from DUAL;

select 'Order : &&2' from DUAL;
select 'Consignment: &&3' from DUAL;

--select to_char(SYSDATE, 'DD/MM/YY    HH:MI:SS') from DUAL;

select '' from DUAL;
select 'work_zone,quantity_allocated,allocated_locations' from DUAL;

SELECT 	substr(itl.from_loc_id,1,3) as A,
        sum(itl.update_qty) as  B,
        count(distinct itl.from_loc_id) as C
FROM inventory_transaction itl
INNER JOIN order_header oh
	ON oh.order_id = itl.reference_id
where itl.code = 'Allocate'
and itl.client_id = 'TS'
and ('&&3' IS NULL or oh.consignment = '&&3')
and ('&&2' IS NULL or itl.reference_id = '&&2')
group by substr(itl.from_loc_id,1,3)
order by A;
	   
	   
select '' from DUAL;
select 'Completed Allocations @ ' || + to_char(SYSDATE, 'HH24:MI') from DUAL WHERE '&&3' IS NOT NULL;
select 'work_zone,quantity_allocated,allocated_locations' from DUAL WHERE '&&3' IS NOT NULL;

SELECT 	substr(itl.from_loc_id,1,3) as A,
        sum(itl.update_qty) as  B,
        count(distinct itl.from_loc_id) as C
FROM inventory_transaction itl
INNER JOIN order_header oh
	ON oh.order_id = itl.reference_id
where itl.code = 'Allocate'
and itl.client_id = 'TS'
and oh.status IN ('Complete','Shipped')
and oh.consignment = '&&3'
group by substr(itl.from_loc_id,1,3)
order by A;

select '' from DUAL;
select 'To Do Allocations @ ' || + to_char(SYSDATE, 'HH24:MI') from DUAL WHERE '&&3' IS NOT NULL;
select 'work_zone,quantity_allocated,allocated_locations' from DUAL WHERE '&&3' IS NOT NULL;

SELECT 	substr(itl.from_loc_id,1,3) as A,
        sum(itl.update_qty) as  B,
        count(distinct itl.from_loc_id) as C
FROM inventory_transaction itl
INNER JOIN order_header oh
	ON oh.order_id = itl.reference_id
where itl.code = 'Allocate'
and itl.client_id = 'TS'
and oh.status NOT IN ('Complete','Shipped')
and oh.consignment = '&&3'
group by substr(itl.from_loc_id,1,3)
order by A

/


