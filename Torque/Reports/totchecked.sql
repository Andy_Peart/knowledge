SET FEEDBACK OFF                 
set pagesize 68
set linesize 160
set verify off

clear columns
clear breaks
clear computes

column A1 heading 'Date' format a16
column A2 heading 'User' format a16
column SS heading 'Start Location' format A16
column EE heading 'End Location' format a16
column B heading 'Counts' format 999999
column C heading 'SKUs' format 999999
column D heading 'Units' format 999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'Total SKUs stock checked from &&2 to &&3 - for client ID &&4 as at ' report_date skip 2

break on report
compute sum label 'Totals' of B C D on report

select
A1,
A2,
Z3 SS,
--to_char(A3,'HH24:MM'),
X3 EE,
--to_char(A4,'HH24:MM'),
B,
C,
D
from
(select
to_char(Dstamp,'DD/MM/YY') A1,
user_id A2,
min(Dstamp) A3,
max(Dstamp) A4,
count(*) B,
count(distinct sku_id) C,
sum(original_qty) D
from inventory_transaction
where Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and client_id='&&4'
and code='Stock Check'
group by
to_char(Dstamp,'DD/MM/YY'),
user_id),
(select
Dstamp Z1,
user_id Z2,
from_loc_id Z3
from inventory_transaction
where Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and client_id='&&4'
and code='Stock Check'),
(select
Dstamp X1,
user_id X2,
from_loc_id X3
from inventory_transaction
where Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and client_id='&&4'
and code='Stock Check')
where A3=Z1
and A4=X1
order by
A1,A2
/
