/* **************************************************************************** */
/*                                                                            	*/
/*     FILE NAME  :     itrporaitl.sql                         		      		*/
/*                                                                            	*/
/*     DESCRIPTION:                                                           	*/
/*     How many ITLs are there in the ITL table          						*/
/*                                                                            	*/
/*   DATE       BY   DESCRIPTION						                        */
/*   ========== ==== ===========					                            */
/*   27/03/2013 YB   initial version     				                        */
/*   11/11/2013 YB   Order header added    				                        */
/*   21/11/2013 YB   PAH added    				                          		*/
/*   13/01/2014 YB   Added no of lines and odh shipped date and finish_date pah */
/*   14/01/2014 BK   Fixed formatting                                           */
/*   05/02/2014 BK   Added pre advice archive info                              */
/* **************************************************************************** */

SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','
SET WRAP OFF


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 900
SET TRIMSPOOL ON

/* Column Headings and Formats */
--COLUMN AA FORMAT A45
--COLUMN BB FORMAT A30

--set TERMOUT OFF
column curdate NEW_VALUE report_date
SELECT TO_CHAR(SYSDATE, 'DD/MM/YY  HH:MI') FROM DUAL;
--set TERMOUT ON

/* Top Title */
-- select 'Number of days in ITL table' from DUAL;
-- SELECT SYSDATE -MIN(DSTAMP)
-- FROM INVENTORY_TRANSACTION;

SELECT 'Number of days in Shipping Manifest table' FROM DUAL;
SELECT SYSDATE -MIN(SHIPPED_DSTAMP)||'    ,'||to_char(COUNT(CLIENT_ID),'99999999')
FROM SHIPPING_MANIFEST;

SELECT 'Number of days in Order Header table' FROM DUAL;
SELECT SYSDATE -MIN(SHIPPED_DATE)||'    ,'||to_char(COUNT(CLIENT_ID),'99999999') 
FROM ORDER_HEADER;

SELECT 'Number of days in Pre Advice Header table' FROM DUAL;
SELECT SYSDATE -MIN(FINISH_DSTAMP)||'    ,'||to_char(COUNT(CLIENT_ID),'99999999') 
FROM PRE_ADVICE_HEADER;

SELECT 'Number of days in Pre Advice Header Archive' FROM DUAL;
SELECT SYSDATE -MIN(FINISH_DSTAMP)||'    ,'||to_char(COUNT(CLIENT_ID),'99999999') 
FROM PRE_ADVICE_HEADER_ARCHIVE;



