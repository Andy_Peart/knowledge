SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240
SET TRIMSPOOL ON

--spool SHWH.csv

select 'SKU,Note,Size,Colour,Description,Qty on Hand,Def Num 2,Def Num 3' from DUAL;

Select
iv.SKU_ID ||','||
sku.user_def_note_1||','||
sku.sku_size ||','||
sku.colour ||','||
SKU.Description ||','||
sum(iv.qty_on_hand)||','||
sku.user_def_num_2||','||
sku.user_def_num_3
from inventory iv,sku
where iv.client_id='SH'
and iv.zone_1='SHWH'
and iv.client_id=sku.client_id
and iv.sku_id=sku.sku_id
group by
iv.SKU_ID,
sku.user_def_note_1,
sku.sku_size,
sku.colour,
SKU.Description,
sku.user_def_num_2,
sku.user_def_num_3
order by
iv.SKU_ID
/

--spool off
