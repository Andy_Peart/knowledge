SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','
SET UND OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 320
SET TRIMSPOOL ON



column A format A14
column B format A14
column C format A24
column D format A28
column E format A12
column F format A12
column G format A24
column H format A32
column J format 99999
column K format 99999
column L format 99999
column M format 99999
column N format 99999


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



break on report

compute sum label 'Total'  of J K L M N  on report

--spool dxrep1.csv

select 'Orders_Outstanding - '||to_char(SYSDATE, 'DD/MM/YY  HH24:MI:SS') from DUAL;

select 'Order Type,Status,Order ID,Document No.,Creation Date,Order Date,Account,Name,Ordered,Picked,Tasked,Shipped,Variance' from DUAL;

Select
oh.Order_type A,
oh.status B,
oh.Order_ID C,
oh.instructions D,
trunc(oh.creation_date) E,
trunc(oh.order_date) F,
oh.Customer_id G,
oh.Name H,
sum(ol.qty_ordered) J,
sum(nvl(ol.qty_picked,0)) K,
sum(nvl(ol.qty_tasked,0)) L,
sum(nvl(ol.qty_shipped,0)) M,
sum(ol.qty_ordered-nvl(ol.qty_picked,0)-nvl(ol.qty_tasked,0)-nvl(ol.qty_shipped,0)) N
from order_header oh,order_line ol
where oh.client_id='DX'
and oh.status in ('Released','Allocated','In Progress','Complete')
and oh.order_type='WHOLESALEB'
and oh.creation_date>sysdate-7
and oh.order_id=ol.order_id
and ol.client_id='DX'
group by
oh.Order_type,
oh.status,
oh.Order_ID,
oh.instructions,
trunc(oh.creation_date),
trunc(oh.order_date),
oh.Customer_id,
oh.Name
order by
1
/
 
--spool off
