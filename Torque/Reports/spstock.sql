SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 100
SET TRIMSPOOL ON


spool spstock.csv

select 'SKU,Description,Qty' from DUAL;


select
it.sku_id||','||
sku.description||','||
sum(it.qty_on_hand)
from inventory it,sku
where it.client_id = 'SP'
and it.sku_id=sku.sku_id
and sku.client_id = 'SP'
group by it.sku_id,sku.description
order by it.sku_id
/

spool off
