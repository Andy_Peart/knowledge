SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Zone,Qty Sku,Qty Units' from dual
union all
select "Zone" || ',' || "Qty Sku" || ',' || "Qty Units"
from (
select l.zone_1             as "Zone"
, count(distinct i.sku_id)  as "Qty Sku"
, sum(i.qty_on_hand)        as "Qty Units"
from inventory i
inner join location l on l.site_id = i.site_id and l.location_id = i.location_id
where i.client_id = 'MOUNTAIN'
and l.zone_1 is not null
group by l.zone_1
)
union all
select null from dual
union all
select null from dual
union all
select 'Zone,1,2,3,4,5,="6-10",="11-20",21-50,51-100,100+' from dual
union all
select "Zone" || ',' || "1" || ',' || "2" || ',' || "3" || ',' || "4" || ',' || "5" || ',' || "6-10" || ',' || "11-20" || ',' || "21-50" || ',' || "51-100" || ',' || "100+"
from
(
with one as (
select "Zone", sum("Qty") as "Qty" from (
select l.zone_1 as "Zone"
, i.sku_id      as "Sku"
, count(distinct i.sku_id)    as "Qty"
from inventory i
inner join location l on l.site_id = i.site_id and l.location_id = i.location_id
where i.client_id = 'MOUNTAIN'
group by l.zone_1
, i.sku_id
having sum(i.qty_on_hand) = 1
)group by "Zone"
), two as (
select "Zone", sum("Qty") as "Qty" from (
select l.zone_1 as "Zone"
, i.sku_id      as "Sku"
, count(distinct i.sku_id)    as "Qty"
from inventory i
inner join location l on l.site_id = i.site_id and l.location_id = i.location_id
where i.client_id = 'MOUNTAIN'
group by l.zone_1
, i.sku_id
having sum(i.qty_on_hand) = 2
)group by "Zone"
), three as (
select "Zone", sum("Qty") as "Qty" from (
select l.zone_1 as "Zone"
, i.sku_id      as "Sku"
, count(distinct i.sku_id)    as "Qty"
from inventory i
inner join location l on l.site_id = i.site_id and l.location_id = i.location_id
where i.client_id = 'MOUNTAIN'
group by l.zone_1
, i.sku_id
having sum(i.qty_on_hand) = 3
)group by "Zone"
), four as (
select "Zone", sum("Qty") as "Qty" from (
select l.zone_1 as "Zone"
, i.sku_id      as "Sku"
, count(distinct i.sku_id)    as "Qty"
from inventory i
inner join location l on l.site_id = i.site_id and l.location_id = i.location_id
where i.client_id = 'MOUNTAIN'
group by l.zone_1
, i.sku_id
having sum(i.qty_on_hand) = 4
)group by "Zone"
), five as (
select "Zone", sum("Qty") as "Qty" from (
select l.zone_1 as "Zone"
, i.sku_id      as "Sku"
, count(distinct i.sku_id)    as "Qty"
from inventory i
inner join location l on l.site_id = i.site_id and l.location_id = i.location_id
where i.client_id = 'MOUNTAIN'
group by l.zone_1
, i.sku_id
having sum(i.qty_on_hand) = 5
)group by "Zone"
), six as (
select "Zone", sum("Qty") as "Qty" from (
select l.zone_1 as "Zone"
, i.sku_id      as "Sku"
, count(distinct i.sku_id)    as "Qty"
from inventory i
inner join location l on l.site_id = i.site_id and l.location_id = i.location_id
where i.client_id = 'MOUNTAIN'
group by l.zone_1
, i.sku_id
having sum(i.qty_on_hand) between 6 and 10
)group by "Zone"
), seven as (
select "Zone", sum("Qty") as "Qty" from (
select l.zone_1 as "Zone"
, i.sku_id      as "Sku"
, count(distinct i.sku_id)    as "Qty"
from inventory i
inner join location l on l.site_id = i.site_id and l.location_id = i.location_id
where i.client_id = 'MOUNTAIN'
group by l.zone_1
, i.sku_id
having sum(i.qty_on_hand) between 11 and 20
)group by "Zone"
), eight as (
select "Zone", sum("Qty") as "Qty" from (
select l.zone_1 as "Zone"
, i.sku_id      as "Sku"
, count(distinct i.sku_id)    as "Qty"
from inventory i
inner join location l on l.site_id = i.site_id and l.location_id = i.location_id
where i.client_id = 'MOUNTAIN'
group by l.zone_1
, i.sku_id
having sum(i.qty_on_hand) between 21 and 50
)group by "Zone"
), nine as (
select "Zone", sum("Qty") as "Qty" from (
select l.zone_1 as "Zone"
, i.sku_id      as "Sku"
, count(distinct i.sku_id)    as "Qty"
from inventory i
inner join location l on l.site_id = i.site_id and l.location_id = i.location_id
where i.client_id = 'MOUNTAIN'
group by l.zone_1
, i.sku_id
having sum(i.qty_on_hand) between 51 and 100
)group by "Zone"
), ten as (
select "Zone", sum("Qty") as "Qty" from (
select l.zone_1 as "Zone"
, i.sku_id      as "Sku"
, count(distinct i.sku_id)    as "Qty"
from inventory i
inner join location l on l.site_id = i.site_id and l.location_id = i.location_id
where i.client_id = 'MOUNTAIN'
group by l.zone_1
, i.sku_id
having sum(i.qty_on_hand) > 100
)group by "Zone"
), zones as
(
    select l.zone_1 as "Zone"
    from inventory i
    inner join location l on l.site_id = i.site_id and l.location_id = i.location_id
    where i.client_id = 'MOUNTAIN'
    and l.zone_1 is not null
    group by l.zone_1
)
select zones."Zone"   as "Zone"
, one."Qty"         as "1"
, two."Qty"         as "2"
, three."Qty"       as "3"
, four."Qty"        as "4"
, five."Qty"        as "5"
, six."Qty"         as "6-10"
, seven."Qty"       as "11-20"
, eight."Qty"       as "21-50"  
, nine."Qty"        as "51-100"
, ten."Qty"         as "100+"
from zones
left join one on one."Zone" = zones."Zone"
left join two on two."Zone" = zones."Zone"
left join three on three."Zone" = zones."Zone"
left join four on four."Zone" = zones."Zone"
left join five on five."Zone" = zones."Zone"
left join six on six."Zone" = zones."Zone"
left join seven on seven."Zone" = zones."Zone"
left join eight on eight."Zone" = zones."Zone"
left join nine on nine."Zone" = zones."Zone"
left join ten on ten."Zone" = zones."Zone"
where zones."Zone" is not null
)
/
--spool off