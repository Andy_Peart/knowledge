SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

select tag_id, sku_id, Location_id, qty_on_hand
from inventory
where client_id = 'MOUNTAIN'
and qty_allocated = 0
order by location_id, sku_id
/
