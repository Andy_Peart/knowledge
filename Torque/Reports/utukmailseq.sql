SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep '|'


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 300
SET TRIMSPOOL ON
 
column XX NOPRINT

--spool mkukmail.csv

select
mt.sequence XX,
'H049196'||'|'||
to_char(Sysdate,'DDMMYYYY')||'|'||
contact||'|'||
Name||'|'||
Address1||'|'||
Address2||'|'||
Town||'|'||
County||'|||'||
PostCode||'||'||
'1'||'|'||
'10'||'|'||
'1'||'|'||
instructions||'|'||
USER_DEF_NOTE_2||'|||||'||
'UT'||order_id||'|||||||||||||||'||
'Torque (Uttam) Challenge Way Wigan WN5 0LD'||'||'||
CONTACT_PHONE||'|'||
' '
from order_header oh left join MOVE_TASK mt on oh.order_id=MT.TASK_ID and oh.client_id = mt.client_id,
(select
oh.order_id AAA,
sum(nvl(ol.line_value,0)) BBB
from order_header oh inner join order_line ol on oh.order_id=ol.order_id and oh.client_id=ol.client_id
where oh.client_id='UT'
and oh.country='GBR'
and oh.consignment='&&2'
group by
oh.order_id)
where oh.client_id='UT'
and oh.consignment='&&2'
and oh.order_id=AAA 
ORDER BY mt.sequence
/

--spool off
