SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240


column AA heading 'Shop' format A6
column BB heading 'Date' format A14
column CC heading 'Qty' format 999999

select 'Shop,Date,Qty' from DUAL;


select
customer_id  AA,
trunc(Dstamp) BB,
nvl(sum(update_qty),0) CC
from inventory_transaction
where client_id='&&4'
and code='Shipment'
and Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by
customer_id,
trunc(Dstamp)
order by
AA,BB
/
