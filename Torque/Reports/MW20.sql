SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','
SET WRAP OFF


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500

column A heading 'Order' format A12
column B heading 'Status' format A12
column C heading 'Order Date' format A12
column D heading 'Creation Date' format A12
column E heading 'Consignment' format A20
column F heading 'Name' format A20
column G heading 'User Def Type 1' format A20
column H heading 'SKU ID' format A18
column J heading 'Description' format A40
column K heading 'Colour' format A12
column L heading 'Size' format A12
column M heading 'Ordered' format 99999
column N heading 'Allocat' format 99999
column P heading 'Short  ' format 99999
column R heading 'Tot Stk' format 99999
column S heading 'MMAIL  ' format 99999
column T heading 'A T O O' format 99999
column U heading 'Intake ' format 99999



Select 'Order,Status,Order Date,Creation Date,Consignment,Name,T2T,SKU,Description,Colour,Size,Ordered,Allocated,Short,Total Stock,MMAIL stock,Allocated to Other Orders,Intake' from DUAL;

--break on D dup

--compute sum label 'Order Total' of U V W on D


select
oh.order_id A,
oh.status B,
to_char(oh.order_date,'DD/MM/YY') C,
to_char(oh.creation_date,'DD/MM/YY') D,
oh.consignment E,
oh.name F,
sku.User_Def_Type_1 G,
''''||ol.sku_id H,
sku.description J,
sku.colour K,
sku.sku_size L,
ol.qty_ordered M,
nvl(ol.qty_tasked,0)+nvl(ol.qty_picked,0) N,
ol.qty_ordered-nvl(ol.qty_tasked,0)-nvl(ol.qty_picked,0) P,
nvl(HH1,0) R,
nvl(HH2,0) S,
nvl(QQ1,0) T,
nvl(HH3,0) U
from order_header oh,order_line ol,sku,
(select
sku_id SS1,
sum(qty_on_hand) HH1,
sum(qty_allocated) QQ1
from inventory
where client_id='MOUNTAIN'
and location_id not like 'DESPATCH%'
group by sku_id),
(select
sku_id SS2,
sum(qty_on_hand) HH2,
sum(qty_allocated) QQ2
from inventory
where client_id='MOUNTAIN'
and location_id not like 'DESPATCH%'
and condition_id='MMAIL'
group by sku_id),
(select
i.sku_id SS3,
sum(i.qty_on_hand) HH3
from inventory i,location l
where i.client_id='MOUNTAIN'
and i.location_id=l.location_id
and i.lock_status=l.lock_status
and i.site_id=l.site_id
--and i.zone_1=l.zone_1
and l.loc_type='Receive Dock'
group by i.sku_id)
where oh.client_id='MOUNTAIN'
and oh.order_type='WEB'
and oh.status in ('Allocated','In Progress','Released')
and oh.order_id=ol.order_id
and ol.client_id='MOUNTAIN'
and ol.sku_id=sku.sku_id
and ol.qty_ordered-nvl(ol.qty_tasked,0)-nvl(ol.qty_picked,0)>0
and sku.client_id='MOUNTAIN'
and ol.sku_id=SS1 (+)
and ol.sku_id=SS2 (+)
and ol.sku_id=SS3 (+)
order by
A
/
