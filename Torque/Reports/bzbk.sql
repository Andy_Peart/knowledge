SET FEEDBACK OFF                 
set pagesize 68
set linesize 300
set verify off
set wrap off
set newpage 0

clear columns
clear breaks
clear computes


column A heading 'Order ID' format a16
column B heading 'SKU Ordered' format a16
column BB heading 'SKU Description' format a40
column BBB heading 'Size' format a10
column BBBB heading 'Ordered Group' format a48
column C heading 'SKU Subbed' format a16
column CC heading 'SKU Description' format a40
column CCC heading 'Size' format a10
column CCCC heading 'Subbed Group' format a48
column D heading 'Supld' format 99999
column E heading 'Line ID' format 99999999
column F heading 'Old Line ID' format 99999999
column G heading 'Subbed' format 99999
column H heading 'Tasked' format 99999
column I heading 'Shipped' format 99999

break on A skip 2

select
ol.order_id A,
AA B,
sku1.description BB,
sku1.sku_size BBB,
BB D,
ol.sku_id C,
sku2.description CC,
sku2.sku_size CCC,
ol.qty_ordered G,
sku1.user_def_note_1 BBBB,
sku2.user_def_note_1 CCCC
--ol.line_id E,
--ol.original_line_id F,
--substitute_flag,
--ol.qty_tasked H
--ol.qty_shipped I
from sku sku1,sku sku2,order_header oh,order_line ol,
(select ol.sku_id as AA, ol.qty_ordered as BB, ol.order_id as DD,ol.line_id as CC
from order_line ol,order_header oh
where oh.client_id='TR'
and oh.order_id=ol.order_id
and oh.order_id like '2007&&2%')
--and ol.substitute_flag='Y')
--and oh.order_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&2', 'DD-MON-YYYY')+2)
where oh.client_id='TR'
and oh.order_id=DD
and oh.order_id=ol.order_id
and oh.order_id like '2007&&2%'
and ol.original_line_id=CC
and AA=sku1.sku_id
and ol.sku_id=sku2.sku_id
and sku1.user_def_note_1<>sku2.user_def_note_1
order by ol.order_id
/
