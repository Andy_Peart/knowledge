SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON



with T1 as 
(
select to_char(it.dstamp,'DD-MM-YYYY') as date_, oh.priority, oh.order_id, sum(update_qty) units
from inventory_transaction it inner join order_header oh on it.reference_id = oh.order_id and it.client_id = oh.client_id
where it.client_id = 'MOUNTAIN'
and it.code = 'Shipment'
and it.update_qty > 0
and it.work_group = 'WWW'
and it.dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1
group by to_char(it.dstamp,'DD-MM-YYYY'), oh.priority, oh.order_id
)
,
Unit1 as (
select date_, priority, count(*) orders
from T1
where units = 1
group by date_, priority
)
,
Unit27 as (
select date_, priority, count(*) orders
from T1
where units >=2 and units <=7
group by date_, priority
)
,
Unit8 as (
select date_, priority, count(*) orders
from T1
where units >= 8
group by date_, priority
)
select 'Date, Priority, 1 Unit, 2-7 Units, 8+ Units' from dual
union all
select date_ || ',' || priority || ',' || unit1 || ',' || unit26 || ',' || unit8
from (
select A.date_, A.priority, nvl(Unit1.orders,0) Unit1, nvl(Unit27.orders,0) Unit26, nvl(Unit8.orders,0) Unit8
from (
select distinct to_char(it.dstamp,'DD-MM-YYYY') as date_, oh.priority
from inventory_transaction it inner join order_header oh on it.reference_id = oh.order_id and it.client_id = oh.client_id
where it.client_id = 'MOUNTAIN'
and it.code = 'Shipment'
and it.update_qty > 0
and it.work_group = 'WWW'
and it.dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1
) A
left join Unit1 on A.date_ = Unit1.date_ and A.priority = Unit1.priority
left join Unit27 on A.date_ = Unit27.date_ and A.priority = Unit27.priority
left join Unit8 on A.date_ = Unit8.date_ and A.priority = Unit8.priority
order by 1,2
)
/
