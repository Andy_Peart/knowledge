SET FEEDBACK OFF                 
set verify off
set wrap off
set colsep ','

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 132
set trimspool on

column A heading 'User ID' format a18
column B heading 'Task Type' format a18
column XX heading 'WWW' format a12
column C heading 'Tasks' format 99999999
column D heading 'Units' format 99999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

--break on report 
break on A dup 

--spool mwtasks.csv

select 'Allocate/Picks for SHIPPED orders created between &&2 to &&3 - for client MOUNTAIN' from DUAL;

select 'Task,Stock,Order Type,Units' from DUAL;

select
it.code B,
DECODE(nvl(RD,'Old'),'Old','Old','New') C,
DECODE(it.work_group,'WWW','Mail Order','Outlets') XX,
sum(it.update_qty) D
from inventory_transaction it,order_header oh,(select
it.sku_id SK,
'1' RD,
it.tag_id TG
from inventory_transaction it
where it.client_id='MOUNTAIN'
and it.code='Receipt'
and it.Dstamp>to_date('&&2', 'DD-MON-YYYY'))
where it.client_id='MOUNTAIN'
and oh.client_id='MOUNTAIN'
and it.reference_id=oh.order_id
and oh.status='Shipped'
and oh.creation_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and (it.code='Allocate'
or (it.code='Pick' and it.from_loc_id not in ('CONTAINER','STG','UKPAC','UKPAR')))
and it.sku_id=SK (+)
and it.tag_id=TG (+)
group by 
it.code,
DECODE(nvl(RD,'Old'),'Old','Old','New'),
DECODE(it.work_group,'WWW','Mail Order','Outlets')
order by XX,B
/




--spool off
