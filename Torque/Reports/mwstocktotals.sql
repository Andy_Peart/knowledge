SET FEEDBACK OFF           
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 450
SET TRIMSPOOL ON

spool msstkres.csv 

select
A1,
A2,
A3,
DECODE(SIGN(to_number(A3)-A2),-1,to_number(A3),A2) A4,
A5
from (select
iv.sku_id A1,
sum(iv.qty_on_hand) A2,
nvl(sku.user_def_num_3,0) A3,
sku.user_def_type_8 A5
from inventory iv, sku,location ll
where iv.client_id = 'MOUNTAIN'
and upper(iv.sku_id)=lower(iv.sku_id)
and iv.location_id=ll.location_id
and ll.loc_type='Tag-FIFO'
and ll.site_id='BD2'
and iv.sku_id <> 'TEST'
and iv.sku_id = sku.sku_id
and sku.client_id = 'MOUNTAIN'
group by
iv.sku_id,
nvl(sku.user_def_num_3,0),
sku.user_def_type_8)
order by A1
/


spool off

