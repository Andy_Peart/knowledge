SET FEEDBACK OFF                
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 400
SET TRIMSPOOL ON

column M heading ' ' noprint
--break on M skip 2

--spool ckstock.csv

--select 'STOCK LEVELS' from DUAL;
--select 'Sku,Description,Colour,Size,Group,On Hand,Allocated,Available' from DUAL;

select * from 
(
select
decode(sku.sku_id,'205163119000',2,'205164001000',2,1) M,
sku.sku_id||','
||sku.description||','
||sku.colour||','
||sku.sku_size||','
||sku.product_group||','
||nvl(AA,0)||','
||nvl(BB,0)||','
||nvl(CC,0)
from sku,(select
i.sku_id SS,
sum(i.qty_on_hand) AA,
sum(i.qty_allocated) BB,
sum(i.qty_on_hand-i.qty_allocated) CC
from inventory i
where i.client_id = 'CK'
and i.location_id<>'CKOUT'
and i.location_id = 'CK6000'
group by i.sku_id)
where sku.client_id = 'CK'
and sku.user_def_chk_4<>'Y'
and sku.sku_id=SS  
union all
select null,null from dual
union all 
select
decode(sku.sku_id,'205163119000',2,'205164001000',2,1) M,
sku.sku_id||','
||sku.description||','
||sku.colour||','
||sku.sku_size||','
||sku.product_group||','
||nvl(AA,0)||','
||nvl(BB,0)||','
||nvl(CC,0)
from sku,(select
i.sku_id SS,
sum(i.qty_on_hand) AA,
sum(i.qty_allocated) BB,
sum(i.qty_on_hand-i.qty_allocated) CC
from inventory i
where i.client_id = 'CK'
and i.location_id<>'CKOUT' 
and i.location_id <> 'CK6000'
group by i.sku_id)
where sku.client_id = 'CK'
and sku.user_def_chk_4<>'Y'
and sku.sku_id=SS  
)
/

--spool off

