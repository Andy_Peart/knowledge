set feedback off
set pagesize 68
set linesize 240
set verify off

clear columns
clear breaks
clear computes

column A heading 'Date' format A12
column B heading 'Packs' format 999999
column C heading 'Units' format 999999
column D heading 'Ratio' format 999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'Pack Config Units Picked for client ID MOUNTAIN during Period '&&2' to '&&3' as at ' report_date skip 2

break on report
compute sum label 'Total' of B C on report


select
to_char(it.Dstamp,'DD/MM/YYYY') A,
sum(it.update_qty/sc.ratio_1_to_2) B,
sum(it.update_qty) C
from inventory_transaction it,sku_config sc
where it.client_id='MOUNTAIN'
and it.code='Shipment'
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.sku_id=sc.config_id
group by 
to_char(it.Dstamp,'DD/MM/YYYY')
/

