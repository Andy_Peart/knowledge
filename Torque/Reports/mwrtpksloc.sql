SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Date,Location,Units Picked,Order id' from dual
union all
select "Date"||','||"Loc"||','||"Qty"||','||"Ord"
from
(
    select trunc(it.dstamp) as "Date"
    , it.from_loc_id        as "Loc"
    , sum(it.update_qty)    as "Qty"
    , it.reference_id       as "Ord"
    from inventory_transaction it
    inner join order_header oh on it.reference_id = oh.order_id and it.client_id = oh.client_id
    where it.client_id  ='MOUNTAIN'
    and oh.order_type = 'RETAIL1'
    and it.code = 'Pick'
    and it.from_loc_id <> 'CONTAINER'
    and it.dstamp between to_date('&&2') and to_date('&&3')+1
    group by trunc(it.dstamp)
    , it.from_loc_id
    , it.reference_id
    order by from_loc_id
)
/
--spool off