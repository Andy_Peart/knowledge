SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Status,Consignment,Order #,Num Lines' from dual
/* TORQUE - (MW) - Fully Ugly Mail Orders */
union all
select "Stat" || ',' || "Cons" || ',' || "Order" || ',' || "LineCount"
from
(
select "Stat","Cons","Order","LineCount" /*Added extra lines to be able to order by consignment*/
from
(
select ol.order_id                                          as "Order"
, count(distinct ol.sku_id) over (partition by ol.order_id) as "LineCount"
, ol.sku_id                                                 as "Sku"
, case when nvl(s.ugly,'N') = 'Y' then 1 else 0 end         as "UglyCount"
, NVL(oh.consignment,'x')                                   as "Cons"
, oh.status                                                 as "Stat"
from order_header oh
inner join order_line ol on ol.client_id = oh.client_id and ol.order_id = oh.order_id
inner join sku s on s.client_id = ol.client_id and s.sku_id = ol.sku_id
where oh.client_id = 'MOUNTAIN'
and substr(oh.work_group,2,2) = 'WW'
and oh.status not in ('Shipped','Cancelled')
group by ol.order_id
, ol.sku_id
, nvl(s.ugly,'N')
, oh.consignment
, oh.status
)
group by "Cons","Order","LineCount","Stat"
having "LineCount" = sum("UglyCount")
order by "Cons"
)
/
--spool off