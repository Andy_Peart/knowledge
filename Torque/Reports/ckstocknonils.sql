SET FEEDBACK OFF                
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 400
SET TRIMSPOOL ON

column M heading ' ' noprint
break on M skip 2

--spool ckstock.csv

select 'NON ZERO STOCK LEVELS' from DUAL;
select 'Sku,Description,Colour,Size,On Hand,Allocated,Available' from DUAL;

select
decode(sku.sku_id,'205163119000',2,'205164001000',2,1) M,
sku.sku_id||''','
||sku.description||','
||sku.colour||','
||sku.sku_size||','
||nvl(AA,0)||','
||nvl(BB,0)||','
||nvl(CC,0)
from sku,(select
i.sku_id SS,
sum(i.qty_on_hand) AA,
sum(i.qty_allocated) BB,
sum(i.qty_on_hand-i.qty_allocated) CC
from inventory i
where i.client_id = 'CK'
group by i.sku_id)
where sku.client_id = 'CK'
and sku.sku_id=SS
--and AA<>0 or BB<>0
order by
M,
sku.sku_id
/

--spool off

