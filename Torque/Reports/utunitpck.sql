SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 999
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON

--spool utunitpck.csv

with t1 as (
select user_id, count(distinct reference_id) as "Num of Orders", SUM(update_qty) as "Units"
from inventory_transaction
where client_id = 'UT'
and code= 'Pick'
and to_loc_id <> 'CONTAINER' 
and dstamp between TO_DATE('&&2','DD-MON-YYYY') AND TO_DATE('&&3','DD-MON-YYYY') +1
GROUP BY USER_ID
),
/* UTUOT */
t2 as 
(
select user_id, count(distinct reference_id) as "Num of Orders", SUM(update_qty) as "Units" 
from inventory_transaction
where client_id = 'UT'
and code= 'Pick'
and to_loc_id = 'UTOUT' 
and dstamp between TO_DATE('&&2','DD-MON-YYYY') AND TO_DATE('&&3','DD-MON-YYYY') +1
GROUP BY USER_ID,  to_loc_id
),
/* UTWEB*/
t3 as (
select user_id, code, to_loc_id, count(distinct reference_id) as "Num of Orders", SUM(update_qty) as "Units"
from inventory_transaction
where client_id = 'UT'
and code= 'Pick'
and to_loc_id = 'UTWEB' 
and dstamp between TO_DATE('&&2','DD-MON-YYYY') AND TO_DATE('&&3','DD-MON-YYYY') +1
GROUP BY USER_ID, CODE, to_loc_id
),
/*UTDEB*/
t4 as (
select user_id, code, to_loc_id, count(distinct reference_id) as "Num of Orders", SUM(update_qty) as "Units"
from inventory_transaction
where client_id = 'UT'
and code= 'Pick'
and to_loc_id = 'UTDEB' 
and dstamp between TO_DATE('&&2','DD-MON-YYYY') AND TO_DATE('&&3','DD-MON-YYYY') +1
GROUP BY USER_ID, CODE, to_loc_id
),
/* UTDEBROI */
t5 as (
select user_id, code, to_loc_id, count(distinct reference_id) as "Num of Orders", SUM(update_qty) as "Units"
from inventory_transaction
where client_id = 'UT'
and code= 'Pick'
and to_loc_id = 'UTDEBROI' 
and dstamp between TO_DATE('&&2','DD-MON-YYYY') AND TO_DATE('&&3','DD-MON-YYYY') +1
GROUP BY USER_ID, CODE, to_loc_id
),
/* UTNET */
t6 as (
select user_id, code, to_loc_id, count(distinct reference_id) as "Num of Orders", SUM(update_qty) as "Units"
from inventory_transaction
where client_id = 'UT'
and code= 'Pick'
and to_loc_id = 'UTNET' 
and dstamp between TO_DATE('&&2','DD-MON-YYYY') AND TO_DATE('&&3','DD-MON-YYYY') +1
GROUP BY USER_ID, CODE, to_loc_id
)
/* Main Select */
select t1.user_id, 
       nvl(t1."Num of Orders", 0) as "Total orders", nvl(t1."Units", 0) as "Total Units",
       nvl(t2."Num of Orders", 0) as "UTOUT Orders", nvl(t2."Units", 0) as "UTOUT Units",
       nvl(t3."Num of Orders", 0) as "UTWEB Orders", nvl(t3."Units", 0) as "UTWEB Units",
       nvl(t4."Num of Orders", 0) as "UTDEB Orders", nvl(t4."Units", 0) as "UTDEB Units",
       nvl(t5."Num of Orders", 0) as "UTDEBROI Orders", nvl(t5."Units", 0) as "UTDEBROI Units",
       nvl(t6."Num of Orders", 0) as "UTNET Orders", nvl(t6."Units", 0) as "UTNET Units"
from t1 
     left join t2 on t1.user_id = t2.user_id
     left join t3 on t1.user_id = t3.user_id
     left join t4 on t1.user_id = t4.user_id
     left join t5 on t1.user_id = t5.user_id
     left join t6 on t1.user_id = t6.user_id
order by user_id
/


--spool off
