/******************************************************************************/
/*                                                                            */
/*                            Elite                                           */
/*                                                                            */
/*     FILE NAME  :   pridespatch.sql                                         */
/*                                                                            */
/*     DESCRIPTION:    Datastream for Pringle Despatch Note / Pack List       */
/*                                                                            */
/*   DATE     BY   PROJ       ID         DESCRIPTION                          */
/*   ======== ==== ======     ========   =============                        */
/*   27/03/07 LH   Pringle               Despatch labels by Container         */
/******************************************************************************/

/* Input Parameters are as follows */
-- 1)  Order ID
-- 2)  Container ID
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 0
SET VERIFY OFF
SET FEEDBACK OFF
SET TAB OFF

 
-- Set the line length to the maximum possible, but trim 
-- trailing spaces for both screen output and spool files
SET LINESIZE 32767
SET WRAP ON
SET TRIMOUT ON
SET TRIMSPOOL ON




/* Define the format of the output dates */
DEFINE	OutputDateFormat = 'DD-MON-YYYY HH24:MI:SS'
COLUMN	sorter	NOPRINT

SELECT Distinct '0' sorter,
       'DSTREAM_PRI_DESP_HDR'          ||'|'||
       rtrim('&&2')			||'|'||			/* Order_id */
       rtrim('&&3')							/* Container_id */
from dual


SELECT distinct '3'||sm.picked_dstamp sorter,
'DSTREAM_PRI_SWING_LABEL_HDR'		||'|'||
rtrim (ol.sku_id)			||'|'||
rtrim (sm.qty_picked)
from order_line ol, shipping_manifest sm
where ol.order_id = '&&2'
and ol.qty_picked > '0'
and ol.order_id = sm.order_id
and ol.sku_id = sm.sku_id
and (sm.container_id like '&&3%'
or sm.container_id is null)
union all
SELECT distinct '3'||sm.picked_dstamp sorter,
'DSTREAM_PRI_SWING_LABEL_LINE'		||'|'||
rtrim (ol.sku_id)			||'|'||
rtrim (s.description)			||'|'||
rtrim (s.each_value)			||'|'||
rtrim (s.colour)			||'|'||
rtrim (s.ean)
from order_line ol, sku s, shipping_manifest sm
where ol.order_id = '&&2'
and ol.qty_picked > '0'
and ol.sku_id = s.sku_id
and ol.order_id = sm.order_id
and ol.sku_id = sm.sku_id
and (sm.container_id like '&&3%'
or sm.container_id is null)
order by 1,2
/
