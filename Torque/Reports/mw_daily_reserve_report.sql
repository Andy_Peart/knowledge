SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 332
SET TRIMSPOOL ON


--spool 778.csv

select 
A "SKU_ID",
C "Life Reservation"
from (select 
A,
DECODE(SIGN(to_number(s.user_def_num_4)-B),-1,to_number(s.user_def_num_4),B) C
from sku s,
(select iv.sku_id A, sum(iv.qty_on_hand) B
from inventory iv,location ll
where iv.client_id = 'MOUNTAIN'
and iv.location_id=ll.location_id
and ll.loc_type='Tag-FIFO'
and ll.site_id='BD2'
and iv.sku_id <> 'TEST'
group by iv.sku_id)
where s.sku_id = A
and s.client_id = 'MOUNTAIN'
and A <> 'TEST')
where C > 0
/

--spool off

