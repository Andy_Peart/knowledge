SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

 
with bulk_stock as (
select 
case 
  when instr(upper(sku.description), 'SMOOTHIE')>0 then 'Smoothie'
  when instr(upper(sku.description), 'LUXE')>0 then 'Luxe'
  when instr(upper(sku.description), 'DAILY DREAM')>0 then 'Daily Dream'
  when instr(upper(sku.description), 'VIXEN')>0 then 'Vixen'
  when instr(upper(sku.description), 'DESIRE')>0 then 'Desire'
  when instr(upper(sku.description), 'ROXIE')>0 then 'Roxie'
  when instr(upper(sku.description), 'TEASE')>0 then 'Tease'
end style_name,
sum(inv.qty_on_hand) qty 
from inventory inv inner join sku on inv.client_id = sku.client_id and inv.sku_id = sku.sku_id
inner join location loc on inv.site_id = loc.site_id and loc.location_id = inv.location_id
where inv.client_id = 'CK'
and loc.loc_type = 'Tag-FIFO'
and inv.location_id <> 'CK6000'
and 
(
upper(sku.description) like '%SMOOTHIE%BRA%'
or
upper(sku.description) like '%LUXE%BRA%'
or
upper(sku.description) like '%DAILY DREAM%BRA%'
or
upper(sku.description) like '%VIXEN%BRA%'
or
upper(sku.description) like '%DESIRE%BRA%'
or
upper(sku.description) like '%ROXIE%BRA%'
or
upper(sku.description) like '%TEASE%BRA%'
)
group by case 
  when instr(upper(sku.description), 'SMOOTHIE')>0 then 'Smoothie'
  when instr(upper(sku.description), 'LUXE')>0 then 'Luxe'
  when instr(upper(sku.description), 'DAILY DREAM')>0 then 'Daily Dream'
  when instr(upper(sku.description), 'VIXEN')>0 then 'Vixen'
  when instr(upper(sku.description), 'DESIRE')>0 then 'Desire'
  when instr(upper(sku.description), 'ROXIE')>0 then 'Roxie'
  when instr(upper(sku.description), 'TEASE')>0 then 'Tease'  
end
),
all_stock as (
select sku.description, sum(inv.qty_on_hand) qty 
from inventory inv inner join sku on inv.client_id = sku.client_id and inv.sku_id = sku.sku_id
inner join location loc on inv.site_id = loc.site_id and loc.location_id = inv.location_id
where inv.client_id = 'CK'
and loc.loc_type = 'Tag-FIFO'
and inv.location_id <> 'CK6000'
group by sku.description
)
select * from bulk_stock
union all
select 'Total Bulk stock' stock, sum(qty) qty
from bulk_stock
union all
select 'All stock' stock, sum(qty) qty
from all_stock
/