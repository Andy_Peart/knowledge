SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep '|'


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 300
SET TRIMSPOOL ON



 
column XX NOPRINT

select 'SKU, Location, Work Group, Zone, Consignment, Qty' from dual
union all
select sku_id ||','|| from_loc_id ||','|| work_group ||','|| Work_Zone ||','|| consignment ||','|| qty
from (
select mt.sku_id, mt.from_loc_id, oh.work_group, mt.Work_Zone, oh.consignment, sum(mt.qty_to_move) as qty
from order_header oh,move_task mt
where oh.client_id='MOUNTAIN'
and oh.consignment like '%&&2%'
and mt.client_id='MOUNTAIN'
and oh.order_id=mt.task_id
and mt.work_zone <> 'WEB'
group by mt.sku_id, mt.from_loc_id, oh.work_group, mt.Work_Zone, oh.consignment
order by mt.from_loc_id
)
/
