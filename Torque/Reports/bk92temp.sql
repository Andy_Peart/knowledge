SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON

column A heading 'Date' format a12
column B heading 'Orders' format 99999999
column C heading 'Units Ordered' format 99999999
column D heading 'Units Tasked' format 99999999
column E heading 'Units Picked' format 99999999
column F heading 'Units Shipped' format 99999999


--TTITLE LEFT 'Orders Analysis for client &&2 from &&3 to &&4' skip 1
 
set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DAY, DD MONTH, YYYY') curdate from DUAL;
set TERMOUT ON


break on report

compute sum label 'Total' of B C D E F on report

--spool bk92temp.csv

select 'Orders Analysis for client &&2 from &&3 to &&4' from DUAL;

select 'Date,Orders,Units Ordered,Units Tasked,Units Picked,Units Shipped' from DUAL;

select 
AA A,
sum(BBB) B,
sum(CCC) C,
sum(HH-JJ) D,
sum(EEE) E,
sum(FFF) F
from
(select
to_char(oh.creation_date,'DD/MM/YY') AA,
oh.order_id OOO,
count(distinct oh.order_id) BBB,
sum(ol.qty_ordered) CCC,
nvl(sum(distinct AAA),0) HH,
nvl(sum(distinct JJJ),0) JJ,
nvl(sum(ol.qty_picked),0) EEE,
nvl(sum(ol.qty_shipped),0) FFF
from order_header oh,order_line ol,
(select
reference_id DDD,
sum(update_qty) AAA
from inventory_transaction it
where it.client_id='&&2'
and code='Allocate'
and from_loc_id<>'CONTAINER'
and reference_id in
(select
order_id
from order_header oh
where oh.client_id='&&2'
and oh.creation_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1)
--and oh.status in ('Shipped'))
group by
reference_id),
(select
reference_id DDDD,
sum(update_qty) JJJ
from inventory_transaction it
where it.client_id='&&2'
and code='Deallocate'
and from_loc_id<>'CONTAINER'
and reference_id in
(select
order_id
from order_header oh
where oh.client_id='&&2'
and oh.creation_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1)
--and oh.status in ('Shipped'))
group by
reference_id)
where oh.client_id='&&2'
and oh.creation_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
--and oh.status in ('Shipped')
and oh.client_id=ol.client_id
and oh.order_id=ol.order_id
and oh.order_id=DDD (+)
and oh.order_id=DDDD (+)
group by
to_char(oh.creation_date,'DD/MM/YY'),
oh.order_id)
group by
AA
order by
AA
/

--spool off
