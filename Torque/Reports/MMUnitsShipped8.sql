/******************************************************************************/
/*                                                                            */
/*                            Elite                                           */
/*                                                                            */
/*     FILE NAME  :   mwshortages3.sql                                        */
/*                                                                            */
/*   DATE     BY   PROJ     ID         DESCRIPTION                            */
/*   ======== ==== ======   ========   =============                          */
/*   13/08/07 BK   MM                  Units Shipped by Date (Date Range)     */
/******************************************************************************/

/* Input Parameters are as follows */
-- 1)  From Date
-- 2)  To Date


set feedback off
set pagesize 68
set linesize 240
set newpage 0
set verify off

clear columns
clear breaks
clear computes

column X heading '' format A14
column A heading 'Date' format A16
column B heading 'Qty' format 999999
column Z heading 'Order Type' format A20
column C heading 'Shipments' format 999999
column M format a4 noprint

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'Units Shipped from '&&2' to '&&3' - for client ID MM as at ' report_date skip 2

break on X skip 2 on report
compute sum label 'Totals' of C B on X
compute sum label 'Grand Totals' of C B on report

select
'MM ' X,
to_char(itl.Dstamp, 'YYMM') M,
to_char(itl.Dstamp, 'DD/MM/YY') A,
oh.order_type Z,
count(*) C,
sum(itl.update_qty) B
from inventory_transaction itl, order_header oh
where itl.client_id='MM'
and itl.Reference_id = oh.order_id
and itl.code='Shipment'
and (itl.sku_id not like 'V%'
or itl.sku_id like 'VY%')
and itl.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by
to_char(itl.Dstamp, 'YYMM'),
to_char(itl.Dstamp, 'DD/MM/YY'),
oh.order_type
union
select
'VH ' X,
to_char(itl.Dstamp, 'YYMM') M,
to_char(itl.Dstamp, 'DD/MM/YY') A,
oh.order_type Z,
count(*) C,
sum(itl.update_qty) B
from inventory_transaction itl, order_header oh
where itl.client_id='MM'
and itl.Reference_id = oh.order_id
and itl.code='Shipment'
and itl.sku_id like 'V%'
and itl.sku_id not like 'VY%'
and itl.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by
to_char(itl.Dstamp, 'YYMM'),
to_char(itl.Dstamp, 'DD/MM/YY'),
oh.order_type
order by
X,M,A
/
