SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 9999
--SET NEWPAGE 0
SET LINESIZE 9999
SET TRIMSPOOL ON

--spool ghquickie.csv

select
*
from order_header
where client_id='&&2'
and status='Shipped'
and order_type like '&&5%'
and shipped_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
/

--spool off
