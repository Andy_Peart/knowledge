SET FEEDBACK OFF                 
set pagesize 999
set linesize 160
set verify off

clear columns
clear breaks
clear computes

column A heading 'Order ID ' format a16
column A1 heading 'Zone ' format a8
column B heading 'Units' format 999999
column C heading 'Lines' format 999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'Units Allocated per Zone on &&2 to &&3 - for client ID MOUNTAIN as at ' report_date skip 2

break on A skip 1

compute sum label 'Totals' of B on report

select
it.reference_id A,
ll.zone_1 A1,
sum(it.update_qty) as B,
count(*) as C
from inventory_transaction it,location ll
where it.client_id='MOUNTAIN'
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.code='Allocate'
and it.from_loc_id=ll.location_id
and it.reference_id like '&&4%'
group by
it.reference_id,
ll.zone_1
order by
it.reference_id,
ll.zone_1
/
