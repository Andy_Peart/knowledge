SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

column A heading 'SKU' format A18
column B heading 'Date' format A12
column C heading 'Description' format A40
column D heading 'Qty' format 99999
column E heading 'Tag' format A12


select ',,WRITE OFF REPORT' from DUAL;
select 'Client &&2,,From &&3 to &&4' from DUAL;
select 'SKU,Date,Description,Tag,Qty' from DUAL;
 
set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DAY, DD MONTH, YYYY') curdate from DUAL;
set TERMOUT ON


--break on report 

--compute sum label 'Total' of D on report


select
''''||it.sku_id||'''' A,
to_char(it.Dstamp,'DD/MM/YY') B,
sku.description C,
it.tag_id E,
--sum(it.update_qty) D
it.update_qty D
from inventory_transaction it,sku
where it.client_id='&&2'
and it.Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and it.from_loc_id='SUSPENSE'
and it.to_loc_id='SUSPENSE'
and it.reason_id='SUSP'
and it.sku_id=sku.sku_id
and it.client_id=sku.client_id
--group by
--it.sku_id,
--to_char(it.Dstamp,'DD/MM/YY'),
--sku.description 
--order by
--A,B
/
