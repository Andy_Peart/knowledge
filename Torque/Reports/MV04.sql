SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','
SET WRAP OFF


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500

column A heading 'Order' format A12
column B heading 'Line' format 99999
column C heading 'Order Date' format A12
column D heading 'Creation Date' format A12
column E heading 'Customer' format A20
column F heading 'SKU ID' format A18
column G heading 'Description' format A40
column H heading 'Colour' format A20
column J heading 'Size' format A12
column K heading 'User Def Type 2' format A20
column L heading 'User Def Type 5' format A20
column M heading 'Ordered' format 999999
column N heading 'Shipped' format 999999
column P heading 'Shipped Date' format A12
column Q heading 'Name' format A20
column R heading 'Address 1' format A32
column S heading 'Address 2' format A24
column T heading 'Town' format A12
column U heading 'Postcode' format A12
column V heading 'Country' format A28
column W heading 'Price' format 9999.99
column X heading 'User Def Note 1' format A60
column Y heading 'User Def Note 2' format A24


Select 'Order,Line,Order Date,Creation Date,Description,Colour,Size,User Def Type 1,User Def Type 5,Qty Ordered,Qty Shipped,Shipped Date,Name,Adress 1,Postcode,Country,SKU,Customer,Each Value,user_def_note_1,user_def_note_2' from DUAL;

--break on D dup

--compute sum label 'Order Total' of U V W on D

select
ol.order_id A,
ol.line_id B,
to_char(oh.order_date,'DD-Mon-YYYY') C,
to_char(oh.creation_date,'DD-Mon-YYYY') D,
sku.description G,
sku.colour H,
sku.sku_size J,
sku.User_Def_Type_1 K,
sku.User_Def_Type_5 L,
ol.qty_ordered M,
ol.qty_shipped N,
--it.update_qty N,
to_char(oh.shipped_date,'DD-Mon-YYYY') P,
oh.name Q,
oh.address1 R,
--oh.address2 S,
--oh.town T,
oh.postcode U,
nvl(country.tandata_id,oh.country) V,
''''||ol.sku_id||'''' F,
oh.customer_id E,
nvl(sku.each_value,0) W,
'"'||sku.user_def_note_1||'"' X,
sku.user_def_note_2 Y
from order_header oh,order_line ol,sku,country
where oh.client_id='V'
and oh.shipped_date>sysdate-1
and ol.client_id='V'
and oh.order_id=ol.order_id
and sku.client_id='V'
and ol.sku_id=sku.sku_id
and oh.country=country.iso3_id (+)
order by
A,B
/
