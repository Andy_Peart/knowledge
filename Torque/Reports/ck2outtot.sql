SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 160
SET TRIMSPOOL ON

--ttitle 'CURVY KATE Despatch Quantities from &&2 to &&3' skip 2

column A heading 'Date' format a12
column B heading 'Orders' format 99999999
column C heading 'Units' format 99999999
column D heading 'Stock' format 99999999
column E heading 'Hangers' format 99999999
column M format a4 noprint


break on report

compute sum label 'Totals' of B C D E on report

--spool ck2outtot.csv

select 'Date,Orders,Units,Stock,Hangers' from DUAL;



select
trunc(dstamp) A,
count(distinct reference_id) B,
nvl(B2,0) C,
nvl(B2,0)-nvl(B1,0) D,
nvl(B1,0) E
from inventory_transaction,(select
trunc(dstamp) A1,
--count(distinct reference_id) G,
sum (update_qty) B1
from inventory_transaction
where client_id = 'CK'
and code = 'Shipment'
and dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1
and sku_id in ('5052574061487','5052574061470')
group by
trunc(Dstamp)),(select
trunc(dstamp) A2,
--count(distinct reference_id) G,
sum (update_qty) B2
from inventory_transaction
where client_id = 'CK'
and code = 'Shipment'
and dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1
group by
trunc(Dstamp))
where client_id = 'CK'
and code = 'Shipment'
and update_qty>0
and dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1
and trunc(Dstamp)=A1 (+)
and trunc(Dstamp)=A2 (+)
group by
trunc(Dstamp),
nvl(B2,0),
nvl(B1,0)
/


--spool off

