SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON
SET HEADING ON

select 'Sku,Tag,Descritpion,Date,Location,Locked Code,Qty Locked' from dual
union all
select ''''|| "Sku" || ''',' || "Tag" || ',' || "Description" || ',' || "Date" || ',' || "Location" || ',' || "Locked Code" || ',' ||  "Qty Locked"
from
(
with main as (
select i.sku_id         as "Sku"
, i.tag_id              as "Tag"
, i.description         as "Desc"
, max(trunc(it.dstamp)) as "Date"
, i.location_id         as "Location"
, i.lock_code           as "Locked Code"
, i.client_id           as "Client"
from inventory i
inner join inventory_transaction it on it.client_id = i.client_id and it.sku_id = i.sku_id and it.tag_id = i.tag_id
where i.client_id = 'MOUNTAIN'
and it.code = 'Inv Lock'
and i.lock_status = 'Locked'
group by i.sku_id
, i.tag_id
, i.description
, i.location_id
, i.lock_code
, i.client_id
union all
select i.sku_id         as "Sku"
, i.tag_id              as "Tag"
, i.description         as "Desc"
, max(trunc(it.dstamp)) as "Date"
, i.location_id         as "Location"
, i.lock_code           as "Locked Code"
, i.client_id           as "Client"
from inventory i
inner join inventory_transaction_archive it on it.client_id = i.client_id and it.sku_id = i.sku_id and it.tag_id = i.tag_id
where i.client_id = 'MOUNTAIN'
and it.code = 'Inv Lock'
and i.lock_status = 'Locked'
group by i.sku_id
, i.tag_id
, i.description
, i.location_id
, i.lock_code
, i.client_id
), locked as(
select sum(qty_on_hand) as "Locked"
, client_id             as "Client"
, sku_id                as "Sku"
, tag_id                as "Tag"
, location_id           as "Location"
from inventory
where client_id = 'MOUNTAIN'
and lock_status = 'Locked'
group by client_id
, sku_id
, tag_id
, location_id
)
select main."Sku"       as "Sku"
, main."Tag"            as "Tag"
, main."Desc"           as "Description"
, max(main."Date")      as "Date"
, main."Location"       as "Location"
, main."Locked Code"    as "Locked Code"
, locked."Locked"       as "Qty Locked"
from main
inner join locked on locked."Client" = main."Client" and locked."Sku" = main."Sku" and locked."Tag" = main."Tag" and locked."Location" = main."Location"
group by main."Sku"
, main."Tag"
, main."Desc"
, main."Location"
, main."Locked Code"
, locked."Locked"
order by 4
)
/