SET FEEDBACK OFF                
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 400
SET TRIMSPOOL ON

column M heading ' ' noprint
break on M skip 2

spool ckrm.csv

select 'RECEIPTS for LAST MONTH' from DUAL;
select 'Date,Order No,Customer,Sku,Description,Colour,Size,Qty' from DUAL;

select 
decode(i.sku_id,'205163119000',2,'205164001000',2,1) M,
to_char(i.dstamp, 'DD/MM/YY')||','
||i.reference_id||','
||i.customer_id||','''
||i.sku_id||''','
||sku.description||','
||sku.colour||','
||sku.sku_size||','
||i.update_qty
from inventory_transaction i,sku
where i.client_id = 'CK'
and i.code = 'Receipt'
and to_char(i.Dstamp,'MMYY')=to_char(sysdate-1,'MMYY')
and i.sku_id=sku.sku_id
and sku.client_id = 'CK'
order by 
M,
i.dstamp,
i.sku_id
/

--spool off

