SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Shipments from &&2 &&4 to &&3 &&5' from dual 
union all
select 'Date,Store,Order Id,Lines,Units,Average UPL' from dual
union all
select "Date" || ',' || "Consignment" || ',' || "Store" || ',' || "Order Id" ||',' || "Lines" || ',' || "Units" || ',' || "Average UPL"
from
(
select trunc(oh.shipped_date)                                   as "Date"  
, oh.consignment												as "Consignment"
, oh.work_group                                                 as "Store"
, oh.order_id                                                   as "Order Id"
, count(ol.qty_shipped)                                         as "Lines"
, sum(ol.qty_shipped)                                           as "Units"
, to_char(sum(ol.qty_shipped) / count(ol.qty_shipped),'99.99')  as "Average UPL"
from order_header oh
inner join order_line ol on ol.order_id = oh.order_id and ol.client_id = oh.client_id
where oh.client_id = 'MOUNTAIN'
and substr(oh.work_group,2,2) <> 'WW'
and oh.status = 'Shipped'
and oh.shipped_date between to_date('&&2 &&4', 'DD-MON-YYYY HH24:MI:SS') and to_date('&&3 &&5', 'DD-MON-YYYY HH24:MI:SS')+1
and nvl(ol.qty_shipped,0) > 0
group by trunc(oh.shipped_date)
, oh.work_group
, oh.order_id
, oh.consignment
order by 2
)
/
--spool off