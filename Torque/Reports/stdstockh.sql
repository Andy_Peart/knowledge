SET FEEDBACK OFF
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240
SET TRIMSPOOL ON

--SET TERMOUT OFF

break on A skip 1


--spool stdstockh.csv


select nvl(name,client_id) from client where client_id='&&2'
union all
select 'Location Type,Qty On Hand,Qty Unallocatable,Qty Allocated,Qty Available' from DUAL
union all
select 
A1||','||
A2||','||
A3||','||
A4||','||
A5
from (select
distinct
loc_type A1,
nvl(BB,0) A2,
nvl(DD,0) A3,
nvl(CC,0) A4,
nvl(BB,0)-nvl(DD,0)-nvl(CC,0) A5
from location,(select
lc.loc_type AA,
nvl(sum(jc.qty_on_hand),0) BB,
nvl(sum(jc.qty_allocated),0) CC
from inventory jc,location lc
where jc.client_id like '&&2'
and jc.location_id=lc.location_id
and jc.site_id=lc.site_id
group by
lc.loc_type),(select
lc.loc_type AAA,
nvl(sum(jc.qty_on_hand),0)-nvl(sum(jc.qty_allocated),0) DD
from inventory jc,location lc
where jc.client_id like '&&2'
and jc.location_id=lc.location_id
and jc.site_id=lc.site_id
and lc.disallow_alloc='Y'
group by
lc.loc_type)
where location.loc_type=AA (+)
and location.loc_type=AAA (+)
order by 1)
union all
select 
A1||','||
A2||','||
A3||','||
A4||','||
A5
from (select
AA A1,
BB A2,
CC A4,
DD A3,
BB-DD-CC A5
from (select
'Totals' AA,
nvl(sum(jc.qty_on_hand),0) BB,
nvl(sum(jc.qty_allocated),0) CC
from inventory jc
where jc.client_id like '&&2'),(select
nvl(sum(jc.qty_on_hand),0)-nvl(sum(jc.qty_allocated),0) DD
from inventory jc,location lc
where jc.client_id like '&&2'
and jc.location_id=lc.location_id
and jc.site_id=lc.site_id
and lc.disallow_alloc='Y'))
/


--spool off

