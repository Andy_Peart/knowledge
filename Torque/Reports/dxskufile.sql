SET FEEDBACK OFF             
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 148
SET TRIMSPOOL ON

--SET TERMOUT OFF

--spool dxskufile.csv


select 'SKU,EAN,Description' from DUAL;


select
--'"'||sku_id||'",'||
--'"'||EAN||'",'||
--'"'||Description||'"'
sku_id||','||
EAN||','||
Description
from sku
where client_id='DX'
order by
sku_id
/

--spool off
--SET TERMOUT ON


