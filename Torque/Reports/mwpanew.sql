SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 999
SET TRIMSPOOL ON


--Please create a report that looks at pre-advice lines user defined type 4. 
--If this field states NEW1 please report if existing skus are in stock and which location 
--and work zone they are in. The report needs to allow to the to enter a creation date range. 

--spool mwpanew.csv


select 'NEW1 Pre Advice items report for MOUNTAIN on '||trunc(sysdate) from DUAL
union all
select 'Pre-Advice ID,Sku Id,Qty on Hand,Location,Work Zone' from DUAL
union all
select * from (select
ph.Pre_Advice_ID||','||
pl.sku_id||','||
QQ||','||
LL||','||
WZ
from pre_advice_header ph,pre_advice_line pl,(select
iv.sku_id SS,
iv.location_id LL,
lc.work_zone WZ,
sum(iv.qty_on_hand) QQ
from inventory iv,location lc
where iv.client_id='MOUNTAIN'
and iv.location_id=lc.location_id
and iv.site_id=lc.site_id
and lc.loc_type='Tag-FIFO'
group by
iv.sku_id,
iv.location_id,
lc.work_zone)
where ph.client_id = 'MOUNTAIN'
and trunc(ph.creation_date) between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and ph.pre_advice_id=pl.pre_advice_id
and ph.client_id=pl.client_id
and pl.user_def_type_4='NEW1'
and pl.sku_id=SS
order by
ph.Pre_Advice_ID,
pl.sku_id,
LL)
/


--spool off
