SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Sku,Description,Qty On Hand,Location Id,Work Zone' from dual
union all
select '''' || "Sku" || ''',' || "Description" || ',' || "Qty on Hand" || ',' || "Location id" || ',' || "Work Zone"
from
(
with t1 as (                /*Marking every sku thats located outside Web1/2 with an 'x'*/
select i.sku_id as "Sku"
, i.client_id as "Client"
, 'x' as "x"
from inventory i
inner join location l on l.site_id = i.site_id and l.location_id = i.location_id
where client_id = 'MOUNTAIN'
and l.work_zone not in (
'AMW'
, 'ZMW'
, '1WOPS'
, 'WEB1'
, 'WEB2'
)), t2 as (                             /*Making sure totals for every sku is <= 30*/
select i.sku_id         as "Sku"
, i.client_id           as "Client"
, sum(i.qty_on_hand)
from inventory i
inner join location l on l.location_id = i.location_id and l.site_id = i.site_id
where i.client_id = 'MOUNTAIN'
and l.work_zone in (
'WEB1'
, 'WEB2')
group by i.sku_id
, i.client_id
having sum(i.qty_on_hand) <= 30
)
select t2."Sku"         as "Sku"
, i.description         as "Description"
, sum(i.qty_on_hand)    as "Qty on Hand"
, i.location_id         as "Location id"
, l.work_zone           as "Work Zone"
from inventory i
left join t1 on t1."Sku" = i.sku_id and t1."Client" = i.client_id
inner join location l on l.site_id = i.site_id and l.location_id = i.location_id
inner join t2 on t2."Sku" = i.sku_id and t2."Client" = i.client_id
where i.client_id = 'MOUNTAIN'
and t1."x" is null
and l.work_zone not in (
'AMW'
, 'ZMW'
, '1WOPS'
)
group by t2."Sku"
, i.description
, i.location_id
, l.work_zone
having sum(i.qty_on_hand) <= 30
order by 1
)
/
--spool off