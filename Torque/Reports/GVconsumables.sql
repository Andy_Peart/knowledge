SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON


column A heading 'SKU' format a18
column A1 heading 'EAN' format a20
column A2 heading 'Description' format a40
column A3 heading 'Location' format a10
column B heading 'Qty' format 999999


select 'SKU,EAN,Description,Location,Qty,' from DUAL;

select
sku.sku_id A,
''''||sku.EAN||'''' A1,
sku.description A2,
nvl(LL,'') A3,
nvl(QQ,0) B,
' '
from sku,(select 
sku_id SS,
location_id LL,
sum(qty_on_hand) QQ
from inventory
where client_id='GV'
group by
sku_id,
location_id)
where sku.client_id='GV'
and sku.sku_id like 'C%'
and sku.sku_id=SS (+)
order by
sku.sku_id
/
 
 

