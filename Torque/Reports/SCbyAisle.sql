SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240


column Z heading 'Client' format A8
column Y heading 'Location' format A12
column A heading 'SKU' format A18
column B heading 'EAN' format A16
column BB heading 'UPC' format A16
column C heading 'Variance' format 999999
column D heading 'Colour' format a20
column E heading 'Description' format a40

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


select 'Location,&&5' from DUAL;
select 'Client,SKU,EAN,UPC,Variance,Colour,Description' from DUAL;

--break on BB dup skip 1 on report
break on report

--compute sum label 'Total' of DD on BB
compute sum label 'Totals' of F G H on report

select * from
(select
i.client_id Z,
--i.from_loc_id Y,
''''||i.sku_id||'''' A,
''''||sku.ean||'''' B,
''''||sku.upc||'''' BB,
sum(i.update_qty) C,
sku.colour D,
sku.description E
from inventory_transaction i,sku
where i.client_id='&&2'
and i.code='Stock Check'
and i.from_loc_id like '&&5%'
and i.Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and sku.client_id='&&2'
and i.sku_id=sku.sku_id
group by
i.client_id,
--i.from_loc_id,
i.sku_id,
sku.ean,
sku.upc,
sku.colour,
sku.description)
where C>0
order by
A
/
