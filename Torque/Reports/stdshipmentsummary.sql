SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240
SET TRIMSPOOL ON

--spool shipmentsummary.csv

select 'Shipments Summary Report - for client ID &&2 from &&3 to &&4 - as at  ' 
    || to_char(sysdate,'dd/mm/yy  hh24:mi:ss') from dual;
		   
select 'Date,Client Id,Reference Id,Order Type,Seller,Priority,Despatch Method,Qty Ordered,Qty Shipped,Differance,Customer Name,Customer id,Creation Date' from dual;


select
A1||','||
A2||','||
A3||','||
A4||','||
A5||','||
A6||','||
A7||','||
A8||','||
A9||','||
A10||','||
A11||','||
customer_id||','||
A12
from (select
to_char(oh.shipped_date, 'DD-MM-YYYY') A1,
OH.client_id A2,
OH.order_id A3,
oh.order_type A4,
oh.seller_name A5,
oh.priority A6,
oh.dispatch_method A7,
sum(ol.qty_ordered) A8,
sum(ol.qty_shipped) A9,
sum(ol.qty_ordered)-sum(ol.qty_shipped) A10,
oh.name A11,
oh.customer_id,
to_char(oh.creation_date, 'DD-MM-YYYY') A12
from order_header oh inner join order_line ol on oh.order_id = ol.order_id
where oh.client_id='&&2'
and ol.client_id='&&2'
and oh.shipped_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
group by
to_char(oh.shipped_date, 'DD-MM-YYYY'),
OH.client_id,
OH.order_id,
oh.order_type,
oh.seller_name,
oh.priority,
oh.dispatch_method,
oh.name,
oh.customer_id,
to_char(oh.creation_date, 'DD-MM-YYYY')
order by
oh.order_id)
/

--spool off
