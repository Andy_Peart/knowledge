SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

column A heading 'Client ID' format A12
column B heading 'SKU' format A18
column C heading 'Description' format A40
column D heading 'Location' format A12
column E heading 'Qty on Hand' format 999999
column F heading 'Qty Allocated' format 999999
column G heading 'Rec Date' format a12
column H heading 'Rec Time' format a12


select 'Client ID,SKU,Description,Location,Qty on Hand,Qty Allocated,Rec. Date,Rec. Time' from DUAL;

select
it.client_id A,
''''||it.sku_id||'''' B,
sku.description C,
it.location_id D,
it.qty_on_hand E,
it.qty_allocated F,
to_char(it.Receipt_Dstamp, 'DD/MM/YYYY') G,
to_char(it.Receipt_Dstamp, 'HH:MI:SS') H
from inventory it,sku
where it.client_id='&&2'
and sku.client_id='&&2'
and it.tag_id is null
and it.sku_id=sku.sku_id
order by B
/

--select
--'Total Records =  '||count(*)
--from inventory it
--where it.client_id='&&2'
--and it.tag_id is null

