SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON

col aa format A12
col bb format A15
col cc format A12
col dd format A10
col ee format A30
col hh format A15

PROMPT "Shipped Dates","Order ID","TO Number","Customer ID","Name","Cartons","Container","SKU","Qty"
SELECT TRUNC(oh.shipped_date) as AA, 
itl.reference_id as BB,
oh.purchase_order as CC,
oh.customer_id as DD,
oh.name as EE,
COUNT( DISTINCT itl.container_id) as FF,
itl.container_id as GG,
itl.sku_id as HH,
sum(itl.update_qty) as II
FROM inventory_transaction itl
LEFT JOIN order_header oh
ON itl.reference_id  = oh.order_id
AND itl.client_id = oh.client_id
WHERE itl.client_id = '&2'
AND itl.dstamp 
BETWEEN to_date('&3', 'DD-MON-YYYY') 
AND to_date('&4', 'DD-MON-YYYY')+1
AND itl.code = 'Shipment'
GROUP BY oh.shipped_date,
itl.reference_id,
oh.purchase_order,
oh.customer_id,
oh.name,
itl.container_id,
itl.sku_id
ORDER BY 1, 4
/