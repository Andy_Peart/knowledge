SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 2000
SET TRIMSPOOL ON

select 'Report for Pre-advice: &&3' from dual
union all
select 'SKU,Description,Line Id,User def type1,User def type2,Qty due,Qty received,Discrepancy' from dual
union all
select sku_id || ',' || sku_desc || ',' || line_id || ',' || user_def_type_1 || ',' || user_def_type_2 || ',' || qty_due || ',' || qty_rec || ',' || discrep
from (
select sku_id, libsku.getuserlangskudesc(client_id,sku_id) sku_desc, line_id, user_def_type_1, user_def_type_2, nvl(qty_due,0) qty_due, nvl(qty_received,0) qty_rec, nvl(qty_received,0)-nvl(qty_due,0) discrep
from pre_advice_line
where client_id = '&&2'
and pre_advice_id = '&&3'
)
/