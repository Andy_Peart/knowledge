set feedback off
set pagesize 66
set linesize 120
set verify off


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

 

column A heading 'Location' format A11
column B heading 'SKU' format A30
column C heading 'Description' format A40
column D heading 'Qty' format 999999
column E heading '  Moved' format A14

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

set und '-'
set heading on
set newpage 0

ttitle  LEFT 'Stock Units by Location Range from &&2 to &&3 - for client ID &&4 as at ' report_date RIGHT 'Page:' FORMAT 999 SQL.PNO skip 4

break on report

compute sum label 'Total' of D on report

break on row skip 1

select
i.sku_id B,
Sku.description C,
i.location_id A,
i.qty_on_hand D,
'  ____________' E
from inventory i,sku
where i.client_id like '&&4'
and to_number(substr(location_id,2,5)) between &&2 and &&3
--and substr(location_id,1,1)='T'
and i.sku_id=sku.sku_id
order by i.location_id,i.sku_id
/
ttitle  LEFT ' '
set heading off
break on row skip 0
set newpage 1

select
'Location Count =',
count (*) D
from (select distinct location_id from inventory
where client_id like '&&4'
and to_number(substr(location_id,2,5)) between &&2 and &&3)
--and substr(location_id,1,1)='T')
/


select
'     SKU Count =',
count (*) D
from (select distinct sku_id from inventory
where client_id like '&&4'
and to_number(substr(location_id,2,5)) between &&2 and &&3)
--and substr(location_id,1,1)='T')
/

select
'    Unit Count =',
nvl(sum(qty_on_hand),0) D
from inventory
where client_id like '&&4'
and to_number(substr(location_id,2,5)) between &&2 and &&3
--and substr(location_id,1,1)='T'
/

