SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 99
SET NEWPAGE 0
SET LINESIZE 300
SET TRIMSPOOL ON


column A1 heading 'Date' format a20
column A2 heading 'Client' format a8
column A3 heading 'Code' format a32
column B heading 'Key' format 9999999999

select
trunc(Dstamp) A1,
client_id A2,
code A3,
max(key) B
from inventory_transaction
where client_id in ('T','TR')
and trunc(Dstamp)=to_date('&&2', 'DD-MON-YYYY')
and code in ('Receipt','Preadvice Status','Order Status',
'Shipment','Relocate','Condition Update','Adjustment')
group by
trunc(Dstamp),
client_id,
code
order by
trunc(Dstamp),
client_id,
code
/
