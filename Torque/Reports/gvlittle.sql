SET FEEDBACK OFF                
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 66
SET NEWPAGE 0
SET LINESIZE 120
SET TRIMSPOOL ON

column AA Heading 'SKU ID' format A16
column AB Heading 'Description' format A40
column BB Heading 'Store' format A40
column CC Heading 'Store Name' format A32
column SS Heading 'Consignment' format A12
column QQ Heading 'Qty' format 9999
column TT Heading 'Order Type' format A14
column F format A10 noprint

break on BB dup skip 1 on report

compute sum label 'Total' of QQ on report

ttitle '     GIVE Orders for Consignment &&2' RIGHT 'Page:' FORMAT 999 SQL.PNO skip 2



Select
--oh.consignment SS,
oh.ship_dock||' - '||ad.name BB,
ol.sku_id AA,
ol.qty_ordered QQ,
sku.description AB
from order_header oh,order_line ol,sku,address ad
where oh.client_id='GV'
and oh.consignment like '&&2'
and ol.client_id='GV'
and oh.order_id=ol.order_id
and sku.client_id='GV'
and ol.sku_id=sku.sku_id
and sku.user_def_chk_4='Y'
and oh.ship_dock=ad.address_id
order by
ol.sku_id
/
