set pagesize 68
set linesize 120
set verify off

ttitle 'Client &&4 - Warehouse Intake Quantity' skip 2

column A heading 'Date' format a8
column B heading 'Qty Receipted' format 99999999
column M format a4 noprint


break on report

compute sum label 'Total' of B on report

select 
to_char(Dstamp, 'YYMM') M,
to_char(dstamp, 'DD/MM/YY') A,
sum (update_qty) B
from inventory_transaction
where client_id = '&&4'
and code = 'Putaway'
and dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1 
group by 
to_char(Dstamp, 'YYMM'),
to_char(dstamp, 'DD/MM/YY')
order by 
M,A
/
