SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ''


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 900
SET TRIMSPOOL ON
 
column XX NOPRINT

--spool ascendia.csv

select order_id||','||
to_char(sysdate,'DDMMYYYY')||','||
contact||','||
name||','||
address1||','||
address2||','||
town||','||
county||','||
postcode||','||
country.iso2_id||','||
contact_phone||','||
contact_email||','||
contact_mobile||','||
'1'||','||
user_def_note_1||','||
user_def_note_2||','||
purchase_order||','||
'Torque Martland Park Challenge Way Wigan WN5 0LD'
from order_header oh inner join country on oh.country = country.iso3_id
where oh.client_id='UT'
and oh.consignment='&&2'
/

--spool off
