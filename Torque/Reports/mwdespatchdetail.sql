SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 400
SET TRIMSPOOL ON



select 'STORE CODE,ORDER NO,SKU,T2T NO,QTY,DESCRIPTION,CONTAINER ID,LOCATION ID' from DUAL;



select
it.work_group||','||
it.reference_id||','''||
it.sku_id||''','||
substr(REF,1,24)||','||
it.update_qty||','||
sku.description||','||
it.container_id||','||
it.from_loc_id
from inventory_transaction it,sku,(select
it.tag_id TAG,
ph.notes REF
from inventory_transaction it,pre_advice_header ph
where it.client_id = 'MOUNTAIN'
and it.code='Receipt'
and ph.client_id = 'MOUNTAIN'
and it.reference_id=ph.pre_advice_id
union
select
it.tag_id TAG,
ph.notes REF
from inventory_transaction_archive it,pre_advice_header ph
where it.client_id = 'MOUNTAIN'
and it.code='Receipt'
and ph.client_id = 'MOUNTAIN'
and it.reference_id=ph.pre_advice_id)
where it.client_id = 'MOUNTAIN'
and it.reference_id='&&2'
and it.code='Pick'
and to_loc_id='CONTAINER'
and sku.client_id='MOUNTAIN'
and sku.sku_id=it.sku_id
and it.tag_id=TAG
order by it.sku_id
/


