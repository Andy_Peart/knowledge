SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 200
SET TRIMSPOOL ON


--spool tplocs.csv

select 'Location,SKU,Description,Qty,Last Count' from DUAL;

select
C ||','||
A ||','||
B ||','||
QQQ ||','||
DS
from (select
it.location_id C,
it.sku_id A,
sku.description B,
sum(it.qty_on_hand) QQQ,
trunc(it.count_dstamp) DS
from inventory it,sku
where it.client_id = 'TP'
and it.location_id<>'SUSPENSE'
and it.sku_id=sku.sku_id
and sku.client_id = 'TP'
group by it.location_id,it.sku_id,sku.description,trunc(it.count_dstamp))
order by 1
/

--spool off
