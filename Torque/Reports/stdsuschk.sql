SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240



select 'Client,SKU,Description,Tag,Orig Qty,Update Qty,From Loc,Date,User ID'
from dual;


select 
it.client_id ||','''||
it.sku_id ||''','||
sku.description  ||','||
it.tag_id ||','||
it.original_qty ||','||
it.update_qty ||','||
it.from_loc_id ||','||
to_char(it.Dstamp, 'DD/MM/YYYY HH:MI') ||','||
it.user_id 
from inventory_transaction it inner join sku on it.sku_id = sku.sku_id and it.client_id = sku.client_id
where it.client_id='&&2'
and it.from_loc_id <> 'SUSPENSE'
and it.to_loc_id='SUSPENSE'
and it.dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and it.sku_id=sku.sku_id
and sku.client_id='&&2'
and it.update_qty <> 0
order by it.sku_id
/
