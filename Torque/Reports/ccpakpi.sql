SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 999
SET TRIMSPOOL ON

--spool ccpakpi.csv

select 'Pre Advice KPI report for Receive Dates from &&2 to &&3' from DUAL;
select 'Pre-Advice ID,Supplier,QC Passed Date,Receive Date,Number of Cartons,Number of lines,Number of Units' from DUAL;


select
ph.Pre_Advice_ID,
ph.Supplier_id,
trunc(ph.User_Def_Date_1),
trunc(ph.actual_dstamp),
ph.User_Def_Num_1,
count(*),
sum(pl.qty_received)
from pre_advice_header ph,pre_advice_line pl
where ph.client_id = 'CC'
and ph.actual_dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and ph.pre_advice_id=pl.pre_advice_id
and ph.client_id=pl.client_id
group by
ph.Pre_Advice_ID,
ph.Supplier_id,
trunc(ph.User_Def_Date_1),
trunc(ph.actual_dstamp),
ph.User_Def_Num_1
order by
ph.Pre_Advice_ID
/


--spool off
