-- BKI Changed 20/06/2012 -- Removed filter in totals section for to_loc_id <>'MARSHALL'

SET FEEDBACK OFF                 
set verify off
set wrap off
set colsep ','

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 132
set trimspool on


column A format a18
column B format a18
column XX format a12
column C format 99999999
column D format 99999999
column R format 99990.99
column S format 99
column T format 99990.99
column A1 format a10
column Z noprint

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


--spool mw2hourly.csv

select 'PICK Tasks starting at '||to_char(SYSDATE-(1/24), 'HH24')||'00 hours on '||to_char(SYSDATE, 'DD/MM/YYYY') from DUAL;

select 'User,Tasks,Units,Units per SKU,Multiplier,Expected kpi,Zone' from DUAL;

select
A,
C,
D,
D/C R,
'100' S,
(D/C)*100 T,
A1,
CASE WHEN D>(D/C)*100 THEN 'OK' ELSE ' ' END W
from (
select
sku.user_def_type_8 A1,
it.user_id A,
count(*) C,
sum(it.update_qty) D
from inventory_transaction it inner join sku on it.sku_id = sku.sku_id
where to_number(to_char(it.dstamp,'DDMMYYYYHH24'))=to_number(to_char(sysdate,'DDMMYYYYHH24'))-1
and it.client_id='MOUNTAIN'
and sku.client_id = 'MOUNTAIN'
and it.update_qty<>0
and it.station_id not like 'Auto%'
and it.code='Pick' 
and it.work_group<>'WWW'
and it.from_loc_id not in ('CONTAINER','STG','UKPAC','UKPAR','MARSHALL')
group by sku.user_def_type_8, it.user_id
)
order by 1
/

--spool off
