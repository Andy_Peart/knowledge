SET FEEDBACK OFF
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240
SET TRIMSPOOL ON

--SET TERMOUT OFF

--Please arrange to run on the hour and e-mail out.


--spool mwweboutord.csv

select 'Creation Date/Time,Order Type,Order Date/Time,Order Id, Consignment, Ordered Units, Priority, Weight' from DUAL;

select
to_char(creation_date,'DD-MM-YYYY HH24:MI')||','||
Order_type||','||
to_char(Order_date,'DD-MM-YYYY HH24:MI')||','||
Order_id||','||
consignment||','||
order_volume*10000||','||priority||','||order_weight
from order_header
where client_id='MOUNTAIN'
and status not in ('Shipped','Cancelled','Picked')
and Order_type in ('WEB','AMZ','PLY','EBY')
and nvl(consignment,'0') not in ('SHORTAGE','PROBLEM')
order by
trunc(creation_date),
order_type,
trunc(order_date)
/

--spool off
