SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Date,Time,User,Sku,Qty,From Location,To Location,Code' from dual
union all
select "Date" || ',' || "Time" || ',' || "User" || ',' || "Sku" || ',' || "Qty" || ',' || "From Location" || ',' || "To Location" || ',' || "Code"
from
(
select to_char(dstamp, 'DD/MON')    as "Date"
, to_char(dstamp, 'HH24:MI')        as "Time"
, user_id                           as "User"
, sku_id                            as "Sku"
, sum(nvl(update_qty,0))            as "Qty"
, from_loc_id                       as "From Location"
, to_loc_id                         as "To Location"
, code                              as "Code"
from inventory_transaction
where client_id = 'MOUNTAIN'
and code in ('Relocate','Stock Check','Inv UnLock','Inv Lock')
and dstamp between to_date('&&2 &&4', 'DD-MON-YYYY HH24:MI:SS') and to_date('&&3 &&5', 'DD-MON-YYYY HH24:MI:SS')
group by to_char(dstamp, 'DD/MON')
, to_char(dstamp, 'HH24:MI')
, user_id
, sku_id
, from_loc_id
, to_loc_id
, code
order by "User","Time"
)
union all
select 'Date,Time,User,Sku,Qty,From Location,To Location,Code' from dual
union all
select "Date" || ',' || "Time" || ',' || "User" || ',' || "Sku" || ',' || "Qty" || ',' || "From Location" || ',' || "To Location" || ',' || "Code"
from
(
select to_char(dstamp, 'DD/MON')    as "Date"
, to_char(dstamp, 'HH24:MI')        as "Time"
, user_id                           as "User"
, sku_id                            as "Sku"
, sum(nvl(update_qty,0))            as "Qty"
, from_loc_id                       as "From Location"
, to_loc_id                         as "To Location"
, code                              as "Code"
from inventory_transaction
where client_id = 'MOUNTAIN'
and code in ('Relocate','Stock Check','Inv UnLock','Inv Lock')
and dstamp between to_date('&&2 &&4', 'DD-MON-YYYY HH24:MI:SS') and to_date('&&3 &&5', 'DD-MON-YYYY HH24:MI:SS')
group by to_char(dstamp, 'DD/MON')
, to_char(dstamp, 'HH24:MI')
, user_id
, sku_id
, from_loc_id
, to_loc_id
, code
order by "User","Time"
)
union all
select '---------------------------' from dual
union all
select '----------Totals-----------' from dual
union all
select '---------------------------' from dual
union all
select 'Date,User,Qty,Code' from dual
union all
select "Date" || ',' || "User" || ',' || sum("Qty") || ',' || "Code" from (
select to_char(dstamp, 'DD/MON')    as "Date"
, user_id                           as "User"
, sum(nvl(original_qty,0))          as "Qty"
, code                              as "Code"
from inventory_transaction
where client_id = 'MOUNTAIN'
and code in ('Inv Lock')
and dstamp between to_date('&&2 &&4', 'DD-MON-YYYY HH24:MI:SS') and to_date('&&3 &&5', 'DD-MON-YYYY HH24:MI:SS')
group by to_char(dstamp, 'DD/MON')
, user_id
, sku_id
, code
having sum(nvl(original_qty,0))<>0
order by user_id
) group by "Date","User","Code"
union all
select '---------------------------' from dual
union all
select 'Date,User,Qty,Code' from dual
union all
select "Date" || ',' || "User" || ',' || sum("Qty") || ',' || "Code" from (
select to_char(dstamp, 'DD/MON')    as "Date"
, user_id                           as "User"
, sum(nvl(original_qty,0))          as "Qty"
, code                              as "Code"
from inventory_transaction
where client_id = 'MOUNTAIN'
and code in ('Inv UnLock')
and dstamp between to_date('&&2 &&4', 'DD-MON-YYYY HH24:MI:SS') and to_date('&&3 &&5', 'DD-MON-YYYY HH24:MI:SS')
group by to_char(dstamp, 'DD/MON')
, user_id
, sku_id
, code
having sum(nvl(original_qty,0))<>0
order by user_id
) group by "Date","User","Code"
union all
select '---------------------------' from dual
union all
select 'Date,User,Qty,Code' from dual
union all
select "Date" || ',' || "User" || ',' || sum("Qty") || ',' || "Code" from (
select to_char(dstamp, 'DD/MON')    as "Date"
, user_id                           as "User"
, sum(nvl(original_qty,0))          as "Qty"
, code                              as "Code"
from inventory_transaction
where client_id = 'MOUNTAIN'
and code in ('Relocate')
and dstamp between to_date('&&2 &&4', 'DD-MON-YYYY HH24:MI:SS') and to_date('&&3 &&5', 'DD-MON-YYYY HH24:MI:SS')
group by to_char(dstamp, 'DD/MON')
, user_id
, sku_id
, code
having sum(nvl(original_qty,0))<>0
order by user_id
) group by "Date","User","Code"
union all
select '---------------------------' from dual
union all
select 'Date,User,Qty,Code' from dual
union all
select "Date" || ',' || "User" || ',' || sum("Qty") || ',' || "Code" from (
select to_char(dstamp, 'DD/MON')    as "Date"
, user_id                           as "User"
, sum(nvl(original_qty,0))+ sum(nvl(update_qty,0)) as "Qty"
, code                              as "Code"
from inventory_transaction
where client_id = 'MOUNTAIN'
and code in ('Stock Check')
and dstamp between to_date('&&2 &&4', 'DD-MON-YYYY HH24:MI:SS') and to_date('&&3 &&5', 'DD-MON-YYYY HH24:MI:SS')
group by to_char(dstamp, 'DD/MON')
, user_id
, sku_id
, code
having sum(nvl(original_qty,0))+ sum(nvl(update_qty,0)) <>0
order by user_id
) group by "Date","User","Code"
union all
select '---------------------------' from dual
union all
select 'Date,User,Qty,Locations,Code' from dual
union all
select "Date" || ',' || "User" || ',' || sum("Qty") || ',' || sum("Count") || ',' || "Code" from (
select to_char(dstamp, 'DD/MON')    as "Date"
, user_id                           as "User"
, sum(nvl(original_qty,0))          as "Qty"
, count(distinct from_loc_id)       as "Count"
, code                              as "Code"
from inventory_transaction
where client_id = 'MOUNTAIN'
and code in ('Inv Lock')
and dstamp between to_date('&&2 &&4', 'DD-MON-YYYY HH24:MI:SS') and to_date('&&3 &&5', 'DD-MON-YYYY HH24:MI:SS')
group by to_char(dstamp, 'DD/MON')
, user_id
, sku_id
, code
having sum(nvl(original_qty,0))<>0
order by user_id
) group by "Date","User","Code"
union all
select '---------------------------' from dual
union all
select 'Date,User,Qty,Locations,Code' from dual
union all
select "Date" || ',' || "User" || ',' || sum("Qty") || ',' || sum("Count") || ',' || "Code" from (
select to_char(dstamp, 'DD/MON')    as "Date"
, user_id                           as "User"
, sum(nvl(original_qty,0))          as "Qty"
, count(distinct from_loc_id)       as "Count"
, code                              as "Code"
from inventory_transaction
where client_id = 'MOUNTAIN'
and code in ('Inv UnLock')
and dstamp between to_date('&&2 &&4', 'DD-MON-YYYY HH24:MI:SS') and to_date('&&3 &&5', 'DD-MON-YYYY HH24:MI:SS')
group by to_char(dstamp, 'DD/MON')
, user_id
, sku_id
, code
having sum(nvl(original_qty,0))<>0
order by user_id
) group by "Date","User","Code"
union all
select '---------------------------' from dual
union all
select 'Date,User,Qty,Code' from dual
union all
select "Date" || ',' || "User" || ',' || sum("Qty") || ',' || "Code" from (
select to_char(dstamp, 'DD/MON')    as "Date"
, user_id                           as "User"
, sum(nvl(original_qty,0))          as "Qty"
, 'LP ' || code                     as "Code"
from inventory_transaction
where client_id = 'MOUNTAIN'
and code in ('Relocate')
and dstamp between to_date('&&2 &&4', 'DD-MON-YYYY HH24:MI:SS') and to_date('&&3 &&5', 'DD-MON-YYYY HH24:MI:SS')
and from_loc_id like '%LP%'
group by to_char(dstamp, 'DD/MON')
, user_id
, sku_id
, code
having sum(nvl(original_qty,0))<>0
order by user_id
) group by "Date","User","Code"
union all
select '---------------------------' from dual
union all
select 'Date,User,Qty,Locations Checked,Code' from dual
union all
select "Date" || ',' || "User" || ',' || sum("Qty") || ',' || count("Count") || ',' || "Code" from (
select to_char(dstamp, 'DD/MON')    as "Date"
, user_id                           as "User"
, sum(nvl(original_qty,0))+ sum(nvl(update_qty,0)) as "Qty"
, count(distinct from_loc_id) as "Count"
, code                              as "Code"
from inventory_transaction
where client_id = 'MOUNTAIN'
and code in ('Stock Check')
and dstamp between to_date('&&2 &&4', 'DD-MON-YYYY HH24:MI:SS') and to_date('&&3 &&5', 'DD-MON-YYYY HH24:MI:SS')
group by to_char(dstamp, 'DD/MON')
, user_id
, sku_id
, code
having sum(nvl(original_qty,0))+ sum(nvl(update_qty,0)) <>0
order by user_id
) group by "Date","User","Code"
/
--spool off