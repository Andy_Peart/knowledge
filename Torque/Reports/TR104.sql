SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 50


--set termout off
--spool TR104.csv

select 'TR104 Shipped yesterday '||to_char(sysdate-1,'DD/MM/YYYY') from Dual;

select
CID||','||
a.name||','||
DG||','||
nvl(CCC,0)||','||
'End'
from
(select
CID,
DG
from
(select distinct product_group DG
from sku
where client_id='TR'
and length(product_group)=2),
(SELECT
distinct sm.CUSTOMER_ID CID
from shipping_manifest sm
where sm.client_id='TR'
and trunc(sm.shipped_dstamp)=trunc(sysdate-1))),
(SELECT
sm.CUSTOMER_ID AAA,
SKU.PRODUCT_GROUP BBB,
sum(sm.QTY_SHIPPED) CCC
from shipping_manifest sm, order_header oh, sku
where oh.client_id='TR'
and sm.client_id='TR'
and trunc(sm.shipped_dstamp)=trunc(sysdate-1)
and sm.customer_id=oh.customer_id
and sm.order_id=oh.order_id
and sm.consignment=oh.consignment
and sm.sku_id=sku.sku_id
and sku.client_id='TR'
group by
sm.CUSTOMER_ID,
SKU.PRODUCT_GROUP),address a
where DG=BBB (+)
and CID=AAA (+)
and CID=a.address_id
and a.client_id='TR'
order by CID,DG
/

select
'Total'||',,'||
DG||','||
nvl(CCC,0)||','||
'End'
from
(select distinct product_group DG
from sku
where client_id='TR'
and length(product_group)=2),
(SELECT
SKU.PRODUCT_GROUP BBB,
sum(sm.QTY_SHIPPED) CCC
from shipping_manifest sm, order_header oh, sku
where oh.client_id='TR'
and sm.client_id='TR'
and trunc(sm.shipped_dstamp)=trunc(sysdate-1)
and sm.customer_id=oh.customer_id
and sm.order_id=oh.order_id
and sm.consignment=oh.consignment
and sm.sku_id=sku.sku_id
and sku.client_id='TR'
group by
SKU.PRODUCT_GROUP)
where DG=BBB (+)
order by DG
/

--spool off
--set termout on
