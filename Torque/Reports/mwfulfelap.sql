SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON

with T1 as (
select pick_date, user_id, sum(all_picks) units_picked, round(avg(fulfilment),2) avg_fulfilment
from (
    select to_char(dstamp,'DD-MM-YYYY') pick_date, it.user_id, consignment, loc.work_zone, sum(case when update_qty = 0 then 1 else 0 end) zero_picks, sum(update_qty) all_picks
    ,case when sum(update_qty)>0 then (1-round(sum(case when update_qty = 0 then 1 else 0 end) / sum(update_qty),3))*100 else 0 end fulfilment
    from inventory_transaction it inner join location loc on it.site_id = loc.site_id and it.from_loc_id = loc.location_id
    where it.client_id = 'MOUNTAIN'
    and it.code = 'Pick'
	and it.work_group = 'WWW'
    and it.elapsed_time > 0
    and it.dstamp between to_date('&&2','DD-MON-YYYY') and to_date('&&3','DD-MON-YYYY')+1    
    group by it.user_id, loc.work_zone, consignment, to_char(dstamp,'DD-MM-YYYY')
    order by 1,2,3
)
group by pick_date, user_id
),
T2 as (
select to_char(time_picked_1,'DD-MM-YYYY') pick_date, user_id, round(avg(to_number(substr(to_char(time_diff),18,2))+to_number(substr(to_char(time_diff),15,2))*60),0) avg_seconds
from (
	select A.user_id, A.consignment, A.work_zone, A.from_loc_id, A.time_picked time_picked_1, B.time_picked time_pick_2, A.rn, B.rn, B.time_picked - A.time_picked time_diff
	from (
		select user_id, consignment, loc.work_zone, from_loc_id, dstamp time_picked,
		row_number() over (partition by consignment, work_zone order by user_id, consignment, work_zone, dstamp) rn
		from inventory_transaction it inner join location loc on it.site_id = loc.site_id and it.from_loc_id = loc.location_id
		where it.client_id = 'MOUNTAIN'
		and it.code = 'Pick'
        and it.work_group = 'WWW'
		and it.elapsed_time > 0
		and it.dstamp between to_date('&&2','DD-MON-YYYY') and to_date('&&3','DD-MON-YYYY')+1
		order by 1,2,3,5
	) A
	left join 
	(
		select user_id, consignment, loc.work_zone, from_loc_id, dstamp time_picked,
		row_number() over (partition by consignment, work_zone order by user_id, consignment, work_zone, dstamp)+1 rn
		from inventory_transaction it inner join location loc on it.site_id = loc.site_id and it.from_loc_id = loc.location_id
		where it.client_id = 'MOUNTAIN'
		and it.code = 'Pick'
        and it.work_group = 'WWW'
		and it.elapsed_time > 0
		and it.dstamp between to_date('&&2','DD-MON-YYYY') and to_date('&&3','DD-MON-YYYY')+1
		order by 1,2,3,5
	)
	B on A.user_id = B.user_id and A.work_zone = B.work_zone and A.consignment = B.consignment and A.rn = B.rn
	order by 1,2,3,5
)
group by to_char(time_picked_1,'DD-MM-YYYY'), user_id
order by 1,2
)
select 'Pick date,User ID,Average fulfilment,Average elapsed time,Units picked' from dual
union all
select pick_date || ',' || user_id || ',' || avg_fulfilment || ',' || avg_seconds  || ',' || units_picked
from (
	select t1.pick_date, t1.user_id, t1.avg_fulfilment, t2.avg_seconds, t1.units_picked
	from t1 left join t2 on t1.pick_date = t2.pick_date and t1.user_id = t2.user_id
	where avg_seconds is not null
)
/