SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 200
SET TRIMSPOOL ON
 
 
set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


column B heading 'Type' format A12
column B1 heading 'Date Received' format A16
column B2 heading 'Order' format A18
column B3 heading 'Location' format A12
column C heading 'Qty' format 9999999
column D heading 'Bars Used' format 99999


--break on B dup skip 1 on B1 dup skip 1 on B2 dup skip 1 on report
break on B dup skip 1 on B1 dup on report
compute sum label 'Type Total' of C on B
compute sum label 'Date Total' of C on B1
--compute sum label 'Order Total' of C on B2
compute sum label 'Total' of C on report

--TTITLE LEFT 'Oldest Stock Report for BERWIN  as at ' report_date RIGHT 'Page:' FORMAT 999 SQL.PNO skip 2

select 'Type,Date Received,Order,Location,Qty,' from DUAL;

select
DECODE(sku.description,'JACKETS','JACKETS','JACKET','JACKETS',
'WAISTCOAT','WAISTCOATS','WAISTCOATS','WAISTCOATS',
'SUIT','SUITS','SUITS','SUITS',sku.description) B,
trunc(ii.receipt_dstamp) B1,
ii.sku_id B2,
ii.location_id B3,
sum(ii.qty_on_hand) C
from inventory ii,sku
where ii.client_id='BW'
and sku.client_id='BW'
and ii.sku_id=sku.sku_id
and trunc(sysdate)-trunc(ii.receipt_dstamp)>&&2
group by
DECODE(sku.description,'JACKETS','JACKETS','JACKET','JACKETS',
'WAISTCOAT','WAISTCOATS','WAISTCOATS','WAISTCOATS',
'SUIT','SUITS','SUITS','SUITS',sku.description),
trunc(ii.receipt_dstamp),
ii.sku_id,
ii.location_id
order by
B,B1,B2,B3
/
