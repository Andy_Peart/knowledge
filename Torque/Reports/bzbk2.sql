SET FEEDBACK OFF                 
set pagesize 68
set linesize 300
set verify off
set wrap off
set newpage 0

clear columns
clear breaks
clear computes


column A heading 'Order ID' format a16
column B heading 'SKU Ordered' format a16
column BB heading 'SKU Description' format a40
column BBB heading 'Size' format a12
column BBBB heading 'Group' format a32
column C heading 'Original SKU' format a16
column CC heading 'SKU Description' format a40
column CCC heading 'Size' format a12
column CCCC heading 'Group' format a32
column D heading 'Ordrd' format 99999
column E heading 'Line' format 99999
column F heading 'Old|Line' format 99999
column G heading 'Ordrd' format 99999
column H heading 'Tasked' format 99999
column I heading 'Shipped' format 99999
column J heading 'SF' format A2


break on A skip 2

select
ol.order_id A,
ol.sku_id B,
sku1.description BB,
sku1.sku_size BBB,
sku1.user_def_note_1 BBBB,
ol.line_id E,
ol.substitute_flag J,
ol.original_line_id F,
ol.qty_ordered G,
ol.qty_tasked H,
AA C,
HH CC,
GG CCC,
II CCCC
--ol.qty_shipped I
from sku sku1,order_header oh,order_line ol,
(select ol.sku_id as AA, ol.qty_ordered as BB, ol.order_id as DD,ol.line_id as CC,sku.description as HH,sku.sku_size as GG,sku.user_def_note_1 as II
from order_line ol,order_header oh,sku
where oh.client_id='TR'
and oh.order_id=ol.order_id
and ol.substitute_flag='Y'
and ol.sku_id=sku.sku_id
and sku.client_id='TR'
--and ol.original_line_id is not null
and oh.order_id like '2007&&2%')
--and ol.substitute_flag='Y')
--and oh.order_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&2', 'DD-MON-YYYY')+2)
where oh.client_id='TR'
and oh.order_id like '2007&&2%'
and oh.order_id=ol.order_id
--and oh.order_id=DD (+)
and ol.original_line_id=CC (+)
and ol.sku_id=sku1.sku_id
and sku1.client_id='TR'
--and sku1.sku_size<>sku2.sku_size
order by 
ol.order_id,
ol.line_id
/
