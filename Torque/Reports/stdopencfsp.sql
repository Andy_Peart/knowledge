SET FEEDBACK OFF
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','

CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240
SET TRIMSPOOL ON


select 'Client, Pre Advice, Date Created, Date Due, Supplier, CFSP' from dual
union all
select client_id || ',' || pre_advice_id || ',' || date_created || ',' || date_due || ',' || supplier_id || ',' || cfsp from
(
select client_id, pre_advice_id, date_created, date_due, supplier_id, 
case 
 when nvl(flag,'X') = 'BOND' and condition_id = 'CFSP' then 'YES'
 else 'NO'
end cfsp
from (
select ph.client_id, ph.pre_advice_id, to_char(ph.CREATION_DATE,'DD-MON-YYYY') date_created, to_char(ph.DUE_DSTAMP,'DD-MON-YYYY') Date_due,  ph.SUPPLIER_ID, pl.user_def_type_5 flag, pl.condition_id, sum(nvl(qty_received,0)) qty_rec
from pre_advice_header ph inner join pre_advice_line pl on ph.pre_advice_id = pl.pre_advice_id and ph.client_id = pl.client_id
where ph.client_id = '&&2'
and ph.status = 'Released'
and ph.creation_date between to_date('&&3') and to_date('&&4')
group by ph.client_id, ph.pre_advice_id, to_char(ph.CREATION_DATE,'DD-MON-YYYY'), to_char(ph.DUE_DSTAMP,'DD-MON-YYYY'),  ph.SUPPLIER_ID, pl.user_def_type_5, pl.condition_id
having sum(nvl(qty_received,0))=0
order by 3
))
/