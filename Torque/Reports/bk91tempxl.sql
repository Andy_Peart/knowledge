SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 400

column A heading 'Code' format A12
column B heading 'Txn Date' format A12
column C heading 'Txn Time' format A10
column D heading 'Elapsed Time' format 99999999
column DD heading 'Elapsed Time' format A14
column DDD heading 'System Time' format A14
column E heading 'From Location' format A14
column F heading 'Update Qty' format 99999999
column G heading 'Final Location' format A14
column H heading 'User ID' format A14
column M format a4 noprint

--select 'Order ID,Ordered,Picked' from DUAL;
 
set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DAY, DD MONTH, YYYY') curdate from DUAL;
set TERMOUT ON



select 'Analysis from &&2 &&5 to &&3 &&6 for Client &&4' from DUAL;

select 'Code,Date,Time,Elapsed Time,System Time,From Location,Qty,To Location,User ID' from DUAL;


break on H dup skip 2 

select
Code A,
to_char(Dstamp,'YY/MM') M,
to_char(Dstamp,'DD/MM/YYYY') B,
to_char(Dstamp,'HH24:MI:SS') C,
substr(Dstamp-lag(Dstamp,1) over (order by user_id,Dstamp),12,8) DD,
to_char(trunc(elapsed_time/60/60),'09') ||to_char(trunc(mod(elapsed_time,3600)/60),'09') ||to_char(mod(mod(elapsed_time,3600),60),'09') DDD,
from_loc_id E,
update_qty F,
final_loc_id G,
user_id H
from inventory_transaction
where client_id='&&4'
and trunc(Dstamp) between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')
and to_number(to_char(Dstamp,'HH24MISS'))>=ltrim(replace('&&5',':',''),'0')
and to_number(to_char(Dstamp,'HH24MISS'))<=ltrim(replace('&&6',':',''),'0')
and elapsed_time>0
order by 
user_id,
Dstamp
/

