SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON


column Q format 99999999
column Q2 format 99999999
column Q3 format 99999999
column Q4 format 99999999


break on report
compute sum of Q Q2 Q3 Q4 on report

--spool mwinvbymonth.csv

select 'Week,Sku,Description,Floor,Work Zone,Location,Qty On Hand,Cartons,SKU foot print,Approx space required' from Dual;

select
RECDATE,
''''||SKU||'''',
DSC,
FLR,
WZ,
''''||LL||'''',
Q,
ceil(Q/24) Q2,
ceil(ceil(Q/24)/8) Q3,
ceil(ceil(Q/24)/8) * 7.33 Q4
from (select
to_char(iv.receipt_dstamp,'YYYY MM Mon') RECDATE,
iv.sku_id SKU,
iv.description DSC,
nvl(mf.floorno,0) FLR,
L.work_zone WZ,
iv.location_id LL,
--LISTAGG(iv.location_id, ':') WITHIN GROUP (ORDER BY iv.location_id) AS LL,
sum(iv.qty_on_hand) Q
from inventory iv,location L,mountain_floor mf
where iv.client_id='MOUNTAIN'
and iv.location_id=L.location_id
and iv.site_id=L.site_id
and L.work_zone=mf.workzone (+)
and L.work_zone not in ('1WOPS','AMW','JMW','OMW','PMW','ZMW')
group by
to_char(iv.receipt_dstamp,'YYYY MM Mon'),
iv.sku_id,
iv.description,
nvl(mf.floorno,0),
L.work_zone,
iv.location_id
order by
RECDATE DESC,
iv.sku_id)
where substr(RECDATE,1,1)='2'
/

--spool off

