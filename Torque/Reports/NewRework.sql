SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON

SELECT 'Work Group,Consignment,Order ID,Sku,Description,From Location,To Location,Original Qty,Update Qty,Transaction Date,Container' from dual
Union All 
select "Work_Group" ||','|| "Consignment" ||','|| "Order_ID" ||','|| "Sku" ||','|| "Description" ||','|| "From_Location" ||','|| "To_Location" ||','|| "Original_Qty" ||','|| "Update_Qty" ||','|| "Transaction_Date" ||','|| "Container"
from (
select it.work_group					as "Work_Group"
, it.consignment						as "Consignment"
, it.reference_id						as "Order_ID"
, it.sku_id								as "Sku"
, sku.description						as "Description"
, it.from_loc_id						as "From_Location"
, it.to_loc_id							as "To_Location"
, it.original_qty						as "Original_Qty"
, it.update_qty							as "Update_Qty"
, to_char(it.dstamp, 'DD-MON-YYYY')		as "Transaction_Date"
, it.container_id						as "Container"
from inventory_transaction it
left join sku on sku.client_id = it.client_id and sku.sku_id = it.sku_id
where it.client_id = 'MOUNTAIN'
and it.consignment = '&&2'
and it.work_group = 'FBA'
and it.to_loc_id = 'CONTAINER'
and it.code = 'Pick'
and it.dstamp between trunc(sysdate)-20 and trunc(sysdate)+1
order by it.container_id
)
/