SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
with all_stock as (
  select to_char(sysdate, 'DD-MON-YYYY') as ReportDate,
  sku.sku_id, '"=""'||sku.sku_id||'"""' ProductCode,replace(sku.description,',',' ')  Description,sku.colour Colour,'"=""'||sku.sku_size||'"""' SkuSize, 
  sum(inv.qty_on_hand) qty, sum(inv.qty_allocated) qty_alloc
  from sku sku  
  left join inventory inv on inv.sku_id = sku.sku_id and inv.client_id = sku.client_id
  where sku.client_id = 'CF'
  group by sku.sku_id, sku.description, sku.colour, sku.sku_size
),
intake_stock as (
  select inv.sku_id, sum(inv.qty_on_hand) qty
  from inventory inv inner join  location loc on inv.location_id = loc.location_id and inv.site_id = loc.site_id
  where inv.client_id = 'CF'
  and loc.loc_type in ('Receive Dock')
  and inv.lock_status <> 'Locked'  
  group by inv.sku_id
),
suspense_stock as (
  select inv.sku_id, sum(inv.qty_on_hand) qty
  from inventory inv
  where inv.client_id = 'CF'
  and inv.location_id = 'SUSPENSE'
  and inv.lock_status <> 'Locked'  
  group by inv.sku_id
),
shipdock_stock as (
  select inv.sku_id, sum(inv.qty_on_hand) qty
  from inventory inv inner join  location loc on inv.location_id = loc.location_id and inv.site_id = loc.site_id
  where inv.client_id = 'CF'
  and loc.loc_type in ('ShipDock')
  group by inv.sku_id
),
reject_stock as (
 select sku_id, sum(qty) qty
 from (                                  
    select inv.sku_id, inv.lock_status, inv.location_id,  sum(inv.qty_on_hand) qty
    from inventory inv inner join location loc on inv.location_id = loc.location_id and inv.site_id = loc.site_id
    where inv.client_id = 'CF'
    and inv.lock_status = 'UnLocked'    
    and nvl(loc.disallow_alloc,'N') = 'Y' and loc.loc_type <> 'ShipDock' and loc.loc_type <> 'Receive Dock' and loc.location_id <> 'SUSPENSE'
    and inv.condition_id = 'GOOD'
    group by inv.sku_id, inv.lock_status, inv.location_id
    union all
    select inv.sku_id, inv.lock_status, inv.location_id,  sum(inv.qty_on_hand) qty
    from inventory inv inner join location loc on inv.location_id = loc.location_id and inv.site_id = loc.site_id
    where inv.client_id = 'CF'
    and inv.lock_status = 'Locked'  
    and inv.condition_id not in ('QUAR')
    group by inv.sku_id, inv.lock_status, inv.location_id
  ) 
  group by sku_id
),
dispersal_stock as (
  select inv.sku_id, sum(inv.qty_on_hand) qty
  from inventory inv
  where inv.client_id = 'CF'
  and inv.condition_id  in ('QUAR')
  group by inv.sku_id
)
select 'Report Date,Product Code,Description,Colour,Size,Available,Total Stock,Intake,Suspense,ShipDock,Locked Loc,Allocated,Quarentine' headers from dual
union all
select ReportDate || ',' || ProductCode || ',' || Description || ',' || Colour || ',' || SKUSize || ',' || Available || ',' ||
TotalStock || ',' || Intake || ',' || Suspense || ',' || ShipDock || ',' || Rejected || ',' || Allocated ||  ',' ||
Quarentine  
from (
select a.ReportDate, a.ProductCode, a.Description, a.Colour, a.SkuSize, 
nvl(a.Qty,0) - nvl(b.Qty,0) - nvl(d.Qty,0) - nvl(e.Qty,0) - nvl(f.Qty,0) - nvl(a.Qty_alloc,0) - nvl(h.Qty,0) as Available,
nvl(a.Qty,0) as TotalStock,
nvl(b.Qty,0) as Intake,
nvl(d.Qty,0) as Suspense,
nvl(e.Qty,0) as ShipDock,
nvl(f.Qty,0) as Rejected,
nvl(a.Qty_alloc,0) as Allocated,
nvl(h.Qty,0) as Quarentine
from all_stock a
left join intake_stock b on a.sku_id = b.sku_id
left join suspense_stock d on a.sku_id = d.sku_id
left join shipdock_stock e on a.sku_id = e.sku_id
left join reject_stock f on a.sku_id = f.sku_id
left join dispersal_stock h on a.sku_id = h.sku_id
)
union all
select 'Total Good Stock , ' || to_char( sum(inv.qty_on_hand))
from inventory inv inner join location loc on inv.location_id = loc.location_id and inv.site_id = loc.site_id
    where inv.client_id = 'CF'
    and inv.lock_status = 'UnLocked'    
    and nvl(loc.disallow_alloc,'N') = 'N' and loc.loc_type <> 'ShipDock' and loc.loc_type <> 'Receive Dock' and loc.location_id <> 'SUSPENSE'
/
--spool off