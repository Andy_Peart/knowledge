/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :     ccordershort.sql                         		      */
/*                                                                            */
/*     DESCRIPTION:                                                           */
/*     CSV check stock levels for one order to detail where stock is          */
/*                                                                            */
/*   DATE       BY   DESCRIPTION						                      */
/*   ========== ==== ===========					                          */
/*   28/03/2014 YB   CC Control DOCK			  							  */
/******************************************************************************/
SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','
SET WRAP OFF


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

--SPOOL CC_ORDER_SHORT_.CSV

SELECT
	oh.customer_id
, oh.consignment
, oh.name
, ''                       AS Total
,SUM(NVL(ol.qty_tasked,0)) AS qty
, oh.order_id
FROM
	order_header oh
INNER JOIN order_line ol
ON
	oh.order_id      = ol.order_id
	AND oh.client_id = ol.client_id
WHERE
	oh.client_id       = 'CC'
	AND oh.status NOT                         IN ('Shipped','Cancelled')
	AND oh.creation_date BETWEEN TRUNC(sysdate)-10 AND TRUNC(sysdate)+1
	AND oh.consignment LIKE '&2%'
GROUP BY
	oh.consignment
, oh.customer_id
, oh.name
, oh.order_id
ORDER BY 1
/

--SPOOL OFF
