SET FEEDBACK OFF
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240
SET TRIMSPOOL ON

--SET TERMOUT OFF

break on A skip 1


spool stdstockhz.csv


select 'Work Zone,Qty On Hand,Qty Unallocatable,Qty Allocated,Qty Available' from DUAL
union all
select
A1||','||
A2||','||
A3||','||
A4||','||
A5
from (select
distinct
work_zone A1,
nvl(BB,0) A2,
nvl(DD,0) A3,
nvl(CC,0) A4,
nvl(BB,0)-nvl(DD,0)-nvl(CC,0) A5
from location,(select
lc.work_zone AA,
sum(DECODE(lc.work_zone,'PGPL',1,jc.qty_on_hand)) BB,
sum(DECODE(lc.work_zone,'PGPL',1,jc.qty_allocated)) CC
from inventory jc,location lc
where jc.client_id like '&&2'
and jc.location_id=lc.location_id
and jc.site_id=lc.site_id
group by
lc.work_zone),(select
lc.work_zone AAA,
nvl(sum(jc.qty_on_hand),0)-nvl(sum(jc.qty_allocated),0) DD
from inventory jc,location lc
where jc.client_id like '&&2'
and jc.location_id=lc.location_id
and jc.site_id=lc.site_id
and lc.disallow_alloc='Y'
group by
lc.work_zone)
where location.work_zone=AA (+)
and location.work_zone=AAA (+)
and nvl(BB,0)<>0
order by 1)
union all
select
A1||','||
A2||','||
A3||','||
A4||','||
A5
from (select
AA A1,
BB A2,
CC A4,
DD A3,
BB-DD-CC A5
from (select
'Totals' AA,
sum(DECODE(lc.work_zone,'PGPL',1,jc.qty_on_hand)) BB,
sum(DECODE(lc.work_zone,'PGPL',1,jc.qty_allocated)) CC
from inventory jc,location lc
where jc.client_id like '&&2'
and jc.location_id=lc.location_id
and jc.site_id=lc.site_id),(select
nvl(sum(jc.qty_on_hand),0)-nvl(sum(jc.qty_allocated),0) DD
from inventory jc,location lc
where jc.client_id like '&&2'
and jc.location_id=lc.location_id
and jc.site_id=lc.site_id
and lc.disallow_alloc='Y'))
/


spool off

