SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET COLSEP ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON
SET HEADING OFF 

select 'LB WEB RETURNS: '  || TO_DATE('&&2','DD-MON-YYYY') || ' to ' ||TO_DATE('&&3','DD-MON-YYYY') FROM DUAL
UNION ALL
select ' ' from dual
UNION ALL
select 'SO Number,Item,Description,Qty,R/E,Return Reason,Notes,Value' from dual
union all
SELECT "SO Number"  || ',' || "Item Returned"  || ',' ||  "description"  || ',' || "Qty"  || ',' ||  "RET_TYPE"  || ',' ||  "Return Reason"  || ',' ||  "Notes"  || ',' ||  ReturnsValue
from (
select 
 replace(itl.user_def_type_2,' ','_') as "Order ID",  
	case replace(itl.user_def_type_1,' ','_') 
	when lag(replace(itl.user_def_type_1,' ','_')) 
		over (order by replace(itl.user_def_type_1,' ','_')) 
		then '' else replace(itl.user_def_type_1,' ','_') 
	end as "SO Number"	
	, '"=""'||itl.sku_id||'"""' AS "Item Returned"
,libsku.getuserlangskudesc(itl.client_id,itl.sku_id) "description"
, to_char(sum(itl.update_qty)) AS  "Qty"
, CASE nvl(itl.user_def_chk_1,'N')
		WHEN 'Y' THEN 'EXCHANGE'
		WHEN 'N' THEN 'RETURN'
		ELSE 'N/A'
	END AS "RET_TYPE"
, CASE itl.user_def_type_2
		WHEN 'A' THEN 'FAULTY/POOR QUALITY'
		WHEN 'B' THEN 'NOT LIKED'
		WHEN 'C' THEN 'TOO LONG'
		WHEN 'D' THEN 'TOO SHORT'
		WHEN 'E' THEN 'TOO BIG'
		WHEN 'F' THEN 'TOO SMALL'
		WHEN 'G' THEN 'FABRIC'
		WHEN 'H' THEN 'COLOUR'
		WHEN 'I' THEN 'ORDERED FOR CHOICE'
		WHEN 'J' THEN 'INCORRECT ITEM'
		WHEN 'K' THEN 'ARRIVED LATE'
		WHEN 'L' THEN 'OTHER'
		WHEN 'M' THEN 'CREASED'
		ELSE itl.user_def_type_2 
	END AS "Return Reason"
, replace(itl.user_def_note_1,',','') AS "Notes" 
, to_char( s.UnitValue * sum(itl.update_qty)) ReturnsValue 
FROM
	inventory_transaction itl
  left join 
  (
    select client_id, order_id, sku_id, sum(line_value/qty_ordered) UnitValue,sum(qty_ordered)qty_ordered from order_line
    where client_id = 'LB' 
    group by client_id, order_id, sku_id
  ) s on itl.client_id = s.client_id and itl.sku_id = s.sku_id and itl.user_def_type_1 = s.order_id
WHERE
	itl.client_id        = 'LB'
	and itl.code in ('Receipt')
	AND itl.reference_id = 'R'
  AND itl.dstamp between TO_DATE('&&2','DD-MON-YYYY') and TO_DATE('&&3','DD-MON-YYYY')+1
group by s.UnitValue, s.qty_ordered,
replace(itl.user_def_type_2,' ','_') 
, itl.user_def_type_1
, '"=""'||itl.sku_id||'"""' 
, CASE nvl(itl.user_def_chk_1,'N')
		WHEN 'Y' THEN 'EXCHANGE'
		WHEN 'N' THEN 'RETURN'
		ELSE 'N/A' 
	END 
,CASE itl.user_def_type_2
		WHEN 'A' THEN 'FAULTY/POOR QUALITY'
		WHEN 'B' THEN 'NOT LIKED'
		WHEN 'C' THEN 'TOO LONG'
		WHEN 'D' THEN 'TOO SHORT'
		WHEN 'E' THEN 'TOO BIG'
		WHEN 'F' THEN 'TOO SMALL'
		WHEN 'G' THEN 'FABRIC'
		WHEN 'H' THEN 'COLOUR'
		WHEN 'I' THEN 'ORDERED FOR CHOICE'
		WHEN 'J' THEN 'INCORRECT ITEM'
		WHEN 'K' THEN 'ARRIVED LATE'
		WHEN 'L' THEN 'OTHER'
		WHEN 'M' THEN 'CREASED'
		ELSE itl.user_def_type_2 
	END
, itl.user_def_note_1
, libsku.getuserlangskudesc(itl.client_id,itl.sku_id)
)
/