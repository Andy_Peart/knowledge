SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 900
SET TRIMSPOOL ON


select 'Order ID,Tracking Reference,Carrier,Delivery Details,,' from DUAL;

Select distinct
oh.order_id||','||
oh.user_def_note_2||','||
i.from_loc_id||','||
oh.name||',"'||
oh.address1||'","'||
oh.address2||'",'||
oh.town||','||
oh.postcode
from order_header oh, inventory_transaction i
where oh.client_id = 'MOUNTAIN'
and i.dstamp > sysdate-99
and i.reference_id=oh.order_id
and i.code = 'Pick'
and i.to_loc_id = 'STG'
--and i.from_loc_id = 'DHL'
and i.client_id = 'MOUNTAIN'
/
