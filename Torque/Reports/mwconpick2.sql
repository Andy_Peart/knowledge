SET FEEDBACK OFF                 
set verify off
set wrap off
set colsep ','

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 500
set trimspool on


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

--break on report 

--spool mwconpick2.csv

select 'Retail NON CONTAINER Picks from &&2 to &&3 - for client MOUNTAIN' from DUAL;

select 'Store Id,Units,Tasks,Time Taken,KPI,ACCS,,EQIP,,FOOT,,KAPP,,KOUT,,MBOT,,MJAC,,MTOP,,WBOT,,WJAC,,WTOP,,NULL,' from DUAL;

select
XX||','||
K9||','||
K10||','||
K11||','||
replace(to_char((K9/K11)*3600,'999999'),' ','')||','||
K20||','||
replace(to_char((K20*100)/K9,'999.99'),' ','')||','||
K21||','||
replace(to_char((K21*100)/K9,'999.99'),' ','')||','||
K22||','||
replace(to_char((K22*100)/K9,'999.99'),' ','')||','||
K23||','||
replace(to_char((K23*100)/K9,'999.99'),' ','')||','||
K24||','||
replace(to_char((K24*100)/K9,'999.99'),' ','')||','||
K25||','||
replace(to_char((K25*100)/K9,'999.99'),' ','')||','||
K26||','||
replace(to_char((K26*100)/K9,'999.99'),' ','')||','||
K27||','||
replace(to_char((K27*100)/K9,'999.99'),' ','')||','||
K28||','||
replace(to_char((K28*100)/K9,'999.99'),' ','')||','||
K29||','||
replace(to_char((K29*100)/K9,'999.99'),' ','')||','||
K30||','||
replace(to_char((K30*100)/K9,'999.99'),' ','')||','||
K31||','||
replace(to_char((K31*100)/K9,'999.99'),' ','')
from (select
XX,
sum(K9) K9,
sum(K10) K10,
sum(K11) K11,
sum(K20) K20,
sum(K21) K21,
sum(K22) K22,
sum(K23) K23,
sum(K24) K24,
sum(K25) K25,
sum(K26) K26,
sum(K27) K27,
sum(K28) K28,
sum(K29) K29,
sum(K30) K30,
sum(K31) K31
from (select
XX,
DECODE(B,'ZZZZ',D,0) K9,
DECODE(B,'ZZZZ',C,0) K10,
DECODE(B,'ZZZZ',F,0) K11,
DECODE(B,'ACCS',D,0) K20,
DECODE(B,'EQIP',D,0) K21,
DECODE(B,'FOOT',D,0) K22,
DECODE(B,'KAPP',D,0) K23,
DECODE(B,'KOUT',D,0) K24,
DECODE(B,'MBOT',D,0) K25,
DECODE(B,'MJAC',D,0) K26,
DECODE(B,'MTOP',D,0) K27,
DECODE(B,'WBOT',D,0) K28,
DECODE(B,'WJAC',D,0) K29,
DECODE(B,'WTOP',D,0) K30,
DECODE(B,'YYYY',D,0) K31
from (select
XX,
B,
D,
C,
F,
replace(to_char((D/F)*3600,'999999'),' ','') G
from (select
'A' ZZ,
nvl(sku.user_def_type_8,'YYYY') B,
it.work_group XX,
count(*) C,
sum(it.update_qty) D,
sum(it.elapsed_time) F
from inventory_transaction it,sku
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='MOUNTAIN'
and it.reference_id like 'MOR%'
and it.update_qty<>0
and it.container_id is null
and it.station_id not like 'Auto%'
and it.code='Pick'
and it.elapsed_time>0
and it.from_loc_id<>'P999'
and it.sku_id=sku.sku_id
and it.client_id=sku.client_id
group by nvl(sku.user_def_type_8,'YYYY'),it.work_group
union
select
'A' ZZ,
'ZZZZ' B,
it.work_group XX,
count(*) C,
sum(it.update_qty) D,
sum(it.elapsed_time) F
from inventory_transaction it,sku
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='MOUNTAIN'
and it.reference_id like 'MOR%'
and it.update_qty<>0
and it.container_id is null
and it.station_id not like 'Auto%'
and it.code='Pick'
and it.elapsed_time>0
and it.from_loc_id<>'P999'
and it.sku_id=sku.sku_id
and it.client_id=sku.client_id
group by it.work_group
union
select
'A' ZZ,
nvl(sku.user_def_type_8,'YYYY') B,
it.work_group XX,
count(*) C,
sum(it.update_qty) D,
sum(it.elapsed_time) F
from inventory_transaction_archive it,sku
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='MOUNTAIN'
and it.reference_id like 'MOR%'
and it.update_qty<>0
and it.container_id is null
and it.station_id not like 'Auto%'
and it.code='Pick'
and it.elapsed_time>0
and it.from_loc_id<>'P999'
and it.sku_id=sku.sku_id
and it.client_id=sku.client_id
group by nvl(sku.user_def_type_8,'YYYY'),it.work_group
union
select
'A' ZZ,
'ZZZZ' B,
it.work_group XX,
count(*) C,
sum(it.update_qty) D,
sum(it.elapsed_time) F
from inventory_transaction_archive it,sku
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='MOUNTAIN'
and it.reference_id like 'MOR%'
and it.update_qty<>0
and it.container_id is null
and it.station_id not like 'Auto%'
and it.code='Pick'
and it.elapsed_time>0
and it.from_loc_id<>'P999'
and it.sku_id=sku.sku_id
and it.client_id=sku.client_id
group by it.work_group)
order by XX,B))
group by XX)
order by XX
/

--spool off
