SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep '|'


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 300
SET TRIMSPOOL ON
 
column XX NOPRINT

--spool mkukmail.csv

select
order_id  XX,
'H049196'||'|'||
to_char(Sysdate,'DDMMYYYY')||'|'||
contact||'|'||
Name||'|'||
Address1||'|'||
Address2||'|'||
Town||'|'||
County||'|||'||
PostCode||'||'||
'1'||'|'||
'10'||'|'||
'1'||'|'||
instructions||'|'||
USER_DEF_NOTE_2||'|||||'||
'UT'||order_id||'|||||||||||||||'||
'Torque (Uttam) Challenge Way Wigan WN5 0LD'||'||'||
CONTACT_PHONE||'|'||
' '
from order_header,(select
oh.order_id AAA,
sum(nvl(ol.line_value,0)) BBB
from order_header oh, order_line ol
where oh.client_id='UT'
and oh.country='GBR'
and oh.consignment='&&2'
and ol.client_id='UT'
and oh.order_id=ol.order_id
group by
oh.order_id)
where client_id='UT'
and consignment='&&2'
and order_id=AAA 
ORDER BY ORDER_ID
/

--spool off
