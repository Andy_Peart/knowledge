SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 65

--spool promstk2.csv

select 'Pick ID,Docket,Cartons,PCS,Despatched Date,Division,' from DUAL;

select
distinct *
from (select
OO||','||
SS||','||
QQ||','||
nvl(UD1,'')||','||
to_char(DD,'dd-Mon-yy')||','||
DIV||','||
' '
from (select
it.reference_id OO,
it.sku_id SS,
DECODE(it.from_loc_id,'TR','Retail','T','Home Shopping','Unknown') DIV,
sum(it.update_qty) QQ,
oh.order_date DD
from inventory_transaction it,order_header oh
where it.client_id='PR'
and it.code='Shipment'
and oh.client_id='PR'
and it.reference_id=oh.order_id
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by
it.reference_id,
it.sku_id,
DECODE(it.from_loc_id,'TR','Retail','T','Home Shopping','Unknown'),
oh.order_date),(select
sku_id SKUSKU,
user_def_type_1 UD1
from sku
where sku.client_id='PR')
where SS=SKUSKU (+))
order by 1
/

--spool off
