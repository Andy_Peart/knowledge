SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON


--spool tpstocksplit.csv

select ',SKU,Description,Qty' from DUAL;

select
',BAROO'||',Total,'||
sum(it.qty_on_hand) 
from inventory it,sku
where it.client_id = 'TP'
and it.sku_id=sku.sku_id
and sku.client_id = 'TP'
and sku.product_group='BAROO'
/
select
',TIPPITOES'||',Total,'||
sum(it.qty_on_hand) 
from inventory it,sku
where it.client_id = 'TP'
and it.sku_id=sku.sku_id
and sku.client_id = 'TP'
and sku.product_group<>'BAROO'
/


select
'BAROO'||','||
it.sku_id||','||
sku.description||','||
sum(it.qty_on_hand) 
from inventory it,sku
where it.client_id = 'TP'
and it.sku_id=sku.sku_id
and sku.client_id = 'TP'
and sku.product_group='BAROO'
group by it.sku_id,sku.description
order by 1
/

select
'TIPPITOES'||','||
it.sku_id||','||
sku.description||','||
sum(it.qty_on_hand) 
from inventory it,sku
where it.client_id = 'TP'
and it.sku_id=sku.sku_id
and sku.client_id = 'TP'
and sku.product_group<>'BAROO'
group by it.sku_id,sku.description
order by 1
/


--spool off
