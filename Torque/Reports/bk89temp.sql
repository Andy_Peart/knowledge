SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','
SET UND OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 320




column A heading 'Date' format a12
column C heading 'Orders' format 99999999
column D heading 'Units|Ordered' format 999999999
column E heading 'Units|Picked' format 999999999
column F heading 'Units|Shipped' format 999999999
column M format a4 noprint



select 'Date,Orders,Ordered,Picked,Shipped' from DUAL;
 
set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DAY, DD MONTH, YYYY') curdate from DUAL;
set TERMOUT ON


break on report

compute sum label 'Total' of D E F on report


TTITLE LEFT 'Orders from Client &&2 Period &&3 to &&4' skip 2


select
to_char(oh.creation_date, 'YYMM') M,
to_char(oh.creation_date,'DD/MM/YYYY') A,
count(distinct oh.order_id) C,
sum(ol.qty_ordered) D,
sum(ol.qty_picked) E,
sum(ol.qty_shipped) F
from order_header oh,order_line ol
where oh.client_id='&&2'
and oh.creation_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and oh.order_id=ol.order_id
group by
to_char(oh.creation_date, 'YYMM'),
to_char(oh.creation_date,'DD/MM/YYYY') 
order by
to_char(oh.creation_date, 'YYMM'),
to_char(oh.creation_date,'DD/MM/YYYY') 
/
