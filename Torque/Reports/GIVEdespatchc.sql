/********************************************************************************************/
/*                                                                                          */
/*                            Elite                                                         */
/*                                                                                          */
/*     FILE NAME  :   GIVEdespatchc.sql                                                     */
/*                                                                                          */
/*     DESCRIPTION:    Datastream for GIVE Despatch Note / Pack List by consignment         */
/*                                                                                          */
/*   DATE     BY   PROJ       ID         DESCRIPTION                                        */
/*   ======== ==== ======     ========   =============                                      */
/*   18/08/09 RW   GIVE                  Despatch Note For GIVE Retail by consignment       */
/*                                                                                          */
/********************************************************************************************/

/* Input Parameters are as follows */
-- 1)  GMT
-- 2)  Order_id
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 0
SET VERIFY OFF
SET FEEDBACK OFF
SET TAB OFF

 
-- Set the line length to the maximum possible, but trim 
-- trailing spaces for both screen output and spool files
SET LINESIZE 32767
SET WRAP ON
SET TRIMOUT ON
SET TRIMSPOOL ON




/* Define the format of the output dates */
DEFINE	OutputDateFormat = 'DD-MON-YYYY HH24:MI:SS'
COLUMN	sorter	NOPRINT

SELECT Distinct '0' sorter,
       'DSTREAM_GIVE_DESPC_HDR'          	||'|'||
       rtrim('&&2')                            /* Consignment_id */
from dual
union all
SELECT distinct '1' sorter,
'DSTREAM_GIVE_DESPC_ADD_HDR'              ||'|'||
rtrim (oh.name)					||'|'||
rtrim (oh.customer_id)				||'|'||
rtrim (a.contact_email)				||'|'||
decode(oh.address1,null,'',oh.address1||'|')||
decode(oh.address2,null,'',oh.address2||'|')||
decode(oh.town,null,'',oh.town||'|')||
decode(oh.county,null,'',oh.county||'|')||
decode(t.text,null,'',t.text||'|')||          /* Country converted using workstation lookup) */
decode(oh.postcode,null,'',oh.postcode||'|')
from order_header oh, country c, language_text t, address a
where oh.consignment = '&&2'
AND oh.CLIENT_ID = 'GV'
and oh.country = c.iso3_id
and language = 'EN_GB'
and label = 'WLK'||c.iso3_id
and oh.ship_dock = a.address_id  
union all
select distinct '1' sorter,
'DSTREAM_GIVE_DESPC_ORD_HDR'      			||'|'||
rtrim (oh.consignment)          			||'|'||
rtrim (to_char(sm.shipped_dstamp, 'DD-MON-YYYY'))||'|'||
rtrim (sm.location_id)        			||'|'||
rtrim (sm.user_id)          				||'|'||
rtrim (sm.site_id)          				||'|'||
rtrim (A)              					||'|'||
rtrim (moo)              				||'|'||
rtrim (instructions)
from order_header oh, shipping_manifest sm, (select count(distinct smm.container_id) A 
from shipping_manifest smm where smm.consignment = '&&2'),
(select min(order_id) as moo from order_header where consignment = '&&2')
where oh.consignment = '&&2'
and oh.order_id = sm.order_id
and oh.client_id = 'GV'
union all
SELECT '1'||sh.container_id SORTER,
'DSTREAM_GIVE_DESPC_LINE'           ||'|'||
rtrim (sh.container_id)       	||'|'||
rtrim (s.EAN)        			||'|'||
--rtrim (s.user_def_type_2||'-'||s.user_def_type_1||'-'||s.user_def_type_4)              ||'|'||
rtrim (s.user_def_type_6)   		||'|'||   
rtrim (s.description)               ||'|'||
rtrim (s.COLOUR)          		||'|'||
rtrim (s.SKU_SIZE||decode(s.user_def_type_8,'.','',s.user_def_type_8))          		||'|'||
rtrim (sum(sh.qty_shipped))         ||'|'||
rtrim (s.user_def_type_5) 		||'|'||
rtrim (sh.order_id)
from sku s, shipping_manifest sh
where sh.consignment = '&&2'
and sh.qty_picked > '0'
and sh.sku_id = s.sku_id
and s.client_id = 'GV'
group by
sh.container_id,
s.EAN,
s.user_def_type_6,
s.description,
s.COLOUR,
s.SKU_SIZE,
decode(s.user_def_type_8,'.','',s.user_def_type_8),
s.user_def_type_5,
sh.order_id
order by 1,2
/