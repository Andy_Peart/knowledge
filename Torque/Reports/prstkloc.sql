SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

 
select 'Location, Docket, Qty' from dual
union all
select loc || ',' || doc  || ',' ||  qty
from (
SELECT i.location_id loc
, i.user_def_type_4 doc
, sum(i.qty_on_hand) qty
FROM inventory i inner join location l on i.location_id = l.location_id
WHERE i.client_id in ('PR','PS')
AND i.location_id like '&2%'
AND i.qty_on_hand > 0
and l.site_id = 'PROM'
and l.work_zone in ('P', 'WFH1')
group BY i.location_id, i.user_def_type_4
ORDER BY i.location_id, i.user_def_type_4
)
/