set feedback off
set verify off



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
set pagesize 66
set linesize 128
set newpage 0

column A heading 'Container' format A10
column B heading 'SKU' format A15
column C heading 'Update|Qty' format 99999
column CC heading ' Found Qty' format a10
column D heading 'Description' format A40
column F heading 'Colour' format A16
column G heading 'Size' format A10
column H heading 'User ID' format A12
column J heading 'Consignment' format A12


--Select 'Container,SKU,Update Qty,Found Qty,Description,Colour,Size,User ID' from DUAL; 
 
set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DAY, DD MONTH, YYYY') curdate from DUAL;
set TERMOUT ON

ttitle  LEFT 'Final Loc: &&3' CENTER 'Date: &&2'  RIGHT 'Page:' FORMAT 999 SQL.PNO skip 2



break on A skip 1 dup on report

compute sum label 'CTN QTY' of C on A
compute sum label 'LOC QTY' of C on report

select
it.container_id A,
--to_char(it.Dstamp,'DD/MM/YYYY') BB,
it.sku_id B,
it.update_qty C,
'          ' CC,
sku.description D,
sku.colour F,
sku.sku_size G,
it.user_id H
from inventory_transaction it,sku
where it.client_id='TR'
and it.code='Pick'
and it.final_loc_id not like 'TRWEB%'
and it.final_loc_id not like 'TRALT%'
and it.to_loc_id='CONTAINER'
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&2', 'DD-MON-YYYY')+1
and it.final_loc_id like '&&3%'
and sku.client_id='TR'
and it.sku_id=sku.sku_id
order by
it.container_id
/

