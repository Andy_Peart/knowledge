SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON

--spool mwstocknotin12.csv

select 'Sku Id,Description,Location,Work Zone,Received,Retail Qty Available,Web Allocations,Web Stock' from DUAL;

select
distinct
''''||i.sku_id ||''','||
max(i.description) ||','''||
max(ll.location_id) ||''','||
max(ll.work_zone) ||','||
max(trunc(i.receipt_dstamp)) ||','||
sum(i.qty_on_hand-i.qty_allocated) ||','||
--sum(i.qty_allocated) ||','||
nvl(QWEBA,0) ||','||
nvl(QWEB,0)
from inventory i, location ll,(
	select distinct i.sku_id SKU,
	sum(i.qty_on_hand-i.qty_allocated) QWEB,
	sum(i.qty_allocated) QWEBA
	from inventory i, location ll
	where i.client_id='MOUNTAIN'
	and i.location_id=ll.location_id
	and ll.work_zone like '12%'
	group by i.sku_id)
where i.client_id='MOUNTAIN'
and lower(i.sku_id)=upper(i.sku_id)
and i.location_id=ll.location_id
and upper(ll.loc_type) like'TAG%'
and ll.work_zone not in ('WRET','WPF')
and ll.zone_1 in ('RETAIL')
and i.sku_id=SKU (+)
and nvl(QWEB,0)=0
group by
i.sku_id,
--i.description,
--ll.location_id,
--ll.work_zone,
nvl(QWEBA,0),
nvl(QWEB,0)
order by
1
/

--spool off
