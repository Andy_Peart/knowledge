SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

select 'Store No,Store Name,Order No,Lines,Units,Consignment' from DUAL
union all
select A ||','|| B ||','|| C ||','|| D ||','|| E ||','|| F
from (
	select oh.customer_id A, a.name B, 'List ' || LLL C, sum(OO) D, sum(QQQ) E, oh.consignment F, substr(oh.consignment,8,4) M, substr(oh.consignment,5,3) N
	from order_header oh,address a,
		(select	order_id OOO, list_id LLL, sum(qty_to_move) QQQ 
		from order_header oh, move_task mt
		where oh.client_id='TR' and mt.client_id='TR' and oh.consignment=mt.consignment and oh.order_id=mt.task_id
		and oh.order_type <> 'WEB'
		group by order_id, list_id),
		(select oh.order_id  ID, count(*) OO, nvl(sum(ol.qty_picked),0) PP, nvl(sum(ol.qty_tasked),0) KK, nvl(sum(ol.qty_shipped),0) SS
		from order_header oh,order_line ol
		where oh.client_id='TR' and oh.status not in ('Cancelled','Hold','Shipped') and oh.order_id=ol.order_id and ol.client_id='TR'
		and oh.order_type <> 'WEB'
		group by oh.order_id)
	where oh.order_id=OOO and oh.order_id=ID and oh.status not in ('Cancelled','Hold','Shipped') and oh.consignment like '&&2%'
	and substr(oh.consignment,5,1)='&&3' and oh.client_id='TR' and a.client_id='TR' and LLL is not null and oh.customer_id=a.address_id
	and oh.order_type <> 'WEB'
	group by oh.customer_id, a.name, LLL, oh.consignment
	union
	select oh.customer_id A, a.name B, oh.order_id C, OO D, KK E, oh.consignment F, substr(oh.consignment,8,4) M, substr(oh.consignment,5,3) N
	from order_header oh,address a,
		(select order_id OOO, list_id LLL, sum(qty_to_move) QQQ
		from order_header oh, move_task mt
		where oh.client_id='TR' and mt.client_id='TR' and mt.status='Released' and oh.consignment=mt.consignment and oh.order_id=mt.task_id
		and oh.order_type <> 'WEB'
		group by order_id, list_id),
		(select oh.order_id  ID, count(*) OO, nvl(sum(ol.qty_picked),0) PP, nvl(sum(ol.qty_tasked),0) KK, nvl(sum(ol.qty_shipped),0) SS
		from order_header oh,order_line ol
		where oh.client_id='TR' and oh.status not in ('Cancelled','Hold','Shipped') and oh.order_id=ol.order_id and ol.client_id='TR'
		and oh.order_type <> 'WEB'
		group by oh.order_id) 
	where oh.order_id=OOO and oh.order_id=ID and oh.status not in ('Cancelled','Hold','Shipped') and oh.consignment like '&&2%'
	and substr(oh.consignment,5,1)='&&3' and oh.client_id='TR' and a.client_id='TR' and LLL is null and oh.customer_id=a.address_id
	order by N, M
)
/
