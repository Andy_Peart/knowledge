SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ' '

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9999

column A format a12
column B format a18
column C format a12
column D format 99999999
column E format a20
column F format a30
column G format a12
column H format 99999999999999
column I format a30

select 'Status       Released Date      Customer                       Sales Order ID     Total Qty'
from DUAL;

select distinct 
oh.status A,
to_char(oh.creation_date, 'DD/MM/YY') B,
oh.name F,
oh.order_id G,
sum(ol.qty_ordered) H
from order_header oh, order_line ol
where oh.order_id = ol.order_id
and oh.client_id = 'CK'
and status <> 'Shipped'
and status <> 'Cancelled'
group by 
oh.status, 
to_char(oh.creation_date, 'DD/MM/YY'), 
oh.name, 
oh.order_id
order by 1
/