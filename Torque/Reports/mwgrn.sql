set pagesize 68
set linesize 120
set verify off

clear screen
clear columns
clear breaks
clear computes
clear buffer
clear sql

column AA heading 'T2TBARCODE' format a10
column A heading 'EXTBARCODE' format a15
column B heading 'DATE IN'
column BB heading 'TIME IN'
column C heading 'QTY IN' format 999999
column AAA heading 'DESC' format a15
column AAB heading 'SIZE' format a7
column ABB heading 'COLOUR' format a12

ttitle center "Mountain Warehouse - Goods Receipt Report." skip 3

break on report

compute sum label 'TOTAL' of c on report

select substr(s.ean, 1, 6) AA, 
inv.sku_id A,
s.user_def_type_2 AAA,
s.sku_size AAB,
s.colour ABB,
to_char(inv.dstamp, 'dd/MM/YYYY') B,
to_char(inv.dstamp, 'hh24:mi') BB,
inv.update_qty C
from inventory_transaction inv, sku s
where inv.client_id = 'MOUNTAIN'
and inv.code = 'Receipt'
and inv.sku_id=s.sku_id
order by inv.dstamp
/
