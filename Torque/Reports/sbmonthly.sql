SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 300
SET TRIMSPOOL ON


--spool sb99.csv


select 'Order,Client ID,Status,Type,Number of lines,Dispatch Method,Shipped Date,Name' from DUAL;

select
order_id,
client_id,
status,
order_type,
num_lines,
dispatch_Method,
to_char(shipped_date,'DD/MM/YYYY'),
name
from order_header 
where client_id='SB'
and to_char(shipped_date,'mm/yyyy')=to_char(sysdate-8,'mm/yyyy')
and order_id like 'C%'
order by order_id
/

--spool off

