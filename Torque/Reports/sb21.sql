SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

select 'SKU,Description,PO Number,PO Qty,Qty Received,Variance,Vendor,Date/Time' from DUAL;

column A format A12
column B format A40
column C format A20
column D format 99999
column E format 99999
column EE format 99999
column F format A10 
column G format A24 

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



--ttitle  LEFT 'TML Retail - Open Orders Report ' RIGHT 'Printed ' report_date skip 2


--break on CC skip 1

--compute sum label 'Grand Total' of QQQ on report
--compute sum label 'Total Units' of QQQ on HH

select
it.sku_id A,
'"' || sku.description || '"' B,
nvl(it.REFERENCE_ID,'   ') C,
nvl(pl.qty_due,0) D,
sum(it.UPDATE_QTY) E,
sum(it.UPDATE_QTY)-nvl(pl.qty_due,0) EE,
ph.supplier_id F,
max(to_char(it.DSTAMP,'DD/MM/YYYY HH24:MI:SS')) G
from inventory_transaction it,pre_advice_header ph,pre_advice_line pl,sku
where it.CLIENT_ID='SB'
and it.station_id not like 'Auto%'
and it.CODE='Receipt'
and it.DSTAMP>sysdate-8
--and it.Dstamp between to_date('24-oct-2007', 'DD-MON-YYYY') and to_date('31-oct-2007', 'DD-MON-YYYY')+1
and it.REFERENCE_ID=ph.pre_advice_id (+)
and it.REFERENCE_ID=pl.pre_advice_id (+)
and it.line_id=pl.line_id (+)
and it.sku_id=pl.sku_id (+)
and it.sku_id=sku.sku_id
--and it.sku_id='1752'
group by
it.sku_id,
sku.description,
nvl(it.REFERENCE_ID,'   '),
nvl(pl.qty_due,0),
ph.supplier_id
order by
C,A
/
