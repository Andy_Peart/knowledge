SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON
 
column A format A6
column B format A6
column C format A12
column D format A12
column E format A12
column F format A18
column FF format A28
column G format A40
column GG format A16
column H format 99999
column I format 99999
column J format 99999


select 'Sending Client,Receiving Client,Order ID,Date Shipped,Date Completed,SKU,Description,Colour,CIMS Code,Qty Shipped,Qty Received, Shortfall'
from dual;


select
oh.client_id A,
ph.client_id B,
oh.order_id C,
to_char(oh.shipped_date,'DD/MM/YYYY') D,
to_char(it.Dstamp,'DD/MM/YYYY') E,
ol.sku_id||'''' F,
sku.description G,
sku.colour GG,
sku.user_def_type_2||'-'||sku.user_def_type_1||'-'||sku.user_def_type_4||'-'||sku.sku_size FF,
nvl(ol.qty_shipped,0) H,
nvl(pl.qty_received,0) I,
nvl(ol.qty_shipped,0)-nvl(pl.qty_received,0) J
from pre_advice_header ph,pre_advice_line pl,order_header oh,order_line ol,sku,inventory_transaction it
where oh.order_id='&&2'
and oh.order_id=ph.pre_advice_id
and oh.order_id=ol.order_id
and ph.pre_advice_id=pl.pre_advice_id
and ol.sku_id=pl.sku_id
and ol.line_id=pl.line_id
and pl.sku_id=sku.sku_id
and sku.client_id=oh.client_id
and oh.order_id=it.reference_id
and it.client_id=oh.client_id
and it.notes like 'In Progress --> Complete'
order by A,B,C,F
/
