SET FEEDBACK OFF
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
set colsep ','
set heading off
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9999
SET TRIMSPOOL ON


column A heading 'SKU' format a20
column B heading 'Description' format a40
column C heading 'Location' format a12
column D heading 'QTY' format 9999999
column E heading 'Range' format a25
column F heading 'Season' format a25

--spool sbfullsku.csv

select 'SKU,Description,Location,QTY,Range,Season'
from dual;

select
s.sku_id A,
s.description B,
i.location_id C,
sum(nvl(i.qty_on_hand, 0)) D,
s.user_def_type_5 E,
s.user_def_type_3 F
from inventory i 
inner join sku s on i.client_id = s.client_id and i.sku_id = s.sku_id
where i.client_id = 'SB'
group by
s.sku_id,
s.description,
i.location_id,
s.user_def_type_5,
s.user_def_type_3
order by
A,C
/

--spool off

