SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ' '



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 100

column A heading 'Docket (SKU)' format A14
column B heading 'Location' format A12
column C heading 'Qty' format 9999999
column D heading 'Date Received' format A16

break on report

select 'Prominent - QC Call Off Sheet for Order &&2' from DUAL;

select 
'Docket (SKU)' A,
'Location' B,
'Qty' C
from DUAL;
select ' ' from DUAL;
 

compute sum label 'Total' of C on report

break on A skip 1 on B skip 1

select
ol.sku_id A,
M.from_loc_id B,
i.receipt_id C
from order_line ol,inventory i, move_task m
WHERE OL.CLIENT_ID='PR'
and ol.order_id='&&2'
and ol.sku_id=i.sku_id
AND I.CLIENT_ID='PR'
and m.task_id=ol.order_id
and i.tag_id=m.tag_id
ORDER BY A
/


select ' ' from DUAL;
select ' ' from DUAL;


select 'Received by QC |.......................|   |..........................|' from DUAL;
select '                        PRINT                         SIGN' from DUAL;

select ' ' from DUAL;
select ' ' from DUAL;
select 'QC Comments.................................................................' from DUAL;
select ' ' from DUAL;
select '           .................................................................' from DUAL;

select ' ' from DUAL;
select ' ' from DUAL;
select 'Received back' from DUAL;
select 'by  Prominent  |.......................|   |..........................|' from DUAL;
select '                        PRINT                         SIGN' from DUAL;
