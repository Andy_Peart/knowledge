SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 200
SET TRIMSPOOL ON

column A heading 'User ID' format a14
column B heading 'Name' format a30
column C heading 'Picks' format 999999
column D heading 'Receipts' format 999999
column E heading 'Relocates' format 999999
column F heading 'Returns' format 999999
column G heading 'IDTs' format 999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



--spool band.csv


select 'User ID,Name,Code,Start of Break,End of Break,Code,Time Differance,' from DUAL;


select
UU,
name,
CC,
to_char(DD,'HH24:MI') F,
to_char(DDD,'HH24:MI') G,
CCC,
EEE-EE E,
' '
from (select
UU,
code CC,
DD,
to_number(to_char(DD,'HH24'),99)*60+to_number(to_char(DD,'MI'),99) EE
from (select
user_id UU,
max(Dstamp) DD
from inventory_transaction
where client_id='&&2'
and trunc(Dstamp)=to_date('&&3', 'DD-MON-YYYY')
and to_char(Dstamp,'HH24MI') < '&&4'
and elapsed_time>0
group by
user_id),inventory_transaction
where client_id='&&2'
and UU=user_id
and DD=Dstamp),(select
UUU,
code CCC,
DDD,
to_number(to_char(DDD,'HH24'),99)*60+to_number(to_char(DDD,'MI'),99) EEE
from (select
user_id UUU,
min(Dstamp) DDD
from inventory_transaction
where client_id='&&2'
and trunc(Dstamp)=to_date('&&3', 'DD-MON-YYYY')
and to_char(Dstamp,'HH24MI') > '&&5'
and elapsed_time>0
group by
user_id),inventory_transaction
where client_id='&&2'
and UUU=user_id
and DDD=Dstamp),application_user
where UU=UUU
and UU=user_id
order by
UU
/

--spool off
