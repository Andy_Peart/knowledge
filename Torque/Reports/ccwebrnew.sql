SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Return Date,Client,Order #,Creation Date,Contact,Country,Sku,Description,Colour,Size,Qty,Reason ID,Reason' from dual
union all
select "RDate" ||','|| "Client" ||','|| "Ord" ||','|| "CDate" ||','|| "Contact" ||','|| "Country" ||','|| "Sku"
||','|| "Desc" ||','|| "Col" ||','|| "Size" ||','|| "Qty" ||','|| "Reason" ||','|| "Reason note"
from
(
select to_char(itl.dstamp, 'DD-MON-YYYY')   as "RDate"
, itl.client_id                             as "Client"
, itl.reference_id                          as "Ord"
, to_char(oh.CREATION_DATE,'DD-MON-YYYY')   as "CDate"
, oh.contact                                as "Contact"
, oh.country                                as "Country"
, itl.sku_id                                as "Sku"
, s.description                             as "Desc"
, s.colour                                  as "Col"
, s.sku_size                                as "Size"
, itl.UPDATE_QTY                            as "Qty"
, substr(itl.REASON_ID,3,2)                 as "Reason"
, r.notes                                   as "Reason note"
from inventory_transaction itl
left join order_header oh on itl.client_id = oh.client_id and oh.order_id = itl.reference_id
inner join SKU S on itl.sku_id = s.sku_id and itl.client_id = s.client_id
left join return_reason r on itl.reason_id = r.REASON_ID
where itl.client_id = 'CC'
and itl.dstamp between to_date('&&2') and to_date('&&3')+1
and itl.code in ('Return')
order by 1
)
/
--spool off