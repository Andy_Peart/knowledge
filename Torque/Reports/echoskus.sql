/* SKU Count Report */

SET FEEDBACK OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES


 
/* Set up page and line */
SET PAGESIZE 10
SET NEWPAGE 0
SET LINESIZE 132
 
/* Top Title */
TTITLE LEFT "ECHO WAREHOUSE SKU COUNT" skip 2

column A heading 'No. Of Different|SKUs In Stock'
column B heading 'Site'

select site_id B,count (distinct sku_id) A
from inventory
where client_id = 'ECHO'
group by site_id
/

