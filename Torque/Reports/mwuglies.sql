SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

select 'Sku,Description,Qty on Hand,Location,Zone' from dual
union all
select "Sku" || ',' || "Description" || ',' || "Qty on Hand" || ',' || "Location" || ',' || "Zone"
from
(
select i.sku_id         as "Sku"
, sku.description       as "Description"
, sum(i.qty_on_hand)    as "Qty on Hand"
, i.location_id         as "Location"
, l.work_zone           as "Zone"
from inventory i
inner join sku on sku.client_id = i.client_id and sku.sku_id = i.sku_id
inner join location l on l.site_id = i.site_id and l.location_id = i.location_id
where i.client_id = 'MOUNTAIN'
and sku.ugly = 'Y'
and l.work_zone not in (
'ZMW'
, 'AMW'
, '1WOPS'
, 'BULK'
)
group by i.sku_id
, sku.description
, i.location_id
, l.work_zone
order by 4
)
/

--spool off