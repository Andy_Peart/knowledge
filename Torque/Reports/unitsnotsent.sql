set feedback off
set pagesize 68
set linesize 240
set verify off

clear columns
clear breaks
clear computes

column A heading 'Order No.' format A16
column B heading 'Ordered' format 999999
column C heading 'Picked' format 999999
column D heading 'Not Picked' format 999999
column E heading 'Lines' format 999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'Units Completed but NOT Dispatched - for client ID PRI as at ' report_date skip 2
set head on

break on report
compute sum label 'Wholesale Total' of E B C D  on report

select 
AA A,
EE E,
BB B,
CC C,
BB-CC D
from (select
oh.order_id as AA,
nvl(sum(ol.qty_ordered),0) as BB,
nvl(sum(ol.qty_picked),0) as CC,
nvl(sum(ol.qty_shipped),0) as DD,
count(*) EE
from order_header oh,order_line ol
where oh.client_id='PRI'
and oh.order_id=ol.order_id
and oh.status<>'Shipped'
and substr(oh.order_id,1,2)='31'
group by oh.order_id)
where cc>0
/

ttitle  LEFT ' '
set head off

break on report
compute sum label 'Retail Total' of B C D E on report

select 
AA A,
EE E,
BB B,
CC C,
BB-CC D
from (select
oh.order_id as AA,
nvl(sum(ol.qty_ordered),0) as BB,
nvl(sum(ol.qty_picked),0) as CC,
nvl(sum(ol.qty_shipped),0) as DD,
count(*) EE
from order_header oh,order_line ol
where oh.client_id='PRI'
and oh.order_id=ol.order_id
and oh.status<>'Shipped'
and substr(oh.order_id,1,2)='51'
group by oh.order_id)
where cc>0
/

