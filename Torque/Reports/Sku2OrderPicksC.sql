/******************************************************************************/
/*                                                                            */
/*                            Elite                                           */
/*                                                                            */
/*     FILE NAME  :    Sku2OrderPicksC.sql                                    */
/*                                                                            */
/*     DESCRIPTION:    Datastream for Picking by SKU per order for TR         */
/*                                                                            */
/*   DATE     BY   PROJ       ID         DESCRIPTION                          */
/*   ======== ==== ======     ========   =============                        */
/*   14/12/07 BK   TR                   Picking by SKU for TR (original SQL)  */
/*   14/12/07 LH   TR                   Adaption of original SQL for Complier */
/*                                                                            */
/******************************************************************************/

/* Input Parameters are as follows */
-- 1)  GMT
-- 2)  Client
-- 3)  Order_id
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 0
SET VERIFY OFF
SET FEEDBACK OFF
SET TAB OFF

 
-- Set the line length to the maximum possible, but trim 
-- trailing spaces for both screen output and spool files
SET LINESIZE 32767
SET WRAP ON
SET TRIMOUT ON
SET TRIMSPOOL ON

/* Define the format of the output dates */
DEFINE	OutputDateFormat = 'DD-MON-YYYY'
COLUMN	sorter	NOPRINT

SELECT Distinct '0' sorter,
       'DSTREAM_TR_PICK_BY_SKU_HDR'          			||'|'||
	'&&2'			       			||'|'||
	'&&3'							||'|'||
	sysdate							||'|'||
	'&&4'
from dual
union all
select Distinct '0' sorter,
		'DSTREAM_TR_PICK_BY_SKU_LINE'		||'|'||
rtrim (ol.sku_id)		||'|'||
rtrim (sku.description)		||'|'||
rtrim (sku.colour)	||'|'||
rtrim (sku.sku_size)	||'|'||
rtrim (oh.order_id)	||'|'||
rtrim (sku.ean)	||'|'||
rtrim (ol.line_id) 
from order_header oh,order_line ol,sku
where oh.client_id='&&2'
and oh.order_id=ol.order_id
--and oh.status<>'Shipped'
and oh.order_id like '&&3%'
and ol.sku_id = sku.sku_id
/
