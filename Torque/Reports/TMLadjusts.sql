SET FEEDBACK OFF                
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 400
SET TRIMSPOOL ON

column A heading 'SKU' format a18
column B heading 'CIMS' format a28
column C heading 'Qty Due' format 999999
column D heading 'Date' format a12
column E heading 'Notes' format a12
column F heading 'Reason Code' format a12


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

--break on report

--compute sum label 'Total' of C D  on report

--spool tmladjusts.csv

select 'SKU,CIMS Code,Update Qty,Txn Date,Notes,Reason Code' from DUAL;


select
FFF||'''' A,
sku.user_def_type_2||'-'||sku.user_def_type_1||'-'||sku.user_def_type_4||'-'||sku.sku_size B,
HHH C,
KKK D,
CCC E,
AAA F
from sku,(select
reason_id AAA,
notes CCC,
sku_id FFF,
to_char(dstamp,'DD/MM/YY') KKK,
sum(update_qty) HHH
from inventory_transaction
where client_id = 'TR'
and dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1
and code = 'Adjustment'
and reason_id='T995'
group by
reason_id,
notes,
sku_id,
to_char(dstamp,'DD/MM/YY'))
where FFF=sku.sku_id
and sku.client_id = 'TR'
order by
A,D
/

--spool off
