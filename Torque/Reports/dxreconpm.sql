SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','
SET UND OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 320
SET TRIMSPOOL ON


column A1 format A18
column A format A28
column B format A40
column Z format A12
column C format 9999999
column D format 9999999
column E format 9999999
column F format 9999999
column G format 9999999
column H format 9999999
column J format 9999999
column K format 9999999
column L format 9999999
column N format 9999999
column P format 9999999
column Q format 9999999
column V format 9999999
column W format 9999999


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(sysdate, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


COLUMN NEWSEQ NEW_VALUE SEQNO


select 'S://redprairie/apps/home/dcsdba/reports/LIVE/DXDR'||to_char(sysdate, '_YYYYMMDD')||'.csv' NEWSEQ from DUAL;
--select 'LIVE/DXDR'||to_char(sysdate, '_YYYYMMDD')||'.csv' NEWSEQ from DUAL;

--break on report

--compute sum label 'Total'  of C D E J K G N P Q F V W on report


spool &SEQNO append

--select ',Elite W,Elite QUAR,Elite FATY,Elite DAMG,Elite QVC,Elite IDEAL,Elite Suspense,Elite Transit,Elite Total' from DUAL;

select '  ' from DUAL;

Select
'Shipments - '||
DECODE(DECODE(oh.order_type,'HOME',oh.order_type||substr(oh.user_def_type_1,3,2),oh.order_type),'HOMEHH','Hot Hair','HOMENI','Natural Image',
'HOMEPY','Paula Young','WHOLESALEB','WHOLESALE',DECODE(oh.order_type,'HOME',oh.order_type||substr(oh.user_def_type_1,3,2),oh.order_type)) A,
0-sum(update_qty) D,
',,,,,',
0-sum(update_qty) D
from inventory_transaction it,order_header oh
where it.client_id='DX'
and it.code='Shipment'
and trunc(it.Dstamp)=trunc(sysdate)
and oh.client_id='DX'
and it.reference_id=oh.order_id
and nvl(condition_id,' ') not in ('QVC','IDEAL')
group by
DECODE(DECODE(oh.order_type,'HOME',oh.order_type||substr(oh.user_def_type_1,3,2),oh.order_type),'HOMEHH','Hot Hair','HOMENI','Natural Image',
'HOMEPY','Paula Young','WHOLESALEB','WHOLESALE',DECODE(oh.order_type,'HOME',oh.order_type||substr(oh.user_def_type_1,3,2),oh.order_type))
order by A
/

Select
'Shipments - '||
DECODE(DECODE(oh.order_type,'HOME',oh.order_type||substr(oh.user_def_type_1,3,2),oh.order_type),'HOMEHH','Hot Hair','HOMENI','Natural Image',
'HOMEPY','Paula Young','WHOLESALEB','WHOLESALE',DECODE(oh.order_type,'HOME',oh.order_type||substr(oh.user_def_type_1,3,2),oh.order_type)) A,
',,,',
0-sum(update_qty) D,
',',
0-sum(update_qty) D
from inventory_transaction it,order_header oh
where it.client_id='DX'
and it.code='Shipment'
and trunc(it.Dstamp)=trunc(sysdate)
and oh.client_id='DX'
and it.reference_id=oh.order_id
and condition_id='QVC'
group by
DECODE(DECODE(oh.order_type,'HOME',oh.order_type||substr(oh.user_def_type_1,3,2),oh.order_type),'HOMEHH','Hot Hair','HOMENI','Natural Image',
'HOMEPY','Paula Young','WHOLESALEB','WHOLESALE',DECODE(oh.order_type,'HOME',oh.order_type||substr(oh.user_def_type_1,3,2),oh.order_type))
order by A
/

Select
'Shipments - '||
DECODE(DECODE(oh.order_type,'HOME',oh.order_type||substr(oh.user_def_type_1,3,2),oh.order_type),'HOMEHH','Hot Hair','HOMENI','Natural Image',
'HOMEPY','Paula Young','WHOLESALEB','WHOLESALE',DECODE(oh.order_type,'HOME',oh.order_type||substr(oh.user_def_type_1,3,2),oh.order_type)) A,
',,,,',
0-sum(update_qty) D,
'',
0-sum(update_qty) D
from inventory_transaction it,order_header oh
where it.client_id='DX'
and it.code='Shipment'
and trunc(it.Dstamp)=trunc(sysdate)
and oh.client_id='DX'
and it.reference_id=oh.order_id
and condition_id='IDEAL'
group by
DECODE(DECODE(oh.order_type,'HOME',oh.order_type||substr(oh.user_def_type_1,3,2),oh.order_type),'HOMEHH','Hot Hair','HOMENI','Natural Image',
'HOMEPY','Paula Young','WHOLESALEB','WHOLESALE',DECODE(oh.order_type,'HOME',oh.order_type||substr(oh.user_def_type_1,3,2),oh.order_type))
order by A
/


Select
'UnShipments' A,
nvl(sum(update_qty),0) D,
',,,,,',
nvl(sum(update_qty),0) D
from inventory_transaction it
where it.client_id='DX'
and it.code='UnShipment'
and trunc(it.Dstamp)=trunc(sysdate)
/

select
'Returns' A,
CCC-QUAR-FATY-DAMG-QVC-IDEAL-SUSP C,
QUAR D,
FATY E,
DAMG F,
QVC H,
IDEAL J,
--SUSP K,
0-TRT L,
CCC-TRT N
from (Select
nvl(sum(update_qty),0) CCC
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and from_loc_id in ('DXBRET','DXBREJ')
and nvl(to_loc_id,' ') not in ('DXBRET','DXBREJ')
and code='Relocate'),(Select
nvl(sum(update_qty),0) QUAR
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and from_loc_id in ('DXBRET','DXBREJ')
and nvl(to_loc_id,' ') not in ('DXBRET','DXBREJ')
and condition_id='QUAR'
and code='Relocate'),(Select
nvl(sum(update_qty),0) FATY
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and from_loc_id in ('DXBRET','DXBREJ')
and nvl(to_loc_id,' ') not in ('DXBRET','DXBREJ')
and condition_id='FATY'
and code='Relocate'),(Select
NVL(sum(update_qty),0) DAMG
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and from_loc_id in ('DXBRET','DXBREJ')
and nvl(to_loc_id,' ') not in ('DXBRET','DXBREJ')
and condition_id='DMGD'
and code='Relocate'),(Select
nvl(sum(update_qty),0) QVC
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and condition_id in ('QVC')
and from_loc_id in ('DXBRET','DXBREJ')
and nvl(to_loc_id,' ') not in ('DXBRET','DXBREJ')
and code='Relocate'),(Select
nvl(sum(update_qty),0) IDEAL
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and condition_id in ('IDEAL')
and from_loc_id in ('DXBRET','DXBREJ')
and nvl(to_loc_id,' ') not in ('DXBRET','DXBREJ')
and code='Relocate'),(Select
nvl(sum(update_qty),0) SUSP
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and from_loc_id='SUSPENSE'
and from_loc_id in ('DXBRET','DXBREJ')
and nvl(to_loc_id,' ') not in ('DXBRET','DXBREJ')
and code='Relocate'),(Select
nvl(sum(update_qty),0) TRT
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and from_loc_id in ('DXBRET','DXBREJ')
and nvl(to_loc_id,' ') not in ('DXBRET','DXBREJ')
and code='Relocate')
/


 
select
'Movements' A,
0-QUARB+QUARA-FATYB+FATYA-DAMGB+DAMGA+QVC+IDEAL+SUSP-TRT C,
QUARB-QUARA D,
FATYB-FATYA E,
DAMGB-DAMGA F,
0-QVC H,
0-IDEAL J,
--0-SUSP K,
TRT L,
0 N
--CCC N
from (Select
nvl(sum(update_qty),0) CCC
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and to_loc_id in ('DXBRET','DXBREJ')
and nvl(from_loc_id,' ') not in ('DXBRET','DXBREJ')
and code='Relocate'),(Select
nvl(sum(update_qty),0) QUARA
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and from_loc_id in ('DXBREJ1')
and condition_id='QUAR'
and code='Relocate'),(Select
nvl(sum(update_qty),0) QUARB
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and to_loc_id in ('DXBREJ1')
and nvl(from_loc_id,' ') not in ('DXBRET','DXBREJ')
and condition_id='QUAR'
and code='Relocate'),(Select
nvl(sum(update_qty),0) FATYA
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and from_loc_id in ('DXBREJ1')
and condition_id='FATY'
and code='Relocate'),(Select
nvl(sum(update_qty),0) FATYB
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and to_loc_id in ('DXBREJ1')
and nvl(from_loc_id,' ') not in ('DXBRET','DXBREJ')
and condition_id='FATY'
and code='Relocate'),(Select
NVL(sum(update_qty),0) DAMGA
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and from_loc_id in ('DXBREJ1')
and condition_id='DMGD'
and code='Relocate'),(Select
NVL(sum(update_qty),0) DAMGB
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and to_loc_id in ('DXBREJ1')
and nvl(from_loc_id,' ') not in ('DXBRET','DXBREJ')
and condition_id='DMGD'
and code='Relocate'),(Select
nvl(sum(update_qty),0) QVC
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and condition_id in ('QVC')
and nvl(from_loc_id,' ') in ('C01A')
--and nvl(from_loc_id,' ') not in ('DXBRET','DXBREJ')
and code='Relocate'),(Select
nvl(sum(update_qty),0) IDEAL
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and condition_id in ('IDEAL')
and nvl(from_loc_id,' ') in ('B01A')
--and nvl(from_loc_id,' ') not in ('DXBRET','DXBREJ')
and code='Relocate'),(Select
nvl(sum(update_qty),0) SUSP
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and from_loc_id='SUSPENSE'
and nvl(from_loc_id,' ') not in ('DXBRET','DXBREJ')
and code='Relocate'),(Select
nvl(sum(update_qty),0) TRT
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and nvl(from_loc_id,' ') not in ('DXBRET','DXBREJ')
and to_loc_id in ('DXBRET','DXBREJ')
and code='Relocate')
/

select
'Receipts' A,
CCC-QUAR-FATY-DAMG-QVC-IDEAL-SUSP-TRT C,
QUAR D,
FATY E,
DAMG F,
QVC H,
IDEAL J,
--SUSP K,
TRT L,
CCC N
from (Select
nvl(sum(update_qty),0) CCC
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and code='Receipt'
and reference_id in (select
pre_advice_id from pre_advice_header
where client_id='DX')),(Select
nvl(sum(update_qty),0) QUAR
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and condition_id='QUAR'
and code='Receipt'
and reference_id in (select
pre_advice_id from pre_advice_header
where client_id='DX')),(Select
nvl(sum(update_qty),0) FATY
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and condition_id='FATY'
and code='Receipt'
and reference_id in (select
pre_advice_id from pre_advice_header
where client_id='DX')),(Select
NVL(sum(update_qty),0) DAMG
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and condition_id='DMGD'
and code='Receipt'
and reference_id in (select
pre_advice_id from pre_advice_header
where client_id='DX')),(Select
nvl(sum(update_qty),0) QVC
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and condition_id in ('QVC')
and code='Receipt'
and reference_id in (select
pre_advice_id from pre_advice_header
where client_id='DX')),(Select
nvl(sum(update_qty),0) IDEAL
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and condition_id in ('IDEAL')
and code='Receipt'
and reference_id in (select
pre_advice_id from pre_advice_header
where client_id='DX')),(Select
nvl(sum(update_qty),0) SUSP
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and from_loc_id='SUSPENSE'
and code='Receipt'
and reference_id in (select
pre_advice_id from pre_advice_header
where client_id='DX')),(Select
nvl(sum(update_qty),0) TRT
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and final_loc_id in  ('DXBRET','DXBREJ')
and code='Receipt')
/

select
'Receipts (Blind)' A,
CCC-QUAR-FATY-DAMG-QVC-IDEAL-SUSP-TRT C,
QUAR D,
FATY E,
DAMG F,
QVC H,
IDEAL J,
--SUSP K,
TRT L,
CCC N
from (Select
nvl(sum(update_qty),0) CCC
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
--and to_loc_id not in ('DXBRET','DXBREJ')
and code='Receipt'
and nvl(reference_id,' ') not in (select
pre_advice_id from pre_advice_header
where client_id='DX')),(Select
nvl(sum(update_qty),0) QUAR
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and condition_id='QUAR'
and nvl(to_loc_id,' ') not in ('DXBRET','DXBREJ')
and code='Receipt'
and nvl(reference_id,' ') not in (select
pre_advice_id from pre_advice_header
where client_id='DX')),(Select
nvl(sum(update_qty),0) FATY
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and condition_id='FATY'
and nvl(to_loc_id,' ') not in ('DXBRET','DXBREJ')
and code='Receipt'
and nvl(reference_id,' ') not in (select
pre_advice_id from pre_advice_header
where client_id='DX')),(Select
NVL(sum(update_qty),0) DAMG
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and condition_id='DMGD'
and nvl(to_loc_id,' ') not in ('DXBRET','DXBREJ')
and code='Receipt'
and nvl(reference_id,' ') not in (select
pre_advice_id from pre_advice_header
where client_id='DX')),(Select
nvl(sum(update_qty),0) QVC
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and condition_id in ('QVC')
and nvl(to_loc_id,' ') not in ('DXBRET','DXBREJ')
and code='Receipt'
and nvl(reference_id,' ') not in (select
pre_advice_id from pre_advice_header
where client_id='DX')),(Select
nvl(sum(update_qty),0) IDEAL
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and condition_id in ('IDEAL')
and nvl(to_loc_id,' ') not in ('DXBRET','DXBREJ')
and code='Receipt'
and nvl(reference_id,' ') not in (select
pre_advice_id from pre_advice_header
where client_id='DX')),(Select
nvl(sum(update_qty),0) SUSP
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and from_loc_id='SUSPENSE'
and nvl(to_loc_id,' ') not in ('DXBRET','DXBREJ')
and code='Receipt'
and nvl(reference_id,' ') not in (select
pre_advice_id from pre_advice_header
where client_id='DX')),(Select
nvl(sum(update_qty),0) TRT
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and nvl(to_loc_id,' ') in  ('DXBRET','DXBREJ')
and code='Receipt')
/

select
'Adjustments' A,
CCC-QUARA+QUARB-FATYA+FATYB-DAMGA+DAMGB-QVCA-IDEALA-SUSP-TRT C,
QUARA-QUARB D,
FATYA-FATYB E,
DAMGA-DAMGB F,
QVCA H,
IDEALA J,
--SUSP K,
TRT L,
CCC N
from (Select
nvl(sum(update_qty),0) CCC
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and code in ('Adjustment')),(Select
nvl(sum(update_qty),0) QUARA
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and from_loc_id in ('DXBREJ1')
and condition_id='QUAR'
and code in ('Adjustment','Cond Update')),(Select
nvl(sum(update_qty),0) QUARB
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and from_loc_id in ('DXBREJ1')
and notes like 'QUAR%'
and code in ('Cond Update')),(Select
nvl(sum(update_qty),0) FATYA
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and from_loc_id in ('DXBREJ1')
and condition_id='FATY'
and code in ('Adjustment','Cond Update')),(Select
nvl(sum(update_qty),0) FATYB
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and from_loc_id in ('DXBREJ1')
and notes like 'FATY%'
and code in ('Cond Update')),(Select
nvl(sum(update_qty),0) DAMGA
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and from_loc_id in ('DXBREJ1')
and condition_id='DMGD'
and code in ('Adjustment','Cond Update')),(Select
nvl(sum(update_qty),0) DAMGB
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and from_loc_id in ('DXBREJ1')
and notes like 'DMGD%'
and code in ('Cond Update')),(Select
nvl(sum(update_qty),0) QVCA
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and from_loc_id not in ('DXBREJ1')
and condition_id in ('QVC')
and ((code='Adjustment'
and to_loc_id not in  ('DXBRET','DXBREJ'))
or (code='Cond Update' and notes<>' '))),(Select
nvl(sum(update_qty),0) IDEALA
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and from_loc_id not in ('DXBREJ1')
and condition_id in ('IDEAL')
and ((code='Adjustment'
and to_loc_id not in  ('DXBRET','DXBREJ'))
or (code='Cond Update' and notes<>' '))),(Select
nvl(sum(update_qty),0) SUSP
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and from_loc_id='SUSPENSE'
and code in ('Cond Update')),(Select
nvl(sum(update_qty),0) TRT
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and to_loc_id in  ('DXBRET','DXBREJ')
and code in ('Adjustment'))
/

select
'Stock Checks' A,
CCC-QUAR-FATY-DAMG-QVC-IDEAL-SUSP-TRT C,
QUAR D,
FATY E,
DAMG F,
QVC H,
IDEAL J,
--SUSP K,
TRT L,
CCC N
from (Select
nvl(sum(update_qty),0) CCC
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and code='Stock Check'),(Select
nvl(sum(update_qty),0) QUAR
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and final_loc_id in ('DXBREJ1')
and condition_id='QUAR'
and code='Stock Check'),(Select
nvl(sum(update_qty),0) FATY
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and final_loc_id in ('DXBREJ1')
and condition_id='FATY'
and code='Stock Check'),(Select
NVL(sum(update_qty),0) DAMG
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and final_loc_id in ('DXBREJ1')
and condition_id='DMGD'
and code='Stock Check'),(Select
nvl(sum(update_qty),0) QVC
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and condition_id in ('QVC')
and code='Stock Check'),(Select
nvl(sum(update_qty),0) IDEAL
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and condition_id in ('IDEAL')
and code='Stock Check'),(Select
nvl(sum(update_qty),0) SUSP
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and from_loc_id='SUSPENSE'
and code='Stock Check'),(Select
nvl(sum(update_qty),0) TRT
from inventory_transaction
where client_id='DX'
and trunc(Dstamp)=trunc(sysdate)
and final_loc_id in  ('DXBRET','DXBREJ')
and code='Stock Check')
/

select '  ' from DUAL;

select
'Closing Stock' A,
CCC-QUAR-FATY-DAMG-QVC-IDEAL-SUSP-TRT C,
QUAR D,
FATY E,
DAMG F,
QVC H,
IDEAL J,
--SUSP K,
TRT L,
CCC N
from (Select
sum(qty_on_hand) CCC
from inventory
where client_id='DX'),(Select
sum(qty_on_hand) QUAR
from inventory
where client_id='DX'
and location_id in ('DXBREJ1')
and condition_id='QUAR'),(Select
sum(qty_on_hand) FATY
from inventory
where client_id='DX'
and location_id in ('DXBREJ1')
and condition_id='FATY'),(Select
NVL(sum(qty_on_hand),0) DAMG
from inventory
where client_id='DX'
and location_id in ('DXBREJ1')
and condition_id='DMGD'),(Select
nvl(sum(qty_on_hand),0) QVC
from inventory
where client_id='DX'
and zone_1 in ('QVC')),(Select
nvl(sum(qty_on_hand),0) IDEAL
from inventory
where client_id='DX'
and zone_1 in ('IDEAL')),(Select
nvl(sum(qty_on_hand),0) SUSP
from inventory
where client_id='DX'
and location_id='SUSPENSE'),(Select
nvl(sum(qty_on_hand),0) TRT
from inventory
where client_id='DX'
and location_id in  ('DXBRET','DXBREJ'))
/

spool off
