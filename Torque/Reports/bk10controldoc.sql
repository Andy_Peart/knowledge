SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 66
SET NEWPAGE 0
SET LINESIZE 240

column A heading 'Store' format A6
column B heading 'Name' format A30
column C heading 'Order/List' format A20
column D heading 'Move Tasks' format 99999999
column E heading 'Units to Pick' format 99999999
column G heading 'Units of HANG' format 99999999
column F heading 'Consignment' format A14
column M heading 'Run Number' format a4 

ttitle  LEFT 'Outstanding Move Tasks for Week &&2 RUN &&3' skip 2


--select 'Store,Name,Consignment,Move Tasks,Units to Pick,Units of HANG' from DUAL;
 
set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DAY, DD MONTH, YYYY') curdate from DUAL;
set TERMOUT ON


break on A dup skip 1 on report 

compute sum label 'Total' of D on report


select
oh.customer_id A,
a.name B,
oh.consignment F,
sum(nvl(CT,0)) D,
substr(oh.consignment,7,4) M
from order_header oh,address a,
(select
oh.consignment OOO,
--nvl(count(*),0) CT
nvl(sum(mt.qty_to_move),0) CT
from order_header oh, move_task mt
where oh.client_id='TR'
and oh.order_type <> 'WEB'
and mt.client_id='TR'
--and mt.status<>'Error'
and mt.status='Released'
and oh.consignment=mt.consignment
and oh.order_id=mt.task_id
group by
oh.consignment)
where oh.consignment=OOO (+)
and oh.status not in ('Cancelled','Hold')
and oh.consignment like '&&2%'
and substr(oh.consignment,7,1)='&&3'
and oh.client_id='TR'
and oh.order_type <> 'WEB'
and a.client_id='TR'
and oh.customer_id=a.address_id
and CT>0
group by
oh.customer_id,
a.name ,
oh.consignment
order by A,B
/
