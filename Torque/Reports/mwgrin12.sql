SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET WRAP Off


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 66
SET NEWPAGE 0
SET LINESIZE 132
SET HEADING OFF

 
column E heading 'Pre Advice' format A10
column F heading 'Line' format 99999
column G heading 'SKU Code' format A18
column H heading 'Description' format A40
column I heading 'Qty Due' format 9999999
column J heading 'Zone 12' format 9999999

ttitle left 'Goods Received Report - for MOUNTAIN pre-advice &&2 ' 


SET PAGESIZE 66
SET HEADING ON

--BREAK ON B on D on E on REPORT
--column B new_value null
--column D new_value null
--column E new_value null

--BREAK ON REPORT
--compute sum label 'Total' of J  on report


select
ph.pre_advice_id E,
ph.line_id F,
ph.sku_id G,
sku.description H,
nvl(ph.qty_due,0) I,
nvl(QQ,0) J
from pre_advice_line ph,sku,(select
iv.sku_id SK,
sum(iv.qty_on_hand) QQ
from inventory iv,location L
where iv.client_id='MOUNTAIN'
and iv.site_id=L.site_id
and iv.location_id=L.location_id
and L.work_zone='12'
group by
iv.sku_id)
where ph.client_id='MOUNTAIN'
and ph.pre_advice_id='&&2'
and ph.sku_id=sku.sku_id
and sku.client_id='MOUNTAIN'
and ph.sku_id=SK (+)
order by
E,F
/
