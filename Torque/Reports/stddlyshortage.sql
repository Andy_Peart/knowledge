SET FEEDBACK OFF                 
set verify off
set wrap off
set colsep ','

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 300
set trimspool on

column CID format A10
column CD format A12
column RID format A20
column Q1 format 99999
column Q2 format 99999
column Q3 format 99999



--spool stddlyshortage.csv

break on CD dup skip 1
compute sum label 'Totals' of Q1 Q2 Q3 on CD



Select 'Client,Condition,Customer ID,Order Id,Ordered Qty,Allocated,Shortage' from DUAL;

select
CID,
decode(substr(rid,1,1),'S','Wholesale',decode(cd,'3','Retail','203','Outlet','299','Pallet Outlet',cd)) cd,
AID,
RID,
Q1,
Q1-Q2 Q3,
Q2
from (select
oh.client_id CID,
oh.order_id RID,
ol.condition_id cd,
oh.customer_id AID,
sum(ol.qty_ordered) Q1,
nvl(QQQ,0) Q2
from order_header oh,order_line ol,(select
task_id TK,
sum(qty_ordered-qty_tasked) QQQ
from generation_shortage
where client_id='&&2'
group by task_id
union
select
reference_id TK,
sum(update_qty) QQQ
from inventory_transaction
where client_id='&&2'
and code='Deallocate'
and trunc(Dstamp)=trunc(sysdate)
group by reference_id)
where oh.client_id='&&2'
and trunc(oh.creation_date)=trunc(sysdate)
and ol.order_id=oh.order_id
and ol.client_id=oh.client_id
and oh.order_id=TK(+)
group by
oh.client_id,
oh.order_id,
ol.condition_id,
oh.customer_id,
nvl(qqq,0))
order by CD,RID
/

--spool off


