SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 120

select 'Number of orders picked today for Mail Order client T =  '||count(distinct reference_id)
from inventory_transaction
where client_id='T'
and code='Pick'
and Dstamp>trunc(sysdate)
/

