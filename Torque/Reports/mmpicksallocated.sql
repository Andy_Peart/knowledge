SET FEEDBACK OFF                 
set pagesize 68
set linesize 120
set verify off
set wrap off
set newpage 0

clear columns
clear breaks
clear computes


column A heading 'Status' format A12
column B heading 'Order Date' format A13
column C heading 'Creation Date' format A13
column D heading 'Pty' format 999
column E heading 'Customer' format A10
column F heading 'Contact' format A32
column G heading 'Order ID' format A12
column H heading 'Ordered' format 999999
column I heading 'Shipped' format 999999



set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



ttitle  LEFT 'Picks Allocated for Client ID &&2' RIGHT 'Printed ' report_date skip 2


--break on A skip 2


select
oh.status A,
to_char(oh.order_date, 'DD/MM/YY') B,
to_char(oh.creation_date, 'DD/MM/YY') C,
oh.priority D,
oh.customer_id E,
oh.contact F,
oh.order_id G,
nvl(sum(nvl(ol.qty_ordered,0)),0) H,
nvl(sum(nvl(ol.qty_shipped,0)),0) I
from order_header oh,order_line ol
where oh.client_id='&&2'  
and oh.order_id=ol.order_id
and status='Allocated'
group by
oh.status,
oh.order_date,
oh.creation_date,
oh.priority,
oh.customer_id,
oh.contact,
oh.order_id
order by
oh.status,
--oh.order_date,
--oh.creation_date
oh.order_id
/