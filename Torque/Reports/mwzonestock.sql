SET FEEDBACK OFF                 
set verify off
set wrap off
set colsep ','

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 132
set trimspool on


--spool mwzonestock.csv

select 'Location,SKU,Description,Qty,Work Zone' from DUAL;

select
ll.location_id ||','''||
i.sku_id ||''','||
i.description ||','||
sum(i.qty_on_hand) ||','||
ll.work_zone 
from location ll, inventory i
where ll.site_id like '&&2'
and ll.work_zone like '&&3'
and ll.current_volume<>0
and ll.location_id=i.location_id
and i.client_id='MOUNTAIN'
group by
ll.location_id,
i.sku_id,
i.description,
ll.work_zone 
order by
ll.location_id,
i.sku_id
/

--spool off
