SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 400
SET TRIMSPOOL ON


column X noprint
column A format A10
column B format A20
column C format 9999999



set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON





--break on G skip 1 dup

--compute sum of QQQ on CCC

--spool cktracking.csv

select 'Order Id,Customer,Tracking Number,Qty Shipped' from DUAL;

select
it.reference_id A,
oh.inv_name,
oh.instructions B,
sum(it.update_qty) C
from inventory_transaction it,order_header oh
where it.client_id = 'CK'
and it.code = 'Shipment'
and trunc(it.Dstamp)=trunc(sysdate)
and oh.client_id='CK'
and oh.order_id=it.reference_id
group by
it.reference_id,
oh.inv_name,
oh.instructions
order by
it.reference_id
/

--spool off