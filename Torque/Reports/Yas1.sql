set pagesize 9999
set linesize 2000
set trimspool on
set colsep ','
set feedback off


select
key,
tag_id, 
client_id, 
sku_id, 
From_Loc_Id, 
update_qty,
Dstamp 
FROM INVENTORY_TRANSACTION
WHERE Client_Id = 'T'
and SITE_ID = 'BRADFORD'
and Code = 'Adjustment'
and update_qty < 0
and trunc(DSTAMP) =trunc(sysdate)
and Reason_Id = 'T760'
/
