SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 600
SET TRIMSPOOL ON

COLUMN A FORMAT a18
COLUMN B FORMAT 999999
COLUMN C FORMAT a40
COLUMN D FORMAT A12
COLUMN Q FORMAT 999999
COLUMN LL FORMAT A300


--spool stdbestsellers1.csv

select ',,Best Sellers for client &&2 for period &&3 to &&4' from Dual;

select 'Qty,Sku,Description,Stock,Locations' from DUAL;

select
B,
''''||A||'''',
C,
nvl(QQ,0) Q,
LL
from (select
A,
C,
QQ,
LL,
sum(B) B
from (select
--oh.order_type D,
sum(ol.Qty_shipped) B,
ol.Sku_id A,
SKU.description C
from order_header oh,order_line ol,sku
where oh.client_id='&&2'
and oh.shipped_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and ol.client_id='&&2'
and oh.order_id=ol.order_id
and sku.client_id='&&2'
and ol.sku_id=sku.sku_id
group by
--oh.order_type,
ol.Sku_id,
SKU.description
union
select
--oh.order_type D,
sum(ol.Qty_shipped) B,
ol.Sku_id A,
SKU.description C
from order_header_archive oh,order_line_archive ol,sku
where oh.client_id='&&2'
and oh.shipped_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and ol.client_id='&&2'
and oh.order_id=ol.order_id
and sku.client_id='&&2'
and ol.sku_id=sku.sku_id
group by
--oh.order_type,
ol.Sku_id,
SKU.description)
,(select iv.sku_id PA,
LISTAGG(iv.location_id||'('||iv.qty_on_hand||')', ':') WITHIN GROUP (ORDER BY iv.location_id) AS LL,
sum(qty_on_hand) QQ
from inventory iv,location L
where iv.client_id = '&&2'
and iv.site_id=L.site_id
and iv.location_id=L.location_id
and L.loc_type='Tag-FIFO'
group by iv.sku_id)
where A=PA (+)
group by A,C,QQ,LL)
where B>'&&5'
order by
B DESC
/

--spool off
