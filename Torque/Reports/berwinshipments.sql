SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON


column A heading 'Type' format a18
column A1 heading 'Name' format a12
column A2 heading 'Reference' format a14
column A3 heading 'SKU' format a14
column A4 heading 'Date' format a14
column B heading 'Units' format 999999
column K heading 'Units' format 999999
column B1 heading 'Rate' format A6
column C heading 'Value' format 999999.99

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON




break on A dup skip 1 on A1 dup skip 1 on report


--compute sum label 'Sub Total' of B C on A2
compute sum label 'Summary' of B C on A1
compute sum label 'Totals' of B C on A
compute sum label 'Overall' of B C on report

--spool berwinshipments.csv

select 'Type,Name,Reference,SKU,Date Shipped,Units,Value' from DUAL;

select
'Shipments' A,
'Hungarian' A1,
reference_id A2,
sku_id A3,
to_char(Dstamp,'dd/mm/yy') A4,
sum(update_qty) B,
sum(update_qty)*0.03 C
from inventory_transaction
where client_id='BW'
and to_char(Dstamp,'mm/yyyy')=to_char(sysdate-1,'mm/yyyy')
and code='Shipment'
--Hungarian
and sku_id like '%-%'
and sku_id not in (select
distinct sku_id
from inventory
where client_id='BW'
and qty_on_hand>0)
group by
reference_id,
sku_id,
to_char(Dstamp,'dd/mm/yy')
union
select
'Shipments' A,
'Chinese' A1,
reference_id A2,
sku_id A3,
to_char(Dstamp,'dd/mm/yy') A4,
sum(update_qty) B,
sum(update_qty)*0.05 C
from inventory_transaction
where client_id='BW'
and to_char(Dstamp,'mm/yyyy')=to_char(sysdate-1,'mm/yyyy')
and code='Shipment'
--Chinese
and sku_id not like '%-%'
and sku_id not in (select
distinct sku_id
from inventory
where client_id='BW'
and qty_on_hand>0)
group by
reference_id,
sku_id,
to_char(Dstamp,'dd/mm/yy')
union
select
A,
A1,
A2,
' ' A3,
A4,
B,
CASE WHEN (C<20 AND B>9) THEN 20
     WHEN (B<10) THEN 0
  	ELSE C
	END C
from (select
'Shipments (Part)' A,
'Hungarian' A1,
reference_id A2,
--sku_id A3,
to_char(Dstamp,'dd/mm/yy') A4,
sum(update_qty) B,
sum(update_qty)*0.18 C
from inventory_transaction
where client_id='BW'
and to_char(Dstamp,'mm/yyyy')=to_char(sysdate-1,'mm/yyyy')
and code='Shipment'
--Hungarian
and sku_id like '%-%'
and sku_id in (select
distinct sku_id
from inventory
where client_id='BW'
and qty_on_hand>0)
group by
reference_id,
--sku_id,
to_char(Dstamp,'dd/mm/yy'))
union
select
A,
A1,
A2,
'  ' A3,
A4,
B,
CASE WHEN (C<20 AND B>9) THEN 20
     WHEN (B<10) THEN 0
  	ELSE C
	END C
from (select
'Shipments (Part)' A,
'Chinese' A1,
reference_id A2,
--sku_id A3,
to_char(Dstamp,'dd/mm/yy') A4,
sum(update_qty) B,
sum(update_qty)*0.20 C
from inventory_transaction
where client_id='BW'
and to_char(Dstamp,'mm/yyyy')=to_char(sysdate-1,'mm/yyyy')
and code='Shipment'
--Chinese
and sku_id not like '%-%'
and sku_id in (select
distinct sku_id
from inventory
where client_id='BW'
and qty_on_hand>0)
group by
reference_id,
--sku_id,
to_char(Dstamp,'dd/mm/yy'))
order by
A,A1,A2
/

--spool off


