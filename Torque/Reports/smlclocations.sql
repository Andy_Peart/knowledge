set feedback off
set pagesize 68
set linesize 240
set verify off
set colsep ' '


clear columns
clear breaks
clear computes

break on Client dup skip 1

select
i.client_id "Client",
L.loc_type "Loc. Type",
count(distinct i.location_id) "Used Locations",
sum(i.qty_on_hand) "Stock on Hand",
sum(i.qty_allocated) "Allocations"
from inventory i,location L
where i.client_id in ('S2','IF','EX','GG','VF')
and i.location_id=L.location_id
and i.site_id=L.site_id
group by
i.client_id,
L.loc_type
order by 1,2
/
