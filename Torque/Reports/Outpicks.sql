SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

 
select 'Outpick Report - for client ID &&2 as at ' 
        ||  to_char(sysdate,'dd/mm/yy  hh24:mi:ss') from dual;

select 'Date Allocated,Sales Order No,Date of Customers Order,Date Order arrived at TORQUE,Customer,Location,SKU Code,Description,Allocated,Ordered,Last User'
from dual;

select
to_char(iv.Move_Dstamp, 'DD/MM/YY ') ||','||
oc.order_id ||','||
to_char(oh.order_date,'DD/MM/YY ') ||','||
to_char(oh.creation_date,'DD/MM/YY ') ||','||
oh.customer_id ||','||
iv.location_id ||','||
iv.sku_id ||','||
sku.description ||','||
iv.qty_allocated ||','||
ol.qty_ordered ||','||
oh.last_updated_by
from inventory iv,sku,order_container oc, order_header oh, order_line ol
where oh.client_id='&&2'  
and iv.container_id=oc.container_id
and oc.order_id=oh.order_id
and oc.order_id=ol.order_id
and iv.sku_id=ol.sku_id
and sku.client_id='&&2'  
and iv.sku_id=sku.sku_id
and sysdate<iv.move_dstamp+2
and iv.location_id<>'BR04'
and iv.location_id<>'MARSHALB'
and oh.status='Allocated'
order by iv.Move_Dstamp,oc.order_id,iv.sku_id
/
select
',,,,,,,Totals' as A,
nvl(sum(iv.qty_allocated),0) ||','||
nvl(sum(ol.qty_ordered),0)
from inventory iv,order_container oc, order_header oh, order_line ol
where oh.client_id='&&2'  
and iv.container_id=oc.container_id
and oc.order_id=oh.order_id
and oc.order_id=ol.order_id
and iv.sku_id=ol.sku_id
and sysdate<iv.move_dstamp+2
and iv.location_id<>'BR04'
and iv.location_id<>'MARSHALB'
and oh.status='Allocated'
/
