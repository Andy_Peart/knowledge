SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON



set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON





select 'Retail Picks by SKU from &&2 to &&3 - for client  MOUNTAIN' from DUAL;

select 'Date,SKU,LPG,Units Picked' from DUAL;



select
to_char(it.dstamp,'DD-MON-YYYY') || ',' ||
''''||it.sku_id||'''' || ',' ||
sku.user_def_type_8 || ',' ||
sum(it.update_qty) as a1
from inventory_transaction it,location ll, sku
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.sku_id = sku.sku_id and it.client_id = sku.client_id
and it.client_id='MOUNTAIN'
and it.update_qty<>0
and it.code='Pick'
and it.work_group<>'WWW'
and it.from_loc_id=ll.location_id
and ll.site_id='BD2'
and ll.zone_1 in ('RETAIL','DYN1')
group by
it.sku_id, sku.user_def_type_8,it.dstamp
order by
it.dstamp
/

