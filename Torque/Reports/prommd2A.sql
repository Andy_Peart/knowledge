SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep '|'


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 200
SET TRIMSPOOL ON
 
--spool prom2.csv


select
A||'|'||
B||'|'||
C||'|'||
D||'|'||
E||'|'||
F||'|'||
G||'|'||
H||'|'||
J||'|'||
K||'|'||
L||'|EA||'||
M||'|'
from (select
ph.pre_advice_id A,
to_char(ph.creation_date,'DD/MM/YYYY') B,
sku.user_def_type_2 C,
pl.line_id D,
pl.sku_id E,
sku.description F,
sku.product_group G,
sku.colour H,
sku.sku_size J,
pl.QTY_DUE K,
pl.QTY_RECEIVED L,
--it.update_qty P,
to_char(ph.finish_dstamp,'DD/MM/YYYY') M
from inventory_transaction it,pre_advice_header ph,pre_advice_line pl,sku
where it.client_id='PR'
and it.code='Receipt'
and it.Dstamp>sysdate-1
and ph.client_id='PR'
and it.reference_id=ph.pre_advice_id
and pl.client_id='PR'
and ph.pre_advice_id=pl.pre_advice_id
and it.sku_id=pl.sku_id
and sku.client_id='PR'
and it.sku_id=sku.sku_id)
order by 1
/


--spool off

