SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 68
SET NEWPAGE 0
SET LINESIZE 240

column A heading 'Order ID' format A12
column B heading 'Order|Creation Date' format A14
column D heading 'Status' format A14
column C heading 'Customer ID' format A14
column M format a4 noprint


--select 'Order ID,Ordered,Picked' from DUAL;
 
set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DAY, DD MONTH, YYYY') curdate from DUAL;
set TERMOUT ON


break on C dup skip 1

--compute sum label 'Total' of D on report


TTITLE left 'Multiple Customer Orders for Client &&2' skip 1

--SET PAGESIZE 66


select
oh.customer_id C,
oh.order_id A,
oh.status D,
to_char(oh.Creation_date,'YY/MM') M,
to_char(oh.Creation_date,'DD/MM/YYYY') B
from order_header oh,
(select
customer_id cc,
count(*) ct
from order_header
where client_id='&&2'
and status not in ('Shipped','Cancelled')
group by
customer_id)
where oh.client_id='&&2'
and status not in ('Shipped','Cancelled')
and ct>1
and oh.customer_id=cc
order by
C,A
/

