SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON


column A format A14
column B format A20
column C format A18
column D format A14
column E format A14
column F format 999999
column G format 999999
column H format 999999
column J format A12
column K format A10
column L format A16

--break on C dup
--compute sum label 'Totals' of F G H on C


--SET TERMOUT OFF
--spool mwsuppstkchk.csv

select 'SKU,Supplier Id,From Location,To Location,Original Qty,Update Qty,New Qty,Date,Time,User,Accuracy'
from dual;

select
''''||sku_id ||'''' B,
supplier_id C,
from_loc_id D,
to_loc_id E,
original_qty F,
update_qty G,
original_qty+update_qty H,
to_char(Dstamp,'DD/MM/YYYY') J,
to_char(Dstamp,'HH24:MM:SS') K,
user_id L,
'' M
from inventory_transaction
where client_id = 'MOUNTAIN'
and code in ('Stock Check')
and from_loc_id in ('ZONE12QC')
and Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
union
select
''''||sku_id ||'''' B,
supplier_id C,
from_loc_id D,
to_loc_id E,
original_qty F,
update_qty G,
original_qty+update_qty H,
to_char(Dstamp,'DD/MM/YYYY') J,
to_char(Dstamp,'HH24:MM:SS') K,
user_id L,
'' M
from inventory_transaction_archive
where client_id = 'MOUNTAIN'
and code in ('Stock Check')
and from_loc_id in ('ZONE12QC')
and Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
union
select
'Totals' B,
C,
'' D,
'' E,
F,
G,
H,
'' J,
'' K,
'' L,
replace(to_char(H*100/F,'9990.00'),' ','') M
from (select
C,
sum(F) F,
sum(G) G,
sum(H) H
from (select
supplier_id C,
original_qty F,
update_qty G,
original_qty+update_qty H
from inventory_transaction
where client_id = 'MOUNTAIN'
and code in ('Stock Check')
and from_loc_id in ('ZONE12QC')
and Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
union
select
supplier_id C,
original_qty F,
update_qty G,
original_qty+update_qty H
from inventory_transaction_archive
where client_id = 'MOUNTAIN'
and code in ('Stock Check')
and from_loc_id in ('ZONE12QC')
and Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1)
group by C)
order by C,B,J,K
/

--spool off
--SET TERMOUT ON
