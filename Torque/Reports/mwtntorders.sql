SET FEEDBACK OFF                 
set verify off
set wrap off
set colsep ','

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 132
set trimspool on

--spool mwtntorders.csv

select 'Shipments for Orders Created from &&2 to &&3 - for client MOUNTAIN' from DUAL;

select 'Order_id,Store Id,Consignment,Lines,Qty Shipped' from DUAL;

select
A||','||
A2||','||
CASE
WHEN B like '%TNT' THEN 'TNT' ELSE '' END ||','||
C||','||
D
from (select
oh.order_id A,
oh.work_group A2,
oh.consignment B,
count(*) C,
sum(ol.qty_shipped) D
from order_header oh, order_line ol
where oh.creation_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and oh.client_id='MOUNTAIN'
and oh.order_id like 'MOR%'
and oh.status='Shipped'
and ol.client_id=oh.client_id
and ol.order_id=oh.order_id
group by
oh.order_id,
oh.work_group,
oh.consignment)
order by
A
/

--spool off
