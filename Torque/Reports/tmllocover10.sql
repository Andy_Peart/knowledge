SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','
SET WRAP OFF


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500

select 'Client,Location,SKU,Qty,Product' from dual
union all
select client_id || ',' || location_id || ',' || sku_id || ',' || qty || ',' || product_group
from (
select inv.client_id, inv.location_id, inv.sku_id, sum(inv.qty_on_hand) qty, sku.product_group 
from inventory inv inner join sku on inv.sku_id = sku.sku_id and inv.client_id = sku.client_id
where inv.client_id = '&&2'
and sku.product_group = 'MS'
and inv.location_id not in ('SUSPENSE','I1')
and inv.location_id not like 'HOLD%'
group by inv.location_id, inv.sku_id,sku.product_group,inv.client_id
having sum(inv.qty_on_hand) > 10
order by 2
)
/