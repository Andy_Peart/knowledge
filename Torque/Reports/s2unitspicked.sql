/******************************************************************************/
/*                                                                            */
/*                            Elite                                           */
/*                                                                            */
/*     FILE NAME  :   mwshortages3.sql                                        */
/*                                                                            */
/*   DATE     BY   PROJ     ID         DESCRIPTION                            */
/*   ======== ==== ======   ========   =============                          */
/*   26/06/07 BK   MM                  Units Picked by User ID (Date Range)   */
/******************************************************************************/

/* Input Parameters are as follows */
-- 1)  From Date
-- 2)  To Date


set feedback off
set pagesize 68
set linesize 240
set newpage 0
set verify off

clear columns
clear breaks
clear computes

column A heading 'User ID' format A16
column F heading 'Client ID' format A14
column B1 heading 'Single Qtys' format 999999
column B2 heading 'Multiple Qtys' format 999999
column B3 heading 'Overall Qty' format 999999
column C heading 'Lines' format 999999
column D heading 'Orders' format 999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'Units Picked from '&&3' to '&&4' - for Client &&2 as at ' report_date skip 2

break on F dup skip 1 on report
compute sum label 'Client Totals' of C D B1 B2 B3 on F
compute sum label 'Overall Totals' of C D B1 B2 B3 on report

select
A,
F,
C,
D,
nvl(B1,0) B1,
nvl(B2,0) B2,
nvl(B1,0)+nvl(B2,0) B3
from (select
A,
F,
count(*) C,
count(distinct RD) D,
sum(QQ) B
from (select
client_id F,
user_id A,
reference_id RD,
sku_id SK,
sum(update_qty) QQ
from inventory_transaction
where client_id='&&2'
and code='Pick'
and (elapsed_time>0 or list_id is not null)
and Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
group by
client_id,
user_id,
reference_id,
sku_id)
where QQ>0
group by A,F
),(select
A1,
F1,
count(*) C1,
count(distinct RD) D1,
sum(QQ) B1
from (select
client_id F1,
user_id A1,
reference_id RD,
--sku_id SK,
--line_id LL,
sum(update_qty) QQ
from inventory_transaction
where client_id='&&2'
and code='Pick'
and (elapsed_time>0 or list_id is not null)
and Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
group by
client_id,
user_id,
reference_id)
--sku_id,
--line_id)
where QQ=1
group by A1,F1),(select
A2,
F2,
count(*) C2,
count(distinct RD) D2,
sum(QQ) B2
from (select
client_id F2,
user_id A2,
reference_id RD,
--sku_id SK,
--line_id LL,
sum(update_qty) QQ
from inventory_transaction
where client_id='&&2'
and code='Pick'
and (elapsed_time>0 or list_id is not null)
and Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
group by
client_id,
user_id,
reference_id)
--sku_id,
--line_id)
where QQ>1
group by A2,F2)
where A=A1 (+)
and A=A2 (+)
and F=F1 (+)
and F=F2 (+)
order by F,A
/
