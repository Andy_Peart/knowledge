SET FEEDBACK OFF                 
set verify off
set wrap off
set colsep ','

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 900
set trimspool on


select
AA||','||
DD||','||
QQ
from (select
oh.client_id AA,
trunc(creation_date) DD,
count(*) QQ
from order_header oh
where status not in ('Cancelled','Shipped')
and creation_date<sysdate-&&3
and client_id like '&&2%'
group by
oh.client_id,
trunc(creation_date)
order by AA,DD)
/
