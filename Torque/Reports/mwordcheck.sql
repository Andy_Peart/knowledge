set pagesize 68
set linesize 120
set verify off

ttitle center "Mountain Warehouse - Order Check Sheet." skip 3

column A heading 'Order' format a8
column C heading 'Line' format 999999
column B heading 'SKU' format a13
column D heading 'QTY' format 99999
column DA heading 'QTY Al' format 99999
column E heading 'Store' format a5
column F heading 'Desc' format a30

break on report

compute sum label 'TOTAL' of D on report
compute sum label '' of DA on report

select oh.order_id A,
ol.line_id C,
oh.customer_id E,
ol.sku_id B,
ol.qty_ordered D,
ol.qty_tasked DA,
s.user_def_type_2 F
from order_header oh, order_line ol, sku s
where oh.client_id = 'MOUNTAIN'
and oh.order_id = upper ('&&2')
and oh.order_id = ol.order_id
and ol.sku_id = s.sku_id
/
