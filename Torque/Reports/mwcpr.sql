SET FEEDBACK OFF           
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 350
SET TRIMSPOOL ON

break on A skip 1 dup


--spool mwtemp.csv 

select 'Customer,Container,Order,SKU,Description,Qty,Date,User' from DUAL;


select
sm.Customer_id,
sm.Container_id A,
sm.Order_id,
sm.SKU_id,
sku.description,
--sm.Line_id,
sm.Qty_shipped,
trunc(sysdate),
sm.user_id
from shipping_manifest sm,sku
where sm.client_id='MOUNTAIN'
and sm.order_id='&&2'
and sm.sku_id=sku.sku_id
and sku.client_id='MOUNTAIN'
/

select
'No. of Containers = '||count(distinct container_id)
from shipping_manifest
where client_id='MOUNTAIN'
and order_id='&&2'
/


--spool off

