SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9999
SET TRIMSPOOL ON

break on AA dup

compute sum label 'Total'  of QQ on AA


--spool WWWpicks.csv

select 'MOUNTAIN picks from &&2 to &&3' from DUAL;

select 'Order ID,SKU ID,Location,location Zone,Work Zone,Pick Qty' from DUAL;

select
it.reference_id,
it.sku_id,
it.from_loc_id,
ll.zone_1 AA,
ll.work_zone,
it.update_qty QQ
from inventory_transaction it,location ll
where it.client_id='MOUNTAIN'
and it.code='Pick'
and it.work_group='WWW'
and trunc(it.Dstamp) between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
--and trunc(it.Dstamp)>trunc(sysdate)-1
and it.from_loc_id not in ('CONTAINER','STG','UKPAC','UKPAR')
and it.from_loc_id=ll.location_id
order by
ll.zone_1,
ll.work_zone
/

--spool off
