SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
set linesize 120
set pagesize 0


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 

select 'SKU ID,Description,Colour,Size,Stock Qty' from Dual;


select 
i.sku_id ||','||
sku.description ||','||
sku.colour ||','||
sku.sku_size ||','||
sum(qty_on_hand-qty_allocated)
from inventory i,sku
where i.location_id not in ('PACKM','IN','IN2','IN3','QCHOLD','WP1','SUSPENSE')
and i.condition_id ='GOOD'
and i.lock_status = 'UnLocked'
and i.sku_id like 'VY%'
and i.sku_id=sku.sku_id
group by i.sku_id,sku.description,sku.colour,sku.sku_size
order by i.sku_id
/



 

