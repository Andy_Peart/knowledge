SET FEEDBACK OFF             
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 148
SET TRIMSPOOL ON


select 'Order,Customer,Name,Address 1,Address 2,Town,County,Postcode' from DUAL;

select
Order_id||','||
Customer_id||','||
Name||','||
Address1||','||
Address2||','||
Town||','||
County||','||
Postcode
from order_header
where client_id='DX'
and consignment='&&2'
order by order_id
/
