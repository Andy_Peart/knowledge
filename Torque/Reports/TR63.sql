SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 80

column A heading 'PA Ref' format a12
column B heading 'Code' format a12
column C heading 'Size' format a12
column D heading 'Qty' format 999999
column E heading 'Condition' format a12
column F heading 'SKU' format a18


break on A skip 1 dup on report

compute sum label 'Total' of D on A
compute sum label 'Total' of D on report


--set termout off
--spool TR063.csv

select 'TR63 - Receipts for '||to_char(sysdate-1,'DD/MM/YYYY') from Dual;

select 'PA Ref,Product Code,Size,Qty,Condition,SKU' from DUAL;


SELECT 
it.REFERENCE_ID A, 
SKU.USER_DEF_TYPE_3 B, 
SKU.SKU_SIZE C, 
sum(it.UPDATE_QTY) D, 
it.CONDITION_ID E, 
''''||it.SKU_ID||'''' F,
''
from pre_advice_header ph,inventory_transaction it,sku
where it.CLIENT_ID='TR'
and it.CODE LIKE 'Receipt' 
and trunc(it.Dstamp)=trunc(sysdate-1)
and it.CLIENT_ID=ph.CLIENT_ID
and it.CLIENT_ID=SKU.CLIENT_ID
and it.REFERENCE_ID=ph.PRE_ADVICE_ID
and it.SKU_ID=SKU.SKU_ID 
group by
it.REFERENCE_ID, 
SKU.USER_DEF_TYPE_3, 
SKU.SKU_SIZE, 
it.CONDITION_ID, 
it.SKU_ID
order by
it.REFERENCE_ID, 
it.SKU_ID, 
it.CONDITION_ID
/

--spool off
--set termout on
