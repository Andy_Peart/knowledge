SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

--Select 'SB08 C Mail Order Returns' from DUAL;
select 'Client ID,Description,Condition,QC Status,SKU,Qty on Hand,Allocated,Qty on hand less allocated' from DUAL;



column A format A12
column B format A40
column C format A10
column D format A10
column E format 999999
column F format 999999
column G format 999999
column H format A12

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



--ttitle  LEFT 'TML Retail - Open Orders Report ' RIGHT 'Printed ' report_date skip 2


break on report

compute sum label 'Total Units' of E F G on report


select
i.client_id H,
'"' || sku.description || '"' B,
i.condition_id C,
i.qc_status D,
i.sku_id A,
--sum(i.qty_on_hand) E,
--sum(i.qty_allocated) F,
--sum(i.qty_on_hand-i.qty_allocated) G
i.qty_on_hand E,
i.qty_allocated F,
i.qty_on_hand-i.qty_allocated G
from inventory i,sku
where i.client_id='SB'
and i.sku_id=sku.sku_id
--group by
--i.sku_id,
--sku.description,
--i.condition_id,
--i.qc_status
order by
i.sku_id
/
