SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Shipped Date,Store,Order id,Container,Units Shipped' from dual
union all
select "Date"||','||"Store"||','||"Order"||','||"Container"||','||"Units"
from
(
select "Date", "Store", "Order", "Container", "Units"
from
(
select trunc(it.dstamp) as "Date"
, it.work_group         as "Store"
, it.reference_id       as "Order"
, count(distinct it.container_id)   as "Container"
, sum(it.update_qty)                as "Units"
from inventory_transaction_archive it
left join order_header oh on oh.client_id = it.client_id and oh.order_id = it.reference_id
where it.client_id ='MOUNTAIN'
and it.dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.code = 'Shipment'
and oh.order_type = 'STAT'
group by
trunc(it.dstamp),
it.work_group,
it.reference_id
union all
select trunc(it.dstamp)
, it.work_group
, it.reference_id
, count(distinct it.container_id)
, sum(it.update_qty)
from inventory_transaction it
left join order_header oh on oh.client_id = it.client_id and oh.order_id = it.reference_id
where it.client_id ='MOUNTAIN'
and it.dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.code = 'Shipment'
and oh.order_type = 'STAT'
group by
trunc(it.dstamp),
it.work_group,
it.reference_id
)
order by 
"Date", "Store", "Order"
)
/
--spool off