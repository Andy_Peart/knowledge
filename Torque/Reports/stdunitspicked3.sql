SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON



--spool stpicks2.csv

select 'Date,Order Id,Qty Picked,Order Type,Customer Name,Customer_ID' from DUAL;
select trunc(it.Dstamp) D,
it.reference_id A,
sum(it.update_qty) B,
oh.order_type,
oh.name,
oh.customer_id
from inventory_transaction it
inner join order_header oh on it.reference_id = oh.order_id and it.client_id = oh.client_id
inner join location l on l.location_id = it.from_loc_id and l.site_id = it.site_id
where it.client_id='&&2'
and code='Pick'
and l.loc_type in ('Tag-FIFO','Tag-LIFO')
and Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
group by
trunc(it.Dstamp),
it.reference_id,
oh.order_type,
oh.name,
oh.customer_Id
/