SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 100
SET TRIMSPOOL ON


--spool denimstock.csv

select 'SKU,Description,Size,Colour,Qty' from DUAL;

select
it.sku_id||','||
sku.description||','||
sku.sku_size||','||
sku.colour||','||
sum(it.qty_on_hand)
from inventory it,sku
where it.client_id = 'DN'
and it.location_id not like 'SUSP%'
and it.location_id not like 'DESP%'
and it.sku_id=sku.sku_id
and sku.client_id = 'DN'
group by 
it.sku_id,
sku.description,
sku.sku_size,
sku.colour
order by 1
/

--spool off
