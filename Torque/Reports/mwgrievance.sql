SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','
SET WRAP OFF


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
set linesize 500
SET TRIMSPOOL ON

--break on report
--compute sum of Q1 Q2 on report

--spool mwgrievance.csv

Select 'Picks by User Id from &&2 to &&3' from DUAL;

select 'User Id,LPG,Txns,Units' from DUAL;

select
A||','||
LPG||','||
Q1||','||
Q2
from (select
A,
LPG,
sum(Q1) Q1,
sum(Q2) Q2
from (select
it.user_id A,
sku.user_def_type_8 LPG,
count(*) Q1,
sum(it.update_qty) Q2
from inventory_transaction it,sku
where it.client_id = 'MOUNTAIN'
and it.code = 'Pick'
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.elapsed_time>0
and it.sku_id=sku.sku_id
and sku.client_id = 'MOUNTAIN'
group by
it.user_id,
sku.user_def_type_8
union
select
it.user_id A,
sku.user_def_type_8 LPG,
count(*) Q1,
sum(it.update_qty) Q2
from inventory_transaction_archive it,sku
where it.client_id = 'MOUNTAIN'
and it.code = 'Pick'
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.elapsed_time>0
and it.sku_id=sku.sku_id
and sku.client_id = 'MOUNTAIN'
group by
it.user_id,
sku.user_def_type_8)
group by
A,
LPG
order by A,LPG)
/

--spool off

