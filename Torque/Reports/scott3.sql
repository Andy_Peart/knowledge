SET FEEDBACK OFF                 
set verify off
set wrap off
set colsep ','

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 300
set trimspool on


--spool scott3.csv



select 'Detail for '||to_char(sysdate-25, 'MON YYYY') from DUAL
union all
select 'Run Date / Time : '||to_char(sysdate,'DD/MM/YYYY HH24:MI:SS') from DUAL
union all
Select 'Site,Client,Units Receipted,Orders Received,Units Ordered,Units Allocated,Shortfall,Units Picked,% Accuracy,Units Shipped,% Fulfilled,Orders Shipped,Average Order Size,Oldest Order NOT Shipped,Units Adjusted DOWN,Units Adjusted UP,Nett Adjustments,Current Stock' from DUAL
union all
select * from (select
upper(BB)||','||
upper(AA)||','||
nvl(C1,0)||','||
nvl(D1,0)||','||
nvl(D2,0)||','||
nvl(C2,0)||','||
nvl(D2-C2,0)||','||
nvl(C2A,0)||','||
ltrim(to_char((C2A/C2)*100,'999.00'))||','||
nvl(C3,0)||','||
ltrim(to_char((C3/C2)*100,'999.00'))||','||
nvl(D3,0)||','||
ltrim(to_char(C3/D3,'999999'))||','||
E10||','||
nvl(C4,0)||','||
nvl(C5,0)||','||
nvl(C4+C5,0)||','||
nvl(Q6,0)
from (select
distinct
nvl(cl.name,cl.client_id) AA,
it.site_id BB
from inventory_transaction it,client cl
where to_char(it.Dstamp, 'MM YYYY')=to_char(sysdate-25, 'MM YYYY')
and it.site_id is not null
and it.client_id=cl.client_id),(select
nvl(cl.name,cl.client_id) A1,
it.site_id B1,
sum(it.update_qty) C1
from inventory_transaction it,client cl
where it.code like 'Receipt%'
and to_char(it.Dstamp, 'MM YYYY')=to_char(sysdate-25, 'MM YYYY')
and it.client_id=cl.client_id
group by
nvl(cl.name,cl.client_id),it.site_id),(select
nvl(cl.name,cl.client_id) A2,
oh.from_site_id B2,
count(distinct oh.order_id) D1,
sum(DECODE(substr(oh.order_id,1,3),'MOR',(DECODE(substr(ol.user_def_note_1,1,8),'Original',substr(ol.user_def_note_1,31,4),ol.qty_ordered)),ol.qty_ordered)) D2,
--sum(nvl(ol.qty_tasked,0)+(nvl(ol.qty_picked,0))) C2,
sum(nvl(ol.qty_picked,0)) C2A,
sum(nvl(ol.qty_shipped,0)) C3
from order_header oh,client cl,order_line ol
where status not in ('Cancelled')
and to_char(oh.creation_date, 'MM YYYY')=to_char(sysdate-25, 'MM YYYY')
and oh.client_id=cl.client_id
and oh.order_id=ol.order_id
and ol.client_id=oh.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),(select
nvl(cl.name,cl.client_id) A2A,
oh.from_site_id B2A,
sum(DECODE(it.code,'Deallocate',(it.update_qty*-1),it.update_qty)) C2
from order_header oh,client cl,inventory_transaction it
where oh.status not in ('Cancelled')
and to_char(oh.creation_date, 'MM YYYY')=to_char(sysdate-25, 'MM YYYY')
and oh.client_id=cl.client_id
and it.client_id=oh.client_id
and it.reference_id=oh.order_id
and it.code in ('Allocate','Deallocate')
and it.user_id<>'Mvtcdae'
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),(select
nvl(cl.name,cl.client_id) A3,
oh.from_site_id B3,
count(distinct oh.order_id) D3
from order_header oh,client cl
where status='Shipped'
and to_char(oh.creation_date, 'MM YYYY')=to_char(sysdate-25, 'MM YYYY')
and oh.client_id=cl.client_id
group by
nvl(cl.name,cl.client_id),
oh.from_site_id),(select
nvl(cl.name,cl.client_id) A4,
it.site_id B4,
sum(DECODE(it.client_id,'TR',(DECODE(substr(reason_id,1,1),'T',0,'H',0,it.update_qty)),it.update_qty)) C4
from inventory_transaction it,client cl
where it.code='Adjustment'
and to_char(it.Dstamp, 'MM YYYY')=to_char(sysdate-25, 'MM YYYY')
and it.update_qty<0
and it.client_id=cl.client_id
group by
nvl(cl.name,cl.client_id),it.site_id),(select
nvl(cl.name,cl.client_id) A5,
it.site_id B5,
sum(DECODE(it.client_id,'TR',(DECODE(substr(reason_id,1,1),'T',0,'H',0,it.update_qty)),it.update_qty)) C5
from inventory_transaction it,client cl
where it.code='Adjustment'
and to_char(it.Dstamp, 'MM YYYY')=to_char(sysdate-25, 'MM YYYY')
and it.update_qty>0
and it.client_id=cl.client_id
group by
nvl(cl.name,cl.client_id),it.site_id),(select
nvl(cl.name,cl.client_id) A6,
iv.site_id B6,
sum(iv.qty_on_hand) Q6
from inventory iv,client cl
where iv.client_id=cl.client_id
group by
nvl(cl.name,cl.client_id),iv.site_id),(select
nvl(cl.name,cl.client_id) A10,
min(trunc(creation_date)) E10
from order_header oh,client cl
where status not in ('Cancelled','Shipped')
and creation_date<sysdate-2
and oh.client_id=cl.client_id
group by
nvl(cl.name,cl.client_id))
where AA=A1 (+)
and BB=B1 (+)
and AA=A2 (+)
and AA=A2A (+)
and BB=B2 (+)
and AA=A3 (+)
and BB=B3 (+)
and AA=A4 (+)
and BB=B4 (+)
and AA=A5 (+)
and BB=B5 (+)
and AA=A6 (+)
and BB=B6 (+)
and AA=A10 (+)
order by upper(BB),upper(AA))
/

--spool off




