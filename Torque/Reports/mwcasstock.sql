SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 1000
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Sku id,Tag id,EAN,Description,Sku Size,Colour,Location id,Location Zone,Work Zone,Units,Units Allocated,Product Group/Dept,User Def Type 1,User Def Type 5,UDT6/Type,User Def Note 1,Receipt Id' from dual
union all
select sku_id||','||tag_id||','||ean||',"'||description||'",'||sku_size||','||colour||','||location_id||','||location_zone||','||work_zone||','||units||','||unitsAlloc||','||"PRODUCT GROUP/DEPT"||','||user_def_type_1||','||user_def_type_5
||','||"UDT6/TYPE"||',"'||user_def_note_1||'",'||receipt_id
from
(
select i.sku_id
, i.tag_id
, s.ean
, s.description
, s.sku_size
, s.colour
, i.location_id
, i.zone_1 location_zone
, l.work_zone
, sum(i.qty_on_hand) units
, sum(i.qty_allocated) unitsAlloc
, s.product_group "PRODUCT GROUP/DEPT"
, s.user_def_type_1
, s.user_def_type_5
, s.user_def_type_6 "UDT6/TYPE"
, s.user_def_note_1
, i.receipt_id
from INVENTORY i
inner join SKU s on s.sku_id = i.sku_id and s.client_id = i.client_id
inner join LOCATION l on l.location_id = i.location_id and l.site_id = i.site_id
where i.client_id = 'MOUNTAIN'
and
(
l.work_zone like 'CASLEVEL%'
or
l.work_zone like 'GAS%'
or
l.location_id = 'Z005'
)
group by i.sku_id
, i.tag_id
, s.ean
, s.description
, s.sku_size
, s.colour
, i.location_id
, i.zone_1
, l.work_zone
, s.product_group
, s.user_def_type_1
, s.user_def_type_5
, s.user_def_type_6
, s.user_def_note_1
, i.receipt_id
order by i.sku_id
, i.tag_id
)
/
--spool off