SET FEEDBACK OFF
SET VERIFY OFF
SET TAB OFF
SET COLSEP ' '

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Column Headings and Formats */
COLUMN A heading "Product|Group" FORMAT a12
COLUMN B heading "Unit|Qty" FORMAT 9999999
COLUMN C heading "Locs|Qty" FORMAT 9999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


SET LINESIZE 60
SET WRAP OFF
SET PAGES 500
SET NEWPAGE 0

/* Top Title */
TTITLE LEFT 'Product Group Totals for client &&2 as at ' report_date SKIP 2

/* Set up page and line */

break on report

compute sum label 'Total_________' of B C on report


with t1 as (
select
case  when sku.product_group  is null then '(No Group)____________'
      else sku.product_group||'________________'
end A,
sum(i.qty_on_hand) B,
count(*) C
from inventory i
inner join sku on sku.sku_id = i.sku_id and sku.client_id = i.client_id
inner join location l on l.site_id = i.site_id and i.location_id = l.location_id
where i.client_id='PR'
and l.work_zone in ('BDB1','BDH1','BDC1')
group by
sku.product_group
order by
sku.product_group
)
select A,sum(B) B,sum(C) C
from T1
group by A
order by 1
/
