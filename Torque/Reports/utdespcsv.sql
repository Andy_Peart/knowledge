SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep '|'


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 300
SET TRIMSPOOL ON


with t1 as
(
SELECT   
         oh.ORDER_ID,
         upper(oh.CONTACT) as CONTACT,
      	 upper(oh.NAME) as NAME,
         initcap(oh.ADDRESS1) as ADDRESS1,
         initcap(oh.ADDRESS2) as ADDRESS2,
         upper(oh.TOWN) as TOWN,
         initcap(oh.COUNTY) as COUNTY,
         upper(oh.POSTCODE) as POSTCODE,
         upper(replace(c.TANDATA_ID,'_',' ')) as COUNTRY,
         oh.CUSTOMER_ID,
         oh.Purchase_order,
         oh.dispatch_Method
FROM   ORDER_HEADER oh,  COUNTRY c
WHERE (nvl(oh.COUNTRY,'GBR')=c.ISO3_ID)
AND oh.CLIENT_ID= 'UT'
AND oh.ORDER_ID= '&&2'
)
select 
'Order ID: ' || t1.order_id ||
chr(10) || 
'Customer Code: ' || t1.customer_id ||
chr(10) || 
'Customer Reference: ' || t1.purchase_order ||
chr(10) || 
'Delivery Method: ' || t1.dispatch_Method ||
chr(10) || 
case 
when t1.contact is null then ''
else 'Contact: ' || t1.contact
end 
||
chr(10) ||
case
when t1.name is null then ''
else 'Name: ' || t1.name
end 
||
chr(10) ||  'Address: ' ||
chr(10) ||  t1.address1 ||
chr(10) ||  t1.address2 ||
chr(10) ||  t1.town ||
chr(10) ||  t1.county ||
chr(10) ||  t1.postcode ||
chr(10)
from T1
union all
select 'Carton, Style, SKU, Description, Size, Colour, Quantity' from dual
union all
select container_id || ',' || style  || ',' || sku_id  || ',' || description  || ',' ||  sku_size  || ',' || colour  || ',' || qty
from
(
SELECT   
sm.container_id,
sk.PRODUCT_GROUP as STYLE,
ol.SKU_ID,
(Initcap(sk.USER_DEF_TYPE_1)||' '||Initcap(sk.DESCRIPTION)) as Description,
sk.sku_size,
Initcap(sk.colour) as Colour,
sum(sm.QTY_PICKED) as QTY
FROM   ORDER_LINE ol, ORDER_HEADER oh, SHIPPING_MANIFEST sm, SKU sk, COUNTRY c
WHERE (ol.ORDER_ID=oh.ORDER_ID AND ol.CLIENT_ID=oh.CLIENT_ID)
AND (ol.CLIENT_ID=sm.CLIENT_ID AND ol.ORDER_ID=sm.ORDER_ID AND ol.LINE_ID=sm.LINE_ID)
AND (ol.CLIENT_ID=sk.CLIENT_ID AND ol.SKU_ID=sk.SKU_ID)
AND (nvl(oh.COUNTRY,'GBR')=c.ISO3_ID)
AND oh.CLIENT_ID= 'UT'
AND oh.ORDER_ID= '&&2'
GROUP BY 
ol.SKU_ID,
sm.container_id,
(Initcap(sk.USER_DEF_TYPE_1)||' '||Initcap(sk.DESCRIPTION)),
sk.PRODUCT_GROUP,
sk.colour,
sk.sku_size
ORDER BY sm.container_id,
sk.PRODUCT_GROUP,
ol.SKU_ID
)
/