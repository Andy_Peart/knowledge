SET FEEDBACK OFF                 
set verify off
set wrap off
set colsep ','

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 300
set trimspool on


--spool stdfulfilment.csv


Select 'Customer,Name,Contact,Qty Ordered,Qty Allocated,Qty Shipped,% Allocated,% Shipped' from DUAL
union all
select * from (select
CUS||','||
NAM||','||
NAM2||','||
nvl(Q1,0)||','||
nvl(Q1-Q2,0)||','||
nvl(Q3,0)||','||
to_char(((Q1-Q2)/Q1)*100,'999.000')||','||
to_char(DECODE(Q1-Q2,0,0,Q3/(Q1-Q2)*100),'999.000')||','||
' '
from (select
AA,
BB,
CUS,
NAM,
NAM2,
sum(QQ1) Q1,
sum(nvl(QQQ,0)) Q2,
sum(QQ2) Q3
from (select
oh.client_id AA,
DECODE(oh.client_id,'TR','ALL ORDERS','T','ALL ORDERS',nvl(order_type,' ')) BB,
oh.order_id OID,
oh.customer_id CUS,
oh.name NAM,
oh.contact NAM2,
sum(qty_ordered) QQ1,
sum(qty_shipped) QQ2
from order_header oh,order_line ol
where oh.status='Shipped'
and oh.order_type like '&&5%'
and oh.client_id like '&&2'
and oh.shipped_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and oh.client_id=ol.client_id
and oh.order_id=ol.order_id
group by
oh.client_id,
DECODE(oh.client_id,'TR','ALL ORDERS','T','ALL ORDERS',nvl(order_type,' ')),
oh.order_id,
oh.customer_id,
oh.name,
oh.contact),(select
client_id CID,
task_id RID,
sum(qty_ordered-qty_tasked) QQQ
from generation_shortage
where client_id like '&&2'
group by client_id,task_id)
where AA=CID (+)
and OID=RID (+)
group by
AA,BB,CUS,NAM,NAM2)
order by 1)
union all
select * from (select
'TOTALS'||','||
' '||','||
' '||','||
nvl(Q1,0)||','||
nvl(Q1-Q2,0)||','||
nvl(Q3,0)||','||
to_char(((Q1-Q2)/Q1)*100,'999.000')||','||
to_char(DECODE(Q1-Q2,0,0,Q3/(Q1-Q2)*100),'999.000')||','||
' '
from (select
AA,
sum(QQ1) Q1,
sum(nvl(QQQ,0)) Q2,
sum(QQ2) Q3
from (select
oh.client_id AA,
oh.order_id OID,
oh.customer_id CUS,
oh.name NAM,
oh.contact NAM2,
sum(qty_ordered) QQ1,
sum(qty_shipped) QQ2
from order_header oh,order_line ol
where oh.status='Shipped'
and oh.order_type like '&&5%'
and oh.client_id like '&&2'
and oh.shipped_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and oh.client_id=ol.client_id
and oh.order_id=ol.order_id
group by
oh.client_id,
DECODE(oh.client_id,'TR','ALL ORDERS','T','ALL ORDERS',nvl(order_type,' ')),
oh.order_id,
oh.customer_id,
oh.name,
oh.contact),(select
client_id CID,
task_id RID,
sum(qty_ordered-qty_tasked) QQQ
from generation_shortage
where client_id like '&&2'
group by client_id,task_id)
where AA=CID (+)
and OID=RID (+)
group by
AA)
order by 1)
/




--spool off



