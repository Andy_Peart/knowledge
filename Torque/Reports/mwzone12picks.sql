SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON


select 'Web Picks from Zone 12 from &&2 to &&3' from DUAL;
select '' from DUAL;

select
'Zone 12 picks : '||QQ||
'  Total Picks : '||QQQ||
'  Percentage : '||to_char(QQ*100/QQQ,'90.00')
from (select
ll.work_zone ZZ,
sum(it.update_qty) QQ
from inventory_transaction it,location ll
where it.client_id='MOUNTAIN'
and it.code='Pick'
and it.work_group='WWW'
and trunc(it.Dstamp) between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.from_loc_id not in ('CONTAINER','STG','UKPAC','UKPAR')
and it.from_loc_id=ll.location_id
group by
ll.work_zone),(select
sum(it.update_qty) QQQ
from inventory_transaction it
where it.client_id='MOUNTAIN'
and it.code='Pick'
and it.work_group='WWW'
and trunc(it.Dstamp) between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.from_loc_id not in ('CONTAINER','STG','UKPAC','UKPAR'))
where ZZ='12'
/

