SET PAGESIZE 62    
SET NEWPAGE 0
SET LINESIZE 132
SET verify off

ttitle left "Mountain Warehouse - Stock Summary by Location" skip 2

select sku.sku_id SKU, inv.location_id Location, inv.qty_on_hand Qty
from sku, inventory inv
where sku.client_id = 'MOUNTAIN'
and sku.sku_id = inv.sku_id
and inv.location_id not like 'MARSHAL'
and inv.zone_1 not like 'Z3OUT'
/*group by sku.sku_id*/
order by 2
/
