SET FEEDBACK OFF                 
set pagesize 999
set linesize 160
set verify off

clear columns
clear breaks
clear computes

column A heading 'Order ID ' format a16
column A1 heading 'SKU ID ' format A18
column B heading 'Update Qty' format 999999
column C heading 'Original Qty' format 999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'Unit Allocations Overridden on Orders from &&2 to &&3 - for client ID MOUNTAIN as at ' report_date skip 2

break on A skip 1

compute sum label 'Totals' of B on report

select
it.reference_id A,
it.sku_id A1,
it.update_qty as B,
original_qty C
from inventory_transaction it
where it.client_id='MOUNTAIN'
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.code='Pick'
and it.reference_id like '&&4%'
and update_qty<>original_qty
order by
A,A1
/
