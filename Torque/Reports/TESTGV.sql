SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 480
SET TRIMSPOOL ON

column X heading 'Qty' format 9999999
column Y heading 'Qty 2' format 9999999
column Z heading 'QTY' format 9999999
column M heading 'QTY' format 9999999
column N heading 'QTY' format 9999999
column P heading 'QTY' format 9999999
column Q heading 'Qty' format 9999999
column B heading 'Qty 2' format 9999999

select 'Intake UC between &&2 and &&3' from DUAL;
select 'Recvd Qty Sku C,Recvd Qty Sku U,RECRV Qty Sku C,RECRV Qty Sku U, Total Recvd minus RECRV' from DUAL;

break on report

select X,Y,X+Y Z 
 from (select nvl(sum (i.update_qty),0) X
from inventory_transaction i, sku s 
where i.client_id = 'GV'
and s.sku_id like ('G%')
and s.sku_id = i.sku_id (+)
and i.to_loc_id ='ELITE'
and i.code ='Receipt'
and i.dstamp between to_date('01-SEP-09', 'DD/MM/YY') and to_date('11-DEC-09', 'DD/MM/YY')+1),
(select nvl(sum (i.update_qty),0) Y
from inventory_transaction i, sku s
where i.client_id = 'GV'
and s.sku_id like ('G%')
and s.sku_id = i.sku_id (+)
and i.from_loc_id ='ELITE'
and i.code ='Adjustment'
and i.reason_id ='RECRV'
and i.dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1)
/
select M, N, P, Q, M+N+P+Q B
from
(select
nvl(sum (i.update_qty),0) M
from inventory_transaction i, sku s 
where i.client_id = 'GV'
and s.sku_id like ('C%')
and s.sku_id = i.sku_id (+)
and i.to_loc_id ='ELITE'
and i.code ='Receipt'
and i.dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1),
(select
nvl(sum (i.update_qty),0) N
from inventory_transaction i, sku s 
where i.client_id = 'GV'
and s.sku_id like ('U%')
and s.sku_id = i.sku_id (+)
and i.to_loc_id ='ELITE'
and i.code ='Receipt'
and i.dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1),
(select
nvl(sum (i.update_qty),0) P
from inventory_transaction i, sku s
where i.client_id = 'GV'
and s.sku_id like ('C%')
and s.sku_id = i.sku_id (+)
and i.from_loc_id ='ELITE'
and i.code ='Adjustment'
and i.reason_id ='RECRV'
and i.dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1),
(select
nvl(sum (i.update_qty),0) Q
from inventory_transaction i, sku s
where i.client_id = 'GV'
and s.sku_id like ('U%')
and s.sku_id = i.sku_id (+)
and i.from_loc_id ='ELITE'
and i.code ='Adjustment'
and i.reason_id ='RECRV'
and i.dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1)
/