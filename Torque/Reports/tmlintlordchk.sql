/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :     intlordchck.sql                         		      */
/*                                                                            */
/*     DESCRIPTION:                                                           */
/*     CSV container id and SKU for checking TML Internationl orders          */
/*                                                                            */
/*   DATE       BY   DESCRIPTION						                      */
/*   ========== ==== ===========					                          */
/*   03/04/2014 SS   INTL ORDERS CHECKS			  							  */
/******************************************************************************/
SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','
SET WRAP OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

/* SPOOL TML_INTL_ORDERS_CHECK_.CSV */

select container_id, sku_id
from shipping_manifest
where site_id = 'WFD'
and client_id in ('T', 'TR')
and consignment = '&2'
order by container_id
/

/* SPOOL OFF */