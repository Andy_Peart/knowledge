set pagesize 9999
set linesize 2000
set trimspool on
set colsep ','
set feedback off


select
key,
tag_id, 
client_id, 
sku_id, 
update_qty,
Dstamp
FROM INVENTORY_TRANSACTION
WHERE Client_Id in ('T','TR')
and reason_id='SUSP'
and SITE_ID = 'BRADFORD'
and Code = 'Adjustment'
and trunc(DSTAMP)=trunc(sysdate)
/

