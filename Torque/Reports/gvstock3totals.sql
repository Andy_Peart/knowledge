SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 

column A heading 'Category' format A30
column B heading 'Qty' format 9999999

SET PAGESIZE 0
--select to_char(SYSDATE, 'DD/MM/YY    HH:MI:SS') Stock_Report from DUAL;

select '' from DUAL;

select 'GIVe STOCK LEVELS' from DUAL;

select '' from DUAL;
select '' from DUAL;

select
'Merchandise Qty on Hand' A,
sum (i.qty_on_hand) B
from inventory i, sku s, location l
where i.client_id = 'GV'
and s.sku_id like('G%')
and s.sku_id = i.sku_id
and i.location_id = l.location_id
and l.location_id <>'SUSPENSE'
/
select '' from DUAL;
/
select 
'Consumable Qty on Hand' A,
sum (i.qty_on_hand) B
from inventory i, sku s, location l
where i.client_id = 'GV'
and s.sku_id like('C%')
and s.sku_id = i.sku_id
and i.location_id = l.location_id
and l.location_id <>'SUSPENSE'
/
select '' from DUAL;
/
select
'Uniform Qty on Hand' A,
sum (i.qty_on_hand) B
from inventory i, sku s, location l
where i.client_id = 'GV'
and s.sku_id like('U%')
and s.sku_id = i.sku_id
and i.location_id = l.location_id
and l.location_id <>'SUSPENSE'
/
