SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500

COLUMN A heading "Picker" FORMAT a14
COLUMN B heading "Date" FORMAT a10
COLUMN BB heading "Time" FORMAT a10
COLUMN C heading "Bin" FORMAT A8
COLUMN D heading "SKU" FORMAT A30
COLUMN E heading "Tag" FORMAT A10
COLUMN F heading "OQ" FORMAT 99999
COLUMN G heading "Checker" FORMAT a14
COLUMN H heading "Date" FORMAT a10
COLUMN HH heading "Time" FORMAT a10
COLUMN J heading "UQ" FORMAT 99999
COLUMN K heading "DQ" FORMAT 99999
COLUMN L heading "Colour" FORMAT a20
COLUMN M heading "Description" FORMAT a60



--select 'Multiple SKU Extract - for client ID TR as at ' 
--        ||  to_char(sysdate,'dd/mm/yy  hh24:mi:ss') from dual;

select 'Picker,Date Picked,Time Picked,Bin,SKU,Tag,Qty zeroed,Stock Checker,Date Checked,Time Checked,Check Result,Differance,Colour,Description,Work Zone'
from dual;


select
	UU A,
	to_char(DD,'DD-Mon-YY') B,
	to_char(DD,'HH24:MI:SS') BB,
	LL C,
	''''||SS||'''' D,
	TT E,
	it.original_qty F,
	it.user_id G,
	to_char(it.Dstamp,'DD-Mon-YY') H,
	to_char(it.Dstamp,'HH24:MI:SS') HH,
	it.update_qty J,
	it.update_qty+it.original_qty K,
	sku.colour L,
	sku.description M,
	l.work_zone
from inventory_transaction it,sku,location l,
(
select
    itl.User_id UU,
    itl.dstamp DD,
    itl.from_loc_id LL,
    itl.sku_id SS,
    itl.tag_id TT
from inventory_transaction itl
where itl.client_id='&&3'
    and itl.code= 'Pick'
    and nvl(itl.update_qty,0)<nvl(original_qty,0)
    and itl.dstamp = (
        select max(dstamp) 
        from inventory_transaction
        where client_id='&&3'
        and code= 'Pick'
        and nvl(update_qty,0)<nvl(original_qty,0)
        and tag_id = itl.tag_id
        and sku_id = itl.sku_id
        and from_loc_id = itl.from_loc_id
        )
)
where it.client_id='&&3'
	and it.code= 'Stock Check'
	and it.tag_id=TT
	and it.sku_id=SS
	and it.from_loc_id = l.location_id
	and it.site_id = l.site_id
	and trunc(it.Dstamp)=to_date('&&2','dd-mon-yyyy')
	and it.Dstamp>DD
	and it.sku_id=sku.sku_id
	and sku.client_id='&&3'
order by DD desc
/
