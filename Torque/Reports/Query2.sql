SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240



--select 'Multiple SKU Extract - for client ID TR as at ' 
--        ||  to_char(sysdate,'dd/mm/yy  hh24:mi:ss') from dual;

select 'SKU,Original Qty,Update Qty,From Location,To Location,Txn Date,Txn Time,Code,User ID,Description,Size,Colour'
from dual;


select
'''' ||
iv.sku_id ||''','||
nvl(iv.original_qty,0) ||','||
nvl(iv.update_qty,0) ||','||
iv.from_loc_id ||','||
iv.to_loc_id ||','||
to_char(Dstamp, 'DD/MM/YYYY') ||','||
to_char(Dstamp, 'HH:MI:SS') ||','||
iv.code ||','||
iv.user_id ||','||
sku.description ||','||
sku.sku_size ||','||
sku.colour
from inventory_transaction iv,sku
where iv.client_id='TR'
and iv.sku_id in ('&&2','&&3','&&4','&&5','&&6','&&7','&&8','&&9')
and sku.client_id='TR'
and iv.sku_id=sku.sku_id
order by iv.sku_id
/
