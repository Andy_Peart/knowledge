SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
set linesize 130

--ttitle 'Mountain Warehouse Overpicks for Period &&2 to &&3' skip 2

column A heading 'Order ID' format a10
column B heading 'Creation Date' format A12
column C heading 'Order Qty' format 99999999
column D heading 'Tasked Qty' format 99999999
column E heading 'Customer' format a40
column F heading 'Country' format a16
column G heading 'SKU' format a16
column H heading 'Description' format a40
column J heading 'Size' format a10




select 'Order ID,Creation Date,Unavailable Item Qty,Unavailable Item Tasked Qty,Catalogue Code,SKU ID,Description,Size,,' from DUAL;


select
oh.order_id A,
trunc(oh.creation_date) B,
--to_char(oh.creation_date,'DD/MM/YY') B,
ol.qty_ordered C,
nvl(ol.qty_tasked,0) D,
S2.user_def_type_4 F,
--oh.customer_id||' - '||oh.address1 E,
--nvl(country.tandata_id,oh.country) F,
''''||ol.sku_id||'''' G,
S1.description H,
S1.sku_size J
from order_header oh,order_line ol,country,sku S1, sku S2
where oh.client_id='T'
and oh.status not in ('Cancelled','Shipped')
and oh.consignment='SHORT'
and substr(oh.country,1,2)<>'GB'
and ol.client_id='T'
and oh.order_id=ol.order_id
and ol.qty_ordered<>nvl(ol.qty_tasked,0)
and oh.country=country.iso3_id (+)
and S1.client_id='T'
and ol.sku_id=S1.sku_id
and S2.client_id='TR'
and ol.sku_id=S2.sku_id
order by A,G
/
