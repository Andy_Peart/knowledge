SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 200
set trimspool on

column A heading 'Order_id' format a12
column B heading 'Status' format a12
column C heading 'Uploaded' format a5
column D heading 'Creation_Date' format a18
column F heading 'Shipped_Qty' format 5


--spool bkguess.csv

select 'TML - Orders Shipped but not Uploaded to Pro Logic from 60 days ago' from Dual;

select 'Client_id,Order_ID,Status,Uploaded,Creation_date,Shipped_Qty' from DUAL;

select 
oh.client_id||','||
oh.order_id||','|| 
oh.status||','||
oh.uploaded||','||
trunc(oh.creation_date)||','|| 
nvl(sum(ol.qty_shipped),0)
from order_header oh, order_line ol
where oh.client_id in ('TR','T')
and oh.creation_date>sysdate-60
and oh.uploaded<>'Y'
and oh.status = 'Shipped'
and oh.order_id = ol.order_id
group by oh.client_id,oh.order_id, oh.status, oh.uploaded, oh.creation_date
order by 
oh.client_id,
oh.creation_date
/
select 'End of Report' from DUAL;


--spool off
