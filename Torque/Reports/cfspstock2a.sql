SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON

break on report

compute sum label 'Total' of Q1 Q2 Q3 Q4 QQ1 QQ2 QQ3 QQ4 QQ41 QQ42 QQ43 QQ5 QQ6 QQ7 QQ8 QQ4 QQ81 QQ82 QQ83 QQQ on report

--spool CFSPstock2a.csv


Select 'PROMINENT CFSP WAREHOUSE RECONCILIATION (UNITS)' from DUAL;

select 'Style,Red Prairie PR,Red Prairie T,Red Prairie TR,Red Prairie Total,T.PROMIN,TR.PROMIN,T.WH1,TR.WH2,T.OTHER,TR.OTHER,PR.ALL,T.PROMIN,TR.PROMIN,T.WH1,TR.WH2,T.OTHER,TR.OTHER,PR.ALL,ADJ BAL' from DUAL;

select
substr(S1,1,24),
Q1,
Q2,
Q3,
Q4,
nvl(QQ1,0) QQ1,
nvl(QQ2,0) QQ2,
nvl(QQ3,0) QQ3,
nvl(QQ4,0) QQ4,
nvl(QQ41,0) QQ41,
nvl(QQ42,0) QQ42,
nvl(QQ43,0) QQ43,
nvl(QQ5,0) QQ5,
nvl(QQ6,0) QQ6,
nvl(QQ7,0) QQ7,
nvl(QQ8,0) QQ8,
nvl(QQ81,0) QQ81,
nvl(QQ82,0) QQ82,
nvl(QQ83,0) QQ83,
Q4-(nvl(QQ1,0)+nvl(QQ2,0)+nvl(QQ3,0)+nvl(QQ4,0)+nvl(QQ41,0)+nvl(QQ42,0)+nvl(QQ43,0)+nvl(QQ5,0)+nvl(QQ6,0)+nvl(QQ7,0)+nvl(QQ8,0)+nvl(QQ81,0)+nvl(QQ82,0)+nvl(QQ83,0)) QQQ
from 
(select
nvl(S1,'No Style') S1,
Sum(Q1) Q1,
Sum(Q2) Q2,
Sum(Q3) Q3,
Sum(Q1+Q2+Q3) Q4
from (select
sku.user_def_type_5 S1,
'PR' C1,
sum(i.qty_on_hand) Q1,
0 Q2,
0 Q3
from inventory i,sku
where i.client_id='PR'
and i.sku_id=sku.sku_id
and sku.client_id='PR'
and sku.user_def_type_2='TML'
--and sku.sku_id like '5%'
--and sku.EAN like '5%'
and i.location_id<>'SUSPENSE'
group by
sku.user_def_type_5
union
select
sku.user_def_type_2||+'-'||+sku.user_def_type_1||+'-'||+sku.user_def_type_4 S1,
'T' C1,
0 Q1,
sum(i.qty_on_hand) Q2,
0 Q3
from inventory i,sku
where i.client_id='T'
and i.sku_id=sku.sku_id
and sku.client_id='T'
and i.location_id<>'SUSPENSE'
group by
sku.user_def_type_2||+'-'||+sku.user_def_type_1||+'-'||+sku.user_def_type_4
union
select
sku.user_def_type_2||+'-'||+sku.user_def_type_1||+'-'||+sku.user_def_type_4 S1,
'TR' C1,
0 Q1,
0 Q2,
sum(i.qty_on_hand) Q3
from inventory i,sku
where i.client_id='TR'
and i.sku_id=sku.sku_id
and sku.client_id='TR'
and i.location_id<>'SUSPENSE'
group by
sku.user_def_type_2||+'-'||+sku.user_def_type_1||+'-'||+sku.user_def_type_4)
group by S1)
,(select
sku.user_def_type_2||+'-'||+sku.user_def_type_1||+'-'||+sku.user_def_type_4 X1,
nvl(sum(it.update_qty),0) QQ1
from inventory_transaction it,pre_advice_header ph,sku
where it.client_id='T'
and it.code='Receipt'
and it.reference_id=ph.pre_advice_id
and ph.client_id='T'
and ph.status='In Progress'
and ph.supplier_id='PROMIN'
and it.sku_id=sku.sku_id
and sku.client_id='T'
group by
sku.user_def_type_2||+'-'||+sku.user_def_type_1||+'-'||+sku.user_def_type_4)
,(select
sku.user_def_type_2||+'-'||+sku.user_def_type_1||+'-'||+sku.user_def_type_4 X2,
nvl(sum(it.update_qty),0) QQ2
from inventory_transaction it,pre_advice_header ph,sku
where it.client_id='TR'
and it.code='Receipt'
and it.reference_id=ph.pre_advice_id
and ph.client_id='TR'
and ph.status='In Progress'
and ph.supplier_id='PROMIN'
and it.sku_id=sku.sku_id
and sku.client_id='TR'
group by
sku.user_def_type_2||+'-'||+sku.user_def_type_1||+'-'||+sku.user_def_type_4)
,(select
sku.user_def_type_2||+'-'||+sku.user_def_type_1||+'-'||+sku.user_def_type_4 X3,
nvl(sum(it.update_qty),0) QQ3
from inventory_transaction it,pre_advice_header ph,sku
where it.client_id='T'
and it.code='Receipt'
and it.reference_id=ph.pre_advice_id
and ph.client_id='T'
and ph.status='In Progress'
and ph.supplier_id='WH1'
and it.sku_id=sku.sku_id
and sku.client_id='T'
group by
sku.user_def_type_2||+'-'||+sku.user_def_type_1||+'-'||+sku.user_def_type_4)
,(select
sku.user_def_type_2||+'-'||+sku.user_def_type_1||+'-'||+sku.user_def_type_4 X4,
nvl(sum(it.update_qty),0) QQ4
from inventory_transaction it,pre_advice_header ph,sku
where it.client_id='TR'
and it.code='Receipt'
and it.reference_id=ph.pre_advice_id
and ph.client_id='TR'
and ph.status='In Progress'
and ph.supplier_id='WH2'
and it.sku_id=sku.sku_id
and sku.client_id='TR'
group by
sku.user_def_type_2||+'-'||+sku.user_def_type_1||+'-'||+sku.user_def_type_4)
,(select
sku.user_def_type_2||+'-'||+sku.user_def_type_1||+'-'||+sku.user_def_type_4 X41,
nvl(sum(it.update_qty),0) QQ41
from inventory_transaction it,pre_advice_header ph,sku
where it.client_id='T'
and it.code='Receipt'
and it.reference_id=ph.pre_advice_id
and ph.client_id='T'
and ph.status='In Progress'
and ph.supplier_id not in ('PROMIN','WH1')
and it.sku_id=sku.sku_id
and sku.client_id='T'
group by
sku.user_def_type_2||+'-'||+sku.user_def_type_1||+'-'||+sku.user_def_type_4)
,(select
sku.user_def_type_2||+'-'||+sku.user_def_type_1||+'-'||+sku.user_def_type_4 X42,
nvl(sum(it.update_qty),0) QQ42
from inventory_transaction it,pre_advice_header ph,sku
where it.client_id='TR'
and it.code='Receipt'
and it.reference_id=ph.pre_advice_id
and ph.client_id='TR'
and ph.status='In Progress'
and ph.supplier_id not in ('PROMIN','WH2')
and it.sku_id=sku.sku_id
and sku.client_id='TR'
group by
sku.user_def_type_2||+'-'||+sku.user_def_type_1||+'-'||+sku.user_def_type_4)
,(select
sku.user_def_type_5 X43,
nvl(sum(it.update_qty),0) QQ43
from inventory_transaction it,pre_advice_header ph,sku
where it.client_id='PR'
and it.code='Receipt'
and it.reference_id=ph.pre_advice_id
and ph.client_id='PR'
and ph.status='In Progress'
and it.sku_id=sku.sku_id
and sku.client_id='PR'
and sku.user_def_type_2='TML'
group by
sku.user_def_type_5)
,(select
sku.user_def_type_2||+'-'||+sku.user_def_type_1||+'-'||+sku.user_def_type_4 X5,
nvl(sum(it.update_qty),0) QQ5
from inventory_transaction it,pre_advice_header ph,sku
where it.client_id='T'
and it.code='Receipt Reverse'
and it.reference_id=ph.pre_advice_id
and ph.client_id='T'
and ph.status='In Progress'
and ph.supplier_id='PROMIN'
and it.sku_id=sku.sku_id
and sku.client_id='T'
group by
sku.user_def_type_2||+'-'||+sku.user_def_type_1||+'-'||+sku.user_def_type_4)
,(select
sku.user_def_type_2||+'-'||+sku.user_def_type_1||+'-'||+sku.user_def_type_4 X6,
nvl(sum(it.update_qty),0) QQ6
from inventory_transaction it,pre_advice_header ph,sku
where it.client_id='TR'
and it.code='Receipt Reverse'
and it.reference_id=ph.pre_advice_id
and ph.client_id='TR'
and ph.status='In Progress'
and ph.supplier_id='PROMIN'
and it.sku_id=sku.sku_id
and sku.client_id='TR'
group by
sku.user_def_type_2||+'-'||+sku.user_def_type_1||+'-'||+sku.user_def_type_4)
,(select
sku.user_def_type_2||+'-'||+sku.user_def_type_1||+'-'||+sku.user_def_type_4 X7,
nvl(sum(it.update_qty),0) QQ7
from inventory_transaction it,pre_advice_header ph,sku
where it.client_id='T'
and it.code='Receipt Reverse'
and it.reference_id=ph.pre_advice_id
and ph.client_id='T'
and ph.status='In Progress'
and ph.supplier_id='WH1'
and it.sku_id=sku.sku_id
and sku.client_id='T'
group by
sku.user_def_type_2||+'-'||+sku.user_def_type_1||+'-'||+sku.user_def_type_4)
,(select
sku.user_def_type_2||+'-'||+sku.user_def_type_1||+'-'||+sku.user_def_type_4 X8,
nvl(sum(it.update_qty),0) QQ8
from inventory_transaction it,pre_advice_header ph,sku
where it.client_id='TR'
and it.code='Receipt Reverse'
and it.reference_id=ph.pre_advice_id
and ph.client_id='TR'
and ph.status='In Progress'
and ph.supplier_id='WH2'
and it.sku_id=sku.sku_id
and sku.client_id='TR'
group by
sku.user_def_type_2||+'-'||+sku.user_def_type_1||+'-'||+sku.user_def_type_4)
,(select
sku.user_def_type_2||+'-'||+sku.user_def_type_1||+'-'||+sku.user_def_type_4 X81,
nvl(sum(it.update_qty),0) QQ81
from inventory_transaction it,pre_advice_header ph,sku
where it.client_id='T'
and it.code='Receipt Reverse'
and it.reference_id=ph.pre_advice_id
and ph.client_id='T'
and ph.status='In Progress'
and ph.supplier_id not in ('PROMIN','WH1')
and it.sku_id=sku.sku_id
and sku.client_id='T'
group by
sku.user_def_type_2||+'-'||+sku.user_def_type_1||+'-'||+sku.user_def_type_4)
,(select
sku.user_def_type_2||+'-'||+sku.user_def_type_1||+'-'||+sku.user_def_type_4 X82,
nvl(sum(it.update_qty),0) QQ82
from inventory_transaction it,pre_advice_header ph,sku
where it.client_id='TR'
and it.code='Receipt Reverse'
and it.reference_id=ph.pre_advice_id
and ph.client_id='TR'
and ph.status='In Progress'
and ph.supplier_id not in ('PROMIN','WH2')
and it.sku_id=sku.sku_id
and sku.client_id='TR'
group by
sku.user_def_type_2||+'-'||+sku.user_def_type_1||+'-'||+sku.user_def_type_4)
,(select
sku.user_def_type_5 X83,
nvl(sum(it.update_qty),0) QQ83
from inventory_transaction it,pre_advice_header ph,sku
where it.client_id='PR'
and it.code='Receipt Reverse'
and it.reference_id=ph.pre_advice_id
and ph.client_id='PR'
and ph.status='In Progress'
and it.sku_id=sku.sku_id
and sku.client_id='PR'
and sku.user_def_type_2='TML'
group by
sku.user_def_type_5)
where S1=X1 (+)
and S1=X2 (+)
and S1=X3 (+)
and S1=X4 (+)
and S1=X41 (+)
and S1=X42 (+)
and S1=X43 (+)
and S1=X5 (+)
and S1=X6 (+)
and S1=X7 (+)
and S1=X8 (+)
and S1=X81 (+)
and S1=X82 (+)
and S1=X83 (+)
order by
S1
/

--spool off


