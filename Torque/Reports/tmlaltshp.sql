SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','
SET WRAP OFF


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON



select 'Created, PO, H no, Shrt, Trs, Shipped, Type' from DUAL
union all
select DTE || ',' || PO || ',' || ORD || ',' || SH || ',' || TRS || ',' || SHP || ',' || TYP
from 
(
SELECT A.DTE, A.PO, A.ORD, B.SH, C.TRS, A.SHP, A.TYP
FROM (
select oh.purchase_order PO, oh.order_id ORD, to_char(oh.creation_date, 'DD-MON-YYYY') DTE, oh.order_type as TYP, to_char(oh.shipped_date,'DD-MON-YYYY') as SHP
from order_header oh inner join order_line ol on oh.order_id = ol.order_id
where oh.client_id = 'TR'
and ol.client_id = 'TR'
and oh.ship_dock in ('TRALT','TRALTCC')
and oh.status = 'Shipped'
and oh.shipped_date between to_date('&&2', 'DD/MM/YY') and to_date('&&2', 'DD/MM/YY')+1
and ol.user_def_note_1 like 'ALTE%'
group by oh.purchase_order, oh.order_id, to_char(oh.creation_date, 'DD-MON-YYYY'), oh.order_type, to_char(oh.shipped_date,'DD-MON-YYYY')
) A
LEFT JOIN 
(
select oh.order_id ORD, sum(ol.qty_ordered) SH
from order_header oh inner join order_line ol on oh.order_id = ol.order_id inner join sku on sku.sku_id = ol.sku_id
where oh.client_id = 'TR'
and ol.client_id = 'TR'
and oh.ship_dock in ('TRALT','TRALTCC')
and sku.client_id = 'TR'
and ol.user_def_note_1 like 'ALT%'
and sku.product_group in ('MS','WS')
and oh.status = 'Shipped'
and oh.shipped_date between to_date('&&2', 'DD/MM/YY') and to_date('&&2', 'DD/MM/YY')+1
group by oh.order_id
) B
ON A.ORD = B.ORD
LEFT JOIN 
(
select oh.order_id ORD, sum(ol.qty_ordered) TRS
from order_header oh inner join order_line ol on oh.order_id = ol.order_id inner join sku on sku.sku_id = ol.sku_id
where oh.client_id = 'TR'
and ol.client_id = 'TR'
and oh.ship_dock in ('TRALT','TRALTCC')
and sku.client_id = 'TR'
and ol.user_def_note_1 like 'ALT%'
and sku.product_group in ('MP','WP')
and oh.status = 'Shipped'
and oh.shipped_date between to_date('&&2', 'DD/MM/YY') and to_date('&&2', 'DD/MM/YY')+1
group by oh.order_id
) C
ON A.ORD = C.ORD
order by TYP, DTE
)
/