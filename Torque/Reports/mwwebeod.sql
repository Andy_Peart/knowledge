SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Work Group,Consignment,Priority,Order id,Creation Date,Creation Time,Order Date,Order Time,Status,Orders,Lines Ordered,Units Ordered,Units Tasked,Units Picked,Units Short,Units Shipped,Order Value,Order Weight,Order Volume' from dual
union all
select work_group||','||consignment||','||priority||','||order_id||','||creationDate||','||creationTime||','||orderDate||','||orderTime||','||status||','||orders||','||linesOrdered||','||
unitsOrdered||','||unitsTasked||','||unitsPicked||','||unitsShort||','||unitsShipped||','||order_value||','||order_weight||','||order_volume
from
(
    select oh.work_group
    , oh.consignment
    , oh.priority
    , oh.order_id
    , case when to_char(oh.creation_date,'hh24') >= '22' then trunc(oh.creation_date + 1) else trunc(oh.creation_date) end creationDate
    , to_char(oh.creation_date,'hh24:mi') creationTime
    , trunc(oh.order_date) orderDate
    , to_char(oh.order_date,'hh24:mi') orderTime
    , oh.status
    , 1 orders
    , oh.num_lines linesOrdered
    , coalesce(sum(ol.qty_ordered),0) unitsOrdered
    , coalesce(sum(ol.qty_tasked),0) unitsTasked
    , coalesce(sum(ol.qty_picked),0) unitsPicked
    , sum(ol.qty_ordered) - coalesce(sum(ol.qty_tasked),0) - coalesce(sum(ol.qty_picked),0) unitsShort
    , coalesce(sum(ol.qty_shipped),0) unitsShipped
    , oh.order_value
    , oh.order_weight
    , oh.order_volume
    from ORDER_HEADER oh
    inner join ORDER_LINE ol on ol.order_id = oh.order_id and ol.client_id = oh.client_id
    where oh.client_id = 'MOUNTAIN'
    and oh.order_type = 'WEB'
    and oh.status in ('Allocated','In Progress','Released','Hold','Picked')
    group by oh.work_group
    , oh.consignment
    , oh.priority
    , oh.order_id
    , oh.creation_date
    , oh.order_date
    , oh.status
    , oh.num_lines
    , oh.order_value
    , oh.order_weight
    , oh.order_volume
    order by oh.consignment
)
/
--spool off