SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 80
set trimspool on

--SET TERMOUT OFF
--SPOOL RSRadjustments.csv

select 'Reference,SKU,Description,Adjustment Qty' from DUAL;

select
it.reference_id||','||
--oh.name||','||
it.sku_id||','||
sku.description||','||
it.update_qty
from inventory_transaction it,sku,location loc
where it.client_id='ECHO'
and it.code='Adjustment'
and trunc(it.Dstamp)=trunc(sysdate)
and it.sku_id=sku.sku_id
and sku.client_id='ECHO'
and it.from_loc_id=loc.location_id
and loc.zone_1 like '%RSR%'
order by 1
/


--spool off

