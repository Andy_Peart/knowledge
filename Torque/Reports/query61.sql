SET FEEDBACK OFF
SET VERIFY OFF
SET TAB OFF
SET COLSEP ' '

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Column Headings and Formats */
COLUMN A heading "Product|Group" FORMAT a12
COLUMN B heading "Unit|Qty" FORMAT 9999999
COLUMN C heading "Locs|Qty" FORMAT 9999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


SET LINESIZE 60
SET WRAP OFF
SET PAGES 66
SET NEWPAGE 0

/* Top Title */
TTITLE LEFT 'Product Group Totals for client &&2 as at ' report_date SKIP 2

/* Set up page and line */

break on report

compute sum label 'Total_________' of B C on report

with t0 as (select
	case 
	when sku.product_group = 'BATHTIME' then 'TIPPITOES________'
	when sku.product_group = 'HIGHCHAIRS' then 'TIPPITOES________'
	when sku.product_group = 'OUT AND AB' then 'TIPPITOES________'
	when sku.product_group = 'STROLLERS' then 'TIPPITOES________'
	when sku.product_group = 'TOILET TRA' then 'TIPPITOES________'
	when sku.product_group = 'TIPPITOES' then 'TIPPITOES________'
	when sku.product_group  = 'TOC' then 'CLSHOES__________'
	when sku.product_group  is null then '(No Group)____________'
	when sku.product_group = 'MP' and sku.description like '%CHINO%' then 'CHINOS___________' 
	else sku.product_group||'________________'
	end A,
	i.qty_on_hand B
	from inventory i,sku
	where i.client_id='&&2'
	and sku.client_id='&&2'
	and i.sku_id=sku.sku_id
	and i.location_id <> 'TP1'
	order by sku.product_group
),
t1 as (
	select A, sum(B) B, count(*) C
	from t0 
	group by A
)
select A,sum(B) B,sum(C) C
from T1
group by A
order by 1
/
