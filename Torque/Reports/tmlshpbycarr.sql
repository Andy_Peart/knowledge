SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON

column A heading 'Carrier' format a20
column B heading 'Date Shipped' format a20
column C heading 'Orders' format 999999999
column D heading 'Units' format 999999999
column E heading 'Units per order' format 9999999.99

break on A skip 1
compute sum label 'Total' of C D on A


select 'Carrier,Date Shipped,Orders,Units,Units per order' from dual;

select carrier A, date_shipped B, count(*) C, sum(qty) D, round(sum(qty)/ count(*),2) E
from (
select to_char(oh.shipped_date,'DD-MON-YYYY') date_shipped, oh.order_id, sum(ol.qty_shipped) qty,
CASE
 when dispatch_method = 4 or dispatch_method = 5 then 'OCS'
 when dispatch_method = 2 or dispatch_method = 3 or dispatch_method = 7 or dispatch_method = 997 then 'Hermes'
 when dispatch_method = 6 or dispatch_method = 9 or dispatch_method = 11 or dispatch_method = 12 then 'UK Mail'
 when dispatch_method = 1 then
  case
    when oh.user_def_type_5 in ('T213','T220','T231','T237','T239','T240','T265','T266','T268','T270','T271','T272','T295','T296','T299','T306') 
    and trim(to_char(oh.shipped_date, 'DAY')) = 'FRIDAY'  then 'UK Mail'
    when oh.user_def_type_5 in ('T284','T283','T276','T263','T261','T259','T244','T236','T223','T219','T215') 
    and trim(to_char(oh.shipped_date, 'DAY')) = 'SUNDAY'  then 'UK Mail'
    else 'Click and Collect'
  end
 else 'Other'
end carrier
from order_header oh inner join order_line ol on oh.order_id = ol.order_id and oh.client_id = ol.client_id
where oh.client_id = 'TR'
and oh.order_id like 'H%'
and oh.shipped_date between to_date('&&2') and to_date('&&3')
and oh.status = 'Shipped'
group by
to_char(oh.shipped_date,'DD-MON-YYYY'), oh.order_id, 
CASE
 when dispatch_method = 4 or dispatch_method = 5 then 'OCS'
 when dispatch_method = 2 or dispatch_method = 3 or dispatch_method = 7 or dispatch_method = 997 then 'Hermes'
 when dispatch_method = 6 or dispatch_method = 9 or dispatch_method = 11 or dispatch_method = 12 then 'UK Mail'
 when dispatch_method = 1 then
  case
    when oh.user_def_type_5 in ('T213','T220','T231','T237','T239','T240','T265','T266','T268','T270','T271','T272','T295','T296','T299','T306') 
    and trim(to_char(oh.shipped_date, 'DAY')) = 'FRIDAY'  then 'UK Mail'
    when oh.user_def_type_5 in ('T284','T283','T276','T263','T261','T259','T244','T236','T223','T219','T215') 
    and trim(to_char(oh.shipped_date, 'DAY')) = 'SUNDAY'  then 'UK Mail'
    else 'Click and Collect'
  end
 else 'Other'
end
)
where carrier <> 'Other'
group by  carrier, date_shipped
order by 1,2
/
