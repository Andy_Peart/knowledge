SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 120

column A heading 'Cust' format a40
column AA heading 'Grp' format a6
column B heading 'Type' format a12
column BB heading 'Type' format a15
column C heading 'Qty' format 999999


--break on A skip 2 on AA dup skip 1 on report

--compute sum label 'Total' of C on A
--compute sum label 'Total' of C on AA
--compute sum label 'Total' of C on report


--set termout off
--spool TR042.csv

select 'TR42 Shipped last week - Starting '||to_char(sysdate-7,'DD/MM/YYYY') from Dual;

SELECT
distinct 'OT '||nvl(oh.order_type,'Blank') BB,
'END'
FROM shipping_manifest sm, order_header oh
WHERE sm.client_id='TR'
and sm.client_id=oh.client_id
and sm.order_id=oh.order_id
and sm.customer_id=oh.customer_id
and sm.consignment=oh.consignment
and trunc(sm.shipped_dstamp)>trunc(sysdate-8)
and trunc(sm.shipped_dstamp)<trunc(sysdate)
order by
BB
/


SELECT
sm.CUSTOMER_ID||' - '||a.name A,
sku.product_group AA,
nvl(oh.order_type,'Blank') B,
nvl(sum(sm.QTY_SHIPPED),0) C
FROM shipping_manifest sm, sku, order_header oh, address a
WHERE sm.client_id='TR'
and sm.client_id=sku.client_id
and sm.sku_id=sku.sku_id
and sm.client_id=oh.client_id
and sm.order_id=oh.order_id
and sm.customer_id=oh.customer_id
and sm.consignment=oh.consignment
and trunc(sm.shipped_dstamp)>trunc(sysdate-8)
and trunc(sm.shipped_dstamp)<trunc(sysdate)
and sm.customer_id=a.address_id
and a.client_id='TR'
group by
sm.CUSTOMER_ID,
a.name,
sku.product_group,
oh.order_type
order by
A,AA,B
/

select 'END' from DUAL;


--spool off
--set termout on
