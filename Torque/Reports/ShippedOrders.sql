set feedback off
set pagesize 68
set linesize 112
set verify off

clear columns
clear breaks
clear computes


column A heading 'Shipped' format A16
column BB heading 'Order Type' format A16
column B heading 'Order ID' format A20
column C heading 'Customer' format A32
column D heading 'Shipments' format 9999999
column E heading 'Units' format 9999999
column M format a4 noprint

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'Total Units Despatched from &&2 to &&3 - for client ID &&5 as at ' report_date RIGHT 'Page:'FORMAT 999 SQL.PNO skip 2


break on REPORT on BB skip 2 on A skip 1

compute sum label 'Report Totals' of D E on REPORT
compute sum label 'Type Totals' of D E on BB 
compute sum label 'Daily Totals' of D E on A 
 

select
to_char(ol.Dstamp, 'YYMM') M,
to_char(ol.Dstamp, 'DD/MM/YY ') as A,
oh.order_type as BB,
oh.order_id as B,
oh.inv_name as C,
--nvl(sum(ol.qty_ordered),0) as D,
count(*) as D,
nvl(sum(ol.update_qty),0) as E
from order_header oh, inventory_transaction ol
where  oh.order_id=ol.reference_id
and oh.client_id='&&5'
and (oh.order_type like '%&&4%' or oh.order_type is null)
and oh.status in ('Shipped')
and ol.dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and ol.code='Shipment'
group by
oh.order_type,
to_char(ol.Dstamp, 'YYMM'),
to_char(ol.Dstamp, 'DD/MM/YY '),
oh.order_type,
oh.order_id,
oh.inv_name
order by
oh.order_type,
to_char(ol.Dstamp, 'YYMM'),
to_char(ol.Dstamp, 'DD/MM/YY '),
oh.order_id
/

