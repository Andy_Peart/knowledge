SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Sku,Description,Qty on Hand,Released Orders,Allocated/In Progress Orders' from dual
union all
select "Sku" || ',' || "Description" || ',' || "Qty on Hand" || ',' || "Released Orders" || ',' || "Allocated/In Progress Orders" from (
with t1 as (
select kl.sku_id as "Sku"
, s.description as "Desc"
, nvl(sum(i.qty_on_hand*kl.quantity), 0) as "Qty"
from kit_line kl
left join inventory i on i.sku_id = kl.kit_id and i.client_id = kl.client_id
left join sku s on s.client_id = kl.client_id and s.sku_id = kl.sku_id
where kl.client_id = 'GG'
group by kl.sku_id
, s.description
)
--KIT LINES INDIVIDUAL INVENTORY
, t2 as (
select sku_id as "Sku"
, sum(qty_on_hand) as "Qty"
from inventory
where client_id = 'GG'
and sku_id in (select sku_id from kit_line where client_id = 'GG')
group by sku_id
)
--RELEASED ORDERS USING KIT HEADERS TO WORK OUT QTY'S
, t3 as (
select kl.sku_id as "Sku"
, sum((nvl(ol.qty_ordered,0)-nvl(ol.qty_picked,0))*kl.quantity) as "Qty"
from kit_line kl
left join order_line ol on ol.sku_id = kl.kit_id and ol.client_id = kl.client_id
left join order_header oh on oh.order_id = ol.order_id and oh.client_id = ol.client_id
where kl.client_id = 'GG'
and oh.status = 'Released'
group by kl.sku_id
)
----ALLOCATED/IN PROGRESS ORDERS USING KIT HEADERS TO WORK OUT QTY'S
, t4 as (
select kl.sku_id as "Sku"
, sum((nvl(ol.qty_ordered,0)-nvl(ol.qty_picked,0))*kl.quantity) as "Qty"
from kit_line kl
left join order_line ol on ol.sku_id = kl.kit_id and ol.client_id = kl.client_id
left join order_header oh on oh.order_id = ol.order_id and oh.client_id = ol.client_id
where kl.client_id = 'GG'
and oh.status in ('In Progress', 'Allocated')
group by kl.sku_id
)
--RELEASED ORDERS WHERE ORDER LINE IS A KIT LINE DIRECTLY
, t5 as (
select ol.sku_id as "Sku"
, sum(nvl(ol.qty_ordered,0)-nvl(ol.qty_picked,0)) as "Qty"
from order_line ol
inner join order_header oh on oh.client_id = ol.client_id and oh.order_id = ol.order_id
where ol.client_id = 'GG'
and ol.sku_id in (select sku_id from kit_line where client_id = 'GG')
and oh.status = 'Released'
group by ol.sku_id
)
--ALLOCATED/IN PROGRESS ORDERS WHERE ORDER LINE IS A KIT LINE DIRECTLY
, t6 as (
select ol.sku_id as "Sku"
, sum(nvl(ol.qty_ordered,0)-nvl(ol.qty_picked,0)) as "Qty"
from order_line ol
inner join order_header oh on oh.client_id = ol.client_id and oh.order_id = ol.order_id
where ol.client_id = 'GG'
and ol.sku_id in (select sku_id from kit_line where client_id = 'GG')
and oh.status in ('In Progress', 'Allocated')
group by ol.sku_id)
select t1."Sku"                             as "Sku"
, t1."Desc"                                 as "Description"
, sum(nvl(t1."Qty",0) + nvl(t2."Qty",0))    as "Qty on Hand"
, sum(nvl(t3."Qty",0) + nvl(t5."Qty",0))    as "Released Orders"
, sum(nvl(t4."Qty",0) + nvl(t6."Qty",0))    as "Allocated/In Progress Orders"
from t1
left join t2 on t2."Sku" = t1."Sku"
left join t3 on t3."Sku" = t1."Sku"
left join t4 on t4."Sku" = t1."Sku"
left join t5 on t5."Sku" = t1."Sku"
left join t6 on t6."Sku" = t1."Sku"
group by t1."Sku"
, t1."Desc"
union all
select * from (
--SKU'S AND QTY FOR SKUS NOT ASSOCIATED WITH KITS
with t1 as (
select i.sku_id as "Sku"
, s.description as "Desc"
, sum(i.qty_on_hand) as "Qty"
from inventory i
left join sku s on s.client_id = i.client_id and s.sku_id = i.sku_id
inner join location l on l.site_id = i.site_id and l.location_id = i.location_id
where i.client_id = 'GG'
and i.sku_id not in (select sku_id from kit_line where client_id = 'GG')
and i.sku_id not in (select kit_id from kit_line where client_id = 'GG')
group by i.sku_id
, s.description
)
--RELEASED ORDERS WHERE ORDER LINE IS NOT ASSOCIATED WITH KITS
, t2 as (
select ol.sku_id as "Sku"
, sum(nvl(ol.qty_ordered,0) - nvl(ol.qty_picked,0)) as "Qty"
from order_line ol
inner join order_header oh on oh.order_id = ol.order_id and oh.client_id = ol.client_id
where ol.client_id = 'GG'
and ol.sku_id not in (select sku_id from kit_line where client_id = 'GG')
and ol.sku_id not in (select kit_id from kit_line where client_id = 'GG')
and oh.status = 'Released'
group by ol.sku_id
)
--ALLOCATED/IN PROGRESS ORDERS WHERE ORDER LINE IS NOT ASSOCIATED WITH KITS
, t3 as (
select ol.sku_id as "Sku"
, sum(nvl(ol.qty_ordered,0) - nvl(ol.qty_picked,0)) as "Qty"
from order_line ol
inner join order_header oh on oh.order_id = ol.order_id and oh.client_id = ol.client_id
where ol.client_id = 'GG'
and ol.sku_id not in (select sku_id from kit_line where client_id = 'GG')
and ol.sku_id not in (select kit_id from kit_line where client_id = 'GG')
and oh.status in ('In Progress','Allocated')
group by ol.sku_id
)
select t1."Sku"         as "Sku"
, t1."Desc"             as "Description"
, sum(nvl(t1."Qty",0))  as "Qty"
, sum(nvl(t2."Qty",0))  as "Released"
, sum(nvl(t3."Qty",0))  as "Allocated"
from t1
left join t2 on t2."Sku" = t1."Sku"
left join t3 on t2."Sku" = t1."Sku"
group by t1."Sku"
, t1."Desc"
) 
order by 1
)
/
--spool off