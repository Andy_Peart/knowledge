/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :     promhangs.sql				      */
/*                                                                            */
/*     DESCRIPTION:     Prom hangs stock to be moved to Wakefield                  */
/*                                                                            */
/*   DATE     BY   DESCRIPTION						                                    */
/*   ======== ==== ===========					                                      */
/*   27/01/15 SS   Prom hangs stock to be moved to Wakefield                  */
/*   
/******************************************************************************/
SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','
SET WRAP OFF


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500



/* Column Headings and Formats */
COLUMN BA heading "Locs Range" FORMAT a20
COLUMN BB heading "Location" FORMAT a16
COLUMN BC heading "Units" FORMAT 9999999
COLUMN BD heading "SKUs" FORMAT 9999999
COLUMN BE heading "Loc Type" FORMAT a16




set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



/* Top Title */
select 'Locs Range, Location, Units, SKUs, Loc type' from dual;


BREAK ON BA on REPORT skip 2
column BA new_value null
compute count label 'Total locs' of BB on BA skip 2
compute sum label 'Totals' of BC on report



select A.volume BA, A.location_id as BB, A.units as BC,  A.skus as BD,  B.loc_type as BE
from
(
select location_id, sum(qty_on_hand) units, count(*) skus, 
case
  when sum(qty_on_hand)>0 and sum(qty_on_hand)<=10  then 'from 1 to 10'
  when sum(qty_on_hand)>10 and sum(qty_on_hand)<=25  then  'from 11 to 25'
  when sum(qty_on_hand)>25 and sum(qty_on_hand)<=50  then  'from 26 to 50'
  when sum(qty_on_hand)>50 and sum(qty_on_hand)<=75  then  'from 51 to 75'
  when sum(qty_on_hand)>75 and sum(qty_on_hand)<=100  then  'from 76 to 100'
  when sum(qty_on_hand)>100 and sum(qty_on_hand)<=500  then  'from 101 to 500'
  when sum(qty_on_hand)>500 and sum(qty_on_hand)<=1000  then  'from 501 to 1000'
  when sum(qty_on_hand)>1000 and sum(qty_on_hand)<=2000  then  'from 1001 to 2000'
  when sum(qty_on_hand)>2000 then 'over 2000' 
end volume  
from inventory
where client_id = 'PR'
and site_id = 'PROM'
and (
location_id like 'W01%'
or
location_id like 'W03%'
or
location_id like 'W05%'
or
location_id like 'W07%'
or
location_id like 'W09%'
or
location_id like 'W11%'
or
location_id like 'W13%'
or
location_id like 'W15%'
or
location_id like 'W17%'
or
location_id like 'W19%'
or
location_id like 'W21%'
or
location_id like 'W23%'
or
location_id like 'W25%'
or
location_id like 'W27%'
or
location_id like 'W29%'
or
location_id like 'W31%'
or
location_id like 'W33%'
or
location_id like 'W35%'
or
location_id like 'W37%'
or
location_id like 'W39%'
or
location_id like 'W41%'
or
location_id like 'W43%'
or
location_id like 'W45%'
or
location_id like '46A%'
or
location_id like '46B%'
or
location_id like '46C%'
or
location_id like '46D%'
or
location_id like '48A%'
or
location_id like '48B%'
or
location_id like '48C%'
or
location_id like '48D%'
or
location_id like '00%'
or
location_id like 'SH%'
or
location_id like 'QCH%'
or
location_id like 'AQLH%'
or
location_id like 'PROCH%'
or
location_id like 'HGL%'
or
location_id like 'BW%'
)
group by location_id
) A
left join LOCATION B
on A.location_id = B.LOCATION_ID
where B.site_id = 'PROM'
order by 3
/