/********************************************************************************************/
/*                                                                                          */
/*                            Elite                                                         */
/*                                                                                          */
/*     FILE NAME  :   SBgiftnote.sql                                                        */
/*                                                                                          */
/*     DESCRIPTION:    Datastream for SB Gift Note                                          */
/*                                                                                          */
/*   DATE     BY   PROJ       ID         DESCRIPTION                                        */
/*   ======== ==== ======     ========   =============                                      */
/*   01/09/10 RW   SB                    Gift Note                                          */
/*                                                                                          */
/********************************************************************************************/

/* Input Parameters are as follows */
-- 1)  GMT
-- 2)  consignment
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 0
SET VERIFY OFF
SET FEEDBACK OFF
SET TAB OFF

 
-- Set the line length to the maximum possible, but trim 
-- trailing spaces for both screen output and spool files
SET LINESIZE 32767
SET WRAP ON
SET TRIMOUT ON
SET TRIMSPOOL ON


/* Define the format of the output dates */
DEFINE	OutputDateFormat = 'DD-MON-YYYY HH24:MI:SS'
COLUMN	sorter	NOPRINT

SELECT Distinct '0' sorter,
       'DSTREAM_SB_GIFT_HDR'           ||'|'||
       rtrim('&&2')                      /* order */
from dual
union all
SELECT distinct '1' sorter,
'DSTREAM_SB_GIFTNOTE_HDR'              	||'|'||
rtrim (oh.order_id)						||'|'||
rtrim (oh.user_def_note_2)||chr(13)||chr(13)||chr(10)
from order_header oh
where oh.order_id = '&&2'
AND oh.CLIENT_ID = 'SB'
and oh.user_def_note_2 is not null

/