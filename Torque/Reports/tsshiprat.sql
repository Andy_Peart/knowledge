SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON

select 'Order', 'Customer', 'Units' from dual
union all
select to_char(odh.order_id), to_char(odh.name), to_char(sum(odl.QTY_PICKED*sku.USER_DEF_NUM_1)) 
from ORDER_HEADER odh 
inner join order_line odl on odh.order_id = odl.order_id and odh.client_id = odl.client_id
inner join sku sku on odh.client_id = sku.client_id and odl.sku_id = sku.sku_id
where odh.client_id = 'TS'
and odh.order_type = 'RET'
and odh.status = 'Shipped'
and odh.shipped_date between trunc (sysdate) and trunc (sysdate)+1
group by  to_char(odh.order_id), to_char(odh.name)
/
