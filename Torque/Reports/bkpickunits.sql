SET FEEDBACK OFF                 
set pagesize 68
set linesize 72
set verify off
set wrap off
set newpage 0
set colsep ' '
set und on

clear columns
clear breaks
clear computes


column TT heading 'TYPE' format A11
column QQQ heading 'UNITS' format 9999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



ttitle  LEFT 'TML Retail - OK to Consign Totals for &&2 ' RIGHT 'Printed ' report_date skip 2


break on report

compute sum label 'Total' of QQQ on report

select
oh.order_type TT,
sum(PP) QQQ
from order_header oh,address a,
(select ID,OO,PP from
(select
oh.order_id ID,
oh.order_type  TT,
sum(ol.qty_ordered) OO,
nvl(sum(ol.qty_tasked),0) PP
from order_header oh,order_line ol
where oh.client_id='TR'
and oh.order_id=ol.order_id
group by
oh.order_id,
oh.order_type)
where OO>PP
or TT not in ('PREALLOC-B','PREALLOC-M'))
where oh.order_type<>'REPLEN'
and oh.order_id=ID
and oh.status not in ('Cancelled','Hold','Shipped')
and (oh.consignment is null and oh.order_type<>'RATIO' and oh.status<>'Released')
and oh.client_id='TR'
and a.client_id='TR'
--and a.user_def_date_1 is null
and oh.customer_id=a.address_id
--and ((('&&2'='MONDAY') and (a.delivery_open_mon='Y') and (a.user_def_type_2 is not null))
--or (('&&2'='TUESDAY') and (a.delivery_open_tue='Y') and (a.user_def_type_3 is not null))
--or (('&&2'='WEDNESDAY') and (a.delivery_open_wed='Y') and (a.user_def_type_4 is not null))
--or (('&&2'='THURSDAY') and (a.delivery_open_thur='Y') and (a.user_def_type_5 is not null))
--or (('&&2'='FRIDAY') and (a.delivery_open_fri='Y') and (a.user_def_type_1 is not null)))
and ((('&&2'='MONDAY') and (a.delivery_open_mon='Y'))
or (('&&2'='TUESDAY') and (a.delivery_open_tue='Y'))
or (('&&2'='WEDNESDAY') and (a.delivery_open_wed='Y'))
or (('&&2'='THURSDAY') and (a.delivery_open_thur='Y'))
or (('&&2'='FRIDAY') and (a.delivery_open_fri='Y')))
group by
oh.order_type
/
