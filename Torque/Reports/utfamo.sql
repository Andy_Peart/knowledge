SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET COLSEP ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 999
SET LINESIZE 200
SET TRIMSPOOL ON
SET HEADING ON 


column A heading 'Order Type' format A12
column C heading 'Country' format A8
column F heading 'Dispatch Method' format A16
column AA heading 'Order' format A12
column BB heading 'Date' format A14
column CC heading 'Order|Qty' format 999999

--select 'Shop,Date,Qty' from DUAL;

--break on A dup skip 2 on F dup skip 1

spool utfamo.csv

select
KKK K,
DDD A,
FFF F,
CCC C,
AAA AA,
BBB BB,
OOO CC from
(select
oh.user_def_chk_4 KKK,
oh.order_type  DDD,
oh.Dispatch_method FFF,
oh.country CCC,
oh.order_id  AAA,
--to_char(oh.creation_date,'DD MON, HH24:MI') BBB,
to_char(oh.creation_date,'DD MON') BBB,
nvl(sum(ol.qty_ordered),0) OOO,
nvl(sum(ol.qty_tasked),0) TTT,
count(*) CT
from order_header oh, order_line ol
where oh.client_id='UT'
and oh.status not in ('Shipped','Cancelled')
--and (oh.order_id like 'C%' or oh.order_id like 'E%')
and oh.order_id=ol.order_id
and oh.order_type like '&&2%'
group by
oh.user_def_chk_4,
oh.order_type,
oh.Dispatch_method,
oh.country,
oh.order_id,
to_char(oh.creation_date,'DD MON'))
where OOO=TTT
--and CT=1
order by
K DESC,A,F,BB,AA
/

spool off
