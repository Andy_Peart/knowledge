SET FEEDBACK OFF                 
set pagesize 68
set linesize 160
set verify off

clear columns
clear breaks
clear computes

column C heading 'Client ID' format a10
column D heading 'Date' format a16
column A heading 'Reference' format a16
column B heading 'Qty Received' format 99999999
column M format a4 noprint

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'Total Units Received from &&3 to &&4 - for client ID &&2 as at ' report_date skip 2

break on report

compute sum label 'Totals' of B on report


select
to_char(it.Dstamp, 'YYMM') M,
to_char(it.Dstamp, 'DD/MM/YY') D,
it.reference_id A,
sum(it.update_qty) as B
from inventory_transaction it
where it.Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and it.client_id='&&2'
and it.station_id not like 'Auto%'
and it.code like 'Receipt%'
group by
to_char(it.Dstamp, 'YYMM'),
to_char(it.Dstamp, 'DD/MM/YY'),
it.reference_id
union
select
to_char(it.Dstamp, 'YYMM') M,
to_char(it.Dstamp, 'DD/MM/YY') D,
it.reference_id A,
sum(it.update_qty) as B
from inventory_transaction_archive it
where it.Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and it.client_id='&&2'
and it.station_id not like 'Auto%'
and it.code like 'Receipt%'
group by
to_char(it.Dstamp, 'YYMM'),
to_char(it.Dstamp, 'DD/MM/YY'),
it.reference_id
order by M,D,A
/
