SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 900
SET TRIMSPOOL ON
SET HEADING ON

with all_stock as (
  select to_char(sysdate, 'DD-MON-YYYY') as reportdate,
  inv.sku_id, '"=""'||inv.sku_id||'"""' ProductCode,replace(replace(sku.description,',',' '),'�','')  Description,replace(sku.colour,',',' ') as Colour,'"=""'||sku.sku_size||'"""' SkuSize, 
  sum(inv.qty_on_hand) qty, sum(inv.qty_allocated) qty_alloc
  from inventory inv inner join sku on inv.sku_id = sku.sku_id and inv.client_id = sku.client_id
  where inv.client_id = 'JY'
  group by inv.sku_id, sku.description, replace(sku.colour,',',' '), sku.sku_size
),
intake_stock as (
  select inv.sku_id, sum(inv.qty_on_hand) qty
  from inventory inv inner join  location loc on inv.location_id = loc.location_id and inv.site_id = loc.site_id
  where inv.client_id = 'JY'
  and loc.loc_type in ('Receive Dock')
  and inv.lock_status <> 'Locked'
  group by inv.sku_id
),
qchold_stock as (
  select inv.sku_id, sum(inv.qty_on_hand) qty
  from inventory inv
  where inv.client_id = 'JY'
  and inv.condition_id = 'QCHOLD'
  and inv.lock_status <> 'Locked'
  group by inv.sku_id
),
suspense_stock as (
  select inv.sku_id, sum(inv.qty_on_hand) qty
  from inventory inv
  where inv.client_id = 'JY'  
  and inv.location_id not in ('JYFLTY','JYFLTY1')
  and  inv.location_id = 'SUSPENSE' 
  group by inv.sku_id
),
shipdock_stock as (
  select inv.sku_id, sum(inv.qty_on_hand) qty
  from inventory inv inner join  location loc on inv.location_id = loc.location_id and inv.site_id = loc.site_id
  where inv.client_id = 'JY'
  and loc.loc_type in ('ShipDock')
  and inv.lock_status <> 'Locked'
  group by inv.sku_id
),
reject_stock as (
 select sku_id, sum(qty) qty
 from (
  select inv.sku_id, inv.lock_status, inv.location_id, sum(inv.qty_on_hand) qty
  from inventory inv inner join location loc on inv.location_id = loc.location_id and inv.site_id = loc.site_id
  where inv.client_id = 'JY'
   and loc.location_id in ('JYFLTY','JYFLTY1')
  group by inv.sku_id, inv.lock_status, inv.location_id               
)
group by sku_id
),
locked_stock as (
  select inv.sku_id, sum(inv.qty_on_hand) qty
  from inventory inv inner join  location loc on inv.location_id = loc.location_id and inv.site_id = loc.site_id
  where inv.client_id = 'JY'
	and loc.location_id not in ('JYFLTY','JYFLTY1','Suspense')
	and loc.loc_type = 'Tag-FIFO'
  and ( inv.lock_status = 'Locked'
   or loc.lock_status in ('Locked','Outlocked')
   or nvl(loc.disallow_alloc,'N') = 'Y')
  group by inv.sku_id
)
select 'Report Date, Product Code, Description, Colour, Size, Total Stock, Intake, QCHold, Suspense, ShipDock, Rejected, Locked, Allocated, Available' from dual
union all
select reportdate || ',' || productcode || ',' || description || ',' || colour || ',' || skusize || ',' || 
TotalStock || ',' || Intake || ',' || QCHold || ',' || Suspense || ',' || ShipDock || ',' || Rejected || ',' || Locked || ',' ||Allocated || ',' || Available
from (
select a.ReportDate, a.ProductCode, a.Description, a.Colour, a.SkuSize, 
nvl(a.Qty,0) as TotalStock,
nvl(b.Qty,0) as Intake,
nvl(c.Qty,0) as QCHold,
nvl(d.Qty,0) as Suspense,
nvl(e.Qty,0) as ShipDock,
nvl(f.qty,0) as rejected,
nvl(g.qty,0) as Locked,
nvl(a.Qty_alloc,0) as Allocated,
nvl(a.Qty,0)-nvl(b.Qty,0)-nvl(c.Qty,0)-nvl(d.Qty,0)-nvl(e.Qty,0)-nvl(f.Qty,0)-nvl(g.Qty,0)-nvl(a.Qty_alloc,0) as Available
from all_stock a
left join intake_stock b on a.sku_id = b.sku_id
left join qchold_stock c on a.sku_id = c.sku_id
left join suspense_stock d on a.sku_id = d.sku_id
left join shipdock_stock e on a.sku_id = e.sku_id
left join reject_stock f on a.sku_id = f.sku_id
left join locked_stock g on a.sku_id = g.sku_id
)
/