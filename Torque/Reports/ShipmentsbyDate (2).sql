SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET LINESIZE 240

column B heading 'Client' format a12
column A heading 'Type' format a12
column C heading ' ' format a6
column G heading 'Shipped' format 99999999

break on report
compute sum label 'Total' of G on report

select ' ' from DUAL;
select '      Shipments by Order Type' from DUAL;
select '  From &&2 to &&3' from DUAL;
select '          for CLIENT  &&4' from DUAL;
select ' ' from DUAL;

SET PAGESIZE 100

SELECT
--oh.client_id B,
'    ' C,
nvl(oh.ORDER_TYPE,'None') A,
sum(nvl(ol.QTY_SHIPPED,0)) G
from order_header oh,order_line ol
where oh.client_id='&&4'
and oh.client_id=ol.client_id
and oh.order_id=ol.order_id
and oh.STATUS='Shipped'
and oh.shipped_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by
--oh.client_id,
oh.ORDER_TYPE
order by
--oh.client_id,
oh.ORDER_TYPE
/

