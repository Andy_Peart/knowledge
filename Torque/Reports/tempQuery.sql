SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 6000
--SET NEWPAGE 0
SET LINESIZE 9999


--select * from inventory_transaction
--where client_id='TR'
--and code='Stock Check'
--and Dstamp between to_date('01-sep-2007', 'DD-MON-YYYY') and to_date('20-sep-2007', 'DD-MON-YYYY')+1
select * from order_line
where client_id='TR'
and creation_date between to_date('21-sep-2007', 'DD-MON-YYYY') and to_date('20-dec-2007', 'DD-MON-YYYY')+1
order by order_id,line_id
/
