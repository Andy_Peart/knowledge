SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 

column A heading 'Category' format A11
column B heading 'Qty' format 9999999
column C heading 'VH Qty' format 9999999
column D heading 'ALL' format 9999999

set heading off
set pagesize 0

select 'Stock Holding Report for CURVY KATE on ',to_char(SYSDATE, 'DD/MM/YY    HH:MI:SS') from DUAL;


select '' from DUAL;
select '' from DUAL;
select 'NORMAL STOCK' from DUAL;
select '' from DUAL;


select
'Stock' A,
nvl(sum(i.qty_on_hand),0) B
from inventory i
--where i.location_id not in ('IN','IN2','IN3','QCHOLD','WP1','SUSPENSE','CATALOGUES')
where i.client_id like 'CK'
and i.location_id not in ('CK6000')
and i.sku_id not in ('5052574061470','5052574061487')
/




select
'Allocated' A,
nvl(sum(i.qty_allocated),0) B
from inventory i
--where i.location_id not in ('IN','IN2','IN3','QCHOLD','WP1','SUSPENSE','CATALOGUES')
where i.client_id like 'CK'
and i.sku_id not in ('5052574061470','5052574061487')
--and i.sku_id not in ('205163119000','205164001000')
/
select
'Available' A,
nvl(sum(i.qty_on_hand-i.qty_allocated),0) B
from inventory i
--where i.location_id not in ('IN','IN2','IN3','QCHOLD','WP1','SUSPENSE','CATALOGUES')
where i.client_id like 'CK'
and i.sku_id not in ('5052574061470','5052574061487')
/


select '' from DUAL;
select '' from DUAL;
select 'HANGERS' from DUAL;
select '' from DUAL;

select
'Stock' A,
nvl(sum(i.qty_on_hand),0) B
from inventory i
--where i.location_id not in ('IN','IN2','IN3','QCHOLD','WP1','SUSPENSE','CATALOGUES')
where i.client_id like 'CK'
and i.sku_id in ('5052574061470','5052574061487')
/


select
'Allocated' A,
nvl(sum(i.qty_allocated),0) B
from inventory i
--where i.location_id not in ('IN','IN2','IN3','QCHOLD','WP1','SUSPENSE','CATALOGUES')
where i.client_id like 'CK'
and i.sku_id in ('5052574061470','5052574061487')
/
select
'Available' A,
nvl(sum(i.qty_on_hand-i.qty_allocated),0) B
from inventory i
--where i.location_id not in ('IN','IN2','IN3','QCHOLD','WP1','SUSPENSE','CATALOGUES')
where i.client_id like 'CK'
and i.sku_id in ('5052574061470','5052574061487')
/



break on report

compute sum label 'Total' of B C on report


select '' from DUAL;
select '' from DUAL;
select 'SUMMARY' from DUAL;
select '' from DUAL;

select
--DECODE(i.location_id,'CKOUT','CKOUT',DECODE(i.sku_id,'5052574061470','HANGERS','5052574061487','HANGERS','STOCK')) A,
DECODE(i.sku_id,'5052574061470','HANGERS','5052574061487','HANGERS','STOCK') A,
nvl(sum(i.qty_on_hand),0) B
--nvl(sum(i.qty_allocated),0) C
from inventory i
where i.client_id like 'CK'
group by
DECODE(i.sku_id,'5052574061470','HANGERS','5052574061487','HANGERS','STOCK') 
--DECODE(i.location_id,'CKOUT','CKOUT',DECODE(i.sku_id,'5052574061470','HANGERS','5052574061487','HANGERS','STOCK'))
/

select '' from DUAL;
select '' from DUAL;
select 'STOCK IN CKOUT' from DUAL;
select '' from DUAL;

select
DECODE(i.sku_id,'5052574061470','HANGERS','5052574061487','HANGERS','STOCK') A,
nvl(sum(i.qty_on_hand),0) B
--nvl(sum(i.qty_allocated),0) C
from inventory i
where i.client_id like 'CK'
and i.location_id='CKOUT'
group by
DECODE(i.sku_id,'5052574061470','HANGERS','5052574061487','HANGERS','STOCK') 
/

