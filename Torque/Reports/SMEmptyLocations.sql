SET FEEDBACK OFF                 
set pagesize 66
set linesize 110
set verify off
set wrap off
set newpage 0
set colsep ' '

clear columns
clear breaks
clear computes


column A heading 'Site' format a12
column B heading 'Location' format a12
column C heading 'Loc Type' format a12
column D heading 'Check String' format a16
column E heading 'Lock Status' format a12
column F heading 'In Stage' format a12
column G heading 'Out Stage' format a12
column H heading 'Pick Sequence' format 999999999



set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD-MON-YYYY  HH:MI:SS') curdate from DUAL;
set TERMOUT ON


TTITLE LEFT 'Empty Locations for OPS/&&2 ' RIGHT report_date skip 1 -
RIGHT 'Page: ' Format 999 SQL.PNO skip 2 -

BREAK ON site_id skip page
 

select
site_id A,
location_id B,
loc_type C,
check_string D,
lock_status E,
in_stage F,
out_stage G,
pick_sequence H
from location
where current_volume=0
and nvl(alloc_volume,0)=0
and location_id like '&&2%'
and site_id='WORPRI'
order by location_id
/
