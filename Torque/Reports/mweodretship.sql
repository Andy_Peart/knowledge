SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Shipped Date,Order,Order Date,Store,Consignment,Status,Lines Shipped,Units Shipped' from dual
union all
select "Shipped Date" || ',' || "Order" || ',' || "Order Date" || ',' || "Store" || ',' || "Consignment" || ',' || "Status" || ',' || "Lines Shipped" || ',' || "Units Shipped"
from
(
select trunc(oh.shipped_date)                                   as "Shipped Date"
, oh.order_id                                                   as "Order"
, trunc(oh.order_date)                                          as "Order Date"
, oh.work_group                                                 as "Store"
, oh.consignment                                                as "Consignment"
, oh.status          	                                        as "Status"
, sum(case when nvl(ol.qty_shipped,0) > 0 then 1 else 0 end)    as "Lines Shipped"
, sum(nvl(ol.qty_shipped,0))                                    as "Units Shipped"
from order_header oh
inner join order_line ol on ol.client_id = oh.client_id and ol.order_id = oh.order_id
where oh.client_id = 'MOUNTAIN'
and oh.order_type = 'RETAIL1'
and (
		(oh.shipped_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1 and oh.status = 'Shipped') 
		or 
		(oh.last_update_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1 and oh.status = 'Cancelled')
	)
group by oh.shipped_date
, oh.order_id
, oh.order_date
, oh.work_group
, oh.consignment
, oh.status
order by 6 desc,5,4
)
/
--spool off