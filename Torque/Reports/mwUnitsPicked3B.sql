/******************************************************************************/
/*                                                                            */
/*                            Elite                                           */
/*                                                                            */
/*     FILE NAME  :   mwshortages3.sql                                        */
/*                                                                            */
/*   DATE     BY   PROJ     ID         DESCRIPTION                            */
/*   ======== ==== ======   ========   =============                          */
/*   22/01/06 BK   Mountain            Units Picked by Date Range             */
/******************************************************************************/

/* Input Parameters are as follows */
-- 1)  From Date
-- 2)  To Date

SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 120
SET TRIMSPOOL ON

column AA heading 'Date' format A12
column A heading 'User ID' format A16
column X heading 'Zone' format A6
column B heading 'Units' format 999999
column C heading 'Picks' format 999999
column D heading 'Orders' format 999999
column E heading 'Ave Units per Order' format 99990D99


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

--spool totalunitspicked.csv

select 'Units Picked from &&2 to &&3 - for client ID MOUNTAIN' from DUAL;

--break on AA dup skip 2 on A dup skip 1 
--compute sum label 'Total' of B C D E on AA


break on report
compute sum label 'Full Total' of B C on report


select 'Date,User ID,Units,Picks' from DUAL;


select
trunc(it.Dstamp) AA,
it.user_id A,
sum(it.update_qty) B,
count(*) C
--count(distinct reference_id) D,
--sum(it.update_qty)/count(distinct reference_id) E
from inventory_transaction it,order_header oh
where it.client_id='MOUNTAIN'
and it.code='Pick'
and it.to_loc_id in ('DESPATCH','STG','UKPAR','UKPAK')
--and it.to_loc_id in ('DESPATCH','MAIL ORDER')
and it.update_qty<>0
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.reference_id=oh.order_id
and oh.client_id='MOUNTAIN'
group by
trunc(it.Dstamp),
it.user_id
order by
AA,A
/

--spool off
