--The customer has requested a report to show the number of transfers from 
--Retail (TR) and Mail Order (T). They also want to know how many units are 
--being shipped for the Wholesale side of the business. 
--This shouldn't be too tricky as when a transfer is created, it is created 
--in the format of an order to T760. When a Wholesale order is created it is 
--to T801. Therefore, working on a date range & store (T number)  parameter, 
--the report would show:
 
--Store Number (T Number)
--Store Name
--Order Number
--Units Shipped
--Date Shipped
 
--Both of these are to be accessible to KEYUSER and TMLMANAGER user groups.

SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240


column AA heading 'Client ID' format A6
column A heading 'Store' format A6
column B heading 'Name' format A20
column C heading 'Order ID' format A14
column D heading 'Units' format 999999
column E heading 'Date ' format A14

select 'Client ID,Store,Name,Order ID,Units,Date' from DUAL;

select
oh.client_id AA,
oh.customer_id  A,
a.name B,
oh.order_id C,
nvl(sum(ol.qty_ordered),0) D,
trunc(oh.creation_date) E
from order_header oh, order_line ol,address a
where ((oh.client_id='TR' and oh.customer_id in ('T760','T801'))
or (oh.client_id='T' and oh.customer_id like 'T801'))
and oh.customer_id=a.address_id
and oh.order_id=ol.order_id
and oh.creation_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by
oh.client_id,
oh.customer_id,
a.name,
oh.order_id,
trunc(oh.creation_date)
order by
AA,A,C
/
