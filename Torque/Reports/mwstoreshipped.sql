SET FEEDBACK OFF                
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 400
SET TRIMSPOOL ON


--spool brshipped.csv

select 'Branch,Shipped Qty' from DUAL;

select
work_group||','||
sum(qty_shipped)
from shipping_manifest 
where client_id ='&&4'
and shipped_dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')
and work_group<>'WWW'
group by
work_group
order by
work_group
/
select
'Total'||','||
sum(qty_shipped)
from shipping_manifest 
where client_id ='&&4'
and shipped_dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')
and work_group<>'WWW'
/

--spool off
