SET FEEDBACK OFF
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
set colsep ','
set heading off
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240
SET TRIMSPOOL ON


--spool OrderShorts.csv

select 'Input Id = "&&2"' from DUAL;

select 'Consignment,Order ID,Customer,Notes,Description,Ordered,Available' from dual;


select
oh.consignment ||','||
oh.order_id  ||','||
oh.customer_id  ||','||
sku.user_def_note_2||','||
sku.description  ||','||
nvl(ol.qty_ordered,0) ||','||
nvl(ol.qty_tasked,0) 
from order_header oh,order_line ol,sku
where oh.Client_ID='SH'
and (oh.order_id='&&2' or replace(oh.consignment,' ','')='&&2')
and oh.status in ('Allocated','Released','In Progress','On Hold')
and oh.order_id=ol.order_id
and ol.Client_ID='SH'
and ol.qty_ordered>0
and (nvl(ol.qty_tasked,0)+nvl(ol.qty_picked,0))<ol.qty_ordered
and sku.Client_ID='SH'
and ol.sku_id=sku.sku_id
order by
oh.order_id,
sku.user_def_note_2
/

--spool off

