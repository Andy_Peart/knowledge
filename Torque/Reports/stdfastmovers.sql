SET FEEDBACK OFF             
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 300
SET TRIMSPOOL ON


--set termout off
--spool stdsm.csv

select 'Units shipped within last 90 days' from dual
union all
select '---------------------------------' from dual
union all
select 'SKU, Description, Qty shipped' from dual
union all
select sku_id || ',' || description  || ',' ||  qty
from (
select it.sku_id, sku.description,  sum(it.update_qty) qty
from inventory_transaction it inner join sku on it.sku_id = sku.sku_id
where it.client_id = '&2'
and sku.client_id = '&2'
and it.code = 'Shipment'
and trunc(it.dstamp) > trunc(sysdate)-90
group by it.sku_id, sku.description
having sum(it.update_qty)>=10
order by 3 desc
)
/

--spool off
--set termout on
