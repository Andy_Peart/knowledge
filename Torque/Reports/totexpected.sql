SET FEEDBACK OFF                 
set pagesize 68
set linesize 160
set verify off

clear columns
clear breaks
clear computes

column D heading 'Due Date' format a16
column A heading 'Advice ID' format a16
column B heading 'Ordered' format 999999
column C heading 'Received' format 999999
column M format A4 noprint

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'Total Units Expected from &&2 to &&3 - for client ID PRI as at ' report_date skip 2

break on report

compute sum label 'Totals' of B C on report

select
to_char(due_Dstamp, 'YYMM') M,
to_char(due_dstamp,'DD/MM/YY') D,
ph.pre_advice_id A,
sum(pl.qty_due) B,
sum(nvl(pl.qty_received,0)) C
from pre_advice_header ph,pre_advice_line pl
where ph.Due_Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and ph.client_id='PRI'
and ph.pre_advice_id=pl.pre_advice_id
group by
to_char(due_Dstamp, 'YYMM'),
to_char(due_dstamp,'DD/MM/YY'),
ph.pre_advice_id
order by
to_char(due_Dstamp, 'YYMM'),
to_char(due_dstamp,'DD/MM/YY'),
ph.pre_advice_id
/
