SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 52

 

select 'Pick ID,Docket,PCS,Despatched Date,Status,Division,' from DUAL;

select
distinct *
from (select
OO||','||
i.sku_id||','||
sku.user_def_type_1||','||
to_char(DD,'dd-Mon-yy')||','||
TT||','||
i.user_def_note_1||','||
' '
from inventory i,sku,(select
oh.order_id OO,
sku_id SS,
oh.order_date DD,
oh.status TT
from order_line ol,order_header oh
where ol.client_id='PR'
and ol.order_id=oh.order_id
and oh.client_id='PR'
and oh.order_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&2', 'DD-MON-YYYY')+1)
where i.client_id='PR'
and i.site_id='PROM'
and i.sku_id=sku.sku_id
and sku.client_id='PR'
and i.sku_id=SS (+)
order by
OO,
i.sku_id)
/
