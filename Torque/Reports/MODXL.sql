SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 90


column A1 heading 'Orders' format 9999999
column A2 heading 'Containers' format 9999999
column B1 heading 'Units' format 9999999
column D heading 'Method' format A12


select 'Client &&2 Breakdown of Mail Order Despatch Methods from &&3 to &&4' FROM duaL;

select 'Method,Orders,Containers,Units,,' from DUAL;

break on report

compute sum label 'Totals' of A1 A2 B1 on report

select
'OCS' D,
count(distinct it.reference_id) A1,
count(distinct it.reference_id||it.container_id) A2,
nvl(sum(it.update_qty),0) B1
from inventory_transaction it
where it.client_id='&&2'
and it.code='Pick'
and to_loc_id='CONTAINER'
and it.Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
--and (it.container_id like '55%' or it.container_id like '50%')
and it.container_id like '55%'
and it.reference_id not like 'NP%'
union
select
'RM Next Day' D,
count(distinct it.reference_id) A1,
count(distinct it.reference_id||it.container_id) A2,
nvl(sum(it.update_qty),0) B1
from inventory_transaction it
where it.client_id='&&2'
and it.code='Pick'
and to_loc_id='CONTAINER'
and it.Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
--and (it.container_id like 'Z%' or it.container_id like 'J%')
and it.container_id like 'ZJ%'
and it.reference_id not like 'NP%'
union
select
'RM Normal' D,
count(distinct it.reference_id) A1,
count(distinct it.reference_id||it.container_id) A2,
nvl(sum(it.update_qty),0) B1
from inventory_transaction it
where it.client_id='&&2'
and it.code='Pick'
and to_loc_id='CONTAINER'
and it.Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
--and (it.container_id like 'B%' or it.container_id like 'R%')
and it.container_id like 'BR%'
and it.reference_id not like 'NP%'
union
select
'Airmail' D,
count(distinct it.reference_id) A1,
count(distinct it.reference_id||it.container_id) A2,
nvl(sum(it.update_qty),0) B1
from inventory_transaction it
where it.client_id='&&2'
and it.code='Pick'
and to_loc_id='CONTAINER'
and it.Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
--and it.container_id not like '55%'
--and it.container_id not like '50%'
--and it.container_id not like 'Z%'
--and it.container_id not like 'J%'
--and it.container_id not like 'B%'
--and it.container_id not like 'R%'
and it.container_id like 'BFPO%'
and it.reference_id not like 'NP%'
union
select
'Packet Post' D,
count(distinct it.reference_id) A1,
count(distinct it.reference_id||it.container_id) A2,
nvl(sum(it.update_qty),0) B1
from inventory_transaction it
where it.client_id='&&2'
and it.code='Pick'
and to_loc_id='CONTAINER'
and it.Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and it.container_id not like 'ZJ%'
and it.container_id not like 'BR%'
and it.container_id not like '55%'
and it.container_id not like 'BFPO%'
and it.reference_id not like 'NP%'
/
