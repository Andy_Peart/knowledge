SET FEEDBACK OFF                 
set pagesize 68
set linesize 132
set verify off
set wrap off
set newpage 0

clear columns
clear breaks
clear computes


column B heading 'Order Type' format a20
column C heading 'Orders' format 999999
column D heading 'Units' format 999999
column F heading 'Date' format a12

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

--spool utordersbytype.txt

ttitle  LEFT 'Orders by Date and Type from &&2 to &&3 - for client ID UT as at ' report_date skip 2

break on REPORT on F

compute sum label 'Grand Totals' of C D on REPORT
compute sum label 'Day Total' of C D on F 
 


SELECT F, B, C, D FROM (
select
trunc(oh.creation_date) F,
DECODE(oh.order_type,'WEB',DECODE(oh.ship_dock,'UTNET',oh.user_def_type_1,oh.ship_dock),'CONCESSION',DECODE(substr(oh.customer_id,1,2),'RS','CONCESSION RETAIL','OA','CONCESSION OASIS',oh.order_type),oh.order_type) B,
count(distinct oh.order_id) C,
sum(ol.qty_ordered) D
FROM ORDER_HEADER OH,order_line ol
WHERE oh.creation_date BETWEEN TO_DATE('&&2', 'DD-MON-YYYY') AND TO_DATE('&&3', 'DD-MON-YYYY')+1
AND oh.CLIENT_ID='UT'
AND nvl(OH.CUSTOMER_ID,'XX') NOT LIKE 'HF%'
AND ol.CLIENT_ID='UT'
and oh.order_id=ol.order_id
group by
trunc(oh.creation_date),
DECODE(oh.order_type,'WEB',DECODE(oh.ship_dock,'UTNET',oh.user_def_type_1,oh.ship_dock),'CONCESSION',DECODE(substr(oh.customer_id,1,2),'RS','CONCESSION RETAIL','OA','CONCESSION OASIS',oh.order_type),oh.order_type)
union
select
trunc(oh.creation_date) F,
'CONCESSION HOF' B,
count(distinct oh.order_id) C,
sum(ol.qty_ordered) D
FROM ORDER_HEADER OH,order_line ol
WHERE oh.creation_date BETWEEN TO_DATE('&&2', 'DD-MON-YYYY') AND TO_DATE('&&3', 'DD-MON-YYYY')+1
AND oh.CLIENT_ID='UT'
and oh.order_type like 'CONCESSION'
AND nvl(OH.CUSTOMER_ID,'XX') LIKE 'HF%'
AND ol.CLIENT_ID='UT'
and oh.order_id=ol.order_id
group by
trunc(oh.creation_date),
oh.order_type)
ORDER BY F,B
/


--spool off
