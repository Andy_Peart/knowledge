SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
--SET NEWPAGE 0
SET LINESIZE 900
SET TRIMSPOOL ON

--break on report
--compute sum label 'Total' of QTY on report

--spool PRcustomNEW.csv

--select to_char(sysdate,'HH24:MI:SS') from DUAL;

select UPPER('Supplier,Status,Declared,Docket,Channel,Style,Qty,Transaction,Userdef1,Userdef2') from DUAL;

select
A||','||
B||','||
DECODE(B,'BOND',DECODE(G,'WH2FC','Y','WH2EU','Y','N'),'N')||','||
D||','||
E||','||
F||','||
QTY||','||
G||','||
H||','||
D2 from (select
DECODE(
	it.client_id,
		'PR','Y',
		'PS', 'Y',
		DECODE(
			it.supplier_id,
				'PROMIN','Y',
				'N')
			) 
		A,
nvl(it.user_def_type_5,'FREE_CIRC') B,
'N' C,
it.user_def_type_8 D,
--nvl(DOCK,nvl(INV,upper(it.notes))) D,
--nvl(DOCK,nvl(INV,'TAG-'||it.tag_id)) D,
DECODE(it.code,'Shipment',(DECODE(substr(it.reference_id,1,1),'H','ONLINE SALE',
'F',DECODE(substr(it.customer_id,1,2),'WH','TRANSFER TO WH','INTERNATIONAL'),'TRANSFER TO STORE')),'Adjustment','ADJUSTMENT','STOCKCHECK') E,
DECODE(
	it.client_id,
		'PR',sku.user_def_type_5,
		'PS',sku.user_def_type_5,
		sku.user_def_type_2||+'-'||+sku.user_def_type_1||+'-'||+sku.user_def_type_4
	) F,
abs(it.update_qty) QTY,
DECODE(it.code,'Adjustment','WH2FC','Stock Check','WH2FC',CEX) G,
it.code||'_'||it.client_id H,
it.reference_id as D2
from inventory_transaction it,sku,(select
oh.order_id REF,
DECODE(c.CE_EU_TYPE,'EU','WH2EU','EC','WH2EU','UK','WH2FC','WH2EX') CEX
from order_header oh,country c
where oh.client_id in ('PR','PS','T','TR')
and oh.country=c.iso3_id)
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and ((it.client_id in ('T','TR') and it.code='Shipment')
or (it.client_id in ('PR','PS','T','TR') and it.code='Adjustment' and it.update_qty<0 and it.from_loc_id<>'SUSPENSE')
or (it.client_id in ('PR','PS','T','TR') and it.code='Stock Check' and it.update_qty<0))
and sku.client_id=it.client_id
and it.sku_id=sku.sku_id
and ((sku.client_id in ('PS','PR') and sku.user_def_type_2='TML') or sku.client_id in ('T','TR'))
and it.reference_id=REF (+) )
where QTY>0
order by D2,F
/

select
',,,,,TOTAL',
sum(abs(it.update_qty)) QTY
from inventory_transaction it,sku
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and ((it.client_id in ('T','TR') and it.code='Shipment')
or (it.client_id in ('PR','PS','T','TR') and it.code='Adjustment' and it.update_qty<0 and it.from_loc_id<>'SUSPENSE')
or (it.client_id in ('PR','PS','T','TR') and it.code='Stock Check' and it.update_qty<0))
and sku.client_id=it.client_id
and it.sku_id=sku.sku_id
and ((sku.client_id in ('PS','PR') and sku.user_def_type_2='TML') or sku.client_id in ('T','TR'))
/

--select to_char(sysdate,'HH24:MI:SS') from DUAL;


--spool off

