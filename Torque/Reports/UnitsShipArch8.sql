/******************************************************************************/
/*                                                                            */
/*                            Elite                                           */
/*                                                                            */
/*     FILE NAME  :   UnitShipArch8.sql                                       */
/*                                                                            */
/*   DATE     BY   PROJ     ID         DESCRIPTION                            */
/*   ======== ==== ======   ========   =============                          */
/*   09/12/09 RW   MM                  Units Shipped by Date (Date Range)     */
/*                                     from Archive tables (code copied from  */
/*                                     BK job UnitsShipped8.sql)              */
/******************************************************************************/

/* Input Parameters are as follows */
-- 1)  From Date
-- 2)  To Date
-- 3)  Client ID


set feedback off
set pagesize 68
set linesize 240
set newpage 0
set verify off

clear columns
clear breaks
clear computes

column A heading 'Date' format A16
column B heading 'Qty' format 999999
column Z heading 'Order Type' format A20
column R heading 'Stock Type' format A20
column C heading 'Shipments' format 999999
column M format a4 noprint

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'Units Shipped from '&&2' to '&&3' - for client ID &&4 as at ' report_date skip 2

--break on report
--compute sum label 'Totals' of C B on report


select
to_char(itl.Dstamp, 'YYMM') M,
to_char(itl.Dstamp, 'DD/MM/YY') A,
oh.order_type Z,
decode(sku.user_def_type_4,'CATALOGUE','CATALOGUES','STOCK') R,
count(*) C,
sum(itl.update_qty) B
from inventory_transaction_archive itl, order_header_archive oh,sku
where itl.client_id='&&4'
and itl.Reference_id = oh.order_id
and oh.client_id='&&4'
and sku.client_id='&&4'
and itl.sku_id=sku.sku_id
and itl.code='Shipment'
and itl.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by
to_char(itl.Dstamp, 'YYMM'),
to_char(itl.Dstamp, 'DD/MM/YY'),
oh.order_type,
decode(sku.user_def_type_4,'CATALOGUE','CATALOGUES','STOCK')
--order by
--to_char(itl.Dstamp, 'YYMM'),
--to_char(itl.Dstamp, 'DD/MM/YY'),
--oh.order_type,
--decode(sku.user_def_type_4,'CATALOGUE','CATALOGUES','STOCK')
union
select
'ZZZ' M,
'  ' A,
'  ' Z,
'STOCK TOTAL' R,
count(*) C,
sum(itl.update_qty) B
from inventory_transaction_archive itl, order_header_archive oh,sku
where itl.client_id='&&4'
and itl.Reference_id = oh.order_id
and oh.client_id='&&4'
and sku.client_id='&&4'
and itl.sku_id=sku.sku_id
and itl.code='Shipment'
and itl.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
--and sku.user_def_type_4<>'CATALOGUE'
order by M,A,Z,R
/
