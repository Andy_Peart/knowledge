SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep '|' 


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 800
SET TRIMSPOOL ON
 
column XX NOPRINT

--spool ukmail.csv

select
  '9946292' as "home non pod",
  trim(ORDER_ID),
  ORDER_TYPE,
  PRIORITY,
  WORK_GROUP,
  CONSIGNMENT,
  TO_CHAR(sysdate, 'dd-mm-yyyy') as "Collection Date",
  DISPATCH_METHOD,
  NAME,
  CONTACT,
  ADDRESS1,
  ADDRESS2,
  town,
  county,
  postcode,
  country,
  contact_phone,
  SUBSTR(INSTRUCTIONS,1,35),
  '1' as "no of cartons",
  '1' as "Weight",
  '99' as "local prod code",
  '71' as "local service code for 24hrs"
from order_header
where client_id = 'MOUNTAIN'
and order_id = '&2'
and priority <> 1200
order by order_id
/


--spool off


