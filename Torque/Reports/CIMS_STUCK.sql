SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 2000
SET TRIMSPOOL ON
 
column A heading 'Wrk unit Num' format A20
column B format A6
column C format A6
column D format A15
column E format A500
column F format A6
column FF format A6
column G format 999
column GG format 999
column H format 99999
column I format 99999
column J format A50
column k heading 'note' format A500

select 
I.WRK_UNIT_NUM,I.SRC_BIN,I.DEST_BIN, I.SKU_BARCODE,I.TOT_QTY,I.STY_SIZE,I.MAIN_SIZE
,D.SOURCE_NUM, D.DEST_NUM,D.SOURCE_TYPE, D.DEST_TYPE,to_char(F.YAUD_UPTIME,'dd-mon-yyyy HH:MI:SS'),substr(E.CIMS_ERROR_TEXT,1,150)
from 
cdi.cdi_move_items@elite_pro I
inner join
cdi.cdi_move_defs@elite_pro d
on i.wrk_unit_num = d.wrk_unit_num
inner join
cdi.cdi_move_control@elite_pro c
on c.wrk_unit_num = d.wrk_unit_num
LEFT OUTER JOIN
cdi.CDI_MOVE_ERRORS@elite_pro E
ON e.wrk_unit_num = I.wrk_unit_num
AND E.WRK_UNIT_ITEM = I.WRK_UNIT_ITEM
left outer JOIN
cdi.YAUD2CDI_MOVE_DEFS@elite_pro F
ON I.wrk_unit_num = F.wrk_unit_num
where ready <> 'S'
/


