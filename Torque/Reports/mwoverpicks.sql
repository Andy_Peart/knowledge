SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
set linesize 300
set trimspool on


--ttitle 'Mountain Warehouse Overpicks for Period &&2 to &&3' skip 2

column A heading 'User ID' format a16
column B heading 'Original Qty' format 99999999
column C heading 'Update Qty' format 99999999
column D heading 'Excess Qty' format 99999999
column E heading 'SKU ID' format a18
column F heading 'Description' format a40
column G heading 'Location' format a10


break on A dup

compute sum label 'Total' of B C D  on A

--spool mwoverpicks.csv

select 'User ID,Work Group,SKU ID,Description,Location Id,Original Qty,Update Qty,Excess Qty,,' from DUAL;

select
i.user_id A,
i.work_group WG,
''''||i.sku_id||'''' E,
sku.description F,
i.from_loc_id G,
i.original_qty B,
i.update_qty C,
i.update_qty-i.original_qty D,
' '
from inventory_transaction i,sku
where i.client_id = 'MOUNTAIN'
and i.code = 'Pick'
and i.dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')
and i.update_qty>i.original_qty
and sku.client_id = 'MOUNTAIN'
and i.sku_id=sku.sku_id
order by A,WG
/

--spool off

