/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :     tmlintlship.sql                         		      */
/*                                                                            */
/*     DESCRIPTION:                                                           */
/*     Tries to show user what was shipped for uk / intl           */
/*                                                                            */
/*   DATE       BY   DESCRIPTION						                          */
/*   ========== ==== ===========					                              */
/*   04/04/2013 YB   initial version     				                          */
/******************************************************************************/
SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ' '
SET WRAP OFF


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500

/* Column Headings and Formats */
COLUMN CLIENT HEADING "Client" FORMAT A5
COLUMN "FIRST TWO OF CUST ID" HEADING "First Two of Cust ID" FORMAT A10
COLUMN "TOTAL" HEADING "Total" FORMAT 999999

break on report

--compute sum label 'Total' of TOTAL on report

set TERMOUT OFF
COLUMN CURDATE NEW_VALUE REPORT_DATE
select to_char(SYSDATE, 'DD/MM/YYYY  HH24:MI') curdate from DUAL;
SET TERMOUT ON


SELECT 'Date from: &&2 to: &&3' dates from DUAL;

--SPOOL PR2TML_TRANSFER_.CSV

/* Top Title */
select 'Client  Order Type  Shipped Units' from DUAL;



--This is old sql query

--SUBSTR(SHIPPING_MANIFEST.CUSTOMER_ID, 1, 2) AS "FIRST TWO OF CUST ID", 
--SUM(SHIPPING_MANIFEST.QTY_SHIPPED) AS "TOTAL"
--FROM SHIPPING_MANIFEST
--WHERE SHIPPING_MANIFEST.CLIENT_ID IN ('TR')
--AND SHIPPING_MANIFEST.SHIPPED_DSTAMP 
--BETWEEN TO_DATE('&&2 00:00', 'DD-MON-YYYY HH24:MI') 
--AND TO_DATE('&&3 23:59', 'DD-MON-YYYY HH24:MI')
--AND SHIPPING_MANIFEST.CUSTOMER_ID NOT IN ('WH1','WH2') 
--GROUP BY SHIPPING_MANIFEST.CLIENT_ID, SUBSTR(SHIPPING_MANIFEST.CUSTOMER_ID, 1, 2)
--ORDER BY SUBSTR(SHIPPING_MANIFEST.CUSTOMER_ID, 1, 2)

--End of old sql query







select 
sm.client_id as "Client",
decode(substr(sm.customer_id,1,1),'W','International','A','International','[unknown]') as "Order type",  
SUM(sm.QTY_SHIPPED) AS "Units shipped"
from shipping_manifest sm
where ((sm.order_id like 'F%' and customer_id not in ('WH1', 'WH2')) or (sm.customer_id like 'A%') and sm.client_id = 'TR')
and sm.client_id in ('T', 'TR')
AND sm.SHIPPED_DSTAMP 
BETWEEN TO_DATE('&&2 00:00', 'DD-MON-YYYY HH24:MI') 
AND TO_DATE('&&3 23:59', 'DD-MON-YYYY HH24:MI')
group by 
sm.client_id,
decode(substr(sm.customer_id,1,1),'W','International','A','International','[unknown]')
/




