SET FEEDBACK OFF                 
set verify off
set wrap off
set colsep ','

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 300
set trimspool on

column CID format A10
column CDT format A10
column CD format A14
column customer_id format A10
column RID format A20
column sku_id format A50
column Q1 format 999999
column Q2 format 999999
column Q3 format 999999
column EAN format A15



--spool stdshortage.csv

break on CDT dup skip 1 on CDT dup skip 1
compute sum label 'Totals' of Q1 Q2 Q3 on CDT 



Select 'Client,Creation Date,Condition,Customer Id,Order Id,SkuId,Ordered Qty,Allocated,Shortage,EAN,OL_UDType1' from DUAL;

select mn.CID, mn.CDT, DECODE(mn.CD,'3','Retail','203','Outlet','299','Pallet Outlet',mn.CD) CD, mn.customer_id, mn.RID, mn.sku_id,mn.Q1, mn.Q1-mn.Q2 Q3,mn.Q2, sku.ean, UDType1
from (
	select 	oh.client_id CID, 	oh.order_id RID, 	oh.customer_id, 	trunc(oh.creation_date) CDT,
		ol.condition_id CD, 	replace(ol.sku_id,',','') sku_id, 	sum(ol.qty_ordered) Q1, 	nvl(QQQ,0) Q2, ol.user_def_type_1 UDType1
	from order_header oh,order_line ol,(
		select task_id TK, sku_id TK1, sum(qty_ordered-qty_tasked) QQQ
		from generation_shortage
		where client_id='&&2'
		group by task_id, sku_id
	)
	where oh.client_id='&&2'
	and oh.creation_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
	and ol.order_id=oh.order_id
	and ol.client_id=oh.client_id
	and (ol.order_id = TK (+) and ol.sku_id = TK1 (+))
	group by oh.client_id, oh.order_id, oh.customer_id, ol.sku_id, trunc(oh.creation_date), ol.condition_id, nvl(QQQ,0), ol.user_def_type_1
) Mn  inner join sku on mn.sku_id = sku.sku_id and mn.cid = sku.client_id
order by mn.CDT,mn.CD,mn.RID
/

--spool off


