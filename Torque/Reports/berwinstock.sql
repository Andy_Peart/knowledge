SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 50
SET NEWPAGE 1
SET LINESIZE 200
SET TRIMSPOOL ON
 

column A heading 'SKU' format A18
column B heading 'Type' format A12
column C heading 'Qty' format 9999999
column D heading 'Bars Used' format 99999


break on report
compute sum label 'Total' of C D on report

TTITLE LEFT 'Total Stock Holding for BERWIN ' skip 2

--select 'Item,Qty,Bars,' from DUAL;

select
B,
C,
DECODE(B,'TROUSERS',C/100,C/35) D
from (select
DECODE(sku.description,'JACKETS','JACKETS','JACKET','JACKETS',
'WAISTCOAT','WAISTCOATS','WAISTCOATS','WAISTCOATS',
'SUIT','SUITS','SUITS','SUITS',sku.description) B,
sum(ii.qty_on_hand) C,
' '
from inventory ii,sku
where ii.client_id='BW'
and sku.client_id='BW'
and ii.sku_id=sku.sku_id
group by
DECODE(sku.description,'JACKETS','JACKETS','JACKET','JACKETS',
'WAISTCOAT','WAISTCOATS','WAISTCOATS','WAISTCOATS',
'SUIT','SUITS','SUITS','SUITS',sku.description) )
order by
B
/

--spool off
