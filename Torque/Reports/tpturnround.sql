SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 100
SET TRIMSPOOL ON

--break on D3 dup skip 1 on report

--compute sum label 'Sub Total' of B C K on A2

--spool tptr.csv

select 'TP TOTALS,&&2,to,&&3' from DUAL;

select
D3,
count(*)
from (select
A1,
CASE
WHEN (D1>D2) THEN 0
ELSE (D2-D1) END D3
from (select
order_id  A1,
DECODE (to_char(creation_date,'DY'),'FRI',trunc(creation_date)+2,
        'SAT',trunc(creation_date)+2,
        'SUN',trunc(creation_date)+1,trunc(creation_date)) D1,
trunc(shipped_date) D2
from order_header
where client_id = 'TP'
and shipped_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1))
group by D3
order by
D3
/

select '' from DUAL;
select '' from DUAL;

select 'Order Id,Turn Round' from DUAL;

select
A1,
CASE
WHEN (D1>D2) THEN 0
ELSE (D2-D1) END D3
from (select
order_id  A1,
DECODE (to_char(creation_date,'DY'),'FRI',trunc(creation_date)+2,
        'SAT',trunc(creation_date)+2,
        'SUN',trunc(creation_date)+1,trunc(creation_date)) D1,
trunc(shipped_date) D2
from order_header
where client_id = 'TP'
and shipped_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1)
order by
D3,A1
/

--spool off
