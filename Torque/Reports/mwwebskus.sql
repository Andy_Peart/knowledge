SET FEEDBACK OFF
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
set colsep ','
set heading off
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 300
set trimspool on

column A heading 'EXTBARCODE' format a20
column B heading 'QTY' format 9999999
column C heading 'QTY' format 9999999

select 'EXTBARCODE,STOCK,RESERVE'
from dual;

select 
A,
B,
C
from (select i.sku_id A, sum(i.qty_on_hand) B,
nvl(s.user_def_num_3,0) C
from inventory i, sku s,location ll
where i.client_id = 'MOUNTAIN'
and i.location_id=ll.location_id
and ll.loc_type='Tag-FIFO'
and ll.site_id='BD2'
and i.sku_id <> 'TEST'
and i.sku_id = s.sku_id
and s.client_id = 'MOUNTAIN'
group by s.user_def_num_3, i.sku_id)
where B-C <= 0
order by A
/
