SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240
SET TRIMSPOOL ON


column D heading 'Date' format a16
column A heading 'Reference' format A20
column B heading 'Qty' format 999999
column C heading 'Condition ID' format a10
column M noprint

set TERMOUT OFF
column curdate NEW_VALUE report_date
set TERMOUT ON

set heading on

break on D skip 1 dup on B dup on report

compute sum label 'Type Total' of Q on B
compute sum label 'Day Total' of Q on D
compute sum label 'Total' of Q on report

--spool ccunitspicked.csv

select CONCAT('Report Date and Time : ',to_char(SYSDATE, 'DD/MM/YY  HH24:MI')) from DUAL;
select '' from DUAL;

select 'Units Picked from &&2 to &&3 - for Crew Clothing ' from DUAL;
select '' from Dual;

select 'Date,Condition ID,Reference, Qty Picked' from DUAL;

select
trunc(it.Dstamp) D,
odh.work_group B,
it.reference_id A,
sum(it.update_qty) Q
from inventory_transaction it
inner join order_header odh
on it.client_id = odh.client_id
and it.reference_id=odh.order_id
where it.client_id='CC'
and it.code='Pick'
and it.update_qty>0
and it.elapsed_time>0
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by
trunc(it.Dstamp),
odh.work_group,
it.reference_id
order by D,B
/
--spool off
