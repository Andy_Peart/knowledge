SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET COLSEP ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET LINESIZE 200
SET HEADING OFF 
SET PAGESIZE 0
SET TRIMSPOOL ON


column A heading 'Category' format A28
column B heading 'Qty' format 9999999

select 'Prominent Stock Holding' from DUAL;
select to_char(SYSDATE, 'DD/MM/YY    HH:MI:SS') from DUAL;

select '' from DUAL;
select 'ALL Items' from DUAL;
select 'Category,Qty' from DUAL;





select
'LSP1 Total' A,
count(distinct i.location_id) B
from inventory i
where i.client_id like 'PR'
and i.site_id like 'PROM'
and i.zone_1 like 'LSP1'
/


select
'LS1  Total' A,
count(distinct i.location_id) B
from inventory i
where i.client_id like 'PR'
and i.site_id like 'PROM'
and i.zone_1 like 'LS1'
/


select
'LSS1 Total' A,
count(distinct i.location_id) B
from inventory i
where i.client_id like 'PR'
and i.site_id like 'PROM'
and i.zone_1 like 'LSS1'
/


select
'LSH1 Total NOT ratio packs' A,
nvl(sum(i.qty_on_hand),0) B
from inventory i,sku
where i.client_id like 'PR'
and i.site_id like 'PROM'
and i.zone_1 like 'LSH1'
and location_id != 'REJECT'
and sku.client_id=i.client_id
and sku.sku_id=i.sku_id
and (sku.user_def_type_9 is null or sku.user_def_type_9=1)
/


select
'LSH1 Total Ratio Packs' A,
nvl(sum(i.qty_on_hand*sku.user_def_type_9),0) B
from inventory i,sku
where i.client_id like 'PR'
and i.site_id like 'PROM'
and i.zone_1 like 'LSH1'
and sku.client_id=i.client_id
and sku.sku_id=i.sku_id
and sku.user_def_type_9 is not null
and sku.user_def_type_9>1 
/


