SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 2000
SET TRIMSPOOL ON

WITH T1 AS (
     select inv.client_id, inv.sku_id, SKU.DESCRIPTION, SKU.USER_DEF_TYPE_4, SKU.SKU_SIZE, sum(inv.QTY_ON_HAND) AS "QTY_ON_HAND", sum(inv.QTY_ALLOCATED) AS "QTY_ALLOCATED"
     from inventory inv left join sku on inv.sku_id=sku.sku_id and inv.client_id=sku.client_id
     where inv.client_id = 'WF'
     group by inv.client_id, inv.sku_id, SKU.DESCRIPTION, SKU.USER_DEF_TYPE_4, SKU.SKU_SIZE
     order by inv.SKU_ID
),
T2 AS (
     SELECT CLIENT_ID, SKU_ID, SUM(INV.QTY_ON_HAND) AS QTY_ON_HAND
     FROM INVENTORY INV
     WHERE CLIENT_ID = 'WF'
     AND INV.LOCATION_ID = 'SUSPENSE'
     GROUP BY CLIENT_ID, SKU_ID
),
T3 AS (
     SELECT INV.CLIENT_ID, INV.SKU_ID, SUM(INV.QTY_ON_HAND) AS "INTAKE"
     from inventory inv 
     inner join location loc on inv.location_id = loc.location_id and inv.site_id = loc.site_id
     where inv.client_id = 'WF'
     and loc.loc_type = 'Receive Dock'
     GROUP BY INV.CLIENT_ID, INV.SKU_ID
),
T4 AS (
     SELECT CLIENT_ID, SKU_ID, SUM(INV.QTY_ON_HAND) AS PICKED
     FROM INVENTORY INV INNER JOIN location LOC ON INV.LOCATION_ID=LOC.LOCATION_ID AND INV.SITE_ID=LOC.SITE_ID
     WHERE CLIENT_ID = 'WF'
     AND LOC.LOC_TYPE IN ('Marshalling','ShipDock','Stage')
     GROUP BY CLIENT_ID, SKU_ID
),
T5 AS (
     select inv.client_id, inv.sku_id,  SUM(inv.QTY_ON_HAND) AS "LOCKED"
     from inventory inv 
     WHERE INV.CLIENT_ID = 'WF'
     AND INV.LOCK_STATUS = 'Locked'
     group by inv.client_id, inv.sku_id
),
T6 AS (
     select inv.client_id, inv.sku_id,  sum(inv.QTY_ON_HAND)-sum(inv.QTY_ALLOCATED) AS "AVAILABLE"
     from inventory inv INNER JOIN location LOC ON INV.LOCATION_ID=LOC.LOCATION_ID AND INV.SITE_ID=LOC.SITE_ID
     WHERE CLIENT_ID = 'WF'
     AND LOC.LOC_TYPE NOT IN ('Marshalling','ShipDock','Suspense','Stage','Sampling')
     AND INV.LOCK_STATUS = 'UnLocked'
     group by inv.client_id, inv.sku_id
),
T7 AS (
     SELECT INV.CLIENT_ID, INV.SKU_ID, SUM(INV.QTY_ON_HAND) AS SAMPLING
     from inventory inv 
     inner join location loc on inv.location_id = loc.location_id and inv.site_id = loc.site_id
     where inv.client_id = 'WF'
     and loc.loc_type = 'Sampling'
    group by inv.client_id, inv.sku_id
)
select 'SKU,Description,Variant ID,Size,Client,Qty on hand,Qty Allocated,Suspense,Intake,Picked,Sampling,Locked,Available' A1 from dual
union all
SELECT  T1.sku_id || ',' || T1.DESCRIPTION || ',' || T1.USER_DEF_TYPE_4 || ',' ||  T1.SKU_SIZE || ',' || 'WF,' || T1.QTY_ON_HAND || ',' || T1.QTY_ALLOCATED || ',' || 
NVL(T2.QTY_ON_HAND,0) || ',' || nvl(T3.INTAKE,0)  || ',' || nvl(T4.PICKED,0)  || ',' || nvl(T7.SAMPLING,0)  || ',' || nvl(T5."LOCKED",0)  || ',' || nvl(T6.AVAILABLE,0)
FROM T1 
  LEFT JOIN T2
    ON T1.SKU_ID=T2.SKU_ID 
  LEFT JOIN T3
    ON T1.SKU_ID=T3.SKU_ID 
  LEFT JOIN T4
    ON T1.SKU_ID=T4.SKU_ID 
  LEFT JOIN T5
    ON T1.SKU_ID=T5.SKU_ID
  LEFT JOIN T7
    ON T1.SKU_ID=T7.SKU_ID    
  left join t6
    ON T1.SKU_ID=T6.SKU_ID
/