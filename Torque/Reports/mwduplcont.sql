
SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 66
SET NEWPAGE 0
SET LINESIZE 240

column A heading 'STORE ID' format A12
column AA heading 'STORE ID' format A12
column B heading 'CONTAINER' format A14
column C heading 'DUPLICATE COUNT' format 999
--column D heading 'ORDER ID' format A14
--column E heading 'PICK DATE' format A14



select
  oh.work_group A
  , sh.work_group AA
  , sh.container_id B
  , count (distinct oh.order_id) C
from order_header oh
left join shipping_manifest sh on oh.order_id =sh.order_id
  where oh.client_id ='MOUNTAIN'
  and sh.client_id ='MOUNTAIN'
  and oh.order_id like 'MOR%'
  and oh.from_site_id = 'BD2'
  and oh.creation_date > trunc(sysdate) - 15
  and sh.work_group <>'WWW'
  and sh.picked_dstamp between trunc(sysdate)and trunc(sysdate+1) --todays
group by  sh.container_id,oh.work_group, sh.work_group
having  count (distinct oh.order_id) > 1
/