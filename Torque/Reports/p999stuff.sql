set pagesize 66
set linesize 120
set verify off
set newpage 0
set feedback off

ttitle 'MOUNTAIN Warehouse P999 Allocations from &&2 to &&3' skip 2

column A heading 'Date' format a12
column B heading 'Order ID' format a14
column C heading 'SKU' format a18
column D heading 'Update Qty' format 99999
column M format a4 noprint


break on A skip 2 on B skip 1 nodup on report

compute sum label 'Total' of D on report


select 
to_char(dstamp, 'DD/MM/YY') A,
--to_char(Dstamp, 'YYMM') M,
Reference_id B,
sku_id C,
update_qty D
from inventory_transaction
where client_id='MOUNTAIN'
and code='Allocate'
and from_loc_id='P999'
and Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
order by
Reference_id,
sku_id
/
