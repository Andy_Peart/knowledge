SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Client,Country,Order #,Shipped Date,Dispatch Method,Shipdock,Sku ID,Size,Style,Colour,Returned Date,Reason ID,Reason Desc.,% of Order Returned,Item Returned?,Product Group,Good/Bad,PO Number,Original Pick Location,Location Zone' from dual
union all
/* TORQUE - (STD) - Web Returns with Order Detail */
select "Cli"||','|| "Country"||','|| "Ord"||','|| "Date"||','|| "Dispatch Method"||','|| "Shipdock"||','|| "Sku"||','|| "Size"||','|| "Style"||','|| "Colour"||','|| "Returned Date"||','|| "Return ID"||','|| "Return Reason"||','|| ("Qty Returned"/ "Qty Shipped")*100 || '%'||','||"Returned?"
||','|| "PG"||','|| "ToLoc"||','|| "PO Num"||','|| from_loc_id||','|| zone_1
from
(
select "Cli", "Country", "Ord", "Date", "Dispatch Method", "Shipdock", "Sku", "Size", "Style", "Colour", "Returned Date", "Return ID", "Return Reason", "PG", "ToLoc"
, "Count",  "Qty Shipped", "Qty Returned", "Returned?", "PO Num", from_loc_id, zone_1
from
(
select "Cli", "Country", "Ord", "Date", "Dispatch Method", "Shipdock", "Sku", "Size", "Style", "Colour", "Returned Date", "Return ID", "Return Reason", "PG", "ToLoc"
, sum("Count")over(partition by "Ord" order by "Ord") "Count", sum(qty_shipped)over(partition by "Ord" order by "Ord") "Qty Shipped", sum(qty_returned)over(partition by "Ord" order by "Ord") "Qty Returned", "Returned?", "PO Num"
from
(
with t1 as (
    select it.client_id    as "Cli"
    , it.reference_id      as "Ord"
    , it.sku_id            as "Sku"
    , it.update_qty        as "Qty"
    , trunc(it.dstamp)     as "Date"
    , it.reason_id         as "RRC"
    , rr.notes             as "Desc"
    , it.to_loc_id         as "ToLoc"
    , 'o'                  as "Ret"
    , it.line_id           as "Line"
    from inventory_transaction it
    left join return_reason rr on rr.reason_id = it.reason_id
    where it.client_id = '&&2'
    and it.code = 'Return'
    and it.dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
    union all
        select it.client_id    as "Cli"
    , it.reference_id      as "Ord"
    , it.sku_id            as "Sku"
    , it.update_qty        as "Qty"
    , trunc(it.dstamp)     as "Date"
    , it.reason_id         as "RRC"
    , rr.notes             as "Desc"
    , it.to_loc_id         as "ToLoc"
    , 'o'                  as "Ret"
    , it.line_id           as "Line"
    from inventory_transaction_archive it
    left join return_reason rr on rr.reason_id = it.reason_id
    where it.client_id = '&&2'
    and it.code = 'Return'
    and it.dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
)
select oh.client_id             as "Cli"
, oh.country                    as "Country"
, oh.order_id                   as "Ord"
, trunc(oh.shipped_date)        as "Date"
, oh.dispatch_method            as "Dispatch Method"
, oh.ship_dock                  as "Shipdock"
, ol.sku_id                     as "Sku"
, s.sku_size                    as "Size"
, s.style                       as "Style"
, s.colour                      as "Colour"
, t1."Date"      as "Returned Date"
, t1."RRC"        as "Return ID"
, t1."Desc"      as "Return Reason"
, s.product_group               as "PG"
, case  when t1."ToLoc" like '%BAD%' then 'Bad' 
        when t1."ToLoc" like '%EX%' then 'Exchange' 
        when t1."ToLoc" like '%GOOD%' then 'Good'
    else t1."ToLoc" end                                                         as "ToLoc"
, case when t1."RRC" is null then 0 else 1 end                                    as "Count"
, T1."RRC"
, ol.qty_shipped                                                                   
, ol.qty_returned                    
, case when nvl(t1."Ret",'x') = 'o' then 'Returned' else 'Not Ret' end            as "Returned?"
, oh.purchase_order as "PO Num"
from order_header oh
inner join order_line ol on ol.client_id = oh.client_id and ol.order_id = oh.order_id
inner join sku s on s.sku_id = ol.sku_id and s.client_id = oh.client_id
left join t1 on t1."Cli" = oh.client_id and t1."Ord" = oh.order_id and t1."Sku" = ol.sku_id and t1."Line" = ol.line_id
where oh.client_id = '&&2'
)
group by "Cli", "Country", "Ord", "Date", "Dispatch Method", "Shipdock", "Sku", "Size", "Style", "Colour", "Returned Date", "Return ID", "Return Reason", "PG", "ToLoc"
, "Count", qty_shipped, qty_returned, "Returned?", "PO Num"
)
left join (
    select it.reference_id
    , it.sku_id
    , it.from_loc_id
    , l.zone_1
    from inventory_transaction it
    inner join location l on l.site_id = it.site_id and l.location_id = it.from_loc_id
    where it.client_id = '&&2'
    and it.code = 'Pick'
    and l.loc_type in ('Tag-LIFO','Tag-FIFO')
    and it.update_qty <> 0
    union all
    select it.reference_id
    , it.sku_id
    , it.from_loc_id
    , l.zone_1
    from inventory_transaction_archive it
    inner join location l on l.site_id = it.site_id and l.location_id = it.from_loc_id
    where it.client_id = '&&2'
    and it.code = 'Pick'
    and l.loc_type in ('Tag-LIFO','Tag-FIFO')
    and it.update_qty <> 0
) x on x.reference_id = "Ord" and x.sku_id = "Sku"
where "Count" <> 0
order by "Ord", "Returned Date", "Returned?"
)
/
--spool off