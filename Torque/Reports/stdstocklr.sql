SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON
 

column A heading 'Location' format A11
column B heading 'SKU' format A20
column C heading 'Description' format A250
column D heading 'Qty' format 9999999
column E heading '' format A14

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

break on report

compute sum label 'Total' of D on report

select 'SKU,Description,Size,Colour,Location,Qty,' from DUAL;

select
i.sku_id B,
Sku.description C,
sku_size C1,
colour C2,
i.location_id A,
sum(i.qty_on_hand) D,
' '
from inventory i,sku
where i.client_id like '&&4'
and location_id>='&&2'
and location_id<='&&3'
and i.sku_id=sku.sku_id
group by
i.sku_id,
Sku.description,
sku_size,
colour,
i.location_id
order by 
i.sku_id,
Sku.description,
sku_size,
colour,
i.location_id
/
