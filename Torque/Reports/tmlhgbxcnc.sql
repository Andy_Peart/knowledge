SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET TRIMSPOOL ON

select 'Date,Store,Stock type,Units shipped' as A1 from dual
union all
select shipped_date || ',' || customer_id || ',' || stock_type || ',' || shipped
from (
select to_char(oh.shipped_date,'DD-MM-YYYY') shipped_date, oh.customer_id, 
case 
    when sku.product_group in ('WC','WW','WD','WO','MZ','WB','WM','WJ','WP','MC','MW','MP','MJ') then 'Hang'
    else 'Box'
end stock_type,
sum(ol.qty_shipped) shipped
from order_header oh inner join order_line ol on oh.order_id = ol.order_id and oh.client_id = ol.client_id
inner join sku on sku.client_id = oh.client_id and sku.sku_id = ol.sku_id
where oh.client_id = 'TR'
and (oh.customer_id like 'T2__' or oh.customer_id like 'T3__' or oh.customer_id like 'H___')
and oh.shipped_date between to_date('&&2') and to_date('&&3')+1
group by to_char(oh.shipped_date,'DD-MM-YYYY'), oh.customer_id,
case 
    when sku.product_group in ('WC','WW','WD','WO','MZ','WB','WM','WJ','WP','MC','MW','MP','MJ') then 'Hang'
    else 'Box'
end 
union all
select to_char(oh.shipped_date,'DD-MM-YYYY') shipped_date, oh.user_def_type_5, 'CnC' as stock_type, sum(ol.qty_shipped) shipped
from order_header oh inner join order_line ol on oh.order_id = ol.order_id and oh.client_id = ol.client_id
inner join sku on sku.client_id = oh.client_id and sku.sku_id = ol.sku_id
where oh.client_id = 'TR'
and (oh.user_def_type_5 like 'T2__' or oh.user_def_type_5 like 'T3__' or oh.user_def_type_5 like 'H___')
and oh.dispatch_method = '1'
and oh.shipped_date between to_date('&&2') and to_date('&&3')+1
group by to_char(oh.shipped_date,'DD-MM-YYYY'), oh.user_def_type_5
order by 1,2,3
)
/