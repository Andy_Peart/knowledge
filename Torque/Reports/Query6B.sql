SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 480




Select 'Code,Client ID,SKU,From Location,Original Qty,Reference,To Location,Update Qty,Final Location,Transaction Date,Transaction Time,Pick Label ID,Notes,Container,Pallet,Consignment,Supplier,User ID,Workstation,Customer'
from dual;

 
select 
it.code ||','||
it.client_id ||','''||
it.sku_id ||''','||
it.from_loc_id  ||','||
it.original_qty ||','||
it.reference_id ||','||
it.to_loc_id  ||','||
it.update_qty ||','||
it.final_loc_id ||','||
to_char(it.Dstamp, 'DD/MM/YYYY') ||','||
to_char(it.Dstamp, 'HH:MI:SS') ||','||
it.Print_Label_ID ||','||
it.notes ||','||
it.container_id ||','||
it.Pallet_id ||','||
it.consignment ||','||
it.supplier_id ||','||
it.user_id ||','||
it.station_id ||','||
it.customer_id
FROM INVENTORY_TRANSACTION IT
WHERE IT.CLIENT_ID='&&3'
AND IT.CODE IN ('Picked','Shipment')
and it.dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&2', 'DD-MON-YYYY')+1
order by
it.code,
it.Dstamp,
it.sku_id 
/
