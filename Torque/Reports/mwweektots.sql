SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET HEAD ON
SET colsep ','

clear columns
clear breaks
clear computes

SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 68
set verify off

--ttitle 'Mountain Warehouse Totals for Period &&2 to &&3' skip 2

column A heading 'Week Commencing' format a16
column AA heading 'Type' format a24

column B heading 'Quantity' format 99999999
column BB heading 'Lines' format 99999999

break on A skip 1

--compute sum label 'Total' of B on report

--set termout off
--spool WeekTots.csv

select 'Mountain Warehouse Totals for Period &&2 to &&3' from DUAL;
select 'Week Commencing,Type,Number,Units,,' from DUAL;

select
to_date('&&2', 'DD/MM/YY')+(trunc((trunc(Dstamp)-to_date('&&2', 'DD/MM/YY'))/7)*7) A,
'All Receipts' AA,
count(*) BB,
sum (update_qty) B
from inventory_transaction
where client_id = 'MOUNTAIN'
and station_id not like 'Auto%'
and code = 'Receipt'
and dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')
group by
to_date('&&2', 'DD/MM/YY')+(trunc((trunc(Dstamp)-to_date('&&2', 'DD/MM/YY'))/7)*7) 
union
select
to_date('&&2', 'DD/MM/YY')+(trunc((trunc(Dstamp)-to_date('&&2', 'DD/MM/YY'))/7)*7) A,
decode(work_group,'WWW','Mail Order Shipments','Retail Shipments') AA,
count(*) BB,
sum (update_qty) B
from inventory_transaction
where client_id = 'MOUNTAIN'
and code = 'Shipment'
and dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')
group by
to_date('&&2', 'DD/MM/YY')+(trunc((trunc(Dstamp)-to_date('&&2', 'DD/MM/YY'))/7)*7),
decode(work_group,'WWW','Mail Order Shipments','Retail Shipments')
order by A,AA
/

set newpage NONE
set head off


select
'All Weeks' A,
'All Receipts' AA,
count(*) BB,
sum (update_qty) B
from inventory_transaction
where client_id = 'MOUNTAIN'
and station_id not like 'Auto%'
and code = 'Receipt'
and dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')
union
select
'All Weeks' A,
decode(work_group,'WWW','Mail Order Shipments','Retail Shipments') AA,
count(*) BB,
sum (update_qty) B
from inventory_transaction
where client_id = 'MOUNTAIN'
and code = 'Shipment'
and dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')
group by
decode(work_group,'WWW','Mail Order Shipments','Retail Shipments')
order by A,AA
/

--spool off
--set termout on
