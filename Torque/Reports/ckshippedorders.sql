SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 160
SET TRIMSPOOL ON

--ttitle 'CURVY KATE Despatch Quantities from &&2 to &&3' skip 2

column C heading 'Units' format 99999999


break on report

compute sum label 'Totals' of C on report

--spool ckorders.csv

select 'CK orders from &&2 to &&3' from DUAL;
select 'Order Id,Customer,Tracking Number,Units Shipped' from DUAL;

select
A||','||
B||','||
B2||','||
C
from (select
oh.order_id A,
customer_id B,
instructions B2,
sum(ol.qty_shipped) C
from order_header oh,order_line ol
where oh.client_id='CK'
and oh.status='Shipped'
and ol.client_id='CK'
and oh.order_id=ol.order_id
and oh.order_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
--and oh.order_date>sysdate-10
group by
oh.order_id,
customer_id,
instructions)
order by
A
/

--spool off
