SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

column A heading 'Store' format A6
column B heading 'Name' format A30
column C heading 'List ID' format A12
column CC heading 'Order No' format A20
column D heading 'Lines' format 99999999
column E heading 'To Pick' format 99999999
column EE heading 'Picked' format 99999999
column F heading 'Consignment' format A14
column M format a4 noprint



--select 'Store,Name,List No,Order No,Lines,To Pick,Consignment,Total Cartons,Shipped,Delivery Note Created,Checked Inventory,Order Header Updated,Labels Attached,Picker,Time Out,Time In,Hours,Pick Rate' from DUAL;
select 'Store,Name,List No,Order No,Picked,Consignment,Move Task,HANG,RET' from DUAL;
 
set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DAY, DD MONTH, YYYY') curdate from DUAL;
set TERMOUT ON


--break on report

--compute sum of D E EE on report


select
oh.customer_id A,
a.name B,
LLL C,
oh.order_id CC,
--sum(OO) D,
sum(QQQ) E,
--sum(PP) EE,
oh.consignment F,
substr(oh.consignment,7,4) M
from order_header oh,address a,
(select
reference_id OOO,
nvl(list_id,'  ') LLL,
sum(update_qty) QQQ
from inventory_transaction
where client_id='TR'
and code='Pick'
and consignment like '&&2%'
and substr(consignment,7,1)='&&3'
--and reference_id='28539'
and from_loc_id<>'CONTAINER'
group by
reference_id,
list_id)
where oh.order_id=OOO
and oh.status not in ('Cancelled','Hold','Shipped')
and oh.consignment like '&&2%'
and substr(oh.consignment,7,1)='&&3'
and oh.client_id='TR'
and a.client_id='TR'
--and LLL is not null
and oh.customer_id=a.address_id
group by
oh.customer_id,
a.name,
LLL,
oh.order_id,
oh.consignment
order by A,C,CC
/
