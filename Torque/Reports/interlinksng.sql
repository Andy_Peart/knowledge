SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 2000
set trimspool on


column M noprint

--spool import.txt

SELECT
oh.user_def_type_7 M,
oh.ORDER_ID||'|3||'||
oh.NAME||'|'||
oh.USER_DEF_TYPE_1||'|'||
oh.ADDRESS1||'|'||
oh.ADDRESS2||'|'||
oh.TOWN||'|'||
oh.POSTCODE||'|'||
oh.instructions||'||1|'||
case when oh.dispatch_method = 'SPEEDY' then '233'
else '211' end||'|'||
oh.COUNTRY||'|||'||
oh.CONSIGNMENT||'|'||
oh.DISPATCH_METHOD||'|||1|'||
oh.INV_CONTACT_PHONE||'||||||'||
oh.INV_CONTACT_EMAIL||'|'||
oh.INV_CONTACT_PHONE||'||||||'||
' '
from   order_header oh
inner join ORDER_LINE ol on oh.order_id=ol.order_id and oh.client_id=ol.client_id
inner join MOVE_TASK mt on mt.sku_id = ol.sku_id and mt.client_id = ol.client_id and mt.task_id = oh.order_id
where oh.client_id ='SB'
and mt.client_id = 'SB'
and oh.STATUS = 'Allocated'
and oh.order_type not in ('REPLENC','NEW')
and oh.CONSIGNMENT='&2'
and (ol.BACK_ORDERED is null
or ol.BACK_ORDERED <> 'Y')
and oh.country ='GBR'
order by mt.SEQUENCE, oh.order_id
/

--spool off