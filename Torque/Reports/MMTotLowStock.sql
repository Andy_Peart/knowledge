set pagesize 0
set linesize 200
set verify off
clear columns
clear breaks
clear computes

set heading off
set feedback off

select 'Total number of MM SKUs with stock less than &&2 =',count(*) from
(select sku_id C,
sum(qty_on_hand)-sum(nvl(qty_allocated,0)) B
from inventory 
where client_id = 'MM'
and (sku_id not like 'V%'
or sku_id like 'VY%')
group by sku_id)
where B < '&&2'
--and D=0
/
select '' from DUAL;
select '' from DUAL;

select 'Total number of VH SKUs with stock less than &&2 =',count(*) from
(select sku_id C,
sum(qty_on_hand)-sum(nvl(qty_allocated,0)) B
from inventory 
where client_id = 'MM'
and sku_id like 'V%'
and sku_id not like 'VY%'
group by sku_id)
where B < '&&2'
--and D=0
/
