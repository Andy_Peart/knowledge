SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 100
SET TRIMSPOOL ON



column A format A14
column B format A10
column C format A10
column D format A8
column E format A10
column F format 9999999
column G format 9999999
column M noprint

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



--set termout off
--spool SBMshipments.csv


select 'Sweaty Betty Shipments per Month by Retail Store from &&2 to &&3' from DUAL;
Select 'Date,Store ID,Shipments,Qty Shipped' from DUAL;


break on A dup on report

compute sum label 'MONTH TOTALS' of F G on A
compute sum label 'RUN TOTALS' of F G on report


select
to_char(itl.Dstamp, 'MM YYYY (Mon)') A,
--oh.order_type B,
''''||itl.customer_id||'''' D,
count(*) F,
sum(DECODE(itl.code,'UnShipment',0-itl.update_qty,itl.update_qty)) G,
' '
from inventory_transaction itl,order_header oh
where itl.client_id='SB'
and itl.code in ('Shipment','UnShipment')
and itl.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and itl.reference_id like '%T%'
and oh.client_id='SB'
and itl.reference_id=oh.order_id
group by
to_char(itl.Dstamp, 'MM YYYY (Mon)'),
--oh.order_type,
itl.customer_id
order by A,D
/

--spool off
--set termout on
