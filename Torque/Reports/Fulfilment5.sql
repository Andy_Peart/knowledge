SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 90


column A heading 'Day Created' format A24
column B heading ' ' format A14
column C heading 'Orders in' format A10
column D heading 'Orders out'format 999999
column E heading 'Percentage' format 99990.99
column F heading 'Shipped on' format A30
column J format A12
column K format A10
column L format A16
column M noprint
column P noprint

break on A skip 1 on report

select 'client &&2 for week commencing Saturday &&3' from DUAL;
 
select 'Day Created,Orders in,Shipped on,Orders out,Percentage,,' from DUAL;

with created as (
    select to_char(creation_date,'DD-MON-YYYY DAY') created_date, count(*) created_orders
    from order_header
    where client_id = 'TR'
    and creation_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&2', 'DD-MON-YYYY')+7
    and status='Shipped' and dispatch_method in ('2','3','4','5','997') and ship_dock not like 'TRALT%' and order_id like 'H%'
    group by to_char(creation_date,'DD-MON-YYYY DAY')
    order by 1
)
,
shipped as (
    select to_char(creation_date,'DD-MON-YYYY DAY') created_date, to_char(shipped_date,'DD-MON-YYYY DAY') shipped_date, trunc(shipped_date)-trunc(creation_date)+1 as days_,
    count(*) shipped_orders
    from order_header
    where client_id = 'TR'
    and creation_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&2', 'DD-MON-YYYY')+7
    and status='Shipped' and dispatch_method in ('2','3','4','5','997') and ship_dock not like 'TRALT%' and order_id like 'H%'
    group by to_char(creation_date,'DD-MON-YYYY DAY'), to_char(shipped_date,'DD-MON-YYYY DAY'), trunc(shipped_date)-trunc(creation_date)+1
    order by 1,3
),
all_shipped as (
    select trunc(shipped_date)-trunc(creation_date)+1 as days_, count(*) shipped_orders
    from order_header
    where client_id = 'TR'
    and creation_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&2', 'DD-MON-YYYY')+7
    and status='Shipped' and dispatch_method in ('2','3','4','5','997') and ship_dock not like 'TRALT%' and order_id like 'H%'
    group by trunc(shipped_date)-trunc(creation_date)+1
)
select 'X' as M, created_date A, to_char(created_orders) C,
case when days_ <100 then  'Day ' || to_char(days_) else 'Cleared within 2 days' end F,
shipped_orders D,
case when percentage < 100 then percentage else round(shipped_orders/created_orders,4)*100 end E
from (
    select c.created_date, s.shipped_date, c.created_orders, s.shipped_orders, s.days_, round(s.shipped_orders/c.created_orders,4)*100 percentage
    from created c inner join shipped s on c.created_date = s.created_date
    union all
    select c.created_date, 'Cleared witin 2 days', c.created_orders, sum(s.shipped_orders) shipped_orders, 100, 100
    from created c inner join shipped s on c.created_date = s.created_date
    where s.days_<=2
    group by c.created_date, c.created_orders
order by 1,5    
)
union all
select 'X' M,'' A,'' C,'Total Orders' F, sum(shipped_orders) D,100 E
from all_shipped
union all
select 'X' M,'' A,'' C,'Total cleared within 2 days' F, ship2 D, round(ship2/shipall,4)*100 E
from (
    select 'X' as y, sum(shipped_orders) ship2
    from all_shipped
    where days_ <=2
) A
left join
(
    select 'X' as y, sum(shipped_orders) shipall
    from all_shipped
) B
on A.y = B.y
/
