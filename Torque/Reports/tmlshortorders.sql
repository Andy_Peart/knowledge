SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 300
SET TRIMSPOOL ON

select 'Client,Order Status,Consignment,Order Number,Po Number,User Def 5,Despatch Method,Customer Number,SKU,Sku user def 4,sku size,Description,Qty Ordered,BO Qty,Bonded,Intake,Allocated,Loading,Susp/Lock,PO Due,Booked Date,Order Date,Name,Email,Delivery,Country,Del Method'
from dual
union all
select * from (SELECT
oh.client_id ||','||
oh.status ||','||
oh.consignment ||','||
oh.order_id ||','||
oh.purchase_order ||','||
oh.user_def_type_5 ||','||
oh.DISPATCH_METHOD ||','||
oh.customer_id ||','||
ol.sku_id ||','||
sku.user_def_type_4 ||','||
sku.sku_size ||','||
sku.description ||','||
ol.qty_ordered ||','||
sum(nvl(ol.qty_ordered,0)-nvl(ol.qty_tasked,0)) ||','||
nvl(QQ,0) ||','||
nvl(QQ2,0) ||','||
nvl(QQ3,0) ||','||
nvl(QQ4,0) ||','||
nvl(QQ5,0) ||','||
trunc(D2) ||',,'||
trunc(oh.order_date) ||','||
oh.contact ||','||
oh.contact_email ||','||
oh.priority ||','||
oh.country ||','||
oh.ce_reason_notes
--case when sum(ol.qty_ordered)over(partition by oh.order_id) = sum(sum(ol.qty_ordered)-sum(ol.qty_picked)-sum(ol.qty_tasked))over(partition by oh.order_id) then 'x' else '' end as "All short?"
FROM order_header oh, order_line ol,sku,(select
sku_id SS,
sum(qty_on_hand-qty_allocated) QQ
from inventory
where client_id='TR'
and location_id='BOND'
--and lock_status='Locked'
group by
sku_id),(select
sku_id SS2,
sum(qty_on_hand-qty_allocated) QQ2
from inventory iv,location ll
where iv.client_id='TR'
and iv.location_id<>'BOND'
and iv.location_id=ll.location_id
and ll.loc_type='Receive Dock'
group by
sku_id),(select
sku_id SS3,
sum(qty_allocated) QQ3
from inventory
where client_id='TR'
group by
sku_id),(select
sku_id SS4,
sum(qty_on_hand-qty_allocated) QQ4
from inventory iv,location ll
where iv.client_id='TR'
and iv.location_id=ll.location_id
and ll.loc_type='Ship Dock'
group by
sku_id),(select
sku_id SS5,
sum(qty_on_hand-qty_allocated) QQ5
from inventory iv,location ll
where iv.client_id='TR'
and iv.location_id=ll.location_id
and iv.site_id=ll.site_id
and (ll.loc_type='Suspense'
or (ll.loc_type='Tag-FIFO'
and iv.lock_status in ('Locked','OutLocked')))
group by
sku_id),(select
pl.sku_id S2,
max(ph.due_dstamp) D2
from pre_advice_header ph,pre_advice_line pl
where ph.client_id='TR'
and ph.client_id=pl.client_id
and ph.pre_advice_id=pl.pre_advice_id
and nvl(pl.qty_received,0)=0
group by
pl.sku_id)
WHERE oh.order_id=ol.order_id
--AND oh.order_type IN ('WEB','AMZ','EBAY')
AND oh.status NOT IN ('Cancelled','Shipped')
--AND oh.consignment = 'SHORT'
AND oh.client_id = 'TR'
AND ol.client_id = 'TR'
AND nvl(ol.qty_ordered,0)-nvl(ol.qty_tasked,0)-nvl(ol.qty_picked,0)<>0
and ol.sku_id=sku.sku_id
and sku.client_id='TR'
and ol.sku_id=SS (+)
and ol.sku_id=SS2 (+)
and ol.sku_id=SS3 (+)
and ol.sku_id=SS4 (+)
and ol.sku_id=SS5 (+)
and ol.sku_id=S2 (+)
GROUP BY
oh.client_id,
oh.status,
oh.consignment,
oh.order_id,
oh.customer_id,
ol.sku_id,
sku.description,
ol.qty_ordered,
QQ,
QQ2,
QQ3,
QQ4,
QQ5,
trunc(D2),
oh.order_date,
oh.contact,
oh.contact_email,
oh.priority,
oh.country,
oh.ce_reason_notes,
oh.purchase_order,
oh.user_def_type_5,
oh.DISPATCH_METHOD,
sku.user_def_type_4,
sku.sku_size
ORDER BY oh.order_id)
/

--spool off