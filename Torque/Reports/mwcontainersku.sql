SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON




set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


select
trunc(dstamp)||','||
work_group||','||
container_id||','||
sum(update_qty)||','||
user_id
from inventory_transaction
where trunc(dstamp) = trunc(sysdate)
and client_id ='MOUNTAIN'
and code ='Pick'
and work_group <> 'WWW'
and from_loc_id <> 'CONTAINER'
group by
trunc(dstamp),
work_group,
container_id,
user_id
order by
trunc(dstamp),
work_group,
container_id,
user_id
/
