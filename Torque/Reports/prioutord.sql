SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9999

column A format a12
column B format a13
column C format a12
column D format 99999999
column E format a13
column F format a30
column G format 99999999999999
column H format 99999999999999
column I format a30

select 'Status, Released Date, Order Type, Priority, Customer Code, Customer Desc, Sales Order Id, Total Unit Qty, Pick Instr'
from DUAL;

select distinct oh.status A,
to_char(oh.creation_date, 'DD/MM/YY') B,
'order_type' C,
case oh.priority when 1 then 5
when 2 then 4
when 3 then 3
when 4 then 2
when 5 then 1
else 0 end D,
oh.inv_name E,
oh.name F,
oh.order_id G,
sum(ol.qty_ordered) H,
oh.user_def_note_1 I
from order_header oh, order_line ol
where oh.order_id = ol.order_id
and oh.client_id = 'PRI'
and status <> 'Shipped'
and status <> 'Cancelled'
group by oh.status, to_char(oh.creation_date, 'DD/MM/YY'), oh.priority, oh.inv_name, oh.name, oh.order_id, oh.user_def_note_1
order by 1
/