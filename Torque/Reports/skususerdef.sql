SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 2500
SET TRIMSPOOL ON


/*Input SQL here*/
select 'Sku,EAN,Description,User Def 1,User Def 2,User Def 3,User Def 4,User Def 5,User Def 6,User Def 7,User Def 8,User Num 1,User Num 2,User Num 3,User Num 4,User Note 1,User Note 2,User Chk 1,User Chk 2,User Chk 3,User Chk 4' from dual
union all
select '''' || sku_id || ''',''' || EAN || ''',' || description || ',' || user_def_type_1 || ',' || user_def_type_2 || ',' || user_def_type_3 || ',' || user_def_type_4 || ',' || user_def_type_5 || ',' || user_def_type_6 || ',' || user_def_type_7 || ',' || user_def_type_8 || ',' ||
user_def_num_1 || ',' || user_def_num_2 || ',' || user_def_num_3 || ',' || user_def_num_4 || ',' || user_def_note_1 || ',' || user_def_note_2 || ',' || user_def_chk_1 || ',' || user_def_chk_2 || ',' || user_def_chk_3 || ',' || user_def_chk_4 from (
select sku_id
, EAN
, description
, user_def_type_1
, user_def_type_2
, user_def_type_3
, user_def_type_4
, user_def_type_5
, user_def_type_6
, user_def_type_7
, user_def_type_8
, user_def_num_1
, user_def_num_2
, user_def_num_3
, user_def_num_4
, user_def_note_1
, user_def_note_2
, user_def_chk_1
, user_def_chk_2
, user_def_chk_3
, user_def_chk_4
from sku
where client_id = '&&2'
)
/
--spool off