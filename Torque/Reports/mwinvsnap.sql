SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET TRIMSPOOL ON

select 'BD2 Stock' stock_type, count(*) SKUs, sum(qty) Total_stock
from (
select inv.sku_id, sum(inv.qty_on_hand) qty
from inventory inv inner join sku on inv.sku_id = sku.sku_id and inv.client_id= sku.client_id
inner join location loc on loc.location_id = inv.location_id and inv.site_id = loc.site_id
where inv.client_id = 'MOUNTAIN'
and loc.work_zone not like 'CASLEVEL%'
and loc.work_zone not like 'GAS CONT%'
and sku.product_group not in ('NFC','NFF','NFS')
group by inv.sku_id
)
union all
select 'CAS Stock' stock_type, count(*) SKUs, sum(Qty) Total_stock
from (
select inv.sku_id, sum(inv.qty_on_hand) qty
from inventory inv inner join sku on inv.sku_id = sku.sku_id and inv.client_id= sku.client_id
inner join location loc on loc.location_id = inv.location_id and inv.site_id = loc.site_id
where inv.client_id = 'MOUNTAIN'
and loc.work_zone like 'CASLEVEL%'
and sku.product_group not in ('NFC','NFF','NFS')
group by inv.sku_id
)
union all
select 'Gas stock' stock_type, count(*) SKUs, sum(qty) Total_stock
from (
select inv.sku_id, sum(inv.qty_on_hand) qty
from inventory inv inner join sku on inv.sku_id = sku.sku_id and inv.client_id= sku.client_id
inner join location loc on loc.location_id = inv.location_id and inv.site_id = loc.site_id
where inv.client_id = 'MOUNTAIN'
and loc.work_zone like 'GAS CONT%'
group by inv.sku_id
)
union all
select 'Stationery' stock_type, count(*) SKUs, sum(qty) Total_stock
from (
select inv.sku_id, sum(inv.qty_on_hand) qty
from inventory inv inner join sku on inv.sku_id = sku.sku_id and inv.client_id= sku.client_id
inner join location loc on loc.location_id = inv.location_id and inv.site_id = loc.site_id
where inv.client_id = 'MOUNTAIN'
and sku.product_group in ('NFC','NFF','NFS')
group by inv.sku_id
)

/