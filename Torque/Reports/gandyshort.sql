SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET COLSEP ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 200
SET TRIMSPOOL ON
SET HEADING OFF 

column Z heading 'Status' format A12
column A heading 'Order' format A20
column B heading 'Line' format 999999
column C heading 'SKU' format A60
column F heading 'Style' format A20
column D heading 'Qty1' format 9999999
column E heading 'Qty2' format 9999999





select 'Gandy Consignment &&2 Shipment Detail' from DUAL;
--select to_char(SYSDATE, 'DD/MM/YY    HH:MI:SS') from DUAL;

select 'Status,Order ID,Line ID,SKU ID,Qty Ordered,Qty Allocated,Shortage,Description,' from DUAL;


break on A dup skip 1

--compute sum label 'Total' of D on report

select
oh.status||','|| 
oh.order_id||','||
ol.line_id||','||
sku.sku_id||','||
to_char (nvl(ol.qty_ordered,0))||','||
to_char(nvl(ol.qty_tasked,0)) ||','||
to_char(nvl(ol.qty_ordered,0)-nvl(ol.qty_tasked,0)) ||','||
sku.description A1
from order_header oh, order_line ol,sku sku
where oh.client_id = 'GA'
and oh.consignment = '&&2'
and oh.client_id=ol.client_id
and oh.order_id=ol.order_id
and ol.sku_id=sku.sku_id
and sku.client_id = 'GA'
order by
oh.order_id,
ol.line_id
/
