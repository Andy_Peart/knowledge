SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 2500
SET TRIMSPOOL ON

break on "Date" skip 1;
compute sum label 'Totals' of hour0 hour1 hour2 hour3 hour4 hour5 hour6 hour7 hour8 hour9 hour10 hour11 hour12 hour13 hour14 hour15 hour16 hour17 hour18 hour19 hour20 hour21 hour22 hour23 CL on "Date";

select 'Date,Priority,00:00,01:00,02:00,03:00,04:00,05:00,06:00,07:00,08:00,09:00,10:00,11:00,12:00,13:00,14:00,15:00,16:00,17:00,18:00,19:00,20:00,21:00,22:00,23:00,Totals' from dual;
select "Date","Pri",
sum(hour0) hour0,
sum(hour1) hour1,
sum(hour2) hour2,
sum(hour3) hour3,
sum(hour4) hour4,
sum(hour5) hour5,
sum(hour6) hour6,
sum(hour7) hour7,
sum(hour8) hour8,
sum(hour9) hour9,
sum(hour10) hour10,
sum(hour11) hour11,
sum(hour12) hour12,
sum(hour13) hour13,
sum(hour14) hour14,
sum(hour15) hour15,
sum(hour16) hour16,
sum(hour17) hour17,
sum(hour18) hour18,
sum(hour19) hour19,
sum(hour20) hour20,
sum(hour21) hour21,
sum(hour22) hour22,
sum(hour23) hour23,
sum(hour0)+sum(hour1)+sum(hour2)+sum(hour3)+sum(hour4)+sum(hour5)+sum(hour6)+sum(hour7)+sum(hour8)+sum(hour9)+sum(hour10)+sum(hour11)+sum(hour12)+
sum(hour13)+sum(hour14)+sum(hour15)+sum(hour16)+sum(hour17)+sum(hour18)+sum(hour19)+sum(hour20)+sum(hour21)+sum(hour22)+sum(hour23) CL
from (select "Date", "Pri",
DECODE("Hour",'00',"Orders",0) hour0,
DECODE("Hour",'01',"Orders",0) hour1,
DECODE("Hour",'02',"Orders",0) hour2,
DECODE("Hour",'03',"Orders",0) hour3,
DECODE("Hour",'04',"Orders",0) hour4,
DECODE("Hour",'05',"Orders",0) hour5,
DECODE("Hour",'06',"Orders",0) hour6,
DECODE("Hour",'07',"Orders",0) hour7,
DECODE("Hour",'08',"Orders",0) hour8,
DECODE("Hour",'09',"Orders",0) hour9,
DECODE("Hour",'10',"Orders",0) hour10,
DECODE("Hour",'11',"Orders",0) hour11,
DECODE("Hour",'12',"Orders",0) hour12,
DECODE("Hour",'13',"Orders",0) hour13,
DECODE("Hour",'14',"Orders",0) hour14,
DECODE("Hour",'15',"Orders",0) hour15,
DECODE("Hour",'16',"Orders",0) hour16,
DECODE("Hour",'17',"Orders",0) hour17,
DECODE("Hour",'18',"Orders",0) hour18,
DECODE("Hour",'19',"Orders",0) hour19,
DECODE("Hour",'20',"Orders",0) hour20,
DECODE("Hour",'21',"Orders",0) hour21,
DECODE("Hour",'22',"Orders",0) hour22,
DECODE("Hour",'23',"Orders",0) hour23
from (
select trunc(creation_date)     as "Date"
, to_char(creation_date,'HH24') as "Hour"
, count(*)                      as "Orders"
, priority                      as "Pri"
from order_header
where client_id='MOUNTAIN'
and substr(work_group,2,2) = 'WW'
and creation_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by trunc(creation_date)
, to_char(creation_date,'HH24')
, priority
order by trunc(creation_date)
, to_char(creation_date,'HH24')
)
)
group by "Date","Pri"
order by "Date","Pri"
/
--spool off