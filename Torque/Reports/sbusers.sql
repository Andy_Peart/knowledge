SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 99
SET NEWPAGE 0
SET LINESIZE 80



column A heading 'User ID' format A18
column B heading 'Number' format 99999
column C heading 'Units' format 99999
column M format A4 noprint


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle 'SB Receipts by User ID from &&2 to &&3 between &&4 and &&5'



break on report

compute sum label 'Totals' of B C on report

select
user_id,
count(*) B,
sum(update_qty) C
from inventory_transaction
where client_id='SB'
and station_id not like 'Auto%'
and code like 'Receipt%'
and reference_id not like 'C%'
and reference_id not like 'P%'
and dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and to_number(to_char(dstamp,'HH24MI')) between to_number('&&4') and to_number('&&5')
group by
user_id
order by
user_id
/
