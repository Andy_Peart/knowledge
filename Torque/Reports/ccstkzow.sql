SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

select 'Owner ID,CCBX,CCHG,CCBLK1,CCBLK2,CCRT,CCBSK,CCSUB,CCMIXED,CCSHOESPLT,CCSHOESSTD' from dual;
select it.owner_id,
SUM(case when loc.work_zone = 'CCBX' then it.qty_on_hand else 0 end) as CCBX,
SUM(case when loc.work_zone = 'CCHG' then it.qty_on_hand else 0 end) as CCHG,
SUM(case when loc.work_zone = 'CCBLK1' then it.qty_on_hand else 0 end) as CCBLK1,
SUM(case when loc.work_zone = 'CCBLK2' then it.qty_on_hand else 0 end) as CCBLK2,
SUM(case when loc.work_zone = 'CCRT' then it.qty_on_hand else 0 end) as CCRT,
SUM(case when loc.work_zone = 'CCBSK' then it.qty_on_hand else 0 end) as CCBSK,
SUM(case when loc.work_zone = 'CCSUB' then it.qty_on_hand else 0 end) as CCSUB,
SUM(case when loc.work_zone = 'CCMIXED' then it.qty_on_hand else 0 end) as CCMIXED,
SUM(case when loc.work_zone = 'CCSHOESPLT' then it.qty_on_hand else 0 end) as CCSHOESPLT,
SUM(case when loc.work_zone = 'CCSHOESSTD' then it.qty_on_hand else 0 end) as CCSHOESSTD
from inventory it inner join location loc on it.location_id = loc.location_id and it.site_id = loc.site_id
where it.client_id='CC' 
and it.site_id = 'WIGAN'
and loc.loc_type = 'Tag-FIFO'
and it.lock_status = 'UnLocked'
and loc.lock_status = 'UnLocked'
and nvl(loc.disallow_alloc,'N') = 'N' 
group by it.OWNER_ID
order by it.OWNER_ID
/