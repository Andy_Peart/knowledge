SET FEEDBACK OFF
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 3500
SET TRIMSPOOL ON

--SET TERMOUT OFF

break on A dup 
compute sum label 'Type Total' of H I J K on A

--spool stdoutord.csv


select 'Order Type,Order Date,Creation Date/Time,Ship by Date,Deliver by Date,Status,Customer,Name,Order ID,Country,Seller,Consignment,Lines,Qty Due,Qty Tasked,Qty Picked,Dispatch Method,Priority' from DUAL;


select
oh.order_type A,
to_char(oh.order_date,'DD-MON-YY HH24:MI:SS') C,
to_char(oh.creation_date,'DD-MON-YY HH24:MI:SS') B,
to_char(oh.ship_by_date,'DD-MON-YY HH24:MI:SS') B2,
to_char(oh.deliver_by_date,'DD-MON-YY HH24:MI:SS') B3,
oh.status D,
oh.customer_id E,
oh.contact F,
oh.order_id G,
oh.country G2,
oh.seller_name R,
oh.consignment S,
nvl(count(distinct ol.line_id),0) H,
sum(nvl(ol.qty_ordered,0)) I,
sum(nvl(ol.qty_tasked,0)) J,
sum(nvl(ol.qty_picked,0)) K,
oh.dispatch_method L,
oh.priority M
--oh.instructions N
from order_header oh 
inner join order_line ol 
on oh.client_id = ol.client_id
and oh.order_id = ol.order_id
where oh.client_id = '&&2'
and status not in ('Shipped','Cancelled')
group by
oh.order_type,
oh.order_date,
oh.creation_date,
oh.ship_by_date,
oh.deliver_by_date,
oh.status,
oh.customer_id,
oh.contact,
oh.order_id,
oh.country,
oh.seller_name,
oh.consignment,
oh.dispatch_method,
oh.priority
--oh.instructions
order by 
oh.order_type,
oh.creation_date
/

--spool off
