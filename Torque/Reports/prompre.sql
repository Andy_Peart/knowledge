SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 200
SET TRIMSPOOL ON

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



--spool productivity.csv

select 'Pre Advice Log - for client PROMINENT' from DUAL;

select 'Pre Advice ID,Contract No,Container No,Qty,' from DUAL;

Select
ph.pre_advice_id||','||
pl.User_Def_Type_8||','||
ph.Notes||','||
sum(pl.qty_due)||','||
' '
from pre_advice_header ph,pre_advice_line pl
where ph.client_id='PR'
--and ph.pre_advice_id like '21917%'
and ph.Creation_date>to_date('17-mar-2010','DD-MON-YYYY')
and ph.status='Hold'
and ph.client_id=pl.client_id
and ph.pre_advice_id=pl.pre_advice_id
group by
ph.pre_advice_id,
pl.User_Def_Type_8,
ph.Notes
order by
ph.pre_advice_id,
pl.User_Def_Type_8
/
