SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

column A heading 'Store' format A6
column B heading 'Name' format A30
column C heading 'Order/List' format A20
column D heading 'Lines' format 99999999
column E heading 'To Pick' format 99999999
column EE heading 'Picked' format 99999999
column F heading 'Consignment' format A14
column M format a4 noprint



select 'Store,Name,Order No/List No,Lines,To Pick,Consignment,Week Number,Run Letter,Run Number' from DUAL;
 
set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DAY, DD MONTH, YYYY') curdate from DUAL;
set TERMOUT ON


break on report

compute sum label 'Totals' of D E on report


select
oh.customer_id A,
a.name B,
'List ' || LLL C,
sum(OO) D,
sum(QQQ) E,
oh.consignment F,
substr(oh.consignment,7,4) M,
'&&2',
'&&3',
DECODE('&&3','E',a.user_def_type_1,
             'A',a.user_def_type_2,
             'B',a.user_def_type_3,
             'C',a.user_def_type_4,
             'D',a.user_def_type_5,'XXX'),
' '
from order_header oh,address a,
(select
order_id OOO,
list_id LLL,
sum(qty_to_move) QQQ
from order_header oh, move_task mt
where oh.client_id='TR'
and oh.order_type <> 'WEB'
and mt.client_id='TR'
and oh.consignment=mt.consignment
and oh.order_id=mt.task_id
group by
order_id,
list_id),
(select
oh.order_id  ID,
--nvl(sum(ol.qty_ordered),0) OO,
count(*) OO,
nvl(sum(ol.qty_picked),0) PP,
nvl(sum(ol.qty_tasked),0) KK,
nvl(sum(ol.qty_shipped),0) SS
from order_header oh,order_line ol
where oh.client_id='TR'
and oh.order_type <> 'WEB'
and oh.status not in ('Cancelled','Hold','Shipped')
and oh.order_id=ol.order_id
and ol.client_id='TR'
group by
oh.order_id)
where oh.order_id=OOO
and oh.order_id=ID
and oh.status not in ('Cancelled','Hold','Shipped')
and oh.consignment like '&&2%'
--and substr(oh.consignment,7,1)='&&3'
and substr(oh.consignment,5,1)='&&3'
and oh.client_id='TR'
and oh.order_type <> 'WEB'
and a.client_id='TR'
and LLL is not null
and oh.customer_id=a.address_id
group by
oh.customer_id,
a.name,
LLL,
oh.consignment,
DECODE('&&3','E',a.user_def_type_1,
             'A',a.user_def_type_2,
             'B',a.user_def_type_3,
             'C',a.user_def_type_4,
             'D',a.user_def_type_5,'XXX')
--order by
--oh.customer_id
union
select
oh.customer_id A,
a.name B,
oh.order_id C,
OO D,
KK E,
oh.consignment F,
substr(oh.consignment,7,4) M,
'&&2',
'&&3',
DECODE('&&3','E',a.user_def_type_1,
             'A',a.user_def_type_2,
             'B',a.user_def_type_3,
             'C',a.user_def_type_4,
             'D',a.user_def_type_5,'XXX'),
' '
from order_header oh,address a,
(select
order_id OOO,
list_id LLL,
sum(qty_to_move) QQQ
from order_header oh, move_task mt
where oh.client_id='TR'
and oh.order_type <> 'WEB'
and mt.client_id='TR'
and mt.status='Released'
and oh.consignment=mt.consignment
and oh.order_id=mt.task_id
group by
order_id,
list_id),
(select
oh.order_id  ID,
--nvl(sum(ol.qty_ordered),0) OO,
count(*) OO,
nvl(sum(ol.qty_picked),0) PP,
nvl(sum(ol.qty_tasked),0) KK,
nvl(sum(ol.qty_shipped),0) SS
from order_header oh,order_line ol
where oh.client_id='TR'
and oh.order_type <> 'WEB'
and oh.status not in ('Cancelled','Hold','Shipped')
and oh.order_id=ol.order_id
and ol.client_id='TR'
group by
oh.order_id)
where oh.order_id=OOO
and oh.order_id=ID
and oh.status not in ('Cancelled','Hold','Shipped')
and oh.consignment like '&&2%'
--and substr(oh.consignment,7,1)='&&3'
and substr(oh.consignment,5,1)='&&3'
and oh.client_id='TR'
and oh.order_type <> 'WEB'
and a.client_id='TR'
and LLL is null
and oh.customer_id=a.address_id
order by
M,A
/
