SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON


select 'Picker,Zero picks,Non zero picks,Fulfilment' from dual
union all
select user_id || ',' || zero_picks || ',' || all_picks || ',' || fulful
from (
    select user_id, sum(zero_picks) zero_picks, sum(all_picks) all_picks,
    case when sum(all_picks)>0 then round((1-sum(zero_picks)/sum(all_picks))*100,1) else 0 end fulful
    from (
        select to_char(dstamp,'DD-MM-YYYY') pick_date, it.user_id, sum(case when update_qty = 0 then 1 else 0 end) zero_picks, sum(update_qty) all_picks    
        from inventory_transaction it
        where it.client_id = 'MOUNTAIN'
        and it.code = 'Pick'
        and it.work_group = 'WWW'
        and it.elapsed_time > 0
        and it.dstamp between to_date('&&2','DD-MON-YYYY') and to_date('&&3','DD-MON-YYYY')+1    
        group by it.user_id, to_char(dstamp,'DD-MM-YYYY')
    )
    group by user_id
    order by 1,2
)
union all 
select '----' from dual
union all
select 'Pick date,Picker,Batch ID,Work Zone,Zero picks,Non zero picks,Fulfilment' from dual
union all
select pick_date || ',' || user_id || ',' || consignment  || ',' || work_zone || ',' || zero_picks || ',' || all_picks || ',' || fulfilment
from (
    select to_char(dstamp,'DD-MM-YYYY') pick_date, it.user_id, consignment, loc.work_zone, sum(case when update_qty = 0 then 1 else 0 end) zero_picks, sum(update_qty) all_picks
    ,case when sum(update_qty)>0 then (1-round(sum(case when update_qty = 0 then 1 else 0 end) / sum(update_qty),3))*100 else 0 end fulfilment
    from inventory_transaction it inner join location loc on it.site_id = loc.site_id and it.from_loc_id = loc.location_id
    where it.client_id = 'MOUNTAIN'
    and it.code = 'Pick'
	and it.work_group = 'WWW'
    and it.elapsed_time > 0
    and it.dstamp between to_date('&&2','DD-MON-YYYY') and to_date('&&3','DD-MON-YYYY')+1    
    group by it.user_id, loc.work_zone, consignment, to_char(dstamp,'DD-MM-YYYY')
    order by 1,2,3
)
/