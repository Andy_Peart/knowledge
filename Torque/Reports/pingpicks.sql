SET FEEDBACK OFF             
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 300
SET TRIMSPOOL ON

--spool pingpicks.csv

Select 'Picks of 15 or more Units per Order' from DUAL;
select 'Date,No.,Qty' from DUAL;

select
D,
count(*) C,
sum(B) B
from (select
trunc(it.Dstamp) D,
reference_id E,
sum(it.update_qty) as B
from inventory_transaction it
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='PG'
and it.code like 'Pick'
and elapsed_time>0
group by
trunc(it.Dstamp),
reference_id )
where B>14
group by D
order by D
/

select
D,
count(*) C,
sum(B) B
from (select
trunc(oh.shipped_date) D,
oh.order_id E,
sum(ol.qty_picked) as B
from order_header oh,order_line ol
where oh.client_id='PG'
and oh.shipped_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and oh.client_id=ol.client_id
and oh.order_id=ol.order_id
group by
trunc(oh.shipped_date),
oh.order_id )
where B>14
group by D
order by D



Select 'Picks of 5 to 14 Units per Order' from DUAL;
select 'Date,No.,Qty' from DUAL;

select
D,
count(*) C,
sum(B) B
from (select
trunc(it.Dstamp) D,
reference_id E,
sum(it.update_qty) as B
from inventory_transaction it
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='PG'
and it.code like 'Pick'
and elapsed_time>0
group by
trunc(it.Dstamp),
reference_id )
where B>4
and B<15
group by D
order by D
/


select
D,
count(*) C,
sum(B) B
from (select
trunc(oh.shipped_date) D,
oh.order_id E,
sum(ol.qty_picked) as B
from order_header oh,order_line ol
where oh.client_id='PG'
and oh.shipped_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and oh.client_id=ol.client_id
and oh.order_id=ol.order_id
group by
trunc(oh.shipped_date),
oh.order_id )
where B>4
and B<15
group by D
order by D



Select 'Picks of less than 5 Units per Order' from DUAL;
select 'Date,No.,Qty' from DUAL;

select
D,
count(*) C,
sum(B) B
from (select
trunc(it.Dstamp) D,
reference_id E,
sum(it.update_qty) as B
from inventory_transaction it
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='PG'
and it.code like 'Pick'
and elapsed_time>0
group by
trunc(it.Dstamp),
reference_id )
where B<5
group by D
order by D
/

select
D,
count(*) C,
sum(B) B
from (select
trunc(oh.shipped_date) D,
oh.order_id E,
sum(ol.qty_picked) as B
from order_header oh,order_line ol
where oh.client_id='PG'
and oh.shipped_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and oh.client_id=ol.client_id
and oh.order_id=ol.order_id
group by
trunc(oh.shipped_date),
oh.order_id )
where B<5
group by D
order by D



Select 'ALL Picks' from DUAL;
select 'Date,No.,Qty' from DUAL;

select
D,
count(*) C,
sum(B) B
from (select
trunc(it.Dstamp) D,
reference_id E,
sum(it.update_qty) as B
from inventory_transaction it
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='PG'
and it.code like 'Pick'
and elapsed_time>0
group by
trunc(it.Dstamp),
reference_id )
group by D
order by D
/

select
D,
count(*) C,
sum(B) B
from (select
trunc(oh.shipped_date) D,
oh.order_id E,
sum(ol.qty_picked) as B
from order_header oh,order_line ol
where oh.client_id='PG'
and oh.shipped_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and oh.client_id=ol.client_id
and oh.order_id=ol.order_id
group by
trunc(oh.shipped_date),
oh.order_id )
group by D
order by D


--spool off
