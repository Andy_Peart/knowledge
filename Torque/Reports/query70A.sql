SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 40

--SET TERMOUT OFF
--SPOOL Q70A.csv


select 
sku_id||','||
nvl(BB,0)
from sku,(select
ii.sku_id AA,
sum(ii.qty_on_hand - ii.qty_allocated) BB
from inventory ii,(select 
distinct location_id LL
from location
where loc_type='Tag-FIFO')
where ii.client_id = 'MOUNTAIN'
and ii.location_id=LL
group by ii.sku_id)
where client_id='MOUNTAIN'
and sku_id=AA (+)
order by sku_id
/


--SPOOL OFF
--SET TERMOUT ON
