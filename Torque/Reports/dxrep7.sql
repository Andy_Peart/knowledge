SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep '|'


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9999
SET TRIMSPOOL ON

--set feedback on

update inventory_transaction
set uploaded='X'
where client_id = 'DX'
--and trunc(Dstamp)>trunc(sysdate-7)
and code = 'Adjustment'
and uploaded = 'N'
--and reason_id in ('SUB','DMGD','NOTEN')
/


--set feedback off
--SET TERMOUT OFF

--spool dxrep7.csv

select 'Adjustment_Report - '||to_char(SYSDATE, 'DD/MM/YY  HH24:MI:SS') from DUAL;

select 'Date,Product,Description,Reason,Qty,User ID' from DUAL;


select 
to_char(i.Dstamp,'DD/MM/YY')||','||
i.sku_id||','||
sku.description||','||
i.reason_id||','||
i.update_qty||','||
i.user_id
from inventory_transaction i,sku
where i.client_id='DX'
--and trunc(i.Dstamp)>trunc(sysdate-7)
and i.code = 'Adjustment'
and i.uploaded = 'X'
and sku.client_id = 'DX'
and i.sku_id=sku.sku_id
/

--spool off

--set feedback on
--set termout on


update inventory_transaction
set uploaded='Y'
where client_id = 'DX'
--and trunc(Dstamp)>trunc(sysdate-7)
and code = 'Adjustment'
and uploaded = 'X'
/


commit;

 
