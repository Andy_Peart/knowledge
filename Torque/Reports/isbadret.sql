SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Date,Order #,Sku,Description,Reason Code,Reason' from dual
union all
select to_char("Date", 'DD-MON-YYYY HH24:MI') || ',' || "Ord" || ',' || "Sku" || ',' || "Desc" || ',' || "Code" || ',' || "Reason" from
(
select it.dstamp        as "Date"
, it.reference_id       as "Ord"
, it.sku_id             as "Sku"
, lower(s.description)  as "Desc"
, it.reason_id          as "Code"
, rr.notes              as "Reason"
from inventory_transaction it
inner join sku s on s.client_id = it.client_id and s.sku_id = it.sku_id
inner join return_reason rr on rr.reason_id = it.reason_id
where it.client_id = 'IS'
and it.code = 'Return'
and it.dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.to_loc_id = 'ISBAD'
order by it.dstamp
)
/
--spool off