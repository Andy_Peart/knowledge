/******************************************************************************/
/*                                                                            */
/*                            Elite                                           */
/*                                                                            */
/*     FILE NAME  :   Ebioxdesp.sql                                           */
/*                                                                            */
/*     DESCRIPTION:   Datastream for Ebiox Despatch Note                      */
/*                                                                            */
/*   DATE     BY   PROJ       ID         DESCRIPTION                          */
/*   ======== ==== ======     ========   =============                        */
/*   23/02/12 RW    Ebiox                Delivery Note For Ebiox              */
/*                                                                            */
/******************************************************************************/

/* Input Parameters are as follows */
-- 1)  GMT
-- 2)  Order_id
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 0
SET VERIFY OFF
SET FEEDBACK OFF
SET TAB OFF

 
-- Set the line length to the maximum possible, but trim 
-- trailing spaces for both screen output and spool files
SET LINESIZE 32767
SET WRAP ON
SET TRIMOUT ON
SET TRIMSPOOL ON

/* Define the format of the output dates */
DEFINE	OutputDateFormat = 'DD-MON-YYYY HH24:MI:SS'
COLUMN	sorter	NOPRINT

SELECT Distinct '0' sorter,
       'DSTREAM_EBIOXDESP_HDR'          		||'|'||
       rtrim('&&2')                            /* Order_id */
from dual
union all
SELECT '1' sorter,
'DSTREAM_EBIOXDESP_ADD_HDR'       			||'|'||
rtrim (oh.user_def_note_1)					||'|'||     /*Extended version of company name */
decode(oh.name,null,'',oh.name||'|')||
decode(oh.address1,null,'',oh.address1||'|')||
decode(oh.address2,null,'',oh.address2||'|')||
decode(oh.town,null,'',oh.town||'|')||
decode(oh.county,null,'',oh.county||'|')||
decode(t.text,null,'',t.text||'|')||          /* Country converted using workstation lookup) */
decode(oh.postcode,null,'',oh.postcode)
from order_header oh, country c, language_text t
where oh.order_id = '&&2'
and oh.client_id = 'EX'
and oh.country = c.iso3_id
and t.language = 'EN_GB'
and t.label = 'WLK'||c.iso3_id
union all
select distinct '1' sorter,
'DSTREAM_EBIOXDESP_ORD_HDR'				||'|'||
rtrim (oh.order_id)					||'|'||
rtrim (to_char(oh.order_date, 'DD-MON-YYYY'))	||'|'||		
rtrim (to_char(oh.shipped_date, 'DD-MON-YYYY HH24:MI'))	||'|'||	
rtrim (oh.user_def_type_1) 	
from order_header oh
where oh.order_id = '&&2'
and oh.client_id = 'EX'
union all
SELECT Distinct '1' sorter,
'DSTREAM_EBIOXDESP_LINE'            	   	||'|'||
rtrim (ol.sku_id)						||'|'||
rtrim (sk.description)		    		 	||'|'||
rtrim (ol.created_by)					||'|'||
rtrim (ol.qty_ordered)					||'|'||
rtrim (ol.user_def_type_1)              		||'|'|| 
rtrim (ol.qty_shipped)					
from order_line ol, sku sk
where ol.order_id = '&&2'
and ol.client_id = 'EX'
and ol.sku_id = sk.sku_id
and ol.client_id = sk.client_id
/