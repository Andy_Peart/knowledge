SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9999
SET TRIMSPOOL ON

column A heading 'SKU' format A16
column B heading 'Qty' format 999999
column C heading 'Picks' format 999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


--spool mwpackinv.csv

select 'Sku Id,Work Zone,Location,Receipt Date,Ratio Pack,Unit Stock' from Dual;

select
it.sku_id,
lc.work_zone,
it.location_id,
trunc(receipt_dstamp),
sc.ratio_1_to_2,
sum(qty_on_hand) C
from inventory it,sku_config sc,location lc
where it.client_id='MOUNTAIN'
and it.sku_id=sc.config_id
and sc.client_id='MOUNTAIN'
and it.location_id not in ('CONTAINER','STG','UKPAC','UKPAR','MARSHALL','DESPATCH','MAIL ORDER')
and lc.work_zone<>'ZMW'
and it.location_id=lc.location_id
and it.site_id=lc.site_id
group by
it.sku_id,
lc.work_zone,
it.location_id,
trunc(receipt_dstamp),
sc.ratio_1_to_2
order by
it.sku_id,
lc.work_zone,
it.location_id,
trunc(receipt_dstamp);

select ' ' from dual;

select 'SKU, Pack config, Qty' from dual;

select inv.sku_id, inv.config_id, sum(qty_on_hand) qty
from inventory inv INNER JOIN location loc on inv.location_id = loc.location_id AND INV.SITE_ID = LOC.SITE_ID
where inv.client_id = 'MOUNTAIN'
and config_id like 'E%'
and loc.site_id = 'BD2'
and (
  loc.zone_1 = 'RETAIL'
  OR 
  loc.work_zone = 'WEB'
  OR 
  loc.work_zone = 'BULK'
)
group by inv.sku_id, loc.work_zone, inv.location_id, inv.config_id, to_char(inv.receipt_dstamp,'DD-MON-YYYY')
order by 1
/

--spool off
