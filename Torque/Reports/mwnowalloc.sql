SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 5000
SET TRIMSPOOL ON


--spool mwnowalloc.csv

select 'Order,SKU,Qty,Allocated,Latest,Description,Locations of Latest' from DUAL;

select
'&&2'||','||
A||','||
B||','||
D||','||
DST||','||
DSC||','||
LISTAGG(ix.location_id||'('||ix.qty_on_hand||')', ':') WITHIN GROUP (ORDER BY ix.location_id) AS LCX
from (select
distinct
it.sku_id A,
it.update_qty B,
trunc(iv.receipt_dstamp) D,
DST,
iv.description DSC
from inventory_transaction it,inventory iv,(select
i.sku_id SKY,
trunc(max(i.receipt_dstamp)) DST
from inventory i,location L
where i.client_id='MOUNTAIN'
and i.site_id=L.site_id
and i.location_id=L.location_id
and L.loc_type='Tag-FIFO'
group by i.sku_id)
where it.client_id='MOUNTAIN'
and it.reference_id='&&2'
and it.code='Allocate'
--and it.to_loc_id='CONTAINER'
and iv.client_id='MOUNTAIN'
and it.sku_id=iv.sku_id
and it.tag_id=iv.tag_id
and it.sku_id=SKY ),inventory ix
where DST-D>10
and ix.client_id='MOUNTAIN'
and A=ix.sku_id
and ix.location_id not in ('DESPATCH','MAIL ORDER','SUSPENSE')
and ix.location_id not like 'BD%'
and DST=trunc(ix.receipt_dstamp)
group by
A,
B,
D,
DST,
DSC
order by
A
/


--spool off


