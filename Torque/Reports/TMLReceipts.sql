SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ''


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 100
SET NEWPAGE 1
SET LINESIZE 60


column A heading 'Client' format A8
column B heading 'Type' format a14
column C heading 'Units' format 99999999
column D heading 'Split' format 999999.99
column E heading ' ' format A1



set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON




break on A dup skip 1 on report

compute sum label 'Total' of C on A
compute sum label 'Overall' of C on report


select
A,
B,
C,
--CC,
(C*100)/CC D,
'%' E
from (select
'TR' A,
'Hanging' B,
sum(update_qty) C
from inventory_transaction
where client_id='TR'
and Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and station_id not like 'Auto%'
and (code like 'Receipt%' or (code='Adjustment' and reason_id='RECRV'))
and to_loc_id like 'HANG%'
union
select
'TR' A,
'Cufflinks' B,
sum(update_qty) C
from inventory_transaction
where client_id='TR'
and Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and station_id not like 'Auto%'
and (code like 'Receipt%' or (code='Adjustment' and reason_id='RECRV'))
and to_loc_id like 'CUF%'
union
select
'TR' A,
'Boxed' B,
sum(update_qty) C
from inventory_transaction
where client_id='TR'
and Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and station_id not like 'Auto%'
and (code like 'Receipt%' or (code='Adjustment' and reason_id='RECRV'))
and nvl(to_loc_id,' ') not like 'HANG%'
and nvl(to_loc_id,' ') not like 'CUF%'
union
select
'T' A,
'Hanging' B,
sum(update_qty) C
from inventory_transaction
where client_id='T'
and Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and station_id not like 'Auto%'
and (code like 'Receipt%' or (code='Adjustment' and reason_id='RECRV'))
and to_loc_id like 'ZZ%'
union
select
'T' A,
'Cufflinks' B,
sum(update_qty) C
from inventory_transaction
where client_id='T'
and Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and station_id not like 'Auto%'
and (code like 'Receipt%' or (code='Adjustment' and reason_id='RECRV'))
and to_loc_id like 'CF%'
union
select
'T' A,
'Boxed' B,
sum(update_qty) C
from inventory_transaction
where client_id='T'
and Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and station_id not like 'Auto%'
and (code like 'Receipt%' or (code='Adjustment' and reason_id='RECRV'))
and nvl(to_loc_id,' ') not like 'ZZ%'
and nvl(to_loc_id,' ') not like 'CF%'),
(select
client_id AA,
sum(update_qty) CC
from inventory_transaction
where client_id in ('T','TR')
and Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and station_id not like 'Auto%'
and (code like 'Receipt%' or (code='Adjustment' and reason_id='RECRV'))
group by client_id)
where A=AA
order by A,B
/
