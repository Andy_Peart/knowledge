SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON


column A format a12
column B format 999999
column C format 999999
column D format 999999
column E format 999999
column F format 999990.99

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON




--break on A4 dup on report

--compute sum label 'Sub Total' of B C on A2
--compute sum label 'Summary' of B C on A1
--compute sum label 'Totals for Day' of B C on A4
--compute sum label 'Overall' of B C on report

--spool berwinexcess.csv

select 'Date,Units Received,Units Picked,Total,Excess,Additional Charge' from DUAL;


select
A,
B,
C,
D,
decode(sign(E),1,E,-1,0,0) E,
decode(sign(E),1,E,-1,0,0)*0.035 F
from (select
distinct
trunc(Dstamp) A,
nvl(RECT,0) B,
nvl(SHIP,0) C,
nvl(RECT,0)+nvl(SHIP,0) D,
--(nvl(RECT,0)+nvl(SHIP,0))-11500 E
CASE
WHEN sysdate<to_date('15-sep-2014','dd-mon-yyyy')
THEN (nvl(RECT,0)+nvl(SHIP,0))-8500 
ELSE (nvl(RECT,0)+nvl(SHIP,0))-11500 END E
from inventory_transaction,(select
trunc(Dstamp) DT1,
sum(update_qty) RECT
from inventory_transaction
where client_id='BW'
and to_char(Dstamp,'mm/yyyy')=to_char(sysdate-12,'mm/yyyy')
and (code like 'Receipt%' or (code='Adjustment' and reason_id in ('RECRV','STK CHECK')))
and reference_id not like '%-%'
group by
trunc(Dstamp)),(select
trunc(Dstamp) DT2,
sum(update_qty) SHIP
from inventory_transaction
where client_id='BW'
and to_char(Dstamp,'mm/yyyy')=to_char(sysdate-12,'mm/yyyy')
and code='Shipment'
group by
trunc(Dstamp))
where client_id='BW'
and to_char(Dstamp,'mm/yyyy')=to_char(sysdate-12,'mm/yyyy')
and (code in ('Receipt','Shipment') or (code='Adjustment' and reason_id in ('RECRV','STK CHECK')))
and trunc(Dstamp)=DT1 (+)
and trunc(Dstamp)=DT2 (+))
order by
A
/

--spool off
