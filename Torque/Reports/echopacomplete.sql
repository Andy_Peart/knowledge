SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET COLSEP ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 200
SET TRIMSPOOL ON
SET HEADING OFF 

column Z heading 'Status' format A12
column A heading 'pre_advice' format A20
column B heading 'Line' format 999999
column C heading 'SKU' format A18
column D heading 'Qty1' format 9999999
column E heading 'Qty2' format 9999999

select 'ECHO Pre Advice &&2 Detail' from DUAL;
--select to_char(SYSDATE, 'DD/MM/YY    HH:MI:SS') from DUAL;

select 'Status,Pre Advice ID,Line ID,SKU ID,Qty Due,Qty Received' from DUAL;


--break on report
--compute sum label 'Total' of D on report





select
ph.status Z,
ph.pre_advice_id A,
pl.line_id B,
pl.sku_id C,
pl.qty_due E,
nvl(pl.qty_received,0) D
from pre_advice_header ph, pre_advice_line pl
where ph.client_id like 'ECHO'
--and ph.status='Complete'
and ph.pre_advice_id='&&2'
and ph.client_id=pl.client_id
and ph.pre_advice_id=pl.pre_advice_id
/
