SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON

SELECT 'Client,Code,Sku,Receipt Date,Receipted Qty,Reversal Date,Reversed Qty' from dual
Union All 
select A ||','|| B ||','|| C ||','|| D ||','|| E ||','|| F ||','|| G  from (
select it.client_id as "A"
, it.code as "B"
, it.sku_id as "C"
, to_char(it.dstamp, 'DD-MON-YYYY') as "D"
, it.update_qty as "E"
,  x."date" as "F"
, -(x.update_qty) as "G"
from inventory_transaction it
LEFT JOIN
( 
    select client_id
    , code
    , to_char(dstamp, 'DD-MON-YYYY') as "date" 
    , sku_id
    , tag_id
    , update_qty
    from inventory_transaction  
    where client_id = '&&2'
    and code = 'Receipt Reverse'
) x on x.client_id = it.client_id and x.sku_id = it.sku_id and x.tag_id = it.tag_id
where it.client_id = '&&2'
and it.code = 'Receipt'
and it.dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and it.reference_id not in (select
    pre_advice_id from pre_advice_header
    where client_id= '&&2')
order by it.dstamp
)
/