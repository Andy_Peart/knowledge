SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 340

column A heading 'Client ID' format A12
column B heading 'SKU' format A25
column BB heading 'EAN' format A16
column BBB heading 'UPC' format A16
column C heading 'Description' format A50
column CC heading 'Colour' format A20
column CCC heading 'Group' format A15																																
column D heading 'Location' format A12
column E heading 'Qty on Hand' format 999999
column F heading 'Qty Allocated' format 999999
column FF heading 'Remainder' format 999999
column J heading 'Tag ID' format a12
column HH heading 'Alloc' format A10
column II heading 'Count' format A10
column JJ heading 'Receipt' format A10
column KK heading 'Move' format A10
column LL heading 'Location' format A12
column MM heading 'Condition' format A12


select 'Client ID,Location,SKU,EAN,UPC,Tag ID,Qty on Hand,Qty Allocated,Remainder,Colour,Description,Group Type,Allocation Group, Count date, Receipt Date, Move Date, Size, Condition' from DUAL;


select
it.client_id A,
it.location_id D,
''''||it.sku_id||'''' B,
''''||sku.EAN||'''' BB,
''''||sku.UPC||'''' BBB,
nvl(it.tag_id,0) J,
nvl(it.qty_on_hand,0) E,
nvl(it.qty_allocated,0) F,
nvl(it.qty_on_hand-it.qty_allocated,0) FF,
sku.colour CC,
replace(sku.description,',','') C,
sku.product_group CCC,
sku.allocation_group  HH,
to_char(it.COUNT_DSTAMP,'DD-MM-YYYY') as II,
to_char(it.RECEIPT_DSTAMP,'DD-MM-YYYY') JJ,
to_char(it.MOVE_DSTAMP,'DD-MM-YYYY') KK,
sku.sku_size LL,
it.condition_id MM
from inventory it,sku
where it.client_id='&&2'
--and it.location_id like '&&3%'
and substr(it.location_id,1,2) between '&&3' and '&&4'
and sku.client_id='&&2'
and it.sku_id=sku.sku_id
order by D,B
/
