set feedback off
set pagesize 70
set linesize 120
set verify off

clear columns
clear breaks
clear computes

column A heading 'Order ID' format A16
column B heading 'Order Date' format A16
column C heading 'Delivery Date' format A16
column D heading 'Order Qty' format 999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'Orders to be Released - for client ID 'PRI' as at ' report_date skip 2

break on report
compute sum label 'Total' of D E on report

select
oh.order_id A,
to_char(oh.order_date,'DD/MM/YY') B,
to_char(oh.deliver_by_date,'DD/MM/YY') C,
nvl(sum(ol.qty_ordered),0) D
from order_header oh,order_line ol
where oh.status='Released'
and oh.client_id='PRI'
and oh.order_id=ol.order_id (+)
group by
oh.order_id,
to_char(oh.order_date,'DD/MM/YY'),
to_char(oh.deliver_by_date,'DD/MM/YY')
/

