SET FEEDBACK OFF             
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 9999
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON

--spool mwinv99.csv

select
sku.user_def_type_1,
sku.user_def_type_3,
sku.sku_size,
inv.*
from inventory inv,sku
where inv.client_id='MOUNTAIN'
and inv.sku_id=sku.sku_id
and sku.client_id='MOUNTAIN'
and inv.zone_1='RACK1'
/


--spool off
