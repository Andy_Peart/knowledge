SET FEEDBACK OFF             
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

--SET TERMOUT OFF
--SPOOL TEMPPRI.CSV

--select to_char(SYSDATE, 'DD/MM/YY  HH:MI:SS') from DUAL;



select 'Sku Code,Location,Qty,Description,Style,Colour,Size,Last Putaway,Last Pick,'
from dual;

--break on report

--compute sum label 'Total' of QQ on report


Select
PA  ||','||
LL  ||','||
QQ  ||','||
sku.description  ||','||
substr(PA,1,6) ||','||
sku.colour  ||','||
sku.sku_size  ||','||
DECODE(to_char(RB, 'DD/MM/YY'),'01/01/00','None',to_char(RB, 'DD/MM/YY'))  ||','||
DECODE(to_char(PB, 'DD/MM/YY'),'01/01/00','None',to_char(PB, 'DD/MM/YY'))  ||','||
'  '
from
(select
PA,
LL,
QQ,
nvl(BB,'01-jan-00') PB,
nvl(CC,'01-jan-00') RB
from
(select sku_id PA,
location_id LL,
sum(qty_on_hand) QQ
from inventory
where client_id = '&&3'
group by sku_id,
location_id),
(select i.Sku_id PP,
max (i.dstamp) BB
from inventory_transaction i
where i.client_id = '&&3'
and i.code = 'Pick'
group by i.sku_id),
(select i.Sku_id RA,
max (i.dstamp) CC
from inventory_transaction i
where i.client_id = '&&3'
and i.code = 'Putaway'
group by i.sku_id)
where PA=PP (+)
and PA=RA (+)),
sku
where sku.client_id = '&&3'
and PA=sku.sku_id
and PB <sysdate - '&&2'
and RB <sysdate - '&&2'
order by PA
/

--select to_char(SYSDATE, 'DD/MM/YY  HH:MI:SS') from DUAL;


--SPOOL OFF
--SET TERMOUT ON
