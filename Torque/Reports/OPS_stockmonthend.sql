SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 160

Select 'SKU ID,QTY ON HAND,QTY ALLOCATED,QTY FREE' from DUAL;

column A heading 'SKU ID' format a20
column B heading 'QTY ON HAND' format 9999999
column C heading 'QTY ALLOCATED' format 9999999
column D heading 'QTY FREE' format 9999999

select sku_id A,  sum(qty_on_hand) B, sum(qty_allocated) C, sum(qty_on_hand)-sum(qty_allocated)  D
  from inventory
 where sku_id in
       (select distinct sku_id from inventory where client_id = 'OPS')
  and location_id <> 'OPSOUT'
  and location_id <> 'SUSPENSE'
  and location_id <> 'CONTAINER'
  and location_id <> 'SMREJ'
  and location_id <> 'OA1-SAMPLE'     
 group by sku_id
 order by sku_id
/