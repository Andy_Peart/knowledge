SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','
SET WRAP OFF


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500

with t1 as (
select inv.SKU_ID, sku.user_def_note_2 ,  sku.COLOUR , sku.SKU_SIZE 
from inventory inv
inner join sku sku on inv.sku_id = sku.sku_id and inv.client_id = sku.client_id
where inv.client_id = 'CC'
group by inv.SKU_ID, sku.user_def_note_2 ,  sku.COLOUR , sku.SKU_SIZE 
),
t2 as (
select inv.sku_id, inv.owner_id, sum(inv.qty_on_hand) as qty
from inventory inv
inner join location loc on inv.location_id = loc.location_id and inv.site_id = loc.site_id
where inv.owner_id = 'CC203'
group by inv.sku_id, inv.owner_id
), 
t3 as (
select inv.sku_id, inv.owner_id, sum(inv.qty_on_hand) as qty
from inventory inv
inner join location loc on inv.location_id = loc.location_id and inv.site_id = loc.site_id
where inv.owner_id = 'CC3'
group by inv.sku_id, inv.owner_id
),
t4 as (
select inv.sku_id, inv.owner_id, sum(inv.qty_on_hand) as qty
from inventory inv
inner join location loc on inv.location_id = loc.location_id and inv.site_id = loc.site_id
where inv.owner_id = 'CC310'
group by inv.sku_id, inv.owner_id
)
,
t5 as (
select inv.sku_id, inv.owner_id, sum(inv.qty_on_hand) as qty
from inventory inv
inner join location loc on inv.location_id = loc.location_id and inv.site_id = loc.site_id
where inv.owner_id = 'CC4'
group by inv.sku_id, inv.owner_id
)
,
t6 as (
select inv.sku_id, inv.owner_id, sum(inv.qty_on_hand) as qty
from inventory inv
inner join location loc on inv.location_id = loc.location_id and inv.site_id = loc.site_id
where inv.owner_id = 'CC2'
group by inv.sku_id, inv.owner_id
)
,
t7 as (
select inv.sku_id, inv.owner_id, sum(inv.qty_on_hand) as qty
from inventory inv
inner join location loc on inv.location_id = loc.location_id and inv.site_id = loc.site_id
where inv.owner_id = 'CC900'
group by inv.sku_id, inv.owner_id
)
,
t8 as (
select inv.sku_id, inv.owner_id, sum(inv.qty_on_hand) as qty
from inventory inv
inner join location loc on inv.location_id = loc.location_id and inv.site_id = loc.site_id
where inv.owner_id = 'CC33'
group by inv.sku_id, inv.owner_id
)
select 'SKU ID,DESCRIPTION,COLOUR,SKU SIZE,203,3,310,4,2,900,33' A1 FROM DUAL
UNION ALL
SELECT SKU_ID || ',' || DESCRIPTION || ',' || COLOUR || ',' || SKU_SIZE || ',' || A203 || ',' || A3 || ',' || A310 || ',' || A4 || ',' || A2 || ',' || A900 || ',' || A33 A1
FROM (
select 
t1.sku_id
, t1.user_def_note_2 as DESCRIPTION
, t1.colour
, t1.sku_size
, nvl(t2.qty,0) as A203
, nvl(t3.qty,0) as A3
, nvl(t4.qty,0) as A310
, nvl(t5.qty,0) as A4
, nvl(t6.qty,0) as A2
, nvl(t7.qty,0) as A900
, nvl(t8.qty,0) as A33
from t1
left join t2 on t1.sku_id = t2.sku_id
left join t3 on t1.sku_id = t3.sku_id
left join t4 on t1.sku_id = t4.sku_id
left join t5 on t1.sku_id = t5.sku_id
left join t6 on t1.sku_id = t6.sku_id
left join t7 on t1.sku_id = t7.sku_id
left join t8 on t1.sku_id = t8.sku_id
)
/