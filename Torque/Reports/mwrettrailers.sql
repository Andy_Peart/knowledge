SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

select 'North/South,Total Units,Carton Prediction' from dual
union all
select "N/S" || ',' || "Total Units" || ',' || "Cartons"
from
(
select a.user_def_type_8            as "N/S"
, sum(ol.qty_tasked)                as "Total Units"
, ceil(sum(ol.qty_tasked)/20)       as "Cartons"
from order_header oh
inner join order_line ol on oh.client_id = ol.client_id and oh.order_id = ol.order_id
inner join address a on oh.client_id = a.client_id and oh.work_group = a.address_id
where oh.client_id = 'MOUNTAIN'
and oh.consignment = '&&2'
and oh.creation_date between trunc(sysdate)-3 and trunc(sysdate)+1
group by  a.user_def_type_8
)
/

--spool off