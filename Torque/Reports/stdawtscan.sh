#!/bin/bash
#
#   TORQUE - (STD) - Scanned Status for unshipped orders
#

timezone=$1
client=$2

reportname="${client}_OPEN_ORDER_SCAN_STATUS"
sqlfile="stdawtscan.sql"

datetime=$(date +%F_%H%M%S)
filename="${reportname}_${datetime}.csv"

sqlplus -s $ORACLE_USR << ! > ${HOME}/ReportOutput/results/${filename}
@$DCS_USERRPTDIR/${sqlfile} $client
exit
/
!

echo "File written to:"
echo "\\\\rp2\\user_reports\\${filename}"