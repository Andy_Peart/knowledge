set feedback off
set pagesize 68
set linesize 240
set verify off
set colsep ' '


clear columns
clear breaks
clear computes

column D heading 'Date' format a16
column A heading 'Reference' format A20
column A2 heading 'Order Type' format A12
column B heading 'Qty' format 999999
column B2 heading 'Lines' format 999999
column M noprint


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

set heading on

ttitle  LEFT 'Units Dispatched from '&&2' to '&&3' - for client ID &&4 as at ' report_date skip 2

break on D skip 1 on report

compute sum label 'Day Total' of B B2 on D
compute sum label 'Total' of B B2 on report

--spool hdunits.txt

select
trunc(it.Dstamp) D,
it.reference_id A,
oh.order_type A2,
count(*) B2,
sum(it.update_qty) B
from inventory_transaction it,order_header oh
where it.client_id='&&4'
and it.code='Shipment'
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.reference_id=oh.order_id
and oh.client_id='&&4'
group by
trunc(it.Dstamp),
it.reference_id,
oh.order_type
order by
trunc(it.Dstamp),
it.reference_id,
oh.order_type
/

--spool off

