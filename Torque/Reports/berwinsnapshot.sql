set feedback on


insert into berwin_snapshot(
the_date,the_sku,the_qty)
select
trunc(sysdate),
sku_id,
sum(qty_on_hand)
from inventory
where client_id='BW'
group by
sku_id
/

--rollback;
commit;
