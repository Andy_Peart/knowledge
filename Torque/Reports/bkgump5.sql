SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','
SET UND OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 320


Select 'SKU, Tag ID, Location, Client, Condition, Total Qty, Total Allocated' from DUAL;

column A format A14
column AA format A40
column B format A10
column C format A10
column D format A6
column E format A12
column F format 99999999
column G format 99999999
column M format a4 noprint



set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


Select
i.SKU_ID A,
sku.description AA,
nvl(i.Tag_ID,'') B,
i.Location_ID C,
i.Client_ID D,
i.Condition_ID E,
sum(i.Qty_on_hand) F,
sum(i.Qty_Allocated) G
from inventory i,sku
where i.client_id='TR'
and i.location_id like 'TML%'
and i.client_id=sku.client_id
and i.sku_id=sku.sku_id
group by
i.SKU_ID,
sku.description,
i.Tag_ID,
i.Location_ID,
i.Client_ID,
i.Condition_ID
order by
C
/
