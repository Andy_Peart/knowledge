SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON

SELECT 'CLIENT_ID','ORDER_ID','DATE CREATED','TIME CREATED','ORDER TYPE','NO OF LINES','QTY ORDERED','QTY SHIPPED' FROM DUAL
UNION ALL
SELECT
	*
FROM
	(
		SELECT
			odla.client_id
		, odla.order_id
		,TO_CHAR(TRUNC(odla.creation_date)) AS "date_created"
		,TO_CHAR(odla.creation_date, 'HH24:MI') AS "time_created"
		, ODHA.ORDER_TYPE
		, TO_CHAR(COUNT(odla.LINE_ID))   AS "lines"
		, TO_CHAR(SUM(odla.QTY_ORDERED)) AS "Qty Ordered"
		, TO_CHAR(SUM(NVL(odla.QTY_SHIPPED,0))) AS "Qty Shipped"
		FROM
			ORDER_LINE_ARCHIVE odla
		INNER JOIN ORDER_HEADER_ARCHIVE odha
		ON
			odla.order_id     =odha.order_id
			AND odla.client_id=odha.client_id
		WHERE
			odla.client_id = '&&2'
			AND ODHA.CREATION_DATE BETWEEN to_date('&&3', 'DD-MON-YYYY') AND to_date(
			'&&4', 'DD-MON-YYYY')+1
		GROUP BY
			odla.client_id
		, odla.order_id
		, TO_CHAR(TRUNC(odla.creation_date))
		,TO_CHAR(odla.creation_date, 'HH24:MI')
		, ODHA.ORDER_TYPE
		UNION ALL
		SELECT
			odla.client_id
		, odla.order_id
		,TO_CHAR(TRUNC(odla.creation_date)) AS "date_created"
		,TO_CHAR(odla.creation_date, 'HH24:MI') AS "time_created"
		, ODHA.ORDER_TYPE
		, TO_CHAR(COUNT(odla.LINE_ID))   AS "lines"
		, TO_CHAR(SUM(odla.QTY_ORDERED)) AS "Qty Ordered"
		, TO_CHAR(SUM(NVL(odla.QTY_SHIPPED,0))) AS "Qty Shipped"
		FROM
			ORDER_LINE odla
		INNER JOIN ORDER_HEADER odha
		ON
			odla.order_id     =odha.order_id
			AND odla.client_id=odha.client_id
		WHERE
			odla.client_id = '&&2'
			AND odha.CREATION_DATE BETWEEN to_date('&3', 'DD-MON-YYYY') AND to_date(
			'&&4', 'DD-MON-YYYY')+1
		GROUP BY
			odla.client_id
		, odla.order_id
		, TO_CHAR(TRUNC(odla.creation_date))
		,TO_CHAR(odla.creation_date, 'HH24:MI')
		, ODHA.ORDER_TYPE
		union all
	SELECT
			odla.client_id
		, odla.order_id
		,TO_CHAR(TRUNC(odla.creation_date)) AS "date_created"
		,TO_CHAR(odla.creation_date, 'HH24:MI') AS "time_created"
		, ODHA.ORDER_TYPE
		, TO_CHAR(COUNT(odla.LINE_ID))   AS "lines"
		, TO_CHAR(SUM(odla.QTY_ORDERED)) AS "Qty Ordered"
		, TO_CHAR(SUM(NVL(odla.QTY_SHIPPED,0))) AS "Qty Shipped"
		FROM
			ORDER_LINE_ARCHIVE@linux_live odla
		INNER JOIN ORDER_HEADER_ARCHIVE@linux_live odha
		ON
			odla.order_id     =odha.order_id
			AND odla.client_id=odha.client_id
		WHERE
			odla.client_id = '&&2'
			AND ODHA.CREATION_DATE BETWEEN to_date('&&3', 'DD-MON-YYYY') AND to_date(
			'&&4', 'DD-MON-YYYY')+1
		GROUP BY
			odla.client_id
		, odla.order_id
		, TO_CHAR(TRUNC(odla.creation_date))
		,TO_CHAR(odla.creation_date, 'HH24:MI')
		, ODHA.ORDER_TYPE
		UNION ALL
		SELECT
			odla.client_id
		, odla.order_id
		,TO_CHAR(TRUNC(odla.creation_date)) AS "date_created"
		,TO_CHAR(odla.creation_date, 'HH24:MI') AS "time_created"
		, ODHA.ORDER_TYPE
		, TO_CHAR(COUNT(odla.LINE_ID))   AS "lines"
		, TO_CHAR(SUM(odla.QTY_ORDERED)) AS "Qty Ordered"
		, TO_CHAR(SUM(NVL(odla.QTY_SHIPPED,0))) AS "Qty Shipped"
		FROM
			ORDER_LINE@linux_live odla
		INNER JOIN ORDER_HEADER@linux_live odha
		ON
			odla.order_id     =odha.order_id
			AND odla.client_id=odha.client_id
		WHERE
			odla.client_id = '&&2'
			AND odha.CREATION_DATE BETWEEN to_date('&3', 'DD-MON-YYYY') AND to_date(
			'&&4', 'DD-MON-YYYY')+1
		GROUP BY
			odla.client_id
		, odla.order_id
		, TO_CHAR(TRUNC(odla.creation_date))
		,TO_CHAR(odla.creation_date, 'HH24:MI')
		, ODHA.ORDER_TYPE
	)
ORDER BY
	2
/
