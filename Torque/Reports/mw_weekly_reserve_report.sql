SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 132

select 
A "SKU",
B "Actual Inventory Level",
to_number(s.user_def_num_4) "Reservation Level",
DECODE(SIGN(to_number(s.user_def_num_4)-B),-1,to_number(s.user_def_num_4),B) "Actual Res Inv Level"
from sku s,
(select iv.sku_id A, sum(iv.qty_on_hand) B
from inventory iv
where iv.client_id = 'MOUNTAIN'
and iv.location_id <> 'DESPATCH'
and iv.location_id <> 'SUSPENSE'
and iv.sku_id <> 'TEST'
group by iv.sku_id)
where s.sku_id = A
and s.client_id = 'MOUNTAIN'
and A <> 'TEST'
/