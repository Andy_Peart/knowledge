
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON

select
'A total of '||count(*)||' SKUS not in Zone 12'
from (select
distinct
sku_id SKU
from inventory
where client_id='MOUNTAIN')
where lower(SKU)=upper(SKU)
and SKU not in (select
distinct
sku_id
from inventory
where client_id='MOUNTAIN'
and zone_1='WEB')
/
