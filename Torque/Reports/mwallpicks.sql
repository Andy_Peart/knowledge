SET FEEDBACK OFF             
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 300
SET TRIMSPOOL ON

--spool mwallpicks.csv

select 'Picks from &&2 to &&3' from DUAL;
select 'SKU,LPG,Current Stock,WEB Units Picked,RETAIL Units Picked,Total Picks,Stock Turn' from DUAL;

select
sku.sku_id||','||
sku.user_def_type_8||','||
nvl(IQTY2,0)||','||
nvl(Q1,0)||','||
nvl(Q2,0)||','||
nvl(IQTY,0)||','||
CASE
WHEN nvl(IQTY,0)=0 THEN '0'
ELSE replace(to_char(nvl(IQTY2,0)/IQTY*GAP/7,'9990'),' ','') END
from sku,(select
to_date('&&3', 'DD-MON-YYYY')-to_date('&&2', 'DD-MON-YYYY') GAP,
it.sku_id ISKU,
sum(it.update_qty) IQTY,
sum(DECODE(work_group,'WWW',update_qty,0)) Q1,
sum(DECODE(work_group,'WWW',0,update_qty)) Q2
from inventory_transaction it
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='MOUNTAIN'
and it.code like 'Pick'
and elapsed_time>0
group by
it.sku_id), (select
sku_id ISKU2,
sum(qty_on_hand) IQTY2
from inventory
where client_id='MOUNTAIN'
group by sku_id)
where sku.client_id='MOUNTAIN'
and sku.sku_id=ISKU (+)
and sku.sku_id=ISKU2
order by 1
/


--spool off

