SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9999
SET TRIMSPOOL ON

column M noprint

--spool mworderact4.csv



select '00' M,
'Order Shipment Activity from &&2 to &&3' from Dual
union all
select '01' M,
'Date,,,Orders,Units'from Dual
union all
select
trunc(Dstamp)||'3'||DECODE(oh.priority,888,'NEXT DAY',' ')||','||substr(it.reference_id,1,3) M,
trunc(Dstamp)||','||
DECODE(oh.priority,888,'NEXT DAY',' ')||','||
substr(it.reference_id,1,3)||','||
count(distinct it.reference_id)||','||
sum(it.update_qty)  KK
from inventory_transaction it,order_header oh
where it.client_id='MOUNTAIN'
and it.code='Shipment'
and it.work_group='WWW'
--and substr(it.reference_id,1,3) in ('EBY','AMZ','WWW','PLA')
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.reference_id=oh.order_id
and oh.client_id='MOUNTAIN'
--and nvl(oh.priority,0)<>888
group by 
trunc(Dstamp),
DECODE(oh.priority,888,'NEXT DAY',' '),
substr(it.reference_id,1,3)
union all
select
trunc(Dstamp)||'5'||substr(it.reference_id,1,3) M,
trunc(Dstamp)||','||
'UPGRADED'||','||
substr(it.reference_id,1,3)||','||
count(distinct it.reference_id)||','||
sum(it.update_qty)
from inventory_transaction it,order_header oh
where it.client_id='MOUNTAIN'
and it.code='Pick'
and it.work_group='WWW'
--and substr(it.reference_id,1,3) in ('EBY','AMZ','WWW','PLA')
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.reference_id=oh.order_id
and it.to_loc_id ='UKPAR'
and oh.client_id='MOUNTAIN'
and oh.priority<>888
group by 
trunc(Dstamp),
substr(it.reference_id,1,3)
order by M
/

--spool off
