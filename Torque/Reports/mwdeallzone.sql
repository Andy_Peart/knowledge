SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 800
SET TRIMSPOOL ON


column A heading 'SKU' format a30
column B heading 'Location' format a20
column C heading 'Reference' format a30
column D heading 'Qty' format 999999
column E heading 'Date' format a30
column F heading 'Work Group' format a30
column G heading 'Consignment' format a30
column H heading 'Lines deallocated' format 999999
column I heading 'Zone' format a10

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON




break on I skip 1 dup on report

compute sum label 'Total' of H on I

select 'SKU, From Location, Reference, Update Qty, Txn Date, Work Group, Consignment, Lines deallocated, Zone' from DUAL;

select sku_id A, from_loc_id B, reference_id C, update_qty D, txn_date E, work_group F, consignment G, lines_dealloc H, work_zone I
from (
select loc.work_zone, it.sku_id, it.from_loc_id, it.reference_id, it.update_qty, to_char(dstamp,'DD-MM-YYYY') as txn_date, it.work_group, it.consignment, count(*) lines_dealloc
from inventory_transaction it inner join location loc on it.from_loc_id = loc.location_id and it.site_id = loc.site_id
where it.client_id = 'MOUNTAIN'
and it.code = 'Deallocate'
and it.Dstamp between to_date('&&2 &&3', 'DD-MON-YYYY HH24:MI:SS') and to_date('&&2 &&4', 'DD-MON-YYYY HH24:MI:SS')
AND it.consignment is not null
and it.work_group <> 'WWW'
group by loc.work_zone, it.sku_id, it.from_loc_id, it.reference_id, it.update_qty, to_char(dstamp,'DD-MM-YYYY'), it.work_group, it.consignment
order by work_zone
)
/
--spool off