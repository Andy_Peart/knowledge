SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'All Orders and units 00-00' from dual union all
select 'Orders,Units,Date' from dual union all
select "Ord" || ',' || "Qty" || ',' || "Date" from
(
select sum("Ord") as "Ord",sum("Qty") as "Qty",to_char("Date",'DD-MON-YYYY') as "Date" from
(
select count(distinct oh.order_id) as "Ord"
, sum(ol.qty_ordered) as "Qty"
, oh.creation_date as "Date"
from order_header oh
inner join order_line ol on ol.order_id = oh.order_id and ol.client_id = oh.client_id
where oh.client_id = 'MOUNTAIN'
and substr(oh.work_group,2,2) = 'WW'
and oh.creation_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by oh.creation_date
order by 3
)
group by to_char("Date",'DD-MON-YYYY')
)
union all select null from dual union all
select 'All Orders and units from 17-17' from dual union all
select 'Orders,Units,Date' from dual union all
select "Ord" || ',' || "Qty" || ',' || "Date" from
(
select sum("Ord") as "Ord",sum("Qty") as "Qty",to_char("Date",'DD-MON-YYYY') as "Date" from
(
select count(distinct oh.order_id)          as "Ord"
, sum(ol.qty_ordered)       as "Qty"
, case  when to_char(oh.creation_date, 'HH24:MI') > '17:00' then oh.creation_date+1
        else oh.creation_date end as "Date"
from order_header oh
inner join order_line ol on ol.order_id = oh.order_id and ol.client_id = oh.client_id
where oh.client_id = 'MOUNTAIN'
and substr(oh.work_group,2,2) = 'WW'
and oh.creation_date between to_date('&&2', 'DD-MON-YYYY')-1 and to_date('&&3', 'DD-MON-YYYY')+1
group by case  when to_char(oh.creation_date, 'HH24:MI') > '17:00' then oh.creation_date+1
        else oh.creation_date end
order by 3
)
group by to_char("Date",'DD-MON-YYYY')
)
/
--spool off