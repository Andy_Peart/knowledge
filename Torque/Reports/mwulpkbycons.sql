SET FEEDBACK OFF                 
set verify off
set wrap off
set colsep ','

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 132
set trimspool on


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



select 'Order, Status, Work Group, Consignment, Order Date, Number of Lines, Number of Units, Shipped Date'
from dual
union all
select order_id || ',' || status || ',' || work_group || ',' || consignment || ',' || order_date || ',' || lines || ',' || units || ',' || shipped_date
from (
select oh.order_id,  oh.status, oh.work_group, oh.consignment, to_char(oh.order_date, 'DD-MM-YYYY') as order_date,  count(*) lines, sum(it.update_qty) units, to_char(oh.shipped_date, 'DD-MM-YYYY') as shipped_date
from order_header oh INNER JOIN inventory_transaction it on oh.order_id = it.reference_id and oh.client_id = it.client_id
where oh.client_id IN ('AA', 'MOUNTAIN')
--and order_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and oh.consignment like '&&2%'
and it.reference_id like 'MOR%'
and it.update_qty<>0
and it.station_id not like 'Auto%'
and it.code in ('Pick','Consol')
and it.elapsed_time>0
and it.from_loc_id<>'P999'
group by oh.order_id,  oh.status, oh.work_group, oh.consignment, to_char(oh.order_date, 'DD-MM-YYYY'), to_char(oh.shipped_date, 'DD-MM-YYYY')
)
/