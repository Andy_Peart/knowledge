SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON


--spool

select 'Store name, Order Type, Order ID, Units' from dual
union all
select store_name || ',' || order_type || ',' || order_id || ',' || units
from (
select ad.NAME store_name, oh.order_type, oh.order_id, sum(ol.QTY_SHIPPED) units
from order_header oh inner join order_line ol on oh.client_id = ol.client_id and oh.order_id = ol.order_id
inner join address ad on ad.client_id = oh.client_id and ad.client_id = ol.client_id and ad.address_id = oh.customer_id
where oh.client_id = 'TR'
and oh.COUNTRY = '&&2'
AND OH.ORDER_ID LIKE 'F%'
and oh.SHIPPED_DATE between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
group by ad.NAME, oh.order_type, oh.order_id, oh.COUNTRy, ad.address_id
ORDER BY 1,2,3
)
/


--spool off