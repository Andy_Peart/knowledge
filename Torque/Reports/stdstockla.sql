SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 900
SET TRIMSPOOL ON
 

column A heading 'Location' format A11
column B heading 'SKU' format A30
column C heading 'Description' format A200
column D heading 'Qty' format 9999999
column E heading 'WorkZone' format A14

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

break on report

compute sum label 'Total' of D on report

select 'SKU,Description,Size,Colour,Location,Qty,Work Zone,Location Type,No of SKUs in location' from DUAL;

with t1 as (
    select location_id, count(distinct sku_id) cnt
    from inventory
    where client_id = '&&2'
    group by location_id
),
t2 as (
    select i.sku_id B, Sku.description C, sku_size C1, sku.colour C2, i.location_id A, sum(i.qty_on_hand) D, loc.work_zone E, loc.loc_type F
    from inventory i inner join location loc on i.location_id = loc.location_id and i.site_id = loc.site_id
    inner join sku on i.sku_id=sku.sku_id 
    and i.client_id=sku.client_id
    where i.client_id like '&&2'
    group by i.sku_id, Sku.description, sku_size,sku.colour, i.location_id, loc.work_zone, loc.loc_type
)
select t2.*, t1.cnt
from t2 left join t1 on t2.A = t1.location_id
order by B,C,C1,C2,A
/
