SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

 
column A format A10
column B format A15
column C format A28
column D format A20
column E format 9999999
column F format A18
column G format A40
column H format 999999999
column I format 999999999
column J format 999999999


ttitle left 'Goods Outstanding Report - for TM LEWIN' skip 1

select 'Date      ,Supplier Id    ,Supplier Name               ,Pre Advice ID       , Line ID,Product Code      ,Description                             ,   Ordered,  Received, Shortfall'
from dual;


select 
to_char(ph.Due_Dstamp, 'DD/MM/YY') A,
ph.supplier_id B,
ad.name C,
ph.pre_advice_id D,
pl.line_id E, 
pl.sku_id F, 
sku.description G,
pl.qty_due H,
pl.qty_received I,
pl.qty_due-pl.qty_received J
from pre_advice_header ph,pre_advice_line pl,sku,address ad
where ph.Due_Dstamp between to_date('&2', 'DD-MON-YYYY') and to_date('&3', 'DD-MON-YYYY')+1
and pl.qty_due>pl.qty_received
and ph.supplier_id=ad.address_id
and ph.pre_advice_id=pl.pre_advice_id
and pl.sku_id=sku.sku_id
order by ph.pre_advice_id,pl.line_id, to_char(ph.Due_Dstamp, 'DD/MM/YY')
/

