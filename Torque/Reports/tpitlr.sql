SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON

--spool tprec.csv

select 'ITL,E,Receipt'||','
--||decode(sign(A1), 1, '+', -1, '-', '+')||','
--||A1||','
||''||','
||''||','
||decode(sign(A2), 1, '+', -1, '-', '+')||','
||A2||','
||AA||'000000,'
||'TP'||','
||SS||','
||''||','
||''||','
||''||','
||''||','
||RR||','
||''||','
||CI||','
||''||','
--||RI||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
--||SI||','
||DECODE(SI2,'TAIWAN',SI2,SI)||','
||''||','
||PI||','
||''||','
||OI||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||SUP||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||U4||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||'1'||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
from (select
to_char(it.dstamp, 'YYYYMMDD') AA,
it.client_id CT,
it.sku_id SS,
it.reference_id RR,
it.condition_id CI,
it.site_id SI,
ph.user_def_type_1 SI2,
it.pallet_id PI,
it.owner_id OI,
it.supplier_id SUP,
'N' U4,
sum(it.original_qty) A1,
sum(it.update_qty) A2
from inventory_transaction it,pre_advice_header ph
where it.client_id = 'TP'
and (it.code = 'Receipt' or (it.code='Adjustment' and it.reason_id='RECRV'))
and it.tag_id not like 'RET%'
--and trunc(it.Dstamp)>trunc(sysdate)-40
and (it.uploaded = 'N'
or it.uploaded is null)
and it.reference_id=ph.pre_advice_id
and ph.client_id='TP'
--and it.reference_id in (select
--pre_advice_id from pre_advice_header
--where client_id ='TP')
group by
to_char(it.dstamp, 'YYYYMMDD'),
it.client_id,
it.sku_id,
it.reference_id,
it.condition_id,
it.site_id,
ph.user_def_type_1,
it.pallet_id,
it.owner_id,
it.supplier_id
union
select
to_char(it.dstamp, 'YYYYMMDD') AA,
it.client_id CT,
it.sku_id SS,
it.reference_id RR,
it.condition_id CI,
it.reason_id RI,
it.site_id SI,
it.pallet_id PI,
it.owner_id OI,
it.supplier_id SUP,
'Y' U4,
sum(it.original_qty) A1,
sum(it.update_qty) A2
from inventory_transaction it
where it.client_id = 'TP'
and (it.code = 'Receipt' or (it.code='Adjustment' and it.reason_id='RECRV'))
and it.tag_id not like 'RET%'
--and trunc(it.Dstamp)>trunc(sysdate)-40
and (it.uploaded = 'N'
or it.uploaded is null)
and it.reference_id not in (select
pre_advice_id from pre_advice_header
where client_id ='TP')
group by
to_char(it.dstamp, 'YYYYMMDD'),
it.client_id,
it.sku_id,
it.reference_id,
it.condition_id,
it.reason_id,
it.site_id,
it.pallet_id,
it.owner_id,
it.supplier_id)
where A2>0
order by
RR,SS
/

--spool off

--SET FEEDBACK ON

update inventory_transaction
set uploaded='Y'
where client_id = 'TP'
and (code = 'Receipt' or (code='Adjustment' and reason_id='RECRV'))
and (uploaded = 'N'
or uploaded is null)
/




commit;
--rollback;

