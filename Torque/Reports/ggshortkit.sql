SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Kit Component Shortage Report ' || to_char(sysdate,'dd/mm/yy  hh24:mi:ss') from dual
union all
select 'Order,Line Id,Sku Id,Order Qty,Tasked Qty,Kit Qty,Required,In Stock,Kit Sku Id,Description' from dual
union all
select * from
(
select
A1||','||
A2||','||
A3||','||
A31||','||
A4||','||
A5||','||
A6||','||
A7||','||
A8||','||
A9
from (
  select * from (
  select
  ol.order_id A1,
  ol.line_id A2,
  ol.sku_id A3,
  nvl(ol.qty_ordered,0) A31,
  nvl(ol.qty_tasked,0) A4,
  nvl(KITQTY,1) A5,
  nvl(ol.qty_ordered,0)*nvl(KITQTY,1) A6,
  nvl(QQQ,0) A7,
  nvl(KITSKU,ol.sku_id) A8,
  sku.description A9
  from order_header oh,order_line ol,(
  select
  kit_id KIT,
  sku_id KITSKU,
  quantity KITQTY
  from kit_line
  where client_id= '&&2'),sku,(select
  sku_id SSS,
  sum(qty_on_hand-qty_allocated) QQQ
  from inventory
  where client_id= '&&2'
  and lock_status = 'UnLocked'
  group by sku_id)
where oh.client_id= '&&2'
and oh.status in ('Allocated','Released')
and ol.client_id=oh.client_id
and ol.order_id=oh.order_id
and ol.sku_id=KIT (+)
and nvl(KITSKU,ol.sku_id)=sku.sku_id
and sku.client_id= '&&2'
and sku.sku_id=SSS (+)
and oh.client_id = '&&2'
)
order by A1,A2
)
where A4<A31
and A7<A6
)
/
--spool off