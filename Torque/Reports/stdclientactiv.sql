SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 999
SET TRIMSPOOL ON

select 'Date, User ID, Client, Site ID, Hour' from dual
union all
select date_ || ',' || user_id || ',' || client_id || ',' || site_id || ',' || hrs
from (
select to_char(dstamp,'DD-MM-YYYY') date_, user_id, client_id, site_id, to_char(dstamp,'HH24') hrs 
from inventory_transaction
where dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and user_id not in ('Allocdae','oraifdordae','oraifdpadae','Invupdate','Mvtcdae','Receive','Backorder') 
and client_id is not null
group by to_char(dstamp,'DD-MM-YYYY'), user_id, client_id, site_id, to_char(dstamp,'HH24')
order by 1,3
)
/

--spool off

