set pagesize 2000
set linesize 2000
set verify off
clear columns
clear breaks
clear computes

undef 2
undef 3
undef 4
undef 5


column A heading 'SKU' format a20
column B heading 'Qty' format 99999
column F heading 'Location' format A14

select i.sku_id A,
B,
i.location_id F
from inventory i,
(select inv.sku_id C,
sum(qty_on_hand) B
from inventory inv
group by inv.sku_id)
where client_id = '&&5'
and B < '&&2'
and i.sku_id = C
and i.location_id<>'DESPATCHPR'
and i.location_id<>'CONTAINER'
and i.location_id not like 'QC%'
and i.location_id>'&&3'
and i.location_id<'&&4'
group by i.sku_id, B, i.location_id
order by 1
/
