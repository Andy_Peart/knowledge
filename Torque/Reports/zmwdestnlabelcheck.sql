SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 66
SET NEWPAGE 0
SET LINESIZE 240

column A heading 'CONTAINER NO' format A12
column AA heading 'IncorrectLabelDest' format A20
column B heading 'CorrectDest' format A14
column C heading 'ORDER NO' format A12


select *
from yo_labelcheck a
/