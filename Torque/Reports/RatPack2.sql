SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240



select 'Ratio Check,Report - for,client ID &&2 and Pre Advice ID &&3' 
from dual;

select 'SKU Id,Bar Code,Description,Line ID,Qty Due,UserDef-3,SC,Size,Colour'
from dual;

select
ph.sku_id ||''','''||
sku.ean ||''','||
sku.description ||','||
ph.line_id ||','||
ph.qty_due ||','||
sku.user_def_type_3  ||','||
DECODE(substr(sku.sku_size,length(sku.sku_size)-4,5),'SHORT','1','H-REG','2',
'1-REG','2','2-REG','2','3-REG','2','4-REG','2','5-REG','2','6-REG','2',
'7-REG','2','8-REG','2','9-REG','2','0-REG','2','-LONG','3','XLONG','4','9') ||','||
sku.sku_size ||','||
sku.colour
from pre_advice_line ph,sku
where ph.client_id='&&2'
and ph.pre_advice_id='&&3'
and sku.client_id='&&2'
and ph.sku_id=sku.sku_id
order by
sku.user_def_type_3,
DECODE(substr(sku.sku_size,length(sku.sku_size)-4,5),'SHORT','1','H-REG','2',
'1-REG','2','2-REG','2','3-REG','2','4-REG','2','5-REG','2','6-REG','2',
'7-REG','2','8-REG','2','9-REG','2','0-REG','2','-LONG','3','XLONG','4','9'),
sku.sku_size,
sku.colour
/
