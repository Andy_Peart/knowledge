SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON

with T1 as
(
select order_id, count(*) containers
from (
select order_id, container_id, count(*)
from shipping_manifest
where client_id = 'MOUNTAIN'
and picked_dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+5
group by order_id, container_id
order by 1,2
)
group by order_id
union all
select order_id, count(*) containers
from (
select order_id, container_id, count(*)
from shipping_manifest_archive
where client_id = 'MOUNTAIN'
and picked_dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+5
group by order_id, container_id
order by 1,2
)
group by order_id
),
T_arch as (
select oh.creation_date, oh.client_id
||','|| oh.work_group
||','|| oh.consignment
||','|| to_char(oh.creation_date, 'DD/MM/YYYY')
||','|| to_char(oh.creation_date, 'HH24:MI')
||','|| to_char(oh.last_update_date, 'DD/MM/YYYY')  
||','|| to_char(oh.last_update_date, 'HH24:MI') 
||','|| to_char(oh.shipped_date, 'DD/MM/YYYY')  
||','|| to_char(oh.shipped_date, 'HH24:MI') 
||','|| case when oh.client_id = 'SS' and oh.order_type = 'RETAIL' then oh.postcode else oh.customer_id end 
||','|| oh.order_id
||','|| oh.order_type
||','|| '' 
||','|| case when oh.client_id = 'GG' then oh.country else oh.dispatch_method end
||','|| to_char(oh.priority)
||','|| oh.ship_dock
||','|| oh.status
||','|| to_char(1) 
||','|| to_char(count(ol.line_id)) 
||','|| to_char(sum(ol.qty_ordered))
||','|| to_char(sum(nvl(ol.qty_shipped,0)))
||','|| 
case 
  when oh.status = 'Shipped' then to_char(sum(nvl(ol.qty_ordered,0)-nvl(ol.qty_shipped,0)))
  else '0' 
end 
||','|| to_char(sum(nvl(ol.qty_tasked,0))) 
||','|| nvl(T1.containers,0)
||','|| nvl(OH.single_order_sortation,'N') AS ARCH_COLUMNS
from order_header_archive oh inner join order_line_archive ol on oh.order_id=ol.order_id and oh.client_id=ol.client_id
left join T1 on oh.order_id = T1.order_id
where oh.client_id = 'MOUNTAIN'
and oh.creation_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by oh.client_id
, oh.work_group
, oh.consignment
, to_char(oh.creation_date, 'DD/MM/YYYY')
, to_char(oh.creation_date, 'HH24:MI')
, to_char(oh.last_update_date, 'DD/MM/YYYY')  
, to_char(oh.last_update_date, 'HH24:MI') 
, to_char(oh.shipped_date, 'DD/MM/YYYY')  
, to_char(oh.shipped_date, 'HH24:MI') 
, case when oh.client_id = 'SS' and oh.order_type = 'RETAIL' then oh.postcode else oh.customer_id end
, oh.order_id
, oh.order_type
,''
, oh.country
, oh.dispatch_method
, to_char(oh.priority)
, oh.ship_dock
, oh.status
, to_char(1)
, T1.containers
, oh.creation_date
, OH.SINGLE_ORDER_SORTATION
order by oh.creation_date
),
T_current as (
select oh.creation_date, oh.client_id
||','|| oh.work_group
||','|| oh.consignment
||','|| to_char(oh.creation_date, 'DD/MM/YYYY')
||','|| to_char(oh.creation_date, 'HH24:MI')
||','|| to_char(oh.last_update_date, 'DD/MM/YYYY')  
||','|| to_char(oh.last_update_date, 'HH24:MI') 
||','|| to_char(oh.shipped_date, 'DD/MM/YYYY')  
||','|| to_char(oh.shipped_date, 'HH24:MI') 
||','|| case when oh.client_id = 'SS' and oh.order_type = 'RETAIL' then oh.postcode else oh.customer_id end
||','|| oh.order_id
||','|| oh.order_type
||','|| '' 
||','|| case when oh.client_id = 'GG' then oh.country else oh.dispatch_method end
||','|| to_char(oh.priority)
||','|| oh.ship_dock
||','|| oh.status
||','|| to_char(1) 
||','|| to_char(count(ol.line_id)) 
||','|| to_char(sum(ol.qty_ordered))
||','|| to_char(sum(nvl(ol.qty_shipped,0)))
||','|| 
case 
  when oh.status = 'Shipped' then to_char(sum(nvl(ol.qty_ordered,0)-nvl(ol.qty_shipped,0)))
  else '0' 
end 
||','|| to_char(sum(nvl(ol.qty_tasked,0))) 
||','|| nvl(T1.containers,0) 
||','|| nvl(OH.single_order_sortation,'N') AS CURRENT_COLUMNS
from order_header oh inner join order_line ol on oh.order_id=ol.order_id and oh.client_id=ol.client_id
left join T1 on oh.order_id = T1.order_id
where oh.client_id = 'MOUNTAIN'
and oh.creation_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by oh.client_id
, oh.work_group
, oh.consignment
, to_char(oh.creation_date, 'DD/MM/YYYY')
, to_char(oh.creation_date, 'HH24:MI')
, to_char(oh.last_update_date, 'DD/MM/YYYY')  
, to_char(oh.last_update_date, 'HH24:MI') 
, to_char(oh.shipped_date, 'DD/MM/YYYY')  
, to_char(oh.shipped_date, 'HH24:MI') 
, case when oh.client_id = 'SS' and oh.order_type = 'RETAIL' then oh.postcode else oh.customer_id end 
, oh.order_id
, oh.order_type
,''
, oh.country
, oh.dispatch_method
, to_char(oh.priority)
, oh.ship_dock
, oh.status
, to_char(1)
, T1.containers
, oh.creation_date
, single_order_sortation
ORDER BY oh.creation_date
)
select 
'Client,Work Group,Consignment,Creation Date,Creation Time,Last Update Date,Last Update Time,Shipped Date,Shipped Time,Customer id,Order,Order Type,Hub,Dispatch Method,Priority,Ship Dock,Status,Orders,Lines,Order qty,Shipped qty,Short,Allocated qty,Containers,Single Order Sortation'
from dual
union all
SELECT ARCH_COLUMNS FROM T_ARCH
union all
SELECT CURRENT_COLUMNS FROM T_CURRENT
/
