SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

select 'Date,Reference,Units' from DUAL;


column A format A14
column A1 format A14
column B format 99999
column M format A4 noprint


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



--ttitle  LEFT 'TML Retail - Open Orders Report ' RIGHT 'Printed ' report_date skip 2


break on A1 on report

compute sum label 'Daily Units' of B on A1
compute sum label 'Total Units' of B on report


select 
to_char(Dstamp, 'YYMM') M,
to_char(dstamp,'DD-MM-YYYY') A1,
reference_id A,
sum(update_qty) B
from inventory_transaction
where client_id='SB'
and station_id not like 'Auto%'
and code like 'Receipt%'
and reference_id like 'C%'
and dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by
to_char(Dstamp, 'YYMM'),
to_char(dstamp,'DD-MM-YYYY'),
reference_id
order by
to_char(Dstamp, 'YYMM'),
to_char(dstamp,'DD-MM-YYYY'),
reference_id
/

select '' from DUAL;
select '' from DUAL;
select '' from DUAL;

select 
to_char(Dstamp, 'YYMM') M,
to_char(dstamp,'DD-MM-YYYY') A1,
reference_id A,
sum(update_qty) B
from inventory_transaction
where client_id='SB'
and station_id not like 'Auto%'
and code like 'Receipt%'
and reference_id like 'SR%'
and reference_id not like 'SR0000%'
and dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by
to_char(Dstamp, 'YYMM'),
to_char(dstamp,'DD-MM-YYYY'),
reference_id
order by
to_char(Dstamp, 'YYMM'),
to_char(dstamp,'DD-MM-YYYY'),
reference_id
/
select '' from DUAL;
select '' from DUAL;
select '' from DUAL;

select 
to_char(Dstamp, 'YYMM') M,
to_char(dstamp,'DD-MM-YYYY') A1,
reference_id A,
sum(update_qty) B
from inventory_transaction
where client_id='SB'
and station_id not like 'Auto%'
and code like 'Receipt%'
and reference_id like 'SR0000%'
and dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by
to_char(Dstamp, 'YYMM'),
to_char(dstamp,'DD-MM-YYYY'),
reference_id
order by
to_char(Dstamp, 'YYMM'),
to_char(dstamp,'DD-MM-YYYY'),
reference_id
/
