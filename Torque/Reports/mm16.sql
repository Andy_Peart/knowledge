/******************************************************************************/
/*                                                                            */
/*   Logistics and Internet Systems Ltd                                       */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics and Internet Systems Ltd  */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Internet   */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Internet Systems Limited, 2003               */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/
 
/******************************************************************************/
/*                                                                            */
/*                             LIS                                            */
/*                                                                            */
/*     FILE NAME  :     mm16.sql   	                                      */
/*                                                                            */
/*     DESCRIPTION:    Datastream to output all available data for            */
/*                     a Delivery Note (Shipping Documentation)               */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   12/07/07 CVH  DCS    SCRnnnnnn Cloned from shippingdoc.sql		      */
/*                                  for Morrison McConnell (carton sequence   */
/******************************************************************************/
/* Input Parameters are as follows */
-- 1) Timezone Name
-- 2) Client ID
-- 3) Order ID
-- 4) Destination ID(s)
-- 5) Number of Copies

 
/* Setup time zone */
@logintimezonesetup.sql '&&1'


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 0
SET VERIFY OFF
SET FEEDBACK OFF
SET TAB OFF

 
-- Set the line length to the maximum possible, but trim 
-- trailing spaces for both screen output and spool files
SET LINESIZE 32767
SET WRAP ON
SET TRIMOUT ON
SET TRIMSPOOL ON

/*********************** IMPORTANT ***********************/
/*                                                       */
/* You must ensure that no single row exceeds 1024 bytes.*/
/* This is why the lines below are split into separate   */
/* UNIONs, and their current maximum length is given.    */
/* Any row longer than 1024 bytes will simply not appear.*/
/* (Incidentally, this is an Oracle limitation, not ours)*/
/*                                                       */
/*********************** IMPORTANT ***********************/

/*********************** NOTE ALSO ***********************/
/* The general principle of these UNIONS is to use       */
/* UNION ALL to ensure we get all possible duplicate rows*/
/* where available (eg. Shipping Manifest, Inventory),but*/
/* then we use 'Distinct' on the other selects that we   */
/* know have unique values (eg. SKU, Order Header/Line)  */
/* since UNION ALL and UNION can affect eachother.       */
/*********************** NOTE ALSO ***********************/

/*********************** NOTE ALSO ***********************/
/* If you add any new columns to the output of this      */
/* datastream, then you must also update the relevant    */
/* section in the ".dsc" file for Complier     		 */
/*********************** NOTE ALSO ***********************/


/* Define the format of the output dates */
DEFINE	OutputDateFormat = 'DD/MM/YYYY'
COLUMN	sorter	NOPRINT	

SELECT 	Distinct '0' sorter, 'DSTREAM_MM16_HDR'	||'|'||
	rtrim('&&4')				||'|'|| /* Destination ID(s) */
	rtrim('&&5')				||'|'|| /* Number of copies  */
	rtrim('&&2')				||'|'|| /* Client ID         */
	rtrim('&&3')					/* Order ID          */
FROM DUAL
UNION ALL
SELECT 	Distinct '0' sorter, 
	'DSTREAM_MM16_LINE_1_ORDER_HEADER_1' 		||'|'|| 
	rtrim(OH.CLIENT_ID)					||'|'||
	rtrim(OH.ORDER_ID)					||'|'||
	rtrim(OH.ORDER_TYPE)					||'|'||
	rtrim(OH.STATUS)					||'|'||
	rtrim(nvl(PRIORITY, 0))					||'|'||
	rtrim(nvl(REPACK, 'N'))					||'|'||
	rtrim(OH.SHIP_DOCK)					||'|'||
	rtrim(OH.WORK_GROUP)					||'|'||
	rtrim(OH.CONSIGNMENT)					||'|'||
	rtrim(OH.DELIVERY_POINT)				||'|'||
	rtrim(OH.LOAD_SEQUENCE)					||'|'||
	rtrim(OH.FROM_SITE_ID)					||'|'||
	rtrim(OH.TO_SITE_ID)					||'|'||
	rtrim(OH.OWNER_ID)					||'|'||
	rtrim(OH.CUSTOMER_ID)					||'|'||
	rtrim(to_char(OH.ORDER_DATE, '&&OutputDateFormat'))	||'|'||
	rtrim(to_char(OH.SHIP_BY_DATE,'&&OutputDateFormat'))	||'|'||
	rtrim(to_char(OH.DELIVER_BY_DATE,'&&OutputDateFormat'))	||'|'||
	rtrim(to_char(OH.SHIPPED_DATE, '&&OutputDateFormat'))	||'|'||
	rtrim(to_char(OH.DELIVERED_DSTAMP,'&&OutputDateFormat'))||'|'||
	rtrim(OH.SIGNATORY)					||'|'||
	rtrim(OH.PURCHASE_ORDER)				||'|'||
	rtrim(OH.CARRIER_ID)					||'|'||
	rtrim(OH.DISPATCH_METHOD)				||'|'||
	rtrim(OH.SERVICE_LEVEL)					||'|'||
	rtrim(nvl(FASTEST_CARRIER, 'N'))			||'|'||
	rtrim(nvl(CHEAPEST_CARRIER, 'N'))			||'|'||
	rtrim(OH.CONTACT)					||'|'||
	rtrim(OH.CONTACT_PHONE)					||'|'||
	rtrim(OH.CONTACT_FAX)					||'|'||
	rtrim(OH.CONTACT_EMAIL)					||'|'||
	rtrim(OH.NAME)						||'|'||
	rtrim(OH.ADDRESS1)					||'|'||
	rtrim(OH.ADDRESS2)					||'|'||
	rtrim(OH.TOWN)						||'|'||
	rtrim(OH.COUNTY)					||'|'||
	rtrim(OH.POSTCODE)					||'|'||
	rtrim(OH.COUNTRY)					||'|'||
	rtrim(OH.INSTRUCTIONS)					||'|'||
	rtrim(nvl(OH.ORDER_VOLUME, 0))				||'|'||
	rtrim(nvl(OH.ORDER_WEIGHT, 0))				||'|'||
	rtrim(OH.FREIGHT_CHARGES)				||'|'||
	rtrim(nvl(OH.EXPORT, 'N'))				||'|'||
	rtrim(nvl(OH.HIGHEST_LABEL, 0))				||'|'||
	rtrim(OH.CE_REASON_CODE)				||'|'||
	rtrim(OH.CE_REASON_NOTES)				||'|'||
	rtrim(OH.CE_ORDER_TYPE)					||'|'||
	rtrim(L.TEXT)
FROM 	ORDER_HEADER OH, LANGUAGE_TEXT L
WHERE	OH.Client_ID = '&&2'
AND 	OH.Order_ID = '&&3'
AND	L.LABEL(+) = 'WLK' || nvl(OH.COUNTRY,'zz')
AND	L.LANGUAGE(+) = 'EN_GB'
UNION ALL
SELECT 	Distinct '0' sorter, 
	'DSTREAM_MM16_LINE_1_ORDER_HEADER_2' 			||'|'||
	rtrim(OH.CLIENT_ID)					||'|'||
	rtrim(OH.ORDER_ID)					||'|'||
	rtrim(OH.INV_ADDRESS_ID)				||'|'||
	rtrim(OH.INV_CONTACT)					||'|'||
	rtrim(OH.INV_CONTACT_PHONE)				||'|'||
	rtrim(OH.INV_CONTACT_FAX)				||'|'||
	rtrim(OH.INV_CONTACT_EMAIL)				||'|'||
	rtrim(OH.INV_NAME)					||'|'||
	rtrim(OH.INV_ADDRESS1)					||'|'||
	rtrim(OH.INV_ADDRESS2)					||'|'||
	rtrim(OH.INV_TOWN)					||'|'||
	rtrim(OH.INV_COUNTY)					||'|'||
	rtrim(OH.INV_POSTCODE)					||'|'||
	rtrim(OH.INV_COUNTRY)					||'|'||
	rtrim(L.TEXT)
FROM 	ORDER_HEADER OH, LANGUAGE_TEXT L
WHERE	OH.Client_ID = '&&2'
AND 	OH.Order_ID = '&&3'
AND	L.LABEL(+) = 'WLK' || nvl(OH.INV_COUNTRY,'zz')
AND	L.LANGUAGE(+) = 'EN_GB'
UNION ALL
SELECT 	Distinct '0' sorter, 
	'DSTREAM_MM16_LINE_1_ORDER_HEADER_3' 		||'|'||
	rtrim(OH.CLIENT_ID)					||'|'||
	rtrim(OH.ORDER_ID)					||'|'||
	rtrim(OH.USER_DEF_TYPE_1)				||'|'||
	rtrim(OH.USER_DEF_TYPE_2)				||'|'||
	rtrim(OH.USER_DEF_TYPE_3)				||'|'||
	rtrim(OH.USER_DEF_TYPE_4)				||'|'||
	rtrim(OH.USER_DEF_TYPE_5)				||'|'||
	rtrim(OH.USER_DEF_TYPE_6)				||'|'||
	rtrim(OH.USER_DEF_TYPE_7)				||'|'||
	rtrim(OH.USER_DEF_TYPE_8)				||'|'||
	rtrim(nvl(OH.USER_DEF_CHK_1, 'N'))			||'|'||
	rtrim(nvl(OH.USER_DEF_CHK_2, 'N'))			||'|'||
	rtrim(nvl(OH.USER_DEF_CHK_3, 'N'))			||'|'||
	rtrim(nvl(OH.USER_DEF_CHK_4, 'N'))			||'|'||
	rtrim(to_char(OH.USER_DEF_DATE_1,'&&OutputDateFormat'))	||'|'||
	rtrim(to_char(OH.USER_DEF_DATE_2,'&&OutputDateFormat'))	||'|'||
	rtrim(to_char(OH.USER_DEF_DATE_3,'&&OutputDateFormat'))	||'|'||
	rtrim(to_char(OH.USER_DEF_DATE_4,'&&OutputDateFormat'))	||'|'||
	rtrim(nvl(OH.USER_DEF_NUM_1, 0))			||'|'||
	rtrim(nvl(OH.USER_DEF_NUM_2, 0))			||'|'||
	rtrim(nvl(OH.USER_DEF_NUM_3, 0))			||'|'||
	rtrim(nvl(OH.USER_DEF_NUM_4, 0))			||'|'||
	rtrim(OH.USER_DEF_NOTE_1)				||'|'||
	rtrim(OH.USER_DEF_NOTE_2)				
FROM 	ORDER_HEADER OH
WHERE	OH.Client_ID = '&&2'
AND 	OH.Order_ID = '&&3'
UNION ALL
SELECT 	Distinct OL.CLIENT_ID||OL.ORDER_ID||lpad(OL.LINE_ID,6) sorter, 
	'DSTREAM_MM16_LINE_2_ORDER_LINE_1'		||'|'||
	rtrim(OL.CLIENT_ID)					||'|'||
	rtrim(OL.ORDER_ID)					||'|'||
	rtrim(nvl(OL.LINE_ID, 0))				||'|'||
	rtrim(OL.SKU_ID)					||'|'||
	rtrim(OL.CUSTOMER_SKU_ID)				||'|'||
	rtrim(OL.CONFIG_ID)					||'|'||
	rtrim(OL.TRACKING_LEVEL)				||'|'||
	rtrim(OL.BATCH_ID)					||'|'||
	rtrim(OL.ORIGIN_ID)					||'|'||
	rtrim(OL.CONDITION_ID)					||'|'||
	rtrim(OL.LOCK_CODE)					||'|'||
	rtrim(nvl(OL.QTY_ORDERED, 0))				||'|'||
	rtrim(nvl(OL.QTY_TASKED, 0))				||'|'||
	rtrim(nvl(OL.QTY_PICKED, 0))				||'|'||
	rtrim(nvl(OL.QTY_SHIPPED, 0))				||'|'||
	rtrim(nvl(OL.QTY_DELIVERED, 0))				||'|'||
	rtrim(nvl(OL.LINE_VALUE, 0))				||'|'||
	rtrim(OL.NOTES)						||'|'||
	rtrim(OL.RULE_ID)					||'|'||
	rtrim(nvl(OL.CATCH_WEIGHT, 0))                        	||'|'||
	rtrim(OL.SPEC_CODE)
FROM 	ORDER_LINE OL
WHERE   OL.Order_ID = '&&3'
AND     OL.Client_ID = '&&2'
UNION ALL
SELECT 	Distinct OL.CLIENT_ID||OL.ORDER_ID||lpad(OL.LINE_ID,6) sorter,
	'DSTREAM_MM16_LINE_2_ORDER_LINE_2'		||'|'||
	rtrim(OL.CLIENT_ID)					||'|'||
	rtrim(OL.ORDER_ID)					||'|'||
	rtrim(nvl(OL.LINE_ID, 0))				||'|'||
	rtrim(OL.USER_DEF_TYPE_1)				||'|'||
	rtrim(OL.USER_DEF_TYPE_2)				||'|'||
	rtrim(OL.USER_DEF_TYPE_3)				||'|'||
	rtrim(OL.USER_DEF_TYPE_4)				||'|'||
	rtrim(OL.USER_DEF_TYPE_5)				||'|'||
	rtrim(OL.USER_DEF_TYPE_6)				||'|'||
	rtrim(OL.USER_DEF_TYPE_7)				||'|'||
	rtrim(OL.USER_DEF_TYPE_8)				||'|'||
	rtrim(nvl(OL.USER_DEF_CHK_1, 'N'))			||'|'||
	rtrim(nvl(OL.USER_DEF_CHK_2, 'N'))			||'|'||
	rtrim(nvl(OL.USER_DEF_CHK_3, 'N'))			||'|'||
	rtrim(nvl(OL.USER_DEF_CHK_4, 'N'))			||'|'||
	rtrim(to_char(OL.USER_DEF_DATE_1,'&&OutputDateFormat'))	||'|'||
	rtrim(to_char(OL.USER_DEF_DATE_2,'&&OutputDateFormat'))	||'|'||
	rtrim(to_char(OL.USER_DEF_DATE_3,'&&OutputDateFormat'))	||'|'||
	rtrim(to_char(OL.USER_DEF_DATE_4,'&&OutputDateFormat'))	||'|'||
	rtrim(nvl(OL.USER_DEF_NUM_1, 0))			||'|'||
	rtrim(nvl(OL.USER_DEF_NUM_2, 0))			||'|'||
	rtrim(nvl(OL.USER_DEF_NUM_3, 0))			||'|'||
	rtrim(nvl(OL.USER_DEF_NUM_4, 0))			||'|'||
	rtrim(OL.USER_DEF_NOTE_1)				||'|'||
	rtrim(OL.USER_DEF_NOTE_2)
FROM 	ORDER_LINE OL
WHERE   OL.Order_ID = '&&3'
AND     OL.Client_ID = '&&2'
UNION ALL
SELECT 	Distinct OL.CLIENT_ID||OL.ORDER_ID||lpad(OL.LINE_ID,6) sorter,
	'DSTREAM_MM16_LINE_3_SKU_1'			||'|'||
	rtrim(S.CLIENT_ID)					||'|'||
	rtrim(S.SKU_ID)						||'|'||
	rtrim(S.EAN)						||'|'||
	rtrim(S.UPC)						||'|'||
	rtrim(S.DESCRIPTION)					||'|'||
	rtrim(S.PRODUCT_GROUP)					||'|'||
	rtrim(nvl(S.EACH_HEIGHT, 0))				||'|'||
	rtrim(nvl(S.EACH_WEIGHT, 0))				||'|'||
	rtrim(nvl(S.EACH_VOLUME, 0))				||'|'||
	rtrim(nvl(S.EACH_VALUE, 0))				||'|'||
	rtrim(S.QC_STATUS)					||'|'||
	rtrim(nvl(S.SHELF_LIFE, 0))				||'|'||
	rtrim(nvl(S.QC_FREQUENCY, 0))				||'|'||
	rtrim(nvl(S.QC_REC_COUNT, 0))				||'|'||
	rtrim(nvl(S.SPLIT_LOWEST, 'N'))				||'|'||
	rtrim(nvl(S.CONDITION_REQD, 'N'))			||'|'||
	rtrim(nvl(S.EXPIRY_REQD, 'N'))				||'|'||
	rtrim(nvl(S.ORIGIN_REQD, 'N'))				||'|'||
	rtrim(nvl(S.SERIAL_AT_PACK, 'N'))			||'|'||
	rtrim(nvl(S.SERIAL_AT_PICK, 'N'))			||'|'||
	rtrim(nvl(S.SERIAL_AT_RECEIPT, 'N'))			||'|'||
	rtrim(S.SERIAL_RANGE)					||'|'||
	rtrim(S.SERIAL_FORMAT)					||'|'||
	rtrim(nvl(S.SERIAL_VALID_MERGE, 'N'))			||'|'||
	rtrim(nvl(S.SERIAL_NO_REUSE, 'N'))			||'|'||
	rtrim(nvl(S.PICK_COUNT_QTY, 0))				||'|'||
	rtrim(nvl(S.COUNT_FREQUENCY, 0))			||'|'||
	rtrim(to_char(S.COUNT_DSTAMP, '&&OutputDateFormat'))	||'|'||
	rtrim(S.COUNT_LIST_ID)					||'|'||
	rtrim(nvl(S.OAP_WIP_ENABLED, 'N'))			||'|'||
	rtrim(nvl(S.KIT_SKU, 'N'))				||'|'||
	rtrim(nvl(S.KIT_SPLIT, 'N'))				||'|'||
	rtrim(nvl(S.KIT_TRIGGER_QTY, 0))			||'|'||
	rtrim(nvl(S.KIT_QTY_DUE, 0))				||'|'||
	rtrim(S.KITTING_LOC_ID)					||'|'||
	rtrim(S.ALLOCATION_GROUP)				||'|'||
	rtrim(S.PUTAWAY_GROUP)					||'|'||
	rtrim(nvl(S.ABC_DISABLE, 'N'))				||'|'||
	rtrim(S.HANDLING_CLASS)					||'|'||
	rtrim(nvl(S.OBSOLETE_PRODUCT, 'N'))			||'|'||
	rtrim(nvl(S.NEW_PRODUCT, 'N'))				||'|'||
	rtrim(nvl(S.DISALLOW_UPLOAD, 'N'))			||'|'||
	rtrim(nvl(S.DISALLOW_CROSS_DOCK, 'N'))			||'|'||
	rtrim(nvl(S.MANUF_DSTAMP_REQD, 'N'))			||'|'||
	rtrim(nvl(S.MANUF_DSTAMP_DFLT, 'N'))			||'|'||
	rtrim(nvl(S.MIN_SHELF_LIFE, 0))				||'|'||
	rtrim(S.COLOUR)						||'|'||
	rtrim(S.SKU_SIZE)					||'|'||
	rtrim(nvl(S.HAZMAT, 'N'))				||'|'||
	rtrim(S.HAZMAT_ID)					||'|'||
	rtrim(nvl(S.SHIP_SHELF_LIFE, 0))			||'|'||
	rtrim(nvl(S.NMFC_NUMBER, 0))				||'|'||
	rtrim(S.INCUB_RULE)					||'|'||
	rtrim(nvl(S.INCUB_HOURS, 0))				||'|'||
	rtrim(nvl(S.EACH_WIDTH, 0))				||'|'||
	rtrim(nvl(S.EACH_DEPTH, 0))				||'|'||
	rtrim(nvl(S.REORDER_TRIGGER_QTY, 0))			||'|'||
	rtrim(nvl(S.LOW_TRIGGER_QTY, 0))			||'|'||
	rtrim(nvl(S.DISALLOW_MERGE_RULES, 'N'))			||'|'||
	rtrim(nvl(S.PACK_DESPATCH_REPACK, 'N'))			||'|'||
	rtrim(S.SPEC_ID)					||'|'||
	rtrim(S.CE_WAREHOUSE_TYPE)				||'|'||
	rtrim(S.CE_CUSTOMS_EXCISE)				||'|'||
	rtrim(nvl(S.CE_STANDARD_COST, 0))			||'|'||
	rtrim(S.CE_STANDARD_CURRENCY)
FROM 	SKU S, ORDER_LINE OL
WHERE   OL.Order_ID = '&&3'
AND     OL.Client_ID = '&&2'
AND     OL.SKU_ID = S.SKU_ID
AND     OL.Client_ID = S.Client_ID
UNION ALL
SELECT 	Distinct OL.CLIENT_ID||OL.ORDER_ID||lpad(OL.LINE_ID,6) sorter,
	'DSTREAM_MM16_LINE_3_SKU_2'			||'|'||
	rtrim(S.CLIENT_ID)					||'|'||
	rtrim(S.SKU_ID)						||'|'||
	rtrim(S.USER_DEF_TYPE_1)				||'|'||
	rtrim(S.USER_DEF_TYPE_1)				||'|'||
	rtrim(S.USER_DEF_TYPE_2)				||'|'||
	rtrim(S.USER_DEF_TYPE_3)				||'|'||
	rtrim(S.USER_DEF_TYPE_4)				||'|'||
	rtrim(S.USER_DEF_TYPE_5)				||'|'||
	rtrim(S.USER_DEF_TYPE_6)				||'|'||
	rtrim(S.USER_DEF_TYPE_7)				||'|'||
	rtrim(S.USER_DEF_TYPE_8)				||'|'||
	rtrim(nvl(S.USER_DEF_CHK_1, 'N'))			||'|'||
	rtrim(nvl(S.USER_DEF_CHK_2, 'N'))			||'|'||
	rtrim(nvl(S.USER_DEF_CHK_3, 'N'))			||'|'||
	rtrim(nvl(S.USER_DEF_CHK_4, 'N'))			||'|'||
	rtrim(to_char(S.USER_DEF_DATE_1, '&&OutputDateFormat'))	||'|'||
	rtrim(to_char(S.USER_DEF_DATE_2, '&&OutputDateFormat'))	||'|'||
	rtrim(to_char(S.USER_DEF_DATE_3, '&&OutputDateFormat'))	||'|'||
	rtrim(to_char(S.USER_DEF_DATE_4, '&&OutputDateFormat'))	||'|'||
	rtrim(nvl(S.USER_DEF_NUM_1, 0))				||'|'||
	rtrim(nvl(S.USER_DEF_NUM_2, 0))				||'|'||
	rtrim(nvl(S.USER_DEF_NUM_3, 0))				||'|'||
	rtrim(nvl(S.USER_DEF_NUM_4, 0))				||'|'||
	rtrim(S.USER_DEF_NOTE_1)				||'|'||
	rtrim(S.USER_DEF_NOTE_2)
FROM 	SKU S, ORDER_LINE OL
WHERE   OL.Order_ID = '&&3'
AND     OL.Client_ID = '&&2'
AND     OL.SKU_ID = S.SKU_ID
AND     OL.Client_ID = S.Client_ID
UNION ALL
SELECT 	Distinct OL.CLIENT_ID||OL.ORDER_ID||lpad(OL.LINE_ID,6) sorter,
	'DSTREAM_MM16_LINE_4_SKU_CONFIG'			||'|'||
 	rtrim(SC.CLIENT_ID)					||'|'||
 	rtrim(SSC.SKU_ID)					||'|'||
	rtrim	(
		nvl((nvl (SC.Ratio_1_to_2, 1) 
		* nvl (SC.Ratio_2_to_3, 1) 
		* nvl (SC.Ratio_3_to_4, 1) 
		* nvl (SC.Ratio_4_to_5, 1) 
		* nvl (SC.Ratio_5_to_6, 1) 
		* nvl (SC.Ratio_6_to_7, 1) 
		* nvl (SC.Ratio_7_to_8, 1)),  0)
		)						||'|'||
 	rtrim(SC.CONFIG_ID)					||'|'||
 	rtrim(nvl(SC.TAG_VOLUME, 0))				||'|'||
 	rtrim(nvl(SC.VOLUME_AT_EACH, 'N'))			||'|'||
 	rtrim(SC.TRACK_LEVEL_1)					||'|'||
 	rtrim(nvl(SC.RATIO_1_TO_2, 0))				||'|'||
 	rtrim(SC.TRACK_LEVEL_2)					||'|'||
 	rtrim(nvl(SC.RATIO_2_TO_3, 0))				||'|'||
 	rtrim(SC.TRACK_LEVEL_3)					||'|'||
 	rtrim(nvl(SC.RATIO_3_TO_4, 0))				||'|'||
 	rtrim(SC.TRACK_LEVEL_4)					||'|'||
 	rtrim(nvl(SC.RATIO_4_TO_5, 0))				||'|'||
 	rtrim(SC.TRACK_LEVEL_5)					||'|'||
 	rtrim(nvl(SC.RATIO_5_TO_6, 0))				||'|'||
 	rtrim(SC.TRACK_LEVEL_6)					||'|'||
 	rtrim(nvl(SC.RATIO_6_TO_7, 0))				||'|'||
 	rtrim(SC.TRACK_LEVEL_7)					||'|'||
 	rtrim(nvl(SC.RATIO_7_TO_8, 0))				||'|'||
 	rtrim(SC.TRACK_LEVEL_8)					||'|'||
 	rtrim(nvl(SC.EACH_PER_LAYER, 0))			||'|'||
 	rtrim(nvl(SC.LAYER_HEIGHT, 0))				||'|'||
 	rtrim(nvl(SC.SPLIT_LOWEST, 'N'))			||'|'||
 	rtrim(nvl(SC.VOLUME_2, 0))				||'|'||
 	rtrim(nvl(SC.WEIGHT_2, 0))				||'|'||
 	rtrim(nvl(SC.HEIGHT_2, 0))				||'|'||
 	rtrim(nvl(SC.WIDTH_2,  0))				||'|'||
 	rtrim(nvl(SC.DEPTH_2,  0))				||'|'||
 	rtrim(nvl(SC.VOLUME_3, 0))				||'|'||
 	rtrim(nvl(SC.WEIGHT_3, 0))				||'|'||
 	rtrim(nvl(SC.HEIGHT_3, 0))				||'|'||
 	rtrim(nvl(SC.WIDTH_3,  0))				||'|'||
 	rtrim(nvl(SC.DEPTH_3,  0))				||'|'||
 	rtrim(nvl(SC.VOLUME_4, 0))				||'|'||
 	rtrim(nvl(SC.WEIGHT_4, 0))				||'|'||
 	rtrim(nvl(SC.HEIGHT_4, 0))				||'|'||
 	rtrim(nvl(SC.WIDTH_4,  0))				||'|'||
 	rtrim(nvl(SC.DEPTH_4,  0))				||'|'||
 	rtrim(nvl(SC.VOLUME_5, 0))				||'|'||
 	rtrim(nvl(SC.WEIGHT_5, 0))				||'|'||
 	rtrim(nvl(SC.HEIGHT_5, 0))				||'|'||
 	rtrim(nvl(SC.WIDTH_5,  0))				||'|'||
 	rtrim(nvl(SC.DEPTH_5,  0))				||'|'||
 	rtrim(nvl(SC.VOLUME_6, 0))				||'|'||
 	rtrim(nvl(SC.WEIGHT_6, 0))				||'|'||
 	rtrim(nvl(SC.HEIGHT_6, 0))				||'|'||
 	rtrim(nvl(SC.WIDTH_6,  0))				||'|'||
 	rtrim(nvl(SC.DEPTH_6,  0))				||'|'||
 	rtrim(nvl(SC.VOLUME_7, 0))				||'|'||
 	rtrim(nvl(SC.WEIGHT_7, 0))				||'|'||
 	rtrim(nvl(SC.HEIGHT_7, 0))				||'|'||
 	rtrim(nvl(SC.WIDTH_7,  0))				||'|'||
 	rtrim(nvl(SC.DEPTH_7,  0))
FROM	SKU_SKU_CONFIG SSC, SKU_CONFIG SC, ORDER_LINE OL
WHERE	OL.Client_ID = '&&2'
AND	OL.Order_ID = '&&3'
AND 	SSC.SKU_ID = OL.SKU_ID
AND 	SSC.Client_ID = OL.Client_ID
AND 	SSC.Config_ID = SC.Config_ID
AND 	(SC.Client_ID = OL.Client_ID or SC.Client_ID is null)
UNION ALL
SELECT 	SM.CLIENT_ID||SM.ORDER_ID||lpad(SM.LINE_ID,6)||SM.KEY sorter,
	'DSTREAM_MM16_LINE_5_MANIFEST_1'		 	||'|'||
	rtrim(SM.CLIENT_ID)                         		||'|'||
	rtrim(SM.ORDER_ID)                         		||'|'||
	rtrim(nvl(SM.LINE_ID, 0))                      		||'|'||
	rtrim(SM.TAG_ID)                         		||'|'||
	rtrim(SM.SKU_ID)                         		||'|'||
	rtrim(SM.CONFIG_ID)                         		||'|'||
	rtrim(SM.BATCH_ID)                         		||'|'||
	rtrim(to_char(SM.EXPIRY_DSTAMP, '&&OutputDateFormat')) 	||'|'||
	rtrim(SM.WORK_GROUP)                         		||'|'||
	rtrim(SM.CONSIGNMENT)                         		||'|'||
	rtrim(SM.CONTAINER_ID)                         		||'|'||
	rtrim(SM.PALLET_ID)                         		||'|'||
	rtrim(SM.PALLET_CONFIG)                        		||'|'||
	rtrim(nvl(SM.PALLET_VOLUME, 0))                        	||'|'||
	rtrim(nvl(SM.PALLET_HEIGHT, 0))                        	||'|'||
	rtrim(nvl(SM.PALLET_DEPTH,  0))                        	||'|'||
	rtrim(nvl(SM.PALLET_WIDTH,  0))                        	||'|'||
	rtrim(nvl(SM.PALLET_WEIGHT, 0))                        	||'|'||
	rtrim(SM.SITE_ID)                         		||'|'||
	rtrim(SM.LOCATION_ID)                         		||'|'||
	rtrim(SM.OWNER_ID)                         		||'|'||
	rtrim(nvl(SM.QTY_PICKED, 0))                         	||'|'||
	rtrim(to_char(SM.PICKED_DSTAMP, '&&OutputDateFormat')) 	||'|'||
	rtrim(nvl(SM.QTY_SHIPPED, 0))                         	||'|'||
	rtrim(to_char(SM.SHIPPED_DSTAMP, '&&OutputDateFormat'))	||'|'||
	rtrim(nvl(SM.SHIPPED, 'N'))                         	||'|'||
	rtrim(nvl(SM.QTY_DELIVERED, 0))                        	||'|'||
	rtrim(to_char(SM.DELIVERED_DSTAMP,'&&OutputDateFormat'))||'|'||
	rtrim(nvl(SM.DELIVERED, 'N'))                         	||'|'||
	rtrim(nvl(SM.POD_CONFIRMED, 'N'))                      	||'|'||
	rtrim(SM.POD_EXCEPTION_REASON)                         	||'|'||
	rtrim(SM.STATION_ID)                         		||'|'||
	rtrim(SM.USER_ID)                         		||'|'||
	rtrim(SM.TRAILER_POSITION)                         	||'|'||
	rtrim(SM.RECEIPT_ID)                         		||'|'||
	rtrim(to_char(SM.RECEIPT_DSTAMP, '&&OutputDateFormat'))	||'|'||
	rtrim(to_char(SM.MANUF_DSTAMP, '&&OutputDateFormat'))  	||'|'||
	rtrim(SM.SUPPLIER_ID)                         		||'|'||
	rtrim(SM.ORIGIN_ID)                         		||'|'||
	rtrim(SM.CONDITION_ID)                         		||'|'||
	rtrim(SM.LOCK_STATUS)                         		||'|'||
	rtrim(SM.LOCK_CODE)                         		||'|'||
	rtrim(SM.BOL_ID)                         		||'|'||
	rtrim(SM.NOTES)                         		||'|'||
	rtrim(nvl(SM.FROM_LABEL, 0))                         	||'|'||
	rtrim(nvl(SM.TO_LABEL, 0))                         	||'|'||
	rtrim(SM.SPEC_CODE)                         		||'|'||
	rtrim(SM.CE_ROTATION_ID)                  		||'|'||
	rtrim(SM.CE_CONSIGNMENT_ID) 		               	||'|'||
	rtrim(SM.CE_RECEIPT_TYPE)              		    	||'|'||
	rtrim(SM.CE_ORIGINATOR)                  		||'|'||
	rtrim(SM.CE_ORIGINATOR_REFERENCE)        	  	||'|'||
	rtrim(SM.CE_COO)           			 	||'|'||
	rtrim(SM.CE_CWC) 			                ||'|'||
	rtrim(SM.CE_UCR)        			        ||'|'||
	rtrim(SM.CE_UNDER_BOND)                  		||'|'||
	rtrim(nvl(SM.CATCH_WEIGHT, 0))                        	||'|'||
	rtrim(to_char(SM.CE_DOCUMENT_DSTAMP, '&&OutputDateFormat'))
FROM 	SHIPPING_MANIFEST SM
WHERE	SM.Client_ID = '&&2'
AND 	SM.Order_ID = '&&3'
UNION ALL
SELECT 	SM.CLIENT_ID||SM.ORDER_ID||lpad(SM.LINE_ID,6)||SM.KEY sorter,
	'DSTREAM_MM16_LINE_5_MANIFEST_2'		 	||'|'||
	rtrim(SM.CLIENT_ID)                         		||'|'||
	rtrim(SM.ORDER_ID)                         		||'|'||
	rtrim(SM.USER_DEF_TYPE_1)                         	||'|'||
	rtrim(SM.USER_DEF_TYPE_2)                         	||'|'||
	rtrim(SM.USER_DEF_TYPE_3)                         	||'|'||
	rtrim(SM.USER_DEF_TYPE_4)                         	||'|'||
	rtrim(SM.USER_DEF_TYPE_5)                         	||'|'||
	rtrim(SM.USER_DEF_TYPE_6)                         	||'|'||
	rtrim(SM.USER_DEF_TYPE_7)                         	||'|'||
	rtrim(SM.USER_DEF_TYPE_8)                         	||'|'||
	rtrim(nvl(SM.USER_DEF_CHK_1, 'N'))                     	||'|'||
	rtrim(nvl(SM.USER_DEF_CHK_2, 'N'))                     	||'|'||
	rtrim(nvl(SM.USER_DEF_CHK_3, 'N'))                     	||'|'||
	rtrim(nvl(SM.USER_DEF_CHK_4, 'N'))                     	||'|'||
	rtrim(to_char(SM.USER_DEF_DATE_1, '&&OutputDateFormat'))||'|'||
	rtrim(to_char(SM.USER_DEF_DATE_2, '&&OutputDateFormat'))||'|'||
	rtrim(to_char(SM.USER_DEF_DATE_3, '&&OutputDateFormat'))||'|'||
	rtrim(to_char(SM.USER_DEF_DATE_4, '&&OutputDateFormat'))||'|'||
	rtrim(nvl(SM.USER_DEF_NUM_1, 0))                        ||'|'||
	rtrim(nvl(SM.USER_DEF_NUM_2, 0))                        ||'|'||
	rtrim(nvl(SM.USER_DEF_NUM_3, 0))                        ||'|'||
	rtrim(nvl(SM.USER_DEF_NUM_4, 0))                        ||'|'||
	rtrim(SM.USER_DEF_NOTE_1)                         	||'|'||
	rtrim(SM.USER_DEF_NOTE_2)
FROM 	SHIPPING_MANIFEST SM
WHERE	SM.Client_ID = '&&2'
AND 	SM.Order_ID = '&&3'
UNION ALL
SELECT 	Distinct SN.CLIENT_ID||SN.ORDER_ID||lpad(SN.LINE_ID,6)||SN.SERIAL_NUMBER sorter,
	'DSTREAM_MM16_LINE_6_SERIAL_NUMBERS'	||'|'||
	rtrim(SN.CLIENT_ID)					||'|'||
	rtrim(SN.ORDER_ID)					||'|'||
	rtrim(nvl(SN.LINE_ID, 0))				||'|'||
	rtrim(SN.SERIAL_NUMBER)					||'|'||
	rtrim(SN.SKU_ID)					||'|'||
	rtrim(SN.TAG_ID)					||'|'||
	rtrim(nvl(SN.PICK_KEY, 0))				||'|'||
	rtrim(SN.STATUS)					||'|'||
	rtrim(SN.SITE_ID)					||'|'||
	rtrim(to_char(SN.RECEIPT_DSTAMP, '&&OutputDateFormat'))	||'|'||
	rtrim(to_char(SN.PICKED_DSTAMP, '&&OutputDateFormat'))	||'|'||
	rtrim(to_char(SN.SHIPPED_DSTAMP, '&&OutputDateFormat'))	||'|'||
	rtrim(nvl(SN.REPACKED, 'N'))				||'|'||
	rtrim(nvl(SN.CREATED, 'N'))
FROM 	SERIAL_NUMBER SN
WHERE	SN.Client_ID = '&&2'
AND 	SN.Order_ID = '&&3'
UNION ALL
SELECT 	Distinct RMH.CLIENT_ID||RMH.ORDER_ID sorter,
	'DSTREAM_MM16_LINE_6_REPACK_MANIFEST_HEADER'	||'|'||
	rtrim(RMH.CLIENT_ID)					||'|'||
	rtrim(RMH.ORDER_ID)					||'|'||
	rtrim(RMH.PALLET_ID)					||'|'||
	rtrim(nvl(RMH.HEIGHT, 0))				||'|'||
	rtrim(nvl(RMH.DEPTH, 0))				||'|'||
	rtrim(nvl(RMH.WIDTH, 0))				||'|'||
	rtrim(nvl(RMH.WEIGHT, 0))				||'|'||
	rtrim(RMH.PALLET_CONFIG)				||'|'||
	rtrim(nvl(RMH.UPLOADED, 'N'))				||'|'||
	rtrim(RMH.PALLET_NUMBER)				
FROM	REPACK_MANIFEST_HEADER RMH
WHERE	RMH.Client_ID = '&&2'
AND	RMH.Order_ID = '&&3'
UNION ALL
SELECT 	Distinct RML.CLIENT_ID||RML.ORDER_ID sorter,
	'DSTREAM_MM16_LINE_7_REPACK_MANIFEST_LINE'	||'|'||
	rtrim(RML.CLIENT_ID)					||'|'||
	rtrim(RML.ORDER_ID)					||'|'||
	rtrim(RML.PALLET_ID)					||'|'||
	rtrim(RML.CONTAINER_ID)					||'|'||
	rtrim(RML.SKU_ID)					||'|'||
	rtrim(RML.TAG_ID)					||'|'||
	rtrim(RML.CONDITION_ID)					||'|'||
	rtrim(RML.ORIGIN_ID)					||'|'||
	rtrim(RML.BATCH_ID)					||'|'||
	rtrim(nvl(RML.QTY, 0))					
FROM	REPACK_MANIFEST_LINE RML
WHERE	RML.Client_ID = '&&2'
AND	RML.Order_ID = '&&3'	
ORDER BY 1,2
/
