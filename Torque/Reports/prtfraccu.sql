/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :     prtfraccu.sql                           		      */
/*                                                                            */
/*     DESCRIPTION:                                                           */
/*     CSV version compares shipments from pr to received in t or tr          */
/*     TORQUE (PR) Transfer Accuracy Report                                   */
/*   DATE       BY   DESCRIPTION					                          */
/*   ========== ==== ===========				                              */
/*   25/03/2013 YB   David Hy, sql created into RP report 					  */
/******************************************************************************/
SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','
SET WRAP OFF


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
 

-- break on report

-- compute sum label 'Total' of D E I J on report

--set TERMOUT OFF
--column curdate NEW_VALUE report_date
select 'TORQUE (PR) Transfer Accuracy Report ',to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
--set TERMOUT ON

--SPOOL PR2TML_TRANSFER_.CSV

--Top Title  
select 'STATUS','PR ORDER','DOCKET','PRODUCT','STYLE REF','DESCRIPTION','QTY_ORDERED','QTY_TASKED','QTY_PICKED','TML_DIV','PRE_ADVICE_ID','PRODUCT','STATUS','USER_DEF_TYPE_5','QTY_DUE','QTY_RECEIVED','DIFF' from DUAL;



--DH: not proud of this SQL, added complexity with the totals 
select * from
(
select * from
( 
select * from /*Transactions for PR in progress and complete, append any T/TR orders regardless of status*/
( 
SELECT 
       PR.status, PR.order_id, PR.docket,   PR.sku_id, PR.Style, PR.description, PR.qty_ordered, PR.qty_tasked, PR.qty_picked , 
       TML.client_id as TML_client_id, TML.pre_advice_id as TML_pre_advice_id,  TML.sku_id as TML_sku_id,
       TML.status as TML_status, TML.user_def_type_5 as TML_user_def_type_5, TML.qty_due as TML_qty_due, TML.qty_received as TML_qty_received, TML.diff 
from 
( 
select 
a.status, b.order_id, b.user_def_type_4 as docket, b.sku_id, c.user_def_type_5 as style, c.description, sum(b.qty_ordered) qty_ordered, sum(b.qty_tasked) qty_tasked, sum(b.qty_picked) qty_picked  
from order_header a 
inner join order_line b on a.client_id = b.client_id and a.order_id = b.order_id 
inner join sku c on c.client_id = a.client_id and c.sku_id = b.sku_id 
where a.client_id = 'PR' 
and c.user_def_type_2 = 'TML' 
and a.status in ('In Progress','Complete') 
group by a.status, b.order_id, b.user_def_type_4, b.sku_id, c.description, c.user_def_type_5
) PR 
left join 
( 
select 
a.client_id, b.pre_advice_id, b.sku_id ,a.status, b.user_def_type_5, sum(b.qty_due) as qty_due, sum(b.qty_received) as qty_received, sum(nvl(b.qty_received,0) - nvl(b.qty_due,0)) as diff 
from pre_advice_header a 
inner join pre_advice_line b on a.client_id = b.client_id and a.pre_advice_id = b.pre_advice_id 
inner join sku c on c.client_id = a.client_id and c.sku_id = b.sku_id 
where a.client_id in ('T','TR') 
and a.supplier_id = 'PROMIN' 
group by a.client_id, b.pre_advice_id, b.sku_id ,a.status, b.user_def_type_5
) TML on PR.order_id = TML.pre_advice_id and PR.sku_id = TML.sku_id 
union all
SELECT 
      null, PR.order_id, null, null,null, null,   sum(PR.qty_ordered),  sum(PR.qty_tasked),  sum(PR.qty_picked) ,  /*Sub Totals*/
      null, null, null, null, null, sum(TML.qty_due),  sum(TML.qty_received),  sum(TML.diff) 
from 
( 
select 
a.status, b.order_id, b.user_def_type_4 as docket, b.sku_id, c.user_def_type_5 as style, c.description, b.qty_ordered, b.qty_tasked, b.qty_picked 
from order_header a 
inner join order_line b on a.client_id = b.client_id and a.order_id = b.order_id 
inner join sku c on c.client_id = a.client_id and c.sku_id = b.sku_id 
where a.client_id = 'PR' 
and c.user_def_type_2 = 'TML' 
and a.status in ('In Progress','Complete') 
) PR 
left join 
( 
select 
a.client_id, b.pre_advice_id, b.sku_id ,a.status, b.user_def_type_5, b.qty_due, b.qty_received, nvl(b.qty_received,0) - nvl(b.qty_due,0) as diff 
from pre_advice_header a 
inner join pre_advice_line b on a.client_id = b.client_id and a.pre_advice_id = b.pre_advice_id 
inner join sku c on c.client_id = a.client_id and c.sku_id = b.sku_id 
where a.client_id in ('T','TR') 
and a.supplier_id = 'PROMIN' 
) TML on PR.order_id = TML.pre_advice_id and PR.sku_id = TML.sku_id 
group by PR.order_id
)  
union  
select * from  /*Transactions for T/TR in progress, append any PR orders regardless of status*/
( 
select PR.status, PR.order_id, PR.docket, PR.sku_id, PR.style, PR.description, PR.qty_ordered, PR.qty_tasked, PR.qty_picked , 
       TML.client_id as TML_client_id, TML.pre_advice_id as TML_pre_advice_id,   TML.sku_id as TML_sku_id,
       TML.status as TML_status, TML.user_def_type_5 as TML_user_def_type_5, TML.qty_due as TML_qty_due, TML.qty_received as TML_qty_received, TML.diff 
from 
( 
select 
 a.client_id , b.pre_advice_id, b.sku_id,a.status, b.user_def_type_5, sum(b.qty_due) as qty_due, sum(b.qty_received) as qty_received, sum(nvl(b.qty_received,0) - nvl(b.qty_due,0)) as diff 
from pre_advice_header a 
inner join pre_advice_line b on a.client_id = b.client_id and a.pre_advice_id = b.pre_advice_id 
inner join sku c on c.client_id = a.client_id and c.sku_id = b.sku_id 
where a.client_id in ('T','TR') 
and a.supplier_id = 'PROMIN' 
and a.status in ('In Progress' ) 
group by a.client_id , b.pre_advice_id, b.sku_id,a.status, b.user_def_type_5
) TML left join 
( 
select 
 a.status, b.order_id, b.user_def_type_4 as docket, b.sku_id, c.user_def_type_5 as style, c.description, sum(b.qty_ordered) as qty_ordered, sum(b.qty_tasked) as qty_tasked, sum(b.qty_picked) as qty_picked
from order_header a 
inner join order_line b on a.client_id = b.client_id and a.order_id = b.order_id 
inner join sku c on c.client_id = a.client_id and c.sku_id = b.sku_id 
where a.client_id = 'PR' 
and c.user_def_type_2 = 'TML' 
group by  a.status, b.order_id, b.user_def_type_4, b.sku_id, c.description, c.user_def_type_5
) PR ON PR.ORDER_ID = TML.PRE_ADVICE_ID AND PR.SKU_ID = TML.SKU_ID 
union all
SELECT 
      null, null, null, null,null,null,   sum(PR.qty_ordered),  sum(PR.qty_tasked),  sum(PR.qty_picked) , /*Sub Totals*/
      null, TML.pre_advice_id, null, null, null, sum(TML.qty_due),  sum(TML.qty_received),  sum(TML.diff) 
from 
( 
select 
 a.client_id , b.pre_advice_id, b.sku_id,a.status, b.user_def_type_5, b.qty_due, b.qty_received, nvl(b.qty_received,0) - nvl(b.qty_due,0) as diff 
from pre_advice_header a 
inner join pre_advice_line b on a.client_id = b.client_id and a.pre_advice_id = b.pre_advice_id 
inner join sku c on c.client_id = a.client_id and c.sku_id = b.sku_id 
where a.client_id in ('T','TR') 
and a.supplier_id = 'PROMIN' 
and a.status in ('In Progress' ) 
) TML left join 
( 
select 
 a.status, b.order_id, b.user_def_type_4 as docket, b.sku_id, c.description, b.qty_ordered, b.qty_tasked, b.qty_picked 
from order_header a 
inner join order_line b on a.client_id = b.client_id and a.order_id = b.order_id 
inner join sku c on c.client_id = a.client_id and c.sku_id = b.sku_id 
where a.client_id = 'PR' 
and c.user_def_type_2 = 'TML' 
) PR ON PR.ORDER_ID = TML.PRE_ADVICE_ID AND PR.SKU_ID = TML.SKU_ID  
group by TML.pre_advice_id
) 
) order by nvl(Order_id,TML_pre_advice_id)  , nvl(status,TML_status)
) 
union all 
select null, null, null, null,null, null,   sum(qty_ordered),  sum(qty_tasked),  sum(qty_picked) , /*Totals*/
      null, null, null, null,null,  sum(TML_qty_due),  sum(TML_qty_received),  sum(diff) 
from
(
SELECT 
       PR.status, PR.order_id, PR.docket, PR.sku_id, PR.description, PR.qty_ordered, PR.qty_tasked, PR.qty_picked , 
       TML.client_id as TML_client_id, TML.pre_advice_id as TML_pre_advice_id,  TML.sku_id as TML_sku_id,
       TML.status as TML_status, TML.user_def_type_5 as TML_user_def_type_5, TML.qty_due as TML_qty_due, TML.qty_received as TML_qty_received, TML.diff 
from 
( 
select 
a.status, b.order_id, b.user_def_type_4 as docket, b.sku_id, c.description, b.qty_ordered, b.qty_tasked, b.qty_picked 
from order_header a 
inner join order_line b on a.client_id = b.client_id and a.order_id = b.order_id 
inner join sku c on c.client_id = a.client_id and c.sku_id = b.sku_id 
where a.client_id = 'PR' 
and c.user_def_type_2 = 'TML' 
and a.status in ('In Progress','Complete') 
) PR 
left join 
( 
select 
a.client_id, b.pre_advice_id, b.sku_id ,a.status, b.user_def_type_5, b.qty_due, b.qty_received, nvl(b.qty_received,0) - nvl(b.qty_due,0) as diff 
from pre_advice_header a 
inner join pre_advice_line b on a.client_id = b.client_id and a.pre_advice_id = b.pre_advice_id 
inner join sku c on c.client_id = a.client_id and c.sku_id = b.sku_id 
where a.client_id in ('T','TR') 
and a.supplier_id = 'PROMIN' 
) TML on PR.order_id = TML.pre_advice_id and PR.sku_id = TML.sku_id  
union  
select PR.status, PR.order_id, PR.docket,  PR.sku_id, PR.description, PR.qty_ordered, PR.qty_tasked, PR.qty_picked , 
       TML.client_id as TML_client_id, TML.pre_advice_id as TML_pre_advice_id,  TML.sku_id as TML_sku_id,
       TML.status as TML_status, TML.user_def_type_5 as TML_user_def_type_5, TML.qty_due as TML_qty_due, TML.qty_received as TML_qty_received, TML.diff 
from 
( 
select 
 a.client_id , b.pre_advice_id, b.sku_id,a.status, b.user_def_type_5, b.qty_due, b.qty_received, nvl(b.qty_received,0) - nvl(b.qty_due,0) as diff 
from pre_advice_header a 
inner join pre_advice_line b on a.client_id = b.client_id and a.pre_advice_id = b.pre_advice_id 
inner join sku c on c.client_id = a.client_id and c.sku_id = b.sku_id 
where a.client_id in ('T','TR') 
and a.supplier_id = 'PROMIN' 
and a.status in ('In Progress' ) 
) TML left join 
( 
select 
 a.status, b.order_id, b.user_def_type_4 as docket, b.sku_id, c.description, b.qty_ordered, b.qty_tasked, b.qty_picked 
from order_header a 
inner join order_line b on a.client_id = b.client_id and a.order_id = b.order_id 
inner join sku c on c.client_id = a.client_id and c.sku_id = b.sku_id 
where a.client_id = 'PR' 
and c.user_def_type_2 = 'TML' 
) PR ON PR.ORDER_ID = TML.PRE_ADVICE_ID AND PR.SKU_ID = TML.SKU_ID 
) t
/