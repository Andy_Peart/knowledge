SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 120

column A heading 'PO' format a12
column B heading 'Code' format a14
column C heading 'Qty' format 999999
column D heading 'Date' format a12

--break on A skip 2 on AA dup skip 1 on report

--compute sum label 'Total' of C on A
--compute sum label 'Total' of C on AA
--compute sum label 'Total' of C on report

--set termout off
--spool TR070Full.csv

select 'TR70 - All Retail receipts to date as at : '||to_char(sysdate,'dd-Mon-yyyy') from Dual;

select 'PreAdvice,Product Code,Qty,Receipt Date' from DUAL;

SELECT 
it.REFERENCE_ID, 
SKU.USER_DEF_TYPE_3, 
sum(it.UPDATE_QTY), 
to_char(it.DSTAMP,'dd-Mon-yyyy')
from inventory_transaction it,sku
where it.client_id='TR'
and it.code='Receipt'
and it.client_id=sku.client_id
and it.sku_id=sku.sku_id
group by
it.REFERENCE_ID, 
SKU.USER_DEF_TYPE_3, 
to_char(it.DSTAMP,'dd-Mon-yyyy')
order by
it.REFERENCE_ID, 
SKU.USER_DEF_TYPE_3
/

--spool off
--SET TERMOUT ON
