SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON


column A heading 'SKU' format a16
column B heading 'Date' format a12
column B2 heading 'Date' format a12
column M format a12 noprint
column D heading 'Units' format 999999
column D2 heading 'Units' format 999999
column G heading 'Days' format 999
column F heading '   Rate' format a7
column E heading 'Charge' format 999999.99

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



break on A dup skip 1 on report

compute sum label 'SKU Charge' of  D2 E on A
compute sum label 'Total Charge' of E on report

spool timexstorage.csv

select 'SKU,Date Received,Qty Received,Date Shipped,Qty Shipped,Days,Rate,Charge' from DUAL;

select
A,
B,
sum(D) D,
B2,
sum(D2) D2,
--DYS,
G,
F,
sum(E) E
from (select
R1 A,
R4 B,
R5 D,
DYS,
nvl(S4,'') B2,
nvl(S5,0) D2,
DECODE(sign(nvl(S4+1,trunc(sysdate))-R4-7+DYS),'-1',0,nvl(S4+1,trunc(sysdate))-R4-7+DYS) G,
'0.015/7' F,
CASE WHEN S5 is null THEN
   (DECODE(sign(nvl(S4+1,trunc(sysdate))-R4-7+DYS),'-1',0,nvl(S4+1,trunc(sysdate))-R4-7+DYS))*R5*0.015/7 
ELSE
   (DECODE(sign(nvl(S4+1,trunc(sysdate))-R4-7+DYS),'-1',0,nvl(S4+1,trunc(sysdate))-R4-7+DYS))*S5*0.015/7 
END E
from (select
it.sku_id S1,
it.tag_id S2,
max(trunc(it.Dstamp)) S4,
sum(it.update_qty) S5
from inventory_transaction it
where it.client_id='TIMEX'
and to_char(it.Dstamp,'mm/yyyy')=to_char(sysdate-1,'mm/yyyy')
and it.code='Shipment'
group by
it.sku_id,
it.tag_id
--trunc(it.Dstamp)
),(select
it.sku_id R1,
it.tag_id R2,
min(trunc(it.Dstamp)) R4,
0 DYS,
sum(it.update_qty) R5
from inventory_transaction it
where it.client_id='TIMEX'
and it.station_id not like 'Auto%'
and to_char(it.Dstamp,'mm/yyyy')=to_char(sysdate-1,'mm/yyyy')
and (it.code='Receipt' or (it.code='Adjustment' and it.reason_id in ('ADJ','RECRV','STK CHECK')))
group by
it.sku_id,
it.tag_id
--trunc(it.Dstamp)
union
select
the_sku||' B/F' R1,
the_tag R2,
trunc(the_date) R4,
--trunc(the_receipt_date) R4A,
DECODE(SIGN(trunc(the_date)-trunc(the_receipt_date)-7),1,7,trunc(the_date)-trunc(the_receipt_date)) DYS,
the_qty R5
from timex_snapshot
where the_date=to_date(('01'||to_char(sysdate-1,'-mon-yyyy')),'DD-MON-YYYY'))
where R1=S1(+)
and R2=S2(+)
and R5>0)
group by
A,
B,
B2,
--DYS,
G,
F
order by
A,B
/


spool off

