SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

select 'Date,User,Job,Minutes,Notes,Tasks,Units' from dual
union all
select "Date" || ',' || "User" || ',' || "Job" || ',' || "Minutes" || ',' || "Notes" || ',' || "Tasks" || ',' || "Units"
from
(
select "Date"
, "User"
, "Job"
, sum(round((to_date("endTime",'HH24:MI') - to_date("startTime",'HH24:MI'))*60*24)) as "Minutes"
, "Notes"
, sum("Tasks")  as "Tasks"
, sum("Units")  as "Units"
from
(
with js as (
select user_id                                      as "User"
, job_id                                            as "Job"
, job_unit                                          as "Type"
, dstamp                                            as "Time"
, notes                                             as "Notes"
, rank() over(partition by user_id order by dstamp) as "Ordered"
, client_id                                         as "Client"
from inventory_transaction 
where client_id = 'MOUNTAIN'
and dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and code = 'Job Start'
), je as (
select user_id                                      as "User"
, job_id                                            as "Job"
, job_unit                                          as "Type"
, dstamp                                            as "Time"
, notes                                             as "Notes"
, rank() over(partition by user_id order by dstamp) as "Ordered"
, client_id                                         as "Client"
from inventory_transaction 
where client_id = 'MOUNTAIN'
and dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and code = 'Job End'
), tots as (
select it.user_id                                      as "User"
, it.dstamp                                            as "Time"
, sum(case when it.update_qty = 0 then 0 else 1 end)   as "Tasks"
, sum(it.update_qty)                                   as "Units"
, it.client_id                                         as "Client"
, it.code                                              as "Code"
from inventory_transaction it
inner join LOCATION l on l.location_id = it.from_loc_id and l.site_id = it.site_id
where it.client_id = 'MOUNTAIN'
and it.dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and
(
    it.code not in ('Pick','Consol')
    or
    (
        it.code in ('Pick') and l.loc_type in ('Tag-FIFO','Tag-LIFO')
    )
)
and substr(it.work_group,2,2) = 'WW'
group by it.user_id
, it.dstamp
, it.client_id
, it.code
)
select case when trunc(tots."Time") is null then to_date('&&2', 'DD-MON-YYYY') else trunc(tots."Time") end  as "Date"
, js."User"                     as "User"
, to_char(js."Time", 'HH24:MI') as "startTime"
, to_char(je."Time", 'HH24:MI') as "endTime"
, js."Job"                      as "Job"
, js."Notes"                    as "Notes"
, SUM(tots."Tasks")             as "Tasks"
, SUM(tots."Units")             as "Units"
, js."Type"                     as "Type"
from js
inner join je on je."User" = js."User" and je."Ordered" = js."Ordered" and je."Job" = js."Job"
left join tots on tots."User" = js."User" and tots."Client" = js."Client" and tots."Time" between js."Time" and je."Time"
group by trunc(tots."Time")
, js."User"
, js."Time"
, je."Time"
, js."Job"
, js."Notes"
, js."Type"
) group by "Date"
, "User"
, "Job"
, "Notes"
order by 2, 3
)
/

--spool off