SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 100
SET TRIMSPOOL ON

column AA heading 'Date' format A12
column A1 heading 'Received' format 99999
column B1 heading 'Shipped' format 99999
column C1 heading 'Percentage' format 990.99

--spool sbotr.csv

select 'Date,Ordered,Shipped,Percentage' from DUAL;


select
AA,
A1,
B1,
B1*100/A1 C1
from (select
trunc(creation_date+0.416666) AA,
count(*) A1
from order_header
where client_id='SB'
and substr(order_id,1,1) in ('C','E')
and dispatch_method not like '%USA%'
and creation_date+0.416666 between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by
trunc(creation_date+0.416666)),(select
trunc(creation_date+0.416666) BB,
count(*) B1
from order_header
where client_id='SB'
and substr(order_id,1,1) in ('C','E')
and dispatch_method not like '%USA%'
and creation_date+0.416666 between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and trunc(shipped_date)<=trunc(creation_date+0.416667)+1
group by
trunc(creation_date+0.416666))
where AA=BB
order by AA
/

--spool off
