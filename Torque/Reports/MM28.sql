SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 160

column A heading 'Type' format a12
column B heading 'Order' format a12
column BB heading 'Status' format a16
column C heading 'Customer' format a12
column D heading 'Line' format 99999
column E heading 'SKU' format a18
column F heading 'Qty1' format 99999
column G heading 'Qty2' format 99999
column H heading 'Date' format A12
column J heading 'MM Order No' format A12
column K heading 'PO Number' format A12


break on A skip 1


--set termout off
--spool MM28.csv

select 'MM28 Short Shipped Report for orders created since '||to_char(sysdate-16,'DD/MM/YYYY') from Dual;

select 'Order Type,Order No,Status,Customer,Line,SKU,Qty Ordered,Qty Shipped,Creation Date,MM Order No,PO Number,' from DUAL;

SELECT
oh.ORDER_TYPE A,
oh.ORDER_ID B,
oh.status BB,
oh.CUSTOMER_ID C,
ol.LINE_ID D,
ol.SKU_ID E,
ol.QTY_ORDERED F,
nvl(ol.QTY_SHIPPED,0) G,
to_char(oh.CREATION_DATE,'DD/MM/YYYY') H,
ol.USER_DEF_TYPE_1 J,
oh.PURCHASE_ORDER K,
' '
from order_header oh,order_line ol
where  oh.client_id='MM'
and oh.client_id=ol.client_id
and oh.order_id=ol.order_id
and oh.STATUS='Shipped'
and trunc(oh.creation_date)>trunc(sysdate-17)
and trunc(oh.creation_date)<trunc(sysdate-2)
and ol.qty_ordered>nvl(ol.qty_picked,0)+nvl(ol.qty_shipped,0)
order by
oh.ORDER_TYPE,
oh.ORDER_ID
/

--spool off
--set termout on
