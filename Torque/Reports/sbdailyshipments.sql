SET FEEDBACK OFF                
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 100
set trimspool on

select
distinct
order_id||'|'||trunc(shipped_dstamp)
from shipping_manifest
where client_id='SB'
and order_id like 'C%'
and trunc(shipped_dstamp)>trunc(sysdate-1)
order by 1
/
