SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

select 'MM1', to_char(dstamp, 'YYYYMMDDHH24MISS'), reference_id
from inventory_transaction
where client_id = 'MM'
and code = 'Order Status'
and notes like ('%Shipped')
and dstamp between sysdate-1 and sysdate
/
