SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 900
SET TRIMSPOOL ON

--spool utallstock.csv

select 'Location,SKU,C1,C2,C3,Qty' from DUAL;


select
LC||','||
SK||','||
C1||','||
C2||','||
C3||','||
QQQ
from (SELECT
i.location_id LC,
i.sku_id SK,
s.user_def_type_2 C1,
s.user_def_type_1 C2,
s.user_def_type_4 C3,
SUM(i.qty_on_hand) QQQ
FROM inventory i, sku s
WHERE i.client_id = 'UT' 
AND s.client_id = 'UT' 
AND i.sku_id = s.sku_id 
AND i.location_id NOT IN ('SUSPENSE','CONTAINER','UTQC','UTDEB','UTOUT','UTFAULTY','UTFIX','UTIN','UTRET','UTWEB')
group by
i.location_id,
i.sku_id,
s.user_def_type_2,
s.user_def_type_1,
s.user_def_type_4
ORDER BY LC,SK)
/

--spool off
