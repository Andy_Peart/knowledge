SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 200
SET TRIMSPOOL ON
 

column A heading 'Date' format A12
column B heading 'Order ID' format A12
column B1 heading 'SKU' format A18
column B2 heading 'Description' format A24
column C heading 'Num' format 9999999
column D heading 'Qty' format 9999999
column E heading 'Ave' format 99999.99


break on report

compute sum label 'Totals' of C D on report


--spool pud.csv

select 'BERWIN PICKS from &&2 to &&3' from DUAL;

select 'Order ID,Lines,Units,Average' from DUAL;


select
B,
C,
D,
D/C E
from (select
it.reference_id B,
count(*) C,
sum(it.update_qty) D
from inventory_transaction it
where it.client_id='BW'
and it.code in ('Pick')
and it.dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')
and update_qty>0
group by
it.reference_id)
order by
B
/


--spool off


