SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


--spool mkready.csv


select 'Menkind Ready Exchanges - Printed on '||to_char(SYSDATE, 'DD/MM/YY  HH:MI') from DUAL;
select 'Pre Advice Id,Qty Due,Qty Received,PA Status,Order Id,Order Status,Consignment,Qty Tasked,Qty Shipped' from DUAL;

select
PA,
Q1,
Q2,
ST,
ORD,
ST2,
CS,
Q3,
Q4
from (select
ph.pre_advice_id PA,
sum(pl.qty_due) Q1,
sum(nvl(pl.qty_received,0)) Q2,
ph.status ST,
oh.order_id ORD,
oh.status ST2,
oh.consignment CS,
sum(nvl(ol.qty_tasked,0)) Q3,
sum(nvl(ol.qty_shipped,0)) Q4
from pre_advice_header ph,pre_advice_line pl,order_header oh,order_line ol
where ph.client_id='MK'
and (ph.pre_advice_id like 'Q%' or ph.pre_advice_id like 'SR%')
and pl.pre_advice_id=ph.pre_advice_id
and pl.client_id='MK'
and oh.client_id='MK'
and oh.order_id=ph.pre_advice_id
and ol.client_id='MK'
and oh.order_id=ol.order_id
group by
ph.pre_advice_id,
ph.status,
oh.order_id,
oh.status,
oh.consignment)
where Q1=Q2
and ST2 not in ('Shipped','Cancelled')
order by
PA
/

--spool off

