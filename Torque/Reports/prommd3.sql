SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep '|'


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 200
SET TRIMSPOOL ON
 
--spool soconf.csv

select
A||'|'||
B||'|'||
C||'|'||
D||'|'||
D2||'|'||
E||'|'||
F||'|'||
G||'|'||
sum(H)||'|||'
from (select
sku.user_def_type_2  A,
itl.reference_id B,
to_char(oh.ship_by_date,'DD/MM/YYYY') C,
to_char(itl.dstamp,'DD/MM/YYYY') D,
oh.address1 D2,
itl.line_id E,
--itl.sku_id F,
--DECODE(sku.user_def_type_2,'TML',nvl(PPP,'???')||sku.description||'-'||sku.sku_size,itl.sku_id) F,
DECODE(sku.user_def_type_2,'TML',nvl(PPP,ol.user_def_type_4)||sku.user_def_type_8||substr(sku.colour,5,20)||'-'||sku.sku_size,ol.sku_id) F,
ol.qty_ordered G,
itl.update_qty H
from inventory_transaction itl,order_header oh,order_line ol,sku,(select
sku_id  SSS,
max(pre_advice_id) PPP
from pre_advice_line
where client_id='PR'
group by sku_id)
where itl.client_id='PR'
and itl.code='Shipment'
and itl.dstamp>sysdate-1
and itl.reference_id=oh.order_id
and oh.client_id='PR'
and ol.client_id='PR'
and oh.order_id=ol.order_id
and itl.sku_id=ol.sku_id
and itl.line_id=ol.line_id
and sku.client_id='PR'
and itl.sku_id=sku.sku_id
and itl.sku_id=SSS (+))
group by
A,B,C,D,D2,E,F,G
order by 1
/

--spool off

