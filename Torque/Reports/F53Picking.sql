SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON

--SET FEEDBACK ON

update inventory_transaction
set uploaded='R'
where client_id = 'F53'
and code='Pick'
and uploaded = 'N'
and reference_id in  (select
order_id 
from order_header
where client_id='F53'
and order_type<>'WEB')
/

COLUMN M NOPRINT

SET FEEDBACK OFF            

--spool F53PICK.csv


--to_char(sysdate, 'YYYYMMDD HH:MI'),

select
distinct
RRR||'1' M,
'FLY53,'||
RRR
from (select
nvl(reference_id,'XXXX') RRR,
sku_id SSS,
update_qty QQQ
from inventory_transaction 
where client_id = 'F53'
and code='Pick'
and uploaded = 'R')
union
select
distinct
RRR||'2' M,
SSS||','||
QQQ
from (select
nvl(reference_id,'XXXX') RRR,
sku_id SSS,
update_qty QQQ
from inventory_transaction 
where client_id = 'F53'
and code='Pick'
and uploaded = 'R')
order by M
/


--spool off

--SET FEEDBACK ON

update inventory_transaction
set uploaded='Y'
where client_id = 'F53'
and code='Pick'
and uploaded = 'R'
/


commit;
--rollback;

