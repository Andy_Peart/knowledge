/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :     tmlsku2style.sql                         		      */
/*                                                                            */
/*     DESCRIPTION:                                                           */
/*     Quick report to convert tml sku ean to cims style number          */
/*                                                                            */
/*   DATE           BY   DESCRIPTION						                          */
/*   ======== ==== ===========					                              */
/*   24/05/2013 YB   initial version     				                          */
/******************************************************************************/
SET FEEDBACK OFF                 
SET ECHO OFF
set VERIFY off
set TAB off
SET colsep '    '
SET WRAP OFF


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500

/* Column Headings and Formats */
column a HEADING "Sku No" FORMAT A14
column B HEADING "Description" FORMAT A45
column C HEADING "Style" FORMAT A15
COLUMN D HEADING "Size" FORMAT A15

break on report

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

/* Top Title */
select 'Sku No' from DUAL;


select SKU_ID "A", 
DESCRIPTION "B", 
USER_DEF_TYPE_2||'-'||USER_DEF_TYPE_1||'-'||USER_DEF_TYPE_4 "C", 
SKU_SIZE "D"
from SKU
where CLIENT_ID = 'TR'
and SKU_ID in ('&2','&3','&4','&5','&6','&7','&8','&9')
/