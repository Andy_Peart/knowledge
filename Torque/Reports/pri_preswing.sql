/**************************************************************************************/
/*                                                                                    */
/*                            Elite                                                   */
/*                                                                                    */
/*     FILE NAME  :   pri_preswing.sql                                                */
/*                                                                                    */
/*     DESCRIPTION:    Datastream for Pringle Pre Advice Labels                       */
/*                                                                                    */
/*   DATE     BY   PROJ       ID         DESCRIPTION                                  */
/*   ======== ==== ======     ========   =============                                */
/*   14/10/08 LH   Pringle               PreAdvice labels by pre_advice_id and date   */
/*                                                                                    */
/**************************************************************************************/

/* Input Parameters are as follows */
-- 2)  Pre Advice ID
-- 3)  Dstamp
 
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 0
SET VERIFY OFF
SET FEEDBACK OFF
SET TAB OFF

 
-- Set the line length to the maximum possible, but trim 
-- trailing spaces for both screen output and spool files
SET LINESIZE 32767
SET WRAP ON
SET TRIMOUT ON
SET TRIMSPOOL ON




/* Define the format of the output dates */
DEFINE	OutputDateFormat = 'DD-MON-YYYY HH24:MI:SS'
COLUMN	sorter	NOPRINT

SELECT Distinct '0' sorter,
       'DSTREAM_PRI_PRESWING_HDR'          ||'|'||
       rtrim('&&2')            ||'|'||/* Pre Advice ID */
       rtrim('&&3')            /* Date */
from dual

SELECT distinct '3'||itl.dstamp sorter,
'DSTREAM_PRI_PRESWING_LABEL_HDR'    ||'|'||
rtrim (itl.sku_id)      ||'|'||
rtrim (itl.update_qty)
from inventory_transaction itl
where itl.reference_id = '&&2'
and trunc(dstamp) = '&&3'
and itl.client_id = 'PRI'
and itl.code = 'Receipt'
and itl.update_qty > 0
union all
SELECT distinct '3'||itl.dstamp sorter,
'DSTREAM_PRI_PRESWING_LABEL_LINE'    ||'|'||
rtrim (itl.sku_id)      ||'|'||
rtrim (s.description)      ||'|'||
rtrim (s.each_value)      ||'|'||
rtrim (s.colour)      ||'|'||
rtrim (s.ean)
from inventory_transaction itl, sku s
where itl.reference_id = '&&2'
and trunc(dstamp) = '&&3'
and itl.sku_id = s.sku_id
and itl.client_id = 'PRI'
and itl.update_qty > 0
and itl.code = 'Receipt'
order by 1,2
/