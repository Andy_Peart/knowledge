SET FEEDBACK OFF                
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 400
SET TRIMSPOOL ON

--spool end.csv

select 'SHIPMENTS' from DUAL;
select 'Date,Order No,Customer,Sku,Description,Colour,Size,Qty' from DUAL;

select 
to_char(i.dstamp, 'DD/MM/YY')||','
||i.reference_id||','
||i.customer_id||','''
||i.sku_id||''','
||sku.description||','
||sku.colour||','
||sku.sku_size||','
||i.update_qty
from inventory_transaction i,sku
where i.client_id = 'EN'
and i.code = 'Shipment'
and i.uploaded = 'N'
and i.sku_id=sku.sku_id
and sku.client_id = 'EN'
order by 
i.dstamp,
i.sku_id
/

--spool off

--set feedback on

update inventory_transaction
set uploaded='Y'
where client_id = 'EN'
and code = 'Shipment'
and uploaded = 'N'
/

commit;
--rollback;
