SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','
SET WRAP OFF


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON



select 'Selected period: ' || to_date('&&2', 'DD/MM/YY') || ' - ' || to_date('&&3', 'DD/MM/YY')  from dual
union all
select 'Zone, UserID, No of stores, Avg store units per zone, Avg line per store,  Units, Lines, Time taken (min), KPI Units, KPI Lines, Units per SKU, Time between picks (sec), Target lines, Achieved (%), Zone walk time, Carton make up and seal, Scan item, Zone carton fill, Calculated cartones, Scan time, Walk time, Carton make up and seal time, Total calculated time, Performamce (%)'  from dual
union all
select X.pick_zone || ',' ||  X.user_id || ',' ||  Y.no_of_stores || ',' ||  
round(X.units/Y.no_of_stores) || ',' ||   
round(X.lines/Y.no_of_stores) || ',' || 
X.units || ',' ||  X.lines || ',' ||  X.time_taken || ',' ||  X.kpi_units || ',' ||  X.kpi_lines || ',' ||  X.units_per_sku || ',' ||  X.time_between || ',' ||  X.target_lines || ',' ||  X.achieved || ',' ||
X.zone_walk_time
|| ',' ||
X.carton_make_up
|| ',' ||
X.scan_item
|| ',' ||
X.zone_carton_fill
|| ',' ||
X.calculated_cartons
|| ',' ||
X.scan_time
|| ',' ||
--the below block is for WALK_TIME
CASE 
     when X.pick_zone = 'A' then Y.no_of_stores * 15.5
     when X.pick_zone = 'B' then Y.no_of_stores * 10
     when X.pick_zone = 'C' then Y.no_of_stores * 10
     when X.pick_zone = 'D' then Y.no_of_stores * 7
     when X.pick_zone = 'T' then Y.no_of_stores * 3.5
     when X.pick_zone = 'U' then Y.no_of_stores * 3.5
     when X.pick_zone = 'V' then Y.no_of_stores * 6.5
     when X.pick_zone = 'W' then Y.no_of_stores * 3
     when X.pick_zone = 'X' then Y.no_of_stores * 5.5
     when X.pick_zone = 'Y' then Y.no_of_stores * 3.5
     when X.pick_zone = 'Z' then Y.no_of_stores * 4
END
|| ',' ||
X.carton_make_up_time
|| ',' ||
--the below block is for TOTAL_CALCULATED_TIME
CASE 
     when X.pick_zone = 'A' then to_char(Y.no_of_stores * 15.5 + X.calculated_cartons+X.scan_time+X.carton_make_up_time)
     when X.pick_zone = 'B' then to_char(Y.no_of_stores * 10 + X.calculated_cartons+X.scan_time+X.carton_make_up_time)
     when X.pick_zone = 'C' then to_char(Y.no_of_stores * 10 + X.calculated_cartons+X.scan_time+X.carton_make_up_time)
     when X.pick_zone = 'D' then to_char(Y.no_of_stores * 7 + X.calculated_cartons+X.scan_time+X.carton_make_up_time)
     when X.pick_zone = 'T' then to_char(Y.no_of_stores * 3.5 + X.calculated_cartons+X.scan_time+X.carton_make_up_time)
     when X.pick_zone = 'U' then to_char(Y.no_of_stores * 3.5 + X.calculated_cartons+X.scan_time+X.carton_make_up_time)
     when X.pick_zone = 'V' then to_char(Y.no_of_stores * 6.5 + X.calculated_cartons+X.scan_time+X.carton_make_up_time)
     when X.pick_zone = 'W' then to_char(Y.no_of_stores * 3 + X.calculated_cartons+X.scan_time+X.carton_make_up_time)
     when X.pick_zone = 'X' then to_char(Y.no_of_stores * 5.5 + X.calculated_cartons+X.scan_time+X.carton_make_up_time)
     when X.pick_zone = 'Y' then to_char(Y.no_of_stores * 3.5 + X.calculated_cartons+X.scan_time+X.carton_make_up_time)
     when X.pick_zone = 'Z' then to_char(Y.no_of_stores * 4 + X.calculated_cartons+X.scan_time+X.carton_make_up_time)
END
|| ',' ||
--the below block is for PERFORMANCE
CASE 
     when X.pick_zone = 'A' then to_char( 100*ROUND((Y.no_of_stores * 15.5 + X.calculated_cartons+X.scan_time+X.carton_make_up_time)/X.time_taken,2 ))
     when X.pick_zone = 'B' then to_char( 100*ROUND((Y.no_of_stores * 10 + X.calculated_cartons+X.scan_time+X.carton_make_up_time)/X.time_taken,2 ))
     when X.pick_zone = 'C' then to_char( 100*ROUND((Y.no_of_stores * 10 + X.calculated_cartons+X.scan_time+X.carton_make_up_time)/X.time_taken,2 ))
     when X.pick_zone = 'D' then to_char( 100*ROUND((Y.no_of_stores * 7 + X.calculated_cartons+X.scan_time+X.carton_make_up_time)/X.time_taken,2 ))
     when X.pick_zone = 'T' then to_char( 100*ROUND((Y.no_of_stores * 3.5 + X.calculated_cartons+X.scan_time+X.carton_make_up_time)/X.time_taken,2 ))
     when X.pick_zone = 'U' then to_char( 100*ROUND((Y.no_of_stores * 3.5 + X.calculated_cartons+X.scan_time+X.carton_make_up_time)/X.time_taken,2 ))
     when X.pick_zone = 'V' then to_char( 100*ROUND((Y.no_of_stores * 6.5 + X.calculated_cartons+X.scan_time+X.carton_make_up_time)/X.time_taken,2 ))
     when X.pick_zone = 'W' then to_char( 100*ROUND((Y.no_of_stores * 3 + X.calculated_cartons+X.scan_time+X.carton_make_up_time)/X.time_taken,2 ))
     when X.pick_zone = 'X' then to_char( 100*ROUND((Y.no_of_stores * 5.5 + X.calculated_cartons+X.scan_time+X.carton_make_up_time)/X.time_taken,2 ))
     when X.pick_zone = 'Y' then to_char( 100*ROUND((Y.no_of_stores * 3.5 + X.calculated_cartons+X.scan_time+X.carton_make_up_time)/X.time_taken,2 ))
     when X.pick_zone = 'Z' then to_char( 100*ROUND((Y.no_of_stores * 4 + X.calculated_cartons+X.scan_time+X.carton_make_up_time)/X.time_taken,2 ))
END
from (
select pick_zone , user_id , units , lines , time_taken , kpi_units , kpi_lines , units_per_sku , time_between  ,
CASE 
     when pick_zone = 'A' then 100
     when pick_zone = 'B' then 110
     when pick_zone = 'C' then 150
     when pick_zone = 'D' then 129
     when pick_zone = 'T' then 212
     when pick_zone = 'U' then 196
     when pick_zone = 'V' then 195
     when pick_zone = 'W' then 207
     when pick_zone = 'X' then 165
     when pick_zone = 'Y' then 193
     when pick_zone = 'Z' then 164     
END as target_lines,
CASE 
     when pick_zone = 'A' then round(kpi_lines/100,2)*100
     when pick_zone = 'B' then round(kpi_lines/110,2)*100
     when pick_zone = 'C' then round(kpi_lines/150,2)*100
     when pick_zone = 'D' then round(kpi_lines/129,2)*100
     when pick_zone = 'T' then round(kpi_lines/212,2)*100
     when pick_zone = 'U' then round(kpi_lines/196,2)*100
     when pick_zone = 'V' then round(kpi_lines/195,2)*100
     when pick_zone = 'W' then round(kpi_lines/207,2)*100
     when pick_zone = 'X' then round(kpi_lines/165,2)*100
     when pick_zone = 'Y' then round(kpi_lines/193,2)*100
     when pick_zone = 'Z' then round(kpi_lines/160,2)*100
END as achieved,
CASE 
     when pick_zone = 'A' then 15.5
     when pick_zone = 'B' then 10
     when pick_zone = 'C' then 10
     when pick_zone = 'D' then 7
     when pick_zone = 'T' then 3.5
     when pick_zone = 'U' then 3.5
     when pick_zone = 'V' then 6.5
     when pick_zone = 'W' then 3
     when pick_zone = 'X' then 5.5
     when pick_zone = 'Y' then 3.5
     when pick_zone = 'Z' then 4     
END as zone_walk_time,
0.5 as carton_make_up,
0.08 as scan_item,
CASE 
     when pick_zone = 'A' then 6
     when pick_zone = 'B' then 23
     when pick_zone = 'C' then 15
     when pick_zone = 'D' then 15
     when pick_zone = 'T' then 23
     when pick_zone = 'U' then 23
     when pick_zone = 'V' then 23
     when pick_zone = 'W' then 23
     when pick_zone = 'X' then 23
     when pick_zone = 'Y' then 23
     when pick_zone = 'Z' then 23
END as zone_carton_fill,
CASE 
     when pick_zone = 'A' then round(units/6)
     when pick_zone = 'B' then round(units/23)
     when pick_zone = 'C' then round(units/15)
     when pick_zone = 'D' then round(units/15)
     when pick_zone = 'T' then round(units/23)
     when pick_zone = 'U' then round(units/23)
     when pick_zone = 'V' then round(units/23)
     when pick_zone = 'W' then round(units/23)
     when pick_zone = 'X' then round(units/23)
     when pick_zone = 'Y' then round(units/23)
     when pick_zone = 'Z' then round(units/23)
END as calculated_cartons,
lines*0.08 as scan_time,
CASE 
     when pick_zone = 'A' then round(units/6)*0.5
     when pick_zone = 'B' then round(units/23)*0.5
     when pick_zone = 'C' then round(units/15)*0.5
     when pick_zone = 'D' then round(units/15)*0.5
     when pick_zone = 'T' then round(units/23)*0.5
     when pick_zone = 'U' then round(units/23)*0.5
     when pick_zone = 'V' then round(units/23)*0.5
     when pick_zone = 'W' then round(units/23)*0.5
     when pick_zone = 'X' then round(units/23)*0.5
     when pick_zone = 'Y' then round(units/23)*0.5
     when pick_zone = 'Z' then round(units/23)*0.5
END as carton_make_up_time
from (
select A.pick_zone , A.user_id , B.units , B.lines , A.time_taken ,
round((B.units*60)/A.time_taken,2) kpi_units  ,
round((B.lines*60)/A.time_taken,2) kpi_lines ,
round(B.units/B.lines,2) units_per_sku ,
round((A.time_taken / B.lines)*60,1) time_between
from (
select user_id, substr(from_loc_id,1,1) as pick_zone, round(sum(elapsed_time)/60) as time_taken
from inventory_transaction
where client_id = 'MOUNTAIN'
and code = 'Pick'
and reference_id like 'MOR%'
and dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1
and elapsed_time > 0
and update_qty > 0
and from_loc_id <> 'CONTAINER'
and from_loc_id not like '0%'
and from_loc_id not like '1%'
and from_loc_id not like '2%'
and from_loc_id not like '3%'
and from_loc_id not like '4%'
and from_loc_id not like '5%'
and from_loc_id not like '6%'
and from_loc_id not like '7%'
and from_loc_id not like '8%'
and from_loc_id not like '9%'
and from_loc_id not like 'G%'
and key not in (
  select key_no from (
    select user_id, substr(from_loc_id,1,1),min(dstamp), min(key) as key_no
    from inventory_transaction
    where client_id = 'MOUNTAIN'
    and code = 'Pick'
    and reference_id like 'MOR%'
    and dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1
    and update_qty > 0
    and elapsed_time > 0
    and from_loc_id <> 'CONTAINER'
    and from_loc_id not like '0%'
    and from_loc_id not like '1%'
    and from_loc_id not like '2%'
    and from_loc_id not like '3%'
    and from_loc_id not like '4%'
    and from_loc_id not like '5%'
    and from_loc_id not like '6%'
    and from_loc_id not like '7%'
    and from_loc_id not like '8%'
    and from_loc_id not like '9%'
    and from_loc_id not like 'G%'
    group by user_id, substr(from_loc_id,1,1)
  )
)
group by user_id, substr(from_loc_id,1,1)
order by substr(from_loc_id,1,1)
) A 
inner join 
(
select user_id, substr(from_loc_id,1,1) as pick_zone, sum(update_qty) as units, count(user_id) as lines, round(sum(elapsed_time)/60) as time_taken
from inventory_transaction
where client_id = 'MOUNTAIN'
and code = 'Pick'
and reference_id like 'MOR%'
and dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1
and update_qty > 0
and elapsed_time > 0
and from_loc_id <> 'CONTAINER'
and from_loc_id not like '0%'
and from_loc_id not like '1%'
and from_loc_id not like '2%'
and from_loc_id not like '3%'
and from_loc_id not like '4%'
and from_loc_id not like '5%'
and from_loc_id not like '6%'
and from_loc_id not like '7%'
and from_loc_id not like '8%'
and from_loc_id not like '9%'
and from_loc_id not like 'G%'
group by user_id, substr(from_loc_id,1,1)
order by  substr(from_loc_id,1,1)
) B
on A.user_id = B.user_id and A.pick_zone = B.pick_zone
where A.time_taken > 0
)
) X
left join 
(
select user_id, pick_zone, count(*) no_of_stores
from 
(
select user_id, substr(from_loc_id,1,1) as pick_zone, reference_id, count(reference_id)
from inventory_transaction
where client_id = 'MOUNTAIN'
and code = 'Pick'
and reference_id like 'MOR%'
and dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1
and update_qty > 0
and elapsed_time > 0
and from_loc_id <> 'CONTAINER'
and from_loc_id not like '0%'
and from_loc_id not like '1%'
and from_loc_id not like '2%'
and from_loc_id not like '3%'
and from_loc_id not like '4%'
and from_loc_id not like '5%'
and from_loc_id not like '6%'
and from_loc_id not like '7%'
and from_loc_id not like '8%'
and from_loc_id not like '9%'
and from_loc_id not like 'G%'
group by user_id, substr(from_loc_id,1,1) , reference_id
order by  1
)
group by user_id, pick_zone
order by 1
) Y
on X.pick_zone = Y.pick_zone and X.user_id = Y.user_id
/