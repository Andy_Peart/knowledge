SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET TRIMSPOOL ON

select 'SKU,Qty,Location,Work zone,Location zone' from dual
union all
select sku_id || ',' || qty || ',' || location_id || ',' || work_zone || ',' || location_zone
from (
    select A.sku_id, A.qty, B.location_id, B.work_zone, B.location_zone
    from (
        select sku_id, sum(qty) qty, site_id, count(*)
        from (
            select inv.sku_id, inv.location_id, inv.site_id, sum(qty_on_hand) qty
            from inventory inv 
            where inv.client_id = 'MOUNTAIN'
            and inv.location_id not in ('MAIL ORDER','CONTAINER','SUSPENSE','DESPATCH')
            group by inv.sku_id, inv.location_id, site_id
            having sum(qty_on_hand)<=20
        )
        group by sku_id, site_id
        having count(*)=1
    ) A 
    left join
    (
        select inv.sku_id, max(inv.location_id) location_id, max(loc.work_zone) work_zone, max(loc.zone_1) location_zone
        from inventory inv inner join location loc on inv.location_id = loc.location_id and inv.site_id = loc.site_id
        where inv.client_id = 'MOUNTAIN'
        and inv.location_id not in ('MAIL ORDER','CONTAINER','SUSPENSE','DESPATCH')
        group by inv.sku_id
    ) B
    on A.sku_id = B.sku_id
)
/