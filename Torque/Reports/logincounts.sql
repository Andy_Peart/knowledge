SET FEEDBACK OFF                
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 400
SET TRIMSPOOL ON

column A heading 'Store No.' format a12
column B heading 'Name' format a30
column C heading 'PA ID' format a12
column D heading 'Date' format a12
column E heading 'Lines' format 999999
column F heading 'Qty Due' format 999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

--break on report

--compute sum label 'Total' of C D  on report

spool logincounts.csv

select 'Log in counts from &&2 to &&3' from DUAL;

select'User ID,Client_vis_group,Login Count' from DUAL;


select
user_id,
Client_vis_group,
count(*)
from user_log
where code='Login'
and mdi_type='W'
and dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1 
group by
user_id,
Client_vis_group
order by 1
/

spool off

