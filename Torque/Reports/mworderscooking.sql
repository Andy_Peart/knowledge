SET FEEDBACK OFF                 
set verify off
set wrap off
set colsep ','

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 300
set trimspool on


spool mworderscooking.csv



select 'Status,Total Orders' from DUAL
union all
select * from (select
oh.status||','||
count(*)
from order_header oh
where oh.client_id='MOUNTAIN'
and oh.status not in ('Cancelled','Shipped')
and work_group='WWW'
group by oh.status
order by 1)
/

select 'Status,Order Id,Lines,Consignment' from DUAL
union all
select * from (select
oh.status||','||
oh.order_id||','||
oh.num_lines||','||
oh.consignment
from order_header oh
where oh.client_id='MOUNTAIN'
and oh.status not in ('Cancelled','Shipped')
and work_group='WWW'
order by
1)
/

spool off

