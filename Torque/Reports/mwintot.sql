set pagesize 68
set linesize 120
set verify off
set feedback off

ttitle 'Mountain Warehouse Intake Quantity' skip 2

column A heading 'Date' format a8
column C heading 'Notes' format a10
column B heading 'Qty Receipted' format 99999999

break on report

compute sum label 'Total' of B on report

select 
to_char (dstamp, 'DD/MM/YY') A,
Notes C,
sum (update_qty) B
from inventory_transaction
where client_id = 'MOUNTAIN'
and code = 'Receipt'
and dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1 
and lower(sku_id)=upper(sku_id)
group by 
to_char(dstamp, 'DD/MM/YY'),
Notes
order by A, C
/

ttitle 'Mountain Warehouse Intake Quantity - Summary' skip 2

select 
'ALL' A,
Notes C,
sum (update_qty) B
from inventory_transaction
where client_id = 'MOUNTAIN'
and code = 'Receipt'
and dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1 
and lower(sku_id)=upper(sku_id)
group by 
--to_char(dstamp, 'DD/MM/YY'),
Notes
order by C
/
