SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

COLUMN A heading "SKU" FORMAT A18
COLUMN B heading "Description" FORMAT A40
COLUMN C heading "Colour" FORMAT A18
COLUMN D heading "Units" FORMAT 999999
COLUMN E heading "Tasks" FORMAT 999999



--select 'Multiple SKU Extract - for client ID TR as at ' 
--        ||  to_char(sysdate,'dd/mm/yy  hh24:mi:ss') from dual;

select 'SKU,Description,Colour,Qty,Tasks'
from dual;



select
''''||mt.sku_id||'''' A,
mt.description B,
sku.colour C,
sum(mt.qty_to_move) D,
count(*) E
from move_task mt,sku
where mt.client_id='T'
and mt.task_type='O'
and mt.status='Released'
--and trunc(mt.Dstamp)>trunc(sysdate-3)
and mt.sku_id=sku.sku_id
and sku.client_id='T'
group by
mt.sku_id,
mt.description,
sku.colour
order by D DESC, E DESC
/
