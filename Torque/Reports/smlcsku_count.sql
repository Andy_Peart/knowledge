/* SKU Count Report */

SET FEEDBACK OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES


 
/* Set up page and line */
SET PAGESIZE 10
SET NEWPAGE 0
SET LINESIZE 132
 
/* Top Title */
TTITLE LEFT "WAREHOUSE SKU COUNT" skip 2

column A heading 'No. Of Different|SKUs In Stock'
column B heading 'Site'
column C heading 'Client'


select 
client_id C,
site_id B,
count (distinct sku_id) A
from inventory
where client_id in ('IF','S2','GG','VF','EX')
group by 
client_id,
site_id
/

