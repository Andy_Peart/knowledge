SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 200
set trimspool on
set newpage 0
set verify off
set colsep ','



break on A dup on report
compute sum label 'WK Total' of B D on A
compute sum label 'Run Total' of B D on report



select 'Week,Work Group,Picks,Units' from DUAL;

select
trunc(to_date('&&2', 'DD-MON-YYYY'))+(trunc(((trunc(Dstamp))-(trunc(to_date('&&2', 'DD-MON-YYYY'))))/7)*7) A,
--trunc(((trunc(it.Dstamp))-(trunc(to_date('&&2', 'DD-MON-YYYY'))))/7)+1 A,
it.work_group C,
--nvl(NM,' '),
count(*) B,
sum (it.update_qty) D
from inventory_transaction it,(select
address_id WG,
name NM
from address 
where client_id = 'MOUNTAIN')
where it.client_id = 'MOUNTAIN'
and it.code = 'Pick'
and it.work_group<>'WWW'
and it.elapsed_time>0
and it.dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')
and it.work_group=WG (+)
group by
trunc(to_date('&&2', 'DD-MON-YYYY'))+(trunc(((trunc(Dstamp))-(trunc(to_date('&&2', 'DD-MON-YYYY'))))/7)*7),
--trunc(((trunc(it.Dstamp))-(trunc(to_date('&&2', 'DD-MON-YYYY'))))/7)+1,
it.work_group
--nvl(NM,' ')
union
select
trunc(to_date('&&2', 'DD-MON-YYYY'))+(trunc(((trunc(Dstamp))-(trunc(to_date('&&2', 'DD-MON-YYYY'))))/7)*7) A,
--trunc(((trunc(it.Dstamp))-(trunc(to_date('&&2', 'DD-MON-YYYY'))))/7)+1 A,
it.work_group C,
--nvl(NM,' '),
count(*) B,
sum (it.update_qty) D
from inventory_transaction_archive it,(select
address_id WG,
name NM
from address 
where client_id = 'MOUNTAIN')
where it.client_id = 'MOUNTAIN'
and it.code = 'Pick'
and it.work_group<>'WWW'
and it.elapsed_time>0
and it.dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')
and it.work_group=WG (+)
group by
trunc(to_date('&&2', 'DD-MON-YYYY'))+(trunc(((trunc(Dstamp))-(trunc(to_date('&&2', 'DD-MON-YYYY'))))/7)*7),
--trunc(((trunc(it.Dstamp))-(trunc(to_date('&&2', 'DD-MON-YYYY'))))/7)+1,
it.work_group
--nvl(NM,' ')
order by A,C
/

