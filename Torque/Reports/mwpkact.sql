SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','
SET WRAP OFF


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON



select 'Date picked, Zone, UserID, Units per SKU, Units, Lines, Total stores, Store zone ave - units, Store zone ave - lines, Time taken (min),KPI Units, KPI Lines' from dual
union all
select X.date_picked  || ',' || X.pick_zone || ',' ||  X.user_id || ',' ||  X.units_per_sku || ',' || X.units || ',' ||  X.lines || ',' ||  Y.no_of_stores || ',' ||  
round(X.units/Y.no_of_stores) || ',' ||   
round(X.lines/Y.no_of_stores) || ',' || 
X.time_between || ',' ||  
X.kpi_units || ',' ||  X.kpi_lines
from (
select date_picked, pick_zone , user_id , units , lines , time_taken , kpi_units , kpi_lines , units_per_sku , time_between
from (
select A.date_picked, A.pick_zone , A.user_id , B.units , B.lines , A.time_taken ,
round((B.units*60)/A.time_taken,2) kpi_units  ,
round((B.lines*60)/A.time_taken,2) kpi_lines ,
round(B.units/B.lines,2) units_per_sku ,
round((A.time_taken / B.lines)*60,1) time_between
from (
select to_char(dstamp,'DD-MM-YYYY') as date_picked,
user_id, substr(from_loc_id,1,1) as pick_zone, round(sum(elapsed_time)/60) as time_taken
from inventory_transaction
where client_id = 'MOUNTAIN'
and code = 'Pick'
and reference_id like 'MOR%'
and dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1
and elapsed_time > 0
and update_qty > 0
and from_loc_id <> 'CONTAINER'
and from_loc_id not like '0%'
and from_loc_id not like '1%'
and from_loc_id not like '2%'
and from_loc_id not like '3%'
and from_loc_id not like '4%'
and from_loc_id not like '5%'
and from_loc_id not like '6%'
and from_loc_id not like '7%'
and from_loc_id not like '8%'
and from_loc_id not like '9%'
and from_loc_id not like 'G%'
and key not in (
  select key_no from (
    select user_id, substr(from_loc_id,1,1),min(dstamp), min(key) as key_no
    from inventory_transaction
    where client_id = 'MOUNTAIN'
    and code = 'Pick'
    and reference_id like 'MOR%'
    and dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1
    and update_qty > 0
    and elapsed_time > 0
    and from_loc_id <> 'CONTAINER'
    and from_loc_id not like '0%'
    and from_loc_id not like '1%'
    and from_loc_id not like '2%'
    and from_loc_id not like '3%'
    and from_loc_id not like '4%'
    and from_loc_id not like '5%'
    and from_loc_id not like '6%'
    and from_loc_id not like '7%'
    and from_loc_id not like '8%'
    and from_loc_id not like '9%'
    and from_loc_id not like 'G%'
    group by user_id, substr(from_loc_id,1,1)
  )
)
group by to_char(dstamp,'DD-MM-YYYY'),user_id, substr(from_loc_id,1,1)
order by substr(from_loc_id,1,1)
) A 
inner join 
(
select to_char(dstamp,'DD-MM-YYYY') as date_picked, user_id, substr(from_loc_id,1,1) as pick_zone, sum(update_qty) as units, count(user_id) as lines, round(sum(elapsed_time)/60) as time_taken
from inventory_transaction
where client_id = 'MOUNTAIN'
and code = 'Pick'
and reference_id like 'MOR%'
and dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1
and update_qty > 0
and elapsed_time > 0
and from_loc_id <> 'CONTAINER'
and from_loc_id not like '0%'
and from_loc_id not like '1%'
and from_loc_id not like '2%'
and from_loc_id not like '3%'
and from_loc_id not like '4%'
and from_loc_id not like '5%'
and from_loc_id not like '6%'
and from_loc_id not like '7%'
and from_loc_id not like '8%'
and from_loc_id not like '9%'
and from_loc_id not like 'G%'
group by to_char(dstamp,'DD-MM-YYYY'), user_id, substr(from_loc_id,1,1)
order by  substr(from_loc_id,1,1)
) B
on A.user_id = B.user_id and A.pick_zone = B.pick_zone and a.date_picked = b.date_picked
where A.time_taken > 0
)
) X
left join 
(
select date_picked, user_id, pick_zone, count(*) no_of_stores
from 
(
select to_char(dstamp, 'DD-MM-YYYY') as date_picked, user_id, substr(from_loc_id,1,1) as pick_zone, reference_id, count(reference_id)
from inventory_transaction
where client_id = 'MOUNTAIN'
and code = 'Pick'
and reference_id like 'MOR%'
and dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1
and update_qty > 0
and elapsed_time > 0
and from_loc_id <> 'CONTAINER'
and from_loc_id not like '0%'
and from_loc_id not like '1%'
and from_loc_id not like '2%'
and from_loc_id not like '3%'
and from_loc_id not like '4%'
and from_loc_id not like '5%'
and from_loc_id not like '6%'
and from_loc_id not like '7%'
and from_loc_id not like '8%'
and from_loc_id not like '9%'
and from_loc_id not like 'G%'
group by to_char(dstamp, 'DD-MM-YYYY'), user_id, substr(from_loc_id,1,1) , reference_id
order by  1
)
group by date_picked, user_id, pick_zone
order by 1
) Y
on X.pick_zone = Y.pick_zone and X.user_id = Y.user_id and X.date_picked = Y.date_picked
/