SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240
SET TRIMSPOOL ON

column A heading 'Store' format A6
column B heading 'Name' format A30
column C heading 'Order/List' format A20
column D heading 'Lines' format 99999999
column O heading 'Ordered' format 99999999
column E heading 'To Pick' format 99999999
column P heading 'Picked' format 99999999
column S heading 'Shipped' format 99999999
column F heading 'Consignment' format A14
column M format a4 noprint



select 'Store,Name,Order No,Status,Order Qty,Lines,To Pick,Picked,Shipped,Consignment,Order Date,Order Time,Total Cartons,Shipped,Delivery Note Created,Checked Inventory,Order Header Updated,Labels Attached,Picker,Time Out,Time In,Hours,Pick Rate' from DUAL;
 
--set TERMOUT OFF
--column curdate NEW_VALUE report_date
--select to_char(SYSDATE, 'DAY, DD MONTH, YYYY') curdate from DUAL;
--set TERMOUT ON


break on report

compute sum label 'Totals' of O D E P S on report

select
oh.customer_id A,
a.name B,
oh.order_id C,
oh.status,
ORD O,
OO D,
KK E,
PP P,
SS S,
oh.consignment F,
substr(oh.consignment,9,4) M,
to_char(oh.order_date,'DD/MM/YY') OD1,
to_char(oh.order_date,'HH:MI:SS') OD2,
' '
from order_header oh,address a,
(select
oh.order_id  ID,
nvl(sum(ol.qty_ordered),0) ORD,
count(*) OO,
nvl(sum(ol.qty_picked),0) PP,
nvl(sum(ol.qty_tasked),0) KK,
nvl(sum(ol.qty_shipped),0) SS
from order_header oh,order_line ol
where oh.client_id='GV'
and oh.order_id=ol.order_id
group by
oh.order_id)
where oh.order_id=ID
--and oh.status not in ('Cancelled','Hold','Shipped')
and oh.consignment like '&&2%'
and substr(oh.consignment,9,1)='&&3'
and oh.client_id='GV'
and a.client_id='GV'
and oh.customer_id=a.address_id
order by
M,A,C
/
