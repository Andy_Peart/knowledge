SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','
SET WRAP OFF


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500

select 'Order Date,Priority,Units,Despatch method'  A1 from dual
union all
select od || ',' || pr || ',' || qty || ',' || dm A1
from (
  select od, pr, sum(units) qty, dm
  from (
    select ORDER_DATE, to_char(order_date,'DD-MON-YYYY') as OD, PRIORITY as Pr, ORDER_VOLUME*10000 AS Units, dispatch_method DM
    from order_header oh
    where oh.client_id = 'MOUNTAIN'
    and oh.order_type like 'WEB'
                  and status in ('Allocated', 'Released', 'Hold')
    and consignment not in ('Short', 'Problem')
    order by 1
  )
  group by od,pr,dm
)
/
