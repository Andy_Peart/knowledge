SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 200
SET TRIMSPOOL ON



set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


--break on A dup skip 1 on A1 dup skip 1 on report


--compute sum label 'Summary' of B C on A1
--compute sum label 'Totals' of B C on A
--compute sum label 'Overall' of B C on report


select 'SKU ID,Description,Qty On Hand,Qty Allocated,' from DUAL;

select
i.sku_id||','||
s.description||','||
sum(i.qty_on_hand)||','||
sum(i.qty_allocated)||','||
' '
from inventory i, sku s
where i.client_id = '&&2'
and i.sku_id = s.sku_id
group by
i.sku_id,
s.description
order by
i.sku_id
/
