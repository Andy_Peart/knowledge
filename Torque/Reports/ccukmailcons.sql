SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep '|'


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 300
SET TRIMSPOOL ON
 
column XX NOPRINT

--spool mkukmail.csv

SELECT
oh.order_id  XX,
'113000'||'|'|| 
to_char(Sysdate,'DDMMYYYY')||'|'||
oh.contact||'|'||
oh.Name||'|'||
oh.Address1||'|'||
oh.Address2||'|'||
oh.Town||'|'||
oh.County||'|||'||
oh.PostCode||'||'||
(SELECT COUNT(DISTINCT CONTAINER_ID)
FROM SHIPPING_MANIFEST
WHERE CLIENT_ID = 'CC'
AND CONSIGNMENT = OH.CONSIGNMENT
AND ORDER_ID = OH.ORDER_ID)||'|'||
(SELECT COUNT(DISTINCT CONTAINER_ID)*10
FROM SHIPPING_MANIFEST
WHERE CLIENT_ID = 'CC'
AND CONSIGNMENT = OH.CONSIGNMENT
AND ORDER_ID = OH.ORDER_ID)||'|'||
'1'||'|'||
oh.instructions||'|'||
''||'|||||'||
'CC'||OH.order_id||'|||||||||||||||'||
'Torque (Crew Clothing) Challenge Way Wigan WN5 0LD'||'||'||
' '
from order_header oh
WHERE OH.CLIENT_ID = 'CC'
AND OH.CONSIGNMENT = '&&2' 
ORDER BY OH.ORDER_ID
/

--spool off
