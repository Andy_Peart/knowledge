SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 300
SET TRIMSPOOL ON

column A heading 'Shipment Date' format A16
column B heading 'Carrier' format A12
column C heading 'Qty' format 9999999

break on A on report 
compute sum label 'Daily Total' of C on report
--compute sum label 'Overall Total' of C on report

--spool ggg.csv

select 'Shipment Date,Carrier,Order Qty' from DUAL;


select
trunc(it.Dstamp) A,
to_loc_id B,
count(*) C
from inventory_transaction it
where it.client_id='MOUNTAIN'
and it.to_loc_id like 'UKPA%'
and it.code='Marshal'
and trunc(it.Dstamp)=trunc(sysdate)
group by
trunc(it.Dstamp),
to_loc_id
order by
trunc(it.Dstamp),
to_loc_id
/


--spool off
