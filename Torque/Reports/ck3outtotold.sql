SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 360
SET TRIMSPOOL ON

--ttitle 'CURVY KATE Despatch Quantities from &&2 to &&3' skip 2

column A heading 'Date' format a12
column B heading 'Orders' format 99999999
column C heading 'Units' format 99999999
column D heading 'Stock' format 99999999
column E heading 'Hangers' format 99999999
column M format a4 noprint


break on report

compute sum label 'Totals' of B C D E on report

--spool ck3outtot.csv

select 'Date,Customer,Name,Orders,Units,Stock,Hangers' from DUAL;


select
trunc(it.dstamp) A,
nvl(it.customer_id,'BLANK') G,
NAME,
--' ' PC,
count(distinct it.reference_id) B,
nvl(B2,0) C,
nvl(B2,0)-nvl(B1,0) D,
nvl(B1,0) E
from inventory_transaction it,(select
trunc(dstamp) A1,
nvl(customer_id,'BLANK') G1,
--count(distinct reference_id) G,
sum (update_qty) B1
from inventory_transaction
where client_id = 'CK'
and code = 'Shipment'
and dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1
and sku_id in ('5052574061487','5052574061470')
group by
trunc(Dstamp),customer_id),(select
trunc(dstamp) A2,
nvl(customer_id,'BLANK') G2,
count(distinct reference_id) G,
sum (update_qty) B2
from inventory_transaction
where client_id = 'CK'
and code = 'Shipment'
and dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1
and update_qty>0
group by
trunc(Dstamp),customer_id),(select
distinct
address_id ADD1,
name NAME
--postcode PC
from address
where client_id = 'CK')
where it.client_id = 'CK'
and it.code = 'Shipment'
and it.update_qty>0
and it.dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1
and nvl(it.customer_id,'BLANK')=G1 (+)
and nvl(it.customer_id,'BLANK')=G2 (+)
and trunc(it.Dstamp)=A1 (+)
and trunc(it.Dstamp)=A2 (+)
and it.customer_id=ADD1 (+)
group by
trunc(it.Dstamp),
it.customer_id,
NAME,
--PC,
nvl(B2,0),
nvl(B1,0)
order by A,G
/

--spool off

