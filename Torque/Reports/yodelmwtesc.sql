SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep '|' 


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 5000
SET TRIMSPOOL ON
 
column XX NOPRINT

--spool ukmail.csv
select col1 || '|' || purchase_order || '|' || order_type || '|' || priority || '|' || work_group || '|' || order_id || '|' || collection_date || '|' || dispatch_method || '|' || 
name || '|' || contact || '|' || address1 || '|' || address2 || '|' || town || '|' || county || '|' || postcode || '|' || country || '|' || contact_phone || '|' || 
instructions || '|' || no_of_cartons || '|' || weight || '|' || local_prod_code || '|' || local_service_code as a1
from (
select
	'tesco' as col1,
	trim(PURCHASE_ORDER) purchase_order,
	ORDER_TYPE,
	PRIORITY,
	WORK_GROUP,
	ORDER_ID,
	TO_CHAR(sysdate, 'dd-mm-yyyy') Collection_date,
	DISPATCH_METHOD,
	NAME,
	CONTACT,
	ADDRESS1,
	ADDRESS2,
	town,
	county,
	postcode,
	country,
	contact_phone,
	SUBSTR(INSTRUCTIONS,1,35) instructions,
	'1' as no_of_cartons,
	'1' as Weight,
	'99' as local_prod_code,
	'71' as local_service_code
from order_header
where client_id = 'MOUNTAIN'
and consignment = '&2'
and purchase_order is not null
and priority = 1200
order by lower(name), ORDER_ID
)
/


--spool off


