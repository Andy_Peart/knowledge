SET FEEDBACK OFF           
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON

--spool mwplus10wks.csv

SELECT
 'Barcode,Dept,Type,Ref,LPG,Description,Colour,Size,Total Stock,Intake,QC Hold,Suspense,Locked,Picked,Damaged Returns,Returns,Allocated,Bonded,Available,Reserve,WWW,AAA'
FROM
  dual
UNION ALL
SELECT
A||''','||  /* Barcode */
D3||','||  /* Product Group Dept */
D2||','|| /* USDT6 Type */
D1||','|| /* USDT1 Ref */
D4||','|| /* LPG */
B||','|| /* Description */
C||','''|| /* Colour */
D||''','|| /* Size */
H||','||  /* Total Stock */
J||','|| /* Intake */
K||','||  /* QC Hold */
L||','||  /* Suspense */
L2||','|| /* Locked */
M||','||  /* Picked */
N||','||  /* Damaged Returns */
P||','||  /* Returns */
Q||','||  /* Allocated */
X||','|| /* Bonded Stock */
R||','||  /* Available */
RSV||','|| /* Reserve */
CASE 
WHEN R>RSV THEN RSV
ELSE R END||','|| /* WWW */
CASE 
WHEN R>RSV THEN R-RSV
ELSE 0 END  /* AAA */
FROM
  (
    SELECT
      i.sku_id A         /* Barcode */
    , min(receipt_dstamp) ADT
    , sku.description B     /* Description */
    , sku.colour C         /* Colour */
    , sku.sku_size D       /* Size */
    , SKU.USER_DEF_TYPE_1  D1   /* USDT1 */
    , SKU.USER_DEF_TYPE_6 D2   /* USDT6 */
    , SKU.PRODUCT_GROUP D3     /* USDT6 */
    , nvl(sku.user_def_num_3,0) RSV
    , SKU.USER_DEF_TYPE_8 D4   /* LPG */
    , NVL(sum(i.qty_on_hand),0) H     /* Total Stock */
    , NVL(X,0) X         /* Bonded Stock */
    , NVL(J,0) J         /* Intake */
    , NVL(K,0) K         /* QC Hold */
    , NVL(L,0) L         /* Suspense */
    , NVL(L2,0) L2         /* Locked */
    , NVL(M,0) M         /* Picked */
    , NVL(N,0) N         /* Damaged Returns */
    , NVL(P,0) P         /* Returns */
    , NVL(SUM(i.qty_allocated),0) Q /* Allocated */
    , NVL(SUM
        (i.qty_on_hand-i.qty_allocated) /* Available */
          -NVL(J,0)    /* Intake */
          -NVL(K,0)    /* QC Hold */
          -NVL(L2,0)    /* Locked */
          -NVL(M,0)    /* Picked */
          -NVL(N,0)    /* Damaged Returns */
          -NVL(P,0)    /* Returns */
        ,0) R       /* Available Total */  
    FROM
      inventory i 
    ,sku
    , ( /* Intake */
        SELECT DISTINCT
          jc.sku_id JA
        ,SUM(jc.qty_on_hand) J
        FROM 
          inventory jc 
        , location lc
        WHERE
          jc.client_id            ='MOUNTAIN'
          AND jc.location_id      =lc.location_id
          AND jc.site_id          =lc.site_id
          AND lc.loc_type        IN ('Receive Dock')
          AND jc.location_id NOT IN ('Z013')
        GROUP BY
          jc.sku_id
        ,jc.owner_id
      )
    , ( /* QC Hold */
        SELECT DISTINCT
          jc.sku_id JB
        ,SUM(jc.qty_on_hand) K
        FROM
          inventory jc
        WHERE
          jc.client_id       ='MOUNTAIN'
          AND jc.condition_id='QCHOLD'
        GROUP BY
          jc.sku_id
        ORDER BY
          jc.sku_id
      )
    , ( /* Suspense */
        SELECT
          sku_id JC
        , SUM(qty_on_hand-qty_allocated) L
        FROM
          inventory iv
        ,location ll
        WHERE
          iv.client_id      ='MOUNTAIN'
          AND iv.location_id=ll.location_id
          AND iv.site_id    =ll.site_id
          AND ll.loc_type   ='Suspense'
        GROUP BY
          sku_id
      )
    , ( /* BOND */
        SELECT
          sku_id JX
        , SUM(qty_on_hand-qty_allocated) X
        FROM
          inventory iv
        WHERE
          iv.client_id      ='MOUNTAIN'
          AND iv.zone_1='BOND'
          AND iv.user_def_type_5='BOND'
        GROUP BY
          sku_id
      )
    , ( /* Locked */
        SELECT
          sku_id JC2
        , SUM(qty_on_hand-qty_allocated) L2
        FROM
          inventory iv
        ,location ll
        WHERE
          iv.client_id        ='MOUNTAIN'
          AND iv.location_id  =ll.location_id
          AND iv.site_id      =ll.site_id
          AND ll.loc_type     ='Tag-FIFO'
          AND iv.lock_status IN ('Locked','OutLocked')
        GROUP BY
          sku_id
      )
    , ( /* Picked */
        SELECT DISTINCT
          jc.sku_id JD
        ,SUM(jc.qty_on_hand-jc.qty_allocated) M
        FROM
          inventory jc
        , location lc
        WHERE
          jc.client_id      ='MOUNTAIN'
          AND jc.location_id=lc.location_id
          AND jc.site_id    =lc.site_id
          AND lc.loc_type  IN ('ShipDock','Stage')
        GROUP BY
          jc.sku_id
      )
    ,( /* Damaged Returns */
        SELECT
          sku_id JE
        , SUM(qty_on_hand-qty_allocated) N
        FROM
          inventory iv
        WHERE
          iv.client_id        ='MOUNTAIN'
          AND iv.location_id = 'Z013'
        GROUP BY
          sku_id
      )
    ,( /* Returns */
        SELECT
          sku_id JG
        , SUM(qty_on_hand-qty_allocated) P
        FROM
          inventory iv
        WHERE
          iv.client_id='MOUNTAIN'
          AND iv.location_id LIKE 'RET%'
        GROUP BY
          sku_id
      )
    WHERE
      i.client_id      ='MOUNTAIN'
      AND sku.client_id='MOUNTAIN'
      AND i.Sku_id     =sku.sku_id
      AND i.sku_id     = JA (+)
      AND i.sku_id     = JB (+)
      AND i.sku_id     = JC (+)
      AND i.sku_id     = JC2 (+)
      AND i.sku_id     = JD (+)
      AND i.sku_id     = JX (+)
      AND i.sku_id     = JE (+)
      AND i.sku_id     = JG (+)
      AND i.location_id <> 'SUSPENSE'
    GROUP BY
      i.sku_id
    , sku.description
    , sku.colour
    , sku.sku_size
    , SKU.USER_DEF_TYPE_1
    , SKU.USER_DEF_TYPE_6
    , SKU.PRODUCT_GROUP
    , SKU.USER_DEF_TYPE_8
    , nvl(sku.user_def_num_3,0)
    , NVL(J,0)
    , NVL(X,0)
    , NVL(K,0)
    , NVL(L,0)
    , NVL(L2,0)
    , NVL(M,0)
    , NVL(N,0)
    , NVL(P,0)
    ORDER BY
      sku.product_group
    , sku.user_def_type_6
    , sku.description
    , sku.colour
    , sku.sku_size
    , sku.user_def_type_1
  )
      where ADT<sysdate-70
and A not in (select
distinct
sku_id
from inventory_transaction
where client_id='MOUNTAIN'
and code='Pick'
and elapsed_time>0
and Dstamp>sysdate-35)
/

--spool off
