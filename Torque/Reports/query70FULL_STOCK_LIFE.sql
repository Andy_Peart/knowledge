SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 40

--SET TERMOUT OFF
--SPOOL Q70A.csv


select sku_id ||','||
case
   when nvl(BB, 0) > s.user_def_num_3 then
    s.user_def_type_2
   when nvl(BB, 0) > s.user_def_num_4 and nvl(BB, 0) <= s.user_def_num_3 then
    s.user_def_type_2
   else to_char(0)
end as "Parallel Site Allocation"
from sku s,
(select ii.sku_id AA,sum(ii.qty_on_hand - ii.qty_allocated) BB
from inventory ii,(select distinct location_id LL
from location
where loc_type = 'Tag-FIFO'
and location_id not in ('Z001','Z002','Z003','Z004','Z005','Z006','Z007','Z008','Z009','Z010','Z011','Z012','Z013','Z014') )
where ii.client_id = 'MOUNTAIN'
and ii.location_id = LL
group by ii.sku_id)
where client_id = 'MOUNTAIN'
and sku_id = AA(+) 
order by sku_id
/


--SPOOL OFF
--SET TERMOUT ON
