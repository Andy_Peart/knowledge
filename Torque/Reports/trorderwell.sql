ttitle  LEFT 'Stock Allocated but no Consignment' skip 2

select sum(ol.qty_ordered) "Total Ordered", sum (ol.qty_tasked) "Total Tasked", sum(ol.qty_picked) "Total Picked", sum(ol.qty_shipped) "Total Shipped"
from order_line ol, order_header oh
where oh.client_id = 'TR'
and oh.status = 'Allocated'
and oh.consignment is null
and oh.order_id = ol.order_id;

ttitle  LEFT 'Stock Allocated but has Consignment' skip 2

select sum(ol.qty_ordered) "Total Ordered", sum (ol.qty_tasked) "Total Tasked", sum(ol.qty_picked) "Total Picked", sum(ol.qty_shipped) "Total Shipped"
from order_line ol, order_header oh
where oh.client_id = 'TR'
and oh.status = 'Allocated'
and oh.consignment is not null
and oh.order_id = ol.order_id;

ttitle  LEFT 'Stock In Progress but has no Consignment' skip 2

select sum(ol.qty_ordered) "Total Ordered", sum (ol.qty_tasked) "Total Tasked", sum(ol.qty_picked) "Total Picked", sum(ol.qty_shipped) "Total Shipped"
from order_line ol, order_header oh
where oh.client_id = 'TR'
and oh.status = 'In Progress'
and oh.consignment is null
and oh.order_id = ol.order_id;

ttitle  LEFT 'Stock In Progress but has Consignment' skip 2

select sum(ol.qty_ordered) "Total Ordered", sum (ol.qty_tasked) "Total Tasked", sum(ol.qty_picked) "Total Picked", sum(ol.qty_shipped) "Total Shipped"
from order_line ol, order_header oh
where oh.client_id = 'TR'
and oh.status = 'In Progress'
and oh.consignment is not null
and oh.order_id = ol.order_id;
