set feedback off
set pagesize 68
set linesize 240
set verify off

clear columns
clear breaks
clear computes

column D heading 'Date' format a16
column A heading 'Reference' format A20
column B heading 'Qty' format 999999
column M noprint


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

set heading on

ttitle  LEFT 'Units Dispatched from &&3 &&4 to &&5 &&6 - for client ID &&2 as at ' report_date skip 2

break on D skip 1 on report

compute sum label 'Day Total' of B on D
compute sum label 'Total' of B on report



select
trunc(Dstamp) D,
reference_id A,
sum(update_qty) B
from inventory_transaction
where client_id='&&2'
and code='Shipment'
--and (('&&4' is null and Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&5', 'DD-MON-YYYY')+1)
--or ('&&4' is not null and Dstamp between to_date('&&3 &&4', 'DD-MON-YYYY HH24:MI:SS') and to_date('&&5 &&6', 'DD-MON-YYYY HH24:MI:SS')))
and (('&&4' is null and '&&6' is null and Dstamp between to_date('&&3 00:00:00', 'DD-MON-YYYY HH24:MI:SS') and to_date('&&5 23:59:59', 'DD-MON-YYYY HH24:MI:SS'))
or ('&&4' is null and '&&6' is not null and Dstamp between to_date('&&3 00:00:00', 'DD-MON-YYYY HH24:MI:SS') and to_date('&&5 &&6', 'DD-MON-YYYY HH24:MI:SS'))
or ('&&4' is not null and '&&6' is null and Dstamp between to_date('&&3 &&4', 'DD-MON-YYYY HH24:MI:SS') and to_date('&&5 23:59:59', 'DD-MON-YYYY HH24:MI:SS'))
or ('&&4' is not null and '&&6' is not null and Dstamp between to_date('&&3 &&4', 'DD-MON-YYYY HH24:MI:SS') and to_date('&&5 &&6', 'DD-MON-YYYY HH24:MI:SS')))
group by
trunc(Dstamp),
reference_id
order by
trunc(Dstamp),
reference_id
/
