SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','
SET WRAP OFF


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
 
select  'Pre Advice ID, Due Date,Units Due,Receipt Date,Actual Units,Days +/-' from dual
union all
select 
  a.reference_id || ',' ||
  to_char(trunc(c.due_dstamp),'DD/MM/YYYY') ||',' ||
  to_char(b.qty_due)   ||','||
  to_char(trunc(a.dstamp),'DD/MM/YYYY')  ||',' ||
  to_char(a.update_qty)  ||  ',' ||
  to_char(To_date(trunc(a.dstamp),'DD.MM.YYYY') - To_date(trunc(c.due_dstamp),'DD.MM.YYYY') - 2*(trunc(next_day(To_date(trunc(a.dstamp),'DD.MM.YYYY')-1,'FRI')) -
  trunc(next_day(To_date(trunc(c.due_dstamp),'DD.MM.YYYY')-1,'FRI')))/7)   
from 
( 
  select a.key, a.client_id, a.reference_id, a.code, a.line_id, a.update_qty, a.dstamp from inventory_transaction a
  where a.client_id = 'MOUNTAIN'  
  and a.dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
  and a.code = 'Receipt'
  and a.reference_id like 'MWF%' 
      union  
  select a.key, a.client_id, a.reference_id, a.code, a.line_id, a.update_qty, a.dstamp from inventory_transaction_archive a
  where a.client_id = 'MOUNTAIN'  
  and a.dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
  and a.code = 'Receipt'
  and a.reference_id like 'MWF%' 
) a
inner join pre_advice_line b on a.client_id = b.client_id and a.reference_id = b.pre_advice_id and a.line_id = b.line_id
inner join pre_advice_header c on a.client_id = c.client_id and a.reference_id = c.pre_advice_id
where a.client_id = 'MOUNTAIN'  
and a.dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and a.code = 'Receipt'
and a.reference_id like 'MWF%' 
/

 
