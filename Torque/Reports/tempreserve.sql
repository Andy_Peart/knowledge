select * from (select
A "SKU_ID",
D "Amazon",
E "Life",
C,
--C "Successful Reservation",
D+E F,
B
from (select
s.sku_id A,
B,
DECODE(SIGN(to_number(s.user_def_type_2)-B),-1,to_number(s.user_def_type_2),B) D,
DECODE(SIGN(to_number(s.user_def_num_4)-B),-1,to_number(s.user_def_num_4),B) E,
DECODE(SIGN(to_number(s.user_def_num_3)-B),-1,to_number(s.user_def_num_3),B) C
from sku s
,(select iv.sku_id A, sum(iv.qty_on_hand) B
from inventory iv
where iv.client_id = 'MOUNTAIN'
and iv.location_id <> 'DESPATCH'
and iv.location_id <> 'SUSPENSE'
and iv.sku_id <> 'TEST'
group by iv.sku_id)
where s.sku_id = A
and s.client_id = 'MOUNTAIN'
and upper(s.user_def_type_2)=lower(s.user_def_type_2)
and s.sku_id <> 'TEST')
where C > 0)
where C<>F
and B>C
/
