SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240
SET TRIMSPOOL ON



column A format A10
column A2 format A10
column D format A10
column D2 format A16
column E format A40
column F format 99999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



--ttitle  LEFT 'TML Retail - Open Orders Report ' RIGHT 'Printed ' report_date skip 2


--break on A skip 1

--compute sum label 'Grand Total' of QQQ on report
--compute sum label 'Total Units' of QQQ on HH

--spool sb_preadvices.csv

select 'Pre Advice ID,Due Date,SKU,Colour,Description,Qty Due' from DUAL;

select
substr(ph.pre_advice_id,2,20) A,
trunc(ph.due_dstamp) A2,
pl.SKU_ID D,
sku.colour D2,
'"'||sku.description||'"' E,
pl.qty_due F
from pre_advice_header ph,pre_advice_line pl,sku
where ph.CLIENT_ID='SB'
and ph.pre_advice_id like 'P%'
and ph.pre_advice_id=pl.pre_advice_id
and pl.client_id='SB'
and sku.client_id='SB'
and pl.sku_id=sku.sku_id
and ph.due_dstamp>sysdate-93
order by
A,D
/

--spool off
