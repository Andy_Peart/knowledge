SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON


column B heading 'Order Type' format a20
column C2 heading 'Orders' format 999999
column D heading 'Units' format 999999
column F heading 'Date' format a12

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON





break on REPORT on F skip 1

compute sum label 'Report Totals' of C C2 D on REPORT
compute sum label 'Daily Totals' of C C2 D on F 


--spool stdship2.csv
 
select 'Shipments by Date from &&3 to &&4 - for client ID &&2 as at ' from DUAL;

select 'Date,Order Type,Orders,Units' from DUAL;

select
	to_char(it.Dstamp, 'DD/MM/YY') F,
	oh.order_type B,
	count(distinct oh.order_id) C2,
	sum(it.update_qty) D
from inventory_transaction it, order_header oh
where it.Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
	and it.client_id='&&2'
	and it.update_qty<>0
	and it.code='Shipment'
	and it.reference_id=oh.order_id
	and oh.client_id='&&2'
group by
	to_char(Dstamp, 'DD/MM/YY'),
	oh.order_type
union
select
	to_char(it.Dstamp, 'DD/MM/YY') F,
	oh.order_type B,
	count(distinct oh.order_id) C2,
	sum(it.update_qty) D
from inventory_transaction_archive it, order_header_archive oh
where it.Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
	and it.client_id='&&2'
	and it.update_qty<>0
	and it.code='Shipment'
	and it.reference_id=oh.order_id
	and oh.client_id='&&2'
group by
to_char(Dstamp, 'DD/MM/YY'), oh.order_type
order by
F,B
/

--spool off
