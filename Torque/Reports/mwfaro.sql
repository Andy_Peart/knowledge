SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','
set heading on


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240
SET TRIMSPOOL ON

column A heading 'Order Type' format A12
column AA heading 'Order' format A12
column BB heading 'Date' format A14
column CC heading 'Order Qty' format 999999
column DD heading 'Order Qty' format 999999

--spool mwretail.csv

select 'Order Id,Ordered,Allocated' from DUAL;

select
--DDD A,
AAA AA,
--to_char(BBB,'DD-MON-YYYY') BB,
OOO CC,
TTT DD
from (select
oh.order_type  DDD,
oh.order_id  AAA,
oh.creation_date BBB,
nvl(sum(ol.qty_ordered),0) OOO,
nvl(sum(ol.qty_tasked),0) TTT
from order_header oh, order_line ol
where oh.client_id='MOUNTAIN'
and oh.status='Allocated'
and oh.order_type not like 'WEB'
and oh.order_id=ol.order_id
group by
oh.order_type,
oh.order_id,
oh.creation_date)
where OOO<>TTT
order by
AA
/

--spool off


