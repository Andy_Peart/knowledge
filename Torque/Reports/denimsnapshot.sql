set feedback on


insert into denim_snapshot(
the_date,the_sku,the_zone,the_qty)
select
trunc(sysdate),
sku_id,
zone_1,
sum(qty_on_hand)
from inventory
where client_id='DN'
group by
sku_id,
zone_1
/

commit
/
