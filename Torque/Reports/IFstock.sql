SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 160
SET TRIMSPOOL ON



--break on report
--compute sum label 'Total' of E F on report

--spool IFstock.csv


select 'Product,Description,Qty On Hand,Qty Allocated,Location,Zone' from DUAL;


select
AAA||','||
DESCR||','||
Q1||','||
Q2||','||
LOC||','||
ZZZ
from (select
iv.sku_id AAA,
sku.user_def_note_1 DESCR,
sum(iv.qty_on_hand) Q1,
sum(iv.qty_allocated) Q2,
iv.location_id LOC,
iv.zone_1 ZZZ
from inventory iv,sku
where iv.client_id='IF'
and iv.zone_1 not in ('ShipDock','Receive Dock')
and iv.client_id=sku.client_id
and iv.sku_id=sku.sku_id
group by
iv.sku_id,
sku.user_def_note_1,
iv.location_id,
iv.zone_1)
order by 1
/

select
AAA||','||
DESCR||','||
Q1||','||
Q2||','||
LOC||','||
ZZZ
from (select
iv.sku_id AAA,
sku.user_def_note_1 DESCR,
sum(iv.qty_on_hand) Q1,
sum(iv.qty_allocated) Q2,
iv.location_id LOC,
iv.zone_1 ZZZ
from inventory iv,sku
where iv.client_id='IF'
and iv.zone_1='Receive Dock'
and iv.client_id=sku.client_id
and iv.sku_id=sku.sku_id
group by
iv.sku_id,
sku.user_def_note_1,
iv.location_id,
iv.zone_1)
order by 1
/

--spool off


