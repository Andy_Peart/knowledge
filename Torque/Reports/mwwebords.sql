SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Orders,Units,Date,Priority' from dual
union all
select "Ord" || ',' || "Qty" || ',' || "Date to group with" || ',' || "Pri"
from (
select count(distinct "Orders") as "Ord", sum("Qty") as "Qty", "Date to group with", "Pri"
from
(
select oh.order_id          as "Orders"
, sum(ol.qty_ordered)       as "Qty"
, case  when oh.priority like '8%' and to_char(oh.creation_date, 'HH24:MI:SS') > '&&4' then to_char(oh.creation_date+1, 'DD-MON-YYYY')
        when oh.priority not like '8%' and to_char(oh.creation_date, 'HH24:MI:SS') > '&&5' then to_char(oh.creation_date+1, 'DD-MON-YYYY')
        else to_char(oh.creation_date, 'DD-MON-YYYY') end as "Date to group with"
, case when oh.priority like '8%' then '8* Priorities' else '<>8* Priorities' end as "Pri"
from order_header oh
inner join order_line ol on ol.order_id = oh.order_id and ol.client_id = oh.client_id
where oh.client_id = 'MOUNTAIN'
and substr(oh.work_group,2,2) = 'WW'
and oh.creation_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by case  when oh.priority like '8%' and to_char(oh.creation_date, 'HH24:MI:SS') > '&&4' then to_char(oh.creation_date+1, 'DD-MON-YYYY')
        when oh.priority not like '8%' and to_char(oh.creation_date, 'HH24:MI:SS') > '&&5' then to_char(oh.creation_date+1, 'DD-MON-YYYY')
        else to_char(oh.creation_date, 'DD-MON-YYYY') end
, case when oh.priority like '8%' then '8* Priorities' else '<>8* Priorities' end
, oh.order_id
)
group by "Date to group with", "Pri"
order by 3
)
/
--spool off