set pagesize 68
set linesize 140
set verify off

clear columns
clear breaks
clear computes

column A heading 'SKU' format a20
column B heading 'DESCRIPTION' format a40
column C heading 'Qty' format 999999
column D heading 'Location' format a16
column E heading 'User Name' format a16
column F heading 'Date' format a16

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MM') curdate from DUAL;
set TERMOUT ON


ttitle  CENTER 'Locked Stock Report - for client ID &2 as at ' report_date skip 2

break on report

compute sum label 'Total' of C on report


select
iv.sku_id A,
sku.description B,
iv.qty_on_hand C,
iv.location_id D,
it.user_id E,
to_char(it.dstamp,'DD/MM/YY') F
from inventory iv,sku,Inventory_transaction it
where iv.client_id='&2'
and iv.lock_status='Locked'
and sku.client_id='&2'
and iv.sku_id=sku.sku_id
and it.client_id='&2'
and iv.sku_id=it.sku_id
and it.code='Inv Lock'
and it.dstamp=iv.count_dstamp
/
