SET FEEDBACK OFF
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
set colsep ','
set heading off
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240
SET TRIMSPOOL ON

set feedback on


update order_line
set user_def_chk_4='M'
where rowid in (
  select ol.rowid
  from order_header oh inner join order_line ol on oh.order_id = ol.order_id and oh.client_id = ol.client_id
  where oh.Client_ID in ('MOUNTAIN','AA')
  and oh.status in ('Allocated','Released','In Progress','Hold')
  and oh.order_date>sysdate-1
  and ol.qty_ordered>=0
  and ol.user_def_chk_4='N'
  and 
  (
    (nvl(ol.qty_tasked,0)+nvl(ol.qty_picked,0)<ol.qty_ordered)
    or 
    (nvl(ol.qty_tasked,0)+nvl(ol.qty_picked,0) = 0)
  )
  )
/

--spool jjj.csv 
select 'Order Date,Order ID,Status,Original Qty,Qty Ordered,Qty Allocated,Qty Picked,Qty on Hand,Reserve,SKU ID,Description,Colour,Size,' from dual;
 
select
to_char(oh.order_date, 'DD/MM/YYYY HH24:MI') ||','||
oh.order_id  ||','||
oh.status  ||','||
nvl(ol.user_def_num_3,0) ||','||
nvl(ol.qty_ordered,0) ||','||
nvl(ol.qty_tasked,0) ||','||
nvl(ol.qty_picked,0) ||','||
nvl(QQQ,0)||','||
sku.user_def_num_3||','''||
ol.sku_id  ||''','||
sku.description  ||','||
sku.colour  ||','||
sku.sku_size
from order_header oh inner join order_line ol on oh.order_id = ol.order_id and oh.client_id = ol.client_id
inner join sku on sku.client_id = oh.client_id and sku.client_id = ol.client_id and sku.sku_id = ol.sku_id
left join 
  (
  select sku_id SSS, client_id, sum(qty_on_hand) QQQ
  from inventory
  where Client_ID in ('MOUNTAIN','AA')
  and location_id<>'SUSPENSE'
  group by sku_id, client_id
  ) Tab
on Tab.SSS = ol.sku_id and Tab.client_id = ol.client_id
where oh.Client_ID in ('MOUNTAIN','AA')
and oh.order_date>sysdate-1
and ol.user_def_chk_4='M'
order by to_char(oh.order_date, 'DD/MM/YYYY'), oh.order_id
/

--spool off


update order_line
set user_def_chk_4='Y'
where client_id in ('MOUNTAIN','AA')
and user_def_chk_4 = 'M'
/


commit;

