SET FEEDBACK OFF                 
set verify off
set wrap off
set colsep ','

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 300
set trimspool on


--spool eds34.csv


select * from
(Select 'Client,Order Type,Qty Ordered,Qty Allocated,Qty Shipped,% Allocated,% Shipped' from DUAL
union all
select * from (select
upper(AA)||','||
DECODE(BB,'ZZZ','Adjustments',BB)||','||
nvl(Q1,0)||','||
nvl(Q2,0)||','||
nvl(Q3,0)||','||
to_char((Q2/Q1)*100,'999.00')||','||
to_char((Q3/Q2)*100,'999.00')
from (select
AA,
BB,
--OID,
sum(QQ1) Q1,
sum(QQQ) Q2,
sum(QQ2) Q3
from (select
oh.client_id AA,
DECODE(oh.client_id,'TR','ALL ORDERS','T','ALL ORDERS',nvl(order_type,' ')) BB,
oh.order_id OID,
sum(qty_ordered) QQ1,
sum(qty_shipped) QQ2
from order_header oh,order_line ol
where oh.status='Shipped'
--and oh.order_id like 'WWW2903%'
and oh.client_id like '%&&5'
and oh.shipped_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and oh.client_id=ol.client_id
and oh.order_id=ol.order_id
group by
oh.client_id,
DECODE(oh.client_id,'TR','ALL ORDERS','T','ALL ORDERS',nvl(order_type,' ')),
oh.order_id),(select
client_id CID,
reference_id RID,
sum(original_qty) QQQ
from inventory_transaction
where code='Pick'
and client_id like '%&&5'
and Dstamp > to_date('&&3', 'DD-MON-YYYY')-7
and to_loc_id='CONTAINER'
group by
client_id,
reference_id)
where AA=CID
and OID=RID
group by
AA,BB)
order by 1))
where '&&2'='N'
union all
select * from
(Select 'Client,Order Type,Customer,Qty Ordered,Qty Allocated,Qty Shipped,% Allocated,% Shipped' from DUAL
union all
select * from (select
upper(AA)||','||
DECODE(BB,'ZZZ','Adjustments',BB)||','||
CUS||','||
nvl(Q1,0)||','||
nvl(Q2,0)||','||
nvl(Q3,0)||','||
to_char((Q2/Q1)*100,'999.00')||','||
to_char((Q3/Q2)*100,'999.00')
from (select
AA,
BB,
CUS,
--OID,
sum(QQ1) Q1,
sum(QQQ) Q2,
sum(QQ2) Q3
from (select
oh.client_id AA,
DECODE(oh.client_id,'TR','ALL ORDERS','T','ALL ORDERS',nvl(order_type,' ')) BB,
oh.order_id OID,
oh.customer_id CUS,
sum(qty_ordered) QQ1,
sum(qty_shipped) QQ2
from order_header oh,order_line ol
where oh.status='Shipped'
--and oh.order_id like 'WWW2903%'
and oh.client_id like '%&&5'
and oh.shipped_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and oh.client_id=ol.client_id
and oh.order_id=ol.order_id
group by
oh.client_id,
DECODE(oh.client_id,'TR','ALL ORDERS','T','ALL ORDERS',nvl(order_type,' ')),
oh.order_id,
oh.customer_id),(select
client_id CID,
reference_id RID,
sum(original_qty) QQQ
from inventory_transaction
where code='Pick'
and client_id like '%&&5'
and Dstamp > to_date('&&3', 'DD-MON-YYYY')-7
and to_loc_id='CONTAINER'
group by
client_id,
reference_id)
where AA=CID
and OID=RID
group by
AA,BB,CUS)
order by 1))
where '&&2'='Y'
/



--spool off



