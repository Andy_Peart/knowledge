SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET TRIMSPOOL ON

with shipped as (
    select to_char(itl.dstamp,'DD-MM-YYYY') shipped, itl.reference_id, sum(itl.update_qty) qty
    from inventory_transaction itl
    where itl.client_id = 'SP'
    and itl.code = 'Shipment'
    and itl.dstamp between to_date('&&2') and to_date('&&3')+1
    group by to_char(itl.dstamp,'DD-MM-YYYY'), itl.reference_id
),
altered as (
    select itl.reference_id, sum(itl.update_qty) qty
    from inventory_transaction itl
    where itl.client_id = 'SP'
    and itl.code = 'Pick'
    and itl.to_loc_id = 'KITTING'
    and itl.from_loc_id || itl.to_loc_id <> 'CONTAINERKITTING'
    and itl.dstamp between to_date('&&2')-7 and to_date('&&3')+1
    group by itl.reference_id
)
select 'Date shipped,Order Id,Total shipped,Alteration shiped' from dual
union all
select shipped || ',' || reference_id || ',' || shqty || ',' || nvl(altqty,0)
from (
	select sh.shipped, sh.reference_id, sh.qty shqty, alt.qty altqty
	from shipped sh left join altered alt on sh.reference_id = alt.reference_id
	order by 1,2
)
/