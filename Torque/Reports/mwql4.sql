set pagesize 68
set linesize 120
set verify off
set newpage 0

break on row skip 1

ttitle left "Mountain Warehouse - Location Range Search Between '&&2' and '&&3'" skip 2

select sku.sku_id SKU, sku.description, inv.location_id Location, inv.qty_on_hand Qty, '__________' Moved
from sku, inventory inv
where sku.client_id = 'MOUNTAIN'
and sku.sku_id = inv.sku_id
and inv.location_id not like 'MARSHAL'
and inv.zone_1 not like 'Z3OUT'
and inv.location_id between upper ('&&2') and upper ('&&3')
order by 3
/
/
/
/


