/******************************************************************************/
/*                                                                            */
/*                            Elite                                           */
/*                                                                            */
/*     FILE NAME  :   mwshortages3.sql                                        */
/*                                                                            */
/*   DATE     BY   PROJ     ID         DESCRIPTION                            */
/*   ======== ==== ======   ========   =============                          */
/*   26/06/07 BK   MM                  Units Picked by User ID (Date Range)   */
/******************************************************************************/

/* Input Parameters are as follows */
-- 1)  From Date
-- 2)  To Date


set feedback off
set pagesize 68
set linesize 240
set newpage 0
set verify off

clear columns
clear breaks
clear computes

column A heading 'User ID' format A16
column B heading 'Qty' format 999999
column C heading 'Lines' format 999999
column D heading 'Orders' format 999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'Units Picked from &&3 &&4 to &&5 &&6 - for client ID &&2 as at ' report_date skip 2

break on report
compute sum label 'Totals' of C D B on report


select
user_id A,
count(*) C,
count(distinct reference_id) D,
sum(update_qty) B
from inventory_transaction
where client_id='&&2'
and code='Pick'
and update_qty>0
and (elapsed_time>0 or list_id is not null)
--and (('&&4' is null and Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&5', 'DD-MON-YYYY')+1)
and (('&&4' is null and '&&6' is null and Dstamp between to_date('&&3 00:00:00', 'DD-MON-YYYY HH24:MI:SS') and to_date('&&5 23:59:59', 'DD-MON-YYYY HH24:MI:SS'))
or ('&&4' is null and '&&6' is not null and Dstamp between to_date('&&3 00:00:00', 'DD-MON-YYYY HH24:MI:SS') and to_date('&&5 &&6', 'DD-MON-YYYY HH24:MI:SS'))
or ('&&4' is not null and '&&6' is null and Dstamp between to_date('&&3 &&4', 'DD-MON-YYYY HH24:MI:SS') and to_date('&&5 23:59:59', 'DD-MON-YYYY HH24:MI:SS'))
or ('&&4' is not null and '&&6' is not null and Dstamp between to_date('&&3 &&4', 'DD-MON-YYYY HH24:MI:SS') and to_date('&&5 &&6', 'DD-MON-YYYY HH24:MI:SS')))
group by user_id
order by user_id
/

