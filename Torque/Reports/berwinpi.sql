SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 800
SET TRIMSPOOL ON
 

column A heading 'Ref' format A10
column B heading 'Code' format A8
column C heading 'Description' format A40
column D heading 'Cost' format 99990.00
column E heading 'Units' format 99999
column E1 heading 'Units' format 99999
column F heading 'Total' format 99990.00


break on report
compute sum label 'Total' of F on report

--spool bpi.csv


select 'REFNumber,Refurb Code,Refurb Description,Customer,Cost Per Unit,Units Received,Units Processed,Total Cost' from DUAL;


select
A1 A,
A2 B,
A3 C,
A99,
A4 D,
A6 E,
A5 E1,
--A4*A5/100 F
A4*A5 F
from (select
j.job_id A1,
substr(j.description,1,3) A2,
sku.description A3,
sku.user_def_num_1 A4,
CASE
WHEN instr(j.description,' ',5)=0
THEN to_number(substr(j.description,5,4))
ELSE to_number(substr(j.description,5,(instr(j.description,' ',5)-5)))
END as A5, 
--to_number(substr(j.description,5,4)) A5,
CASE
WHEN instr(j.description,' ',5)=0
THEN '' 
ELSE ltrim(substr(j.description,(instr(j.description,' ',5)),32))
END as A99,
--substr(j.description,9,32) A99,
nvl(QR,0) A6
from jobs j,sku,(select
substr(pl.pre_advice_id,5,6) PID,
sum(pl.qty_received) QR
from pre_advice_line pl
where pl.client_id='BW'
group by pl.pre_advice_id)
where j.job_id like 'RE%'
and j.client_id='BW'
--and upper(substr(j.description,5,4))=lower(substr(j.description,5,4))
and j.internal_linked='N'
and substr(j.job_id,4,6)=PID (+)
and sku.client_id='BW'
and sku.sku_id=substr(j.description,1,3))
order by A
/

--spool off

set feedback on

update jobs
set internal_linked='Y'
where job_id like 'RE%'
and client_id='BW'
and internal_linked='N'
/

--rollback;
commit;
 