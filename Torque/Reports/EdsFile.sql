SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240


select 'Location,SKU,Description,EAN,Condition,Qty on Hand' from DUAL;

Select
iv.Location_ID ||','''||
iv.SKU_ID ||''','||
SKU.Description ||','''||
SKU.ean ||''','||
iv.Condition_id ||','||
sum(iv.qty_on_hand)
from inventory iv,sku
where iv.client_id='&&2'
and iv.client_id=sku.client_id
and iv.sku_id=sku.sku_id
group by
iv.Location_ID,
iv.SKU_ID,
SKU.Description,
SKU.ean,
iv.Condition_id
order by
iv.Location_ID,
iv.SKU_ID
/

