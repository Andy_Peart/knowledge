SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','
SET WRAP OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 1900


SELECT 'Pre Advice,Supplier,Creation Date,Due Date,Started,Completed,Qty due,Qty received,Difference,Owner ID' from dual
union all
select pre_advice_id || ',' || supplier_id || ',' || created || ',' || due_date || ',' || started || ',' || completed || ',' || 
	qty_due || ',' || qty_received || ',' || diff || ',' || owner_id
from (
	select ph.pre_advice_id, ph.supplier_id, 
		to_char(ph.creation_date,'DD-MON-YYYY HH24:MI') as created,
		to_char(ph.due_dstamp,'DD-MON-YYYY HH24:MI') as due_date, 
		to_char(ph.ACTUAL_DSTAMP   ,'DD-MON-YYYY HH24:MI') as started, 
		to_char(ph.FINISH_DSTAMP  ,'DD-MON-YYYY HH24:MI') as completed, 
		sum(pl.qty_due) qty_due, 
		sum(pl.qty_received) qty_received,
		sum(pl.qty_received)  - sum(pl.qty_due) as diff, 
		pl.owner_id
	from pre_advice_header ph inner join pre_advice_line pl on ph.pre_advice_id = pl.pre_advice_id and ph.client_id = pl.client_id
	where ph.client_id = 'CC'
		and ph.status in ('Complete')
		and ph.finish_dstamp between to_date('&&2') and to_date('&&3')+1
		--and nvl(supplier_id,'X') <> 'TRANSFER'
	group by ph.pre_advice_id, ph.supplier_id, to_char(ph.creation_date,'DD-MON-YYYY HH24:MI'),
		to_char(ph.due_dstamp,'DD-MON-YYYY HH24:MI'), 	to_char(ph.FINISH_DSTAMP  ,'DD-MON-YYYY HH24:MI'), 
		to_char(ph.ACTUAL_DSTAMP   ,'DD-MON-YYYY HH24:MI'),	pl.owner_id
	order by 1
)
/