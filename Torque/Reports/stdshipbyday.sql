SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Date,Order Type,Orders Shipped,Units Shipped,Units Picked' from dual
union all
select "Shipped Date" ||','|| "Order Type" ||','|| "Orders" ||','|| "Qty Shipped" ||','|| "Qty Picked"
from
(
with pick_units as (
    select sum(it.update_qty) qty
    , trunc(it.dstamp) dstamp
    , oh.order_type ord_type
    from inventory_transaction it
    inner join order_header oh on oh.order_id = it.reference_id and oh.client_id = it.client_id
    where it.client_id = '&&2'
    and it.code = 'Pick'
    and it.dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
    group by trunc(it.dstamp)
    , oh.order_type
)
select trunc(oh.shipped_date)   as "Shipped Date"
, oh.order_type                 as "Order Type"
, count(distinct oh.order_id)   as "Orders"
, sum(ol.qty_shipped)           as "Qty Shipped"
, pu.qty                        as "Qty Picked"
from order_header oh
inner join order_line ol on ol.order_id = oh.order_id and ol.client_id = oh.client_id
left join pick_units pu on pu.dstamp = trunc(oh.shipped_date) and pu.ord_type = oh.order_type
where oh.client_id = '&&2'
and oh.shipped_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
group by trunc(oh.shipped_date)
, oh.order_type
, pu.qty
order by 1, 2
)
/
--spool off