SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET COLSEP ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET HEADING OFF
SET PAGESIZE 0
SET LINESIZE 200
SET TRIMSPOOL ON

column A heading 'Order Ref' format A20
column C heading 'Customer' format A12
column E heading 'Date' format A12
column B heading 'Units Shipped' format 9999999

break on C skip 1 on report 
compute sum label 'Cust Total' of B on C
compute sum label 'Full Total' of B on report

select 'Prominent Shipments by Customer' from DUAL;
select to_char(SYSDATE, 'DD/MM/YY    HH:MI:SS') from DUAL;


select 'Customer,Order Ref,Date,Units,Type' from DUAL;

select
sku.user_def_type_2 C,
itl.reference_id A,
to_char(itl.Dstamp,'DD/MM/YYYY') E,
nvl(sum(itl.update_qty*nvl(sku.user_def_type_9,1)),0) B,
DECODE(sku.user_def_type_1,'H','Hanging','Boxed') D
from inventory_transaction itl,sku
where itl.client_id='PR'
and itl.site_id='PROM'
--and i.zone_1 like 'LSH1'
and itl.code='Shipment'
and itl.Dstamp>sysdate-8
and itl.from_loc_id='PR'
and sku.client_id='PR'
and itl.sku_id=sku.sku_id
group by
sku.user_def_type_2,
itl.reference_id,
to_char(itl.Dstamp,'DD/MM/YYYY'),
DECODE(sku.user_def_type_1,'H','Hanging','Boxed')
order by C,A
/
