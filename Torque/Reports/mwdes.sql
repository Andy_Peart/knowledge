set pagesize 68
set linesize 120
set verify off

clear columns
clear breaks
clear computes

column AA heading 'T2TBARCODE' format a10
column A heading 'EXTBARCODE' format a15
column B heading 'DATE OUT'
column BB heading 'TIME OUT'
column C heading 'QTY OUT' format 999999
column AAA heading 'DESC' format a15
column AAB heading 'SIZE' format a7
column ABB heading 'COLOUR' format a7
column D heading 'DEST' format a9
column E heading 'TRID' format a6

ttitle center "Mountain Warehouse - Goods Despatch Report." skip 3

break on e skip 2

compute sum label 'TOTAL' of c on E

select substr(s.ean, 1, 6) AA,
inv.sku_id A,
s.user_def_type_2 AAA,
s.sku_size AAB,
s.colour ABB,
to_char(inv.dstamp, 'dd/MM/YYYY') B,
to_char(inv.dstamp, 'hh24:mi') BB,
oh.customer_id D,
inv.reference_id E,
inv.update_qty C
from inventory_transaction inv, sku s, order_header oh
where inv.client_id = 'MOUNTAIN'
and to_char(inv.dstamp, 'dd/mm/yy') between upper ('&1') and upper ('&2')
and inv.code = 'Shipment'
and inv.sku_id=s.sku_id
and inv.reference_id = oh.order_id
order by inv.dstamp
/
