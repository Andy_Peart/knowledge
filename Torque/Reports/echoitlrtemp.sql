SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON

SET FEEDBACK ON

update inventory_transaction
set uploaded='R'
where client_id = 'ECHO'
and (code like 'Receipt%' or (code='Adjustment' and reason_id='RECRV'))
and uploaded = 'N'
/


if (select count(*) 
from inventory_transaction
where client_id = 'ECHO'
and (code like 'Receipt%' or (code='Adjustment' and reason_id='RECRV'))
and uploaded = 'R') = 0 then exit;








--SET FEEDBACK OFF            

COLUMN NEWSEQ NEW_VALUE SEQNO


select 'S://redprairie/apps/home/dcsdba/reports/TEMP/ECHORTEMP'||substr(to_char(10000000+next_seq),2,8)||'.csv' NEWSEQ from sequence_file
--select 'LIVE/ECHOR'||substr(to_char(10000000+next_seq),2,8)||'.csv' NEWSEQ from sequence_file
where control_id=24
/

--set termout off
spool &SEQNO


select 'ITL'||','
||'E'||','
||code||','
||decode(sign(original_qty), 1, '+', -1, '-', '+')||','
||nvl(original_qty,0)||','
||decode(sign(update_qty), 1, '+', -1, '-', '+')||','
||update_qty||','
||to_char(dstamp, 'YYYYMMDDHH24MISS')||','
||client_id||','
||sku_id||','
||from_loc_id||','
||to_loc_id||','
||final_loc_id||','
||tag_id||','
||reference_id||','
||line_id||','
||condition_id||','
||notes||','
||reason_id||','
||batch_id||','
||to_char(expiry_dstamp, 'YYYYMMDDHH24MISS')||','
||user_id||','
||shift||','
||station_id||','
||site_id||','
||container_id||','
||pallet_id||','
||list_id||','
||owner_id||','
||origin_id||','
||work_group||','
||consignment||','
||to_char(manuf_dstamp, 'YYYYMMDDHH24MISS')||','
||lock_status||','
||qc_status||','
||session_type||','
||summary_record||','
||elapsed_time||','
||supplier_id||','
||user_def_type_1||','
||user_def_type_2||','
||user_def_type_3||','
||user_def_type_4||','
||user_def_type_5||','
||user_def_type_6||','
||user_def_type_7||','
||user_def_type_8||','
||user_def_chk_1||','
||user_def_chk_2||','
||user_def_chk_3||','
||user_def_chk_4||','
||to_char(user_def_date_1, 'YYYYMMDDHH24MISS')||','
||to_char(user_def_date_2, 'YYYYMMDDHH24MISS')||','
||to_char(user_def_date_3, 'YYYYMMDDHH24MISS')||','
||to_char(user_def_date_4, 'YYYYMMDDHH24MISS')||','
||user_def_num_1||','
||user_def_num_2||','
||user_def_num_3||','
||user_def_num_4||','
||user_def_note_1||','
||user_def_note_2||','
||From_Site_Id||','
||To_Site_Id||',,'
--||Time_Zone_Name||','
||Job_Id||','
||Job_Unit||','
||Manning||','
||Spec_Code||','
||Config_Id||','
||Estimated_Time||','
||Task_Category||','
||Sampling_Type||','
||to_char(Complete_Dstamp, 'YYYYMMDDHH24MISS')||','
||Grn||','
||to_char(dstamp, 'YYYYMMDDHH24MISS')||','
||Group_Id||','
||Uploaded||','
||Uploaded_Vview||','
||Uploaded_Ab||','
||Sap_Idoc_Type||','
||Sap_Tid||','
||Ce_Orig_Rotation_Id||','
||Ce_Rotation_Id||','
||Ce_Consignment_Id||','
||Ce_Receipt_Type||','
||Ce_Originator||','
||Ce_Originator_Reference||','
||Ce_Coo||','
||Ce_Cwc||','
||Ce_Ucr||','
||Ce_Under_Bond||','
||to_char(Ce_Document_Dstamp, 'YYYYMMDDHH24MISS')||','
||Uploaded_Customs||','
||Uploaded_Labor||','
||Asn_Id||','
||Customer_Id||','
||Print_Label_Id||','
||Lock_Code||','
||Ship_Dock||','
||Ce_Duty_Stamp||','
||Pallet_Grouped
from inventory_transaction 
where client_id = 'ECHO'
and (code like 'Receipt%' or (code='Adjustment' and reason_id='RECRV'))
and uploaded = 'R'
/


spool off

SET FEEDBACK ON

update inventory_transaction
set uploaded='Y'
where client_id = 'ECHO'
and (code like 'Receipt%' or (code='Adjustment' and reason_id='RECRV'))
and uploaded = 'R'
/

Update sequence_file
set next_seq=next_seq+1
where control_id=24 
/


--commit;
rollback;

