SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240
SET TRIMSPOOL ON

select 'Stock Summary Report - for client IS - as at  ' || to_char(sysdate,'dd/mm/yy  hh24:mi:ss') from dual
union all
select
sku_style  ||','|| description  ||','|| colour  ||','|| sum(qty_on_hand)  ||','|| nvl(J,0)  ||','||
nvl(K,0)  ||','|| nvl(L,0)  ||','|| nvl(L2,0)  ||','|| nvl(M,0)  ||','|| nvl(sum(qty_allocated),0)  ||','|| 
nvl(sum(qty_on_hand-qty_allocated)-nvl(J,0)-nvl(K,0)-nvl(L,0)-nvl(L2,0)-nvl(M,0),0)
from (
select substr(i.sku_id,1,instr(i.sku_id,'-',1,3)-1) sku_style, sku.description , sku.colour,i.qty_on_hand, J, K, L, L2, M, i.qty_allocated
from inventory i,sku,
	(
	select distinct substr(jc.sku_id,1,instr(jc.sku_id,'-',1,3)-1) JA,sum(jc.qty_on_hand) J 
	from inventory jc, location lc
	where jc.client_id='IS'
		and jc.location_id=lc.location_id
		and jc.site_id=lc.site_id
		and lc.loc_type in ('Receive Dock')
	group by substr(jc.sku_id,1,instr(jc.sku_id,'-',1,3)-1),jc.owner_id
	), 
	(
	select distinct substr(jc.sku_id,1,instr(jc.sku_id,'-',1,3)-1) JB,sum(jc.qty_on_hand) K 
	from inventory jc
	where jc.client_id='IS'
		and jc.condition_id='QCHOLD'
	group by substr(jc.sku_id,1,instr(jc.sku_id,'-',1,3)-1)
	),
	(
	select substr(iv.sku_id,1,instr(iv.sku_id,'-',1,3)-1) JC, sum(qty_on_hand-qty_allocated) L
	from inventory iv,location ll
	where iv.client_id='IS'
		and iv.location_id=ll.location_id
		and iv.site_id=ll.site_id
		and ll.loc_type='Suspense'
	group by substr(iv.sku_id,1,instr(iv.sku_id,'-',1,3)-1)
	),
	(
	select substr(iv.sku_id,1,instr(iv.sku_id,'-',1,3)-1) JC2, sum(qty_on_hand-qty_allocated) L2
	from inventory iv,location ll
	where iv.client_id='IS'
		and iv.location_id=ll.location_id
		and iv.site_id=ll.site_id
		and iv.lock_status in ('Locked','OutLocked')
	group by substr(iv.sku_id,1,instr(iv.sku_id,'-',1,3)-1)
	),
	(
	select distinct substr(jc.sku_id,1,instr(jc.sku_id,'-',1,3)-1) JD,sum(jc.qty_on_hand) M 
	from inventory jc, location lc
	where jc.client_id='IS'
	and jc.location_id=lc.location_id
	and jc.site_id=lc.site_id
	and lc.loc_type in ('ShipDock','Stage')
	group by substr(jc.sku_id,1,instr(jc.sku_id,'-',1,3)-1)
	)
where i.client_id='IS'
and sku.client_id='IS'
and i.sku_id=sku.sku_id
and substr(i.sku_id,1,instr(i.sku_id,'-',1,3)-1) = JA (+)
and substr(i.sku_id,1,instr(i.sku_id,'-',1,3)-1) = JB (+)
and substr(i.sku_id,1,instr(i.sku_id,'-',1,3)-1) = JC (+)
and substr(i.sku_id,1,instr(i.sku_id,'-',1,3)-1) = JC2 (+)
and substr(i.sku_id,1,instr(i.sku_id,'-',1,3)-1) = JD (+)
order by 1
)
group by sku_style , description, colour, J,K,L,L2,M
/

--spool off
--SET TERMOUT ON


