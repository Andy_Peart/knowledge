SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Client,Sku,Description,User Def 5,User Def 1,User Def 7,Code,Reference' from dual
union all
select "Cli" || ',' || "Sku" || ',' || "Desc" || ',' || "U5" || ',' || "U1" || ',' || "U7" || ',' || "Code" || ',' || "Ref" from
(
select * from
(
select it.client_id     as "Cli"
, it.sku_id             as "Sku"
, s.description         as "Desc"
, it.user_def_type_5    as "U5"
, it.user_def_type_1    as "U1"
, it.user_def_type_7    as "U7"
, it.code               as "Code"
, it.reference_id       as "Ref"
from inventory_transaction it
inner join sku s on s.client_id = it.client_id and s.sku_id = it.sku_id
where it.client_id = 'TR'
and it.code = 'Shipment'
order by it.reference_id
)
where "Ref" in (
select regexp_substr('&&2','[^,]+',1,level) from dual
connect by regexp_substr('&&2','[^,]+',1,level) is not null
)
)
/
--spool off