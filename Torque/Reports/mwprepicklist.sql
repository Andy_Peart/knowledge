SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 2000
set trimspool on
set newpage 0
set verify off
set colsep ','

--spool mwprepicklist.csv

select 'Zone,Branch,Consignment,Picks,Units,KPI,Time in Minutes' from DUAL
union all
select
A||','||
B||','||
B2||','||
C||','||
D||','||
E||','||
replace(to_char(C*60/E,'9990.99'),' ','')
from (select
work_zone A,
mt.work_group B,
mt.consignment B2,
count(*) C,
sum(qty_to_move) D,
DECODE(work_zone,
'FOOT',77,
'EQIP',59,
'MJAC',88,
'WJAC',88,
'WBOT',131,
'ACCS',117,
'MTOP',131,
'WTOP',131,
'KOUT',131,
'KAPP',131,
'MBOT',131,1) E
from move_task mt
where mt.client_id='MOUNTAIN'
and mt.task_type='O'
and work_zone<>'12'
and mt.from_loc_id not in ('CONTAINER')
and mt.Dstamp>trunc(sysdate)-6/24
and mt.work_group<>'WWW'
and mt.Task_id like 'MOR%'
group by
work_zone,
mt.work_group,
mt.consignment
union
select
work_zone A,
'[Total]' B,
' ' B2,
count(*) C,
sum(qty_to_move) D,
DECODE(work_zone,
'FOOT',77,
'EQIP',59,
'MJAC',88,
'WJAC',88,
'WBOT',131,
'ACCS',117,
'MTOP',131,
'WTOP',131,
'KOUT',131,
'KAPP',131,
'MBOT',131,1) E
from move_task mt
where mt.client_id='MOUNTAIN'
and mt.task_type='O'
and work_zone<>'12'
and mt.from_loc_id not in ('CONTAINER')
and mt.Dstamp>trunc(sysdate)-6/24
and mt.work_group<>'WWW'
and mt.Task_id like 'MOR%'
group by
work_zone
order by 1)
where E>1
/

--spool off
