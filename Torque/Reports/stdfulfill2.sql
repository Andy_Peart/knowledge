SET FEEDBACK OFF                 
set verify off
set wrap off
set colsep ','

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 300
set trimspool on


Select 'Date,Order Type,,Qty Ordered,Qty Allocated,Qty Shipped,% Allocation Fulfilled,% Order Fulfilled' from DUAL
union all
select replace(FRED,'  ',' ') from (
    select to_char(DD,'YYMMDD')
    ||
    ',0,' MM, 
    DD||','|| BB||','|| BB1 ||','|| nvl(Q1,0)||','|| nvl(Q1-Q2,0)||','||nvl(Q3,0)||','|| to_char((Q3/(Q1-Q2))*100,'999.000')||','|| to_char((Q3/Q1)*100,'999.000') FRED
    from (
        select AA, DD, BB, BB1, sum(QQ1) Q1, nvl(sum(QQQ),0) Q2, sum(QQ2) Q3
        from (
            select oh.client_id AA, trunc(oh.shipped_date) DD, 
                DECODE(
                    oh.client_id,
                        'TR','ALL ORDERS',
                        'T','ALL ORDERS',
                        DECODE(
                            substr(oh.customer_id,1,2),
                                'HF',nvl(order_type,' ')||' HOF',
                                nvl(order_type,' ')
                            )
                    ) 
                    BB,
                    decode(
                        oh.client_id,
                            'UT',oh.user_def_type_2,
                            'ALL ORDERS'
                    ) BB1,
                oh.order_id OID, sum(qty_ordered) QQ1, sum(qty_shipped) QQ2
            from order_header oh,order_line ol
            where oh.status='Shipped'
                and nvl(oh.order_type,' ') like '&&5%' and oh.client_id like '&&2' and oh.shipped_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
                and oh.client_id=ol.client_id and oh.order_id=ol.order_id
            group by 
                oh.client_id, trunc(oh.shipped_date), oh.order_id,
                DECODE(oh.client_id,'TR','ALL ORDERS','T','ALL ORDERS',DECODE(substr(oh.customer_id,1,2),'HF',nvl(order_type,' ')||' HOF',nvl(order_type,' '))),
                decode(oh.client_id,'UT',oh.user_def_type_2,'ALL ORDERS')
            )
            ,
            (
            select oh.client_id CID, oh.order_id RID, sum(gs.qty_ordered-gs.qty_tasked) QQQ 
            from generation_shortage gs,order_header oh 
            where gs.client_id='&&2' and gs.task_id=oh.order_id and gs.client_id=oh.client_id
            group by oh.client_id, oh.order_id
            ) 
        where AA=CID (+) and OID=RID (+)
    group by AA,DD,BB,BB1
    )
union
select to_char(DD,'YYMMDD')||',1,' MM, DD||','||BB||',,'||nvl(Q1,0)||','||nvl(Q1-Q2,0)||','||nvl(Q3,0)||','||to_char((Q3/(Q1-Q2))*100,'999.000')||','||to_char((Q3/Q1)*100,'999.000')
from (
    select AA, DD, BB, sum(QQ1) Q1, nvl(sum(QQQ),0) Q2, sum(QQ2) Q3 
    from (
        select oh.client_id AA, trunc(oh.shipped_date) DD, 'Daily Total' BB, oh.order_id OID, sum(qty_ordered) QQ1, sum(qty_shipped) QQ2
        from order_header oh,order_line ol
        where oh.status='Shipped' and nvl(oh.order_type,' ') like '&&5%' and oh.client_id like '&&2'
        and oh.shipped_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
        and oh.client_id=ol.client_id and oh.order_id=ol.order_id
        group by oh.client_id,  trunc(oh.shipped_date),oh.order_id
        )
        ,
        (
        select oh.client_id CID, oh.order_id RID, sum(gs.qty_ordered-gs.qty_tasked) QQQ
        from generation_shortage gs,order_header oh
        where gs.client_id='&&2' and gs.task_id=oh.order_id and gs.client_id=oh.client_id
        group by oh.client_id, oh.order_id
        )
    where AA=CID (+) and OID=RID (+)
    group by AA,DD,BB
    )
ORDER BY MM
)
UNION ALL
select ' ' from dual where '&&2' = 'UT'
union all
select 'Cancelled orders:' from dual where '&&2' = 'UT'
union all
SELECT DD || ',' || BB || ',' || BB1 || ',' || Q1 || ',' || Q2 || ',' || Q3
from (
        select DD, BB, BB1, sum(QQ1) Q1, nvl(sum(QQQ),0) Q2, sum(nvl(QQ2,0)) Q3
        from (
            select oh.client_id AA, trunc(it.dstamp) DD, nvl(order_type,' ')  BB, oh.user_def_type_2 BB1,
            oh.order_id OID, sum(qty_ordered) QQ1, sum(qty_shipped) QQ2
            from order_header oh inner join  order_line ol on oh.order_id = ol.order_id and oh.client_id = ol.client_id
            inner join inventory_transaction it on oh.client_id = it.client_id and oh.order_id = it.reference_id
            where oh.status='Cancelled'
                and nvl(oh.order_type,' ') like '&&5%' and oh.client_id = '&&2' and '&&2' = 'UT'
                and it.code = 'Order Status' and it.notes like '%--> Cancelled'
                and it.dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
                and oh.client_id=ol.client_id and oh.order_id=ol.order_id
            group by 
                oh.client_id, trunc(it.dstamp), oh.order_id, nvl(order_type,' '), oh.user_def_type_2
            )
            ,
            (
            select oh.client_id CID, oh.order_id RID, sum(gs.qty_ordered-gs.qty_tasked) QQQ 
            from generation_shortage gs,order_header oh 
            where gs.client_id='&&2' and gs.task_id=oh.order_id and gs.client_id=oh.client_id
            group by oh.client_id, oh.order_id
            ) 
        where AA=CID (+) and OID=RID (+)
    group by AA,DD,BB,BB1
)
/
