SET FEEDBACK OFF             
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 300
SET TRIMSPOOL ON



--set termout off
--spool stdsm.csv

select 'Client id,Sku id,Qty on Hand,Description,Colour,Sku Size,Days since last Receipt,Days since last Shipment,Days since last Allocation' from dual
union all
select client_id||','||sku_id||','||"Qty"||','||description||','||colour||','||sku_size||','||"DSR"||','||"DSS"||','||"DSA"
from
(
with rec as (
select i.sku_id                 as "Sku"
, max(trunc(i.receipt_dstamp))  as "Date"
from inventory i
where i.client_id = '&&2'
group by i.sku_id
), shipment as (
select i.sku_id                 as "Sku"
, max(trunc(i.dstamp))          as "Date"
from inventory_transaction i
where i.client_id = '&&2'
and i.code = 'Shipment'
group by i.sku_id
), allocation as (
select i.sku_id                 as "Sku"
, max(trunc(i.dstamp))          as "Date"
from inventory_transaction i
where i.client_id = '&&2'
and i.code = 'Allocate'
group by i.sku_id
)
select i.client_id
, i.sku_id
, sum(i.qty_on_hand) as "Qty"
, s.description
, s.colour
, s.sku_size
, trunc(sysdate) - rec."Date"           as "DSR"
, trunc(sysdate) - shipment."Date"      as "DSS"
, trunc(sysdate) - allocation."Date"    as "DSA"
from inventory i
inner join sku s on s.sku_id = i.sku_id and s.client_id = i.client_id
left join rec on rec."Sku" = i.sku_id
left join shipment on shipment."Sku" = i.sku_id
left join allocation on allocation."Sku" = i.sku_id
where i.client_id = '&&2'
and rec."Date" < trunc(sysdate - '&&3') 
and shipment."Date" < trunc(sysdate - '&&3')
and allocation."Date" < trunc(sysdate - '&&3')
group by i.client_id
, i.sku_id
, s.description
, s.colour
, s.sku_size
, rec."Date"
, shipment."Date"
, allocation."Date"
order by rec."Date" desc
)
/

--spool off
--set termout on
