define client="&&1"

SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ,


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 1000
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Carrier Id,Order Id,Tracking Number,Scan Status,Scan Id,Scan Date' from dual
union all
select carrier_id||','||order_id||','||trackingNumber||','||scanStatus||','||scanId||','||scanDate
from
(
    select carrier_id
    , order_id
    , signatory trackingNumber
    , case when scanDate is null then 'Not Scanned' else 'Scanned' end scanStatus
    , scanId
    , scanDate
    from
    (
        select oh.carrier_id
        , oc.order_id
        , oh.signatory
        , (select max(pallet_id) from INVENTORY_TRANSACTION where client_id = '&&client' and code = 'Repack' and reference_id = oh.order_id) scanId
        , (select max(dstamp) from INVENTORY_TRANSACTION where client_id = '&&client' and code = 'Repack' and reference_id = oh.order_id) scanDate           
        from ORDER_CONTAINER oc
        inner join ORDER_HEADER oh on oh.order_id = oc.order_id and oh.client_id = oc.client_id
        left join CARRIERS c on c.client_id = oc.client_id and  oc.pallet_id like coalesce(c.consignment_prefix, 'XXX')||'%'
        where oc.client_id = '&&client'
        and oh.order_type = 'WEB'
    )
    order by carrier_id
    , scanStatus desc
    , order_id
)
/
--spool off