SET FEEDBACK OFF                 
set verify off
set wrap off
set colsep ','

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 132
set trimspool on

--spool utorders.csv

select 'UTNET Orders Shipped from &&2 to &&3 - for client UTTAM' from DUAL;

select 'Order_id,Courier,Shipped Date,Marketplace,Tracking URL' from DUAL;

select
A||','||
A2||','||
B||','||
C||','||
D
from (select
oh.order_id A,
oh.work_group A2,
trunc(oh.shipped_date) B,
user_def_type_1 C,
DOCUMENTATION_TEXT_3 D
from order_header oh
where oh.shipped_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and oh.client_id='UT'
and oh.ship_dock='UTNET'
and oh.status='Shipped')
order by
A
/

--spool off
