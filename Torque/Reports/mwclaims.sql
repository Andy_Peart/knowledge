-- Modified 22/6/2012 BKI Added USER_ID at request of Steve Foster Ticket # 11146

SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON


--spool mwclaims.csv

select 'Contents of Container &&2' from DUAL;

select
it.sku_id||''','||
sku.description||','||
it.update_qty||','||
sku.each_value||','||
it.user_id
from inventory_transaction it,sku
where it.client_id ='MOUNTAIN'
and trunc(it.dstamp) > trunc(sysdate)-&&3
and it.code ='Pick'
and it.to_loc_id='CONTAINER'
and it.container_id='&&2'
and sku.client_id ='MOUNTAIN'
and it.sku_id=sku.sku_id
order by
it.sku_id
/


--spool off

