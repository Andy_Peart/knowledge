SET FEEDBACK OFF                 
set pagesize 68
set linesize 136
set verify off
set wrap off
set newpage 0
set colsep ' '
set und on

clear columns
clear breaks
clear computes


column A heading 'ROUTE' format A6
column AAA heading 'AAA' format A6 NOPRINT
column CC heading 'DOCK' format A4
column CCC heading 'CCC' format A6 NOPRINT
column D heading 'STORE NAME' format A32
column BB heading 'ORDER' format A8
column SS heading 'STATUS' format A11
column SSS heading 'M T STATUS' format A11
column DD heading 'CREATED' format A11
column TT heading 'TYPE' format A11
column GG heading 'CONSIGNMENT' format A12
column HH heading 'CONSIGN' format A14
column MM heading 'OPENS' format A18
column NN heading 'PICK' format A8
column QQQ heading 'UNITS' format 99999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



ttitle  LEFT 'TML Client T - Retail Orders for Picking and Despatching on &&2 ' RIGHT 'Printed ' report_date skip 2


break on HH skip 1 on AAA skip 2 on CCC skip 1

compute sum of QQQ on CCC


select
--'OK TO CONSIGN' HH,
DECODE ('&&2','MONDAY',a.user_def_type_2,
     	      'TUESDAY',a.user_def_type_3,
     	      'WEDNESDAY',a.user_def_type_4,
     	      'THURSDAY',a.user_def_type_5,
     	      'FRIDAY',a.user_def_type_1,'N') A,
DECODE ('&&2','MONDAY',a.user_def_type_2,
     	      'TUESDAY',a.user_def_type_3,
     	      'WEDNESDAY',a.user_def_type_4,
     	      'THURSDAY',a.user_def_type_5,
     	      'FRIDAY',a.user_def_type_1,'N') AAA,
oh.order_id BB,
--oh.ship_dock CC,
--oh.ship_dock CCC,
oh.user_def_type_5 CC,
oh.user_def_type_5 CCC,
a.name D,
PP QQQ,
--oh.consignment GG,
oh.status SS,
oh.move_task_status SSS,
to_char(oh.creation_date, 'DD/MM/YYYY') DD,
oh.order_type TT,
--DECODE (oh.ship_dock,'T239','S F P',
DECODE (oh.user_def_type_5,'T239','S F P',
                       'T212','S F P',
                       'T228','S F P','Normal') NN,
to_char(a.user_def_date_1, 'DD/MM/YYYY HH:MI') MM
from order_header oh,address a,
(select ID,OO,PP from
(select
oh.order_id  ID,
oh.order_type  TT,
sum(ol.qty_ordered) OO,
nvl(sum(ol.qty_tasked),0) PP
from order_header oh,order_line ol
where oh.client_id='TR'
and oh.dispatch_method=1
and oh.ship_dock = 'TRWEBCC'
and oh.order_id=ol.order_id
group by
oh.order_id,
oh.order_type))
where
oh.order_id=ID
and oh.status not in ('Cancelled','Hold','Shipped','Released')
and (oh.consignment is null or oh.consignment='')
and oh.client_id='TR'
and a.client_id='TR'
and oh.order_type = 'WEB'
--and oh.ship_dock=a.address_id
and oh.user_def_type_5=a.address_id
and ((('&&2'='MONDAY') and (a.delivery_open_mon='Y'))
or (('&&2'='TUESDAY') and (a.delivery_open_tue='Y'))
or (('&&2'='WEDNESDAY') and (a.delivery_open_wed='Y'))
or (('&&2'='THURSDAY') and (a.delivery_open_thur='Y'))
or (('&&2'='FRIDAY') and (a.delivery_open_fri='Y')))
order by
A,
CC,
BB
/


set heading off
set pagesize 0

Select 'END of REPORT' from DUAL;
