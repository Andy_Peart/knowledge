SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON

with last_year as (
select to_char(to_date('&&2','DD-MON-YYYY'),'DD-MON') || '-' || (to_CHAR(to_date('&&2','DD-MON-YYYY'), 'YYYY')-1) last_year_start,
to_char(to_date('&&3','DD-MON-YYYY'),'DD-MON') || '-' || (to_CHAR(to_date('&&3','DD-MON-YYYY'), 'YYYY')-1) last_year_end
from dual
),
this_year_data as (
select oh.order_id, oh.customer_id, to_char(oh.shipped_date,'DD-MON-YYYY') date_shipped, sum(ol.qty_shipped) qty
from order_header oh inner join order_line ol on oh.order_id = ol.order_id and oh.client_id = ol.client_id
where oh.client_id = 'TR'
and oh.status = 'Shipped'
and oh.order_type <> 'WEB'
and oh.ship_dock not like '%CC'
and oh.customer_id not like 'W%'
and oh.customer_id not like  'T7%'
and oh.shipped_date between to_date('&&2') and to_date('&&3')
group by oh.order_id, oh.customer_id, to_char(oh.shipped_date,'DD-MON-YYYY')
union all
select oh.order_id, oh.customer_id, to_char(oh.shipped_date,'DD-MON-YYYY') date_shipped, sum(ol.qty_shipped) qty
from order_header_archive oh inner join order_line_archive ol on oh.order_id = ol.order_id and oh.client_id = ol.client_id
where oh.client_id = 'TR'
and oh.status = 'Shipped'
and oh.order_type <> 'WEB'
and oh.ship_dock not like '%CC'
and oh.customer_id not like 'W%'
and oh.customer_id not like  'T7%'
and oh.shipped_date between to_date('&&2') and to_date('&&3')
group by oh.order_id, oh.customer_id, to_char(oh.shipped_date,'DD-MON-YYYY')
),
last_year_data as (
select oh.order_id, oh.customer_id, to_char(oh.shipped_date,'DD-MON-YYYY') date_shipped, sum(ol.qty_shipped) qty
from order_header oh inner join order_line ol on oh.order_id = ol.order_id and oh.client_id = ol.client_id, last_year
where oh.client_id = 'TR'
and oh.status = 'Shipped'
and oh.order_type <> 'WEB'
and oh.ship_dock not like '%CC'
and oh.customer_id not like 'W%'
and oh.customer_id not like  'T7%'
and oh.shipped_date between to_date(last_year.last_year_start) and to_date(last_year.last_year_end)
group by oh.order_id, oh.customer_id, to_char(oh.shipped_date,'DD-MON-YYYY')
union all
select oh.order_id, oh.customer_id, to_char(oh.shipped_date,'DD-MON-YYYY') date_shipped, sum(ol.qty_shipped) qty
from order_header_archive oh inner join order_line_archive ol on oh.order_id = ol.order_id and oh.client_id = ol.client_id, last_year
where oh.client_id = 'TR'
and oh.status = 'Shipped'
and oh.order_type <> 'WEB'
and oh.ship_dock not like '%CC'
and oh.customer_id not like 'W%'
and oh.customer_id not like  'T7%'
and oh.shipped_date between to_date(last_year.last_year_start) and to_date(last_year.last_year_end)
group by oh.order_id, oh.customer_id, to_char(oh.shipped_date,'DD-MON-YYYY')
),
T2 as (
select ty.customer_id store_no, round(avg(ty.qty),2) avg_units_this_year, round(avg(ly.qty),2) avg_units_last_year
from this_year_data ty inner join last_year_data ly on ty.customer_id = ly.customer_id
group by ty.customer_id
order by 1
)
select 'Period: ' || '&&2' || ' to ' || '&&3' from dual
union all
select 'Total Average for this year: ' || round(avg(avg_units_this_year),2) || ' units per order' line
from T2
union all
select 'Total Average for last year: ' || round(avg(avg_units_last_year),2) || ' units per order' line
from T2
union all 
select 'Store no,Avg units per order this year,Avg units per order last year' from dual
union all
select store_no || ',' || avg_units_this_year || ',' || avg_units_last_year line
from T2
/
