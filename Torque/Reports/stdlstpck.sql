SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Date,Consignment,Type,User,Lines,Units,Start Time,End Time,Time Taken,Break in Minutes,KPI,Client' from dual
union all
select "Date" ||','|| "Consignment" ||','|| "Type" ||','|| "User" ||','|| "Lines" ||','|| "Units" ||','|| "startTime" ||','|| "endTime" ||','|| substr("timeSpent", 12, 5) ||','|| '0' ||','||/*round(("Units"/"Minutes")*60)*/'=(F'||to_char(to_number(rownum)+1)||'/(I'||to_char(to_number(rownum)+1)||'*24*60-J'||to_char(to_number(rownum)+1)||'))*60' ||','|| "Client"
from (
select "Client", "Date", "Consignment", "Type", "User", "Lines", "Units", "startTime", "endTime", "timeSpent", case when "Minutes" = 0 then 1 else "Minutes" end as "Minutes"
from
(
select "Client"
, "Date"
, "Consignment"
, case  when ("Consignment" like ('CL%') and "List" is null and "maxLines" = 0.0001) or ("Group ID" like '% SP%' or "Group ID" like '%SINGLE%') then 'Single Pick'
        when ("Consignment" like ('%TR%') and "List" is not null and "minLines" > 0.0001) or ("Group ID" like '%TLY%') or ("Group ID" LIKE '%TROLLEY%') then 'Trolley'
        when "Group ID" like '% BP %'  then 'Bulk Pick'
        when "List" is not null and "maxLines" = 0.0001 then 'Cluster'
        end as "Type"
, "User"        
, "Lines"
, "Units"
, "startTime"
, "endTime"
, "timeSpent"
, "Minutes"
from
(
select it.client_id                 as "Client"
, trunc(it.dstamp)                  as "Date"
, it.consignment                    as "Consignment"
, it.user_id                        as "User"
, min(oh.order_volume)              as "minLines"
, max(oh.order_volume)              as "maxLines"
, max(it.list_id)                   as "List"
, count(it.update_qty)              as "Lines"
, sum(it.update_qty)                as "Units"
, min(to_char(it.dstamp, 'HH24:MI'))as "startTime"
, max(to_char(it.dstamp, 'HH24:MI'))as "endTime"
, max(it.dstamp) - min(it.dstamp)   as "timeSpent"
, ((to_number(substr(to_char(max(it.dstamp),'HH24:MI'),1,2))*60)+to_number(substr(to_char(max(it.dstamp),'HH24:MI'),4,2)))-
  ((to_number(substr(to_char(min(it.dstamp),'HH24:MI'),1,2))*60)+to_number(substr(to_char(min(it.dstamp),'HH24:MI'),4,2))) as "Minutes"
, oh.consignment_grouping_id        as "Group ID"  
from inventory_transaction it
inner join order_header oh on oh.order_id = it.reference_id and oh.client_id = it.client_id
inner join location l on l.site_id = it.site_id and l.location_id = it.from_loc_id
where it.client_id in (
    select regexp_substr('&&2','[^,]+',1,level) from dual
connect by regexp_substr('&&2','[^,]+',1,level) is not null
)
and it.update_qty <> 0
and it.code = 'Pick'
and it.dstamp between to_date('&&3') and to_date('&&4')+1
and l.loc_type in ('Tag-LIFO','Tag-FIFO')
group by trunc(it.dstamp)
, it.consignment
, it.user_id
, it.client_id
, oh.consignment_grouping_id
)
where "Lines" > 9
)
where "Type" is not null
order by "Client"
, "Type"
, "Date"
, "startTime"
)
/
--spool off