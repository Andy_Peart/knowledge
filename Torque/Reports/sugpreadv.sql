SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','
SET WRAP OFF


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON



select 'Pre Advice ID, SKU, Style, Colour, Size, Received, Qty Due, Qty Received' from dual
union all
select pre_advice_id || ',' || sku_id  || ',' || styl  || ',' || colour  || ',' || sku_size  || ',' ||  finished  || ',' ||  qty_due  || ',' ||  qty_received
from (
select ph.pre_advice_id, pl.sku_id, sku.user_def_type_1 styl, sku.colour, sku.sku_size, to_char(ph.finish_dstamp, 'DD-MM-YYYY') finished, pl.QTY_DUE, nvl(pl.QTY_RECEIVED,0) qty_received
from pre_advice_header ph  inner join  pre_advice_line pl on ph.pre_advice_id = pl.pre_advice_id inner join sku on pl.sku_id = sku.sku_id
where pl.client_id = 'SUG'
and ph.client_id = 'SUG'
and sku.client_id = 'SUG'
and ph.pre_advice_id = '&2'
)
/