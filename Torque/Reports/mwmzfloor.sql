SET FEEDBACK OFF                 
set verify off
set wrap off
set colsep ','

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 132
set trimspool on


select 'MOUNTAIN stock in multiple locations on floor &&2' from DUAL;
select 'SKU,Description,Locations(Stock)' from DUAL;

select
SK||','||
LISTAGG(LD, ':') WITHIN GROUP (ORDER BY LD) LL
from (select
SK,
LD||'('||QQ||')' LD
from (select
i.SKU_ID||''','||upper(i.description) SK,
i.location_id LD,
sum(i.qty_on_hand) QQ
from inventory i,(select
i.sku_id A,
count(distinct i.location_id) B
from inventory i,location L,mountain_floor mf
where i.client_id = 'MOUNTAIN'
and i.location_id not like 'Z%'
and i.location_id not in
('MAIL ORDER',
'MARSHALL',
'DESPATCH',
'STG',
'CONTAINER',
'SUSPENSE',
'UKPAC',
'UKPAR')
and i.location_id=L.location_id
and i.site_id=L.site_id
and L.work_zone=mf.workzone
and mf.floorno='&&2'
group by
sku_id),location L,mountain_floor mf
where B > 1
and i.location_id not like 'Z%'
and i.location_id not in
('MAIL ORDER',
'MARSHALL',
'DESPATCH',
'STG',
'CONTAINER',
'SUSPENSE',
'UKPAC',
'UKPAR')
and i.sku_id = A
and i.location_id=L.location_id
and i.site_id=L.site_id
and L.work_zone=mf.workzone
and mf.floorno='&&2'
group by
i.SKU_ID,
i.description,
i.location_id))
GROUP BY SK
order by 1
/



