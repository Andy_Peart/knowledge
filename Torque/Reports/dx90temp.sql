SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 68
SET NEWPAGE 0
SET LINESIZE 240
SET TRIMSPOOL ON

column A heading 'Picker' format A16
column B heading 'Revenue Stream' format A16
column C heading 'Orders' format 999999999
column D heading 'Units' format 999999999

--select 'Order ID,Ordered,Picked' from DUAL;
 
set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DAY, DD MONTH, YYYY') curdate from DUAL;
set TERMOUT ON


break on A dup skip 1

compute sum label 'Picker Totals' of C D on A


TTITLE left 'Picker Totals from &&3:00 to &&4:00 on &&2 for Client DAXBOURNE' skip 2

--SET PAGESIZE 66

select
it.user_id A,
DECODE(oh.order_type,'WH',DECODE(substr(oh.user_def_type_5,1,3),'STO','RETAIL','WHOLESALE'),oh.order_type) B,
count(distinct it.reference_id) C,
sum(it.update_qty) D
from inventory_transaction it,order_header oh
where it.code in ('Pick')
and it.client_id='DX'
and trunc(it.Dstamp)=to_date('&&2', 'DD-MON-YYYY')
and to_char(it.Dstamp,'HH24') between '&&3' and '&&4'-1
and it.elapsed_time>0
and it.reference_id=oh.order_id
and oh.client_id='DX'
group by
it.user_id,
DECODE(oh.order_type,'WH',DECODE(substr(oh.user_def_type_5,1,3),'STO','RETAIL','WHOLESALE'),oh.order_type)
order by
A,
B
/
