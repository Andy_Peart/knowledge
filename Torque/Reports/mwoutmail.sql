set pagesize 0
set linesize 120
set verify off
set feedback off

ttitle 'Mountain Warehouse Despatch Quantity' skip 2

column A heading 'SKU' format a16
column B heading 'Qty' format 99999999
column C heading 'Address' format a32

select 'SKU,Qty,Name,Address1,Address2' from DUAL;

--break on report

--compute sum label 'Total' of B on report

select 
i.sku_id||','||
i.update_qty||','||
oh.name||','||
oh.address1||','||
oh.address2
from inventory_transaction i,order_header oh
where i.client_id = 'MOUNTAIN'
and i.code = 'Shipment'
and i.reference_id not like ('MOR%')
--and i.condition_id='MMAIL'
and i.dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY') 
and i.reference_id=oh.order_id
and oh.client_id='MOUNTAIN'
order by i.sku_id
/


select 'NOTHING TO REPORT' from
(select 
count(*) ct
from inventory_transaction i,order_header oh
where i.client_id = 'MOUNTAIN'
and i.code = 'Shipment'
and i.reference_id not like ('MOR%')
--and i.condition_id='MMAIL'
and i.dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY') 
and i.reference_id=oh.order_id
and oh.client_id='MOUNTAIN')
where ct=0
/
