SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 2000
SET TRIMSPOOL ON


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


select 'Wholesale Picks from Retail  '||to_char(SYSDATE-1, 'DD/MM/YYYY') from DUAL;

select 'Order,Customer,Notes,Sku,Description,Qty' from DUAL;

select
--trunc(it.Dstamp)||','||
it.reference_id||','||
it.customer_id||','||
sku.user_def_note_1||','||
it.sku_id||','||
sku.description||','||
it.update_qty
from inventory_transaction it,order_header oh,sku,location loc
where it.client_id='SH'
and it.code like 'Pick'
and it.reference_id=oh.order_id
and oh.client_id='SH'
and oh.order_type like 'WSALE%'
and it.from_loc_id=loc.location_id
and loc.zone_1='SHHG'
and trunc(it.Dstamp)=trunc(sysdate)
and it.sku_id=sku.sku_id
and sku.client_id='SH'
order by
trunc(it.Dstamp),
it.reference_id,
sku.user_def_note_1
/
