SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 200
SET TRIMSPOOL ON
 

column A heading 'SKU' format A18
column B heading 'Location' format A12
column C heading 'Qty' format 9999999


--break on A

--spool gvstock.csv

select 'SKU,Zone,Qty,' from DUAL;

select
'ALL STOCK' A,
DECODE(ll.zone_1,'INTERIM',DECODE(substr(ii.location_id,1,3),'AQL','AQL',ii.location_id),ll.zone_1)  B,
sum(ii.qty_on_hand) C,
' '
from inventory ii, location ll
where ii.client_id='GV'
and ii.location_id=ll.location_id
and ll.site_id='GIVE'
group by
DECODE(ll.zone_1,'INTERIM',DECODE(substr(ii.location_id,1,3),'AQL','AQL',ii.location_id),ll.zone_1) 
order by
DECODE(ll.zone_1,'INTERIM',DECODE(substr(ii.location_id,1,3),'AQL','AQL',ii.location_id),ll.zone_1) 
/



select
ii.sku_id A,
DECODE(ll.zone_1,'INTERIM',DECODE(substr(ii.location_id,1,3),'AQL','AQL',ii.location_id),ll.zone_1)  B,
sum(ii.qty_on_hand) C,
' '
from inventory ii, location ll
where ii.client_id='GV'
and ii.location_id=ll.location_id
and ll.site_id='GIVE'
group by
ii.sku_id,
DECODE(ll.zone_1,'INTERIM',DECODE(substr(ii.location_id,1,3),'AQL','AQL',ii.location_id),ll.zone_1)
order by
ii.sku_id,
DECODE(ll.zone_1,'INTERIM',DECODE(substr(ii.location_id,1,3),'AQL','AQL',ii.location_id),ll.zone_1)
/


--spool off
