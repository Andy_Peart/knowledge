SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','
SET WRAP OFF


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
set linesize 110

--ttitle 'Mountain Warehouse Overpicks for Period &&2 to &&3' skip 2

column A heading 'Order ID' format a10
column B heading 'Creation Date' format A12
column C heading 'Order Qty' format 99999999
column D heading 'Tasked Qty' format 99999999
column E heading 'Customer' format a40
column F heading 'Country' format a16

select 'Order ID,Creation Date,Order Qty,Tasked Qty,Customer,Country,,' from DUAL;


select
OOO A,
trunc(HHH) B,
ORD C,
TTT D,
NNN E,
CCC F,
' '
from (select
oh.order_id OOO,
---oh.country CCC,
nvl(country.tandata_id,oh.country) CCC,
oh.creation_date HHH,
oh.customer_id||' - '||oh.address1 NNN,
sum(ol.qty_ordered) ORD,
sum(nvl(ol.qty_tasked,0)) TTT,
sum(ol.qty_ordered)-sum(nvl(ol.qty_tasked,0)) QQQ
from order_header oh,order_line ol,country
where oh.client_id='TR'
and oh.order_type = 'WEB'
and oh.status<>'Cancelled'
and oh.consignment is null
and oh.country<>'&&2'
and ol.client_id='TR'
and oh.order_id=ol.order_id
and oh.country=country.iso3_id (+)
group by
oh.order_id,
nvl(country.tandata_id,oh.country),
oh.creation_date,
oh.customer_id||' - '||oh.address1)
where QQQ>0
order by
OOO
/
