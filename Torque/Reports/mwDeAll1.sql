/******************************************************************************/
/*                                                                            */
/*                            Elite                                           */
/*                                                                            */
/*     FILE NAME  :   mwdeall1.sql                                            */
/*                                                                            */
/*   DATE     BY   PROJ     ID         DESCRIPTION                            */
/*   ======== ==== ======   ========   =============                          */
/*   23/01/06 BK   Mountain            De-Allocations by Date                 */
/******************************************************************************/

/* Input Parameters are as follows */
-- 1)  From Date
-- 2)  To Date

set feedback off
set pagesize 68
set linesize 240
set verify off

clear columns
clear breaks
clear computes

column A heading 'Date' format A16
column B heading 'Lines' format 999999
column C heading 'Qty' format 999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'De-Allocations by Date from '&&2' to '&&3' - for client ID MOUNTAIN as at ' report_date skip 2

break on report
compute sum label 'Total' of B C on report


select 
to_char(Dstamp,'DD/MM/YY') A,
count(*) B,
sum(update_qty) C 
from inventory_transaction
where client_id='MOUNTAIN'
and Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and code='Deallocate'
group by 
to_char(Dstamp,'DD/MM/YY')
/

