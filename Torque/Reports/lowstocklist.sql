set pagesize 68
set linesize 120
set newpage 0
set verify off
set heading on
set feedback off

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle LEFT 'Stock Locations with quantity less than &&5 (Location Range &&2 to &&3) for Client ID &&4 as at ' report_date RIGHT 'Page:'FORMAT 999 SQL.PNO skip 2

column A heading 'Zone' format a12
column B heading 'Location' format a12
column C heading 'SKU' format a20
column D heading 'QTY' format 999999

--break on s.sku_id

select A,
B,
C,
D
from
 (select loc.zone_1 A,
 inv.location_id B,
 inv.sku_id C,
 sum(inv.QTY_ON_HAND) D,
 sum(inv.qty_allocated) E
 from inventory inv,location loc
 where inv.client_id = '&&4'
 and inv.location_id between '&&2' and '&&3'
 and inv.location_id = loc.location_id
 group by loc.zone_1, inv.location_id, inv.sku_id)
where D < '&&5'
--and E=0
order by A,B,C
/

ttitle  LEFT ' '
set heading off
break on row skip 0
set newpage 1


select 
'Location Count =',
count (*) D
from
 (select loc.zone_1 A,
 inv.location_id B,
 inv.sku_id C,
 sum(inv.QTY_ON_HAND) D,
 sum(inv.qty_allocated) E
 from inventory inv,location loc
 where inv.client_id = '&&4'
 and inv.location_id between '&&2' and '&&3'
 and inv.location_id = loc.location_id
 group by loc.zone_1, inv.location_id, inv.sku_id)
where D < '&&5'
/


select
'     SKU Count =',
count (distinct C) D
from
 (select loc.zone_1 A,
 inv.location_id B,
 inv.sku_id C,
 sum(inv.QTY_ON_HAND) D,
 sum(inv.qty_allocated) E
 from inventory inv,location loc
 where inv.client_id = '&&4'
 and inv.location_id between '&&2' and '&&3'
 and inv.location_id = loc.location_id
 group by loc.zone_1, inv.location_id, inv.sku_id)
where D < '&&5'
/


select 
'    Unit Count =',
nvl(sum(D),0) D
from
 (select loc.zone_1 A,
 inv.location_id B,
 inv.sku_id C,
 sum(inv.QTY_ON_HAND) D,
 sum(inv.qty_allocated) E
 from inventory inv,location loc
 where inv.client_id = '&&4'
 and inv.location_id between '&&2' and '&&3'
 and inv.location_id = loc.location_id
 group by loc.zone_1, inv.location_id, inv.sku_id)
where D < '&&5'
/




