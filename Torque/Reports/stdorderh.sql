SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON


select 'Order ID','Order Type','Ship Dock', 'Creation Date', 'Creation Time','Customer ID','Qty Ordered' FROM DUAL
union all
SELECT
	TO_CHAR(oh.order_id)
, TO_CHAR(oh.ORDER_TYPE)
, TO_CHAR(oh.SHIP_DOCK)
, TO_CHAR(oh.CREATION_DATE, 'DD-MON-YYYY') AS "Creation Date"
, TO_CHAR(oh.CREATION_DATE, 'HH24:MI')     AS "Creation Time"
, TO_CHAR(oh.CUSTOMER_ID)
, TO_CHAR(SUM(ol.qty_ordered)) AS "Qty Ordered"
FROM
	order_header oh
INNER JOIN order_line ol
ON
	oh.order_id     =ol.order_id
	AND oh.client_id=ol.client_id
WHERE
	oh.client_id = '&2'
	AND oh.CREATION_DATE BETWEEN to_date('&3', 'DD-MON-YYYY') AND to_date('&4', 'DD-MON-YYYY')+1
GROUP BY
	oh.ORDER_TYPE
, oh.order_id
, oh.SHIP_DOCK
, oh.CUSTOMER_ID
, oh.CREATION_DATE
/