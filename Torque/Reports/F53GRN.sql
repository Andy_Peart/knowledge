SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON

--SET FEEDBACK ON

update inventory_transaction
set uploaded='R'
where client_id = 'F53'
and (code like 'Receipt%' or (code='Adjustment' and reason_id='RECRV'))
--and uploaded = 'N'
/

COLUMN M NOPRINT

--SET FEEDBACK OFF            

--spool F53GRN.csv


--to_char(sysdate, 'YYYYMMDD HH:MI'),

select
distinct
RRR||'1' M,
'FLY53,FLY53,'||
RRR||','||
to_char(DDD, 'YYYYMMDD HH:MI')||','||
PPP||',,HGRN'
from (select
nvl(reference_id,'XXXX') RRR,
Dstamp DDD,
supplier_id PPP,
sku_id SSS,
update_qty QQQ
from inventory_transaction
where client_id = 'F53'
and (code like 'Receipt%' or (code='Adjustment' and reason_id='RECRV'))
and uploaded = 'R')
union
select
distinct
RRR||'2' M,
SSS||','||
LLL||','||
QQQ||','||
PPP||',,,LGRN'
from (select
nvl(reference_id,'XXXX') RRR,
line_id LLL,
substr('0000000000000000'||supplier_id,nvl(length(supplier_id),0)+1,16) PPP,
sku_id SSS,
update_qty QQQ
from inventory_transaction
where client_id = 'F53'
and (code like 'Receipt%' or (code='Adjustment' and reason_id='RECRV'))
and uploaded = 'R')
order by M
/


--spool off

--SET FEEDBACK ON

update inventory_transaction
set uploaded='Y'
where client_id = 'F53'
and (code like 'Receipt%' or (code='Adjustment' and reason_id='RECRV'))
and uploaded = 'R'
/


commit;
--rollback;

