SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON

with t1 as 
(
    select sku_id, location_id || ' (' || qty || ')' as locqty
    from (
        select sku_id, location_id, sum(qty_on_hand) qty
        from inventory
        where client_id = 'MOUNTAIN'
		and location_id not in ('Z012','CONTAINER','MAIL ORDER','DESPATCH')
		and REGEXP_LIKE(sku_id, '^[[:digit:]]+$')
        group by sku_id, location_id
    )
)
select 'SKU,Locations (Qtys)' from dual
union all
select sku_id || ',' || locqty
from (
    select sku_id,LISTAGG(locqty, ',') WITHIN GROUP (ORDER BY locqty) AS locqty
    from t1
    group by sku_id
)
/