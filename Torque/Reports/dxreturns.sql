SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9999
SET TRIMSPOOL ON

--spool dxreturns.csv

select 'Date,Reference,Units' from DUAL;

select
trunc(i.Dstamp)||','||
i.reference_id||','||
sum(i.update_qty)
from inventory_transaction i
where i.client_id='DX'
and i.code = 'Receipt'
and i.to_loc_id in ('DXBRET','DXBREJ')
and i.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by
trunc(i.Dstamp),
i.reference_id
order by
trunc(i.Dstamp),
i.reference_id
/

--spool off
