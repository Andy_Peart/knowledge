SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 600
SET TRIMSPOOL ON

column A heading 'User ID' format a24
column B heading 'Name' format a30
column C1 heading 'C Picks' format 999999
column C2 heading 'T Picks' format 999999
column D1 heading 'Receipts' format 999999
column D2 heading 'Receipts' format 999999
column D3 heading 'Receipts' format 999999
column E heading 'Relocates' format 999999
column E2 heading 'Relocates' format 999999
column F heading 'Returns' format 999999
column G heading 'IDTs' format 999999
column H format A6
column M noprint

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


break on MM dup skip 2 on J11 dup

compute sum label 'Totals' of FF C1 J1 on J11

--spool mwjobivity.csv

--select trunc(sysdate)-1 from DUAL;

select 'Date,Name,Start,Stop,Job ID,Notes,Minutes,Txns,Units' from DUAL;



select MM,
J11,
MM1,
MM2,
JJ,
substr(NN1, 1, 16),
FF, 
C1,
J1
from 
(    select MM,
J11,
MM1,
MM2, 
((to_number(substr(MM2, 1, 2)) * 60) +
to_number(substr(MM2, 4, 2))) - ((to_number(substr(MM1, 1, 2)) * 60) +
to_number(substr(MM1, 4, 2))) FF,
JJ,
NN1,
sum(CC) C1,
sum(JA1) J1
from 
(/*Determine the start and job stop times*/
select trunc(MM1) MM,
UU1 J11,
to_char(MM1, 'HH24:MI:SS') MM1,
to_char(MM2, 'HH24:MI:ss') MM2,
JJ,
NN1,
UN CC,
nvl(UQ, 0) JA1
from 
(
select distinct it.job_id JJ,
it.Dstamp MM1,
substr(it.notes, 1, 40) NN1,
it.user_id UU1,
au.name BBB,
rank() over(partition by it.user_id order by it.Dstamp) as rank1
from inventory_transaction it, application_user au
/*where trunc(it.Dstamp)=trunc(sysdate-1)*/
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY') + 1
and it.client_id = 'MOUNTAIN'
and code like 'Job Start' 
and it.user_id = au.user_id
),
(
select distinct it.job_id JJ2,
it.Dstamp MM2,
substr(it.notes, 1, 40) NN2,
it.user_id UU2,
DECODE(it.update_qty, 1, 0, 0, 0, 1) UN,
DECODE(it.update_qty,
1,
0,
it.update_qty) UQ,
rank() over(partition by it.user_id order by it.Dstamp) as rank2
from inventory_transaction it
/*where trunc(it.Dstamp)=trunc(sysdate-1)*/
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY') + 1
and it.client_id = 'MOUNTAIN'
and code like 'Job End' 
)
where rank1 = rank2
and UU1 = UU2
and JJ = JJ2
and MM2 > MM1
group by UU1, MM1, MM2, JJ, NN1, UN, nvl(UQ, 0)                        
union
select /*Find all relevant ITLs that occur between*/
trunc(MM1) MM,
it.user_id J11,
to_char(MM1, 'HH24:MI:SS') MM1,
to_char(MM2, 'HH24:MI:SS') MM2,
JJ,
NN1, 
count(*) CC,
sum(update_qty) JA1
from inventory_transaction it,
(select distinct jobs.code JobCode,
it.job_id JJ,
it.Dstamp MM1,
substr(it.notes, 1, 40) NN1,
it.user_id UU1,
au.name BBB,
rank() over(partition by it.user_id order by it.Dstamp) as rank1
from inventory_transaction it, application_user au,
(
select distinct job_id, ITLCodes.code
from jobs
inner join
(
select 'ADJUSTMENT' code from dual union all
select 'ALLOCATE' code from dual union all
select 'COND UPDATE' code from dual union all
select 'CONFIG UPDATE' code from dual union all
select 'CONSIGN STATUS' code from dual union all
select 'DEALLOCATE' code from dual union all
select 'GRID PICK END' code from dual union all
select 'GRID PICK START' code from dual union all
select 'INV LOCK' code from dual union all
select 'INV UNLOCK' code from dual union all
select 'JOB END' code from dual union all
select 'JOB START' code from dual union all
select 'KIT BUILD' code from dual union all
select 'MARSHAL' code from dual union all
select 'ORDER STATUS' code from dual union all
select 'OWNER UPDATE' code from dual union all
select 'PICK' code from dual union all
select 'PREADV LINE' code from dual union all
select 'PREADV STATUS' code from dual union all
select 'PUTAWAY' code from dual union all
select 'RECEIPT' code from dual union all
select 'RECEIPT REVERSE' code from dual union all
select 'RELOCATE' code from dual union all
select 'REPACK' code from dual union all
select 'RETURN' code from dual union all
select 'SHIPMENT' code from dual union all
select 'STOCK CHECK' code from dual union all
select 'UPI STATUS' code from dual union all
select 'UNPICK' code from dual union all
select 'UNSHIPMENT' code from dual union all
select 'USER DEF UPDATE' code from dual union all
select 'VEHICLE LOAD' code from dual
) ITLCodes on jobs.description like '%('||ITLCodes.code||')%' 
where jobs.client_id = 'MOUNTAIN'
) jobs
/*where trunc(it.Dstamp)=trunc(sysdate-1)*/
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY') + 1
and it.client_id = 'MOUNTAIN'
and it.code like 'Job Start'
and it.user_id = au.user_id 
and it.job_id = jobs.job_id
),
(select distinct it.job_id JJ2,
it.Dstamp MM2,
substr(it.notes, 1, 40) NN2,
it.user_id UU2,
rank() over(partition by it.user_id order by it.Dstamp) as rank2
from inventory_transaction it
/*where trunc(it.Dstamp)=trunc(sysdate-1)*/
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY') + 1
and it.client_id = 'MOUNTAIN'
and code like 'Job End' )
where it.Dstamp between MM1 and MM2 + 0.00007
and it.client_id = 'MOUNTAIN'
and it.update_qty <> 0
and it.from_loc_id <> 'CONTAINER'
and upper(it.code) = upper(JobCode)
and it.user_id = UU1
and rank1 = rank2
and UU1 = UU2
and JJ = JJ2
and MM2 > MM1   
group by it.user_id, MM1, MM2, JJ, NN1) 
group by MM, J11, MM1, MM2, JJ, NN1)
order by MM,J11
/

--spool off
