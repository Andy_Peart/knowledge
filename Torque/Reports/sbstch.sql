SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep '|'


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9999
SET TRIMSPOOL ON



update inventory_transaction
set uploaded='S'
where client_id='SB'
and code like 'Order Stat%'
and uploaded = 'N'
and reference_id in (select
order_id from order_header
where client_id = 'SB'
and priority='1')
/

commit;


update order_header
set uploaded='S'
where client_id='SB'
and priority='1'
and uploaded = 'N'
/

commit;


select
BB||'|'||
FF||'|'||
to_char(GG,'DD-MM-YYYY')
from (select
order_id BB,
--Status FF
'RECEIVED' FF,
Creation_date GG
from order_header
where client_id = 'SB'
and uploaded='S'
and priority='1'
and order_id not in(select
distinct
reference_id
from inventory_transaction
where client_id='SB'
and code like 'Order Stat%'
and uploaded='S'))
union
select
BB||'|'||
DECODE(oh.status,'Shipped','COMPLETE','Cancelled','CANCELLED','RECEIVED')||'|'||
to_char(DD,'DD-MM-YYYY')
from order_header oh ,(select
reference_id BB,
max(Dstamp) DD
from inventory_transaction
where client_id='SB'
and code like 'Order Stat%'
and uploaded='S'
group by
reference_id)
where oh.client_id='SB'
and BB=oh.order_id
and oh.priority='1'
order by 1
/


update inventory_transaction
set uploaded='Y'
where client_id='SB'
and code like 'Order Stat%'
and uploaded = 'S'
/

commit;

update order_header
set uploaded='Y'
where client_id='SB'
and uploaded = 'S'
/

commit;
