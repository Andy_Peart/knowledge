SET FEEDBACK OFF             
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

column A heading 'Date' format a20
column B heading 'Supplier' format a12
column C heading 'Ref' format a12
column D heading 'SKU' format a18
column E heading 'Style and Colour' format a50
column F heading 'QTY' format 9999999
column G heading 'Location' format a10
column H heading 'Condition Code' format a10
column J heading 'Condition' format a30
column M format a4 noprint

select 'Date,Supplier_id,Reference_id,Sku_id,Style / Colour,Qty,Location,Condition Code,Condition'
from dual;

break on A

compute sum label 'Day Total' of F on A



Select
to_char(it.Dstamp, 'YYMM') M,
to_char(it.Dstamp, 'DD-MON-YYYY') A,
it.supplier_id B,
it.reference_id C,
i.sku_id D,
sku.description||' - '||sku.colour E,
i.qty_on_hand F,
i.location_id G,
i.condition_id H,
DECODE(i.condition_id,'GSEAL','Awaiting gold seal'
	             ,'SIZES','Awaiting measurement sheets'
		     ,'GSMMT','Awaiting gold seal and measurement sheets'
		     ,'GLEBE','Sent to Hawick for Alterations'
		     ,'AQL','AQL in progress'
		     ,'GOOD','Awaiting putaway'
		     ,'RTM','Stock to return to supplier' 
		     ,'LND','Size set to London',' ') j
from inventory_transaction it, inventory i,sku
where i.client_id = 'PRI'
and i.location_id in ('QC*', 'QCREJECT', 'QC0', 'QC1', 'QC2', 'QCHOLD', 'WP1')
and i.Receipt_id=it.reference_id
and i.Tag_id=it.Tag_id
and it.code='Receipt'
and i.sku_id=sku.sku_id
order by
M,A,B,C,D
/
