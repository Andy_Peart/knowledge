SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep '|'


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 200
SET TRIMSPOOL ON
 
--spool stockrecon.csv

select
A||'|'||
B||'|'||
DECODE(CC,'',C,CC)||'|'||
D||'|'||
E||'|'||
F||'|'||
G||'|'||
H||'|'||
J||'|0|0|'||
K
from (select
sku.user_def_type_2  A,
--DECODE(sku.user_def_type_2,'TML',i.receipt_id||sku.description||'-'||sku.sku_size,i.sku_id) B,
DECODE(sku.user_def_type_2,'TML',i.user_def_type_4||sku.user_def_type_8||substr(sku.colour,5,20)||'-'||sku.sku_size,i.sku_id) B,
NVL(I.USER_DEF_TYPE_8,'') CC,
substr(i.sku_id,1,5) C,
to_char(i.receipt_dstamp,'DD/MM/YYYY') D,
i.tag_id E,
i.location_id F,
'PICK' G,
i.qty_on_hand H,
i.qty_allocated J,
i.qty_on_hand-i.qty_allocated K
from inventory i,sku
where i.client_id='PR'
and i.zone_1<>'P'
and sku.client_id='PR'
AND I.SKU_ID=SKU.SKU_ID)
order by 1
/

--spool off

