SET FEEDBACK OFF                 
set pagesize 68
set linesize 160
set verify off

clear columns
clear breaks
clear computes

column A heading 'Ordered' format 99999
column B heading ' Tasked' format 99999
column C heading ' Picked' format 99999
column D heading 'Shipped' format 99999
column E heading 'Consigned' format a12
column F heading 'Status' format a16


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

ttitle  LEFT 'Order Well Summary for Orders for client ID &&2 as at ' report_date skip 2

break on F skip 2

select
oh.status F,
DECODE(oh.consignment,null,'NO','YES') E,
sum(nvl(ol.qty_ordered,0)) A,
sum (nvl(ol.qty_tasked,0)) B,
sum(nvl(ol.qty_picked,0)) C,
sum(nvl(ol.qty_shipped,0)) D
from order_line ol, order_header oh
where oh.client_id = '&&2'
and oh.status in ('Allocated','In Progress')
and oh.order_id = ol.order_id
group by
oh.status,
DECODE(oh.consignment,null,'NO','YES')
order by
oh.status,
DECODE(oh.consignment,null,'NO','YES')
/
