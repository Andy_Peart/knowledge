SET FEEDBACK OFF                 
set verify off
set wrap off
set colsep ','

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 900
set trimspool on


--spool mwrelocates.csv

select 'Date,From Location,To Location,From Zone,To Zone,Qty' from DUAL
union all
select 
A||','||
B||','||
D||','||
C||','||
E||','||
Q
from (select
trunc(it.Dstamp) A,
it.from_loc_id B,
ff.zone_1 C,
it.to_loc_id D,
tt.zone_1 E,
sum(it.update_qty) Q
from inventory_transaction it,location ff,location tt
where it.client_id='MOUNTAIN'
and it.code='Relocate'
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.to_loc_id=tt.location_id
and tt.site_id=it.site_id
and it.from_loc_id=ff.location_id
and ff.site_id=it.site_id
group by
trunc(it.Dstamp),
it.from_loc_id,
ff.zone_1,
it.to_loc_id,
tt.zone_1 
order by
A,C)
/

--spool off

