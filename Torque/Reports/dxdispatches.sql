SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9999
SET TRIMSPOOL ON


--spool dxdispatches.csv

select 'Daxbourne Total Dispatches from &&2 to &&3 ' from DUAL;


--select 'Date,Order Type,Rev.Stream,Product,Description,Shipped Qty' from DUAL;
select 'Date,Product,Description,Shipped Qty' from DUAL;

select
trunc(i.Dstamp)||','''||
i.sku_id||','||
sku.description||','||
sum(i.update_qty)
from inventory_transaction i,sku
where i.client_id='DX'
and i.code = 'Shipment'
and i.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and sku.client_id = 'DX'
and i.sku_id=sku.sku_id
group by
trunc(i.Dstamp),
i.sku_id,
sku.description
union
select
trunc(i.Dstamp)||','''||
i.sku_id||','||
sku.description||','||
sum(i.update_qty)
from inventory_transaction_archive i,sku
where i.client_id='DX'
and i.code = 'Shipment'
and i.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and sku.client_id = 'DX'
and i.sku_id=sku.sku_id
group by
trunc(i.Dstamp),
i.sku_id,
sku.description
order by
1
/

--spool off

