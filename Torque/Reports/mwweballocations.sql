SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 999
SET TRIMSPOOL ON

column A heading 'Date' format A12
column A1 heading 'Order ID' format A24
column A2 heading 'SKU ID' format A20
column A3 heading 'Description' format A40
column F heading 'Instructions' format A24
column B1 heading 'Qty1' format 999999
column B2 heading 'Qty2' format 999999
column B3 heading 'Qty3' format 999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



break on report
compute sum label 'Total' of B on report

--spool mw004.csv


select 'Consignment,Order ID,SKU,Qty,Location,Qty in Location,Allocated Qty,Reserve Qty' from DUAL;

select
mt.consignment,
mt.task_id,
mt.sku_id,
mt.qty_to_move,
mt.from_loc_id,
QQQ,
AAA,
to_number(sku.user_def_num_4)
from move_task mt,(select
sku_id SSS,
location_id LLL,
sum(qty_on_hand) QQQ,
sum(qty_allocated) AAA
from inventory 
where client_id='MOUNTAIN'
and zone_1='RETAIL'
group by
sku_id,
location_id),sku
where mt.client_id='MOUNTAIN'
and mt.consignment='&&2'
and mt.from_loc_id=LLL
and sku.client_id='MOUNTAIN'
and mt.sku_id=sku.sku_id
order by
mt.task_id,
mt.sku_id
/

--spool off
