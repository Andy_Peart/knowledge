SET FEEDBACK OFF
SET VERIFY OFF
SET TAB OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES


TTITLE 'Pick Sheets'

column A heading 'List Id' format a19
column B heading 'SKU' format a16
column C heading 'Description' format a42
column D heading 'Pick Qty' format 99999
column E heading 'Location' format A12

/* Set up page and line */
SET LINESIZE 112
SET NEWPAGE 0
SET WRAP OFF
SET PAGESIZE 99
SET HEAD ON

break on B skip page

--compute sum label 'Total' of D D2 on report

select
list_id A,
sku_id B,
description C,
sum(qty_to_move) D,
from_loc_id E
from move_task
where consignment like '&&2'
group by
list_id,
sku_id,
description,
from_loc_id
order by
list_id,
sku_id,
description,
from_loc_id
/
