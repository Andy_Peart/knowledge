SET HEADING OFF;

SET FEEDBACK OFF;

SET LINESIZE 300;

select 
client_id,
upper('&2'),
order_id,
'&4'
from
order_header
where 
client_id = UPPER('&1')
AND consignment = upper('&3');