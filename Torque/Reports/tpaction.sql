SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON

--spool tpaction.csv

select 'Date,Receipts,Returns,Retail Picks,Web Picks' from Dual;



select
DD,
nvl(Q1,0),
nvl(Q1R,0),
nvl(Q2,0),
nvl(Q3,0)
from (select
distinct
trunc(Dstamp) DD
from inventory_transaction
where client_id='TP'
and Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1),
(select
trunc(Dstamp) D1,
sum(update_qty) Q1
from inventory_transaction
where client_id='TP'
and Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and code='Receipt'
and tag_id not like 'RET%'
group by
trunc(Dstamp)),
(select
trunc(Dstamp) D1R,
sum(update_qty) Q1R
from inventory_transaction
where client_id='TP'
and Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and code='Receipt'
and tag_id like 'RET%'
group by
trunc(Dstamp)),
(select
trunc(it.Dstamp) D2,
sum(it.update_qty) Q2
from inventory_transaction it,order_header oh
where it.client_id='TP'
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.code='Pick'
and oh.client_id='TP'
and it.reference_id=oh.order_id
and oh.order_type<>'WEB'
group by
trunc(Dstamp)),
(select
trunc(it.Dstamp) D3,
sum(it.update_qty) Q3
from inventory_transaction it,order_header oh
where it.client_id='TP'
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.code='Pick'
and oh.client_id='TP'
and it.reference_id=oh.order_id
and oh.order_type='WEB'
group by
trunc(Dstamp))
where DD=D1 (+)
and DD=D1R (+)
and DD=D2 (+)
and DD=D3 (+)
order by DD
/

--spool off
