SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

Select 'Store,Store Name,Consignment,Units,Allocation Group' from DUAL;


column A format A12
column AA noprint
column B format A30
column C format A12
column D format 999999
column E format A12

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



--ttitle  LEFT 'TML Retail - Open Orders Report ' RIGHT 'Printed ' report_date skip 2


break on AA skip 1 on report

compute sum label 'Total' of D on report



select
oh.customer_id  AA,
oh.customer_id  A,
a.name  B,
oh.consignment C,
sum(ol.qty_ordered-nvl(ol.qty_picked,0)) D,
sku.allocation_group  B
from order_header oh,order_line ol,sku,address a
where oh.client_id='TR'
and oh.status<>'Shipped'
and oh.order_id=ol.order_id
and ol.sku_id=sku.sku_id
and oh.customer_id=a.address_id
and a.client_id='TR'
group by
oh.customer_id,
a.name,
oh.consignment,
sku.allocation_group
order by
oh.customer_id,
oh.consignment,
sku.allocation_group
/
