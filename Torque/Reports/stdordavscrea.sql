SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','
SET WRAP OFF



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000


SELECT 'Order id, Order volume, Order type, Order date, Order time, Creation date, Creation time, Date difference'  A1 FROM DUAL
UNION ALL
SELECT "ORDER ID" || ',' || "NUMBER OF UNITS" || ',' || "STREAM" || ',' || "ORDER DATE" || ',' || "ORDER TIME" || ',' || "ORDER TIME2" || ',' || "CREATION TIME2" || ',' || "diff_date"
FROM 
(
select 
order_id AS "ORDER ID"
, order_volume as "NUMBER OF UNITS"
, order_type as "STREAM"
, to_char(order_date, 'DD-MON-YYYY') as "ORDER DATE"
, to_char(order_date, 'HH24:MI:SS') as "ORDER TIME"
, to_char(CREATION_date, 'DD-MON-YYYY') as "ORDER TIME2"
, to_char(CREATION_date, 'HH24:MI:SS') as "CREATION TIME2"
, TO_TIMESTAMP (CREATION_date) - TO_TIMESTAMP(order_date) as "diff_date"
 from order_header
where client_id = '&&2'
AND ORDER_DATE between to_date('&&3', 'DD/MM/YY') and to_date('&&4', 'DD/MM/YY')+1
)
/
