set pagesize 68
set linesize 120
set verify off

select user_id,
to_char(ul.dstamp, 'DD/MM/YY') DateOn,
to_char(ul.dstamp, 'HH24:MI:SS') timeon,
ul.code,
ul.work_type,
ul.rdt,
A
from user_log ul, (select sum(UPDATE_QTY) A
from inventory_transaction
where user_id = 'MOUNTAIN'
and to_char(dstamp, 'DD/MM/YY') = '&&2'
and code = 'Pick')
where ul.user_id = 'MOUNTAIN'
and ul.rdt = 'R'
and to_char(ul.dstamp, 'DD/MM/YY') = '&&2'
order by ul.dstamp
/
