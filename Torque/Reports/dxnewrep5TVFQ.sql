SET FEEDBACK OFF
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','
SET UND OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 100
SET TRIMSPOOL ON



column A format A16
column B format A12
column C format A20
column D format 99999
column E format A20
column F format A20
column G format A36
column H format A12
column J format A40
column K format 99999
column L format 99999
column N format 99999


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



--break on A dup skip page on C dup skip 1

--compute sum label 'Order Total'  of K L N on C




select 'Orders_Received - '||to_char(SYSDATE, 'DD/MM/YY  HH24:MI:SS') from DUAL;

select 'Order Type,Order,Creation Date,Lines,Instructions' from DUAL;

Select
oh.order_type||' QVC,'||
--DECODE(oh.order_type,'HOME',oh.order_type||' '||substr(oh.user_def_type_1,3,2),oh.order_type)||','||
oh.order_id||','||
to_char(oh.creation_date,'DD/MM/YY')||','||
nvl(oh.num_lines,0)||','||
oh.instructions
from order_header oh
where oh.client_id='DX'
and oh.order_type='TVFINAL'
and oh.customer_id='19762'
and oh.creation_date>sysdate-1
order by
oh.order_id
/

