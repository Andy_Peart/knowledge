set pagesize 2000
set linesize 2000
set verify off

clear columns
clear breaks
clear computes

ttitle "Mountain Warehouse - Order Total Check Sheet by Aisle - Order '&&2'" skip 3

column A heading 'Isle' format a8
column B heading 'Total Qty Picked' format 999999

select loc.zone_1 A,
sum (itl.UPDATE_QTY) B
from order_header oh, inventory_transaction itl, location loc
where oh.client_id = 'MOUNTAIN'
and oh.order_id = ('&&2')
and oh.order_id = itl.reference_id
and itl.code = 'Pick'
and itl.from_loc_id = loc.location_id
group by loc.zone_1
order by 1
/
