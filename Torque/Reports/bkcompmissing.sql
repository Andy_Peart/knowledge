SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','
SET UND OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 280


Select 'SKU ID,Season,Style,Quality,Size,Stock Type,Qty' 
from DUAL;


column A format A18
column B format A10
column C format A10
column D format A10
column E format A16
column F format A42
column G format A12
column S format 9999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



break on report

compute sum label 'Totals'  of S  on report

select * from (select
v.sku_id A,
SKU.USER_DEF_TYPE_2 B,
SKU.USER_DEF_TYPE_1 C,
SKU.USER_DEF_TYPE_4 D,
sku.sku_size G,
v.stock_type E,
sum(v.qty) S
from v_ism_pl_pl v,sku
where v.stock_type in ('GAR-SUSPENSE','GAR-HOLD','RET-ALLOC-STOCK','RETAIL-STOCK')
and v_loc_num='90003'
and v.sku_id not in
(select distinct i.sku_id
from inventory i
where i.client_id='TR')
and sku.client_id='TR'
and v.sku_id=sku.sku_id
group by
v.sku_id,
SKU.USER_DEF_TYPE_2,
SKU.USER_DEF_TYPE_1,
SKU.USER_DEF_TYPE_4,
sku.sku_size,
v.stock_type)
where S<>0
order by
A
/
