SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 200
SET TRIMSPOOL ON

column A heading 'User ID' format a24
column B heading 'Name' format a30
column C1 heading 'C Picks' format 999999
column C2 heading 'T Picks' format 999999
column D1 heading 'Receipts' format 999999
column D2 heading 'Receipts' format 999999
column D3 heading 'Receipts' format 999999
column E heading 'Relocates' format 999999
column E2 heading 'Relocates' format 999999
column F heading 'Returns' format 999999
column G heading 'IDTs' format 999999
column H format A6
column M noprint

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


break on M skip 2

--compute sum label ',Totals' of C1 C2 D1 D2 D3 E E2 on report

--spool sbjobivity.csv

--select trunc(sysdate)-1 from DUAL;

select 'Date,Name,Start,Stop,Minutes,Job ID,Notes,Txns,Units' from DUAL;


select
MM M,MM||','||J11||','||MM1||','||MM2||','||(FF-mod(FF,60))/60||':'||substr(to_char(100+mod(FF,60)),2,2)||','||
JJ||','||NN1||','||C1||','||J1
from (
	select MM, J11, MM1, MM2,
	((to_number(substr(MM2,1,2))*60)+to_number(substr(MM2,4,2)))-((to_number(substr(MM1,1,2))*60)+to_number(substr(MM1,4,2))) FF,
	JJ, NN1, sum(CC) C1, sum(JA1) J1 
	from (
		select trunc(MM1) MM, UU1 J11, to_char(MM1,'HH24:MI') MM1, to_char(MM2,'HH24:MI') MM2, JJ, NN1, UN CC, nvl(UQ,0) JA1
		from (
			select distinct it.job_id JJ, substr(it.job_id,3,6) JJ1, it.Dstamp MM1,
				substr(it.notes,1,12) NN1, it.user_id UU1, au.name BBB, 
				rank() over(partition by it.user_id order by it.Dstamp) as rank1
			from inventory_transaction it,application_user au
			where trunc(it.Dstamp) between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')
			and it.client_id='SB'
			and code like 'Job Start'
			and it.user_id=au.user_id
			)
			,
			(
			select distinct substr(it.job_id,3,6) JJ2, it.Dstamp MM2, substr(it.notes,1,12) NN2, it.user_id UU2,
				DECODE(it.update_qty,1,0,0,0,1) UN, DECODE(it.update_qty,1,0,it.update_qty) UQ,
				rank() over(partition by it.user_id order by it.Dstamp) as rank2
			from inventory_transaction it
			where trunc(it.Dstamp) between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')
			and it.client_id='SB'
			and code like 'Job End'
			)
		where rank1=rank2
		and UU1=UU2
		and JJ1=JJ2
		group by UU1,MM1,MM2,JJ,NN1,UN,nvl(UQ,0)
union
		select trunc(MM1) MM, it.user_id J11, to_char(MM1,'HH24:MI') MM1, to_char(MM2,'HH24:MI') MM2, JJ,NN1, count(*) CC, sum(update_qty) JA1
		from 
			inventory_transaction it
			,
			(
			select distinct it.job_id JJ, substr(it.job_id,3,6) JJ1, it.Dstamp MM1, substr(it.notes,1,12) NN1,
				it.user_id UU1, au.name BBB, 
				rank() over(partition by it.user_id order by it.Dstamp) as rank1
			from inventory_transaction it,application_user au
			where trunc(it.Dstamp) between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')
			and it.client_id='SB'
			and code like 'Job Start'
			and it.user_id=au.user_id
			)
			,
			(
			select distinct substr(it.job_id,3,6) JJ2, it.Dstamp MM2, substr(it.notes,1,12) NN2, it.user_id UU2,
				rank() over(partition by it.user_id order by it.Dstamp) as rank2
			from inventory_transaction it
			where trunc(it.Dstamp) between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')
			and it.client_id='SB' and code like 'Job End'
			)
		where it.Dstamp between MM1 and MM2+0.00007
		and it.client_id='SB'
		and it.update_qty<>0
		and it.from_loc_id<>'CONTAINER'
		and it.code=DECODE(substr(JJ1,1,3),'STK','Stock Check','PIC','Pick','REL','Relocate','INT','Receipt','RET','Receipt','XXXX')
		and it.user_id=UU1
		and rank1=rank2
		and UU1=UU2
		and JJ1=JJ2
		group by it.user_id,MM1,MM2,JJ,NN1
	)
	where JJ like 'TT%' or JJ like 'TU%'  or JJ like 'SB%'
	group by MM, J11, MM1, MM2, JJ,NN1
)
order by MM,J11,MM1
/
--spool off
