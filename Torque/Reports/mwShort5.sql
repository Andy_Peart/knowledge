/******************************************************************************/
/*                                                                            */
/*                            Elite                                           */
/*                                                                            */
/*     FILE NAME  :   mwshortages5.sql                                        */
/*                                                                            */
/*   DATE     BY   PROJ     ID         DESCRIPTION                            */
/*   ======== ==== ======   ========   =============                          */
/*   28/02/08 BK   Mountain            Allocation shortage Population         */
/******************************************************************************/

/* Input Parameters are as follows */
-- 1)  From Date
-- 2)  To Date

set feedback off
set pagesize 68
set linesize 140
set verify off
set heading on

clear columns
clear breaks
clear computes

column AA heading 'Date' format A10
column A heading 'Order ID' format A12
column S heading 'Status' format A12
column B heading 'SKU ID' format A16
column C heading 'Shortage' format 999999
column D heading 'Available' format 999999
column DD heading '' format A6
column E heading 'Location' format A10
column F heading 'Description' format A40


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


TTITLE LEFT 'Mail Order Allocation Shortages from &&1 - Fulfillment Availability as at ' report_date skip 2

break on AA on A skip 1 on S on B on C  on F dup on report
--compute sum label 'Total' of D on report



select
to_char(gs.Dstamp,'DD/MM/YY') AA,
gs.task_id A,
SS S,
gs.sku_id B,
gs.qty_ordered-gs.qty_tasked C,
nvl(QQQ,0) D,
'    ' DD,
nvl(LLL,' ')  E,
sku.description F
from generation_shortage gs,sku,
(select
order_id OO,
status SS
from order_header
where client_id='MOUNTAIN'
and order_type='WEB'),
(select
sku_id SSS,
location_id LLL,
nvl(qty_on_hand-qty_allocated,0) QQQ
from inventory
where client_id='MOUNTAIN'
and location_id<>'DESPATCH'
and qty_on_hand-qty_allocated>0
and condition_id='GOOD')
--and condition_id='MMAIL')
where gs.client_id='MOUNTAIN'
and gs.work_group='WWW'
--and gs.condition_id='GOOD'
and gs.condition_id='MMAIL'
and gs.Dstamp>=to_date('&&1', 'DD-MON-YYYY')-1
and gs.sku_id=SSS
and gs.sku_id=sku.sku_id
and gs.task_id=OO
order by AA,A,B
/


set heading off

select 'NOTHING TO REPORT' from
(select
count(*) CT
from generation_shortage gs,order_header oh
where gs.client_id='MOUNTAIN'
and gs.task_id = oh.order_id
and oh.order_type='WEB'
and gs.condition_id='MMAIL'
--and gs.condition_id='GOOD'
and gs.Dstamp>=to_date('&&1', 'DD-MON-YYYY')-1)
where ct=0
/

