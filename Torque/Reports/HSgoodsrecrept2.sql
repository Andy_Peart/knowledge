SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON



select 'Transaction Date,Pre-Advice,SKU,Description,Condition,Qty Due,Qty Received,Difference' from dual
union all
select "AA" || ',' || "A" || ',' || "B" || ',' ||"D"|| ',' || "H"|| ',' || "E" || ',' || "F" || ',' || "G"  from
(
select nvl(to_char(it.Dstamp, 'DD/MM/YYYY'),'Not received') AA
, pl.pre_advice_id A
, pl.Sku_id B
, pl.Condition_id H
, sku.description D
, pl.qty_due E
, nvl(pl.qty_received,'0') F
, nvl(pl.qty_due,0)-nvl(pl.qty_received,0) G
from Pre_advice_line pl
left join inventory_transaction it on it.reference_id = pl.pre_advice_id and it.sku_id = pl.sku_id and it.client_id=pl.client_id AND it.line_id = pl.line_id
inner join sku on sku.sku_id = pl.sku_id
where  pl.client_id='HS'
and pl.pre_advice_id='&&2'
group by  pl.line_id
, nvl(to_char(it.Dstamp, 'DD/MM/YYYY'),'Not received')
, pl.pre_advice_id 
, pl.Sku_id 
, sku.description
, pl.Condition_id 
, pl.qty_due 
, nvl(pl.qty_received,'0')
, nvl(pl.qty_due,0)-nvl(pl.qty_received,0)
order by nvl(to_char(it.Dstamp, 'DD/MM/YYYY'),'Not received')
);
