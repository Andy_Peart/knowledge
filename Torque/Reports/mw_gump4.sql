SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','
SET UND OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 320


column A format A10
column B format A6
column C format A28
column D format A12
column E format 999999
column F format 999999
column H format A10
column I format A10
column J format A10



--set termout off
--spool MW-Shipments.csv

select 'Shipment Date,Store ID,Store Name,Order ID,Qty Ordered,Qty Shipped,,' from DUAL;

Select
to_char(it.dstamp,'DD/MM/YYYY') A,
it.Customer_ID B,
nvl(a.name,' ') C,
it.reference_ID D,
sum(ol.qty_ordered) E,
sum(it.Update_qty) F,
' '
from inventory_transaction it,order_header oh,order_line ol,address a
where it.client_id='MOUNTAIN'
and it.code='Shipment'
and trunc(it.Dstamp)=trunc(sysdate-1)
and it.reference_id=oh.order_id
and it.reference_id<>'WWW'
and oh.client_id='MOUNTAIN'
and it.reference_id=ol.order_id
and it.sku_id=ol.sku_id
and ol.client_id='MOUNTAIN'
and a.client_id = 'MOUNTAIN'
and it.customer_id=a.address_id (+)
group by
to_char(it.dstamp,'MMYY'),
to_char(it.dstamp,'DD/MM/YYYY'),
it.Customer_ID,
a.name,
it.reference_ID
union
Select
to_char(it.dstamp,'DD/MM/YYYY') A,
it.Customer_ID B,
nvl(oh.name,' ') C,
it.reference_ID D,
sum(ol.qty_ordered) E,
sum(it.Update_qty) F,
' '
from inventory_transaction it,order_header oh,order_line ol
where it.client_id='MOUNTAIN'
and it.code='Shipment'
and trunc(it.Dstamp)=trunc(sysdate-1)
and it.reference_id=oh.order_id
and it.customer_id='WWW'
and oh.client_id='MOUNTAIN'
and it.reference_id=ol.order_id
and it.sku_id=ol.sku_id
and ol.client_id='MOUNTAIN'
group by
to_char(it.dstamp,'MMYY'),
to_char(it.dstamp,'DD/MM/YYYY'),
it.Customer_ID,
oh.name,
it.reference_ID
order by
D
/

--spool off
--SET TERMOUT ON

