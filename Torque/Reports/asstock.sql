SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Sku,EAN Code,Description,Allocated Qty,Qty on Hand' from dual
union all
select "Sku" || ',' || "EAN" || ',' || "Desc" || ',' || "All" || ',' || "Qty" from
(
select sku.sku_id   as "Sku"
, sku.ean           as "EAN"
, sku.description   as "Desc"
, nvl(x.alloc,0)    as "All"
, nvl(x.qty,0)      as "Qty"
from sku 
left join 
(
select inv.sku_id
, sum(inv.qty_on_hand) as qty
, sum(inv.qty_allocated) as alloc
from inventory inv inner join location loc on inv.site_id = loc.site_id and loc.location_id = inv.location_id
where inv.client_id = 'AS'
and loc.loc_type in ('Tag-FIFO','Tag-LIFO')
group by inv.sku_id
) x
on sku.sku_id = x.sku_id
where sku.client_id = 'AS'
)
/
--spool off