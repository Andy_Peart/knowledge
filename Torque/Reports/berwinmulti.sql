SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 66
SET NEWPAGE 0
SET LINESIZE 120



ttitle 'BERWIN stock in multiple locations' skip 2

column AAA heading 'SKU' format A16
column BBB heading 'Location' format 99999999
column CCC heading 'Stock Qty' format 99999999


break on AAA skip 1



select
SKU_ID AAA,
location_id BBB,
sum(qty_on_hand) CCC
from inventory i, (select
sku_id A,
count(distinct location_id) B
from inventory
where client_id = 'BW'
group by
sku_id) Z
where b > 1
and i.sku_id = z.a
group by
SKU_ID,
location_id 
order by AAA,BBB
/
