/******************************************************************************/
/*                                                                            */
/*                            Elite                                           */
/*                                                                            */
/*     FILE NAME  :   clientxdespatch.sql                                     */
/*                                                                            */
/*     DESCRIPTION:    Datastream for Client X Despatch Note                  */
/*                                                                            */
/*   DATE     BY   PROJ       ID         DESCRIPTION                          */
/*   ======== ==== ======     ========   =============                        */
/*   30/08/06 MS   ClientX               Despatch Note for Client X           */
/******************************************************************************/

/* Input Parameters are as follows */
-- 1)  Order ID
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 0
SET VERIFY OFF
SET FEEDBACK OFF
SET TAB OFF

 
-- Set the line length to the maximum possible, but trim 
-- trailing spaces for both screen output and spool files
SET LINESIZE 32767
SET WRAP ON
SET TRIMOUT ON
SET TRIMSPOOL ON




/* Define the format of the output dates */
DEFINE	OutputDateFormat = 'DD-MON-YYYY HH24:MI:SS'
COLUMN	sorter	NOPRINT

SELECT Distinct mt.list_id||oh.order_id||'0' sorter,
       'DSTREAM_CLIENTX_DESP_HDR'          ||'|'||
       rtrim('&&2')                        ||'|'||    /* consignment */
       rtrim('&&3')			   ||'|'||    /* reprint */
       rtrim('&&4')			   ||'|'||    /* destination */
	rtrim(oh.order_id)
from order_header oh, move_task mt
where oh.consignment = '&&2'
and oh.status = 'Allocated'
and oh.order_id = mt.task_id
union all
SELECT distinct mt.list_id||oh.order_id||'1' sorter,
'DSTREAM_CLIENTX_DESP_ORD_HDR'              ||'|'||
rtrim (sysdate)				||'|'||
rtrim (mt.list_id)				||'|'||
rtrim (oh.order_id)				||'|'||
rtrim (oh.name)					||'|'||
rtrim (oh.customer_id)				||'|'||
rtrim (oh.address1)				||'|'||
rtrim (oh.address2)				||'|'||
rtrim (oh.town)					||'|'||
rtrim (oh.county)				||'|'||
rtrim (oh.INSTRUCTIONS)				||'|'||
rtrim (oh.postcode)				||'|'||
rtrim (oh.INV_NAME)					||'|'||
rtrim (oh.INV_ADDRESS1)					||'|'||
rtrim (oh.INV_ADDRESS2)					||'|'||
rtrim (oh.INV_TOWN)					||'|'||
rtrim (oh.INV_COUNTY)					||'|'||
rtrim (oh.INV_POSTCODE)					||'|'||
rtrim (oh.INV_CONTACT_FAX)					||'|'||
rtrim (oh.ORDER_DATE)					||'|'||
rtrim (oh.DELIVER_BY_DATE)				||'|'||
rtrim (oh.SHIP_BY_DATE)					||'|'||
rtrim (oh.PURCHASE_ORDER)				||'|'||
rtrim (oh.USER_DEF_TYPE_1)				||'|'||
rtrim (oh.USER_DEF_TYPE_2)				||'|'||
rtrim (oh.USER_DEF_TYPE_3)				||'|'||
rtrim (oh.USER_DEF_TYPE_4)				||'|'||
rtrim (oh.USER_DEF_TYPE_5)				||'|'||
rtrim (oh.USER_DEF_TYPE_6)				||'|'||
rtrim (oh.USER_DEF_TYPE_7)				||'|'||
rtrim (oh.USER_DEF_TYPE_8)				||'|'||
rtrim (oh.USER_DEF_CHK_1)				||'|'||
rtrim (oh.USER_DEF_CHK_2)				||'|'||
rtrim (oh.USER_DEF_CHK_3)				||'|'||
rtrim (oh.USER_DEF_CHK_4)				||'|'||
rtrim (oh.USER_DEF_DATE_1)				||'|'||
rtrim (oh.USER_DEF_DATE_2)				||'|'||
rtrim (oh.USER_DEF_DATE_3)				||'|'||
rtrim (oh.USER_DEF_DATE_4)				||'|'||
rtrim (oh.USER_DEF_NUM_1)				||'|'||
rtrim (oh.USER_DEF_NUM_2)				||'|'||
rtrim (oh.USER_DEF_NUM_3)				||'|'||
rtrim (oh.USER_DEF_NUM_4)				||'|'||
rtrim (oh.USER_DEF_NOTE_1)				||'|'||
rtrim (oh.USER_DEF_NOTE_2)
from order_header oh, move_task mt
where oh.consignment = '&&2'
and oh.status = 'Allocated'
and oh.order_id = mt.task_id
union all
select distinct nvl(mt.list_id,mt3.max_list_id)||oh.order_id||'2' sorter,
'DSTREAM_CLIENTX_DESP_ORDER_LINE'              ||'|'||
rtrim (ol.sku_id)					||'|'||
rtrim (ol.qty_ordered)					||'|'||
rtrim (s.description)					||'|'||
rtrim (nvl(mt.from_loc_id,'N/A'))					||'|'||
rtrim (nvl(mt.qty_to_move,0))					||'|'||
rtrim (ol.line_value)					||'|'||
rtrim (ol.USER_DEF_TYPE_1)				||'|'||
rtrim (ol.USER_DEF_TYPE_2)				||'|'||
rtrim (ol.USER_DEF_TYPE_3)				||'|'||
rtrim (ol.USER_DEF_TYPE_4)				||'|'||
rtrim (ol.USER_DEF_TYPE_5)				||'|'||
rtrim (ol.USER_DEF_TYPE_6)				||'|'||
rtrim (ol.USER_DEF_TYPE_7)				||'|'||
rtrim (ol.USER_DEF_TYPE_8)				||'|'||
rtrim (ol.USER_DEF_CHK_1)				||'|'||
rtrim (ol.USER_DEF_CHK_2)				||'|'||
rtrim (ol.USER_DEF_CHK_3)				||'|'||
rtrim (ol.USER_DEF_CHK_4)				||'|'||
rtrim (ol.USER_DEF_DATE_1)				||'|'||
rtrim (ol.USER_DEF_DATE_2)				||'|'||
rtrim (ol.USER_DEF_DATE_3)				||'|'||
rtrim (ol.USER_DEF_DATE_4)				||'|'||
rtrim (ol.USER_DEF_NUM_1)				||'|'||
rtrim (ol.USER_DEF_NUM_2)				||'|'||
rtrim (ol.USER_DEF_NUM_3)				||'|'||
rtrim (ol.USER_DEF_NUM_4)				||'|'||
rtrim (ol.USER_DEF_NOTE_1)				||'|'||
rtrim (ol.USER_DEF_NOTE_2)
from move_task mt, order_header oh, order_line ol, sku s, (select max(list_id) max_list_id, task_id, client_id from move_task group by task_id ,client_id) mt3 
where ol.order_id = oh.order_id
and ol.client_id = mt.client_id(+)
and oh.consignment = '&&2'
and oh.status = 'Allocated'
and ol.sku_id = s.sku_id
and ol.sku_id = mt.sku_id(+)
and ol.order_id = mt.task_id(+)
and ol.order_id = mt3.task_id(+)
order by 1, 2
/
