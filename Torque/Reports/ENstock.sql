SET FEEDBACK OFF                
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 400
SET TRIMSPOOL ON

--spool ENstock.csv

select 'STOCK LEVELS' from DUAL;
select 'Sku,Description,Colour,Size,Qty' from DUAL;

select
i.sku_id||''','
||sku.description||','
||sku.colour||','
||sku.sku_size||','
||sum(i.qty_on_hand)
from inventory i,sku
where i.client_id = 'EN'
and i.sku_id=sku.sku_id
and sku.client_id = 'EN'
group by
i.sku_id,
sku.description,
sku.colour,
sku.sku_size
order by
i.sku_id
/

--spool off

