SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET COLSEP ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 2000
SET TRIMSPOOL ON
SET HEADING OFF 

column A heading 'CODE' format A15
column B heading 'REFERENCE ID' format A20
column C heading 'SKU' format 999999
column D heading 'DESCRIPTION' format A20
column E heading 'QTY' format A60
column F heading 'CONDITION' format A10
column G heading 'FINAL LOCATION' format A10
column H heading 'USER ID' format A10
column I heading 'CATEGORY' format A20
column J heading 'DATE' format 9999999

SELECT 'Code, Reference ID, SKU, Description, Qty, Condition, Final Location, User ID, Category, Date' from dual;

break on A dup skip 1

select
it.code
||','||it.reference_id
||','||it.sku_id
||','||sk.DESCRIPTION
||','||it.update_qty
||','||it.condition_id
||','||it.FINAL_LOC_ID
||','||it.user_id
||','||sk.STACK_DESCRIPTION
||','||to_char(it.COMPLETE_DSTAMP, 'DD-MON-YYYY')
from 
INVENTORY_TRANSACTION it INNER JOIN sku sk ON  
IT.CLIENT_ID = SK.CLIENT_ID
AND IT.SKU_ID = SK.SKU_ID
WHERE
it.client_id = 'SB'
and it.code = '&&2'
and it.dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
order by
it.FINAL_LOC_ID
/
