SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Client,Sku,Description,User Def 3,User Def 5,Positive Adjustment,Negative Adjustment' from dual
union all
select "Client" || ',' || "Sku" || ',' || "Description" || ',' || "User def 3" || ',' || "User def 5" || ',' || "+/-" || "Adj" from
(
with t1 as (
select client_id    as "Client"
, sku_id            as "Sku"
, description       as "Description"
, user_def_type_3   as "User def 3"
, user_def_type_5   as "User def 5"
, tag_id            as "Tag"
from inventory
where client_id = 'MOUNTAIN'
and user_def_type_5 = 'BOND'
group by client_id
, sku_id
, description
, user_def_type_3
, user_def_type_5
, tag_id
), t2 as (
select client_id    as "Client"
, sku_id            as "Sku"
, tag_id            as "Tag"
, sum(update_qty)   as "Adj"
from inventory_transaction
where client_id = 'MOUNTAIN'
and dstamp between to_date ('&&2') and to_date('&&3')+1
and code in ('Adjustment','Stock Check')
and from_loc_id <> 'SUSPENSE'
group by client_id
, sku_id
, tag_id
)
select t1."Client"
, '''' || t1."Sku" || '''' as "Sku"
, t1."Description"
, t1."User def 3"
, t1."User def 5"
, case when t2."Adj" < 0 then ',' else '' end as "+/-"
, t2."Adj"
from t1
inner join t2 on t1."Client" = t2."Client" and t1."Sku" = t2."Sku" and t1."Tag" = t2."Tag"
where t2."Adj" <> 0
order by t1."Sku"
)
/
--spool off