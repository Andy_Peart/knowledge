/* SKU Count Report */

SET FEEDBACK OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 68
SET NEWPAGE 0
SET LINESIZE 132
 
/* Top Title */
--TTITLE CENTER _SITENAME SKIP 2 -
TTITLE LEFT "PROMINENT WAREHOUSE SKU COUNT" skip 2

column A heading 'No. Of Different|SKUs In Stock'
column B heading '  '

break on report
compute sum label 'Total' of A on report

select 
DECODE(zone_1,'LSH1','Hanging','Boxed') B,
count (distinct sku_id) A
from inventory
where client_id in ('PR','PS')
group by
DECODE(zone_1,'LSH1','Hanging','Boxed')
/
