/******************************************************************************/
/*                                                                            */
/*                            Elite                                           */
/*                                                                            */
/*     FILE NAME  :   opsprepick.sql                                          */
/*                                                                            */
/*     DESCRIPTION:    Datastream for OPS Pre Pick Note                       */
/*                                                                            */
/*   DATE     BY   PROJ     ID         DESCRIPTION                            */
/*   ======== ==== ======   ========   =============                          */
/*   16/09/11 BK   ECHO                Pre Pick Note for ECHO    (20172-04)   */
/*                                                                            */
/******************************************************************************/

/* Input Parameters are as follows */
-- 1)  Order ID
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES


SET PAGESIZE 0
SET VERIFY OFF
SET FEEDBACK OFF
SET TAB OFF

 
-- Set the line length to the maximum possible, but trim 
-- trailing spaces for both screen output and spool files
SET LINESIZE 320
SET WRAP ON
SET TRIMOUT ON
SET TRIMSPOOL ON




/* Define the format of the output dates */
DEFINE	OutputDateFormat = 'DD-MON-YYYY HH24:MI:SS'
COLUMN	sorter	NOPRINT


SELECT Distinct rtrim (oh.order_id) sorter,
       'DSTREAM_ECHO_PICK_A'        ||'|'||
       rtrim (oh.order_id)
from order_header oh
where oh.client_id = 'ECHO'
and (oh.order_id = '&&2'
or oh.consignment='&&2')
union all
select Distinct oh.order_id sorter,
'DSTREAM_ECHO_PICK_HDR'          	||'|'||
rtrim (oh.order_id)       	 	||'|'||
rtrim (oh.name) 			||'|'||
rtrim (trunc(oh.order_date)) 		||'|'||
rtrim (oh.num_lines)
from order_header oh
where oh.client_id = 'ECHO'
and (oh.order_id = '&&2'
or oh.consignment='&&2')
union all
select oh.order_id sorter,
'DSTREAM_ECHO_PICK_LINE' ||'|'||
rtrim (ol.sku_id) ||'|'||
rtrim (s.description) ||'|'||
rtrim (s.colour) ||'|'||
rtrim (s.ean) ||'|'||
rtrim (s.sku_size) ||'|'||
--rtrim (ol.from_loc_id) ||'|'||
rtrim (nvl(ol.update_qty,0))
from inventory_transaction ol, sku s,order_header oh
where ol.client_id = 'ECHO'
and oh.client_id = 'ECHO'
and ol.reference_id=oh.order_id
and (oh.order_id = '&&2'
or oh.consignment='&&2')
and ol.code='Allocate'
and ol.sku_id = s.sku_id
order by 1,2;


--exit;