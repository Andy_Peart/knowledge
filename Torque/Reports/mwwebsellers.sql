SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 120
set newpage 0
set verify off
set colsep ','
set trimspool on



column A heading 'Location' format A10
column C heading 'SKU' format A20
column B format 999999
column D format 999999
column F format 999 noprint

break on report

compute sum label 'Total' of B D on report

--spool WEBtots.csv

select 'MOUNTAIN WEB picks from &&2 to &&3' from DUAL;
select 'SKU,Description,Picks,Units' from DUAL;

select
it.sku_id C,
sku.description CC,
count(*) B,
sum (it.update_qty) D
from inventory_transaction it,location loc,sku
where it.client_id = 'MOUNTAIN'
and it.code = 'Pick'
and it.dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')
and it.from_loc_id=loc.location_id
and loc.site_id='BD2'
and loc.zone_1='WEB'
and it.sku_id=sku.sku_id
and sku.client_id = 'MOUNTAIN'
group by
it.sku_id,
sku.description
order by
C
/

--spool off
