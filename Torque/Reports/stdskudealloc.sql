SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 900
SET TRIMSPOOL ON
SET HEADING ON

with t1 as (
    select distinct oh.order_id, itl.sku_id
    from order_header oh inner join inventory_transaction itl on oh.order_id = itl.reference_id and oh.client_id = itl.client_id
    where oh.client_id = '&&2'
    and oh.status = 'Shipped'
    and itl.code = 'Deallocate'
    and oh.shipped_date between to_date('&&3') and to_date('&&4')+1    
)
select 'Order ID,SKU,Consignment,Created,Last Updated,Shipped,Customer ID,Order type,Ship dock,Qty Ordered,Qty \Shipped' COL1 from dual
union all
select order_id || ',' || sku_id || ',' || consignment || ',' || created || ',' || last_updated || ',' || shipped || ',' || customer_id || ',' || order_type || ',' || ship_dock || ',' || qty_ordered || ',' || qty_shipped from (
select oh.order_id, ol.sku_id, oh.consignment,to_char(oh.creation_date,'DD-MM-YYYY HH24:MI') created, to_char(oh.last_update_date,'DD-MM-YYYY HH24:MI') last_updated,
to_char(oh.shipped_date,'DD-MM-YYYY') shipped, oh.customer_id, oh.order_type, oh.ship_dock, sum(ol.qty_ordered) qty_ordered, sum(nvl(ol.qty_shipped,0)) qty_shipped
from order_header oh inner join t1 on oh.order_id = t1.order_id
inner join order_line ol on oh.order_id = ol.order_id and oh.client_id = ol.client_id and t1.sku_id = ol.sku_id
group by oh.order_id, ol.sku_id, oh.consignment,to_char(oh.creation_date,'DD-MM-YYYY HH24:MI') , to_char(oh.last_update_date,'DD-MM-YYYY HH24:MI') ,
to_char(oh.shipped_date,'DD-MM-YYYY') , oh.customer_id, oh.order_type, oh.ship_dock
order by 1,2
)
/