set feedback off
set pagesize 70
set linesize 150
set verify off
set colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

 

column A heading 'Location' format A11
column B heading 'SKU' format A30
column C heading 'Description' format A40
column D heading 'Qty on Hand' format 999999
column D1 heading 'Qty Allocated' format 999999
column E heading '  Moved' format A14

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

set und '-'
set heading on
set newpage 0

ttitle  LEFT 'Stock Units by Location Range from &&2 to &&3 - for Daxbourne as at ' report_date RIGHT 'Page:' FORMAT 999 SQL.PNO skip 4

break on report

compute sum label 'Total' of D on report

break on row skip 1

select
i.sku_id B,
Sku.description C,
i.location_id A,
sum(i.qty_on_hand) D,
sum(i.qty_allocated) D1,
'  ____________' E
from inventory i,sku
where i.client_id like 'DX'
and location_id between '&&2' and '&&3'
and i.sku_id=sku.sku_id
group by
i.sku_id,
Sku.description,
i.location_id 
order by i.location_id,i.sku_id
/

ttitle  LEFT ' '
set heading off
break on row skip 0
set newpage 1

select
'Location Count =',
count (*) D
from (select distinct location_id from inventory
where client_id like 'DX'
and location_id between '&&2' and '&&3')
/


select
'     SKU Count =',
count (*) D
from (select distinct sku_id from inventory
where client_id like 'DX'
and location_id between '&&2' and '&&3')
/

select
'    Unit Count =',
nvl(sum(qty_on_hand),0) D
from inventory
where client_id like 'DX'
and location_id between '&&2' and '&&3'
/

