/* MK Orders Giftwrapped */

SET FEEDBACK OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES


 
/* Set up page and line */
SET PAGESIZE 10
SET NEWPAGE 0
SET LINESIZE 132
 
/* Top Title */
TTITLE LEFT "Number of Giftwrapped orders for MenKind Web from &&2 to &&3" skip 2

column A heading 'No. Of Giftwraps'

SELECT COUNT(CLIENT_ID) A
FROM ORDER_HEADER
WHERE CLIENT_ID = 'MK'
AND ORDER_TYPE NOT IN ('RETAIL')
AND SUBTOTAL_3 > 0 
AND SHIPPED_DATE 
BETWEEN TO_DATE('&&2','DD-MM-YYYY') 
AND TO_DATE('&&3','DD-MM-YYYY')+1

/

