SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 200
set trimspool on
set newpage 0
set verify off
set colsep ','

column C format a4
column B format a10


break on A dup on report
compute sum label 'Day Total' of B B2 D on A
compute sum label 'Run Total' of B B2 D on report


--spool mwday99.csv

select 'Day,Work Group,Order,Picks,Units' from DUAL;


select
trunc(oh.shipped_date) A,
oh.work_group C,
oh.order_id B,
count(*) B2,
sum (ol.qty_shipped) D
from order_header oh,order_line ol
where oh.client_id = 'MOUNTAIN'
and oh.order_id like 'MOR%'
and oh.consignment not like '%GRID%'
and oh.consignment not like '%GAS%'
and oh.shipped_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and ol.client_id = 'MOUNTAIN'
and ol.order_id=oh.order_id
and ol.sku_id not like '3%' 
and ol.sku_id<>'5704122000690'
and ol.qty_shipped>0
group by
trunc(oh.shipped_date),
oh.work_group,
oh.order_id
union
select
trunc(oh.shipped_date) A,
oh.work_group C,
oh.order_id B,
count(*) B2,
sum (ol.qty_shipped) D
from order_header_archive oh,order_line_archive ol
where oh.client_id = 'MOUNTAIN'
and oh.order_id like 'MOR%'
and oh.consignment not like '%GRID%'
and oh.consignment not like '%GAS%'
and oh.shipped_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and ol.client_id = 'MOUNTAIN'
and ol.order_id=oh.order_id
and ol.sku_id not like '3%' 
and ol.sku_id<>'5704122000690'
and ol.qty_shipped>0
group by
trunc(oh.shipped_date),
oh.work_group,
oh.order_id
order by A,C
/


--spool off
