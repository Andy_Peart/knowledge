SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

select '  ' from DUAL;
select 'Week &&2 ' from DUAL;
select '  ' from DUAL;


select
'RELEASED HANGING UNITS = '||sum(qty_to_move) RRR
from order_header oh, move_task mt
where oh.client_id='TR'
and mt.client_id='TR'
and mt.status='Released'
and oh.status not in ('Cancelled','Hold','Shipped')
and oh.consignment like '&&2%'
and oh.consignment=mt.consignment
and oh.order_id=mt.task_id
and mt.from_loc_id like 'HANG%'
/
