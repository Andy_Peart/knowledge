SET FEEDBACK OFF
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
set colsep ','
set heading off
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9999

column A heading 'Pre Advice' format a12
column B heading 'SKU' format a20
column C heading 'EAN' format a20
column D heading 'Qty' format 9999999
column E heading 'Condition' format a20

select 'Pre Advice,SKU,Qty,Condition' from dual;

select 
i.receipt_id A,
i.sku_id B,
--s.ean C,
sum(nvl(i.qty_on_hand, 0)) D,
i.condition_id E
from inventory i
where i.client_id = 'MM'
--and s.sku_id = i.sku_id
and i.condition_id='HELD'
group by 
i.receipt_id,
i.sku_id,
i.condition_id
order by B
/
