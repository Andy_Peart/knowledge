--Want the report to show all fully allocated mail orders 
--so any order beginning with prefix "C" 
--where quantity ordered equals quantity tasked. . . 

SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 66
SET NEWPAGE 0
SET LINESIZE 240


column A heading 'Order Type' format A12
column C heading 'Country' format A8
column F heading 'Dispatch Method' format A16
column AA heading 'Order' format A12
column BB heading 'Date' format A14
column CC heading 'Order|Qty' format 999999
column TT heading 'Tasked|Qty' format 999999

--select 'Shop,Date,Qty' from DUAL;

break on A dup skip 2 on F dup skip 1

select
DDD A,
FFF F,
CCC C,
AAA AA,
BBB BB,
OOO CC,
TTT TT 
from (select
oh.order_type  DDD,
oh.Dispatch_method FFF,
oh.country CCC,
oh.order_id  AAA,
trunc(oh.creation_date) BBB,
count(*) ct,
nvl(sum(ol.qty_ordered),0) OOO,
nvl(sum(ol.qty_tasked),0) TTT
from order_header oh, order_line ol
where oh.client_id='UT'
--and (oh.order_id like 'C%' or oh.order_id like 'E%')
and oh.status not in ('Shipped','Cancelled')
and oh.order_id=ol.order_id
and oh.order_type like '&&2%'
group by
oh.order_type,
oh.Dispatch_method,
oh.country,
oh.order_id,
trunc(oh.creation_date))
where ct>1
and OOO>TTT
and TTT>0
order by
A,F,BB,AA
/


