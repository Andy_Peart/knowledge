SET FEEDBACK OFF             
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 300
SET TRIMSPOOL ON


column A format a18
column B format a8
column C format 999999
column D format 999999
column E format 999999
column F format a32
column G format a10
column H format a18
column J format a8
column K format a10
column L format a10

--break on CL dup skip 2

--set termout off
--spool smlcsm.csv

select 'Client,Sku Code,Condition,Current Stock,Description,Colour,Size,Days since last Receipt,Days since last Shipment'
from dual
union all
select
CL||','||
A||','||
B||','||
C||','||
F||','||
H||','||
J||','||
K||','||
L from (Select
''''||PA||'''' A,
CL,
CA B,
QQ C,
sku.description F,
sku.colour H,
sku.sku_size J,
--to_char(CB, 'DD/MM/YY') K,
trunc(sysdate)-trunc(CB) K,
DECODE(to_char(PB, 'DD/MM/YY'),'01/01/00','None',trunc(sysdate)-trunc(PB)) L
from
(select
CL,
PA,
CA,
CB,
QQ,
nvl(BB,'01-jan-00') PB
from
(select client_id CL,
sku_id PA,
condition_id CA,
max(trunc(receipt_dstamp)) CB,
sum(qty_on_hand) QQ
from inventory
where client_id in ('IF','S2','GG','VF','EX')
group by client_id,sku_id,condition_id),
(select
i.client_id CC,
i.Sku_id PP,
max (i.dstamp) BB
from inventory_transaction i
where i.client_id in ('IF','S2','GG','VF','EX')
and i.code = 'Shipment'
group by i.client_id,i.sku_id)
where CL=CC (+)
and PA=PP (+)),
sku
where sku.client_id in ('IF','S2','GG','VF','EX')
and CL=sku.client_id
and PA=sku.sku_id
and PB <sysdate - '&&2'
and CB <sysdate - '&&2'
order by CL,PB,CB)
/

--spool off
--set termout on
