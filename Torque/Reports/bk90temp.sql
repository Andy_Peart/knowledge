SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 68
SET NEWPAGE 0
SET LINESIZE 240

column A heading 'Code' format A12
column B heading 'Txn Date' format A12
column C heading 'Txn Time' format A10
column D heading 'Elapsed Time' format 99999999
column DD heading 'Elapsed Time' format A14
column DDD heading 'System Time' format A14
column E heading 'From Location' format A14
column F heading 'Update Qty' format 99999999
column G heading 'Final Location' format A14
column H heading 'User ID' format A14
column M format a4 noprint

--select 'Order ID,Ordered,Picked' from DUAL;
 
set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DAY, DD MONTH, YYYY') curdate from DUAL;
set TERMOUT ON


--break on report

--compute sum label 'Total' of D on report


TTITLE left 'Period &&5 to &&6 on &&4 for User ID &&3 on Client &&2' skip 2

--SET PAGESIZE 66

select
Code A,
to_char(Dstamp,'YY/MM') M,
to_char(Dstamp,'DD/MM/YYYY') B,
to_char(Dstamp,'HH24:MI:SS') C,
substr(Dstamp-lag(Dstamp,1) over (order by Dstamp),12,8) DD,
--to_char(Dstamp-lag(Dstamp,1) over (order by Dstamp),'HH24:MI:SS') DD,
to_char(trunc(elapsed_time/60/60),'09') ||to_char(trunc(mod(elapsed_time,3600)/60),'09') ||to_char(mod(mod(elapsed_time,3600),60),'09') DDD,
--elapsed_time D,
from_loc_id E,
update_qty F,
final_loc_id G,
user_id H
from inventory_transaction
where code in ('Relocate','Pick')
and client_id='&&2'
and user_id='&&3'
--and Dstamp between to_date('&&4', 'DD-MON-YYYY') and to_date('&&5', 'DD-MON-YYYY')+1
and trunc(Dstamp)=to_date('&&4', 'DD-MON-YYYY')
and to_char(Dstamp,'HH24') between '&&5' and '&&6'-1
and elapsed_time>0
order by 
M,B,C
/
