/******************************************************************************/
/*                                                                            */
/*                            Elite                                           */
/*                                                                            */
/*     FILE NAME  :   pridespatch.sql                                         */
/*                                                                            */
/*     DESCRIPTION:    Datastream for Pringle Despatch Note / Pack List       */
/*                                                                            */
/*   DATE     BY   PROJ       ID         DESCRIPTION                          */
/*   ======== ==== ======     ========   =============                        */
/*   29/08/06 LH   Pringle               Despatch Note for Pringle            */
/*   08/09/06 MS   Pringle    		     Pack List Added					  */
/*   04/10/06 LH   Pringle               Added extra fields for note	      */
/******************************************************************************/

/* Input Parameters are as follows */
-- 1)  Order ID
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 0
SET VERIFY OFF
SET FEEDBACK OFF
SET TAB OFF

 
-- Set the line length to the maximum possible, but trim 
-- trailing spaces for both screen output and spool files
SET LINESIZE 32767
SET WRAP ON
SET TRIMOUT ON
SET TRIMSPOOL ON




/* Define the format of the output dates */
DEFINE	OutputDateFormat = 'DD-MON-YYYY HH24:MI:SS'
COLUMN	sorter	NOPRINT

SELECT Distinct '0' sorter,
       'DSTREAM_PRI_DESP_HDR'          ||'|'||
       rtrim('&&2')                            /* Order_id */
from dual
union all
SELECT '1' sorter,
'DSTREAM_PRI_DESP_ADD_HDR'              ||'|'||
rtrim (oh.name)						||'|'||
rtrim (oh.customer_id)				||'|'||
rtrim (oh.address1)					||'|'||
rtrim (oh.address2)					||'|'||
rtrim (oh.town)						||'|'||
rtrim (oh.county)					||'|'||
rtrim (oh.country)					||'|'||
rtrim (oh.postcode)
from order_header oh
where oh.order_id = '&&2'
union all
select distinct '1' sorter,
'DSTREAM_PRI_DESP_ORD_HDR'			||'|'||
rtrim (oh.order_id)					||'|'||
rtrim (to_char(sm.picked_dstamp, 'DD-MON-YYYY HH24:MI'))				||'|'||
rtrim (sm.location_id)				||'|'||
rtrim (sm.user_id)					||'|'||
rtrim (sm.site_id)					||'|'||
rtrim (oh.purchase_order)			||'|'||
rtrim (oh.user_def_type_6)			||'|'||
rtrim (oh.user_def_type_4)			||'|'||
rtrim (substr(oh.order_id, 3, 7))	||'|'||
rtrim (substr(oh.order_id, 10, 14))
from order_header oh, shipping_manifest sm
where oh.order_id = '&&2'
and oh.order_id = sm.order_id
union all
SELECT '1'||sh.container_id SORTER,
'DSTREAM_PRI_DESP_LINE'              ||'|'||
rtrim (sh.container_id)       ||'|'||
rtrim (s.sku_id)				||'|'||
rtrim (s.description)                ||'|'||
rtrim (s.COLOUR)					||'|'||
rtrim (s.SKU_SIZE)					||'|'||
rtrim (sh.qty_shipped)				||'|'||
rtrim (s.user_def_type_5)
from sku s, shipping_manifest sh
where sh.order_id = '&&2'
and sh.qty_picked > '0'
and sh.sku_id = s.sku_id
union all
select distinct '2'||sh.container_id sorter,
'DSTREAM_PRI_PACK_HDR'			||'|'||
rtrim ('&&2')				 /* Order_id */
from order_header oh, shipping_manifest sh
where oh.order_id = '&&2'
and oh.order_id = sh.order_id
union all
select distinct '2'||sh.container_id sorter,
'DSTREAM_PRI_PACK_ORD_ADD'              ||'|'||
rtrim (oh.name)						||'|'||
rtrim (oh.customer_id)				||'|'||
rtrim (oh.address1)					||'|'||
rtrim (oh.address2)					||'|'||
rtrim (oh.town)						||'|'||
rtrim (oh.county)					||'|'||
rtrim (oh.country)					||'|'||
rtrim (oh.postcode)
from order_header oh, shipping_manifest sh
where oh.order_id = '&&2'
and oh.order_id = sh.order_id
union all
select distinct '2'||sm.container_id sorter,
'DSTREAM_PRI_PACK_ORD_HDR'			||'|'||
rtrim (oh.order_id)					||'|'||
rtrim (to_char(sm.picked_dstamp, 'DD-MON-YYYY HH24:MI'))				||'|'||
rtrim (sm.location_id)				||'|'||
rtrim (sm.user_id)					||'|'||
rtrim (sm.site_id)					||'|'||
rtrim (oh.purchase_order)			||'|'||
rtrim (oh.user_def_type_6)			||'|'||
rtrim (oh.user_def_type_4)			||'|'||
rtrim (substr(oh.order_id, 3, 7))	||'|'||
rtrim (substr(oh.order_id, 10, 14))
from order_header oh, shipping_manifest sm
where oh.order_id = '&&2'
and oh.order_id = sm.order_id
union all
SELECT '2'||sh.container_id SORTER,
'DSTREAM_PRI_PACK_ORD_LINE'              ||'|'||
rtrim (s.sku_id)       ||'|'||
rtrim (s.description)                ||'|'||
rtrim (s.COLOUR)					||'|'||
rtrim (s.SKU_SIZE)					||'|'||
rtrim (sh.qty_picked)				||'|'||
rtrim (s.user_def_type_5)			||'|'||
rtrim (sh.container_id)	
from sku s, shipping_manifest sh
where sh.order_id = '&&2'
and sh.qty_picked > '0'
and sh.sku_id = s.sku_id
/*and sh.order_id = sh.order_id
and s.sku_id = sh.sku_id*/
union all
SELECT distinct '3'||sm.picked_dstamp sorter,
'DSTREAM_PRI_SWING_LABEL_HDR'		||'|'||
rtrim (ol.sku_id)			||'|'||
rtrim (ol.qty_picked)
from order_line ol, shipping_manifest sm
where ol.order_id = '&&2'
and ol.qty_picked > '0'
and ol.order_id = sm.order_id
and ol.sku_id = sm.sku_id
union all
SELECT distinct '3'||sm.picked_dstamp sorter,
'DSTREAM_PRI_SWING_LABEL_LINE'		||'|'||
rtrim (ol.sku_id)			||'|'||
rtrim (s.description)			||'|'||
rtrim (s.each_value)			||'|'||
rtrim (s.colour)			||'|'||
rtrim (s.ean)
from order_line ol, sku s, shipping_manifest sm
where ol.order_id = '&&2'
and ol.qty_picked > '0'
and ol.sku_id = s.sku_id
and ol.order_id = sm.order_id
and ol.sku_id = sm.sku_id
order by 1,2
/