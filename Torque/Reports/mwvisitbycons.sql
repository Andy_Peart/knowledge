SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Code,Consignment,Lines,Units,Unique Locations,2FLA,2FLB,2FLC,2FLD,FOOT,GFLA,GFLB,GFLC,GFLOOR,SND,WBOT,DRY STORE,WEB,WEB1,WEB2,WRET,WRETA,WRETB,SMALL,OTHER' from dual
union all
select "Code" || ',' || "Cons" || ',' || "Lines" || ',' || "Units" || ',' || "Locs" || ',' || "2FLA" || ',' || "2FLB" || ',' || "2FLC" || ',' || "2FLD" || ',' || "FOOT" || ',' || "GFLA" || ',' || "GFLB" 
|| ',' || "GFLC" || ',' || "GFLOOR" || ',' || "SND" || ',' || "WBOT" || ',' || "DRY STORE" || ',' || "WEB" || ',' || "WEB1" || ',' || "WEB2" || ',' || "WRET" || ',' || "WRETA" || ',' || "WRETB" || ',' || "SMALL" || ',' || "OTHER"
from (
select "Code"
, "Cons"
, sum("Lines") "Lines"
, sum("Units") "Units"
, count(distinct "Unique Locations") "Locs"
, sum(case when "Zone" = '2FLA' then 1 else 0 end) as "2FLA"
, sum(case when "Zone" = '2FLB' then 1 else 0 end) as "2FLB"
, sum(case when "Zone" = '2FLC' then 1 else 0 end) as "2FLC"
, sum(case when "Zone" = '2FLD' then 1 else 0 end) as "2FLD"
, sum(case when "Zone" = 'FOOT' then 1 else 0 end) as "FOOT"
, sum(case when "Zone" = 'GFLA' then 1 else 0 end) as "GFLA"
, sum(case when "Zone" = 'GFLB' then 1 else 0 end) as "GFLB"
, sum(case when "Zone" = 'GFLC' then 1 else 0 end) as "GFLC"
, sum(case when "Zone" = 'GFLOOR' then 1 else 0 end) as "GFLOOR"
, sum(case when "Zone" = 'SND' then 1 else 0 end) as "SND"
, sum(case when "Zone" = 'WBOT' then 1 else 0 end) as "WBOT"
, sum(case when "Zone" = 'DRY STORE' then 1 else 0 end) as "DRY STORE"
, sum(case when "Zone" = 'WEB' then 1 else 0 end) as "WEB"
, sum(case when "Zone" = 'WEB1' then 1 else 0 end) as "WEB1"
, sum(case when "Zone" = 'WEB2' then 1 else 0 end) as "WEB2"
, sum(case when "Zone" = 'WRET' then 1 else 0 end) as "WRET"
, sum(case when "Zone" = 'WRETA' then 1 else 0 end) as "WRETA"
, sum(case when "Zone" = 'WRETB' then 1 else 0 end) as "WRETB"
, sum(case when "Zone" = 'SMALL' then 1 else 0 end) as "SMALL"
, sum(case when "Zone" not in ('2FLA','2FLA','2FLB','2FLC','2FLD','FOOT','GFLA','GFLB','GFLC','GFLOOR','SND','WBOT','DRY STORE','WEB','WEB1','WEB2','WRET','WRETA','WRETB','SMALL') then 1 else 0 end) as "OTHER"
from (
select it.code          as "Code"
, it.consignment        as "Cons"
, count(it.code)        as "Lines"
, sum(it.update_qty)    as "Units"
, it.from_loc_id        as "Unique Locations"
, l.work_zone           as "Zone"
from inventory_transaction it
inner join location l on l.site_id = it.site_id and l.location_id = it.from_loc_id
where it.client_id = 'MOUNTAIN'
and it.dstamp between to_date('&&2 &&4','DD-MON-YYYY HH24:MI:SS') and to_date('&&3 &&5','DD-MON-YYYY HH24:MI:SS')
and it.code = 'Pick'
group by it.code
, it.consignment
, it.from_loc_id
, l.work_zone
)
group by "Code"
, "Cons"
)
/
--spool off