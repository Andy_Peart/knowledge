set feedback off
set verify off
set colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

 

column A heading '' format A11
column B heading '' format A30
column C heading 'Description' format A40
column D heading 'Qty' format 999999
column E heading '  Moved' format A14

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

set pagesize 9999
set linesize 120
set trimspool on
set und off

ttitle  LEFT 'Empty Locations in Range from &&2 to &&3 - for Daxbourne as at ' report_date RIGHT 'Page:' FORMAT 999 SQL.PNO skip 4

--break on report

--compute sum label 'Total' of D on report

break on row skip 1

--spool dxempty.csv

select
location_id A,
'___________________________' B
from location
where location_id like 'DX%'
and location_id between '&&2' and '&&3'
and location_id not in (select
distinct location_id
from inventory 
where client_id like 'DX')
/

--spool off
