SET FEEDBACK OFF
SET VERIFY OFF
SET TAB OFF
SET COLSEP ' '

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Column Headings and Formats */
COLUMN B heading "SKU" FORMAT a18
COLUMN Q heading "Expected|Qty" FORMAT 999999
COLUMN C heading "Colour" FORMAT A12
COLUMN Z heading "Size" FORMAT A12
COLUMN D heading "Description" FORMAT a40
COLUMN M noprint


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


SET LINESIZE 130
SET WRAP OFF
SET PAGES 66
SET COLSEP ' '
SET NEWPAGE 0

/* Top Title */
TTITLE LEFT 'GRN Report for PO &&3 - Client ID &&2' RIGHT 'Date: ' report_date SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 1 - 

/* Set up page and line */

--break on AA dup skip 1 on report

compute sum label 'Totals' of Q on report


SELECT
pal.SKU_ID B,
nvl(CCC,'') C,
nvl(ZZZ,'') Z,
nvl(DDD,'') D,
nvl(PAL.Qty_Due,0) Q
FROM Pre_Advice_Header PAH,Pre_Advice_Line PAL, (select 
sku_id SSS,
description DDD,
colour CCC,
sku_size ZZZ,
ean EEE 
from sku where client_id='&&2')
where PAH.Pre_Advice_ID = '&&3'
and PAH.client_id = '&&2'
and PAL.client_id = '&&2'
and PAH.Pre_Advice_ID = PAL.Pre_Advice_ID
AND PAL.SKU_ID = SSS (+)
ORDER BY
B
/

