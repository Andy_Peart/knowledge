SET FEEDBACK OFF                 
set pagesize 999
set linesize 180
set verify off
set newpage 0

clear columns
clear breaks
clear computes

column A heading 'TAG ' format a12
column B heading 'SKU' format A18
column C heading 'MWF' format A12
column D heading 'Description' format A40
column E heading 'Location' format A10
column F heading 'Qty' format 999999
column G heading 'Moved to Location' format A20


break on row skip 1

ttitle left "Mountain Warehouse - Location Range Search Between '&&2' and '&&3'" skip 2

select 
inv.tag_id A,
sku.sku_id B, 
inv.receipt_id C,
sku.description D, 
inv.location_id E, 
inv.qty_on_hand F, 
'___________________' G
from sku, inventory inv
where sku.client_id = 'MOUNTAIN'
and sku.sku_id = inv.sku_id
and inv.location_id not like 'MARSHAL'
and inv.zone_1 not like 'Z3OUT'
and inv.location_id between upper ('&&2') and upper ('&&3')
and inv.location_id<>'SUSPENSE'
order by E
/

