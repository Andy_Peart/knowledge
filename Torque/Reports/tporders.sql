SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 200
SET TRIMSPOOL ON

--break on D3 dup skip 1 on report

--compute sum label 'Sub Total' of B C K on A2

--spool tporders.csv

select 'TP ORDERS FROM &&2 to &&3' from DUAL;

select 'Date,Customer,Lines,Orders,Units Shipped' from DUAL;

select
trunc(oh.shipped_date),
oh.customer_id,
oh.num_lines,
count(distinct oh.order_id),
sum(ol.qty_shipped)
from order_header oh, order_line ol
where oh.client_id='TP'
and oh.shipped_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and ol.client_id='TP'
and oh.order_id=ol.order_id
group by
trunc(oh.shipped_date),
oh.customer_id,
oh.num_lines
order by
trunc(oh.shipped_date),
oh.customer_id
/

--spool off
