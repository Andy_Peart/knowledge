SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep '|'


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 300
SET TRIMSPOOL ON
 
column XX NOPRINT

--spool &&3'.csv

select
order_id  XX,
DECODE('&&3','UKPAC','I364008','I099976')||'|'||
to_char(Sysdate,'DDMMYYYY')||'|'||
contact||'|'||
Name||'|'||
Address1||'|'||
Address2||'|'||
Town||'|'||
County||'|||'||
PostCode||'||'||
'1'||'|'||
DECODE('&&3','UKPAC','1000','10')||'|'||
DECODE('&&3','UKPAC','545','1')||'|'||
user_def_note_1||'|'||
user_def_note_2||'|||||'||
purchase_order||'|||||||||||||||'||
'Torque Commerce Court Challenge Way Cutler Heights Lane Bradford BD4 8NW'||'||'||
' '
from order_header,(select
oh.order_id AAA,
sum(nvl(ol.line_value,0)) BBB
from order_header oh, order_line ol
where oh.client_id='T'
--and oh.country='GBR'
and oh.purchase_order='&&2'
and ol.client_id='T'
and oh.order_id=ol.order_id
group by
oh.order_id)
where client_id='T'
and purchase_order='&&2'
and order_id=AAA
order by XX
/


--spool off


