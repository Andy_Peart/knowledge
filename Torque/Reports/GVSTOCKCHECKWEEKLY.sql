SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 160

Select 'SKU ID,DESCRIPTION,COLOUR,DATE,UPDATE QTY' from DUAL;

--column A heading 'SKU ID' format a20
--column B heading 'QTY' format 9999999
--column C heading 'DATE' format 9999999

select sku_id,C "Description",D "Colour",B "Date",A "Update Quantity"
from (select it.sku_id, sum(update_qty) A,max(trunc(dstamp))B ,s.description C, s.colour D
from inventory_transaction it, sku s
where it.client_id = 'GV'
--and to_loc_id <> 'SUSPENSE'
--and from_loc_id <> 'SUSPENSE'
and code = 'Adjustment'
and trunc(it.dstamp) > trunc(sysdate - 7)
and it.sku_id not like ('C%')
and it.sku_id = s.sku_id
group by it.sku_id,s.description, s.colour)
where A<>0
/
