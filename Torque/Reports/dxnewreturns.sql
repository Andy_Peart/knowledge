SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9999
SET TRIMSPOOL ON


--spool dxnewreturns.csv

select 'New Returns Report from &&2 to &&3' from DUAL;

select 'Date,Order Type,Rev.Stream,Product,Description,Condition,Qty' from DUAL;


select
A1||','||
A2||','||
DECODE(substr(A2,1,5),'WHOLE',A2,(DECODE(A6,'QVC','QVC','IDEAL','IDEAL',A3)))||','||
A4||','||
A5||','||
A6||','||
A7
from (select
trunc(i.Dstamp) A1,
nvl(CC,decode(substr(i.reference_id,1,2),'SI','WHOLESALEB','PK','WHOLESALE','UNKNOWN')) A2,
substr(DD,3,2) A3,
i.sku_id A4,
sku.description A5,
i.condition_id A6,
i.update_qty A7
from inventory_transaction i,sku,(select
nvl(order_type,' ') CC,
nvl(user_def_type_1,' ') DD,
order_id OO,
customer_id CID
from order_header
where client_id='DX')
where i.client_id='DX'
and i.code = 'Receipt'
and i.to_loc_id in ('DXBRET','DXBREJ')
and i.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and sku.client_id = 'DX'
and i.sku_id=sku.sku_id
and i.reference_id=OO (+))
order by
1
/

--spool off
