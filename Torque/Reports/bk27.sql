SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

column A heading 'Store' format A6
column B heading 'Name' format A32
column C heading 'Order Date' format A12
column D heading 'Units' format 99999999
column DD heading 'Shipped' format 99999999
column F heading 'Order ID' format A10
column G heading 'Consignment' format A12
column GG heading 'Container' format A12
column H heading 'SKU' format A18
column J heading 'Season' format A10
column K heading 'Style' format A10
column L heading 'Qual' format A10
column N heading 'Size' format A12
column P heading 'Carton ID' format A12
column M format a4 noprint

select 'Store,Name,Order ID,Order Date,Consignment,Container,SKU,Season,Style,Qual,Size,Units' from DUAL;
 
set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DAY, DD MONTH, YYYY') curdate from DUAL;
set TERMOUT ON

break on B dup skip 1 on report skip 1

compute sum label 'Total' of D DD on B

select
it.customer_id A,
name B,
it.reference_id F,
to_char(Dstamp,'DD/MM/YY') C,
it.consignment G,
it.container_id GG,
it.sku_id H,
sku.User_Def_Type_2 J,
sku.User_Def_Type_1 K,
sku.User_Def_Type_4 L,
sku.sku_size N,
nvl(it.update_qty,0) D
from inventory_transaction it,address,sku
where it.client_id='TR'
and it.code='Shipment'
and trunc(it.Dstamp)>='01-OCT-2007'
and it.sku_id=sku.sku_id
and sku.client_id='TR'
and it.customer_id in ('T600','T601','T602','T603','T604','T606')
and it.customer_id=address_id
and address.client_id='TR'
order by
it.customer_id,
it.reference_id,
it.sku_id
/
