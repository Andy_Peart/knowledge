SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON


select 'GRNs for consignment &2' from dual
union all
select '------------------------' from dual
union all
select 'GRN, Status, Docket, Receive Date'
from dual
union all
select order_id  ||','||  status  ||','||  docket  ||','|| rec_date
from (
select oh.order_id, oh.status, ol.user_def_type_4 docket, max(to_char(inv.receipt_dstamp, 'DD-MM-YYYY')) rec_date
from order_header oh inner join order_line ol on oh.order_id = ol.order_id and oh.client_id = ol.client_id inner join inventory inv on inv.sku_id = ol.sku_id
where oh.client_id in ('PR','PS')
and ol.client_id in ('PR','PS')
and inv.client_id in ('PR','PS')
and oh.status in ('Released', 'In Progress','Allocated')
and oh.consignment = '&2'
group by oh.order_id, oh.status, ol.user_def_type_4
order by 2
)
/