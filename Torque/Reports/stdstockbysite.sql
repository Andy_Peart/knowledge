SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ' '



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 9999
SET LINESIZE 500
SET TRIMSPOOL ON

column A heading 'Site' format A10
column B heading 'Client' format A10
column Q1 heading 'Qty on Hand' format 99999999
column Q2 heading 'Qty Allocated' format 99999999


break on A skip 1 on report
compute sum label 'Site Total' of Q1 Q2 on A
compute sum label 'Full Total' of Q1 Q2 on report



--spool stdstkbysite.csv

--select 'Site,Client,Qty on Hand,Qty Allocated' from Dual;


select
site_id A,
client_id B,
sum(qty_on_hand) Q1,
sum(qty_allocated) Q2
from inventory
where site_id like '&&2%'
group by
site_id,
client_id
order by 1,2
/

--spool off



