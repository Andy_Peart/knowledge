SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


select 'Orders to pick on &&2 :' from dual
union all
select 'Order ID,Store No,Store Name,Units,Creation Date,Creation Time,Order Type,Cuf-off time,Consignment' from DUAL
union all
select order_id  || ',' || address_id || ',' || name  || ',' || units  || ',' || creation_da  || ',' || creation_ti  || ',' || order_type || ',' || cut_off_time || ',' || consignment
from (
select
oh.order_id,
a.address_id, 
a.name,
oh.order_volume*10000 as units,
to_char(oh.creation_date,'DD-MM-YYYY') as creation_da,
to_char(oh.creation_date,'HH24:MI') as creation_ti,
oh.order_type,
CASE 
     WHEN oh.user_def_type_5  = 'T219' then '10am'
     WHEN oh.user_def_type_5  = 'T223' then '10am'
     WHEN oh.user_def_type_5  = 'T244' then '10am'
     WHEN oh.user_def_type_5  = 'T292' then '10am'     
     else '12 midday'
END AS cut_off_time,
oh.consignment
from address a inner join order_header oh on a.address_id = oh.user_def_type_5
where a.client_id = 'TR'
and oh.order_id like 'H%'
and oh.client_id = 'TR'
and oh.dispatch_method = '1'
and a.address_id like 'T%'
and oh.ship_dock = 'TRWEBCC'
and oh.status not in ('Cancelled','Hold','Shipped','Released')
AND 
(
CASE 
     WHEN '&&2'  = 'SUNDAY' then oh.user_def_type_5
END 
 in (
'T215',
'T236',
'T261',
'T283',
'T284',
'T263',
'T276',
'T259',
'T219',
'T223',
'T244',
'T292'
)     
or 
CASE 
     WHEN '&&2'  = 'FRIDAY' then oh.user_def_type_5
END 
 in (
'T213',
'T216',
'T220',
'T231',
'T234',
'T237',
'T239',
'T240',
'T265',
'T268',
'T270',
'T271',
'T272',
'T295',
'T296',
'T299',
'T238',
'T260',
'T266'
)     
)
order by 8,2
)
/