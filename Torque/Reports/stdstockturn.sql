SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
with t1 as (
select i.sku_id as "Sku"
, sum(i.qty_on_hand) as "Qty"
from inventory i
inner join location l on l.site_id = i.site_id and l.location_id = i.location_id
where i.client_id = '&2'
--and l.loc_type in ('Tag-FIFO','Tag-LIFO')
group by i.sku_id
), t2 as (
select "Sku",sum("Pick") as "Pick","Desc" from (
select it.sku_id as "Sku"
, case when it.code = 'Pick' then sum(it.update_qty) when it.code = 'UnPick' then -(sum(it.update_qty)) end as "Pick"
, s.description as "Desc"
from inventory_transaction it
inner join sku s on s.client_id = it.client_id and s.sku_id = it.sku_id
where it.client_id = '&2'
and it.dstamp between sysdate-7 and sysdate+1
and it.code in ('Pick','UnPick')
group by it.sku_id, it.code, s.description
) group by "Sku", "Desc"
)
select 'Sku,Description,Qty on Hand,,Qty picked over last 7 days,Stock Turn %' from dual
union all
select '''' ||t2."Sku" || ''',' || t2."Desc" || ',' || nvl(t1."Qty",0) || ',,' || t2."Pick" || ',' || to_char(t2."Pick"/(t2."Pick"+nvl(t1."Qty",0)),'99.9999')*100 || '%'
from t2
left join t1 on t1."Sku" = t2."Sku"
/
--spool off