SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 480
SET TRIMSPOOL ON

column A heading 'Store No.' format a12
column B heading 'Name' format a30
column C heading 'No. of Returns' format 999999
column D heading 'Units Returned' format 999999

select 'Returns by Product between &&2 and &&3' from DUAL;
select 'Store No,Name,No of Returns,Units Returned,DP,MA,MC,MF,MI,MJ,MK,ML,MP,MS,MT,MU,MW,MX,MZ,WA,WC,WD,WH,WI,WJ,WK,WL,WM,WP,WS,WV,WW,ZV,OTHER' from DUAL;

break on report

--compute sum label 'Total' of C D  on report

select
it.reason_id "Store No",
nvl(a.name,'Not Found') "Name",
count(*) "No of Returns",
sum (it.update_qty) "Units Returned",
sum (case when s.product_group='DP' then  it.update_qty else 0 end) as DP,
sum (case when s.product_group='MA' then  it.update_qty else 0 end) as MA,
sum (case when s.product_group='MC' then  it.update_qty else 0 end) as MC,
sum (case when s.product_group='MF' then  it.update_qty else 0 end) as MF,
sum (case when s.product_group='MI' then  it.update_qty else 0 end) as MI,
sum (case when s.product_group='MJ' then  it.update_qty else 0 end) as MJ,
sum (case when s.product_group='MK' then  it.update_qty else 0 end) as MK,
sum (case when s.product_group='ML' then  it.update_qty else 0 end) as ML,
sum (case when s.product_group='MP' then  it.update_qty else 0 end) as MP,
sum (case when s.product_group='MS' then  it.update_qty else 0 end) as MS,
sum (case when s.product_group='MT' then  it.update_qty else 0 end) as MT,
sum (case when s.product_group='MU' then  it.update_qty else 0 end) as MU,
sum (case when s.product_group='MW' then  it.update_qty else 0 end) as MW,
sum (case when s.product_group='MX' then  it.update_qty else 0 end) as MX,
sum (case when s.product_group='MZ' then  it.update_qty else 0 end) as MZ,
sum (case when s.product_group='WA' then  it.update_qty else 0 end) as WA,
sum (case when s.product_group='WC' then  it.update_qty else 0 end) as WC,
sum (case when s.product_group='WD' then  it.update_qty else 0 end) as WD,
sum (case when s.product_group='WH' then  it.update_qty else 0 end) as WH,
sum (case when s.product_group='WI' then  it.update_qty else 0 end) as WI,
sum (case when s.product_group='WJ' then  it.update_qty else 0 end) as WJ,
sum (case when s.product_group='WK' then  it.update_qty else 0 end) as WK,
sum (case when s.product_group='WL' then  it.update_qty else 0 end) as WL,
sum (case when s.product_group='WM' then  it.update_qty else 0 end) as WM,
sum (case when s.product_group='WP' then  it.update_qty else 0 end) as WP,
sum (case when s.product_group='WS' then  it.update_qty else 0 end) as WS,
sum (case when s.product_group='WV' then  it.update_qty else 0 end) as WV,
sum (case when s.product_group='WW' then  it.update_qty else 0 end) as WW,
sum (case when s.product_group='ZV' then  it.update_qty else 0 end) as ZV,
sum (case when s.product_group in ('SAMPLE', '0', null) then it.update_qty else 0 end) as "OTHER"
from inventory_transaction it,address a, sku s
where it.client_id = 'TR'
and it.code = 'Adjustment'
AND IT.DSTAMP BETWEEN TO_DATE('&&2', 'DD/MON/YYYY') AND TO_DATE('&&3', 'DD/MON/YYYY')+1
and (it.reason_id like 'T%' or it.reason_id like 'H%')
and a.client_id = 'TR'
and it.reason_id=a.address_id (+)
and (it.sku_id=s.sku_id (+)
and s.client_id = 'TR')
group by
it.reason_id,
a.name
order by
it.reason_id
/

