SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','
SET UND OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 320
SET TRIMSPOOL ON



column A format A10
column K format 99999
column L format 99999
column M format 99999
column N format 99999
column P format 99999
column Q format 99999


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



break on report

compute sum label 'Total'  of K L M N P Q on report

--spool sboo.csv


select ',,Retail,,,Mail Order' from DUAL;
select 'Date,Lines,Order Qty,Orders,Lines,Order Qty,Orders' from DUAL;


select
B,
B2-F2 K,
B3-F3 L,
B4-F4 M,
F2 N,
F3 P,
F4 Q
from (select
to_char(it.Dstamp,'YYYY MM') B,
count(*) B2,
sum(it.update_qty) B3,
count(distinct it.reference_id) B4
from inventory_&&4 it
where it.client_id='SB'
and it.code='Shipment'
and it.update_qty>0
and it.Dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1
group by
to_char(it.Dstamp,'YYYY MM')),
(select
to_char(it.Dstamp,'YYYY MM') F,
count(*) F2,
sum(it.update_qty) F3,
count(distinct it.reference_id) F4
from inventory_&&4 it
where it.client_id='SB'
and it.code='Shipment'
and it.update_qty>0
and it.Dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1
and substr(it.reference_id,1,1) in ('C','E')
group by
to_char(it.Dstamp,'YYYY MM'))
where B=F(+)
order by B
/

--spool off

