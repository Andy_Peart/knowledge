SET FEEDBACK OFF                 
set pagesize 68
set linesize 160
set verify off

clear columns
clear breaks
clear computes

column D heading 'Date' format a16
column A heading 'Rev Stream' format a20
column AA heading ' ' format a8
column F heading 'Orders' format 9999999
column B heading 'Qty Shipped' format 9999999999
column M format a4 noprint

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'Total Units Shipped between &&2 and &&3 - for client DAXBOURNE as at ' report_date skip 2

break on D dup skip 1

compute sum label 'Totals' of B on report


select
trunc(it.Dstamp) D,
--DECODE(substr(oh.user_def_type_5,1,3),'1WH','WHOLESALE','STO','RETAIL',oh.order_type) A,
DECODE(oh.order_type,'WH',DECODE(substr(oh.user_def_type_5,1,3),'STO','RETAIL','WHOLESALE'),oh.order_type) A,
DECODE(oh.customer_id,'15530','IDEAL','19762','QVC','') AA,
count(distinct oh.order_id) F,
sum(it.update_qty) as B
from inventory_transaction it, order_header oh
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='DX'
and it.code='Shipment'
and it.reference_id=oh.order_id
and oh.client_id='DX'
group by
trunc(it.Dstamp),
--DECODE(substr(oh.user_def_type_5,1,3),'1WH','WHOLESALE','STO','RETAIL',oh.order_type),
DECODE(oh.order_type,'WH',DECODE(substr(oh.user_def_type_5,1,3),'STO','RETAIL','WHOLESALE'),oh.order_type),
DECODE(oh.customer_id,'15530','IDEAL','19762','QVC','')
order by D,A
/

select
'Totals' D,
--DECODE(substr(oh.user_def_type_5,1,3),'1WH','WHOLESALE','STO','RETAIL',oh.order_type) A,
DECODE(oh.order_type,'WH',DECODE(substr(oh.user_def_type_5,1,3),'STO','RETAIL','WHOLESALE'),oh.order_type) A,
DECODE(oh.customer_id,'15530','IDEAL','19762','QVC','') AA,
count(distinct oh.order_id) F,
sum(it.update_qty) as B
from inventory_transaction it, order_header oh
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='DX'
and it.code='Shipment'
and it.reference_id=oh.order_id
and oh.client_id='DX'
group by
--DECODE(substr(oh.user_def_type_5,1,3),'1WH','WHOLESALE','STO','RETAIL',oh.order_type),
DECODE(oh.order_type,'WH',DECODE(substr(oh.user_def_type_5,1,3),'STO','RETAIL','WHOLESALE'),oh.order_type),
DECODE(oh.customer_id,'15530','IDEAL','19762','QVC','') 
order by D,A
/

select
'Number of Orders = '||count(distinct it.reference_id) B
from inventory_transaction it
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='DX'
and it.station_id not like 'Auto%'
and it.code like 'Shipment%'
/

