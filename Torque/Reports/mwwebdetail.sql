SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

select 'Date,Picker,Sku,Picked Qty,Location,Work Zone,Order ID,Batch ID,Order Type,Ugly?' from dual
union all
select "Date" || ',' || "Picker" || ',''' || "Sku" || ''',' || "Picked Qty" || ',' || "Location" || ',' || "Work Zone" || ',' || "Order ID" || ',' || "Batch ID" || ',' || "Order Type" || ',' || "Ugly?"
from
(
select to_char(it.dstamp, 'DD-MM-YYYY HH24:MI:SS') 	as "Date"
, it.user_id                                        as "Picker"
, it.sku_id                                         as "Sku"
, sum(it.update_qty)                                as "Picked Qty"
, it.from_loc_id                                    as "Location"
, l.work_zone                                       as "Work Zone"
, it.reference_id                                   as "Order ID"
, it.consignment                                    as "Batch ID"
, oh.order_type                                     as "Order Type"
, case when sku.ugly = 'Y' then 'x' else '' end     as "Ugly?"
from inventory_transaction it
inner join order_header oh on oh.client_id = it.client_id and it.reference_id = oh.order_id
inner join sku on sku.client_id = it.client_id and sku.sku_id = it.sku_id
inner join location l on l.site_id = it.site_id and l.location_id = it.from_loc_id
where it.client_id = 'MOUNTAIN'
and it.dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1
and substr(oh.work_group,2,2) = 'WW'
and it.code = 'Pick'
and it.elapsed_time > 0
group by to_char(it.dstamp, 'DD-MM-YYYY HH24:MI:SS')
, it.user_id
, it.sku_id
, it.from_loc_id
, l.work_zone
, it.reference_id
, it.consignment
, oh.order_type
, case when sku.ugly = 'Y' then 'x' else '' end
order by 1
)
/

--spool off