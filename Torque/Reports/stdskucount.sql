SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET COLSEP ' '

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 80
SET TRIMSPOOL ON

column A heading 'SKU' format a48
column B heading 'QTY' format 99999
column D NOPRINT

--Break on report

--compute count label 'TOTAL COUNT:' of A on report

select
'&&2 - SKUs with Unallocated Stock less than &&3 = ' A,
count(*) B
from (select 
sku_id ss,
sum(qty_on_hand-qty_allocated) ct
from inventory
where client_id = '&&2'
group by sku_id)
where ct<&&3
/
