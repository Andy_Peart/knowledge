SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON

column A heading 'Reference' format A12
column B heading 'SKU ID' format A18
column C heading 'Qty' format 999999
column D heading 'Reason' format A32
column E heading 'Required' format A12
column F heading 'Condition' format A12
column G heading 'Date' format A14
column H format A20



--set termout off
--spool mw_returns.csv

--Select 'MOUNTAIN Returns - &&2 to &&3' from DUAL;


select 'Reference,SKU,Qty,Reason,Required,Condition,Date,CPlus' from DUAL;
select A||','||B||','||C||','||D||','||E||','||F||','||G||','||H
from
(
select
Reference_id A,
''''||SKU_ID||'''' B,
Update_qty C,
User_def_note_1 D,
Notes E,
User_def_type_8 F,
to_char(Dstamp,'DD/MM/YYYY') G,
User_def_type_1 H
from inventory_transaction
where client_id='MOUNTAIN'
and code='Receipt'
and user_def_note_2 in ('Y','y')
and Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
order by
to_char(Dstamp,'YYYY/MM/DD'),
Reference_id
)
/


--spool off
--set termout on
