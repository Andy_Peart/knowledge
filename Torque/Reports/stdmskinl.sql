SET FEEDBACK OFF
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240
SET TRIMSPOOL ON

--SET TERMOUT OFF

break on B dup skip 1


--spool stdmskinl.csv

select 'Location,Sku,Description,Work Zone,Qty on Hand' from DUAL;



select
B||','||
C||','||
D||','||
U||','||
Q
from (
	select
	loc_id B,
	iv.sku_id C,
	iv.description D,
    ll.work_zone U,
	sum(iv.qty_on_hand) Q
	from (
		select loc_id,count(*) as CT 
		from (
			select 
			iv.sku_id as sku_id,
			ll.loc_type,
			iv.location_id as loc_id,
			sum(iv.qty_on_hand)
			from inventory iv,location ll
			where iv.client_id='&&2'
			and iv.location_id=ll.location_id
			and ll.loc_type like 'Tag-FIFO'
			group by iv.sku_id,ll.loc_type,iv.location_id
		)
		group by loc_id
	),inventory iv, location ll
	where CT>1
	and iv.client_id='&&2'
	and iv.location_id=loc_id
    and iv.location_id = ll.location_id
    and iv.site_id = ll.site_id
	group by loc_id,iv.sku_id,iv.description, ll.work_zone
	order by loc_id,iv.sku_id
)
/


--spool off
