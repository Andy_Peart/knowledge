SET FEEDBACK OFF                 
set pagesize 68
set linesize 160
set verify off
set wrap off
set newpage 0
set colsep ' '
set und on

clear columns
clear breaks
clear computes


column DD heading 'Created' format A10
column AA heading 'Age' format 999
column SS heading 'Status' format A11
column BB heading 'Order #' format A20
column CC heading 'Store' format A5
column CCC heading '' format A17
column TT heading 'Type' format A10
column OOO heading 'Ordered' format 99999
column PPP heading 'Picked' format 99999
column TTT heading 'Tasked' format 99999
column SSS heading 'Shipped' format 99999
column GG heading 'Consignment' format A12
column MM heading 'Opened' format A10



set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DAY, DD MONTH, YYYY') curdate from DUAL;
set TERMOUT ON

ttitle  LEFT 'TML Retail - Open Orders over 3 days old - to check as at    ' report_date skip 2


break on DD skip 1 

--compute sum of QQQ on CCC


select
to_char(oh.creation_date, 'DD/MM/YY') DD,
trunc(5*(trunc(sysdate)-trunc(oh.creation_date)))/7 AA,
oh.status SS,
oh.order_id BB,
oh.customer_id CC,
a.name CCC,
oh.order_type TT,
OO OOO,
PP PPP,
KK TTT,
SS SSS,
oh.consignment GG,
--to_char(a.user_def_date_1, 'DD/MM/YY') MM
CASE WHEN (trunc(a.user_def_date_1)-trunc(sysdate)>0) THEN 'Not Open Yet'
  	ELSE 'Trading'
	END MM
from order_header oh,address a,
(select
oh.order_id  ID,
nvl(sum(ol.qty_ordered),0) OO,
nvl(sum(ol.qty_picked),0) PP,
nvl(sum(ol.qty_tasked),0) KK,
nvl(sum(ol.qty_shipped),0) SS
from order_header oh,order_line ol
where oh.client_id='TR'
and ol.client_id='TR'
and oh.order_id=ol.order_id
group by
oh.order_id)
where oh.order_id=ID
and trunc(sysdate)-trunc(oh.creation_date)>5
and ((trunc(a.user_def_date_1)-trunc(sysdate)<1) or (a.user_def_date_1 is null))
and oh.status not in ('Cancelled','Hold','Shipped')
--and (oh.consignment is null and oh.order_type<>'RATIO' and oh.status<>'Released')
and oh.client_id='TR'
and a.client_id='TR'
--and a.user_def_date_1 is null
and oh.customer_id=a.address_id
order by
oh.creation_date,
oh.customer_id
/
