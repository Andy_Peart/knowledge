SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

select 'Code,Sku,Desc.,Size,CLMS Code,Product Group,Tag ID,From Location,To Location,Original Qty,Update Qty,Date,Time,User' from dual
union all
select "Code" || ',' || "Sku" || ',' || "Description" || ',' || "Size" || ',' || "CLMS Code" || ',' || "Product Group"
|| ',' || "Tag ID" || ',' || "From Location" || ',' || "To Location" || ',' || "Original Qty" 
|| ',' || "Update Qty" || ',' || "Date" || ',' || "Time" || ',' || "User"
from 
(
select it.code				        as "Code"                   
, it.sku_id							as "Sku"
, sku.description					as "Description"
, sku.sku_size						as "Size"
, sku.user_def_type_4				as "CLMS Code"
, sku.product_group                 as "Product Group"
, it.tag_id							as "Tag ID"
, it.from_loc_id					as "From Location"
, it.to_loc_id						as "To Location"
, it.original_qty					as "Original Qty"
, it.update_qty						as "Update Qty"
, to_char(it.dstamp, 'DD-MON-YYYY')	as "Date"
, to_char(it.dstamp, 'HH:MI:SS')	as "Time"
, it.user_id						as "User"
from inventory_transaction it 
left join sku on sku.sku_id = it.sku_id and sku.client_id = it.client_id
where it.client_id='&&2'
and it.code='Stock Check'
and it.dstamp>to_date('&&3','DD-MON-YYYY')
and it.dstamp<to_date('&&4','DD-MON-YYYY')+1
and substr(it.from_loc_id,1,2) between '&&5' and '&&6'
order by to_char(it.dstamp, 'DD-MON-YYYY')
, it.user_id
)
/

