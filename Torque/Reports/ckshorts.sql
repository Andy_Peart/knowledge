SET FEEDBACK OFF
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
set colsep ','
set heading off
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240
SET TRIMSPOOL ON


--spool OrderShorts.csv

select 'Order ID,Customer Detail,SKU ID,Description,Size,Colour,Ordered,Available' from dual;

select
oh.order_id||',"'||
customer_id ||','||
NAME ||','||   
ADDRESS1 ||','||
ADDRESS2 ||','||
TOWN ||','||
COUNTY ||','||
POSTCODE ||','||
COUNTRY ||'",'||
--oh.status  ||','||
ol.sku_id  ||''','||
sku.description  ||','||
sku.colour  ||','||
sku.sku_size  ||','||
nvl(ol.qty_ordered,0) ||','||
nvl(ol.qty_tasked,0)
from order_header oh,order_line ol,sku
where oh.Client_ID='CK'
and oh.status in ('Allocated','Released')
--and oh.order_date>sysdate-2
and oh.order_id=ol.order_id
and ol.Client_ID='CK'
and ol.qty_ordered>0
and nvl(ol.qty_tasked,0)<ol.qty_ordered
and sku.Client_ID='CK'
and ol.sku_id=sku.sku_id
order by
oh.order_id,
ol.sku_id
/

--spool off
