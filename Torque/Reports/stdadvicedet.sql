SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Pre Advice,Sku,Qty Due,Qty Received,Location(Qty),Difference' from dual
union all
select "Pre Advice" || ',' || "Sku" || ',' || "Qty Due" || ',' || "Qty Received" || ',' || "Location(Qty)" || ',' || "Difference"
from
(
with t1 as (
select pah.pre_advice_id    as "Id"
, pah.client_id             as "Cl"
, pal.sku_id                as "Sku"
, sum(pal.qty_due)          as "Due"
, sum(pal.qty_received)     as "Rec"
from pre_advice_header pah
inner join pre_advice_line pal on pal.client_id = pah.client_id and pah.pre_advice_id = pal.pre_advice_id
where pah.client_id = '&&2'
and pah.pre_advice_id = '&&3'
group by pah.pre_advice_id
, pal.sku_id
, pah.client_id
), t2 as (
select sku_id           as "Sku"
, location_id           as "Loc"
, client_id             as "Cl"
, sum(qty_on_hand)      as "Qty"
, receipt_id            as "Id"
from inventory
where client_id = '&&2'
and receipt_id = '&&3'
group by sku_id
, location_id
, client_id
, receipt_id
)
select t1."Id"                                                  as "Pre Advice"
, t1."Sku"                                                      as "Sku"
, t1."Due"                                                      as "Qty Due"
, nvl(t1."Rec",0)                                               as "Qty Received"
, case  when t2."Loc" is null then '-' 
        else nvl(t2."Loc",'-') || '(' || t2."Qty" || ')' end    as "Location(Qty)"
, t1."Due" - nvl(t1."Rec",0)                                    as "Difference"
from t1
left join t2 on t2."Cl" = t1."Cl" and t2."Id" = t1."Id" and t2."Sku" = t1."Sku"
order by 2,5
)
/
--spool off