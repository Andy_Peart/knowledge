SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 132
SET TRIMSPOOL ON

column A heading 'SKU' format a20
column B heading 'Qty' format 99999
column F heading 'Location' format A14


select 'SKU,Qty,Location,' from DUAL;


select 
i.sku_id A,
i.qty_on_hand B,
i.location_id F,
' '
from inventory i,
(select inv.sku_id C,
sum(qty_on_hand) B,
sum(qty_allocated) D
from inventory inv
where client_id = '&&5'
group by inv.sku_id)
where client_id = '&&5'
and B < '&&2'
and D=0
and i.sku_id = C
and i.location_id<>'DESPATCHPR'
and i.location_id<>'CONTAINER'
and i.location_id not like 'QC%'
and i.location_id>'&&3'
and i.location_id<'&&4'
order by 1
/
