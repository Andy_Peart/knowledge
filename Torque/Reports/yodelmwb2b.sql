SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ',' 


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 800
SET TRIMSPOOL ON
 
column XX NOPRINT

--spool ukmail.csv

select
  'TORQUERETAILSERVICESLTD001' as "b2b",
  trim(ORDER_ID),
  ORDER_TYPE,
  PRIORITY,
  WORK_GROUP,
  CONSIGNMENT,
  TO_CHAR(sysdate, 'dd-mm-yyyy') as "Collection Date",
  DISPATCH_METHOD,
  CONTACT,
  ADDRESS1,
  ADDRESS2,
  town,
  county,
  postcode,
  country,
  contact_phone,
  SUBSTR(INSTRUCTIONS,1,35),
  '1' as "no of cartons",
  '1' as "Weight",
  '01' as "local prod code for b2",
  '02' as "local service code 24hrs"
from order_header
where client_id = 'MOUNTAIN'
and ORDER_ID = '&2'
order by order_id
/


--spool off


