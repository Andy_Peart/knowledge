SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON

--break on report

--compute sum label 'Total' of Q1 Q2 Q3 Q4 QQ1 QQ2 QQ3 QQ4 QQ41 QQ42 QQ43 QQ5 QQ6 QQ7 QQ8 QQ4 QQ81 QQ82 QQ83 QQQ on report

--spool CFSPbrief.csv


Select 'PROMINENT CFSP BRIEF REPORT from &&3 to &&4' from DUAL;

select 'Date,Code,Client,Sku,Site,Update Qty,User Def Type 4,User Def Type 5,User Def Type 7,User Def Type 8' from DUAL;


select
trunc(Dstamp)||','||
code||','||
client_id||','||
sku_id||','||
site_id||','||
update_qty||','||
User_Def_Type_4||','||
User_Def_Type_5||','||
User_Def_Type_7||','||
User_Def_Type_8
from inventory_transaction 
where client_id in ('TR','T','PR','PS')
and code in ('Receipt','Shipment','Stock Check','Adjustment','Receipt Reverse')
and user_def_type_7='&&2'
and Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
order by
1
/


--spool off


