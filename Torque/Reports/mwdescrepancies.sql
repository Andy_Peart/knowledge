SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON

set pagesize 66
set newpage 0
set linesize 100
set heading on

clear columns
clear breaks
clear computes


TTitle 'Client &&2 - Task Descrepancies from &&3 to &&4' skip 2

column A heading 'Date' format A20
column X heading 'Work Group' format A12
column B heading 'Not Enough|Total Units' format 999999999

break on A skip 1

--compute sum label 'Totals' of A1 B1 on report

select
to_char(dstamp,'dd/mm/yyyy') A,
DECODE(work_group,'WWW','WWW','others') X,
sum(old_qty_to_move-qty_to_move) B
from move_descrepancy
where client_id='&&2'
and reason_code='EX_NE'
and Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
group by
to_char(dstamp,'yyyy/mm/dd'),
to_char(dstamp,'dd/mm/yyyy'),
DECODE(work_group,'WWW','WWW','others')
order by
to_char(dstamp,'yyyy/mm/dd'),
DECODE(work_group,'WWW','WWW','others')
/


column B heading 'Over Picked|Total Units' format 999999999

select
to_char(dstamp,'dd/mm/yyyy') A,
DECODE(work_group,'WWW','WWW','others') X,
sum(qty_to_move-old_qty_to_move) B
from move_descrepancy
where client_id='&&2'
and reason_code='EX_OR'
and Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
group by
to_char(dstamp,'yyyy/mm/dd'),
to_char(dstamp,'dd/mm/yyyy'),
DECODE(work_group,'WWW','WWW','others')
order by
to_char(dstamp,'yyyy/mm/dd'),
DECODE(work_group,'WWW','WWW','others')
/
