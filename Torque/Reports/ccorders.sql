SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON


--spool ccorders.csv

select'Creation Date,Order Type,Earliest,Latest,Total Orders' from DUAL;

select
trunc(creation_date) A,
Order_type B,
to_char(min(creation_date),'HH:MI:SS') C,
to_char(mAX(creation_date),'HH:MI:SS') D,
count(*) E
from order_header
where client_id='CC'
and creation_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by
trunc(creation_date),
Order_type
order by A
/

--spool off
