SET FEEDBACK OFF             
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

column A heading 'SKU' format A18
column B heading 'Ordered' format 99999999
column C heading 'Pickable' format 99999999
column D heading 'Picked' format 99999999
column E heading 'Pick Shortages' format 99999999




 
set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DAY, DD MONTH, YYYY') curdate from DUAL;
set TERMOUT ON





select 'SUMMARY of UNITS for Period &&2 to &&3' from DUAL;

select 'SKU,Ordered,Pickable,Picked,Shortages' from DUAL;

break on report skip 2
compute sum label 'Total' of B C D E on report


select
''''||AAA||'''' A,
BBB B,
BBB C,
DDD D,
DDD-BBB E
from
(select
DD AAA,
sum(DDD) BBB,
sum(nvl(JJJ,0)) CCC,
sum(nvl(KKK,0)) DDD
from
(select
ol.sku_id DD,
sum(ol.qty_ordered) DDD
from order_header oh,order_line ol
where oh.client_id='TR'
and oh.creation_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and oh.status in ('Shipped')
and oh.client_id=ol.client_id
and oh.order_id=ol.order_id
group by ol.sku_id),
(select
it.Sku_id JJ,
sum(update_qty) JJJ
from inventory_transaction it
where code='Allocate'
and it.client_id='TR'
and reference_id in
(select
order_id
from order_header oh
where oh.client_id='TR'
and oh.creation_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and oh.status in ('Shipped'))
and from_loc_id<>'CONTAINER'
group by it.sku_id),
(select
it.Sku_id KK,
sum(update_qty) KKK
from inventory_transaction it
where code='Pick'
and it.client_id='TR'
and reference_id in
(select
order_id
from order_header oh
where oh.client_id='TR'
and oh.creation_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and oh.status in ('Shipped'))
and from_loc_id<>'CONTAINER'
group by it.sku_id)
where DD=JJ (+)
and DD=KK (+)
group by DD
order by DD)
where CCC>BBB 
and BBB>DDD
union
select
''''||AAA||'''' A,
BBB B,
CCC C,
DDD D,
DDD-BBB E
from
(select
DD AAA,
sum(DDD) BBB,
sum(nvl(JJJ,0)) CCC,
sum(nvl(KKK,0)) DDD
from
(select
ol.sku_id DD,
sum(ol.qty_ordered) DDD
from order_header oh,order_line ol
where oh.client_id='TR'
and oh.creation_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and oh.status in ('Shipped')
and oh.client_id=ol.client_id
and oh.order_id=ol.order_id
group by ol.sku_id),
(select
it.Sku_id JJ,
sum(update_qty) JJJ
from inventory_transaction it
where code='Allocate'
and it.client_id='TR'
and reference_id in
(select
order_id
from order_header oh
where oh.client_id='TR'
and oh.creation_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and oh.status in ('Shipped'))
and from_loc_id<>'CONTAINER'
group by it.sku_id),
(select
it.Sku_id KK,
sum(update_qty) KKK
from inventory_transaction it
where code='Pick'
and it.client_id='TR'
and reference_id in
(select
order_id
from order_header oh
where oh.client_id='TR'
and oh.creation_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and oh.status in ('Shipped'))
and from_loc_id<>'CONTAINER'
group by it.sku_id)
where DD=JJ (+)
and DD=KK (+)
group by DD
order by DD)
--where CCC<=BBB 
--and BBB>DDD or BBB>CCC
/
