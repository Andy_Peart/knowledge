set feedback on


insert into timex_snapshot(
the_date,the_sku,the_qty,the_tag,the_receipt_date)
select
trunc(sysdate),
sku_id,
sum(qty_on_hand),
tag_id,
trunc(receipt_Dstamp)
from inventory
where client_id='TIMEX'
group by
sku_id,
tag_id,
trunc(receipt_Dstamp)
/

--rollback;
commit;
