SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep '|'


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9999
SET TRIMSPOOL ON

--spool returns.txt

select 'ITL|E|Receipt|+|0|+|'||
nvl(sb.qty,0)||'|'||
to_char(sysdate,'DD-MM-YYYY')||'|'||
'SB'||'|'||
sb.skucode||'|||||'||
sb.orderno||'|'||
ol.line_id||'|'||
'GOOD'||'||"'||
sb.reason||'"|'||
'|||||||||ELITE||||M|||||||'||
sb.orderno||'||'||
sb.collection||'|'||
'||||||||||||||||||||||||||||||||||||||||||||||||||||||||||'
from sb_returns sb,order_line ol
where ol.client_id='SB'
and ol.order_id=sb.orderno
and ol.sku_id=sb.skucode
/
--spool off

delete from sb_returns;
commit;

