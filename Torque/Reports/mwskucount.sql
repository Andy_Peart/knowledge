set pagesize 68
set linesize 120
set verify off

ttitle center "Mountain Warehouse - Unallocated SKU's less than '&&2'." skip 3

column A heading 'SKU' format a13
column B heading 'QTY' format 99999
column D NOPRINT

Break on report

compute count label 'TOTAL COUNT:' of A on report

select sku_id A,
qty_on_hand B,
qty_allocated D
from inventory
where client_id = 'MOUNTAIN'
and qty_on_hand < ('&&2')
and qty_allocated=0
/
