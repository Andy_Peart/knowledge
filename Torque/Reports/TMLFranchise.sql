SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET WRAP OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
set linesize 200

--ttitle 'Mountain Warehouse Overpicks for Period &&2 to &&3' skip 2

column A heading 'Pick ID' format A10
column B1 heading 'Season' format A12
column B2 heading 'Style No' format A12
column C heading 'Style Qual' format A12
column D heading 'Fit' format A10
column E heading 'Colour' format A20
column F heading 'Sales Id' format A14
column G heading 'Item' format A8
column H heading 'Delivery' format A8
column J heading 'Desp Id' format A8
column K heading 'Size' format A8
column L heading 'Qty Ordered' format 999999
column M heading 'Qty Picked' format 999999
column N heading 'Discrepancy' format 999999
column P heading 'Barcode' format A20

Select 'Pick ID,Season,Style No,Style Qual,Fit,Colour,Sales Id,Item,Delivery,Desp Id,Size,Qty Ordered,Qty Picked,Discrepancy,Barcode' from DUAL;


select 
ol.user_def_type_1 A,
sku.user_def_type_2 B1,
sku.user_def_type_1 B2,
sku.user_def_type_4 C,
ol.user_def_type_4 D,
sku.colour E,
ol.user_def_type_2 F,
ol.user_def_type_3 G,
oh.customer_id H,
oh.order_id J,
''''||ol.user_def_type_5||'''' K,
nvl(ol.qty_ordered,0) L,
nvl(ol.qty_picked,0) M,
nvl(ol.qty_ordered,0)-nvl(ol.qty_picked,0) N,
''''||ol.sku_id||'''' P,
' '
from order_header oh,order_line ol,sku 
where oh.client_id = 'TR'
and ol.client_id=oh.Client_id 
and oh.order_id = '&&2'
and ol.order_id = oh.order_id
and ol.sku_id=sku.sku_id
and sku.client_id=oh.Client_id
order by
A
/
