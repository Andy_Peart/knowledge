SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9999
SET TRIMSPOOL ON

--spool huwshipments.csv

select 'HUW shipments from &&2 to &&3' from DUAL;

select 'Date,Order ID,Product,Line No,Description,Qty Ordered,Qty Shipped' from DUAL;

select
trunc(i.Dstamp) AA,
i.reference_id BB,
i.sku_id CC,
i.line_id KK,
sku.description DD,
ol.qty_ordered OO,
sum(i.update_qty) QQ
from inventory_transaction i,sku,order_line ol
where i.client_id='HU'
and i.code = 'Shipment'
and i.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and ol.client_id='HU'
and i.reference_id=ol.order_id
and i.sku_id=ol.sku_id
and i.line_id=ol.line_id
and sku.client_id='HU'
and i.sku_id=sku.sku_id
group by
trunc(i.Dstamp),
i.reference_id,
i.sku_id,
i.line_id,
sku.description,
ol.qty_ordered
order by
trunc(i.Dstamp),
i.reference_id,
i.line_id
/


--spool off
