SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON


--spool trespass1.csv

select 'SKU,Location,User Def 7,Stock,Cartons' from DUAL;

select
replace(D,' ','')
from (select
A||','||
A2||','||
B||','||
C||','||
CASE
WHEN B2='BLANK' THEN ''
ELSE to_char(C/B2,'9999.00')
END D
from (select
i.sku_id A,
i.location_id A2,
i.user_def_type_7 B,
case
when upper(i.user_def_type_7)=lower(i.user_def_type_7)
then i.user_def_type_7
ELSE 'BLANK'
END B2,
sum(i.qty_on_hand) C
from inventory i,sku
where i.client_id='TS'
and i.location_id not in ('TSIN','TSOUT','SUSPENSE')
and sku.client_id=i.client_id
and sku.sku_id=i.sku_id
group by
i.sku_id,
i.location_id,
i.user_def_type_7)
order by A)
/

--spool off
