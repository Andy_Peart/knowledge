SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','
SET WRAP OFF


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 900

column A heading 'Order' format A8
column B heading 'Receipt Date' format A32
column C heading 'Qty' format 999999999
column M format A6 NOPRINT


select 'MW23 Last weeks Web Receipts,,Report Run at : '||to_char(SYSDATE, 'DD/MM/YYYY  HH:MI:SS') from DUAL;
Select 'Date,Web Total' from DUAL;

break on report

compute sum label 'Weekly Total' of C on report


select
'"'||to_char(it.Dstamp,'Day,DD Month,YYYY')||'"' B,
to_char(it.Dstamp,'YYMMDD') M,
sum(it.update_qty) C
from inventory_transaction it
where it.client_id='MOUNTAIN'
and it.code='Receipt'
and it.Dstamp>sysdate-8
and substr(it.reference_id,1,1) in ('0','1','2','3','4','5','6','7','8','9')
group by
to_char(it.Dstamp,'YYMMDD'),
'"'||to_char(it.Dstamp,'Day,DD Month,YYYY')||'"'
order by
M
/

