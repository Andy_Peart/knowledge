SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 200
SET TRIMSPOOL ON

column A heading 'User ID' format a24
column B heading 'Name' format a30
column C1 heading 'C Picks' format 999999
column C2 heading 'T Picks' format 999999
column D1 heading 'Receipts' format 999999
column D2 heading 'Receipts' format 999999
column D3 heading 'Receipts' format 999999
column E heading 'Relocates' format 999999
column E2 heading 'Relocates' format 999999
column F heading 'Returns' format 999999
column G heading 'IDTs' format 999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


break on report

compute sum label ',Totals' of C1 C2 D1 D2 D3 E E2 on report

--spool productivity.csv

select 'Productivity by User ID from &&2 to &&3 - for client  SB' from DUAL;

select 'Date,Log on,Name,Picks(C/E),Picks(T),Receipts(P),Receipts(S),Receipts(C/E/U),Relocates,Relocated Units' from DUAL;


select
JJ A,
BB B,
nvl(JA1,0) C1,
nvl(JA2,0) C2,
nvl(JB1,0) D1,
nvl(JB2,0) D2,
nvl(JB3,0) D3,
nvl(JC,0) E,
nvl(JC2,0) E2
from (select
distinct
trunc(it.Dstamp)||','||it.user_id JJ,
au.name BB
from inventory_transaction it,application_user au
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='SB'
and it.user_id=au.user_id),
(select
trunc(Dstamp)||','||user_id J11,
sum(update_qty) JA1
from inventory_transaction
where Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and client_id='SB'
and (reference_id like 'C%' or reference_id like 'E%')
and update_qty<>0
and ((code='Pick' and to_loc_id='CONTAINER')
or (code='Pick' and to_loc_id<>'CONTAINER' and from_loc_id<>'CONTAINER'))
group by trunc(Dstamp),user_id),
(select
trunc(Dstamp)||','||user_id J12,
sum(update_qty) JA2
from inventory_transaction
where Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and client_id='SB'
and reference_id not like 'C%'
and reference_id not like 'E%'
and update_qty<>0
and ((code='Pick' and to_loc_id='CONTAINER')
or (code='Pick' and to_loc_id<>'CONTAINER' and from_loc_id<>'CONTAINER'))
group by trunc(Dstamp),user_id),
(select
trunc(Dstamp)||','||user_id J21,
sum(update_qty) JB1
from inventory_transaction
where Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and client_id='SB'
and update_qty<>0
and code='Receipt'
and reference_id like 'P%'
--and elapsed_time>0
and station_id like 'RDT%'
group by trunc(Dstamp),user_id),
(select
trunc(Dstamp)||','||user_id J22,
sum(update_qty) JB2
from inventory_transaction
where Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and client_id='SB'
and update_qty<>0
and code='Receipt'
and reference_id not like 'P%'
and reference_id not like 'C%'
and reference_id not like 'E%'
and reference_id not like 'U%'
--and elapsed_time>0
and station_id like 'RDT%'
group by trunc(Dstamp),user_id),
(select
trunc(Dstamp)||','||user_id J23,
sum(update_qty) JB3
from inventory_transaction
where Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and client_id='SB'
and update_qty<>0
and code='Receipt'
and (reference_id like 'C%' or reference_id like 'E%' or reference_id like 'U%')
--and elapsed_time>0
and station_id like 'RDT%'
group by trunc(Dstamp),user_id),
(select
trunc(Dstamp)||','||user_id J3,
count(*) JC,
sum(update_qty) JC2
from inventory_transaction
where Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and client_id='SB'
and update_qty<>0
and code='Relocate'
--and elapsed_time>0
and station_id like 'RDT%'
group by trunc(Dstamp),user_id)
where JJ=J11 (+)
and JJ=J12 (+)
and JJ=J21 (+)
and JJ=J22 (+)
and JJ=J23 (+)
and JJ=J3 (+)
and nvl(JA1,0)+nvl(JA2,0)+nvl(JB1,0)+nvl(JB2,0)+nvl(JB3,0)+nvl(JC,0)>0
order by JJ
/

--spool off
