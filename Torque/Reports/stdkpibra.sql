SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 1500
SET TRIMSPOOL ON
SET HEADING ON

select 'CLIENT_ID,DATE,USER_ID,INTAKE_MINS,INTAKE_QTY,INTAKE_SORT_MINS,INTAKE_SORT_QTY,RET_WEB_MINS,RET_WEB_QTY,RET_WH_MINS,RET_WH_QTY,PACK_MINS,PACK_QTY,PICK_WEB_MINS,PICK_WEB_QTY,PICK_WHO_MINS,PICK_WHO_QTY,PICK_RET_MINS,PICK_RET_QTY,RELOC_MINS,RELOC_QTY,REWORK_MINS,REWORK_QTY,KIMBEL_MINS,KIMBEL_QTY,DESP_MINS,ADMIN_MINS,STOCK_MINS,STOCK_LOCS,STOCK_QTY,HOUSEK_MINS,OTHER_MINS,TRANSF_MINS,TRANSF_QTY,STOCK_CONT_MINS,STOCK_CONT_LOCS,STOCK_CONT_QTYS,PACK_UWEAR_MINS,PACK_UWEAR_QTYS,ULABEL_MINS,ULABEL_QTY,QC_MINS,QC_QTY,SH_GI_MINS,SH_GI_QTY,SH_RET_MINS,SH_RET_QTY,DELIVER MINS,DELIVER QTY,CONDENSE_MINS,CONDENSE_QTY,SORT_MINS,SORT_QTY,PICKWEBLIST_MINS,PICKWEBLIST_QTY,PACKWEBCLUSTER_MINS,PACKWEBCLUSTER_QTY,PACK_MINS,PACK_QTY,PICK_WEB_CLUSTER_MINS,PICK_WEB_CLUSTER_UNITS,PICK_WEB_TROLLEY_MINS,PICK_WEB_TROLLEY_QTY,RET_REWORK_MINS,RET_REWORK_QTY,STOCK_CHECK_ORDER_MIMS,STOCK_CHECK_ORDER_QTY' from dual;
with 
job_starts as (
                select it.client_id, it.code, it.user_id, it.job_id, it.job_unit, it.dstamp, notes,
                rank() over(partition by it.user_id order by it.Dstamp) as rank1
                from inventory_transaction it
                where it.client_id = 'CK'
                and it.code = 'Job Start'
                and it.Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
),
job_ends as (
                select it.client_id, it.code, it.user_id, it.job_id,   it.job_unit, it.dstamp, notes,
                rank() over(partition by it.user_id order by it.Dstamp) as rank1, 
                /* decrease qty by 1 typed on PCs as PCs don't allow to type 0 */
                case when it.update_qty = 1 and (it.station_id like 'DT%' or it.station_id like 'PC%') then 0 else  it.update_qty end tasks, 
                case when it.update_qty = 1 and (it.station_id like 'DT%' or it.station_id like 'PC%') then 0 else  it.update_qty end units  
                from inventory_transaction it
                where it.client_id = 'CK'
                and it.code = 'Job End'
                and it.Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
),
job_regs as (
  select s.client_id, trunc(s.dstamp) txn_date,   s.user_id, 
  to_char(s.dstamp,'HH24:MI') time_start, to_char(e.dstamp,'HH24:MI') time_end, 
  ((to_number(substr(to_char(e.dstamp,'HH24:MI'),1,2))*60)+to_number(substr(to_char(e.dstamp,'HH24:MI'),4,2)))-
  ((to_number(substr(to_char(s.dstamp,'HH24:MI'),1,2))*60)+to_number(substr(to_char(s.dstamp,'HH24:MI'),4,2))) minutes,
  s.job_id, e.tasks, e.units, 'NON AUTO' as job_type
  from job_starts s inner join job_ends e on s.rank1 = e.rank1 and s.user_id = e.user_id and s.job_id = e.job_id
  where s.job_unit <> 'AUTO' and e.job_unit <> 'AUTO'
  union all
  select it.client_id, trunc(s.dstamp) txn_date, s.user_id, 
  to_char(s.dstamp,'HH24:MI') time_start, to_char(e.dstamp,'HH24:MI') time_end, 
  ((to_number(substr(to_char(e.dstamp,'HH24:MI'),1,2))*60)+to_number(substr(to_char(e.dstamp,'HH24:MI'),4,2)))-
  ((to_number(substr(to_char(s.dstamp,'HH24:MI'),1,2))*60)+to_number(substr(to_char(s.dstamp,'HH24:MI'),4,2))) minutes,
  s.job_id, count(*) tasks, sum(it.update_qty) units, 'AUTO' as job_type
  from job_starts s, job_ends e, inventory_transaction it
  where it.Dstamp between s.dstamp and e.dstamp+0.00007
  and it.client_id='&&2' and it.update_qty<>0 and NVL(it.from_loc_id,' ')<>'CONTAINER'
  and it.code not in ('PreAdv Line')
  and it.user_id=s.user_id
  and s.rank1=e.rank1
  and s.user_id=e.user_id
  and s.job_id=e.job_id
  and s.job_unit = 'AUTO' and e.job_unit = 'AUTO'
  group by it.client_id, s.dstamp,e.dstamp, s.user_id, s.job_id, s.notes
),
all_users as (
  select distinct client_id, to_char(dstamp,'DD-MON-YYYY') as date_, user_id
  from inventory_transaction
  where client_id = 'CK'
  and user_id not in ('Mvtcdae','Allocdae')
  and dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
),
kpi_intake as (
  select client_id, txn_date, user_id, sum(units) qty, sum(minutes) mins
  from job_regs
  where job_id = 'BRGI'
  group by client_id, txn_date, user_id
),
kpi_ulabel as (
  select client_id, txn_date, user_id, sum(units) qty, sum(minutes) mins
  from job_regs
  where job_id = 'BRULABEL'
  group by client_id, txn_date, user_id
),
kpi_qc as (
  select client_id, txn_date, user_id, sum(units) qty, sum(minutes) mins
  from job_regs
  where job_id = 'BRQC'
  group by client_id, txn_date, user_id
),
kpi_stock_control as (
  select client_id, txn_date, user_id, sum(units) qty, sum(minutes) mins
  from job_regs
  where job_id = 'BRSC'
  group by client_id, txn_date, user_id
),
kpi_pack_uwear as (
  select client_id, txn_date, user_id, sum(units) qty, sum(minutes) mins
  from job_regs
  where job_id = 'BRUPACK'
  group by client_id, txn_date, user_id
),
kpi_intake_sort as (
  select client_id, txn_date, user_id, sum(units) qty, sum(minutes) mins
  from job_regs
  where job_id = 'BRGISORT'
  group by client_id, txn_date, user_id
),
kpi_ret_web as  (
  select client_id, txn_date, user_id, sum(units) qty, sum(minutes) mins
  from job_regs
  where job_id = 'BRRETWEB'
  group by client_id, txn_date, user_id
),
kpi_ret_wh as (
  select client_id, txn_date, user_id, sum(units) qty, sum(minutes) mins
  from job_regs
  where job_id = 'BRRET'
  group by client_id, txn_date, user_id
),
kpi_pack as (
  select client_id, txn_date, user_id, sum(units) qty, sum(minutes) mins
  from job_regs
  where job_id = 'BRPACKWEB'
  group by client_id, txn_date, user_id
),
kpi_pick_web as (
  select client_id, txn_date, user_id, sum(units) qty, sum(minutes) mins
  from job_regs
  where job_id = 'BRPICKWEB'
  group by client_id, txn_date, user_id
),
kpi_pick_ret as (
  select client_id, txn_date, user_id, sum(units) qty, sum(minutes) mins
  from job_regs
  where job_id = 'BRPICKRET'
  group by client_id, txn_date, user_id
),
kpi_pick_who as (
  select client_id, txn_date, user_id, sum(units) qty, sum(minutes) mins
  from job_regs
  where job_id = 'BRPICKWHO'
  group by client_id, txn_date, user_id
),
kpi_reloc as (
  select client_id, txn_date, user_id, sum(units) qty, sum(minutes) mins
  from job_regs
  where job_id = 'BRRELOC'
  group by client_id, txn_date, user_id
),
kpi_rework as (
  select client_id, txn_date, user_id, sum(units) qty, sum(minutes) mins
  from job_regs
  where job_id = 'BRREWORK'
  group by client_id, txn_date, user_id
),
kpi_kimbel as (
  select client_id, txn_date, user_id, sum(units) qty, sum(minutes) mins
  from job_regs
  where job_id in ('KIMBEL', 'LABEL')
  group by client_id, txn_date, user_id
),
kpi_desp as (
  select client_id, txn_date, user_id, sum(units) qty, sum(minutes) mins
  from job_regs
  where job_id = 'BRDESP'
  group by client_id, txn_date, user_id
),
kpi_admin as (
  select client_id, txn_date, user_id, sum(units) qty, sum(minutes) mins
  from job_regs
  where job_id = 'BRADMIN'
  group by client_id, txn_date, user_id
),
kpi_stock as (
  select client_id, txn_date, user_id, sum(units) qty, sum(minutes) mins
  from job_regs
  where job_id = 'BRSTOCKCHK'
  group by client_id, txn_date, user_id
),
kpi_housek as (
  select client_id, txn_date, user_id, sum(units) qty, sum(minutes) mins
  from job_regs
  where job_id = 'BRHOUSEK'
  group by client_id, txn_date, user_id
),
kpi_transf as (
  select client_id, txn_date, user_id, sum(units) qty, sum(minutes) mins
  from job_regs
  where job_id = 'BRTF'
  group by client_id, txn_date, user_id
),
kpi_other as (
  select client_id, txn_date, user_id, sum(units) qty, sum(minutes) mins
  from job_regs
  where job_id not in ('BRSC','BRUPACK','BRGI','BRULABEL','BRGISORT','BRRETWEB','BRRET','BRPACKWEB','BRPICKWEB','BRPICKRET','BRPICKWHO','BRRELOC','BRREWORK','BRKIMBEL','BRLABEL','BRDESP','BRADMIN','BRSTOCKCHK','BRHOUSEK', 'BRTF','BRQC', 'BRSHGI', 'BRSHRET', 'BRDELIVER','BRCONDENSE','BRSORT','BRPACKWEBCLU','BRPICKWEBLIS','BRPACK','BRPICKWEBCLU','BRPICKWEBTRO','BRRETREWORK','BRSTOCKCHKO')
  group by client_id, txn_date, user_id
),
kpi_sh_gi as (
  select client_id, txn_date, user_id, sum(units) qty, sum(minutes) mins
  from job_regs
  where job_id = 'BRSHGI'
  group by client_id, txn_date, user_id
),
kpi_sh_ret as (
  select client_id, txn_date, user_id, sum(units) qty, sum(minutes) mins
  from job_regs
  where job_id = 'BRSHRET'
  group by client_id, txn_date, user_id
),
kpi_deliver as (
  select client_id, txn_date, user_id, sum(units) qty, sum(minutes) mins
  from job_regs
  where job_id = 'BRDELIVER'
  group by client_id, txn_date, user_id
),
kpi_condense as (
  select client_id, txn_date, user_id, sum(units) qty, sum(minutes) mins
  from job_regs
  where job_id = 'BRCONDENSE'
  group by client_id, txn_date, user_id
),
kpi_sort as (
  select client_id, txn_date, user_id, sum(units) qty, sum(minutes) mins
  from job_regs
  where job_id = 'BRSORT'
  group by client_id, txn_date, user_id
),
kpi_pick_web_lis as (
  select client_id, txn_date, user_id, sum(units) qty, sum(minutes) mins
  from job_regs
  where job_id = 'BRPICKWEBLIS'
  group by client_id, txn_date, user_id
),
kpi_pack_web_clu as (
  select client_id, txn_date, user_id, sum(units) qty, sum(minutes) mins
  from job_regs
  where job_id = 'BRPACKWEBCLU'
  group by client_id, txn_date, user_id
),
kpi_packx as (
  select client_id, txn_date, user_id, sum(units) qty, sum(minutes) mins
  from job_regs
  where job_id = 'BRPACK'
  group by client_id, txn_date, user_id
),
kpi_pick_web_clu as (
  select client_id, txn_date, user_id, sum(units) qty, sum(minutes) mins
  from job_regs
  where job_id = 'BRPICKWEBCLU'
  group by client_id, txn_date, user_id
),
kpi_pick_web_tro as (
  select client_id, txn_date, user_id, sum(units) qty, sum(minutes) mins
  from job_regs
  where job_id = 'BRPICKWEBTRO'
  group by client_id, txn_date, user_id
),
kpi_ret_rework as (
  select client_id, txn_date, user_id, sum(units) qty, sum(minutes) mins
  from job_regs
  where job_id = 'BRRETREWORK'
  group by client_id, txn_date, user_id
),
kpi_stock_checko as (
  select client_id, txn_date, user_id, sum(units) qty, sum(minutes) mins
  from job_regs
  where job_id = 'BRSTOCKCHKO'
  group by client_id, txn_date, user_id
)
SELECT A.client_id, A.date_, A.user_id, 
NVL(KINT.MINS,0) intake_mins,NVL(KINT.QTY,0) intake_qty, 
NVL(KINTSORT.MINS,0) intake_sort_mins,NVL(KINTSORT.QTY,0) intake_sort_qty, 
NVL(KRETWB.MINS,0) ret_web_mins, NVL(KRETWB.QTY,0) ret_web_qty, 
NVL(KRETWH.MINS,0) ret_wh_mins, NVL(KRETWH.QTY,0) ret_wh_qty, 
NVL(KPACK.MINS,0) pack_mins, NVL(KPACK.QTY,0) pack_qty, 
NVL(KPKWB.MINS,0) pick_web_mins, NVL(KPKWB.QTY,0) pick_web_qty, 
NVL(KPKWH.MINS,0) pick_who_mins, NVL(KPKWH.QTY,0) pick_who_qty, 
NVL(KPKRET.MINS,0) pick_ret_mins, NVL(KPKRET.QTY,0) pick_ret_qty, 
NVL(KREL.MINS,0) reloc_mins, NVL(KREL.QTY,0) reloc_qty, 
NVL(KREW.MINS,0) rework_mins, NVL(KREW.QTY,0) rework_qty, 
NVL(KKIM.MINS,0) kimbel_mins, NVL(KKIM.QTY,0) kimbel_qty, 
NVL(KDES.MINS,0) desp_mins,
NVL(KADM.MINS,0) admin_mins,
NVL(KSTO.MINS,0) stock_mins, 0 stock_locs, NVL(KSTO.QTY,0) stock_qty, 
NVL(KHOU.MINS,0) housek_mins,
NVL(KOTH.MINS,0) other_mins,
NVL(KTRA.MINS,0) transf_mins, NVL(KTRA.QTY,0) transf_qty ,
NVL(KSTK.MINS,0) stock_cont_mins, 0 stock_cont_locs, NVL(KSTK.QTY,0) stock_cont_qty, 
NVL(KPUW.MINS,0) pack_uwear_mins, NVL(KPUW.QTY,0) pack_uwear_qty,
NVL(ULAB.MINS,0) ulabel_mins,NVL(ULAB.QTY,0) ulabel_qty,
NVL(QC.MINS,0) qc_mins,NVL(QC.QTY,0) QC_qty,
NVL(KSHG.MINS,0) sh_gi_mins, NVL(KSHG.QTY,0) sh_gi_qty, 
NVL(KSHR.MINS,0) sh_rets_mins, NVL(KSHR.QTY,0) sh_rets_qty,
NVL(KDEL.MINS,0) deliver_mins, NVL(KDEL.QTY,0) deliver_qty,
NVL(KCON.MINS,0) condense_mins, NVL(KCON.QTY,0) condense_qty,
NVL(KSOR.MINS,0) sort_mins, NVL(KSOR.QTY,0) sort_qty,
NVL(KPWL.MINS,0) web_list_pick_mins, NVL(KPWL.QTY,0) web_list_pick_qty,
NVL(KPWC.MINS,0) pack_web_sing_mins, NVL(KPWC.QTY,0) pack_web_sing_qty,
NVL(KPX.MINS,0) pack_mins, NVL(KPX.QTY,0) pack_qty,
NVL(KPWCL.MINS,0) pick_web_clu_mins, NVL(KPWCL.QTY,0) pick_web_clu_qty,
NVL(KPWCT.MINS,0) pick_web_trol_mins, NVL(KPWCT.QTY,0) pick_web_trol_qty,
NVL(KRR.MINS,0) ret_rework_mins, NVL(KRR.QTY,0) ret_rework_qty,
NVL(KSC.MINS,0) stock_chko_mins, NVL(KSC.QTY,0) stock_chko_qty
from all_users A 
left join kpi_intake KINT on A.user_id = KINT.user_id
left join kpi_ulabel ULAB on A.user_id = ULAB.user_id
left join kpi_qc QC on A.user_id = QC.user_id
left join kpi_intake_sort KINTSORT on A.user_id = KINTSORT.user_id
left join kpi_ret_web KRETWB on A.user_id = KRETWB.user_id
left join kpi_ret_wh KRETWH on A.user_id = KRETWH.user_id
left join kpi_pack KPACK on A.user_id = KPACK.user_id
left join kpi_pick_web KPKWB on A.user_id = KPKWB.user_id
left join kpi_pick_ret KPKRET on A.user_id = KPKRET.user_id
left join kpi_pick_who KPKWH on A.user_id = KPKWH.user_id
left join kpi_reloc KREL on A.user_id = KREL.user_id
left join kpi_rework KREW on A.user_id = KREW.user_id
left join kpi_kimbel KKIM on A.user_id = KKIM.user_id
left join kpi_desp KDES on A.user_id = KDES.user_id
left join kpi_admin KADM on A.user_id = KADM.user_id
left join kpi_stock KSTO on A.user_id = KSTO.user_id
left join kpi_housek KHOU on A.user_id = KHOU.user_id
left join kpi_other KOTH on A.user_id = KOTH.user_id
left join kpi_transf KTRA on A.user_id = KTRA.user_id
left join kpi_stock_control KSTK on A.user_id = KSTK.user_id
left join kpi_pack_uwear KPUW on A.user_id = KPUW.user_id
left join kpi_sh_gi KSHG on A.user_id = KSHG.user_id
left join kpi_sh_ret KSHR on A.user_id = KSHR.user_id
left join kpi_deliver KDEL on A.user_id = KDEL.user_id
left join kpi_condense KCON on A.user_id = KCON.user_id
left join kpi_sort KSOR on A.user_id = KSOR.user_id
left join kpi_pick_web_lis KPWL on A.user_id = KPWL.user_id
left join kpi_pack_web_clu KPWC on A.user_id = KPWC.user_id
left join kpi_packx KPX on A.user_id = KPWC.user_id
left join kpi_pick_web_clu KPWCL on A.user_id = KPWC.user_id
left join kpi_pick_web_tro KPWCT on A.user_id = KPWC.user_id
left join kpi_ret_rework KRR on A.user_id = KPWC.user_id
left join kpi_stock_checko KSC on A.user_id = KPWC.user_id
/