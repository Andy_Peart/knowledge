SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9999
SET TRIMSPOOL ON



--spool mwlhourly.csv

Select 'MOUNTAIN Stock with Qty_on_hand less than 10' from DUAL
union all
select 'Sku,Description,Work Zone,Location,,Qty on Hand,Qty Allocated' from DUAL
union all
select *
from (select
''''||C||''''||','||
sku.description||','||
ll.work_zone||','||
''''||C2||''''||','||
DECODE(CT,1,' ','*')||','||
B||','||
D
from (select
inv.sku_id C,
inv.location_id C2,
CT,
qty_on_hand B,
qty_allocated D
from inventory inv,(select
A1,
CT
from (select inv.sku_id A1,
count(*) CT,
sum(qty_on_hand) B1
from inventory inv
where client_id = 'MOUNTAIN'
and inv.location_id not in
('MAIL ORDER',
'MARSHALL',
'DESPATCH',
'STG',
'CONTAINER',
'Z013')
group by inv.sku_id)
where B1 < 10)
where inv.client_id = 'MOUNTAIN'
and inv.location_id not in
('MAIL ORDER',
'MARSHALL',
'DESPATCH',
'STG',
'CONTAINER',
'Z013')
and inv.sku_id=A1),sku,location ll
where C=sku.sku_id
and sku.client_id='MOUNTAIN'
and C2=ll.location_id
and ll.site_id='BD2'
and ll.work_zone<>'12'
order by
1)
/

--spool off

