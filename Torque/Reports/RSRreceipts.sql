SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 80
set trimspool on

--SET TERMOUT OFF
--SPOOL RSRreceipts.csv

select 'Order Reference,Supplier ID,SKU,Description,Qty Receipted' from DUAL;

select
it.reference_id||','||
it.supplier_id||','||
it.sku_id||','||
sku.description||','||
it.update_qty
from inventory_transaction it,sku
where it.client_id='ECHO'
and it.code='Receipt'
and trunc(it.Dstamp)=trunc(sysdate)
and it.reference_id like 'RSR%'
and it.sku_id=sku.sku_id
and sku.client_id='ECHO'
order by 1
/


--spool off

