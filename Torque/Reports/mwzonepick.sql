SET FEEDBACK OFF                 
set verify off
set wrap off
set colsep ','

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 132
set trimspool on

column A heading 'User ID' format a18
column B heading 'Task Type' format a18
column XX heading 'WWW' format a12
column C heading 'Tasks' format 99999999
column D heading 'Units' format 99999999
column Z noprint


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

--break on report 
break on A dup 

--spool mwzonepick.csv

select 'Total Tasks by ZONE from &&2 to &&3 - for client MOUNTAIN' from DUAL;

select 'Zone,User,Task,Order Type,Tasks,Units,Time Taken,KPI' from DUAL;

select
ZZ Z,
R||','||
A||','||
B||','||
XX||','||
C||','||
D||','||
F||','||
to_char((D/F)*3600,'9999')
from (select
'A' ZZ,
L.work_zone R,
it.user_id A,
it.code B,
DECODE(it.work_group,'WWW','Mail Order','Outlets') XX,
count(*) C,
sum(it.update_qty) D,
sum(it.elapsed_time) F
from inventory_transaction it,Location L
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='MOUNTAIN'
and it.update_qty<>0
and it.station_id not like 'Auto%'
and it.code='Pick'
and it.elapsed_time>0
and it.site_id=L.site_id
and it.from_loc_id=L.location_id
group by it.user_id,L.work_zone,it.code,DECODE(it.work_group,'WWW','Mail Order','Outlets')
union
select
'B' ZZ,
L.work_zone R,
'GRAND TOTALS' A,
it.code B,
DECODE(it.work_group,'WWW','Mail Order','Outlets') XX,
count(*) C,
sum(it.update_qty) D,
sum(it.elapsed_time) F
from inventory_transaction it,location L
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='MOUNTAIN'
and it.update_qty<>0
and it.station_id not like 'Auto%'
and it.code='Pick'
and it.elapsed_time>0
and it.site_id=L.site_id
and it.from_loc_id=L.location_id
group by it.code,L.work_zone,DECODE(it.work_group,'WWW','Mail Order','Outlets'))
order by ZZ,R,A,B,XX
/



--spool off
