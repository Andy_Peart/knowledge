/* Input Parameters are as follows */
-- 1)  From Date
-- 2)  To Date


set feedback off
set pagesize 68
set linesize 240
set newpage 0
set verify off
set colsep ' '

clear columns
clear breaks
clear computes

column X heading '' format A14
column A heading 'Date' format A16
column B heading 'Qty' format 999999
column C heading 'SKU' format A18
column D heading 'Description' format A48
column M format a4 noprint

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'VH Units Returned from '&&2' to '&&3' - for client ID MM as at ' report_date skip 2

break on report
compute sum label 'Total' of  B on report

select
to_char(i.Dstamp, 'YYMM') M,
to_char(i.Dstamp, 'DD/MM/YY') A,
i.sku_id C,
sku.description D,
i.update_qty B
from inventory_transaction i,sku
where i.client_id='MM'
and i.code='Adjustment'
and i.to_loc_id like 'PVH%'
and i.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and i.client_id=sku.client_id
and i.sku_id=sku.sku_id
Order by M,A,C
/
