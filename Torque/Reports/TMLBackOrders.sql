SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
set linesize 110

--ttitle 'Mountain Warehouse Overpicks for Period &&2 to &&3' skip 2

column A heading 'Order ID' format a14
column B1 heading 'Order Date' format A12
column B2 heading 'Creation Date' format A12
column C heading 'Consignment' format A12
column D heading 'Status' format A12
column E heading 'Order Qty' format 99999999
column F heading 'Tasked Qty' format 99999999
column G heading 'Shipped Qty' format 99999999
column H heading 'Ship Dock' format A8

select 'Order ID,Order Date,Creation Date,Consignment,Status,Order Qty,Tasked Qty,Shipped Qty,Ship Dock,,' from DUAL;

select 'Allocated,Back Orders,,,,,,,,' from DUAL;

select A,B1,B2,C,D,E,F,G,H
from (select
oh.order_id A,
to_char(oh.order_date,'DD/MM/YY') B1,
to_char(oh.creation_date,'DD/MM/YY') B2,
oh.consignment C,
oh.status D,
sum(nvl(ol.qty_ordered,0)) E,
sum(nvl(ol.qty_tasked,0)) F,
sum(nvl(ol.qty_shipped,0)) G,
oh.ship_dock H
from order_header oh,order_line ol
where oh.client_id='T'
and oh.status='Allocated'
and oh.consignment not like 'SHORT%'
and oh.order_id=ol.order_id
and oh.order_id not like 'T%'
and ol.client_id='T'
group by
oh.order_id,
to_char(oh.order_date,'DD/MM/YY'),
to_char(oh.creation_date,'DD/MM/YY'),
oh.consignment,
oh.status,
oh.ship_dock)
where E>F
order by
A
/

select 'Released,Back Orders,,,,,,,,' from DUAL;

select A,B1,B2,C,D,E,F,G,H
from (select
oh.order_id A,
to_char(oh.order_date,'DD/MM/YY') B1,
to_char(oh.creation_date,'DD/MM/YY') B2,
oh.consignment C,
oh.status D,
sum(nvl(ol.qty_ordered,0)) E,
sum(nvl(ol.qty_tasked,0)) F,
sum(nvl(ol.qty_shipped,0)) G,
oh.ship_dock H
from order_header oh,order_line ol
where oh.client_id='T'
and oh.status='Released'
and oh.consignment not like 'SHORT%'
and oh.order_id=ol.order_id
and oh.order_id not like 'T%'
and ol.client_id='T'
group by
oh.order_id,
to_char(oh.order_date,'DD/MM/YY'),
to_char(oh.creation_date,'DD/MM/YY'),
oh.consignment,
oh.status,
oh.ship_dock)
where E<>G
order by
A
/


select 'Released,RP Order Header,Update,,,,,,,' from DUAL;

select A,B1,B2,C,D,E,F,G,H
from (select
oh.order_id A,
to_char(oh.order_date,'DD/MM/YY') B1,
to_char(oh.creation_date,'DD/MM/YY') B2,
oh.consignment C,
oh.status D,
sum(nvl(ol.qty_ordered,0)) E,
sum(nvl(ol.qty_tasked,0)) F,
sum(nvl(ol.qty_shipped,0)) G,
oh.ship_dock H
from order_header oh,order_line ol
where oh.client_id='T'
and oh.status='Released'
and oh.consignment not like 'SHORT%'
and oh.order_id=ol.order_id
and oh.order_id not like 'T%'
and ol.client_id='T'
group by
oh.order_id,
to_char(oh.order_date,'DD/MM/YY'),
to_char(oh.creation_date,'DD/MM/YY'),
oh.consignment,
oh.status,
oh.ship_dock)
where E=G
order by
A
/

select 'In Progress,RP Order Header,Update,,,,,,,' from DUAL;

select A,B1,B2,C,D,E,F,G,H
from (select
oh.order_id A,
to_char(oh.order_date,'DD/MM/YY') B1,
to_char(oh.creation_date,'DD/MM/YY') B2,
oh.consignment C,
oh.status D,
sum(nvl(ol.qty_ordered,0)) E,
sum(nvl(ol.qty_tasked,0)) F,
sum(nvl(ol.qty_shipped,0)) G,
oh.ship_dock H
from order_header oh,order_line ol
where oh.client_id='T'
and oh.status='In Progress'
and oh.consignment not like 'SHORT%'
and oh.order_id=ol.order_id
and oh.order_id not like 'T%'
and ol.client_id='T'
group by
oh.order_id,
to_char(oh.order_date,'DD/MM/YY'),
to_char(oh.creation_date,'DD/MM/YY'),
oh.consignment,
oh.status,
oh.ship_dock)
where E=G
order by
A
/



select 'In Progress,Back Orders,,,,,,,,' from DUAL;

select A,B1,B2,C,D,E,F,G,H
from (select
oh.order_id A,
to_char(oh.order_date,'DD/MM/YY') B1,
to_char(oh.creation_date,'DD/MM/YY') B2,
oh.consignment C,
oh.status D,
sum(nvl(ol.qty_ordered,0)) E,
sum(nvl(ol.qty_tasked,0)) F,
sum(nvl(ol.qty_shipped,0)) G,
oh.ship_dock H
from order_header oh,order_line ol
where oh.client_id='T'
and oh.status='In Progress'
and oh.consignment not like 'SHORT%'
and oh.order_id=ol.order_id
and oh.order_id not like 'T%'
and ol.client_id='T'
group by
oh.order_id,
to_char(oh.order_date,'DD/MM/YY'),
to_char(oh.creation_date,'DD/MM/YY'),
oh.consignment,
oh.status,
oh.ship_dock)
where E<>F+G
and E<>G
order by
A
/

select 'In Progress,Available to pick,fully,,,,,,,' from DUAL;

select A,B1,B2,C,D,E,F,G,H
from (select
oh.order_id A,
to_char(oh.order_date,'DD/MM/YY') B1,
to_char(oh.creation_date,'DD/MM/YY') B2,
oh.consignment C,
oh.status D,
sum(nvl(ol.qty_ordered,0)) E,
sum(nvl(ol.qty_tasked,0)) F,
sum(nvl(ol.qty_shipped,0)) G,
oh.ship_dock H
from order_header oh,order_line ol
where oh.client_id='T'
and oh.status='In Progress'
and oh.consignment not like 'SHORT%'
and oh.order_id=ol.order_id
and oh.order_id not like 'T%'
and ol.client_id='T'
group by
oh.order_id,
to_char(oh.order_date,'DD/MM/YY'),
to_char(oh.creation_date,'DD/MM/YY'),
oh.consignment,
oh.status,
oh.ship_dock)
where E=F+G
and E<>G
order by
A
/
