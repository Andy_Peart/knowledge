SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 2000
set trimspool on


column M noprint

COLUMN CT NEW_VALUE CTNUM
COLUMN VS NEW_VALUE VSNUM

select
count(*) CT
from shipping_manifest
where client_id='MOUNTAIN'
and trunc(shipped_dstamp)=trunc(sysdate)
and work_group<>'WWW'
and nvl(container_id,'XX') like 'JJD%'
/

select substr(version,1,3) VS from yo_version;

SET FEEDBACK OFF

COLUMN NEWSEQ NEW_VALUE SEQNO


select 'S://redprairie/apps/home/dcsdba/reports/LIVE/UKD40183.'||substr(to_char(1000+next_seq),2,3) NEWSEQ from sequence_file
--select 'LIVE/UKD40183.'||substr(to_char(1000+next_seq),2,3) NEWSEQ from sequence_file
where control_id=15
and &CTNUM<>0
/

--set termout off
spool &SEQNO


select
'HD'
||'40183'
||to_char(sysdate,'DD/MM/YYYY')
||'000'
||substr('&SEQNO',length('&SEQNO')-2,3)
||lpad(A+B+B+3,8,'0')
||'000002000'
||&VSNUM
||','
from (select
count(distinct container_id) A,
count(distinct work_group) B
from shipping_manifest
where client_id='MOUNTAIN'
and trunc(shipped_dstamp)=trunc(sysdate)
and work_group<>'WWW'
and nvl(container_id,'XX') like 'JJD%')
where &CTNUM>0
/

select
'ASGB '
||'      '
||'TORQUE LOGISTICS LTD               '
||'MOUNTAIN                           '
||'ANCHOR WORKS                       '
||'HOLME LANE                         '
||'BRADFORD                           '
||'                                   '
||'BD4 6NA  '
||'        '
||'            '
||'                                   '
||'                                   '
||'                         '
||'                         '
||'                 '
||'                                        '
||'                 '
||'                 '
||','
from dual
where &CTNUM>0
/


select
CID M,
BODY
from (select
CID,
CO1
||substr(1000+Rownum,2,3)
||CO2 BODY
from (select
distinct
WG||'1' CID,
'CO'
||'057049017'
||'9609628'
||'0002'
||'          ' CO1,
to_char(sysdate,'DD/MM/YYYY')
||substr(min(CT),8,12)
||substr(max(CT),8,12)
||lpad(count(distinct CT),6,'0')
||'000C0001'
||rpad(' ',19,' ')
||rpad(' ',35,' ')
||'     '
||rpad(' ',35,' ')
||'FTP     '
||'       '
||'       '
||lpad(count(distinct CT),6,'0')
||rpad(nvl(SC,'ECO'),4,' ')
||'   '
||rpad(' ',70,' ')
||',' CO2
--from shipping_manifest sm,address ad,servicecodes sc
from (select
sm.work_group WG,
sm.container_id CT
from shipping_manifest sm
where sm.client_id='MOUNTAIN'
and trunc(sm.shipped_dstamp)=trunc(sysdate)
and sm.work_group<>'WWW'
and nvl(sm.container_id,'XX') like 'JJD%'
group by
sm.work_group,
sm.container_id),(select distinct t.servicecode SC,a.postcode PC, a.address_id from address a
inner join  (select
       REGEXP_SUBSTR(ltrim(rtrim( substr(a.from_postcode,0, length(a.from_postcode) - 3) )), '\D+' ) as SubPostCode
      ,to_number(REGEXP_SUBSTR(ltrim(rtrim( substr(a.from_postcode,0, length(a.from_postcode) - 3) )), '*\d+' )) as fromIndex
      ,to_number(REGEXP_SUBSTR(ltrim(rtrim( substr(a.to_postcode,0, length(a.to_postcode) - 3) )), '*\d+' )   )  as toIndex
      ,c.servicecode
    from yo_destination_station a
    inner join yo_destination_prdservices b on a.servicectr_reamusid = b.servicectr_reamusid and b.Allowed = 'Y' --service
  --  inner join yo_destination_prdservices b1 on a.hub_reamusid = b1.servicectr_reamusid  and b1.Allowed = 'Y'     --hub
    inner join yo_service c on b.productcode = c.productcode and b.featurecode = c.featurecode  --service
  --  inner join yo_service c1 on b1.productcode = c1.productcode and b1.featurecode = c1.featurecode  --hub
    inner join yo_reamusid d on a.servicectr_reamusid = d.reamusid  --?
    inner join yo_reamusid d1 on a.hub_reamusid = d1.reamusid  --?
    where a.productcode = '01'
    and c.featureid in ('003','006','035') and c.productcode = '01'
union all
    --No situation
    select
       REGEXP_SUBSTR(ltrim(rtrim( substr(a.from_postcode,0, length(a.from_postcode) - 3) )), '\D+' ) as SubPostCode
      ,to_number(REGEXP_SUBSTR(ltrim(rtrim( substr(a.from_postcode,0, length(a.from_postcode) - 3) )), '*\d+' )) as fromIndex
      ,to_number(REGEXP_SUBSTR(ltrim(rtrim( substr(a.to_postcode,0, length(a.to_postcode) - 3) )), '*\d+' )   )  as toIndex
      ,hc.servicecode
    from yo_destination_station a
    inner join yo_destination_prdservices b on a.servicectr_reamusid = b.servicectr_reamusid and b.Allowed = 'E' --service
  --  inner join yo_destination_prdservices b1 on a.hub_reamusid = b1.servicectr_reamusid and  b1.Allowed = 'E'     --hub
    inner join yo_destination_exception c on b.productcode = c.productcode and b.featurecode = c.featurecode and a.from_postcode = c.from_postcode  --service
  --  inner join yo_destination_exception c1 on b1.productcode = c1.productcode and b1.featurecode = c1.featurecode and a.from_postcode = c1.from_postcode  --hub
  --FIX: DPH was missing link to services, remove hard coded to this service table
        inner join yo_service hc on b.productcode = hc.productcode and b.featurecode = hc.featurecode  --service
    --    inner join yo_service hc1 on b1.productcode = hc1.productcode and b1.featurecode = hc1.featurecode  --hub
    inner join yo_reamusid d on a.servicectr_reamusid = d.reamusid  --?
    inner join yo_reamusid d1 on a.hub_reamusid = d1.reamusid  --?
    where a.productcode = '01'
    and c.featurecode in ('03','06','35') and c.productcode = '01' )  t
on (t.SubPostCode =  REGEXP_SUBSTR(ltrim(rtrim( substr(a.postcode,0, length(a.postcode) - 3) )), '\D+' ) --SW04 e.g match on SW, then  04 between XX and XX
          and to_number( REGEXP_SUBSTR(ltrim(rtrim( substr(a.postcode,0, length(a.postcode) - 3) )), '*\d+' ) ) >= t.fromIndex
          and to_number( REGEXP_SUBSTR(ltrim(rtrim( substr(a.postcode,0, length(a.postcode) - 3) )), '*\d+' ) ) <= t.toIndex    )
  where client_ID = 'MOUNTAIN' and a.address_id not in('SOS','VIE','WAR','DUB','RAT')
  group by  t.servicecode ,a.postcode , a.address_id  )   yodel
where WG=yodel.address_id --remove address table, use address table already in yodel query
group by
WG,
SC
order by CID)
union
select
distinct
sm.work_group||'2' CID,
'ADGB '
||'      '
||rpad('MOUNTAIN',35,' ')
||rpad(upper(nvl(ad.name,' ')),35,' ')
||rpad(upper(nvl(ad.address1,' ')),35,' ')
||rpad(upper(nvl(ad.address2,' ')),35,' ')
||rpad(upper(nvl(ad.town,' ')),35,' ')
||rpad(upper(nvl(ad.county,' ')),35,' ')
||rpad(upper(ad.postcode),9,' ')
||rpad(' ',20,' ')
||rpad(upper(nvl(ad.contact,' ')),35,' ')
||rpad(' ',35,' ')
||rpad(upper(nvl(ad.contact_phone,' ')),25,' ')
||rpad(upper(nvl(ad.contact_fax,' ')),25,' ')
||rpad(' ',17,' ')
||rpad(upper(nvl(ad.contact_email,' ')),40,' ')
||rpad(' ',17,' ')
||rpad(' ',17,' ')
||',' BODY
from shipping_manifest sm,address ad
where sm.client_id='MOUNTAIN'
and trunc(sm.shipped_dstamp)=trunc(sysdate)
and sm.work_group<>'WWW'
and nvl(sm.container_id,'XX') like 'JJD%'
and sm.work_group=ad.address_id
and ad.client_id='MOUNTAIN'
union
select
distinct
sm.work_group||'3'||sm.container_id CID,
'PA'
||rpad(' ',10,' ')
||RN
||'JD0002240183'
||substr(sm.container_id,length(sm.container_id)-5,6)
||rpad(' ',17,' ')
||rpad(' ',33,' ')
||'PARCEL'
||'J '
||rpad(' ',141,' ')
||',' BODY
from shipping_manifest sm,(select
work_group WG,
substr(1000+Rownum,2,3) RN
from (select
distinct
work_group
from shipping_manifest
where client_id='MOUNTAIN'
and trunc(shipped_dstamp)=trunc(sysdate)
and work_group<>'WWW'
and nvl(container_id,'XX') like 'JJD%'
--and container_id is not null
order by work_group))
where sm.client_id='MOUNTAIN'
and trunc(sm.shipped_dstamp)=trunc(sysdate)
and sm.work_group<>'WWW'
and nvl(sm.container_id,'XX') like 'JJD%'
and sm.work_group=WG)
where &CTNUM>0
order by CID
/



select
'TR'
||lpad(A+B+B+3,8,'0')
from (select
count(distinct container_id) A,
count(distinct work_group) B
from shipping_manifest
where client_id='MOUNTAIN'
and trunc(shipped_dstamp)=trunc(sysdate)
and work_group<>'WWW'
and nvl(container_id,'XX') like 'JJD%')
where &CTNUM>0
/


spool off


Update sequence_file
set next_seq=next_seq+1
where control_id=15
and &CTNUM<>0
/


commit;
--rollback;


