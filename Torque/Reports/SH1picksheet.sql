SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET LINESIZE 120
SET TRIMSPOOL ON
SET HEADING OFF

--spool SHps.txt

select '                        SAHARA Pick Sheet' from DUAL;
select '                        -----------------' from DUAL;
select ' ' from DUAL;

column AA noprint
column R heading 'Order' format a16
column A heading 'SKU' format a20
column B heading ' ' format a4
column C heading 'Location' format a12
column D heading 'Pick Qty' format 99999
column D2 heading 'Stock' format 9999999999
column E heading 'Picked' format A12

select
'TO:  '||
name||',   '||
address1||',   '||
address2||',   '||
postcode
from order_header
where client_id='SH'
and order_id like '&&2'
/

select ' ' from DUAL;

select 'Order            SKU                  Pick Qty    Pick From    Location        Picked' from DUAL;
select '-----            ---                  --------    ---------    --------        ------' from DUAL;
select ' ' from DUAL;

break on row skip 1 on report

compute sum label 'Total' of D D2 on report

select
ol.order_id R,
ol.sku_id A,
sum(mt.qty_to_move) D,
QQQ D2,
'   ' B,
mt.from_loc_id C,
'___________' E
from move_task mt,order_line ol,(select
sku_id SSS,
location_id LLL,
sum(qty_on_hand) QQQ
from inventory
where client_id='SH'
group by
sku_id,
location_id)
where mt.client_id='SH'
and ol.order_id like '&&2'
and ol.client_id=mt.client_id
and ol.order_id=mt.task_id
and ol.sku_id=mt.sku_id
and ol.line_id=mt.line_id
and ol.sku_id=SSS
and LLL=mt.from_loc_id (+)
group by
ol.order_id,
ol.sku_id,
QQQ,
mt.from_loc_id
order by C
/
--spool off
