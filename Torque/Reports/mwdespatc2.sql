/******************************************************************************/
/*                                                                            */
/*                            Elite                                           */
/*                                                                            */
/*     FILE NAME  :   mwdespatch.sql                                          */
/*                                                                            */
/*     DESCRIPTION:    Datastream for Mountain Warehouse Despatch Note        */
/*                                                                            */
/*   DATE     BY   PROJ     ID         DESCRIPTION                            */
/*   ======== ==== ======   ========   =============                          */
/*   27/04/05 LH   Mountain            Despatch Note for Mountain Warehouse   */
/******************************************************************************/

/* Input Parameters are as follows */
-- 1)  Order ID
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 0
SET VERIFY OFF
SET FEEDBACK OFF
SET TAB OFF

 
-- Set the line length to the maximum possible, but trim 
-- trailing spaces for both screen output and spool files
SET LINESIZE 32767
SET WRAP ON
SET TRIMOUT ON
SET TRIMSPOOL ON




/* Define the format of the output dates */
DEFINE	OutputDateFormat = 'DD-MON-YYYY HH24:MI:SS'
COLUMN	sorter	NOPRINT

SELECT Distinct '0' sorter,
       'DSTREAM_MW_DESP_HDR'          ||'|'||
       rtrim('&&1')                            /* Order_id */
from dual
union all
SELECT '0' sorter,
'DSTREAM_MW_DESP_ADD_HDR'              ||'|'||
rtrim (ad.name)						||'|'||
rtrim (oh.customer_id)				||'|'||
rtrim (ad.address1)					||'|'||
rtrim (ad.address2)					||'|'||
rtrim (ad.town)						||'|'||
rtrim (ad.county)					||'|'||
rtrim (ad.country)					||'|'||
rtrim (ad.postcode)					||'|'||
rtrim (ad.contact_fax)
from address ad, order_header oh
where oh.order_id = '&&1'
and oh.customer_id = ad.address_id
union all
select distinct '0' sorter,
'DSTREAM_MW_DESP_ORD_HDR'			||'|'||
rtrim (oh.order_id)					||'|'||
rtrim (to_char(oh.shipped_date, 'DD-MON-YYYY HH24:MI'))				||'|'||
rtrim (sm.location_id)				||'|'||
rtrim (sm.user_id)					||'|'||
rtrim (sm.site_id)					||'|'||
rtrim (sm.consignment)		
from order_header oh, shipping_manifest sm
where oh.order_id = '&&1'
and oh.order_id = sm.order_id
union all
SELECT distinct s.user_def_type_5,
'DSTREAM_MW_DESP_LINE'              ||'|'||
rtrim (substr(s.user_def_type_5,1,6))                 ||'|'||
rtrim (ol.sku_id)					||'|'||
rtrim (s.USER_DEF_TYPE_2)			||'|'||
rtrim (s.COLOUR)					||'|'||
rtrim (s.SKU_SIZE)					||'|'||
rtrim (ol.qty_shipped)				
from order_line ol, sku s
where ol.order_id = '&&1'
and ol.qty_shipped > '0'
and ol.sku_id = s.sku_id
/*'DSTREAM_MW_DESP_LINE'              ||'|'||
rtrim (substr(loc.zone_1,1,1))       ||'|'||
rtrim (substr(s.user_def_type_5,1,6))                 ||'|'||
rtrim (ol.sku_id)					||'|'||
rtrim (s.USER_DEF_TYPE_2)			||'|'||
rtrim (s.COLOUR)					||'|'||
rtrim (s.SKU_SIZE)					||'|'||
rtrim (inv.update_qty)			
from order_line ol, inventory_transaction inv, sku s, location loc
where ol.order_id = '&&1'
and ol.order_id = inv.reference_id
and ol.sku_id = inv.sku_id
and inv.to_loc_id = 'MARSHAL'
and inv.from_loc_id <> 'Z001'
and inv.code = 'Pick'
and inv.update_qty > '0'
and ol.sku_id = s.sku_id
and inv.from_loc_id = loc.location_id*/
/