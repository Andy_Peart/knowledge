SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 200
SET TRIMSPOOL ON

--spool mwstockinzone.csv

select 'SKU,Description,Qty Allocated,Work Zone' from DUAL;

select
iv.sku_id||','||
iv.description||','||
iv.qty_allocated||','||
ll.work_zone
from inventory iv,location ll
where iv.client_id='MOUNTAIN'
and iv.qty_allocated>0
and iv.location_id=ll.location_id
and ll.site_id='BD2'
and ll.work_zone='&&2'
order by 1
/

--spool off
