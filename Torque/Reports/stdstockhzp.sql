set feedback off
set pagesize 68
set linesize 240
set verify off
set colsep ' '


clear columns
clear breaks
clear computes

column A1 heading 'Work Zone' format A12
column A2 heading 'Qty On Hand' format 999999999
column A3 heading 'Qty Unallocatable' format 999999999
column A4 heading 'Qty Allocated' format 999999999
column A5 heading 'Qty Available' format 999999999



set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

set heading on

break on report
compute sum label 'Totals' of A2 A3 A4 A5 on report


ttitle  LEFT 'Stock Holding by Zone for client ID &&2 as at ' report_date skip 2


select
distinct
work_zone A1,
DECODE(AA,'PGPL',nvl(PP,0),nvl(BB,0)) A2,
nvl(DD,0) A3,
DECODE(AA,'PGPL',0,nvl(CC,0)) A4,
DECODE(AA,'PGPL',nvl(PP,0),nvl(BB,0)-nvl(DD,0)-nvl(CC,0)) A5
from location,(select
lc.work_zone AA,
sum(jc.qty_on_hand) BB,
sum(jc.qty_allocated) CC,
count(distinct jc.location_id) PP
from inventory jc,location lc
where jc.client_id like '&&2'
and jc.location_id=lc.location_id
and jc.site_id=lc.site_id
group by
lc.work_zone),(select
lc.work_zone AAA,
nvl(sum(jc.qty_on_hand),0)-nvl(sum(jc.qty_allocated),0) DD
from inventory jc,location lc
where jc.client_id like '&&2'
and jc.location_id=lc.location_id
and jc.site_id=lc.site_id
and lc.disallow_alloc='Y'
group by
lc.work_zone)
where location.work_zone=AA (+)
and location.work_zone=AAA (+)
and nvl(BB,0)<>0
--and ((location.work_zone not like 'EX%'
--and '&&2'<>'EX') or '&&2'='EX')
order by 1
/

