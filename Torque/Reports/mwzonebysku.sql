SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON

with t1 as (
    select order_id 
    from order_header
    where client_id = 'MOUNTAIN'
    and consignment = '&&2'
),
t2 as (
    select distinct loc.work_zone wzone
    from inventory_transaction it inner join location loc on it.site_id = loc.site_id and it.from_loc_id = loc.location_id
    where it.client_id = 'MOUNTAIN'
    and it.reference_id  in (select order_id from t1)
    and it.sku_id = '&&3'
    and it.code = 'Pick'
    and it.dstamp between trunc(sysdate)-7 and trunc(sysdate)+1
    and it.to_loc_id in ('CONTAINER','DESPATCH')
)
select 'Work zones where SKU &&3 was picked under Consignment &&2' from dual
union all
select 'Work zone,User ID' from dual
union  all
select wzone || ',' || user_id
from (
    select distinct loc.work_zone wzone, user_id
    from inventory_transaction it inner join location loc on it.site_id = loc.site_id and it.from_loc_id = loc.location_id
    where it.client_id = 'MOUNTAIN'
    and it.reference_id  in (select order_id from t1)
    and it.code = 'Pick'
    and loc.work_zone in (select wzone from t2)
    and it.dstamp between trunc(sysdate)-7 and trunc(sysdate)+1
    and it.to_loc_id in ('CONTAINER','DESPATCH')
    order by 1,2
)
/