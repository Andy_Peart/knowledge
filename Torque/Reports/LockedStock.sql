SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240



select 'Locked Stock Report - for client ID &&2 as at ' 
        ||  to_char(sysdate,'dd/mm/yy  hh24:mi:ss') from dual;

select 'SKU Code,Description,Qty,Location,Last User,Date'
from dual;

select
iv.sku_id ||','||
DDD ||','||
iv.qty_on_hand ||','||
iv.location_id ||','||
it.user_id ||','||
to_char(it.dstamp,'DD/MM/YY')
from inventory iv,Inventory_transaction it,(select 
sku_id SSS,
description DDD
from sku where client_id='&&2')
where iv.client_id='&&2'
and iv.lock_status='Locked'
and iv.sku_id=SSS (+)
and it.client_id='&2'
and iv.sku_id=it.sku_id
and it.code='Inv Lock'
and it.dstamp=iv.count_dstamp
order by iv.sku_id
/
select
',Total' ||','||
nvl(sum(iv.qty_on_hand),0)
from inventory iv,Inventory_transaction it
where iv.client_id='&&2'
and iv.lock_status='Locked'
and iv.sku_id=it.sku_id
and it.code='Inv Lock'
and it.dstamp=iv.count_dstamp
/
