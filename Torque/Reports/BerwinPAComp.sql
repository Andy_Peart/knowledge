SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET COLSEP ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 200
SET TRIMSPOOL ON
SET HEADING OFF 

column Z heading 'Date' format A12
column A heading 'pre_advice' format A20
column C heading 'SKU' format A18
column D heading 'Desc' format A32
column E heading 'Qty1' format 9999999
column F heading 'Qty2' format 9999999
column G heading 'Qty3' format 9999999



--spool pac.csv

--select 'Berwin Completed,Pre Advice,Detail from &&2,to &&3' from DUAL;
--select to_char(SYSDATE, 'DD/MM/YY    HH:MI:SS') from DUAL;

select 'Date,Pre Advice ID,SKU ID,Description,Qty Due,Qty Received,Variance' from DUAL;


break on report
compute sum label 'Total' of E F G on report


select * from (select
trunc(ph.finish_Dstamp) Z,
ph.pre_advice_id A,
''''||pl.sku_id||'''' C,
sku.description D,
sum(nvl(pl.qty_due,0)) E,
sum(nvl(pl.qty_received,0)) F,
sum(nvl(pl.qty_due,0))-sum(nvl(pl.qty_received,0)) G
from pre_advice_header ph,pre_advice_line pl,sku
where ph.client_id='BW'
and ph.finish_Dstamp>sysdate-1 
and ph.status='Complete'
and pl.client_id like 'BW'
and ph.pre_advice_id=pl.pre_advice_id
and sku.client_id='BW'
and pl.sku_id=sku.sku_id
group by
trunc(ph.finish_Dstamp),
ph.pre_advice_id,
pl.sku_id,
sku.description)
where G<>0
order by A,C
/

--spool off

