SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET WRAP OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
set linesize 500
set trimspool on


column A heading 'Order ID' format a14
column B heading 'Client ID' format a14
column C heading 'Line ID' format a10
column D heading 'Customer' format A8
column D1 heading 'Customer Name' format A32
column E heading 'Notes' format A32
column F heading 'SKU ID' format a18
column G heading 'Style' format a18
column H heading 'Description' format a40
column H1 heading 'Colour' format a12
column H2 heading 'Size' format a12
column H3 heading 'Fit' format a12
column J heading 'EAN' format a18
column L heading 'Customer SKU' format a18
column M heading 'Order Qty' format 9999999999
column N heading 'Tasked Qty' format 9999999999
column P heading 'Picked Qty' format 9999999999
column R heading 'Shipped Qty' format 9999999999
column S heading 'Delivered Qty' format 9999999999
column T heading 'Qty Short' format 9999999999
column V heading 'Creation Date' format A12
column V2 heading 'Creation Time' format A12
column X1 heading 'Price 1' format 9999.99
column X2 heading 'Price 1' format 9999.99

select 'GIVe Orders - Released - Allocated - In Progress - Hold - Awaiting - Complete --- as at '||to_char(SYSDATE, 'DD/MM/YY  HH:MI') from DUAL;

select 'Order ID,Client,Line ID,Customer,Customer Name,Notes,SKU ID,Style,Description,Colour,Size,Fit,EAN,Qty Ordered,Qty Tasked,Qty Picked,Qty Shipped,Qty Delivered,Qty Short,Creation Date,Creation Time,GBP Price,EUR Price,' from DUAL;

break on A dup on report

compute sum label 'Totals' of N M P R S T on A
compute sum label 'Grand Totals' of N M P R S T on report

select 
oh.order_id A, 
oh.client_id B, 
DECODE(oh.client_id,'GV',ol.user_def_num_2,ol.line_id) C,
oh.customer_id,
ad.name,
ol.notes E,
ol.sku_id,
sku.user_def_type_6,
sku.description H,
sku.colour,
sku.sku_size,
sku.user_def_type_8,
''''||sku.ean,
sum(nvl(ol.qty_ordered,0)) M,
sum(nvl(ol.qty_tasked,0)) N,
sum(nvl(ol.qty_picked,0)) P,
sum(nvl(ol.qty_shipped,0)) R,
sum(nvl(ol.qty_delivered,0)) S,
sum(nvl(ol.qty_ordered,0)-nvl(ol.qty_tasked,0)-nvl(ol.qty_picked,0)) T,
to_char(oh.creation_date,'DD/MM/YY') V, 
to_char(oh.creation_date,'HH:MI:SS') V2, 
sku.user_def_num_3 X1, 
sku.user_def_num_4 X2, 
' '
from order_header oh,order_line ol,sku,address ad
where oh.client_id = '&&2'
and ol.client_id=oh.Client_id 
and oh.order_id = ol.order_id
and oh.status in ('Released','Allocated','In Progress','Hold','Awaiting','Complete')
and sku.client_id='&&2'
and ol.sku_id=sku.sku_id
and ad.client_id='&&2'
and oh.customer_id=ad.address_id
group by
oh.order_id, 
oh.client_id, 
DECODE(oh.client_id,'GV',ol.user_def_num_2,ol.line_id),
oh.customer_id,
ad.name,
ol.notes,
ol.sku_id,
sku.user_def_type_6,
sku.description,
sku.colour,
sku.sku_size,
sku.user_def_type_8,
sku.ean,
sku.description,
to_char(oh.creation_date,'DD/MM/YY'), 
to_char(oh.creation_date,'HH:MI:SS'),
sku.user_def_num_3, 
sku.user_def_num_4
order by
oh.order_id
/
