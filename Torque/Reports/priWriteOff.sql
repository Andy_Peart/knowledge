SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 120

column A heading 'SKU' format A22
column B heading 'Date' format A12
column C heading 'Description' format A40
column D heading 'Qty' format 99999
column E heading 'Loc' format a12
column F heading 'User' format a12


select ',,WRITE OFF REPORT' from DUAL;
select 'Client PRINGLE,,From &&2 to &&3' from DUAL;
select 'SKU,Date,Description,Qty,Location,User ID,,' from DUAL;
 
set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DAY, DD MONTH, YYYY') curdate from DUAL;
set TERMOUT ON


break on report 

compute sum label 'Total' of D on report


select
''''||AAA||'''' A,
BBB B,
CCC C,
QQQ D,
EEE E,
FFF F,
' '
from (select
it.sku_id AAA,
max(to_char(it.Dstamp,'DD/MM/YY')) BBB,
sku.description CCC,
sum(it.update_qty) QQQ,
max(it.from_loc_id) EEE,
max(it.user_id) FFF
from inventory_transaction it,sku
where it.client_id='PRI'
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.code='Stock Check'
and it.sku_id=sku.sku_id
and it.client_id=sku.client_id
group by
it.sku_id,
sku.description)
where QQQ<>0
order by
A,B
/
