SET FEEDBACK OFF
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
set colsep ','
set heading off
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 80

column A heading 'SKU' format a20
column B heading 'Description' format a40
column C heading 'Qty' format 9999999

select 'SKU,Description,Qty' from dual;

select 
s.sku_id||','||
s.description||','||
nvl(sum(i.qty_on_hand),0)
from sku s, inventory i
where s.client_id = 'MOUNTAIN'
and s.sku_id = i.sku_id
and location_id <> 'DESPATCH'
and location_id <> 'N001'
group by 
s.sku_id,
s.description
order by 
s.sku_id
/
