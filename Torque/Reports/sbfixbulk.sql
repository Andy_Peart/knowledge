set serveroutput on;
set feedback off;
set linesize 1000;
set verify off;

declare
    i number;
    pragma autonomous_transaction;       
begin
    
    update move_task
    set to_loc_id = 'PACKSB'
    where client_id = 'SB'
    and task_type = 'O'
    and to_loc_id <> 'PACKSB'
    and task_id in 
    (
        select regexp_substr('&2','[^,]+',1,level) from dual
        connect by regexp_substr('&2','[^,]+',1,level) is not null
    );
    i := SQL%rowcount;   
    
    commit;

    if i > 0 then dbms_output.put_line(i || ' task/s updated'); 
    else dbms_output.put_line('Nothing to update');
    end if;
    
end;
/