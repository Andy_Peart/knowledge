SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 999
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON


SELECT   
oh.consignment,
ol.SKU_ID,
sm.container_id,
sm.USER_ID,
sk.DESCRIPTION as Description,
substr(sk.description,-7) as STYLE,
Initcap(sk.colour) as Colour,
oh.order_date as order_date,
EXTRACT(YEAR FROM oh.order_date) AS ORDYEAR,
EXTRACT(MONTH FROM oh.order_date) AS ORDMONTH,
EXTRACT(DAY FROM oh.order_date) AS DAY,
sk.sku_size,
sk.each_value,
(Initcap(sk.DESCRIPTION)||'- '||sk.sku_size||'- '||Initcap(sk.colour)) as details,
case
when ROW_NUMBER() OVER(PARTITION BY ol.SKU_ID ORDER BY ol.SKU_ID) > 1 then null
else ol.qty_ordered
end as qty_ordered,
sum(sm.QTY_PICKED) as QTY,
oh.ORDER_ID,
oh.CLIENT_ID,
upper(replace(oh.CONTACT,'"','')) as CONTACT,
initcap(replace(oh.name,'"','')) as NAME,
initcap(replace(oh.ADDRESS1,'"','')) as ADDRESS1,
initcap(replace(oh.ADDRESS2,'"','')) as ADDRESS2,
upper(replace(oh.TOWN,'"','')) as TOWN,
initcap(replace(oh.COUNTY,'"','')) as COUNTY,
upper(replace(oh.POSTCODE,'"','')) as POSTCODE,
upper(replace(c.TANDATA_ID,'_',' ')) as COUNTRY,
oh.CUSTOMER_ID,
oh.user_def_type_1 as cust_ref
FROM   ORDER_LINE ol, ORDER_HEADER oh, SHIPPING_MANIFEST sm, SKU sk, COUNTRY c
WHERE (ol.ORDER_ID=oh.ORDER_ID AND ol.CLIENT_ID=oh.CLIENT_ID)
AND (ol.CLIENT_ID=sm.CLIENT_ID AND ol.ORDER_ID=sm.ORDER_ID AND ol.LINE_ID=sm.LINE_ID)
AND (ol.CLIENT_ID=sk.CLIENT_ID AND ol.SKU_ID=sk.SKU_ID)
AND (nvl(oh.COUNTRY,'GBR')=c.ISO3_ID)
AND oh.CLIENT_ID= 'CK'
AND oh.ORDER_ID= '&&2'
GROUP BY 
oh.consignment,
oh.ORDER_ID,
oh.CLIENT_ID,
sm.container_id,
ol.SKU_ID,
sk.DESCRIPTION,
substr(sk.description,-7),
sk.colour,
sk.sku_size,
sk.each_value,
ol.qty_ordered,
sm.USER_ID,
oh.CONTACT,
oh.name,
oh.ADDRESS1,
oh.ADDRESS2,
oh.TOWN,
oh.COUNTY,
oh.POSTCODE,
c.TANDATA_ID,
oh.CUSTOMER_ID,
oh.order_date,
oh.user_def_type_1
ORDER BY 
sm.container_id,
ol.SKU_ID
/
