SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

 
select 'Location, Docket, Product, Size, Style, Date Received, Qty' from dual;
select loc ||','|| docket ||','|| product ||','|| sku_size ||','|| sku_style ||','|| received ||','|| qty
from (
select inv.location_id loc, inv.user_def_type_4 docket, sku.product_group product, sku.sku_size, inv.user_def_type_7 sku_style, to_char(inv.receipt_dstamp,'DD-MM-YYYY') received, sum(inv.qty_on_hand) qty
from inventory inv inner join sku on inv.sku_id = sku.sku_id and inv.client_id = sku.client_id
where inv.client_id in ('PR','PS')
and inv.location_id like 'PRHG%'
group by inv.location_id, inv.user_def_type_4, sku.product_group, sku.sku_size, inv.user_def_type_7, to_char(inv.receipt_dstamp,'DD-MM-YYYY')
order by 1,2,3,4
)
/