--Want the report to show all fully allocated mail orders 
--so any order beginning with prefix "C" 
--where quantity ordered equals quantity tasked. . . 

SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 66
SET NEWPAGE 0
SET LINESIZE 240


column A heading 'Order Type' format A12
column F heading 'Dispatch Method' format A16
column AA heading 'Order' format A12
column BB heading 'Creation Date' format A14
column CC heading 'Order|Qty' format 999999
column TT heading 'Tasked|Qty' format 999999
column WW heading 'Stock|Qty' format 999999

--select 'Shop,Date,Qty' from DUAL;

--break on A dup skip 2 on F dup skip 1

select
distinct
DDD A,
FFF F,
AAA AA,
BBB BB
--SSS BB,
--OOO CC,
--TTT TT,
--nvl(QQ,0) WW
from (select
oh.order_type  DDD,
oh.Dispatch_method FFF,
oh.order_id  AAA,
ol.sku_id SSS,
trunc(oh.creation_date) BBB,
count(*) ct,
nvl(sum(ol.qty_ordered),0) OOO,
nvl(sum(ol.qty_tasked),0) TTT
from order_header oh, order_line ol
where oh.client_id='SB'
and (oh.order_id like 'C%' or oh.order_id like 'E%')
and oh.status not in ('Cancelled')
and oh.order_id=ol.order_id
and ol.client_id='SB'
and ol.back_ordered='Y'
group by
oh.order_type,
oh.Dispatch_method,
oh.order_id,
ol.sku_id,
trunc(oh.creation_date)),(select
sku_id QS,
sum(qty_on_hand-qty_allocated) QQ
from inventory
where client_id='SB'
group by sku_id)
where OOO>TTT
and SSS=QS 
order by
A,F,AA
/

