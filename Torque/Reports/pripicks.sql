SET FEEDBACK OFF             
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 68
SET NEWPAGE 0
SET LINESIZE 110

column CC heading 'Picks' format 99999999
column KK heading 'Units' format 99999999


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


TTITLE LEFT 'PRINGLE - Total units picked Minus Outlet Retail between &&2 and &&3 as at ' report_date skip 2


select
count(*) CC,
sum(it.update_qty) KK
from inventory_transaction it,order_header oh
where it.client_id='PRI'
and it.code='Pick'
and it.to_loc_id='DESPATCHPR'
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id=oh.client_id
and it.reference_id=oh.order_id
and substr(oh.order_id,1,2) in ('31','51')
and oh.inv_name not in ('ARN','DES','LOG')
/