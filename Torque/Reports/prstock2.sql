SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET COLSEP ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET HEADING OFF
SET PAGESIZE 0
SET LINESIZE 200
SET TRIMSPOOL ON

column A heading 'Category' format A12
column C heading 'Customer' format A12
column B heading 'Qty' format 9999999

break on report 
compute sum label '   Total' of B on report

select 'Prominent Stock Holding by Customer' from DUAL;
select to_char(SYSDATE, 'DD/MM/YY    HH:MI:SS') from DUAL;

select '' from DUAL;
select 'HANGING' from DUAL;

select
--'Hanging' A,
sku.user_def_type_2 C,
sum(i.qty_on_hand*nvl(sku.user_def_type_9,1)) B
from inventory i,sku
where i.client_id like 'PR'
and i.site_id like 'PROM'
and i.zone_1 like 'LSH1'
--and location_id != 'REJECT'
and sku.client_id=i.client_id
and sku.sku_id=i.sku_id
group by
sku.user_def_type_2
/


select '' from DUAL;
select 'PALLETS' from DUAL;

select
--'Pallets' A,
sku.user_def_type_2 C,
nvl(count(distinct i.location_id),0) B
from inventory i,sku
where i.client_id like 'PR'
and i.site_id like 'PROM'
and i.zone_1 in ('LSP1','LS1')
--and i.zone_1 like 'LSP1'
and sku.client_id=i.client_id
and sku.sku_id=i.sku_id
group by
sku.user_def_type_2
/



select '' from DUAL;
select 'SHELVING' from DUAL;

select
--'Shelving' A,
sku.user_def_type_2 C,
nvl(count(distinct i.location_id),0) B
from inventory i,sku
where i.client_id like 'PR'
and i.site_id like 'PROM'
and i.zone_1 like 'LSS1'
--and i.zone_1 in ('LSS1','LS1')
and sku.client_id=i.client_id
and sku.sku_id=i.sku_id
group by
sku.user_def_type_2
/


