set pagesize 64
set linesize 2000
set verify off
clear columns
clear breaks
clear computes

set heading on
set feedback off

column A heading 'Group' format a20
column B heading 'Receipts' format 9999999
column C heading 'Picks' format 9999999


set heading off

select 'TML Retail Picks/Receipts from &&2 to &&3' from DUAL;

set heading on

break on report

compute sum label 'Total' of B C on report


select
DECODE (A1,'GROUP1','Mens Shirts',
    'GROUP2','Mens Knits',
    'GROUP3','Womens Shirts/Knits',
    'GROUP4','Tailoring',
    'GROUP5','Accessories',
    'GROUP6','Mens Ties',A1) A,
nvl(B2,0) B,
nvl(B1,0) C
from
(select
sku.allocation_group A1,
sum(it.update_qty) B1
from inventory_transaction it,sku
where it.client_id='TR'
and it.client_id=sku.client_id
and it.sku_id=sku.sku_id
and it.code='Pick'
and it.from_loc_id<>'CONTAINER'
--and it.to_loc_id='CONTAINER'
and trunc(it.Dstamp)>=to_date('&&2','dd-mon-yyyy')
and trunc(it.Dstamp)<=to_date('&&3','dd-mon-yyyy')
group by
sku.allocation_group),
(select
sku.allocation_group A2,
sum(it.update_qty) B2
from inventory_transaction it,sku
where it.client_id='TR'
and it.client_id=sku.client_id
and it.sku_id=sku.sku_id
and it.station_id not like 'Auto%'
and it.code='Receipt'
and trunc(it.Dstamp)>=to_date('&&2','dd-mon-yyyy')
and trunc(it.Dstamp)<=to_date('&&3','dd-mon-yyyy')
group by
sku.allocation_group)
where A1=A2 (+)
order by A1
/


select
A1 A,
nvl(B2,0) B,
nvl(B1,0) C
from
(select
sku.product_group A1,
sum(it.update_qty) B1
from inventory_transaction it,sku
where it.client_id='TR'
and it.client_id=sku.client_id
and it.sku_id=sku.sku_id
and it.code='Pick'
and it.from_loc_id<>'CONTAINER'
--and it.to_loc_id='CONTAINER'
and trunc(it.Dstamp)>=to_date('&&2','dd-mon-yyyy')
and trunc(it.Dstamp)<=to_date('&&3','dd-mon-yyyy')
group by
sku.product_group),
(select
sku.product_group A2,
sum(it.update_qty) B2
from inventory_transaction it,sku
where it.client_id='TR'
and it.client_id=sku.client_id
and it.sku_id=sku.sku_id
and it.station_id not like 'Auto%'
and it.code='Receipt'
and trunc(it.Dstamp)>=to_date('&&2','dd-mon-yyyy')
and trunc(it.Dstamp)<=to_date('&&3','dd-mon-yyyy')
group by
sku.product_group)
where A1=A2 (+)
order by A1
/

