SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep '~'


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240


column A heading 'Order Date' format A11
column B heading 'Shipped' format A11
column C heading 'Order ID' format A12
column D heading 'Name' format A24
column E heading 'Address_1' format A36
column F heading 'Post Code' format A12
column G heading 'SKU' format A18


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



--ttitle  LEFT 'TML Retail - Duplicate Order Addresses ' RIGHT 'Printed ' report_date skip 2

select 'Client,Order Date,Shipped Date,Order ID,Name,Address 1,Post Code,SKU,Description,Colour,Size,Ordered,Shipped' from DUAL;


--break on F skip 1

select
oh.client_id ||','||
to_char(oh.order_date, 'DD/MM/YY') ||','||
to_char(oh.shipped_date, 'DD/MM/YY') ||','||
--CT,
oh.order_id ||','||
--customer_id ||','||
oh.name ||','||
oh.address1 ||','||
oh.postcode ||','||
ol.sku_id ||','||
sku.description ||','||
sku.colour ||','||
sku.sku_size  ||','||
ol.qty_ordered  ||','||
ol.qty_shipped
from order_header oh, order_line ol,sku,
(select postcode  PC,name NM,count(*) CT
from order_header
Where Client_id = 'MM'
and Order_type = 'MAILORDER'
group by postcode,name)
Where oh.Client_id = 'MM'
and oh.Order_type = 'MAILORDER'
and oh.order_id=ol.order_id
and oh.postcode=PC
and oh.name=NM
and CT>1
and ol.sku_id=sku.sku_id
order by
oh.postcode,
oh.name,
oh.order_id
/
