WITH T1 AS (
    select client_id, to_char(dstamp, 'DD-MON-YYYY') date_, user_id, reference_id, 
    case 
        when to_loc_id = 'TRGOOD' then update_qty
        else 0
    end good_stock,
    case 
        when to_loc_id = 'TRSEC' then update_qty
        else 0
    end sec_stock,
    case 
        when to_loc_id = 'TRBAD' then update_qty
        else 0
    end bad_stock
    from inventory_transaction
    where client_id = '&&2'
    and code = 'Return'
    and dstamp between to_date('&&3','DD-MON-YYYY') and to_date('&&4','DD-MON-YYYY')+1
)
select 'Client,Date,User ID,Orers,Good units,Seconds units,Bad units' from dual
union all
select client_id || ',' || date_ || ',' || user_id || ',' || orders || ',' || good_stock || ',' || sec_stock || ',' || bad_stock
from (
    select client_id, date_, user_id, count(*) orders, sum(good_stock) good_stock, sum(sec_stock) sec_stock, sum(bad_stock) bad_stock
    from (
        select client_id, date_, user_id, reference_id, sum(good_stock) good_stock, sum(sec_stock) sec_stock, sum(bad_stock) bad_stock
        from t1
        group by client_id, date_, user_id, reference_id
        order by 1,2
    )
    group by client_id, date_, user_id
    order by 1,2
)
/
