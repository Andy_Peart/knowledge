SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET TRIMSPOOL ON

select 'SKU,Description,Location,Sku Category,Qty' A1 from dual
union all
select sku_id || ',' || description || ',' || location_id || ',' || user_def_type_4 || ',' || qty
from (
    select inv.sku_id, sku.description, inv.location_id, sku.user_def_type_4, sum(inv.qty_on_hand) qty
    from inventory inv inner join sku on inv.sku_id = sku.sku_id and inv.client_id = sku.client_id
    where inv.client_id = 'TU'
    group by inv.sku_id, sku.description, inv.location_id, sku.user_def_type_4
)
/