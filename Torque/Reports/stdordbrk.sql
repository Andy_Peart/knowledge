SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 900
SET TRIMSPOOL ON
SET HEADING ON


select 'Client: &&2, Date from &&3 to &&4'  from dual
union all
select '------------' from dual
union all
select 'Method,Units,Orders' from dual
union all
select dmethod || ',' || units || ',' || orders
from (
select
    case 
    when client_id = 'IS' then
        case 
            when dispatch_method like '%OCS' then 'OCS'
            when dispatch_method = 'ISHERMES48' then 'Standard'
            when dispatch_method = 'ISHERMES24' then 'Next Day'
            else dispatch_method
        end
    when client_id = 'BE' then dispatch_method
    else dispatch_method
    end
    dmethod, sum(order_volume)*10000 units, count(*) orders
from order_header
where client_id = '&&2'
and creation_date between to_date('&&3') and to_date('&&4')+1
group by 
    case 
    when client_id = 'IS' then
        case 
            when dispatch_method like '%OCS' then 'OCS'
            when dispatch_method = 'ISHERMES48' then 'Standard'
            when dispatch_method = 'ISHERMES24' then 'Next Day'
            else dispatch_method
        end
    when client_id = 'BE' then dispatch_method
    else dispatch_method
    end
)
union all
select '------------' from dual
union all
select 'Date,Single orders,Multi orders' from dual
union all
select date_ || ',' || singles || ',' || multis
from (
select A.date_, A.orders singles, B.orders multis
from (
select to_char(creation_date,'YYYY-MM-DD') date_, count(*) orders
from order_header
where client_id = 'IS'
and order_volume = 0.0001
and creation_date between to_date('&&3') and to_date('&&4')+1
group by to_char(creation_date,'YYYY-MM-DD')
) A
left join (
select to_char(creation_date,'YYYY-MM-DD') date_, count(*) orders
from order_header
where client_id = 'IS'
and order_volume > 0.0001
and creation_date between to_date('&&3') and to_date('&&4')+1
group by to_char(creation_date,'YYYY-MM-DD')
) B
on A.date_ = B.date_
)
/