SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Sku,Total Locations,Live Locations' from dual
union all
select "Sku" || ',' || "sCount" || ',' || "pCount" from
(
select "Sku","sCount","pCount"
from(
with itl as (
select sku_id
, from_loc_id as "Location"
, to_char(max(dstamp), 'DD-MON-YYYY HH24:MI') as "Last_Picked"
from inventory_transaction
where client_id = '&&2'
and code = 'Pick'
and update_qty <> 0
group by sku_id
, from_loc_id
)
select i.sku_id as "Sku"
, i.location_id as "Location"
, itl."Last_Picked"
, count(i.location_id)over(partition by i.sku_id) as "sCount"
, count(itl."Last_Picked")over(partition by i.sku_id) as "pCount"
, sum(i.qty_on_hand)    as "Qty"
from inventory i
inner join location l on l.site_id = i.site_id and l.location_id = i.location_id
left join itl on itl.sku_id = i.sku_id and itl."Location" = i.location_id
where i.client_id = '&&2'
and l.loc_type in ('Tag-LIFO','Tag-FIFO')
group by i.sku_id
, i.location_id
, itl."Last_Picked"
)
group by "Sku","sCount","pCount"
order by 1
)
/
--spool off