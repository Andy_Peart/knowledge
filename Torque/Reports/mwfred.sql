SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 300
SET TRIMSPOOL ON

--spool mwfred.csv

select 'SKU,Description,Location,Qty' from DUAL;

select
inv.sku_id||','||
sku.description||','||
--inv.zone_1||','||
inv.location_id||','||
inv.qty_on_hand 
from inventory inv,sku
where inv.client_id = 'MOUNTAIN'
and substr(inv.location_id,1,1) in ('F','G','W')
and inv.sku_id = sku.sku_id
and sku.client_id = 'MOUNTAIN'
order by 1
--inv.zone_1,
--inv.location_id
/

--spool off
