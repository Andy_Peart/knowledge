SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','
SET WRAP OFF


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 320

column A heading 'Completed' format A12
column B heading 'Site' format A12
column C heading 'Client' format A4
column D heading 'Adice ID' format A12
column E heading 'Line ID' format 9999999
column F heading 'GRN' format A10
column G heading 'Supp ID' format A10
column GG heading 'Supp Name' format A32
column H heading 'Product' format A10
column J heading 'Description' format A40
column K heading 'Prima No' format A12
column L heading 'SKU ID' format A18
column N heading 'Size' format A12
column P heading 'PL Season' format A10
column Q heading 'PL Group' format A10
column R heading 'PL Ident' format A20
column S heading 'PL Colour' format A14
column T heading 'PL Size' format A12
column U heading 'Received' format 9999999
column V heading 'Ordered' format 9999999
column W heading 'Discrep' format 9999999

column M format a4 noprint


 
set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DAY, DD MONTH, YYYY') curdate from DUAL;
set TERMOUT ON

select 'Run Date =  '|| to_char(sysdate,'dd/mm/yy  hh24:mi:ss') from dual;

Select 'Completed,Site,Client,Advice ID,Line ID,GRN,Supp ID,Supp Name,Product,Description,Prima No,SKU ID,Size,PL Season,PL Group,PL Ident,PL Colour,PL Size,Received,Ordered,Discrep' from DUAL;

break on D dup

compute sum label 'Order Total' of U V W on D


Select
to_char(ph.finish_dstamp,'DD-MON-YY') A,
ph.site_id B,
ph.client_id C,
ph.pre_advice_id D,
pl.line_id E,
ph.notes F,
ph.supplier_id G,
ph.name GG,
sku1.product_group H,
sku1.description J,
sku1.user_def_type_3 K,
--''''||pl.sku_id||'''' L,
pl.sku_id L,
substr(pl.sku_id,9,3) N,
sku2.User_Def_Type_2 P,
sku2.User_Def_Type_1 Q,
--''''||sku2.User_Def_Type_4||'''' R,
sku2.User_Def_Type_4 R,
sku2.colour S,
sku2.sku_size N,
nvl(pl.qty_received,0) U,
nvl(pl.qty_due,0) V,
nvl(pl.qty_received,0)-nvl(pl.qty_due,0) W
from pre_advice_header ph,pre_advice_line pl,sku sku1,sku sku2
where ph.client_id='T'
and ph.finish_dstamp>sysdate-1
and ph.pre_advice_id=pl.pre_advice_id
and sku1.client_id='T'
and pl.sku_id=sku1.sku_id
and sku2.client_id='TR'
and sku1.ean=sku2.ean
order by
ph.pre_advice_id,
sku1.product_group,
sku1.description,
pl.line_id,
pl.sku_id
/


select '' from dual;
select '' from dual;
select 'End of Report' from dual;
