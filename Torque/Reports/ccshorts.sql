SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON

--break on CC dup on report

--compute sum of Q1 Q2 Q3 Q4 on CC
--compute sum of Q1 Q2 Q3 Q4 on report

--spool ccshorts.csv

select 'Type,Sku,No. of Orders,Qty Ordered,Qty Allocated,Qty Short' from Dual;

select
DECODE(CC,'3','Retail','203','Outlet','299','Pallet Outlet',CC)||','||
SS||','||
Q1||','||
Q2||','||
Q3||','||
Q4
from (select
condition_id CC,
'A' M,
sku_id SS,
count(distinct task_id) Q1,
sum(qty_ordered) Q2,
sum(qty_tasked) Q3,
sum(qty_ordered-nvl(qty_tasked,0)) Q4
from generation_shortage
where client_id='CC'
and trunc(Dstamp)=trunc(sysdate)
group by condition_id,sku_id
union
select
condition_id CC,
'B' M,
'Group Totals' SS,
count(distinct task_id) Q1,
sum(qty_ordered) Q2,
sum(qty_tasked) Q3,
sum(qty_ordered-nvl(qty_tasked,0)) Q4
from generation_shortage
where client_id='CC'
and trunc(Dstamp)=trunc(sysdate)
group by condition_id
union
select
'X' CC,
'X' M,
'Overall Totals' SS,
count(distinct task_id) Q1,
sum(qty_ordered) Q2,
sum(qty_tasked) Q3,
sum(qty_ordered-nvl(qty_tasked,0)) Q4
from generation_shortage
where client_id='CC'
and trunc(Dstamp)=trunc(sysdate)
order by CC,M,SS)
/





--spool off
