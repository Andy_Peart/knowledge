SET FEEDBACK OFF             
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 300
SET TRIMSPOOL ON

--spool mwnewzonenew.csv

--select to_char(SYSDATE, 'DD/MM/YY  HH:MI:SS') curdate from DUAL;

select 'Due Date,Container,LPG,Pre Advice Id,SKU,Description,Size,Colour,Cartons Expected,Units Due,Current Stock' from dual;

select
DTE||','||
CT||','||
LPG||','||
PID||','''||
SKU||''','||
DES||','||
SSZ||','||
SCL||','||
QQ||','||
QU||','||
QS
from (select
trunc(mo.thedate) DTE,
container CT,
PID,
LPG,
SKU,
DES,
SSZ,
SCL,
mo.cartons QQ,
QU,
nvl(QS,0) QS
from mountainorders mo,(select
--trunc(ph.due_dstamp) DTE,
pl.pre_advice_id PID,
sku.user_def_type_8 LPG,
pl.sku_id SKU,
sku.description DES,
sku.sku_size SSZ,
sku.colour SCL,
pl.qty_due QU
from pre_advice_header ph,pre_advice_line pl,sku
where ph.client_id='MOUNTAIN'
--and ph.due_dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and ph.pre_advice_id=pl.pre_advice_id
and pl.client_id='MOUNTAIN'
and sku.client_id = 'MOUNTAIN'
and sku.sku_id=pl.sku_id),(select
sku.user_def_type_8 LPG2,
iv.sku_id SKU2,
sum(qty_on_hand) QS
from inventory iv,sku
where iv.client_id='MOUNTAIN'
and iv.sku_id=sku.sku_id
and sku.client_id='MOUNTAIN'
group by
sku.user_def_type_8,
iv.sku_id)
where mo.cw_order=PID
and mo.thedate between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and LPG=LPG2 (+)
and SKU=SKU2 (+)
group by 
trunc(mo.thedate),
container,
PID,
LPG,
SKU,
DES,
SSZ,
SCL,
mo.cartons,
QU,
QS
order by
DTE,
CT,
PID,
LPG,
SKU)
/

--spool off

