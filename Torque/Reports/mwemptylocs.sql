SET FEEDBACK OFF                 
set verify off
set wrap off
set colsep ','

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 132
set trimspool on


--spool mwemptylocs.csv

select 'Location,Lock Status,Work Zone,Zone 1' from DUAL;

select
location_id ||','||
--site_id ||','||
lock_status ||','||
work_zone ||','||
zone_1
from location
where site_id like '&&2'
and work_zone like '&&3'
and current_volume=0
order by
location_id
/

--spool off
