SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON

COLUMN NEWSEQ NEW_VALUE SEQNO

SET FEEDBACK ON

update inventory_transaction
set uploaded='X'
where client_id = 'WOL'
and code = 'Shipment'
and (uploaded = 'N'
or uploaded is null)
/



SET FEEDBACK OFF

select 'live/WOLITLS'||to_char(SYSDATE, 'YYYYMMDD')||'.csv' NEWSEQ from DUAL;
--select 'live/WOLITLS'||to_char(SYSDATE, 'YYYYMMDD')||'.csv' NEWSEQ from DUAL;



spool &SEQNO

select 'ITL,E,Shipment'||','
--||decode(sign(A1), 1, '+', -1, '-', '+')||','
--||A1||','
||''||','
||''||','
||decode(sign(A2), 1, '+', -1, '-', '+')||','
||A2||','
||AA||'000000,'
||'WOL'||','
||SS||','
||''||','
||''||','
||''||','
||''||','
||RR||','
||''||','
--||CI||','
||''||','
||''||','
||RI||','
||''||','
||''||','
||''||','
||''||','
||''||','
||SI||','
||''||','
||PI||','
||''||','
||OI||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||URL||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||'N'||','
from (select
to_char(it.dstamp, 'YYYYMMDD') AA,
it.client_id CT,
it.sku_id SS,
it.line_id LL,
it.reference_id RR,
it.reason_id RI,
it.site_id SI,
it.pallet_id PI,
it.owner_id OI,
oh.user_def_note_2 URL,
sum(it.original_qty) A1,
sum(it.update_qty) A2
from inventory_transaction it,order_header oh
where it.client_id = 'WOL'
and it.code = 'Shipment'
--and trunc(it.Dstamp)>trunc(sysdate)-1
and it.uploaded = 'X'
and it.reference_id=oh.order_id
and oh.client_id = 'WOL'
group by
to_char(it.dstamp, 'YYYYMMDD'),
it.client_id,
it.sku_id,
it.line_id,
it.reference_id,
it.reason_id,
it.site_id,
it.pallet_id,
it.owner_id,
oh.user_def_note_2)
order by
RR,SS
/


spool off



SET FEEDBACK ON

update inventory_transaction
set uploaded='Y'
where client_id = 'WOL'
and code = 'Shipment'
and uploaded = 'X'
/


commit;
--rollback;






