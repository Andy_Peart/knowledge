SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Sku,Bond,Current/Archive' from dual
union all
select "Sku" || ',' || "Bond" || ',' || "Current/Archive" from
(
select sku_id       as "Sku"
, user_def_type_5   as "Bond"
, 'c'               as "Current/Archive"
from inventory_transaction
where client_id = 'SB'
group by sku_id
, user_def_type_5
union all
select sku_id
, user_def_type_5
, 'a'
from inventory_transaction_archive
where client_id = 'SB'
group by sku_id
, user_def_type_5
)
/
--spool off