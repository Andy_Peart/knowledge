SET FEEDBACK OFF
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240
SELECT 'ISM,E,MOUNTAIN'
  ||','
  || JA
  ||','
  || ',,,,GOOD,'
  ||','
  || JX
  ||','
  || NVL(JB,0)
  ||','
  || NVL(HB,0)
  ||','
  || NVL(GB,0)
  ||','
  || NVL(JC,0)
  ||','
  || NVL(HC,0)
  ||','
  || NVL(GC,0)
  ||','
  || NVL(JB-JC,0)
  ||','
  || NVL(HB-HC,0)
  ||','
  || NVL(GB-GC,0)
  ||','
  || ',+,,'
  ||','
  || sku.description
  ||','
  || 'EUROPE/LONDON'
  ||','
  || sku.colour
  ||','
  || sku.sku_size
  ||','
  || sku.user_def_type_1  
FROM
  (SELECT i.sku_id JA,
    COUNT(*) JX,
    SUM(i.qty_on_hand) JB,
    SUM(i.qty_allocated) JC
  FROM inventory i
  WHERE i.client_id='MOUNTAIN'
  AND i.sku_id NOT LIKE '**%'
  and zone_1 = 'RACK1'
  GROUP BY i.sku_id
  ),
  (SELECT i.sku_id HA,
    SUM(i.qty_on_hand) HB,
    SUM(i.qty_allocated) HC
  FROM inventory i
  WHERE i.client_id='MOUNTAIN'
  AND i.sku_id NOT LIKE '**%'
  AND i.Lock_status='UnLocked'
  and zone_1 = 'RACK1'
  GROUP BY i.sku_id
  ),
  (SELECT i.sku_id GA,
    SUM(i.qty_on_hand) GB,
    SUM(i.qty_allocated) GC
  FROM inventory i
  WHERE i.client_id='MOUNTAIN'
  AND i.sku_id NOT LIKE '**%'
  AND i.Lock_status='Locked'
  and zone_1 = 'RACK1'
  GROUP BY i.sku_id
  ),
  sku
WHERE JA=HA (+)
AND JA  =GA (+)
AND JA  =sku.sku_id
ORDER BY JA 
/