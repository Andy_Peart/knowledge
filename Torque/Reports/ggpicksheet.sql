SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 66
SET LINESIZE 200
SET TRIMSPOOL ON
SET HEADING OFF
SET NEWPAGE 0

--spool SHps.txt

column AA noprint
column R heading 'Order' format a15
column A heading 'SKU' format a17
column A2 heading 'KIT SKU' format a12
column B heading ' ' format a4
column C heading 'Location' format a12
column D format 999999
column D1 format 9999999999
column D2 heading 'Description' format a40
column E heading 'Picked' format A12
column F heading 'Line Id' format 99999
column M noprint
column N noprint

TTITLE  CENTER "GORILLA GLUE KIT PICK SHEET for &&2" RIGHT "Page:" FORMAT 999 SQL.PNO skip 2 -
        LEFT "Order           Line Id SKU ID  Order Qty Kit Qty Actual Pick Kit Sku Id   Description                " skip 1 -
        LEFT "-----           ------- ------  --------- ------- ----------- ----------   -----------                " skip 2


break on row skip 1 on R skip page ON F ON A

select
ol.order_id R,
ol.line_id F,
ol.sku_id||' - '||
nvl(ol.qty_tasked,0) A,
kt.quantity D,
nvl(ol.qty_tasked,0)*kt.quantity D1,
--'   ',
kt.sku_id A2,
CASE
WHEN oh.country like 'GB%' THEN sku.description 
ELSE
DECODE(kt.sku_id,'6044001','** '||substr(sku.description,1,21)||' ** DG NOTE REQ **',sku.description)
END D2
from order_header oh,order_line ol,kit_line kt,sku
where oh.client_id='GG'
and (oh.order_id='&&2'
or oh.consignment='&&2')
and ol.client_id=oh.client_id
and ol.order_id=oh.order_id
and kt.kit_id=ol.sku_id
and kt.client_id='GG'
and kt.sku_id=sku.sku_id
and sku.client_id='GG'
order by R,F
/
