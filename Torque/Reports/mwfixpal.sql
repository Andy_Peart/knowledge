set serveroutput on;
set feedback off;
set linesize 1000

declare
    pragma autonomous_transaction;
    
    v_inv_pallet varchar2(30);
    v_inv_container varchar2(30);
    
    v_mt_pallet varchar2(30);
    v_mt_task varchar2(30);
    v_mt_container varchar2(30);

    cursor get_pallet
    is
    select i.pallet_id
    , i.container_id
    , mt.pallet_id
    , mt.task_id
    , mt.container_id
    from move_task mt
    inner join inventory i on i.client_id = mt.client_id and i.container_id = mt.container_id
    where mt.client_id = 'MOUNTAIN'
    and mt.pallet_id <> i.pallet_id
    and mt.container_id = i.container_id
    group by mt.pallet_id
    , mt.container_id
    , i.pallet_id
    , i.container_id
    , mt.task_id
    order by i.pallet_id;

begin
    open get_pallet;
    loop
        fetch get_pallet into v_inv_pallet, v_inv_container, v_mt_pallet, v_mt_task, v_mt_container;
            if get_pallet%NotFound then exit;
            end if;
        dbms_output.put_line('Updated move task pallet from '||v_mt_pallet||' to '||v_inv_pallet||' in container '||v_inv_container||';');
        
        update move_task set pallet_id = v_inv_pallet where client_id = 'MOUNTAIN' and container_id = v_inv_container and pallet_id = v_mt_pallet and task_id = v_mt_task and container_id = v_mt_container;
        commit;
        
    end loop;
    close get_pallet;
    
end;
/