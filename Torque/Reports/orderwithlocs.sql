SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

select 'Consignment,Order,Sku,Picked from,Qty Picked,Qty on hand at this location' from dual
union all
select "Consignment" || ',' || "Order" || ',' || "Sku" || ',' || "Picked from" || ',' || "Qty Picked" || ',' || "Qty on hand at this location" from
(
with t1 as
(
select consignment
, reference_id
, sku_id
, from_loc_id
, sum(update_qty) as "Picked"
, client_id
from inventory_transaction
where client_id = 'MOUNTAIN'
and dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and update_qty <> 0
and from_loc_id <> 'CONTAINER'
and code = 'Pick'
group by consignment
, reference_id
, sku_id
, from_loc_id
, client_id
),t2 as
(
select sku_id
, client_id
, location_id
, sum(qty_on_hand) as "Stock"
from inventory
where client_id = 'MOUNTAIN'
and location_id not in ('MAIL ORDER','DESPATCH','CONTAINER')
group by sku_id
, client_id
, location_id
)
select t1.consignment   as "Consignment"
, t1.reference_id       as "Order"
, t1.sku_id             as "Sku"
, t1.from_loc_id        as "Picked from"
, t1."Picked"           as "Qty Picked"
, nvl(t2."Stock",0)     as "Qty on hand at this location"
from t1 left join t2 on t2.sku_id = t1.sku_id and t2.client_id = t1.client_id and t2.location_id = t1.from_loc_id
order by 3,4,2
)
/

--spool off