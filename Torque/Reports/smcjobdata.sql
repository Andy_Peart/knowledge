SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 200
SET TRIMSPOOL ON


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


--break on MM skip 2

--compute sum label ',Totals' of C1 C2 D1 D2 D3 E E2 on report

--spool smcjobdata.csv


select 'Client,Job ID,Date,User Name,Start Job,Stop Job,Elapsed Time,Calculated Time' from DUAL;

select
CL||','||
JJ1||','||
trunc(MM)||','||
J11||','||
MM1||','||
MM2||','||
FF||','||
--FF||'hrs '||MS||'mins'||','||
CASE
WHEN FF<2
THEN FF ELSE FF2
--THEN FF||'hrs '||MS||'mins'
--ELSE FF2||'hrs '||MS2||'mins'
END
from (select
CL,
MM,
J11,
MM1,
MM2,
JJ1,
to_char((FF/60),'990.99') FF,
--trunc(FF/60) FF,
--FF-(trunc(FF/60)*60) MS,
to_char((FF*0.88/60),'90.99') FF2,
--trunc(FF*0.88/60) FF2,
--trunc(FF*0.88-(trunc(FF*0.88/60)*60)) MS2,
JJC
from (select
CL,
MM,
J11,
MM1,
MM2,
JJ1,
((to_number(substr(MM2,1,2))*60)+to_number(substr(MM2,4,2)))-((to_number(substr(MM1,1,2))*60)+to_number(substr(MM1,4,2))) FF,
substr(JJ1,3,2) JJC
from (select
CL,
MM1 MM,
--UU1 J11,
BBB J11,
to_char(MM1,'HH24:MI') MM1,
CASE
WHEN trunc(MM1)=trunc(MM2)
THEN to_char(MM2,'HH24:MI')
ELSE '16:30'
END MM2,
JJ1,
--NN1,
UN CC,
nvl(UQ,0) JA1
from (select
distinct
it.client_id CL,
--substr(it.job_id,3,6) JJ1,
it.job_id JJ1,
it.Dstamp MM1,
--substr(it.notes,1,12) NN1,
it.user_id UU1,
au.name BBB,
rank() over(partition by it.job_id,it.user_id order by it.Dstamp) as rank1
from inventory_transaction it,application_user au
where it.client_id in ('ECHO','OB','SH','HU','CK','OPS', 'NB', 'SUG')
--and it.Dstamp between to_date('&&2', 'DD-MON-YYYY')-1 and to_date('&&3', 'DD-MON-YYYY')+1
and code like 'Job Start'
and it.user_id=au.user_id),(select
distinct
--substr(it.job_id,3,6) JJ2,
it.job_id JJ2,
it.Dstamp MM2,
--substr(it.notes,1,12) NN2,
it.user_id UU2,
DECODE(it.update_qty,1,0,0,0,1) UN,
DECODE(it.update_qty,1,0,it.update_qty) UQ,
rank() over(partition by it.job_id,it.user_id order by it.Dstamp) as rank2
from inventory_transaction it
where it.client_id in ('ECHO','OB','SH','HU','CK','OPS', 'NB', 'SUG')
--and it.Dstamp between to_date('&&2', 'DD-MON-YYYY')-1 and to_date('&&3', 'DD-MON-YYYY')+1
and code like 'Job End')
where rank1=rank2
and UU1=UU2
and JJ1=JJ2
--and trunc(MM1)=trunc(MM2)
and MM1 between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by CL,BBB,MM1,MM2,JJ1,UN,nvl(UQ,0)))
order by CL,JJ1,MM)
/

--spool off
