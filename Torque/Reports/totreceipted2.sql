SET FEEDBACK OFF                 
set pagesize 68
set linesize 160
set verify off

clear columns
clear breaks
clear computes

column D heading 'Receipt Date' format a16
column F heading 'Line ID' format 99999999
column A heading 'SKU ID' format a20
column E heading 'Description' format a40
column B heading 'Ordered' format 999999
column C heading 'Received' format 999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'Total Units Receipted on Pre-Advice ID &&2 - for client ID &&3 as at ' report_date skip 2

break on report

compute sum label 'Totals' of B C on report

select
to_char(it.Dstamp, 'DD/MM/YY') D,
it.line_id F,
ph.sku_id A,
sku.description E,
ph.qty_due B,
it.update_qty C
from inventory_transaction it,pre_advice_line ph,sku
where it.client_id='&&3'
and it.station_id not like 'Auto%'
and it.code like 'Receipt%'
and it.reference_id=ph.pre_advice_id
and it.reference_id='&&2'
and it.line_id=ph.line_id
and ph.sku_id=sku.sku_id (+)
order by
it.line_id
/
