SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 500
SET NEWPAGE 1
SET LINESIZE 500
SET TRIMSPOOL ON
 

column A heading 'Product Group' format A13
column B heading 'User Def Type 5' format A15
column C heading 'Qty on Hand' format 99999999
column D heading 'Qty Allocated' format 99999999
column E heading 'Location Count' format 999999
column F heading 'Sku Count' format 999999

   
select s.product_group A
, s.user_def_type_5 B
, sum(i.qty_on_hand) C
, sum(i.qty_allocated) D
, count(distinct i.tag_id) E
, count(distinct i.sku_id) F
from inventory i
inner join sku s on s.client_id = i.client_id and s.sku_id = i.sku_id
inner join location l on l.site_id = i.site_id and l.location_id = i.location_id
where i.client_id = 'GG'
and s.product_group = 'PACKAGING'
group by s.product_group
, s.user_def_type_5
;
column A heading 'Product Group' format A13
column B heading 'User Def Type 5' format A15
column C heading 'Qty on Hand' format 99999999
column D heading 'Qty Allocated' format 99999999
column E heading 'Location Count' format 999999
column F heading 'Sku Count' format 999999

   
select s.product_group A
, s.user_def_type_5 B
, sum(i.qty_on_hand) C
, sum(i.qty_allocated) D
, count(distinct i.tag_id) E
, count(distinct i.sku_id) F
from inventory i
inner join sku s on s.client_id = i.client_id and s.sku_id = i.sku_id
inner join location l on l.site_id = i.site_id and l.location_id = i.location_id
where i.client_id = 'GG'
and s.product_group = 'STOCK'
group by s.product_group
, s.user_def_type_5
/