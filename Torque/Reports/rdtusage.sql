SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Date last used,RDT number,Client last used,Client Code' from dual
/* TORQUE - (STD) - RDT by Client */
union all
select "Date" ||','|| "RDT" ||','|| "Client" ||','|| "ID"
from
(
select to_char(max(it.dstamp), 'DD-MON-YYYY HH24:MI')    as "Date"
, it.station_id                 as "RDT"
, c.name                        as "Client"
, it.client_id                  as "ID"
from inventory_transaction it
inner join client c on c.client_id = it.client_id
where it.dstamp between to_date('&&2', 'DD-MON-YYYY') and sysdate
and substr(it.station_id,1,3) = 'RDT'
--and it.code in ('Stock Check','Allocate','Marshal','Receipt','Relocate','Job Start','Job End','Pick','Inv Lock','Inv UnLock','Repack','PreAdv Status','Consol','PreAdv Line','Return') /* Comment out to lock down */
and it.client_id is not null
group by it.station_id
, c.name
, it.client_id
order by to_char(max(it.dstamp), 'DD-MON-YYYY HH24:MI') desc
, it.station_id
)
/
--spool off