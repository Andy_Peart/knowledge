SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

Select 'Date,Type,PO/SO Number,Product,Description,EAN,Qty,Name,Address 1,Address 2,Post Code,Batch,Expiry' from DUAL; 
 
select
A||','||
B||','||
C||','||
D||','||
DESCR||','''||
EAN||''','||
nvl(QQ,0)||','||
P||','||
Q||','||
R||','||
S||','||
UserDef1||','||
UserDef2
from (select
trunc(itl.Dstamp) A,
'Shipment' B,
itl.reference_id C,
itl.sku_id D,
sku.user_def_note_1 DESCR,
sku.ean EAN,
sum(DECODE(itl.code, 'UnShipment', 0 - itl.update_qty, itl.update_qty)) QQ,
oh.name P,
oh.address1 Q,
oh.address2 R,
oh.postcode S,
itl.user_def_type_1 UserDef1,
itl.user_def_type_2 UserDef2 
from inventory_transaction itl, sku, order_header oh
where itl.client_id = 'EX'
and itl.code in ('Shipment', 'UnShipment')
and itl.Dstamp > sysdate - 30
and oh.client_id = 'EX'
and itl.reference_id = oh.order_id
and itl.client_id = sku.client_id
and itl.sku_id = sku.sku_id
group by
trunc(itl.Dstamp),
itl.reference_id,
itl.sku_id,
sku.user_def_note_1,
sku.ean,
oh.name,
oh.address1,
oh.address2,
oh.postcode,
itl.user_def_type_1,
itl.user_def_type_2
union
select
trunc(itl.Dstamp) A,
'Receipt' B,
itl.reference_id C,
itl.sku_id D,
sku.user_def_note_1 DESCR,
sku.ean EAN,
sum(DECODE(itl.code, 'Receipt', itl.update_qty, 0 - itl.update_qty)) QQ,
' ' P,
' ' Q,
' ' R,
' ' S,
itl.user_def_type_1 UserDef1,
itl.user_def_type_2 UserDef2 
from inventory_transaction itl,sku
where itl.client_id = 'EX'
and itl.code like 'Receipt%'
and itl.Dstamp > sysdate - 30
and itl.client_id = sku.client_id
and itl.sku_id = sku.sku_id
group by
trunc(itl.Dstamp),
itl.reference_id,
itl.sku_id,
sku.user_def_note_1,
sku.ean,
itl.user_def_type_1,
itl.user_def_type_2
union
select
trunc(itl.Dstamp) A,
itl.code B,
itl.reference_id C,
itl.sku_id D,
sku.user_def_note_1 DESCR,
sku.ean EAN,
itl.update_qty QQ,
' ' P,
' ' Q,
' ' R,
' ' S,
itl.user_def_type_1 UserDef1,
itl.user_def_type_2 UserDef2 
from inventory_transaction itl, sku
where itl.client_id = 'EX'
and itl.code in ('Adjustment', 'Stock Check')
and itl.Dstamp > sysdate - 30
and itl.client_id = sku.client_id
and itl.sku_id = sku.sku_id)
order by 
B, A, C, D 
/
