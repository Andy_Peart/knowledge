SET FEEDBACK OFF                 
set pagesize 68
set newpage 0
set linesize 160
set verify off

clear columns
clear breaks
clear computes


column A heading 'Container' format A16
column B heading 'Store' format A10
column C heading 'Name' format A40
column D heading 'Consignment' format A12
column E heading 'Units' format 999999
column F heading 'SKU' format A18
column G heading 'Description' format A40
column J heading 'TopToToe' format A10


set heading off


--select 'Shipments for Consignment &&2' from DUAL;
 
set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DAY, DD MONTH, YYYY') curdate from DUAL;
set TERMOUT ON


set heading on

ttitle  LEFT 'Shipments for Week Commencing &&3' RIGHT 'Page:' FORMAT 999 SQL.PNO skip 2

--break on A skip page dup on J skip 1 dup on report 
break on A skip page on J skip 1 on report 

compute sum label 'Total' of E on A
compute sum label 'Grand Total' of E on report

select
it.container_id A,
sku.user_def_type_1 J,
it.sku_id F,
sku.description G,
sum(it.update_qty) E
from inventory_transaction it,sku
where it.client_id='MOUNTAIN'
and it.code='Shipment'
and it.work_group='&&2'
and it.container_id is not null
and trunc(it.Dstamp) between to_date('&&3', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.sku_id=sku.sku_id
and sku.client_id='MOUNTAIN'
group by
it.container_id,
sku.user_def_type_1,
it.sku_id,
sku.description
order by
A,J,F
/

