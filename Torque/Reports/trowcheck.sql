SET FEEDBACK OFF             
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 148
SET TRIMSPOOL ON

column A format a12
column B format a8
column C format a12
column D format 9990.99
column E format 999999
column F format a12
column G format a8


COLUMN LWD NEW_VALUE LASTWD

select DECODE(to_char(sysdate,'DAY'),'MONDAY   ',trunc(sysdate-3),trunc(sysdate-1)) LWD from DUAL;


break on G dup skip 1 on F dup skip 1

--compute sum label 'Totals' of D E on B

--spool tmlnew.csv

select 'Date,Area,Category,Order ID,Country,Packages,Weight'  from dual;

select
A,
G,
DECODE(JJ,'Track','Tracking',(DECODE(sign(2-D),'-1','Over= 2kg','Under 2kg'))) F,
B,
C,
count(*) E,
sum(D) D
from (select
distinct
CASE
WHEN oh.user_def_chk_2='Y' THEN 'Track'
ELSE ' ' END JJ,
trunc(sm.SHIPPED_DSTAMP) A,
oh.user_def_type_4 G,
sm.ORDER_ID B,
oh.country C,
sm.pallet_id,
to_number(sm.TRAILER_POSITION) D
from shipping_manifest sm, order_header oh
where sm.client_id='T'
and sm.SHIPPED_DSTAMP>to_date('&LASTWD','DD/MM/YY')
and sm.TRAILER_POSITION is not null
and sm.order_id=oh.order_id
and oh.client_id='T')
group by
A,
G,
DECODE(JJ,'Track','Tracking',(DECODE(sign(2-D),'-1','Over= 2kg','Under 2kg'))),
B,
C
order by A,G,F,B
/

--spool off
--set termout on
