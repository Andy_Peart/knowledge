SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 66
SET NEWPAGE 1
SET LINESIZE 9999
SET TRIMSPOOL OFF

column A heading 'Order ID' format A15
column B heading 'SKU' format A15
column C heading 'Qty ordered' format 999999
column D heading 'Qty allocated' format 999999
column E heading 'Shortage' format 999999
column G heading 'Description' format A65

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

break on report
compute sum label 'Total' of E on report

ttitle 'Shortage Report - 'report_date'                      Consignment - '&&3

select gs.task_id A, gs.sku_id B, nvl(gs.qty_ordered,0) C, nvl(gs.qty_tasked,0) D, nvl(gs.qty_ordered,0)-nvl(gs.qty_tasked,0) E, s.description G
from generation_shortage gs inner join order_header oh on gs.client_id = oh.client_id and gs.task_id = oh.order_id
inner join SKU s on s.sku_id = gs.sku_id and s.client_id = gs.client_id
where gs.client_id = '&&2'
and oh.consignment = '&&3'
order by 1
/

--spool off

