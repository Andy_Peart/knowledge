SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Sku id,Description,Location,Qty in Location' from dual
union all
select sku_id||','||description||','||location_id||','||qty
from
(
    select sku_id
    , description
    , location_id
    , qty
    from
    (
        select i.sku_id
        , s.description
        , i.location_id
        , sum(i.qty_on_hand) qty
        from inventory i
        inner join location l on l.site_id = i.site_id and l.location_id = i.location_id
        inner join sku s on s.client_id = i.client_id and s.sku_id = i.sku_id
        where i.client_id = '&&2'
        and l.loc_type in ('Tag-FIFO', 'Tag-LIFO')
        and l.disallow_alloc = 'N'
        group by i.sku_id
        , s.description
        , i.location_id
    )
    where qty <= '&&3'
    order by sku_id
)
/
--spool off