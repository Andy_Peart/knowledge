SET PAGESIZE 62    
SET NEWPAGE 0
SET LINESIZE 132
SET verify off 

clear columns
clear breaks

ttitle left "Mountain Warehouse - Order Details." skip 4

column A heading 'ORDER' format a8
column B heading 'SKU' format a15
column C heading 'Qty ORDERED' format 9999999999
column D heading 'LOC' format a5
column E heading 'QTY IN LOC' format 9999999999
column F heading 'Store' format a10

break on oh.order_ID skip 1

select oh.order_id A, ol.sku_id B, ol.qty_ordered C, inv.location_id D, inv.qty_on_hand E, oh.name F
from order_header oh, order_line ol, inventory inv
where oh.client_id = 'MOUNTAIN'
and oh.status = 'In Progress'
and oh.order_id = ol.order_id
and ol.sku_id = inv.sku_id
and inv.tag_id is null
and inv.location_id <> 'MARSHAL'
and oh.order_id = ('&&2')
order by 4
/
