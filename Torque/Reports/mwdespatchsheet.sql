SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET LINESIZE 200
SET TRIMSPOOL ON
SET HEADING OFF





column AA noprint
column A format a9
column B format a10
column C format a12
column D format a18
column E format a40
column F format 999999
column G format 999999
column H format a10
column J format a10

break on report skip page

--spool mwtext.txt

select 'CONTAINERS SUMMARY for Order &&2' from DUAL;
select ' ' from DUAL;

select 'Container       Lines     Qty' from DUAL;


select
container_id B,
count(*),
sum(Qty_shipped) G
from shipping_manifest
where client_id='MOUNTAIN'
and order_id='&&2'
group by
container_id 
order by B
/

select ' ' from DUAL;

select
'No. of Containers = '||count(distinct container_id) E
from shipping_manifest
where client_id='MOUNTAIN'
and order_id='&&2'
/
select ' ' from DUAL;


--select '              ORDER &&2' from DUAL;
--select ' ' from DUAL;
select 'Customer  Container  Order        SKU                Description                                 Line     Qty   Date     User' from DUAL;
select '--------  ---------  -----        ---                -----------                                 ----     ---   ----     ----' from DUAL;
select ' ' from DUAL;

--break on row skip 1 on report

--compute sum label 'Total' of D D2 D4 on report


select
sm.Customer_id A,
sm.Container_id B,
sm.Order_id C,
sm.SKU_id D,
sku.description E,
sm.Line_id F,
sm.Qty_shipped G,
trunc(sysdate) H,
sm.user_id J
from shipping_manifest sm,sku
where sm.client_id='MOUNTAIN'
and sm.order_id='&&2'
and sm.sku_id=sku.sku_id
and sku.client_id='MOUNTAIN'
order by 
sm.Container_id,
sm.Order_id,
sm.SKU_id 
/


--spool off
