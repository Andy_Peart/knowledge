set pagesize 68
set linesize 120
set verify off

ttitle 'Mountain Warehouse - Stock Adjustment on between '&&2' and '&&3'' skip 2

column A heading 'Date' format a8
column B heading 'SKU' format a15
column C heading 'Qty Adjusted' format 999999
column D heading 'By User' format a10
column E heading 'Reason Code' format a60

select to_char(inv.DSTAMP, 'DD/MM/YY') A,
inv.SKU_ID B,
inv.UPDATE_QTY C,
inv.USER_ID D,
ar.notes E
from inventory_transaction inv, adjust_reason ar
where inv.client_id = 'MOUNTAIN'
and inv.code = 'Adjustment'
and inv.reason_id = ar.reason_id
and inv.update_qty > 0
and inv.dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY') 
/
