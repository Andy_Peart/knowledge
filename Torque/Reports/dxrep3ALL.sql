SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','
SET UND OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 320
SET TRIMSPOOL ON




column A1 format A18
column A format A18
column B format A40
column Z format A12
column C format 9999999
column D format 9999999
column E format 9999999
column F format 9999999
column G format 9999999
column H format 9999999
column J format 9999999
column K format 9999999
column L format 9999999
column N format 9999999
column P format 9999999
column Q format 9999999
column V format 9999999
column W format 9999999


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



break on report

compute sum label 'Total'  of C D E J K G N P Q F V W on report

--set termout off
--spool dxrep3all.csv

select 'Full Comparison_Report - '||to_char(SYSDATE, 'DD/MM/YY  HH24:MI:SS') from DUAL;
select 
distinct 
'Report Date: '||to_char(SYSDATE, 'DD/MM/YY  HH:MI')||',Last Update: '||to_char(the_date,'DD/MM/YY HH:MI')
from dx_skufile;

select 'Category,Product,Description,Dax_qty,Elite_qty,Difference,        ,Elite_Pick,Elite_Alloc,Elite_Hold,Elite_TV,Elite_Transit,Elite_REJ1,Elite_Susp,Elite_Free,Elite_True_Diff' from DUAL;
--                                     DXQTY     QQQ                           Q4         Q5           Q2        TV        TRT          QTN        Q1      
select 
sku.product_group A1,
sku.sku_id A,
sku.description B,
nvl(DXQTY,0) C,
nvl(QQQ,0) D,
nvl(DXQTY,0)-nvl(QQQ,0) E,
' ',
nvl(Q4,0) J,
nvl(Q5,0) K,
nvl(Q2,0) G,
nvl(TV,0) N,
nvl(TRT,0) P,
nvl(QTN,0) Q,
nvl(Q1,0) F, 
nvl(QQQ,0)-nvl(Q4,0)-nvl(Q5,0)-nvl(Q2,0)-nvl(TV,0)-nvl(TRT,0)-nvl(QTN,0) V,
nvl(DXQTY,0)-(nvl(QQQ,0)-nvl(Q4,0)-nvl(Q5,0)-nvl(Q2,0)-nvl(TRT,0)+nvl(Q1,0)) W
--nvl(DXQTY,0)-nvl(QQQ,0)+nvl(Q4,0)+nvl(Q5,0)+nvl(TV,0)+nvl(QTN,0) w
from sku,(Select 
sku_id SSS,
sum(qty_on_hand) QQQ
from inventory
where client_id='DX'
group by sku_id),(Select 
sku_id S5,
sum(qty_allocated) Q5
from inventory
where client_id='DX'
group by sku_id),(Select 
sku_id S1,
sum(qty_on_hand) Q1
from inventory
where client_id='DX'
and location_id='SUSPENSE'
group by sku_id),(Select 
sku_id STV,
sum(qty_on_hand) TV
from inventory
where client_id='DX'
and zone_1 in ('QVC','IDEAL')
and (condition_id<>'HOLD' or condition_id is null)
group by sku_id),(Select 
sku_id STR,
sum(qty_on_hand) TRT
from inventory
where client_id='DX'
and location_id in  ('DXBRET','DXBREJ')
and (condition_id<>'HOLD' or condition_id is null)
group by sku_id),(Select 
sku_id SQT,
sum(qty_on_hand) QTN
from inventory
where client_id='DX'
and location_id in  ('DXBREJ1')
and (condition_id<>'HOLD' or condition_id is null)
group by sku_id),(Select 
sku_id S4,
sum(qty_on_hand) Q4
from inventory
where client_id='DX'
and location_id='CONTAINER'
group by sku_id),(Select 
sku_id S2,
sum(qty_on_hand) Q2
from inventory
where client_id='DX'
and condition_id='HOLD'
group by sku_id),(Select 
sku_id S3,
sum(qty_on_hand) Q3
from inventory
where client_id='DX'
and condition_id='RETURN'
group by sku_id),(select 
THE_SKU,
sum(the_qty) DXQTY 
from dx_skufile 
group by the_sku)
where sku.client_id='DX'
and sku.sku_id not like '*%'
and sku.sku_id=SSS (+) 
and sku.sku_id=S5 (+)
and sku.sku_id=S1 (+)
and sku.sku_id=STV (+)
and sku.sku_id=STR (+)
and sku.sku_id=SQT (+)
and sku.sku_id=S4 (+)
and sku.sku_id=S2 (+)
and sku.sku_id=S3 (+)
and sku.sku_id=THE_SKU (+)
and (QQQ<>0 or DXQTY<>0)
order by
A
/


 
--spool off
--set termout on
