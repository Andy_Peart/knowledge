SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','
SET UND OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 320


column A format A8
column B format A10
column C format A10
column D format A10
column E format A16
column F format A42
column G format A12
column H format A18
column I format 9999999
column J format 9999999
column K format 9999999
column K1 format 9999999
column K2 format 9999999
column L format 9999999
column M format 9999999
column N format 9999999
column O format 9999999
column P format 9999999
column Q format 9999999
column R format 9999999
column S format 9999999
column T format 9999999
column T2 format 9999999
column U format 9999999
column V format 9999999
column W format 9999999
column X format 9999999
column Y format 9999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



break on report

compute sum label 'Totals'  of I J K K1 K2 L M N O P Q R S T T2 U V W X Y  on report

--spool bmkc.csv

Select 'Group,Season,Style,Quality,Product Code,Description,Size,SKU ID,All Stock,Obscure,QCHOLD,Locked,Seconds,Picked,HOLD,Allocated,SUSPENSE,Available,SECONDS,GAR-LOCKED,GAR-SUSPENSE,GAR-HOLD,GAR-CUST-ALLOC,RET-ALLOC-STOCK,RETAIL-STOCK,CIMS-TOTAL,Difference,RP Transit' from DUAL;

select
SKU.PRODUCT_GROUP A,
SKU.USER_DEF_TYPE_2 B,
SKU.USER_DEF_TYPE_1 C,
SKU.USER_DEF_TYPE_4 D,
SKU.USER_DEF_TYPE_3 E,
'"' || sku.description || '"' F,
'''' || sku.sku_size || '''' G,
'''' || sku.sku_id || '''' H,
nvl(JAA,0) I,
nvl(HB,0) J,
nvl(KC,0) K,
nvl(KC1,0) K1,
nvl(KC2,0) K2,
nvl(LD,0) L,
nvl(ME,0) M,
nvl(JF,0) N,
nvl(NG,0) O,
nvl(JAA-nvl(HB,0)-nvl(KC,0)-nvl(KC1,0)-nvl(KC2,0)-nvl(LD,0)-nvl(ME,0)-nvl(JF,0)-nvl(NG,0),0) P,
nvl(QG,0) Q,
nvl(RG,0) R,
nvl(SG,0) S,
nvl(TG,0) T,
nvl(TG2,0) T2,
nvl(UG,0) U,
nvl(VG,0) V,
nvl(QG,0)+nvl(RG,0)+nvl(SG,0)+nvl(TG,0)+nvl(TG2,0)+nvl(UG,0)+nvl(VG,0) W,
nvl(JAA-(nvl(QG,0)+nvl(RG,0)+nvl(SG,0)+nvl(TG,0)+nvl(TG2,0)+nvl(UG,0)+nvl(VG,0)),0) X,
nvl(WB,0) Y
from 
(select
i.sku_id JA,
count(*) JX,
sum(i.qty_on_hand) JAA,
sum(i.qty_allocated) JF
from inventory i
where i.client_id='TR'
and i.sku_id not like '**%'
group by
i.sku_id),
(select
i.sku_id HA,
sum(i.qty_on_hand) HB
from inventory i
where i.client_id='TR'
and i.sku_id not like '**%'
and i.Condition_id not in ('QCHOLD','HOLD','Allocated','GOOD')
group by
i.sku_id),
(select
i.sku_id KA,
sum(i.qty_on_hand) KC
from inventory i
where i.client_id='TR'
and i.sku_id not like '**%'
and i.Condition_id in ('QCHOLD')
group by
i.sku_id),
(select
i.sku_id KA1,
sum(i.qty_on_hand) KC1
from inventory i
where i.client_id='TR'
and i.sku_id not like '**%'
and i.lock_status='Locked'
and nvl(i.lock_code,' ')<>'LOCK'
and nvl(i.location_id,' ')<>'SUSPENSE'
group by
i.sku_id),
(select
i.sku_id KA2,
sum(i.qty_on_hand) KC2
from inventory i
where i.client_id='TR'
and i.sku_id not like '**%'
and i.lock_code in ('LOCK')
group by
i.sku_id),
(select
i.sku_id LA,
sum(i.qty_on_hand) LD
from inventory i
where i.client_id='TR'
and i.sku_id not like '**%'
and (i.location_id like 'CONT%' or i.location_id like 'SHIP%')
group by
i.sku_id),
(select
i.sku_id MA,
sum(i.qty_on_hand) ME
from inventory i
where i.client_id='TR'
and i.sku_id not like '**%'
and i.Condition_id='HOLD'
group by
i.sku_id),
(select
i.sku_id NA,
sum(i.qty_on_hand) NG
from inventory i
where i.client_id='TR'
and i.sku_id not like '**%'
and i.location_id='SUSPENSE'
group by
i.sku_id),
(select
sku_id QA,
sum(qty) QG
from v_ism_pl_pl
where stock_type='GAR-QC-HOLD'
and v_loc_num=90003
group by sku_id),
(select
sku_id RA,
sum(qty) RG
from v_ism_pl_pl
where stock_type='GAR-LOCKED'
and v_loc_num=90003
group by sku_id),
(select
sku_id SA,
sum(qty) SG
from v_ism_pl_pl
where stock_type='GAR-SUSPENSE'
and v_loc_num=90003
group by sku_id),
(select
sku_id TA,
sum(qty) TG
from v_ism_pl_pl
where stock_type='GAR-HOLD'
and v_loc_num=90003
group by sku_id),
(select
sku_id TA2,
sum(qty) TG2
from v_ism_pl_pl
where stock_type='GAR-CUST-ALLOC'
and v_loc_num=90003
group by sku_id),
(select
sku_id UA,
sum(qty) UG
from v_ism_pl_pl
where stock_type='RET-ALLOC-STOCK'
and v_loc_num=90003
group by sku_id),
(select
sku_id VA,
sum(qty) VG
from v_ism_pl_pl
where stock_type='RETAIL-STOCK'
and v_loc_num=90003
group by sku_id),
(select
sku_id WA,
nvl(sum(qty_received),0) WB
from pre_advice_header ph,pre_advice_line pl
where ph.client_id='TR'
and ph.status='In Progress'
and ph.pre_advice_id=pl.pre_advice_id
group by
sku_id),sku
where sku.client_id='TR'
and JA=sku.sku_id
and JA=HA (+)
and JA=KA (+)
and JA=KA1 (+)
and JA=KA2 (+)
and JA=LA (+)
and JA=MA (+)
and JA=NA (+)
and JA=QA (+)
and JA=RA (+)
and JA=SA (+)
and JA=TA (+)
and JA=TA2 (+)
and JA=UA (+)
and JA=VA (+)
and JA=WA (+)
order by
SKU.PRODUCT_GROUP,
SKU.USER_DEF_TYPE_2,
SKU.USER_DEF_TYPE_1,
SKU.USER_DEF_TYPE_4,
SKU.USER_DEF_TYPE_3,
SKU.SKU_SIZE 
/

--spool off

