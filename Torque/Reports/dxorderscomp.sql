SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','
SET UND OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 320
SET TRIMSPOOL ON



column A format A24
column B format A40
column C format A14
column D format A28
column E format A12
column F format A12
column G format A24
column H format A32
column J format 99999
column K format 99999
column L format 99999
column M format 99999
column N format 99999


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



break on report

compute sum label 'Total'  of N J K on report

spool dxo1.csv

--select 'DAXBOURNE - Complete Orders '||to_char(SYSDATE, 'DD/MM/YY  HH24:MI:SS') from DUAL;

select 'Order ID,Order Type,Orders,Lines,Order Qty,Source' from DUAL;


Select
oh.Order_ID A,
oh.Order_type C,
count(distinct oh.Order_ID) N,
oh.num_lines J,
sum(ol.qty_ordered) K,
oh.user_def_type_1 B
from order_header oh,order_line ol
where oh.client_id='DX'
and oh.status='Complete'
and oh.order_id=ol.order_id
and ol.client_id='DX'
group by
oh.Order_ID,
oh.Order_type,
oh.num_lines,
oh.user_def_type_1
order by
1
/

spool off