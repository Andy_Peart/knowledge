SET FEEDBACK OFF           
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 300
SET TRIMSPOOL ON



column M heading '' format a4 noprint
column D heading 'Date' format a12
column A heading 'SKU ID' format a18
column B heading '+Adjustment' format 99999999
column F heading '-Adjustment' format 99999999
column C heading 'Description' format A40

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



break on report

compute sum label 'Totals' of Q4 B4 F4 G4 on report

--spool mwzonereport.csv
select 'Report for &&2 to &&3' from DUAL;
select 'Zone,Stock,Adjustment Up,Adjustment Down,Total Movement,Accuracy %' from DUAL;


select
--to_number(R4) R5,
R4 R5,
Q4,
nvl(B,0) B4,
0-nvl(F,0) F4,
nvl(B,0)+nvl(F,0) G4,
to_char((((nvl(B,0)+nvl(F,0))*100)/Q4),'990.000') H4
from (select
A4,
sum(B) B,
sum(F) F
from (select
A4,
B,
F
from
(select
nvl(WZ,ll.work_zone) A4,
sum(it.update_qty) as B,
0 as F
from inventory_transaction it,location ll,(select
distinct
it.sku_id SKU,
ld.work_zone WZ
from inventory_transaction it,location ld
where it.client_id='MOUNTAIN'
and it.code='Stock Check'
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
--and trunc(it.Dstamp) = trunc(sysdate)-1
and it.from_loc_id=ld.location_id
and it.site_id=ld.site_id)
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
--where trunc(it.Dstamp) = trunc(sysdate)-1
and it.client_id='MOUNTAIN'
and it.code='Adjustment'
and it.from_loc_id=ll.location_id
and it.site_id=ll.site_id
and it.sku_id=SKU (+)
and it.update_qty>0
group by
nvl(WZ,ll.work_zone))
--where B>0
union
select
A4,
B,
F
from
(select
nvl(WZ,ll.work_zone) A4,
0 as B,
sum(it.update_qty) as F
from inventory_transaction it,location ll,(select
distinct
it.sku_id SKU,
ld.work_zone WZ
from inventory_transaction it,location ld
where it.client_id='MOUNTAIN'
and it.code='Stock Check'
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
--and trunc(it.Dstamp) = trunc(sysdate)-1
and it.from_loc_id=ld.location_id
and it.site_id=ld.site_id)
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
--where trunc(it.Dstamp) = trunc(sysdate)-1
and it.client_id='MOUNTAIN'
and it.code='Adjustment'
and it.from_loc_id=ll.location_id
and it.site_id=ll.site_id
and it.sku_id=SKU (+)
and it.update_qty<0
group by
nvl(WZ,ll.work_zone)))
group by A4),(select
ll.work_zone R4,
sum(iv.qty_on_hand) as Q4
from inventory iv,location ll
where iv.client_id='MOUNTAIN'
and iv.location_id=ll.location_id
and iv.site_id=ll.site_id
group by ll.work_zone)
where R4=A4(+)
order by R5
/

--spool off
