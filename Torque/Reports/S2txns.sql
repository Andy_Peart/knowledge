SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON




set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



--spool s2txns.csv


Select 'Date,Type,PO/SO Number,Product,Description,EAN,Qty,Name,Address 1,Address 2,Post Code' from DUAL;


select
A||','||
B||','||
C||','||
D||','||
DESCR||','''||
EAN||''','||
nvl(QQ,0)||','||
P||','||
Q||','||
R||','||
S
from (select
trunc(itl.Dstamp) A,
'Shipment' B,
itl.reference_id C,
itl.sku_id D,
sku.DESCRIPTION DESCR,
sku.ean EAN,
sum(DECODE(itl.code,'UnShipment',0-itl.update_qty,itl.update_qty)) QQ,
oh.name P,
oh.address1 Q,
oh.address2 R,
oh.postcode S
from inventory_transaction itl,sku,order_header oh
where itl.client_id='S2'
and itl.code in ('Shipment','UnShipment')
and TRUNC(itl.Dstamp) >TRUNC(sysdate-1)
and oh.client_id='S2'
and itl.reference_id=oh.order_id
and itl.client_id=sku.client_id
and itl.sku_id=sku.sku_id
group by
trunc(itl.Dstamp),
itl.reference_id,
itl.sku_id,
sku.DESCRIPTION,
sku.ean,
oh.name,
oh.address1,
oh.address2,
oh.postcode
union
select
trunc(itl.Dstamp) A,
'Receipt' B,
itl.reference_id C,
itl.sku_id D,
sku.DESCRIPTION DESCR,
sku.ean EAN,
sum(DECODE(itl.code,'Receipt',itl.update_qty,0-itl.update_qty)) QQ,
' ' P,
' ' Q,
' ' R,
' ' S
from inventory_transaction itl,sku
where itl.client_id='S2'
and itl.code like 'Receipt%'
and TRUNC(itl.Dstamp) >TRUNC(sysdate-1)
and itl.client_id=sku.client_id
and itl.sku_id=sku.sku_id
group by
trunc(itl.Dstamp),
itl.reference_id,
itl.sku_id,
sku.DESCRIPTION,
sku.ean
union
select
trunc(itl.Dstamp) A,
itl.code B,
itl.reference_id C,
itl.sku_id D,
sku.DESCRIPTION DESCR,
sku.ean EAN,
itl.update_qty QQ,
' ' P,
' ' Q,
' ' R,
' ' S
from inventory_transaction itl,sku
where itl.client_id='S2'
and itl.code in ('Adjustment','Stock Check')
and TRUNC(itl.Dstamp) >TRUNC(sysdate-1)
and itl.client_id=sku.client_id
and itl.sku_id=sku.sku_id)
order by 
B,A,C,D
/


--spool off
