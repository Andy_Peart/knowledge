SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

 
with total as (
select sum(inv.qty_on_hand) as total_qty
from inventory inv inner join sku on inv.sku_id = sku.sku_id and inv.client_id = sku.client_id
where inv.client_id in ('PR','PS')
and inv.location_id like 'PRHG%'
),
products as (
select sku.product_group pg,
case 
when sku.product_group = 'MJ' then 'Jackets'
when sku.product_group = 'MP' then 'Trousers'
when sku.product_group = 'MW' then 'Waistcoats'
when sku.product_group = 'MC' then 'Coats'
end pgname,
sum(inv.qty_on_hand) qty
from inventory inv inner join sku on inv.sku_id = sku.sku_id and inv.client_id = sku.client_id
where inv.client_id in ('PR','PS')
and inv.location_id like 'PRHG%'
group by sku.product_group
),
main_query as (
select pgname,qty, to_char(round(qty/total_qty,2)*100) percentage,
to_char(case when pg in( 'MJ','MC') then 35 else 75 end) qty_per_bar,
to_char(case when pg in ('MJ','MC') then round(qty/35,0) else round(qty/75,0) end) bars_used
from products, total
union all
select 'Total', total_qty, '', '', ''
from total
)
select 'Group,Qty,Percentage,Qty per bar,Bars used' A1 from dual
union all
select pgname || ',' || qty || ',' || percentage || ',' || qty_per_bar || ',' || bars_used
from (
select pgname,qty, percentage, qty_per_bar, bars_used from main_query
union all 
select 'Avg per bar', round(sum(qty)/sum(bars_used),0) avg_per_bar ,'', '', ''
from main_query
where pgname <> 'Total'
)
/