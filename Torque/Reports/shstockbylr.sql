SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240
SET TRIMSPOOL ON

--spool 1234.csv

select 'Location,SKU,Note,Description,Condition,Qty Allocated,Qty Available' from DUAL;

Select
iv.Location_ID ||','||
iv.SKU_ID ||','||
sku.user_def_note_2||','||
SKU.Description ||','||
--SKU.ean ||','||
iv.Condition_id ||','||
sum(nvl(iv.qty_allocated,0)) ||','||
sum(iv.qty_on_hand-nvl(iv.qty_allocated,0))
from inventory iv,sku
where iv.client_id='SH'
and iv.client_id=sku.client_id
and iv.sku_id=sku.sku_id
and iv.location_id between '&&2' and '&&3'
group by
iv.Location_ID,
iv.SKU_ID,
sku.user_def_note_2,
SKU.Description,
SKU.ean,
iv.Condition_id
order by
iv.Location_ID,
iv.SKU_ID
/

--spool off
