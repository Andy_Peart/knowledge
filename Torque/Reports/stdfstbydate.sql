SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON

with T_inventory as (
select inv.sku_id, libsku.getuserlangskudesc(inv.client_id,inv.sku_id) des, inv.location_id, loc.work_zone, to_char(inv.COUNT_DSTAMP,'DD-MON-YYYY') count_date, loc.zone_1,
SUM(inv.qty_on_hand) qty_on_hand, sum(nvl(inv.qty_allocated,0)) qty_allocated
from inventory inv inner join location loc on inv.location_id = loc.location_id and inv.site_id = loc.site_id
where client_id = '&&2'
and loc.zone_1 = '&&6'
and loc.work_zone like '%&&7%'
group by inv.sku_id, libsku.getuserlangskudesc(inv.client_id,inv.sku_id), inv.location_id, loc.work_zone, to_char(inv.COUNT_DSTAMP,'DD-MON-YYYY') , loc.zone_1
),
T_picked_skus as (
select it.code, it.sku_id, sum(it.update_qty) picked_qty
from inventory_transaction it
where it.client_id = '&&2'
and it.code IN ('Pick')
and it.from_loc_id <> 'CONTAINER'
and it.update_qty>0
and trunc(it.dstamp) between to_date('&&3','DD-MON-YYYY') and to_date('&&4','DD-MON-YYYY')+1
and it.sku_id in (  select sku_id from T_inventory )
group by it.sku_id, it.code
having sum(it.update_qty)>=nvl('&&5',0)
),
T_relocated_skus as (
select it.code, it.sku_id, it.to_loc_id, sum(it.update_qty) reloc_qty
from inventory_transaction it
where it.client_id = '&&2'
and it.code IN ('Relocate')
and it.from_loc_id <> 'CONTAINER'
and it.update_qty>0
and trunc(it.dstamp) between to_date('&&3','DD-MON-YYYY') and to_date('&&4','DD-MON-YYYY')+1
and it.sku_id in (  select sku_id from T_inventory )
group by it.sku_id, it.code, it.to_loc_id
)
select 'Client: &&2,Date: from &&3 to &&4,Min pick qty: ' || NVL('&&5',0)  from dual
union all
select 'SKU,Description,Location,Work Zone, Location Zone, Count Date,Qty,Qty allocated,Qty picked,Qty relocated' from dual
union all
select SKU_ID || ',"' || DES  || '",' || LOCATION_ID || ',' || WORK_ZONE || ',' || ZONE_1 || ',' || COUNT_DATE || ',' || QTY_ON_HAND || ',' || QTY_ALLOCATED || ',' ||  PICKED_QTY || ',' || RELOC_QTY
FROM (
select inv.*, picks.picked_qty, reloc.reloc_qty
from T_inventory inv left join T_picked_skus picks on inv.sku_id = picks.sku_id
left join T_relocated_skus reloc on inv.sku_id = reloc.sku_id and inv.location_id = reloc.to_loc_id
order by picks.picked_qty desc
)
WHERE PICKED_QTY>0
/