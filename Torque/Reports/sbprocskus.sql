SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9999
SET TRIMSPOOL ON

column A heading 'SKU' format A16
column B heading 'Qty' format 999999
column C heading 'Picks' format 999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


--spool sbprocskus.csv

select 'SKU, Bong Flag, Ident No' from dual;

with T_current as (
select sku_id, user_def_type_5 bond_flag, count(*) cnt
from inventory_transaction
where client_id = 'SB'
and (code = 'Receipt' or code = 'Adjustment')
group by sku_id, user_def_type_5
),
T_archive as (
select sku_id, user_def_type_5 bond_flag, count(*) cnt
from inventory_transaction_archive
where client_id = 'SB'
and (code = 'Receipt' or code = 'Adjustment')
group by sku_id, user_def_type_5
),
T_cur_arc as (
select sku_id, bond_flag, cnt
from T_current
union all
select sku_id, bond_flag, cnt
from T_archive
)
select A.sku_id, A.bond_flag, sku.user_def_type_4 ident_no
from (
select distinct sku_id, bond_flag
from T_cur_arc
) A left join sku on A.sku_id = sku.sku_id
where sku.client_id = 'SB'
/

--spool off
