SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON


column A heading 'Type' format a14
column A2 heading 'Reference' format a20
column A3 heading 'SKU' format a22
column A4 heading 'Date' format a14
column B heading 'Units' format 999999
column K heading 'Units' format 999999
column B1 heading 'Handling Rate' format A10
column C1 heading 'Handling Charge' format 999999.99
column B2 heading 'Sortation Rate' format A10
column C2 heading 'Sortation Charge' format 999999.99
column B3 heading 'Handball Rate' format A10
column C3 heading 'Handball Charge' format 999999.99
column C4 heading 'Basic Charge' format 999999.99

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON




break on A dup on report

compute sum label 'Type Total' of K B C1 C2 C3 C4 on A
compute sum label 'Monthly Total' of K B C1 C2 C3 C4 on report

--spool denimreceipts.csv

select 'Type,Reference,Date Received,Units Expected,Units Received,Handling Rate,Handling Charge,Sortation Rate,Sortation Charge,Handball Rate,Handball Charge,Basic Charge' from DUAL;



select
'Receipts BULK' A,
it.reference_id A2,
--it.sku_id A3,
to_char(it.Dstamp,'dd/mm/yy') A4,
sum(nvl(DD,0)) K,
sum(it.update_qty) B,
--'0.006' B1,
--sum(it.update_qty*0.006) C1,
--'0.025' B2,
--sum(it.update_qty*0.025) C2,
--'0.011' B3,
--sum(it.update_qty*0.011) C3,
--7.50 C4
'0.00' B1,
sum(it.update_qty*0.00) C1,
'0.00' B2,
sum(it.update_qty*0.00) C2,
'0.00' B3,
sum(it.update_qty*0.00) C3,
0.00 C4
from inventory_transaction it,(select
pre_advice_id AA,
sku_id SS,
line_id LL,
qty_due DD
from pre_advice_line
where client_id='SP')
where it.client_id='SP'
and it.station_id not like 'Auto%'
--and to_char(it.Dstamp,'mm/yyyy')=to_char(sysdate-2,'mm/yyyy')
and trunc(Dstamp)>=trunc(sysdate)-28
and trunc(Dstamp)<>trunc(sysdate)
and (it.code='Receipt' or (it.code='Adjustment' and it.reason_id in ('RECRV','STK CHECK','RET')))
and it.reference_id=AA (+)
and it.sku_id=SS (+)
and it.line_id=LL (+)
group by
it.reference_id,
to_char(it.Dstamp,'dd/mm/yy')
union
select
'Receipts PICK' A,
it.reference_id A2,
--it.sku_id A3,
to_char(it.Dstamp,'dd/mm/yy') A4,
sum(nvl(DD,0)) K,
sum(it.update_qty) B,
--'0.006' B1,
--sum(it.update_qty*0.006) C1,
--'0.00' B2,
--sum(it.update_qty*0.00) C2,
--'0.011' B3,
--sum(it.update_qty*0.011) C3,
--7.50 C4
'0.00' B1,
sum(it.update_qty*0.00) C1,
'0.00' B2,
sum(it.update_qty*0.00) C2,
'0.00' B3,
sum(it.update_qty*0.00) C3,
0.00 C4
from inventory_transaction it,(select
pre_advice_id AA,
sku_id SS,
line_id LL,
qty_due DD
from pre_advice_line
where client_id='SP'),location L
where it.client_id='SP'
--and to_char(it.Dstamp,'mm/yyyy')=to_char(sysdate-2,'mm/yyyy')
and trunc(Dstamp)>=trunc(sysdate)-28
and trunc(Dstamp)<>trunc(sysdate)
and (it.code='Receipt' or (it.code='Adjustment' and it.reason_id in ('RECRV','STK CHECK','RET')))
and it.reference_id=AA (+)
and it.sku_id=SS (+)
and it.line_id=LL (+)
and it.to_loc_id=L.location_id
and L.site_id='SP'
and L.zone_1='2'
group by
it.reference_id,
to_char(it.Dstamp,'dd/mm/yy')
order by
A,A4,A2
/


--spool off
