SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

select 'ITL'||','
||'E'||','
||code||','
||decode(sign(original_qty), 1, '+', -1, '-', '+')||','
||original_qty||','
||decode(sign(update_qty), 1, '+', -1, '-', '+')||','
||update_qty||','
||to_char(dstamp, 'YYYYMMDDHH24MISS')||','
||client_id||','
||sku_id||','
||from_loc_id||','
||to_loc_id||','
||tag_id||','
||reference_id||','
||line_id||','
||condition_id||','
||notes||','
||reason_id||','
||batch_id||','
||expiry_dstamp||','
||user_id||','
||shift||','
||station_id||','
||'ELITE'||','
||container_id||','
||pallet_id||','
||list_id||','
||owner_id||','
||origin_id||','
||work_group||','
||consignment||','
||manuf_dstamp||','
||lock_status||','
||qc_status||','
||session_type||','
||summary_record||','
||elapsed_time||','
||supplier_id||','
||user_def_type_1||','
||user_def_type_2||','
||user_def_type_3||','
||user_def_type_4||','
||user_def_type_5||','
||user_def_type_6||','
||user_def_type_7||','
||user_def_type_8||','
||user_def_chk_1||','
||user_def_chk_2||','
||user_def_chk_3||','
||user_def_chk_4||','
||user_def_date_1||','
||user_def_date_2||','
||user_def_date_3||','
||user_def_date_4||','
||user_def_num_1||','
||user_def_num_2||','
||user_def_num_3||','
||user_def_num_4||','
||user_def_note_1||','
||user_def_note_2||','
||from_site_id||','
||to_site_id||','
||''||','
||job_id||','
||job_unit||','
||manning||','
||spec_code||','
||config_id||','
||estimated_time||','
||task_category||','
||sampling_type||','
||to_char(complete_dstamp, 'YYYYMMDDHH24MISS')||','
||grn||','
||group_id||','
||uploaded||','
||uploaded_vview||','
||uploaded_ab||','
||sap_idoc_type||','
||sap_tid||','
||ce_orig_rotation_id||','
||ce_rotation_id||','
||ce_consignment_id||','
||ce_receipt_type||','
||ce_originator||','
||ce_originator_reference||','
||ce_coo||','
||ce_cwc||','
||ce_ucr||','
||ce_under_bond||','
||''||','
||uploaded_customs||','
||uploaded_labor||','
||asn_id||','
||customer_id||','
||print_label_id||','
||lock_code||','
||ship_dock||','
||''||','
||pallet_grouped
from inventory_transaction
where client_id = 'SB'
and code = 'Shipment'
and uploaded = 'N'
for update
;

/*update inventory_transaction
set uploaded = 'Y'
where client_id = 'SB'
and code = 'Shipment'
and uploaded = 'N'
;

rollback
;*/
