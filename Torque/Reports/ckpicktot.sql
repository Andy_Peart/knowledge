SET FEEDBACK OFF                
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

set pagesize 68
set linesize 120
set verify off

ttitle 'CURVY KATE Pick Quantities from &&2 to &&3' skip 2

column A heading 'Date' format a12
column C heading 'Zone' format a12
column F heading 'Mode' format a12
column B heading 'Qty' format 99999999
column M format a4 noprint


break on A dup skip 1 on report

compute sum label 'Total' of B on report

select
trunc(it.dstamp) A,
decode(it.sku_id,'5052574061470','Hangers','5052574061487','Hangers','205163119000','Hangers','205164001000','Hangers','Stock') F,
loc.Zone_1 C,
sum (it.update_qty) B
from inventory_transaction it,location loc
where it.client_id = 'CK'
and it.code = 'Pick'
and it.to_loc_id='CONTAINER'
and it.dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1
and it.from_loc_id=loc.location_id
and it.site_id=loc.site_id
group by
trunc(it.Dstamp),
decode(it.sku_id,'5052574061470','Hangers','5052574061487','Hangers','205163119000','Hangers','205164001000','Hangers','Stock'),
loc.Zone_1
order by
A,F,C
/


set pagesize 0
set heading off
select ' ' from DUAL;
select ' ' from DUAL;


select
trunc(dstamp) A,
'Total Number of Orders  = '||count(distinct reference_id) D
from inventory_transaction
where client_id = 'CK'
and code = 'Pick'
and from_loc_id='CONTAINER'
and dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1
group by
trunc(Dstamp)
order by A
/
