SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

column I format 999999
column J format 999999
column K format 999999
column L format 999999
column M format 999999
column N format 999999


select 'Stock Summary Report - for client ID &&2 in locations &&3 to &&4 - as at  ' 
    || to_char(sysdate,'dd/mm/yy  hh24:mi:ss') from dual;


select 'Product Code,Description,Total Stock,Intake,QC Hold,Suspense,Picks,Available'
from dual;

select
''''||i.sku_id||''''  ||','||
'"'||DDD||'"'  ||','||
sum(i.qty_on_hand)  ||','||
nvl(J,0)  ||','||
nvl(K,0)  ||','||
nvl(L,0)  ||','||
nvl(sum(i.qty_allocated),0)  ||','||
nvl(sum(i.qty_on_hand-i.qty_allocated)-nvl(J,0)-nvl(K,0)-nvl(L,0),0)
from inventory i, 
(select  sku_id SSS,
description DDD
from sku where client_id='&&2'),
(select distinct jc.sku_id JA,sum(jc.qty_on_hand) J from inventory jc
where jc.client_id='&&2'
and jc.location_id between '&&3' and '&&4'
and jc.location_id in ('I1','I2','I3')
group by jc.sku_id
order by jc.sku_id),
(select distinct jc.sku_id JB,sum(jc.qty_on_hand) K from inventory jc
where jc.client_id='&&2'
and jc.location_id between '&&3' and '&&4'
and jc.condition_id='QCHOLD'
group by jc.sku_id
order by jc.sku_id),
(select distinct jc.sku_id JC,sum(jc.qty_on_hand) L from inventory jc
where jc.client_id='&&2'
and jc.location_id between '&&3' and '&&4'
and jc.location_id='SUSPENSE'
group by jc.sku_id
order by jc.sku_id)
where i.client_id='&&2'
and i.Sku_id=SSS
and i.sku_id = JA (+)
and i.sku_id = JB (+)
and i.sku_id = JC (+)
and location_id between '&&3' and '&&4'
and i.location_id<>'SUSPENSE'
group by i.sku_id,DDD,nvl(J,0),nvl(K,0),nvl(L,0)
order by i.sku_id
/
