SET FEEDBACK OFF                 
set pagesize 68
set linesize 132
set verify off
set wrap off
set newpage 0

clear columns
clear breaks
clear computes


column A heading 'Task Type' format a18
column B heading 'User ID' format a18
column C heading 'Tasks' format 99999999
column D heading 'Units' format 99999999
column E heading 'Elapsed Time' format 99999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'Total Tasks by User ID from &&3 &&4 to &&5 &&6 - for client ID &&2 as at ' report_date RIGHT 'Page:'FORMAT 999 SQL.PNO skip 2 

BREAK ON A SKIP 1
COMPUTE SUM OF C D E ON A;

with T1 as (
  select key, CODE, list_id, user_id,client_id, update_qty,elapsed_time,Dstamp,TO_LOC_ID,FROM_LOC_ID, reference_id, reason_id  from inventory_transaction
)
SELECT
CODE A,
user_id B,
count(client_id) C,
sum(update_qty) D,
sum(nvl(elapsed_time,0)) E
from T1
where client_id='&&2'
and (
		(
			'&&4' is null and '&&6' is null and Dstamp between to_date('&&3 00:00:00', 'DD-MON-YYYY HH24:MI:SS') and to_date('&&5 23:59:59', 'DD-MON-YYYY HH24:MI:SS')
		)
		or 
		(
			'&&4' is null and '&&6' is not null and Dstamp between to_date('&&3 00:00:00', 'DD-MON-YYYY HH24:MI:SS') and to_date('&&5 &&6', 'DD-MON-YYYY HH24:MI:SS')
		)
		or 
		(
		'	&&4' is not null and '&&6' is null and Dstamp between to_date('&&3 &&4', 'DD-MON-YYYY HH24:MI:SS') and to_date('&&5 23:59:59', 'DD-MON-YYYY HH24:MI:SS')
		)
		or 
		(
			'&&4' is not null and '&&6' is not null and Dstamp between to_date('&&3 &&4', 'DD-MON-YYYY HH24:MI:SS') and to_date('&&5 &&6', 'DD-MON-YYYY HH24:MI:SS')
		)
	)
and update_qty<>0
and (
		code in ('Shipment','Receipt','Receipt Reverse','Putaway','Replenish','Relocate','Stock Check') 
		or 
		(code='Pick' and (elapsed_time>0 or (list_id is not null and from_loc_id <> 'MARSHALL' and from_loc_id <> 'MARSHALB' and to_loc_id <> 'MARSHALL')))
	)
	AND ('&&2'<>'TR' OR TO_LOC_ID<>'TRALT' OR TO_LOC_ID<>'TRALTCC' OR FROM_LOC_ID<>'MARSHALB')
    and nvl(reference_id,'X') not like '%/%'
	and nvl(reference_id,'X') not like 'T___'
GROUP BY CODE,USER_ID
union all
SELECT
case
  when to_loc_id not like 'HANG%' and to_loc_id not like 'HOLDRET%' then 'Returns - Box'
  else 'Returns - Hang'
end A,
user_id B,
count(client_id) C,
sum(update_qty) D,
sum(nvl(elapsed_time,0)) E
from T1
where client_id='&&2'
and (('&&4' is null and '&&6' is null and Dstamp between to_date('&&3 00:00:00', 'DD-MON-YYYY HH24:MI:SS') and to_date('&&5 23:59:59', 'DD-MON-YYYY HH24:MI:SS'))
or ('&&4' is null and '&&6' is not null and Dstamp between to_date('&&3 00:00:00', 'DD-MON-YYYY HH24:MI:SS') and to_date('&&5 &&6', 'DD-MON-YYYY HH24:MI:SS'))
or ('&&4' is not null and '&&6' is null and Dstamp between to_date('&&3 &&4', 'DD-MON-YYYY HH24:MI:SS') and to_date('&&5 23:59:59', 'DD-MON-YYYY HH24:MI:SS'))
or ('&&4' is not null and '&&6' is not null and Dstamp between to_date('&&3 &&4', 'DD-MON-YYYY HH24:MI:SS') and to_date('&&5 &&6', 'DD-MON-YYYY HH24:MI:SS')))
and update_qty<>0
and nvl(reason_id,'X') not in ('T760','RECRV','DAMGD') and update_qty<>0
	and code='Receipt' 
	and (nvl(reference_id,'X') like '%/%' or nvl(reference_id,'X') like 'T___')
	and to_loc_id not like 'HOLDRET%'
	and to_loc_id not like 'HANG%'	
GROUP BY CODE,USER_ID,
case
  when to_loc_id not like 'HANG%' and to_loc_id not like 'HOLDRET%' then 'Returns - Box'
  else 'Returns - Hang'
end
union all
SELECT
case
  when to_loc_id not like 'HANG%' and to_loc_id not like 'HOLDRET%' then 'Returns - Box'
  else 'Returns - Hang'
end A,
user_id B,
count(client_id) C,
sum(update_qty) D,
sum(nvl(elapsed_time,0)) E
from T1
where client_id='&&2'
and (('&&4' is null and '&&6' is null and Dstamp between to_date('&&3 00:00:00', 'DD-MON-YYYY HH24:MI:SS') and to_date('&&5 23:59:59', 'DD-MON-YYYY HH24:MI:SS'))
or ('&&4' is null and '&&6' is not null and Dstamp between to_date('&&3 00:00:00', 'DD-MON-YYYY HH24:MI:SS') and to_date('&&5 &&6', 'DD-MON-YYYY HH24:MI:SS'))
or ('&&4' is not null and '&&6' is null and Dstamp between to_date('&&3 &&4', 'DD-MON-YYYY HH24:MI:SS') and to_date('&&5 23:59:59', 'DD-MON-YYYY HH24:MI:SS'))
or ('&&4' is not null and '&&6' is not null and Dstamp between to_date('&&3 &&4', 'DD-MON-YYYY HH24:MI:SS') and to_date('&&5 &&6', 'DD-MON-YYYY HH24:MI:SS')))
and update_qty<>0
and nvl(reason_id,'X') not in ('T760','RECRV','DAMGD') and update_qty<>0
	and code='Receipt' 
	and (nvl(reference_id,'X') like '%/%' or nvl(reference_id,'X') like 'T___')
and (to_loc_id like 'HOLDRET%' or to_loc_id like 'HANG%')
GROUP BY CODE,USER_ID,
case
  when to_loc_id not like 'HANG%' and to_loc_id not like 'HOLDRET%' then 'Returns - Box'
  else 'Returns - Hang'
end
order by A, B
/