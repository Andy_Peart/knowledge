define client="&&1"
define fromDate="&&2"
define toDate="&&3"

SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ,


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 1000
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Shipped Date,Carrier Id,Order Id,Tracking Number,Scan Date,Scan Id' from dual
union all
select shipped_date||','||carrier_id||','||order_id||','||trackingNumber||','||scanStatus||','||scanDate||','||scanId
from
(
    select shipped_date
    , carrier_id
    , order_id
    , trackingNumber
    , case when scanDate is null then 'Not Scanned' else 'Scanned' end scanStatus
    , scanDate
    , scanId
    from
    (
        select oh.shipped_date
        , oh.carrier_id
        , oh.order_id
        , oh.signatory trackingNumber
        , (select max(dstamp) from INVENTORY_TRANSACTION where client_id = 'MOUNTAIN' and code = 'Repack' and reference_id = oh.order_id) scanDate
        , (select max(pallet_id) from INVENTORY_TRANSACTION where client_id = 'MOUNTAIN' and code = 'Repack' and reference_id = oh.order_id) scanId
        from ORDER_HEADER oh
        where oh.client_id = '&&client'
        and oh.order_type = 'WEB'
        and oh.shipped_date between to_date('&&fromdate') and to_date('&&todate')+1
    )
    order by shipped_date
    , carrier_id
    , scanStatus desc
    , order_id
)
/
--spool off