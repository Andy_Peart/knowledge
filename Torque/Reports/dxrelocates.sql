SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9999
SET TRIMSPOOL ON

column A heading 'Date' format A16
column A1 heading 'Order ID' format A24
column A2 heading 'SKU ID' format A20
column A3 heading 'Description' format A40
column B heading 'Allocated' format 999999
column C heading 'Picked' format 999999
column D heading 'Shortage' format 999999

column M noprint


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



--break on report
--compute sum label 'Total' of B C D on report

select 'Relocates - '||to_char(SYSDATE, 'DD/MM/YY  HH24:MI:SS') from DUAL;

select 'Date,SKU,Description,From,Zone,To,Zone,Qty' from DUAL;


select
distinct
it.Dstamp M,
to_char(it.Dstamp,'DD/MM/YY')||','||
--it.reference_id||','||
it.sku_id||','||
sku.description||','||
it.from_loc_id||','||
ff.zone_1||','||
it.to_loc_id||','||
tt.zone_1||','||
it.update_qty
from inventory_transaction it,sku,location ff,location tt
where it.client_id='DX'
and it.code='Relocate'
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.sku_id=sku.sku_id
and sku.client_id='DX'
and it.to_loc_id=tt.location_id
and tt.site_id='DX'
and it.from_loc_id=ff.location_id
and ff.site_id='DX'
order by
it.Dstamp
/
