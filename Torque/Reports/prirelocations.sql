SET FEEDBACK OFF             
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET COLSEP ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 400

SET TRIMSPOOL ON

column A heading 'SKU' format a18
column B heading 'From Loc' format a12
column C heading 'To Loc' format a12
column D heading 'Qty' format 9999999
column E heading 'Txn Date' format a12
column F heading 'Txn Time' format a12
column G heading 'User ID' format a10
column H heading 'Station' format a10
column M format a4 noprint

--select 'SKU,From Loc.,To Loc.,Qty,Txn Date,Txn Time,User ID,Station' from dual;

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


--TTITLE LEFT 'PRINGLE Relocations from &&2 to &&3 as at ' report_date  RIGHT 'Page:' FORMAT 999 SQL.PNO skip 2


--break on A

--compute sum label 'Day Total' of F on A

Spool kkk.csv

Select 'PRINGLE Relocations from &&2 to &&3'  from DUAL;
select 'SKU,From Loc,To Loc,Qty,Txn Date,Txn Time,User ID,Station' from DUAL;



Select
sku_id A,
from_loc_id B,
to_loc_id C,
update_qty D,
to_char(Dstamp, 'DD/MM/YYYY') E,
to_char(Dstamp, 'HH24:MI:SS') F,
user_id G,
station_id H
from inventory_transaction
where client_id = 'PRI'
and code='Relocate'
and dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1 
order by
Dstamp
/

spool off
