SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 2000
set trimspool on


column M noprint

--spool import.txt

SELECT DISTINCT
oh.user_def_type_7 M,
oh.ORDER_ID||'|3||'||
oh.NAME||'|'||
oh.USER_DEF_TYPE_1||'|'||
oh.ADDRESS1||'|'||
oh.ADDRESS2||'|'||
oh.TOWN||'|'||
oh.POSTCODE||'|'||
oh.instructions||'||1|'||
case 
  when oh.dispatch_method = 'SPEEDY' then '233'
  else 
    case 
      when oh.user_def_type_1 = 'SWEATY BETTY STORE' then '212'
      else null
    end
end||'|'||
oh.COUNTRY||'|||'||
oh.CONSIGNMENT||'|'||
oh.DISPATCH_METHOD||'|||1|'||
oh.INV_CONTACT_PHONE||'||||||'||
case
  when oh.dispatch_method = 'STANDARDCC' then 'Klaudia.Zielnik@torque.eu'
  else oh.INV_CONTACT_EMAIL
end
||'|'||
case
  when oh.dispatch_method = 'STANDARDCC' then ' '
  else oh.INV_CONTACT_PHONE
end
||'||||||'||
' '
from   order_header oh
inner join ORDER_LINE ol on oh.order_id=ol.order_id and oh.client_id=ol.client_id
where oh.client_id ='SB'
and oh.STATUS = 'Allocated'
and oh.order_type not in ('REPLENC','NEW')
and oh.CONSIGNMENT='&&2'
and (ol.BACK_ORDERED is null or ol.BACK_ORDERED <> 'Y')
and oh.country ='GBR'
order by oh.user_def_type_7
/

--spool off
