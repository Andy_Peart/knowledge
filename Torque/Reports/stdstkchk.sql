SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Client id,Site id,Date,Time,User id,Location,Start Qty,Positive Adjustment,Negative Adjustment,Qty Counted,Sku count,Tag Count' from dual
union all
select client_id||','||"Site"||','||"Date"||','||"Time"||','||"User"||','||from_loc_id||','||"Start Qty"||','||"Pos"||','||"Neg"||','||"Counted Qty"||','||"Skus"||','||"Tags"
from
(
    select 
    client_id
    , "Site"
    , trunc(max("Date"))                as "Date"
    , to_char(max("Date"), 'HH24:MI')   as "Time"
    , max(user_id) as "User"
    , from_loc_id
    , max("Qty") - sum("Neg") - sum("Pos") as "Start Qty"
    , sum("Pos") as "Pos"
    , sum("Neg") as "Neg"
    , max("Qty") as "Counted Qty"
    , count(distinct sku_id) as "Skus"
    , count(distinct tag_id) as "Tags"
    from
    (
        /*with minMaxLocs as (*/
        /* Use these min max locations to use when searching for the NOINVENTORY stock checks */
        /*
            select site_id
            , min(from_loc_id) as "Min"
            , max(from_loc_id) as "Max"
            from inventory_transaction
            where client_id = '&&2'
            and code = 'Stock Check'
            and dstamp between to_date('&&3') and to_date('&&4')+1
            group by site_id
        ),*/with noInv as (
            select '&&2' client_id
            , (select user_def_type_1 from client where client_id = '&&2') as "Site"
            , it.dstamp as "Date"
            , it.user_id
            , it.from_loc_id
            , it.sku_id
            , it.tag_id
            from inventory_transaction it
            where it.dstamp between to_date('&&3') and to_date('&&4')+1
            and it.code = 'Stock Check'
            and it.from_loc_id between '&&5' and '&&6'
            and it.site_id in (select max(site_id) from inventory_transaction where client_id = '&&2')
            and it.sku_id = 'NOINVENTORY'
        ), Qty as (
            /* Getting latest transaction for each tag at a certain location so not to multiply the units counted */
            select from_loc_id
            , sum(original_qty) + sum("Pos") + sum("Neg") as "Qty"
            from
            (
                select from_loc_id
                , case when update_qty > 0 then update_qty else 0 end as "Pos"
                , case when update_qty < 0 then update_qty else 0 end as "Neg"
                , original_qty
                , row_number()over(partition by from_loc_id, sku_id, tag_id order by dstamp desc) as "RN"
                from inventory_transaction
                where client_id = '&&2'
                and dstamp between to_date('&&3') and to_date('&&4')+1
                and code = 'Stock Check'
            ) 
            where "RN" = 1
            group by from_loc_id
        )
        select it.client_id
        , c.user_def_type_1 as "Site"
        , it.dstamp as "Date"
        , it.user_id
        , it.from_loc_id
        , case when it.update_qty > 0 then it.update_qty else 0 end as "Pos"
        , case when it.update_qty < 0 then it.update_qty else 0 end as "Neg"
        , it.update_qty
        , Qty."Qty"
        , it.sku_id
        , it.tag_id
        from inventory_transaction it
        inner join client c on c.client_id = it.client_id
        inner join Qty on Qty.from_loc_id = it.from_loc_id
        where it.client_id = '&&2'
        and it.dstamp between to_date('&&3') and to_date('&&4')+1
        and it.code = 'Stock Check'
        union all
        select noInv.client_id
        , noInv."Site"
        , noInv."Date"
        , noInv.user_id
        , noInv.from_loc_id
        , 0
        , 0
        , 0
        , 0
        , ''
        , ''
        from noInv
    )
    group by client_id
    , "Site"
    , from_loc_id
    order by "Date"
    , "Time"
    , from_loc_id
)
/
--spool off