SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET TRIMSPOOL ON

with t1 as (
    select rownum rn1,col1 from (
        select 'Consignment,SKU,Number of orders' col1 from dual
        union all
        select consignment || ',' || sku_id || ',' || qty_of_orders
        from (
            select oh.consignment, ol.sku_id, count(*) qty_of_orders
            from order_header oh inner join order_line ol on oh.order_id = ol.order_id and oh.client_id = ol.client_id
            where oh.client_id = 'MOUNTAIN'
            and oh.creation_date between to_date('&&2') and to_date('&&3')+1
            and oh.order_volume = 0.0001
            group by to_char(oh.creation_date, 'DD-MM-YYYY'), oh.consignment, ol.sku_id
            order by 1,2
        )
    )
),
t2 as (
    select rownum rn2,col2
    from (
        select 'Date,SKU,Number of skus' col2 from dual
        union all
        select created || ',' || sku_id || ',' || amount
        from (
            select to_char(oh.creation_date, 'DD-MM-YYYY') created, ol.sku_id, count(*) amount
            from order_header oh inner join order_line ol on oh.order_id = ol.order_id and oh.client_id = ol.client_id
            where oh.client_id = 'MOUNTAIN'
            and oh.creation_date between to_date('&&2') and to_date('&&3')+1
            and oh.order_volume = 0.0001
            group by to_char(oh.creation_date, 'DD-MM-YYYY'), ol.sku_id
            order by 1,2
        )
    )
)
select 'Date range: &&2 - &&3' from dual 
union all
select col1 || ',,' || col2 from 
(
select t1.col1, t2.col2
from t1 left outer join t2 on t1.rn1 = t2.rn2
)
/