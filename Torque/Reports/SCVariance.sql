SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240


column Z heading 'List ID' format A12
column A heading 'Location' format A8
column B heading 'Tag ID' format A20
column C heading 'SKU' format A18
column CC heading 'EAN' format A16
column CCC heading 'UPC' format A16
column D heading 'Description' format a40
column E heading 'Colour' format a20
column F heading 'Original Qty' format 999999
column G heading 'Counted Qty' format 999999
column H heading 'Update Qty' format 999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



select 'List ID,Location,Tag ID,SKU,EAN,UPC,Description,Colour,Original Qty,Counted Qty,Update Qty' from DUAL;

--break on BB dup skip 1 on report
break on report

--compute sum label 'Total' of DD on BB
compute sum label 'Totals' of F G H on report


select
i.list_id Z,
i.from_loc_id A,
i.tag_id  B,
''''||i.sku_id||'''' C,
''''||sku.ean||'''' CC,
''''||sku.upc||'''' CCC,
sku.description D,
sku.colour E,
nvl(i.original_qty,0) F,
nvl(i.original_qty,0)+nvl(i.update_qty,0) G,
nvl(i.update_qty,0) H
from inventory_transaction i,sku
where i.client_id='&&2'
and code='Stock Check'
and list_id like '%&&3'
and sku.client_id='&&2'
and i.sku_id=sku.sku_id
and i.update_qty<>0
order by
A,C
/

select
',,,,Total Variances = '||count(*)
from inventory_transaction i
where i.client_id='&&2'
and code='Stock Check'
and list_id like '%&&3'
and i.update_qty<>0
/
select
',,,,Total SKUs checked = '||count(*)
from inventory_transaction i
where i.client_id='&&2'
and code='Stock Check'
and list_id like '%&&3'
/
select
',,,,Locations checked = '||count(distinct i.from_loc_id)
from inventory_transaction i
where i.client_id='&&2'
and code='Stock Check'
and list_id like '%&&3'
/
select
',,,,Location/SKU variants checked = '||count(distinct i.from_loc_id||i.sku_id)
from inventory_transaction i
where i.client_id='&&2'
and code='Stock Check'
and list_id like '%&&3'
/

