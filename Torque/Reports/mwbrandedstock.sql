SET FEEDBACK OFF           
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 300
SET TRIMSPOOL ON


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



break on report

compute sum label 'Totals' of Q4 B4 F4 G4 on report

--spool mwbrandedstock.csv

select 'T2T,Sku Id,Description,Qty on Hand,Location,Work Zone' from DUAL;


select
sku.user_def_type_1||','''||
iv.sku_id||''','||
sku.description||','||
iv.qty_on_hand||','||
iv.location_id||','||
ll.work_zone
from inventory iv,sku,location ll
where iv.client_id='MOUNTAIN'
and iv.sku_id=sku.sku_id
and sku.client_id='MOUNTAIN'
and sku.user_def_type_1 is not null
and iv.location_id=ll.location_id
and ll.loc_type='Tag-FIFO'
order by
1
/


--spool off

