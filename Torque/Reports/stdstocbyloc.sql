SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ''


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 900
SET TRIMSPOOL ON
 
select 'SKU,Tag,EAN,Description,Size,Colour,Location,Location Zone,Work Zone,Qty,Product group,User Def 1,User Def 5,User Def 6,User Def Note 1,Receipt Date' Headers from dual
union all
/* Do below to be able to order by the sku */
select * from
(
    select
    '"'||i.sku_id||'","' ||i.tag_id|| '","' ||sku.ean||'","' ||sku.description||'","' ||sku_size||'","' ||colour||'","' ||i.location_id||'","' ||i.zone_1||'","' ||l.work_zone||'","' ||i.qty_on_hand||'","' ||sku.product_group||'","' 
    || i.user_def_type_1 || '","' || i.user_def_type_5 || '","' || i.user_def_type_6 || '","' || sku.user_def_note_1 || '","' || to_char(i.receipt_dstamp, 'DD-MON-YYYY') || '"'
    from INVENTORY i 
    inner join SKU on i.client_id = sku.client_id and i.sku_id = sku.sku_id
    inner join location l on l.location_id = i.location_id and i.site_id = l.site_id  
    where i.client_id = '&&2'
    order by 1 /* Sku - Tag combo */
)
/