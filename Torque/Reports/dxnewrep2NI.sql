SET FEEDBACK OFF
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','
SET UND OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 320
SET TRIMSPOOL ON



column A format A10
column B format A12
column C format A20
column D format 99999
column E format A20
column F format A20
column G format A36
column H format A12
column J format A40
column K format 99999
column L format 99999
column N format 99999


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



break on A dup skip page on C dup skip 1

compute sum label 'Order Total'  of K L N on C

update order_header
set uploaded='S'
where client_id = 'DX'
and status='Shipped'
and order_type='HOME'
and nvl(user_def_type_1,' ') like '%NI%'
and uploaded = 'R'
/

commit;

--SET FEEDBACK OFF

--spool dxrep2.csv

select 'Shipments - '||to_char(SYSDATE, 'DD/MM/YY  HH24:MI:SS') from DUAL;

select 'Order Type,Shipped Date,Order,Line,Pick List,Customer,Customer Name,Product,Description,Shipped,Ordered,Difference' from DUAL;

Select 
'HOME NI' A,
--DECODE(oh.order_type,'HOME',oh.order_type||oh.user_def_type_1,oh.order_type) A,
to_char(oh.shipped_date,'DD/MM/YY') B,
oh.order_id C,
ol.line_id D,
nvl(oh.instructions,'No Order') E,
oh.customer_id F,
oh.name G,
ol.sku_id H,
sku.description J,
ol.qty_shipped K,
ol.qty_ordered L,
ol.qty_shipped-ol.qty_ordered N
from order_header oh,order_line ol,sku
where oh.client_id='DX'
and oh.uploaded='S'
and oh.order_type='HOME'
and nvl(oh.user_def_type_1,' ') like '%NI%'
and ol.client_id='DX'
and ol.order_id=oh.order_id
and ol.sku_id=sku.sku_id
and sku.client_id='DX'
order by
A,C,D
/

--spool off

--set feedback on
 
update order_header
set uploaded='Y'
where client_id = 'DX'
and status='Shipped'
and order_type='HOME'
and nvl(user_def_type_1,' ') like '%NI%'
and uploaded = 'S'
/

--rollback;
commit;
