SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 68
SET LINESIZE 120
SET TRIMSPOOL ON
SET HEADING ON



ttitle 'Mountain Warehouse SKU Pick Totals' skip 2

column AA noprint
column A heading 'YYYY WW' format a16
column B heading 'Skus Picked' format 99999999
column C heading 'Qty Picked' format 99999999

ttitle 'Mountain SKU Pick Totals &&2 to &&3' skip 2


select
to_char(Dstamp,'YYYY IW') A,
count(distinct sku_id) B,
sum(update_qty) C
from inventory_transaction
where client_id = 'MOUNTAIN'
and dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')
and code='Pick'
and work_group<>'WWW'
group by
to_char(Dstamp,'YYYY IW')
order by
to_char(Dstamp,'YYYY IW')
/

SET HEADING OFF
SET PAGESIZE 0

select
'Period Total' A,
count(distinct sku_id) B,
sum(update_qty) C
from inventory_transaction
where client_id = 'MOUNTAIN'
and dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')
and code='Pick'
and work_group<>'WWW'
/
