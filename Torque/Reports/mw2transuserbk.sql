-- DMB Amended 29/10/2015 - Removed reference to AA
-- BKI Changed 20/06/2012 -- Removed filter in totals section for to_loc_id <>'MARSHALL'

SET FEEDBACK OFF                 
set verify off
set wrap off
set colsep ','

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 132
set trimspool on

column A0 heading 'Workstation' format a18
column A heading 'User ID' format a18
column B heading 'Task Type' format a18
column XX heading 'WWW' format a12
column C heading 'Tasks' format 99999999
column D heading 'Units' format 99999999
column Z noprint

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

--break on report 
break on A dup 

--spool mwtasks.csv

select 'Total Tasks by User ID &&4 from &&2 &&5 to &&3 &&6 - for client MOUNTAIN' from DUAL;

select 'Workstation,User,Task,Order Type,Tasks,Units' from DUAL;

select ZZ Z, A0 || ',' || A||','|| B||','|| XX||','|| C||','|| D
from (
	select 'A' ZZ, station_id A0, user_id A, code B, DECODE(work_group,'WWW','Mail Order','Outlets') XX, count(*) C, sum(update_qty) D
	from inventory_transaction
	where Dstamp between to_date('&&2 &&5', 'DD-MON-YYYY HH24:MI:SS') and to_date('&&3 &&6', 'DD-MON-YYYY HH24:MI:SS')
	and client_id in ('MOUNTAIN')
	and substr(reference_id,1,2) <> 'MS'
	and user_id like '%&&4%'
	and update_qty<>0
	and station_id not like 'Auto%'
	and (
			(code in ('Receipt','Putaway','Replenish','Relocate','Stock Check') and elapsed_time>0)
			or 
			(code in ('Pick','Consol') and elapsed_time>0)
		)
	group by user_id,code,DECODE(work_group,'WWW','Mail Order','Outlets'), station_id
	union
	select 'B' ZZ, 'GRAND TOTALS' A, '' A0, code B, DECODE(work_group,'WWW','Mail Order','Outlets') XX, count(*) C, sum(update_qty) D
	from inventory_transaction
	where Dstamp between to_date('&&2 &&5', 'DD-MON-YYYY HH24:MI:SS') and to_date('&&3 &&6', 'DD-MON-YYYY HH24:MI:SS')
	and client_id in ('MOUNTAIN')
	and substr(reference_id,1,2) <> 'MS'
	and update_qty<>0
	and station_id not like 'Auto%'
	and (
			(code in ('Receipt','Putaway','Replenish','Relocate','Stock Check') and elapsed_time>0)
			or 
			(code in ('Pick','Consol') and elapsed_time>0)
		)
	group by code,DECODE(work_group,'WWW','Mail Order','Outlets')
)
order by ZZ,A,B,XX
/

--spool off
