SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET LINESIZE 200
SET TRIMSPOOL ON
SET HEADING OFF



select '                        BERWIN Pick Sheet' from DUAL;
select '                        -----------------' from DUAL;
select ' ' from DUAL;
select
DECODE('&&3','1','SEQUENCE - Long, reg, short - Large to small',
             '2','SEQUENCE - Short, reg, long - Small to large',
             '3','SEQUENCE - Long,  short, reg -  Large to small',
             '4','SEQUENCE - Large to small -  Long,  short, reg',
             '5','SEQUENCE - Long,  short, reg -  Small to large','SEQUENCE - INVALID') 
from DUAL;
select ' ' from DUAL;


column AA noprint
column R heading 'Order' format a14
column A heading 'SKU' format a16
column A1 heading 'Description' format a18
column A2 heading 'PORD' format a14
column B heading ' ' format a2
column C heading 'Location' format a6
column D heading 'Pick' format 9999
column D2 heading 'Stck' format 9999
column D4 heading 'Ordered' format 9999
column E heading 'Picked' format A12
column F heading 'Size' format A6
column K heading 'PO' format A10
column J heading 'PO' format A13 
column M format A8 Noprint
column G heading 'X' format A1




select 'Order          SKU              Description        PORD                      PO            Size   Ordered  Pick  From   Locatn  Picked' from DUAL;
select '-----          ---              -----------        ----                      --            ----   -------  ----  ----   ------  ------' from DUAL;
select ' ' from DUAL;

break on row skip 1 on report

compute sum label 'Total' of D D2 D4 on report


select
ol.order_id R,
ol.sku_id A,
DDD A1,
nvl(RRR,'NO PORD!!') A2,
UPPER(ol.user_def_type_1) K,
UPPER(ol.user_def_note_2) J,
ZZZ F,
DECODE('&&3','1',(DECODE(substr(ZZZ,-1,1),'L','1','R','2','S','3','4')||100-substr(ZZZ,1,2)),
             '2',(DECODE(substr(ZZZ,-1,1),'L','3','R','2','S','1','4')||substr(ZZZ,1,2)),
             '3',(DECODE(substr(ZZZ,-1,1),'L',1,'R','3','S','2','4')||100-substr(ZZZ,1,2)),
             '4',(100-substr(ZZZ,1,2)||DECODE(substr(ZZZ,-1,1),'L','1','R','3','S','2','4')),
             '5',(DECODE(substr(ZZZ,-1,1),'L',1,'R','3','S','2','4')||substr(ZZZ,1,2)),'XYZ') M,
ol.qty_ordered D4,
sum(mt.qty_to_move) D,
QQQ D2,
' ' B,
mt.from_loc_id C,
'___________' E
from move_task mt,order_line ol,(select
sku_id SSS,
--location_id LLL,
max(receipt_id) RRR,
sum(qty_on_hand) QQQ
from inventory
where client_id='BW'
--and receipt_id like 'PORD%'
group by
sku_id
--location_id
),(select
sku_id KKK,
description DDD,
sku_size ZZZ
from sku
where client_id='BW'
and upper(substr(sku_size,1,2))=lower(substr(sku_size,1,2))
union
select
sku_id KKK,
description DDD,
'00'||sku_size ZZZ
from sku
where client_id='BW'
and upper(substr(sku_size,1,2))<>lower(substr(sku_size,1,2))
union
select
sku_id KKK,
description DDD,
'00   ' ZZZ
from sku
where client_id='BW'
and sku_size is null)
where mt.client_id='BW'
and ol.order_id='&&2'
and ol.client_id=mt.client_id
and ol.order_id=mt.task_id
and ol.sku_id=mt.sku_id
and ol.line_id=mt.line_id
and ol.sku_id=SSS
--and LLL=mt.from_loc_id (+)
and ol.sku_id=KKK
--and sku.client_id='BERWIN'
group by
ol.order_id,
ol.sku_id,
DDD,
RRR,
UPPER(ol.user_def_type_1),
UPPER(ol.user_def_note_2),
ZZZ,
DECODE('&&3','1',(DECODE(substr(ZZZ,-1,1),'L','1','R','2','S','3','4')||100-substr(ZZZ,1,2)),
             '2',(DECODE(substr(ZZZ,-1,1),'L','3','R','2','S','1','4')||substr(ZZZ,1,2)),
             '3',(DECODE(substr(ZZZ,-1,1),'L',1,'R','3','S','2','4')||100-substr(ZZZ,1,2)),
             '4',(100-substr(ZZZ,1,2)||DECODE(substr(ZZZ,-1,1),'L','1','R','3','S','2','4')),
             '5',(DECODE(substr(ZZZ,-1,1),'L',1,'R','3','S','2','4')||substr(ZZZ,1,2)),'XYZ'),
ol.qty_ordered,
QQQ,
mt.from_loc_id
order by J,M
/



