SET FEEDBACK OFF
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','
SET UND OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 100
SET TRIMSPOOL ON


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



--break on A dup skip page on C dup skip 1

--compute sum label 'Order Total'  of K L N on C


select 'Curvy Kate Orders_Received - '||to_char(SYSDATE, 'DD/MM/YY  HH24:MI:SS') from DUAL;

select 'Creation Date,Order,Lines,Units' from DUAL;

Select
trunc(oh.creation_date)||','||
oh.order_id||','||
nvl(oh.num_lines,0)||','||
sum(ol.qty_ordered)
from order_header oh,order_line ol
where oh.client_id='CK'
and oh.creation_date>sysdate-1
and ol.client_id='CK'
and oh.order_id=ol.order_id
group by
trunc(oh.creation_date),
oh.order_id,
nvl(oh.num_lines,0)
order by
oh.order_id
/



