SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

select 'Order,Sku,EAN,Description,Sku Size,Units Picked' from dual
union all
select "Order" || ',' || "Sku" || ',' || "EAN" || ',' || "Description" || ',' || "Sku Size" || ',' || "Units Picked" from
(
select oh.order_id                  as "Order"
, ol.sku_id                         as "Sku"
, s.ean                             as "EAN"
, s.description                     as "Description"
, nvl(s.sku_size,'No SkuSize Set')  as "Sku Size"
, sum(ol.qty_picked)                as "Units Picked"
from order_header oh
inner join order_line ol on ol.client_id = oh.client_id and ol.order_id = oh.order_id
inner join sku s on s.sku_id = ol.sku_id and s.client_id = ol.client_id
where oh.client_id = 'MB'
and oh.order_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by oh.order_id
, ol.sku_id
, s.ean
, s.description
, s.sku_size
)
/

--spool off