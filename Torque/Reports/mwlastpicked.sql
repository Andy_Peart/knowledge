SET FEEDBACK OFF             
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 1000
SET TRIMSPOOL ON

SELECT 'Sku Code,Pre Advice,Description,LPG,Qty,Earliest Date Received,Latest Date Received,Date Last Picked,Locations'
FROM DUAL;
select
''''||PA||''','||
PR||','||
DD||','||
LPG||','||
QQ||','||
trunc(RD1)||','||
trunc(RD2)||','||
PB||','||
LLL||','||
' ' from
  (
      SELECT I.SKU_ID PA,
      P.PRE_ADVICE_ID PR,
      I.DESCRIPTION DD,
      NVL(SKU.USER_DEF_TYPE_8,'ZZZZ') LPG,
      MIN(RECEIPT_DSTAMP) RD1,
      MAX(RECEIPT_DSTAMP) RD2,
      SUM(I.QTY_ON_HAND) QQ,
      NVL(BB,'01-jan-01') PB,
      LISTAGG(I.LOCATION_ID    ||'(' ||I.QTY_ON_HAND    ||')', ':') WITHIN GROUP (ORDER BY I.LOCATION_ID) LLL
      FROM INVENTORY I,
                  PRE_ADVICE_LINE P,
                  LOCATION L,
                  SKU,
      (
          SELECT PP,      MAX(BB) BB    FROM   
                                                                            (SELECT I.SKU_ID PP,
                                                                            MAX(TRUNC(I.DSTAMP)) BB
                                                                            FROM INVENTORY_TRANSACTION I
                                                                            INNER JOIN PRE_ADVICE_LINE P ON P.SKU_ID  = I.SKU_ID
                                                                            WHERE I.CLIENT_ID = 'MOUNTAIN'
                                                                            AND I.CODE        = 'Pick'
                                                                            AND ELAPSED_TIME  >0
                                                                            GROUP BY I.SKU_ID
                                                                            UNION
                                                                            SELECT I.SKU_ID PP,
                                                                            MAX(TRUNC(I.DSTAMP)) BB
                                                                            FROM INVENTORY_TRANSACTION_ARCHIVE I
                                                                            WHERE I.CLIENT_ID = 'MOUNTAIN'
                                                                            AND I.CODE        = 'Pick'
                                                                            AND ELAPSED_TIME  >0
                                                                            GROUP BY I.SKU_ID
                                                                          )
      GROUP BY PP
      )
  WHERE I.CLIENT_ID = 'MOUNTAIN'
                AND I.SKU_ID      =PP (+)
                AND I.LOCATION_ID =L.LOCATION_ID
                AND I.SITE_ID     =L.SITE_ID
                AND L.LOC_TYPE    ='Tag-FIFO'
                AND I.SKU_ID      =SKU.SKU_ID
                AND SKU.CLIENT_ID = 'MOUNTAIN'
                AND P.SKU_ID = I.SKU_ID
  GROUP BY I.SKU_ID,
                    P.PRE_ADVICE_ID,
                    I.DESCRIPTION,
                    NVL(SKU.USER_DEF_TYPE_8,'ZZZZ'),
                    NVL(BB,'01-jan-01')
  )
ORDER BY 1
--select to_char(SYSDATE, 'DD/MM/YY  HH:MI:SS') curdate from DUAL;

--spool off
