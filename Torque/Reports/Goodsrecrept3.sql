SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

 
column A format A10
column B format A15
column C format A28
column E format A20
column F format 9999999
column G format A18
column H format A40
column I format 999999999
column J format 999999999
column K format 999999999


select 'Goods Outstanding Report - for TM LEWIN for Pre Advice ID &2' 
from dual;

select 'Due Date,Supplier Id,Supplier Name,Pre Advice ID,Line ID,Product Code,Description,Ordered,Received,Shortfall'
from dual;


select 
to_char(ph.Due_Dstamp,'DD/MM/YY') A,
ph.supplier_id B,
ad.name C,
ph.pre_advice_id E,
pl.line_id F, 
pl.sku_id G, 
sku.description H,
pl.qty_due I,
nvl(pl.qty_received,0) J,
pl.qty_due-nvl(pl.qty_received,0) K
from pre_advice_header ph,pre_advice_line pl,sku,address ad
where (pl.qty_received is null or pl.qty_due>pl.qty_received)
and ph.pre_advice_id='&2' 
and ph.supplier_id=ad.address_id
and ph.pre_advice_id=pl.pre_advice_id
and pl.sku_id=sku.sku_id
and pl.client_id=sku.client_id
order by to_char(ph.Due_Dstamp,'DD/MM/YY'),pl.pre_advice_id,pl.line_id
/

