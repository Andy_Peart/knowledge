SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240


select
'ISM,E,&&2' ||','||
JA ||','||
KA ||','||
KB ||','||
KC ||','||
KD ||','||
KE ||','||
KF ||','||
JX ||','||
nvl(JB,0) ||','||
nvl(HB,0) ||','||
nvl(GB,0) ||','||
nvl(JC,0) ||','||
nvl(HC,0) ||','||
nvl(GC,0) ||','||
nvl(JB-JC,0) ||','||
nvl(HB-HC,0) ||','||
nvl(GB-GC,0) ||','||
'0,+,0,0' ||','||
sku.description ||','||
'EUROPE/LONDON'
from
(select
i.sku_id JA,
i.site_id KA,
i.owner_id KB,
i.supplier_id KC,
i.batch_id KD,
i.condition_id KE,
i.origin_id KF,
count(*) JX,
sum(i.qty_on_hand) JB,
sum(i.qty_allocated) JC
from inventory i
where i.client_id='&&2'
and i.sku_id not like '**%'
--and i.sku_id like 'A6%'
group by
i.sku_id,
i.site_id,
i.owner_id,
i.supplier_id,
i.batch_id,
i.condition_id,
i.origin_id),
(select
i.sku_id HA,
i.supplier_id SA,
sum(i.qty_on_hand) HB,
sum(i.qty_allocated) HC
from inventory i
where i.client_id='&&2'
and i.sku_id not like '**%'
--and i.sku_id like 'A6%'
and i.Lock_status='UnLocked'
group by
i.sku_id,i.supplier_id),
(select
i.sku_id GA,
i.supplier_id SB,
sum(i.qty_on_hand) GB,
sum(i.qty_allocated) GC
from inventory i
where i.client_id='&&2'
and i.sku_id not like '**%'
--and i.sku_id like 'A6%'
and i.Lock_status='Locked'
group by
i.sku_id,i.supplier_id),sku
where JA=HA (+)
and KC=SA (+)
and JA=GA (+)
and KC=SB (+)
and JA=sku.sku_id
--and JA like 'YOTWDWHT%'
order by JA
/


