SET FEEDBACK OFF             
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 148
SET TRIMSPOOL ON

select 'SKU,Qty,EAN,Description' from DUAL;


select
inventory.sku_id||','||
sum(qty_on_hand)||','||
--location_id||','||
EAN||','||
sku.Description
from inventory,sku
where inventory.client_id='DX'
and location_id='&&2'
and sku.client_id='DX'
and inventory.sku_id=sku.sku_id
group by
inventory.sku_id,
EAN,
sku.Description
/
