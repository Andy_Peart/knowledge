SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240
SET TRIMSPOOL ON

--spool mw20637.csv

select 'Extract from &&2 to &&3' from DUAL;
select 'DATE,TYPE,LINES,SKUS,UNITS PICKED,UNITS PER SKU,PACK PICKS,PACK SKUS,PACK%,AVE SIZE,ORDERS,GOOD RETURNS,BAD RETURNS' from DUAL;

select
CD,
WG,
C1,
C2,
CT1,
to_char(CT1/C2,'990.99') C4,
CT2,
CT3,
to_char((CT2/CT1)*100,'990.99') PC,
to_char(RT/CT3,'990.99') RT,
C3,
nvl(RTQ2,0),
nvl(RTQ,0)
from (select
CD,
WG,
count(*) C1,
count(distinct SK) C2,
count(distinct RF) C3,
sum(CT1) CT1,
sum(CT2) CT2,
sum(CT3) CT3,
sum(RT) RT
from (select
--trunc(Dstamp) CD,
to_date('02/01/12', 'DD/MM/YY')+(trunc((trunc(Dstamp)-to_date('02/01/12', 'DD/MM/YY'))/7)*7) CD,
DECODE(work_group,'WWW','WEB','RETAIL') WG,
update_qty CT1,
sku_id SK,
reference_id RF,
CASE
WHEN nvl(RT,0)>0 THEN update_qty
ELSE 0 END CT2,
CASE
WHEN nvl(RT,0)>0 THEN 1
ELSE 0 END CT3,
nvl(RT,0) RT
from inventory_transaction it,(select
config_id SKU,
ratio_1_to_2 RT
from sku_config
where client_id='MOUNTAIN')
where it.client_id='MOUNTAIN'
and it.code='Pick'
and upper(it.sku_id)=lower(it.sku_id)
--and it.work_group<>'WWW'
and (it.elapsed_time>0 or it.list_id is not null)
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.sku_id=SKU (+))
group by CD,WG),(select
trunc(Dstamp) CD1,
DECODE(work_group,'WWW','WEB','RETAIL') RG,
sum(update_qty) RTQ
from inventory_transaction it
where it.client_id='MOUNTAIN'
and upper(it.sku_id)=lower(it.sku_id)
and it.code='Receipt'
--and to_loc_id in ('Z013','RET1','RET2','RET3')
and to_loc_id in ('Z013')
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by
trunc(Dstamp),
DECODE(work_group,'WWW','WEB','RETAIL')),(select
trunc(Dstamp) CD2,
DECODE(work_group,'WWW','WEB','RETAIL') RG2,
sum(update_qty) RTQ2
from inventory_transaction it
where it.client_id='MOUNTAIN'
and upper(it.sku_id)=lower(it.sku_id)
and it.code='Receipt'
and to_loc_id in ('RET1','RET2','RET3')
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by
trunc(Dstamp),
DECODE(work_group,'WWW','WEB','RETAIL'))
where CD=CD1(+)
and WG=RG(+)
and CD=CD2(+)
and WG=RG2(+)
order by CD,WG
/



--spool off

