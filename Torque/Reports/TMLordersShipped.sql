SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET COLSEP ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 200
SET TRIMSPOOL ON
SET HEADING OFF 

Break on A dup skip 1

select 'Date Range: &&2 - &&3' from dual
union all
select 'Stock type,Units Shipped' from dual
union all
select stock_type || ',' || units_shipped from (
select decode(sku.product_group, 'MP', 'Trousers', 'Shirts') stock_type,sum(sm.qty_shipped) units_shipped
from shipping_manifest sm inner join sku on sm.sku_id = sku.sku_id and sm.client_id = sku.client_id
where sm.client_id = 'T'
and sm.shipped_dstamp between to_date('&&2') and to_date('&&3')+1
and sku.product_group in ('MP','MS')
group by decode(sku.product_group, 'MP', 'Trousers', 'Shirts')
)
/
