SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','
set trim on



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 32766
SET TRIMSPOOL ON


select 'Location,SKU ID,Colour,Size,Description,Available,Last Count, Location QTY' from DUAL;

select q1.location_id || ',' || q1.sku_id || ',' || q1.colour || ',' || q1.sku_size || ',' || q1.description || ',' || q1.qty || ',' || q1.count_date || ',' || q2.loc_qty
from 
(
select 
i.client_id,
i.site_id,
i.location_id,
i.sku_id,
sku.colour,
sku.sku_size,
sku.description,
to_char(i.count_dstamp,'DD/MM/YYYY') as count_date,
SUM(i.qty_on_hand - i.qty_allocated) as qty
from inventory i 
inner join sku on (i.client_id = sku.client_id AND i.sku_id = sku.sku_id)
WHERE i.client_id = '&&3'
AND i.location_id like '&&2%'
group by 
i.client_id,
i.location_id,
i.sku_id,
sku.colour,
sku.sku_size,
sku.description,
to_char(i.count_dstamp,'DD/MM/YYYY'),
i.site_id
) q1 inner join (
select 
i.client_id,
i.site_id,
i.location_id,
SUM(i.qty_on_hand-i.qty_allocated) as loc_qty
from inventory i
where i.client_id = '&&3'
AND i.location_id like '&&2%'
group by 
i.client_id,
i.site_id,
i.location_Id
) q2 on (q1.site_id = q2.site_id AND q1.client_id = q2.client_id AND q1.location_id = q2.location_id)
WHERE
('&&5' = 'SKU'
AND q1.qty <= &&4)
OR
('&&5' = 'LOCATION'
AND q2.loc_qty <= &&4)
order by 1
/


spool off
