SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 320

column A heading 'Date' format A12
column B heading 'Site' format A12
column C heading 'Client' format A6
column D heading 'Pre Advice ID' format A12
column E heading 'Line ID' format 999999
column F heading 'GRN' format A12
column G heading 'Supp ID' format A6
column H heading 'Supp Name' format A36
column J heading 'Group' format A6
column K heading 'Description' format A40
column KK heading 'SKU' format A18
column L heading 'Size' format A12
column M heading 'Qty1' format 999999
column N heading 'Qty2' format 999999
column P heading 'Qty3' format 999999

--set termout off
--spool TR030.csv

Select 'TML Receipts completed '||to_char(sysdate-1,'DD/MM/YYYY') from Dual;

Select 'Date,Site,Client,Pre Advice,Line ID,GRN,Supp ID,Supplier Name,Group,Description,SKU,Size,Qty Rec,Qty Due,Variance,' from DUAL;



break on D dup skip 1

compute sum label 'Total' of M N P on D


select 
to_char(ph.finish_dstamp,'DD-Mon-YYYY') A,
ph.site_id B,
ph.client_id C,
ph.pre_advice_id D, 
pl.line_id E,
ph.notes F,
ph.supplier_id G, 
ph.name H,
sku.product_group J, 
sku.description K,
''''||pl.sku_id||'''' KK,
sku.sku_size L,
pl.qty_received M, 
pl.qty_due N, 
pl.qty_received-pl.qty_due P,
' '
from pre_advice_header ph,pre_advice_line pl,sku
where ph.client_id like 'TR'
and ph.client_id=pl.client_id 
and ph.client_id=sku.client_id 
and pl.sku_id=sku.sku_id
and ph.pre_advice_id=pl.pre_advice_id
and trunc(ph.finish_dstamp)=trunc(sysdate-1)
order by ph.pre_advice_id, sku.product_group, sku.description, pl.line_id, pl.sku_id
/

--spool off
--set termout on
