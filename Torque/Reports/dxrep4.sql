SET FEEDBACK OFF                
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET COLSEP ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 400
SET TRIMSPOOL ON

column A format A12
column B format A14
column C format 99999
column D format A20
column E format A12
column F format A24
column H format A24
column J format A40
column K format 99999
column L format 99999
column M format 99999
column N format A10
column P format A10
column R format A10


break on B dup skip 1

compute sum label 'Order Total'  of K L M on B

--set feedback on



update inventory_transaction
set user_def_chk_2='Q'
where client_id = 'DX'
--and trunc(Dstamp)>trunc(sysdate-7)
and code = 'Receipt'
and (user_def_chk_2 = 'N'
or user_def_chk_2 is null)
and reference_id like 'PO%'
--and to_loc_id not in ('DXBRET','DXBREJ')
/



--set feedback off

--spool dxrep4.csv

select 'Completed_Receipts - '||to_char(SYSDATE, 'DD/MM/YY  HH24:MI:SS') from DUAL;

select 'Received Date,Pre Advice,Line,Notes,Supplier,Supplier Name,Product,Description,Condition,Received,Ordered,Differance,Location,Zone' from DUAL;


Select 
to_char(i.dstamp,'DD/MM/YY') A,
ph.pre_advice_id B,
pl.line_id C,
ph.notes D,
ph.supplier_id E,
address.name F,
pl.sku_id H,
sku.description J,
i.condition_id R,
i.update_qty K,
pl.qty_due L,
pl.qty_due-i.update_qty M,
i.to_loc_id N,
tt.zone_1 P
from inventory_transaction i,pre_advice_header ph,pre_advice_line pl,sku,address,location tt
where i.client_id='DX'
and i.user_def_chk_2='Q'
and i.reference_id like 'PO%'
--and i.to_loc_id not in ('DXBRET','DXBREJ')
and i.code='Receipt'
and ph.client_id='DX'
and i.reference_id=ph.pre_advice_id
and pl.client_id='DX'
and pl.pre_advice_id=ph.pre_advice_id
and i.line_id=pl.line_id
and pl.sku_id=sku.sku_id
and sku.client_id='DX'
and ph.supplier_id=address.address_id (+)
and i.to_loc_id=tt.location_id
and tt.site_id='DX'
order by
i.dstamp,
ph.pre_advice_id,
pl.line_id 
/

--spool off

--set feedback on

update inventory_transaction
set user_def_chk_2='Y'
where client_id = 'DX'
--and trunc(Dstamp)>trunc(sysdate-7)
and code = 'Receipt'
and user_def_chk_2 = 'Q'
and reference_id like 'PO%'
--and to_loc_id not in ('DXBRET','DXBREJ')
/



commit;
--rollback;

