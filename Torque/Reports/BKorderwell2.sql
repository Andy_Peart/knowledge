SET FEEDBACK OFF                 
set pagesize 68
set linesize 160
set newpage 0
set verify off

clear columns
clear breaks
clear computes

column A heading 'Ordered' format 99999
column B heading ' Tasked' format 99999
column C heading ' Picked' format 99999
column D heading 'Shipped' format 99999
column E heading 'Consigned' format a10
column F heading 'Status' format a14
column G heading 'Order' format a22
column H heading 'Date' format a12


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

ttitle  LEFT 'Order Well Summary for orders created between &&2 and &&3 for client &&4 as at ' report_date skip 2

break on F skip 2
compute sum label 'Status Total' of A B C D on F

select
oh.status F,
DECODE(oh.consignment,null,'NO','YES') E,
to_char(oh.order_date, 'DD/MM/YY') H,
oh.order_id G,
sum(nvl(ol.qty_ordered,0)) A,
sum (nvl(ol.qty_tasked,0)) B,
sum(nvl(ol.qty_picked,0)) C,
sum(nvl(ol.qty_shipped,0)) D
from order_line ol, order_header oh
where oh.client_id = '&&4'
and oh.order_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and oh.status in ('Allocated','In Progress')
and oh.order_id = ol.order_id
group by
oh.status,
DECODE(oh.consignment,null,'NO','YES'),
to_char(oh.order_date, 'DD/MM/YY'),
oh.order_id
order by
oh.status,
DECODE(oh.consignment,null,'NO','YES'),
to_char(oh.order_date, 'DD/MM/YY'),
oh.order_id
/
