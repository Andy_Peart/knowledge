SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON

select 'SKU', ' From Location', 'Work Zone', 'Priority', 'Consignment'  from dual
union all
select  to_char(SKU_id), to_char(FROM_LOC_ID), to_char(work_zone), to_char(Priority), to_char(CONSIGNMENT) 
from move_task
where client_id = 'MOUNTAIN'
and work_group = 'WWW'
and FROM_LOC_ID <> 'Container'
and task_id <> 'PALLET'
and work_zone not in ('WEB1', 'WEB2', 'WPF', 'WRET', 'WPF2', 'ZMW', 'WPF1','1WOPS')
and priority <> 50
group by SKU_id, FROM_LOC_ID, work_zone, PRIORITY, CONSIGNMENT
order by 2

/