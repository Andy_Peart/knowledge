SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 40


select 
sku_id||','||
nvl(BB,0)
from sku,(select
sku_id AA,
sum(qty_on_hand - qty_allocated) BB
from inventory i
where client_id = 'MOUNTAIN'
and i.location_id <> 'DESPATCH'
and i.location_id <> 'SUSPENSE'
and i.location_id not in ('Z001','Z002','Z003','Z004','Z005','Z006','Z007','Z008','Z009','Z010','Z011','Z012','Z013','Z014')
and i.sku_id <> 'TEST'
group by sku_id)
where client_id='MOUNTAIN'
and sku_id=AA (+)
order by sku_id
/

