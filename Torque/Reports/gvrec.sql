SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 180



select 'ITL,E,Receipt,'||','
||decode(sign(A1), 1, '+', -1, '-', '+')||','
||A1||','
||decode(sign(A2), 1, '+', -1, '-', '+')||','
||A2||','
||AA||','
||'GIVE'||','
||SS||','
||''||','
||''||','
||''||','
||''||','
||RR||','
||LI||','
||'GOOD'||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||'WORGIVE'||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||SUP||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||',,'
from (select
to_char(i.dstamp, 'YYYYMMDDHH24') AA,
i.sku_id SS,
RR,
LI,
SUP,
sum(i.original_qty) A1,
sum(i.update_qty) A2
from inventory_transaction i, location ll,(select
sku_id SKU,
reference_id RR,
line_id LI,
supplier_id SUP,
tag_id TAG
from inventory_transaction 
where client_id = 'GV'
and code = 'Receipt'
and dstamp > sysdate-1)
where i.client_id = 'GV'
and i.code = 'Relocate'
and i.uploaded = 'N'
and i.dstamp > sysdate-1
and i.to_loc_id=ll.location_id
and (ll.disallow_alloc<>'Y' or i.to_loc_id='OUTLET')
and i.tag_id=TAG
group by
to_char(i.dstamp, 'YYYYMMDDHH24'),
i.sku_id,
RR,
LI,
SUP)
order by
SS
/


--update inventory_transaction
--set uploaded='Y'
--where client_id = 'GV'
--and code = 'Relocate'
--and uploaded = 'N'
--/

--commit;
