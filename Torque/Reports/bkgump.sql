SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','
SET UND OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 320


select 'Key,Client,SKU,EAN,Reference,Customer,Qty,Date,Time,Uploaded' from DUAL;


column A format 9999999999
column B format A10
column C format A18
column D format A18
column E format A12
column F format A16
column G format 999999
column H format A10
column I format A10
column J format A10


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



break on report

compute sum label 'Total'  of G  on report




Select 
it.Key A,
it.Client_ID B,
''''||it.SKU_ID||'''' C,
''''||sku.EAN||'''' D,
it.Reference_ID E,
it.Customer_ID F,
it.Update_qty G,
to_char(it.Dstamp,'DD/MM/YY') H,
to_char(it.Dstamp,'HH:MI:SS') I,
it.Uploaded J
from inventory_transaction it,sku
where it.code='Shipment'
and it.Uploaded<>'Y'
and it.client_id='&&2'
and it.Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and it.sku_id=sku.sku_id
and sku.client_id='&&2'
order by
A
/
 
