SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Order Date,Order Time,Creation Date,Creation Time,Order #,Order Type,Customer,Contact,Status,Style,Colour,Size,Sku,Description,EAN,User Def Type 1,Qty Ordered,Qty Shipped,Discrepancy,Difference?,Country' from dual
union all
select "Ord Date" ||','|| "Ord Time" ||','|| "Creation Date" ||','|| "Creation Time" ||','|| "Ord" ||','|| "Type" ||','|| "Cust" ||','|| "Contact" ||','|| "Status" ||','|| "Style"
||','|| "Colour" ||','|| "Size" ||','|| "Sku" ||','|| "Desc" ||','|| "EAN" ||','|| "UDT1" ||','|| "Qty Ordered" ||','|| "Qty Shipped" ||','|| "Disc" ||','|| "Diff?" ||','|| country
from
(
select trunc(oh.order_date)             as "Ord Date"
, to_char(oh.order_date, 'HH24:MI')     as "Ord Time"
, trunc(oh.creation_date)               as "Creation Date"
, to_char(oh.creation_date, 'HH24:MI')  as "Creation Time"
, oh.order_id                           as "Ord"
, oh.order_type                         as "Type"
, oh.customer_id                        as "Cust"
, oh.contact                            as "Contact"
, oh.status                             as "Status"
, s.style                               as "Style"
, s.colour                              as "Colour"
, s.sku_size                            as "Size"
, ol.sku_id                             as "Sku"
, s.description                         as "Desc"
, s.ean                                 as "EAN"
, s.user_def_type_1                     as "UDT1"
, ol.qty_ordered                        as "Qty Ordered"
, nvl(ol.qty_shipped,0)                 as "Qty Shipped"
, ol.qty_ordered - nvl(ol.qty_shipped,0)as "Disc"
, case when ol.qty_ordered - nvl(ol.qty_shipped,0) = 0 then 'N' else 'Y' end as "Diff?"
, oh.country
from order_header oh
inner join order_line ol on ol.client_id = oh.client_id and ol.order_id = oh.order_id
inner join sku s on s.sku_id = ol.sku_id and s.client_id = ol.client_id
where oh.client_id = '&&2'
and oh.shipped_date between to_date('&&3') and to_date('&&4')+1
and status = 'Shipped'
union all
select trunc(oh.order_date)             as "Ord Date"
, to_char(oh.order_date, 'HH24:MI')     as "Ord Time"
, trunc(oh.creation_date)               as "Creation Date"
, to_char(oh.creation_date, 'HH24:MI')  as "Creation Time"
, oh.order_id                           as "Ord"
, oh.order_type                         as "Type"
, oh.customer_id                        as "Cust"
, oh.contact                            as "Contact"
, oh.status                             as "Status"
, s.style                               as "Style"
, s.colour                              as "Colour"
, s.sku_size                            as "Size"
, ol.sku_id                             as "Sku"
, s.description                         as "Desc"
, s.ean                                 as "EAN"
, s.user_def_type_1                     as "UDT1"
, ol.qty_ordered                        as "Qty Ordered"
, nvl(ol.qty_shipped,0)                 as "Qty Shipped"
, ol.qty_ordered - nvl(ol.qty_shipped,0)as "Disc"
, oh.country
, case when ol.qty_ordered - nvl(ol.qty_shipped,0) = 0 then 'N' else 'Y' end as "Diff?"
from order_header_archive oh
inner join order_line_archive ol on ol.client_id = oh.client_id and ol.order_id = oh.order_id
inner join sku s on s.sku_id = ol.sku_id and s.client_id = ol.client_id
where oh.client_id = '&&2'
and oh.shipped_date between to_date('&&3') and to_date('&&4')+1
and status = 'Shipped'
order by 1, 2
)
/
--spool off