/******************************************************************************/
/*                                                                            */
/*                            Elite                                           */
/*                                                                            */
/*     FILE NAME  :   prilocations.sql                                        */
/*                                                                            */
/*     DESCRIPTION:    Datastream for Pringle Location Labels                 */
/*                                                                            */
/*   DATE     BY   PROJ     ID         DESCRIPTION                            */
/*   ======== ==== ======   ========   =============                          */
/*   29/08/06 LH   Pringle             Location Label Creator for Pringle     */
/******************************************************************************/

/* Input Parameters are as follows */
-- 1)  Start Location
-- 2)  End Location
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 0
SET VERIFY OFF
SET FEEDBACK OFF
SET TAB OFF

 
-- Set the line length to the maximum possible, but trim 
-- trailing spaces for both screen output and spool files
SET LINESIZE 32767
SET WRAP ON
SET TRIMOUT ON
SET TRIMSPOOL ON




/* Define the format of the output dates */
DEFINE	OutputDateFormat = 'DD-MON-YYYY HH24:MI:SS'
COLUMN	sorter	NOPRINT

SELECT Distinct '0' sorter,
       'DSTREAM_PRI_LOCATIONS_HDR'          ||'|'||
       rtrim('&&1')             ||'|'||              /* Location_id Start*/
       rtrim('&&2')									/* Location_id End*/
from dual
union all
SELECT '0' sorter,
'DSTREAM_PRI_LOCATIONS_LINE'              ||'|'||
rtrim (location_id)
from location
where location_id between '&&1' and '&&2'
/