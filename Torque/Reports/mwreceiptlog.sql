SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
set linesize 300
set trimspool on

--spool mwreceiptlog.csv

break on A1

select 'SKU,Receipt Date,Qty Received' from Dual;


select
A1,
A2,
A3
from (select
it.sku_id A1,
trunc(It.dstamp) A2,
sum(it.update_qty) A3
from inventory_transaction it
where it.client_id='MOUNTAIN'
and it.code='Receipt'
and nvl(it.notes,' ') not in ('REFUND','EXCHANGE')
and it.from_loc_id<>'RET1'
and it.dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by
it.sku_id,
trunc(It.dstamp)
union
select
it.sku_id A1,
trunc(It.dstamp) A2,
sum(it.update_qty) A3
from inventory_transaction_archive it
where it.client_id='MOUNTAIN'
and it.code='Receipt'
and nvl(it.notes,' ') not in ('REFUND','EXCHANGE')
and it.from_loc_id<>'RET1'
and it.dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by
it.sku_id,
trunc(It.dstamp))
order by
A1,A2
/

--spool off
