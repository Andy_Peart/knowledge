SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

column D heading 'Total' format 99999999



--select 'Order ID,Ordered,Picked' from DUAL;
 
set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DAY, DD MONTH, YYYY') curdate from DUAL;
set TERMOUT ON



--compute sum label 'Total' of D DD on report


select 'HANGING UNITS for' from DUAL;
select 'Period &&2 to &&3' from DUAL;
select '' from DUAL;
select '' from DUAL;

break on report skip 2

select
'Ordered  =   '||sum(ol.qty_ordered) D
from order_header oh,order_line ol,sku
where oh.client_id='TR'
and oh.creation_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and oh.status in ('Shipped')
and oh.client_id=ol.client_id
and oh.order_id=ol.order_id
and ol.client_id=sku.client_id
and ol.sku_id=sku.sku_id
and sku.product_group in ('MJ','MP','MW','MC','MZ','WJ','WP','WW','WC','WM','WH','WD')
/


select
'Pickable =   '||sum(update_qty) D
from inventory_transaction it,sku
where code='Allocate'
and User_id='Allocdae'
and it.client_id='TR'
and it.client_id=sku.client_id
and it.sku_id=sku.sku_id
and sku.product_group in ('MJ','MP','MW','MC','MZ','WJ','WP','WW','WC','WM','WH','WD')
and reference_id in
(select
order_id
from order_header oh
where oh.client_id='TR'
and oh.creation_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and oh.status in ('Shipped'))
and from_loc_id<>'CONTAINER'
/



select
'Picked   =   '||sum(update_qty) D
from inventory_transaction it,sku
where code='Pick'
and it.client_id='TR'
and it.client_id=sku.client_id
and it.sku_id=sku.sku_id
and sku.product_group in ('MJ','MP','MW','MC','MZ','WJ','WP','WW','WC','WM','WH','WD')
and reference_id in
(select
order_id
from order_header oh
where oh.client_id='TR'
and oh.creation_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and oh.status in ('Shipped'))
and from_loc_id<>'CONTAINER'
/
