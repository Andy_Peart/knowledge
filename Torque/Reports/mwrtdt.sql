SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Order Date,Orders,Lines,Units,Lines per Order,Units per Order,Units per Line' from dual
union all
select "Order Date" ||','|| "Orders" ||','|| "Lines" ||','|| "Units" ||','|| "Lines per Order" ||','|| "Units per Order" ||','|| "Units per Line"
from
(
    select *
    from
    (
        select trunc(oh.order_date)                                   as "Order Date"
        , count(distinct oh.order_id)                                 as "Orders"
        , count(ol.qty_picked)                                        as "Lines"
        , sum(ol.qty_picked)                                          as "Units"
        , round(count(ol.qty_picked)/count(distinct oh.order_id), 2)  as "Lines per Order"
        , round(sum(ol.qty_picked)/count(distinct oh.order_id), 2)    as "Units per Order"
        , round(sum(ol.qty_picked)/count(ol.qty_picked), 2)           as "Units per Line"
        from order_header oh
        inner join order_line ol on ol.order_id = oh.order_id and oh.client_id = ol.client_id
        where oh.client_id = 'MOUNTAIN'
        and oh.order_type in ('RETAIL1')
        and oh.order_date between to_date('&&2') and to_date('&&3')+1
        and nvl(ol.qty_picked,0) <> 0
        group by trunc(oh.order_date)
    union all
        select trunc(oh.order_date)                                   as "Order Date"
        , count(distinct oh.order_id)                                 as "Orders"
        , count(ol.qty_picked)                                        as "Lines"
        , sum(ol.qty_picked)                                          as "Units"
        , round(count(ol.qty_picked)/count(distinct oh.order_id), 2)  as "Lines per Order"
        , round(sum(ol.qty_picked)/count(distinct oh.order_id), 2)    as "Units per Order"
        , round(sum(ol.qty_picked)/count(ol.qty_picked), 2)           as "Units per Line"
        from order_header_archive oh
        inner join order_line_archive ol on ol.order_id = oh.order_id and oh.client_id = ol.client_id
        where oh.client_id = 'MOUNTAIN'
        and oh.order_type in ('RETAIL1')
        and oh.order_date between to_date('&&2') and to_date('&&3')+1
        and nvl(ol.qty_picked,0) <> 0
        group by trunc(oh.order_date)
    )
    order by 1
)
/
--spool off