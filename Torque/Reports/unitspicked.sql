set feedback off
set pagesize 68
set linesize 240
set verify off

clear columns
clear breaks
clear computes

column D heading 'Receipt Date' format a16
column A heading 'Order Reference' format A16
column B heading 'Qty' format 999999
column S heading 'Trade Type' format A12
column M noprint


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

ttitle  LEFT 'Units Picked from '&&2' to '&&3' - for client ID &&4 as at ' report_date skip 2

break on D skip 2 on S skip 1 on report
--break on D skip 2 on report

compute sum label 'Day Total' of B on D
compute sum label 'Total' of B on S
compute sum label 'Full Total' of B on report

select
to_char(Dstamp,'DD/MM/YYYY') D,
to_char(Dstamp,'YYYY/MM/DD') M,
reference_id A,
case 
 when owner_id = 'PRIWEB' THEN 'WEB'
 WHEN REFERENCE_ID LIKE '31%' THEN 'WHOLESALE'
 ELSE 'RETAIL'
 end as S,
 sum(update_qty) B
from inventory_transaction
where client_id='&&4'
and code='Pick'
and from_loc_id='CONTAINER'
and Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by
to_char(Dstamp,'DD/MM/YYYY'),
to_char(Dstamp,'YYYY/MM/DD'),
substr(reference_id,1,2),
reference_id,
case 
 when owner_id = 'PRIWEB' THEN 'WEB'
 WHEN REFERENCE_ID LIKE '31%' THEN 'WHOLESALE'
 ELSE 'RETAIL'
 end 
 order by
to_char(Dstamp,'YYYY/MM/DD'),
substr(reference_id,1,2),
reference_id
/
