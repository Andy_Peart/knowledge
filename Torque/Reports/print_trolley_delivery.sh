#!/bin/sh

echo "Starting Script"

echo ""

pClientId=$2

pTrolley=$3

pCons=$4

pPrinterId=$5

echo "Printer: $pPrinterId"

echo ""

{

sqlplus -s $ORACLE_USR << !
@/home2/dcsdba/reports/print_trolley_delivery.sql $pClientId $pPrinterId $pTrolley $pCons "VALID"
exit
/
!
} |
while read line

do

  if [ "$line" ] # Line not NULL

  then

      set $line

	if [ "$4" == "VALID" ]
	
	then
		clientId="$1"
		printerId="$2"
		orderId="$3"
		primarySite="http://torquecourier/Order/PrintByUrl?labelOnly=N&generateNow=Y&clientId=$clientId&printerId=$printerId&orderId=$orderId"
		primaryResponse=$(curl --write-out %{http_code} --silent --max-time 60 --output /dev/null "$primarySite")
		echo $primaryResponse
		echo "Client: $clientId"
		echo "Order: $orderId"
		echo $primarySite
		echo ""
		
	fi

  fi   

done
