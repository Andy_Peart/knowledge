SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Shipped Date,Order Id,Sku,Embossing,Qty Shipped,UserDefType1' from dual
union all
select "Shipped Date"||','||"Order Id"||','||"Sku"||','||"Embossing"||','||"Qty Shipped" || ',' || "UDT1" from
(
select trunc(oh.shipped_date)   as "Shipped Date"
, oh.order_id                   as "Order Id"
, ol.sku_id                     as "Sku"
, ol.user_def_note_1            as "Embossing"
, ol.qty_shipped                as "Qty Shipped"
, ol.user_def_type_1             as "UDT1"
from order_header oh
inner join order_line ol on ol.order_id = oh.order_id and ol.client_id = oh.client_id
inner join sku s on s.client_id = ol.client_id and s.sku_id = ol.sku_id
where oh.client_id = 'AZ'
and oh.status = 'Shipped'
/*and ol.user_def_note_1 is not null*/
and oh.shipped_Date between to_date('&&2') and to_date('&&3')+1
order by 1
)
/
--spool off