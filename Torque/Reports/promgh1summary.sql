SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 200
SET TRIMSPOOL ON
 

column XX heading 'HB' format a8
column A heading 'CN' format a8
column B heading 'Ord Ref' format A20
column D heading 'Date' format A10
column C heading 'Items' format 999999
column E heading 'Boxed' format 999999.99
column F heading 'Hanging' format 999999.99

break on XX dup skip 1

--spool promghsummary.csv

select 'PROMINENT Shipments from &&2 to &&3' from DUAL;

select 'Customer Name,Order reference,Shipped date,Lines shipped,Units shipped(B),Units shipped(H)' from dual;


select
A||' '||oh.user_def_type_3 XX,
B,
D,
count(*) C,
sum(H-nvl(H2,0)) E,
sum(nvl(H2,0)) F
from (select
sku.user_def_type_2  A,
itl.reference_id B,
to_char(itl.dstamp,'DD/MM/YYYY') D,
itl.line_id E,
itl.sku_id F,
sum(itl.update_qty*nvl(sku.user_def_type_9,1)*DECODE(itl.code,'Shipment',1,-1)) H
from inventory_transaction itl,sku
where itl.client_id='PR'
--and itl.zone_1<>'P'
and itl.code in ('UnShipment','Shipment')
--and itl.dstamp>sysdate-1
and itl.from_loc_id='PR'
and itl.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and sku.client_id='PR'
and sku.user_def_type_2 like '%&&4%'
and itl.sku_id=sku.sku_id
group by
sku.user_def_type_2,
itl.reference_id,
to_char(itl.dstamp,'DD/MM/YYYY'),
itl.line_id,
itl.sku_id),(select
sku.user_def_type_2  A2,
itl.reference_id B2,
to_char(itl.dstamp,'DD/MM/YYYY') D2,
itl.line_id E2,
itl.sku_id F2,
sum(itl.update_qty*nvl(sku.user_def_type_9,1)*DECODE(itl.code,'Shipment',1,-1)) H2
from inventory_transaction itl,sku
where itl.client_id='PR'
--and itl.zone_1<>'P'
and itl.code in ('UnShipment','Shipment')
--and itl.dstamp>sysdate-1
and itl.from_loc_id='PR'
and itl.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and sku.client_id='PR'
and sku.user_def_type_1= 'H'
and sku.user_def_type_2 like '%&&4%'
and itl.sku_id=sku.sku_id
group by
sku.user_def_type_2,
itl.reference_id,
to_char(itl.dstamp,'DD/MM/YYYY'),
itl.line_id,
itl.sku_id),order_header oh
where A=A2(+)
and B=B2(+)
and D=D2(+)
and E=E2(+)
and F=F2(+)
and B=oh.order_id
and oh.client_id='PR'
group by
oh.user_def_type_3,A,B,D
union
select
A||' '||X XX,
'Z Average Order Size' B,
' ' D,
X1 C,
X2/X1 E,
X3/X1 F
from (select
oh.user_def_type_3 X,
A,
count(distinct B) X1,
sum(H-nvl(H2,0)) X2,
sum(nvl(H2,0)) X3
from (select
sku.user_def_type_2  A,
itl.reference_id B,
to_char(itl.dstamp,'DD/MM/YYYY') D,
itl.line_id E,
itl.sku_id F,
sum(itl.update_qty*nvl(sku.user_def_type_9,1)*DECODE(itl.code,'Shipment',1,-1)) H
from inventory_transaction itl,sku
where itl.client_id='PR'
--and itl.zone_1<>'P'
and itl.code in ('UnShipment','Shipment')
--and itl.dstamp>sysdate-1
and itl.from_loc_id='PR'
and itl.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and sku.client_id='PR'
and sku.user_def_type_2 like '%&&4%'
and itl.sku_id=sku.sku_id
group by
sku.user_def_type_2,
itl.reference_id,
to_char(itl.dstamp,'DD/MM/YYYY'),
itl.line_id,
itl.sku_id),(select
sku.user_def_type_2  A2,
itl.reference_id B2,
to_char(itl.dstamp,'DD/MM/YYYY') D2,
itl.line_id E2,
itl.sku_id F2,
sum(itl.update_qty*nvl(sku.user_def_type_9,1)*DECODE(itl.code,'Shipment',1,-1)) H2
from inventory_transaction itl,sku
where itl.client_id='PR'
--and itl.zone_1<>'P'
and itl.code in ('UnShipment','Shipment')
--and itl.dstamp>sysdate-1
and itl.from_loc_id='PR'
and itl.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and sku.client_id='PR'
and sku.user_def_type_1= 'H'
and sku.user_def_type_2 like '%&&4%'
and itl.sku_id=sku.sku_id
group by
sku.user_def_type_2,
itl.reference_id,
to_char(itl.dstamp,'DD/MM/YYYY'),
itl.line_id,
itl.sku_id),order_header oh
where A=A2(+)
and B=B2(+)
and D=D2(+)
and E=E2(+)
and F=F2(+)
and B=oh.order_id
and oh.client_id='PR'
group by
oh.user_def_type_3,A)
order by
XX,B,D
/

--spool off

