SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240
SET TRIMSPOOL ON

COLUMN NEWSEQ NEW_VALUE SEQNO
COLUMN FOLD NEW_VALUE FOLDER

select 'S://Redprairie/Apps/live_dcs/comms/outtray/TMS'||substr(to_char(1000000000+next_seq),2,9)||'.csv' NEWSEQ from sequence_file
--select 'TMS'||substr(to_char(1000000000+next_seq),2,9)||'.csv' NEWSEQ from sequence_file
where control_id=10

/


SET TERMOUT OFF
spool &SEQNO


select
A1||','||
A2||','||
A3||','||
A4||','||
A5||','||
A6||','||
A7||','||
A8||','||
A9||','||
A10||','||
A10A||','||
A10B||','||
A10C||','||
A11||','||
A12||','||
A12A||','||
'"'||A13||'",'||
A14
from (select
'ISM,E,T' A1,
i.sku_id A2,
' , , , ,GOOD, ' A3,
count(*) A4,
sum(qty_on_hand) A5,
sum(qty_on_hand)-nvl(LK1,0) A6,
nvl(LK1,0) A7,
sum(qty_allocated) A8,
sum(qty_allocated)-nvl(LK2,0) A9,
nvl(LK2,0) A10,
sum(qty_on_hand-qty_allocated) A10A,
sum(qty_on_hand-qty_allocated)-nvl(LK2,0) A10B,
nvl(LK1,0)-nvl(LK2,0) A10C,
'0,+' A11,
nvl(SUSP,0) A12,
'0' A12A,
sku.description A13,
'EUROPE/LONDON' A14
from inventory i,sku,
(select sku_id JB, sum(qty_on_hand) LK1, sum(qty_allocated) LK2 from inventory
where client_id='TR'
and lock_status='Locked'
group by sku_id),
(select sku_id JC,sum(qty_on_hand) SUSP from inventory
where client_id='TR'
and location_id='SUSPENSE'
group by sku_id)
where i.client_id='TR'
and i.sku_id = JB (+)
and i.sku_id = JC (+)
--and i.sku_id = '5050975968909'
and i.sku_id=sku.sku_id
and sku.client_id='TR'
group by
i.sku_id,
nvl(LK1,0),
nvl(LK2,0),
nvl(SUSP,0),
sku.description)
order by A2
/

spool off
SET TERMOUT ON

Update sequence_file
set next_seq=next_seq+1
where control_id=10
/

commit;







