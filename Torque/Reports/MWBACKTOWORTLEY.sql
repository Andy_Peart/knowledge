SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','
SET WRAP OFF


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500

column A heading 'SKU ID' format A18
column J heading 'Description' format A40
column B heading 'Qty in Bulk' format 99999
column C heading 'Qty on Shelves' format 99999
column D heading 'Reserve Qty  ' format 99999
column E heading 'Required' format 99999

--set termout off
--spool MWBTW.csv



Select 'SKU,Description,Qty in Bulk,Qty on Shelves,Reserve Qty,Required,,' from DUAL;

--break on D dup

--compute sum label 'Order Total' of U V W on D


select
''''||sku.sku_id||'''' as A,
sku.description as J,
nvl(B1,0) as B,
nvl(B2,0) as C,
nvl(sku.user_def_num_4,0) as D,
nvl(sku.user_def_num_4,0)-nvl(B2,0) as E,
' '
from sku,(select
sku_id  A1,
sum(qty_on_hand) B1
from inventory
where client_id='MOUNTAIN'
--and condition_id='MMAIL'
and location_id like 'M%'
or location_id like 'H%'
group by
sku_id),(select
sku_id  A2,
sum(qty_on_hand) B2
from inventory
where client_id='MOUNTAIN'
--and condition_id='MMAIL'
and substr(location_id,1,1) in ('A','B','C','D','E','F','G','P','S')
and location_id<>'DESPATCH'
and location_id<>'SUSPENSE'
group by
sku_id)
where sku.client_id='MOUNTAIN'
and sku.sku_id=A1 (+) 
and sku.sku_id=A2 (+)
and nvl(B1,0)+nvl(B2,0)>0
/


--spool off
--set termout on

