SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 52


spool PromStock.csv
 

select 'Location,SKU,Qty,Customer' from DUAL;

select
i.location_id||','||
i.sku_id||','||
sum(i.qty_on_hand)
from inventory i
where i.client_id='PR'
and i.site_id='PROM'
and i.zone_1<>'P'
and i.location_id between '&&2' and '&&3'
group by
i.location_id,
i.sku_id,
order by
i.location_id,
i.sku_id
/


spool off
