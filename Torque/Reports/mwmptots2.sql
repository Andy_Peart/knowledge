SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

column A heading 'User ID' format a14
column B heading 'Name' format a30
column C heading 'Picks' format 99999999
column C1 heading 'Pick1' format 99999999
column C2 heading 'Pick2' format 99999999
column D heading 'Receipts' format 99999999
column E2 heading 'Relocate Tasks' format 99999999
column E heading 'Relocate Units' format 99999999
column F heading 'Returns' format 99999999
column G heading 'IDTs' format 99999999
column K heading 'Tasks' format 99999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


break on report

compute sum label 'Totals' of C C1 C2 D E E2 F G K on report

--spool mwmptots2.csv

select 'Retail Shipments by Month from &&2 to &&3 - for client  MOUNTAIN' from DUAL;

select 'Month,Tasks,Units Shipped,Pack Configs,Pack Config Units' from DUAL;



select
J1,
nvl(KA,0) K,
nvl(JA,0) C,
nvl(CT,0) C2,
nvl(JF,0) C1
from
(select
to_char(it.Dstamp,'YYYY MM') J1,
sum(it.update_qty) JA,
count(*) KA
from inventory_transaction it
--,location ll
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='MOUNTAIN'
and it.update_qty<>0
and it.code='Shipment'
--and it.from_loc_id=ll.location_id
--and ll.site_id='BD2'
--and ll.zone_1 in ('RETAIL','DYN1')
group by to_char(it.Dstamp,'YYYY MM')
union
select
to_char(it.Dstamp,'YYYY MM') J1,
sum(it.update_qty) JA,
count(*) KA
from inventory_transaction_archive it
--,location ll
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='MOUNTAIN'
and it.update_qty<>0
and it.code='Shipment'
--and it.from_loc_id=ll.location_id
--and ll.site_id='BD2'
--and ll.zone_1 in ('RETAIL','DYN1')
group by to_char(it.Dstamp,'YYYY MM')),
(select
to_char(it.Dstamp,'YYYY MM') J6,
count(*) CT,
sum(it.update_qty) JF
from inventory_transaction it,sku_config sc
--,location ll
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='MOUNTAIN'
and sc.client_id='MOUNTAIN'
and it.sku_id=sc.config_id
and it.code='Shipment'
and it.update_qty<>0
--and it.from_loc_id=ll.location_id
--and ll.site_id='BD2'
--and ll.zone_1 in ('RETAIL','DYN1')
group by
to_char(it.Dstamp,'YYYY MM')
union
select
to_char(it.Dstamp,'YYYY MM') J6,
count(*) CT,
sum(it.update_qty) JF
from inventory_transaction_archive it,sku_config sc
--,location ll
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='MOUNTAIN'
and sc.client_id='MOUNTAIN'
and it.sku_id=sc.config_id
and it.code='Shipment'
and it.update_qty<>0
--and it.from_loc_id=ll.location_id
--and ll.site_id='BD2'
--and ll.zone_1 in ('RETAIL','DYN1')
group by
to_char(it.Dstamp,'YYYY MM'))
where J1=J6 (+)
order by J1
/

--spool off
