SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 50
SET NEWPAGE 1
SET LINESIZE 200
SET TRIMSPOOL ON


 

column A heading 'Date' format A12
column B heading 'Type' format A20
column C heading 'Qty' format 9999999
column D heading '' format 999


break on A skip 1 on report
compute sum label 'Day Total' of C on A
compute sum label 'Total' of C on report
--break on report
--compute sum label 'Total' of C D on report

TTITLE LEFT 'Total Received by Date Range &&2 to &&3' skip 2

select
trunc(it.Dstamp) A,
it.reference_id B,
--DECODE(sku.description,'JACKETS','JACKETS','JACKET','JACKETS',
--'WAISTCOAT','WAISTCOATS','WAISTCOATS','WAISTCOATS',
--'SUIT','SUITS','SUITS','SUITS',sku.description) B,
sum(it.update_qty) C,
' '
from inventory_transaction it,sku
where it.client_id='BW'
and it.station_id not like 'Auto%'
and (it.code like 'Receipt%' or (it.code='Adjustment' and it.reason_id='RECRV'))
and it.dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1
and sku.client_id='BW'
and it.sku_id=sku.sku_id
group by
trunc(it.Dstamp),
it.reference_id
--DECODE(sku.description,'JACKETS','JACKETS','JACKET','JACKETS',
--'WAISTCOAT','WAISTCOATS','WAISTCOATS','WAISTCOATS',
--'SUIT','SUITS','SUITS','SUITS',sku.description) 
order by
A,B
/


CLEAR BREAKS
CLEAR COMPUTES
SET PAGESIZE 0
SET NEWPAGE 0

SELECT '' FROM dual;
SELECT '' FROM dual;

select
'Number of Orders = ',
count(distinct reference_id) D
from inventory_transaction it
where it.client_id='BW'
and it.station_id not like 'Auto%'
and (it.code='Receipt' or (it.code='Adjustment' and it.reason_id='RECRV'))
and it.dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1
/




