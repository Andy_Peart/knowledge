SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 480
SET TRIMSPOOL ON

column A heading 'Qty' format 9999999
column X heading 'Qty' format 9999999
column Y heading 'Qty 2' format 9999999
column Z heading 'Total' format 9999999

select 'Relocate Sku G from RETAQL to GIVE locations between &&2 and &&3' from DUAL;
select 'Relocate Qty Sku G,Relocate Qty Sku C,Relocate Qty Sku U, Total' from DUAL;

break on report

select A, X, Y, A+X+Y Z
from (select nvl(sum (i.update_qty),0) A 
from inventory_transaction i, location l, sku s 
where i.client_id = 'GV'
and s.sku_id like ('G%')
and s.sku_id = i.sku_id (+)
and i.from_loc_id ='RETAQL'
and i.to_loc_id = l.location_id
and l.zone_1 ='GIVE'
and i.code ='Relocate'
and l.site_id ='GIVE'
and i.dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1),
(select nvl(sum (i.update_qty),0) X
from inventory_transaction i, location l, sku s 
where i.client_id = 'GV'
and s.sku_id like ('C%')
and s.sku_id = i.sku_id (+)
and i.from_loc_id ='RETAQL'
and i.to_loc_id = l.location_id
and l.zone_1 ='GIVE'
and i.code ='Relocate'
and l.site_id ='GIVE'
and i.dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1),
(select nvl(sum (i.update_qty),0) Y
from inventory_transaction i, location l, sku s 
where i.client_id = 'GV'
and s.sku_id like ('U%')
and s.sku_id = i.sku_id (+)
and i.from_loc_id ='RETAQL'
and i.to_loc_id = l.location_id
and l.zone_1 ='GIVE'
and i.code ='Relocate'
and l.site_id ='GIVE'
and i.dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&2', 'DD/MM/YY')+1)
/