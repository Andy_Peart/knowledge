SET FEEDBACK OFF             
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 2000
SET TRIMSPOOL ON

select 'Order ID,SkuID,Sku Description,EAN,Qty Ordered,Qty Picked,Pallet,Container' from dual
union all 
select a.order_id || ',' || a.sku_id || ',' || b.description || ',' || b.ean || ',' || to_char(a.qty_ordered) || ',' || to_char(t.update_qty) || ',' || pallet_id || ',' || container_id
from order_line a
inner join sku b on a.sku_id = b.sku_id and a.client_id = b.client_id
inner join order_header c on a.order_id = c.order_id and a.client_id = c.client_id
left join 
(
	select 	a.client_id, a.reference_id, a.sku_id, sum(a.update_qty) update_qty, a.pallet_id, a.container_id, a.line_id
	from inventory_transaction a 
	inner join order_header c on a.reference_id = c.order_id and a.client_id = c.client_id
	where a.client_id = 'HS' 
	and code in ('Pick','UnPick') 
	and nvl(a.from_loc_id,'XXXXX') <> 'CONTAINER'
	and c.consignment = '&&2'
	group by a.client_id, a.reference_id, a.sku_id, a.pallet_id, a.container_id, a.line_id
) t on a.client_id = t.client_id and t.reference_id = a.order_id and t.sku_id = a.sku_id and t.line_id = a.line_id 
where a.client_id = 'HS'
and c.consignment = '&&2'
/