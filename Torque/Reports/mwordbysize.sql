SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 900
SET TRIMSPOOL ON
SET HEADING ON

select 'Priority,Creation date,Creation time,Units in order,Total orders,Total units' from dual
union all
select priority || ',' || creation_date || ',' || creation_time || ',' || units_in_order || ',' || total_orders || ',' || total_units 
from (
select oh.priority, to_char(oh.creation_date, 'DD-MM-YYYY') as creation_date, to_char(oh.creation_date,'HH24') || ' to ' || to_char(to_char(oh.creation_date,'HH24')+1) creation_time,
case 
    when oh.order_volume = 0.0001 then '1'
    when oh.order_volume = 0.0002 then '2'
    else '>2'
end units_in_order, count(*) total_orders, round(sum(oh.order_volume*10000))  total_units
from order_header oh
where oh.client_id = 'MOUNTAIN'
and oh.order_type = 'WEB'
--and oh.priority like '%&&4%'
and oh.creation_date between to_date('&&2') and to_date('&&3')+1
group by
oh.priority, to_char(oh.creation_date, 'DD-MM-YYYY'), to_char(oh.creation_date,'HH24'),
case 
    when oh.order_volume = 0.0001 then '1'
    when oh.order_volume = 0.0002 then '2'
    else '>2'
end 
order by 1,2,3,4
)
/