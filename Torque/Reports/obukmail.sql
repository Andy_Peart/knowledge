SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep '|'


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 300
SET TRIMSPOOL ON
 
column XX NOPRINT

--spool obukmail.csv

select
order_id  XX,
'I680007'||'|'||
to_char(Sysdate,'DDMMYYYY')||'|'||
contact||'|'||
Name||'|'||
Address1||'|'||
Address2||'|'||
Town||'|'||
County||'|||'||
PostCode||'||'||
'&&3'||'|'||
'10'||'|'||
'1'||'|'||
instructions||'|'||
USER_DEF_NOTE_2||'|||||'||
'OB'||order_id||'|||||||||||||||'||
'Torque (Olibar Brown) Wortley Moor R Wortley Leeds LS12 4JH'||'||'||
' '
from order_header,(select
oh.order_id AAA,
sum(nvl(ol.line_value,0)) BBB
from order_header oh, order_line ol
where oh.client_id='OB'
and oh.country='GBR'
and oh.order_id='&&2'
and ol.client_id='OB'
and oh.order_id=ol.order_id
group by
oh.order_id)
where client_id='OB'
and order_id='&&2'
and order_id=AAA 
ORDER BY ORDER_ID
/

--spool off
