SET FEEDBACK OFF                 
set pagesize 68
set linesize 120
set verify off
set wrap off
set newpage 0
set colsep ' '
set und on

clear columns
clear breaks
clear computes


column A heading 'Order Date' format A11
column B heading 'Pallet' format A10
column C heading 'Order ID' format A22
column D heading 'Consignment' format A14
column DD heading 'Check String' format A12
column E heading 'User ID' format A14
column L heading 'List No' format A13
column F heading 'SKUs' format 99999
column G heading 'Units' format 99999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



ttitle  LEFT 'TML Retail - Units still in CONTAINER for Ship Dock &&2 ' RIGHT 'Printed ' report_date skip 2


break on report

compute sum label 'Totals' of F G on report

select
to_char(it.dstamp, 'DD/MM/YY') A,
it.pallet_id B,
it.reference_id C,
it.consignment D,
lc.check_string DD,
it.user_id E,
mt.list_id L,
count(*) F,
sum(mt.qty_to_move) G
from inventory_transaction it, inventory iv, move_task mt, location lc
where it.to_loc_id='CONTAINER'
and iv.location_id='CONTAINER'
and it.client_id='TR'
and iv.client_id='TR'
and it.reference_id like '%&&2%'
and it.pallet_id=iv.pallet_id
and it.tag_id=iv.tag_id
and it.consignment=mt.consignment
and it.reference_id=mt.task_id
and it.customer_id=lc.location_id
and lc.loc_type='ShipDock'
group by
to_char(it.dstamp, 'DD/MM/YY'),
it.pallet_id,
it.reference_id,
it.consignment,
lc.check_string,
it.user_id,
mt.list_id
order by
to_char(it.dstamp, 'DD/MM/YY'),
it.pallet_id,
it.reference_id,
it.consignment,
it.user_id
/

