SET FEEDBACK OFF           
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 300
SET TRIMSPOOL ON



column M heading '' format a4 noprint
column D heading 'Date' format a12
column A heading 'SKU ID' format a18
column B heading '+Adjustment' format 99999999
column F heading '-Adjustment' format 99999999
column C heading 'Description' format A40

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


--ttitle  LEFT 'Units Adjusted from &&2 to &&3 - for client ID &&4 as at ' report_date skip 2

select 'Date,SKU ID,Adjustment Up,Adjustment Down,Description,,' from DUAL;

break on report

compute sum label 'Totals' of B F on report

select * from
(select
to_char(it.Dstamp, 'YYMM') M,
to_char(it.Dstamp, 'DD/MM/YY') D,
it.sku_id A,
sum(it.update_qty) as B,
0 as F,
sku.description C
from inventory_transaction it,sku
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='&&4'
and sku.client_id='&&4'
and it.code='Adjustment'
and it.sku_id=sku.sku_id
group by
to_char(it.Dstamp, 'YYMM'),
to_char(it.Dstamp, 'DD/MM/YY'),
it.sku_id,
sku.description)
where B>0
union 
select * from
(select
to_char(it.Dstamp, 'YYMM') M,
to_char(it.Dstamp, 'DD/MM/YY') D,
it.sku_id A,
0 as B,
sum(it.update_qty) as F,
sku.description C
from inventory_transaction it,sku
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='&&4'
and sku.client_id='&&4'
and it.code='Adjustment'
and it.sku_id=sku.sku_id
group by
to_char(it.Dstamp, 'YYMM'),
to_char(it.Dstamp, 'DD/MM/YY'),
it.sku_id,
sku.description)
where F<0
/
