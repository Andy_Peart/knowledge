/******************************************************************************/
/*                                                                            */
/*                            Elite                                           */
/*                                                                            */
/*     FILE NAME  :   mwshortages4.sql                                        */
/*                                                                            */
/*   DATE     BY   PROJ     ID         DESCRIPTION                            */
/*   ======== ==== ======   ========   =============                          */
/*   25/01/06 BK   Mountain            Pick shortage Totals by Work Group     */
/******************************************************************************/

/* Input Parameters are as follows */
-- 1)  From Date
-- 2)  To Date


set feedback off
set pagesize 68
set linesize 240
set verify off

clear columns
clear breaks
clear computes

column A heading 'Work Group' format A16
column B heading 'Qty' format 999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'Pick Shortage Totals by Work Group from '&&2' to '&&3' - for client ID MOUNTAIN as at ' report_date skip 2

break on report
compute sum label 'Total' of B on report

select
work_group A,
sum(old_qty_to_move-qty_to_move) B
from move_descrepancy
where client_id='MOUNTAIN'
and Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and reason_code='EX_NE'
group by 
work_group
/

