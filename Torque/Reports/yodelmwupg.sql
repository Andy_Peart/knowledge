SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep '|' 


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 800
SET TRIMSPOOL ON
 
column XX NOPRINT

--spool ukmail.csv
-- check and update itl lines user_def_chk_2 to Z
UPDATE INVENTORY_TRANSACTION
SET USER_DEF_CHK_2 = 'Z'
WHERE CLIENT_ID    = 'MOUNTAIN'
AND DSTAMP BETWEEN TRUNC(sysdate)-1 AND TRUNC(sysdate)+1
AND CODE                    = 'Pick'
AND FROM_LOC_ID             in ('CONTAINER','UKPAR')
AND TO_LOC_ID               = 'UKPAR'
AND NVL(USER_DEF_CHK_2,'N') = 'N'
/
select
  '9946292' as "home non pod",
  trim(ORDER_ID),
  ORDER_TYPE,
  PRIORITY,
  WORK_GROUP,
  CONSIGNMENT,
  TO_CHAR(sysdate, 'dd-mm-yyyy') as "Collection Date",
  DISPATCH_METHOD,
  NAME,
  CONTACT,
  ADDRESS1,
  ADDRESS2,
  town,
  county,
  postcode,
  country,
  contact_phone,
  SUBSTR(INSTRUCTIONS,1,35),
  '1' as "no of cartons",
  '1' as "Weight",
  '99' as "local prod code",
  '71' as "local service code for 24hrs"
from order_header
where client_id = 'MOUNTAIN'
and order_id in (
SELECT ITL.REFERENCE_ID
FROM inventory_transaction itl
WHERE ITL.CLIENT_ID = 'MOUNTAIN'
AND ITL.DSTAMP BETWEEN TRUNC(sysdate)-1 AND TRUNC(sysdate)+1
AND ITL.CODE        = 'Pick'
AND ITL.FROM_LOC_ID IN ('CONTAINER','UKPAR')
and ITL.TO_LOC_ID   = 'UKPAR'
AND NVL(ITL.USER_DEF_CHK_2,'N') = 'Z'
GROUP BY ITL.REFERENCE_ID )
order by order_id
/
UPDATE INVENTORY_TRANSACTION
SET USER_DEF_CHK_2 = 'Y'
WHERE CLIENT_ID    = 'MOUNTAIN'
AND DSTAMP BETWEEN TRUNC(sysdate)-1 AND TRUNC(sysdate)+1
AND CODE                    = 'Pick'
AND FROM_LOC_ID             in ('CONTAINER','UKPAR')
AND TO_LOC_ID               = 'UKPAR'
AND NVL(USER_DEF_CHK_2,'N') = 'Z'
/

--spool off


