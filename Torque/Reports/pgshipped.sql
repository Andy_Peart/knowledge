SET FEEDBACK OFF                
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 400
SET TRIMSPOOL ON


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

spool pgrec.csv

select 'Total Units Shipped from &&2 to &&3 - for client PING as at '||to_char(SYSDATE, 'DD/MM/YY  HH:MI') from DUAL;

select 'Date,Reference,Sku Id,Qty Shipped' from DUAL;

select 
D||','||
A||','||
XX||','||
B
from (select
trunc(it.Dstamp) D,
it.reference_id A,
DECODE(it.sku_id,'P01104-999-99','P01104-999-99','P99955-999-99','P99955-999-99','P00179-999-99','P00179-999-99',
'P00998-999-99','P00998-999-99','ALL OTHERS') XX,
sum(it.update_qty) as B
from inventory_transaction it
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='PG'
and it.station_id not like 'Auto%'
and it.code like 'Shipment%'
group by
trunc(it.Dstamp),
it.reference_id,
DECODE(it.sku_id,'P01104-999-99','P01104-999-99','P99955-999-99','P99955-999-99','P00179-999-99','P00179-999-99',
'P00998-999-99','P00998-999-99','ALL OTHERS')
order by D,A)
union all
select 
D||','||
A||','||
XX||','||
B
from (select
'' D,
'TOTALS ' A,
DECODE(it.sku_id,'P01104-999-99','P01104-999-99','P99955-999-99','P99955-999-99','P00179-999-99','P00179-999-99',
'P00998-999-99','P00998-999-99','ALL OTHERS') XX,
sum(it.update_qty) as B
from inventory_transaction it
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='PG'
and it.station_id not like 'Auto%'
and it.code like 'Shipment%'
group by
DECODE(it.sku_id,'P01104-999-99','P01104-999-99','P99955-999-99','P99955-999-99','P00179-999-99','P00179-999-99',
'P00998-999-99','P00998-999-99','ALL OTHERS'))
/

spool off
