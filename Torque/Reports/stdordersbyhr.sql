SET FEEDBACK OFF
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 900
SET TRIMSPOOL ON

--SET TERMOUT OFF
break on report
compute sum label 'Totals' of C0 C1 C2 C3 C4 C5 C6 C7 C8 C9 C10 C11 C12 C13 C14 C15 C16 C17 C18 C19 C20 C21 C22 C23 CL on report

spool stdordersbyhr.csv

select 'Orders Received &&3 to &&4 for Client &&2' from DUAL;
select 'Date,00,01,02,03,04,05,06,07,08,09,10,11,12,13,14,15,16,17,18,19,20,21,22,23,ALL' from DUAL;

select
A,
sum(C0) C0,
sum(C1) C1,
sum(C2) C2,
sum(C3) C3,
sum(C4) C4,
sum(C5) C5,
sum(C6) C6,
sum(C7) C7,
sum(C8) C8,
sum(C9) C9,
sum(C10) C10,
sum(C11) C11,
sum(C12) C12,
sum(C13) C13,
sum(C14) C14,
sum(C15) C15,
sum(C16) C16,
sum(C17) C17,
sum(C18) C18,
sum(C19) C19,
sum(C20) C20,
sum(C21) C21,
sum(C22) C22,
sum(C23) C23,
sum(C0)+sum(C1)+sum(C2)+sum(C3)+sum(C4)+sum(C5)+sum(C6)+sum(C7)+sum(C8)+sum(C9)+sum(C10)+sum(C11)+sum(C12)+
sum(C13)+sum(C14)+sum(C15)+sum(C16)+sum(C17)+sum(C18)+sum(C19)+sum(C20)+sum(C21)+sum(C22)+sum(C23) CL
from (select
A,
DECODE(B,'00',CT,0) C0,
DECODE(B,'01',CT,0) C1,
DECODE(B,'02',CT,0) C2,
DECODE(B,'03',CT,0) C3,
DECODE(B,'04',CT,0) C4,
DECODE(B,'05',CT,0) C5,
DECODE(B,'06',CT,0) C6,
DECODE(B,'07',CT,0) C7,
DECODE(B,'08',CT,0) C8,
DECODE(B,'09',CT,0) C9,
DECODE(B,'10',CT,0) C10,
DECODE(B,'11',CT,0) C11,
DECODE(B,'12',CT,0) C12,
DECODE(B,'13',CT,0) C13,
DECODE(B,'14',CT,0) C14,
DECODE(B,'15',CT,0) C15,
DECODE(B,'16',CT,0) C16,
DECODE(B,'17',CT,0) C17,
DECODE(B,'18',CT,0) C18,
DECODE(B,'19',CT,0) C19,
DECODE(B,'20',CT,0) C20,
DECODE(B,'21',CT,0) C21,
DECODE(B,'22',CT,0) C22,
DECODE(B,'23',CT,0) C23
from (select
trunc(creation_date) A,
to_char(creation_date,'HH24') B,
count(*) CT
from order_header
where client_id='&&2'
and creation_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
group by
trunc(creation_date),
to_char(creation_date,'HH24')
order by A,B))
group by A
order by A
/

select 'PERCENTAGES' from DUAL;

select
A,
to_char((C0*100)/CL,'990.99'),
to_char((C1*100)/CL,'990.99'),
to_char((C2*100)/CL,'990.99'),
to_char((C3*100)/CL,'990.99'),
to_char((C4*100)/CL,'990.99'),
to_char((C5*100)/CL,'990.99'),
to_char((C6*100)/CL,'990.99'),
to_char((C7*100)/CL,'990.99'),
to_char((C8*100)/CL,'990.99'),
to_char((C9*100)/CL,'990.99'),
to_char((C10*100)/CL,'990.99'),
to_char((C11*100)/CL,'990.99'),
to_char((C12*100)/CL,'990.99'),
to_char((C13*100)/CL,'990.99'),
to_char((C14*100)/CL,'990.99'),
to_char((C15*100)/CL,'990.99'),
to_char((C16*100)/CL,'990.99'),
to_char((C17*100)/CL,'990.99'),
to_char((C18*100)/CL,'990.99'),
to_char((C19*100)/CL,'990.99'),
to_char((C20*100)/CL,'990.99'),
to_char((C21*100)/CL,'990.99'),
to_char((C22*100)/CL,'990.99'),
to_char((C23*100)/CL,'990.99'),
100
from (select
A,
sum(C0) C0,
sum(C1) C1,
sum(C2) C2,
sum(C3) C3,
sum(C4) C4,
sum(C5) C5,
sum(C6) C6,
sum(C7) C7,
sum(C8) C8,
sum(C9) C9,
sum(C10) C10,
sum(C11) C11,
sum(C12) C12,
sum(C13) C13,
sum(C14) C14,
sum(C15) C15,
sum(C16) C16,
sum(C17) C17,
sum(C18) C18,
sum(C19) C19,
sum(C20) C20,
sum(C21) C21,
sum(C22) C22,
sum(C23) C23,
sum(C0)+sum(C1)+sum(C2)+sum(C3)+sum(C4)+sum(C5)+sum(C6)+sum(C7)+sum(C8)+sum(C9)+sum(C10)+sum(C11)+sum(C12)+
sum(C13)+sum(C14)+sum(C15)+sum(C16)+sum(C17)+sum(C18)+sum(C19)+sum(C20)+sum(C21)+sum(C22)+sum(C23) CL
from (select
A,
DECODE(B,'00',CT,0) C0,
DECODE(B,'01',CT,0) C1,
DECODE(B,'02',CT,0) C2,
DECODE(B,'03',CT,0) C3,
DECODE(B,'04',CT,0) C4,
DECODE(B,'05',CT,0) C5,
DECODE(B,'06',CT,0) C6,
DECODE(B,'07',CT,0) C7,
DECODE(B,'08',CT,0) C8,
DECODE(B,'09',CT,0) C9,
DECODE(B,'10',CT,0) C10,
DECODE(B,'11',CT,0) C11,
DECODE(B,'12',CT,0) C12,
DECODE(B,'13',CT,0) C13,
DECODE(B,'14',CT,0) C14,
DECODE(B,'15',CT,0) C15,
DECODE(B,'16',CT,0) C16,
DECODE(B,'17',CT,0) C17,
DECODE(B,'18',CT,0) C18,
DECODE(B,'19',CT,0) C19,
DECODE(B,'20',CT,0) C20,
DECODE(B,'21',CT,0) C21,
DECODE(B,'22',CT,0) C22,
DECODE(B,'23',CT,0) C23
from (select
trunc(creation_date) A,
to_char(creation_date,'HH24') B,
count(*) CT
from order_header
where client_id='&&2'
and creation_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
group by
trunc(creation_date),
to_char(creation_date,'HH24')
order by A,B))
group by A
order by A)
/

select
'Total %s',
to_char((C0*100)/CL,'990.99'),
to_char((C1*100)/CL,'990.99'),
to_char((C2*100)/CL,'990.99'),
to_char((C3*100)/CL,'990.99'),
to_char((C4*100)/CL,'990.99'),
to_char((C5*100)/CL,'990.99'),
to_char((C6*100)/CL,'990.99'),
to_char((C7*100)/CL,'990.99'),
to_char((C8*100)/CL,'990.99'),
to_char((C9*100)/CL,'990.99'),
to_char((C10*100)/CL,'990.99'),
to_char((C11*100)/CL,'990.99'),
to_char((C12*100)/CL,'990.99'),
to_char((C13*100)/CL,'990.99'),
to_char((C14*100)/CL,'990.99'),
to_char((C15*100)/CL,'990.99'),
to_char((C16*100)/CL,'990.99'),
to_char((C17*100)/CL,'990.99'),
to_char((C18*100)/CL,'990.99'),
to_char((C19*100)/CL,'990.99'),
to_char((C20*100)/CL,'990.99'),
to_char((C21*100)/CL,'990.99'),
to_char((C22*100)/CL,'990.99'),
to_char((C23*100)/CL,'990.99'),
100
from (select
sum(C0) C0,
sum(C1) C1,
sum(C2) C2,
sum(C3) C3,
sum(C4) C4,
sum(C5) C5,
sum(C6) C6,
sum(C7) C7,
sum(C8) C8,
sum(C9) C9,
sum(C10) C10,
sum(C11) C11,
sum(C12) C12,
sum(C13) C13,
sum(C14) C14,
sum(C15) C15,
sum(C16) C16,
sum(C17) C17,
sum(C18) C18,
sum(C19) C19,
sum(C20) C20,
sum(C21) C21,
sum(C22) C22,
sum(C23) C23,
sum(C0)+sum(C1)+sum(C2)+sum(C3)+sum(C4)+sum(C5)+sum(C6)+sum(C7)+sum(C8)+sum(C9)+sum(C10)+sum(C11)+sum(C12)+
sum(C13)+sum(C14)+sum(C15)+sum(C16)+sum(C17)+sum(C18)+sum(C19)+sum(C20)+sum(C21)+sum(C22)+sum(C23) CL
from (select
A,
DECODE(B,'00',CT,0) C0,
DECODE(B,'01',CT,0) C1,
DECODE(B,'02',CT,0) C2,
DECODE(B,'03',CT,0) C3,
DECODE(B,'04',CT,0) C4,
DECODE(B,'05',CT,0) C5,
DECODE(B,'06',CT,0) C6,
DECODE(B,'07',CT,0) C7,
DECODE(B,'08',CT,0) C8,
DECODE(B,'09',CT,0) C9,
DECODE(B,'10',CT,0) C10,
DECODE(B,'11',CT,0) C11,
DECODE(B,'12',CT,0) C12,
DECODE(B,'13',CT,0) C13,
DECODE(B,'14',CT,0) C14,
DECODE(B,'15',CT,0) C15,
DECODE(B,'16',CT,0) C16,
DECODE(B,'17',CT,0) C17,
DECODE(B,'18',CT,0) C18,
DECODE(B,'19',CT,0) C19,
DECODE(B,'20',CT,0) C20,
DECODE(B,'21',CT,0) C21,
DECODE(B,'22',CT,0) C22,
DECODE(B,'23',CT,0) C23
from (select
trunc(creation_date) A,
to_char(creation_date,'HH24') B,
count(*) CT
from order_header
where client_id='&&2'
and creation_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
group by
trunc(creation_date),
to_char(creation_date,'HH24')
order by A,B)))
/


spool off
