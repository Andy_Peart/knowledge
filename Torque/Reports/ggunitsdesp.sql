SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Date Shipped,Sku,Qty Shipped' from dual
union all
select "Date" || ',' || "Sku" || ',' || "Qty" from
(
select "Date"
, "Sku"
, max("Qty") as "Qty"
from
(
--SHIPMENTS WHERE SKU LINE IS THE KIT LINE
with t1 as (
select sku_id       as "Sku"
, sum(update_qty)   as "Qty"
, trunc(dstamp)     as "Date"
from inventory_transaction
where client_id = 'GG'
and dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and code = 'Shipment'
and sku_id in (select sku_id from kit_line where client_id = 'GG')
group by sku_id
, trunc(dstamp)
)
--SHIPMENTS THAT BREAK THE SKU INTO IT'S KIT LINES
, t2 as (
select kl.sku_id                        as "Sku"
, sum(nvl(it.update_qty,0)*kl.quantity) as "Qty"
, trunc(it.dstamp)                      as "Date"
from kit_line kl
inner join inventory_transaction it on it.client_id = kl.client_id and it.sku_id = kl.kit_id
where kl.client_id = 'GG'
and it.code = 'Shipment'
and dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by kl.sku_id
, trunc(dstamp)
)
--SHIPMENTS WHERE THE SKU ISN'T A KIT LINE WHATSOEVER
, t3 as (
select sku_id       as "Sku"
, sum(update_qty)   as "Qty"
, trunc(dstamp)     as "Date"
from inventory_transaction
where client_id = 'GG'
and dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and code = 'Shipment'
and sku_id not in (select sku_id from kit_line where client_id = 'GG')
group by sku_id
, trunc(dstamp)
)
select nvl(nvl(t1."Date",t2."Date"),t3."Date")      as "Date"
, s.sku_id                                          as "Sku"
, nvl(t1."Qty",0)+nvl(t2."Qty",0)+nvl(t3."Qty",0)   as "Qty"
from sku s
left join t1 on t1."Sku" = s.sku_id
left join t2 on t2."Sku" = s.sku_id
left join t3 on t3."Sku" = s.sku_id
where s.client_id = 'GG'
)
group by "Date"
, "Sku"
having max("Qty") <> 0
order by 1, 2
)
/
--spool off