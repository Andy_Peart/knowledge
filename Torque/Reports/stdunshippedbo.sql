SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240
SET TRIMSPOOL ON

--spool stdunshippedbo.csv


select 'Client Id,Order Type,Order Id,Creation Date,Status,Sku Id,Line Id,Ordered,Fulfilled,UnFulfilled,Free Stock,Locked Stock,In Suspense,Current Intake,Delivery Due' from DUAL;

select
distinct
CL||','||
DDD||','||
AAA||','||
BBB||','||
FFF||','||
SSS||','||
LLL||','||
OOO||','||
TTT||','||
OT||','||
nvl(QQ-nvl(QQ3,0)-nvl(QQ4,0),0)||','||
nvl(QQ2,0)||','||
nvl(QQ4,0)||','||
nvl(QQ3,0)||','||
trunc(PD)
from (select
oh.client_id CL,
oh.order_type  DDD,
oh.status FFF,
oh.order_id  AAA,
ol.sku_id SSS,
ol.line_id LLL,
trunc(oh.creation_date) BBB,
count(*) ct,
sum(nvl(ol.qty_ordered,0)) OOO,
sum(nvl(ol.qty_picked,0)+nvl(ol.qty_tasked,0)) TTT,
sum(nvl(ol.qty_ordered,0))-(sum(nvl(ol.qty_picked,0)+nvl(ol.qty_tasked,0))) OT
from order_header oh, order_line ol
where oh.client_id='&&2'
--and oh.shipped_date>sysdate-1
and oh.status not in ('Shipped','Cancelled')
and oh.order_id=ol.order_id
and ol.client_id='&&2'
and ol.back_ordered='Y'
group by
oh.client_id,
oh.order_type,
oh.status,
oh.order_id,
ol.sku_id,
ol.line_id,
trunc(oh.creation_date)),(select
sku_id QS,
sum(qty_on_hand-qty_allocated) QQ
from inventory
where client_id='&&2'
and lock_status<>'Locked'
group by sku_id),(select
sku_id QS2,
sum(qty_on_hand-qty_allocated) QQ2
from inventory
where client_id='&&2'
and lock_status='Locked'
group by sku_id),(select
jc.sku_id QS3,
sum(jc.qty_on_hand) QQ3
from inventory jc,location lc
where jc.client_id like '&&2'
and jc.location_id=lc.location_id
and jc.site_id=lc.site_id
and lc.loc_type like 'Receive%'
group by
jc.sku_id),(select
jc.sku_id QS4,
sum(jc.qty_on_hand) QQ4
from inventory jc,location lc
where jc.client_id like '&&2'
and jc.location_id=lc.location_id
and jc.site_id=lc.site_id
and lc.loc_type like 'Suspens%'
group by
jc.sku_id),(select
pl.sku_id PSK,
min(ph.due_dstamp) PD
from pre_advice_header ph,pre_advice_line pl
where ph.client_id='&&2'
and ph.status<>'Complete'
and pl.client_id='&&2'
and ph.pre_advice_id=pl.pre_advice_id
and pl.qty_due>pl.qty_received
and ph.due_dstamp>sysdate-5
group by
pl.sku_id)
where SSS=QS (+)
and SSS=QS2 (+)
and SSS=QS3 (+)
and SSS=QS4 (+)
and SSS=PSK (+)
and OOO>0
and OOO<=nvl(QQ,0)
order by
1
/


--spool off
