SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON

set pagesize 999
set linesize 160
set heading on

clear columns
clear breaks
clear computes


TTitle 'Client &&2 Breakdown of Mail Order Despatch Methods from &&3 to &&4' skip 2

column AA heading 'Method' format A12
column BB heading 'Container' format A16
column ZZ heading 'Order ID' format A12


break on AA dup skip page

--compute sum label 'Totals' of A1 B1 on report

select
distinct 
'OCS         ' AA,
reference_id ZZ,
it.container_id BB
from inventory_transaction it
where it.client_id='&&2'
and it.code='Pick'
and to_loc_id='CONTAINER'
and it.Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
--and (it.container_id like '55%' or it.container_id like '50%')
and it.container_id like '55%'
and reference_id not like 'NP%'
union
select
distinct 
'RM Next Day ' AA,
reference_id ZZ,
it.container_id BB
from inventory_transaction it
where it.client_id='&&2'
and it.code='Pick'
and to_loc_id='CONTAINER'
and it.Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
--and (it.container_id like 'Z%' or it.container_id like 'J%')
and it.container_id like 'ZJ%'
and reference_id not like 'NP%'
union
select
distinct 
'RM Normal   ' AA,
reference_id ZZ,
it.container_id BB
from inventory_transaction it
where it.client_id='&&2'
and it.code='Pick'
and to_loc_id='CONTAINER'
and it.Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
--and (it.container_id like 'B%' or it.container_id like 'R%')
and it.container_id like 'BR%'
and reference_id not like 'NP%'
union
select
distinct 
'AIRMAIL     ' AA,
reference_id ZZ,
it.container_id BB
from inventory_transaction it
where it.client_id='&&2'
and it.code='Pick'
and to_loc_id='CONTAINER'
and it.Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
--and it.container_id not like '55%'
--and it.container_id not like '50%'
--and it.container_id not like 'Z%'
--and it.container_id not like 'J%'
--and it.container_id not like 'B%'
--and it.container_id not like 'R%'
and it.container_id like 'BFPO%'
and reference_id not like 'NP%'
union
select
distinct 
'Packet Post ' AA,
reference_id ZZ,
it.container_id BB
from inventory_transaction it
where it.client_id='&&2'
and it.code='Pick'
and to_loc_id='CONTAINER'
and it.Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and it.container_id not like 'ZJ%'
and it.container_id not like 'BR%'
and it.container_id not like '55%'
and it.container_id not like 'BFPO%'
and reference_id not like 'NP%'
order by AA,ZZ
/
