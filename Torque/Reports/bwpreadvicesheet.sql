SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 60
SET NEWPAGE 0
SET LINESIZE 200
SET TRIMSPOOL ON
SET HEADING ON
SET UND ''''




ttitle left '                        BERWIN Pre Advice Receipt Sheet' skip 2
-- from DUAL;
--select '                        -------------------------------' from DUAL;
--select ' ' from DUAL;


column R heading 'Pre Advice' format a16
column A heading 'SKU' format a16
column A1 heading 'Description' format a28
column D heading 'Qty Due' format 99999
column E heading '    Qty Received' format A16
column M format A8 Noprint



--      12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
--select 'Pre Advice       SKU              Description                   Qty Due     Qty Received' from DUAL;
--select '----------       ---              -----------                   -------     ------------' from DUAL;
--select ' ' from DUAL;

break on row skip 1 on report

compute sum label 'Total' of D on report


select
ph.pre_advice_id R,
pl.sku_id A,
sku.description A1,
pl.qty_due D,
'    ____________' E
from pre_advice_header ph,pre_advice_line pl,sku
where ph.client_id='BW'
and ph.pre_advice_id like '&&2'
and ph.client_id=pl.client_id
and ph.pre_advice_id=pl.pre_advice_id
and pl.sku_id=sku.sku_id
and sku.client_id='BW'
order by R,A
/
