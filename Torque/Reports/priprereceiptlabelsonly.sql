/*********************************************************************************/
/*                                                                               */
/*                            Elite                                              */
/*                                                                               */
/*     FILE NAME  :   pridespatch.sql                                            */
/*                                                                               */
/*     DESCRIPTION:    Datastream for Pringle Labels from Pre-advice             */
/*                                                                               */
/*   DATE     BY   PROJ       ID         DESCRIPTION                             */
/*   ======== ==== ======     ========   =============                           */
/*   13/10/08 LH   Pringle               Pre-Receipts labels by Pre-Receipt_id   */
/*********************************************************************************/

/* Input Parameters are as follows */
-- 2)  Pre-Advice ID
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 0
SET VERIFY OFF
SET FEEDBACK OFF
SET TAB OFF

 
-- Set the line length to the maximum possible, but trim 
-- trailing spaces for both screen output and spool files
SET LINESIZE 32767
SET WRAP ON
SET TRIMOUT ON
SET TRIMSPOOL ON




/* Define the format of the output dates */
DEFINE	OutputDateFormat = 'DD-MON-YYYY HH24:MI:SS'
COLUMN	sorter	NOPRINT

SELECT Distinct '0' sorter,
       'DSTREAM_PRI_DESP_HDR'          ||'|'||
       rtrim('&&2')			||'|'||			/* Order_id */
       rtrim('&&3')							/* Container_id */
from dual

SELECT distinct '3'||sm.picked_dstamp sorter,
'DSTREAM_PRI_SWING_LABEL_HDR'		||'|'||
rtrim (ol.sku_id)			||'|'||
rtrim (sm.qty_picked)
from order_line ol, shipping_manifest sm
where ol.order_id = '&&2'
and sm.container_id = '&&3'
and ol.qty_picked > '0'
and ol.order_id = sm.order_id
and ol.sku_id = sm.sku_id
union all
SELECT distinct '3'||sm.picked_dstamp sorter,
'DSTREAM_PRI_SWING_LABEL_LINE'		||'|'||
rtrim (ol.sku_id)			||'|'||
rtrim (s.description)			||'|'||
rtrim (s.each_value)			||'|'||
rtrim (s.colour)			||'|'||
rtrim (s.ean)
from order_line ol, sku s, shipping_manifest sm
where ol.order_id = '&&2'
and sm.container_id = '&&3'
and ol.qty_picked > '0'
and ol.sku_id = s.sku_id
and ol.order_id = sm.order_id
and ol.sku_id = sm.sku_id
order by 1,2
/