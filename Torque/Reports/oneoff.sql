SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 999
SET TRIMSPOOL ON


--spool one_off.csv

select 'Item Code,Receipt Date,Stock Qty,Location' from Dual;

select
Description,
trunc(Receipt_Dstamp),
qty_on_hand,
location_id
from inventory
where client_id='BW'
Order by
trunc(Receipt_Dstamp)
/

--spool off
