SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

 
select 'Location,Docket,SKU,Product,Size,Style,Date Received,Qty' from dual;
select loc ||','|| docket ||','|| sku_no ||','||  product ||','|| sku_size ||','|| style_no ||','|| received ||','|| qty
from (
select inv.location_id loc, inv.user_def_type_4 docket, inv.sku_id sku_no, sku.product_group product, sku.sku_size, 
sku.user_def_type_5 style_no, to_char(inv.receipt_dstamp,'DD-MM-YYYY') received, sum(inv.qty_on_hand) qty
from inventory inv inner join sku on inv.sku_id = sku.sku_id and inv.client_id = sku.client_id
where inv.client_id in ('PR','PS')
and inv.location_id not like 'PRHG%'
and inv.location_id not in ('T','PROMHOLD','REJECT','I1')
and 
(
inv.location_id like '1___'
or
inv.location_id like '4___'
or
inv.location_id like 'TW%'
or
inv.location_id between 'P01' and 'P99'
or
inv.location_id like 'TOP%'
)
group by inv.location_id, inv.user_def_type_4, inv.sku_id, sku.product_group, sku.sku_size, to_char(inv.receipt_dstamp,'DD-MM-YYYY'), sku.user_def_type_5
order by 1,2,3
)
/