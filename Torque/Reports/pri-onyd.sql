SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 66
SET NEWPAGE 0
SET LINESIZE 240

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


TTitle 'VEW Outstanding Pick Orders NOT despatched     at ' report_date skip 2

column A heading 'Order|Type' format A5
column B heading 'Prty' format 9999
column BB heading 'Rev|Prty' format A4
column C heading 'Customer|Code' format A8
column D heading 'Customer Name' format A32
column E heading 'Sales Order' format A15
column F heading 'Status' format A11
column G heading 'Date' format A10
column H heading 'Orders' format 9999
column J heading 'Qty' format 99999
column K heading ' ' format A32
column M heading ' ' format A62

break on G skip 1 on report

compute sum label 'Date Total' of H J on G 
compute sum label 'Run Total' of H J on report


SELECT
to_char(oh.CREATION_DATE,'DD/MM/YYYY') G,
ol.USER_DEF_NOTE_1 A,
oh.PRIORITY B,
DECODE(oh.PRIORITY,'1','  5','2','  4','3','  3','4','  2','5','  1','  0') BB,
oh.INV_NAME C, 
oh.NAME D,
oh.ORDER_ID E,
oh.STATUS F,
count(distinct oh.order_id) H,
sum(ol.QTY_ORDERED) J,
oh.USER_DEF_NOTE_2 K
FROM
Order_Header oh,Order_Line ol
where oh.client_id='PRI'
and oh.status not in ('Cancelled','Shipped')
and oh.order_id not like 'PR%'
and oh.client_id=ol.client_id
and oh.order_id=ol.order_id
--and ol.sku_id-sku.sku_id
--and sku.client_id=ol.client_id
group by
to_char(oh.CREATION_DATE,'DD/MM/YYYY'),
ol.USER_DEF_NOTE_1,
--oh.ORDER_TYPE,
oh.PRIORITY,
oh.INV_NAME, 
oh.NAME,
oh.ORDER_ID,
oh.STATUS,
oh.USER_DEF_NOTE_2
order by
to_char(oh.CREATION_DATE,'DD/MM/YYYY'),
oh.NAME
/

SET PAGESIZE 0

select '' from DUAL;
select '' from DUAL;
select '                  ********************************************************************' from DUAL;
select '                  * Order Types Key                                                  *' from DUAL;
select '                  * P11: Wholesale account seasons order                             *' from DUAL;
select '                  * P12: Wholesale account by return order                           *' from DUAL;
select '                  * P13: Cash Sale: Account 23100 is UK; Account 23101 is Export     *' from DUAL;
select '                  * P21: Wholesale account seasons embroidery order                  *' from DUAL;
select '                  * P22: Wholesale account embroidery by return                      *' from DUAL;
select '                  * P42: Sale line order                                             *' from DUAL;
select '                  * P71: Pringle promotion and donation order                        *' from DUAL;
select '                  * DOS: Retail distribution order                                   *' from DUAL;
select '                  *                                                                  *' from DUAL;
select '                  * P12/P13/P22 and P71                                              *' from DUAL;
select '                  * Dispatch same day if order received at Elite by 1.00 pm          *' from DUAL;
select '                  *                                                                  *' from DUAL;
select '                  ********************************************************************' from DUAL;
select '' from DUAL;
select '' from DUAL;
select 'Status                                                                   Units Ordered' from DUAL;
select '--------------------------------------------------------------------------------------' from DUAL;



SELECT
oh.status F,
DECODE(oh.STATUS,'Complete','- order has been picked and is about to be packed and shipped',
                 'Released','- order has been received and awaits allocation to run',
                 'Allocated','- stock has been allocated but picking has not yet started',
                 'At Hawick','- order has been sent to Hawick for embroidery',
                 'In Progress','- order is currently being picked',oh.status) M,
sum(ol.QTY_ORDERED) J
FROM
Order_Header oh,Order_Line ol
where oh.client_id='PRI'
and oh.order_id not like 'PR%'
and oh.status not in ('Cancelled','Shipped')
and oh.client_id=ol.client_id
and oh.order_id=ol.order_id
group by
oh.STATUS
order by
oh.STATUS
/












