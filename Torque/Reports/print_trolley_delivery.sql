SET HEADING OFF;

SET FEEDBACK OFF;

SET LINESIZE 300;

select distinct
client_id,
upper('&2'),
task_id,
'&5',
from_loc_id
from move_task
WHERE 
from_loc_id like upper('&3')||'%'
AND client_id = UPPER('&1')
AND '&4' = 'X'
UNION ALL
select distinct
client_id,
upper('&2'),
order_id,
'&5',
hub_county
from
order_header
where client_id = UPPER('&1')
AND consignment = UPPER('&4')
AND hub_county like '&3-__'
order by from_loc_id;