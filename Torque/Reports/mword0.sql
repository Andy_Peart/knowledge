set pagesize 2000
set linesize 2000
set verify off

clear columns
clear breaks
clear computes

ttitle "Mountain Warehouse - Order - Picked Less than Ordered" skip 3

column A heading 'Order Number' format a10
column B heading 'SKU' format 999999999999999
column C heading 'Del' format a5
column D heading 'Qty Ord' format 99999
column E heading 'Qty Pick' format 99999
column F heading 'Loc' format a5
column G heading 'User' format a10
column H heading 'When Picked' format a15

break on report

compute sum label 'Total' of D on report
compute sum of E on report

select ol.order_id A,
ol.sku_id B,
oh.customer_id C,
ol.QTY_ORDERED D,
ol.QTY_PICKED E,
inv.from_loc_id F,
inv.user_id G,
to_char (inv.dstamp,'DD/MM/YY hh:mi') H
from order_line ol, order_header oh, inventory_transaction inv
where ol.client_id = 'MOUNTAIN'
and ol.order_id = ('&&2')
and oh.order_id = ol.order_id
and ol.qty_picked < ol.qty_ordered
and ol.order_id = inv.reference_id
and inv.code = 'Pick'
and ol.sku_id = inv.sku_id
order by ol.sku_id
/
