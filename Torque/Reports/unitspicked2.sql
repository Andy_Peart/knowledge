set feedback off
set pagesize 68
set linesize 240
set verify off

clear columns
clear breaks
clear computes

column A heading 'User ID' format A16
column B heading 'Qty' format 999999
column S heading 'Trade Type' format A12

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'Units Picked from '&&2' to '&&3' - for client ID PRI as at ' report_date skip 2

break on A skip 1 on report
compute sum label 'User Total' of B on A
compute sum label 'Total' of B on report


select
user_id A,
DECODE(substr(reference_id,1,2),'31','WHOLESALE'
                              ,'51','RETAIL','UNKNOWN') S,
sum(update_qty) B
from inventory_transaction
where client_id='PRI'
and code='Pick'
and from_loc_id='CONTAINER'
and to_loc_id='DESPATCHPR'
and Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by 
user_id,
substr(reference_id,1,2)
order by 
user_id,
substr(reference_id,1,2)
/

