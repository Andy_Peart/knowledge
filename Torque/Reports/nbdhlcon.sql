SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep '|'


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 3000
SET TRIMSPOOL ON

SELECT 
	'DEFAULT' 
	, odh.order_id 
	, nvl(initcap(odh.name),'.. ')	
	, initcap(odh.contact) 
	, initcap(odh.address1) 
	, initcap(odh.address2) 
	, NULL 
	, upper(odh.town) 
	, upper(odh.county) 
	, upper(odh.postcode) 
	, upper(odh.country) 
	, cou.iso2_id
	, decode(odh.contact_phone,null,odh.inv_contact_phone||'.',odh.contact_phone) 
	, case when cou.ce_eu_type = 'EU' then 'U'
				when odh.country = 'USA' then 'P'
		else '' end
	, 1
	, '0.5' 
	, '1' 
	, 'Clothing' 
	, 1 --sum(ODL.LINE_VALUE) 
	, 0 
	, 0 
	, NULL 
	, TO_CHAR(SYSDATE,	'DD/MM/YYYY') 
	, 1 
    , 'customercare@nubianskin.com'
	, DECODE(odh.contact_email,NULL,	odh.inv_contact_email,odh.contact_email) 
FROM ORDER_HEADER ODH INNER JOIN ORDER_LINE ODL ON ODH.ORDER_ID=ODL.ORDER_ID AND ODH.CLIENT_ID=ODL.CLIENT_ID
left join country cou on odh.country=cou.iso3_id
where odh.client_id = 'NB'
AND ODH.CONSIGNMENT = '&2'
--and nvl(ODL.QTY_TASKED,0) <> 0
group by 
	'DEFAULT' 
	, odh.order_id 
	, nvl(initcap(odh.name),'.. ')	
	, initcap(odh.contact) 
	, initcap(odh.address1) 
	, initcap(odh.address2) 
	, NULL 
	, upper(odh.town) 
	, upper(odh.county) 
	, upper(odh.postcode) 
	, upper(odh.country) 
	, cou.iso2_id
	, decode(odh.contact_phone,null,odh.inv_contact_phone||'.',odh.contact_phone) 
	, case when cou.ce_eu_type = 'EU' then 'U'
				when odh.country = 'USA' then 'P'
		else '' end
	, 1
	, '0.5' 
	, '1' 
	, 'Clothing' 
	, 1 
	, 0 
	, 0 
	, NULL 
	, TO_CHAR(SYSDATE,	'DD/MM/YYYY') 
	, 1 
    , 'customercare@nubianskin.com'
	, DECODE(odh.contact_email,NULL,	odh.inv_contact_email,odh.contact_email) 
order by odh.order_id
/