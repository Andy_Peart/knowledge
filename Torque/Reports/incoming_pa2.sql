SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','
SET UND OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 320

/* Column Headings and Formats */
COLUMN A heading "Client ID" FORMAT a10
COLUMN B heading "Supplier" FORMAT a10
COLUMN C heading "Pre Advice ID" FORMAT a16
COLUMN D heading "Line ID" FORMAT 999999
COLUMN E heading "Creation Date" FORMAT a14
COLUMN F heading "SKU" FORMAT a18
COLUMN G heading "Season" FORMAT a10
COLUMN H heading "Style" FORMAT a10
COLUMN J heading "Qual" FORMAT a14
COLUMN K heading "Size" FORMAT a8
COLUMN L heading "Qty Due" FORMAT 999999
COLUMN N heading "Colour" FORMAT a14

--Select 'Client ID,Supplier ID,Pre Advice ID,Line ID,Creation Date,SKU ID,Season,Style,Qual,Size,Qty Due' from DUAL;
Select 'Client ID,Pre Advice ID,SKU ID,Season,Style,Qual,Size,Qty Due,Colour' from DUAL;


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


break on report

compute sum label 'Totals' of L on report




SELECT
PAH.client_id A,
--PAH.supplier_id B,
PAH.Pre_Advice_ID C,
--PAL.Line_ID D,
--to_char(pah.creation_date, 'DD/MM/YY') E,
''''||substr(PAL.SKU_ID,1,12)||'''' F,
U2 G,
U1 H,
U4 J,
ZZZ K,
nvl(PAL.Qty_Due,0) L,
CCC N
FROM Pre_Advice_Header PAH, Pre_Advice_Line PAL, (select 
sku_id SSS,
user_def_type_2 U2,
user_def_type_1 U1,
user_def_type_4 U4,
sku_size ZZZ,
colour CCC
from sku where client_id='&&2')
WHERE PAH.Pre_Advice_ID = PAL.Pre_Advice_ID
AND PAL.SKU_ID = SSS (+)
AND PAH.Pre_Advice_ID = '&&3'
and PAH.client_id = '&&2'
ORDER BY PAL.line_id
/
