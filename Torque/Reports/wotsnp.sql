set feedback off
set pagesize 66
set linesize 100
set verify off
set newpage 0


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

 

column A heading 'Order ID' format A10
column B heading 'SKU code' format A18
column C heading 'Description' format A40
column D heading 'Cust Code' format A10
column E heading 'Qty Ordered' format 999999
column M format a4 noprint


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

set und '-'
set heading on
set newpage 0

ttitle  LEFT 'Wots NOT picked for client ID &&2 as at ' report_date RIGHT 'Page:' FORMAT 999 SQL.PNO skip 2

break on A skip 1
compute sum label 'Total' of E on A


select
oh.order_id A,
ol.sku_id B,
sku.description C,
sku.User_def_type_5 D,
ol.qty_ordered E
from order_header oh,order_line ol,sku
where oh.Client_ID='&&2'
and oh.order_id=ol.order_id
and ol.qty_picked is null
and ol.sku_id=sku.sku_id
order by A,B
/
