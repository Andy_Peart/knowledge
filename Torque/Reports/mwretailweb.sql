SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON

--break on report
--compute sum of Q on report

--spool mwretailweb.csv

select 'Products with <= &&2 in WEB zone' from Dual;
select 'Sku Id,Description,Work Zone,Location,Qty' from Dual;



select
''''||iv.sku_id||''','||
iv.description||','||
L.work_zone||','||
iv.location_id||'('||to_char(iv.receipt_dstamp,'DD/MM/YY HH24:MI')||')' ||','||
sum(qty_on_hand) QTY
from inventory iv,location L
where iv.client_id='MOUNTAIN'
and iv.zone_1='RETAIL'
and upper(iv.sku_id)=lower(iv.sku_id)
and iv.sku_id not in (select
WEBSKU
from (select
sku_id WEBSKU,
sum(qty_on_hand) WEBQTY
from inventory
where client_id='MOUNTAIN'
and zone_1='WEB'
group by
sku_id)
where WEBQTY>&&2)
and iv.location_id=L.location_id
and iv.site_id=L.site_id
group by
iv.sku_id,iv.description,L.work_zone,iv.location_id,iv.receipt_dstamp
order by
1
/


--spool off
