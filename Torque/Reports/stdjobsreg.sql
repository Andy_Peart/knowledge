SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 900
SET TRIMSPOOL ON
SET HEADING ON

with 
job_starts as 
(
	select it.code, it.user_id, it.job_id, it.job_unit, it.dstamp, notes,
	rank() over(partition by it.user_id order by it.Dstamp) as rank1
	from inventory_transaction it
	where it.client_id = '&&2'
	and it.code = 'Job Start'
	and it.Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
),
job_ends as 
(
	select it.code, it.user_id, it.job_id, it.job_unit, it.dstamp, notes,
	rank() over(partition by it.user_id order by it.Dstamp) as rank1, it.update_qty tasks, it.update_qty units
	from inventory_transaction it
	where it.client_id = '&&2'
	and it.code = 'Job End'
	and it.Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
)
select 'Date,Name,Minutes,Job ID,Notes,Txns,Units,Type,Orders' from dual
union all
select txn_date || ',' || user_id || ',' ||  minutes || ',' || job_id || ',' || notes || ',' || tasks || ',' || units || ',' || job_type || ',' || case when orders = '0' then '' else orders end
from (
    select txn_date, user_id, sum(minutes) minutes, job_id, notes, sum(tasks) tasks, sum(units) units, job_type, to_char(sum(orders)) orders
    from (
        select trunc(s.dstamp) txn_date, s.user_id, to_char(s.dstamp,'HH24:MI') time_start, to_char(e.dstamp,'HH24:MI') time_end, 
        ((to_number(substr(to_char(e.dstamp,'HH24:MI'),1,2))*60)+to_number(substr(to_char(e.dstamp,'HH24:MI'),4,2)))-
        ((to_number(substr(to_char(s.dstamp,'HH24:MI'),1,2))*60)+to_number(substr(to_char(s.dstamp,'HH24:MI'),4,2))) minutes,
        s.job_id, s.notes, e.tasks, e.units, 'NON AUTO' as job_type, 0 as orders
        from job_starts s inner join job_ends e on s.rank1 = e.rank1 and s.user_id = e.user_id and s.job_id = e.job_id
        where s.job_unit <> 'AUTO' and e.job_unit <> 'AUTO'
    union all
        select trunc(s.dstamp) txn_date, 
        s.user_id, to_char(s.dstamp,'HH24:MI') time_start, to_char(e.dstamp,'HH24:MI') time_end, 
        ((to_number(substr(to_char(e.dstamp,'HH24:MI'),1,2))*60)+to_number(substr(to_char(e.dstamp,'HH24:MI'),4,2)))-
        ((to_number(substr(to_char(s.dstamp,'HH24:MI'),1,2))*60)+to_number(substr(to_char(s.dstamp,'HH24:MI'),4,2))) minutes,
        s.job_id, s.notes, count(*) - decode(s.job_id, 'Job End', 0, 1) tasks, sum(it.update_qty) - decode(s.job_id, 'Job End', 0, 1) units, 'AUTO' as job_type, count(distinct it.reference_id) as orders
        from job_starts s, job_ends e, inventory_transaction it
        where it.Dstamp between s.dstamp and e.dstamp+0.00007
        and it.client_id='&&2' and it.update_qty<>0 and NVL(it.from_loc_id,'X')<>'CONTAINER'
        and it.user_id=s.user_id
        and s.rank1=e.rank1
        and s.user_id=e.user_id
        and s.job_id=e.job_id
        /* and it.elapsed_time > 0 */
        /* and it.code not in ('Job End') */
        and s.job_unit = 'AUTO' and e.job_unit = 'AUTO'
        and (it.from_loc_id is null or it.from_loc_id not in (select l.location_id from LOCATION l where l.site_id in (select distinct site_id from INVENTORY where client_id = '&&2') and l.loc_type in ('Stage','Shipdock')))
        group by s.dstamp,e.dstamp, s.user_id, s.job_id, s.notes	
    )
    group by txn_date, user_id, job_id, notes, job_type
    order by 1,2,4,5
)
/