SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','
SET UND OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 320
SET TRIMSPOOL ON




column A1 format A18
column A format A18
column B format A40
column Z format A12
column C format 9999999
column D format 9999999
column E format 9999999
column F format 9999999
column G format 9999999
column H format 9999999
column J format 9999999
column K format 9999999
column L format 9999999
column N format 9999999


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



break on report

compute sum label 'Total'  of C D E F G H J N K L on report

--set termout off
--spool W.csv

select ',,W Comparison_Report - '||to_char(SYSDATE, 'DD/MM/YY  HH24:MI:SS') from DUAL;

select 
distinct 
'Report Date: '||to_char(SYSDATE, 'DD/MM/YY  HH:MI')||',Last Update: '||to_char(the_date,'DD/MM/YY HH:MI')
from dx_skufile
/

select 'Category,Product,Description,Dax_qty,Elite_qty,Difference,          ,Elite_Pick,Elite_Alloc,Elite_Hold,Elite_Susp,Elite_Free,Elite_Priam,Elite_True_Diff' from DUAL;
--                                                QQQ                             Q4          Q5        Q2         Q3         
select * from (select
sku.product_group A1,
sku.sku_id A,
sku.description B,
to_number(nvl(the_qty,0)) C,
nvl(QQQ,0) D,
to_number(nvl(the_qty,0))-nvl(QQQ,0) E,
' ' F1,
nvl(Q4,0) F,
nvl(Q5,0) G,
nvl(Q2,0) H,
nvl(Q3,0) J,
-- Elite_Free (Elite_qty less the above 4).
nvl(QQQ,0)-nvl(Q2,0)-nvl(Q3,0)-nvl(Q4,0)-nvl(Q5,0) N,
-- Elite_Priam (Elite_qty less Elite_alloc and Elite_susp).
--nvl(QQQ,0)-nvl(Q2,0)-nvl(Q3,0)-nvl(Q4,0) K,
nvl(QQQ,0)-nvl(Q3,0)-nvl(Q5,0) K,
-- Elite_True_Diff
to_number(nvl(the_qty,0))-nvl(QQQ,0)-nvl(Q3,0)-nvl(Q5,0) L
from sku,(Select
sku_id SSS,
sum(qty_on_hand) QQQ
from inventory
where client_id='DX'
and zone_1 not in ('IDEAL','QVC')
and location_id not in ('DXBRET','DXBREJ','DXBREJ1')
and (condition_id<>'HOLD' or condition_id is null)
group by sku_id),(Select
sku_id S2,
sum(qty_on_hand) Q2
from inventory
where client_id='DX'
and zone_1 not in ('IDEAL','QVC')
and location_id not in ('DXBRET','DXBREJ','DXBREJ1')
and condition_id='HOLD'
group by sku_id),(Select
sku_id S3,
sum(qty_on_hand) Q3
from inventory
where client_id='DX'
and location_id='SUSPENSE'
group by sku_id),(Select
ol.sku_id S4,
sum(nvl(ol.qty_picked,0)-nvl(ol.qty_shipped,0)) Q4
from order_line ol,order_header oh
where ol.client_id='DX' 
and ol.order_id=oh.order_id
and oh.client_id='DX'
and oh.status not in ('Cancelled','Shipped')
and oh.order_type<>'TVFINAL'
group by ol.sku_id),(select 
mt.sku_id S5,
sum(mt.qty_to_move) Q5
from move_task mt,order_header oh
where mt.client_id='DX'
and mt.task_id=oh.order_id
and oh.client_id='DX'
and oh.order_type<>'TVFINAL'
group by mt.sku_id),(select 
the_SKU,
the_qty 
from dx_skufile 
where the_warehouse like 'W %')
where sku.client_id='DX'
and sku.sku_id not like '*%'
and sku.sku_id=the_sku (+)
and sku.sku_id=SSS (+)
and sku.sku_id=S2 (+)
and sku.sku_id=S3 (+)
and sku.sku_id=S4 (+)
and sku.sku_id=S5 (+))
where C<>0 or D<>0 or G<>0
order by
A
/

 
--spool off

spool off
--set termout on
