/********************************************************************************************/
/*                                                                                          */
/*                            Elite                                                         */
/*                                                                                          */
/*     FILE NAME  :   mwdespatchmm.sql                                                      */
/*                                                                                          */
/*     DESCRIPTION:    Datastream for Mountain Warehouse Despatch Note                      */
/*                                                                                          */
/*   DATE     BY   PROJ     ID         DESCRIPTION                                          */
/*   ======== ==== ======   ========   =============                                        */
/*   27/04/05 LH   Mountain            Despatch Note for Mountain Warehouse                 */
/*   10/01/06 LH   Mountain            Despatch Note ammendment to add zone                 */
/*   01/05/08 LH   Mountain            Despatch Note for Mail Order                         */
/*   06/05/08 LH   Mountain            Despatch Note Alter for Consignment and Allocation   */
/********************************************************************************************/

/* Input Parameters are as follows */
-- 2)  Order ID
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 0
SET VERIFY OFF
SET FEEDBACK OFF
SET TAB OFF

 
-- Set the line length to the maximum possible, but trim 
-- trailing spaces for both screen output and spool files
SET LINESIZE 32767
SET WRAP ON
SET TRIMOUT ON
SET TRIMSPOOL ON


/* Define the format of the output dates */
DEFINE  OutputDateFormat = 'DD-MON-YYYY HH24:MI:SS'
COLUMN  sorter  NOPRINT

SELECT Distinct '0' sorter,
       'DSTREAM_MW_DESP_MM1_HDR'          ||'|'||
       rtrim('&&1')                            /* order_id */
from dual
union all
SELECT '0' sorter,
'DSTREAM_MW_DESP_MM1_ADD_HDR'              ||'|'||
rtrim (initcap(name)) ||'|'||
rtrim (initcap(address1)) ||'|'||
rtrim (initcap(address2)) ||'|'||
rtrim (upper(town)) ||'|'||
rtrim (upper(postcode))
from order_header oh
where oh.client_id = 'MOUNTAIN'
and oh.order_id = '&&1'
--union all
--select distinct '0' sorter,
--'DSTREAM_MW_DESP_MM1_ORD_HDR'      ||'|'||
--rtrim (oh.order_id) ||'|'||
--rtrim (to_char(oh.order_date, 'DD-MON-YYYY'))
--from order_header oh
--where oh.client_id = 'MOUNTAIN'
--and oh.order_id = '&&2'
--union all
--SELECT  '0' sorter,
--'DSTREAM_MW_DESP_MM1_LINE'              ||'|'||
--rtrim (ol.sku_id) ||'|'||
--rtrim (upper(s.description)) ||'|'||
--rtrim (upper(s.sku_size)) ||'|'||
--rtrim (ol.qty_tasked)
--from order_line ol, order_header oh, sku s
--where oh.client_id = 'MOUNTAIN'
--and oh.order_id = ol.order_id
--and ol.sku_id = s.sku_id
--and oh.order_id = '&&2'
--union all
--select '0' sorter,
--'DSTREAM_MW_DESP_MM1_LINE_TOTAL'              ||'|'||
--rtrim (sum(qty_tasked))
--from order_line
--where client_id = 'MOUNTAIN'
--and order_id = '&&2'
--/