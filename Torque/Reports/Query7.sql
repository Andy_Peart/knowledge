SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240


select 'SKU,Description,Range,Size,Season,Colour,Clients Code,Qty'
from dual;

select
'''' ||
AA ||''','||
BB ||','||
CC ||','||
DD ||','||
EE ||','||
FF ||','||
GG ||','||
HH B
from (select
sku.sku_id AA,
sku.description BB,
SKU.User_def_type_1 CC,
SKU.Sku_size DD,
SKU.User_def_type_2 EE,
sku.colour FF,
sku.user_def_type_5 GG,
nvl(QQQ,0) HH,
sku.user_def_chk_1 JJ
from sku,(select
sku_id as SSS,
sum(qty_on_hand) QQQ
from inventory
where client_id like 'V'
and location_id not in ('IN','IN2','IN3','QCHOLD','WP1','SUSPENSE')
group by
sku_id)
where sku.client_id like 'V'
and sku.sku_id=SSS (+))
where HH>0 or JJ='N'
order by AA
/
