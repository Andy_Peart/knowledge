SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','
SET UND OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 160
SET TRIMSPOOL ON

column A heading 'Creation Date' format a28
column A2 heading 'Location' format a28
column A3 heading 'Customer ID' format a10
column A31 heading 'Customer Name' format a28
column B heading 'Orders' format 999999
column D heading 'Qty Shipped' format 99999999
column M format a6 noprint

break on report

compute sum label 'Total' of B on report


--set termout off
--spool GV-Shopping.csv

select 'Number of orders per day' from DUAL;
select 'Creation Date,Orders,' from DUAL;

Select
to_char(oh.creation_date,'YYMMDD') M,
to_char(oh.creation_date,'DD/MM/YYYY Day') A,
count(*) B,
--sum(ol.qty_ordered),
--sum(ol.qty_Shipped) D,
' '
from order_header oh
where oh.client_id='GV'
and oh.creation_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and oh.order_type like 'HOME%'
group by
to_char(oh.creation_date,'YYMMDD'),
to_char(oh.creation_date,'DD/MM/YYYY Day')
order by
to_char(oh.creation_date,'YYMMDD'),
to_char(oh.creation_date,'DD/MM/YYYY Day')
/

select ' ' from DUAL;
select 'Geographical location of Orders' from DUAL;
select 'Location,Orders,' from DUAL;

Select
oh.town A2,
count(*) B,
--sum(ol.qty_ordered),
--sum(ol.qty_Shipped) D,
' '
from order_header oh
where oh.client_id='GV'
and oh.creation_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and oh.order_type like 'HOME%'
group by
oh.town
order by
oh.town
/

CLEAR BREAKS
CLEAR COMPUTES

select ' ' from DUAL;
select 'Customers who have repeat ordered' from DUAL;
select 'Customer Name,Orders,Customer ID,' from DUAL;

select * from (Select
oh.name A31,
count(*) B,
oh.customer_id A3,
--sum(ol.qty_ordered),
--sum(ol.qty_Shipped) D,
' '
from order_header oh
where oh.client_id='GV'
and oh.creation_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and oh.order_type like 'HOME%'
group by
oh.customer_id,
oh.name
order by
oh.customer_id,
oh.name)
where B>1
/


column BB format a10
column GG format a20
column HH format a40

select ' ' from DUAL;
select 'Orders on SHORTWEB consignment' from DUAL;
select 'Order ID,Date,Customer ID,Customer Name,SKU,Description,' from DUAL;

select
oh.order_id A,
to_char(oh.creation_date,'DD/MM/YY') BB,
oh.customer_id A3,
oh.name A31,
--ol.qty_ordered C,
--nvl(ol.qty_tasked,0) C,
--nvl(country.tandata_id,oh.country) F,
ol.sku_id GG,
S1.description HH,
' '
--S1.sku_size J
from order_header oh,order_line ol,sku S1
where oh.client_id='GV'
and oh.status<>'Cancelled'
and oh.consignment='SHORTWEB'
and ol.client_id='GV'
and oh.order_id=ol.order_id
and ol.qty_ordered<>nvl(ol.qty_tasked,0)
--and oh.country=country.iso3_id (+)
and S1.client_id='GV'
and ol.sku_id=S1.sku_id
order by A
/

--spool off
--SET TERMOUT ON

