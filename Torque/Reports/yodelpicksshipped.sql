SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 200
SET TRIMSPOOL ON


column WW format A16


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

break on ww dup skip 1

--compute sum of CT on A

--spool yodelpicksshipped.csv

select 'Yodel Shipments for '||to_char(SYSDATE-1, 'DD/MM/YY  ') from DUAL;

select 'Date,Work Group and PC,Order ID,Container,Skus,Qty Shipped' from DUAL;


select
trunc(sm.shipped_dstamp),
sm.work_group||'  '||nvl(PC,' ') WW,
sm.order_id,
nvl(sm.container_id,'Not Scanned') CC,
count(*),
sum(qty_shipped)
from shipping_manifest sm,(select 
distinct
address_id WG,
postcode PC
from address
where client_id='MOUNTAIN')
where sm.client_id='MOUNTAIN'
and sm.work_group<>'WWW'
and trunc(sm.shipped_dstamp)=trunc(sysdate)-1
and sm.work_group=WG (+)
group by
trunc(sm.shipped_dstamp),
sm.work_group||'  '||nvl(PC,' '),
sm.order_id,
nvl(sm.container_id,'Not Scanned')
order by
WW,sm.order_id
/


--spool off
