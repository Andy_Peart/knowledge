/********************************************************************************************/
/*                                                                                          */
/*                            Elite                                                         */
/*                                                                                          */
/*     FILE NAME  :   tautoepdf.sql                                                         */
/*                                                                                          */
/*     DESCRIPTION:    Datastream for TML Mailorder Automatic Email to Customer pdf format  */
/*                                                                                          */
/*   DATE     BY   PROJ       ID         DESCRIPTION                                        */
/*   ======== ==== ======     ========   =============                                      */
/*   04/08/11 RW   TMR                   TML Mailorder Automatic Email with pdf to Customer */
/*                                                                                          */
/********************************************************************************************/

/* Input Parameters are as follows */
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 0
SET VERIFY OFF
SET FEEDBACK OFF
SET TAB OFF

 -- Set the line length to the maximum possible, but trim 
-- trailing spaces for both screen output and spool files
SET LINESIZE 32767
SET WRAP ON
SET TRIMOUT ON
SET TRIMSPOOL ON

/* Define the format of the output dates */
DEFINE	OutputDateFormat = 'DD-MON-YYYY'
COLUMN	sorter	NOPRINT

SELECT Distinct '0' sorter,
       'DSTREAM_T_AUTOEPDF_HDR'
from dual
union all
SELECT distinct '1' sorter,
'DSTREAM_T_AUTOEPDF_DETAIL_HDR'              ||'|'||
rtrim (oh.order_id)  ||'|'||
rtrim (oh.name)                    ||'|'||
rtrim (oh.contact)                        ||'|'||
rtrim (address1)                          ||'|'||
rtrim (address2)                          ||'|'||
rtrim (town)                              ||'|'||
rtrim (county)                            ||'|'||
rtrim (postcode)                          ||'|'||
rtrim (country)                           ||'|'||
rtrim (oh.contact_email)                  ||'|'||
rtrim (inv_contact)                       ||'|'||
rtrim (inv_name)                          ||'|'||
rtrim (inv_address1)                      ||'|'||
rtrim (inv_address2)                      ||'|'||
rtrim (inv_town)                          ||'|'||
rtrim (inv_county)                        ||'|'||
rtrim (inv_postcode)                      ||'|'||
decode(t.text,null,'',t.text		  ||'|')||          /* Country converted using workstation lookup) */
rtrim (inv_contact_email)                 ||'|'||                          
rtrim (decode(substr(itl.container_id,1,2), 'ZJ', 'Your tracking number is ' || itl.container_id ||'.',
                                     '55', '',
                                     'DW', '',
                                     'DD', '',
                                     'DL', '',
                                     'DH', '',
                                     ''))   ||'|'||
rtrim (trunc(itl.dstamp))                 ||'|'||
rtrim (decode(substr(itl.container_id,1,2), 'ZJ', 'You can track your order by going to http://www.royalmail.com/portal/rm/track.',
                                     '55', '',
                                     'DW', '',
                                     'DD', '',
                                     'DL', '',
                                     'DH', '',
                                     ''))   ||'|'||
rtrim (decode(substr(itl.container_id,1,2), 'ZJ', 'Royal Mail Special Delivery - Next Day',
                                     '55', 'Overseas Courier Service Worldwide',
                                     'DL', 'Royal Mail Standard Recorded Delivery',
                                     ''))  ||'|'||
rtrim (decode(substr(itl.container_id,1,2), 'ZJ', 'Just to remind you Royal Mail will be delivering your order and they will require a signature. If no one is at the address they will leave a card with details on how you can arrange to get your parcel.',
                                     '55', '',
                                     'DW', '',
                                     'DD', '',
                                     'DL', '',
                                     'DH', '',
                                     ''))	||'|'||
rtrim (decode(substr(itl.container_id,1,2), 'ZJ', 'We are pleased to inform you that your order has been despatched and it will be with you on the next working day.',
                                     '55', 'We are pleased to inform you that your order has been despatched and it will be with you within the next 7 - 10 working days.',
                                     'DW', 'We are pleased to inform you that your order has been despatched and it will be with you within the next 3 working days.',
                                     'DD', 'We are pleased to inform you that your order has been despatched and it will be with you within the next 3 working days.',
                                     'DL', 'We are pleased to inform you that your order has been despatched and it will be with you within the next 3 working days.',
                                     'DH', 'We are pleased to inform you that your order has been despatched and it will be with you within the next 3 working days.',
                                     ''))
from order_header oh, inventory_transaction itl, country c, language_text t
where (oh.client_id = 'T'
and oh.order_id = '&&2'
and itl.code = 'Shipment'
--and trunc(itl.dstamp) = trunc(sysdate)
and itl.reference_id = oh.order_id
and oh.contact_email is not null
and itl.container_id like ('ZJ%'))
and oh.country = c.iso3_id
and t.language = 'EN_GB'
and t.label = 'WLK'||c.iso3_id
or (oh.client_id = 'T'
and oh.order_id = '&&2'
and itl.code = 'Shipment'
--and trunc(itl.dstamp) = trunc(sysdate)
and itl.reference_id = oh.order_id
and oh.contact_email is not null
and itl.container_id like ('D%'))
and oh.country = c.iso3_id
and t.language = 'EN_GB'
and t.label = 'WLK'||c.iso3_id
or (oh.client_id = 'T'
and oh.order_id = '&&2'
and itl.code = 'Shipment'
--and trunc(itl.dstamp) = trunc(sysdate)
and itl.reference_id = oh.order_id
and oh.contact_email is not null
and itl.container_id like ('55%'))
and oh.country = c.iso3_id
and t.language = 'EN_GB'
and t.label = 'WLK'||c.iso3_id
order by 1;

exit;
