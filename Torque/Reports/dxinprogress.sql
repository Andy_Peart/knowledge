SET FEEDBACK OFF                 
set pagesize 68
set linesize 160
set newpage 0
set verify off

clear columns
clear breaks
clear computes

column D heading 'Date' format a16
column A heading 'ID' format a16
column B heading 'Ordered' format 99999999
column C heading 'Received' format 99999999
column M format a4 noprint

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'Pre Advice In Progress Report for DAXBOURNE as at ' report_date skip 2

--break on report

--compute sum label 'Totals' of B on report


select
trunc(ph.creation_date) D,
ph.pre_advice_id A,
sum(pl.qty_due) B,
sum(pl.qty_received) C
from pre_advice_header ph,pre_advice_line pl
where ph.client_id='DX'
and ph.status='In Progress'
and ph.pre_advice_id=pl.pre_advice_id
and pl.client_id='DX'
group by
trunc(ph.creation_date),
ph.pre_advice_id
order by 
trunc(ph.creation_date),
ph.pre_advice_id
/
