SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9999
SET TRIMSPOOL ON

set feedback on

update inventory_transaction
set uploaded='X'
where client_id = 'MOUNTAIN'
and trunc(Dstamp)=trunc(sysdate)
and work_group<>'WWW'
and code = 'Shipment'
--and uploaded = 'N'
and rownum<201
/

set feedback off
--SET TERMOUT OFF


COLUMN NEWSEQ NEW_VALUE SEQNO
COLUMN FOLD NEW_VALUE FOLDER


select 'S://redprairie/apps/home/dcsdba/reports/LIVE/MDW'||substr(to_char(100000+next_seq),2,5)||'.csv' NEWSEQ from sequence_file
--select 'LIVE/MDW'||substr(to_char(100000+next_seq),2,5)||'.csv' NEWSEQ from sequence_file
where control_id=23
/

--set termout off
spool &SEQNO


select 'ITL'||','
||'E'||','
||code||','
||decode(sign(i.original_qty), 1, '+', -1, '-', '+')||','
||nvl(i.original_qty,0)||','
||decode(sign(i.update_qty), 1, '+', -1, '-', '+')||','
||i.update_qty||','
||to_char(i.dstamp, 'YYYYMMDDHH24MISS')||','
||i.client_id||','
||i.sku_id||','
||i.from_loc_id||','
||i.to_loc_id||','
||i.final_loc_id||','
||i.tag_id||','
||i.reference_id||','
||i.line_id||','
||i.condition_id||','
||i.notes||','
||i.reason_id||','
||i.batch_id||','
||i.expiry_dstamp||','
||i.user_id||','
||i.shift||','
||i.station_id||','
||i.site_id||','
||i.container_id||','
||i.pallet_id||','
||i.list_id||','
||i.owner_id||','
||i.origin_id||','
||i.work_group||','
||i.consignment||','
||i.manuf_dstamp||','
||i.lock_status||','
||i.qc_status||','
||i.session_type||','
||i.summary_record||','
||i.elapsed_time||','
||i.supplier_id||','
--||oh.order_id||','||
--||oh.user_def_type_7||','||
--DECODE(oh.status,'Shipped',decode(i.update_qty,0,'Cancelled','Shipped'),'Part Shipped')||','
||i.user_def_type_1||','
||i.user_def_type_2||','
||i.user_def_type_3||','
||i.user_def_type_4||','
||i.user_def_type_5||','
||i.user_def_type_6||','
||i.user_def_type_7||','
||i.user_def_type_8||','
||i.user_def_chk_1||','
||i.user_def_chk_2||','
||i.user_def_chk_3||','
||i.user_def_chk_4||','
||i.user_def_date_1||','
||i.user_def_date_2||','
||i.user_def_date_3||','
||i.user_def_date_4||','
||i.user_def_num_1||','
||i.user_def_num_2||','
||i.user_def_num_3||','
||i.user_def_num_4||','
||i.user_def_note_1||','
||i.user_def_note_2
from inventory_transaction i
where i.client_id='MOUNTAIN'
and i.code = 'Shipment'
and i.uploaded = 'X'
--and trunc(i.Dstamp)>trunc(sysdate-7)
/

spool off

set feedback on
set termout on


update inventory_transaction
set uploaded='Y'
where client_id = 'MOUNTAIN'
--and trunc(Dstamp)>trunc(sysdate-7)
and code = 'Shipment'
and uploaded = 'X'
/


Update sequence_file
set next_seq=next_seq+1
where control_id=23;



--rollback;
commit;

 
