/******************************************************************************/
/*                                                                            */
/*                            Elite                                           */
/*                                                                            */
/*     FILE NAME  :   priputaway.sql                                          */
/*                                                                            */
/*     DESCRIPTION:    Datastream for Pringle Putaway inc. tag_id barcodes	  */
/*                                                                            */
/*   DATE     BY   PROJ       ID         DESCRIPTION                          */
/*   ======== ==== ======     ========   =============                        */
/*   09/05/05 LH   Pringle               Putaway Form                         */
/******************************************************************************/

/* Input Parameters are as follows */
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 0
SET VERIFY OFF
SET FEEDBACK OFF
SET TAB OFF

 
-- Set the line length to the maximum possible, but trim 
-- trailing spaces for both screen output and spool files
SET LINESIZE 32767
SET WRAP ON
SET TRIMOUT ON
SET TRIMSPOOL ON




/* Define the format of the output dates */
DEFINE	OutputDateFormat = 'DD-MON-YYYY HH24:MI:SS'
COLUMN	sorter	NOPRINT

SELECT '0' sorter,
'DSTREAM_PRI_PUTAWAY_HDR'				||'|'||
rtrim (sysdate)		
from dual			
union all
SELECT unique '0'||i.tag_id sorter,
'DSTREAM_PRI_PUTAWAY_LINE'              ||'|'||
rtrim (i.tag_id)						||'|'||
rtrim (i.receipt_id)				||'|'||
rtrim (i.sku_id)					||'|'||
rtrim (i.location_id)					||'|'||
rtrim (i.qty_on_hand)				||'|'||
rtrim (s.ean)
from inventory i, sku s
where i.client_id = 'PRI'
and receipt_id = '&&2'
and i.location_id like 'QC%'
and i.tag_id not like '%/S'
and i.lock_code is null
and s.sku_id = i.sku_id
group by i.tag_id, i.sku_id, i.location_id, i.qty_on_hand, i.receipt_id, s.ean
order by 1, 2
/