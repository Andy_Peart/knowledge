SET FEEDBACK OFF
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
set colsep ','
set heading off
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240
SET TRIMSPOOL ON


--spool OBPALIST.csv

select 'SKU,Style,Colour,Size,Season,Qty,EAN' from dual;

select
sku.sku_id||','||
sku.user_def_note_1||','||
sku.colour||','||
sku.sku_size||',,'||
pl.qty_due||',,'||
sku.EAN
from pre_advice_header ph,pre_advice_line pl,sku
where ph.client_id='OB'
and ph.pre_advice_id='&&2'
and pl.client_id like 'OB'
and ph.pre_advice_id=pl.pre_advice_id
and sku.client_id='OB'
and pl.sku_id=sku.sku_id
order by 1
/

--spool off
