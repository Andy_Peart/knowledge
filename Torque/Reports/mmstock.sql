SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 

column A heading 'Category' format A11
column B heading '&&2 Qty' format 9999999
column C heading 'VH Qty' format 9999999
column D heading 'ALL' format 9999999

set heading off
set pagesize 100

select 'Stock Holding Report for Client ID &&2 on ',to_char(SYSDATE, 'DD/MM/YY    HH:MI:SS') from DUAL;

set heading on




select
'Stock' A,
sum(i.qty_on_hand) B
from inventory i
where i.location_id not in ('IN','IN2','IN3','QCHOLD','WP1','SUSPENSE','CATALOGUES')
and i.client_id like '&&2'
/

SET PAGESIZE 0

select
'Allocated' A,
sum(i.qty_allocated) B
from inventory i
where i.location_id not in ('IN','IN2','IN3','QCHOLD','WP1','SUSPENSE','CATALOGUES')
and i.client_id like '&&2'
/
select
'Available' A,
sum(i.qty_on_hand-i.qty_allocated) B
from inventory i
where i.location_id not in ('IN','IN2','IN3','QCHOLD','WP1','SUSPENSE','CATALOGUES')
and i.client_id like '&&2'
/

select
'Catalogues' A,
nvl(sum(i.qty_on_hand),0) B
from inventory i
where i.location_id='CATALOGUES'
and i.client_id like '&&2'
/

select 'in IN ' A,
nvl(sum(jc.qty_on_hand),0) B 
from inventory jc
where jc.location_id='IN'
and jc.client_id like '&&2'
/
select 'in IN2' A,
nvl(sum(jc.qty_on_hand),0) B 
from inventory jc
where jc.location_id='IN2'
and jc.client_id like '&&2'
/
select 'in IN3' A,
nvl(sum(jc.qty_on_hand),0) B 
from inventory jc
where jc.location_id='IN3'
and jc.client_id like '&&2'
/
select 'in QCHOLD' A,
nvl(sum(jc.qty_on_hand),0) B 
from inventory jc
where jc.location_id='QCHOLD'
and jc.client_id like '&&2'
/

select 'in WP1' A,
nvl(sum(jc.qty_on_hand),0) B
from inventory jc
where jc.location_id='WP1'
and jc.client_id like '&&2'
/

select 
'in SUSPENSE ' A,
nvl(sum(jc.qty_on_hand),0) B
from inventory jc
where jc.location_id='SUSPENSE'
and jc.client_id like '&&2'
/


select
'Full Total' A,
sum(i.qty_on_hand) B
from inventory i
where i.client_id like '&&2'
/
