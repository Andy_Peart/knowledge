SET FEEDBACK OFF
SET VERIFY OFF
SET TAB OFF
SET COLSEP ' '

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 9999
SET LINESIZE 500
SET TRIMSPOOL ON

column A Heading 'Category'
column B Heading 'Qty'
column C Heading 'No.'
column D Heading 'Date'
column AA Heading 'Loc Type'
column BB Heading 'On Hand'
column CC Heading 'Allocated'
column E Heading 'Orders'
column F Heading 'Qty Ordered'
column G Heading 'Qty Short'
column OT Heading 'Order Type'
column D2 Heading 'Location'
column D3 Heading 'Location'
column D4 Heading 'Location'

SET LINESIZE 130
SET WRAP OFF
SET PAGES 66
SET COLSEP ' '
SET NEWPAGE 1

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

--spool obnew.txt


TTITLE LEFT 'Goods Received' 

select
trunc(it.Dstamp) D,
DECODE(substr(it.to_loc_id,1,3),'SSQ','Quarantine','SSC','Consumables','SSP','Consumables','Good') A,
sum(it.update_qty) as B
from inventory_transaction it
--where (trunc(it.Dstamp)=to_date('&&2', 'DD-MON-YYYY') or trunc(it.Dstamp)=to_date('&&3', 'DD-MON-YYYY'))
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='SS'
and it.station_id not like 'Auto%'
and it.code like 'Receipt%'
group by
trunc(it.Dstamp),
DECODE(substr(it.to_loc_id,1,3),'SSQ','Quarantine','SSC','Consumables','SSP','Consumables','Good')
order by D,A
/

TTITLE LEFT 'Goods Ordered'

select
trunc(oh.creation_date) D,
oh.order_type OT,
count(distinct oh.order_id) E,
sum(ol.qty_ordered) F,
nvl(QQQ,0) G
from order_header oh,order_line ol,(select
trunc(Dstamp) DDATE,
sum(qty_ordered-qty_tasked) QQQ
from generation_shortage
where client_id='SS'
group by
trunc(Dstamp))
where oh.client_id='SS'
--and (trunc(oh.creation_date)=to_date('&&2', 'DD-MON-YYYY') or trunc(oh.creation_date)=to_date('&&3', 'DD-MON-YYYY'))
and oh.creation_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and oh.client_id=ol.client_id
and oh.order_id=ol.order_id
and trunc(oh.creation_date)=DDATE (+)
group by
trunc(oh.creation_date),
oh.order_type,
nvl(QQQ,0)
order by D
/


TTITLE LEFT 'Goods Allocated'

select
trunc(it.Dstamp) D,
it.final_loc_id D3,
DECODE(substr(it.from_loc_id,1,3),'SSQ','Quarantine','SSC','Consumables','SSP','Consumables','Good') A,
sum(it.update_qty) as B
from inventory_transaction it
--where (trunc(it.Dstamp)=to_date('&&2', 'DD-MON-YYYY') or trunc(it.Dstamp)=to_date('&&3', 'DD-MON-YYYY'))
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='SS'
and it.code like 'Allocate'
group by
trunc(it.Dstamp),
it.final_loc_id,
DECODE(substr(it.from_loc_id,1,3),'SSQ','Quarantine','SSC','Consumables','SSP','Consumables','Good')
order by D,A
/


TTITLE LEFT 'Goods Dispatched (NOT WEB) Original Version by each items dispatch date'

select
trunc(it.Dstamp) D,
DECODE(substr(it.from_loc_id,1,3),'SSQ','Quarantine','SSC','Consumables','SSP','Consumables','Good') A,
count(distinct reference_id) E,
sum(it.update_qty) as B
from inventory_transaction it
--where (trunc(it.Dstamp)=to_date('&&2', 'DD-MON-YYYY') or trunc(it.Dstamp)=to_date('&&3', 'DD-MON-YYYY'))
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='SS'
and it.code like 'Shipment'
and it.from_loc_id<>'SSWEB'
group by
trunc(it.Dstamp),
DECODE(substr(it.from_loc_id,1,3),'SSQ','Quarantine','SSC','Consumables','SSP','Consumables','Good')
order by D,A
/

TTITLE LEFT 'Goods Dispatched (NOT WEB) Version 2 by order shipped date'

select
D,
OT,
count(*) C,
sum(B) B
from (select
trunc(oh.shipped_date) D,
oh.order_id E,
oh.order_type OT,
--DECODE(substr(it.from_loc_id,1,3),'SSQ','Quarantine','SSC','Consumables','SSP','Consumables','Good') A,
sum(ol.qty_shipped) as B
from order_header oh,order_line ol
where oh.client_id='SS'
and oh.shipped_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and oh.order_id=ol.order_id
and oh.order_type<>'WEB'
group by
trunc(oh.shipped_date),
oh.order_id,
oh.order_type)
group by D,OT
order by D
/

TTITLE LEFT 'Goods Dispatched (NOT WEB) 21 units or under'

select  shipped_date as D,
        count(DISTINCT order_id) E,
        sum(qty_ordered) F
from (  select  trunc(oh.shipped_date) shipped_date,
                oh.order_id,
                sum(ol.qty_ordered) qty_ordered
        from order_header oh,order_line ol
        where oh.client_id='SS'
        and oh.shipped_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
        and oh.order_id=ol.order_id
        and oh.order_type<>'WEB'
        having sum(ol.qty_shipped) <= 21
        group by    trunc(oh.shipped_date),
                    oh.order_id,
                    oh.order_type)
group by shipped_date
order by shipped_date
/


TTITLE LEFT 'All consumables by order received date'

select  trunc(it.Dstamp) D,
        sum(it.update_qty) as B
from inventory_transaction it
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='SS'
and (it.code like 'Receipt%' )
and substr(it.to_loc_id,1,3) IN ('SSC','SSP')
group by
trunc(it.Dstamp)
order by D
/

TTITLE LEFT 'Web Orders Dispatched (Single Unit) by order shipped date'

select
D,
count(*) C,
sum(B) B
from (select
trunc(oh.shipped_date) D,
oh.order_id E,
sum(ol.qty_shipped) as B
from order_header oh,order_line ol
where oh.client_id='SS'
and oh.shipped_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and oh.order_id=ol.order_id
and oh.order_type='WEB'
group by
trunc(oh.shipped_date),
oh.order_id)
where B=1
group by D
order by D
/

TTITLE LEFT 'Web Orders Dispatched (Multiple Units) by order shipped date'

select
D,
count(*) C,
sum(B) B
from (select
trunc(oh.shipped_date) D,
oh.order_id E,
sum(ol.qty_shipped) as B
from order_header oh,order_line ol
where oh.client_id='SS'
and oh.shipped_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and oh.order_id=ol.order_id
and oh.order_type='WEB'
group by
trunc(oh.shipped_date),
oh.order_id)
where B>1
group by D
order by D
/




ttitle  LEFT 'STOCK HOLDING as at ' report_date 

select
DECODE(substr(jc.location_id,1,3),'SSQ','Quarantine','SSC','Consumables','SSP','Consumables','Good') A,
lc.loc_type AA,
nvl(sum(jc.qty_on_hand),0) BB,
nvl(sum(jc.qty_allocated),0) CC
from inventory jc,location lc
where jc.client_id like 'SS'
and jc.location_id=lc.location_id
and jc.site_id=lc.site_id
group by
DECODE(substr(jc.location_id,1,3),'SSQ','Quarantine','SSC','Consumables','SSP','Consumables','Good'),
lc.loc_type
order by 1,2
/

--spool off
