SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 800
SET TRIMSPOOL ON
 

column A heading 'Date' format A12
column B heading 'PreAdvice ID' format A20
column B1 heading 'SKU' format A18
column B2 heading 'Description' format A24
column C heading 'Qty' format 9999999


--break on B dup on report
--compute sum label 'PreAdvice Total' of C on B
--compute sum label 'Day Total' of C on report

--spool bpr6.csv

select 'PORD Number,RP Order,Berwin Order,Client,Units Due,Warehouse Units,Processing Units,Container,Due Date,Labels Received,Rework Required,Deliver Date,Completion Date,Garment Type,Location,Ref Number' from DUAL;

select
A1||','||
A2||','||
A99||','||
--A3||','||
A4||','||
A5||','||
A5B||','||
A6||','||
A7||','||
A8||','||
A9||',"'||
A10||'",'||
--A11||','||
A12||','||
A13||','||
A14||','||
A15||','||
A16||',,,'||
' '
from (select
--'1 '||
ph.pre_advice_id A1,
ORDNUM A2,
' ' A99,
--pl.sku_id A3,
nvl(Client,' ') A4,
sum(pl.qty_due) A5,
sum(IVQ) A5B,
sum(QQQ) A6,
rtrim(ph.user_def_note_2) A7,
trunc(nvl(thedate,ph.due_dstamp)) A8,
trunc(ph.user_def_date_1+0.05) A9,
rtrim(ph.notes) A10,
trunc(LODATE+0.05) A11,
trunc(DBD) A12,
trunc(ph.user_def_date_2+0.05) A13,
rtrim(min(sku.user_def_note_2)) A14,
ph.user_def_note_1 A15,
REFNUM A16
from pre_advice_header ph,pre_advice_line pl,sku,(select
sku_id IVSKU,
receipt_id IVRC,
sum(qty_on_hand) IVQ
from inventory
where client_id='BW'
group by
sku_id,receipt_id),(select
distinct
oh.name Client,
oh.order_id ORDNUM,
ol.user_def_type_1 REFNUM,
oh.user_def_date_1 LODATE,
oh.deliver_by_date DBD,
ol.sku_id SKU,
ol.user_def_note_2 PORD,
sum(ol.qty_ordered) QQQ
from order_header oh,order_line ol
where oh.client_id='BW'
and oh.status not in ('Cancelled','Shipped')
and oh.order_id=ol.order_id
and ol.client_id='BW'
group by
oh.name,
oh.order_id,
ol.user_def_type_1,
oh.user_def_date_1,
oh.deliver_by_date,
ol.user_def_note_2,
ol.sku_id),containerdates cd
where ph.client_id='BW'
and ph.notes is not null
and ph.status='Complete'
--and ph.pre_advice_id in ('PORD152205')
and pl.client_id='BW'
and ph.pre_advice_id=pl.pre_advice_id
and pl.sku_id=sku.sku_id
and sku.client_id='BW'
and pl.sku_id=IVSKU
and ph.pre_advice_id=IVRC
and pl.sku_id=SKU (+)
and pl.pre_advice_id=PORD (+)
and ph.user_def_note_2=cd.container_id (+)
and ph.pre_advice_id not in (select
distinct
ph.pre_advice_id
from order_header oh,order_line ol,inventory iv,sku,pre_advice_header ph,pre_advice_line pl
where oh.client_id='BW'
and oh.status<>'Cancelled'
and oh.order_id=ol.order_id
and ol.client_id='BW'
and upper(ol.user_def_note_2) not like 'PORD%'
and nvl(ol.qty_shipped,0)=0
and ol.sku_id=iv.sku_id
and iv.client_id='BW'
and ol.sku_id=sku.sku_id
and sku.client_id='BW'
and iv.receipt_id=ph.pre_advice_id
and ph.client_id='BW'
and ph.notes is not null
and pl.pre_advice_id=ph.pre_advice_id
and pl.client_id='BW'
and pl.sku_id=ol.sku_id)
group by
ph.pre_advice_id,
ORDNUM,
--pl.sku_id,
nvl(Client,' '),
rtrim(ph.user_def_note_2),
--QQQ,
trunc(nvl(thedate,ph.due_dstamp)),
trunc(ph.user_def_date_1+0.05),
rtrim(ph.notes),
trunc(LODATE+0.05),
trunc(DBD),
trunc(ph.user_def_date_2+0.05),
ph.user_def_note_1,
REFNUM,
ph.user_def_type_1
union
select
--'2 '||
ph.pre_advice_id A1,
ORDNUM A2,
' ' A99,
--'ALL' A3,
nvl(Client,' ') A4,
sum(pl.qty_due) A5,
0 A5B,
QQQ A6,
rtrim(ph.user_def_note_2) A7,
trunc(nvl(thedate,ph.due_dstamp)) A8,
trunc(ph.user_def_date_1+0.05) A9,
rtrim(ph.notes) A10,
trunc(LODATE+0.05) A11,
trunc(DBD) A12,
trunc(ph.user_def_date_2+0.05) A13,
rtrim(min(sku.user_def_note_2)) A14,
ph.user_def_note_1 A15,
REFNUM A16
from pre_advice_header ph,pre_advice_line pl,sku,(select
distinct
oh.name Client,
oh.order_id ORDNUM,
ol.user_def_type_1 REFNUM,
oh.user_def_date_1 LODATE,
oh.deliver_by_date DBD,
ol.user_def_note_2 PORD,
sum(ol.qty_ordered) QQQ
from order_header oh,order_line ol
where oh.client_id='BW'
and oh.status<>'Cancelled'
and oh.order_id=ol.order_id
and ol.client_id='BW'
group by
oh.name,
oh.order_id,
ol.user_def_type_1,
oh.user_def_date_1,
oh.deliver_by_date,
ol.user_def_note_2),containerdates cd
where ph.client_id='BW'
and ph.notes is not null
and ph.status='Released'
--and ph.pre_advice_id in ('PORD152205')
and pl.client_id='BW'
and ph.pre_advice_id=pl.pre_advice_id
and pl.sku_id=sku.sku_id
and sku.client_id='BW'
and ph.pre_advice_id=PORD (+)
and ph.user_def_note_2=cd.container_id (+)
group by
ph.pre_advice_id,
ORDNUM,
nvl(Client,' '),
rtrim(ph.user_def_note_2),
QQQ,
trunc(nvl(thedate,ph.due_dstamp)),
trunc(ph.user_def_date_1+0.05),
rtrim(ph.notes),
trunc(LODATE+0.05),
trunc(DBD),
trunc(ph.user_def_date_2+0.05),
ph.user_def_note_1,
REFNUM,
ph.user_def_type_1
union
select
distinct
--'3 '||
ph.pre_advice_id A1,
oh.order_id A2,
substr(ol.user_def_note_2,1,16) A99,
--ol.sku_id A3,
oh.name A4,
0 A5,
sum(iv.qty_on_hand) A5B,
sum(ol.qty_ordered) A6,
rtrim(ph.user_def_note_2) A7,
trunc(nvl(thedate,ph.due_dstamp)) A8,
trunc(ph.user_def_date_1+0.05) A9,
rtrim(ph.notes) A10,
trunc(oh.user_def_date_1) A11,
trunc(oh.deliver_by_date) A12,
trunc(ph.user_def_date_2+0.05) A13,
--rtrim(sku.user_def_note_2) A14,
ol.user_def_note_1||' - '||substr(ol.notes,4,3) A14,
ph.user_def_note_1 A15,
ol.user_def_type_1 A16
from order_header oh,order_line ol,inventory iv,sku,pre_advice_header ph,pre_advice_line pl,containerdates cd
where oh.client_id='BW'
and oh.status<>'Cancelled'
and oh.order_id=ol.order_id
and ol.client_id='BW'
and upper(ol.user_def_note_2) not like 'PORD%'
and nvl(ol.qty_shipped,0)=0
and ol.sku_id=iv.sku_id
and iv.client_id='BW'
and ol.sku_id=sku.sku_id
and sku.client_id='BW'
and iv.receipt_id=ph.pre_advice_id
and ph.client_id='BW'
and ph.notes is not null
--and ph.pre_advice_id in ('PORD152205')
and pl.pre_advice_id=ph.pre_advice_id
and pl.client_id='BW'
and pl.sku_id=ol.sku_id
and ph.user_def_note_2=cd.container_id (+)
group by
ph.pre_advice_id,
oh.order_id,
substr(ol.user_def_note_2,1,16),
--ol.sku_id,
oh.name,
rtrim(ph.user_def_note_2),
trunc(nvl(thedate,ph.due_dstamp)),
trunc(ph.user_def_date_1+0.05),
rtrim(ph.notes),
trunc(oh.user_def_date_1),
trunc(oh.deliver_by_date),
trunc(ph.user_def_date_2+0.05),
--rtrim(sku.user_def_note_2),
ol.user_def_note_1||' - '||substr(ol.notes,4,3),
ph.user_def_note_1,
ol.user_def_type_1 )
order by
A12,A8,A2,A1,A7
/

--spool off


