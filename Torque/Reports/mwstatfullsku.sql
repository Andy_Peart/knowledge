SET FEEDBACK OFF
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
set colsep ','
set heading off
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9999
SET TRIMSPOOL ON

column A heading 'EXTBARCODE' format a20
column D heading 'QTY' format 9999999


select 'EXTBARCODE,QTY'
from dual;

select A,
D
from (select A,
B - C "D"
from (select i.sku_id A, sum(i.qty_on_hand) B,
nvl(s.user_def_num_3,0) C
from inventory i, sku s,location ll
where i.client_id = 'MOUNTAIN'
and upper(i.sku_id)<>lower(i.sku_id)
and i.location_id=ll.location_id
and ll.loc_type in ('Tag-FIFO','Suspense')
and ll.site_id='BD2'
and i.sku_id <> 'TEST'
and i.sku_id = s.sku_id
and s.client_id = 'MOUNTAIN'
group by s.user_def_num_3, i.sku_id)
order by A)
where D > 0
/
