SET FEEDBACK OFF                 
set pagesize 66
set linesize 136
set verify off
set newpage 0

clear columns
clear breaks
clear computes

column A heading 'BULKY ITEM PICKS' format 999999
column B heading 'TOTAL PICKS' format 999999


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'BULKY AND TOTAL ITEMS PICKED - from &&2 to &&3 - MOUNTAIN as at ' report_date skip 2


select A,
B, 
CASE
WHEN B=0 THEN 0
ELSE ROUND((A/B)*100,2)
END "PERCENTAGE" 
FROM (select(select COUNT(*)
from inventory_transaction it, sku s
where it.client_id ='MOUNTAIN'
and it.code ='Pick'
and it.from_loc_id != 'CONTAINER'
and s.sku_id =it.sku_id
and s.client_id =it.client_id
and it.work_group <>'WWW'
and (s.description like '%TENT5'
or s.description like '%SHOE%'
or s.description like '%BOOT%'
or s.description like '%JACKET%'
or s.description like '%TABLE%'
or s.description like '%CHAIR%'
or s.description like '%FLEECE%'
or s.description like '%SLEEPING BAG%')
and it.dstamp between to_date('&&2','DD-MON-YYYY') and to_date('&&3','DD-MON-YYYY')+1
) AS A,
(
select COUNT(*)
from inventory_transaction it, sku s
where it.client_id ='MOUNTAIN'
and it.code ='Pick'
and it.from_loc_id != 'CONTAINER'
and s.sku_id =it.sku_id
and s.client_id =it.client_id
and it.work_group <>'WWW'
and it.dstamp between to_date('&&2','DD-MON-YYYY') and to_date('&&3','DD-MON-YYYY')+1
) AS B
FROM DUAL
) TEST
/
