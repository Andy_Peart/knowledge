SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET COLSEP ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 200
SET TRIMSPOOL ON
SET HEADING OFF 

select 'Curvy Kate Pre Advice &&2 Detail' from dual
union all
select 'Status,Pre-Advice ID,Line ID,Sku ID,Description,Size,Qty Due,Qty Received' from dual
union all
select "Status" || ',' || "Pre-Advice ID" || ',' || "Line ID" || ',' || "Sku ID" || ',' || "Description" || ',' || "Size" || ',' || "Qty Due" || ',' || "Qty Received" from
(
select ph.status          as "Status"
, ph.pre_advice_id        as "Pre-Advice ID"
, pl.line_id              as "Line ID"
, pl.sku_id               as "Sku ID"
, sku.description         as "Description"
, sku.sku_size || case when length(sku.user_def_note_1)<=2 then sku.user_def_note_1 end            as "Size"
, pl.qty_due              as "Qty Due"
, nvl(pl.qty_received,0)  as "Qty Received"
from pre_advice_header ph
inner join pre_advice_line pl on ph.client_id = pl.client_id and ph.pre_advice_id=pl.pre_advice_id
inner join sku on ph.client_id = sku.client_id and pl.sku_id=sku.sku_id
where ph.client_id like 'CK'
and ph.pre_advice_id = '&&2'
)
/
