define client="&&1"
define fromDate="&&2"
define toDate="&&3"

SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ,


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 1000
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Order Type,Customer Id,Dispatch Method,Order Id,Creation Date,Order Date,Shipped Date,Shipped Hour,Picked Date,Picked Hour,Status,Lines,Units Ordered,Units Picked,Units Shipped,Units Outstanding' from dual
    union all
select order_type||','||customer_id||','||dispatch_method||','||order_id||','||creationDate||','||orderDate||','||shippedDate||','||shippedHour
||','||pickedDate||','||status||','||num_lines||','||unitsOrdered||','||unitsPicked||','||unitsShipped||','||outstanding from
(
    select oh.order_type
    , oh.customer_id
    , oh.dispatch_method
    , oh.order_id
    , trunc(oh.creation_date) creationDate
    , trunc(oh.order_date) orderDate
    , trunc(oh.shipped_date) shippedDate
    , to_char(oh.shipped_date, 'HH24') shippedHour
    , coalesce((select max(to_char(dstamp, 'DD-MON-YYYY,HH24')) from INVENTORY_TRANSACTION where client_id = '&&client' and code = 'Pick' and reference_id = oh.order_id),',') pickedDate
    , oh.status
    , oh.num_lines
    , sum(ol.qty_ordered) unitsOrdered
    , coalesce(sum(ol.qty_picked),0) unitsPicked
    , coalesce(sum(ol.qty_shipped),0) unitsShipped
    , sum(ol.qty_ordered) - coalesce(sum(ol.qty_picked),0) outstanding
    from ORDER_HEADER oh
    inner join ORDER_LINE ol on ol.order_id = oh.order_id and ol.client_id = oh.client_id
    inner join SKU s on s.sku_id = ol.sku_id and s.client_id = ol.client_id
    where oh.client_id = '&&client'
    and oh.status = 'Shipped'
    and oh.shipped_date between to_date('&&fromDate') and to_date('&&toDate') + 1
    group by oh.order_type
    , oh.customer_id
    , oh.dispatch_method    
    , oh.order_id
    , trunc(oh.creation_date) 
    , trunc(oh.order_date)
    , oh.status
    , oh.num_lines
    , trunc(oh.shipped_date)
    , to_char(oh.shipped_date, 'HH24')
        union
    select oh.order_type
    , oh.customer_id
    , oh.dispatch_method
    , oh.order_id
    , trunc(oh.creation_date) creationDate
    , trunc(oh.order_date) orderDate
    , null shippedDate
    , null shippedHour
    , coalesce((select max(to_char(dstamp, 'DD-MON-YYYY,HH24')) from INVENTORY_TRANSACTION where client_id = '&&client' and code = 'Pick' and reference_id = oh.order_id),',') pickedDate
    , oh.status
    , oh.num_lines
    , sum(ol.qty_ordered) unitsOrdered
    , coalesce(sum(ol.qty_picked),0) unitsPicked
    , coalesce(sum(ol.qty_shipped),0) unitsShipped
    , sum(ol.qty_ordered) - coalesce(sum(ol.qty_picked),0) outstanding
    from ORDER_HEADER oh
    inner join ORDER_LINE ol on ol.order_id = oh.order_id and ol.client_id = oh.client_id
    inner join SKU s on s.sku_id = ol.sku_id and s.client_id = ol.client_id
    where oh.client_id = '&&client'
    and oh.status in ('Allocated','Released','Hold','In Progress','Complete','Picked')
    group by oh.order_type
    , oh.customer_id
    , oh.dispatch_method
    , oh.order_id
    , trunc(oh.creation_date) 
    , trunc(oh.order_date)
    , oh.status
    , oh.num_lines
    , trunc(oh.shipped_date)
    , to_char(oh.shipped_date, 'HH24')
)
/
--spool off