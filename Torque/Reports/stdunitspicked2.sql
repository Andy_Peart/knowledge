SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON


break on D skip 1 on report

compute sum label 'Day Total' of B on D
compute sum label 'Total' of B on report

--spool stpicks2.csv

select 'Date,Order Id,Qty Picked' from DUAL;



select
trunc(Dstamp) D,
reference_id A,
sum(update_qty) B
from inventory_transaction
where client_id='&&2'
and code='Pick'
and elapsed_time>0
and Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
group by
trunc(Dstamp),
reference_id
union
select
trunc(Dstamp) D,
reference_id A,
sum(update_qty) B
from inventory_transaction_archive
where client_id='&&2'
and code='Pick'
and elapsed_time>0
and Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
group by
trunc(Dstamp),
reference_id
order by D,A
/

--spool off
