SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep '|'


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 300
SET TRIMSPOOL ON
 
column XX NOPRINT

--spool mkukmail.csv

SELECT
OH.order_id  XX,
'H680001'||'|'||
to_char(Sysdate,'DDMMYYYY')||'|'||
replace(oh.contact, '"')||'|'||
replace(oh.Name, '"')||'|'||
replace(oh.Address1, '"')||'|'||
replace(oh.Address2, '"')||'|'||
replace(oh.Town, '"')||'|'||
replace(oh.County, '"')||'|||'||
replace(oh.PostCode, '"')||'||'||
'1'||'|'||
'10'||'|'||
'1'||'|'||
replace(oh.instructions, '"')||'|'||
''||'|||||'||
'PG'||OH.order_id||'|||||||||||||||'||
'Torque (Ping) Challenge Way Wigan WN5 0LD'||'||'||
' '
from order_header oh
WHERE OH.CLIENT_ID = 'PG'
AND OH.ORDER_ID = '&&2' 
/

--spool off
