set feedback off
set pagesize 66
set linesize 1500
set verify off
set newpage 0
SET WRAP OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 1500

column A heading 'SKU code' format A60

select 'SKUs in Multiple Locations between &&2 and &&3 for client ID SB as at ' || to_char(sysdate, 'DD-MON-YYYY') from dual
union all
select ' ' from dual
union all
SELECT 'SKU code, Description, Location, Qty on Hand, Stack Description, Commodity Description, def8, def3, def2'
from dual
union all
select A ||','|| B ||','|| C ||','|| D ||','|| stack_description ||','|| commodity_desc ||','|| user_def_type_8 ||','|| user_def_type_3 ||','|| user_def_note_2
from
(select
'="' || i.sku_id || '"' A,
sku.description B,
i.location_id C,
sku.stack_description,
sku.COMMODITY_DESC,
sku.user_def_type_8,
sku.user_def_type_3,
sku.user_def_note_2,
sum(i.qty_on_hand) D
from inventory i,sku,(select sku_id as SK from
(select sku_id,count(*) as CT from
(select sku_id,location_id,sum(qty_on_hand)
from inventory
where client_id='SB'
and location_id not in ('CONTAINER','VDESP','HOLD','IN','IN2','IN3','IN4','IN5','PACKM','PACKSB')
and location_id between '&&2' and '&&3'
group by sku_id,location_id
order by sku_id,location_id)
group by sku_id)
where CT>1)
where i.sku_id=SK
and i.sku_id=sku.sku_id
and i.client_id='SB'
and sku.client_id='SB'
and i.location_id not in ('CONTAINER','VDESP','HOLD','IN','IN2','IN3','IN4','IN5','PACKM','PACKSB')
and location_id between '&&2' and '&&3'
group by i.sku_id,sku.description,i.location_id,sku.stack_description,
sku.COMMODITY_DESC,
sku.user_def_type_8,
sku.user_def_type_3,
sku.user_def_note_2
order by i.sku_id,i.location_id)
/

select
'Location Count =',
count (*) D
from (select distinct location_id from inventory
where client_id like 'SB'
and location_id between '&&2' and '&&3'
and substr(location_id,1,1)<>'T'
and substr(location_id,1,1)<>'V')
/


select
'     SKU Count =',
count (*) D
from (select distinct sku_id from inventory
where client_id like 'SB'
and location_id between '&&2' and '&&3'
and substr(location_id,1,1)<>'T'
and substr(location_id,1,1)<>'V')
/

select
'    Unit Count =',
nvl(sum(qty_on_hand),0) D
from inventory
where client_id like 'SB'
and location_id between '&&2' and '&&3'
and substr(location_id,1,1)<>'T'
and substr(location_id,1,1)<>'V'
/
