SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','
SET WRAP OFF


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 1500

select 'Sku,Box No,EAN,Description,Sets Qty,Boxes Qty,TNT Tracking No,Qty Ordered,Qty Picked,Order ID,Customer Name,Contact,Address,Address,City,Postcode,Country,Senders ID,Senders Address,Senders Address,City,Senders Postcode,Senders Country,Senders Contact Email' from dual
union all
select sku_id || ',' || box_no || ',' || ean || ',' || description || ',' || sets_qty || ',' || boxes_qty || ',' || tnt_tracking_number || ',' || qty_ordered || ',' || qty_picked || ',' || order_id || ',' || name || ',' || contact || ',' || address1 || ',' || address2 || ',' || town || ',' || postcode || ',' || country || ',' || client_id || ',' || client_address1 || ',' || client_address2 || ',' || client_town || ',' || client_postcode || ',' || client_country || ',' || client_email
from (
with ship_man as (
	select sm.sku_id, sm.order_id, sm.line_id, sm.client_id, sm.container_id, sum(sm.qty_picked) qty_picked, sk.description, sk.ean
	from shipping_manifest sm left join sku sk on sk.sku_id = sm.sku_id and sk.client_id = sm.client_id
	where sm.client_id = 'HS'
	and sm.order_id = '&&2'
	group by sm.order_id, sm.line_id, sm.client_id, sm.container_id, sm.sku_id, sk.description, sk.ean
),
ord_line as (
	select a.client_id, a.order_id, A.line_id, b.sku_id, A.container_id, B.qty_ordered
	from 
	(
		select sm.client_id, sm.order_id, sm.line_id, max(sm.container_id) container_id
		from shipping_manifest sm 
		where sm.client_id = 'HS'
		and sm.order_id = '&&2'
		group by sm.order_id, client_id, sm.line_id
	) A left join 
	(
		select ol.line_id, ol.sku_id, sum(ol.qty_ordered) qty_ordered
		from order_line ol
		where ol.client_id = 'HS'
		and ol.order_id = '&&2'
		group by ol.line_id,ol.sku_id
		order by 1
	) B
	on A.line_id = B.line_id
	order by 2
),
smol as (
    select ol.order_id, sm.sku_id, sm.container_id, sm.ean, sm.description, ol.qty_ordered, sm.qty_picked
    from ord_line ol left join ship_man sm on ol.line_id = sm.line_id
)
select 	smol.SKU_ID,
	smol.container_id as Box_no,
	smol.EAN as EAN,
  	Initcap(smol.DESCRIPTION) as Description,
	Initcap(oh.USER_DEF_TYPE_3) as Sets_Qty,
	Initcap(oh.USER_DEF_TYPE_4) as Boxes_Qty,
	Initcap(oh.USER_DEF_TYPE_6) as TNT_tracking_number,
	smol.Qty_ordered,
	smol.QTY_PICKED,
	oh.ORDER_ID,
	upper(oh.NAME) as NAME,
	upper(oh.CONTACT) as CONTACT,
	initcap(oh.ADDRESS1) as ADDRESS1,
	initcap(oh.ADDRESS2) as ADDRESS2,
	upper(oh.TOWN) as TOWN,
	upper(oh.POSTCODE) as POSTCODE,
	upper(replace(c.TANDATA_ID,'_',' ')) as COUNTRY,
	oh.CLIENT_ID,
	Initcap(cli.address1) client_address1,
	Initcap(cli.address2) client_address2,
	Initcap(cli.town) client_town,
	Initcap(cli.postcode) client_postcode,
	upper(replace(cc.TANDATA_ID,'_',' ')) as client_country,
	Initcap(cli.contact_email) client_email  
from smol inner join order_header oh on oh.order_id = smol.order_id
	left join country c on c.iso3_id = oh.country
	inner join client cli on cli.client_id = oh.client_id
	left join country cc on cc.iso3_id = cli.country
order by 3
)
/