SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 130
SET TRIMSPOOL ON


column A heading 'SKU ID' format A18
column B heading 'Colour' format A18
column C heading 'Size' format A8
column D heading 'Description' format A40
column E heading 'Location' format A12
column F heading 'Stock' format 99999999
column G heading 'Last Count' format A12


--spool utunder6.csv


select 'Location,SKU ID,Colour,Size,Description,Available,Last Count,,' from DUAL;

select
distinct
i.location_id E,
''''||sku.sku_id||'''' A,
sku.colour B,
sku.sku_size C,
sku.description D,
CCC F,
to_char(i.count_dstamp,'DD/MM/YYYY') G,
' '
from inventory i,sku,
(select
iv.sku_id BBB,
sum(iv.qty_on_hand - iv.qty_allocated) CCC
from inventory iv,location L
where iv.client_id='UT'
and iv.location_id like '&&2%'
and iv.location_id=L.location_id
and iv.site_id=L.site_id
and L.loc_type='Tag-FIFO'
group by
iv.sku_id),location L
where i.client_id='UT'
and i.location_id like '&&2%'
and i.sku_id=BBB
and sku.sku_id=BBB
and sku.client_id='UT'
and CCC<6
and i.location_id=L.location_id
and i.site_id=L.site_id
and L.loc_type='Tag-FIFO'
order by
E,A
/


--spool off
