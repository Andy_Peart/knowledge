SET FEEDBACK OFF                 
set verify off
set wrap off
set colsep ','

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 132
set trimspool on


select 'Total Tasks by User ID from &&2 &&4 to &&3 &&5 for MOUNTAIN' from dual
union all
select 'Workstation,User,Task,Order Type,Tasks,Units' from dual
union all
select "RDT" || ',' || "User" || ',' || "Code" || ',' || "Type" || ',' || "Tasks" || ',' || "Units"
from
(
select station_id                                                               as "RDT"
, user_id                                                                       as "User"
, code                                                                          as "Code"
, case when substr(work_group,2,2) = 'WW' then 'Mail Order' else 'Outlets' end  as "Type"
, sum(case when update_qty = 0 then 0 else 1 end)                               as "Tasks"
, sum(update_qty)                                                               as "Units"
from inventory_transaction
where client_id = 'MOUNTAIN'
and dstamp between to_date('&&2 &&4', 'DD/MM/YYYY HH24:MI:SS') and to_date('&&3 &&5', 'DD/MM/YYYY HH24:MI:SS')+1
and code in ('Pick','Receipt','Putaway','Replenish','Relocate','Stock Check','Consol')
and substr(station_id,1,3) = 'RDT'
and elapsed_time > 0
group by station_id   
, user_id           
, code              
, case when substr(work_group,2,2) = 'WW' then 'Mail Order' else 'Outlets' end
having sum(update_qty) <> 0
union all
select 'Grandtotals'
, 'xxxx'
, code              
, case when substr(work_group,2,2) = 'WW' then 'Mail Order' else 'Outlets' end
, sum(case when update_qty = 0 then 0 else 1 end)
, sum(update_qty)
from inventory_transaction
where client_id = 'MOUNTAIN'
and dstamp between to_date('&&2 &&4', 'DD/MM/YYYY HH24:MI:SS') and to_date('&&3 &&5', 'DD/MM/YYYY HH24:MI:SS')+1
and code in ('Pick','Receipt','Putaway','Replenish','Relocate','Stock Check','Consol')
and substr(station_id,1,3) = 'RDT'
and elapsed_time > 0
group by code              
, case when substr(work_group,2,2) = 'WW' then 'Mail Order' else 'Outlets' end
having sum(update_qty) <> 0
order by 2,3,4,1
)
/