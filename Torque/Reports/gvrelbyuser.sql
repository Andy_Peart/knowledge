SET FEEDBACK OFF                
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 66
SET NEWPAGE 0
SET LINESIZE 100
SET TRIMSPOOL ON

column AA Heading 'Date' format A14
column SS Heading 'From' format A10
column RR Heading 'To' format A12
column UU Heading 'User' format A12
column A2 Heading 'Units' format 999999
column F format A10 noprint

break on report

compute sum label 'Totals' of A2 on report

ttitle '     GIVE Relocates for &&2' skip 2


--select 'Date,From,To,User,Units,' from DUAL;



select
--to_char(it.dstamp, 'DD-MON-YYYY') AA,
it.from_loc_id SS,
l2.zone_1 RR,
it.user_id UU,
sum(it.update_qty) A2
from inventory_transaction it,location l1,location l2
where it.client_id = 'GV'
and trunc(it.Dstamp)=to_date('&&2','dd-mon-yyyy')
and it.code='Relocate'
--and it.uploaded = 'N'
--and it.dstamp > sysdate-1
and it.from_loc_id=l1.location_id
and it.to_loc_id=l2.location_id
and it.from_loc_id='AQL'
and l2.zone_1 in ('GIVE','OUTLET')
group by
--to_char(it.dstamp, 'DD-MON-YYYY'),
it.from_loc_id,
l2.zone_1,
it.user_id 
order by
--to_char(it.dstamp, 'DD-MON-YYYY'),
it.from_loc_id,
l2.zone_1,
it.user_id 
/
