SET FEEDBACK OFF           
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 1000
SET TRIMSPOOL ON



column M heading '' format a4 noprint
column D heading 'Date' format a12
column A heading 'SKU ID' format a25
column B heading '+Adjustment' format 99999999
column F heading '-Adjustment' format 99999999
column C heading 'Description' format A100
column XN format A100
column TG format A50


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


--ttitle  LEFT 'Units Adjusted from &&3 to &&4 - for client ID &&2 as at ' report_date skip 2

break on report

compute sum label 'Totals' of B F on report

--spool stdadjusted.csv

select 'Date,SKU ID,Description,Adjustment Up,Adjustment Down,Reason,Extra Notes,Tag ID' from DUAL;


select * from
(select
to_char(it.Dstamp, 'YYMM') M,
to_char(it.Dstamp, 'DD/MM/YY') D,
it.sku_id A,
sku.description C,
sum(it.update_qty) as B,
0 as F,
it.reason_id R,
it.extra_notes XN,
it.tag_id TG
from inventory_transaction it,sku
where it.Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and it.client_id='&&2'
and sku.client_id='&&2'
and it.code='Adjustment'
and it.sku_id=sku.sku_id
group by
to_char(it.Dstamp, 'YYMM'),
to_char(it.Dstamp, 'DD/MM/YY'),
it.sku_id,
it.reason_id,
sku.description,
it.extra_notes,
it.tag_id
)
where B>0
union 
select * from
(select
to_char(it.Dstamp, 'YYMM') M,
to_char(it.Dstamp, 'DD/MM/YY') D,
it.sku_id A,
sku.description C,
0 as B,
sum(it.update_qty) as F,
it.reason_id R,
it.extra_notes XN,
it.tag_id TG
from inventory_transaction it,sku
where it.Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and it.client_id='&&2'
and sku.client_id='&&2'
and it.code='Adjustment'
and it.sku_id=sku.sku_id
group by
to_char(it.Dstamp, 'YYMM'),
to_char(it.Dstamp, 'DD/MM/YY'),
it.sku_id,
it.reason_id,
sku.description,
it.extra_notes,
it.tag_id
)
where F<0
union
select * from
(select
to_char(it.Dstamp, 'YYMM') M,
to_char(it.Dstamp, 'DD/MM/YY') D,
it.sku_id A,
sku.description C,
sum(it.update_qty) as B,
0 as F,
it.reason_id R,
it.extra_notes XN,
it.tag_id TG
from inventory_transaction_archive it,sku
where it.Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and it.client_id='&&2'
and sku.client_id='&&2'
and it.code='Adjustment'
and it.sku_id=sku.sku_id
group by
to_char(it.Dstamp, 'YYMM'),
to_char(it.Dstamp, 'DD/MM/YY'),
it.sku_id,
it.reason_id,
sku.description,
it.extra_notes,
it.tag_id
)
where B>0
union 
select * from
(select
to_char(it.Dstamp, 'YYMM') M,
to_char(it.Dstamp, 'DD/MM/YY') D,
it.sku_id A,
sku.description C,
0 as B,
sum(it.update_qty) as F,
it.reason_id R,
it.extra_notes XN,
it.tag_id TG
from inventory_transaction_archive it,sku
where it.Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and it.client_id='&&2'
and sku.client_id='&&2'
and it.code='Adjustment'
and it.sku_id=sku.sku_id
group by
to_char(it.Dstamp, 'YYMM'),
to_char(it.Dstamp, 'DD/MM/YY'),
it.sku_id,
it.reason_id,
sku.description,
it.extra_notes,
it.tag_id
)
where F<0
/

--spool off
