SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 600


select 'SKU,Description,Colour,Size,user_def_type_5,User_def_type_1,User_def_type_2,User_def_type_3,User_def_type_4,User_def_type_6,User_def_num_1,User_def_num_2,User_def_num_3,User_def_num_4,User_def_note_1,User_def_note_2,User_def_type_7,User_def_type_8,product_group,creation_date,last_update_date'
from dual;


select
'''' ||
sku_id ||''','||
description ||','||
colour ||','||
Sku_size ||','||
user_def_type_5 ||','||
User_def_type_1 ||','||
User_def_type_2 ||','||
User_def_type_3 ||','||
User_def_type_4 ||','||
User_def_type_6 ||','||
User_def_num_1 ||','||
User_def_num_2 ||','||
User_def_num_3 ||','||
User_def_num_4 ||','||
User_def_note_1 ||','||
User_def_note_2 ||','||
User_def_type_7 ||','||
User_def_type_8 ||','||
product_group ||','||
to_char(creation_date,'DD/MM/YYYY') ||','||
to_char(last_update_date,'DD/MM/YYYY')
from sku
where client_id like 'V'
and (last_update_date>sysdate-1 or creation_date>sysdate-1)
order by creation_date
/

select 'End of Report' from DUAL;
