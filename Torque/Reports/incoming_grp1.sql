/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :     Incoming_pa.sql					      */
/*                                                                            */
/*     DESCRIPTION:     Printout of Pre Advice details to be used by w/house  */
/*                                                                            */
/*   DATE     BY   DESCRIPTION						      */
/*   ======== ==== ===========					              */
/*   04/11/04 BS   initial version     				  	      */
/*   15/09/06 MS   modified to include ean    				      */
/*   19/02/07 BK   modified to change text                                    */ 
/*   12/03/07 BK   modified to include SKU description                        */
/******************************************************************************/
SET FEEDBACK OFF
SET VERIFY OFF
SET TAB OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES



/* Column Headings and Formats */
COLUMN A heading "PO NO" FORMAT a14
COLUMN B heading "SKU" FORMAT a18
COLUMN BA heading "Group" FORMAT a10
COLUMN BB heading "Description" FORMAT a40
COLUMN BC heading "Product" FORMAT a14
COLUMN C heading "Line" FORMAT 9999
COLUMN D heading " Qty|Due" FORMAT 99999
COLUMN E heading " GRN|Qty" FORMAT 99999
COLUMN F heading "EAN" FORMAT a16
COLUMN G heading "GRN" FORMAT a12
COLUMN H heading "NOTES" FORMAT a20
COLUMN J heading "  " FORMAT a2


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


SET LINESIZE 117
SET WRAP OFF
SET PAGES 66
SET COLSEP ''
SET NEWPAGE 0

/* Top Title */
TTITLE LEFT 'TML GRN Report for PO &&2' RIGHT 'Date: ' report_date SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 1 - 

/* Set up page and line */


BREAK ON BA on BC on REPORT
column BA new_value null
column BC new_value null
compute sum label 'Totals' of D E on report


SELECT
	 U5 BA,
	 U3 BC,
	 PAL.SKU_ID B, 
	 DDD BB,
	 PAL.Line_ID C,
         nvl(PAL.Qty_Due,0) D,   
         nvl(PAL.Qty_Received,0) E,
	 '  ' J,
         EEE F 
--         PAH.Notes G,
--         PAH.user_def_note_2 H
FROM Pre_Advice_Header PAH, Pre_Advice_Line PAL, (select 
sku_id SSS,
description DDD,
user_def_type_5 U5,
user_def_type_3 U3,
ean EEE
from sku where client_id='&&3')
WHERE PAH.Pre_Advice_ID = PAL.Pre_Advice_ID
AND PAL.SKU_ID = SSS (+)
AND PAH.Pre_Advice_ID = '&&2'
and PAH.client_id = '&&3'
ORDER BY B,U3,U5;
--PAL.SKU_ID,S.user_def_type_3,S.user_def_type_5;

SET PAGES 0
Select 'End of Report' from DUAL;
