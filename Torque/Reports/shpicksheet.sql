SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 66
SET LINESIZE 120
SET TRIMSPOOL ON
SET HEADING OFF
SET NEWPAGE 0

--spool SHps.txt

column AA noprint
column R heading 'Order' format a16
column A heading 'SKU' format a20
column B heading ' ' format a4
column C heading 'Location' format a12
column D heading 'Pick Qty' format 99999
column D2 heading 'Stock' format 9999999999
column E heading 'Picked' format A12
column M noprint
column N noprint

TTITLE  CENTER "SAHARA PICK SHEETS for &&2" RIGHT "Page:" FORMAT 999 SQL.PNO skip 2 -
        LEFT "Order            SKU                  Pick Qty    Pick From    Location        Picked" skip 1 -
        LEFT "-----            ---                  --------    ---------    --------        ------" skip 2


break on row skip 1 on M skip page 

--compute sum label 'Total' of D D2 on M

select
oh.order_id M,
oh.order_id||'00000' N,
' TO:  '||
oh.name||',   '||
oh.address1||',   '||
oh.address2||',   '||
oh.postcode||',   TOTAL PICK QTY = '||
sum(mt.qty_to_move)  REC
from order_header oh,move_task mt
where oh.client_id='SH'
and (oh.order_id='&&2'
or oh.consignment='&&2')
and oh.order_id=mt.task_id
and oh.client_id=mt.client_id
group by
oh.order_id,
oh.name,
oh.address1,
oh.address2,
oh.postcode 
union
select
R M,
R||C N,
rpad(R,16,' ')||
rpad(A,25,' ')||
lpad(D,5,' ')||
lpad(D2,13,' ')||
lpad(C,12,' ')||
lpad(E,20,' ') REC
from (select
ol.order_id R,
ol.sku_id A,
sum(mt.qty_to_move) D,
QQQ D2,
'    ' B,
mt.from_loc_id C,
'____________' E
from move_task mt,order_header oh,order_line ol,(select
sku_id SSS,
location_id LLL,
sum(qty_on_hand) QQQ
from inventory
where client_id='SH'
group by
sku_id,
location_id)
where mt.client_id='SH'
and oh.order_id=mt.task_id
and oh.client_id=mt.client_id
and (oh.order_id='&&2'
or oh.consignment='&&2')
and ol.client_id=mt.client_id
and ol.order_id=mt.task_id
and ol.sku_id=mt.sku_id
and ol.line_id=mt.line_id
and ol.sku_id=SSS
and LLL=mt.from_loc_id (+)
group by
ol.order_id,
ol.sku_id,
QQQ,
mt.from_loc_id
order by C)
order by M,N
/

--spool off
