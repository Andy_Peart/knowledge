SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240



--select 'Multiple SKU Extract - for client ID TR as at ' 
--        ||  to_char(sysdate,'dd/mm/yy  hh24:mi:ss') from dual;

select 'SKU,Location,Condition,Total Qty,Qty Allocated,Receive Date,Receive Time,Description,Size,Colour'
from dual;


select
'''' ||
iv.sku_id ||''','||
iv.location_id ||','||
iv.condition_id ||','||
nvl(iv.qty_on_hand,0) ||','||
nvl(iv.qty_allocated,0) ||','||
to_char(Receipt_Dstamp, 'DD/MM/YYYY') ||','||
to_char(Receipt_Dstamp, 'HH:MI:SS') ||','||
sku.description ||','||
sku.sku_size ||','||
sku.colour
from inventory iv,sku
where iv.client_id='TR'
and iv.sku_id in ('&&2','&&3','&&4','&&5','&&6','&&7','&&8','&&9')
and sku.client_id='TR'
and iv.sku_id=sku.sku_id
order by iv.sku_id
/
