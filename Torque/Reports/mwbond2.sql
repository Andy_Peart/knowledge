SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
with t1 as (
	select client_id    as "Client"
	, sku_id            as "Sku"
	, tag_id            as "Tag"
    , user_def_type_3   as "User def 3"
    , user_def_type_5   as "User def 5"
    , libsku.getuserlangskudesc(client_id,sku_id) "Description"
	, sum(update_qty)   as "Adj"
	from inventory_transaction
	where client_id = 'MOUNTAIN'
	and dstamp between to_date ('&&2') and to_date('&&3')+1
	and code in ('Adjustment','Stock Check')
	and from_loc_id <> 'SUSPENSE'
    and nvl(user_def_type_5,'X') = 'BOND'
	group by client_id	, sku_id	, tag_id, user_def_type_3, user_def_type_5
)
select 'Client,Sku,Description,User Def 3,User Def 5,Positive Adjustment,Negative Adjustment' from dual
union all
select "Client" || ',' || "Sku" || ',' || "Description" || ',' || "User def 3" || ',' || "User def 5" || ',' || "+/-" || "Adj" from
(
    select t1."Client"
    , '''' || t1."Sku" || '''' as "Sku"
    , t1."Description"
    , t1."User def 3"
    , t1."User def 5"
    , case when t1."Adj" < 0 then ',' else '' end as "+/-"
    , t1."Adj"
    from t1
    where t1."Adj" <> 0
    order by t1."Sku"
)
/
--spool off