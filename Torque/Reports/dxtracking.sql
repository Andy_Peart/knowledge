SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 120
set newpage 0
set verify off
set colsep ','
set trimspool on



column A heading 'Order' format A20
column B heading 'Name' format A20
column C heading 'Tracking' format A20

--break on report

--compute sum label 'Total' of B D on report

--spool DX.csv

select 'Daxbourne Orders Shipped on '||trunc(sysdate) from DUAL;
select 'Order ID,Customer,Tracking ID' from DUAL;

select distinct 
it.reference_id||','||
oh.name||','||
it.pallet_id
from inventory_transaction it,order_header oh
where it.client_id='DX' 
and it.code='Shipment'
and it.dstamp>sysdate-1
and substr(it.pallet_id,1,1) between 'A' and 'Z'
and it.reference_id=oh.order_id
and oh.client_id='DX'
order by 1
/

--spool off
