SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 52


--spool PromSC.csv
 

select DECODE('&&4','YES','Location,SKU,Qty,Customer','Location,SKU,Qty') from DUAL;

select
i.location_id||','||
i.sku_id||','||
sum(i.qty_on_hand)||','||
DECODE('&&4','YES',sku.user_def_type_2,' ')
from inventory i,sku
where i.client_id='PR'
and i.site_id='PROM'
and i.zone_1<>'P'
and i.location_id between '&&2' and '&&3'
and i.sku_id=sku.sku_id
and sku.client_id='PR'
group by
i.location_id,
i.sku_id,
sku.user_def_type_2
--i.user_def_note_1
order by
i.location_id,
i.sku_id
/


--spool off
