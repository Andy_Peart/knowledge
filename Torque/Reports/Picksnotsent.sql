SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

select 'Outstanding Pick Orders NOT despatched - for client ID &&2 - as at  ' 
    || to_char(sysdate,'dd/mm/yy  hh24:mi:ss') from dual;


select 'Status,Order Date,Date at TORQUE,Priority,Cust Code,Cust Name,Order No,Qty on Order,Qty Outstanding'
from dual;


select
oh.status ||','||
to_char(oh.order_date, 'DD/MM/YY') ||','||
to_char(oh.creation_date, 'DD/MM/YY') ||','||
oh.priority ||','||
oh.customer_id ||','||
oh.contact ||','||
oh.order_id ||','||
nvl(sum(nvl(ol.qty_ordered,0)),0) ||','||
nvl(sum(nvl(ol.qty_ordered,0)-nvl(ol.qty_shipped,0)),0)
from order_header oh,order_line ol
where oh.client_id='&&2'  
and (status='Released' or status='In Progress' or status='Allocated' or status='Hold')
and oh.order_id=ol.order_id
and status='Released'
group by
oh.status,
oh.order_date,
oh.creation_date,
oh.priority,
oh.customer_id,
oh.contact,
oh.order_id
order by
oh.status,
oh.order_date,
oh.creation_date
/
select
'Released,,,,,,Totals' as A,
nvl(sum(nvl(ol.qty_ordered,0)),0) ||','||
nvl(sum(nvl(ol.qty_ordered,0)-nvl(ol.qty_shipped,0)),0)
from order_header oh,order_line ol
where oh.client_id='&&2'  
and (status='Released' or status='In Progress' or status='Allocated' or status='Hold')
and oh.order_id=ol.order_id
and status='Released'
/
select
oh.status ||','||
to_char(oh.order_date, 'DD/MM/YY') ||','||
to_char(oh.creation_date, 'DD/MM/YY') ||','||
oh.priority ||','||
oh.customer_id ||','||
oh.contact ||','||
oh.order_id ||','||
nvl(sum(nvl(ol.qty_ordered,0)),0) ||','||
nvl(sum(nvl(ol.qty_ordered,0)-nvl(ol.qty_shipped,0)),0)
from order_header oh,order_line ol
where oh.client_id='&&2'  
and (status='Released' or status='In Progress' or status='Allocated' or status='Hold')
and oh.order_id=ol.order_id
and status='Allocated'
group by
oh.status,
oh.order_date,
oh.creation_date,
oh.priority,
oh.customer_id,
oh.contact,
oh.order_id
order by
oh.status,
oh.order_date,
oh.creation_date
/
select
'Allocated,,,,,,Totals' as A,
nvl(sum(nvl(ol.qty_ordered,0)),0) ||','||
nvl(sum(nvl(ol.qty_ordered,0)-nvl(ol.qty_shipped,0)),0)
from order_header oh,order_line ol
where oh.client_id='&&2'  
and (status='Released' or status='In Progress' or status='Allocated' or status='Hold')
and oh.order_id=ol.order_id
and status='Allocated'
/
select
oh.status ||','||
to_char(oh.order_date, 'DD/MM/YY') ||','||
to_char(oh.creation_date, 'DD/MM/YY') ||','||
oh.priority ||','||
oh.customer_id ||','||
oh.contact ||','||
oh.order_id ||','||
nvl(sum(nvl(ol.qty_ordered,0)),0) ||','||
nvl(sum(nvl(ol.qty_ordered,0)-nvl(ol.qty_shipped,0)),0)
from order_header oh,order_line ol
where oh.client_id='&&2'  
and (status='Released' or status='In Progress' or status='Allocated' or status='Hold')
and oh.order_id=ol.order_id
and status='In Progress'
and ol.qty_tasked=0
group by
oh.status,
oh.order_date,
oh.creation_date,
oh.priority,
oh.customer_id,
oh.contact,
oh.order_id
order by
oh.status,
oh.order_date,
oh.creation_date
/
select
'In Progress,(Tasks=0),,,,,Totals' as A,
nvl(sum(nvl(ol.qty_ordered,0)),0) ||','||
nvl(sum(nvl(ol.qty_ordered,0)-nvl(ol.qty_shipped,0)),0)
from order_header oh,order_line ol
where oh.client_id='&&2'  
and (status='Released' or status='In Progress' or status='Allocated' or status='Hold')
and oh.order_id=ol.order_id
and status='In Progress'
and ol.qty_tasked=0
/
select
oh.status ||','||
to_char(oh.order_date, 'DD/MM/YY') ||','||
to_char(oh.creation_date, 'DD/MM/YY') ||','||
oh.priority ||','||
oh.customer_id ||','||
oh.contact ||','||
oh.order_id ||','||
nvl(sum(nvl(ol.qty_ordered,0)),0) ||','||
nvl(sum(nvl(ol.qty_ordered,0)-nvl(ol.qty_shipped,0)),0)
from order_header oh,order_line ol
where oh.client_id='&&2'  
and (status='Released' or status='In Progress' or status='Allocated' or status='Hold')
and oh.order_id=ol.order_id
and status='In Progress'
and ol.qty_tasked>0
group by
oh.status,
oh.order_date,
oh.creation_date,
oh.priority,
oh.customer_id,
oh.contact,
oh.order_id
order by
oh.status,
oh.order_date,
oh.creation_date
/
select
'In Progress,(Tasks>0),,,,,Totals' as A,
nvl(sum(nvl(ol.qty_ordered,0)),0) ||','||
nvl(sum(nvl(ol.qty_ordered,0)-nvl(ol.qty_shipped,0)),0)
from order_header oh,order_line ol
where oh.client_id='&&2'  
and (status='Released' or status='In Progress' or status='Allocated' or status='Hold')
and oh.order_id=ol.order_id
and status='In Progress'
and ol.qty_tasked>0
/
select
oh.status ||','||
to_char(oh.order_date, 'DD/MM/YY') ||','||
to_char(oh.creation_date, 'DD/MM/YY') ||','||
oh.priority ||','||
oh.customer_id ||','||
oh.contact ||','||
oh.order_id ||','||
nvl(sum(nvl(ol.qty_ordered,0)),0) ||','||
nvl(sum(nvl(ol.qty_ordered,0)-nvl(ol.qty_shipped,0)),0)
from order_header oh,order_line ol
where oh.client_id='&&2'  
and (status='Released' or status='In Progress' or status='Allocated' or status='Hold')
and oh.order_id=ol.order_id
and status='Hold'
group by
oh.status,
oh.order_date,
oh.creation_date,
oh.priority,
oh.customer_id,
oh.contact,
oh.order_id
order by
oh.status,
oh.order_date,
oh.creation_date
/
select
'Hold,,,,,,Totals' as A,
nvl(sum(nvl(ol.qty_ordered,0)),0) ||','||
nvl(sum(nvl(ol.qty_ordered,0)-nvl(ol.qty_shipped,0)),0)
from order_header oh,order_line ol
where oh.client_id='&&2'  
and (status='Released' or status='In Progress' or status='Allocated' or status='Hold')
and oh.order_id=ol.order_id
and status='Hold'
/
select
',,,,,,Grand Totals' as A,
nvl(sum(nvl(ol.qty_ordered,0)),0) ||','||
nvl(sum(nvl(ol.qty_ordered,0)-nvl(ol.qty_shipped,0)),0)
from order_header oh,order_line ol
where oh.client_id='&&2'  
and (status='Released' or status='In Progress' or status='Allocated' or status='Hold')
and oh.order_id=ol.order_id
/
