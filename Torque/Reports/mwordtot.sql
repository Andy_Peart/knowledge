set pagesize 2000
set linesize 2000
set verify off
set feedback off

clear columns
clear breaks
clear computes

ttitle "Mountain Warehouse - Order Total - Order '&&2'" skip 3

column A heading 'Total Qty Picked' format 999999

select nvl(sum (itl.UPDATE_QTY),0) A
from order_header oh, inventory_transaction itl
where oh.client_id = 'MOUNTAIN'
and oh.order_id = ('&&2')
and oh.order_id = itl.reference_id
and itl.code = 'Pick'
/
