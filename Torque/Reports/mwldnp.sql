SET FEEDBACK OFF             
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 300
SET TRIMSPOOL ON


column A format a18
column B format a8
column C format 999999
column D format 999999
column E format 999999
column F format a32
column G format a10
column H format a18
column J format a8
column K format a10
column L format a10

--set termout off
--spool mwldnp.csv

select 'Sku Code,Condition,Qty,Life Reserve,Amazon Reserve,Retail,Description,T2T Code,Colour,Size,Last Rect,Last Pick'
from dual;

Select
''''||PA||'''' A,
CA B,
QQ C,
nvl(sku.user_def_num_4,0) D,
sku.user_def_type_2 D2,
--QQ-nvl(sku.user_def_num_3,0) E,
DECODE(SIGN(QQ-nvl(sku.user_def_num_3,0)),-1,0,QQ-nvl(sku.user_def_num_3,0)) E,
sku.description F,
sku.user_def_type_1 G,
sku.colour H,
sku.sku_size J,
to_char(CB, 'DD/MM/YY') K,
DECODE(to_char(PB, 'DD/MM/YY'),'01/01/00',' ',to_char(PB, 'DD/MM/YY')) L,
' '
from
(select
PA,
CA,
CB,
QQ,
nvl(BB,'01-jan-00') PB
from
(select sku_id PA,
condition_id CA,
max(trunc(receipt_dstamp)) CB,
sum(qty_on_hand) QQ
from inventory
where client_id = 'MOUNTAIN'
and condition_id like '&&3%'
group by sku_id,condition_id),
(select i.Sku_id PP,
max (i.dstamp) BB
from inventory_transaction i
where i.client_id = 'MOUNTAIN'
and i.code = 'Pick'
group by i.sku_id)
where PA=PP (+)),
sku
where sku.client_id = 'MOUNTAIN'
and PA=sku.sku_id
and PB <sysdate - '&&2'
and CB <sysdate - '&&2'
order by CA,PA
/


--spool off
--set termout on
