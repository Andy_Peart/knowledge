SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON

--spool mwbulk.csv

select 'Sku Id,Description,All Stock,In BULK,Allocated,Percentage,LPG,Location(s)' from Dual
union all
select * from (select
''''||A||''','||
BD||','||
B||','||
C||','||
B2||','||
PC||','||
LPG||','||
LL
from (select
BULKSKU A,
BD,
LL,
LPG,
THELOT B,
ALLOC B2,
nvl(BULK,0) C,
to_char(nvl(BULK,0)*100/THELOT,'990.00') PC
from (select
BULKSKU,
BD,
LPG,
LISTAGG(LC||'('||LD||')', ',') WITHIN GROUP (ORDER BY LD) AS LL,
sum(QTY) BULK
from (select
iv.sku_id BULKSKU,
iv.description BD,
iv.location_id LC,
trunc(iv.receipt_dstamp) LD,
sku.user_def_type_8 LPG,
--||'('||trunc(iv.receipt_dstamp)||')' LD,
--iv.location_id||'('||to_char(iv.receipt_dstamp,'DD/MM/YY HH24:MI')||')' LD,
sum(qty_on_hand) QTY
from inventory iv,location l,sku
where iv.client_id='MOUNTAIN'
and iv.location_id=l.location_id
and iv.site_id=l.site_id
and l.work_zone='BULK'
and sku.client_id = 'MOUNTAIN'
and sku.sku_id=iv.sku_id
group by
iv.sku_id,iv.description,iv.location_id,trunc(iv.receipt_dstamp),sku.user_def_type_8)
group by
BULKSKU,
BD,
LPG)
,(select
sku_id THELOTSKU,
sum(qty_on_hand) THELOT,
sum(qty_allocated) ALLOC
from inventory
where client_id='MOUNTAIN'
group by
sku_id)
where BULKSKU=THELOTSKU
and THELOT>0)
where PC>=&&2
order by 1)
/

--spool off

