SET FEEDBACK OFF
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','
SET UND OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 200
SET TRIMSPOOL ON



set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

--spool shlist.csv

Select 
pl.sku_id||','||
sku.description||','||
sku.upc||','||
sku.user_def_type_8||','||
sku.sku_size||','||
sku.user_def_type_1||','||
sku.user_def_num_3||','||
pl.qty_due
from pre_advice_header ph,pre_advice_line pl,sku
where ph.client_id='SH'
and ph.pre_advice_id='&&2'
and ph.pre_advice_id=pl.pre_advice_id
and pl.client_id='SH'
and pl.sku_id=sku.sku_id
and sku.client_id='SH'
order by
pl.sku_id
/

--spool off
