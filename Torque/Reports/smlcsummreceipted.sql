SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240
SET TRIMSPOOL ON

column C heading 'Client ID' format a10
column D heading 'Date' format a16
column A heading 'Reference' format a16
column B heading 'Qty Received' format 99999999
column M format a4 noprint

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON




break on C on report

compute sum label 'Client Totals' of B on C
compute sum label 'Totals' of B on report

--spool smlcsumm.csv

select 'Total Units Received from &&2 to &&3 - as at '||to_char(SYSDATE, 'DD/MM/YY  HH:MI') from DUAL;
select 'Client,Date,Reference,Qty Received' from DUAL;

select
it.client_id C,
trunc(it.Dstamp) D,
it.reference_id A,
sum(it.update_qty) as B
from inventory_transaction it
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id in ('IF','S2','GG','VF','EX')
and it.station_id not like 'Auto%'
and it.code like 'Receipt%'
group by
it.client_id,
trunc(it.Dstamp),
it.reference_id
union
select
it.client_id C,
trunc(it.Dstamp) D,
it.reference_id A,
sum(it.update_qty) as B
from inventory_transaction_archive it
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id in ('IF','S2','GG','VF','EX')
and it.station_id not like 'Auto%'
and it.code like 'Receipt%'
group by
it.client_id,
trunc(it.Dstamp),
it.reference_id
order by C,D,A
/

--spool off
