set feedback off
set pagesize 66
set linesize 130
set verify off


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

 

column A heading 'Location' format A10
column B heading 'SKU' format A20
column C heading 'Qty| on Hand' format 999999
column D heading 'Qty|Allocated' format 999999
column E heading 'Description' format A40
column F heading 'Size' format A6
column G heading 'Colour' format A10
column H heading 'Condition' format A9
column I heading 'Container' format A9


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

set und '-'
set heading on
set newpage 0

ttitle  LEFT 'Inventory Query by Product Code: '&&3' - for client ID &&2 as at ' report_date RIGHT 'Page:' FORMAT 999 SQL.PNO skip 2



break on report on row skip 2
compute sum label '   Totals' of C D on report

select
i.location_id A,
i.sku_id B,
i.qty_on_hand C,
i.qty_allocated D,
Sku.description E,
Sku.sku_size F,
sku.colour G,
i.condition_id H,
i.container_id I
from inventory i,sku
where i.client_id like '&&2'
and sku.client_id like '&&2'
and i.sku_id=sku.sku_id
and sku.user_def_type_3='&&3'
order by i.location_id,i.sku_id
/
