/********************************************************************************************/
/*                                                                                          */
/*                            Elite                                                         */
/*                                                                                          */
/*     FILE NAME  :   tautoemail.sql                                                        */
/*                                                                                          */
/*     DESCRIPTION:    Datastream for TML Mailorder Automatic Email to Customer             */
/*                                                                                          */
/*   DATE     BY   PROJ       ID         DESCRIPTION                                        */
/*   ======== ==== ======     ========   =============                                      */
/*   15/11/07 LH   TMR                   TML Mailorder Automatic Email to Customer          */
/*                                                                                          */
/********************************************************************************************/

/* Input Parameters are as follows */
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 0
SET VERIFY OFF
SET FEEDBACK OFF
SET TAB OFF

 -- Set the line length to the maximum possible, but trim 
-- trailing spaces for both screen output and spool files
SET LINESIZE 32767
SET WRAP ON
SET TRIMOUT ON
SET TRIMSPOOL ON

/* Define the format of the output dates */
DEFINE	OutputDateFormat = 'DD-MON-YYYY'
COLUMN	sorter	NOPRINT

SELECT Distinct '0' sorter,
       'DSTREAM_T_AUTOMAIL_HDR'
from dual
union all
SELECT distinct '1' sorter,
'DSTREAM_T_AUTOMAIL_DETAIL_HDR'              ||'|'||
rtrim (oh.order_id)  ||'|'||
rtrim (oh.name)                    ||'|'||
rtrim (oh.contact)                        ||'|'||
rtrim (address1)                          ||'|'||
rtrim (address2)                          ||'|'||
rtrim (town)                              ||'|'||
rtrim (county)                            ||'|'||
rtrim (postcode)                          ||'|'||
rtrim (country)                           ||'|'||
rtrim (oh.contact_email)                  ||'|'||
rtrim (inv_contact)                       ||'|'||
rtrim (inv_name)                          ||'|'||
rtrim (inv_address1)                      ||'|'||
rtrim (inv_address2)                      ||'|'||
rtrim (inv_town)                          ||'|'||
rtrim (inv_county)                        ||'|'||
rtrim (inv_postcode)                      ||'|'||
rtrim (inv_country)                       ||'|'||
rtrim (inv_contact_email)                  ||'|'||                          
rtrim (itl.container_id)                  ||'|'||
rtrim (trunc(itl.dstamp))                 ||'|'||
rtrim (decode(substr(itl.container_id,1,2), 'ZJ', 'http://www.royalmail.com/portal/rm/track',
                                     '55', 'http://www.ocsworldwide.co.uk/',
                                     'DW', 'http://www.royalmail.com/portal/rm/track',
                                     'DD', 'http://www.royalmail.com/portal/rm/track',
                                     'DL', 'http://www.royalmail.com/portal/rm/track',
                                     'DH', 'http://www.royalmail.com/portal/rm/track',
                                     ''))   ||'|'||
rtrim (decode(substr(itl.container_id,1,2), 'ZJ', 'Royal Mail Special Delivery - Next Day',
                                     '55', 'Overseas Courier Service Worldwide',
                                     'DL', 'Royal Mail 1st Class Recorded Delivery',
                                     ''))  ||'|'||
rtrim (decode(substr(itl.container_id,1,2), 'ZJ', 'Royal Mail will be delivering it and they will require a signature. If no one is at the address a card will be left detailing how you can arrange to get your parcel.',
                                     '55', 'OCS Worldwide will be delivering it and they will require a signature. If no one is at the address a card will be left detailing how you can arrange to get your parcel.',
                                     'DW', 'Royal Mail will be delivering it and they will require a signature. If no one is at the address a card will be left detailing how you can arrange to get your parcel.',
                                     'DD', 'Royal Mail will be delivering it and they will require a signature. If no one is at the address a card will be left detailing how you can arrange to get your parcel.',
                                     'DL', 'Royal Mail will be delivering it and they will require a signature. If no one is at the address a card will be left detailing how you can arrange to get your parcel.',
                                     'DH', 'Royal Mail will be delivering it and they will require a signature. If no one is at the address a card will be left detailing how you can arrange to get your parcel.',
                                     ''))
from order_header oh, inventory_transaction itl
where (oh.client_id = 'T'
and oh.order_id = '&&2'
and itl.code = 'Shipment'
and trunc(itl.dstamp) = trunc(sysdate)
and itl.reference_id = oh.order_id
and oh.contact_email is not null
and itl.container_id like ('ZJ%'))
or (oh.client_id = 'T'
and oh.order_id = '&&2'
and itl.code = 'Shipment'
and trunc(itl.dstamp) = trunc(sysdate)
and itl.reference_id = oh.order_id
and oh.contact_email is not null
and itl.container_id like ('D%'))
order by 1;

exit;
