SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET COLSEP ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 200
SET TRIMSPOOL ON
SET HEADING OFF 

column Z heading 'Status' format A12
column A heading 'pre_advice' format A20
column B heading 'Line' format 999999
column C heading 'SKU' format A18
column D heading 'Qty1' format 9999999
column E heading 'Qty2' format 9999999
column F heading 'Size' format A12


--spool sbpacomplete.csv


select 'Pre Advice ID,Date,SKU ID,Qty Due,Qty Received,Variance' from DUAL;


--break on report
--compute sum label 'Total' of D on report




select
A||','||
B||','||
C||','||
E||','||
D||','||
F
from (select
ph.pre_advice_id A,
trunc(ph.finish_dstamp) B,
pl.sku_id C,
sum(pl.qty_due) E,
sum(nvl(pl.qty_received,0)) D,
sum(nvl(pl.qty_received,0)-pl.qty_due) F
from pre_advice_header ph, pre_advice_line pl
where ph.client_id='SB'
and ph.pre_advice_id='&&2'
--and ph.status='Complete'
--and ph.finish_Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and pl.client_id='SB'
and ph.pre_advice_id=pl.pre_advice_id
group by
ph.pre_advice_id,
trunc(ph.finish_dstamp),
pl.sku_id)
order by 1
/

--spool off
