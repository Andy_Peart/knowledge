SET FEEDBACK OFF
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','
SET UND OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 100
SET TRIMSPOOL ON



column A format A10
column B format A12
column C format A20
column D format 99999
column E format A20
column F format A20
column G format A36
column H format A12
column J format A40
column K format 99999
column L format 99999
column N format 99999


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



--break on A dup skip page on C dup skip 1

--compute sum label 'Order Total'  of K L N on C


--SET FEEDBACK ON

update pre_advice_header
set uploaded='B'
where client_id = 'DX'
and uploaded = 'N'
/

commit;


--SET FEEDBACK OFF

--spool dxnewrep4.csv

select 'Pre_Advices Received - '||to_char(SYSDATE, 'DD/MM/YY  HH24:MI:SS') from DUAL;

select 'Pre Advice,Creation Date,Lines' from DUAL;

Select 
oh.pre_advice_id||','||
to_char(oh.creation_date,'DD/MM/YY')||','||
nvl(oh.num_lines,0)
from pre_advice_header oh
where oh.client_id='DX'
and oh.uploaded='B'
order by
oh.pre_advice_id
/

--spool off

--SET FEEDBACK ON

update pre_advice_header
set uploaded='R'
where client_id = 'DX'
and uploaded = 'B'
/

--rollback;
commit;
