SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','
SET UND OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 320


Select 'Key,Code,Client_ID,Supplier,Reference,Txn Date,Txn Time,User ID,SKU ID,Description,Season,Style,Qual,Size,Update Qty' from DUAL;

column A format 999999999
column B format A10
column C format A10
column D format A10
column E format A12
column F format A12
column G format A12
column H format A12
column I format A18
column J format A40
column K format A10
column L format A10
column M format A14
column N format A8
column P format 99999999



set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

 

Select
it.Key A,
it.Code B,
it.Client_ID C,
it.Supplier_ID D,
it.Reference_ID E,
to_char(it.Dstamp,'DD/MM/YY') F,
to_char(it.Dstamp,'HH:MI:SS') G,
it.User_ID H,
it.SKU_ID I,
sku.Description J,
sku.User_def_type_2 K,
sku.User_def_type_1 L,
sku.User_def_type_4 M,
sku.sku_Size N,
it.Update_Qty P
from inventory_transaction it,sku
where trunc(Dstamp)>sysdate-8
--and it.reason_id='RECRV'
and it.client_id='T'
and it.code like 'Receipt Rev%'
--and it.code='Adjustment'
and sku.client_id='T'
and it.sku_id=sku.sku_id
order by
Key
/

