SET FEEDBACK OFF                
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 400
SET TRIMSPOOL ON

select 'SUPPLIER_ID,ITEM,QUANTITY,1,2,DATE_DUE,3,4' from DUAL;

select
'9198'||','
||i.sku_id||','
||sum(i.qty_on_hand)||','
||','
||','
||','
||','
||','
from inventory i
where i.client_id = 'TP'
and i.location_id<>'TP1'
group by i.sku_id;
/



--spool off

