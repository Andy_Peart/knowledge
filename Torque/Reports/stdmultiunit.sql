SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 3000
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Sku,Diff Locations,Location/s' from dual
union all
select "Sku" || ',' || "sCount" || ',' || "Location" from
(
select "Sku", "sCount", listagg("Zone" || ' - ' || "Location" || '(' || "Qty" || ') ' || '(' || "lCount" || ')', ', ')within group(order by "Location") as "Location"
from(
select case when i.client_id = 'TH' then s.ean else i.sku_id end as "Sku"
, i.location_id as "Location"
, l.work_zone   as "Zone"
, count(i.location_id)over(partition by i.sku_id) as "sCount"
, count(distinct i.sku_id)over(partition by i.location_id) as "lCount"
, sum(i.qty_on_hand)    as "Qty"
from inventory i
inner join location l on l.site_id = i.site_id and l.location_id = i.location_id
inner join sku s on s.client_id = i.client_id and s.sku_id = i.sku_id
where (i.client_id = '&&2' and i.client_id = 'TH' and i.zone_1 in ('THRETS','THREAD') and i.qty_on_hand <= 3)
or
(i.client_id = replace('&&2','TH',''))
and l.loc_type in ('Tag-LIFO','Tag-FIFO')
group by i.client_id
, i.sku_id
, s.ean
, i.location_id
, l.work_zone
) where "sCount" > 1
group by "Sku"
, "sCount"
order by 1
)
/
--spool off