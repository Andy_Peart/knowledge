SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON


column A heading 'Type' format a18
column A1 heading 'Name' format a12
column A2 heading 'Reference' format a14
column A3 heading 'SKU' format a14
column A4 heading 'Date' format a14
column B heading 'Units' format 999999
column K heading 'Units' format 999999
column KK heading 'Stock' format 999999
column B1 heading 'Rate' format A6
column C heading 'Value' format 999990.99

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON




break on A2 dup skip 1 on report


compute sum label 'Sub Total' of B C K on A2
--compute sum label 'Summary' of B C on A1
--compute sum label 'Totals' of B C on A
compute sum label 'Overall' of B C K on report

--spool berwinnewaugust.csv

select 'Type,Name,Reference,PORD,Date Shipped,Units Shipped,Value' from DUAL;


select
TIT A,
'Chinese' A1,
R1 A2,
U1 A3,
D1 A4,
--sum(BBB) K,
--nvl(sum(BBB2),0) KK,
Q1 B,
Q1*RT C
from (select
it.reference_id R1,
UPPER(substr(ol.user_def_note_2,1,12)) U1,
to_char(it.Dstamp,'dd/mm/yy') D1,
--it.sku_id S1,
sum(it.update_qty*DECODE(it.code,'Shipment',1,-1)) Q1,
DECODE(JC,'P',0.20,0.05) RT,
DECODE(JC,'P','Shipments (Part)','Shipments') TIT
from inventory_transaction it,order_line ol,(select
distinct
substr(job_id,1,9) JA,
UPPER(description) JB,
job_unit JC
from jobs
where client_id='BW')
where it.client_id='BW'
and to_char(it.Dstamp,'mm/yyyy')=to_char(sysdate-12,'mm/yyyy')
and it.code in ('Shipment','UnShipment')
and it.reference_id=ol.order_id
and ol.client_id='BW'
and it.line_id=ol.line_id
and it.sku_id=ol.sku_id
and substr(replace(UPPER(ol.user_def_note_2),'D'),1,9)=JA (+)
and ol.order_id=JB (+)
--and ol.order_id='EL141712'
group by
it.reference_id,
UPPER(substr(ol.user_def_note_2,1,12)),
to_char(it.Dstamp,'dd/mm/yy'),
DECODE(JC,'P',0.20,0.05),
DECODE(JC,'P','Shipments (Part)','Shipments'))
order by A2,A3
/


--spool off
