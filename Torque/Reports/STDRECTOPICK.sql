SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON

SELECT 'Pre-Advice,Sku,Receipt Date,First Picked Date,Days Static' from dual
Union All 
select A ||','|| B ||','|| C ||','|| D ||','|| E from (
SELECT IT.REFERENCE_ID 					as "A"
, IT.SKU_ID 							as "B"
, TO_CHAR(IT.DSTAMP, 'DD-MON-YYYY') 	as "C"
, x."First Picked" 						as "D"
, x."First Picked" - trunc(it.dstamp) 	as "E"
FROM INVENTORY_TRANSACTION IT
left join 
(
     SELECT MIN(trunc(DSTAMP)) AS "First Picked"
    , TAG_ID
    , sku_id
    FROM INVENTORY_TRANSACTION 
    WHERE CODE = 'Pick'
    GROUP BY  TAG_ID
    , sku_id
) x on x.tag_id = it.tag_id and x.sku_id = IT.SKU_ID
WHERE IT.CLIENT_ID = '&&2'
AND IT.Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')
AND IT.CODE = 'Receipt')
Union All 
select F ||','|| G ||','|| H ||','|| I ||','|| J from (
SELECT ITA.REFERENCE_ID 				as "F"
, ITA.SKU_ID 							as "G"
, TO_CHAR(ITA.DSTAMP, 'DD-MON-YYYY') 	as "H"
, x."First Picked" 						as "I"
, x."First Picked" - trunc(ITA.dstamp) 	as "J"
FROM INVENTORY_TRANSACTION_ARCHIVE ITA
left join 
(
     SELECT MIN(trunc(DSTAMP)) AS "First Picked"
    , TAG_ID
    , sku_id
    FROM INVENTORY_TRANSACTION_ARCHIVE
    WHERE CODE = 'Pick'
    GROUP BY  TAG_ID
    , sku_id
) x on x.tag_id = ITA.tag_id and x.sku_id = ITA.SKU_ID
WHERE ITA.CLIENT_ID = '&&2'
AND ITA.Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')
AND ITA.CODE = 'Receipt'
)
/