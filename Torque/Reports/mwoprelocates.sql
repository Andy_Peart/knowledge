SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON

break on report
compute sum of QQ CC on report

--spool mwoprelocates.csv

select 'Relocates from &&2 to &&3' from DUAL;
select 'Pre Advice,User Id,Units,Cartons' from Dual;

select
ph.pre_advice_id A,
ix.user_id B,
sum(ix.update_qty) QQ,
nvl(ph.user_def_num_3,0) CC
from inventory_transaction ix, inventory_transaction it,pre_advice_header ph
where ix.client_id='MOUNTAIN'
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and ix.code='Relocate'
and ix.from_loc_id like 'Z%'
and it.client_id='MOUNTAIN'
and it.code='Receipt'
and ix.tag_id=it.tag_id
and ph.client_id='MOUNTAIN'
and ph.pre_advice_id like 'MWF%'
and it.reference_id=ph.pre_advice_id
group by
ph.pre_advice_id,
ix.user_id,
nvl(ph.user_def_num_3,0)
order by 1,2
/

--spool off
