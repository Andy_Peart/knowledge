SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

--spool stdlockedstock.csv

select 'Locked Stock Report as at ' 
        ||  to_char(sysdate,'dd/mm/yy  hh24:mi:ss') from dual;

select 'Client Id,SKU Code,Description,Qty,Location,Lock Date'
from dual;

select
iv.client_id ||','''||
iv.sku_id ||''','||
iv.description ||','||
iv.qty_on_hand ||','||
iv.location_id ||','||
LD
from inventory iv
,(select
SK,
max(LD) LD
from (select
max(trunc(Dstamp)) LD,
sku_id SK
from inventory_transaction
where client_id like '&&2%'
and code='Inv Lock'
group by sku_id
union
select
max(trunc(Dstamp)) LD,
sku_id SK
from inventory_transaction_archive
where client_id like '&&2%'
and code='Inv Lock'
group by sku_id)
group by SK)
where client_id like '&&2%'
and iv.lock_status='Locked'
and (iv.location_id<>'SUSPENSE' or (iv.location_id='SUSPENSE' and '&&3'='YES'))
and iv.sku_id=SK (+)
order by iv.client_id,iv.sku_id
/

--spool off
