SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ' '



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON

column A format A20

break on report

compute sum label 'Total' of B on report

select 'Orders as at '||to_char(sysdate,'DD-MON-YYYY HH24:MI:SS') from DUAL;

select ' ' from DUAL;


select
--trunc(sysdate),
status A,
count(*) B
from order_header
where client_id='SB'
and status in ('Released','Allocated','In Progress')
and (order_id like 'C%' or order_id like 'E%')
group by
status
/

select ' ' from DUAL;
select ' ' from DUAL;

select
'Latest C Shipment' A,
it.reference_id,
to_char(Dstamp,'DD-MON-YYYY  HH24:MI')
from inventory_transaction it,(select
max(Dstamp) LT
from inventory_transaction it
where it.client_id='SB'
--and trunc(it.Dstamp)=trunc(sysdate)
and reference_id like 'C%'
and it.code='Shipment')
where it.client_id='SB'
and reference_id like 'C%' 
and it.code='Shipment'
and Dstamp=LT
/

select ' ' from DUAL;



select
'Latest C Pick ' A,
it.reference_id,
to_char(Dstamp,'DD-MON-YYYY  HH24:MI')
from inventory_transaction it,(select
max(Dstamp) LT
from inventory_transaction it
where it.client_id='SB'
--and trunc(it.Dstamp)=trunc(sysdate)
and reference_id like 'C%'
and it.code='Pick')
where it.client_id='SB'
and reference_id like 'C%' 
and it.code='Pick'
and Dstamp=LT
/

select ' ' from DUAL;

select
'Latest E Shipment' A,
it.reference_id,
to_char(Dstamp,'DD-MON-YYYY  HH24:MI')
from inventory_transaction it,(select
max(Dstamp) LT
from inventory_transaction it
where it.client_id='SB'
--and trunc(it.Dstamp)=trunc(sysdate)
and reference_id like 'E%'
and it.code='Shipment')
where it.client_id='SB'
and reference_id like 'E%' 
and it.code='Shipment'
and Dstamp=LT
/

select ' ' from DUAL;



select
'Latest E Pick ' A,
it.reference_id,
to_char(Dstamp,'DD-MON-YYYY  HH24:MI')
from inventory_transaction it,(select
max(Dstamp) LT
from inventory_transaction it
where it.client_id='SB'
--and trunc(it.Dstamp)=trunc(sysdate)
and reference_id like 'E%'
and it.code='Pick')
where it.client_id='SB'
and reference_id like 'E%' 
and it.code='Pick'
and Dstamp=LT
/

select ' ' from DUAL;
select 'Oldest Standard Order on Red Prairie: ' from DUAL;
select ' ' from DUAL;

select
'Released     ',
order_id,
to_char(Order_date,'DD-MON-YYYY  HH24:MI')
from order_header
where client_id='SB'
and status='Released'
and order_date=(select
min(order_date)
from order_header
where client_id='SB'
and status='Released'
and order_id like 'C%')
/

select
'Allocated    ',
order_id,
to_char(Order_date,'DD-MON-YYYY  HH24:MI')
from order_header
where client_id='SB'
and status='Allocated'
and order_date=(select
min(order_date)
from order_header
where client_id='SB'
and status='Allocated'
and order_id like 'C%')
/

select ' ' from DUAL;
Select 'Latest Order on Red Prairie:' from DUAL;
select ' ' from DUAL;


select
'Released     ',
order_id,
to_char(Order_date,'DD-MON-YYYY  HH24:MI')
from order_header
where client_id='SB'
and status='Released'
and order_date=(select
max(order_date)
from order_header
where client_id='SB'
and status='Released'
and order_id like 'C%')
/

select
'Allocated    ',
order_id,
to_char(Order_date,'DD-MON-YYYY  HH24:MI')
from order_header
where client_id='SB'
and status='Allocated'
and order_date=(select
max(order_date)
from order_header
where client_id='SB'
and status='Allocated'
and order_id like 'C%')
/

select ' ' from DUAL;


select
'Released     ',
order_id,
to_char(Order_date,'DD-MON-YYYY  HH24:MI')
from order_header
where client_id='SB'
and status='Released'
and order_date=(select
max(order_date)
from order_header
where client_id='SB'
and status='Released'
and order_id like 'E%')
/

select
'Allocated    ',
order_id,
to_char(Order_date,'DD-MON-YYYY  HH24:MI')
from order_header
where client_id='SB'
and status='Allocated'
and order_date=(select
max(order_date)
from order_header
where client_id='SB'
and status='Allocated'
and order_id like 'E%')
/

select ' ' from DUAL;
select ' ' from DUAL;

select
'Total number of Standard Orders received today  '||count(*)
from order_header
where client_id='SB'
and trunc(order_date)=trunc(sysdate)
and order_id like 'C%'
/

select
'Total number of those Orders now shipped         '||count(*)
from order_header
where client_id='SB'
and trunc(order_date)=trunc(sysdate)
and order_id like 'C%'
and status='Shipped'
/

select ' ' from DUAL;
select 'End of Report' from DUAL;

