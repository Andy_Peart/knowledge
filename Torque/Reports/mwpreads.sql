SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Pre Advice ID,Line,Sku,Qty Due,Qty Received' from dual
union all
select "ID" || ',' || "Line" || ',' || "Sku" || ',' || "Due" || ',' || "Rec" 
from
(
select pah.pre_advice_id    as "ID"
, pal.line_id               as "Line"
, pal.sku_id                as "Sku"
, pal.qty_due               as "Due"
, case when it.code = 'Receipt' then sum(it.update_qty)
       when it.code = 'Receipt Reverse' then -(sum(it.update_qty))
       end                  as "Rec"
from pre_advice_header pah
inner join pre_advice_line pal on pal.client_id = pah.client_id and pah.pre_advice_id = pal.pre_advice_id
left join inventory_transaction it on it.client_id = pal.client_id and it.sku_id = pal.sku_id and it.reference_id = pal.pre_advice_id
where pah.client_id = 'MOUNTAIN'
and (nvl(pah.pre_advice_id,'x') = '&&1' 
    or 
    nvl(pah.consignment,'x') = '&&2')
and code in ('Receipt','Receipt Reverse')
and to_loc_id in ('Z001','Z002','Z003','Z004','Z005')
group by pah.pre_advice_id
, pal.line_id
, pal.sku_id
, pal.qty_due
, it.code
order by 1,2
)
/
--spool off