SET FEEDBACK OFF
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 2400
SET TRIMSPOOL ON

--break on A dup skip 1

--compute sum label "Total" of C D F H on A

--spool mwoverorders.csv

SELECT 'Run Date/Time : '||TO_CHAR(SYSDATE, 'DD-MON-YYYY @ HH:MI') FROM DUAL
union all
select 'Priority,Creation Date,Order Date,Orders' from DUAL
union all
select
PRT||','||
CD||','||
OD||','||
CT
from (SELECT
cast(oh.priority || ' - ' || UPPER(oh.dispatch_method) as varchar(36)) as PRT,
trunc(oh.creation_date) CD,
trunc(oh.order_date) OD,
COUNT(*) CT
FROM order_header OH
WHERE OH.client_id = 'MOUNTAIN'
AND OH.order_type  = 'WEB'
AND oh.status NOT IN ('Shipped', 'Cancelled', 'Picked', 'Complete')
and nvl(oh.consignment,' ')<>'SHORT'
group by
cast(oh.priority || ' - ' || UPPER(oh.dispatch_method) as varchar(30)),
trunc(oh.creation_date),
trunc(oh.order_date)
order by CD,OD)
/

--spool off
