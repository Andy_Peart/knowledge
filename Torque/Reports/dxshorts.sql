SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 66
SET NEWPAGE 1
SET LINESIZE 9999
SET TRIMSPOOL ON

column AA heading 'Consignment' format A12
column A heading 'Date' format A12
column A1 heading 'Order ID' format A24
column A2 heading 'SKU ID' format A18
column A3 heading 'Description' format A36
column F heading 'Instructions' format A16
column B heading 'Shortage' format 999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



break on report
compute sum label 'Total' of B on report

--spool dxshorts.csv

ttitle 'Shortage Report - 'report_date'                      Consignment - '&&2 

--select 'Date,Order No,Instructions,SKU,Description,Shortage' from DUAL;

select
distinct
--md.consignment AA,
trunc(md.Dstamp) A,
md.Task_id A1,
oh.instructions F,
md.sku_id A2,
sku.description A3,
md.old_qty_to_move-md.qty_to_move B
from move_descrepancy md,sku,order_header oh
where md.client_id='DX'
and md.reason_code='EX_NE'
and md.sku_id=sku.sku_id
--and md.Dstamp>sysdate-1
and md.consignment like '&&2%'
and sku.client_id='DX'
and md.task_id=oh.order_id
and oh.client_id='DX'
and oh.status in ('Allocated','Released')
order by
A,A1
/

--spool off

