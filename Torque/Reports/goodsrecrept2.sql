SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

 
column B format A15
column C format A28
column E format A20
column F format 9999999
column G format A18
column H format A40
column I format 999999999
column J format 999999999
column K format 999999999


select 'Goods Received Report - for client ID &&2 and Pre Advice ID &&3' 
from dual;

select 'Supplier Id,Supplier Name,Pre Advice ID,Line ID,Product Code,Description,Ordered,Received,Shortfall'
from dual;


select 
ph.supplier_id B,
ad.name C,
ph.pre_advice_id E,
pl.line_id F, 
pl.sku_id ||','|| 
DDD ||','||
pl.qty_due I,
nvl(pl.qty_received,0) J,
pl.qty_due-nvl(pl.qty_received,0) K
from pre_advice_header ph,pre_advice_line pl,address ad,(select 
sku_id SSS,
description DDD
from sku where client_id='&&2')
where ph.client_id='&&2'  
and pl.qty_received is null
and ph.pre_advice_id='&&3' 
and ph.supplier_id=ad.address_id (+)
and ph.pre_advice_id=pl.pre_advice_id
and pl.sku_id=SSS (+)
order by pl.pre_advice_id,pl.line_id
/

select 
it.supplier_id B,
ad.name C,
it.reference_id E,
it.line_id F, 
it.sku_id ||','||  
DDD ||','|| 
ph.qty_due I,
sum(it.update_qty) J,
ph.qty_due-ph.qty_received K
from inventory_transaction it,pre_advice_line ph,address ad,(select 
sku_id SSS,
description DDD
from sku where client_id='&&2')
where ph.client_id='&&2'  
and ph.pre_advice_id='&&3' 
and it.supplier_id=ad.address_id (+)
and it.reference_id=ph.pre_advice_id
and it.line_id=ph.line_id
and it.station_id not like 'Auto%'
and it.code like 'Receipt%'
and it.sku_id=SSS (+)
group by
it.supplier_id,ad.name,it.grn,it.reference_id,it.sku_id,it.line_id,DDD,ph.qty_due,ph.qty_due-ph.qty_received
order by it.reference_id,it.line_id
/
