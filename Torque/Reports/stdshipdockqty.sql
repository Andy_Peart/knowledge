SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Client Name,Client id,Sku,Unaccounted Qty' from dual
union all
select "Client Name" ||','|| "Client id" ||','|| "Sku" ||','|| "Qty"
from
(
with t1 as 
(
    select i.client_id
    , i.sku_id
    , sum(i.qty_on_hand) as "Qty"
    from inventory i
    inner join location l on l.site_id = i.site_id and l.location_id = i.location_id
    where l.loc_type = 'ShipDock'
    and i.client_id = '&&2'
    group by i.client_id
    , i.sku_id
), t2 as (
    select oh.client_id
    , ol.sku_id
    , sum(nvl(ol.qty_picked,0)) as "Qty"
    from order_header oh
    inner join order_line ol on ol.client_id = oh.client_id and ol.order_id = oh.order_id
    where oh.status not in ('Shipped')
    and oh.client_id = '&&2'
    and nvl(ol.qty_picked,0) > 0
    and nvl(ol.qty_shipped,0) = 0
    group by oh.client_id
    , ol.sku_id
)
select c.name   as "Client Name"
, t1.client_id  as "Client id"
, t1.sku_id     as "Sku"
, sum(nvl(t1."Qty",0)) - sum(nvl(t2."Qty",0)) as "Qty"
from t1
left join t2 on t2.client_id = t1.client_id and t2.sku_id = t1.sku_id
left join client c on c.client_id = t1.client_id
group by c.name
, t1.client_id
, t1.sku_id
having sum(nvl(t1."Qty",0)) - sum(nvl(t2."Qty",0)) <> 0
)
/
--spool off