SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Shipped Date,Order Type,Order #,Qty Ordered,Qty Shipped,More than &&5 Units?' from dual
union all
select "Creation" ||','|| "Type" ||','|| "Order" ||','|| "Qty Ord" ||','|| "Qty Ship" ||','|| "More than X Units"
from
(
select trunc(oh.shipped_date) as "Creation"
, oh.order_type as "Type"
, oh.order_id   as "Order"
, sum(ol.qty_ordered)   as "Qty Ord"
, sum(ol.qty_shipped)   as "Qty Ship"
, case when sum(ol.qty_shipped) > '&&5' then 'Y' else 'N' end as "More than X Units"
from order_header oh
inner join order_line ol on ol.order_id = oh.order_id and ol.client_id = oh.client_id
where oh.client_id = '&&2'
/*and oh.order_type = 'WEB'*/
and oh.shipped_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and oh.status = 'Shipped'
group by trunc(oh.shipped_date)
, oh.order_type
, oh.order_id
order by 4 desc
)
/
--spool off