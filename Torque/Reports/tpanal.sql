SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON


column A format a12
column B format a12
column C format a12
column D format a20
column E format 999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON




--break on A4 dup on report

--compute sum label 'Sub Total' of B C on A2
--compute sum label 'Summary' of B C on A1
--compute sum label 'Totals for Day' of B C on A4
--compute sum label 'Overall' of B C on report

--spool tpanalysis.csv

select 'SKU,Date,Code,Reference,Qty,Reason' from DUAL;


select
sku_id A,
trunc(DStamp) B,
code C,
reference_id D,
update_qty E,
reason_id
from inventory_transaction
where client_id='TP'
and trunc(dstamp) between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')
and sku_id like '&&4%'
and code in ('Shipment','Receipt','Adjustment')
order by
A,B,C,D
/

--spool off
