SET FEEDBACK OFF
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 960
SET TRIMSPOOL ON
--ttitle 'CURVY KATE Despatch Quantities from &&2 to &&3' skip 2
column A1 heading 'Client' format a10
column A heading 'Date' format a12
column B heading 'Orders' format 99999999
column C heading 'Units' format 99999999
column D heading 'Stock' format 99999999
column E heading 'Hangers' format 99999999
column F heading 'Type' format a20
BREAK ON report
compute SUM label 'Totals' OF B C D E ON report
--spool ck3outtot.csv
SELECT 'Client,Date,Customer,Name,Orders,Units,Stock,Hangers,Type'
FROM DUAL;

SELECT it.client_id A1,
  TRUNC(it.dstamp) A,
  NVL(it.customer_id,'BLANK') G,
   NAME,
  --' ' PC,
  COUNT(DISTINCT it.reference_id) B,
  NVL(B2,0) C,
  NVL(B2,0)-NVL(B1,0) D,
  NVL(B1,0) E,
  order_type F
FROM inventory_transaction it,
  (SELECT TRUNC(dstamp) A1,
    NVL(customer_id,'BLANK') G1,
    --count(distinct reference_id) G,
    SUM (update_qty) B1
  FROM inventory_transaction
  WHERE client_id = '&&4'
  AND code        = 'Shipment'
  AND dstamp BETWEEN to_date('&&2', 'DD/MM/YY') AND to_date('&&3', 'DD/MM/YY')+1
  AND sku_id                                                                 IN ('5052574061487','5052574061470')
  GROUP BY TRUNC(Dstamp),
    customer_id
  ),
  (SELECT TRUNC(ITL.dstamp) A2,
    NVL(ITL.customer_id,'BLANK') G2,
    COUNT(DISTINCT ITL.reference_id) G,
    SUM (ITL.update_qty) B2,
    oh.order_type 
  FROM inventory_transaction ITL inner join order_header oh on itl.client_id=oh.client_id and ITL.REFERENCE_ID=oh.ORDER_ID
  WHERE ITL.client_id = '&&4'
  AND ITL.code        = 'Shipment'
  AND ITL.dstamp BETWEEN to_date('&&2', 'DD/MM/YY') AND to_date('&&3', 'DD/MM/YY')+1
  AND ITL.update_qty>0
  GROUP BY TRUNC(ITL.Dstamp),
    ITL.customer_id,
    oh.order_type),
  (SELECT DISTINCT address_id ADD1,
    name NAME
    --postcode PC
  FROM address
  WHERE client_id = '&&4'
  )
WHERE it.client_id = '&&4'
AND it.code        = 'Shipment'
AND it.update_qty  >0
AND it.dstamp BETWEEN to_date('&&2', 'DD/MM/YY') AND to_date('&&3', 'DD/MM/YY')+1
AND NVL(it.customer_id,'BLANK')=G1 (+)
AND NVL(it.customer_id,'BLANK')=G2 (+)
AND TRUNC(it.Dstamp)           =A1 (+)
AND TRUNC(it.Dstamp)           =A2 (+)
AND it.customer_id             =ADD1 (+)
GROUP BY it.client_id,
  TRUNC(it.Dstamp),
  it.customer_id,
  NAME,
  --PC,
  NVL(B2,0),
  NVL(B1,0), order_type
ORDER BY A,
  G
/
--spool off
