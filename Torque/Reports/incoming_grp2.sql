/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :     Incoming_grp2.sql				      */
/*                                                                            */
/*     DESCRIPTION:     Printout of Pre Advice details to be used by w/house  */
/*                                                                            */
/*   DATE     BY   DESCRIPTION						      */
/*   ======== ==== ===========					              */
/*   04/11/04 BS   initial version     				  	      */
/*   15/09/06 MS   modified to include ean    				      */
/*   19/02/07 BK   modified to change text                                    */ 
/*   12/03/07 BK   modified to include SKU description                        */
/*   14/05/07 BK   rehashed to show groups and products                       */
/******************************************************************************/
SET FEEDBACK OFF
SET VERIFY OFF
SET TAB OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES



/* Column Headings and Formats */
COLUMN A heading "PO NO" FORMAT a16
COLUMN B heading "SKU" FORMAT a18
COLUMN BA heading "Group" FORMAT a10
COLUMN BB heading "Description" FORMAT a40
COLUMN B1 heading "Size" FORMAT a10
COLUMN B2 heading "Colour" FORMAT a16
COLUMN BC heading "Product" FORMAT a20
COLUMN C heading "Line" FORMAT 999999
COLUMN D heading " Qty|Due" FORMAT 9999999
COLUMN E heading " GRN|Qty" FORMAT 9999999
COLUMN F heading "EAN" FORMAT a16
COLUMN G heading "GRN" FORMAT a12
COLUMN H heading "NOTES" FORMAT a20
COLUMN J heading "  " FORMAT a2


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


SET LINESIZE 132
SET WRAP OFF
SET PAGES 66
SET COLSEP ''
SET NEWPAGE 0

/* Top Title */
TTITLE LEFT 'Date: ' report_date CENTER 'TML GRN Report for PO &&2 - Client &&3' RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 1 - 

/* Set up page and line */


BREAK ON BA on BC on REPORT
column BA new_value null
column BC new_value null
compute sum label 'Totals' of D E on report


SELECT
nvl(gs.goldseal,U5) BA,
--DECODE('&&3','TR',U3,U2) BC,
U3 BC,
PAL.SKU_ID B, 
DDD BB,
EEE B1,
FFF B2,
--PAL.Line_ID C,
nvl(PAL.Qty_Due,0) D,   
nvl(PAL.Qty_Received,0) E,
'  ' J
--GGG F 
FROM Pre_Advice_Header PAH, Pre_Advice_Line PAL, torque.goldseal_temp gs,(select 
sku_id SSS,
description DDD,
sku_size EEE,
colour FFF,
user_def_type_5 U5,
user_def_type_3 U2,
user_def_type_2||'-'||user_def_type_1||'-'||user_def_type_4 U3,
ean GGG
from sku where client_id='&&3')
WHERE PAH.Pre_Advice_ID = PAL.Pre_Advice_ID
AND PAL.SKU_ID = SSS (+)
AND PAH.Pre_Advice_ID = '&&2'
and PAH.client_id = '&&3'
and U5=gs.group1 (+)
ORDER BY B,BC,BA;

