SET FEEDBACK OFF
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240
SET TRIMSPOOL ON

--SET TERMOUT OFF

break on pre_advice_id skip 1


--spool stdoutpa2.csv

select 'Incomplete Pre Advices for client &&2 with creation date from &&3 to &&4' from DUAL
union all
select 'Pre Advice ID,SKU,Qty Due,Qty Received,Difference,Due Date,Supllier' from DUAL
union all
select pre_advice_id||','||sku_id||','||qty_due||','||qty_rec||','||diff||','||due_date||','||supplier_id
from 
(
    select
    ph.pre_advice_id,
    pl.sku_id,
    sum(pl.qty_due) qty_due,
    sum(nvl(pl.qty_received,0)) qty_rec,
    sum(pl.qty_due)-sum(nvl(pl.qty_received,0)) diff,
    trunc(due_dstamp) due_date,
    supplier_id
    from pre_advice_header ph, pre_advice_line pl
    where ph.pre_advice_id = pl.pre_advice_id
    and ph.client_id = '&&2'
    and pl.client_id = '&&2'
    and ph.status not in ('Complete')
    and ph.creation_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
    group by
    ph.pre_advice_id,
    pl.sku_id,
    trunc(due_dstamp),
    supplier_id
    order by
    ph.pre_advice_id,
    sku_id
)
union all
select * from 
(
    select
    'Total'||',,'||
    sum(pl.qty_due)||','||
    sum(nvl(pl.qty_received,0))||','||
    sum(pl.qty_due-nvl(pl.qty_received,0))
    from pre_advice_header ph, pre_advice_line pl
    where ph.pre_advice_id = pl.pre_advice_id
    and ph.client_id = '&&2'
    and ph.status not in ('Complete')
    and ph.creation_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
)
/

--spool off

