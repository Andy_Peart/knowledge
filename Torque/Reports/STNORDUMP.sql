SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ''
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 900
SET TRIMSPOOL ON
select 'Contact Name,Company,Address 1,Address 2,Address 3,City,Zip Code,ISO2 (Country),Email,Phone,Weight,Type,Description,Pieces,Value,Reference,Service Type' Headers from dual
union all
select 
'"'|| case when oh.name is null then oh.customer_id else oh.name end  ||'","' || case when oh.customer_id is null then oh.name else oh.customer_id end ||'","' ||
oh.address1 ||'","' ||
oh.address2||'","' || 
Case when oh.County is null then oh.town else oh.County end||'","' ||  
oh.town ||'","' ||
oh.postcode||'","' || 
c.iso2_id||'","' || 
oh.contact_email ||'","' ||
oh.contact_phone ||'","' ||
'' ||'","' ||
'N'||'","' || 
'Electronic Goods'||'","' || 
''||'","' || 
sum(ol.Line_Value)||'","' ||
oh.order_id ||'","' ||
'PARC' || '"'  
from  Order_Header oh 
inner join Country C on c.iso3_ID = oh.country 
inner join Order_Line OL on ol.order_id = oh.order_id and ol.client_id = oh.client_id 
where oh.client_id = 'ST'
and oh.consignment = '&&2'
Group by 
case when oh.name is null then oh.customer_id else oh.name end  
,case when oh.customer_id is null then oh.name else oh.customer_id end 
,oh.address1 
,oh.address2 
,Case when oh.County is null then oh.town else oh.County end  
,oh.town
,oh.postcode 
,c.iso2_id 
,oh.contact_email 
,oh.contact_phone 
,oh.order_id
/ 
 