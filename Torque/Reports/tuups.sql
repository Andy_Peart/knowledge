SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET TRIMSPOOL ON

select 'OrderId' from dual
union all
select order_id 
from order_header
where client_id = 'TU'
and nvl(country,'GBR') <> 'GBR'
/* and status = 'Shipped'*/
and 
(
order_id = '&&2'
or
consignment = '&&3'
)
/