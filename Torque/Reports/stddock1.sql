SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9999
SET TRIMSPOOL ON


--and if this could run on a date range against pre advice creation date for now please.

--spool stddock1.csv

select 'Pre Advice Id,Status,Due Date,Time,Booking Date,Time,Arrived Date,Time,Finish Date,Time,Elapsed Time' from Dual;

select
A||','''||
B||''''
from (select
A,
B+C||':'||D B
from (select
A,
CASE
WHEN B=32 AND to_char(FDT,'DY') in ('MON','TUE') 
THEN B-16
WHEN B=24 AND to_char(FDT,'DY')='MON' 
THEN B-16
ELSE B END B,
CASE
WHEN C>16 THEN C-16 
ELSE C END C,
D
from (select
bid.reference_id||','||
PAH.STATUS||','||
to_char(PAH.DUE_DSTAMP,'DD/MM/YYYY')||','||
to_char(PAH.DUE_DSTAMP,'HH24:MI')||','||
to_char(bid.BOOKING_DSTAMP,'DD/MM/YYYY')||','||
to_char(bid.BOOKING_DSTAMP,'HH24:MI')||','||
to_char(bid.DSTAMP,'DD/MM/YYYY')||','||
to_char(bid.DSTAMP,'HH24:MI')||','||
to_char(PAH.FINISH_DSTAMP,'DD/MM/YYYY')||','||
to_char(PAH.FINISH_DSTAMP,'HH24:MI') A,
(substr((PAH.FINISH_DSTAMP-bid.DSTAMP),9,2))*8 B,
PAH.FINISH_DSTAMP FDT,
to_number(substr((PAH.FINISH_DSTAMP-bid.DSTAMP),12,2)) C,
substr((PAH.FINISH_DSTAMP-bid.DSTAMP),15,2) D
from booking_in_diary_log bid,PRE_ADVICE_HEADER pah
where bid.REFERENCE_ID=PAH.PRE_ADVICE_ID
and bid.client_id = pah.client_id
and bid.client_id = '&&2'
and bid.code = 'Update Status'
and bid.status in ('Complete')
and bid.reference_type = 'Pre-Advised'
and pah.creation_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1))
order by 1)
/


--spool off
