SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','
SET WRAP OFF


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON

column F1 heading 'Stock Type' format A20
column F2 heading 'Actual Units' format A10


--set termout off
--spool bk_comp.csv



select 'Stock type, Actual units' from DUAL
union all
select F1 || ',' || F2
from (
select  'Retail Mens shirts' F1 , sum(inv.qty_on_hand) F2
from inventory inv inner join sku on inv.sku_id = sku.sku_id
where sku.client_id = 'TR'
and inv.client_id = 'TR'
and sku.product_group not in ('WS')
and (
substr(location_id,1,3) between '101' and '153' 
or 
substr(location_id,1,3) between '201' and '261' 
or 
substr(location_id,1,3) between '301' and '349' 
or 
substr(location_id,1,3) between '410' and '453' 
)
and substr(location_id,3,3) <> 'RET'
and substr(location_id,3,3) <> 'TOP'
union all
select  'Retail Womens shirts' F1  , sum(inv.qty_on_hand) F2
from inventory inv inner join sku on inv.sku_id = sku.sku_id
where sku.client_id = 'TR'
and inv.client_id = 'TR'
and sku.product_group in ('WS')
and (
substr(location_id,1,3) between '101' and '153' 
or 
substr(location_id,1,3) between '201' and '261' 
or 
substr(location_id,1,3) between '301' and '349' 
or 
substr(location_id,1,3) between '401' and '453' 
)
and substr(location_id,3,3) <> 'RET'
and substr(location_id,3,3) <> 'TOP'
union all
select  'Retail Ties' F1 , sum(inv.qty_on_hand) F2
from inventory inv
where inv.client_id = 'TR'
and substr(location_id,1,3) = 'TIE'
union all
select  'Retail Hangings' F1 , sum(inv.qty_on_hand) F2
from inventory inv 
where inv.client_id = 'TR'
and 
(
(location_id like 'HANG____'
and substr(location_id,1,8) not between 'HANG0001' and 'HANG0070'
and substr(location_id,1,8) not between 'HANG3990' and 'HANG3998'
)
or
location_id like 'PRHG%'
or
location_id like 'Z%'
)
union all
select 'Retail Returns' F1 , sum(inv.qty_on_hand) F2
from inventory inv inner join location loc on inv.location_id = loc.location_id
where loc.site_id = 'WFD'
and inv.client_id = 'TR'
and 
(
(substr(inv.location_id,1,5) between '17RET' and '34RET' and substr(inv.location_id,3,3) = 'RET')
or
inv.location_id like '__TOPR%'
)
union all
select 'Retail Cufflinks' F1 , sum(inv.qty_on_hand) F2
from inventory inv inner join location loc on inv.location_id = loc.location_id
where loc.site_id = 'WFD'
and inv.client_id = 'TR'
and 
(
substr(inv.location_id,1,7) between 'CUFF001' and 'CUFF202'
or
substr(inv.location_id,1,7) between 'CUFF213' and 'CUFF300'
)
union all
select  'Retail Umbrellas' F1 , sum(inv.qty_on_hand) F2
from inventory inv inner join location loc on inv.location_id = loc.location_id
where loc.site_id = 'WFD'
and inv.client_id = 'TR'
and substr(inv.location_id,1,7) between 'CUFF203' and 'CUFF212'
union all
select  'Retail Accessories' F1 , sum(inv.qty_on_hand) F2
from inventory inv inner join location loc on inv.location_id = loc.location_id 
where loc.site_id = 'WFD'
and inv.client_id = 'TR'
and 
(
substr(inv.location_id,1,3) between '408' and '409' 
or
inv.location_id = 'MNGIFTBOX'
or
inv.location_id = 'MENGIFTBOX'
or
(substr(inv.location_id,1,5) between '01ACS' and '07ACS' and substr(inv.location_id,3,3) = 'ACS')
)
union all
select  'Retail Seconds' F1 , sum(inv.qty_on_hand) F2
from inventory inv inner join location loc on inv.location_id = loc.location_id
where loc.site_id = 'WFD'
and inv.client_id = 'TR'
and inv.location_id = 'HOLD50'
union all
select  'Retail Seconds hangs' F1 , sum(inv.qty_on_hand) F2
from inventory inv inner join sku on inv.sku_id = sku.sku_id
where sku.client_id = 'TR'
and inv.client_id = 'TR'
and 
(
substr(location_id,1,8) between 'HANG0001' and 'HANG0070'
or
location_id like 'HANG____S'
)
union all
select 'Web Shirts' F1, sum(qty_on_hand) F2
from inventory inv
where inv.client_id = 'TR'
and (
	inv.location_id like 'U%'
	or
	inv.location_id like 'V%'
	or
	inv.location_id like 'W%'
	or
	inv.location_id like 'X%'
	or
	inv.location_id like 'Y%'
	or
	substr(inv.location_id,1,2) between 'BA' and 'BZ'  
	or
	substr(inv.location_id,1,2) between 'CA' and 'CR'  
)
and location_id not like 'WJAY%'
and location_id not like 'WSMA%'
and location_id not like 'WBM%'
and location_id not in ('WMGIFTBOX', 'WBMI1A', 'WEDC1B', 'WEDC1F', 'WJAY1A', 'WMGIFTBOX', 'WSMA1G', 'WVOG1E', 'WVOG1F',  'H1', 'WH1', 'WH2','BR04','BR05','CONTAINER' )
union all
select 'Web Returns' F1 , sum(qty_on_hand) F2
from inventory inv
where inv.client_id = 'TR'
and 
(
inv.location_id like '__TRA%'
OR
inv.location_id like 'RETS%'
OR
substr(inv.location_id,1,2) between '2E' and '2F'  
)
union all
select 'Web Ties' F1, sum(inv.qty_on_hand) F2
from inventory inv 
where inv.client_id = 'TR'
and (
inv.location_id like 'TIE0%'
or
inv.location_id like 'TIE3%'
or
inv.location_id like 'HI%'
)
union all
select 'Web Cufflinks' F1, sum(qty_on_hand) F2
from inventory inv
where inv.client_id = 'TR'
and inv.location_id like 'CF%'
union all
select 'Web Shoes' F1, sum(qty_on_hand) F2
from inventory inv
where inv.client_id = 'TR'
and 
(
inv.location_id like 'TOPSHOE%'
or
inv.location_id like '__RTN%'
or
inv.location_id like 'SHOE%'
)
)
/