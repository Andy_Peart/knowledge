define orderid="&&1"

set feedback off                 
set verify off
set wrap off
set colsep ','

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 500
set trimspool on

select 'ORDER_ID,CUSTOMER_ID,NAME,CONTACT,ADDRESS1,ADDRESS2,TOWN,COUNTY,POSTCODE,COUNTRY,CONTACT_PHONE,CONTACT_EMAIL,CARRIER_ID,SERVICE,ITEM_QTY,WEIGHT,DESCRIPTION OF PACKAGE,CONSIGNMENT REF NO,CARTON NO,LINE VALUE,PRODUCT_CURRENCY,LENGTH (CM),WIDTH (CM),HEIGHT (CM)' from dual
union all
select order_id||','||customer_id||','||name||','||contact||','||address1||','||address2||','||town||','||county||','||postcode||','||"COUNTRY"||','||contact_phone||','||contact_email||','||"CARRIER_ID"||','||"SERVICE CODE"
||','||"ITEM_QTY"||','||"WEIGHT"||','||"DESCRIPTION OF PACKAGE"||','||"CONSIGNMENT REF NO"||','||"CARTON NO"||','||"LINE VALUE"||','||product_currency||','||"LENGTH (CM)"||','||"WIDTH (CM)"||','||"HEIGHT (CM)"
from
(
select max(order_id) ORDER_ID,
max(customer_id) CUSTOMER_ID,
max(name) NAME,
max(contact) CONTACT,
max(address1) ADDRESS1,
max(address2) ADDRESS2,
max(town) TOWN,
max(county) COUNTY,
max(postcode) POSTCODE,
max("COUNTRY") "COUNTRY",
max(contact_phone) CONTACT_PHONE,
max(contact_email) CONTACT_EMAIL,
max("CARRIER_ID") "CARRIER_ID",
max("SERVICE CODE") "SERVICE CODE",
sum("ITEM_QTY") "ITEM_QTY",
sum("WEIGHT") "WEIGHT",
max("DESCRIPTION OF PACKAGE") "DESCRIPTION OF PACKAGE",
max(order_id)||'-'||"CONSIGNMENT REF NO" "CONSIGNMENT REF NO",
COUNT(distinct "CONTAINER_ID") "CARTON NO",
sum("LINE VALUE") "LINE VALUE",
max(product_currency) PRODUCT_CURRENCY,
max("LENGTH (CM)") "LENGTH (CM)",
max("WIDTH (CM)") "WIDTH (CM)",
max("HEIGHT (CM)") "HEIGHT (CM)"
from (
select 
odh.order_id
, odh.customer_id
, odh.name
, odh.contact
, odh.address1
, odh.address2
, odh.town
, odh.county
, odh.postcode
, country.iso2_id as "COUNTRY"
, odh.contact_phone
, odh.contact_email
,'TNT' as "CARRIER_ID"
,''  as "SERVICE CODE"
, x.container_id
, odl.sku_id
, x.update_qty as "ITEM_QTY"
, sum(x.update_qty)over(partition by container_id) as "BOX TOTAL"
, round(sum(x.update_qty*odl.expected_weight), 2) as "WEIGHT" 
, '' as "DESCRIPTION OF PACKAGE"
, x.container_id as "CONSIGNMENT REF NO"
, DENSE_RANK() OVER (ORDER BY x.container_id) as "CARTON NO"
, '' as "SPECIAL INSTRUCTIONS"
, odl.User_Def_Num_2 as "ITEM VALUE"
, sum(X.update_qty*odl.User_Def_Num_2) as "LINE VALUE"
, odl.product_currency
, '60' as "LENGTH (CM)"
, '40' as "WIDTH (CM)"
, '30' as "HEIGHT (CM)"
, sku.description
, sc.iso2_id "CE_COO"
, sku.commodity_code
, case when x.user_def_type_5 = 'CPC BOND' then '3171000'
else '1000001'
end "BOND"
, odh.vat_number 
, odh.documentation_text_1 as "ENTITY"
from order_header odh
inner join order_line odl on odh.order_id = odl.order_id and odh.client_id = odl.client_id
inner join sku on sku.sku_id = odl.sku_id and sku.client_id = odl.client_id
left join country on odh.country = country.iso3_id
left join country sc on sku.ce_coo = country.iso3_id
left join (
    select itl.reference_id
    , itl.container_id
    , itl.line_id
    , itl.sku_id
    , itl.user_def_type_5
    , sum(itl.update_qty) as update_qty    
    from inventory_transaction itl    
    where itl.client_id = 'MOUNTAIN'
    AND itl.reference_id = '&&orderid'
    and code = 'Shipment'    
    group by     
    itl.reference_id
    , itl.container_id
    , itl.line_id
    , itl.sku_id
    , itl.user_def_type_5    
    having sum(itl.update_qty) > 0    
    order by itl.container_id, itl.line_id, itl.sku_id
    ) X
on x.reference_id = odl.order_id and x.sku_id = odl.sku_id and x.line_id = odl.line_id
where odh.client_id = 'MOUNTAIN'
AND odh.order_id = '&&orderid'
and nvl(odl.qty_shipped,0) > 0
group by
odh.order_id
, odh.customer_id
, odh.name
, odh.contact
, odh.address1
, odh.address2
, odh.town
, odh.county
, odh.postcode
, country.iso2_id 
, odh.contact_phone
, odh.contact_email
,'TNT' 
, odl.sku_id
, x.update_qty 
, x.container_id 
, odl.User_Def_Num_2 
, odl.product_currency
, '60' 
, '40' 
, '30' 
, sku.description
, sc.iso2_id
, sku.commodity_code
, case when x.user_def_type_5 = 'CPC BOND' then '3171000'
else '1000001'
end 
, odh.vat_number 
, odh.documentation_text_1
)
group by 
"CONSIGNMENT REF NO"
)
/