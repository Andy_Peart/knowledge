-- ================================================
-- TORQUE - (IT) - Update Country of Origin for Skus
-- ================================================
set serveroutput on;
set verify off;
set feedback off;

-- set proper names for passed in variables
define client="&&2"
define sku="&&3"
define coo="&&4"
define delim=coalesce("&&5", ",")

declare    
    v_Rows number(2):= 0;
begin
    -- call stored procedure to perform update
        TORQUE.TQ_REPORTS.TQ_UPDATE_SKU_CE_COO
        (
            op_Rows => v_Rows,
            p_Client => '&&client', 
            p_Sku => '&&sku', 
            p_COO => '&&coo',
            p_Delimeter => '&&delim'
        );

    -- quick check on rowcount to ensure correct grammar
        if v_Rows = 1 then
            DBMS_OUTPUT.PUT_LINE(v_Rows||' sku updated.');
        else
            DBMS_OUTPUT.PUT_LINE(v_Rows||' skus updated.');
        end if;

end;
/
column sku_id format a30
column country_of_origin format a17

select sku_id
, coalesce(ce_coo, '<not set>') country_of_origin
from SKU
where client_id = '&&client'
and sku_id in
(
    select regexp_substr('&&sku', '[^&&delim]+', 1, level) from dual
    connect by regexp_substr('&&sku', '[^&&delim]+', 1, level) is not null
)
/