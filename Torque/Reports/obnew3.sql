SET FEEDBACK OFF
SET VERIFY OFF
SET TAB OFF
SET COLSEP ','

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET LINESIZE 500
SET TRIMSPOOL ON

--spool obnew3.csv


select 'Goods Received from &&2 to &&3' from DUAL;

select 'Date,Code,Type,Category,Txns,Update Qty' from DUAL;

select
D||','||
D1||','||
D2||','||
A||','||
CT||','||
B 
from (select
trunc(it.Dstamp) D,
it.code D1,
DECODE(substr(it.reference_id,1,2),'R','Web Returns','RA','Wholesale Returns','IF','Retail Returns','TO','Retail Returns','PO','PO Receipts',it.user_def_type_2) D2,
DECODE(substr(it.to_loc_id,1,3),'OBQ','Quarantine','OBP','Consumables','Good') A,
count(*) CT,
sum(it.update_qty) as B
from inventory_transaction it
--where (trunc(it.Dstamp)=to_date('&&2', 'DD-MON-YYYY') or trunc(it.Dstamp)=to_date('&&3', 'DD-MON-YYYY'))
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='OB'
and it.station_id not like 'Auto%'
and it.code like 'Receipt%'
group by
trunc(it.Dstamp),
it.code,
DECODE(substr(it.reference_id,1,2),'R','Web Returns','RA','Wholesale Returns','IF','Retail Returns','TO','Retail Returns','PO','PO Receipts',it.user_def_type_2),
DECODE(substr(it.to_loc_id,1,3),'OBQ','Quarantine','OBP','Consumables','Good')
order by D,D1,D2,A)
/

--spool off

