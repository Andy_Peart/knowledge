/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

set pagesize 68
set newpage 0
set linesize 120
set verify off
set feedback off


column A heading 'SKU' format a16
column B heading 'Description' format a40
column C heading 'Supplier ID' format a20
column CC heading 'Reference ID' format a20
column D heading 'Qty' format 99999999


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DAY DD/MM/YY') curdate from DUAL;
set TERMOUT ON

ttitle 'Sweaty Betty REVERSALS for ' report_date skip 2



--break on report

--compute sum label 'Total' of D on report

set heading on

Select
it.Sku_ID A,
sku.Description B,
it.Supplier_id C,
it.reference_id CC,
sum(it.update_qty) D
from inventory_transaction it,sku
where it.client_id='SB'
and it.dstamp>sysdate-1
and it.Code = 'Adjustment'
and it.Reason_id = 'RECRV'
and it.sku_id=sku.sku_id
and it.client_id=sku.client_id
group by
it.Sku_ID,
sku.Description,
it.Supplier_id,
it.Reference_id
order by
it.Sku_ID
/

set heading off

select 'Nothing to Report' from
(Select
nvl(sum(it.update_qty),0) D
from inventory_transaction it
where it.client_id='SB'
and it.dstamp>sysdate-1
and it.Code = 'Adjustment'
and it.Reason_id = 'RECRV')
where D=0
/

