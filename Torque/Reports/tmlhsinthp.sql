SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

 
select 'Hang Picked Units' from dual
union all
select 'Date, Qty' from dual
union all
select date_ || ',' || qty
from (
select to_char(dstamp,'DD-MM-YYYY') as date_, sum(update_qty) qty
from inventory_transaction
where client_id = 'TR'
and dstamp between to_date('&&2','DD-MON-YYYY') and to_date('&&3','DD-MON-YYYY')+1
and code = 'Pick'
and 
(from_loc_id like 'ZZ%'
or
from_loc_id like 'ZY%'
or
from_loc_id like 'HANG%')
and update_qty > 0
and reference_id like 'H%'
and final_loc_id like 'TRWEB%'
group by to_char(dstamp,'DD-MM-YYYY')
order by 1
)
union all
select ' ' from dual
union all
select 'International Hang Picked Units' from dual
union all
select 'Date, Qty' from dual
union all
select date_ || ',' || qty
from (
select to_char(dstamp,'DD-MM-YYYY') as date_, sum(update_qty) qty
from inventory_transaction
where client_id = 'TR'
and dstamp between to_date('&&2','DD-MON-YYYY') and to_date('&&3','DD-MON-YYYY')+1
and code = 'Pick'
and 
(from_loc_id like 'ZZ%'
or
from_loc_id like 'ZY%'
or
from_loc_id like 'HANG%')
and update_qty > 0
and reference_id like 'F%'
group by to_char(dstamp,'DD-MM-YYYY')
order by 1
)
/