SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep '|'


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 300
SET TRIMSPOOL ON
 
column XX NOPRINT

--spool ukmail.csv

select
order_id  XX,
'I364008'||'|'||
to_char(Sysdate,'DDMMYYYY')||'|'||
contact||'|'||
Name||'|'||
Address1||'|'||
Address2||'|'||
Town||'|'||
County||'|||'||
PostCode||'||'||
'1'||'|'||
'1000'||'|'||
'545'||'|'||
user_def_note_1||'|'||
user_def_note_2||'|||||'||
purchase_order||'|||||||||||||||'||
'Torque Commerce Court Challenge Way Cutler Heights Lane Bradford BD4 8NW'||'||'||
' '
from order_header,(select
oh.order_id AAA,
sum(nvl(ol.line_value,0)) BBB
from order_header oh, order_line ol
where oh.client_id='T'
and oh.country='GBR'
and oh.consignment='&&2'
and ol.client_id='T'
and oh.order_id=ol.order_id
group by
oh.order_id)
where client_id='T'
and consignment='&&2'
and priority=2
and order_id=AAA
and BBB<30
and consignment not like '%ND%'
union
select
order_id  XX,
'I099976'||'|'||
to_char(Sysdate,'DDMMYYYY')||'|'||
contact||'|'||
Name||'|'||
Address1||'|'||
Address2||'|'||
Town||'|'||
County||'|||'||
PostCode||'||'||
'1'||'|'||
'10'||'|'||
'1'||'|'||
user_def_note_1||'|'||
user_def_note_2||'|||||'||
purchase_order||'|||||||||||||||'||
'Torque Commerce Court Challenge Way Cutler Heights Lane Bradford BD4 8NW'||'||'||
' '
from order_header,(select
oh.order_id AAA,
sum(nvl(ol.line_value,0)) BBB
from order_header oh, order_line ol
where oh.client_id='T'
and oh.country='GBR'
and oh.consignment='&&2'
and ol.client_id='T'
and oh.order_id=ol.order_id
group by
oh.order_id)
where client_id='T'
and consignment='&&2'
and priority not in ('1','2','4','5','6','7','8')
and order_id=AAA
and (BBB>=30 or consignment like '%ND%')
union
select
order_id  XX,
'I099976'||'|'||
to_char(Sysdate,'DDMMYYYY')||'|'||
contact||'|'||
Name||'|'||
Address1||'|'||
Address2||'|'||
Town||'|'||
County||'|||'||
PostCode||'||'||
'1'||'|'||
'10'||'|'||
'2'||'|'||
user_def_note_1||'|'||
user_def_note_2||'|||||'||
purchase_order||'|||||||||||||||'||
'Torque Commerce Court Challenge Way Cutler Heights Lane Bradford BD4 8NW'||'||'||
' '
from order_header,(select
oh.order_id AAA,
sum(nvl(ol.line_value,0)) BBB
from order_header oh, order_line ol
where oh.client_id='T'
and oh.country='GBR'
and oh.consignment='&&2'
and ol.client_id='T'
and oh.order_id=ol.order_id
group by
oh.order_id)
where client_id='T'
and consignment='&&2'
and priority = '1'
and order_id=AAA
union
select
order_id  XX,
'I099976'||'|'||
to_char(Sysdate,'DDMMYYYY')||'|'||
contact||'|'||
Name||'|'||
Address1||'|'||
Address2||'|'||
Town||'|'||
County||'|||'||
PostCode||'||'||
'1'||'|'||
'10'||'|'||
'9'||'|'||
user_def_note_1||'|'||
user_def_note_2||'|||||'||
purchase_order||'|||||||||||||||'||
'Torque Commerce Court Challenge Way Cutler Heights Lane Bradford BD4 8NW'||'||'||
' '
from order_header,(select
oh.order_id AAA,
sum(nvl(ol.line_value,0)) BBB
from order_header oh, order_line ol
where oh.client_id='T'
and oh.country='GBR'
and oh.consignment='&&2'
and ol.client_id='T'
and oh.order_id=ol.order_id
group by
oh.order_id)
where client_id='T'
and consignment='&&2'
and priority = '6'
and order_id=AAA
union
select
order_id  XX,
'I099976'||'|'||
to_char(Sysdate,'DDMMYYYY')||'|'||
contact||'|'||
Name||'|'||
Address1||'|'||
Address2||'|'||
Town||'|'||
County||'|||'||
PostCode||'||'||
'1'||'|'||
'10'||'|'||
'7'||'|'||
user_def_note_1||'|'||
user_def_note_2||'|||||'||
purchase_order||'|||||||||||||||'||
'Torque Commerce Court Challenge Way Cutler Heights Lane Bradford BD4 8NW'||'||'||
' '
from order_header,(select
oh.order_id AAA,
sum(nvl(ol.line_value,0)) BBB
from order_header oh, order_line ol
where oh.client_id='T'
and oh.country='GBR'
and oh.consignment='&&2'
and ol.client_id='T'
and oh.order_id=ol.order_id
group by
oh.order_id)
where client_id='T'
and consignment='&&2'
and priority = '7'
and order_id=AAA
order by XX
/

--spool off
