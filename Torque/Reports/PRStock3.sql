SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET COLSEP ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET HEADING OFF
SET PAGESIZE 0
SET LINESIZE 200
SET TRIMSPOOL ON

column A heading 'Category' format A12
column C heading 'Customer' format A12
column B heading 'Qty' format 9999999

break on report 
compute sum label '   Total' of B on report

--spool prstock3.csv

select 'Prominent Stock Holding by Customer' from DUAL;
select to_char(SYSDATE, 'DD/MM/YY    HH:MI:SS') from DUAL;

select '' from DUAL;
select 'HANGING' from DUAL;

select
--'Hanging' A,
sku.user_def_type_2 C,
sum(i.qty_on_hand*nvl(sku.user_def_type_9,1)) B
from inventory i,sku
where i.client_id like 'PR'
and i.site_id like 'PROM'
and i.zone_1 like 'LSH1'
and sku.client_id=i.client_id
and sku.sku_id=i.sku_id
group by
sku.user_def_type_2
/

--spool off
