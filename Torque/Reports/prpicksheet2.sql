SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET LINESIZE 120
SET TRIMSPOOL ON
SET HEADING OFF



select '                  Pre Advice Receipt Sheet' from DUAL;
select '                  ------------------------' from DUAL;
select ' ' from DUAL;

column AA noprint
column R heading 'Order' format a20
column A heading 'SKU' format a16
column A1 heading 'Description' format a40
column B heading ' ' format a4
column C heading 'Location' format a12
column D heading 'Pick Qty' format 99999999
column E heading 'Picked' format A12

break on row skip 1 on report

compute sum label 'Total' of D on report



select 'Pre Advice ID        SKU       Qty Expected    Qty Received' from DUAL;
select '-------------        ---       ------------    ------------' from DUAL;
select ' ' from DUAL;


select
ph.pre_advice_id R,
pl.sku_id A,
--nvl(sku.description,'UNKNOWN') A1,
pl.qty_due D,
'   ',
'___________' E
from pre_advice_header ph,pre_advice_line pl
--,sku
where ph.client_id='&&3'
and ph.pre_advice_id like '&&2%'
and ph.client_id=pl.client_id
and ph.pre_advice_id=pl.pre_advice_id
--and pl.sku_id=sku.sku_id
--and pl.client_id=sku.client_id
order by R,A
/

