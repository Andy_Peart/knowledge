SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON



select 'Client ID','Order ID','Order Date','Shipped Date','Num Lines', 'Qty Order' from DUAL
union all
SELECT order_header.CLIENT_ID,
order_header.ORDER_ID,
TO_CHAR(TRUNC(order_header.ORDER_DATE)),
TO_CHAR(TRUNC (order_header.SHIPPED_DATE)),
TO_CHAR(order_header.NUM_LINES),
TO_CHAR(sum(ORDER_LINE.QTY_ORDERED))
FROM order_header
INNER JOIN ORDER_LINE
ON ORDER_LINE.ORDER_ID       = order_header.ORDER_ID
AND ORDER_LINE.CLIENT_ID     = order_header.CLIENT_ID
WHERE order_header.CLIENT_ID = 'MOUNTAIN'
AND order_header.ORDER_DATE BETWEEN TRUNC(SysDate) -7 AND TRUNC(SysDate) + 1
AND order_header.WORK_GROUP = 'WWW'
group by 
order_header.CLIENT_ID, 
order_header.ORDER_ID, 
TO_CHAR(TRUNC(order_header.SHIPPED_DATE)),
TO_CHAR(TRUNC(order_header.ORDER_DATE)), 
TO_CHAR(order_header.NUM_LINES);
/
