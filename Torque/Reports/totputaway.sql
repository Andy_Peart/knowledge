SET FEEDBACK OFF                 
set pagesize 68
set linesize 140
set verify off

clear columns
clear breaks
clear computes

column D heading 'Putaway Date' format a16
column A heading 'Advice ID' format a16
column B heading 'Received' format 999999
column C heading ' Putaway' format 999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'Total Units Putaway from &&2 to &&3 - for client ID &&4 as at ' report_date skip 2

break on report

compute sum label 'Total' of B C on report

select
KJ A,
sum(nvl(KD,0)) B,
Sum(nvl(JD,0)) C
from
(select
tag_id JT,
--nvl(it.user_def_type_6,' ') JJ,
sum(it.update_qty) as JD
from inventory_transaction it
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='&&4'
and it.code='Putaway'
group by
tag_id,
it.reference_id
),
(select
tag_id KT,
nvl(reference_id,' ') KJ,
sum(it.update_qty) as KD
from inventory_transaction it
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='&&4'
and it.code='Receipt'
group by
tag_id,
it.reference_id
)
where JT=KT
group by KJ
order by
A
/



