SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep '|'


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 300
SET TRIMSPOOL ON
 
column XX NOPRINT

--spool ukmail.csv

select /* Standard Delivery 545 3-5 days */
order_id  XX,
'I364008'||'|'||
to_char(Sysdate,'DDMMYYYY')||'|'||
contact||'|'||
Name||'|'||
Address1||'|'||
Address2||'|'||
Town||'|'||
County||'|||'||
PostCode||'||'||
'1'||'|'||
'1000'||'|'||
'545'||'|'||
user_def_note_1||'|'||
user_def_note_2||'|||||'||
purchase_order||'|||||||||||||||'||
'Torque Commerce Court Challenge Way Cutler Heights Lane Bradford BD4 8NW'||'||'||
' '
from order_header,(select
oh.order_id AAA,
sum(nvl(ol.line_value,0)) BBB
from order_header oh, order_line ol
where oh.client_id='T'
and oh.country='GBR'
and oh.consignment='&&2'
and ol.client_id='T'
and oh.order_id=ol.order_id
group by
oh.order_id)
where client_id='T'
and consignment='&&2'
and priority=2
and order_id=AAA
and BBB<30
and consignment not like '%ND%'
union
select /* Next Day Delivery 1 */
order_id  XX,
'I099976'||'|'||
to_char(Sysdate,'DDMMYYYY')||'|'||
contact||'|'||
Name||'|'||
Address1||'|'||
Address2||'|'||
Town||'|'||
County||'|||'||
PostCode||'||'||
'1'||'|'||
'10'||'|'||
'1'||'|'||
user_def_note_1||'|'||
user_def_note_2||'|||||'||
purchase_order||'|||||||||||||||'||
'Torque Commerce Court Challenge Way Cutler Heights Lane Bradford BD4 8NW'||'||'||
' '||'|'||
regexp_substr(ORDER_HEADER.CONTACT_PHONE,'^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$')||'|'||
regexp_substr(ORDER_HEADER.CONTACT_EMAIL,'[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+\.[a-zA-Z]{2,4}')||'|'||
 case 
  when regexp_substr(ORDER_HEADER.CONTACT_EMAIL,'[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+\.[a-zA-Z]{2,4}') is null 
    and regexp_substr(ORDER_HEADER.CONTACT_PHONE,'^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$') is null then null
  when  regexp_substr(ORDER_HEADER.CONTACT_EMAIL,'[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+\.[a-zA-Z]{2,4}') is not null 
    and regexp_substr(ORDER_HEADER.CONTACT_PHONE,'^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$') is null then 'E'
  when  regexp_substr(ORDER_HEADER.CONTACT_EMAIL,'[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+\.[a-zA-Z]{2,4}') is null 
    and regexp_substr(ORDER_HEADER.CONTACT_PHONE,'^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$') is not null then 'P'
  when  regexp_substr(ORDER_HEADER.CONTACT_EMAIL,'[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+\.[a-zA-Z]{2,4}') is not null 
    and regexp_substr(ORDER_HEADER.CONTACT_PHONE,'^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$') is not null then 'P'
  else null
 end||'|'||
 case 
  when regexp_substr(ORDER_HEADER.CONTACT_EMAIL,'[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+\.[a-zA-Z]{2,4}') is null 
    and regexp_substr(ORDER_HEADER.CONTACT_PHONE,'^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$') is null then null
  when  regexp_substr(ORDER_HEADER.CONTACT_EMAIL,'[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+\.[a-zA-Z]{2,4}') is not null 
    and regexp_substr(ORDER_HEADER.CONTACT_PHONE,'^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$') is null then 'Y'
  when  regexp_substr(ORDER_HEADER.CONTACT_EMAIL,'[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+\.[a-zA-Z]{2,4}') is null 
    and regexp_substr(ORDER_HEADER.CONTACT_PHONE,'^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$') is not null then 'Y'
  when  regexp_substr(ORDER_HEADER.CONTACT_EMAIL,'[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+\.[a-zA-Z]{2,4}') is not null 
    and regexp_substr(ORDER_HEADER.CONTACT_PHONE,'^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$') is not null then 'Y'
  else null
 end
from order_header,(select
oh.order_id AAA,
sum(nvl(ol.line_value,0)) BBB
from order_header oh, order_line ol
where oh.client_id='T'
and oh.country='GBR'
and oh.consignment='&&2'
and ol.client_id='T'
and oh.order_id=ol.order_id
group by
oh.order_id)
where client_id='T'
and consignment='&&2'
and priority  in ('3','997')
and order_id=AAA
--and (BBB>=30 or consignment like '%ND%')
union
select /* FDTS Next Day Before 12 */
order_id  XX,
'I099976'||'|'||
to_char(Sysdate,'DDMMYYYY')||'|'||
contact||'|'||
Name||'|'||
Address1||'|'||
Address2||'|'||
Town||'|'||
County||'|||'||
PostCode||'||'||
'1'||'|'||
'10'||'|'||
'2'||'|'||
user_def_note_1||'|'||
user_def_note_2||'|||||'||
purchase_order||'|||||||||||||||'||
'Torque Commerce Court Challenge Way Cutler Heights Lane Bradford BD4 8NW'||'||'||
' '||'|'||
regexp_substr(ORDER_HEADER.CONTACT_PHONE,'^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$')||'|'||
regexp_substr(ORDER_HEADER.CONTACT_EMAIL,'[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+\.[a-zA-Z]{2,4}')||'|'||
 case 
  when regexp_substr(ORDER_HEADER.CONTACT_EMAIL,'[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+\.[a-zA-Z]{2,4}') is null 
    and regexp_substr(ORDER_HEADER.CONTACT_PHONE,'^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$') is null then null
  when  regexp_substr(ORDER_HEADER.CONTACT_EMAIL,'[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+\.[a-zA-Z]{2,4}') is not null 
    and regexp_substr(ORDER_HEADER.CONTACT_PHONE,'^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$') is null then 'E'
  when  regexp_substr(ORDER_HEADER.CONTACT_EMAIL,'[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+\.[a-zA-Z]{2,4}') is null 
    and regexp_substr(ORDER_HEADER.CONTACT_PHONE,'^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$') is not null then 'P'
  when  regexp_substr(ORDER_HEADER.CONTACT_EMAIL,'[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+\.[a-zA-Z]{2,4}') is not null 
    and regexp_substr(ORDER_HEADER.CONTACT_PHONE,'^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$') is not null then 'P'
  else null
 end||'|'||
 case 
  when regexp_substr(ORDER_HEADER.CONTACT_EMAIL,'[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+\.[a-zA-Z]{2,4}') is null 
    and regexp_substr(ORDER_HEADER.CONTACT_PHONE,'^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$') is null then null
  when  regexp_substr(ORDER_HEADER.CONTACT_EMAIL,'[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+\.[a-zA-Z]{2,4}') is not null 
    and regexp_substr(ORDER_HEADER.CONTACT_PHONE,'^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$') is null then 'Y'
  when  regexp_substr(ORDER_HEADER.CONTACT_EMAIL,'[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+\.[a-zA-Z]{2,4}') is null 
    and regexp_substr(ORDER_HEADER.CONTACT_PHONE,'^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$') is not null then 'Y'
  when  regexp_substr(ORDER_HEADER.CONTACT_EMAIL,'[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+\.[a-zA-Z]{2,4}') is not null 
    and regexp_substr(ORDER_HEADER.CONTACT_PHONE,'^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$') is not null then 'Y'
  else null
 end
from order_header,(select
oh.order_id AAA,
sum(nvl(ol.line_value,0)) BBB
from order_header oh, order_line ol
where oh.client_id='T'
and oh.country='GBR'
and oh.consignment='&&2'
and ol.client_id='T'
and oh.order_id=ol.order_id
group by
oh.order_id)
where client_id='T'
and consignment='&&2'
and priority = '1'
and order_id=AAA
union
select 
order_id  XX,
'I099976'||'|'||
to_char(Sysdate,'DDMMYYYY')||'|'||
contact||'|'||
Name||'|'||
Address1||'|'||
Address2||'|'||
Town||'|'||
County||'|||'||
PostCode||'||'||
'1'||'|'||
'10'||'|'||
'9'||'|'||
user_def_note_1||'|'||
user_def_note_2||'|||||'||
purchase_order||'|||||||||||||||'||
'Torque Commerce Court Challenge Way Cutler Heights Lane Bradford BD4 8NW'||'||'||
' '||'|'||
regexp_substr(ORDER_HEADER.CONTACT_PHONE,'^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$')||'|'||
regexp_substr(ORDER_HEADER.CONTACT_EMAIL,'[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+\.[a-zA-Z]{2,4}')||'|'||
 case 
  when regexp_substr(ORDER_HEADER.CONTACT_EMAIL,'[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+\.[a-zA-Z]{2,4}') is null 
    and regexp_substr(ORDER_HEADER.CONTACT_PHONE,'^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$') is null then null
  when  regexp_substr(ORDER_HEADER.CONTACT_EMAIL,'[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+\.[a-zA-Z]{2,4}') is not null 
    and regexp_substr(ORDER_HEADER.CONTACT_PHONE,'^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$') is null then 'E'
  when  regexp_substr(ORDER_HEADER.CONTACT_EMAIL,'[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+\.[a-zA-Z]{2,4}') is null 
    and regexp_substr(ORDER_HEADER.CONTACT_PHONE,'^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$') is not null then 'P'
  when  regexp_substr(ORDER_HEADER.CONTACT_EMAIL,'[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+\.[a-zA-Z]{2,4}') is not null 
    and regexp_substr(ORDER_HEADER.CONTACT_PHONE,'^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$') is not null then 'P'
  else null
 end||'|'||
 case 
  when regexp_substr(ORDER_HEADER.CONTACT_EMAIL,'[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+\.[a-zA-Z]{2,4}') is null 
    and regexp_substr(ORDER_HEADER.CONTACT_PHONE,'^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$') is null then null
  when  regexp_substr(ORDER_HEADER.CONTACT_EMAIL,'[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+\.[a-zA-Z]{2,4}') is not null 
    and regexp_substr(ORDER_HEADER.CONTACT_PHONE,'^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$') is null then 'Y'
  when  regexp_substr(ORDER_HEADER.CONTACT_EMAIL,'[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+\.[a-zA-Z]{2,4}') is null 
    and regexp_substr(ORDER_HEADER.CONTACT_PHONE,'^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$') is not null then 'Y'
  when  regexp_substr(ORDER_HEADER.CONTACT_EMAIL,'[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+\.[a-zA-Z]{2,4}') is not null 
    and regexp_substr(ORDER_HEADER.CONTACT_PHONE,'^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$') is not null then 'Y'
  else null
 end
from order_header,(select
oh.order_id AAA,
sum(nvl(ol.line_value,0)) BBB
from order_header oh, order_line ol
where oh.client_id='T'
and oh.country='GBR'
and oh.consignment='&&2'
and ol.client_id='T'
and oh.order_id=ol.order_id
group by
oh.order_id)
where client_id='T'
and consignment='&&2'
and priority = '6'
and order_id=AAA
union
select /* UK Mail Saturday Delivery */
order_id  XX,
'I099976'||'|'||
to_char(Sysdate,'DDMMYYYY')||'|'||
contact||'|'||
Name||'|'||
Address1||'|'||
Address2||'|'||
Town||'|'||
County||'|||'||
PostCode||'||'||
'1'||'|'||
'10'||'|'||
'7'||'|'||
user_def_note_1||'|'||
user_def_note_2||'|||||'||
purchase_order||'|||||||||||||||'||
'Torque Commerce Court Challenge Way Cutler Heights Lane Bradford BD4 8NW'||'||'||
' '||'|'||
regexp_substr(ORDER_HEADER.CONTACT_PHONE,'^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$')||'|'||
regexp_substr(ORDER_HEADER.CONTACT_EMAIL,'[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+\.[a-zA-Z]{2,4}')||'|'||
 case 
  when regexp_substr(ORDER_HEADER.CONTACT_EMAIL,'[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+\.[a-zA-Z]{2,4}') is null 
    and regexp_substr(ORDER_HEADER.CONTACT_PHONE,'^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$') is null then null
  when  regexp_substr(ORDER_HEADER.CONTACT_EMAIL,'[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+\.[a-zA-Z]{2,4}') is not null 
    and regexp_substr(ORDER_HEADER.CONTACT_PHONE,'^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$') is null then 'E'
  when  regexp_substr(ORDER_HEADER.CONTACT_EMAIL,'[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+\.[a-zA-Z]{2,4}') is null 
    and regexp_substr(ORDER_HEADER.CONTACT_PHONE,'^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$') is not null then 'P'
  when  regexp_substr(ORDER_HEADER.CONTACT_EMAIL,'[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+\.[a-zA-Z]{2,4}') is not null 
    and regexp_substr(ORDER_HEADER.CONTACT_PHONE,'^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$') is not null then 'P'
  else null
 end||'|'||
 case 
  when regexp_substr(ORDER_HEADER.CONTACT_EMAIL,'[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+\.[a-zA-Z]{2,4}') is null 
    and regexp_substr(ORDER_HEADER.CONTACT_PHONE,'^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$') is null then null
  when  regexp_substr(ORDER_HEADER.CONTACT_EMAIL,'[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+\.[a-zA-Z]{2,4}') is not null 
    and regexp_substr(ORDER_HEADER.CONTACT_PHONE,'^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$') is null then 'Y'
  when  regexp_substr(ORDER_HEADER.CONTACT_EMAIL,'[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+\.[a-zA-Z]{2,4}') is null 
    and regexp_substr(ORDER_HEADER.CONTACT_PHONE,'^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$') is not null then 'Y'
  when  regexp_substr(ORDER_HEADER.CONTACT_EMAIL,'[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+\.[a-zA-Z]{2,4}') is not null 
    and regexp_substr(ORDER_HEADER.CONTACT_PHONE,'^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$') is not null then 'Y'
  else null
 end
from order_header,(select
oh.order_id AAA,
sum(nvl(ol.line_value,0)) BBB
from order_header oh, order_line ol
where oh.client_id='T'
and oh.country='GBR'
and oh.consignment='&&2'
and ol.client_id='T'
and oh.order_id=ol.order_id
group by
oh.order_id)
where client_id='T'
and consignment='&&2'
and priority = '7'
and order_id=AAA
order by XX
/

--spool off
