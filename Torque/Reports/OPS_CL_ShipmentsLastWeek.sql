SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON

select 'Order Ref,Customer,SKU,Qty,Date Shipped,' from DUAL;


select
oh.order_id ||','||
name ||','||
ol.sku_id ||','||
nvl(ol.qty_shipped,0) ||','||
to_char(oh.shipped_date,'DD-MON-YYYY') ||','||
' '
from order_header oh,order_line ol,sku
where oh.client_id = 'OPS'
and oh.status='Shipped'
and oh.shipped_date>sysdate-7
and oh.order_id=ol.order_id
and ol.client_id = 'OPS'
and sku.client_id='OPS'
and sku.product_group='CL'
and sku.sku_id=ol.sku_id
order by
oh.order_id
/
