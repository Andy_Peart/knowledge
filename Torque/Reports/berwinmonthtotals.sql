SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 99
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON
SET HEADING ON


column A4 heading 'Date' format a14
column A5 heading 'Type' format a20
column B heading 'Units' format 999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle 'Berwin Monthly Totals' skip 2


break on A4 dup skip 1 

--compute sum label 'Sub Total' of B C on A2

--spool berwinreceipts.csv

select
to_char(it.Dstamp,'yyyy MON') A4,
'Chinese    Receipts' A5,
sum(it.update_qty) B
from inventory_transaction it
where it.client_id='BW'
and it.station_id not like 'Auto%'
and it.dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')
--and it.code in ('Receipt','Adjustment')
and (it.code like 'Receipt%' or (it.code='Adjustment' and it.reason_id in ('RECRV','STK CHECK')))
--Chinese
and it.sku_id not like '%-%'
group by
to_char(it.Dstamp,'yyyy MON')
union
select
to_char(it.Dstamp,'yyyy MON') A4,
'Chinese    Shipments' A5,
sum(it.update_qty) B
from inventory_transaction it
where it.client_id='BW'
and it.dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')
and it.code in ('Shipment')
--Chinese
and it.sku_id not like '%-%'
group by
to_char(it.Dstamp,'yyyy MON')
union
select
to_char(it.Dstamp,'yyyy MON') A4,
'Hungarian  Receipts' A5,
sum(it.update_qty) B
from inventory_transaction it
where it.client_id='BW'
and it.station_id not like 'Auto%'
and it.dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')
--and it.code in ('Receipt','Adjustment')
and (it.code='Receipt' or (it.code='Adjustment' and it.reason_id in ('RECRV','STK CHECK')))
--Hungarian
and it.sku_id like '%-%'
group by
to_char(it.Dstamp,'yyyy MON')
union
select
to_char(it.Dstamp,'yyyy MON') A4,
'Hungarian  Shipments' A5,
sum(it.update_qty) B
from inventory_transaction it
where it.client_id='BW'
and it.dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')
and it.code in ('Shipment')
--Hungarian
and it.sku_id like '%-%'
group by
to_char(it.Dstamp,'yyyy MON')
order by
A4,A5
/
