SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 120

column A heading 'Date' format a16
column AA heading 'Grp' format a10
column AAA heading 'Grp2' format a20
column B heading 'Type' format a12
column C heading 'Units' format 999999
column D heading 'Orders' format 999999
column M format A4 noprint


--break on A skip 2 on AA dup skip 1 on report

--compute sum label 'Total' of C on A
--compute sum label 'Total' of C on AA
--compute sum label 'Total' of C on report


--set termout off
--spool TR083.csv

select 'TR83 All Shipments as at  '||to_char(sysdate-7,'DD/MM/YYYY') from Dual;
SELECT
distinct 'OT '||nvl(oh.order_type,'   Blank') A,
'END'
FROM order_header oh
WHERE oh.client_id='TR'
order by
A
/



SELECT
to_char(trunc(oh.creation_date),'YYMM') M,
to_char(trunc(oh.creation_date),'DD/MM/YYYY') A,
sku.product_group AA,
'Units Ordered' AAA,
nvl(oh.order_type,'   Blank') B,
nvl(sum(ol.qty_ordered),0) C,
'END'
FROM order_header oh, order_line ol, sku
WHERE oh.client_id='TR'
and oh.client_id=ol.client_id
and oh.order_id=ol.order_id
and oh.client_id=sku.client_id
and ol.sku_id=sku.sku_id
group by
trunc(oh.creation_date),
sku.product_group,
oh.order_type
union
SELECT
to_char(trunc(oh.creation_date),'YYMM') M,
to_char(trunc(oh.creation_date),'DD/MM/YYYY') A,
sku.product_group AA,
'No of Orders' AAA,
nvl(oh.order_type,'   Blank') B,
count(*) C,
'END'
FROM order_header oh, order_line ol, sku
WHERE oh.client_id='TR'
and oh.client_id=ol.client_id
and oh.order_id=ol.order_id
and oh.client_id=sku.client_id
and ol.sku_id=sku.sku_id
group by
trunc(oh.creation_date),
sku.product_group,
oh.order_type
order by
M,A,AA,AAA DESC,B
/

select 'END' from DUAL;


--spool off
--set termout on
