SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 132

column A heading 'User ID' format a14
column B heading 'Name' format a30
column C heading 'Picks' format 999999
column C1 heading 'Pick1' format 999999
column C2 heading 'Pick2' format 999999
column D heading 'Receipts' format 999999
column E2 heading 'Relocate Tasks' format 999999
column E heading 'Relocate Units' format 999999
column F heading 'Returns' format 999999
column G heading 'IDTs' format 999999
column K heading 'Tasks' format 999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


break on report

compute sum label 'Totals' of C C1 C2 D E E2 F G K on report


select 'Productivity by User ID from &&2 to &&3 - for client  &&4' from DUAL;

select 'Log on,Name,Pick Tasks,Units Picked,Pack Configs,Pack Config Units,Receipts,Relocate Tasks,Units Relocated,Unit Returns,Unit IDTs' from DUAL;



select
user_id A,
name B,
--nvl(KA,0)+nvl(KB,0)+nvl(KC,0)+nvl(KD,0)+nvl(KE,0) K,
nvl(KA,0) K,
nvl(JA,0) C,
nvl(CT,0) C2,
nvl(JF,0) C1,
nvl(KB,0) D,
nvl(KC,0) E2,
nvl(JC,0) E,
nvl(JD,0) F,
nvl(JE,0) G
from application_user,
(select
user_id J1,
sum(update_qty) JA,
count(*) KA
from inventory_transaction
where Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and client_id='&&4'
and update_qty<>0
and ((code='Pick' and to_loc_id='CONTAINER')
or (code='Pick' and to_loc_id<>'CONTAINER' and from_loc_id<>'CONTAINER'))
group by user_id),
(select
user_id J2,
sum(update_qty) JB,
count(*) KB
from inventory_transaction
where Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and client_id='&&4'
and update_qty<>0
and station_id not like 'Auto%'
and code='Receipt'
--and elapsed_time>0
group by user_id),
(select
user_id J3,
sum(update_qty) JC,
count(*) KC
from inventory_transaction
where Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and client_id='&&4'
and update_qty<>0
and code='Relocate'
--and elapsed_time>0
group by user_id),
(select
user_id J4,
sum(update_qty) JD,
count(*) KD
from inventory_transaction
where Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and client_id='&&4'
and update_qty<>0
and code='Adjustment'
and reason_id like 'T%'
and reason_id<>'T760'
--and elapsed_time>0
group by user_id),
(select
user_id J5,
sum(update_qty) JE,
count(*) KE
from inventory_transaction
where Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and client_id='&&4'
and update_qty<>0
and code='Adjustment'
and reason_id='T760'
--and elapsed_time>0
group by user_id),
(select
it.user_id J6,
count(*) CT,
sum(it.update_qty) JF
from inventory_transaction it,sku_config sc
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='&&4'
and sc.client_id='&&4'
and it.sku_id=sc.config_id
and it.update_qty<>0
and ((it.code='Pick' and it.to_loc_id='CONTAINER')
or (it.code='Pick' and it.to_loc_id<>'CONTAINER' and it.from_loc_id<>'CONTAINER'))
group by
it.user_id)
where user_id=J1 (+)
and user_id=J2 (+)
and user_id=J3 (+)
and user_id=J4 (+)
and user_id=J5 (+)
and user_id=J6 (+)
and user_id<>'LEIGHH'
and nvl(JA,0)+nvl(JB,0)+nvl(JC,0)+nvl(JD,0)+nvl(JE,0)>0
order by user_id
/

