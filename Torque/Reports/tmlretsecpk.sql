SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

 
select 'Date,Returns' from dual
union all
select date_ || ',' || returns 
from (
	select to_char(dstamp,'DD-MM-YYYY') as date_, sum(update_qty) returns
	from inventory_transaction
	where client_id = 'TR'
	and dstamp between to_date('&&2','DD-MON-YYYY') and to_date('&&3','DD-MON-YYYY')+1
	and code = 'Pick'
	and 
	(
	substr(from_loc_id,1,5) between '17RET' and '34RET' and substr(from_loc_id,3,3) = 'RET'
	)
	and update_qty > 0
	and to_loc_id = 'CONTAINER'
	group by to_char(dstamp,'DD-MM-YYYY')
)
union all
select ' ' from dual
union all
select 'Date,Seconds' from dual
union all
select date_ || ',' || seconds 
from (
select to_char(dstamp,'DD-MM-YYYY') as date_, sum(update_qty) seconds
	from inventory_transaction
	where client_id = 'TR'
	and dstamp between to_date('&&2','DD-MON-YYYY') and to_date('&&3','DD-MON-YYYY')+1
	and code = 'Pick'
	and 
	(
	substr(from_loc_id,1,7) between '01SEC01' and '05SEC59' and substr(from_loc_id,3,3) = 'SEC'
	or
	substr(from_loc_id,1,7) between '06SEC01' and '06SEC59' and substr(from_loc_id,3,3) = 'SEC'
	or
	substr(from_loc_id,1,8) between 'HANG0001' and 'HANG0070'
	or
	substr(from_loc_id,1,8) between 'HANG3990' and 'HANG3997'
	or
	substr(from_loc_id,1,9) between 'HANG0071S' and 'HANG0090S' and substr(from_loc_id,9,1) = 'S'
	)
	and update_qty > 0
	and reference_id not like 'H%'
	and to_loc_id = 'CONTAINER'
	group by to_char(dstamp,'DD-MM-YYYY')
)
/