SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ' '



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 80


column A heading 'Docket (SKU)' format A16
column C heading 'Ordered' format 9999999
column D heading 'Shipped' format 9999999

select 'T M Lewin Shipment from Prominent for Order ID &&2' from DUAL;

set heading off

select
'Date of Shipment - '||to_char(shipped_date,'DD-MON-YYYY HH24:MI')
from order_header
where client_id='PR'
and order_id='&&2'
/

select ' ' from DUAL;


select 
'Docket (SKU)' A,
' Ordered' C,
' Shipped' D
from DUAL;

break on report

compute sum label 'Total' of C D on report

set heading on

select
ol.sku_id A,
ol.qty_ordered C,
ol.qty_shipped D
from order_line ol
where ol.client_id='PR'
and ol.order_id='&&2'
order by 
A
/

select ' ' from DUAL;
select ' ' from DUAL;
select ' ' from DUAL;
select ' ' from DUAL;


select 'Handed over to |.......................|   |..........................|' from DUAL;
select '                        PRINT                         SIGN' from DUAL;

select ' ' from DUAL;
select ' ' from DUAL;
select ' ' from DUAL;
select 'Comments.................................................................' from DUAL;
