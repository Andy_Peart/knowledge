SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ' '



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 64
SET NEWPAGE 0
SET LINESIZE 160


column A1 heading 'Location' format A12
column A2 heading 'Tag' format A12
column A3 heading 'Barcode' format A16
column A4 heading 'Qty' format 999999
column A5 heading 'Description' format A42
column B1 heading 'CIMS Code' format A20
column B4 heading 'Size' format A10

TTitle 'Breakdown of Stock with CIMS style &&2 - Client &&3' skip 2

--select 'SKU,Description,CIMS Code,Size,Qty,,' from DUAL;

--break on report

--compute sum label 'Total' of C on report


select
i.location_id A1,
i.Tag_ID A2,
i.sku_id A3,
i.qty_on_hand A4,
sku.description A5,
sku.user_def_type_2||+'-'||+sku.user_def_type_1||+'-'||+sku.user_def_type_4 B1,
sku.sku_size B4
from inventory i,sku
where i.client_id='&&3'
and i.sku_id=sku.sku_id
and sku.client_id='&&3'
and sku.user_def_type_4='&&2'
order by
A1
/
