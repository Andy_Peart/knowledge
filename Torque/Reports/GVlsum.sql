SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 132
SET TRIMSPOOL ON

column A heading 'SKU' format a20
column A2 heading 'Description' format a40
column B heading 'Qty' format 99999


select 'SKU,Description,Qty,' from DUAL;


select
C A,
sku.description A2,
B-D,
' '
from sku,
(select sku_id C,
sum(qty_on_hand) B,
sum(qty_allocated) D
from inventory
where client_id = 'GV'
and location_id not in ('FAULTY','QCHELD','RETAQL')
group by sku_id)
where B-D < 5
and B-D >0
and C = sku.sku_id
and sku.client_id='GV'
order by 1
/
