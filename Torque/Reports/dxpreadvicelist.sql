SET FEEDBACK OFF
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ' '
SET UND OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 66
SET NEWPAGE 1
SET LINESIZE 200
SET TRIMSPOOL ON



column A heading 'Pre Advice ID'format A20
column B heading 'SKU ID' format A18
column C heading 'Barcode' format A20
column D heading 'Description' format A40
column E heading 'Qty Due' format 99999
column F heading '  ' format A16


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle 'Daxbourne Pre Advice &&2 List' skip 2

break on row skip 1 on report

compute sum label 'Order Total'  of E on report


Select 
ph.pre_advice_id A,
pl.sku_id B,
sku.ean C,
sku.description D,
pl.qty_due E,
'  ____________' F
from pre_advice_header ph,pre_advice_line pl,sku
where ph.client_id='DX'
and ph.pre_advice_id='&&2'
and ph.pre_advice_id=pl.pre_advice_id
and pl.client_id='DX'
and pl.sku_id=sku.sku_id
and sku.client_id='DX'
order by
pl.sku_id
/

