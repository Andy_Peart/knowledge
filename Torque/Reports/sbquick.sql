SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240


column C heading 'Order ID' format A8
column CC heading 'Order Type' format A12
column D heading 'Ord' format 999999
column E heading 'Tas' format 999999
column F heading 'Pic' format 999999


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



--ttitle  LEFT 'TML Retail - Duplicate Order Addresses ' RIGHT 'Printed ' report_date skip 2

select 'Order ID,Order Type,Ordered,Tasked,Picked' from DUAL;


--break on F skip 1


select
oh.order_id C,
oh.order_type HH,
nvl(sum(ol.qty_ordered),0) D,
nvl(sum(ol.qty_tasked),0) E,
nvl(sum(ol.qty_picked),0) F
from order_header oh,order_line ol
where oh.client_id='SB'
and oh.order_id like 'C%'
and oh.order_id=ol.order_id
and oh.order_type like '&&2%'
and oh.status<>'Shipped'
group by
oh.order_id,
oh.order_type
order by
oh.order_id
/
