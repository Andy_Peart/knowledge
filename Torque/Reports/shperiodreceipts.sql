SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 2000
SET TRIMSPOOL ON


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

--spool shreceipts.csv

select 'Receipts from &&2 to &&3 on - '||to_char(SYSDATE-1, 'DD/MM/YYYY') from DUAL;

select 'Pre_advice_id,Supplier,Notes,Sku_id,Description,Qty Due,Qty Received' from DUAL;

select
it.reference_id||','||
it.supplier_id||','||
sku.user_def_note_2||','||
it.sku_id||','||
sku.description||','||
nvl(PL4,0)||','||
sum(it.update_qty)
from inventory_transaction it,sku,(select
pl.pre_advice_id PL1,
pl.line_id PL2,
pl.sku_id PL3,
pl.qty_due PL4
from pre_advice_line pl
where pl.client_id='SH')
where it.client_id='SH'
and it.code like 'Receipt%'
--and trunc(it.Dstamp)=trunc(sysdate)-1
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.sku_id=sku.sku_id
and sku.client_id='SH'
and it.reference_id=PL1 (+)
and it.sku_id=PL3 (+)
and it.line_id=PL2 (+)
group by
it.reference_id,
it.supplier_id,
sku.user_def_note_2,
it.sku_id,
sku.description,
nvl(PL4,0)
order by
it.reference_id,
it.supplier_id,
sku.user_def_note_2
/

--spool off
