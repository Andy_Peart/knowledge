SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 160

column A heading 'Order_id' format a12
column B heading 'Status' format a12
column C heading 'Uploaded' format a5
column D heading 'Creation_Date' format a18
column F heading 'Shipped_Qty' format 5

select '&&2 - Orders Shipped but not Uploaded to Pro Logic from 60 days ago' from Dual;

select 'Order_ID,Status,Uploaded,Creation_date,Shipped_Qty' from DUAL;

select oh.order_id, oh.status, oh.uploaded, oh.creation_date, nvl(sum(ol.qty_shipped),0)
from order_header oh, order_line ol
where oh.client_id = '&&2'
and oh.creation_date between sysdate-60 and sysdate
and oh.uploaded not in ('Y', 'X')
and oh.status = 'Shipped'
and oh.order_id = ol.order_id
group by oh.order_id, oh.status, oh.uploaded, oh.creation_date
order by 4
/