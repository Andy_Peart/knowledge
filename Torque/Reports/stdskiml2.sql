SET FEEDBACK OFF
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON

--SET TERMOUT OFF

--break on B 


--spool stdskiml2.csv

select 'Sku,Locations' from DUAL;


select
''''||A||''','||
LISTAGG(C||'('||D||')', ':') WITHIN GROUP (ORDER BY C)
from (select
i.sku_id A,
i.description B,
i.location_id C,
sum(i.qty_on_hand) D
from inventory i,location ll,(select sku_id as SK from
(select sku_id,count(*) as CT from
(select
iv.sku_id as sku_id,
ll.loc_type,
iv.location_id,
sum(iv.qty_on_hand)
from inventory iv,location ll
where iv.client_id='&&2'
and iv.zone_1='&&3'
and iv.location_id=ll.location_id
and ll.loc_type like 'Tag-FIFO'
group by iv.sku_id,ll.loc_type,iv.location_id)
group by sku_id)
where CT>1)
where i.sku_id=SK
and i.client_id='&&2'
and i.zone_1='&&3'
and i.location_id=ll.location_id
and ll.loc_type like 'Tag-FIFO'
group by i.sku_id,i.description,i.location_id
order by i.sku_id,i.location_id)
group by A
order by A
/


--spool off
