SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep '|'


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 300
SET TRIMSPOOL ON



 
column XX NOPRINT

--spool mwpickassist.csv

select 'Order ID,SKU,Description,From Location,Work Group,Zone,Consignment,Qty' from DUAL;


select
oh.order_id||','''||
mt.sku_id||''','||
mt.description||','||
mt.from_loc_id||','||
oh.work_group||','||
mt.Work_Zone||','||
oh.consignment||','||
mt.qty_to_move
from order_header oh,move_task mt
where oh.client_id='MOUNTAIN'
and oh.consignment like '%&&2%'
and mt.client_id='MOUNTAIN'
and oh.order_id=mt.task_id
order by
oh.order_id,
mt.sku_id
/

--spool off

