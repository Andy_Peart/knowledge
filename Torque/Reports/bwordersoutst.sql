SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','
SET UND OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 320
SET TRIMSPOOL ON



column A format A14
column B format A14
column C format A14
column D format A28
column E format A12
column F format A12
column G format A24
column H format A32
column J format 99999
column K format 99999
column L format 99999
column M format 99999
column N format 99999


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



break on report

compute sum label 'Total'  of J K on report

spool bwoo.csv

select 'BERWIN Orders_Outstanding - '||to_char(SYSDATE, 'DD/MM/YY  HH24:MI:SS') from DUAL;

select 'Order ID,Status,Creation Date,Ship By Date,Lines,Order Qty' from DUAL;


Select
oh.Order_ID C,
oh.status B,
to_char(oh.creation_date,'DD/MM/YY') E,
to_char(oh.ship_by_date,'DD/MM/YY') F,
oh.num_lines J,
sum(qty_ordered) K
from order_header oh,order_line ol
where oh.client_id='BW'
and oh.status not in ('Shipped','Cancelled')
and oh.order_id=ol.order_id
and ol.client_id='BW'
group by
oh.Order_ID,
oh.status,
to_char(oh.creation_date,'DD/MM/YY'),
to_char(oh.ship_by_date,'DD/MM/YY'),
oh.num_lines
order by
1
/

spool off