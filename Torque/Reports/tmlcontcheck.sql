SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON

with T1 as (
select distinct trim(regexp_substr('&&3', '[^;]+', 1, levels.column_value))  as lines
from table(cast(multiset(select level from dual connect by  level <= length (regexp_replace('&&3', '[^;]+'))  + 1) as sys.OdciNumberList)) levels
)
select 'OrderID,ContainerID,QtyPicked' A1 from dual
union all
select reference_id || ',' || container_id || ',' || qty_picked
from (
select reference_id, container_id, sum(nvl(update_qty,0)) qty_picked
from inventory_transaction
where client_id = '&&2'
and code = 'Pick'
and elapsed_time > 0
and update_qty > 0
and reference_id in (select lines from T1)
group by reference_id,container_id
)
/
