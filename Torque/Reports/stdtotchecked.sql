SET FEEDBACK OFF                 
set pagesize 0
set linesize 500
set trimspool on
set verify off
set colsep ','
set heading on

clear columns
clear breaks
clear computes


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

set heading off

break on report
compute sum label 'Totals' of B C D on report

--spool stc.csv

select 'Total SKUs stock checked from &&3 to &&4 - for client ID &&2 as at '||to_char(SYSDATE, 'DD/MM/YY  HH24:MI') from DUAL;

Select 'Date,User,Start Location,End Location,Counts,SKUs,Units' from DUAL;

select
A1||','||
A2||','||
Z3||','||
X3||','||
B||','||
C||','||
D
from
(select
to_char(Dstamp,'DD/MM/YY') A1,
user_id A2,
min(Dstamp) A3,
max(Dstamp) A4,
count(*) B,
count(distinct sku_id) C,
sum(original_qty) D
from inventory_transaction
where Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and client_id='&&2'
and code='Stock Check'
group by
to_char(Dstamp,'DD/MM/YY'),
user_id),
(select
Dstamp Z1,
user_id Z2,
from_loc_id Z3
from inventory_transaction
where Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and client_id='&&2'
and code='Stock Check'),
(select
Dstamp X1,
user_id X2,
from_loc_id X3
from inventory_transaction
where Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and client_id='&&2'
and code='Stock Check')
where A3=Z1
and A4=X1
order by
A1,A2
/
select
',,,Totals'||','||
count(*)||','||
count(distinct sku_id)||','||
sum(original_qty)
from inventory_transaction
where Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and client_id='&&2'
and code='Stock Check'
/


set pagesize 0
set heading off
select '' from DUAL;

select
'Total SKU discrepancies'||','||
count(*)
from inventory_transaction
where Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and client_id='&&2'
and code='Stock Check'
and original_qty<>update_qty
/
select
'Total minus adjustment figure'||','||
nvl(sum(update_qty),0)
from inventory_transaction
where Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and client_id='&&2'
and code='Stock Check'
and update_qty<0
/
select
'Total plus adjustment figure'||','||
nvl(sum(update_qty),0)
from inventory_transaction
where Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and client_id='&&2'
and code='Stock Check'
and update_qty>0
/
select
'Nett adjustments'||','||
nvl(sum(update_qty),0)
from inventory_transaction
where Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and client_id='&&2'
and code='Stock Check'
/
--spool off
