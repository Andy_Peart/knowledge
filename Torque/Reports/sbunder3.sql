SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 130
SET TRIMSPOOL ON

column A heading 'SKU ID' format A18
column B heading 'Colour' format A18
column C heading 'Size' format A8
column D heading 'Description' format A40
column E heading 'Location' format A12
column F heading 'Stock' format 99999999
column G heading 'Last Count' format A12


--set termout off
spool sbunder3.csv


select 'Location,SKU ID,Colour,Size,Description,Available' from DUAL;
 
--set TERMOUT OFF
--column curdate NEW_VALUE report_date
--select to_char(SYSDATE, 'DAY, DD MONTH, YYYY') curdate from DUAL;
--set TERMOUT ON



--compute sum label 'Totals' of B C D E F on report

select
E||','||
A||','||
B||','||
C||','||
D||','||
F
from (select
i.location_id E,
''''||sku.sku_id||'''' A,
sku.colour B,
sku.sku_size C,
sku.description D,
sum(i.qty_on_hand - nvl(i.qty_allocated,0)) F,
' '
from inventory i,location LL,sku,
(select
iv.sku_id BBB,
sum(iv.qty_on_hand - nvl(iv.qty_allocated,0)) CCC
from inventory iv,location LL
where iv.client_id='SB'
and iv.site_id=LL.site_id
and iv.location_id=LL.location_id
and LL.loc_type='Tag-FIFO'
group by
iv.sku_id)
where i.client_id='SB'
and CCC<3
and i.sku_id=BBB
and sku.sku_id=BBB
and sku.client_id='SB'
and i.site_id=LL.site_id
and i.location_id=LL.location_id
and LL.loc_type='Tag-FIFO'
and i.sku_id in (select
sku_id
from inventory_transaction
where client_id='SB'
and code='Shipment'
and Dstamp>sysdate-1)
group by
i.location_id,
sku.sku_id,
sku.colour,
sku.sku_size,
sku.description
order by
i.location_id,
sku.sku_id)
where F>0
/


spool off
--set termout on
