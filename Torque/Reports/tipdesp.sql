/******************************************************************************/
/*                                                                            */
/*                            Elite                                           */
/*                                                                            */
/*     FILE NAME  :   tipdesp.sql                                             */
/*                                                                            */
/*     DESCRIPTION:    Datastream for Tippitoes Despatch Note			      */
/*                                                                            */
/*   DATE     BY   PROJ       ID         DESCRIPTION                          */
/*   ======== ==== ======     ========   =============                        */
/*   22/05/07 LH   TIP                   Despatch Note For Tippitoes	      */
/******************************************************************************/

/* Input Parameters are as follows */
-- 1)  GMT
-- 2)  Order_id
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 0
SET VERIFY OFF
SET FEEDBACK OFF
SET TAB OFF

 
-- Set the line length to the maximum possible, but trim 
-- trailing spaces for both screen output and spool files
SET LINESIZE 32767
SET WRAP ON
SET TRIMOUT ON
SET TRIMSPOOL ON

/* Define the format of the output dates */
DEFINE	OutputDateFormat = 'DD-MON-YYYY HH24:MI:SS'
COLUMN	sorter	NOPRINT

SELECT Distinct '0' sorter,
       'DSTREAM_TIP_DESP_HDR'          ||'|'||
       rtrim('&&2')                            /* Order_id */
from dual
union all
SELECT '1' sorter,
'DSTREAM_TIP_DESP_ADD_HDR'              ||'|'||
rtrim (oh.customer_id)				||'|'||
decode(oh.name,null,'',oh.name||'|')||
decode(oh.address1,null,'',oh.address1||'|')||
decode(oh.address2,null,'',oh.address2||'|')||
decode(oh.town,null,'',oh.town||'|')||
decode(oh.county,null,'',oh.county||'|')||
decode(t.text,null,'',t.text||'|')||          /* Country converted using workstation lookup) */
decode(oh.postcode,null,'',oh.postcode)
from order_header oh, country c, language_text t
where oh.order_id = '&&2'
and oh.client_id = 'TP'
and oh.country = c.iso3_id
and t.language = 'EN_GB'
and t.label = 'WLK'||c.iso3_id
union all
SELECT '1' sorter,
'DSTREAM_TIP_DESP_INV_HDR'              ||'|'||
rtrim (oh.inv_address_id)				||'|'||
decode(oh.inv_name,null,'',oh.inv_name||'|')||
decode(oh.inv_address1,null,'',oh.inv_address1||'|')||
decode(oh.inv_address2,null,'',oh.inv_address2||'|')||
decode(oh.inv_town,null,'',oh.inv_town||'|')||
decode(oh.inv_county,null,'',oh.inv_county||'|')||
decode(t.text,null,'',t.text||'|')||          /* Country converted using workstation lookup) */
decode(oh.inv_postcode,null,'',oh.inv_postcode)
from order_header oh, country c, language_text t
where oh.order_id = '&&2'
and oh.client_id = 'TP'
and oh.inv_country = c.iso3_id
and t.language = 'EN_GB'
and t.label = 'WLK'||c.iso3_id
union all
select distinct '1' sorter,
'DSTREAM_TIP_DESP_ORD_HDR'		||'|'||
rtrim (oh.order_id)			||'|'||
rtrim (trunc(sm.picked_dstamp))		||'|'||
rtrim (sm.location_id)			||'|'||
rtrim (sm.user_id)			||'|'||
rtrim (sm.site_id)			||'|'||
rtrim (oh.purchase_order)		||'|'||
rtrim (oh.user_def_type_6)		||'|'||
rtrim (oh.user_def_type_4)		||'|'||
rtrim (substr(oh.order_id, 3, 7))	||'|'||
rtrim (substr(oh.order_id, 10, 14))	||'|'||
'A'					||'|'||
rtrim (oh.instructions)			||'|'||
'a.directions'
from order_header oh, address a, shipping_manifest sm
/*, (select count(distinct smm.container_id) A from shipping_manifest smm where smm.order_id = '&&2')*/
where oh.order_id = '&&2'
and oh.order_id = sm.order_id
and oh.client_id = 'TP'
/*and oh.client_id = a.client_id*/
/*and oh.customer_id = a.address_id*/ 
union all
SELECT '1'||sh.container_id SORTER,
'DSTREAM_TIP_DESP_LINE'             ||'|'||
rtrim (sh.container_id)				||'|'||
rtrim (s.sku_id)					||'|'||
rtrim (s.EAN)						||'|'||
rtrim (s.description)               ||'|'||
rtrim (s.COLOUR)					||'|'||
rtrim (s.SKU_SIZE)					||'|'||
rtrim (sh.qty_shipped)				||'|'||
rtrim (s.user_def_type_5)
from sku s, shipping_manifest sh
where sh.order_id = '&&2'
and sh.qty_picked > '0'
and sh.sku_id = s.sku_id
and s.client_id = 'TP'
order by 1,2
/