SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

with T1 as (
	select it.from_loc_Id, it.owner_id, it.sku_id, it.tag_id, to_char(it.dstamp, 'DD-MON-YYYY') as date_, it.update_qty, it.notes, nvl(substr(it.extra_notes,1, instr(it.extra_notes,';',1,1)-1),it.extra_notes) ORDER_no, oh.purchase_order
	from inventory_transaction it INNER JOIN order_header oh on it.client_id = oh.client_id and nvl(substr(it.extra_notes,1, instr(it.extra_notes,';',1,1)-1),it.extra_notes) = oh.order_id
	where it.client_id = 'CC'
	and it.dstamp between to_date('&&2') AND to_date('&&3')+1
	and code = 'Owner Update'
	and nvl(it.update_qty,0) < 0
),
T2 as (
	select it.from_loc_Id, it.owner_id, it.sku_id, it.tag_id, to_char(it.dstamp, 'DD-MON-YYYY') as date_, it.update_qty, it.notes, nvl(substr(it.extra_notes,1, instr(it.extra_notes,';',1,1)-1),it.extra_notes) ORDER_no, oh.purchase_order
	from inventory_transaction it INNER JOIN order_header oh on it.client_id = oh.client_id and nvl(substr(it.extra_notes,1, instr(it.extra_notes,';',1,1)-1),it.extra_notes) = oh.order_id
	where it.client_id = 'CC'
	and it.dstamp between to_date('&&2') AND to_date('&&3')+1
	and code = 'Owner Update'
	and nvl(it.update_qty,0) >0
)
select 'Date,Location,SKU,Tag,From Owner,To Owner,Qty,Order ID,Purchase Order' from dual U
UNION ALL
select date_ || ',' || location_id|| ',' || sku_id|| ',' || tag_id|| ',' || from_owner|| ',' || to_owner|| ',' || qty || ',' || order_no || ',' || purchase_order
from (
select T1.date_, T1.from_loc_id location_id, t1.sku_id, t1.tag_id, t1.owner_id from_owner, t2.owner_id to_owner, abs(t1.update_qty) qty, T1.order_no, T1.purchase_order
from T1 left join T2 on T1.sku_id = T2.sku_id and T1.tag_id = T2.tag_id and t1.date_ = t2.date_ and t1.from_loc_id = t2.from_loc_id and t1.notes = t2.notes
)
/