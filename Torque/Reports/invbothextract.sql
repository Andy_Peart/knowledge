SET FEEDBACK OFF                 
set verify off
set wrap off
set colsep ','

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 10000
set trimspool on

select 'KEY,CODE,SITE_ID,FROM_LOC_ID,TO_LOC_ID,FINAL_LOC_ID,SHIP_DOCK,OWNER_ID,CLIENT_ID,SKU_ID,TAG_ID,CONTAINER_ID,PALLET_ID,CONDITION_ID,LOCK_STATUS,LIST_ID,DATE,WORK_GROUP,CONSIGNMENT,SUPPLIER_ID,REFERENCE_ID,LINE_ID,REASON_ID,STATION_ID,USER_ID,GROUP_ID,UPDATE_QTY,ORIGINAL_QTY,UPLOADED,ELAPSED_TIME,JOB_ID,MANNING,JOB_UNIT,NOTES,USER_DEF_TYPE_1,USER_DEF_TYPE_2,USER_DEF_TYPE_3,USER_DEF_TYPE_4,USER_DEF_TYPE_5,USER_DEF_TYPE_6,USER_DEF_TYPE_7,USER_DEF_TYPE_8,USER_DEF_CHK_1,USER_DEF_CHK_2,USER_DEF_CHK_3,USER_DEF_CHK_4,USER_DEF_DATE_1,USER_DEF_DATE_2,USER_DEF_DATE_3,USER_DEF_DATE_4,USER_DEF_NUM_1,USER_DEF_NUM_2,USER_DEF_NUM_3,USER_DEF_NUM_4,USER_DEF_NOTE_1,USER_DEF_NOTE_2,EXTRA_NOTES,PALLET_CONFIG' A1
from dual;

select key, code, site_id, from_loc_id, to_loc_id, final_loc_id, ship_dock, owner_id, client_id, sku_id, tag_id, container_id, pallet_id, condition_id, lock_status, list_id, 
to_char(dstamp, 'DD-MON-YYYY HH24:MI'), 
work_group, consignment, supplier_id, reference_id, line_id, reason_id, station_id, user_id, group_id, update_qty, original_qty, uploaded, elapsed_time, job_id, manning, job_unit, 
notes, user_def_type_1,user_def_type_2,user_def_type_3,user_def_type_4,user_def_type_5,user_def_type_6,user_def_type_7,user_def_type_8,
user_def_chk_1,user_def_chk_2,user_def_chk_3,user_def_chk_4,user_def_date_1,user_def_date_2,user_def_date_3,user_def_date_4,
user_def_num_1,user_def_num_2,user_def_num_3,user_def_num_4,user_def_note_1,user_def_note_2,extra_notes,pallet_config
from inventory_transaction
where client_id = '&&2'
and dstamp between to_date('&&3') and to_date('&&4')+1
and code = '&&5'
and reference_id like '&&6%' 
/

--spool off
