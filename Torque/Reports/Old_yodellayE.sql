Declare
Cursor labels_cur 

is 

select distinct t.*, o.* from order_header o inner join   
  (
select
       REGEXP_SUBSTR(ltrim(rtrim( substr(a.from_postcode,0, length(a.from_postcode) - 3) )), '\D+' ) as SubPostCode
      ,to_number(REGEXP_SUBSTR(ltrim(rtrim( substr(a.from_postcode,0, length(a.from_postcode) - 3) )), '*\d+' )) as fromIndex
      ,to_number(REGEXP_SUBSTR(ltrim(rtrim( substr(a.to_postcode,0, length(a.to_postcode) - 3) )), '*\d+' )   )  as toIndex
      ,d.locationname as serviceLocationName
      ,d1.locationname as hubLocationName
      ,a.servicectr_reamusid
      ,a.hub_reamusid
      ,'Yes' as typ
      ,c.productline1
      ,c.productline2
      ,c.productcode
      ,c.servicecode
      ,c.datecode
      ,c.timecode
      ,c.featureid
    from yo_destination_station a

    inner join yo_destination_prdservices b on a.servicectr_reamusid = b.servicectr_reamusid and b.Allowed = 'Y' --service
  --  inner join yo_destination_prdservices b1 on a.hub_reamusid = b1.servicectr_reamusid  and b1.Allowed = 'Y'     --hub

    inner join yo_service c on b.productcode = c.productcode and b.featurecode = c.featurecode  --service
 --   inner join yo_service c1 on b1.productcode = c1.productcode and b1.featurecode = c1.featurecode  --hub

    inner join yo_reamusid d on a.servicectr_reamusid = d.reamusid  --?
    inner join yo_reamusid d1 on a.hub_reamusid = d1.reamusid  --?
    where a.productcode = '01'
    and c.featureid in ('071','06','005') --and c.productcode = '99'
      
  union all

    --No situation
    select distinct
       REGEXP_SUBSTR(ltrim(rtrim( substr(a.from_postcode,0, length(a.from_postcode) - 3) )), '\D+' ) as SubPostCode
      ,to_number(REGEXP_SUBSTR(ltrim(rtrim( substr(a.from_postcode,0, length(a.from_postcode) - 3) )), '*\d+' ) )as fromIndex
      ,to_number(REGEXP_SUBSTR(ltrim(rtrim( substr(a.to_postcode,0, length(a.to_postcode) - 3) )), '*\d+' )   )  as toIndex
      ,d.locationname as serviceLocationName
      ,d1.locationname as hubLocationName
      ,a.servicectr_reamusid
      ,a.hub_reamusid
      ,'No' as typ
      ,hc.productline1
      ,hc.productline2
      ,hc.productcode
      ,hc.servicecode
      ,hc.datecode
      ,hc.timecode
      ,hc.featureid
    from yo_destination_station a

    inner join yo_destination_prdservices b on a.servicectr_reamusid = b.servicectr_reamusid and b.Allowed = 'E' --service
--    inner join yo_destination_prdservices b1 on a.hub_reamusid = b1.servicectr_reamusid and  b1.Allowed = 'E'     --hub

    inner join yo_destination_exception c on b.productcode = c.productcode and b.featurecode = c.featurecode and a.from_postcode = c.from_postcode  --service
--    inner join yo_destination_exception c1 on b1.productcode = c1.productcode and b1.featurecode = c1.featurecode and a.from_postcode = c1.from_postcode  --hub

  --FIX: DPH was missing link to services, remove hard coded to this service table
        inner join yo_service hc on b.productcode = hc.productcode and b.featurecode = hc.featurecode  --service
--        inner join yo_service hc1 on b1.productcode = hc1.productcode and b1.featurecode = hc1.featurecode  --hub
    
    inner join yo_reamusid d on a.servicectr_reamusid = d.reamusid  --?
    inner join yo_reamusid d1 on a.hub_reamusid = d1.reamusid  --?
    where a.productcode = '01'
    and c.featurecode in ('71','06','05') --and c.productcode = '99' 
  
  )  t on (t.SubPostCode =  REGEXP_SUBSTR(ltrim(rtrim( substr(o.postcode,0, length(o.postcode) - 3) )), '\D+' ) --SW04 e.g match on SW, then  04 between XX and XX
          and to_number( REGEXP_SUBSTR(ltrim(rtrim( substr(o.postcode,0, length(o.postcode) - 3) )), '*\d+' ) ) >= t.fromIndex 
          and to_number( REGEXP_SUBSTR(ltrim(rtrim( substr(o.postcode,0, length(o.postcode) - 3) )), '*\d+' ) ) <= t.toIndex    )
  where client_ID = 'MOUNTAIN'and o.order_id ='&&2' and work_group ='WWW';
  --and a.Order_type = 'RETAIL1'; 
  --and a.country = 'GBR';

    
    data_out labels_cur%rowtype;
    out_file utl_file.file_type;
    lines varchar2(500);
    label_no yodel_labels.label_no%type;
    new_num number; 
    counter number;
   
    
    Begin 
       out_file := utl_file.fopen('YODEL','Elabel_'||to_char(sysdate,'DDMMYYHH24MISS')||'.csv','W'); 
  --  out_file := utl_file.fopen('YODELTEST','label_'||sysdate||'.csv','W');  
     select y.label_no into label_no from Yodel_labels y where y.address_id = '02'; 
     
     
    OPEN labels_cur;
   
  
    
   LOOP
      FETCH labels_cur 
  --    INTO SUBPOSTCODE,  FROMINDEX,  TOINDEX,  SERVICELOCATIONNAME,  HUBLOCATIONNAME,  SERVICECTR_REAMUSID,  HUB_REAMUSID,  TYP,  PRODUCTLINE1,  PRODUCTLINE2,  PRODUCTCODE,  SERVICECODE,  DATECODE,  TIMECODE,  FEATUREID;
      INTO data_out;
       
      EXIT WHEN labels_cur%ROWCOUNT > 1 OR labels_cur%NOTFOUND;
    --  END LOOP;
 --    dbms_output.put_line('subpostcode,FROMINDEX,TOINDEX,SERVICELOCATIONNAME,HUBLOCATIONNAME,SERVICECTR_REAMUSID,HUB_REAMUSID,TYP,PRODUCTLINE1,PRODUCTLINE2,PRODUCTCODE,SERVICECODE,DATECODE,TIMECODE,FEATUREID,CONTACT_PHONE,CONTACT,NAME,ADDRESS1,ADDRESS2,TOWN,COUNTY,POSTCODE,F25,sequence, Address ID');
     lines := ('subpostcode,FROMINDEX,TOINDEX,SERVICELOCATIONNAME,HUBLOCATIONNAME,SERVICECTR_REAMUSID,HUB_REAMUSID,TYP,PRODUCTLINE1,PRODUCTLINE2,PRODUCTCODE,SERVICECODE,DATECODE,TIMECODE,FEATUREID,CONTACT_PHONE,CONTACT,NAME,ADDRESS1,ADDRESS2,TOWN,COUNTY,POSTCODE,F25,sequence,Address ID,ORDER_NUM,INSTRUCTIONS');
     UTL_FILE.put_line(out_file,lines);
     new_num := (1 + label_no);
       IF new_num > 999999 Then
           new_num := 0;
         End if; 
 --     dbms_output.put_line(data_out.subpostcode||','||data_out.FROMINDEX||','||data_out.TOINDEX||','||data_out.SERVICELOCATIONNAME||','||data_out.HUBLOCATIONNAME||','||data_out.SERVICECTR_REAMUSID||','||data_out.HUB_REAMUSID||','||data_out.TYP||','||data_out.PRODUCTLINE1||','||data_out.PRODUCTLINE2||','||data_out.PRODUCTCODE||','||data_out.SERVICECODE||','||data_out.DATECODE||','||data_out.TIMECODE||','||data_out.FEATUREID||',"'||data_out.CONTACT_PHONE||'",'||data_out.CONTACT||','||data_out.NAME||','||data_out.ADDRESS1||','||data_out.ADDRESS2||','||data_out.TOWN||','||data_out.COUNTY||','||data_out.POSTCODE||','||replace(data_out.POSTCODE,' ','')||',"'||to_char(new_num,'000099')||'",'||'WWW');
 --     lines := (data_out.subpostcode||','||data_out.FROMINDEX||','||data_out.TOINDEX||','||data_out.SERVICELOCATIONNAME||','||data_out.HUBLOCATIONNAME||','||data_out.SERVICECTR_REAMUSID||','||data_out.HUB_REAMUSID||','||data_out.TYP||','||data_out.PRODUCTLINE1||','||data_out.PRODUCTLINE2||',"'||data_out.PRODUCTCODE||'",'||data_out.SERVICECODE||',"'||data_out.DATECODE||'","'||data_out.TIMECODE||'","'||data_out.FEATUREID||'","'||data_out.CONTACT_PHONE||'",'||data_out.CONTACT||','||data_out.NAME||','||data_out.ADDRESS1||','||data_out.ADDRESS2||','||data_out.TOWN||','||data_out.COUNTY||','||data_out.POSTCODE||','||replace(data_out.POSTCODE,' ','')||',"'||to_char(new_num,'000099')||'",'||'WWW'||','||data_out.order_id||','||data_out.instructions||'');
        lines := (data_out.subpostcode||','||data_out.FROMINDEX||','||data_out.TOINDEX||','||data_out.SERVICELOCATIONNAME||','||data_out.HUBLOCATIONNAME||','||data_out.SERVICECTR_REAMUSID||','||data_out.HUB_REAMUSID||','||data_out.TYP||','||data_out.PRODUCTLINE1||','||data_out.PRODUCTLINE2||',"'||data_out.PRODUCTCODE||'",'||data_out.SERVICECODE||',"'||data_out.DATECODE||'","'||data_out.TIMECODE||'","'||data_out.FEATUREID||'","'||data_out.CONTACT_PHONE||'","'||data_out.CONTACT||'","'||data_out.NAME||'","'||data_out.ADDRESS1||'","'||data_out.ADDRESS2||'","'||data_out.TOWN||'","'||data_out.COUNTY||'","'||data_out.POSTCODE||'","'||replace(data_out.POSTCODE,' ','')||'","'||to_char(new_num,'000099')||'",'||'WWW'||','||data_out.order_id||','||data_out.instructions||'');
      utl_file.put_line(out_file,lines);
       insert into YO_TOTAL_LABELS (subpostcode,FROMINDEX,TOINDEX,SERVICELOCATIONNAME,HUBLOCATIONNAME,SERVICECTR_REAMUSID,HUB_REAMUSID,TYP,PRODUCTLINE1,PRODUCTLINE2,PRODUCTCODE,SERVICECODE,DATECODE,TIMECODE,FEATUREID,CONTACT_PHONE,CONTACT,NAME,ADDRESS1,ADDRESS2,TOWN,COUNTY,POSTCODE,F25,sequence,Address_ID, Order_Num, instructions) values (data_out.subpostcode,data_out.FROMINDEX,data_out.TOINDEX,data_out.SERVICELOCATIONNAME,data_out.HUBLOCATIONNAME,data_out.SERVICECTR_REAMUSID,data_out.HUB_REAMUSID,data_out.TYP,data_out.PRODUCTLINE1,data_out.PRODUCTLINE2,data_out.PRODUCTCODE,data_out.SERVICECODE,data_out.DATECODE,data_out.TIMECODE,data_out.FEATUREID,data_out.CONTACT_PHONE,data_out.CONTACT,data_out.NAME,data_out.ADDRESS1,data_out.ADDRESS2,data_out.TOWN,data_out.COUNTY,data_out.POSTCODE,replace(data_out.POSTCODE,' ',''),'(J)JD00 022 410 00'||to_char(new_num,'000099'),'WWW',data_out.order_id,data_out.instructions);
              
           
      utl_file.fclose(out_file);
  
 --  new_num := labels_cur%ROWCOUNT;
  update yodel_labels y set y.label_no = new_num where y.address_id ='02'; 
   commit;
 END LOOP;
   CLOSE labels_cur;
   
   End;
/