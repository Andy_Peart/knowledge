/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :     utlinestocko.sql                             		      */
/*                                                                            */
/*     DESCRIPTION:                                                           */
/*     CSV order lines that are short how much inventory is in the w/h        */
/*                                                                            */
/*   DATE       BY   DESCRIPTION					                                    */
/*   ========== ==== ===========				                                      */
/*   11/03/2013 YB   Created new report                      				          */
/******************************************************************************/
SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','
SET WRAP OFF


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500

/* Column Headings and Formats */
COLUMN A HEADING "Owner ID" FORMAT A5
COLUMN B HEADING "Date Created" FORMAT A14
COLUMN BA HEADING "Consignment" FORMAT A14
COLUMN C HEADING "Order Type" FORMAT A14
COLUMN D HEADING "Order" FORMAT A14
COLUMN E HEADING "Status" FORMAT A14
COLUMN F HEADING "Purchase Order" FORMAT A14
COLUMN SKU_ID HEADING "SKU ID" FORMAT A14
COLUMN G HEADING "Ordered" FORMAT 9999
COLUMN H HEADING "Tasked" FORMAT 9999
COLUMN I HEADING "Picked" FORMAT 9999
COLUMN J HEADING "Shipped" FORMAT 9999
COLUMN UTW HEADING "UTW" FORMAT 9999
COLUMN UTR HEADING "UTR" FORMAT 9999
COLUMN UTC heading "UTC" FORMAT 9999

break on report

/* Top Title */
SELECT '"Owner ID","Date Created","Consignment","Order Type","Order ID","Status","Purchase Order","SKU ID","Qty Ordered","Qty Tasked","Qty Picked","Qty Shipped","UTW","UTR","UTC"' FROM DUAL;

WITH T0 AS (
SELECT OH.OWNER_ID A,
TRUNC(OH.CREATION_DATE) B,
OH.CONSIGNMENT BA,
OH.ORDER_TYPE C,
OH.ORDER_ID D,
OH.STATUS E,
OH.PURCHASE_ORDER F,
OL.SKU_ID, 
OL.QTY_ORDERED G,
NVL(OL.QTY_TASKED,0) H,
NVL(OL.QTY_PICKED,0) I,
NVL(OL.QTY_SHIPPED,0) J
FROM ORDER_HEADER OH 
INNER JOIN ORDER_LINE OL 
ON OH.ORDER_ID=OL.ORDER_ID 
AND OH.CLIENT_ID=OL.CLIENT_ID
WHERE OH.CLIENT_ID = 'UT'
AND OH.STATUS 
NOT IN ('Shipped','Cancelled','Complete')
AND NVL(OL.QTY_ORDERED,0)-(NVL(OL.QTY_TASKED,0)+NVL(OL.QTY_PICKED,0)) <> 0),
T1 AS (
SELECT UNIQUE(SKU_ID)
FROM INVENTORY
WHERE CLIENT_ID = 'UT'),
T2 AS (
SELECT SKU_ID,
SUM(NVL(QTY_ON_HAND,0)-NVL(QTY_ALLOCATED,0)) UTW
FROM INVENTORY
WHERE CLIENT_ID = 'UT'
AND OWNER_ID = 'UTW'
AND LOCATION_ID NOT IN ('SUSPENSE','UTOUT','UTRET','UTWEB','UTWEB')
GROUP BY SKU_ID),
T3 AS (
SELECT SKU_ID,
SUM(NVL(QTY_ON_HAND,0)-NVL(QTY_ALLOCATED,0)) UTR
FROM INVENTORY
WHERE CLIENT_ID = 'UT'
AND OWNER_ID = 'UTR'
AND LOCATION_ID NOT IN ('SUSPENSE','UTOUT','UTRET','UTWEB','UTWEB')
GROUP BY SKU_ID),
T4 AS (
SELECT SKU_ID,
SUM(NVL(QTY_ON_HAND,0)-NVL(QTY_ALLOCATED,0)) UTC
FROM INVENTORY
WHERE CLIENT_ID = 'UT'
AND OWNER_ID = 'UTC'
AND LOCATION_ID NOT IN ('SUSPENSE','UTOUT','UTRET','UTWEB','UTWEB')
GROUP BY SKU_ID)
SELECT T0.*, NVL(T2.UTW,0) AS "UTW", 
NVL(T3.UTR,0) AS "UTR", 
NVL(T4.UTC,0) AS "UTC" 
FROM T1 FULL OUTER JOIN T2 ON T1.SKU_ID=T2.SKU_ID
FULL OUTER JOIN T3 ON T1.SKU_ID=T3.SKU_ID
FULL OUTER JOIN T4 ON T1.SKU_ID=T4.SKU_ID
RIGHT JOIN T0 ON T0.SKU_ID=T1.SKU_ID
/