SET FEEDBACK OFF
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 2400
SET TRIMSPOOL ON

select 'Sku,Locations,Zone,Qty on Hand,Qty Picked,Locations Stock Held In,Locations Stock Picked From' from dual
union all
select "Sku" || ',' || "Locations" || ',' || "Zone" || ',' || "Qty On Hand" || ',' || "Qty Picked" || ',' || "Locations Stock Held In" || ',' || "Locations Stock Picked From"
from
(
with sku as
(
select sku_id   as "Sku"
, location_id   as "Loc"
, site_id       as "Site"
from inventory  
where client_id = 'MOUNTAIN'
and location_id not in ('CONTAINER','DESPATCH','MAIL ORDER','SUSPENSE')
union 
select sku_id   
, from_loc_id  
, site_id     
from inventory_transaction
where client_id = 'MOUNTAIN'
and code = 'Pick'
and dstamp between to_date('&&2','DD-MON-YYYY') and to_date('&&2','DD-MON-YYYY')+1
and from_loc_id <> 'CONTAINER'
and update_qty > 0
)
, pick as
(
select sku_id                                           as "Sku"
, from_loc_id                                           as "Loc"
, sum(update_qty)                                       as "Qty"
from inventory_transaction
where client_id = 'MOUNTAIN'
and code = 'Pick'
and dstamp between to_date('&&2','DD-MON-YYYY') and to_date('&&2','DD-MON-YYYY')+1
and from_loc_id <> 'CONTAINER' 
and update_qty > 0
group by sku_id
, from_loc_id
)
, inv as
(
select sku_id                                           as "Sku"
, location_id                                           as "Loc"
, sum(qty_on_hand)                                      as "Qty"
, count(distinct location_id)over(partition by sku_id)  as "Count"
from inventory
where client_id = 'MOUNTAIN'
and location_id not in ('CONTAINER','DESPATCH','MAIL ORDER','SUSPENSE')
group by sku_id
, location_id
)
, actPick as
(
select sku_id       as "Sku"
, sum(update_qty)   as "Tot"
from inventory_transaction
where client_id = 'MOUNTAIN'
and code = 'Pick'
and dstamp between to_date('&&2','DD-MON-YYYY') and to_date('&&2','DD-MON-YYYY')+1
and from_loc_id <> 'CONTAINER'
group by sku_id
having sum(update_qty) > 0
)
, pickFrom as
(
select sku_id                   as "Sku"
, count(distinct from_loc_id)   as "Count"
from inventory_transaction
where client_id = 'MOUNTAIN'
and code = 'Pick'
and dstamp between to_date('&&2','DD-MON-YYYY') and to_date('&&2','DD-MON-YYYY')+1
and from_loc_id <> 'CONTAINER' 
and update_qty > 0
group by sku_id
)
select sku."Sku"            as "Sku"
, sku."Loc"                 as "Locations"
, loc.work_zone             as "Zone"
, nvl(inv."Qty",0)          as "Qty On Hand"
, nvl(pick."Qty",0)         as "Qty Picked"
, nvl(inv."Count",0)        as "Locations Stock Held In"
, nvl(pickFrom."Count",0)   as "Locations Stock Picked From"
from sku
inner join location loc on loc.location_id = sku."Loc" and loc.site_id = sku."Site"
left join inv on inv."Sku" = sku."Sku" and inv."Loc" = sku."Loc"
left join pick on pick."Sku" = sku."Sku" and pick."Loc" = sku."Loc"
left join actPick on actPick."Sku" = sku."Sku"
left join pickFrom on pickFrom."Sku" = sku."Sku"
where actPick."Tot" > 0
order by 1
)
/