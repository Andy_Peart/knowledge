set feedback off
set pagesize 68
set linesize 112
set verify off

clear columns
clear breaks
clear computes


column A heading 'Shipped' format A16
column BB heading 'Order Type' format A16
column B heading 'Order ID' format A20
column R heading 'Style' format A6
column C heading 'Customer' format A32
column D heading 'Shipments' format 9999999
column E heading 'Units' format 9999999
column M format a4 noprint

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'Total Units Despatched from &&2 to &&3 - for client ID &&4 as at ' report_date RIGHT 'Page:'FORMAT 999 SQL.PNO skip 2


break on REPORT on BB skip 2 on A skip 1

compute sum label 'Report Totals' of D E on REPORT
compute sum label 'Type Totals' of D E on BB 
compute sum label 'Daily Totals' of D E on A 
 

select
--to_char(oh.last_update_date, 'YYMM') M,
--to_char(oh.last_update_date, 'DD/MM/YY ') as A,
oh.order_type as BB,
substr(iv.sku_id,1,2) R,
--oh.order_id as B,
--oh.inv_name as C,
--nvl(sum(ol.qty_ordered),0) as D,
count(*) as D,
nvl(sum(iv.update_qty),0) as E
from order_header oh, inventory_transaction iv
where  oh.order_id=iv.reference_id
and oh.client_id='&&4'
and oh.status in ('Shipped')
and iv.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and iv.code='Shipment'
group by
oh.order_type,
substr(iv.sku_id,1,2)
order by
oh.order_type,
substr(iv.sku_id,1,2)
/
