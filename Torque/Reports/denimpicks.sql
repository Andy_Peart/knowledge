SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

--spool denimpicks.csv

select 'Date Picked,Reference,Customer,Sku,Units Picked' from DUAL;

select
trunc(it.Dstamp)||','||
it.reference_id ||','||
it.customer_id ||','||
it.sku_id ||','||
sum(it.update_qty)
--'0.25' B1,
--sum(it.update_qty*0.25) C1,
--5 C2
from inventory_transaction it,location L
where it.client_id='DN'
and trunc(it.Dstamp) between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.code='Pick'
and it.from_loc_id=L.location_id
and L.site_id='DN'
--and L.zone_1='2'
group by
trunc(it.Dstamp),
it.reference_id,
it.customer_id,
it.sku_id
order by
1
/


--spool off
