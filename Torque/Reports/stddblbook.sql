SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Pre Advice id,Sku,Qty received,Qty Due,Description,To Location,User' from dual
union all
select reference_id||','||sku_id||','||update_qty||','||qty_due||','||description||','||to_loc_id||','||user_id
from
(
    select it.reference_id
    , it.sku_id
    , s.description
    , it.to_loc_id
    , it.user_id
    , it.update_qty
    , pal.qty_due
    , count(1)over(partition by it.to_loc_id, it.reference_id, it.sku_id) as "Same"
    from inventory_transaction it
    inner join sku s on s.client_id = it.client_id and s.sku_id = it.sku_id
    inner join pre_advice_line pal on pal.client_id = it.client_id and pal.sku_id = it.sku_id
    where it.client_id = 'TT'
    and it.code = 'Receipt'
    and it.client_id = '&&2'
    and it.reference_id = '&&3'
    and pal.qty_received > pal.qty_due
    order by it.sku_id, it.reference_id
)
where "Same" > 1
/
--spool off