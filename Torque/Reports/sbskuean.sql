SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET TRIMSPOOL ON

select 'Order,Container,Qty,SKU,EAN' A1 from dual
union all
select reference_id || ',' || container_id || ',' || qty || ',="' || sku_id || '",="' || ean || '"'
from (
    select it.reference_id, it.container_id, sum(it.update_qty) qty, sku.sku_id, sku.ean
    from inventory_transaction it inner join sku on it.sku_id = sku.sku_id and it.client_id = sku.client_id
    where it.client_id = 'SB'
    and it.code = 'Pick'
    and nvl(it.update_qty,0) > 0
    and it.elapsed_time > 0
    and it.reference_id = '&&2'
	group by it.reference_id, it.container_id, sku.sku_id, sku.ean
)
/


