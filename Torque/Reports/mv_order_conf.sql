/******************************************************************************/
/*                                                                            */
/*                            Elite                                           */
/*                                                                            */
/*     FILE NAME  :   mv_order_conf.sql                                       */
/*                                                                            */
/*     DESCRIPTION:    Datastream for Madame V Order Confirmation Note        */
/*                                                                            */
/*   DATE     BY   PROJ       ID         DESCRIPTION                          */
/*   ======== ==== ======     ========   =============                        */
/*   06/07/07 LH   Madame V              Order Confirmation Note For Madame V */
/*                                                                            */
/******************************************************************************/

/* Input Parameters are as follows */
-- 1)  GMT
-- 2)  Order_id
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 0
SET VERIFY OFF
SET FEEDBACK OFF
SET TAB OFF

 
-- Set the line length to the maximum possible, but trim 
-- trailing spaces for both screen output and spool files
SET LINESIZE 32767
SET WRAP ON
SET TRIMOUT ON
SET TRIMSPOOL ON




/* Define the format of the output dates */
DEFINE	OutputDateFormat = 'DD-MON-YYYY HH24:MI:SS'
COLUMN	sorter	NOPRINT
COLUMN	sorterii NOPRINT

SELECT Distinct '0' sorter, '' sorterii,
       'DSTREAM_MV_ORDER_CONF_HDR'          ||'|'||
       rtrim('&&2')                            /* Order_id */
from dual
union all
SELECT '1' sorter, '' sorterii,
'DSTREAM_MV_ORDER_CONF_ADD_HDR'              ||'|'||
rtrim (oh.name)      ||'|'||
rtrim (oh.customer_id)    ||'|'||
rtrim (oh.address1)     ||'|'||
rtrim (oh.address2)     ||'|'||
rtrim (oh.town)      ||'|'||
rtrim (oh.county)     ||'|'||
rtrim (oh.country)     ||'|'||
rtrim (oh.postcode)
from order_header oh
where oh.order_id = '&&2'
and oh.client_id = 'V'
union all
SELECT '1' sorter, '' sorterii,
'DSTREAM_MV_ORDER_CONF_INV_HDR'              ||'|'||
rtrim (oh.inv_name)      ||'|'||
rtrim (oh.inv_address_id)    ||'|'||
rtrim (oh.inv_address1)     ||'|'||
rtrim (oh.inv_address2)     ||'|'||
rtrim (oh.inv_town)      ||'|'||
rtrim (oh.inv_county)     ||'|'||
rtrim (oh.inv_country)     ||'|'||
rtrim (oh.inv_postcode)
from order_header oh
where oh.order_id = '&&2'
and oh.client_id = 'V'
union all
select distinct '1' sorter, '' sorterii,
'DSTREAM_MV_ORDER_CONF_ORD_HDR'   ||'|'||
rtrim (oh.order_id)     ||'|'||
rtrim (to_char(oh.creation_date, 'DD-MON-YYYY HH24:MI'))    ||'|'||
rtrim (oh.customer_id)     ||'|'||
rtrim (oh.from_site_id)     ||'|'||
rtrim (oh.purchase_order)   ||'|'||
rtrim (oh.user_def_type_6)   ||'|'||
rtrim (oh.user_def_type_4)   ||'|'||
rtrim (substr(oh.order_id, 3, 7)) ||'|'||
rtrim (substr(oh.order_id, 10, 14)) ||'|'||
rtrim (oh.user_def_type_2) ||'|'||
rtrim (trunc(sysdate))      ||'|'||
rtrim (instructions)
from order_header oh
where oh.order_id = '&&2'
and oh.client_id = 'V'
union all
SELECT '1'||s.user_def_type_5 SORTER, s.description sorterii,
'DSTREAM_MV_ORDER_CONF_LINE'              ||'|'||
rtrim (s.sku_id)    ||'|'||
rtrim (s.description)                ||'|'||
rtrim (s.COLOUR)     ||'|'||
rtrim (s.SKU_SIZE)     ||'|'||
rtrim (ol.qty_ordered)	||'|'||
rtrim (s.user_def_type_5)   ||'|'||
rtrim (s.user_def_type_1)   ||'|'||
rtrim (case oh.user_def_type_2 when 'GBP' then s.user_def_num_1
                               when 'USD' then to_number(s.user_def_type_3, '999,999.00')
                               when 'EUR' then s.user_def_num_3
                               else 00.00 end)
from sku s, order_line ol, order_header oh
where ol.order_id = '&&2'
and s.client_id = 'V'
and ol.sku_id = s.sku_id
and oh.order_id = ol.order_id
order by 1,2,3
/