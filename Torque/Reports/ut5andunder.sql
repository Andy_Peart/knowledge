SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 1200
SET TRIMSPOOL ON

--spool new1.csv

select 'CIMS,Description,Colour,Style,Total Qty,Allocated Qty,Available Qty' from DUAL;



SELECT
s.user_def_type_2 || '-' || s.user_def_type_1 || '-' || s.user_def_type_4 AS CIMS,
s.description,
s.colour,
s.user_def_type_4 AS style_id,
NVL(SUM(i.qty_on_hand), 0) AS total_qty,
NVL(SUM(i.qty_allocated), 0) AS allocated_qty,
NVL(SUM(i.qty_on_hand), 0) - NVL(SUM(i.qty_allocated), 0) AS available_qty
FROM inventory i, sku s
WHERE i.client_id = 'UT' 
AND s.client_id = 'UT' 
AND i.sku_id = s.sku_id 
AND i.location_id NOT IN ('SUSPENSE', 'CONTAINER', 'UTOUT', 'UTFAULTY', 'UTIN', 'UTRET', 'UTWEB')
group by
s.user_def_type_2 || '-' || s.user_def_type_1 || '-' || s.user_def_type_4,
s.description,
s.colour,
s.user_def_type_4
HAVING NVL(SUM(i.qty_on_hand), 0) <= 5
ORDER BY s.user_def_type_4
/

--spool off
