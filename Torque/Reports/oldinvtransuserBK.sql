SET FEEDBACK OFF                 
set pagesize 68
set linesize 132
set verify off
set wrap off
set newpage 0

clear columns
clear breaks
clear computes


column A heading 'User ID' format a18
column B heading 'Task Type' format a18
column C heading 'Tasks' format 99999999
column D heading 'Units' format 99999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'Total Tasks by User ID from &&2 to &&3 - for client ID &&5 as at ' report_date RIGHT 'Page:'FORMAT 999 SQL.PNO skip 2

--break on report 
break on A skip 1



select
user_id A,
code B,
count(*) C,
sum(update_qty) D
from inventory_transaction
where Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and site_id='&&4'
and client_id='&&5'
and user_id like '%&&6%'
and update_qty<>0
and ((code in ('Receipt','Putaway','Replenish','Relocate','Stock Check') and elapsed_time>0)
or (code='Pick' and to_loc_id='CONTAINER')
or (code='Pick' and to_loc_id<>'CONTAINER' and from_loc_id<>'CONTAINER'))
group by user_id,code
order by user_id,code
/

set heading off
set pagesize 0
set newpage 1

select '_______________________________________________________________________' from dual;
select '' from dual;

select
'GRAND TOTALS' A,
code B,
count(*) C,
sum(update_qty) D
--sum(elapsed_time) as TT
from inventory_transaction
where Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and site_id='&&4'
and client_id='&&5'
and update_qty<>0
and ((code in ('Receipt','Putaway','Replenish','Relocate','Stock Check') and elapsed_time>0)
or (code='Pick' and to_loc_id='CONTAINER')
or (code='Pick' and to_loc_id<>'CONTAINER' and from_loc_id<>'CONTAINER'))
group by code
order by code
/

select '_______________________________________________________________________' from dual;
