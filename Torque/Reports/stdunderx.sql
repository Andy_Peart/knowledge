SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 1000
SET TRIMSPOOL ON

--spool stdunderx.csv

select 'SKU ID,Description,Colour,Total Qty,Allocated Qty,Available Qty,Last Count Date' from DUAL;



SELECT
i.sku_id,
s.description,
s.colour,
NVL(SUM(i.qty_on_hand), 0) AS total_qty,
NVL(SUM(i.qty_allocated), 0) AS allocated_qty,
NVL(SUM(i.qty_on_hand), 0) - NVL(SUM(i.qty_allocated), 0) AS available_qty,
max(trunc(i.count_dstamp))
FROM inventory i 
inner join sku s on i.client_id = s.client_id and i.sku_id = s.sku_id
inner join location loc on i.location_id = loc.location_id and i.site_id = loc.site_id
WHERE i.client_id = '&&2' 
AND i.sku_id = s.sku_id 
AND loc.loc_type IN ('Tag-FIFO')
group by
i.sku_id,
s.description,
s.colour
HAVING NVL(SUM(i.qty_on_hand), 0) <&&3
ORDER BY i.sku_id
/

--spool off
