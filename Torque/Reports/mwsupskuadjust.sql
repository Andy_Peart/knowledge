SET FEEDBACK OFF           
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 300
SET TRIMSPOOL ON




set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



break on report

compute sum label 'Totals' of X3 B F BF on report

spool mwsupskuadjust.csv

Select 'Adjustments for Supplier &&5 for Period from &&3 to &&4' from DUAL;

select 'SKU,Current Stock,Period Adjustment Up,Period Adjustment Down' from DUAL;

select
X2,
nvl(X3,0) X3,
B,
F,
B+F BF,
to_char((B+F)*100/X3,'990.99') 
from (select
X2,
X3,
sum(nvl(B,0)) B,
sum(nvl(F,0)) F
from (select
it.sku_id X2,
sum(it.update_qty) as B,
0 as F
from inventory_transaction it,sku
where it.client_id='&&2'
and it.Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and it.code='Adjustment'
and it.reason_id='SUP'
and it.supplier_id='&&5'
and it.update_qty>0
and sku.client_id='&&2'
and it.sku_id=sku.sku_id
group by
it.sku_id
union
select
it.sku_id X2,
0 as B,
sum(it.update_qty) as F
from inventory_transaction it,sku
where it.client_id='&&2'
and it.Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and it.code='Adjustment'
and it.reason_id='SUP'
and it.supplier_id='&&5'
and it.update_qty<0
and sku.client_id='&&2'
and it.sku_id=sku.sku_id
group by
it.sku_id),(select
sku_id S,
sum(qty_on_hand) X3
from inventory
where client_id='&&2'
and supplier_id='&&5'
group by
sku_id)
where X2=S (+)
group by
X2,
X3)
order by 1
/

spool off
