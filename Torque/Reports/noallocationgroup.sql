SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

column A heading 'Group' format A6
column B heading 'SKU' format A20
column C heading 'Description' format A50


select 'Group,SKU,Description' from DUAL;
 
set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DAY, DD MONTH, YYYY') curdate from DUAL;
set TERMOUT ON


select
distinct
sku.product_group,
ol.sku_id B,
sku.description
from order_header oh,order_line ol,sku
where oh.client_id='TR'
and oh.status<>'Shipped'
and oh.order_id=ol.order_id
and sku.client_id='TR'
and ol.sku_id=sku.sku_id
and sku.allocation_group is null
order by
sku.product_group,
ol.sku_id,
sku.description
/

select 'Total',count(*) from (select
distinct
sku.product_group,
ol.sku_id B,
sku.description
from order_header oh,order_line ol,sku
where oh.client_id='TR'
and oh.status<>'Shipped'
and oh.order_id=ol.order_id
and sku.client_id='TR'
and ol.sku_id=sku.sku_id
and sku.allocation_group is null
order by
sku.product_group,
ol.sku_id,
sku.description)
/

