SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON

select 'Client ID,From Site ID,Creation Date,Creation Time,Consinment code,Reference,Order Status,Customer,Line ID,SKU,Description,Qty Ordered,Qty Tasked,Qty Picked,Qty Shipped,Qty Returned,Return Notes,Returned Date,Shipped Date,Line Value,User Defined Type 1,User Defined Type 2,User Defined Type 3,User Defined Type 4,User Defined Type 5,User Defined Type 6,User Defined Type 7,User Defined Type 8,Shipdock,Dispatch Method,Purchase Order' Headers from dual
union all
select client_id||','||from_site_id||','||creation_date||','||creation_time||','||consignment||','||order_id||','||status||','||customer_id||','||line_id||','||sku_id||','||description||','||qty_ordered||','||qty_tasked||','||qty_returned||','||qty_picked||','||qty_shipped||','||notes||','||returned_date||','||shipped_date||','||line_value||','||user_def_type_1||','||user_def_type_2||','||user_def_type_3||','||user_def_type_4||','||user_def_type_5||','||user_def_type_6||','||user_def_type_7||','||user_def_type_8||','||ship_dock||','||dispatch_method||','||purchase_order
from
(
    with returns as
    (
        select it.reference_id order_id
        , trunc(it.dstamp) returned_date
        , rr.notes
        from INVENTORY_TRANSACTION it
        left join RETURN_REASON rr on rr.reason_id = it.reason_id
        where it.client_id = '&&2'
        and it.code = 'Return'
        and it.dstamp >= to_date('&&3')
    )
    select oh.client_id
    , oh.from_site_id
    , trunc(oh.creation_date) creation_date
    , to_char(oh.creation_date, 'HH24:MI:SS') creation_time
    , oh.consignment
    , oh.order_id
    , oh.status
    , oh.customer_id
    , ol.line_id
    , ol.sku_id
    , s.description
    , ol.qty_ordered
    , ol.qty_tasked
    , ol.qty_returned
    , ol.qty_picked
    , ol.qty_shipped
    , r.notes
    , r.returned_date
    , trunc(oh.shipped_date) shipped_date
    , ol.line_value
    , oh.user_def_type_1
    , oh.user_def_type_2
    , oh.user_def_type_3
    , oh.user_def_type_4
    , oh.user_def_type_5
    , oh.user_def_type_6
    , oh.user_def_type_7
    , oh.user_def_type_8
    , oh.ship_dock
    , oh.dispatch_method
    , oh.purchase_order
    from ORDER_HEADER oh
    inner join ORDER_LINE ol on ol.order_id = oh.order_id and ol.client_id = oh.client_id
    inner join SKU s on s.client_id = ol.client_id and s.sku_id = ol.sku_id
    left join RETURNS r on r.order_id = oh.order_id
    where oh.client_id = '&&2'
    and oh.creation_date between to_date('&&3') and to_date('&&4')+1
    order by trunc(oh.creation_date)
    , oh.order_id
    , ol.line_id
)
/