SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON

set pagesize 999
set linesize 160
set heading on

clear columns
clear breaks
clear computes


TTitle 'Client &&2 Breakdown of Mail Order Despatch Methods from &&3 to &&4' skip 2

column A1 heading 'Orders' format 9999999
column B1 heading 'Units' format 9999999
column D heading 'Method' format A12

break on report

compute sum label 'Totals' of A1 B1 on report

select
'OCS' D,
count(distinct it.reference_id) A1,
sum(nvl(it.update_qty,0)) B1
from inventory_transaction it
where it.client_id='&&2'
and it.code='Pick'
and to_loc_id='CONTAINER'
and it.Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and it.pallet_id like '55%'
and reference_id not like 'NP%'
union
select
'RM Next Day' D,
count(distinct it.reference_id) A1,
sum(nvl(it.update_qty,0)) B1
from inventory_transaction it
where it.client_id='&&2'
and it.code='Pick'
and to_loc_id='CONTAINER'
and it.Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and it.pallet_id like 'Z%'
and reference_id not like 'NP%'
union
select
'RM Normal' D,
count(distinct it.reference_id) A1,
sum(nvl(it.update_qty,0)) B1
from inventory_transaction it
where it.client_id='&&2'
and it.code='Pick'
and to_loc_id='CONTAINER'
and it.Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and it.pallet_id like 'B%'
and reference_id not like 'NP%'
union
select
'Airmail' D,
count(distinct it.reference_id) A1,
sum(nvl(it.update_qty,0)) B1
from inventory_transaction it
where it.client_id='&&2'
and it.code='Pick'
and to_loc_id='CONTAINER'
and it.Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
--and length(it.pallet_id)=7
and it.pallet_id not like '55%'
and it.pallet_id not like 'Z%'
and it.pallet_id not like 'B%'
and reference_id not like 'NP%'
/
