SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET TRIMSPOOL ON

select 'Location,Qty' from dual 
union all
select location_id || ',' || qty from 
(
    select location_id, CURRENT_VOLUME*10000 qty
    from location
    where site_id = 'WFD'
    and (
        substr(location_id,1,4) between '101R' and '153R' and location_id like '_______'
        or 
        substr(location_id,1,4) between '201R' and '261R' and location_id like '_______'
        or 
        substr(location_id,1,4) between '301R' and '349R' and location_id like '_______'
        or 
        substr(location_id,1,4) between '410R' and '453R' and location_id like '_______'
    )
    and substr(location_id,3,3) <> 'RET'
    and substr(location_id,3,3) <> 'TOP'
    and substr(location_id,3,2) <> 'IS'
    and lock_status = 'UnLocked'
    and (CURRENT_VOLUME*10000) <= case when '&&2' is null then to_char('0') else '&&2' end
    order by 1
)
/