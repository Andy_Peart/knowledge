SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

column SKU format A35
column B format A45
column QTY format 999999
column D format A7

break on S1 dup on S2 dup on report
compute sum label 'Season Total' of QTY on S1
compute sum label 'Year Total' of QTY on S2
compute sum label 'Grand Total' of QTY on report


--SET TERMOUT OFF
spool tsinventory.csv

select 'Trespass Inventory Report - ' || to_char(sysdate,'dd/mm/yy  hh24:mi:ss') from dual;

select 'SKU,Description,Season,Year,Qty on Hand' from DUAL;

select
a.sku_id as SKU,
b.description B,
DECODE(substr(b.user_def_type_4,2,1),'1','Spring ','2','Winter ','3','Continuity  ','XX') S1,
ascii(b.user_def_type_4)+1940 S2,
sum(a.qty_on_hand) as QTY,
case when b.sku_size in ('P','R') then 'PACK' else 'EACH' end as D
from inventory  a
inner join sku b on a.client_id = b.client_id and a.sku_id = b.sku_id
where a.client_id = 'TS'
group by
a.sku_id,
b.description,
b.user_def_type_4,
b.sku_size
order by S1,S2,SKU
/

spool off
--SET TERMOUT ON

