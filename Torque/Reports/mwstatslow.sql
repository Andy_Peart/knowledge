SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Client,Sku Code,Condition,Current Stock,Description,Colour,Size,Days since last Receipt,Days since last Shipment, Location Zone' a1
from dual
union all
select CLIENT ||','|| SKU ||','|| COND ||','|| QTY ||','|| DESCRIP ||','|| COLOUR ||','|| asciistr(SKU_SIZE) ||','|| DAYS_SINCE_RECD ||','|| DAYS_SINCE_SHIP ||','|| LOC_ZONE a1
from 
(
	Select SKU, 'MOUNTAIN' CLIENT, COND, QTY, sku.description DESCRIP, sku.colour COLOUR, sku.sku_size SKU_SIZE, LOC_ZONE,
	trunc(sysdate)-trunc(REC_DATE) DAYS_SINCE_RECD,
	DECODE(to_char(Shipped_date, 'DD/MM/YY')	,'01/01/00','None',trunc(sysdate)-trunc(Shipped_date))  DAYS_SINCE_SHIP
	from
	(
		select T1.sku_id SKU, T1.condition_id COND, T1.Rec_date REC_DATE, T1.qty QTY, nvl(T2.shipped_date ,'01-jan-00') Shipped_date, T1.loc_zone
		from
		(
			select inv.sku_id, inv.condition_id, max(trunc(inv.receipt_dstamp)) Rec_date, sum(inv.qty_on_hand) qty, inv.zone_1 loc_zone
			from inventory inv 
			where inv.client_id = 'MOUNTAIN'
      and zone_1 not in ('Z3OUT', 'ZMW', 'AMW', 'ZIBI')
			group by inv.sku_id, inv.condition_id, inv.zone_1
		) T1,
		(
			select i.Sku_id, max (i.dstamp) Shipped_date
			from inventory_transaction i
			where i.client_id = 'MOUNTAIN'
			and i.code = 'Shipment'
      AND TRUNC (I.DSTAMP) >= TRUNC (SYSDATE - '&&2')
			group by i.sku_id
		) T2
		where T1.SKU_ID=T2.SKU_ID (+)
	),
	sku
	where sku.client_id = 'MOUNTAIN'
	and SKU=sku.sku_id
	and Shipped_date <sysdate - '&&2'
	and REC_DATE <sysdate - '&&2'
    and sku.product_group in ('STATIONERY', 'NFS')
	order by Shipped_date,REC_DATE
)
/
--spool off