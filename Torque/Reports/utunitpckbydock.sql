/******************************************************************************/
/*                                                                            */
/*                            Elite                                           */
/*                                                                            */
/*     FILE NAME  :   mwshortages3.sql                                        */
/*                                                                            */
/*   DATE     BY   PROJ     ID         DESCRIPTION                            */
/*   ======== ==== ======   ========   =============                          */
/*   21/03/14 VA   UT                  Units Picked by Ship Dock (Date Range)      */
/******************************************************************************/

/* Input Parameters are as follows */
-- 1)  From Date
-- 2)  To Date


SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 1
SET LINESIZE 400
SET TRIMSPOOL ON
SET HEADING OFF

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


spool utunitpick.csv

select 'Units Picked from &&2 to &&3 - for client ID UT as at '||report_date skip 2

select 'User Id,Ship Dock,Lines,Orders,Qty' from DUAL;

select 
user_id, 
oh.order_type, 
count(*), 
count(distinct reference_id), 
sum(update_qty)
from inventory_transaction itl
inner join order_header oh on oh.order_id = itl.reference_id  and oh.client_id = itl.client_id
where itl.client_id='UT'
and code='Pick'
and itl.to_loc_id NOT IN ('CONTAINER')
and Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by 
user_id, 
oh.order_type
order by 
user_id, 
oh.order_type
/

spool off


