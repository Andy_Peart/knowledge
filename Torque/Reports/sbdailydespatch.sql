/******************************************************************************/
/*                                                                            */
/*                            Elite                                           */
/*                                                                            */
/*     FILE NAME  :    sbdailydespatch.sql                                    */
/*                                                                            */
/*     DESCRIPTION:    Datastream for SweatyBetty Despatches per Store Today  */
/*                                                                            */
/*   DATE     BY   PROJ       ID         DESCRIPTION                          */
/*   ======== ==== ======     ========   =============                        */
/*   30/06/07 LH   SB                   Despatch Note For sweatyBetty	      */
/*   23/11/07 CVH  SB                   Cloned to do daily despatches         */
/*   11/12/07 LH   SB                   Ammended to complete Project          */
/******************************************************************************/

/* Input Parameters are as follows */
-- 1)  GMT
-- 2)  StoreID
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 0
SET VERIFY OFF
SET FEEDBACK OFF
SET TAB OFF

 
-- Set the line length to the maximum possible, but trim 
-- trailing spaces for both screen output and spool files
SET LINESIZE 32767
SET WRAP ON
SET TRIMOUT ON
SET TRIMSPOOL ON

/* Define the format of the output dates */
DEFINE	OutputDateFormat = 'DD-MON-YYYY'
COLUMN	sorter	NOPRINT

SELECT Distinct '0' sorter,
       'DSTREAM_SB_DAILY_HDR'          			||'|'||
	'&&2'			       			||'|'||
	sysdate
from dual
union all
SELECT distinct '1' sorter,
'DSTREAM_SB_DAILY_STORE_HDR'              		||'|'||
rtrim (oh.customer_id)					||'|'||
rtrim (oh.name)						||'|'||
rtrim (oh.address1)					||'|'||
rtrim (oh.address2)					||'|'||
rtrim (oh.town)						||'|'||
rtrim (oh.county)					||'|'||
rtrim (oh.country)					||'|'||
rtrim (oh.postcode)					||'|'||
rtrim (count(distinct(sh.container_id)))	||'|'||
rtrim (sum(sh.qty_shipped))	||'|'||
rtrim (oh.contact_email)
from order_header oh, shipping_manifest sh
where oh.status = 'Shipped'
and oh.client_id = 'SB'
and oh.customer_id = '&&2'
and substr(oh.order_id,1,1) <> 'C'
and to_char(oh.shipped_date,'YYYYMMDD') = to_char(sysdate,'YYYYMMDD')
and sh.order_id = oh.order_id
group by oh.customer_id, oh.name, oh.address1, oh.address2, oh.town, oh.county, oh.country, oh.postcode, oh.contact_email
union all
select distinct '2' || oh.order_id sorter,
'DSTREAM_SB_DAILY_ORD_HDR'				||'|'||
rtrim (oh.customer_id)					||'|'||
rtrim (oh.order_id)					||'|'||
rtrim (oh.purchase_order)				||'|'||
rtrim (oh.user_def_type_6)				||'|'||
rtrim (oh.user_def_type_4)				||'|'||
rtrim (substr(oh.order_id, 3, 7))			||'|'||
rtrim (substr(oh.order_id, 10, 14))			||'|'||
rtrim (instructions)					||'|'||
rtrim (to_char(oh.order_date,'&&OutputDateFormat'))		||'|'||
rtrim (to_char(oh.shipped_date,'&&OutputDateFormat'))
from order_header oh
where oh.status = 'Shipped'
and oh.client_id = 'SB'
and oh.customer_id = '&&2'
and substr(oh.order_id,1,1) <> 'C'
and to_char(oh.shipped_date,'YYYYMMDD') = to_char(sysdate,'YYYYMMDD')
union all
SELECT '2'||oh.order_id || sh.container_id || a.name || description || s.colour || decode(sku_size,'XS',1,'S',2,'M',3,'L',4,'XL',5,'XXL',6,'XXL',sku_size) SORTER,
'DSTREAM_SB_DAILY_LINE'              			||'|'||
rtrim (oh.customer_id)					||'|'||
rtrim (oh.order_id)					||'|'||
rtrim (sh.container_id)       				||'|'||
rtrim (a.name)						||'|'||
rtrim (s.sku_id)					||'|'||
rtrim (s.description)                			||'|'||
rtrim (s.COLOUR)					||'|'||
rtrim (s.SKU_SIZE)					||'|'||
rtrim (sum(sh.qty_shipped))					||'|'||
rtrim (s.user_def_type_5)
from sku s, shipping_manifest sh, order_header oh, address a
where oh.client_id = 'SB'
and to_char(oh.shipped_date,'YYYYMMDD') = to_char(sysdate,'YYYYMMDD')
and oh.order_id = sh.order_id
and oh.customer_id = '&&2'
and substr(oh.order_id,1,1) <> 'C'
and sh.qty_picked > '0'
and sh.sku_id = s.sku_id
and s.client_id = 'SB'
and a.address_id(+) = s.user_def_type_6
group by
oh.customer_id, oh.order_id, sh.container_id, a.name, s.sku_id,
 s.description, s.sku_size, s.colour, s.user_def_type_5,
 decode(sku_size,'XS',1,'S',2,'M',3,'L',4,'XL',5,'XXL',6,'XXL',sku_size)
order by 1,2
/
exit
