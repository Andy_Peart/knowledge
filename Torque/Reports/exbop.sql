SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

Select 'Order ID,Creation Date,Product,Description,Qty Outstanding,Name,Address 1,Address 2,Post Code' from DUAL;

select
DDD||','||
BBB||','||
SSS||','||
DESCR||','||
QQQ||','||
P||','||
Q||','||
R||','||
S
from (select
oh.order_id DDD,
trunc(oh.creation_date) BBB,
ol.sku_id SSS,
sku.user_def_note_1 DESCR,
sum(ol.qty_ordered - nvl(ol.qty_shipped, 0)) QQQ,
oh.name P,
oh.address1 Q,
oh.address2 R,
oh.postcode S
from order_header oh, order_line ol, sku
where oh.client_id = 'EX'
and oh.status not in ('Cancelled')
and oh.creation_date > trunc(sysdate) - 30
and oh.order_id = ol.order_id
and oh.client_id = ol.client_id
and ol.back_ordered = 'Y'
and ol.client_id = sku.client_id
and ol.sku_id = sku.sku_id
group by
oh.order_id,
trunc(oh.creation_date),
ol.sku_id,
sku.user_def_note_1,
oh.name,
oh.address1,
oh.address2,
oh.postcode)
order by
DDD, SSS
/
