SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','
SET WRAP OFF


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500

SELECT 'SKU ID, EAN' A1 FROM DUAL
UNION ALL
SELECT SKU|| ',' || EAN
FROM(
select 
s.SKU_ID SKU, 
s.EAN EAN
from sku s
inner join inventory i on s.sku_id = i.sku_id and s.client_id = i.client_id
inner join location l on i.site_id = l.site_id and i.location_id = l.location_id
where s.Client_id = '&&2'
and
(
i.Location_id = '&&3'
or l.Work_zone = '&&4'
)
)
/