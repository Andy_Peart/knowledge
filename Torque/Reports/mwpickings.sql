SET FEEDBACK OFF                 
set pagesize 66
set linesize 100
set verify off
set wrap off
set newpage 0
set colsep ' '
set und on
set heading on

clear columns
clear breaks
clear computes


column S heading 'Route' format A8
column A heading 'Store' format A8
column B heading 'Store Name' format A28
column C heading 'CLIENT' format A10
column D heading 'Order ID' format A14
column E heading 'STATUS' format A14
column F heading 'DESPATCH DATE' format A16
column G heading 'DELIVERY DATE' format A16
column Q heading 'UNITS' format 99999
column J heading 'CREATION DATE' format A14
column SX format a8 noprint



set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



ttitle  LEFT 'Orders for Consigning for Client &&2 ' RIGHT 'Printed ' report_date skip 2


break on S skip 1 dup

--compute sum of QQQ on CCC

select
ad.user_def_type_1 S,
oh.order_id D,
oh.customer_id A,
ad.name B,
QQQ Q,
oh.status E,
to_char(oh.creation_date, 'DD/MM/YYYY') J
--nvl(QQQ,0) Q
from order_header oh, address ad,
(select
order_id OOO,
sum(qty_to_move) QQQ
from order_header oh, move_task mt
where oh.client_id='&&2'
and mt.client_id='&&2'
and oh.order_id=mt.task_id
group by
order_id)
where oh.client_id='&&2'
and ad.client_id='&&2'
and oh.work_group<>'WWW'
and oh.work_group=ad.address_id
and oh.order_id=OOO
and oh.status in ('Released','Allocated','In Progress')
and (oh.consignment is null or oh.consignment='')
order by
S,A
/

--set heading off
--set pagesize 0

--Select '' from DUAL;
--Select 'END of REPORT' from DUAL;

