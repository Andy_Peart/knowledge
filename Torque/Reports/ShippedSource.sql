SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','
SET UND OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 320




column A format A10
column B format A6
column C format A18
column D format A18
column E format 999999
column F format A10
column G format 999999
column H format A10
column I format A10
column J format A10


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


select 'Order ID,SKU ID,Description,Qty,Pre-Release ID,Supplier ID,,' from DUAL;

--break on report

--compute sum label 'Total'  of G  on report


Select
AA||','||
''''||BB||''','||
CC||','||
DD||','||
nvl(REF,' ')||','||
nvl(ph.supplier_id,'NO PRE_ADVICE')||','||
' '
from (Select
oh.order_id  AA,
it2.SKU_ID BB,
sku.description CC,
it2.tag_id TAG,
sum(it2.update_qty) DD
from order_header oh,inventory_transaction it2,sku
where oh.client_id='&&3'
and oh.client_id=it2.client_id
and oh.client_id=sku.client_id
and oh.order_id='&&2'
and it2.code='Shipment'
and it2.reference_id='&&2'
and it2.sku_id=sku.sku_id
group by
oh.order_id,
it2.SKU_ID,
sku.description,
it2.tag_id),(Select
SKU_ID B1,
tag_id TAG1,
reference_id REF
from inventory_transaction
where client_id='&&3'
and code='Receipt'),pre_advice_header ph
where TAG=TAG1 (+)
and BB=B1 (+)
and REF=ph.pre_advice_id (+)
order by
BB
/

select '  ' from DUAL;
select 'Blank in Pre_Release ID column indicates that the reference field was Blank' from DUAL;
select 'or that the related TAG could not be found' from DUAL;
