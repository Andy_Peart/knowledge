set pagesize 68
set linesize 120
set verify off
set feedback off

ttitle 'Timex Intake Quantity from &&2 to &&3' skip 2

column A heading 'Date' format a8
column B heading 'Qty Receipted' format 99999999

break on report

compute sum label 'Total' of B on report

select to_char (dstamp, 'DD/MM/YY') A,
sum (update_qty) B
from inventory_transaction
where client_id = 'TIMEX'
and station_id not like 'Auto%'
and (code like 'Receipt%' or (code='Adjustment' and reason_id='RECRV'))
and dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY') 
group by to_char(dstamp, 'DD/MM/YY')
order by A
/
