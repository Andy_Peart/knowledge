SET FEEDBACK OFF             
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 900
SET TRIMSPOOL ON

--spool mwocs.csv

--select 'DELIVERY CUSTOMER,DELIVERY ADDRESS1,DELIVERY ADDRESS2,TOWN,COUNTY,DELIVERY POST CODE,DELIVERY COUNTRY,ISO CODE,HARD CODE 0,DELIVERY EMAIL,RP ORDER ID,DATE SHIP,SKU DESCRIPTION,QTY,USER DEF TYPE 1,LINE VALUE,LEA,L,SKU PRODUC,CONTAINER ID,D,company name' from DUAL;

select
A||','||
B||','||
C||','||
D||','||
D1||','||
D2||','||
D3||','||
D4||','||
E||','||
F||','||
G||','||
H||','||
J||','||
J2||','||
J3||','||
J4||','||
J5||','||
J6||','||
K||','||
K1||','||
L||','||
M from (SELECT 
  b.contact A,
  b.address1 B,
  b.address2 C,
  b.town D,
  b.county D1,
  b.postcode D2,
  d.TANDATA_ID D3,
  b.country D4,
  B.CONTACT_PHONE E,
  b.contact_email F,
  'MW'||b.order_id G,
  TRUNC(a.dstamp) H,
  e.description J,
  a.update_qty J2,
  c.user_def_type_2 J3,
  c.line_value J4,
  nvl(C.PRODUCT_CURRENCY,'GBP') J5,
  NULL J6,
  e.product_group K,
   'MW'||b.order_id K1,
  CASE
    WHEN b.priority = '599'
    THEN 'U'
    ELSE 'M'
  END L,
b.name M
FROM inventory_transaction a
INNER JOIN order_header b
ON a.client_id     = b.client_id
AND a.reference_id = b.order_id
INNER JOIN order_line c
ON b.client_id = c.client_id
AND b.order_id = c.order_id
AND c.line_id  = a.line_id
LEFT JOIN country d
ON b.country = d.Iso3_Id
INNER JOIN sku e
ON b.client_id = e.client_id
AND c.sku_ID   = e.sku_ID
INNER JOIN shipping_manifest f
ON c.client_id    = f.client_id
AND c.order_id    = f.order_id
AND c.sku_ID      = f.sku_ID
WHERE a.client_id = 'MOUNTAIN'
AND b.client_id   = 'MOUNTAIN'
AND a.code        = 'Shipment'
AND b.priority   IN (505,599)
AND A.dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1)
/

--spool off


