SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 120

column A heading 'Cust' format a8
column AA heading 'Grp' format a6
column B heading 'Type' format a12
column BB heading 'Type' format a15
column C heading 'Qty' format 999999


--break on A skip 2 on AA dup skip 1 on report

--compute sum label 'Total' of C on A
--compute sum label 'Total' of C on AA
--compute sum label 'Total' of C on report



select 'TR55 - Orders created yesterday - units ordered - '||to_char(sysdate-4,'DD/MM/YYYY') from Dual;

select
distinct 'OT '||nvl(oh.order_type,'Blank') BB,
'END'
FROM   Order_header oh
where oh.client_id='TR'
and trunc(oh.creation_date)=trunc(sysdate-4)
order by BB
/

select
oh.CUSTOMER_ID A,
sku.product_group AA,
nvl(oh.order_type,'Blank') B,
nvl(sum(ol.QTY_ORDERED),0) C
FROM   Order_header oh,Order_line ol,sku
where oh.client_id='TR'
and oh.client_id=ol.client_id
and oh.order_id=ol.order_id
and ol.sku_id=sku.sku_id
and ol.client_id=sku.client_id
and trunc(oh.creation_date)=trunc(sysdate-4)
group by
oh.CUSTOMER_ID,
SKU.PRODUCT_GROUP,
oh.ORDER_TYPE
order by
oh.CUSTOMER_ID,
SKU.PRODUCT_GROUP,
oh.ORDER_TYPE
/


select 'END' from DUAL;


