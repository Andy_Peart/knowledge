SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


--spool mkreceipts.csv

select 'Date,Reference Id,Sku Id,Qty Received,Cartons,Pallets,Definition' from DUAL;

select
A||','||
B||','||
C||','||
D||','||
E||','||
F||','||
G
from (select
trunc(it.Dstamp) A,
it.reference_id B,
it.sku_id C,
it.update_qty D,
CASE
WHEN instr(it.user_def_note_1,'C',1)>0
THEN substr(it.user_def_note_1,1,instr(it.user_def_note_1,'C',1)-1)
ELSE '0' END E,
CASE
WHEN it.user_def_note_1 like '%P%' THEN 1
ELSE 0 END F,
substr(it.user_def_note_1,1,12) G
from inventory_transaction it
where it.client_id='MK'
and it.code like 'Receipt%'
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1)
order by 1
/


--spool off

