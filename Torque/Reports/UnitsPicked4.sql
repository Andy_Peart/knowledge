/******************************************************************************/
/*                                                                            */
/*                            Elite                                           */
/*                                                                            */
/*     FILE NAME  :   mwshortages3.sql                                        */
/*                                                                            */
/*   DATE     BY   PROJ     ID         DESCRIPTION                            */
/*   ======== ==== ======   ========   =============                          */
/*   13/08/07 BK   MM                  Units Picked by Date (Date Range)      */
/******************************************************************************/

/* Input Parameters are as follows */
-- 1)  From Date
-- 2)  To Date


set feedback off
set pagesize 68
set linesize 240
set newpage 0
set verify off

clear columns
clear breaks
clear computes

column A heading 'Date' format A16
column B heading 'Qty' format 999999
column C heading 'Picks' format 999999
column M format a4 noprint

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'Units Picked from '&&2' to '&&3' - for client ID &&4 as at ' report_date skip 2

break on report
compute sum label 'Totals' of C B on report


select
to_char(Dstamp, 'YYMM') M,
to_char(Dstamp, 'DD/MM/YY') A,
count(*) C,
sum(update_qty) B
from inventory_transaction
where client_id='&&4'
and code='Pick'
and elapsed_time>0
and Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by 
to_char(Dstamp, 'YYMM'),
to_char(Dstamp, 'DD/MM/YY')
order by 
to_char(Dstamp, 'YYMM'),
to_char(Dstamp, 'DD/MM/YY')
/

