set pagesize 10
set linesize 80
set verify off
clear columns
clear breaks
clear computes

set heading off
set feedback off

select 'Total number of SKUs on Client ID &&3 with stock less than &&2 = ' || to_char (count(*)) from
(select inv.sku_id C,
sum(qty_on_hand) B,
sum(qty_allocated) D
from inventory inv
where inv.client_id = '&&3'
group by inv.sku_id)
where B < '&&2'
and D=0
/
