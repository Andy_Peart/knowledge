/******************************************************************************/
/*                                                                            */
/*                            Elite                                           */
/*                                                                            */
/*     FILE NAME  :   mwshortages3.sql                                        */
/*                                                                            */
/*   DATE     BY   PROJ     ID         DESCRIPTION                            */
/*   ======== ==== ======   ========   =============                          */
/*   26/06/07 BK   MM                  Units Picked by User ID (Date Range)   */
/******************************************************************************/

/* Input Parameters are as follows */
-- 1)  From Date
-- 2)  To Date


set feedback off
set pagesize 68
set linesize 240
set newpage 0
set verify off

clear columns
clear breaks
clear computes

column X heading '' format A14
column A heading 'User ID' format A20
column B heading 'Qty' format 999999
column C heading 'Picks' format 999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'Units Picked from '&&2' to '&&3' - for client ID MM as at ' report_date skip 2


break on X skip 2 on report
compute sum label 'Totals' of C B on X
compute sum label 'Grand Totals' of C B on report


select
'MM ' X,
user_id A,
count(*) C,
sum(update_qty) B
from inventory_transaction
where client_id='MM'
and code='Pick'
--and elapsed_time>0
and (elapsed_time>0 or (from_loc_id<>'CONTAINER' and to_loc_id<>'CONTAINER'))
and Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and (sku_id not like 'V%'
or sku_id like 'VY%')
group by user_id
union
select
'VH ' X,
user_id A,
count(*) C,
sum(update_qty) B
from inventory_transaction
where client_id='MM'
and code='Pick'
--and elapsed_time>0
and (elapsed_time>0 or (from_loc_id<>'CONTAINER' and to_loc_id<>'CONTAINER'))
and Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and sku_id like 'V%'
and sku_id not like 'VY%'
group by user_id
order by X,A
/

