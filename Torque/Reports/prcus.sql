SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET COLSEP ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET HEADING OFF
SET PAGESIZE 0
SET LINESIZE 200
SET TRIMSPOOL ON

column A heading 'Category' format A12
column C heading 'Customer' format A12
column B heading 'Qty' format 9999999

break on report 
compute sum label '   Total' of B D E F G H J K on report

--spool prcus.csv

select 'Prominent Stock Holding by Customer' from DUAL;
select to_char(SYSDATE, 'DD/MM/YY    HH:MI:SS') from DUAL;

select '' from DUAL;
select 'HANGING' from DUAL;

select 'Customer,Total,Jackets,Suits,Trousers,Waistcoats,Shirts,Coats,Others' from Dual;

select
C,B,D,G,E,F,H,J,
B-D-E-F-G-H-J K
from (select
sku.user_def_type_2 C,
sum(i.qty_on_hand*nvl(sku.user_def_type_9,1)) B,
nvl(B1,0) D,
nvl(B2,0) E,
nvl(B3,0) F,
nvl(B4,0) G,
nvl(B5,0) H,
nvl(B6,0) J
from inventory i,sku,(select
sku.user_def_type_2 C1,
sum(i.qty_on_hand*nvl(sku.user_def_type_9,1)) B1
from inventory i,sku
where i.client_id like 'PR'
and i.site_id like 'PROM'
and i.zone_1 like 'LSH1'
and i.location_id<>'REJECT'
and sku.client_id=i.client_id
and sku.sku_id=i.sku_id
and ((sku.user_def_type_2='TML' and sku.user_def_type_8='J')
or (sku.user_def_type_2<>'TML' and (sku.description like 'JK%'
or (substr(sku.description,3,1)<>' ' and substr(sku.sku_id,6,1)='J'))))
group by
sku.user_def_type_2),(select
sku.user_def_type_2 C2,
sum(i.qty_on_hand*nvl(sku.user_def_type_9,1)) B2
from inventory i,sku
where i.client_id like 'PR'
and i.site_id like 'PROM'
and i.zone_1 like 'LSH1'
and i.location_id<>'REJECT'
and sku.client_id=i.client_id
and sku.sku_id=i.sku_id
and ((sku.user_def_type_2='TML' and sku.user_def_type_8='T')
or (sku.user_def_type_2<>'TML' and (sku.description like 'TR%'
or (substr(sku.description,3,1)<>' ' and substr(sku.sku_id,6,1)='T'))))
group by
sku.user_def_type_2),(select
sku.user_def_type_2 C3,
sum(i.qty_on_hand*nvl(sku.user_def_type_9,1)) B3
from inventory i,sku
where i.client_id like 'PR'
and i.site_id like 'PROM'
and i.zone_1 like 'LSH1'
and i.location_id<>'REJECT'
and sku.client_id=i.client_id
and sku.sku_id=i.sku_id
and ((sku.user_def_type_2='TML' and sku.user_def_type_8='WC')
or (sku.user_def_type_2<>'TML' and (sku.description like 'WC%'
or (substr(sku.description,3,1)<>' ' and substr(sku.sku_id,6,1)='W'))))
group by
sku.user_def_type_2),(select
sku.user_def_type_2 C4,
sum(i.qty_on_hand*nvl(sku.user_def_type_9,1)) B4
from inventory i,sku
where i.client_id like 'PR'
and i.site_id like 'PROM'
and i.zone_1 like 'LSH1'
and i.location_id<>'REJECT'
and sku.client_id=i.client_id
and sku.sku_id=i.sku_id
and ((sku.user_def_type_2='TML' and sku.user_def_type_8='S')
or (sku.user_def_type_2<>'TML' and (sku.description like 'SU%'
or (substr(sku.description,3,1)<>' ' and substr(sku.sku_id,6,1)='S'))))
group by
sku.user_def_type_2),(select
sku.user_def_type_2 C5,
sum(i.qty_on_hand*nvl(sku.user_def_type_9,1)) B5
from inventory i,sku
where i.client_id like 'PR'
and i.site_id like 'PROM'
and i.zone_1 like 'LSH1'
and i.location_id<>'REJECT'
and sku.client_id=i.client_id
and sku.sku_id=i.sku_id
and ((sku.user_def_type_2='TML' and sku.user_def_type_8='X')
or (sku.user_def_type_2<>'TML' and sku.description like 'SH%'))
group by
sku.user_def_type_2),(select
sku.user_def_type_2 C6,
sum(i.qty_on_hand*nvl(sku.user_def_type_9,1)) B6
from inventory i,sku
where i.client_id like 'PR'
and i.site_id like 'PROM'
and i.zone_1 like 'LSH1'
and i.location_id<>'REJECT'
and sku.client_id=i.client_id
and sku.sku_id=i.sku_id
and ((sku.user_def_type_2='TML' and sku.user_def_type_8='C')
or (sku.user_def_type_2<>'TML' and sku.description like 'CT%'))
group by
sku.user_def_type_2)
where i.client_id like 'PR'
and i.site_id like 'PROM'
and i.zone_1 like 'LSH1'
and i.location_id<>'REJECT'
and sku.client_id=i.client_id
and sku.sku_id=i.sku_id
and sku.user_def_type_2=C1 (+)
and sku.user_def_type_2=C2 (+)
and sku.user_def_type_2=C3 (+)
and sku.user_def_type_2=C4 (+)
and sku.user_def_type_2=C5 (+)
and sku.user_def_type_2=C6 (+)
group by
sku.user_def_type_2,
nvl(B1,0),
nvl(B2,0),
nvl(B3,0),
nvl(B4,0),
nvl(B5,0),
nvl(B6,0))
order by C
/







--spool off
