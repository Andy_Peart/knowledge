SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 300
SET TRIMSPOOL ON



spool stdunder6.csv


select 'Location,SKU ID,Colour,Size,Description,Available,Last Count' from DUAL;

select
distinct
i.location_id||','||
''''||sku.sku_id||''','||
sku.colour||','''||
sku.sku_size||''','||
sku.description||','||
CCC||','||
to_char(i.count_dstamp,'DD/MM/YYYY')
from inventory i,sku,
(select
iv.sku_id BBB,
sum(iv.qty_on_hand - iv.qty_allocated) CCC
from inventory iv,location L
where iv.client_id='&&3'
and iv.location_id like '&&2%'
and iv.location_id=L.location_id
and iv.site_id=L.site_id
and L.loc_type='Tag-FIFO'
group by
iv.sku_id),location L
where i.client_id='&&3'
and i.location_id like '&&2%'
and i.sku_id=BBB
and sku.sku_id=BBB
and sku.client_id='&&3'
and CCC<6
and i.location_id=L.location_id
and i.site_id=L.site_id
and L.loc_type='Tag-FIFO'
order by
1
/


spool off
