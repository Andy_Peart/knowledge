SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 900
SET TRIMSPOOL ON
 

column A heading 'Location' format A11
column B heading 'SKU' format A13
column C heading 'Description' format A40
column C1 heading 'Size' format A40
column C2 heading 'Colour' format A40
column C3 heading 'Range' format A40
column C4 heading 'Season' format A40
column D heading 'Qty' format 9999999
column E heading '' format A14
column F heading 'Category' format A30
column G heading 'Collection' format A30

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

break on report

compute sum label 'Total' of D on report

select 'SKU,Description,Size,Colour,Range,Season,Location,Qty,Category,Collection' from DUAL;

select
i.sku_id B,
Sku.description C,
sku_size C1,
sku.colour C2,
sku.user_def_type_5 C3,
sku.user_def_type_3 C4,
i.location_id A,
sum(i.qty_on_hand) D,
sku.stack_description F,
sku.COMMODITY_DESC G,
' '
from inventory i,sku
where i.client_id like '&&4'
and location_id>='&&2'
and location_id<='&&3'
and i.sku_id=sku.sku_id
and i.client_id=sku.client_id
group by
i.sku_id,
Sku.description,
sku_size,
sku.colour,
sku.user_def_type_5,
sku.user_def_type_3,
i.location_id,
sku.stack_description,
sku.COMMODITY_DESC
order by 
i.sku_id,
Sku.description,
sku_size,
sku.colour,
i.location_id
/
