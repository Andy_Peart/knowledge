SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9999
SET TRIMSPOOL ON



--spool mwlsum2.csv

Select 'MOUNTAIN Stock with Qty_on_hand less than &&2' from DUAL;

select 'Sku,Description,Work Zone,Location,Qty on Hand,Qty Allocated' from DUAL;

select
''''||C||'''',
sku.description,
ll.work_zone C1,
''''||C2||'''',
B,
D
from (select
inv.sku_id C,
inv.zone_1 C1,
inv.location_id C2,
qty_on_hand B,
qty_allocated D
from inventory inv,(select
A1
from (select inv.sku_id A1,
sum(qty_on_hand) B1
from inventory inv
where client_id = 'MOUNTAIN'
group by inv.sku_id)
where B1 < '&&2')
where inv.client_id = 'MOUNTAIN'
and inv.sku_id=A1),sku,location ll
where C=sku.sku_id
and sku.client_id='MOUNTAIN'
and C2=ll.location_id
and ll.site_id='BD2'
order by
C,C1,C2
/

--spool off

