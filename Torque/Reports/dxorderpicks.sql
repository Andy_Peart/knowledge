set feedback off
set verify off
set newpage 0


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

set pagesize 66
set linesize 200
set trimspool on
 

column A heading 'Order ID' format A20
column B heading 'Order Type' format A12
column C heading 'Location' format A12
column D heading 'SKU' format A18
column E heading 'SKU/Description' format A40
column F heading 'Qty' format 999999
column G heading 'Instructions' format A32
column H heading 'Tick' format A6
column R heading 'Delivery Address' format A40
column S heading 'Invoice Address' format A36


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

set und '-'
set heading on
set newpage 0

ttitle  LEFT 'PICK SHEET in product order for Consignment &&2 and client Daxbourne as at ' report_date RIGHT 'Page:' FORMAT 999 SQL.PNO skip 2


--break on report

--compute sum label 'Total' of D on report


--set pagesize 0

--select 'Order ID             Order Type   Location     SKU                SKU/Description                              Qty Tick   Instructions' from DUAL;
--select '--------             ----------   --------     ---                ---------------                              --- ----   ------------' from Dual;
--select '' from Dual;

break on A dup skip page on G on R 

select
rpad(oh.customer_id,40,' ')||
rpad(name,40,' ')||
rpad(address1,40,' ')||
rpad(address2,40,' ')||
rpad(town,40,' ')||
rpad(county,40,' ')||
rpad(postcode,40,' ')||
rpad('========',40,' ')||
DECODE(oh.user_def_note_2,'','',rpad('PLEASE READ INSTRUCTIONS',40,' '))||
rpad('========',40,' ')||
upper(oh.user_def_note_2) R,
mt.task_id A,
oh.order_type B,
mt.from_loc_id C,
rpad(mt.sku_id,40,' ')||
sku.description E,
mt.qty_to_move F,
'______'  H
from order_header oh,order_line ol,sku,move_task mt
where oh.client_id='DX'
and oh.order_id=ol.order_id
and oh.order_id=mt.task_id
--and oh.status<>'Shipped'
and oh.consignment='&&2'
and ol.sku_id=sku.sku_id
and ol.sku_id=mt.sku_id
order by
mt.task_id,
mt.sku_id
/


