SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240


column A heading 'Order Type' format A12
column AA heading 'Order' format A12
column EE heading 'Web ID' format A10
column BB heading 'Date' format A14
column SKU heading 'SKU ID' format A12
column CC heading 'Order|Qty' format 999999
column CCC heading 'Tasked|Qty' format 999999

select 'Order Type,Web ID,Order ID,Date,SKU,Ordered,Tasked' from DUAL;

--break on A dup skip 1



select
DDD A,
substr(EEE,9,8) EE,
AAA AA,
BBB BB,
SSS SKU,
OOO CC ,
TTT CCC from
(select
oh.order_type  DDD,
oh.order_id  AAA,
oh.user_def_type_7 EEE,
ol.sku_id SSS,
trunc(oh.creation_date) BBB,
nvl(sum(ol.qty_ordered),0) OOO,
nvl(sum(ol.qty_tasked),0) TTT
from order_header oh, order_line ol
where oh.client_id='SB'
and oh.status in ('Allocated','Released')
and oh.order_id like 'C%'
and oh.order_id=ol.order_id
and oh.order_type like '&&2%'
group by
oh.order_type,
oh.order_id,
oh.user_def_type_7,
ol.sku_id,
trunc(oh.creation_date))
where OOO<>TTT
order by
A,BB,AA
/

set heading off

select 'No Orders' from
(select
count(*) ct from
(select
oh.order_type  DDD,
oh.order_id  AAA,
ol.sku_id SSS,
trunc(oh.creation_date) BBB,
nvl(sum(ol.qty_ordered),0) OOO,
nvl(sum(ol.qty_tasked),0) TTT
from order_header oh, order_line ol
where oh.client_id='SB'
and oh.status in ('Allocated','Released')
and oh.order_id like 'C%'
and oh.order_id=ol.order_id
and oh.order_type like '&&2%'
group by
oh.order_type,
oh.order_id,
ol.sku_id,
trunc(oh.creation_date))
where OOO<>TTT)
where ct=0
/
