SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON

select 'Date,User,Qty of Return Units' from DUAL
union all
select  A || ',' || B || ',' || C  from 
(
Select 
 to_char(it.dstamp, 'DD-MON-YYYY') as "A"
, it.user_id as "B"
, sum(it.update_qty) as "C"
FROM
	inventory_transaction it
WHERE 
it.client_id        = 'IS'
And it.code = 'Return'
AND it.dstamp between to_date('&&3', 'DD-MM-YYYY') and to_date('&&4', 'DD-MM-YYYY')+1
group by 
to_char(it.dstamp, 'DD-MON-YYYY')
, it.user_id
 order by A
)
/