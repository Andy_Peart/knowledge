--SET FEEDBACK OFF           
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 999
SET TRIMSPOOL ON

update inventory_transaction
set uploaded='T'
where client_id = 'MOUNTAIN' 
and code = 'Shipment' 
and uploaded = 'N' 
and work_group <> 'WWW'
/

SELECT
                client_id "client_id",
                code "code",
                to_char(dstamp,'YYYYMMDDHH24MISS') "dstamp",
                original_qty "original_qty",
                decode(sign(original_qty), 1, '+', -1, '-', '+') "original_qty_sign",
                reference_id "reference_id",
                sku_id "sku_id",
                update_qty "update_qty",
                decode(sign(update_qty), 1, '+', -1, '-', '+') "update_qty_sign",
                from_loc_id "from_loc_id",
                to_loc_id "to_loc_id",
                final_loc_id "final_loc_id",
                tag_id "tag_id",
                line_id "line_id",
                condition_id "condition_id",
                notes "notes",
                reason_id "reason_id",
                batch_id "batch_id",
                to_char(expiry_dstamp,'YYYYMMDDHH24MISS') "expiry_dstamp",
                user_id "user_id",
                shift "shift",
                station_id "station_id",
                site_id "site_id",
                container_id "container_id",
                pallet_id "pallet_id",
                list_id "list_id",
                owner_id "owner_id",
                origin_id "origin_id",
                work_group "work_group",
                consignment "consignment",
                to_char(manuf_dstamp,'YYYYMMDDHH24MISS') "manuf_dstamp",
                lock_status "lock_status",
                qc_status "qc_status",
                session_type "session_type",
                summary_record "summary_record",
                elapsed_time "elapsed_time",
                supplier_id "supplier_id",
                user_def_type_1 "user_def_type_1",
                user_def_type_2 "user_def_type_2",
                user_def_type_3 "user_def_type_3",
                user_def_type_4 "user_def_type_4",
                user_def_type_5 "user_def_type_5",
                user_def_type_6 "user_def_type_6",
                user_def_type_7 "user_def_type_7",
                user_def_type_8 "user_def_type_8",
                user_def_chk_1 "user_def_chk_1",
                user_def_chk_2 "user_def_chk_2",
                user_def_chk_3 "user_def_chk_3",
                user_def_chk_4 "user_def_chk_4",
                to_char(user_def_date_1,'YYYYMMDDHH24MISS') "user_def_date_1",
                to_char(user_def_date_2,'YYYYMMDDHH24MISS') "user_def_date_2",
                to_char(user_def_date_3,'YYYYMMDDHH24MISS') "user_def_date_3",
                to_char(user_def_date_4,'YYYYMMDDHH24MISS') "user_def_date_4",
                user_def_num_1 "user_def_num_1",
                user_def_num_2 "user_def_num_2",
                user_def_num_3 "user_def_num_3",
                user_def_num_4 "user_def_num_4",
                user_def_note_1 "user_def_note_1",
                user_def_note_2 "user_def_note_2",
                from_site_id "from_site_id",
                to_site_id "to_site_id",
                ' ' "time_zone_name",
                job_id "job_id",
                job_unit "job_unit",
                manning "manning",
                spec_code "spec_code",
                config_id "config_id",
                estimated_time "estimated_time",
                task_category "task_category",
                sampling_type "sampling_type",
                to_char(complete_dstamp,'YYYYMMDDHH24MISS') "complete_dstamp",
                grn "grn",
                group_id "group_id",
                uploaded "uploaded",
                uploaded_vview "uploaded_vview",
                uploaded_ab "uploaded_ab",
                sap_idoc_type "sap_idoc_type",
                sap_tid "sap_tid",
                ce_orig_rotation_id "ce_orig_rotation_id",
                ce_rotation_id "ce_rotation_id",
                ce_consignment_id "ce_consignment_id",
                ce_receipt_type "ce_receipt_type",
                ce_originator "ce_originator",
                ce_originator_reference "ce_originator_reference",
                ce_coo "ce_coo",
                ce_cwc "ce_cwc",
                ce_ucr "ce_ucr",
                ce_under_bond "ce_under_bond",
                to_char(ce_document_dstamp,'YYYYMMDDHH24MISS') "ce_document_dstamp",
                uploaded_customs "uploaded_customs",
                uploaded_labor "uploaded_labor",
                asn_id "asn_id",
                customer_id "customer_id",
                print_label_id "print_label_id",
                lock_code "lock_code",
                ship_dock "ship_dock",
                ce_duty_stamp "ce_duty_stamp",
                pallet_grouped "pallet_grouped",
                key "key"
from inventory_transaction
where client_id = 'MOUNTAIN' 
and code = 'Shipment' 
and uploaded = 'T' 
and work_group <> 'WWW'
/


update inventory_transaction
set uploaded='Y'
where client_id = 'MOUNTAIN' 
and code = 'Shipment' 
and uploaded = 'T' 
and work_group <> 'WWW'
/

commit;

