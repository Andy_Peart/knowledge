SET FEEDBACK OFF             
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240
SET TRIMSPOOL ON

--spool slowmovers.csv

select 'Sku Code,Qty,CIMS,Description,Range,Style,Colour,Size,Last Rect,Last Pick'
from dual;

break on report

compute sum label 'Total' of QQ on report


Select
''''||PA ||''','||
QQ  ||','||
sku.user_def_type_2||'-'||
sku.user_def_type_1||'-'||
sku.user_def_type_4||','||
'"'||sku.description||'",'||
sku.user_def_type_5 ||','||
--sku.user_def_type_3 ||','||
substr(PA,1,6) ||','||
sku.colour  ||','||
sku.sku_size  ||','||
to_char(PD, 'DD/MM/YY')  ||','||
DECODE(to_char(PB, 'DD/MM/YY'),'01/01/00',' ',to_char(PB, 'DD/MM/YY'))
--to_char(PB, 'DD/MM/YY')
from 
(select
PA,
PD,
QQ,
nvl(BB,'01-jan-00') PB
from
(select sku_id PA,
max(trunc(receipt_dstamp)) PD,
sum(qty_on_hand) QQ
from inventory
where client_id = '&&3'
and condition_id = 'GOOD'
and location_id<>'SUSPENSE'
group by sku_id),
(select i.Sku_id PP,
max (i.dstamp) BB
from inventory_transaction i
where i.client_id = '&&3'
and i.code = 'Pick'
group by i.sku_id)
where PA=PP (+)),sku
where PB <sysdate - '&&2'
and PD <sysdate - '&&2'
and PA=sku.sku_id
and sku.client_id = '&&3'
order by PA
/

--spool off
