SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON

break on report
compute sum of Q on report

--spool mwinvrecratio.csv

select 'By Week,Percentage Shipped' from Dual;

select
BB3*10,
to_char((QQ3*100)/QQ,'990.00') QQ4
from (select
BB3,
sum(QQ) QQ,
sum(QQ3) QQ3
from (select
TT,
BB3,
QQ,
sum(QQ2) QQ3
from (select
TT,
--BB2-BB+1 BB3,
ceil((BB2-BB+1)/70) BB3,
QQ,
QQ2 from (select
i.tag_id TT,
i.sku_id SK,
trunc(i.dstamp) BB2,
i.update_qty QQ2
from inventory_transaction i
where i.client_id = 'MOUNTAIN'
and i.code = 'Shipment'),(select
i.tag_id TT2,
i.sku_id SK2,
trunc(i.dstamp) BB,
i.update_qty QQ
from inventory_transaction i
where i.client_id = 'MOUNTAIN'
and i.code = 'Receipt')
where TT=TT2
and SK=SK2)
group by
TT,
BB3,
QQ)
group by BB3)
where BB3<10
order by BB3
/



--spool off

