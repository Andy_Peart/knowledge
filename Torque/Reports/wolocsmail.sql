
SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 300
SET TRIMSPOOL ON
 
column XX NOPRINT



--spool wolocsmail.csv



select
oh.order_id XX,
oh.Name||','||
oh.Address1||','||
oh.Address2||','||
oh.Town||','||
oh.County||','||
oh.PostCode||','||
cty.tandata_id||','||
oh.Country||','||
'0'||','||
oh.contact_email||','||
oh.Order_id||','||
to_char(oh.Shipped_Date,'DD/MM/YYYY HH24:MI')||','||
sku.description||','||
it.update_qty||','||
ol.User_def_type_1||','||
ol.line_Value||','||
' '||','||
' '||','||
sku.product_group||','||
it.container_id||','||
oh.dispatch_method||','||
' '
from inventory_transaction it,order_header oh,order_line ol,sku,country cty
where it.client_id='WOL'
and it.code='Shipment'
and trunc(it.dstamp)=trunc(sysdate)
and oh.client_id='WOL'
and oh.dispatch_method in ('P','U','M')
and oh.order_id=it.reference_id
and ol.client_id='WOL'
and ol.order_id=oh.order_id
and ol.sku_id=sku.sku_id
and ol.line_id=it.line_id
and oh.Country=cty.ISO3_ID
order by XX
/


--spool off

