SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON

--spool mkordersbyseller.csv

select 'Seller,Type,Order,Status,Order Date,Creation Date,Name,E Mail Address' from DUAL;

select
oh.seller_name||','||
oh.order_type||','||
oh.order_id||','||
oh.status||','||
trunc(oh.order_date)||','||
to_char(oh.creation_date,'DD-MON-YY HH24:MI:SS')||','||
oh.contact||','||
oh.contact_email||','||
oh.user_def_type_4
from order_header oh
where oh.client_id = 'MK'
and status not in ('Shipped','Cancelled')
and oh.priority=3
and oh.creation_date>to_date('&&2','DD-MON-YYYY')
order by
oh.seller_name,
oh.order_id
/

--spool off
