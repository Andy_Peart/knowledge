SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 200
set trimspool on
set newpage 0
set verify off
set colsep ','



break on report
compute sum label 'Totals' of D on report


--spool skupicks.csv

select 'WEB picks from &&2 to &&3' from DUAL;
select 'Sku Id,Units Picked' from DUAL;

select
''''||sku_id A,
sum (update_qty) D
from inventory_transaction
where client_id = 'MOUNTAIN'
and code = 'Pick'
and work_group='WWW'
and elapsed_time>0
and dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')
group by
sku_id
order by 
D DESC, A
/

--spool off
