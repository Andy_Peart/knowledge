SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Time scanned,Order id,Scanned Pallet,Carrier,User id' from dual
union all
select "Date" ||','|| "Order #" ||','|| "To Pallet" ||','|| "Carrier" ||','|| "User"
from
(
/* Ensures only the last scan will be picked up */
with latest as (
select max(to_char(start_dstamp, 'DD-MON-YYYY HH24:MI:SS')) as "Date"
, container_id as "Order #"
from torque.fn_tq_movepallet
where start_dstamp > trunc(sysdate)-30
group by container_id
)
select to_char(mp.start_dstamp, 'DD-MON-YYYY HH24:MI:SS') as "Date"
, mp.container_id as "Order #"
, mp.to_pal as "To Pallet"
, OH.CARRIER_ID as "Carrier"
, mp.user_id as "User"
from torque.fn_tq_movepallet mp
inner join dcsdba.order_header oh on oh.order_id = mp.container_id
inner join latest on latest."Date" = to_char(mp.start_dstamp, 'DD-MON-YYYY HH24:MI:SS') and latest."Order #" = mp.container_id
where mp.to_pal in (
        select regexp_substr('&&2','[^,]+',1,level) from dual
        connect by regexp_substr('&&2','[^,]+',1,level) is not null
)
and oh.client_id = 'TR'
order by mp.to_pal
, to_char(mp.start_dstamp, 'DD-MON-YYYY HH24:MI:SS')
)
/
--spool off