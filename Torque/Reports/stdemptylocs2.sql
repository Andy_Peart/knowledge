SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240
SET TRIMSPOOL ON


select 'Site,Location,Loc Type,Check String,Lock Status,In Stage,Out Stage,Pick Sequence'
from dual;

select
site_id ||','||
location_id ||','||
loc_type ||','||
check_string ||','||
lock_status ||','||
in_stage ||','||
out_stage ||','||
pick_sequence
from location
where current_volume=0
and nvl(alloc_volume,0)=0
and zone_1='&&2'
and site_id='&&3'
and lock_status='UnLocked'
order by location_id
/
