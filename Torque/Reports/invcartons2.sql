SET FEEDBACK OFF             
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 300
SET TRIMSPOOL ON

--spool invcartons.csv

select 'SKU,Description,Qty per Carton,Units,Cartons,LPG' from DUAL;

select
SK||','||
SKD||','||
Q4||','||
IQTY||','||
to_char(IQTY/Q4,'9990')||','||
LPG
from (select
sku_id ISKU,
sum(qty_on_hand) IQTY
from inventory
where client_id='MOUNTAIN'
group by
sku_id),(select
SK,
SKD,
LPG,
Q3,
to_char(Q3/CT,'9990') Q4
from (select
SK,
SKD,
LPG,
count(*) CT,
sum(to_number(Q3)) Q3
from (select
PID,
LPG,
QQ,
QU,
CASE
WHEN QQ=0 THEN '0'
ELSE to_char(QU/QQ,'9990') END Q3
from (select
PID,
LPG,
sum(QU) QU,
sum(mo.cartons) QQ
from mountainorders mo,(select
ph.pre_advice_id  PID,
sku.user_def_type_8 LPG,
--pl.sku_id SK,
sum(pl.qty_due) QU
from pre_advice_header ph,pre_advice_line pl,sku
where ph.client_id='MOUNTAIN'
and ph.pre_advice_id=pl.pre_advice_id
and pl.client_id='MOUNTAIN'
and sku.client_id = 'MOUNTAIN'
and sku.sku_id=pl.sku_id
group by
ph.pre_advice_id,
sku.user_def_type_8
union
select
ph.pre_advice_id  PID,
sku.user_def_type_8 LPG,
--pl.sku_id SK,
sum(pl.qty_due) QU
from pre_advice_header_archive ph,pre_advice_line_archive pl,sku
where ph.client_id='MOUNTAIN'
and ph.pre_advice_id=pl.pre_advice_id
and pl.client_id='MOUNTAIN'
and sku.client_id = 'MOUNTAIN'
and sku.sku_id=pl.sku_id
group by
ph.pre_advice_id,
sku.user_def_type_8)
where mo.cw_order=PID
group by
PID,
LPG)),(select
pl.pre_advice_id  PID2,
pl.sku_id SK,
sku.description SKD
from pre_advice_line pl,sku
where pl.client_id='MOUNTAIN'
and sku.client_id='MOUNTAIN'
and pl.sku_id=sku.sku_id
union
select
pl.pre_advice_id  PID2,
pl.sku_id SK,
sku.description SKD
from pre_advice_line_archive pl,sku
where pl.client_id='MOUNTAIN'
and sku.client_id='MOUNTAIN'
and pl.sku_id=sku.sku_id)
Where PID=PID2
group by
SK,
SKD,
LPG))
where ISKU=SK
order by
SK
/

--spool off
