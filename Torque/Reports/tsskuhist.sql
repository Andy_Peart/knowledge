SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 800
SET TRIMSPOOL ON


column A  heading 'Code' format a18
column B  heading 'SKU' format a50
column C heading 'Units' format 999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON




break on report

compute sum label 'Available' of C on report

select 'Code, SKU, Units' from DUAL;

with T1 as (
select code, sku_id, sum(update_qty) qty
from inventory_transaction
where client_id = 'TS'
and sku_id = '&&2'
and code in ('Pick','Receipt', 'Adjustment', 'Receipt Reverse')
and update_qty <> 0
and from_loc_id <> 'CONTAINER'
group by code, sku_id
union all
select code, sku_id, sum(update_qty) qty
from inventory_transaction_archive
where client_id = 'TS'
and sku_id = '&&2'
and code in ('Pick','Receipt', 'Adjustment', 'Receipt Reverse')
and update_qty <> 0
and from_loc_id <> 'CONTAINER'
group by code, sku_id
)
select code A, sku_id B, 
case 
  when code = 'Pick' then sum(qty)*-1
  else sum(qty)
end C
from T1
group by code, sku_id
/
--spool off