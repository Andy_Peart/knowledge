SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Client id,Receipt Date,Location,Location Type,Sku id,Qty Receipted,Receipt id' from dual
union all
select "Client" ||','|| "Receipt Date" ||','|| "Location" ||','|| "Loc Type" ||','|| "Sku id" ||','|| "Qty Receipted" ||','|| "Receipt id"
from
(
select trunc(i.receipt_dstamp)  as "Receipt Date"
, i.location_id                 as "Location"
, i.sku_id                      as "Sku id"
, sum(i.qty_on_hand)            as "Qty Receipted"
, nvl(i.receipt_id,'Blind Receipt') as "Receipt id"
, i.client_id                   as "Client"
, l.loc_type                    as "Loc Type"
from inventory i
inner join location l on l.site_id = i.site_id and l.location_id = i.location_id
where i.client_id = '&&2'
and l.loc_type in ('Receive Dock', 'Sampling')
and l.location_id not like '%BAD%'
group by trunc(i.receipt_dstamp)
, i.location_id
, i.sku_id
, i.receipt_id
, i.client_id
, l.loc_type
order by 1 asc
)
/
--spool off