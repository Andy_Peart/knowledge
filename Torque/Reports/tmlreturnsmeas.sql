SET FEEDBACK OFF                 
set pagesize 68
set linesize 132
set verify off
set wrap off
set newpage 0

clear columns
clear breaks
clear computes


column A heading 'Task Type' format a18
column B heading 'User ID' format a18
column C heading 'Tasks' format 99999999
column D heading 'Units' format 99999999
column E heading 'Elapsed Time' format 99999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'Total Tasks by User ID from &&3 to &&4 - for client ID &&2 as at ' report_date RIGHT 'Page:'FORMAT 999 SQL.PNO skip 2 

BREAK ON A SKIP 1
COMPUTE SUM OF C D E ON A;

with T1 as (
  select key, CODE, list_id, user_id,client_id, update_qty,elapsed_time,Dstamp,TO_LOC_ID,FROM_LOC_ID, reference_id, reason_id  from inventory_transaction
)
SELECT
case
  when to_loc_id not like 'HANG%' and  to_loc_id not like 'Z%' and to_loc_id not like 'HOLDRET%' then 'Box'
  else 'Hang'
end A,
user_id B,
count(client_id) C,
sum(update_qty) D,
sum(nvl(elapsed_time,0)) E
from T1
where client_id='&&2'
and dstamp between to_date('&&3', 'DD-MON-YYYY') and  to_date('&&4', 'DD-MON-YYYY')+1
and update_qty<>0
and nvl(reason_id,'X') not in ('T760','RECRV','DAMGD') and update_qty<>0
	and code='Receipt' 
	and (reference_id like '%/%' or reference_id like 'T___' or reference_id like 'H___' or  reference_id = 'R' or  reference_id = 'M'  or  reference_id like 'RET%')
	and to_loc_id not like 'HOLDRET%'
	and to_loc_id not like 'HANG%'	
	and to_loc_id not like 'Z%'	
GROUP BY CODE,USER_ID,
case
  when to_loc_id not like 'HANG%' and to_loc_id not like 'Z%' and to_loc_id not like 'HOLDRET%' then 'Box'
  else 'Hang'
end
union all
SELECT
case
  when to_loc_id not like 'HANG%' and to_loc_id not like 'Z%' and to_loc_id not like 'HOLDRET%' then 'Box'
  else 'Hang'
end A,
user_id B,
count(client_id) C,
sum(update_qty) D,
sum(nvl(elapsed_time,0)) E
from T1
where client_id='&&2'
and dstamp between to_date('&&3', 'DD-MON-YYYY') and  to_date('&&4', 'DD-MON-YYYY')+1
and update_qty<>0
and nvl(reason_id,'X') not in ('T760','RECRV','DAMGD') and update_qty<>0
	and code='Receipt' 
	and (reference_id like '%/%' or reference_id like 'T___' or reference_id like 'H___' or reference_id = 'R' or reference_id = 'M'  or  reference_id like 'RET%')
and (to_loc_id like 'HOLDRET%' or to_loc_id like 'HANG%' or to_loc_id like 'Z%')
GROUP BY CODE,USER_ID,
case
  when to_loc_id not like 'HANG%' and to_loc_id not like 'Z%' and to_loc_id not like 'HOLDRET%' then 'Box'
  else 'Hang'
end
union all
SELECT  'Box' A,
user_id B,
count(client_id) C,
sum(update_qty) D,
sum(nvl(elapsed_time,0)) E
from T1
where client_id='&&2'
and dstamp between to_date('&&3', 'DD-MON-YYYY') and  to_date('&&4', 'DD-MON-YYYY')+1
and update_qty<>0
and code='Return' 
GROUP BY CODE,USER_ID
/