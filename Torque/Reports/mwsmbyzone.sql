SET FEEDBACK OFF             
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 300
SET TRIMSPOOL ON

--spool mwsmbyzone.csv

--select to_char(SYSDATE, 'DD/MM/YY  HH:MI:SS') curdate from DUAL;

select 'Sku Code,Description,Locations,Received Date,Qty on Hand,Last Picked' from dual;

select
''''||PA||''','||
DD||','||
LL||','||
CB||','||
QQ||','||
--PB
DECODE(PB,'01-JAN-00','None',PB) 
from (select
PA,
DD,
LL,
CB,
QQ,
nvl(BB,'01-jan-00') PB
from
(select i.sku_id PA,
i.description DD,
LISTAGG(i.location_id||'('||i.qty_on_hand||')', ':') WITHIN GROUP (ORDER BY i.location_id) AS LL,
max(trunc(i.receipt_dstamp)) CB,
sum(i.qty_on_hand) QQ
from inventory i,location L
where i.client_id = 'MOUNTAIN'
and i.location_id=L.location_id
and i.site_id=L.site_id
and L.work_zone='&&3'
group by i.sku_id,i.description,i.location_id),
(select i.Sku_id PP,
max(trunc(i.dstamp)) BB
from inventory_transaction i,location L
where i.client_id = 'MOUNTAIN'
and i.code = 'Pick'
and elapsed_time>0
and i.from_loc_id=L.location_id
and i.site_id=L.site_id
and L.work_zone='&&3'
group by i.sku_id)
where PA=PP (+))
where PB <sysdate - '&&2'
and CB <sysdate - '&&2'
order by PB
/


--spool off
