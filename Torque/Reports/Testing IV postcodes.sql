select * from (select
       REGEXP_SUBSTR(ltrim(rtrim( substr(a.from_postcode,0, length(a.from_postcode) - 3) )), '\D+' ) as SubPostCode
      ,to_number(REGEXP_SUBSTR(ltrim(rtrim( substr(a.from_postcode,0, length(a.from_postcode) - 3) )), '*\d+' )) as fromIndex
      ,to_number(REGEXP_SUBSTR(ltrim(rtrim( substr(a.to_postcode,0, length(a.to_postcode) - 3) )), '*\d+' )   )  as toIndex
      ,d.locationname as serviceLocationName
      ,d1.locationname as hubLocationName
      ,a.servicectr_reamusid
      ,a.hub_reamusid
      ,'Yes' as typ
      ,c.productline1
      ,c.productline2
      ,c.productcode
      ,c.servicecode
      ,c.datecode
      ,c.timecode
      ,c.featureid
    from yo_destination_station a

    inner join yo_destination_prdservices b on a.servicectr_reamusid = b.servicectr_reamusid and b.Allowed = 'Y' --service
  --  inner join yo_destination_prdservices b1 on a.hub_reamusid = b1.servicectr_reamusid  and b1.Allowed = 'Y'     --hub

    inner join yo_service c on b.productcode = c.productcode and b.featurecode = c.featurecode  --service
 --   inner join yo_service c1 on b1.productcode = c1.productcode and b1.featurecode = c1.featurecode  --hub

    inner join yo_reamusid d on a.servicectr_reamusid = d.reamusid  --?
    inner join yo_reamusid d1 on a.hub_reamusid = d1.reamusid  --?
    where a.productcode = '01'
    and c.featureid in ('071') and c.productcode = '99' 
      
  union all

    --No situation
    select distinct
       REGEXP_SUBSTR(ltrim(rtrim( substr(a.from_postcode,0, length(a.from_postcode) - 3) )), '\D+' ) as SubPostCode
      ,to_number(REGEXP_SUBSTR(ltrim(rtrim( substr(a.from_postcode,0, length(a.from_postcode) - 3) )), '*\d+' ) )as fromIndex
      ,to_number(REGEXP_SUBSTR(ltrim(rtrim( substr(a.to_postcode,0, length(a.to_postcode) - 3) )), '*\d+' )   )  as toIndex
      ,d.locationname as serviceLocationName
      ,d1.locationname as hubLocationName
      ,a.servicectr_reamusid
      ,a.hub_reamusid
      ,'No' as typ
      ,hc.productline1
      ,hc.productline2
      ,hc.productcode
      ,hc.servicecode
      ,hc.datecode
      ,hc.timecode
      ,hc.featureid
    from yo_destination_station a

    inner join yo_destination_prdservices b on a.servicectr_reamusid = b.servicectr_reamusid and b.Allowed = 'E' --service
--    inner join yo_destination_prdservices b1 on a.hub_reamusid = b1.servicectr_reamusid and  b1.Allowed = 'E'     --hub

    inner join yo_destination_exception c on b.productcode = c.productcode and b.featurecode = c.featurecode and a.from_postcode = c.from_postcode  --service
--    inner join yo_destination_exception c1 on b1.productcode = c1.productcode and b1.featurecode = c1.featurecode and a.from_postcode = c1.from_postcode  --hub

  --FIX: DPH was missing link to services, remove hard coded to this service table
        inner join yo_service hc on b.productcode = hc.productcode and b.featurecode = hc.featurecode  --service
--        inner join yo_service hc1 on b1.productcode = hc1.productcode and b1.featurecode = hc1.featurecode  --hub
    
    inner join yo_reamusid d on a.servicectr_reamusid = d.reamusid  --?
    inner join yo_reamusid d1 on a.hub_reamusid = d1.reamusid  --?
    where a.productcode = '01'
    and c.featurecode in ('71') and c.productcode = '99') where subpostcode ='IV'