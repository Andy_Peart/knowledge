SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 68
SET NEWPAGE 0
SET LINESIZE 80


column AA heading 'Client' format A12
column BB heading 'User ID' format A14
column CC heading 'Date' format A12
column CCC heading 'Time' format A12
column D heading 'Number' format 999999
column DD heading 'Units' format 999999
column EE heading 'Reason Code' format A12

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



ttitle  LEFT 'TML - Returns Productivity Report as at ' report_date RIGHT 'Page:' FORMAT 999 SQL.PNO skip 2


--select 'Shop,Date,Qty' from DUAL;

break on BB dup skip 1 on report
--break on report

--compute sum label 'Total' of DD on BB
compute sum label 'Report Total' of D DD on report



select
client_id  AA,
user_id BB,
to_char(Dstamp,'DD/MM/YYYY') CC,
--to_char(Dstamp,'HH24:MI:SS') CCC,
count(*) D,
sum(nvl(update_qty,0)) DD
--reason_id EE
from inventory_transaction
where client_id='&&2'
and code='Adjustment'
and reason_id like 'T%'
and Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
group by
client_id,
user_id,
to_char(Dstamp,'DD/MM/YYYY')
order by
AA,BB,CC
/
