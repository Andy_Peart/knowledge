SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
set linesize 500
set trimspool on


column AA format a8 noprint
column A heading 'Date' format a8
column B heading 'Retail' format 99999999
column C heading 'WWW' format 99999999
column D heading 'Total' format 99999999

break on report

compute sum label 'Total' of B C D on report



select 'Mountain Warehouse Allocated Quantities &&2 &&5 to &&3 &&6' from DUAL;

select 'Date,Retail,WWW,Total' from DUAL;

select
A1,
nvl(B1,0)-nvl(B2,0) B,
nvl(B2,0) C,
nvl(B1,0) D
from
(select
trunc(dstamp) A1,
sum (update_qty) B1
from inventory_transaction
where client_id = 'MOUNTAIN'
and code = 'Allocate'
--and work_group<>'WWW'
and dstamp between to_date('&&2 &&5', 'DD-MON-YYYY HH24:MI:SS') and to_date('&&3 &&6', 'DD-MON-YYYY HH24:MI:SS')
group by trunc(dstamp)
union
select
trunc(dstamp) A1,
sum (update_qty) B1
from inventory_transaction_archive
where client_id = 'MOUNTAIN'
and code = 'Allocate'
--and work_group<>'WWW'
and dstamp between to_date('&&2 &&5', 'DD-MON-YYYY HH24:MI:SS') and to_date('&&3 &&6', 'DD-MON-YYYY HH24:MI:SS')
group by trunc(dstamp)),
(select
trunc(dstamp) A2,
sum (update_qty) B2
from inventory_transaction
where client_id = 'MOUNTAIN'
and code = 'Allocate'
and work_group='WWW'
and dstamp between to_date('&&2 &&5', 'DD-MON-YYYY HH24:MI:SS') and to_date('&&3 &&6', 'DD-MON-YYYY HH24:MI:SS')
group by trunc(dstamp)
union
select
trunc(dstamp) A2,
sum (update_qty) B2
from inventory_transaction_archive
where client_id = 'MOUNTAIN'
and code = 'Allocate'
and work_group='WWW'
and dstamp between to_date('&&2 &&5', 'DD-MON-YYYY HH24:MI:SS') and to_date('&&3 &&6', 'DD-MON-YYYY HH24:MI:SS')
group by trunc(dstamp))
where A1=A2(+)
order by A1
/
