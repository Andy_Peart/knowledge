SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 600
SET TRIMSPOOL ON


--spool pgstats.csv

select 'Date,Qty Received,Picks 15+ (Orders),Picks 15+ (Units),Picks 5-14 (Orders),Picks 5-14 (Units),Picks under 5 (Orders),Picks under 5 (Units),Qty on Hand - PGBX,Qty on Hand - PGBG,Retail Despatch Units,Ecom Despatch Units,Returns' from Dual;


select
D,
nvl(Q1,0),
nvl(Q2,0),
nvl(Q2A,0),
nvl(Q3,0),
nvl(Q3A,0),
nvl(Q4,0),
nvl(Q4A,0),
' ',
' ',
nvl(Q5,0),
nvl(Q5A,0),
nvl(Q6,0)
from (select
distinct
trunc(Dstamp) D
from inventory_transaction
where Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and client_id='PG'),(select
trunc(it.Dstamp) D1,
sum(it.update_qty) as Q1
from inventory_transaction it
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='PG'
and it.station_id not like 'Auto%'
and it.code like 'Receipt%'
group by
trunc(it.Dstamp)),(select
D2,
count(*) Q2,
sum(B) Q2A
from (select
trunc(it.Dstamp) D2,
it.reference_id R2,
sum(it.update_qty) as B
from inventory_transaction it
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='PG'
and it.code like 'Pick'
and elapsed_time>0
group by
trunc(it.Dstamp),
it.reference_id)
where B>14
group by D2),(select
D3,
count(*) Q3,
sum(B) Q3A
from (select
trunc(it.Dstamp) D3,
reference_id E,
sum(it.update_qty) as B
from inventory_transaction it
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='PG'
and it.code like 'Pick'
and elapsed_time>0
group by
trunc(it.Dstamp),
reference_id )
where B>4
and B<15
group by D3),(select
D4,
count(*) Q4,
sum(B) Q4A
from (select
trunc(it.Dstamp) D4,
reference_id E,
sum(it.update_qty) as B
from inventory_transaction it
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='PG'
and it.code like 'Pick'
and elapsed_time>0
group by
trunc(it.Dstamp),
reference_id )
where B<5
group by D4),(select
trunc(it.Dstamp) D5,
sum(it.update_qty) Q5
from inventory_transaction it,order_header oh
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='PG'
and it.code like 'Shipment'
and it.reference_id=oh.order_id
and oh.client_id='PG'
and oh.order_type like 'RETAIL%'
group by
trunc(it.Dstamp)),(select
trunc(it.Dstamp) D5A,
sum(it.update_qty) Q5A
from inventory_transaction it,order_header oh
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='PG'
and it.code like 'Shipment'
and it.reference_id=oh.order_id
and oh.client_id='PG'
and oh.order_type not like 'RETAIL%'
group by
trunc(it.Dstamp)),(select
trunc(it.Dstamp) D6,
sum(it.update_qty) Q6
from inventory_transaction it
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='PG'
and it.station_id not like 'Auto%'
and it.code like 'Receipt%'
and it.from_loc_id='PGRETURN'
group by
trunc(it.Dstamp))
where D=D1(+)
and D=D2(+)
and D=D3(+)
and D=D4(+)
and D=D5(+)
and D=D5A(+)
and D=D6(+)
order by D
/

--spool off


