SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 100
SET TRIMSPOOL ON


select 'ECHO Adjustments Report' from DUAL;

select 'Date,Time,SKU ID,Description,Size,Qty,' from DUAL;

Select
to_char(it.Dstamp,'DD/MM/YY,HH24:MI:SS')||','||
it.SKU_ID||','||
sku.description||','||
sku.sku_size||','||
it.Update_qty||','||
' '
from inventory_transaction it,sku
where it.client_id='OPS'
and it.code='Adjustment'
and trunc(it.Dstamp)>trunc(sysdate-7)
and it.sku_id=sku.sku_id
and sku.client_id='OPS'
and sku.product_group='ECHO'
order by
it.dstamp
/

