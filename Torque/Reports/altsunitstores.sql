/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :     prombox.sql				      */
/*                                                                            */
/*     DESCRIPTION:     Prom boxed stock to be moved to Wakefield                  */
/*                                                                            */
/*   DATE     BY   DESCRIPTION						                                    */
/*   ======== ==== ===========					                                      */
/*   03/02/15 SS   Outstanding alterations units - trousers and shirts 
/*   30/11/16 PK   Created date changerd to Order Date & User Def type 5 added   */
/*                                                                               */
/******************************************************************************/
SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','
SET WRAP OFF


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500

select 'Date, PO no, Order no, Shirts, Trousers, Status, Consignment, Store Order No' from dual
union all
SELECT DTE || ',' || PO || ',' || ORD || ',' || SH || ',' || TRS || ',' || status || ',' || consignment || ',' || user_def_type_5
from 
(
SELECT A.DTE,A.PO,A.ORD,B.SH,C.TRS,A.status,A.consignment,A.user_def_type_5
FROM (
select oh.purchase_order PO, oh.order_id ORD, to_char(oh.order_date, 'YYYY-MM-DD') DTE, oh.status, oh.consignment, oh.user_def_type_5
from order_header oh inner join order_line ol on oh.order_id = ol.order_id
where oh.client_id = 'TR'
and ol.client_id = 'TR'
and oh.ship_dock  IN ('TRALT','TRALTCC')
and oh.status in ('Allocated', 'In Progress', 'Picked', 'Complete')
and ol.user_def_note_1 like 'ALTE%'
and oh.dispatch_method = '1'
group by oh.purchase_order, oh.order_id, to_char(oh.order_date, 'YYYY-MM-DD'), oh.status, oh.consignment, oh.user_def_type_5
order by DTE
) A
LEFT JOIN 
(
select oh.order_id ORD, sum(ol.qty_ordered) SH
from order_header oh inner join order_line ol on oh.order_id = ol.order_id inner join sku on sku.sku_id = ol.sku_id
where oh.client_id = 'TR'
and ol.client_id = 'TR'
and oh.ship_dock  IN ('TRALT','TRALTCC')
and sku.client_id = 'TR'
and ol.user_def_note_1 like 'ALT%'
and sku.product_group in ('MS','WS')
and oh.order_date > trunc(sysdate) - 21
group by oh.order_id
) B
ON A.ORD = B.ORD
LEFT JOIN 
(
select oh.order_id ORD, sum(ol.qty_ordered) TRS
from order_header oh inner join order_line ol on oh.order_id = ol.order_id inner join sku on sku.sku_id = ol.sku_id
where oh.client_id = 'TR'
and ol.client_id = 'TR'
and oh.ship_dock  IN ('TRALT','TRALTCC')
and sku.client_id = 'TR'
and ol.user_def_note_1 like 'ALT%'
and sku.product_group in ('MP','WP')
and oh.order_date > trunc(sysdate) - 21
group by oh.order_id
) C
ON A.ORD = C.ORD
order by dte
) 
union all
select ' ' from dual
union all
select 'Date, Orders, Shirts, Trousers' from dual
union all
select dte || ',' || orders || ',' || shirts || ',' || trousers
from ( 
select dte, sum(sh) as shirts, sum(trs) as trousers, count(*) orders
from (
SELECT A.DTE, A.PO, A.ORD, B.SH, C.TRS, A.status
FROM (
select oh.purchase_order PO, oh.order_id ORD, to_char(oh.order_date, 'YYYY-MM-DD') DTE, oh.status
from order_header oh inner join order_line ol on oh.order_id = ol.order_id
where oh.client_id = 'TR'
and ol.client_id = 'TR'
and oh.ship_dock  IN ('TRALT','TRALTCC')
and oh.status in ('Allocated', 'In Progress', 'Picked', 'Complete')
and ol.user_def_note_1 like 'ALTE%'
and oh.dispatch_method = '1'
group by oh.purchase_order, oh.order_id, to_char(oh.order_date, 'YYYY-MM-DD'), oh.status
order by dte
) A
LEFT JOIN 
(
select oh.order_id ORD, sum(ol.qty_ordered) SH
from order_header oh inner join order_line ol on oh.order_id = ol.order_id inner join sku on sku.sku_id = ol.sku_id
where oh.client_id = 'TR'
and ol.client_id = 'TR'
and oh.ship_dock  IN ('TRALT','TRALTCC')
and sku.client_id = 'TR'
and ol.user_def_note_1 like 'ALT%'
and sku.product_group in ('MS','WS')
and oh.order_date > trunc(sysdate) - 21
group by oh.order_id
) B
ON A.ORD = B.ORD
LEFT JOIN 
(
select oh.order_id ORD, sum(ol.qty_ordered) TRS
from order_header oh inner join order_line ol on oh.order_id = ol.order_id inner join sku on sku.sku_id = ol.sku_id
where oh.client_id = 'TR'
and ol.client_id = 'TR'
and oh.ship_dock  IN ('TRALT','TRALTCC')
and sku.client_id = 'TR'
and ol.user_def_note_1 like 'ALT%'
and sku.product_group in ('MP','WP')
and oh.order_date > trunc(sysdate) - 21
group by oh.order_id
) C
ON A.ORD = C.ORD
)
group by dte
order by dte
)
/ 