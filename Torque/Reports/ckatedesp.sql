/******************************************************************************/
/*                                                                            */
/*                            Elite                                           */
/*                                                                            */
/*     FILE NAME  :   ckatedesp.sql                                           */
/*                                                                            */
/*     DESCRIPTION:   Datastream for Curvy Kate Despatch Note                 */
/*                                                                            */
/*   DATE     BY   PROJ       ID         DESCRIPTION                          */
/*   ======== ==== ======     ========   =============                        */
/*   02/07/10 RW   TMR                   Delivery Note For Curvy Kate         */
/*                                                                            */
/******************************************************************************/

/* Input Parameters are as follows */
-- 1)  GMT
-- 2)  Order_id
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 0
SET VERIFY OFF
SET FEEDBACK OFF
SET TAB OFF

 
-- Set the line length to the maximum possible, but trim 
-- trailing spaces for both screen output and spool files
SET LINESIZE 32767
SET WRAP ON
SET TRIMOUT ON
SET TRIMSPOOL ON

/* Define the format of the output dates */
DEFINE	OutputDateFormat = 'DD-MON-YYYY HH24:MI:SS'
COLUMN	sorter	NOPRINT

SELECT Distinct '0' sorter,
       'DSTREAM_CKATEDESP_HDR'          ||'|'||
       rtrim('&&2')                            /* Order_id */
from dual
union all
SELECT '1' sorter,
'DSTREAM_CKATEDESP_ADD_HDR'       ||'|'||
rtrim (oh.contact)					||'|'||
decode(oh.name,null,'',oh.name||'|')||
decode(oh.address1,null,'',oh.address1||'|')||
decode(oh.address2,null,'',oh.address2||'|')||
decode(oh.town,null,'',oh.town||'|')||
decode(oh.county,null,'',oh.county||'|')||
decode(t.text,null,'',t.text||'|')||          /* Country converted using workstation lookup) */
decode(oh.postcode,null,'',oh.postcode)
from order_header oh, country c, language_text t
where oh.order_id = '&&2'
and oh.client_id = 'CK'
and oh.country = c.iso3_id
and t.language = 'EN_GB'
and t.label = 'WLK'||c.iso3_id
union all
select distinct '1' sorter,
'DSTREAM_CKATEDESP_ORD_HDR'			||'|'||
rtrim (oh.order_id)						||'|'||
rtrim (to_char(oh.order_date, 'DD-MON-YYYY'))	||'|'||		
rtrim (to_char(oh.shipped_date, 'DD-MON-YYYY HH24:MI'))			
from order_header oh
where oh.order_id = '&&2'
and oh.client_id = 'CK'
union all
SELECT '1'||sh.container_id SORTER,
'DSTREAM_CKATEDESP_LINE'               ||'|'||
rtrim (sh.container_id)       ||'|'||
rtrim (s.sku_id)				||'|'||
rtrim (s.description)                ||'|'||
rtrim (s.COLOUR)					||'|'||
rtrim (s.SKU_SIZE)					||'|'||
rtrim (sh.qty_shipped)				
from sku s, shipping_manifest sh
where sh.order_id = '&&2'
and sh.qty_picked > '0'
and sh.sku_id = s.sku_id
and sh.client_id = 'CK'
order by 1,2
/