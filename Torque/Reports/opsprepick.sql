/******************************************************************************/
/*                                                                            */
/*                            Elite                                           */
/*                                                                            */
/*     FILE NAME  :   opsprepick.sql                                          */
/*                                                                            */
/*     DESCRIPTION:    Datastream for OPS Pre Pick Note                       */
/*                                                                            */
/*   DATE     BY   PROJ     ID         DESCRIPTION                            */
/*   ======== ==== ======   ========   =============                          */
/*   12/07/08 LH   OPS                 Pre Pick Note for OPS                  */
/*                                                                            */
/******************************************************************************/

/* Input Parameters are as follows */
-- 1)  Order ID
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES


SET PAGESIZE 0
SET VERIFY OFF
SET FEEDBACK OFF
SET TAB OFF

 
-- Set the line length to the maximum possible, but trim 
-- trailing spaces for both screen output and spool files
SET LINESIZE 320
SET WRAP ON
SET TRIMOUT ON
SET TRIMSPOOL ON




/* Define the format of the output dates */
DEFINE	OutputDateFormat = 'DD-MON-YYYY HH24:MI:SS'
COLUMN	sorter	NOPRINT

select '0' sorter,
'DSTREAM_OPS_PICK_HDR' ||'|'||
rtrim (oh.order_id) ||'|'||
rtrim (ad.name) ||'|'||
rtrim (trunc(oh.order_date)) ||'|'||
rtrim (oh.num_lines)
from order_header oh, address ad
where oh.client_id = 'OPS'
and order_id = '&&2'
--and oh.customer_id = ad.address_id
and oh.user_def_type_2 = ad.address_id
union all
select '0' sorter,
'DSTREAM_OPS_PICK_LINE' ||'|'||
rtrim (ol.sku_id) ||'|'||
rtrim (s.description) ||'|'||
rtrim (s.colour) ||'|'||
rtrim (s.ean) ||'|'||
rtrim (s.sku_size) ||'|'||
--rtrim (ol.from_loc_id) ||'|'||
rtrim (nvl(ol.update_qty,0))
from inventory_transaction ol, sku s
where ol.reference_id = '&&2'
and ol.client_id = 'OPS'
and ol.code='Allocate'
and ol.sku_id = s.sku_id
order by 1,2;

exit;