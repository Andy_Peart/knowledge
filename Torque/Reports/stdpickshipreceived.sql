SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 500
SET NEWPAGE 1
SET LINESIZE 500
SET TRIMSPOOL ON
 

column A heading 'Client' format A6
column B heading 'OrderType' format A9
column C heading 'Orders Picked' format 999999
column DD heading 'Lines Picked' format 999999
column D heading 'Units Picked' format 999999
column E heading 'Orders Shipped' format 999999
column F heading 'Units Shipped' format 999999
column G heading 'Units Receipted' format 9999999
column H heading 'Units Receipted - Tag ID RET**' format 999999
   
select 
 client.client_id A, 
 client.order_type B,
 Pick.OrderID C,
 Pick.lines DD,
 Pick.qty D, 
 Ship.OrderID E, 
 Ship.qty F,
 receipts.qty_receipt G,
 receipts.qty_returns H
from 
(
  select client_id, nvl(order_type,'N/A') order_type from Order_header   
  where client_id = '&&2'
  group by client_id,order_type  
  union all  
  select null, 'Receipts' from dual 
) client
left join
(
  select a.client_id, nvl(b.order_type,'N/A') order_type, count(distinct a.reference_id) OrderID, sum(a.update_qty ) qty, count(a.update_qty ) lines
  from inventory_transaction a 
  inner join order_header b on a.client_id = b.client_id and b.order_id = a.reference_id
  inner join location l on l.site_id = a.site_id and l.location_id = a.from_loc_id
  where a.client_id = '&&2'
  and a.dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
  and a.code in ('Pick')
  and l.loc_type in ('Tag-LIFO','Tag-FIFO')
  and a.update_qty <> 0
  group by a.client_id, nvl(b.order_type,'N/A')
) Pick on Pick.client_id = client.client_id and Pick.order_type = client.order_type
left join
(
  select a.client_id, nvl(b.order_type,'N/A') order_type, count(distinct a.reference_id) OrderID, sum(a.update_qty ) qty
  from inventory_transaction a 
  inner join order_header b on a.client_id = b.client_id and b.order_id = a.reference_id
  where a.client_id = '&&2'
  and a.dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
  and a.code in ('Shipment')
  group by a.client_id, nvl(b.order_type,'N/A') 
) ship on ship.client_id = client.client_id and ship.order_type = client.order_type
left join 
(
  select 
    a.client_id, 
    sum( case when a.tag_id like 'RET%' then a.update_qty else 0 end) qty_returns,
    sum( case when a.tag_id not like 'RET%' then a.update_qty else 0 end) qty_receipt
  from inventory_transaction a
  where a.client_id = '&&2'
  and a.code = 'Receipt'
  and a.dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
  and a.tag_id not like 'RET%'
  group by a.client_id
) receipts on client.order_type = 'Receipts'
/

 
