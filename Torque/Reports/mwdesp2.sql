/******************************************************************************/
/*                                                                            */
/*                            Elite                                           */
/*                                                                            */
/*     FILE NAME  :   mwdesp2.sql                                             */
/*                                                                            */
/*     DESCRIPTION:    Datastream for Mountain Warehouse Despatch Note        */
/*                                                                            */
/*   DATE     BY   PROJ     ID         DESCRIPTION                            */
/*   ======== ==== ======   ========   =============                          */
/*   27/04/05 LH   Mountain            Despatch Note for Mountain Warehouse   */
/*   23/01/06 LH   Mountain            Despatch Note to Printer only          */
/*   10/11/09 RW   Mountain            Change user_def_2 to short description */
/******************************************************************************/

/* Input Parameters are as follows */
-- 1)  Order ID
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 0
SET VERIFY OFF
SET FEEDBACK OFF
SET TAB OFF

 
-- Set the line length to the maximum possible, but trim 
-- trailing spaces for both screen output and spool files
SET LINESIZE 32767
SET WRAP ON
SET TRIMOUT ON
SET TRIMSPOOL ON




/* Define the format of the output dates */
DEFINE	OutputDateFormat = 'DD-MON-YYYY HH24:MI:SS'
COLUMN	sorter	NOPRINT

SELECT Distinct '0' sorter,
       'DSTREAM_MW_DESP2_HDR'          ||'|'||
       rtrim('&&2')                            /* Order_id */
from dual
union all
SELECT '0' sorter,
'DSTREAM_MW_DESP2_ADD_HDR'              ||'|'||
rtrim (ad.name)						||'|'||
rtrim (oh.customer_id)				||'|'||
rtrim (ad.address1)					||'|'||
rtrim (ad.address2)					||'|'||
rtrim (ad.town)						||'|'||
rtrim (ad.county)					||'|'||
rtrim (ad.country)					||'|'||
rtrim (ad.postcode)					||'|'||
rtrim (ad.contact_fax)
from address ad, order_header oh
where oh.order_id = '&&2'
and oh.customer_id = ad.address_id
and ad.client_id = 'MOUNTAIN'
union all
select distinct '0' sorter,
'DSTREAM_MW_DESP2_ORD_HDR'			||'|'||
rtrim (oh.order_id)					||'|'||
rtrim (to_char(oh.shipped_date, 'DD-MON-YYYY HH24:MI'))				||'|'||
rtrim (sm.location_id)				||'|'||
rtrim (sm.user_id)					||'|'||
rtrim (sm.site_id)					||'|'||
rtrim (sm.consignment)		
from order_header oh, shipping_manifest sm
where oh.order_id = '&&2'
and oh.order_id = sm.order_id
union all
SELECT unique B sorter,
'DSTREAM_MW_DESP2_LINE'					||'|'||
rtrim (substr(B,1,1))					||'|'||
rtrim (substr(s.user_def_type_5,1,6))	||'|'||
rtrim (s.sku_id)						||'|'||
rtrim (substr(s.description,1,14))		||'|'||
--rtrim (s.user_def_type_2)		||'|'||
rtrim (s.colour)						||'|'||
rtrim (s.sku_size)						||'|'||
rtrim (i.update_qty)	
from order_line ol, sku s, inventory_transaction i, (select unique loc.location_id A, loc.zone_1 B
														from location loc
														where site_id = 'BD2')
where ol.client_id = 'MOUNTAIN'
and ol.order_id = i.reference_id
and i.from_loc_id = A
and ol.line_id = i.line_id
and ol.order_id = '&&2'
and i.code = 'Pick'
and ol.sku_id = s.sku_id
and ol.qty_shipped > 0
and from_loc_id <> 'CONTAINER'
order by 1

/