SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 400
 
set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DAY, DD MONTH, YYYY') curdate from DUAL;
set TERMOUT ON

--spool mwpicktots.csv

select 'PICK Analysis for Client MOUNTAIN for Orders Shipped between &&2 and &&3' from DUAL;

select 'Work Group,Order Id,First Pick,Last Pick,Qty Picked,Lines,Elapsed Time,Units KPI,Lines KPI,Picker' from DUAL;

select
AA||','||
A||','||
to_char(ST,'DD/MM/YYYY HH24:MI:SS')||','||
to_char(FN,'DD/MM/YYYY HH24:MI:SS')||','||
UQ||','||
LNS||','||
ET||','||
replace(to_char(UQ/(ET/3600),'999990'),' ','')||','||
replace(to_char(LNS/(ET/3600),'999990'),' ','')||','||
UD
from (select
it.work_group AA,
it.reference_id A,
it.user_id UD,
min(it.Dstamp) ST,
max(it.Dstamp) FN,
sum(it.elapsed_time) ET,
count(*) LNS,
sum(it.update_qty) UQ
from inventory_transaction it,order_header oh
where it.code in ('Pick')
and it.client_id='MOUNTAIN'
and it.work_group<>'WWW'
and it.elapsed_time>0
and it.reference_id=oh.order_id
and it.client_id=oh.client_id
and oh.shipped_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by
it.work_group,
it.reference_id,
it.user_id
order by
AA,A)
/

--spool off
