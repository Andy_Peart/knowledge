SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON

set pagesize 68
set newpage 0
set linesize 240

clear columns
clear breaks
clear computes

column A heading 'Shipped' format A16
column B heading 'Order ID' format A20
column C heading 'Customer' format A12
column CC heading 'Order Status' format A12
column D heading 'Shipments' format 9999999
column E heading 'Units' format 9999999
column M format a4 noprint
column S heading 'Trade Type' format A12


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'Total Units Despatched from &&2 to &&3 - for client ID &&4 as at ' report_date skip 2

break on S skip page on report

compute sum label 'Totals' of D E on S
--compute sum label 'Full Totals' of D E on report


select
DECODE(substr(oh.order_id,1,2),'31','WHOLESALE'
                              ,'51','RETAIL','UNKNOWN') S,
--substr(oh.order_id,1,2) S,
to_char(Dstamp, 'YYMM') M,
to_char(ol.dstamp, 'DD/MM/YY ') as A,
oh.order_id as B,
oh.inv_name as C,
oh.status as CC,
--nvl(sum(ol.qty_ordered),0) as D,
--nvl(sum(ol.qty_shipped),0) as E
count(*) as D,
nvl(sum(ol.update_qty),0) as E
from order_header oh, inventory_transaction ol
--where  oh.order_id=ol.reference_id
--and oh.client_id='&&4'
--and oh.status in ('Shipped')
where ol.dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and ol.client_id='&&4'
and ol.code='Shipment'
and ol.reference_id=oh.order_id
--and oh.order_id like '31%'
group by
substr(oh.order_id,1,2),
to_char(Dstamp, 'YYMM'),
to_char(ol.dstamp, 'DD/MM/YY '),
oh.order_id,
oh.inv_name,
oh.status
order by
substr(oh.order_id,1,2),
to_char(Dstamp, 'YYMM'),
to_char(ol.dstamp, 'DD/MM/YY '),
oh.order_id
/
