SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON


column A heading 'SKU' format a26
column A2 heading 'SKU' noprint
column B heading 'Date' format a12
column C heading 'Type' format a8
column H heading 'Description' format a24
column D heading 'Units' format 999999
column G heading 'Days' format 999
column F heading '   Rate' format a7
column E heading 'Charge' format 999990.99

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



break on A skip 1 on report

compute sum label 'Nett Charge for above SKU' of E on A
compute sum label 'Total Charge' of E on report

--spool chinastorage.csv

select 'SKU,Date,Type,Units,Description,Days,Rate,Charge' from DUAL;


select
AAA A,
--AAA A2,
to_char(BBB,'DD/MM/YY') B,
CCC C,
DDD D,
HHH H,
trunc(sysdate)-1-BBB+DECODE(CCC,'OUT',0,1) G,
DECODE(substr(HHH,5,2),'TR',' 0.04/7',' 0.05/7') F,
(trunc(sysdate)-1-BBB+DECODE(CCC,'OUT',0,1))*DDD*(DECODE(substr(HHH,5,2),'TR','0.04','0.05')/7)*DECODE(CCC,'OUT',-1,1) E
from (select
it.sku_id AAA,
trunc(it.Dstamp) BBB,
DECODE(it.code,'Receipt','IN','Shipment','OUT','IN') CCC,
sum(it.update_qty) DDD,
sku.description HHH
from inventory_transaction it,sku
where it.client_id='BW'
and it.station_id not like 'Auto%'
and it.reference_id <>'TR1'
--and trunc(it.Dstamp)>to_date('06/01/11','dd/mm/yy')
and to_char(it.Dstamp,'mm/yyyy')=to_char(sysdate-12,'mm/yyyy')
and ((it.code like 'Receipt%' or it.code='Shipment') 
or (it.code='Adjustment' and it.reason_id in ('RECRV','STK CHECK')))
and sku.client_id=it.client_id
and sku.sku_id=it.sku_id
group by
it.sku_id,
trunc(it.Dstamp),
DECODE(it.code,'Receipt','IN','Shipment','OUT','IN'),
sku.description)
where DDD<>0
union
select
the_sku A,
--the_sku A2,
to_char(the_date,'DD/MM/YY') B,
'B/FWD' C,
the_qty D,
sku.description H,
trunc(sysdate)-the_date G,
DECODE(substr(sku.description,5,2),'TR',' 0.04/7',' 0.05/7') F,
(trunc(sysdate)-the_date)*the_qty*(DECODE(substr(sku.description,5,2),'TR','0.04','0.05')/7) E
from berwin_snapshot,sku
where the_date=to_date(('01'||to_char(sysdate-12,'-mon-yyyy')),'DD-MON-YYYY')
and sku.client_id='BW'
and sku.sku_id=the_sku
order by A,B,C
/

--spool off
