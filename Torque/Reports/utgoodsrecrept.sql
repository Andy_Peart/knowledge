SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 800
SET TRIMSPOOL ON

 
column A format A10
column B format A15
column C format A28
column D format A6
column E format A20
column F format 9999999
column G format A18
column H format A40
column I format 999999999
column J format 999999999
column K format 999999999
column M format a4 noprint


break on report
compute sum label 'Totals' of I J K on report


spool utgoodsrecrept.csv

select 'Goods Received Report - for client ID &&2 and completed date between &&3 to &&4 (All Pre Advices)' 
from dual;

select 'Date,Supplier Id,Supplier Name,Pre Advice ID,Line ID,Style,Colour,Size,Product Code,Description,EAN,Brand,Group,Origin,Ordered,Received,Shortfall'
from dual;


select 
trunc(ph.finish_dstamp),
ph.supplier_id,
ad.name,
--it.grn,
ph.pre_advice_id,
pl.line_id, 
STYLE,
CLR,
SZ,
pl.sku_id,
DDD,
EAN,
BRAND,
GRP,
ORG,
pl.qty_due I,
pl.qty_received J,
pl.qty_due-pl.qty_received K
from pre_advice_header ph,pre_advice_line pl,address ad, (select 
sku_id SSS,
description DDD,
user_def_type_3 STYLE,
colour CLR,
sku_size SZ,
ean EAN,
user_def_type_1 BRAND,
product_group GRP,
user_def_type_8 ORG
from sku where client_id='&&2')
where ph.client_id='&&2'  
and ph.finish_dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and ph.supplier_id=ad.address_id (+)
and ph.pre_advice_id=pl.pre_advice_id
and pl.client_id='&&2'
and pl.sku_id=SSS
order by 
trunc(ph.finish_dstamp),
ph.pre_advice_id,
pl.line_id
/

spool off
