SET FEEDBACK OFF           
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 2000
SET TRIMSPOOL ON
 
--spool mwcpr3.csv 

select 'Consignment,Customer,Container,Order,Line,SKU,T2T,Description,Qty Picked,Location' from DUAL;
 
select
oh.consignment,
it.Customer_id,
it.Container_id A,
it.reference_id,
it.line_id,
''''||it.SKU_id||'''',
sku.user_def_type_1,
sku.description,
it.update_qty,
it.from_loc_id
from inventory_transaction it 
inner join sku on it.sku_id=sku.sku_id and it.client_id = sku.client_id
inner join order_header oh on it.client_id = oh.client_id and it.reference_id = oh.order_id
where it.client_id in('MOUNTAIN','AA')
and 
(
oh.consignment = '&&2'
or
it.reference_id = '&&3'
)
and it.elapsed_time>0
order by it.container_id,it.line_id
/

--spool off

