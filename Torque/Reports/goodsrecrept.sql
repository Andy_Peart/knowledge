		SET FEEDBACK OFF                 
		SET ECHO OFF
		SET VERIFY OFF
		SET TAB OFF
		SET TERMOUT ON
		SET colsep ','


		/* Clear previous settings */
		CLEAR COLUMNS
		CLEAR BREAKS
		CLEAR COMPUTES
		 
		/* Set up page and line */
		SET PAGESIZE 0
		SET NEWPAGE 0
		SET LINESIZE 240


		--spool goodsrecnew.csv
		select 'Goods Received Report - for client ID &&2 and period &&3 to &&4 (All Pre Advices)' 
		from dual
    union all
		select 'Date,Supplier Id,Supplier Name,Pre Advice ID,Line ID,Product Code,Description,Ordered,Received,Shortfall'
		from dual
    union all
    select f1 || ',' || f2 || ',' || f3 || ',' || f4 || ',' || f5 || ',' || f6 || ',' || f7 || ',' || I || ',' || J  || ',' || K
    from (
		select 
		trunc(ph.finish_dstamp) f1,
		ph.supplier_id f2,
		ad.name f3,
		ph.pre_advice_id f4,
		pl.line_id f5, 
		pl.sku_id f6,
		DDD f7,
		pl.qty_due I,
		pl.qty_received J,
		pl.qty_due-pl.qty_received K
		from pre_advice_header ph,pre_advice_line pl,address ad, (select 
		sku_id SSS,
		description DDD,
		user_def_type_3 STYLE,
		colour CLR,
		sku_size SZ,
		ean EAN,
		user_def_type_1 BRAND,
		product_group GRP,
		user_def_type_8 ORG
		from sku where client_id='&&2')
		where ph.client_id='&&2'  
		and ph.finish_dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
		and ph.supplier_id=ad.address_id (+)
		and ph.pre_advice_id=pl.pre_advice_id
		and pl.client_id='&&2'
		and pl.sku_id=SSS
		order by 
		trunc(ph.finish_dstamp),
		ph.pre_advice_id,
		pl.line_id
    )
		/


		--spool off

