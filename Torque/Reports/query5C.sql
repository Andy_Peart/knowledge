SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

--The fields I need from inventory transaction are: 
--code; sku_id; tag_id; condition_id; reference_id; to_loc_id; update_qty; line_id; tran_date; tran_time; supplier_id; user_id; station_id

--SET TERMOUT OFF
--spool 5C.csv


--select 'Multiple SKU Extract - for client ID TR as at ' 
--        ||  to_char(sysdate,'dd/mm/yy  hh24:mi:ss') from dual;

select 'Code,SKU,UPC,Tag,Condition,Reference,Location,Qty,Line ID,Txn Date,Txn Time,Supplier,User,Station,Description,Size,Colour'
from dual;


select
iv.code ||',''' ||
iv.sku_id ||''','''||
sku.upc ||''','||
iv.tag_id ||','||
iv.condition_id ||','||
iv.reference_id ||','||
iv.to_loc_id ||','||
nvl(iv.update_qty,0) ||','||
iv.line_id ||','||
to_char(Dstamp, 'DD/MM/YYYY') ||','||
to_char(Dstamp, 'HH24:MI:SS') ||','||
iv.supplier_id ||','||
iv.user_id ||','||
iv.station_id ||','||
sku.description ||','||
sku.sku_size ||','||
sku.colour
from inventory_transaction iv,sku
where iv.Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and iv.client_id='&&2'
and iv.code='Shipment'
and iv.sku_id=sku.sku_id
and sku.client_id='&&2'
order by iv.tag_id
/

--spool off
--SET TERMOUT ON
