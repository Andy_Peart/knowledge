/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :     itinvenkey.sql                           		      */
/*                                                                            */
/*     DESCRIPTION:                                                           */
/*     print to screen the inventory transaction using key id		          */
/*     TORQUE (IT) Inventory Transaction from Key                             */
/*   DATE       BY   DESCRIPTION					                          */
/*   ========== ==== ===========				                              */
/*   26/04/2013 YB   Initial sql created                  					  */
/******************************************************************************/

/* Clear previous settings */

SET FEEDBACK OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES


 
/* Set up page and line */
SET PAGESIZE 10
SET NEWPAGE 0
SET LINESIZE 132
 
/* Top Title */
TTITLE LEFT "Key number to Inventory Transaction for Key ID:&&2" skip 2

SELECT KEY,
  CLIENT_ID,
  CODE,
  TO_CHAR(DSTAMP, 'DD-MON-YYYY HH24:MI') TO_DATE,
  SITE_ID,
  FROM_LOC_ID,
  TO_LOC_ID,
  SKU_ID,
  TAG_ID,
  ORIGINAL_QTY,
  UPDATE_QTY,
  USER_ID
FROM INVENTORY_TRANSACTION
WHERE KEY = '&&2'

/