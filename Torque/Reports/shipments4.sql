SET FEEDBACK OFF                 
set pagesize 68
set linesize 132
set verify off
set wrap off
set newpage 0

clear columns
clear breaks
clear computes


column A heading 'Order ID' format a12
column B heading 'Order Type' format a16
column C heading 'Shipments' format 999999
column D heading 'Units' format 999999
column F heading 'Date' format a12

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



ttitle  LEFT 'Shipments by Date from &&2 to &&3 - for client ID &&4 as at ' report_date skip 2

break on REPORT on F skip 2 on B skip 1

compute sum label 'Report Totals' of C D on REPORT
compute sum label 'Daily Totals' of C D on F 
compute sum label 'Type Totals' of C D on B 
 


select
to_char(it.Dstamp, 'DD/MM/YY') F,
oh.order_type B,
oh.order_id A,
count(*) C,
sum(it.update_qty) D
from inventory_transaction it, order_header oh
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='&&4'
and oh.client_id='&&4'
and oh.order_type like '%&&5%'
and it.update_qty<>0
and it.code='Shipment'
and it.reference_id=oh.order_id
group by
to_char(Dstamp, 'DD/MM/YY'),
oh.order_type,
oh.order_id
order by
to_char(Dstamp, 'DD/MM/YY'),
oh.order_type,
oh.order_id
/

