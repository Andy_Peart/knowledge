SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON


select 'Client ID, SKU, Description, Qty on hand, Qty allocated, Location, Date counted' from dual
union all
select client_id || ',' || sku_id || ',' || description || ',' || qty_on_hand || ',' || qty_allocated || ',' || location_id || ',' ||  counted from
(
SELECT 
inventory.client_id,
sku.sku_id,
  sku.description,
  qty_on_hand,
  qty_allocated,
  location_id,
  TO_CHAR(inventory.count_dstamp,'DD-MM-YYYY') counted
FROM Inventory
JOIN sku
ON sku.sku_id             = inventory.sku_id
WHERE inventory.client_ID = 'MOUNTAIN'
and location_id NOT IN ('DESPATCH', 'MAIL ORDER', 'CONTAINER', 'VL', 'VLSTG')
)