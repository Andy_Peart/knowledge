SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 120
set newpage 0
set verify off
set colsep ','
set trimspool on




--break on report

--compute sum label 'Total' of B D on report

--spool socartons.csv

select 'SOCK ONS picks from &&2 to &&3' from DUAL;
select 'Date,Order Id,Lines,Units,Cartons' from DUAL;

select
A||','||
B||','||
C||','||
D||','||
F
from (select
A,
B,
count(*) C,
sum(QQ) D,
to_char(sum(F),'99999.99') F
from (select
trunc(oh.shipped_date) A,
oh.order_id B,
ol.sku_id C,
ol.qty_picked QQ,
sku.user_def_type_1 D,
ol.qty_picked/sku.user_def_type_1 F
from order_header oh,order_line ol,sku
where oh.client_id = 'SO'
and oh.shipped_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')
and oh.order_id=ol.order_id
and ol.client_id = 'SO'
and sku.client_id = 'SO'
and ol.sku_id=sku.sku_id
and nvl(ol.qty_picked,0)>0)
group by
A,B
order by
A,B)
/

--spool off



--spool off
