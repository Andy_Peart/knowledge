SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

select
'Week &&2 Run &&3    Total Orders = '||count(distinct oh.order_id)||'    Units Ordered = '||nvl(sum(ol.qty_ordered),0)
from order_header oh,order_line ol
where oh.client_id='TR'
and ol.client_id='TR'
and oh.order_id=ol.order_id
and oh.status not in ('Cancelled','Hold','Shipped')
and oh.consignment like '&&2%'
and substr(oh.consignment,7,1)='&&3'
/
