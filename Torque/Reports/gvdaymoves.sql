SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

column U8  format A12
column SS  format A18
column D8  format A40
column A2  format 999999
column AA  format A12

 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 350
SET TRIMSPOOL ON

select 'PO Number,SKU,Description,Qty,Date,' from Dual;

select
it.user_def_type_8 U8,
it.sku_id SS,
sku.description D8,
sum(it.update_qty) A2,
to_char(it.dstamp, 'DD/MM/YYYY') AA,
' '
--it.from_loc_id,
--DECODE(l1.zone_1,'INTERIM',it.from_loc_id,l1.zone_1),
--it.to_loc_id,
--DECODE(l2.zone_1,'INTERIM',it.to_loc_id,l2.zone_1)
from inventory_transaction it,location l1,location l2,sku
where it.client_id = 'GV'
and it.code='Relocate'
--and it.dstamp > sysdate-1
and trunc(it.dstamp)=trunc(sysdate)
and it.from_loc_id=l1.location_id
and it.to_loc_id=l2.location_id
and l1.zone_1 in ('INTERIM','GIVE','OUTLET','FAULTY','RETURN')
and l2.zone_1 in ('INTERIM','GIVE','OUTLET','FAULTY')
and DECODE(l1.zone_1,'INTERIM',it.from_loc_id,l1.zone_1)='AQL'
and DECODE(l2.zone_1,'INTERIM',it.to_loc_id,l2.zone_1)='GIVE'
and it.client_id=sku.client_id
and it.sku_id=sku.sku_id
group by
it.user_def_type_8,
it.sku_id,
sku.description,
to_char(it.dstamp, 'DD/MM/YYYY'),
it.from_loc_id,
--DECODE(l1.zone_1,'INTERIM',it.from_loc_id,l1.zone_1),
--it.to_loc_id,
DECODE(l2.zone_1,'INTERIM',it.to_loc_id,l2.zone_1)
order by
U8,SS
/


