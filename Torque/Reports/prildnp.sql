set pagesize 2000
set linesize 2000
set verify off
clear columns
clear breaks
clear computes

break on report

column C heading 'SKU' format a15
column D heading 'Qty' format 99999999
Column E heading 'Last Date' format a20

compute sum label 'Total' of D on report

Select inv.sku_id C,
sum(inv.qty_on_hand) D,
to_char(B, 'DD/MM/YY') E
from inventory inv, (select i.Sku_id A,
max (i.dstamp) B
from inventory_transaction i
where i.client_id = 'PRI'
and i.code = 'Pick'
group by i.sku_id
order by B)
where inv.sku_id = A
and B <sysdate - '&1'
group by inv.sku_id, B
order by B
/
