SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240


column A format A4
column B format A6
column C heading 'SKU' format A18
column D heading 'Qty' format 999999
column E format A12

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



--select 'Transfer Code,Store,SKU,Qty,Stock Type' from DUAL;

--break on report

--compute sum label 'Totals' of D on report


select
'1'||','||
customer_id||','||
sku_id||','||
update_qty||','||
'SHOP_STOCK'
from inventory_transaction
where client_id='TR'
and code='Shipment'
and consignment='&&2'
/
