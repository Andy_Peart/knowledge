#!/bin/bash
#
#   TORQUE - (STD) - End of Day Order Details
#

timezone=$1
client=$2
fromdate=$3
todate=$4

reportname="${client}_${fromdate}_${todate}_END_OF_DAY_ORDER_DETAILS"
sqlfile="stdeodorddet.sql"

datetime=$(date +%F_%H%M%S)
filename="${reportname}_${datetime}.csv"

sqlplus -s $ORACLE_USR << ! > ${HOME}/ReportOutput/results/${filename}
@$DCS_USERRPTDIR/${sqlfile} $client $fromdate $todate
exit
/
!

echo "File written to:"
echo "\\\\rp2\\user_reports\\${filename}"