SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 200
SET TRIMSPOOL ON
 

column A heading 'Location' format A11
column B heading 'SKU' format A12
column C heading 'Description' format A40
column D heading 'Qty' format 9999999
column E heading '' format A14

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

--break on report

--compute sum label 'Total' of D on report

--spool sbqchold.csv


select 'Client ID,SKU,Supplier,Location,Description,Condition,Status,Lock Code,Total Qty,Receipt ID,Receive Date' from DUAL;



select
i.client_id||','||
i.sku_id||','||
i.supplier_id||','||
i.location_id||','||
Sku.description||','||
i.condition_id||','||
i.lock_status||','||
i.lock_code||','||
sum(i.qty_on_hand)||','||
i.receipt_id||','||
trunc(i.receipt_dstamp)
from inventory i,sku
where i.client_id like 'SB'
and location_id like 'QCHOLD%'
and i.client_id=sku.client_id
and i.sku_id=sku.sku_id
group by
i.client_id,
i.sku_id,
i.supplier_id,
i.location_id,
Sku.description,
i.condition_id,
i.lock_status,
i.lock_code,
i.receipt_id,
trunc(i.receipt_dstamp)
order by
i.sku_id,
i.supplier_id,
i.location_id
/

--spool off
