SET FEEDBACK OFF
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 2400
SET TRIMSPOOL ON

--break on A skip 1

--compute sum label 'Tots' of C D F H on A

--spool mwconpicks.csv

select 'PICK Tasks for Consignments like &&2' from DUAL
union all
select 'Picker,Consignment,Work Group,Lines Picked,Units Picked,Start Time,Finish Time' from DUAL
union all
select
A||','||
A2||','||
B||','||
C||','||
D||','||
to_char(F,'HH24:MI:SS')||','||
to_char(G,'HH24:MI:SS')
from (select
it.user_id A,
it.consignment A2,
it.work_group B,
count(*) C,
sum(it.update_qty) D,
min(it.Dstamp) F,
max(it.Dstamp) G
from inventory_transaction it
where it.client_id='MOUNTAIN'
and it.consignment like '&&2%'
and it.update_qty<>0
and it.station_id not like 'Auto%'
and it.code='Pick'
and it.reference_id like 'MOR%'
and it.elapsed_time>0
group by
it.user_id,
it.consignment,
it.work_group
order by A,A2,B)
/

--spool off
