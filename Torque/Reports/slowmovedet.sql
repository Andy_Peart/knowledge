SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 2500
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Sku,Location/s' from dual
union all
select "Sku" || ',' || "Location" from
(
select "Sku",listagg("Zone" || ' - ' || "Location" || '(' || "Qty" || ') ' || '(' || "lCount" || ')', ', ')within group(order by "Location") as "Location"
from(
select i.sku_id as "Sku"
, i.location_id as "Location"
, l.work_zone   as "Zone"
, count(i.location_id)over(partition by i.sku_id) as "sCount"
, count(distinct i.sku_id)over(partition by i.location_id) as "lCount"
, sum(i.qty_on_hand)    as "Qty"
from inventory i
inner join location l on l.site_id = i.site_id and l.location_id = i.location_id
left join (select sku_id, max(dstamp) as "Date" from inventory_transaction where client_id = '&&2' and code = 'Pick' group by sku_id ) X on X.sku_id = i.sku_id
left join (select sku_id, max(dstamp) as "Date" from inventory_transaction_archive where client_id = '&&2' and code = 'Pick' group by sku_id) Y on Y.sku_id = i.sku_id
where i.client_id = '&&2'
and l.loc_type in ('Tag-LIFO','Tag-FIFO')
and nvl(nvl(X."Date",Y."Date"),to_date('01-JAN-1900')) < sysdate - '&&3' /*If an item has never been picked, it will default to 01/01/1900 to still show on report*/
and i.receipt_dstamp < sysdate - '&&3'
group by i.sku_id
, i.location_id
, l.work_zone
) where "sCount" > 1
group by "Sku"
order by 1
)
/
--spool off