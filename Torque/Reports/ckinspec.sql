SET FEEDBACK OFF                
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET COLSEP ','

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

set pagesize 0
set linesize 800
set trimspool on
set verify off


select 'Code,Sku,Description,Size,Condition,Qty,Receipt ID' from DuaL;

select
it.code||','||
it.Sku_id||','||
sku.description||','||
sku.sku_size || sku.user_def_note_1 ||','||
it.condition_id||','||
it.update_qty||','||
it.reference_id||','||
' '
from inventory_transaction it,sku
where it.client_id = 'CK'
and it.station_id not like 'Auto%'
and it.code like  'Receipt%'
and it.dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1
and it.sku_id=sku.sku_id
and sku.client_id='CK'
order by
it.sku_id
/
