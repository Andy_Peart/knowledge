SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 160

column A heading 'Order' format a20


select 'Orders Complete still not Shipped' from DUAL;
select ' ' from DUAL;



Select
distinct oh.Order_id A
from order_header oh,order_line ol
where oh.client_id='MOUNTAIN'
and oh.order_id=ol.order_id
and oh.status in ('Complete')
--and ol.qty_ordered=nvl(ol.qty_picked,0)
order by A
/

