SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 800
SET TRIMSPOOL ON


column Created heading 'Date Created' format a14
column Priority heading 'Priority' format 999999
column Orders heading 'Total orders' format 999999
column Day00o heading 'Orders00' format 999999
column Day00u heading 'Units00' format 999999
column Day0o heading 'Orders0' format 999999
column Day0u heading 'Units0' format 999999
column Day1o heading 'Orders1' format 999999
column Day1u heading 'Units1' format 999999
column Day2o heading 'Orders1' format 999999
column Day2u heading 'Units1' format 999999
column Day3o heading 'Orders1' format 999999
column Day3u heading 'Units1' format 999999
column Day4o heading 'Orders1' format 999999
column Day4u heading 'Units1' format 999999
column Day5o heading 'Orders1' format 999999
column Day5u heading 'Units1' format 999999
column Total_shipped heading 'Total shipped' format 999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON




--break on Created dup skip 1 on Priority dup skip 1 on report
break on Created dup skip 1

--compute sum label 'Summary' of B C on A1
--compute sum label 'Totals' of B C on A
--compute sum label 'Overall' of B C on report

--spool chinareceipts.csv

select ',,,Prev. day,, Same day,, Day 1, , Day 2, , Day 3, , Day 4, , Day5+, , ' from DUAL;

select 'Created, Priority, Orders, Orders, Units, Orders, Units, Orders, Units, Orders, Units, Orders, Units, Orders, Units, Orders, Units, Total' from DUAL;

with ship_man as 
(
select sm.shipped_dstamp, sm.qty_shipped, sm.order_id, sm.client_id
from shipping_manifest sm
where sm.client_id = 'MOUNTAIN'
and sm.shipped_dstamp is not null
union all 
select sm.shipped_dstamp, sm.qty_shipped, sm.order_id, sm.client_id
from shipping_manifest_archive sm
where sm.client_id = 'MOUNTAIN'
and trunc(sm.shipped_dstamp) > trunc(sysdate)-150
),
ord_head as (
select order_id, client_id, creation_date, priority
from order_header OH
where oh.client_id = 'MOUNTAIN'
and
(
oh.work_group = 'WWW'
or
oh.order_type = 'WEB'
)
and oh.status = 'Shipped'
union all
select order_id, client_id, creation_date, priority
from order_header_archive OH
where oh.client_id = 'MOUNTAIN'
and
(
oh.work_group = 'WWW'
or
oh.order_type = 'WEB'
)
and oh.status = 'Shipped'
),
inv_trans as
(
select dstamp, code, notes, reference_id, client_id
from inventory_transaction 
where client_id = 'MOUNTAIN'
and dstamp between to_date('&&2','DD-MON-YYYY') and to_date('&&3','DD-MON-YYYY')+3
union all
select dstamp, code, notes, reference_id, client_id
from inventory_transaction_archive
where client_id = 'MOUNTAIN'
and dstamp between to_date('&&2','DD-MON-YYYY') and to_date('&&3','DD-MON-YYYY')+3
),
T1 as
(
select created, order_id, priority, shipped,
case
  when to_date(created, 'DD-MM-YYYY') = to_date(date_shipped, 'DD-MM-YYYY')+1 then 'day 00'
  when to_date(created, 'DD-MM-YYYY') = to_date(date_shipped, 'DD-MM-YYYY') then 'day 0'
  when to_date(created, 'DD-MM-YYYY') = to_date(date_shipped, 'DD-MM-YYYY')-1 then 'day 1'
  when to_date(created, 'DD-MM-YYYY') = to_date(date_shipped, 'DD-MM-YYYY')-2 then 'day 2'
  when to_date(created, 'DD-MM-YYYY') = to_date(date_shipped, 'DD-MM-YYYY')-3 then 'day 3'
  when to_date(created, 'DD-MM-YYYY') = to_date(date_shipped, 'DD-MM-YYYY')-4 then 'day 4'
  else 'day 5'
end turnaround
from (
select 
case
  when priority = '888' then 
    case 
      when tme >= 20 then  to_char  ( (to_date(created, 'DD-MM-YYYY')+1) , 'DD-MM-YYYY')
      else to_char(  (to_date(created, 'DD-MM-YYYY')), 'DD-MM-YYYY')
    end
  when priority = '801' then 
    case 
      when tme >= 20 then  to_char  ( (to_date(created, 'DD-MM-YYYY')+1) , 'DD-MM-YYYY')
      else to_char(  (to_date(created, 'DD-MM-YYYY')), 'DD-MM-YYYY')
    end
  when priority = '824' then 
    case 
      when tme >= 20 then  to_char  ( (to_date(created, 'DD-MM-YYYY')+1) , 'DD-MM-YYYY')
      else to_char(  (to_date(created, 'DD-MM-YYYY')), 'DD-MM-YYYY')
    end    
  else
    to_char(  (to_date(created, 'DD-MM-YYYY')), 'DD-MM-YYYY')
end created, order_id, priority, date_shipped, shipped
from (
select to_char(oh.creation_date, 'DD-MM-YYYY') created, to_char(oh.creation_date, 'HH24') tme,  oh.order_id, oh.priority, to_char(sm.SHIPPED_DSTAMP, 'DD-MM-YYYY') date_shipped
,sum(sm.QTY_SHIPPED) shipped
from ord_head oh inner join ship_man sm on oh.order_id = sm.order_id
where oh.client_id = 'MOUNTAIN'
and sm.client_id = 'MOUNTAIN'
and oh.order_id not in (
select reference_id
from inv_trans
where client_id = 'MOUNTAIN'
and dstamp between to_date('&&2','DD-MON-YYYY') and to_date('&&3','DD-MON-YYYY')+3
and code = 'Order Status'
and
(
notes = 'Allocated --> Hold'
or
notes = 'In Progress --> Hold'
or
notes = 'Picked --> Hold'
or
notes = 'Released --> Hold'
)
)
and oh.creation_date between to_date('&&2','DD-MON-YYYY') and to_date('&&3','DD-MON-YYYY')+1
group by to_char(oh.creation_date, 'DD-MM-YYYY'), oh.order_id, oh.priority, to_char(sm.SHIPPED_DSTAMP, 'DD-MM-YYYY') , to_char(oh.creation_date, 'HH24')
)
) 
)
select A.*, 
A1.orders day00o, A1.units day00u, 
B.orders day0o, B.units day0u, 
C.orders day1o, C.units day1u, 
D.orders day2o, D.units day2u,
E.orders day3o, E.units day3u,
F.orders day4o, F.units day4u,
G.orders day5o, G.units day5u,
nvl(A1.orders,0)+nvl(B.orders,0)+nvl(C.orders,0)+nvl(D.orders,0)+nvl(E.orders,0)+nvl(F.orders,0)+nvl(G.orders,0) Total_shipped
from (
select created, priority, count(*) orders
from T1
group by created, priority
order by 1,2
) A
left join 
(
select created, priority, count(*) orders, sum(shipped) units
from T1
where turnaround = 'day 00'
group by created, priority
) A1
on A.created = A1.created and A.priority = A1.priority
left join 
(
select created, priority, count(*) orders, sum(shipped) units
from T1
where turnaround = 'day 0'
group by created, priority
) B
on A.created = B.created and A.priority = B.priority
left join 
(
select created, priority, count(*) orders, sum(shipped) units
from T1
where turnaround = 'day 1'
group by created, priority
) C
on A.created = C.created and A.priority = C.priority
left join 
(
select created, priority, count(*) orders, sum(shipped) units
from T1
where turnaround = 'day 2'
group by created, priority
) D
on A.created = D.created and A.priority = D.priority
left join 
(
select created, priority, count(*) orders, sum(shipped) units
from T1
where turnaround = 'day 3'
group by created, priority
) E
on A.created = E.created and A.priority = E.priority
left join 
(
select created, priority, count(*) orders, sum(shipped) units
from T1
where turnaround = 'day 4'
group by created, priority
) F
on A.created = F.created and A.priority = F.priority
left join 
(
select created, priority, count(*) orders, sum(shipped) units
from T1
where turnaround = 'day 5'
group by created, priority
) G
on A.created = G.created and A.priority = G.priority
order by 1,2
/
--spool off