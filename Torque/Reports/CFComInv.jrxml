<?xml version="1.0" encoding="UTF-8"?>
<!-- Created with Jaspersoft Studio version 6.13.0.final using JasperReports Library version 5.2.0  -->
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="CFComInv" language="groovy" pageWidth="842" pageHeight="595" orientation="Landscape" columnWidth="802" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" uuid="e2ffaf50-a2a6-433b-8dd9-ebdbd06d8280">
	<property name="ireport.zoom" value="1.610510000000001"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="2"/>
	<property name="com.jaspersoft.studio.data.defaultdataadapter" value="JDA Live"/>
	<property name="com.jaspersoft.studio.data.sql.tables" value=""/>
	<parameter name="consignment" class="java.lang.String"/>
	<parameter name="orderid" class="java.lang.String"/>
	<queryString>
		<![CDATA[SELECT OH.ORDER_ID
,OH.CONSIGNMENT
,OH.CUSTOMER_ID AS "ADR CUST"
,OH.INV_TOTAL_1
,Initcap(OH.NAME) AS "ADR NAME"
,Initcap(Decode(OH.CONTACT,OH.NAME,'',OH.CONTACT)) AS "ADR CONTACT"
,Initcap(OH.ADDRESS1) AS "ADR ADDRESS 1"
,Initcap(OH.ADDRESS2) AS "ADR ADDRESS 2"
,Initcap(OH.TOWN) AS "ADR TOWN"
,Initcap(OH.COUNTY) AS "ADR COUNTY"
,oh.contact_phone
,oh.contact_email
,OH.POSTCODE AS "ADR POSTCODE"
,Initcap(REPLACE(c.TANDATA_ID,'_',' ')) AS "ADR COUNTRY"
,Initcap(OH.INV_NAME) as INV_NAME
,Initcap(Decode(OH.INV_CONTACT,OH.INV_NAME,'',OH.INV_CONTACT)) as INV_CONTACT
,Initcap(OH.INV_ADDRESS1) as INV_ADDR1
,Initcap(OH.INV_ADDRESS2) as INV_ADDR2
,Initcap(OH.INV_TOWN) as INV_TOWN
,Initcap(OH.INV_COUNTY) as INV_COUNTY
,OH.INV_POSTCODE
,Initcap(REPLACE(ic.TANDATA_ID,'_',' ')) AS "INV COUNTRY"
,CASE WHEN OH.INV_CURRENCY='GBP' THEN 'British Pound'
			WHEN OH.INV_CURRENCY='EUR' THEN 'Euro'
			WHEN OH.INV_CURRENCY='USD' THEN 'US Dollar'
			ELSE OH.INV_CURRENCY END AS "CURRENCY"
,CASE WHEN OH.INV_CURRENCY='GBP' THEN '£'
			WHEN OH.INV_CURRENCY='EUR' THEN '€'
			WHEN OH.INV_CURRENCY='USD' THEN '$'
      WHEN OH.INV_CURRENCY is null THEN '£'
			ELSE OH.INV_CURRENCY END AS "CURRENCY_SYMBOL"
,OH.DISPATCH_METHOD
,decode(OH.USER_DEF_CHK_2,'Y','EXCHANGE ORDER','') as EXCHANGE
,decode(OH.USER_DEF_CHK_2,'Y','FREE EXCHANGE / RETURNED GOODS','') as EXCHANGE2
,TO_CHAR(OH.SHIPPED_DATE, 'DD-MON-YYYY') AS "DESPATCHED DATE"
,OL.SKU_ID
,SUBSTR(SKU.USER_DEF_NOTE_1, 1,INSTR(SKU.USER_DEF_NOTE_1,' ') -1) AS "PROD NAME"
,SKU.DESCRIPTION AS "PROD DESCRIPTION"
,SKU.COLOUR AS "PROD COLOUR"
,SKU.SKU_SIZE
,SKU.COMMODITY_DESC
,SKU.COMMODITY_CODE AS "HTS CODE"
,SKU.USER_DEF_note_1 AS "COMPOSITION"
,SKU.CE_COO AS "ORIGIN"
,sum(OL.QTY_ORDERED) AS "QTY"
,sum(OL.QTY_SHIPPED) AS "QTY_SHIPPED"
,OL.PRODUCT_PRICE AS "UNIT PRICE"
,OL.EXTENDED_PRICE AS "AMOUNT"
,decode(c.CE_EU_TYPE,'EU','20.0%','0.0%') AS "TAX ..."
,OL.EXTENDED_PRICE AS "VALUE"
,OH.User_def_type_6 as "UPSTracking"
,sum(OL.Line_value) as "Line_value"
,OH.signatory
, coalesce(oh.freight_terms, 'DDU') "FREIGHT TERMS"
FROM 	order_header oh

INNER JOIN order_line ol
	ON oh.order_id = ol.order_id
        AND oh.client_id = ol.client_id

LEFT JOIN COUNTRY c
	ON OH.COUNTRY = c.ISO3_ID
LEFT JOIN COUNTRY ic
	ON OH.INV_COUNTRY = ic.ISO3_ID
LEFT JOIN SKU
	ON OL.SKU_ID = SKU.SKU_ID
	AND SKU.CLIENT_ID = OL.CLIENT_ID

WHERE
oh.client_id    = 'CF'

AND (
oh.order_id = $P{orderid}
or
oh.consignment = $P{consignment}
)

GROUP BY OH.CONSIGNMENT
,OH.ORDER_ID
,OH.CUSTOMER_ID
,OH.NAME
,OH.CONTACT
,OH.ADDRESS1
,OH.ADDRESS2
,OH.TOWN
,OH.COUNTY
,OH.POSTCODE
,c.TANDATA_ID
,OH.INV_NAME
,OH.INV_CONTACT
,OH.INV_ADDRESS1
,OH.INV_ADDRESS2
,OH.INV_TOWN
,OH.INV_COUNTY
,OH.INV_POSTCODE
,ic.TANDATA_ID
,OH.INV_CURRENCY
,OH.DISPATCH_METHOD
,OH.USER_DEF_CHK_2
,OH.SHIPPED_DATE
,OH.INV_TOTAL_1
,OL.SKU_ID
,SKU.USER_DEF_TYPE_6
,SKU.DESCRIPTION
,SKU.COLOUR
,SKU.SKU_SIZE
,SKU.COMMODITY_DESC
,SKU.COMMODITY_CODE
,OL.USER_DEF_TYPE_4
,OL.USER_DEF_TYPE_5
,OL.QTY_ORDERED
,OL.QTY_SHIPPED
,OL.PRODUCT_PRICE
,OL.EXTENDED_PRICE
,c.CE_EU_TYPE
,oh.contact_phone
,oh.contact_email
,SKU.USER_DEF_NOTE_1
,SKU.CE_COO
, OH.User_def_type_6
,OL.Line_value
,OH.signatory
,oh.freight_terms
HAVING sum(OL.QTY_SHIPPED)>0

ORDER BY OH.CONSIGNMENT
,OH.ORDER_ID
,OL.SKU_ID]]>
	</queryString>
	<field name="ORDER_ID" class="java.lang.String"/>
	<field name="CONSIGNMENT" class="java.lang.String"/>
	<field name="ADR CUST" class="java.lang.String"/>
	<field name="INV_TOTAL_1" class="java.math.BigDecimal"/>
	<field name="ADR NAME" class="java.lang.String"/>
	<field name="ADR CONTACT" class="java.lang.String"/>
	<field name="ADR ADDRESS 1" class="java.lang.String"/>
	<field name="ADR ADDRESS 2" class="java.lang.String"/>
	<field name="ADR TOWN" class="java.lang.String"/>
	<field name="ADR COUNTY" class="java.lang.String"/>
	<field name="CONTACT_PHONE" class="java.lang.String"/>
	<field name="CONTACT_EMAIL" class="java.lang.String"/>
	<field name="ADR POSTCODE" class="java.lang.String"/>
	<field name="ADR COUNTRY" class="java.lang.String"/>
	<field name="INV_NAME" class="java.lang.String"/>
	<field name="INV_CONTACT" class="java.lang.String"/>
	<field name="INV_ADDR1" class="java.lang.String"/>
	<field name="INV_ADDR2" class="java.lang.String"/>
	<field name="INV_TOWN" class="java.lang.String"/>
	<field name="INV_COUNTY" class="java.lang.String"/>
	<field name="INV_POSTCODE" class="java.lang.String"/>
	<field name="INV COUNTRY" class="java.lang.String"/>
	<field name="CURRENCY" class="java.lang.String"/>
	<field name="CURRENCY_SYMBOL" class="java.lang.String"/>
	<field name="DISPATCH_METHOD" class="java.lang.String"/>
	<field name="EXCHANGE" class="java.lang.String"/>
	<field name="EXCHANGE2" class="java.lang.String"/>
	<field name="DESPATCHED DATE" class="java.lang.String"/>
	<field name="SKU_ID" class="java.lang.String"/>
	<field name="PROD NAME" class="java.lang.String"/>
	<field name="PROD DESCRIPTION" class="java.lang.String"/>
	<field name="PROD COLOUR" class="java.lang.String"/>
	<field name="SKU_SIZE" class="java.lang.String"/>
	<field name="COMMODITY_DESC" class="java.lang.String"/>
	<field name="HTS CODE" class="java.lang.String"/>
	<field name="COMPOSITION" class="java.lang.String"/>
	<field name="ORIGIN" class="java.lang.String"/>
	<field name="QTY" class="java.math.BigDecimal"/>
	<field name="QTY_SHIPPED" class="java.math.BigDecimal"/>
	<field name="UNIT PRICE" class="java.math.BigDecimal"/>
	<field name="AMOUNT" class="java.math.BigDecimal"/>
	<field name="TAX ..." class="java.lang.String"/>
	<field name="VALUE" class="java.math.BigDecimal"/>
	<field name="UPSTracking" class="java.lang.String"/>
	<field name="Line_value" class="java.math.BigDecimal"/>
	<field name="SIGNATORY" class="java.lang.String"/>
	<field name="FREIGHT TERMS" class="java.lang.String"/>
	<variable name="QTY_SHIPPED_1" class="java.math.BigDecimal" resetType="Group" resetGroup="GRP_Order" calculation="Sum">
		<variableExpression><![CDATA[$F{QTY_SHIPPED}]]></variableExpression>
	</variable>
	<variable name="VALUE_1" class="java.math.BigDecimal" resetType="Group" resetGroup="GRP_Order_2" calculation="Sum">
		<variableExpression><![CDATA[$F{VALUE}]]></variableExpression>
	</variable>
	<variable name="QTY_SHIPPED_2" class="java.math.BigDecimal" resetType="Group" resetGroup="GRP_Order_2" calculation="Sum">
		<variableExpression><![CDATA[$F{QTY_SHIPPED}]]></variableExpression>
	</variable>
	<variable name="VALUE_2" class="java.math.BigDecimal" resetType="Group" resetGroup="GRP_Order_2" calculation="Sum">
		<variableExpression><![CDATA[$F{VALUE}]]></variableExpression>
	</variable>
	<variable name="QTY_1" class="java.math.BigDecimal" resetType="Group" resetGroup="GRP_Order" calculation="Sum">
		<variableExpression><![CDATA[$F{QTY}]]></variableExpression>
	</variable>
	<variable name="LINE_VALUE_1" class="java.math.BigDecimal" resetType="Group" resetGroup="GRP_Order_2" calculation="Sum">
		<variableExpression><![CDATA[$F{Line_value}]]></variableExpression>
	</variable>
	<group name="GRP_Order" isStartNewPage="true" footerPosition="ForceAtBottom">
		<groupExpression><![CDATA[$F{ORDER_ID}]]></groupExpression>
		<groupFooter>
			<band height="20">
				<staticText>
					<reportElement x="684" y="0" width="76" height="20" uuid="c5f8bee2-3651-448c-a73f-05975664fec8"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Arial" size="10" isBold="true"/>
					</textElement>
					<text><![CDATA[Total Units]]></text>
				</staticText>
				<textField>
					<reportElement x="762" y="0" width="40" height="20" uuid="141cc944-7681-437b-bf53-b6d41814a72a"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Arial" size="10"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{QTY_1}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<group name="GRP_Order_2">
		<groupExpression><![CDATA[$F{ORDER_ID}]]></groupExpression>
		<groupFooter>
			<band height="44">
				<staticText>
					<reportElement x="615" y="11" width="65" height="20" uuid="c8b555d0-0eba-4c1a-93e9-3014361bd702"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Arial" size="10" isBold="true"/>
					</textElement>
					<text><![CDATA[Total: ]]></text>
				</staticText>
				<textField isBlankWhenNull="true">
					<reportElement x="699" y="11" width="31" height="20" uuid="980912ed-7245-4c0f-9fe0-b60badfdd800"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Arial"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{CURRENCY_SYMBOL}]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement x="730" y="11" width="70" height="20" uuid="7bf3118c-005d-4308-8e2d-084ee422318b"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{LINE_VALUE_1}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<pageHeader>
		<band height="215" splitType="Stretch">
			<staticText>
				<reportElement x="0" y="78" width="80" height="12" uuid="5ab5f82f-2bba-4643-9e6b-f4de7edac230"/>
				<textElement verticalAlignment="Bottom">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Shipping Address]]></text>
			</staticText>
			<staticText>
				<reportElement x="134" y="78" width="80" height="12" uuid="bdfa299a-2b80-4eb3-857a-2b4276ce6190"/>
				<textElement verticalAlignment="Bottom">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Billing Address]]></text>
			</staticText>
			<staticText>
				<reportElement x="255" y="78" width="80" height="12" uuid="88244d52-54ae-46c2-b618-3bb369186fa7"/>
				<textElement verticalAlignment="Bottom">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Senders Address]]></text>
			</staticText>
			<staticText>
				<reportElement x="371" y="78" width="80" height="12" uuid="3fd6aab7-2505-4902-9556-9d531e8c9f1e"/>
				<textElement verticalAlignment="Bottom">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Currency]]></text>
			</staticText>
			<staticText>
				<reportElement x="476" y="78" width="80" height="12" uuid="bc3961d5-e9a5-422d-9f2c-e4984f75bdc0"/>
				<textElement verticalAlignment="Bottom">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Shipping Method]]></text>
			</staticText>
			<staticText>
				<reportElement x="595" y="12" width="90" height="12" uuid="222c2d6a-edab-42fe-9b46-45c201bb260d"/>
				<textElement>
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[DISPATCH DATE]]></text>
			</staticText>
			<staticText>
				<reportElement x="595" y="34" width="90" height="12" uuid="3aee61d0-2f4a-409e-949e-4066f77ed06d"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[DISPATCHED FROM]]></text>
			</staticText>
			<staticText>
				<reportElement x="595" y="87" width="90" height="12" forecolor="#FFFFFF" uuid="42edcf78-45c1-4f10-9164-39223df02500"/>
				<textElement>
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[TRACKING #]]></text>
			</staticText>
			<staticText>
				<reportElement x="595" y="100" width="90" height="12" uuid="8c12ec08-fa94-4fcf-bc07-d47a7e6ddfb5"/>
				<textElement>
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[INVOICE DATE]]></text>
			</staticText>
			<staticText>
				<reportElement x="595" y="113" width="90" height="12" uuid="4c0681aa-d64f-4aa7-aeab-2a25fb4e4df4"/>
				<textElement>
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[INVOICE NUMBER]]></text>
			</staticText>
			<frame>
				<reportElement x="0" y="91" width="120" height="103" isRemoveLineWhenBlank="true" uuid="a7569a0e-693b-40fc-9d04-9f773c269b08"/>
				<textField isBlankWhenNull="true">
					<reportElement x="0" y="0" width="110" height="10" isRemoveLineWhenBlank="true" uuid="2daf3e8a-3835-454d-ac33-f4b6452829c2"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="7"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{ADR NAME}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="0" y="10" width="110" height="10" isRemoveLineWhenBlank="true" uuid="1d3ef20c-631b-4164-9ff0-184361ed162e"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="7"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{ADR CONTACT}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="0" y="20" width="110" height="10" isRemoveLineWhenBlank="true" uuid="dc5c32cb-69c4-4eb4-9572-f1866c80f2f5"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="7"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{ADR ADDRESS 1}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="0" y="30" width="110" height="10" isRemoveLineWhenBlank="true" uuid="f14c3130-0977-406d-b459-6f46433a7013"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="7"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{ADR ADDRESS 2}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="0" y="40" width="110" height="10" isRemoveLineWhenBlank="true" uuid="20f779d1-2b1c-4d4c-9aaf-b40945de5b23"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="7"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{ADR TOWN}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="0" y="50" width="110" height="10" isRemoveLineWhenBlank="true" uuid="1635b542-15c4-4f71-97fd-85bc146f23de"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="7"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{ADR COUNTY}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="0" y="60" width="110" height="10" isRemoveLineWhenBlank="true" uuid="8fbee6e0-d698-4e42-820d-fbbf57ceda58"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="7"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{ADR POSTCODE}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="0" y="70" width="110" height="10" isRemoveLineWhenBlank="true" uuid="79c7bde4-714d-4e2c-9ae0-5f05ff8908ce"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="7"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{ADR COUNTRY}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="21" y="92" width="95" height="10" uuid="882ecefb-5cf0-41d6-a1d1-1c346eff492d"/>
					<textElement>
						<font fontName="Arial" size="7"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{CONTACT_EMAIL}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="0" y="92" width="21" height="10" uuid="3861ce56-5873-45b0-9847-d4bf38f601e1"/>
					<textElement>
						<font fontName="Arial" size="7"/>
					</textElement>
					<text><![CDATA[Email:]]></text>
				</staticText>
				<textField isBlankWhenNull="true">
					<reportElement x="21" y="82" width="95" height="10" uuid="add6e2b3-35f6-4694-a4a1-24345f4aa65d"/>
					<textElement>
						<font fontName="Arial" size="7"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{CONTACT_PHONE}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="0" y="82" width="21" height="10" uuid="b781124d-3208-4165-93fb-c68f5d356228"/>
					<textElement>
						<font fontName="Arial" size="7"/>
					</textElement>
					<text><![CDATA[Tel:]]></text>
				</staticText>
			</frame>
			<frame>
				<reportElement x="134" y="91" width="120" height="88" isRemoveLineWhenBlank="true" uuid="3bfacf4a-4dd4-4aa5-8512-04e7807cbf50"/>
				<textField isBlankWhenNull="true">
					<reportElement x="0" y="0" width="110" height="10" isRemoveLineWhenBlank="true" uuid="fd00e718-c1d0-4232-9257-4310b3312464"/>
					<textElement>
						<font fontName="Arial" size="7"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{INV_NAME}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="0" y="10" width="110" height="10" isRemoveLineWhenBlank="true" uuid="16de31f0-ffcb-4ead-b041-d7a9b5c49e61"/>
					<textElement>
						<font fontName="Arial" size="7"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{INV_CONTACT}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="0" y="20" width="110" height="10" isRemoveLineWhenBlank="true" uuid="6fc09795-8aee-4702-988f-8cd3c33f6e89"/>
					<textElement>
						<font fontName="Arial" size="7"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{INV_ADDR1}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="0" y="30" width="110" height="10" isRemoveLineWhenBlank="true" uuid="c99a73a2-c8a9-40b1-b6a3-a03ea87df9e1"/>
					<textElement>
						<font fontName="Arial" size="7"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{INV_ADDR2}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="0" y="40" width="110" height="10" isRemoveLineWhenBlank="true" uuid="832d71df-394a-46c9-8c27-01abed450416"/>
					<textElement>
						<font fontName="Arial" size="7"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{INV_TOWN}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="0" y="50" width="110" height="10" isRemoveLineWhenBlank="true" uuid="060a0ad5-bcdd-41bb-aa75-68c54eb172f7"/>
					<textElement>
						<font fontName="Arial" size="7"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{INV_COUNTY}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="0" y="60" width="110" height="10" isRemoveLineWhenBlank="true" uuid="099cf8a8-c6f5-4201-b939-ff125f2ae1b0"/>
					<textElement>
						<font fontName="Arial" size="7"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{INV_POSTCODE}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="0" y="70" width="110" height="10" isRemoveLineWhenBlank="true" uuid="ffe6adaf-d753-4a21-87a4-8b54b47c5915"/>
					<textElement>
						<font fontName="Arial" size="7"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{INV COUNTRY}]]></textFieldExpression>
				</textField>
			</frame>
			<textField isBlankWhenNull="true">
				<reportElement x="371" y="92" width="100" height="20" uuid="598365ea-8a2a-473d-bd47-c2d42c211214"/>
				<textElement>
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{CURRENCY}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="476" y="112" width="80" height="12" uuid="c67a1c59-7a51-4436-b6fd-f9934c6fb41d"/>
				<textElement>
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Inco Terms]]></text>
			</staticText>
			<staticText>
				<reportElement x="476" y="146" width="119" height="12" uuid="183276c8-ca10-41dc-9b1d-9b9dde5a6820"/>
				<textElement>
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Tracking no.]]></text>
			</staticText>
			<line>
				<reportElement x="0" y="196" width="802" height="1" uuid="e809cf14-7202-4474-bae6-6ae970bd21b2"/>
				<graphicElement>
					<pen lineWidth="2.0"/>
				</graphicElement>
			</line>
			<staticText>
				<reportElement x="0" y="201" width="80" height="14" uuid="8840c532-d7a2-4197-8b4e-3173eb864c7b"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="7" isBold="true"/>
				</textElement>
				<text><![CDATA[CODE]]></text>
			</staticText>
			<staticText>
				<reportElement x="80" y="201" width="154" height="14" uuid="7cca3a2a-e6bd-4718-9ebc-39d21a40328e"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="7" isBold="true"/>
				</textElement>
				<text><![CDATA[DESCRIPTION]]></text>
			</staticText>
			<staticText>
				<reportElement x="236" y="201" width="79" height="14" uuid="201e5798-6b35-433c-9a01-4693f7da8d98"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="7" isBold="true"/>
				</textElement>
				<text><![CDATA[COLOUR]]></text>
			</staticText>
			<staticText>
				<reportElement x="315" y="201" width="37" height="14" uuid="f2567eed-858d-4412-a423-35cd37f5f187"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="7" isBold="true"/>
				</textElement>
				<text><![CDATA[SIZE]]></text>
			</staticText>
			<staticText>
				<reportElement x="356" y="201" width="101" height="14" uuid="53860847-caa2-4e3b-b06d-cd0d9ec8757c"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="7" isBold="true"/>
				</textElement>
				<text><![CDATA[CATEGORY]]></text>
			</staticText>
			<staticText>
				<reportElement x="464" y="201" width="72" height="14" uuid="a055e1a5-ae5c-4e21-85ff-e993b752d8f7"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="7" isBold="true"/>
				</textElement>
				<text><![CDATA[HTS CODE]]></text>
			</staticText>
			<staticText>
				<reportElement x="297" y="24" width="208" height="20" uuid="5acae3e1-97c2-41ff-a851-a2ee4a821cd4"/>
				<textElement textAlignment="Center">
					<font size="14" isBold="true"/>
				</textElement>
				<text><![CDATA[COMMERCIAL INVOICE]]></text>
			</staticText>
			<textField>
				<reportElement x="476" y="92" width="108" height="20" uuid="4a873109-ed98-4f2a-b482-7112f4a8d165"/>
				<textElement>
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{DISPATCH_METHOD}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="539" y="201" width="116" height="14" uuid="7acf18b6-1ea5-42a7-bbea-f287b048fa85"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="7" isBold="true"/>
				</textElement>
				<text><![CDATA[COMPOSITION]]></text>
			</staticText>
			<staticText>
				<reportElement x="655" y="201" width="57" height="14" uuid="7ce712e1-90b3-49a8-ae6d-e99988569f16"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="7" isBold="true"/>
				</textElement>
				<text><![CDATA[ORIGIN]]></text>
			</staticText>
			<staticText>
				<reportElement x="714" y="201" width="40" height="14" uuid="fee772bc-11dc-4934-8735-9c85d787229f"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial" size="7" isBold="true"/>
				</textElement>
				<text><![CDATA[QTY ]]></text>
			</staticText>
			<textField>
				<reportElement x="686" y="113" width="117" height="12" uuid="3f48d7c5-84b8-4567-953f-9e8ce0e186e8"/>
				<textElement>
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{ORDER_ID}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="686" y="24" width="117" height="63" uuid="8982fcc7-dfea-4bf1-b966-1d62de53ed2e"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="7"/>
				</textElement>
				<text><![CDATA[Cefinn
c/o Torque
Wortley Moor Road
Wortley, Leeds, LS12 4JH
Attn: Peter Coman
Email: cefinn.admin@torque.eu]]></text>
			</staticText>
			<textField pattern="dd MMMMM yyyy">
				<reportElement x="686" y="12" width="117" height="12" uuid="43d3f03c-a09c-4fac-a341-5d0f4b17e55a"/>
				<textElement>
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[new java.util.Date()]]></textFieldExpression>
			</textField>
			<textField pattern="dd MMMMM yyyy">
				<reportElement x="686" y="100" width="117" height="12" uuid="03014b26-dd45-4d7c-ba5e-8892dc8c1e3f"/>
				<textElement>
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[new java.util.Date()]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="255" y="92" width="101" height="87" forecolor="#000000" uuid="2b76bc55-7cda-4f5c-a409-d0d470f42b88"/>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" size="7"/>
				</textElement>
				<text><![CDATA[Cefinn
The Phoneix Brewery
13 Bramley Road
London
W10 6SZ
help@cefinn.com

]]></text>
			</staticText>
			<staticText>
				<reportElement x="297" y="3" width="208" height="20" uuid="c4d3c4bf-856b-438e-8aab-05558717a063"/>
				<textElement textAlignment="Center">
					<font size="14" isBold="true"/>
				</textElement>
				<text><![CDATA[Cefinn]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement x="476" y="158" width="286" height="15" uuid="b0a06059-6dd4-4682-8c5e-118492a2a1b6"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{SIGNATORY}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="754" y="201" width="40" height="14" uuid="b205d2f1-c35a-47bf-af0f-ccbf2981d0cd"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial" size="7" isBold="true"/>
				</textElement>
				<text><![CDATA[VALUE]]></text>
			</staticText>
			<textField>
				<reportElement x="476" y="124" width="108" height="20" uuid="dbff59e9-0497-466a-8a7e-9bddc5cde039"/>
				<textElement>
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{FREIGHT TERMS}]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<detail>
		<band height="21" splitType="Stretch">
			<textField>
				<reportElement x="0" y="0" width="80" height="20" uuid="9b1ee99c-80a4-4fc5-b9e9-d765ea77338c"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{SKU_ID}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="80" y="0" width="154" height="20" uuid="400c72e0-6b9b-4dcf-a103-5a6ac94f3044"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{PROD DESCRIPTION}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="236" y="0" width="79" height="20" uuid="886f2327-f0e4-4611-8e3b-49dbccec8e87"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{PROD COLOUR}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="315" y="0" width="37" height="20" uuid="a3a92e17-a349-411f-b515-a1c1834be170"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{SKU_SIZE}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="356" y="0" width="102" height="20" uuid="e9401adc-d6b5-44e8-bdac-a67307086d6c"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{COMMODITY_DESC}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="464" y="0" width="72" height="20" uuid="1c26d91c-c6be-4b64-afab-139335b9f51f"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{HTS CODE}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="539" y="0" width="116" height="20" uuid="dbbafda7-c5f7-444b-bf12-f08c715a1b6d"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{COMPOSITION}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="655" y="0" width="57" height="20" uuid="8677778f-5ba2-4d30-b201-34b1047a1807"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{ORIGIN}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="714" y="0" width="40" height="20" uuid="d193fee9-d6d8-4d77-a36b-50236ed31698"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial" size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{QTY}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="754" y="1" width="40" height="20" uuid="2ae3986e-0991-405d-9184-479170e2dfeb"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial" size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Line_value}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<pageFooter>
		<band height="22" splitType="Stretch">
			<staticText>
				<reportElement x="0" y="7" width="227" height="15" uuid="d6612aa4-f402-40ec-8317-01b659995c96"/>
				<textElement verticalAlignment="Bottom">
					<font fontName="Arial" size="6"/>
				</textElement>
				<text><![CDATA[REG. NO: 10410585  VAT NO: 252 547 308]]></text>
			</staticText>
			<textField>
				<reportElement x="680" y="2" width="80" height="20" uuid="9f0d84e4-4f91-456d-b31b-d964e1108b65"/>
				<textElement textAlignment="Right"/>
				<textFieldExpression><![CDATA["Page "+$V{PAGE_NUMBER}+" of"]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report">
				<reportElement x="760" y="2" width="40" height="20" uuid="f35f32d7-544c-4cc7-bcd9-1429443056f6"/>
				<textFieldExpression><![CDATA[" " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
</jasperReport>
