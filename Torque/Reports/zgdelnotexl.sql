SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 2000
SET TRIMSPOOL ON

select 'Order,Customer,Contact,Date,SKU,EAN,Description,Qty Ordered,Qty Picked,PO,Shortages' A1 from dual
union all
select "OrderNo" || ',' || "Customer" || ',' || "Contact" || ',' || "Date" || ',' || "Sku" || ',' || "EAN" || ',' || "Desc" || ',' || "QtyOrdered" || ',' || "QtyPicked" || ',' || "PO" || ',' || "Shortages"
from (
select max(oh.customer_id)                  as "Customer"
, max(oh.contact)                           as "Contact"
, oh.order_id                               as "OrderNo"
, to_char(sysdate, 'DD/MM/YYYY HH24:MI')    as "Date"
, ol.sku_id                                 as "Sku"
, max(s.EAN)			                    as "EAN"
, max(nvl(ol.user_def_note_1, s.description))                    as "Desc"
, sum(nvl(ol.qty_ordered,0))                as "QtyOrdered"
, sum(nvl(ol.qty_picked,0))                 as "QtyPicked"
, max(oh.purchase_order)		            as "PO"
, sum(nvl(ol.qty_ordered,0)) - sum(nvl(ol.qty_picked,0)) as "Shortages"
from order_header oh
inner join order_line ol on ol.order_id = oh.order_id and ol.client_id = oh.client_id
inner join sku s on s.client_id = ol.client_id and s.sku_id = ol.sku_id
left join country c on nvl(c.iso3_id, iso2_id) = oh.country
where oh.client_id = 'ZG'
and (oh.consignment = '&&2' or oh.order_id = '&&2')
group by oh.order_id, ol.sku_id
order by oh.order_id, ol.sku_id
)
/