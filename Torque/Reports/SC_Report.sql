SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

column A format A18
column B format A40
column BB format A12
column C format 9999999
column D format A12
column E format A12


--select 'Stock Summary Report - for client ID &&2 - as at  ' 
--    || to_char(sysdate,'dd/mm/yy  hh24:mi:ss') from dual;


select 'SKU,Description,Location,Stock,,Received'
from dual;


break on A dup on report
compute sum label 'Total' of C on A report


select
''''||i.sku_id||''''  A,
sku.description  B,
i.location_id BB,
sum(i.qty_on_hand) C,
' Counted' D,
'  ' E
from inventory i,sku
where i.client_id='&&2'
and sku.client_id='&&2'
and i.Sku_id=sku.sku_id
--and i.location_id not in ('SUSPENSE','PACK1','MARSHALB','CONTAINER')
and i.count_dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
group by i.sku_id,sku.description,i.location_id
union
select
''''||i.sku_id||''''  A,
sku.description  B,
i.location_id BB,
sum(i.qty_on_hand) C,
' NOT Counted' D,
nvl(to_char(i.receipt_dstamp,'DD/MM/YYYY'),'Null') E
from inventory i,sku
where i.client_id='&&2'
and sku.client_id='&&2'
and i.Sku_id=sku.sku_id
--and i.location_id not in ('SUSPENSE','PACK1','MARSHALB','CONTAINER')
and (i.count_dstamp not between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
or i.count_dstamp is null)
--and i.receipt_dstamp<to_date('&&4', 'DD-MON-YYYY')
group by i.sku_id,sku.description,i.location_id,i.receipt_dstamp
order by A
/
