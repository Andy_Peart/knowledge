SET FEEDBACK OFF                 
set verify off
set wrap off
set colsep ','

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 132
set trimspool on

column A heading 'User ID' format a18
column B heading 'Task Type' format a18
column XX heading 'WWW' format a12
column C heading 'Tasks' format 99999999
column D heading 'Units' format 99999999
column Z noprint


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

--break on report 
--break on A dup 

--spool mwtntpick.csv

select 'Retail Picks by CONSIGNMENT from &&2 &&4 to &&3 &&5 - for client MOUNTAIN' from DUAL;

select 'Consignment,Task,Store Id,Order Id,TNT,Units,Lines,Picked,Tasks,Time Taken,KPI' from DUAL;

select
ZZ Z,
R||','||
--A||','||
B||','||
XX||','||
A2||','||
X||','||
OQ||','||
OL||','||
D||','||
C||','||
F||','||
replace(to_char((D/F)*3600,'999999'),' ','')
from (select
'A' ZZ,
CASE
WHEN it.consignment like '%TNT%' THEN 'Yes' ELSE 'No' END X,
it.consignment R,
--it.user_id A,
it.reference_id A2,
it.code B,
it.work_group XX,
--DECODE(it.work_group,'WWW','Mail Order','Outlets') XX,
count(*) C,
sum(it.update_qty) D,
sum(it.elapsed_time) F
from inventory_transaction it
where it.dstamp between to_date('&&2 &&4', 'DD-MON-YYYY HH24:MI:SS') and to_date('&&3 &&5', 'DD-MON-YYYY HH24:MI:SS')
and it.client_id='MOUNTAIN'
and it.reference_id like 'MOR%'
and it.update_qty<>0
and it.station_id not like 'Auto%'
and it.code in('Pick','Consol')
and it.elapsed_time>0
and it.from_loc_id<>'P999'
group by it.consignment,it.reference_id,it.code,it.work_group
union
select
'A' ZZ,
CASE
WHEN it.consignment like '%TNT%' THEN 'Yes' ELSE 'No' END X,
it.consignment R,
--it.user_id A,
it.reference_id A2,
it.code B,
it.work_group XX,
--DECODE(it.work_group,'WWW','Mail Order','Outlets') XX,
count(*) C,
sum(it.update_qty) D,
sum(it.elapsed_time) F
from inventory_transaction_archive it
where it.dstamp between to_date('&&2 &&4', 'DD-MON-YYYY HH24:MI:SS') and to_date('&&3 &&5', 'DD-MON-YYYY HH24:MI:SS')
and it.client_id='MOUNTAIN'
and it.reference_id like 'MOR%'
and it.update_qty<>0
and it.station_id not like 'Auto%'
and it.code in ('Pick','Consol')
and it.elapsed_time>0
and it.from_loc_id<>'P999'
group by it.consignment,it.reference_id,it.code,it.work_group),(select
order_id ORD,
sum(qty_ordered) OQ,
count(*) OL
from order_line
where client_id='MOUNTAIN'
group by order_id)
where A2=ORD
order by ZZ,R,B,XX
/
--spool off

