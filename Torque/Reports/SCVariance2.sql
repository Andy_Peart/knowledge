SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET WRAP OFF
SET HEADING ON


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 68
SET NEWPAGE 0
SET LINESIZE 132


column Z heading 'List ID' format A10
column A heading 'Location' format A8
column B heading 'Tag ID' format A10
column C heading 'SKU' format A18
column D heading 'Description' format a40
column E heading 'Colour' format a10
column F heading 'Original| Qty' format 99999
column G heading 'Counted| Qty' format 99999
column H heading 'Update| Qty' format 99999
column J heading 'Found' format a10

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

ttitle  LEFT 'Stock Check Variances on LIST ID '&&3' for Client ID '&&2' as at ' report_date skip 2


--select 'List ID,Location,Tag ID,SKU,Description,Colour,Orig Qty,Count Qty,Update Qty,Found' from DUAL;

--break on BB dup skip 1 on report
break on report

--compute sum label 'Total' of DD on BB
compute sum label 'Totals' of F G H on report


select
--i.list_id Z,
i.from_loc_id A,
i.tag_id  B,
i.sku_id C,
sku.description D,
sku.colour E,
nvl(i.original_qty,0) F,
nvl(i.original_qty,0)+nvl(i.update_qty,0) G,
nvl(i.update_qty,0) H,
' ___________ ' J
from inventory_transaction i,sku
where i.client_id='&&2'
and code='Stock Check'
and list_id like '%&&3'
and sku.client_id='&&2'
and i.sku_id=sku.sku_id
and i.update_qty<>0
order by
A,C
/
SET HEADING OFF
SET PAGESIZE 0

select '' from DUAL;
select '' from DUAL;

select
'Total Variances = '||count(*)
from inventory_transaction i
where i.client_id='&&2'
and code='Stock Check'
and list_id like '%&&3'
and i.update_qty<>0
/
select
'Total SKUs checked = '||count(*)
from inventory_transaction i
where i.client_id='&&2'
and code='Stock Check'
and list_id like '%&&3'
/
select
'Locations checked = '||count(distinct i.from_loc_id)
from inventory_transaction i
where i.client_id='&&2'
and code='Stock Check'
and list_id like '%&&3'
/
select
'Location/SKU variants checked = '||count(distinct i.from_loc_id||i.sku_id)
from inventory_transaction i
where i.client_id='&&2'
and code='Stock Check'
and list_id like '%&&3'
/

