--Want the report to show all fully allocated mail orders 
--so any order beginning with prefix "C" 
--where quantity ordered equals quantity tasked. . . 

SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ' '
set heading on


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 66
SET NEWPAGE 0
SET LINESIZE 240


column A heading 'Order Type' format A12
column AA heading 'Order' format A12
column BB heading 'Date' format A14
column CC heading 'Order Qty' format 999999

TTITLE LEFT 'Fully Allocated Mail Orders'


break on A dup skip 1


select
DDD A,
AAA AA,
to_char(BBB,'DD-MON-YYYY') BB,
OOO CC from
(select
oh.order_type  DDD,
oh.order_id  AAA,
oh.creation_date BBB,
nvl(sum(ol.qty_ordered),0) OOO,
nvl(sum(ol.qty_tasked),0) TTT
from order_header oh, order_line ol
where oh.client_id='MOUNTAIN'
and oh.order_type like 'WEB'
and oh.order_id=ol.order_id
group by
oh.order_type,
oh.order_id,
oh.creation_date)
where OOO=TTT
order by
A,AA
/

set heading off


select 'NOTHING TO REPORT' from
(select
count(*) ct
from
(select
oh.order_type  DDD,
oh.order_id  AAA,
oh.creation_date BBB,
nvl(sum(ol.qty_ordered),0) OOO,
nvl(sum(ol.qty_tasked),0) TTT
from order_header oh, order_line ol
where oh.client_id='MOUNTAIN'
and oh.order_type like 'WEB'
and oh.order_id=ol.order_id
group by
oh.order_type,
oh.order_id,
oh.creation_date)
where OOO=TTT)
where ct=0
/
