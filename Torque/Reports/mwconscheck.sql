SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 160



column S heading 'Route' format A8
column A heading 'Store' format A8
column B heading 'Store Name' format A28
column C heading 'CLIENT' format A10
column D heading 'Order ID' format A14
column E heading 'STATUS' format A14
column F heading 'DESPATCH DATE' format A16
column G heading 'DELIVERY DATE' format A16
column P heading 'LINES' format 99999
column Q heading 'UNITS' format 99999
column QP heading 'PALLETS' format 99999
column CN heading 'CONSIGNMENT' format A14
column SX format a8 noprint


--select '&&2 &&3' from DUAL;

--set termout off
--spool mwconscheck.csv


Select 'Store,Store Name,Order ID,Lines,Units,Consignment,Route,Reqd Pallets,Picker,Packer,Cartons,,' from DUAL;


--break on S skip 1 dup

--compute sum of QQQ on CCC

select
oh.customer_id A,
ad.name B,
oh.order_id D,
CCC P,
QQQ Q,
oh.consignment CN,
ad.user_def_type_1 S,
(QQQ+149)/300 QP,
'                ',
'                ',
'                ',
'   '
from order_header oh, address ad,
(select
order_id OOO,
count(*) CCC,
sum(qty_to_move) QQQ
from order_header oh, move_task mt
where oh.client_id='&&2'
and mt.client_id='&&2'
and oh.order_id=mt.task_id
group by
order_id)
where oh.client_id='&&2'
and ad.client_id='&&2'
and oh.work_group<>'WWW'
and oh.work_group=ad.address_id
and oh.order_id=OOO
and oh.status in ('Released','Allocated','In Progress')
and oh.consignment like '&&3%'
order by
S,A
/

--spool off
--set termout on

