SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON

set pagesize 999
set linesize 160
set heading on

clear columns
clear breaks
clear computes


TTitle 'TML Nett Adjustments from &&2 to &&3' skip 2

column A heading 'Type' format A14
column B heading 'Client' format A7
column C heading 'Number' format 9999999
column D heading 'Nett Qty' format 9999999999

break on B dup skip 2

select
client_id B,
'1. SUSPENSE' A,
count(*) C,
sum(update_qty) D
from inventory_transaction
where Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and client_id like 'T%'
--and update_qty<>0
and code='Adjustment'
and to_loc_id='SUSPENSE'
and from_loc_id='SUSPENSE'
and reason_id not like 'T%'
--and notes<>'store return'
group by
client_id
union
select
client_id B,
'2. Other' A,
count(*) C,
sum(update_qty) D
from inventory_transaction
where Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and client_id like 'T%'
--and update_qty<>0
and code='Adjustment'
and to_loc_id<>'SUSPENSE'
and from_loc_id<>'SUSPENSE'
and reason_id not like 'T%'
--and notes<>'store return'
group by
client_id
order by
B,A
/
