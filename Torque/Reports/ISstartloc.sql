SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Consignment,Start Loc.,Unit Number' from dual
union all
select "Cons" || ',' || "Loc" || ',' || "Unit Number" from
(
select "Cons", min("Loc") as "Loc", ("Sum"*'&&4'+1) as "Unit Number"
from
(
select consignment  as "Cons"
, from_loc_id       as "Loc"
, floor(sum(qty_to_move) over (order by from_loc_id asc rows between unbounded preceding and current row)/'&&4') as "Sum"
from move_task
where client_id = 'IS'
and work_zone = '&&3'
and consignment = '&&2'
group by from_loc_id
, qty_to_move
, consignment
)
group by "Sum", "Cons"
order by "Unit Number"
)
/
--spool off