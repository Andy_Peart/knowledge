SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 200
set trimspool on
set newpage 0
set verify off
set colsep ','

column A2 format a16
column B format a10


break on A2 skip 1
compute sum label 'Total' of B2 D on A2

--spool mwshipdocks.csv

select 'Ship Dock,Date,Lines,Units' from DUAL;

select
oh.ship_dock A2,
trunc(oh.shipped_date) A,
count(*) B2,
sum (ol.qty_shipped) D
from order_header oh,order_line ol
where oh.client_id = 'MOUNTAIN'
and oh.order_id like 'MOR%'
--and oh.ship_dock in ('GRID','DESPATCH','VL')
and oh.shipped_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and ol.client_id = 'MOUNTAIN'
and ol.order_id=oh.order_id
and ol.qty_shipped>0
group by
oh.ship_dock,
trunc(oh.shipped_date)
union
select
oh.ship_dock A2,
trunc(oh.shipped_date) A,
count(*) B2,
sum (ol.qty_shipped) D
from order_header_archive oh,order_line_archive ol
where oh.client_id = 'MOUNTAIN'
and oh.order_id like 'MOR%'
--and oh.ship_dock in ('GRID','DESPATCH','VL')
and oh.shipped_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and ol.client_id = 'MOUNTAIN'
and ol.order_id=oh.order_id
and ol.qty_shipped>0
group by
oh.ship_dock,
trunc(oh.shipped_date)
order by A2,A
/

select ' ' from DUAL;
select ' ' from DUAL;


select
'ALL SHIP DOCKS' A2,
trunc(oh.shipped_date) A,
count(*) B2,
sum (ol.qty_shipped) D
from order_header oh,order_line ol
where oh.client_id = 'MOUNTAIN'
and oh.order_id like 'MOR%'
--and oh.ship_dock in ('GRID','DESPATCH','VL')
and oh.shipped_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and ol.client_id = 'MOUNTAIN'
and ol.order_id=oh.order_id
and ol.qty_shipped>0
group by
trunc(oh.shipped_date)
union
select
'ALL SHIP DOCKS' A2,
trunc(oh.shipped_date) A,
count(*) B2,
sum (ol.qty_shipped) D
from order_header_archive oh,order_line_archive ol
where oh.client_id = 'MOUNTAIN'
and oh.order_id like 'MOR%'
--and oh.ship_dock in ('GRID','DESPATCH','VL')
and oh.shipped_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and ol.client_id = 'MOUNTAIN'
and ol.order_id=oh.order_id
and ol.qty_shipped>0
group by
trunc(oh.shipped_date)
order by A
/

--spool off
