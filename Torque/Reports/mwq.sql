set verify off

ttitle left "Mountain Warehouse - Product Code Search for '&&2'" skip 2

select sku.sku_id SKU, inv.location_id Location, inv.qty_on_hand Qty
from sku, inventory inv
where sku.client_id = 'MOUNTAIN'
and sku.sku_id = inv.sku_id
and inv.location_id not like 'MARSHAL'
and inv.zone_1 not like 'Z3OUT'
and sku.sku_id = ('&&2')
/*group by sku.sku_id*/
order by 2
/
