SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON

col aa format A12
col bb format A15
col cc format A12
col dd format A10
col ee format A30



SELECT 'Order Type,Date,Customer ID,Order Number,Customer Name,Units,Cartons,Gift bag,Gift' as a1 from dual
union all
select 
ord_type || ',' || date_ || ',' || cust_id || ',' || order_no || ',' || cust_name || ',' || units || ',' || cartons || ',' || gift_bag || ',' || gift
from (
SELECT 
	case 
		when oh.order_type = 'WHOLESALE' then 'WHOLESALE'
		when oh.order_type = 'RETAIL' then 'RETAIL'
		when oh.order_type = 'WEB' then 
			case 
				when oh.country  = 'USA' then 'WEB US' 
				when oh.country  = 'FRA' then 'WEB FR' 
				when oh.country  = 'DEU' then 'WEB DE' 
                when oh.country  = 'ITA' then 'WEB IT'
                when oh.country  = 'AUS' then 'WEB AS' 
				else 'WEB Other' 
			end    
	end ord_type,
	case 
		when inv_currency = 'GBP' then case when order_value>=40 then 'Yes' else 'No' end
		when inv_currency = 'EUR' then case when order_value>=50 then 'Yes' else 'No' end
		when inv_currency = 'USD' then case when order_value>=40 then 'Yes' else 'No' end
		else 'No'
	end gift_bag,
	case 
		when NVL(OH.USER_DEF_CHK_2,'N') = 'N' THEN 'No' ELSE 'Yes'
	end gift,
	TRUNC(oh.shipped_date) as date_, oh.customer_id cust_id , itl.reference_id order_no, oh.name as cust_name, sum (itl.update_qty) units, COUNT( DISTINCT itl.container_id) cartons
	FROM inventory_transaction itl LEFT JOIN order_header oh ON itl.reference_id  = oh.order_id AND itl.client_id = oh.client_id
	WHERE itl.client_id = 'BL'
	AND itl.dstamp BETWEEN to_date('&&2', 'DD-MON-YYYY') AND to_date('&&3', 'DD-MON-YYYY')+1
	AND itl.code = 'Shipment'
	AND oh.order_type in ('WHOLESALE', 'WEB','RETAIL')
	GROUP BY 
	case 
		when oh.order_type = 'WHOLESALE' then 'WHOLESALE'
		when oh.order_type = 'RETAIL' then 'RETAIL'
		when oh.order_type = 'WEB' then 
			case 
				when oh.country  = 'USA' then 'WEB US' 
				when oh.country  = 'FRA' then 'WEB FR' 
				when oh.country  = 'DEU' then 'WEB DE'
                when oh.country  = 'ITA' then 'WEB IT'
                when oh.country  = 'AUS' then 'WEB AS' 
				else 'WEB Other' 
			end    
	end,
	case 
		when inv_currency = 'GBP' then case when order_value>=40 then 'Yes' else 'No' end
		when inv_currency = 'EUR' then case when order_value>=50 then 'Yes' else 'No' end
		when inv_currency = 'USD' then case when order_value>=40 then 'Yes' else 'No' end
		else 'No'
	end,
	case 
		when NVL(OH.USER_DEF_CHK_2,'N') = 'N' THEN 'No' ELSE 'Yes'
	end,	
	TRUNC(oh.shipped_date), oh.customer_id, itl.reference_id, oh.name
ORDER BY 1, 2, 4
)
/
