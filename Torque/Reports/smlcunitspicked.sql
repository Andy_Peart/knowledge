/******************************************************************************/
/*                                                                            */
/*                            Elite                                           */
/*                                                                            */
/*     FILE NAME  :   mwshortages3.sql                                        */
/*                                                                            */
/*   DATE     BY   PROJ     ID         DESCRIPTION                            */
/*   ======== ==== ======   ========   =============                          */
/*   26/06/07 BK   MM                  Units Picked by User ID (Date Range)   */
/******************************************************************************/

/* Input Parameters are as follows */
-- 1)  From Date
-- 2)  To Date


set feedback off
set pagesize 68
set linesize 240
set newpage 0
set verify off

clear columns
clear breaks
clear computes

column A heading 'User ID' format A16
column F heading 'Client ID' format A14
column B heading 'Qty' format 999999
column C heading 'Lines' format 999999
column D heading 'Orders' format 999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'Units Picked from '&&2' to '&&3' - for Small Contracts as at ' report_date skip 2

break on F dup skip 1 on report
compute sum label 'Client Totals' of C D B on F
compute sum label 'Overall Totals' of C D B on report


select
user_id A,
client_id F,
count(*) C,
count(distinct reference_id) D,
sum(update_qty) B
from inventory_transaction
where client_id in ('IF','S2','GG','VF','EX')
and code='Pick'
--and elapsed_time>0
and (elapsed_time>0 or (from_loc_id<>'CONTAINER' and to_loc_id<>'CONTAINER'))
and Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by client_id,user_id
order by client_id,user_id
/

