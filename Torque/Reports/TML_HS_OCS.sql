SET FEEDBACK OFF           
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 350
SET TRIMSPOOL ON

break on A skip 1 dup

COLUMN NEWSEQ NEW_VALUE SEQNO

--spool &SEQNO


SELECT 
oh.CONTACT||',"'||
oh.ADDRESS1||'","'||
oh.ADDRESS2||'","'||
oh.TOWN||'","'||
oh.COUNTY||'","'||
oh.POSTCODE||'","'||
COUNTRY.TANDATA_ID||'","'|| 
COUNTRY.ISO3_ID||'",'||
oh.CONTACT_PHONE||','||
oh.CONTACT_EMAIL||','||
oh.ORDER_ID||','||
sm.SHIPPED_DSTAMP||','|| 
SKU.DESCRIPTION||','||
sm.QTY_SHIPPED||','|| 
oh.USER_DEF_TYPE_2||','||
ol.LINE_VALUE||',,'||
substr(SKU.USER_DEF_TYPE_1,1,1)||','||
nvl(SKU.PRODUCT_GROUP,substr(SKU.USER_DEF_TYPE_1,1,2))||','||
oh.PURCHASE_ORDER
from shipping_manifest sm,order_header oh,order_line ol,sku,country
where sm.client_id='TR'
and sm.SHIPPED_DSTAMP between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and (oh.dispatch_method='4' OR oh.dispatch_method='5')
and oh.order_type = 'WEB'
and sm.ORDER_ID=ol.ORDER_ID
and sm.LINE_ID=ol.LINE_ID
and sm.CLIENT_ID=SKU.CLIENT_ID
and sm.SKU_ID=SKU.SKU_ID 
and ol.CLIENT_ID=oh.CLIENT_ID
and ol.ORDER_ID=oh.ORDER_ID
and oh.COUNTRY=COUNTRY.ISO3_ID
ORDER BY sm.PALLET_ID
/


--spool off


