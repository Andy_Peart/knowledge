SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

 
select 'Fulfilment Report - for TM LEWIN for period &&2 to &&3  ' from dual;

select 'Date Shipped/Hold,Sales Order No,Date of Customers Order,Date Order arrived at TORQUE,Customer,Shipped,Ordered,Location'
from dual;

select
to_char(oh.shipped_date, 'DD/MM/YY ') ||','||
oh.order_id ||','||
to_char(oh.order_date,'DD/MM/YY ') ||','||
to_char(oh.creation_date,'DD/MM/YY ') ||','||
oh.customer_id ||','||
nvl(sum(ol.qty_shipped),0) ||','||
nvl(sum(ol.qty_ordered),0) 
from order_header oh, order_line ol
where  oh.order_id=ol.order_id
and oh.status='Shipped'
and oh.shipped_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by
to_char(oh.shipped_date, 'DD/MM/YY '),
oh.order_id,
to_char(oh.order_date,'DD/MM/YY '),
to_char(oh.creation_date,'DD/MM/YY '),
oh.customer_id
order by
to_char(oh.shipped_date, 'DD/MM/YY '),
oh.order_id
/
select
to_char(oh.shipped_date, 'DD/MM/YY ') ||','||
oh.order_id ||','||
to_char(oh.order_date,'DD/MM/YY ') ||','||
to_char(oh.creation_date,'DD/MM/YY ') ||','||
oh.customer_id ||','||
nvl(sum(ol.qty_shipped),0) ||','||
nvl(sum(ol.qty_ordered),0)
from order_header oh, order_line ol
where  oh.order_id=ol.order_id
and oh.status='Hold'
and oh.order_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by
to_char(oh.shipped_date, 'DD/MM/YY '),
oh.order_id,
to_char(oh.order_date,'DD/MM/YY '),
to_char(oh.creation_date,'DD/MM/YY '),
oh.customer_id
order by
to_char(oh.shipped_date, 'DD/MM/YY '),
oh.order_id
/
select
',,,,Totals' as A,
nvl(sum(ol.qty_shipped),0) ||','||
nvl(sum(ol.qty_ordered),0)
from order_header oh, order_line ol
where  oh.order_id=ol.order_id and 
((oh.status='Shipped' and oh.shipped_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1) or
(oh.status='Hold' and oh.order_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1))
/
select
'Orders' as B,
count(*)
from order_header oh
where
((oh.status='Shipped' and oh.shipped_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1) or
(oh.status='Hold' and oh.order_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1))
/
