SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 900
SET TRIMSPOOL ON


--spool mwoverpicks2.csv

select 'Period &&2 to &&3' from DUAL;
select 'Picked From,Sku,Ratio,Update Qty,Original Qty,Count' from DUAL;

select
it.from_loc_id||''','''||
it.sku_id||''','||
sc.ratio_1_to_2||','||
sum(it.update_qty)||','||
sum(it.original_qty)||','||
count(*)
from inventory_transaction it,sku_config sc,location lc
where it.client_id='MOUNTAIN'
--and it.sku_id='5052776140317'
and it.code='Pick'
and it.elapsed_time>0
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.update_qty>it.original_qty
and it.sku_id=sc.config_id
and sc.client_id='MOUNTAIN'
and it.from_loc_id=lc.location_id
and it.site_id=lc.site_id
and lc.work_zone like '&&4%'
group by
it.from_loc_id,
it.sku_id,
sc.ratio_1_to_2
order by 1
/

--spool off
