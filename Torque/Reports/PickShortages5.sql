/******************************************************************************/
/*                                                                            */
/*                            Elite                                           */
/*                                                                            */
/*     FILE NAME  :   PickShortages3.sql                                      */
/*                                                                            */
/*   DATE     BY   PROJ     ID         DESCRIPTION                            */
/*   ======== ==== ======   ========   =============                          */
/*   25/01/06 BK   Mountain            Pick shortage Totals by Date           */
/******************************************************************************/

/* Input Parameters are as follows */
-- 1)  From Date
-- 2)  To Date


set feedback off
set pagesize 68
set newpage 0
set linesize 140
set verify off

clear columns
clear breaks
clear computes

column A heading 'Date' format A16
column B heading 'Short' format 999999
column C heading 'Consignment' format A16
column D heading 'Order' format A22
column E heading 'Sku' format A24
column F heading 'Description' format A40

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'Pick Shortage Totals by Date from '&&2' to '&&3' - for client ID &&4 as at ' report_date skip 2

break on report on A skip 3
compute sum label 'Day Total' of B on A
compute sum label 'Report Total' of B on report

select
to_char(md.Dstamp,'DD/MM/YY') A,
md.consignment C,
md.task_id D,
md.sku_id E,
md.old_qty_to_move-md.qty_to_move B,
sku.description F
from move_descrepancy md, sku
where md.client_id='&&4'
and md.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and md.reason_code='EX_NE'
and md.sku_id=sku.sku_id
order by
to_char(md.Dstamp,'DD/MM/YY'),
md.consignment,
md.task_id,
md.sku_id
/


