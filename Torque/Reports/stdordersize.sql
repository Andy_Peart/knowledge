SET FEEDBACK OFF             
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 9999
SET NEWPAGE 1
SET LINESIZE 160
SET TRIMSPOOL ON

column C heading 'SIZE' format a10
column ORD heading 'ORDER' format a8
column QTY format 999999
column NM heading 'NAME'
column SHP heading 'SHIPPED'
column CUST heading 'CUSTOMER ID'


BREAK ON C SKIP 2
COMPUTE SUM LABEL 'TOTAL' OF QTY ON C

--spool stdordersize.csv

select
CASE
WHEN QTY>=&&5 THEN '&&5 OR OVER'
ELSE 'UNDER &&5' END C,
ORD,
CUST,
NM,
SHP,
QTY
from (select
oh.order_id ORD,
oh.customer_id CUST,
oh.name NM,
trunc(oh.shipped_date) SHP,
sum(ol.qty_shipped) QTY
from order_header oh,order_line ol
where oh.client_id='&&2'
and oh.shipped_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and oh.order_id=ol.order_id
and ol.client_id='&&2'
group by
oh.order_id,
oh.customer_id,
oh.name,
trunc(oh.shipped_date)
order by QTY)
/

--spool off
