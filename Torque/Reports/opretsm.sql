SET FEEDBACK OFF                
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 400
SET TRIMSPOOL ON


select 'Steve Madden Returns Report' from DUAL;
select 'Date,Time,Order ID,Customer Name,SKU ID,Description,Size,Qty,Reason,' from DUAL;

select
to_char(i.Dstamp,'DD/MM/YY,HH24:MI:SS')||','
||i.reference_id||','
||NN||','
||i.sku_id||','
||s.description||','
||s.sku_size||','
||i.update_qty||','
||i.reason_id||',,'
from inventory_transaction i, sku s,(select
order_id OO,
name NN
from order_header
where client_id='OPS')
where i.client_id = 'OPS'
and i.code = 'Receipt'
and i.dstamp > sysdate-7
and s.sku_id=i.sku_id
and s.client_id='OPS'
and s.product_group ='SM'
and i.reference_id like 'R%'
and substr(i.reference_id,2,10)=OO (+)
/
