SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 9999
SET LINESIZE 120
SET TRIMSPOOL ON
SET HEADING ON




column AA noprint
column D heading 'User ID' format a16
column A heading 'YYYY WW' format a16
column B heading 'Skus Picked' format 99999999
column C heading 'Qty Picked' format 99999999



select
user_id D,
to_char(Dstamp,'YYYY IW') A,
sum(update_qty) C
from inventory_transaction
where client_id = 'MOUNTAIN'
and dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')
and code='Pick'
and to_loc_id not in ('DHL','STG','MAIL ORDER')
and user_is in ('ALEXANDRAW','ANNAP')
group by
to_char(Dstamp,'YYYY IW')
order by
to_char(Dstamp,'YYYY IW')
/
