SET FEEDBACK OFF
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 2400
SET TRIMSPOOL ON

break on A dup skip 1

compute sum label "Total" of C D F H on A

--spool mwpickperiod.csv

select 'Period &&2 &&4 to &&3 &&5' from DUAL
union all
select 'User,Spread,Tasks,Units,Units per SKU,Time on Gun(mins),Scan Gap(secs)' from DUAL
union all
SELECT
A||','||
CASE
WHEN length(BBB)=3 THEN ''
ELSE BBB END||','||
replace(to_char(C,'9990.99'),' ','')||','||
replace(to_char(D,'9990.99'),' ','')||','||
replace(to_char(R,'9990.99'),' ','')||','||
replace(to_char(F/60,'990'),' ','')||','||
replace(to_char(((to_number(substr('&&5',1,2)-substr('&&4',1,2))*60)/C)*60,'99990'),' ','')
--S||','||
--replace(to_char(R*S,'99990.99'),' ','')||','||
--W
from (select
A,
'1' MM,
substr(BBB,1,3) BB,
BBB,
F,
C,
D,
D/C R,
CASE
WHEN to_char(SYSDATE-(1/24), 'HH24')=11
THEN '50'
ELSE '100' END S,
CASE WHEN D>(D/C)*100 THEN 'OK' ELSE ' ' END W
from (select
A,
LISTAGG(B||'('||C||')', ':') WITHIN GROUP (ORDER BY B) AS BBB,
sum(C) C,
sum(D) D,
sum(F) F
from (select
it.user_id A,
lc.work_zone B,
count(*) C,
sum(it.elapsed_time) F,
sum(it.update_qty) D
from inventory_transaction it,location lc
where it.Dstamp between to_date('&&2 &&4', 'DD-MON-YYYY HH24:MI:SS') and to_date('&&3 &&5', 'DD-MON-YYYY HH24:MI:SS')
and it.client_id='MOUNTAIN'
and it.update_qty<>0
and it.station_id not like 'Auto%'
and it.code='Pick'
and it.work_group<>'WWW'
and it.elapsed_time>0
and it.from_loc_id=lc.location_id
and it.site_id=lc.site_id
group by it.user_id,lc.work_zone)
group by A)
union
select
A,
MM,
BB,
BBB,
F/CT F,
C/CT C,
D/CT D,
R,
S,
W
from (select
'Average' A,
'2' MM,
substr(BBB,1,3) BB,
substr(BBB,1,3) BBB,
count(*) CT,
sum(F) F,
sum(C) C,
sum(D) D,
sum(D)/sum(C) R,
CASE
WHEN to_char(SYSDATE-(1/24), 'HH24')=11
THEN '50'
ELSE '100' END S,
' ' W
from (select
A,
LISTAGG(B||'('||C||')', ':') WITHIN GROUP (ORDER BY B) AS BBB,
sum(C) C,
sum(D) D,
sum(F) F
from (select
it.user_id A,
lc.work_zone B,
count(*) C,
sum(it.elapsed_time) F,
sum(it.update_qty) D
from inventory_transaction it,location lc
where it.Dstamp between to_date('&&2 &&4', 'DD-MON-YYYY HH24:MI:SS') and to_date('&&3 &&5', 'DD-MON-YYYY HH24:MI:SS')
and it.client_id='MOUNTAIN'
and it.update_qty<>0
and it.station_id not like 'Auto%'
and it.code='Pick'
and it.work_group<>'WWW'
and it.elapsed_time>0
and it.from_loc_id=lc.location_id
and it.site_id=lc.site_id
group by it.user_id,lc.work_zone)
group by A)
group by
substr(BBB,1,3))
order by BB,MM)
/

--spool off

