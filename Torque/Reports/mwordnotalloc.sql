SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET TRIMSPOOL ON

with t1 as (
    select to_char(oh.creation_date,'DD-MM-YYYY') as date_, oh.order_id, ol.sku_id
    from order_header oh inner join order_line ol on oh.order_id = ol.order_id and oh.client_id = ol.client_id
    where oh.client_id = 'MOUNTAIN'
    and oh.order_type = 'WEB'
    and oh.creation_date between to_date('&&2','DD-MON-YYYY') and to_date('&&3','DD-MON-YYYY')+1
),
t2 as (
    select it.reference_id, it.sku_id
    from inventory_transaction it 
    where it.client_id = 'MOUNTAIN'
    and it.code = 'Allocate'
    and it.work_group in ('WWW','WWX')
    and it.dstamp between to_date('&&2','DD-MON-YYYY') and to_date('&&3','DD-MON-YYYY')+3
)
select 'Creation date,Order,SKU' from dual
union all
select date_ || ',' || order_id || ',' || sku_id
from (
    select date_,order_id,sku_id
    from (
        select t1.date_, t1.order_id, t1.sku_id, t2.sku_id found_sku
        from t1 left join t2 on t1.order_id = t2.reference_id and t1.sku_id = t2.sku_id
    )
    where found_sku is null
)
/