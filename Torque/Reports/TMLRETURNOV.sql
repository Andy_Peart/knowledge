SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON

SELECT 'Carrier,Despatch Country,Returned Date,DW Order,Despatch ID,Customer ID,Customer,Phone,E-Mail,Shipped Date,Item Name (SKU),Product Code,Size,Description,Qty Returned,Value,Return Reason Code,Reason Return Notes,Days until Return' from dual
Union All
select A ||','|| B ||','|| C ||','|| D ||','|| E ||','|| F ||','|| G ||','|| H ||','|| I ||','|| J ||','|| K ||','|| L ||','|| M ||','|| N ||','|| O ||','|| P ||','|| Q ||','|| R ||','|| S  from (
select  
case 
  when oh.ship_dock = 'TRWEB' then 'Web'
  when oh.ship_dock = 'TRWEBCC' then 'Web CC'
  else oh.ship_dock end "Stream"
, oh.CARRIER_ID                                as "A"  
, c.tandata_id                                 as  "B"
, to_char(it.dstamp, 'DD-MON-YYYY')            as "C"
, oh.purchase_order                            as "D"
, it.reference_id                              as "E"
, oh.customer_id                               as "F"
, oh.name                                      as "G"
, oh.contact_phone                             as "H"
, oh.contact_email                             as "I"
, to_char(oh.shipped_date, 'DD-MON-YYYY')      as "J"
, it.sku_id                                    as "K"
, sku.user_def_type_2||'-'||sku.user_def_type_1||'-'||sku.user_def_type_4 as "L"
, sku.sku_size                                 as "M"
, sku.DESCRIPTION                              as "N"
, it.UPDATE_QTY                                as "O"
, to_char((ol.LINE_VALUE)/(ol.qty_shipped)*(ol.qty_returned))             as "P"
, ltrim(it.reason_id, 'TR')                    as "Q"
, nvl(r.notes, 'No Notes')                     as "R"
, (trunc(it.dstamp) - trunc(oh.shipped_date) ) as "S"
, case 
  when it.sampling_type = 'TRSEC' then 'SECONDS'
  when it.sampling_type = 'TRGOOD' then 'GOOD RETURN'
  when it.sampling_type = 'TRBAD' then 'BAD RETURN'
  when it.sampling_type IS NULL then 'RETURN'
  else it.sampling_type end "Return or Exchange?"
from inventory_transaction it
inner join order_header oh on it.client_id = oh.client_id and it.REFERENCE_ID = oh.order_id
inner join order_line ol on oh.client_id = ol.client_id and oh.order_id = ol.order_id and it.line_id = ol.line_id
inner join sku on it.client_id = sku.client_id and it.sku_id = sku.sku_id
left join return_reason r on it.reason_id = r.REASON_ID
left join 
( select
cun.ISO3_id, cun.tandata_id
from country cun
) c on oh.country = c.ISO3_id
where it.code = 'Return'
and it.client_id = 'TR'
and it.reason_id <> 'TR3'
and it.dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
order by "D"
)
/