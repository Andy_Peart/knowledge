SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 160

column A format A14
column B format A20
column C format A18
column D format A14
column E format A14
column F format 999999
column G format 999999
column H format 999999
column J format A12
column K format A10
column L format A16

break on J dup 


--SET TERMOUT OFF
--spool ss.csv

select 'Code,SKU,Tag Id,From Location,To Location,Original Qty,Update Qty,New Qty,Date,Time,User,,'
from dual;

select 
code A,
''''||sku_id ||'''' B, 
tag_id C,
from_loc_id D,
to_loc_id E,
original_qty F,
update_qty G,
original_qty+update_qty H,
to_char(Dstamp,'DD/MM/YYYY') J,
to_char(Dstamp,'HH24:MM:SS') K,
user_id L,
' '
from inventory_transaction
where client_id = 'MOUNTAIN'
and code in ('Stock Check','Adjustment')
and from_loc_id not like ('Z%')
and Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
order by dstamp
/
Select ',,,,,,,,,,' from DUAL;
Select 'Summary,,,,,,,,,,' from DUAL;

select 'Code,SKU,Tag Id,From Location,To Location,Original Qty,Update Qty,New Qty,Date,Time,User,,'
from dual;

select 
code A,
''''||sku_id ||'''' B, 
' ' C,
' ' D,
' ' E,
sum(original_qty) F,
sum(update_qty) G,
sum(original_qty+update_qty) H,
' ' J,
' ' K,
' ' L,
' '
from inventory_transaction
where client_id = 'MOUNTAIN'
and code in ('Stock Check','Adjustment')
and from_loc_id not like ('Z%')
and Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by
code,
sku_id
order by sku_id
/

--spool off
--SET TERMOUT ON
