SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 200
set trimspool on
set newpage 0
set verify off
set colsep ','

--spool mwgridpicking.csv

select 'SKU,Description,Qty Ordered,Condition,Pre Advice Id,Due Date,Container,Tag Id' from DUAL;

select
A||','||
B||','||
C||','||
D||','||
E||','||
F||','||
G||','||
H from (select
ol.sku_id A,
sku.description B,
sum(ol.qty_ordered) C,
nvl(COND,'') D,
ol.user_def_type_2 E,
trunc(ph.due_dstamp) F,
pl.user_def_type_2 G,
nvl(TAG,'') H
from order_header oh,order_line ol,pre_advice_header ph,pre_advice_line pl,sku,(select
iv.sku_id SKU,
iv.receipt_id REC,
max(iv.tag_id) TAG,
max(iv.condition_id) COND
from inventory iv
where client_id='MOUNTAIN'
group by
iv.sku_id,
iv.receipt_id)
where oh.client_id='MOUNTAIN'
and oh.consignment='&&2'
and oh.order_id=ol.order_id
and ol.client_id='MOUNTAIN'
and ol.user_def_type_2=ph.pre_advice_id
and ph.client_id='MOUNTAIN'
and pl.pre_advice_id=ph.pre_advice_id
and pl.client_id='MOUNTAIN'
and pl.sku_id=ol.sku_id
and ol.sku_id=sku.sku_id
and sku.client_id='MOUNTAIN'
and pl.sku_id=SKU (+)
and pl.pre_advice_id=REC (+)
group by
ol.sku_id,
sku.description,
nvl(COND,''),
ol.user_def_type_2,
trunc(ph.due_dstamp),
pl.user_def_type_2,
nvl(TAG,'')
order by
B,A)
/


--spool off


