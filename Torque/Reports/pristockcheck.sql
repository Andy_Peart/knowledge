SET FEEDBACK OFF
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
set colsep ','
set heading off
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9999

column A heading 'SKU' format a20
column B heading 'EAN' format a20
column C heading 'QTY' format 9999999

select 'SKU,EAN,QTY'
from dual;

select unique i.sku_id, s.ean, sum(i.qty_on_hand)
from sku s, inventory i
where i.sku_id = s.sku_id
and to_char(i.count_dstamp, 'DD-MON-YYYY') between upper ('&1') and upper ('&2') 
group by i.sku_id, s.ean
order by i.sku_id
/

