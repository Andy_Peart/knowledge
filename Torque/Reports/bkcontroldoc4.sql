SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

column A heading 'Store' format A6
column B heading 'Name' format A30
column C heading 'List ID' format A12
column CC heading 'Cluster Group' format A12
column D heading 'Picked' format 99999999
column E heading 'Consignment' format A14
column F heading 'Date Picked' format A12
column G heading 'Picker' format A12
column H heading 'Start Time' format A10
column I heading 'Finish Time' format A10
column J heading 'Time Taken' format A10
column K heading 'Containers' format 99999
column M format a4 noprint



select 'Store,Name,List No,Cluster Group,Picked,Consignment,Date Picked,Picker,Start,Finish,Elapsed Time,Containers' from DUAL;
 
set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DAY, DD MONTH, YYYY') curdate from DUAL;
set TERMOUT ON


--break on report

--compute sum of D E EE on report


select
it.customer_id A,
a.name B,
it.list_id C,
sku.allocation_group CC,
sum(it.update_qty) D,
it.consignment E,
to_char(it.Dstamp,'DD/MM/YYYY') F,
it.user_id G,
to_char(min(Dstamp),'HH24:MI:SS') H,
to_char(max(Dstamp),'HH24:MI:SS') I,
substr(min(Dstamp)-max(Dstamp),12,8) J,
count(distinct it.container_id) K
--substr(it.consignment,7,4) M
from inventory_transaction it,address a,sku
where it.client_id='TR'
and it.code='Pick'
and it.list_id is not null
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.from_loc_id<>'CONTAINER'
and a.client_id='TR'
and it.customer_id=a.address_id
and sku.client_id='TR'
and it.sku_id=sku.sku_id
group by
it.customer_id,
a.name,
it.list_id,
sku.allocation_group,
it.consignment,
to_char(it.Dstamp,'DD/MM/YYYY'),
it.user_id
order by A,C,CC,E
/
