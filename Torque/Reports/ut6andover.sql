SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 1
SET LINESIZE 400
SET TRIMSPOOL ON
SET HEADING OFF

--spool u6andover.csv

select 'Location,SKU,CIMS,Description,Colour,Style,Total,Allocated,Available' from DUAL;

select
A||','||
B||','||
C||','||
D||','||
E||','||
F||','||
G||','||
H||','||
J from (SELECT
i.location_id A,
i.sku_id B,
s.user_def_type_2 || '-' || s.user_def_type_1 || '-' || s.user_def_type_4 C,
s.description D,
s.colour E,
s.user_def_type_4 F,
NVL(SUM(i.qty_on_hand), 0) G,
NVL(SUM(i.qty_allocated), 0) H,
NVL(SUM(i.qty_on_hand), 0) - NVL(SUM(i.qty_allocated), 0) J
FROM inventory i, sku s
WHERE i.client_id = 'UT'
AND s.client_id = 'UT'
and i.sku_id not like 'XX%'
AND i.sku_id = s.sku_id
AND i.location_id between '3AB' and '3AD'
group by
i.location_id,
i.sku_id,
s.user_def_type_2 || '-' || s.user_def_type_1 || '-' || s.user_def_type_4,
s.description,
s.colour,
s.user_def_type_4
HAVING NVL(SUM(i.qty_on_hand), 0) > 5)
ORDER BY
A,B,F
/

--spool off
