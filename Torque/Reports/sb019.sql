SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

--Select 'SB08 C Mail,Order Returns' from DUAL;
select 'SKU,Description,PO Number,PO Qty,Qty Received,Variance in Qty,Vendor Name,Date of Receipted' from DUAL;




column A format A10
column B format A40
column C format A10
column CC noprint
column D format 9999
column E format 9999
column F format 9999
column G format A14
column H format A20



set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



--ttitle  LEFT 'TML Retail - Open Orders Report ' RIGHT 'Printed ' report_date skip 2


--break on CC skip 1

--compute sum label 'Grand Total' of QQQ on report
--compute sum label 'Total Units' of QQQ on HH

select
distinct
it.sku_id A,
'"' || sku.description || '"' B,
it.REFERENCE_ID C,
it.REFERENCE_ID CC,
pl.QTY_DUE E,
it.UPDATE_QTY D,
it.UPDATE_QTY-pl.QTY_DUE F,
ph.supplier_id G,
to_char(it.DSTAMP,'DD/MM/YYYY HH24:MI:SS') H
from inventory_transaction it,pre_advice_header ph,pre_advice_line pl,sku
where it.CLIENT_ID='SB'
and it.DSTAMP>sysdate-8
and it.REFERENCE_ID like 'P%'
and it.CODE like 'Receipt%'
and it.reference_id=ph.pre_advice_id
and it.reference_id=pl.pre_advice_id
and it.sku_id=pl.sku_id
and it.sku_id=sku.sku_id
--and ph.SUPPLIER_ID=address.address_id (+)
order by
C,A
/
