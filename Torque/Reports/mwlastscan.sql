SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','
SET WRAP OFF


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON

Select 'UserID', 'Datestamp','From location', 'To location',' Consignment'
from dual;

Select   a.user_id, b.dstamp, B.FROM_LOC_ID , B.TO_LOC_ID , B.CONSIGNMENT
 from
           (
                select distinct user_id
                from inventory_transaction
                where client_id = 'MOUNTAIN'
                and code = 'Pick'
                and reference_id like 'MOR%'
                and dstamp between trunc(sysdate)  and trunc(sysdate) +1
                and update_qty > 0
                and elapsed_time > 0
                and from_loc_id <> 'CONTAINER'
                and group_id = 'MTN RDT-MC'
          ) A 
          LEFT OUTER Join (
                              select it.user_id, it.dstamp, IT.FROM_LOC_ID, IT.TO_LOC_ID,IT.CONSIGNMENT, Row_Number()
                              OVER (PARTITION BY it.user_Id ORder BY it.dstamp desc) as picked
                              from inventory_transaction it
                              where it.client_id = 'MOUNTAIN'
                              and it.code = 'Pick'
                              and it.reference_id like 'MOR%'
                              and it.dstamp between trunc(sysdate)  and trunc(sysdate) +1
                              and it.update_qty > 0
                              and it.elapsed_time > 0
                              and it.from_loc_id <> 'CONTAINER'
                              and it.group_id = 'MTN RDT-MC'
                            group by user_id, it.dstamp, IT.FROM_LOC_ID, IT.TO_LOC_ID,IT.CONSIGNMENT
                              order by dstamp desc
                          ) B
          On a.user_id = B.user_id
Where B.picked = 1
    


/