SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9999
SET TRIMSPOOL ON

--set feedback on

update inventory_transaction
set user_def_chk_2='Z'
where client_id = 'DX'
--and trunc(Dstamp)>trunc(sysdate-7)
and code = 'Receipt'
and (user_def_chk_2 = 'N'
or user_def_chk_2 is null)
and to_loc_id in ('DXBRET','DXBREJ')
and (reference_id like 'QVC%'
or reference_id in (select
order_id
from order_header
where client_id='DX'
and nvl(order_type,' ') ='TVFINAL'
and customer_id='19762'))
/


--set feedback off
--SET TERMOUT OFF

--spool dxrep8QVC.csv

select 'Returns_Report - '||to_char(SYSDATE, 'DD/MM/YY  HH24:MI:SS') from DUAL;

select 'Date,Order Type,Rev.Stream,Notes,Order ID,Customer,Product,Description,Condition ID,Location ID,Qty,Reason,User ID' from DUAL;

select
trunc(i.Dstamp)||','||
CC||','||
UDT1||','||
i.user_def_note_2||','||
i.reference_id||','||
CID||','||
i.sku_id||','||
sku.description||','||
i.condition_id||','||
i.to_loc_id||','||
i.update_qty||','||
i.user_def_type_1||','||
i.user_id
from inventory_transaction i,sku,(select
nvl(order_type,' ') CC,
order_id OO,
customer_id CID,
'QVC' UDT1
from order_header
where client_id='DX')
where i.client_id='DX'
and i.code = 'Receipt'
and i.user_def_chk_2 = 'Z'
and i.to_loc_id in ('DXBRET','DXBREJ')
--and trunc(i.Dstamp)>trunc(sysdate-7)
and sku.client_id = 'DX'
and i.sku_id=sku.sku_id
and i.reference_id=OO (+)
order by
trunc(i.Dstamp),
CC,
i.sku_id
/

--spool off

--set feedback on
--set termout on


update inventory_transaction
set user_def_chk_2='Y'
where client_id = 'DX'
--and trunc(Dstamp)>trunc(sysdate-7)
and code = 'Receipt'
and user_def_chk_2 = 'Z'
and to_loc_id in ('DXBRET','DXBREJ')
/


--rollback;
commit;

 
