SET FEEDBACK OFF                 
set pagesize 68
set linesize 160
set verify off

clear columns
clear breaks
clear computes

column D heading 'Receipt Date' format a16
column A heading 'Advice ID' format a16
column B heading 'Ordered' format 999999
column C heading 'Received' format 999999
column M noprint

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'Total Units Receipted from &&2 to &&3 - for client ID &&4 as at ' report_date skip 2

break on D skip 2 on report

compute sum label 'Period Totals' of B C on report
compute sum label 'Day Totals' of B C on D

select * 
from
(
select
to_char(it.Dstamp,'DD/MM/YYYY') D,
to_char(it.Dstamp,'YYYY/MM/DD') M,
ph.pre_advice_id A,
sum(ph.qty_due) B,
sum(it.update_qty) C
from inventory_transaction it,pre_advice_line ph
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='&&4'
and it.station_id not like 'Auto%'
and it.code like 'Receipt%'
and it.reference_id=ph.pre_advice_id
and it.line_id=ph.line_id
group by
to_char(it.Dstamp,'YYYY/MM/DD'),
to_char(it.Dstamp,'DD/MM/YYYY'),
ph.pre_advice_id
union all
select to_char(it.Dstamp,'DD/MM/YYYY') D,
to_char(it.Dstamp,'YYYY/MM/DD') M,
'ABC'  A,
SUM('0') B,
sum(it.update_qty) C
from inventory_transaction  it
where it.client_id='&&4'
and it.reference_id = 'R'
and it.user_def_type_1 is not null
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.station_id not like 'Auto%'
group by
to_char(it.Dstamp,'YYYY/MM/DD'),
to_char(it.Dstamp,'DD/MM/YYYY')
union all
select to_char(it.Dstamp,'DD/MM/YYYY') D,
to_char(it.Dstamp,'YYYY/MM/DD') M,
reference_id A,
sum(it.update_qty) B,
sum(it.update_qty) C
from inventory_transaction  it
where it.client_id='&&4'
and it.code = 'Return'
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by
to_char(it.Dstamp,'YYYY/MM/DD'),
to_char(it.Dstamp,'DD/MM/YYYY'),
reference_id
)
order by
M,D,A
/