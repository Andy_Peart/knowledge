SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 550
SET TRIMSPOOL ON

with t1 as (
    select order_hb, cust_name, order_no, client_id, sum(b_lines) b_lines, sum(h_lines) h_lines, sum(b_units) b_units, sum(h_units) h_units, line_id
    from (
        select case when l.work_zone = 'BDB1' then 'B'
        when l.work_zone = 'BDH1' then 'H'
        else 'OTH' end as Order_HB
        , case when s.client_id = 'PS' then 'SAP' else s.user_def_type_2 end as cust_name
        , it.reference_id as order_no
        , it.client_id 
        , case when l.work_zone = 'BDB1' then count(nvl(it.update_qty,0)) else 0  end as b_lines
        , case when l.work_zone = 'BDH1' then count(nvl(it.update_qty,0)) else 0 end as h_lines
        , case when l.work_zone = 'BDB1' then sum(nvl(it.update_qty,0))  else 0 end as b_units
        , case when l.work_zone = 'BDH1' then sum(nvl(it.update_qty,0))  else 0 end as h_units
        , to_char(it.dstamp,'DD-MM-YYYY') picked_date
        , it.line_id
        from inventory_transaction it
        inner join location l on l.site_id = it.site_id and l.location_id = it.from_loc_id
        inner join sku s on s.sku_id = it.sku_id and it.client_id = s.client_id
        where it.client_id in ('PS','PR')
        and it.code in ('Pick','UnPick')
        and it.dstamp between to_date('&&2', 'DD-MON-YYYY')-14 and to_date ('&&3', 'DD-MON-YYYY')+1
        and it.from_loc_id <> 'CONTAINER'
        group by l.work_zone, s.client_id, it.reference_id, s.user_def_type_2, it.client_id,to_char(it.dstamp,'DD-MM-YYYY'), it.line_id
        order by 10
    )
    where b_lines+h_lines+b_units+h_units > 0
    group by order_hb, cust_name, order_no, client_id,line_id
),
t2 as (
    select distinct to_char(it.dstamp,'DD-MM-YYYY') shipped_date, it.client_id client_no, it.reference_id, line_id line_no
    from inventory_transaction it
    where it.client_id in ('PS','PR')
    and it.code = 'Shipment'
    and it.dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date ('&&3', 'DD-MON-YYYY')+1
)
select 'Order H/B,Customer Name,Order Reference,Client,Shipped Date,Lines Shipped (B),Lines Shipped (H),Units Shipped (B),Units Shipped (H)' from dual
union all
select order_hb || ',' || cust_name  || ',' ||  order_no  || ',' || client_id || ',' || shipped_date  || ',' || b_lines || ',' ||  h_lines || ',' ||  b_units || ',' ||  h_units
from (
    select shipped_date, order_hb,cust_name,order_no,client_id, sum(b_lines) b_lines, sum(h_lines) h_lines, sum(b_units) b_units, sum(h_units) h_units
    from (
        select t2.shipped_date, t1.*
        from t2 inner join t1 on t1.client_id = t2.client_no and t1.order_no = t2.reference_id and t1.line_id = t2.line_no
    )
    group by shipped_date, order_hb,cust_name,order_no,client_id
)
/
--spool off