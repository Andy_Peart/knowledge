SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240


select 'Location,Site,Lock Status,Work Zone,Zone,Pick Seq,Put Seq,Check Seq,Volume' from DUAL;


select
location_id ||','||
site_id ||','||
lock_status ||','||
work_zone ||','||
zone_1 ||','||
pick_sequence ||','|| 
put_sequence ||','||
check_sequence ||','||
current_volume
from location
where site_id like '&&2'
and work_zone like '&&3%'
order by 
location_id
/
