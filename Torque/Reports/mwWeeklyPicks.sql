
/******************************************************************************/
/*                                                                            */
/*                            Elite                                           */
/*                                                                            */
/*     FILE NAME  :   mwshortages3.sql                                        */
/*                                                                            */
/*   DATE     BY   PROJ     ID         DESCRIPTION                            */
/*   ======== ==== ======   ========   =============                          */
/*   22/01/06 BK   Mountain            Units Picked by Date Range             */
/******************************************************************************/

/* Input Parameters are as follows */
-- 1)  From Date
-- 2)  To Date

SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 120
SET TRIMSPOOL ON

column AA heading 'Order Type' format A12
column A heading 'User ID' format A16
column X heading 'Zone' format A12
column B heading 'Units' format 999999
column C heading 'Picks' format 999999
column D heading 'Orders' format 999999
column E heading 'Ave Units per Order' format 99990D99


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

--spool wkunitspicked.csv

select 'Units Picked during Week Commencing  '||to_char(SYSDATE-7, 'DD/MM/YYYY') from DUAL;

break on AA dup skip 1 on report 
compute sum label 'Total' of B C on AA
compute sum label 'Full Total' of B C on report


select 'Type,User ID,Units,Picks' from DUAL;

select
DECODE(it.work_group,'WWW','WEB','RETAIL') AA,
it.user_id A,
sum(it.update_qty) B,
count(*) C
--from inventory_transaction it,order_header oh,location ll
from inventory_transaction it,location ll
where it.client_id='MOUNTAIN'
and it.code='Pick'
and it.update_qty<>0
and trunc(it.Dstamp)>trunc(sysdate)-7
and trunc(it.Dstamp)<trunc(sysdate)
--and it.dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.from_loc_id=ll.location_id
and ll.site_id='BD2'
and ll.zone_1 in ('RETAIL','DYN1')
group by
DECODE(it.work_group,'WWW','WEB','RETAIL'),
it.user_id 
order by 
AA,A
/

--spool off
