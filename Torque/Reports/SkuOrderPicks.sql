set feedback off
set pagesize 66
set linesize 142
set verify off
set newpage 0


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

 

column B heading 'SKU' format A20
column C heading 'Description' format A40
column D heading 'Colour' format A16
column E heading 'Size' format A8
column F heading 'Order ID' format A12
column G heading 'Line ID' format 99999999
column H heading 'Qty' format 999999
column I heading 'Tag ID' format A10
column J heading 'Location' format A12


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

set und '-'
set heading on
set newpage 0

ttitle  LEFT 'PICK SHEET in product order for Consignment &&2 and client ID &&3 as at ' report_date RIGHT 'Page:' FORMAT 999 SQL.PNO skip 2

--break on report

--compute sum label 'Total' of D on report

--break on row skip 1

select 
ol.sku_id B,
sku.description C,
sku.colour D,
sku.sku_size E,
mt.qty_to_move H,
mt.tag_id I,
mt.from_loc_id J,
oh.order_id F,
ol.line_id G
from order_header oh,order_line ol,sku,move_task mt
where oh.client_id='&&3'
and oh.order_id=ol.order_id
and oh.order_id=mt.task_id
--and oh.status<>'Shipped'
and oh.consignment='&&2'
and ol.sku_id=sku.sku_id
and ol.sku_id=mt.sku_id
order by
ol.sku_id,
oh.order_id
/
