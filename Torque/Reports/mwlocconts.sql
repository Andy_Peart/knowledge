SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Store,Order #,Container ID,Units' from dual
union all
select "Wg" || ',' || "Ord" || ',' || "Cont" || ',' || "Qty" from
(
select it.work_group    as "Wg"
, it.reference_id       as "Ord"
, container_id          as "Cont"
, sum(it.update_qty)    as "Qty"
from inventory_transaction it
inner join address a on a.client_id = it.client_id and a.address_id = it.work_group
inner join sku s on s.client_id = it.client_id and s.sku_id = it.sku_id
where it.client_id = 'MOUNTAIN'
and it.dstamp between to_date('&&2','DD-MON-YYYY') and to_date('&&3','DD-MON-YYYY')+1
--and it.consignment = '&&2'
and it.to_loc_id = 'DESPATCH'
and it.code = 'Pick'
and it.update_qty <> 0
and a.user_def_note_2 = 'LOCAL'
group by it.work_group
, it.reference_id
, container_id
order by it.work_group
)
;
select null from dual
;
select'Store,Order #,Container ID,Sku ID,Sku Desc.,Qty' from dual
union all
select "Wg" || ',' || "Order" || ',' || "Cont" || ',' || "Sku" || ',' || "Desc" || ',' || "Qty" from (
select it.work_group    as "Wg"
, it.reference_id       as "Order"
, it.container_id       as "Cont"
, ''''||it.sku_id||'''' as "Sku"
, s.description         as "Desc"
, sum(it.update_qty)    as "Qty"
from inventory_transaction it
inner join address a on a.client_id = it.client_id and a.address_id = it.work_group
inner join sku s on s.client_id = it.client_id and s.sku_id = it.sku_id
where it.client_id = 'MOUNTAIN'
and it.dstamp between to_date('&&2','DD-MON-YYYY') and to_date('&&3','DD-MON-YYYY')+1
-- and it.consignment = '&&2'
and it.to_loc_id = 'DESPATCH'
and it.code = 'Pick'
and it.update_qty <> 0
and a.user_def_note_2 = 'LOCAL'
group by it.work_group
, it.reference_id
, it.container_id
, it.sku_id
, s.description
order by it.work_group, it.container_id
)
/
--spool off