SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON



set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

with T1 as
(select tag_id, nvl(reference_id, '(no rec ID)') rec_id
from inventory_transaction
where client_id = 'BL'
and code = 'Receipt'),
T2 as
(select it.Customer_ID,
to_char(it.Dstamp, 'DD-MM-YYYY') date_shipped,
it.Sku_id,
it.reference_id sales_order,
sum(it.update_qty) qty_shipped,
loc.zone_1,
T1.rec_id,
it.tag_id
from inventory_transaction it
inner join location loc
on it.from_loc_id = loc.location_id
and it.site_id = loc.site_id
inner join T1
on it.tag_id = T1.tag_id
where it.client_id = 'BL'
and it.site_id = 'WO6'
and loc.zone_1 in ( 'ASOS', 'LIP', 'DEB', 'URB', 'ZAL', 'BL')
and it.code = 'Shipment'
and it.update_qty > 0
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and
to_date('&&3', 'DD-MON-YYYY') + 1
group by it.Customer_ID,
to_char(it.Dstamp, 'DD-MM-YYYY'),
it.Sku_id,
it.reference_id,
loc.zone_1,
T1.rec_id,
it.tag_id)
select 'Date Picked, Customer ID, Location Zone, Sales Order, SKU, Original Receipt ID, Units Picked, Date Shipped, Units Shipped'
from dual
union all
select  date_picked || ',' || customer_id || ',' ||  zone_1 || ',' || sales_order   || ',' || sku_id || ',' || rec_id || ',' || sum(qty_picked) || ',' || date_shipped || ',' || sum(qty_shipped)
from (select TabA.*, T2.date_shipped, T2.qty_shipped
from (select to_char(it.Dstamp, 'DD-MM-YYYY') date_picked,
it.Customer_ID,
loc.zone_1,
it.reference_id sales_order,
it.Sku_id,
T1.rec_id,
it.tag_id,
sum(it.update_qty) qty_picked
from inventory_transaction it
inner join location loc
on it.from_loc_id = loc.location_id
and it.site_id = loc.site_id
left join T1
on it.tag_id = T1.tag_id
where it.client_id = 'BL'
and it.site_id = 'WO6'
and loc.zone_1 in
( 'ASOS', 'LIP', 'DEB', 'URB', 'ZAL', 'BL')
and it.code = 'Pick'
and it.update_qty > 0
and it.to_loc_id = 'CONTAINER'
group by it.Customer_ID,
to_char(it.Dstamp, 'DD-MM-YYYY'),
loc.zone_1,
it.Sku_id,
it.reference_id,
T1.rec_id,
it.tag_id) TabA
inner join T2
on TabA.customer_id = T2. customer_id
and TabA.tag_id = T2.tag_id
and TabA.sales_order = T2.sales_order
and TabA.sku_id = T2.sku_id
and TabA.rec_id = T2.rec_id
order by 1, 2, 3, 4)
group by 
date_picked, customer_id, zone_1 , 
sales_order , sku_id , rec_id , 
date_shipped  
/