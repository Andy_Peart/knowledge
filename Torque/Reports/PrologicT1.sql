SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep '~'


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

select
'90004~90004~GAR-SUSPENSE~TAKE ON~Road-1~FOB~~~~~' ||'~'||
substr(sku.ean,1,12) ||'~~'||
QQ ||'~'||
'~~~~~~~~~~~~' ||'~'||
nvl(sku.each_value,0) ||'~'
--'Price' ||'~'
from sku,
(select
sku_id SS,
sum(qty_on_hand) QQ
from inventory
where client_id='T'
and location_id='SUSPENSE'
group by sku_id)
where SS=sku.sku_id
and sku.client_id='T'
order by SS
/
