SET FEEDBACK OFF           
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 350
SET TRIMSPOOL ON

break on A skip 1 dup

COLUMN NEWSEQ NEW_VALUE SEQNO


select 'S://redprairie/apps/home/dcsdba/reports/LIVE/MW_CONTAINER_SHIPMENTS_'||to_char(SYSDATE,'DDMMYYYYHHMISS')||'.csv' NEWSEQ from DUAL;
--select 'LIVE/MW_CONTAINER_SHIPMENTS_'||to_char(SYSDATE,'DDMMYYYYHHMISS')||'.csv' NEWSEQ from DUAL;


--select 'Customer,Container,Order,SKU,Qty,Date,Containers' from DUAL;

spool &SEQNO

select
sm.key||','||
sm.Customer_id||','||
sm.Container_id||','||
sm.Order_id||','||
sm.SKU_id||','||
--sku.description||','||
--sm.Line_id||','||
sm.Qty_shipped||','||
trunc(sysdate)||','||
RANK2||','||
CT
from shipping_manifest sm,sku,(select
customer_id CUST,
count(distinct container_id) CT
from shipping_manifest
where client_id='MOUNTAIN'
and trunc(shipped_Dstamp)=trunc(sysdate)
and work_group<>'WWW'
group by customer_id),(select
distinct
customer_id CUST2,
container_id CONT2,
rank() over(partition by customer_id order by container_id) RANK2
from shipping_manifest
where client_id='MOUNTAIN'
and trunc(shipped_Dstamp)=trunc(sysdate)
and work_group<>'WWW'
and container_id is not null
group by customer_id,container_id)
where sm.client_id='MOUNTAIN'
and trunc(sm.shipped_Dstamp)=trunc(sysdate)
and sm.work_group<>'WWW'
and sm.sku_id=sku.sku_id
and sku.client_id='MOUNTAIN'
and sm.customer_id=CUST
and sm.customer_id=CUST2
and sm.container_id=CONT2
and sm.container_id is not null
order by
sm.Customer_id,
sm.Container_id,
sm.Order_id,
sm.SKU_id
/


spool off

