SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 260

Select 'Status,Order Date,Creation Date,Order Type,RP Priority,Customer Name,Sales Order ID,Order Suffix,Count of Orders,Qty Ordered,Qty Picked,Qty Tasked,Qty Shipped,Variance' from DUAL;


column A format A12
column A1 format A12
column A2 format A12
column B format A12
column C format A12
column CC format A12
column D format A36
column E format A12
column EE format A12
column EEE noprint
column F format 99999
column G format 99999
column H format 99999
column I format 99999
column J format 99999
column K format 99999
column KK format 99999
column L format A16

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



--ttitle  LEFT 'TML Retail - Open Orders Report ' RIGHT 'Printed ' report_date skip 2


break on EEE skip 1 on report

compute sum label 'Total' of F G H I J K KK on EEE
compute sum label 'GRAND TOTAL' of F G H I J K KK on report

select
oh.STATUS A,
to_char(oh.order_date,'DD-MM-YYYY') A1,
to_char(oh.creation_date,'DD-MM-YYYY') A2,
oh.order_type B,
oh.priority C,
oh.NAME D,
oh.ORDER_ID E,
substr(oh.ORDER_ID,5,5) EE,
substr(oh.ORDER_ID,5,5) EEE,
count(*) F,
sum(ol.QTY_ORDERED) G,
sum(nvl(ol.qty_picked,0)) H,
sum(nvl(ol.qty_tasked,0)) I,
sum(nvl(ol.qty_shipped,0)) J,
sum(ol.QTY_ORDERED)-sum(nvl(ol.qty_shipped,0)) KK
--oh.USER_DEF_TYPE_7 L
from order_header oh,order_line ol
where oh.CLIENT_ID='SB'
and oh.order_ID not like 'C%'
and oh.order_ID=ol.order_id
and oh.status in ('Shipped')
and oh.shipped_date>sysdate-1
--and ol.QTY_ORDERED-nvl(ol.qty_tasked,0)>0
group by
oh.STATUS,
to_char(oh.order_date,'DD-MM-YYYY'),
to_char(oh.creation_date,'DD-MM-YYYY'),
oh.order_type,
oh.priority,
oh.NAME,
oh.order_id
order by
substr(oh.ORDER_ID,5,5),
oh.order_id 
/

