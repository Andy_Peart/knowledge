SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Location,Sku,Condition,Correct Condition' from dual
union all
select location_id||','||sku_id||','||condition_id||','||"Should be"
from
(
        select location_id
        , sku_id
        , condition_id
        , "Should be"
        from
        (
        select i.location_id
        , i.sku_id
        , i.condition_id
        , l.loc_type
        , case  when i.location_id like 'HRET%' and condition_id = 'GOOD' then 'Y'
                when i.location_id like 'HRFB%' and condition_id = 'REFURB' then 'Y'
                when i.location_id like 'HDISP%' and condition_id like 'DISP%' then 'Y'
                when i.location_id like 'HSCRAP%' and condition_id like 'SCRAP%' then 'Y'
                when i.location_id like 'HCABIN98' and condition_id like 'SCRAP%' then 'Y'
                when i.location_id = 'QCREJ' and condition_id = 'DORMANT' then 'Y'
                when i.location_id = 'QCFIX' and condition_id = 'QUERY' then 'Y'
                when l.loc_type in ('Tag-FIFO', 'Tag-LIFO') and i.location_id not like 'HRET%' 
                and i.location_id not like 'HDISP%' and i.location_id not like 'HSCRAP%' 
                and i.location_id not like 'HCABIN98' and i.location_id not like 'HRFB%'
                and i.location_id not in ('QCREJ','QCFIX')
                and i.condition_id = 'GOOD' then 'Y'
                else 'N' end as "Cond Ok?"       
        , case  when i.location_id like 'HRET%' then 'GOOD'
                when i.location_id like 'HRFB%' then 'REFURB'
                when i.location_id like 'HDISP%' then 'DISP*'
                when i.location_id like 'HSCRAP%' then 'SCRAP*'
                when i.location_id like 'HCABIN98' then 'SCRAP*'
                when i.location_id = 'QCREJ' then 'DORMANT'
                when i.location_id = 'QCFIX' then 'QUERY'
                when l.loc_type in ('Tag-FIFO', 'Tag-LIFO') and i.location_id not like 'HRET%' 
                and i.location_id not like 'HDISP%' and i.location_id not like 'HSCRAP%' 
                and i.location_id not like 'HCABIN98' and i.location_id not like 'HRFB%' 
                and i.location_id not in ('QCREJ','QCFIX')
                then 'GOOD' end as "Should be"
        from inventory i
        inner join location l on l.site_id = i.site_id and i.location_id = l.location_id
        where i.client_id = 'HS'
        and l.loc_type in ('Tag-FIFO', 'Tag-LIFO', 'Sampling', 'Receive Dock')
        )
        where "Cond Ok?" = 'N'
        order by location_id
)
/