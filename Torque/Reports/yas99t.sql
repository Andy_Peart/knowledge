SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON



select 'TML Aged Orders Report Client T' from DUAL;


SELECT 'Client_id,Order_date,Creation_date,Creation_time,Order_id,Purchase_order,Status,Order_type,Priority,Ship_dock,T_number,Consignment,Instructions,Value' from DUAL;


SELECT client_id
||','||trunc(order_date)
||','||trunc(creation_date)
||','||to_char(creation_date,'HH24:MI:SS')
||','||order_id
||','||purchase_order
||','||status
||','||order_type
||','||priority
||','||ship_dock
||','||user_def_type_5
||','||consignment
||','||instructions
||','||order_value
FROM order_header
WHERE client_id = 'T' AND
status <>'Shipped' AND
status <>'Cancelled' AND
order_type <>'CUSTOMER'
ORDER BY 
creation_date, 
order_id
/
