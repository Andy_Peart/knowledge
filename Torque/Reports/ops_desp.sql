/******************************************************************************/
/*                                                                            */
/*                            Elite                                           */
/*                                                                            */
/*     FILE NAME  :   opsdespatch.sql                                         */
/*                                                                            */
/*     DESCRIPTION:    Datastream for OPS Despatch Note / Pack List           */
/*                                                                            */
/*   DATE     BY   PROJ       ID         DESCRIPTION                          */
/*   ======== ==== ======     ========   =============                        */
/*   14/07/08 LH   OPS                   Despatch Note for OPS                */
/*	                                                                          */
/******************************************************************************/

/* Input Parameters are as follows */
-- 1)  Order ID
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 0
SET VERIFY OFF
SET FEEDBACK OFF
SET TAB OFF

 
-- Set the line length to the maximum possible, but trim 
-- trailing spaces for both screen output and spool files
SET LINESIZE 32767
SET WRAP ON
SET TRIMOUT ON
SET TRIMSPOOL ON




/* Define the format of the output dates */
DEFINE	OutputDateFormat = 'DD-MON-YYYY HH24:MI:SS'
COLUMN	sorter	NOPRINT

SELECT Distinct '0' sorter,
       'DSTREAM_OPS_DESP_HDR'          ||'|'||
       rtrim('&&2')                            /* Order_id */
from dual
union all
SELECT '1' sorter,
'DSTREAM_OPS_DESP_ADD_HDR'              ||'|'||
rtrim (oh.name)						||'|'||
rtrim (oh.customer_id)				||'|'||
rtrim (oh.address1)					||'|'||
rtrim (oh.address2)					||'|'||
rtrim (oh.town)						||'|'||
rtrim (oh.county)					||'|'||
rtrim (oh.country)					||'|'||
rtrim (oh.postcode)
from order_header oh
where oh.order_id = '&&2'
and oh.client_id = 'OPS'
union all
select distinct '1' sorter,
'DSTREAM_OPS_DESP_ORD_HDR'			||'|'||
rtrim (oh.order_id)					||'|'||
rtrim (to_char(sm.picked_dstamp, 'DD-MON-YYYY HH24:MI'))				||'|'||
rtrim (sm.location_id)				||'|'||
rtrim (sm.user_id)					||'|'||
rtrim (sm.site_id)					||'|'||
rtrim (oh.purchase_order)			||'|'||
rtrim (oh.user_def_type_6)			||'|'||
rtrim (oh.user_def_type_4)			||'|'||
rtrim (substr(oh.order_id, 3, 7))	||'|'||
rtrim (substr(oh.order_id, 10, 14)) ||'|'||
rtrim (A)							||'|'||
decode(oh.order_type,'CHINESE LA','CHINESE LAUNDRY','OPS')
from order_header oh, shipping_manifest sm, (select count(distinct smm.container_id) A from shipping_manifest smm where smm.order_id = '&&2' and smm.client_id = 'OPS' and smm.site_id = 'WORPRI')
where oh.order_id = '&&2'
and oh.order_id = sm.order_id
and oh.client_id = 'OPS'
and sm.site_id = 'WORPRI'
union all
SELECT '1'||sh.container_id SORTER,
'DSTREAM_OPS_DESP_LINE'              ||'|'||
rtrim (sh.container_id)       ||'|'||
rtrim (s.sku_id)				||'|'||
rtrim (s.description)                ||'|'||
rtrim (s.COLOUR)					||'|'||
rtrim (s.SKU_SIZE)					||'|'||
rtrim (sh.qty_shipped)				||'|'||
rtrim (s.user_def_type_5)
from sku s, shipping_manifest sh
where sh.order_id = '&&2'
and sh.qty_picked > '0'
and sh.sku_id = s.sku_id
and sh.client_id = 'OPS'
and sh.site_id = 'WORPRI'
order by 1
/