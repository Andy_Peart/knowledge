SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240



set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


--ttitle  LEFT 'Wots NOT picked for client ID &&2 as at ' report_date RIGHT 'Page:' FORMAT 999 SQL.PNO skip 2

--break on F skip 1 on A skip 1
--compute sum label 'Total' of E on A

select 'Order Date,Order ID,Ordered,Shipped,SKU,Description,Colour,Size' from DUAL;


select
to_char(oh.order_date, 'DD/MM/YYYY') ||','||
oh.order_id  ||','||
--nvl(ol.qty_ordered,0)-nvl(ol.qty_shipped,0)  ||','||
nvl(ol.qty_ordered,0) ||','||
nvl(ol.qty_shipped,0) ||','''||
ol.sku_id  ||''','||
sku.description  ||','||
sku.colour  ||','||
sku.sku_size
from order_header oh,order_line ol,sku
where oh.Client_ID='&&2'
and oh.status='Shipped'
and oh.order_date between to_date('&&3', 'dd-mon-yyyy')  and to_date('&&3', 'dd-mon-yyyy')+1
and oh.order_id=ol.order_id
and (ol.qty_shipped is null or ol.qty_shipped<ol.qty_ordered)
and sku.Client_ID='&&2'
and ol.sku_id=sku.sku_id
order by
to_char(oh.order_date, 'DD/MM/YYYY'),
oh.order_id 
/
