SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 80
set trimspool on

--SET TERMOUT OFF
--SPOOL Q70B.csv

select AA||','||BB||','||CC from (select 
sku_id AA,
case
when nvl(BB, 0) > rsv then
    BB - to_number(amz)
when nvl(BB, 0) > life and nvl(BB, 0) <= rsv then
    BB - to_number(amz)
else nvl(BB, 0)
end BB,
life CC
  from (select sku_id,
user_def_num_3 rsv,
user_def_num_4 life,
case
when lower(user_def_type_2)<>upper(user_def_type_2) then 0
else to_number(nvl(user_def_type_2,0)) end amz
from sku
where client_id='MOUNTAIN'),
       (select ii.sku_id AA, sum(ii.qty_on_hand - ii.qty_allocated) BB
        from inventory ii,
       (select distinct location_id LL
        from location where loc_type = 'Tag-FIFO')
        where ii.client_id = 'MOUNTAIN'
        and ii.location_id = LL
        group by ii.sku_id)
        where sku_id = AA(+))
--where BB>0
order by 1
/


--SPOOL OFF
--SET TERMOUT ON
