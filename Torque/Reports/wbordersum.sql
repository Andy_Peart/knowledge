define from_shipped_date=&&2
define to_shipped_date=&&3

set feedback off
set echo off
set verify off
set tab off
set termout on
set colsep ' '

/* clear previous settings */
clear columns
clear breaks
clear computes

/* set up page and line */
set pagesize 500
set newpage 1
set linesize 500
set trimspool on

column A heading 'Shipped Date' format A10
column B heading 'Orders' format 999999
column C heading 'Units' format 999999

select shipped_date
, count(distinct order_id) orders
, sum(qty_shipped) units
from
(
    select trunc(oh.shipped_date) shipped_date
    , oh.order_id
    , case when ol.user_def_chk_1 = 'Y' then sum(ol.qty_shipped/coalesce(kl.quantity, 1))
      else sum(ol.qty_shipped)
      end qty_shipped
    from ORDER_HEADER oh
    inner join ORDER_LINE ol on ol.order_id = oh.order_id and ol.client_id = oh.client_id
    left join KIT_LINE kl on kl.sku_id = ol.sku_id and kl.client_id = ol.client_id
    where oh.client_id = 'WB'
    and oh.shipped_date between '&&from_shipped_date' and to_date('&&to_shipped_date') + 1
    and oh.status = 'Shipped'
    group by trunc(oh.shipped_date)
    , oh.order_id
    , ol.user_def_chk_1
)
group by shipped_date
order by shipped_date
/