SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Sku,Desc,Qty Short'  from dual
union all
select "Sku" || ',' || "Desc" || ',' || "Qty Ordered" from
(
select ol.sku_id as "Sku"
, s.description as "Desc"
, sum(ol.qty_ordered) - sum(nvl(ol.qty_tasked,0)) -sum(nvl(ol.qty_tasked,0)) as "Qty Ordered"
from order_line ol
inner join sku s on s.client_id = ol.client_id and s.sku_id = ol.sku_id
where ol.client_id = 'SE'
and ol.qty_ordered - nvl(ol.qty_tasked,0)-nvl(ol.qty_picked,0) <> 0
group by ol.sku_id, s.description
order by 1
)
/
--spool off