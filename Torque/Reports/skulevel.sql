SET FEEDBACK OFF             
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 300
SET TRIMSPOOL ON

--spool skulevel.csv

select 'SKU,Description,Order Id,LPG,Qty per Carton' from DUAL;


select
SK||','||
SKD||','||
PID||','||
LPG||','||
to_number(Q3)
from (select
PID,
LPG,
QQ,
QU,
CASE
WHEN QQ=0 THEN '0'
ELSE to_char(QU/QQ,'9990') END Q3
from (select
PID,
LPG,
sum(QU) QU,
sum(mo.cartons) QQ
from mountainorders mo,(select
ph.pre_advice_id  PID,
sku.user_def_type_8 LPG,
--pl.sku_id SK,
sum(pl.qty_due) QU
from pre_advice_header ph,pre_advice_line pl,sku
where ph.client_id='MOUNTAIN'
and ph.pre_advice_id=pl.pre_advice_id
and pl.client_id='MOUNTAIN'
and sku.client_id = 'MOUNTAIN'
and sku.sku_id=pl.sku_id
group by
ph.pre_advice_id,
sku.user_def_type_8
union
select
ph.pre_advice_id  PID,
sku.user_def_type_8 LPG,
--pl.sku_id SK,
sum(pl.qty_due) QU
from pre_advice_header_archive ph,pre_advice_line_archive pl,sku
where ph.client_id='MOUNTAIN'
and ph.pre_advice_id=pl.pre_advice_id
and pl.client_id='MOUNTAIN'
and sku.client_id = 'MOUNTAIN'
and sku.sku_id=pl.sku_id
group by
ph.pre_advice_id,
sku.user_def_type_8)
where mo.cw_order=PID
group by
PID,
LPG)),(select
pl.pre_advice_id  PID2,
pl.sku_id SK,
sku.description SKD
from pre_advice_line pl,sku
where pl.client_id='MOUNTAIN'
and sku.client_id='MOUNTAIN'
and pl.sku_id=sku.sku_id
union
select
pl.pre_advice_id  PID2,
pl.sku_id SK,
sku.description SKD
from pre_advice_line_archive pl,sku
where pl.client_id='MOUNTAIN'
and sku.client_id='MOUNTAIN'
and pl.sku_id=sku.sku_id)
Where PID=PID2
order by
PID,
SK
/

--spool off
