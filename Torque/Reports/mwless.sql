set pagesize 68
set linesize 120
set verify off

ttitle center "Mountain Warehouse - Stock / Location less than '&&2'." skip 3

column A heading 'Zone' format a5
column B heading 'Location' format a5
column C heading 'SKU' format a13
column D heading 'QTY' format 99999

break on s.sku_id

select A,
B,
C,
D
from
 (select loc.zone_1 A,
 inv.location_id B,
 s.sku_id C,
 sum(inv.QTY_ON_HAND) D,
 sum(inv.QTY_ALLOCATED) E
 from sku s, inventory inv, location loc
 where s.client_id = 'MOUNTAIN'
 and s.sku_id = inv.sku_id
 and inv.location_id = loc.location_id
 and inv.location_id <> 'MARSHAL'
 and inv.location_id <> 'DESPATCH'
 and loc.zone_1 = 'AMW'
 group by loc.zone_1, inv.location_id, s.sku_id)
where D < '&&2'
and E=0
order by 1
/