/******************************************************************************/
/*                                                                            */
/*                            Elite                                           */
/*                                                                            */
/*     FILE NAME  :   Denim_Desp.sql                                          */
/*                                                                            */
/*     DESCRIPTION:    Datastream for Denim Despatch Note                     */
/*                                                                            */
/*   DATE     BY   PROJ     ID         DESCRIPTION                            */
/*   ======== ==== ======   ========   =============                          */
/*   10/11/10 LH   Denim               Despatch Note for Denim                */
/******************************************************************************/

/* Input Parameters are as follows */
-- 1)  Order ID
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 0
SET VERIFY OFF
SET FEEDBACK OFF
SET TAB OFF

 
-- Set the line length to the maximum possible, but trim 
-- trailing spaces for both screen output and spool files
SET LINESIZE 32767
SET WRAP ON
SET TRIMOUT ON
SET TRIMSPOOL ON




/* Define the format of the output dates */
DEFINE	OutputDateFormat = 'DD-MON-YYYY HH24:MI:SS'
COLUMN	sorter	NOPRINT

SELECT Distinct '0' sorter,
       'DSTREAM_DN_DESP_HDR'          ||'|'||
       rtrim('&&2')                            /* Order_id */
from dual
union all
SELECT '0' sorter,
'DSTREAM_DN_DESP_ADD_HDR'				||'|'||
rtrim (ad.name)							||'|'||
rtrim (ad.address_id)					||'|'||
rtrim (ad.address1)						||'|'||
rtrim (ad.address2)						||'|'||
rtrim (ad.town)							||'|'||
rtrim (ad.county)						||'|'||
rtrim (ad.country)						||'|'||
rtrim (ad.postcode)						||'|'||
rtrim (ad.contact_fax)
from address ad, order_header oh
where oh.order_id = '&&2'
and oh.client_id = 'DN'
and oh.customer_id = ad.address_id
union all
select distinct '0' sorter,
'DSTREAM_DN_DESP_ORD_HDR'				||'|'||
rtrim (oh.order_id)						||'|'||
rtrim (to_char(oh.shipped_date, 'DD-MON-YYYY HH24:MI'))        ||'|'||
rtrim (sm.location_id)					||'|'||
rtrim (sm.user_id)						||'|'||
rtrim (sm.site_id)
from order_header oh, shipping_manifest sm
where oh.order_id = '&&2'
and oh.order_id = sm.order_id
union all
SELECT unique B sorter,
'DSTREAM_DN_DESP_LINE'					||'|'||
rtrim (substr(B,1,1))					||'|'||
rtrim (substr(s.user_def_type_5,1,6))   ||'|'||
rtrim (ol.sku_id)						||'|'||
rtrim (substr(s.DESCRIPTION,1,14))      ||'|'||
rtrim (s.COLOUR)						||'|'||
rtrim (s.SKU_SIZE)						||'|'||
rtrim (i.update_qty)        
from order_line ol, sku s, inventory_transaction i, (select unique loc.location_id A, loc.zone_1 B
														from location loc
														where site_id = 'DN')
where ol.order_id = '&&2'
and ol.qty_shipped > '0'
and ol.sku_id = s.sku_id
and i.reference_id = ol.order_id
and i.line_id = ol.line_id
and ol.client_id = 'DN'
and i.update_qty >'0'
and i.from_loc_id = A
and i.code = 'Pick' 
and from_loc_id <> 'CONTAINER'
order by 1
/