SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'SE Stock excluding BAD or FAULT Locations' from dual
union all
select 'Sku,Description,Qty' from dual
union all
select '''' || "Sku" || ''',''' || "Desc" || ''',' || "Qty" from
(
select i.sku_id         as "Sku"
, replace(s.description,',',' ')         as "Desc"
, sum(i.qty_on_hand)    as "Qty"
from inventory i
inner join sku s on s.sku_id = i.sku_id and s.client_id = i.client_id
inner join location l on l.location_id = i.location_id and l.site_id = i.site_id
where i.client_id = 'SE'
and i.location_id not in ('SEBAD','SEFAULT')
group by i.sku_id
, s.description
)
/
--spool off