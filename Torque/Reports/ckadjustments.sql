SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Date,Customer,Sku,Description,Colour,Sku Size,Cup Size,Adjustment,Starting Qty,Current Qty' from dual
union all
select "Date" || ',' || "Customer" || ',''' || "Sku" || ''',' || "Description" || ',' || "Colour" || ',' || "Sku Size" || ',' || "Cup Size" || ',' || "Adj"
|| ',' || sum("Qty"-"Adj") || ',' || "Qty"
from
(
with t1 as (
select trunc(it.dstamp)     as "Date"
, it.client_id              as "Client"
, it.customer_id            as "Customer"
, it.sku_id                 as "Sku"
, s.description             as "Description"
, s.colour                  as "Colour"
, s.sku_size                as "Sku Size"
, s.user_def_note_1         as "Cup Size"
, sum(it.update_qty)        as "Adj"
from inventory_transaction it
inner join sku s on s.client_id = it.client_id and s.sku_id = it.sku_id
where it.client_id = 'CK'
and it.dstamp between to_date('&&2') and to_date('&&3')+1
and it.code = 'Adjustment'
group by trunc(it.dstamp)
, it.customer_id
, it.sku_id
, s.description
, s.colour
, s.sku_size
, s.user_def_note_1
, it.client_id
), t2 as (
select sku_id       as "Sku"
, sum(nvl(qty_on_hand,0))  as "Qty"
, client_id         as "Client"
from inventory
where client_id = 'CK'
group by sku_id
, client_id
)
select t1."Date"    as "Date"
, t1."Customer"     as "Customer"
, t1."Sku"          as "Sku"
, t1."Description"  as "Description"
, t1."Colour"       as "Colour"
, t1."Sku Size"     as "Sku Size"
, t1."Cup Size"     as "Cup Size"
, t1."Adj"          as "Adj"
, nvl(t2."Qty",0)          as "Qty"
from t1
left join t2 on t2."Sku" = t1."Sku" and t2."Client" = t1."Client"
where t1."Adj" <> 0
)
group by "Date"
, "Customer"
, "Sku"
, "Description"
, "Colour"
, "Sku Size"
, "Cup Size"
, "Adj"
, "Qty"
/
--spool off