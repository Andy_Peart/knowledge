SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON

break on report
compute sum of Q on report

--spool stdinvbymthsumm.csv

select 'Client Id &&2' from DUAL;
select 'Month,Qty On Hand' from Dual;

select 
RECDATE,
Q
from (select
to_char(receipt_dstamp,'YYYY MM Mon') RECDATE,
sum(qty_on_hand) Q
from inventory
where client_id='&&2'
group by
to_char(receipt_dstamp,'YYYY MM Mon')
order by 
RECDATE DESC)
where substr(RECDATE,1,1)='2'
/


--spool off

