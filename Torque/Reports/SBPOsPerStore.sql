SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET COLSEP ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 200
SET TRIMSPOOL ON
SET HEADING OFF 

column A heading 'Date' format A12
column B heading 'Store' format A20
column C heading 'No of Orders' format 999999



select 'Sweaty Beatys POs per Store between &&2 and &&3' from DUAL;
--select to_char(SYSDATE, 'DD/MM/YY    HH:MI:SS') from DUAL;

select 'Date, Store, No of Orders' from DUAL;


break on A dup skip 1



select to_char(oh.creation_date, 'DD-MM-YYYY') as date_, customer_id, count(*) no_of_orders
from order_header oh 
where client_id = 'SB'
and order_id not like 'C%'
and order_id not like 'E%'
and status not in ('Cancelled')
and creation_date between to_date('&&2') and to_date('&&3')+1
group by to_char(oh.creation_date, 'DD-MM-YYYY') , customer_id
order by 1,2
/