SET FEEDBACK OFF
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240
SET TRIMSPOOL ON

--SET TERMOUT OFF

break on report
compute sum label'Totals' of G H J on report


--spool stdoutpa.csv


select 'Creation Date,Last Update Date,Status,Pre Advice ID,Supplier,Name,Lines,Qty Due,Qty Received,Differance,Due Date,Booked Date' from DUAL;

select
A,
B,
B2,
C,
D,
E,
F,
G,
H,
J,
K
from (select
trunc(ph.creation_date) A,
trunc(LUD) B,
ph.status B2,
ph.pre_advice_id C,
ph.supplier_id D,
nvl(NMR,' ') E,
count(*) F,
sum(pl.qty_due) G,
sum(nvl(pl.qty_received,0)) H,
sum(pl.qty_due)-sum(nvl(pl.qty_received,0)) J,
trunc(ph.due_dstamp) K
from pre_advice_header ph, pre_advice_line pl,(select
address_id ADR,
name NMR
from address
where client_id='&&2'),(select
reference_id RR,
max(Dstamp) LUD
from inventory_transaction
where client_id='&&2'
group by reference_id)
where ph.pre_advice_id = pl.pre_advice_id
and ph.client_id = '&&2'
and ph.status not in ('Complete')
and ph.pre_advice_id=RR
and ph.supplier_id=ADR (+)
group by
trunc(ph.creation_date),
LUD,
ph.status,
ph.pre_advice_id,
ph.supplier_id,
nvl(NMR,' '),
trunc(ph.due_dstamp))
order by
A,C
/

--spool off

