SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','
SET UND OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 320
SET TRIMSPOOL ON


column A1 format A18
column A format A18
column B format A40
column Z format A12
column C format 9999999
column D format 9999999
column E format 9999999
column F format 9999999
column G format 9999999
column H format 9999999
column J format 9999999
column K format 9999999
column L format 9999999
column N format 9999999


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



break on report

compute sum label 'Total'  of C D E G H  on report

--set termout off
--spool EQUA.csv

select ',,EQUA Comparison_Report - '||to_char(SYSDATE, 'DD/MM/YY  HH24:MI:SS') from DUAL;

select 
distinct 
'Report Date: '||to_char(SYSDATE, 'DD/MM/YY  HH:MI')||',Last Update: '||to_char(the_date,'DD/MM/YY HH:MI')
from dx_skufile
/

select 'Category,Product,Description,Dax_qty,TORQUE_qty,Difference' from DUAL;


select * from (select
nvl(product_group,'') A1,
KKK A,
nvl(description,'Not on SKU table') B,
to_number(nvl(the_qty,0)) C,
nvl(QQQ,0) D,
to_number(nvl(the_qty,0))-nvl(QQQ,0) E
from (select distinct KKK
from (select sku_id KKK
from sku
where client_id='DX'
and sku_id not like '*%'
union
select the_sku KKK
from dx_skufile)),(Select
sku_id SSS,
sum(qty_on_hand) QQQ
from inventory
where client_id='DX'
and location_id in ('DXBREJ1')
and condition_id not in ('DMGD','FATY')
group by sku_id),(select
the_SKU,
max(the_qty) the_qty
from dx_skufile
where the_warehouse like 'EQUA%'
group by the_sku),(select
sku.sku_id skusku,
product_group,
description
from sku
where client_id='DX')
where KKK=the_sku (+)
and KKK=SSS (+)
and KKK=skusku (+))
where C<>0 or D<>0
order by
A
/


 
--spool off
--set termout on
