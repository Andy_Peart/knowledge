set feedback off
set pagesize 68
set linesize 240
set verify off

clear columns
clear breaks
clear computes

column D heading 'Date' format a16
column A heading 'Reference' format A18
column A2 heading 'Order Type' format A12
column B heading 'Qty' format 999999
column M noprint


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

set heading on

ttitle  LEFT 'Units Picked from '&&2' to '&&3' - for client ID &&4 as at ' report_date skip 2

break on D skip 1 on report

compute sum label 'Day Total' of B on D
compute sum label 'Total' of B on report



select
trunc(ITL.Dstamp) D,
ITL.reference_id A,
oh.order_type A2,
SUM(ITL.update_qty) B
from inventory_transaction ITL INNER JOIN ORDER_HEADER OH ON OH.ORDER_ID=ITL.REFERENCE_ID AND OH.CLIENT_ID=ITL.CLIENT_ID
where ITL.client_id='&&4'
and ITL.code='Pick'
AND ITL.ELAPSED_TIME>0
and ITL.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by trunc(ITL.Dstamp),ITL.reference_id,oh.order_type
order by trunc(ITL.Dstamp),ITL.reference_id,oh.order_type
/
