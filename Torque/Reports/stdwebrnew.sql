SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON

select 'Date,Client,Order,Order Date,Customer Name,Country,SKU,Description,Colour,Size,Qty,Return Reason,Reason Notes,' from dual;

select  A || ',' || B || ',' || C || ',' || D || ',' || E || ',' || F || ',' || G || ',' || H || ',' || I || ',' || J || ',' || K || ',' || L || ',' || M from 
(
select to_char(itl.dstamp, 'DD-MON-YYYY') as A
, itl.client_id as B
, itl.REFERENCE_ID as C
, to_char(oh.CREATION_DATE,'DD-MON-YYYY') as D
, oh.contact as E
, oh.country as F
, itl.sku_id as G 
, s.description as H
, s.colour as I 
, s.sku_size as J 
, itl.UPDATE_QTY as K  
, substr(itl.REASON_ID,3,2) as L
, r.notes as M
from inventory_transaction itl
inner join order_header oh on itl.client_id = oh.client_id and itl.reference_id = oh.order_id
inner join SKU S on itl.sku_id = s.sku_id and itl.client_id = s.client_id
left join return_reason r on itl.reason_id = r.REASON_ID
where itl.client_id        = '&&2'
AND itl.dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and itl.code in ('Return')
order by 1
)
/