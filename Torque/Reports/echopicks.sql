SET FEEDBACK OFF                 
set pagesize 68
set linesize 160
set verify off
set colsep ' '

clear columns
clear breaks
clear computes

column D heading 'Pick Date' format a16
column A heading 'Order ID' format a16
column B heading 'Ordered' format 999999
column C heading 'Picked' format 999999
column M noprint

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'Total Units Picked from &&2 to &&3 - for client ID ECHO as at ' report_date skip 2

break on D skip 2 on report

compute sum label 'Period Totals' of B C on report
compute sum label 'Day Totals' of B C on D

select
to_char(it.Dstamp,'DD/MM/YYYY') D,
to_char(it.Dstamp,'YYYY/MM/DD') M,
it.reference_id A,
sum(ol.qty_ordered) B,
sum(it.update_qty) C
from inventory_transaction it,order_line ol
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='ECHO'
and it.code like 'Pick'
and it.elapsed_time>0 
--or (it.from_loc_id<>'CONTAINER' and it.to_loc_id<>'CONTAINER'))
and it.reference_id=ol.order_id
and it.line_id=ol.line_id
group by
to_char(it.Dstamp,'YYYY/MM/DD'),
to_char(it.Dstamp,'DD/MM/YYYY'),
it.reference_id
order by
M,D,A
/
