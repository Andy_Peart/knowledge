SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

column A heading 'No' format A10
column B heading 'Name' format A28
column E heading 'Units Shipped' format 999999


variable   v_sysdate  varchar2(11);
variable   v_sysday  varchar2(11);
declare
   vsysdate date;
begin
   vsysdate := sysdate-1;
   if to_char(vsysdate,'DAY') like 'SUNDAY%' then
      vsysdate := sysdate-3;
   end if;
   :v_sysdate:=to_char(vsysdate,'DD-MON-YYYY');
   :v_sysday:=to_char(vsysdate,'DAY  ');
end;
/

--select 'Shipments on,'||to_char(:v_sysdate, 'DAY DD MONTH YYYY')||', for Client TR' from DUAL;
select 'Shipments on,'||:v_sysday||:v_sysdate||', for Client TR' from DUAL;
select 'Store No,Store Name, Units Shipped' from DUAL;
 

break on report 

compute sum label 'Total' of E on report

select
it.customer_id A,
name B,
sum(it.update_qty) E
from inventory_transaction it,address
where it.client_id='TR'
and it.code='Shipment'
and it.Dstamp between to_date(:v_sysdate, 'DD-MON-YYYY') and to_date(:v_sysdate, 'DD-MON-YYYY')+1
--and it.Dstamp between trunc(:v_sysdate-1) and trunc(:v_sysdate)
--and it.Dstamp between trunc(sysdate-1) and trunc(sysdate)
--and trunc(it.Dstamp)=trunc(sysdate-1)
and it.customer_id=address_id
and address.client_id='TR'
group by
it.customer_id,
name
order by
it.customer_id
/

