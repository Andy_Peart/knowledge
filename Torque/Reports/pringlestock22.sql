set feedback off
set pagesize 66
set linesize 120
set verify off


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 

column A heading '  ' format A20
column B heading 'SKU' format A20
column C heading 'Description' format A40
column D heading 'Qty' format 9999999
column E heading '' format A14

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'Stock Units by Location Range from &&2 to &&3 - for client ID &&4 as at ' report_date RIGHT 'Page:' FORMAT 999 SQL.PNO skip 4

set heading on
set pagesize 999

select
'Location Count =' A,
count (*) D
from (select distinct location_id from inventory
where client_id like '&&4'
and location_id>='&&2'
and location_id<='&&3')
/

set heading off
set pagesize 0

select
'     SKU Count =' A,
count (*) D
from (select distinct sku_id from inventory
where client_id like '&&4'
and location_id>='&&2'
and location_id<='&&3')
/

select
'    Unit Count =' A,
sum(qty_on_hand) D
from inventory
where client_id like '&&4'
and location_id>='&&2'
and location_id<='&&3'
/

