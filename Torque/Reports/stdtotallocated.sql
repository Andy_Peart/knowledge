SET FEEDBACK OFF                 
set pagesize 68
set linesize 160
set verify off

clear columns
clear breaks
clear computes

column D heading 'Date' format a16
column A heading 'Reference ID' format a16
column B heading 'Allocated' format 999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'Total Units Allocated from &&2 to &&3 - for client ID &&4 as at ' report_date skip 2

break on report on D skip 2

compute sum label 'Report Totals' of B on report
compute sum label 'Daily Total' of B on D


select
to_char(it.Dstamp, 'DD/MM/YY') D,
it.reference_id A,
sum(it.update_qty) as B
from inventory_transaction it
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='&&4'
and it.code='Allocate'
group by
to_char(it.Dstamp, 'DD/MM/YY'),
it.reference_id
order by
to_char(it.Dstamp, 'DD/MM/YY'),
it.reference_id
/

