SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Client,Sku Code,Condition,Current Stock,Description,Colour,Size,Days since last Receipt,Days since last Shipment,Days since last Allocation,Location Zone,Owner ID,Product Code,Location'
/* TORQUE - (CC) - Slow Movers with Locations */
from dual
union all
select CLIENT ||','|| SKU ||','|| COND ||','|| QTY ||','|| DESCRIP ||','|| COLOUR ||','|| SKU_SIZE ||','|| DAYS_SINCE_RECD ||','|| DAYS_SINCE_SHIP  ||','|| DAYS_SINCE_ALLOC  ||','|| LOC_ZONE ||
 ','||owner_id||','||product_group ||','||location_id
from 
(
	Select SKU, 'CC' CLIENT, COND, QTY, sku.description DESCRIP, sku.colour COLOUR, sku.sku_size SKU_SIZE, LOC_ZONE,
	trunc(sysdate)-trunc(REC_DATE) DAYS_SINCE_RECD,
	DECODE(to_char(Shipped_date, 'DD/MM/YY')	,'01/01/00','None',trunc(sysdate)-trunc(Shipped_date))  DAYS_SINCE_SHIP,
  DECODE(to_char(Alloc_date, 'DD/MM/YY')	,'01/01/00','None',trunc(sysdate)-trunc(Alloc_date))  DAYS_SINCE_ALLOC,
  sku.product_group tml_group, sku.user_def_type_2 tml_season, sku.user_def_type_1 tml_style, sku.user_def_type_4 tml_quality,owner_id,product_group, location_id
	from
	(
		select T1.sku_id SKU, T1.condition_id COND, T1.Rec_date REC_DATE, T1.qty QTY, nvl(T2.shipped_date ,'01-jan-00') Shipped_date, nvl(T3.Alloc_date ,'01-jan-00') Alloc_date, T1.loc_zone,owner_id, t1.location_id
		from
		(
			select inv.sku_id, inv.condition_id, trunc(inv.receipt_dstamp) Rec_date, sum(inv.qty_on_hand) qty, inv.zone_1 loc_zone, inv.owner_id, inv.location_id
			from inventory inv 
			where inv.client_id = 'CC'
			group by inv.sku_id, inv.condition_id, inv.zone_1, inv.owner_id, inv.location_id, trunc(inv.receipt_dstamp)
		) T1,
		(
			select i.Sku_id, max (i.dstamp) Shipped_date
			from inventory_transaction i
			where i.client_id = 'CC'
			AND TRUNC (I.DSTAMP) >= TRUNC (SYSDATE - '&&2')
			group by i.sku_id
		) T2,
    (
			select i.Sku_id, max (i.dstamp) Alloc_date
			from inventory_transaction i
			where i.client_id = 'CC'
         	and i.code = 'Allocate'
			AND TRUNC (I.DSTAMP) >= TRUNC (SYSDATE - '&&2')
			group by i.sku_id
		) T3
		where T1.SKU_ID=T2.SKU_ID (+)
    and T1.SKU_ID=T3.SKU_ID (+)
	),
	sku
	where 
  sku.client_id = 'CC'
	and SKU=sku.sku_id
	and Shipped_date <sysdate - '&&2'
	and REC_DATE <sysdate - '&&2'
  and Alloc_date <sysdate - '&&2'
	order by Shipped_date,REC_DATE
)
/
--spool off