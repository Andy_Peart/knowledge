define from_rec_date=&&2
define to_rec_date=&&3

set feedback off
set echo off
set verify off
set tab off
set termout on
set colsep ' '

/* clear previous settings */
clear columns
clear breaks
clear computes

/* set up page and line */
set pagesize 500
set newpage 1
set linesize 500
set trimspool on

column A heading 'Receipt Date' format A10
column B heading 'POs' format 999999
column C heading 'Units' format 999999

select receipt_date
, count(distinct reference_id) POs
, sum(units_booked_in) units
from
(
    select trunc(it.dstamp) receipt_date
    , it.reference_id
    , sum(it.update_qty) units_booked_in
    from INVENTORY_TRANSACTION it    
    where it.client_id = 'WB'
	and it.code = 'Receipt'
    and it.dstamp between '&&from_rec_date' and to_date('&&to_rec_date') + 1
    and it.reference_id is not null
    group by trunc(it.dstamp)
    , it.reference_id
)
group by receipt_date
order by receipt_date
/