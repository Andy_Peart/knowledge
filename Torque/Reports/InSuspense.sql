set pagesize 68
set linesize 120
set verify off
set feedback off

clear columns
clear breaks
clear computes

column F heading 'Tag ID' format a14
column A heading 'SKU' format a20
column B heading 'DESCRIPTION' format a30
column C heading 'Qty' format 999999
column D heading ' ' format a2
column E heading 'From' format a7

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MM') curdate from DUAL;
set TERMOUT ON


ttitle  CENTER 'Stock in SUSPENSE - for client ID &&1 as at ' report_date skip 2

break on report

compute sum label 'Total' of C on report


select
iv.tag_id F,
iv.sku_id A,
sku.description B,
nvl(iv.qty_on_hand,0) C,
'  ' D,
it.from_loc_id E
from inventory iv,inventory_transaction it,sku
where iv.client_id='&&1'
and iv.location_id='SUSPENSE'
and it.to_loc_id='SUSPENSE'
and iv.sku_id=it.sku_id
and iv.sku_id=sku.sku_id
and to_char(iv.Move_dstamp,'DD/MM/YY HH:MM:SS')=to_char(it.dstamp,'DD/MM/YY HH:MM:SS')
order by A
/
