SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET TRIMSPOOL ON

select 'Order shipped between &&2 and &&3' from dual
union all
select 'Order,UDType6,Status,Shipped Date' from dual
union all
select order_id || ',' || user_def_type_6 || ',' || status || ',' || sh_date
from (
select order_id, user_def_type_6, status, to_char(shipped_date,'DD-MM-YYYY') sh_date
from order_header
where client_id = 'SB'
and shipped_date between to_date('&&2','DD-MON-YYYY') and to_date('&&3','DD-MON-YYYY')+1
order by 1
)
/