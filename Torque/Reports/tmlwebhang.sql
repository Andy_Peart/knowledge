SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 900
SET TRIMSPOOL ON
SET HEADING ON

select 'Date Created,All units ordered,Hang units ordered,Hang percentage' from dual
union all
select created || ',' || qty_ordered || ',' || hang_ordered || ',' || percentage || '%' from (
    select to_char(oh.creation_date,'DD-MM-YYYY') created,  sum(ol.qty_ordered) qty_ordered,
    sum(case when product_group in ('WC','WW','WD','WO','MZ','WB','WM','WJ','WP','MC','MW','MP','MJ') then qty_ordered else 0 end) hang_ordered,
    round((sum(case when product_group in ('WC','WW','WD','WO','MZ','WB','WM','WJ','WP','MC','MW','MP','MJ') then qty_ordered else 0 end)/ sum(qty_ordered))*100) as percentage
    from order_header oh inner join order_line ol on oh.order_id = ol.order_id and oh.client_id = ol.client_id
    inner join sku on sku.client_id = oh.client_id and sku.sku_id  = ol.sku_id
    where oh.client_id = 'TR'
    and oh.order_type = 'WEB'
    and oh.creation_date between to_date('&&2') and to_date('&&3')+1
    group by to_char(oh.creation_date,'DD-MM-YYYY')
    order by 1
)
/