SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 66
SET NEWPAGE 0
SET LINESIZE 240

column A heading 'CONTAINER NO' format A12
column AA heading 'IncorrectLabelDest' format A20
column B heading 'CorrectDest' format A14
column C heading 'ORDER NO' format A12


select a.SEQUENCE_No A
    ,a.Address_id AA 
  ,b.SHIPMANWORKGROUP B 
  ,b.order_id C
from dcsdba.yo_labelcheck a
join 
(
 SELECT SHIPMANWORKGROUP,order_id,container_id
  FROM 
(select
  oh.work_group as HeaderWorkGroup
  , sh.work_group as ShipmanWorkGroup
  , sh.container_id
  ,oh.order_id
 -- ,oh.creation_date
--  ,sh.picked_dstamp
from order_header oh
left join shipping_manifest sh on oh.order_id =sh.order_id
  where oh.client_id ='MOUNTAIN'
  and sh.client_id ='MOUNTAIN'
  and oh.order_id like 'MOR%'
  and oh.from_site_id = 'BD2'
  and oh.creation_date > trunc(sysdate) - 15
  and sh.work_group <>'WWW'
--  and sh.shipped_dstamp is null
  and sh.picked_dstamp between trunc(sysdate)and trunc(sysdate+1) --todays
--  and oh.order_id = 'MOR60903'
--    and sh.container_id = 'JJD0002240183011593'
group by  sh.container_id,oh.work_group, sh.work_group,oh.order_id)
  where SHIPMANWORKGROUP='&&2'
 ) b on 
  b.CONTAINER_ID = a.SEQUENCE_No
  and a.Address_id <> b.SHIPMANWORKGROUP
/