SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 80
SET TRIMSPOOL ON


column A format A12
column B format A10
column C format A10
column D format A8
column E format A10
column F format 99999
column G format 99999
column M noprint

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



--set termout off
--spool MWwebshipments.csv


--select 'Units Shipped by Retail Store from &&2 to &&3 - for client ID SB' from DUAL;
--select '  ' from DUAL;
Select 'Date Shipped,Orders Shipped,Units Shipped' from DUAL;

--break on A on report

--compute sum label 'TOTALS' of F G on report


select
trunc(oh.shipped_date)||','||
count(*)||','||
sum(ol.qty_shipped)
from order_header oh,order_line ol
where oh.client_id='MOUNTAIN'
and oh.shipped_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and oh.work_group='WWW'
and oh.order_id=ol.order_id
and ol.client_id='MOUNTAIN'
group by
trunc(oh.shipped_date)
order by 1
/


--spool off
--set termout on
