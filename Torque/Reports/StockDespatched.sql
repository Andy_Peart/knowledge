set feedback off
set pagesize 68
set linesize 100
set verify off

clear columns
clear breaks
clear computes

column A heading 'Shipped' format A16
column B heading 'Order ID' format A20
column C heading 'Customer' format A12
column D heading 'Ordered' format 9999999
column E heading 'Shipped' format 9999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'Total Units Despatched from &1 to &2 - for client ID PRI as at ' report_date skip 2

btitle CENTER 'Page:'FORMAT 999 SQL.PNO


break on report

compute sum label 'Wholesale Totals' of D E on report

select
to_char(oh.shipped_date, 'DD/MM/YY ') as A,
oh.order_id as B,
oh.inv_name as C,
nvl(sum(ol.qty_ordered),0) as D,
nvl(sum(ol.qty_shipped),0) as E
from order_header oh, order_line ol
where  oh.order_id=ol.order_id
and oh.client_id='PRI'
and oh.status='Shipped'
and oh.shipped_date between to_date('&1', 'DD-MON-YYYY') and to_date('&2', 'DD-MON-YYYY')+1
and oh.order_id like '31%'
group by
to_char(oh.shipped_date, 'DD/MM/YY '),
oh.order_id,
oh.inv_name
order by
to_char(oh.shipped_date, 'DD/MM/YY '),
oh.order_id
/

ttitle  LEFT '  '

break on report

compute sum label 'Retail Totals' of D E on report

select
to_char(oh.shipped_date, 'DD/MM/YY ') as A,
oh.order_id as B,
oh.inv_name as C,
nvl(sum(ol.qty_ordered),0) as D,
nvl(sum(ol.qty_shipped),0) as E
from order_header oh, order_line ol
where  oh.order_id=ol.order_id
and oh.client_id='PRI'
and oh.status='Shipped'
and oh.shipped_date between to_date('&1', 'DD-MON-YYYY') and to_date('&2', 'DD-MON-YYYY')+1
and oh.order_id like '51%'
group by
to_char(oh.shipped_date, 'DD/MM/YY '),
oh.order_id,
oh.inv_name
order by
to_char(oh.shipped_date, 'DD/MM/YY '),
oh.order_id
/
