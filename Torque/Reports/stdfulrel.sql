SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Client,Date/Time,Sku id,Qty Relocated,From Location,To Location,User' from dual
union all
select client_id||','||"Date"||','||sku_id||','||qty||','||from_loc_id||','||to_loc_id||','||user_id
from
(
select client_id
, to_char(dstamp, 'DD/MM/YYYY HH24:MI') as "Date"
, sku_id
, qty
, from_loc_id
, to_loc_id
, user_id
from torque.fn_tq_relocatestock
where client_id = '&&2'
and dstamp between to_date('&&3') and to_date('&&4')+1
order by 2
)
/
--spool off