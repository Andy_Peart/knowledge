set feedback off                 
set verify off
set wrap off
set colsep ','

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 250
set trimspool on

select 'SKU,Description,Expiry Date,Days until Expired,Location id,Units' headers from dual
union all
select sku_id||',"'||description||'",'||expiry_date||','||days_until_expiry||','||location_id||','||units
from
(
    select i.sku_id
    , s.description
    , trunc(i.expiry_dstamp) expiry_date
    , trunc(i.expiry_dstamp) - trunc(sysdate) days_until_expiry
    , i.location_id
    , sum(i.qty_on_hand) units
    from INVENTORY i
    inner join SKU s on s.sku_id = i.sku_id and s.client_id = i.client_id
    where i.client_id = 'WB'
    and i.expiry_dstamp is not null
    group by i.sku_id
    , s.description
    , i.expiry_dstamp
    , i.location_id
    order by 4 /* days until expiry */
    , 1 /* SKU */
)
/