SET FEEDBACK OFF                 
set pagesize 68
set linesize 126
set verify off
set wrap off
set newpage 0
set colsep ' '
set und on

clear columns
clear breaks
clear computes


column BB heading 'Order No' format A11
column CC heading 'Dock' format A6
column CCC heading 'Store Name' format A28
column DD heading 'Order Date       Time' format A26
column SS heading 'Status' format A14
column MM heading 'MT Status' format A11
column GG heading 'Consignment' format A12
column HH format A12 noprint
column TT heading 'Order Type' format A14
column PPP heading 'Units' format 999999
column QQQ heading 'M Units' format 999999
column UU heading ' OK to pick?' format A13

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



ttitle  LEFT 'TML Retail - Open Orders Report as at ' report_date RIGHT 'Page:' FORMAT 999 SQL.PNO skip 2


break on report on HH skip 1

compute sum label 'Grand Total' of PPP on report
compute sum label 'Total Units' of PPP on HH


Select
oh.Order_ID BB,
oh.customer_id CC,
a.name CCC,
to_char(oh.creation_date, 'Dy DD Mon YYYY  HH:MI:SS') DD,
oh.status SS,
--move_task_status MM,
oh.Consignment GG,
oh.Consignment HH,
nvl(PP,0) PPP,
--nvl(QQQ,0) QQQ,
--DECODE (user_def_type_1,'OK FOR PICKING','     Yes',
--			'OK','     Yes','     No') UU,
--user_def_type_1 UU,
oh.Order_Type TT
from order_header oh,
(select
oh.order_id  ID,
nvl(sum(ol.qty_tasked),0) PP
from order_header oh,order_line ol
where oh.client_id='TR'
and oh.order_id=ol.order_id
group by
oh.order_id),address a
--,(select
--order_id OOO,
----list_id LLL,
--sum(qty_to_move) QQQ
--from order_header oh, move_task mt
--where oh.client_id='TR'
--and mt.client_id='TR'
--and oh.consignment=mt.consignment
--and oh.order_id=mt.task_id
--group by
--order_id)
where
oh.STATUS not in ('Cancelled','Shipped')
and oh.CLIENT_ID='TR'
and oh.order_id=ID
--and order_id=OOO (+)
and oh.customer_id=a.address_id
and a.CLIENT_ID='TR'
order by
GG,BB
/



