SET FEEDBACK OFF                 
set verify off
set wrap off
set colsep ','

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 300
set trimspool on

--spool ccfulfill4.csv

Select 'Date,User Def Type 2,Qty Ordered,Qty Allocated,Qty Shipped,% Allocation Fulfilled,% Order Fulfilled' from DUAL
union all
select replace(FRED,'  ',' ') from (select
to_char(DD,'YYMMDD')||',0,' MM,
DD||','||
BB||','||
nvl(Q1,0)||','||
nvl(Q1-Q2,0)||','||
nvl(Q3,0)||','||
--to_char(((Q1-Q2)/Q1)*100,'999.000')||','||
to_char((Q3/(Q1-Q2))*100,'999.000')||','||
to_char((Q3/Q1)*100,'999.000') FRED
from (select
AA,
DD,
BB,
sum(QQ1) Q1,
nvl(sum(QQQ),0) Q2,
sum(QQ2) Q3
from (select
oh.client_id AA,
trunc(oh.shipped_date) DD,
DECODE(oh.user_def_type_2,'3','Retail','203','Outlet','299','Pallet Outlet',oh.user_def_type_2) BB,
oh.order_id OID,
sum(qty_ordered) QQ1,
sum(qty_shipped) QQ2
from order_header oh,order_line ol
where oh.status='Shipped'
and oh.client_id like '&&2'
and oh.shipped_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and oh.client_id=ol.client_id
and oh.order_id=ol.order_id
group by
oh.client_id,
trunc(oh.shipped_date),
DECODE(oh.user_def_type_2,'3','Retail','203','Outlet','299','Pallet Outlet',oh.user_def_type_2),
oh.order_id),(select
oh.client_id CID,
oh.order_id RID,
sum(gs.qty_ordered-gs.qty_tasked) QQQ
from generation_shortage gs,order_header oh
where gs.client_id='&&2'
and gs.task_id=oh.order_id
and gs.client_id=oh.client_id
group by
oh.client_id,
oh.order_id)
where AA=CID (+)
and OID=RID (+)
group by
AA,DD,BB)
-----------------
union
select
to_char(DD,'YYMMDD')||',1,' MM,
DD||','||
BB||','||
nvl(Q1,0)||','||
nvl(Q1-Q2,0)||','||
nvl(Q3,0)||','||
--to_char(((Q1-Q2)/Q1)*100,'999.000')||','||
to_char((Q3/(Q1-Q2))*100,'999.000')||','||
to_char((Q3/Q1)*100,'999.000')
from (select
AA,
DD,
BB,
sum(QQ1) Q1,
nvl(sum(QQQ),0) Q2,
sum(QQ2) Q3
from (select
oh.client_id AA,
trunc(oh.shipped_date) DD,
'Daily Total' BB,
oh.order_id OID,
sum(qty_ordered) QQ1,
sum(qty_shipped) QQ2
from order_header oh,order_line ol
where oh.status='Shipped'
and oh.client_id like '&&2'
and oh.shipped_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and oh.client_id=ol.client_id
and oh.order_id=ol.order_id
group by
oh.client_id,
trunc(oh.shipped_date),
oh.order_id),(select
oh.client_id CID,
oh.order_id RID,
sum(gs.qty_ordered-gs.qty_tasked) QQQ
from generation_shortage gs,order_header oh
where gs.client_id='&&2'
and gs.task_id=oh.order_id
and gs.client_id=oh.client_id
group by
oh.client_id,
oh.order_id)
where AA=CID (+)
and OID=RID (+)
group by
AA,DD,BB)
order by MM)
/



--spool off



