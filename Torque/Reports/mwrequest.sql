SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9999
SET TRIMSPOOL ON

--spool mwrequest.csv

select ' Order ID,Sku,Description,Colour,Size,Order Qty,Qty Available,Reserve Qty' from DUAL;

select
B ||','''||
H||''','||
I||','||
J||','||
K ||','||
QQ ||','||
G ||','||
F
from (select
to_char(oh.order_date, 'DD/MM/YYYY HH24:MI') A,
oh.order_id B,
nvl(ol.qty_ordered,0) C,
nvl(ol.qty_tasked,0) D,
nvl(ol.qty_picked,0) E,
nvl(ol.qty_ordered,0)-nvl(ol.qty_tasked,0)-nvl(ol.qty_picked,0) QQ,
nvl(sku.user_def_num_3,0) F,
nvl(QQQ,0) G,
ol.sku_id H,
sku.description I,
sku.colour J,
sku.sku_size K
from order_header oh,order_line ol,sku,(select
sku_id SSS,
sum(nvl(qty_on_hand,0)-nvl(qty_allocated,0)) QQQ
from inventory
where Client_ID='MOUNTAIN'
group by sku_id)
where oh.Client_ID='MOUNTAIN'
and oh.status='Allocated'
and oh.order_date>sysdate-1
and ol.Client_ID='MOUNTAIN'
and oh.order_id=ol.order_id
and nvl(ol.qty_ordered,0)=0
and sku.Client_ID='MOUNTAIN'
and ol.sku_id=sku.sku_id
and ol.sku_id=SSS (+))
--where (C-(D+E)>0 or C=0)
where C=0
order by
B
/

--spool off
