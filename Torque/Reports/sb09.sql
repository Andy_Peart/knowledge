SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

--Select 'SB08 C Mail Order Returns' from DUAL;
select 'Return Ref,Store No,Store Name,SKU,Qty,Description,Condition' from DUAL;


column A format A12
column B format A6
column C format A28
column D format A10
column E format 999999
column F format A40
column G format A16


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



--ttitle  LEFT 'TML Retail - Open Orders Report ' RIGHT 'Printed ' report_date skip 2


--break on A skip 1

--compute sum label 'Total Units' of E F G on report


select
i.reference_id A,
address_id B,
name C,
i.sku_id D,
i.update_qty E,
'"' || sku.description || '"' F,
i.condition_id F
from inventory_transaction i,sku,address
where i.client_id='SB'
and i.reference_id like 'S%'
and i.dstamp>sysdate-1
--and to_char(i.dstamp,'DD/MM/YY')='24/10/07'
and i.sku_id=sku.sku_id
and substr(i.reference_id,3,3)=address_id
order by
i.reference_id
--i.sku_id
/
