SET FEEDBACK OFF                
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 400
SET TRIMSPOOL ON



set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

--spool pgrec.csv

select 'Returns Received By Date Range to Excel from &&2 to &&3 - for Yumi as at '||to_char(SYSDATE, 'DD/MM/YY  HH:MI') from DUAL;

select 'Sku Id,transaction_date, user_def_type_1, user_def_type_2, Qty Received' from DUAL;

select sku_id||','||','|| transaction_date ||','|| replace(user_def_type_1,',','') ||','|| replace(user_def_type_2,',','') ||','|| update_qty
from (select it.sku_id, trunc(it.Dstamp) transaction_date, user_def_type_1, user_def_type_2, sum(it.update_qty) update_qty
from inventory_transaction it
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='UT'
and (it.code like 'Receipt%' or it.code='Return')
and it.reference_id ='003'
group by
trunc(it.Dstamp),
it.reference_id,
it.sku_id, 
user_def_type_1, 
user_def_type_2
order by transaction_date,user_def_type_1)
union all
select sku_id||','|| transaction_date ||','|| replace(user_def_type_1,',','') ||','|| replace(user_def_type_2,',','') ||','|| update_qty
from (select it.sku_id, line_id, trunc(it.Dstamp) transaction_date, user_def_type_1, user_def_type_2, sum(it.update_qty) update_qty
from inventory_transaction_archive it
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='UT'
and (it.code like 'Receipt%' or it.code='Return')
and it.reference_id ='003'
group by
trunc(it.Dstamp),
it.reference_id,
it.sku_id,
line_id, user_def_type_1, user_def_type_2
order by transaction_date,user_def_type_1)
/

--spool off
