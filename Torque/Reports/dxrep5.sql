SET FEEDBACK OFF                
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET COLSEP ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 400
SET TRIMSPOOL ON

column A format A12
column B format A14
column C format 99999
column D format A20
column E format A12
column F format A12
column G format A12
column H format A24
column J format A40
column K format 99999
column L format 99999
column N format 99999

--break on B dup skip 1

--compute sum label 'Order Total'  of K L N on B


--spool dxrep5.csv

select 'Outstanding_purchase_orders - '||to_char(SYSDATE, 'DD/MM/YY  HH24:MI:SS') from DUAL;

select 'Run Date,Status,Pre Advice,Notes,Creation Date,Due Date,Account,Name,Line,SKU,Description,Ordered,Received,Variance' from DUAL;

Select 
to_char(SYSDATE, 'DD/MM/YY @ HH:MI')||','||
'OPEN'||','||
ph.pre_advice_id||','||
ph.notes||','||
nvl(to_char(ph.last_update_date,'DD/MM/YY'),to_char(ph.creation_date,'DD/MM/YY'))||','||
to_char(ph.due_dstamp,'DD/MM/YY')||','||
ph.supplier_id||','||
address.name||','||
pl.line_id||','||
pl.sku_id||','||
sku.description||','||
pl.qty_due||','||
nvl(pl.qty_received,0)||','||
(pl.qty_due-nvl(pl.qty_received,0))
from pre_advice_header ph,pre_advice_line pl,sku,address
where ph.client_id='DX'
and ph.status<>'Complete'
and pl.client_id='DX'
and pl.pre_advice_id=ph.pre_advice_id
and (pl.qty_due-nvl(pl.qty_received,0))>0
and pl.sku_id=sku.sku_id
and sku.client_id='DX'
and ph.supplier_id=address.address_id (+)
--group by
--ph.status,
--ph.pre_advice_id,
--ph.notes,
--to_char(ph.last_update_date,'DD/MM/YY'),
--to_char(ph.due_dstamp,'DD/MM/YY'),
--ph.supplier_id,
--address.name
order by
ph.pre_advice_id,
pl.line_id
/

--spool off



