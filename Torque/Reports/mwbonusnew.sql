SET FEEDBACK OFF
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 2400
SET TRIMSPOOL ON

break on A dup skip 1

compute sum label "Total" of C D F H on A

--spool mwbonushr2.csv


select 'Period &&2 &&4 to &&3 (NO AAU)' from DUAL
union all
select 'Picker,Zone,Lines Picked,Units Picked,Zone kpi,CalcTime' from DUAL
union all
select
A||','||
B||','||
C||','||
D||','||
G||','||
H
from (select
A,
B,
C,
D,
G,
to_number(to_char((C/G)*60,'99990.00')) H
from (select
it.user_id A,
lc.work_zone B,
DECODE(lc.work_zone,'FOOT',69,'EQIP',53,'MJAC',97,'WJAC',97,'ACCS',128,'12A',60,'12B',60, 144) G,
count(*) C,
sum(it.update_qty) D,
sum(it.elapsed_time) F
from inventory_transaction it,location lc
where it.Dstamp between to_date('&&2 &&4', 'DD-MON-YYYY HH24:MI:SS') and to_date('&&3 &&5', 'DD-MON-YYYY HH24:MI:SS')
and it.client_id='MOUNTAIN'
and it.update_qty<>0
and it.station_id not like 'Auto%'
and it.code='Pick'
and it.reference_id like 'MOR%'
and it.work_group<>'AAU'
and it.elapsed_time>0
and it.from_loc_id=lc.location_id
and it.site_id=lc.site_id
and lc.work_zone not in ('11','12','G')
group by it.user_id,lc.work_zone)
union
select
A,
'[Total]' B,
sum(C) C,
sum(D) D,
0 G,
sum(H) H
from (select
A,
B,
C,
D,
G,
to_number(to_char((C/G)*60,'99990.00')) H
from (select
it.user_id A,
lc.work_zone B,
DECODE(lc.work_zone,'FOOT',69,'EQIP',53,'MJAC',97,'WJAC',97,'ACCS',128,'12A',60,'12B',60, 144) G,
count(*) C,
sum(it.update_qty) D,
sum(it.elapsed_time) F
from inventory_transaction it,location lc
where it.Dstamp between to_date('&&2 &&4', 'DD-MON-YYYY HH24:MI:SS') and to_date('&&3 &&5', 'DD-MON-YYYY HH24:MI:SS')
and it.client_id='MOUNTAIN'
and it.update_qty<>0
and it.station_id not like 'Auto%'
and it.code='Pick'
and it.reference_id like 'MOR%'
and it.work_group<>'AAU'
and it.elapsed_time>0
and it.from_loc_id=lc.location_id
and it.site_id=lc.site_id
and lc.work_zone not in ('11','12','G')
group by it.user_id,lc.work_zone))
group by A
order by A,B)
/

--spool off
