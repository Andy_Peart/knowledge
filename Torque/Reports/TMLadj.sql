SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 9999
SET LINESIZE 9999
SET TRIMSPOOL ON

select 
KEY,
CODE,
CLIENT_ID,
SKU_ID,
TAG_ID,
DSTAMP,
REFERENCE_ID,
LINE_ID,
REASON_ID,
UPDATE_QTY,
ORIGINAL_QTY
from inventory_transaction
where code ='Adjustment'
and client_id ='T'
and reason_id ='T760'
and update_qty <0
and trunc(dstamp)=trunc(sysdate)
/
