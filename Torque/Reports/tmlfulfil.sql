SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','
SET WRAP OFF


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 220

select 'Week &&2 Run &&3,,Ordered,Shipped,Difference,Fulfilment' from DUAL;

select A || ',' || ',' || B || ',' || C || ',' || D || ',' || E
FROM (
select
	'Total for the week' A,
	sum(nvl(ol.qty_ordered,0)) B,
	sum(nvl(ol.qty_shipped,0)) C,
	sum(nvl(ol.qty_ordered,0)-nvl(ol.qty_shipped,0)) D,
	round((sum(nvl(ol.qty_shipped,0))*100)/sum(nvl(ol.qty_ordered,0)),2) E
from order_header oh inner join order_line ol on oh.order_id=ol.order_id and oh.client_id=ol.client_id
inner join sku on oh.client_id=sku.client_id and ol.sku_id=sku.sku_id inner join address a on oh.customer_id=a.address_id
where oh.client_id='TR'
and oh.order_type <> 'WEB'
and oh.consignment like '&&2%'
and substr(oh.consignment,5,1)='&&3'
and oh.creation_date>sysdate-300
and oh.status not in ('Cancelled','Hold')
and a.client_id='TR'
and sku.client_id = 'TR'
)
/

select ' ' from DUAL;


select A || ',' || A1 || ',' || B || ',' || C || ',' || D || ',' || E
FROM (
select
	oh.customer_id A, a.name A1,
	sum(nvl(ol.qty_ordered,0)) B,
	sum(nvl(ol.qty_shipped,0)) C,
	sum(nvl(ol.qty_ordered,0)-nvl(ol.qty_shipped,0)) D,
	round((sum(nvl(ol.qty_shipped,0))*100)/sum(nvl(ol.qty_ordered,0)),2) E
from order_header oh inner join order_line ol on oh.order_id=ol.order_id and oh.client_id=ol.client_id
inner join sku on oh.client_id=sku.client_id and ol.sku_id=sku.sku_id inner join address a on oh.customer_id=a.address_id
where oh.client_id='TR'
and oh.order_type <> 'WEB'
and oh.consignment like '&&2%'
and substr(oh.consignment,5,1)='&&3'
and oh.creation_date>sysdate-300
and oh.status not in ('Cancelled','Hold')
and a.client_id='TR'
and sku.client_id = 'TR'
group by oh.customer_id, a.name
order by E desc
)
/

select ' ' from DUAL;
select 'Store, Name, Order Id, SKU, Descr, Group, CIMS code, Size, Ordered, Shipped, Difference' from DUAL;

select A ||','|| B ||','|| C ||','|| D ||','|| E ||','|| F ||','|| G ||','|| H ||','|| I ||','|| J ||','|| K
from (
select
oh.customer_id A,
a.name B,
oh.order_id C,
''''||ol.sku_id D,
sku.description E,
sku.product_group F,
SKU.USER_DEF_TYPE_2||'/'||SKU.USER_DEF_TYPE_1||'/'||SKU.USER_DEF_TYPE_4 G,
SKU.SKU_Size H,
nvl(ol.qty_ordered,0) I,
nvl(ol.qty_shipped,0) J,
nvl(ol.qty_ordered,0)-nvl(ol.qty_shipped,0) K
from order_header oh,order_line ol,sku,address a
where oh.client_id='TR'
and oh.order_type <> 'WEB'
and oh.consignment like '&&2%'
and substr(oh.consignment,5,1)='&&3'
and oh.creation_date>sysdate-300
and nvl(ol.qty_ordered,0)-nvl(ol.qty_shipped,0)<>0
and oh.status not in ('Cancelled','Hold')
and oh.client_id=ol.client_id
and oh.order_id=ol.order_id
and oh.client_id=sku.client_id
and ol.sku_id=sku.sku_id
and a.client_id='TR'
and oh.customer_id=a.address_id
order by A
)
/