SET FEEDBACK OFF                 
set verify off
set wrap off
set colsep ','

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 132
set trimspool on


column B heading 'Order' format a8
column B2 heading 'Seller' format a16
column C heading 'Shipments' format 999999
column C2 heading 'Orders' format 999999
column D heading 'Units' format 999999
column F heading 'Date' format a12

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


break on REPORT on F skip 1 on XX dup

compute sum label 'Report Totals' of C C2 D on REPORT
compute sum label 'Daily Totals' of C C2 D on F 
compute sum label 'XX Totals' of C C2 D on XX



--spool mkshipments2.csv


select 'Shipments Breakdown by Date from &&2 to &&3 - for client ID MENKIND' from DUAL;

select 'Date,XX,Order,ORD,Seller,U4,Shipments,Orders,Units' from DUAL;

select
F,
CASE
WHEN upper(substr(B2,1,3))='GAD'
AND substr(U4,1,3) in ('PAY','CRE')
THEN 'GADGET GROTTO'
WHEN B='AMZ' or substr(U4,1,3)='AMA'
THEN 'AMAZON'
WHEN upper(substr(B,1,3)) in ('RET') 
THEN 'RETAIL'
WHEN upper(substr(B,1,3)) in ('TES') 
THEN 'TESCO'
WHEN upper(substr(B,1,3)) in ('IN ') 
THEN 'IN STORE'
ELSE 'MENKIND'
END XX,
B,
ORD,
B2,
U4,
C,
C2,
D
from (select
to_char(it.Dstamp, 'DD/MM/YY') F,
oh.order_type B,
oh.seller_name B2,
oh.user_def_type_4 U4,
DECODE(substr(oh.order_id,1,1),'Q','RETURN','') ORD,
count(*) C,
count(distinct oh.order_id) C2,
sum(it.update_qty) D
from inventory_transaction it, order_header oh
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='MK'
and oh.client_id='MK'
and it.update_qty<>0
and it.code='Shipment'
and it.reference_id=oh.order_id
group by
to_char(Dstamp, 'DD/MM/YY'),
oh.order_type,
oh.seller_name,
oh.user_def_type_4,
DECODE(substr(oh.order_id,1,1),'Q','RETURN',''))
order by
1,2,3,4
/

--spool off
