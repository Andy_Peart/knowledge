SET FEEDBACK OFF                 
set pagesize 66
set linesize 136
set verify off
set newpage 0

clear columns
clear breaks
clear computes

column A heading 'User ID' format a12
column B heading 'Location' format a12
column BB heading 'Reference' format a12
column C heading 'SKU ID' format a20
column CC heading 'Description' format a40
column D heading 'Date' format a10
column TT heading 'Time' format a10
column M format a4 noprint

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'Nil Shipments - from &&2 to &&3 - for client ID &&4 as at ' report_date skip 2

break on D skip 1 on report

--compute sum label 'Totals' of E B on report


select
to_char(it.Dstamp, 'YYMM') M,
to_char(it.Dstamp, 'DD/MM/YY') D,
to_char(it.Dstamp, 'HH:MI:SS') TT,
it.user_id A,
it.from_loc_id B,
it.reference_id BB,
it.sku_id C,
sku.description CC
from inventory_transaction it,sku
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='&&4'
and it.code like 'Ship%'
and it.update_qty=0
and it.sku_id=sku.sku_id
order by
to_char(it.Dstamp, 'YYMM'),
to_char(it.Dstamp, 'DD/MM/YY'),
to_char(it.Dstamp, 'HH:MI:SS') 
/

