SET FEEDBACK OFF                 
set verify off
set wrap off
set colsep ','

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 300
set trimspool on


--spool mwretailshipped.csv
select 'Shipments from &&2 to &&3' from DUAL;
select 'Date,Orders,Lines,Units,Average LPO,Average UPO,Average UPL' from DUAL;

select
replace(THELOT,' ','')
from (select
SD||','||
Q3||','||
Q2||','||
Q1||','||
Q4||','||
Q5||','||
Q6 THELOT
from (select
SD,
Q3,
Q2,
Q1,
to_char(Q2/Q3,'9999.99') Q4,
to_char(Q1/Q3,'9999.99') Q5,
to_char(Q1/Q2,'99.99') Q6
from (select
trunc(oh.shipped_date) SD,
sum(ol.qty_shipped) Q1,
count(*) Q2,
count(distinct oh.order_id) Q3
from order_header oh
left join order_line ol on ol.client_id=oh.client_id and oh.order_id=ol.order_id
where oh.client_id='MOUNTAIN'
and oh.status='Shipped'
and oh.work_group<>'WWW'
and oh.shipped_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and nvl(ol.qty_shipped,0)>0
and upper(ol.sku_id)=lower(ol.sku_id)
--and oh.order_id='MOR95984'
group by trunc(oh.shipped_date))
order by
SD))
/

select
replace(THELOT,' ','')
from (select
SD||','||
Q3||','||
Q2||','||
Q1||','||
Q4||','||
Q5||','||
Q6 THELOT
from (select
'   Totals' SD,
Q3,
Q2,
Q1,
to_char(Q2/Q3,'9999.99') Q4,
to_char(Q1/Q3,'9999.99') Q5,
to_char(Q1/Q2,'99.99') Q6
from (select
--trunc(oh.shipped_date) SD,
sum(ol.qty_shipped) Q1,
count(*) Q2,
count(distinct oh.order_id) Q3
from order_header oh
left join order_line ol on ol.client_id=oh.client_id and oh.order_id=ol.order_id
where oh.client_id='MOUNTAIN'
and oh.status='Shipped'
and oh.work_group<>'WWW'
and oh.shipped_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and nvl(ol.qty_shipped,0)>0
and upper(ol.sku_id)=lower(ol.sku_id))))
/


--spool off

