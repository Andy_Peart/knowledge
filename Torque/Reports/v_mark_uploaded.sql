declare
    UploadRows   integer := 10000;
    UploadDays   integer := 100;
    RowsMarked   integer;
begin
    --
    -- NB: This script is not supported by RedPrairie.
    --

    dbms_output.enable (100000);

    dbms_output.put_line ('UploadMarking begins at [' ||
        to_char (sysdate, 'DD-MON-YYYY HH24:MI:SS') || ']');
 
    -- Mark unnecessary rows as uploaded in "chunks".
    begin
        RowsMarked := 0;
 
        -- The SQL below assumes that the job will run
        -- roughly weekly so allows for two weeks worth
        -- of data in order to allow for a missed week.
        loop
            update inventory_transaction
            set uploaded = 'Y'
            where uploaded = 'N'
            and dstamp between
                current_timestamp - numtodsinterval ((UploadDays + 15), 'day') and
                current_timestamp - numtodsinterval (UploadDays, 'day')
            and rownum <= UploadRows;
 
            RowsMarked := RowsMarked + sql%rowcount;
            exit when sql%notfound;
            commit;
        end loop;
    end;

    dbms_output.put_line ('Old ITL marking completed at [' ||
        to_char (sysdate, 'DD-MON-YYYY HH24:MI:SS') || '], [' ||
        RowsMarked || '] rows marked');
 
    --
    -- All "Deallocate", "Putaway" and "Cond Update" ITLs can be 
    -- marked as uploaded.
    --
    begin
        RowsMarked := 0;
 
        loop
            update inventory_transaction
            set uploaded = 'Y'
            where uploaded = 'N'
            and code in ('Deallocate', 'Cond Update', 'Putaway')
            and dstamp <= current_timestamp - numtodsinterval (1, 'day')
            and rownum <= UploadRows;
 
            RowsMarked := RowsMarked + sql%rowcount;
            exit when sql%notfound;
            commit;
        end loop;
    end;

    dbms_output.put_line ('Deallocation/Cond Update marking completed at [' ||
        to_char (sysdate, 'DD-MON-YYYY HH24:MI:SS') || '], [' ||
        RowsMarked || '] rows updated');
 
    --
    -- All Marshal ITLs where CLIENT_ID is NULL can be marked as uploaded.
    --
    begin
        RowsMarked := 0;
 
        loop
            update inventory_transaction
            set uploaded = 'Y'
            where uploaded = 'N'
            and code = 'Marshal'
            and client_id is null
            and dstamp <= current_timestamp - numtodsinterval (1, 'day')
            and rownum <= UploadRows;
 
            RowsMarked := RowsMarked + sql%rowcount;
            exit when sql%notfound;
            commit;
        end loop;
    end;

    dbms_output.put_line ('Marshal marking completed at [' ||
        to_char (sysdate, 'DD-MON-YYYY HH24:MI:SS') || '], [' ||
        RowsMarked || '] rows updated');

    --
    -- All picks and allocations for MOUNTAIN can be marked as uploaded.
    --
    begin
        RowsMarked := 0;
 
        loop
            update inventory_transaction
            set uploaded = 'Y'
            where uploaded = 'N'
            and code in ('Pick', 'Allocate')
            and client_id = 'MOUNTAIN'
            and dstamp <= current_timestamp - numtodsinterval (1, 'day')
            and rownum <= UploadRows;
 
            RowsMarked := RowsMarked + sql%rowcount;
            exit when sql%notfound;
            commit;
        end loop;
    end;

    dbms_output.put_line ('Mountain pick/allocate marking completed at [' ||
        to_char (sysdate, 'DD-MON-YYYY HH24:MI:SS') || '], [' ||
        RowsMarked || '] rows updated'); 

    --
    -- Relocations more than twenty days old can be marked as uploaded.
    --
    begin
        RowsMarked := 0;
 
        loop
            update inventory_transaction
            set uploaded = 'Y'
            where uploaded = 'N'
            and code = 'Relocate'
            and dstamp <= current_timestamp - numtodsinterval (20, 'day')
            and rownum <= UploadRows;
 
            RowsMarked := RowsMarked + sql%rowcount;
            exit when sql%notfound;
            commit;
        end loop;
    end;

    dbms_output.put_line ('Old relocation marking completed at [' ||
        to_char (sysdate, 'DD-MON-YYYY HH24:MI:SS') || '], [' ||
        RowsMarked || '] rows updated'); 

    commit;
end;
/
