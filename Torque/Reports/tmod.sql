SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON

set pagesize 999
set linesize 260
set heading on

clear columns
clear breaks
clear computes


TTitle 'Client T Breakdown of Mail Order Dispatch Methods from &&2 to &&3' skip 2

column A1 heading 'Orders' format 9999999
column A2 heading 'Containers' format 9999999
column B1 heading 'Units' format 9999999
column D heading 'Method' format A25

break on report

compute sum label 'Totals' of A1 A2 B1 on report



select
DECODE(
	oh.dispatch_method,
		1,'Store Orders',
		22,'Store Orders NDD',
		2,'Packet Post',
		3,'NDD',
		997,'NDD 150+',
		4,'OCS',
		5,'OCS Tracked',
			'No dispatchmethod') D,
count(distinct oh.order_id) A1,
nvl(sum(ol.qty_shipped),0) B1
from order_header oh, order_line ol
where oh.client_id='TR'
and oh.shipped_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and oh.order_id=ol.order_id
and oh.order_type = 'WEB'
and ol.client_id='TR'
group by
DECODE(
	oh.dispatch_method,
		1,'Store Orders',
		22,'Store Orders NDD',
		2,'Packet Post',
		3,'NDD',
		997,'NDD 150+',
		4,'OCS',
		5,'OCS Tracked',
			'No dispatchmethod')
/