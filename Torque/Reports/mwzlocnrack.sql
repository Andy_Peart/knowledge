SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON

select 'Zone 1','Update QTY','Date' from DUAL
union all
SELECT 
TO_CHAR(loc2.ZONE_1),
TO_CHAR(SUM(ITL.UPDATE_QTY)),
TO_CHAR(TRUNC(ITL.DSTAMP))
FROM INVENTORY_TRANSACTION ITL inner join location loc
on itl.site_id = loc.site_id
and ITL.FROM_LOC_ID = loc.location_id
inner join location loc2
on itl.site_id = loc2.site_id
and ITL.TO_LOC_ID = loc2.location_id
WHERE 
loc.LOC_TYPE = 'Receive Dock'
and ITL.FROM_LOC_ID like 'Z0%'
and loc2.ZONE_1 = 'RACK1'
and ITL.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
GROUP BY 
TO_CHAR(loc2.ZONE_1),
TO_CHAR(TRUNC(ITL.DSTAMP)) 
union all
SELECT 
TO_CHAR(loc2.ZONE_1),
TO_CHAR(SUM(ITL.UPDATE_QTY)),
TO_CHAR(TRUNC(ITL.DSTAMP))
FROM INVENTORY_TRANSACTION ITL inner join location loc
on itl.site_id = loc.site_id
and ITL.FROM_LOC_ID = loc.location_id
inner join location loc2
on itl.site_id = loc2.site_id
and ITL.TO_LOC_ID = loc2.location_id
WHERE 
loc.ZONE_1 = 'RACK1'
and loc2.ZONE_1 = 'RETAIL'
AND ITL.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
GROUP BY 
TO_CHAR(loc2.ZONE_1),
TO_CHAR(TRUNC(ITL.DSTAMP))
/
