#!/bin/bash
#
#   TORQUE - (STD) - Shipments with Scan Detail
#

timezone=$1
client=$2
fromdate=$3
todate=$4

reportname="${client}_SHIPMENTS_WITH_SCAN_${fromdate}_${todate}_"
sqlfile="stdscanships.sql"

datetime=$(date +%F_%H%M%S)
filename="${reportname}_${datetime}.csv"

sqlplus -s $ORACLE_USR << ! > ${HOME}/ReportOutput/results/${filename}
@$DCS_USERRPTDIR/${sqlfile} $client $fromdate $todate
exit
/
!

echo "File written to:"
echo "\\\\rp2\\user_reports\\${filename}"