SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 60
SET NEWPAGE 0
SET LINESIZE 200

column A heading 'Client' format a12
column B heading 'Order' format a12
column C heading 'Type' format a12
column D heading 'Order Date' format a12
column E heading 'Ship By Date' format a12
column F heading 'SKU' format a18



select
to_char(ih.order_date,'dd/mm/yy') D,
ih.client_id A,
ih.order_id B,
ih.order_type C,
to_char(ih.ship_by_date,'dd/mm/yy') E,
il.sku_id F
from interface_order_header ih,interface_order_line il
where ih.order_id=il.order_id
and ih.client_id='MOUNTAIN'
/

SET PAGESIZE 0

select 'NO ERRORS on MOUNTAIN' from
(select count(*) CT
from interface_order_header ih,interface_order_line il
where ih.order_id=il.order_id
and ih.client_id='MOUNTAIN')
where CT=0
/
