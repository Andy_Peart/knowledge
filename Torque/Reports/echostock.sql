SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET COLSEP ' '

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 

column A heading 'Zone' format A11
column B heading 'Qty on Hand' format 9999999
column C heading 'Qty Allocated' format 9999999
column D heading 'Qty Available' format 9999999
column X format a9
column XX noprint

set linesize 100
set pagesize 0

break on X skip 3

compute sum label 'Total' of B C D on X

set heading off


select 'Stock Holding Report for Client ECHO on ',to_char(SYSDATE, 'DD/MM/YY  @  HH:MI:SS') from DUAL;
select ' ' from DUAL;
select '          Zone        Qty on Hand Qty Allocated Qty Available' from DUAL;

select
DECODE(i.zone_1,'ERSR','RSR','RSR','RSR','EUSA','ECHO','ECHO','ECHO','  ') X,
DECODE(i.zone_1,'ERSR',1,'RSR',2,'EUSA',3,'ECHO',4,5) XX,
i.zone_1 A,
sum(i.qty_on_hand) B,
sum(i.qty_allocated) C,
sum(i.qty_on_hand-i.qty_allocated) D
from inventory i
where i.client_id like 'ECHO'
group by
i.zone_1
order by XX
/
