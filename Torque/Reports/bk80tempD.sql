SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240


column A heading 'Transfer Code' format A16
column B heading 'W/H Loc' format A8
column C heading 'Barcode' format A14
column D heading 'Qty' format 999999
column E heading 'Stock Type' format A12
column F heading 'Bin Loc' format A8

Select 'Transfer Code,W/H Loc,Barcode,Qty,Stock Type,Bin Loc' from DUAL;


select
'4'||','||
'WH1'||','||
substr(sku.ean,1,12)||','||
sum(nvl(i.qty_on_hand,0))||','||
'SUSPENSE'||','||
'WH1'
from inventory i,sku
where i.client_id='TR'
and i.location_id='SUSPENSE'
and sku.client_id='TR'
and i.sku_id=sku.sku_id
group by
sku.ean
order by
sku.ean
/
