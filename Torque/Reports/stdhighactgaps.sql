SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 200
set trimspool on
set newpage 0
set verify off
set colsep ','

--spool mw24928.csv

select 'Date,Time,Actual Elapsed,Seconds Elapsed,System Seconds,Differance,Order Id,From Location,Qty,User' from DUAL;

select
B||','||
C||','||
D||','||
D1||','||
D2||','||
D3||','||
S||','||
E||','||
F||','||
H||','||
J
from (select
B,
C,
D,
substr(D,4,2)*60+substr(D,7,2) D1,
D2,
substr(D,4,2)*60+substr(D,7,2)-D2 D3,
S,
E,
F,
H,
CASE
WHEN substr(D,4,2)*60+substr(D,7,2)-D2>60 THEN 'XXXXXX'
ELSE ' ' END J
from (select
Code A,
to_char(Dstamp,'DD/MM/YYYY') B,
to_char(Dstamp,'HH24:MI:SS') C,
substr(Dstamp-lag(Dstamp,1) over (order by user_id,Dstamp),12,8) D,
elapsed_time D2,
reference_id S,
from_loc_id E,
update_qty F,
user_id H
from inventory_transaction
where client_id='&&2'
and Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and code='Pick'
and elapsed_time>0
and user_id='&&5'
order by
user_id,
Dstamp))
--where ABS(D1-D2)>100
/

--spool off
