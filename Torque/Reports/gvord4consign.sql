SET FEEDBACK OFF                
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 66
SET NEWPAGE 0
SET LINESIZE 120
SET TRIMSPOOL ON

column AA Heading 'Order ID' format A10
column AB Heading 'Status' format A12
column BB Heading 'Store' format A10
column CC Heading 'Store Name' format A32
column SS Heading 'MT Status' format A12
column QQ Heading 'Qty' format 999999
column TT Heading 'Order Type' format A14
column F format A10 noprint

break on BB dup skip 1 on report

compute sum label 'Total' of QQ on report

ttitle '     GIVE Orders for Consignment' RIGHT 'Page:' FORMAT 999 SQL.PNO skip 2


--select 'Date,From,To,User,Units,' from DUAL;

select
oh.order_id AA,
oh.status AB,
oh.ship_dock BB,
ad.name CC,
sum(mt.qty_to_move) QQ,
mt.status SS,
oh.order_type TT
from order_header oh, move_task mt,address ad
where oh.client_id='GV'
and oh.status in ('Allocated','In Progress','Released')
--and mt.client_id='GV'
--and oh.consignment=mt.consignment
and oh.consignment is null
and oh.order_id=mt.task_id (+)
and ad.client_id='GV'
and oh.ship_dock=ad.address_id
group by
oh.order_id,
oh.status,
oh.ship_dock,
ad.name,
mt.status,
oh.order_type
order by
oh.ship_dock
/
