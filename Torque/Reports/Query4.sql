SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240



select 'Reference,SKU,Description,Date Stamp,Update Qty,Notes,Code,Client ID'
from dual;


select
iv.reference_id ||',''' ||
iv.sku_id ||''','||
sku.description ||','||
to_char(Dstamp, 'DD/MM/YYYY HH:MM') ||','||
nvl(iv.update_qty,0) ||','||
iv.notes ||','||
iv.code ||','||
iv.client_id
from inventory_transaction iv,sku
where iv.client_id='&&2'
and iv.reference_id like '&&3%'
and iv.code like '&&4%'
and iv.sku_id=sku.sku_id
order by iv.reference_id
/
