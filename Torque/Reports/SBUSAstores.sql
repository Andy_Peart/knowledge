SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET COLSEP ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 200
SET TRIMSPOOL ON
SET HEADING OFF 

column A heading 'Order' format A20
column B heading 'Status' format A20
column C heading 'Order Volume' format 9999999
column D heading 'No. of Lines' format 9999999
column E heading 'Shipping Date' format 999999
column F heading 'Customer ID' format A20
column G heading 'Store Name' format A20
column H heading 'Country' format A20

select 'Order,Status,Order Volume,Number of Lines,Shipping Date,Customer ID,Store Name,Country' A1 from dual
union all
select order_id || ',' || status || ',' || order_volume || ',' || NUM_LINES || ',' || dd || ',' || customer_id || ',' || name || ',' || country 
from
(
select
oh.order_id,
oh.status,
oh.order_volume,
oh.NUM_LINES,
TO_CHAR(oh.shipped_date,'YYYY-MM-DD') dd,
oh.customer_id,
oh.name,
oh.country
from 
ORDER_HEADER_ARCHIVE oh
where
oh.client_id='SB'
and oh.ORDER_ID like '%T%'
and oh.country = 'USA'
AND oh.CREATION_DATE BETWEEN to_date('&&2', 'DD-MON-YYYY') AND TO_DATE('&&3', 'DD-MON-YYYY')+1
union all
select
oh.order_id,
oh.status,
oh.order_volume,
oh.NUM_LINES,
TO_CHAR(oh.shipped_date,'YYYY-MM-DD') dd,
oh.customer_id,
oh.name,
oh.country
from 
ORDER_HEADER oh
where
oh.client_id='SB'
and oh.ORDER_ID like '%T%'
and oh.country = 'USA'
AND oh.CREATION_DATE BETWEEN to_date('&&2', 'DD-MON-YYYY') AND TO_DATE('&&3', 'DD-MON-YYYY')+1
order by 
dd
)
