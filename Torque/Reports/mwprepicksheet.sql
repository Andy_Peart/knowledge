SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET LINESIZE 200
SET TRIMSPOOL ON
SET HEADING OFF





column AA noprint
column R heading 'Order' format a16
column A heading 'SKU' format a16
column A1 heading 'Description' format a20
column A2 heading 'PORD' format a15
column B heading ' ' format a4
column C heading 'Location' format a18
column D heading 'Pick' format 9999
column D2 heading 'Stck' format 9999
column D4 heading 'Ordered' format 9999999
column E heading 'Picked' format A12
column F heading 'Size' format A6
column J heading 'PO' format A13 
column M format A8 Noprint
column G heading 'X' format A1



select '              Consignment &&2' from DUAL;
select ' ' from DUAL;
select 'From Location      SKU              Description                              Size         Qty' from DUAL;
select '-------------      ---              -----------                              ----         ---' from DUAL;
select ' ' from DUAL;

--break on row skip 1 on report

--compute sum label 'Total' of D D2 D4 on report


select
ln.pick_sequence AA,
mt.from_loc_id C,
mt.sku_id A,
sku.description,
sku.sku_size,
sum(mt.qty_to_move) D
from move_task mt,sku,location ln
where mt.client_id='MOUNTAIN'
and mt.from_loc_id not in ('CONTAINER','UKPAC','UKPAR','STG')
and mt.consignment like '&&2%'
and mt.sku_id=sku.sku_id
and sku.client_id='MOUNTAIN'
and mt.from_loc_id=ln.location_id
and ln.site_id='BD2'
group by
ln.pick_sequence,
ln.site_id,
mt.task_type,
mt.from_loc_id,
mt.sku_id,
sku.description,
sku.sku_size
order by
ln.pick_sequence,
mt.from_loc_id
/

