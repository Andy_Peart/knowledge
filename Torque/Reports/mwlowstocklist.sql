set pagesize 0
set linesize 120
set newpage 0
set verify off
set heading on
set feedback off
set trimspool on



set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


--ttitle LEFT 'Stock with quantity of 1 for Client MOUNTAIN as at ' report_date RIGHT 'Page:'FORMAT 999 SQL.PNO skip 2

column A heading 'Zone' format a12
column B heading 'Location' format a12
column C heading 'SKU' format a20
column D heading 'QTY' format 999999

--spool mwlist.csv

Select 'Zone,Location,SKU,Qty' from DUAL;


select A||','||
B||','||
C||','||
D
from
 (select inv.zone_1 A,
 inv.location_id B,
 inv.sku_id C,
 sum(inv.QTY_ON_HAND) D,
 sum(inv.qty_allocated) E
 from inventory inv
 where inv.client_id = 'MOUNTAIN'
 and inv.zone_1='RETAIL'
 group by inv.zone_1, inv.location_id, inv.sku_id)
where D=1
order by 1
/


--spool off
