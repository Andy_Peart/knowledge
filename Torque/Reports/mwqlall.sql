SET FEEDBACK OFF
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
set colsep ','
set heading off
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9999
SET TRIMSPOOL ON



--spool mwqlall.csv

select 'Tag Id,Sku Id,Description,Receipt Date,Receipt Id,Work Zone,Location,Qty on Hand' from dual;

select
A||','||
B||','||
D||','||
C2||','||
C||','||
--D2||','||
D3||','||
E||','||
F
from (select
inv.tag_id A,
sku.sku_id B,
sku.description D,
--sku.user_def_type_8 D2,
L.work_zone D3,
trunc(inv.receipt_dstamp) C2,
inv.receipt_id C,
inv.location_id E,
inv.qty_on_hand F
from sku, inventory inv,location L
where sku.client_id = 'MOUNTAIN'
and sku.product_group<>'STATIONERY'
and sku.sku_id = inv.sku_id
and inv.client_id = 'MOUNTAIN'
and inv.location_id=L.location_id
and inv.site_id=L.site_id
--and inv.location_id not like 'MARSHAL'
--and inv.zone_1 not like 'Z3OUT'
--and inv.location_id between upper ('&&2') and upper ('&&3')
--and inv.location_id<>'SUSPENSE'
order by B, C2)
/

--spool off
