SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON

select 'Code,Client ID,Sku,Descr.,Condition,From Loc.,Original Qty,Order ID,To Loc.,Date' from dual
union all
select A ||','|| B ||','|| C ||','|| D ||','|| E ||','|| F ||','|| G ||','|| H ||','|| I ||','|| J from (
select it.code                      as "A"
, it.client_id                      as "B"
, it.sku_id                         as "C"
, sku.description                   as "D"
, it.condition_id                   as "E"
, it.from_loc_id                    as "F"
, it.original_qty                   as "G"
, it.reference_id                   as "H"
, it.to_loc_id                      as "I"
, to_char(it.dstamp,'DD-MON-YYYY')  as "J" 
from inventory_transaction it
left join sku on sku.sku_id = it.sku_id
where it.client_id = 'SS'
and it.dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')
and it.code like ('Pick')
and regexp_substr(it.Sku_id, '0|1|2|3|4|5|6|7|8|9',1,1) = 4
and it.sku_id like ('M%')
)
/