SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','
SET WRAP OFF


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON



select 'Work Group, SKU, Deallocated, Picked, Time Diff' from Dual
union all
select work_group || ',' ||  sku_id  || ',' ||  deallocated  || ',' ||  picked  || ',' ||  timediff
from (
WITH parameters as
(
  select
  TRUNC(TO_DATE('&&2', 'DD-MON-YYYY')) fromDate,
  TRUNC(TO_DATE('&&3', 'DD-MON-YYYY')) toDate  
  from DUAL
),
deallocations as
(
  SELECT
  CODE,
  WORK_GROUP,
  SKU_ID,
  DSTAMP
  FROM parameters, INVENTORY_TRANSACTION ITL
  WHERE
  DSTAMP BETWEEN parameters.fromDate AND parameters.toDate
  AND CODE IN ('Deallocate')
  AND CLIENT_ID = 'MOUNTAIN'
),
picks as
(
  SELECT
  CODE,
  WORK_GROUP,
  SKU_ID,
  DSTAMP
  FROM parameters, INVENTORY_TRANSACTION ITL
  WHERE
  DSTAMP BETWEEN parameters.fromDate AND parameters.toDate
  AND CODE IN ('Pick')
  AND CLIENT_ID = 'MOUNTAIN'
)
SELECT
deallocations.WORK_GROUP,
deallocations.SKU_ID,
deallocations.DSTAMP as DEALLOCATED,
picks.dstamp as PICKED,
TRUNC(picks.dstamp) - TRUNC(deallocations.DSTAMP) as TIMEDIFF
FROM deallocations
INNER JOIN picks ON deallocations.work_group = picks.work_group AND deallocations.sku_id = picks.sku_id
)
/