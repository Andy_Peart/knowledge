SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

select
'ISM,E,TR' ||','||
JA ||','||
',,,,GOOD,' ||','||
JX ||','||
nvl(JB,0) ||','||
nvl(HB,0) ||','||
nvl(GB,0) ||','||
nvl(JC,0) ||','||
nvl(HC,0) ||','||
nvl(GC,0) ||','||
nvl(JB-JC,0) ||','||
nvl(HB-HC,0) ||','||
nvl(GB-GC,0) ||','||
',+,,' ||','||
sku.description ||','||
'EUROPE/LONDON'
from
(select
i.sku_id JA,
count(*) JX,
sum(i.qty_on_hand) JB,
sum(i.qty_allocated) JC
from inventory i
where i.client_id='TR'
and i.sku_id not like '**%'
group by
i.sku_id),
(select
i.sku_id HA,
sum(i.qty_on_hand) HB,
sum(i.qty_allocated) HC
from inventory i
where i.client_id='TR'
and i.sku_id not like '**%'
and i.Lock_status='UnLocked'
group by
i.sku_id),
(select
i.sku_id GA,
sum(i.qty_on_hand) GB,
sum(i.qty_allocated) GC
from inventory i
where i.client_id='TR'
and i.sku_id not like '**%'
and i.Lock_status='Locked'
group by
i.sku_id),sku
where sku.client_id='TR'
and JA=HA (+)
and JA=GA (+)
and JA=sku.sku_id
order by JA
/
