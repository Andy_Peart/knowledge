set feedback off
set pagesize 66
set linesize 110
set verify off
set newpage 0


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

 

column A heading 'SKU code' format A20
column B heading 'Description' format A40
column C heading 'Location' format A12
column D heading 'Qty on Hand' format 999999
column M format a4 noprint


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

set und '-'
set heading on
set newpage 0

ttitle  LEFT 'SKUs in more than 3 Locations for client ID &&2 (Style &&3) as at ' report_date RIGHT 'Page:' FORMAT 999 SQL.PNO skip 2

break on A on B skip 1

select 
i.sku_id A,
sku.description B,
i.location_id C,
sum(i.qty_on_hand) D
from inventory i,sku,(select sku_id as SK from
(select sku_id,count(*) as CT from
(select sku_id,location_id,sum(qty_on_hand)
from inventory
where client_id='&&2'
and location_id not in ('CONTAINER','VDESP','HOLD','IN','IN2','IN3','IN4','IN5','PACKM','PACKSB')
group by sku_id,location_id
order by sku_id,location_id)
group by sku_id)
where CT>3)
where i.sku_id=SK
and i.sku_id like '&&3%'
and i.sku_id=sku.sku_id
and i.client_id='&&2'
and sku.client_id='&&2'
and i.location_id not in ('CONTAINER','VDESP','HOLD','IN','IN2','IN3','IN4','IN5','PACKM','PACKSB')
group by i.sku_id,sku.description,i.location_id
order by i.sku_id,i.location_id
/
