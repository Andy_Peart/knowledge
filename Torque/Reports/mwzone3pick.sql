SET FEEDBACK OFF                 
set verify off
set wrap off
set colsep ','

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 132
set trimspool on

column A heading 'User ID' format a18
column B heading 'Task Type' format a18
column XX heading 'WWW' format a12
column C heading 'Tasks' format 99999999
column D heading 'Units' format 99999999
column Z noprint


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

--break on report 
break on A dup 

--spool mwzone3pick.csv

select 'Total Tasks by ZONE from &&2 to &&3 - for client MOUNTAIN' from DUAL;

select 'User,Zone,Lines,Time Taken,Total Time,Percentage' from DUAL;

select
A||','||
R||','||
G||','||
replace(to_char(F/60,'9999'),' ','')||','||
replace(to_char(F2/60,'9999'),' ','')||','||
replace(to_char(F*100/F2,'999.99'),' ','')
from (select
L.work_zone R,
it.user_id A,
sum(it.elapsed_time) F,
count(*) G
from inventory_transaction it,Location L
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='MOUNTAIN'
and it.update_qty<>0
and it.reference_id like 'MOR%'
and it.station_id not like 'Auto%'
and it.code='Pick'
and it.elapsed_time>0
and it.site_id=L.site_id
and it.from_loc_id=L.location_id
group by it.user_id,L.work_zone),(select
it.user_id A2,
sum(it.elapsed_time) F2
from inventory_transaction it
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='MOUNTAIN'
and it.update_qty<>0
and it.reference_id like 'MOR%'
and it.station_id not like 'Auto%'
and it.code='Pick'
and it.elapsed_time>0
group by it.user_id)
where A=A2
order by A,R
/



--spool off
