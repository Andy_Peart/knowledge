SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9999
SET TRIMSPOOL ON

column A heading 'Ship Dock' format A16
column B heading 'Country' format A5
column C heading 'Orders' format 999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


--spool mwpackinv.csv

select 'Date range: &&2 - &&3' from dual;

select 'Ship Dock, Country, Orders' from dual;

select ship_dock, cntry, count(*) orders
from (
	select order_id, final_loc_id ship_dock, cntry, count(*) 
	from (
		select oh.order_id, oh.status, 
    case when loc.work_zone like '%HANG' then 'Hang' else 'Non-Hang' end work_zone,
    it.final_loc_id, count(*) cnt,
		case 
		  when oh.country = 'GBR' then 'UK'
		  else 'OCS'
		end cntry
		from order_header oh inner join inventory_transaction it on oh.client_id = it.client_id and oh.order_id = it.reference_id
		inner join location loc on loc.location_id = it.from_loc_id and it.site_id = loc.site_id
		WHERE  it.CLIENT_ID in ('TR','T')
			and loc.site_id = 'WFD'
			and oh.status = 'Shipped'
			and oh.order_id like 'H%'
			and oh.SHIPPED_DATE between to_date('&&2') and to_date('&&3')+1
			and it.code = 'Allocate'
		group by oh.order_id, oh.status, case when loc.work_zone like '%HANG' then 'Hang' else 'Non-Hang' end, it.final_loc_id,
		case 
		  when oh.country = 'GBR' then 'UK'
		  else 'OCS'
		end 
	)
	group by order_id, final_loc_id, cntry
	having count(*)>1
) 
group by ship_dock, cntry
/

--spool off
