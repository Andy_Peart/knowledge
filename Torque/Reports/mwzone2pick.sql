SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 2500
SET TRIMSPOOL ON

--break on report 
--break on A dup 

--spool mwzone2pick.csv

break on user1 skip 1;
compute sum label 'Totals' of count units on user1;

select 'User,Date,Zone,Tasks,Units,Hrs,Mins,KPI' from DUAL;

select user1,
dstamp,
workzone,
count,
units,
--F||','||
hrs,
mins,
kpi
from (
select
dstamp,
workzone,
user1,
count,
units,
--F||','||
hrs,
replace(to_char(mins-(hrs*60),'99'),' ','') mins,
kpi
from (select
workzone,
user1,
count,
units,
trunc(time/60) time,
replace(to_char(trunc(time/3600),'9999'),' ','') hrs,
replace(to_char(trunc(time/60),'9999'),' ','') mins,
replace(to_char((units/time)*3600,'999999'),' ','') kpi,
(units/time)*3600 l,
dstamp
from (select
L.work_zone workzone,
it.user_id user1,
count(*) count,
sum(it.update_qty) units,
sum(it.elapsed_time) time,
trunc(it.dstamp) dstamp
from inventory_transaction it,Location L
where it.Dstamp between to_date('&&2 &&4', 'DD-MON-YYYY HH24:MI:SS') and to_date('&&3 &&5', 'DD-MON-YYYY HH24:MI:SS')
and it.client_id='MOUNTAIN'
and it.update_qty<>0
and (it.reference_id like 'MOR%' or it.reference_id = 'CONSOL')
and it.station_id not like 'Auto%'
and it.code in ('Pick','Consol')
and it.elapsed_time>0
and it.site_id=L.site_id
and it.from_loc_id=L.location_id
group by it.user_id,L.work_zone,trunc(it.dstamp)))
order by user1,dstamp,workzone DESC
)
/

--spool off
