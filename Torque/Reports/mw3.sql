/******************************************************************************/
/*                                                                            */
/*                            Elite                                           */
/*                                                                            */
/*     FILE NAME  :   mwshortages3.sql                                        */
/*                                                                            */
/*   DATE     BY   PROJ     ID         DESCRIPTION                            */
/*   ======== ==== ======   ========   =============                          */
/*   22/01/06 BK   Mountain            Units Picked by Date Range             */
/******************************************************************************/

/* Input Parameters are as follows */
-- 1)  From Date
-- 2)  To Date


set feedback off
set pagesize 68
set linesize 240
set verify off

clear columns
clear breaks
clear computes


column AA heading 'Order Type' format A12
column A heading 'User ID' format A16
column X heading 'Zone' format A6
column B heading 'Units' format 999999
column C heading 'Picks' format 999999
column D heading 'Orders' format 999999
column E heading 'Ave Units per Order' format 99999.99


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'Units Picked from '&&2' to '&&3' - for client ID MOUNTAIN as at ' report_date skip 2

break on AA dup skip 1 
--compute sum label 'Total' of B C D E on AA
--compute sum label 'Full Total' of B C D E on report


select
nvl(oh.order_type,'  ') AA,
it.user_id A,
'_All' X,
sum(it.update_qty) B,
count(*) C,
count(distinct reference_id) D,
sum(it.update_qty)/count(distinct reference_id) E
from inventory_transaction it,order_header oh
where it.client_id='MOUNTAIN'
and it.code='Pick'
and to_loc_id='DESPATCH'
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.reference_id=oh.order_id
and oh.client_id='MOUNTAIN'
--and oh.order_type='WEB'
group by 
oh.order_type,
it.user_id
union
select
nvl(oh.order_type,'  ') AA,
it.user_id A,
ll.zone_1 X,
sum(it.update_qty) B,
count(*) C,
count(distinct reference_id) D,
sum(it.update_qty)/count(distinct reference_id) E
from inventory_transaction it,order_header oh,location ll
where it.client_id='MOUNTAIN'
and it.code='Pick'
and to_loc_id='DESPATCH'
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.reference_id=oh.order_id
and oh.client_id='MOUNTAIN'
and it.from_loc_id=ll.location_id
--and oh.order_type='WEB'
group by
oh.order_type,
it.user_id,
ll.zone_1
union
select
nvl(oh.order_type,'  ') AA,
'____Totals' A,
' ' X,
sum(it.update_qty) B,
count(*) C,
count(distinct reference_id) D,
sum(it.update_qty)/count(distinct reference_id) E
from inventory_transaction it,order_header oh
where it.client_id='MOUNTAIN'
and it.code='Pick'
and to_loc_id='DESPATCH'
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.reference_id=oh.order_id
and oh.client_id='MOUNTAIN'
--and oh.order_type='WEB'
group by
oh.order_type
order by 
AA,A
/

