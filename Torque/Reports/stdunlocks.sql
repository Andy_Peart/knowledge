SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Time Locked,User Locked,Sku,Location Locked,-,Time Unlocked,User Unlocked,Amount Unlocked,Unlock Code' from dual
union all
select "1Time" || ',' || "1User" || ',' || "Sku" || ',' || "Loc" || ',' || "x" || ',' || "2Time" || ',' || "2User" || ',' || "Qty" || ',' || "Code"
from
(
with t1 as (
select user_id              as "User"
, sku_id                    as "Sku"
, from_loc_id               as "Loc"
, to_char(dstamp,'HH24:MI') as "Time"
, client_id                 as "Client"
from inventory_transaction
where client_id = '&&2'
and code = 'Pick'
and update_qty = 0
and dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
group by user_id
, sku_id
, from_loc_id
, to_char(dstamp,'HH24:MI')
, client_id
), t2 as (
select user_id              as "User"
, sku_id                    as "Sku"
, from_loc_id               as "Loc"
, to_char(dstamp,'HH24:MI') as "Time"
, client_id                 as "Client"
, sum(update_qty)           as "Qty"
, extra_notes               as "Code"
from inventory_transaction
where client_id = '&&2'
and code = 'Inv UnLock'
and dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
group by user_id
, sku_id
, from_loc_id
, to_char(dstamp,'HH24:MI')
, client_id
, extra_notes)
select t1."Time"    as "1Time"
, t1."User"         as "1User"
, t1."Sku"          as "Sku"
, t1."Loc"          as "Loc"
, 'x'               as "x"
, t2."Time"         as "2Time"
, t2."User"         as "2User"
, t2."Qty"          as "Qty"
, t2."Code"         as "Code"
from t1
inner join t2 on t2."Client" = t1."Client" and t2."Sku" = t1."Sku" and t2."Loc" = t1."Loc"
where t1."Time" < t2."Time"
order by 1
)
/
--spool off