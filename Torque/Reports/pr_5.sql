SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ' '



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 64
SET NEWPAGE 0
SET LINESIZE 100


column A heading 'SKU' format A16
column AA heading 'Description' format A42
column B1 heading 'CIMS Code' format A20
column B4 heading 'Size' format A10
column C heading 'Client' format A8

TTitle 'Prominent - Breakdown of QC Call Off for Order &&2' skip 2

--select 'SKU,Description,CIMS Code,Size,Qty,,' from DUAL;

--break on report

--compute sum label 'Total' of C on report


select
pl.sku_id A,
sku.description AA,
sku.user_def_type_2||+'-'||+sku.user_def_type_1||+'-'||+sku.user_def_type_4 B1,
sku.sku_size B4,
oh.ship_dock C
from order_header oh,pre_advice_line pl,sku
where oh.client_id='PR'
and oh.order_id='&&2' 
and pl.client_id=oh.ship_dock
and pl.pre_advice_id='&&2'
and sku.client_id='TR'
and pl.sku_id=sku.sku_id
order by
sku.user_def_type_4,
B1,
A
/
