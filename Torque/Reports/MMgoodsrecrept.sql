SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET WRAP Off


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 66
SET NEWPAGE 0
SET LINESIZE 132
SET HEADING OFF

 
column A heading 'Date' format A12
column B heading 'Supplier' format A20
column D heading 'GRN' format 9999
column E heading 'Pre Advice' format A10
column F heading 'Line' format 99999
column G heading 'SKU Code' format A18
column H heading 'Description' format A32
column I heading 'Order' format 99999
column J heading 'Recvd|Adjst' format 9999999
column K heading ' File| Diff' format 99999
column M format a4 noprint

ttitle left 'Goods Received Report - for client ID &&4 and period &&2 to &&3 (All Pre Advices or Blind)' RIGHT 'Page: ' FORMAT 999 SQL.PNO skip 2
 

SET PAGESIZE 66
SET HEADING ON

--BREAK ON B on D on E on REPORT
--column B new_value null
--column D new_value null
--column E new_value null

--BREAK ON REPORT
--compute sum label 'Totals' of I J K on report

BREAK ON A skip 3
compute sum label 'Daily Total' of J on A


select
to_char(it.Dstamp, 'YYMM') M,
to_char(it.Dstamp, 'DD/MM/YY') A,
nvl(ad.name,'NOT KNOWN') B,
it.grn D,
it.reference_id E,
it.line_id F,
it.sku_id G,
sku.description H,
nvl(ph.qty_due,0) I,
sum(it.update_qty) J,
nvl(ph.qty_due-ph.qty_received,0) K
from inventory_transaction it,pre_advice_line ph,sku,address ad
where it.client_id='&&4'
and it.station_id not like 'Auto%'
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.supplier_id=ad.address_id (+)
and it.reference_id=ph.pre_advice_id (+)
and it.line_id=ph.line_id (+)
and it.code like 'Receipt%'
--and it.code in ('Receipt','Adjustment')
and it.sku_id=sku.sku_id (+)
group by
to_char(it.Dstamp, 'YYMM'),
to_char(it.Dstamp, 'DD/MM/YY'),it.supplier_id,ad.name,it.grn,it.reference_id,it.sku_id,it.line_id,sku.description,ph.qty_due,ph.qty_due-ph.qty_received
order by
to_char(it.Dstamp, 'YYMM'),
to_char(it.Dstamp, 'DD/MM/YY'),it.reference_id,it.line_id
/
