SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 700
SET TRIMSPOOL ON

column Z heading 'Date' format a12
column A heading 'User ID' format a14
column B heading 'Name' format a30
column C heading 'Picks' format 999999
column D heading 'Receipts' format 999999
column E heading 'Relocates' format 999999
column F heading 'Returns' format 999999
column G heading 'IDTs' format 999999
column H heading 'Relocates' format 999999
column I heading 'Returns' format 999999
column J heading 'IDTs' format 999999
column K heading 'IDTs' format 999999
column L heading 'IDTs' format 999999
column M heading 'IDTs' format 999999
column N heading 'IDTs' format 999999
column P heading 'IDTs' format 999999
column Q heading 'IDTs' format 999999
column R heading 'IDTs' format 999999
column S heading 'IDTs' format 999999
column T heading 'IDTs' format 999999
column U heading 'IDTs' format 999999
column V heading 'IDTs' format 999999
column W heading 'IDTs' format 999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(sysdate-6, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


break on report

compute sum label 'Totals' of C D E F G H I J K L M N R S T U V W on report

--spool newprod.csv


select 'Date,User ID,Name,Retail Boxed Pick Tasks,Retail Boxed Pick Units,Retail Hang Tasks,Retail Hang Units,Retail Receipted Units,Retail Receipted Total Bins,Retail Transfers Received from HS,Retail Return Units,Retail Relocate Tasks,Retail Relocate Units,Home Shopping Pick Tasks,Home Shopping Pick Units,Home Shopping Receipted Units,Home Shopping Receipted Bins,Home Shopping Transfers Received from Retail,Home Shopping Relocate Tasks,Home Shopping Relocate Units,Home Shopping Re-Packs,' from DUAL;
---------Z-----A------B------C----------------------D-----------------------E-----------------F----------------G-----------------------H--------------------------I-----------------------------------J-------------------K--------------------L----------------------M----------------------N--------------------------R----------------------------S----------------------------T--------------------------------------------U----------------------------V----------------------------W--------


select
--trunc(sysdate) Z,
to_char(sysdate, 'DD/MM/YYYY') Z,
user_id A,
name B,
nvl(JA,0) C,
nvl(JB,0) D,
nvl(JC,0) E,
nvl(JD,0) F,
nvl(JF,0) G,
nvl(JE,0) H,
nvl(JH,0) I,
nvl(JJ,0) J,
nvl(JK,0) K,
nvl(JL,0) L,
nvl(J71,0) M,
nvl(J72,0) N,
nvl(J92,0) R,
nvl(J91,0) S,
nvl(J102,0) T,
nvl(J111,0) U,
nvl(J112,0) V,
nvl(J122,0) W
from application_user,
(select
user_id J1,
count(*) JA,
sum(update_qty) JB
from inventory_transaction
--where Dstamp>sysdate-6-1
where trunc(Dstamp)=trunc(sysdate)
and client_id='TR'
and update_qty<>0
and code='Pick' 
and (to_loc_id='CONTAINER' or (to_loc_id<>'CONTAINER' and from_loc_id<>'CONTAINER'))
and from_loc_id not like 'HANG%'
group by user_id),
(select
user_id J2,
count(*) JC,
sum(update_qty) JD
from inventory_transaction
where trunc(Dstamp)=trunc(sysdate)
and client_id='TR'
and update_qty<>0
and code='Pick' 
and (to_loc_id='CONTAINER' or (to_loc_id<>'CONTAINER' and from_loc_id<>'CONTAINER'))
and from_loc_id like 'HANG%'
group by user_id),
(select
user_id J3,
count(*) JE,
sum(update_qty) JF
from inventory_transaction
where trunc(Dstamp)=trunc(sysdate)
and client_id='TR'
and update_qty<>0
and code='Receipt' 
and nvl(reference_id,'  ') not like 'F%'
group by user_id),
(select
user_id J4,
count(*) JG,
sum(update_qty) JH
from inventory_transaction
where trunc(Dstamp)=trunc(sysdate)
and client_id='TR'
and update_qty<>0
and code='Receipt'
--and reference_id='X2' 
and reference_id like 'F%'
group by user_id),
(select
user_id J5,
count(*) JI,
sum(update_qty) JJ
from inventory_transaction
where trunc(Dstamp)=trunc(sysdate)
and client_id='TR'
and reason_id not in ('T760','RECRV','DAMGD')
and update_qty<>0
and code='Adjustment' 
group by user_id),
(select
user_id J6,
count(*) JK,
sum(update_qty) JL
from inventory_transaction
where trunc(Dstamp)=trunc(sysdate)
and client_id='TR'
and update_qty<>0
and code='Relocate' 
group by user_id),
(select
user_id J7,
count(*) J71,
sum(update_qty) J72
from inventory_transaction
where trunc(Dstamp)=trunc(sysdate)
and client_id='T'
and update_qty<>0
and code='Pick' 
and (to_loc_id='CONTAINER' or (to_loc_id<>'CONTAINER' and from_loc_id<>'CONTAINER'))
and from_loc_id<>'MARSHALB' 
and to_loc_id<>'BR04'
group by user_id),
(select
user_id J9,
count(*) J91,
sum(update_qty) J92
from inventory_transaction
where trunc(Dstamp)=trunc(sysdate)
and client_id='T'
and update_qty<>0
and code='Receipt'
--and reference_id<>'X2'
--and reference_id is not null
and nvl(reference_id,'  ') not like 'F%'
group by user_id),
(select
user_id J10,
count(*) J101,
sum(update_qty) J102
from inventory_transaction
where trunc(Dstamp)=trunc(sysdate)
and client_id='T'
and update_qty<>0
and code='Receipt'
--and reference_id='X2' 
and reference_id like 'F%'
group by user_id),
(select
user_id J11,
count(*) J111,
sum(update_qty) J112
from inventory_transaction
where trunc(Dstamp)=trunc(sysdate)
and client_id='T'
and update_qty<>0
and code='Relocate'
group by user_id),
(select
user_id J12,
count(*) J121,
sum(update_qty) J122
from inventory_transaction
where trunc(Dstamp)=trunc(sysdate)
and client_id='T'
and update_qty>0
and code='Adjustment'
and reason_id='T760'
--and update_qty<>0
--and code='Receipt'
--and reference_id is null
group by user_id)
where user_id=J1 (+)
and user_id=J2 (+)
and user_id=J3 (+)
and user_id=J4 (+)
and user_id=J5 (+)
and user_id=J6 (+)
and user_id=J7 (+)
and user_id=J9 (+)
and user_id=J10 (+)
and user_id=J11 (+)
and user_id=J12 (+)
and nvl(JA,0)+nvl(JC,0)+nvl(JE,0)+nvl(JG,0)+nvl(JI,0)+nvl(JK,0)+nvl(J71,0)+nvl(J91,0)+nvl(J102,0)+nvl(J111,0)+nvl(J122,0)>0
order by user_id
/

--spool off
