SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 2500
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Order #,Customer Name,Fedex Sku,Qty Shipped,Weight,Currency,Sku value,Commodity Code' from dual
union all
select "Order #" ||','|| "Customer Name" || ',' || "Fedex Sku" || ',' || "Qty Shipped" || ',' || "Weight" || ',' || "Currency" || ',' || "Sku value" || ',' || "CC" from (
select oh.order_id  as "Order #"
, oh.name           as "Customer Name"
, substr(ol.sku_id,1,7) as "Fedex Sku"
, sum(nvl(ol.qty_picked,0))   as "Qty Shipped"
, sum(nvl(ol.qty_picked,0))*0.03 || 'kg' as "Weight"
, ol.product_currency as "Currency"
, sum(ol.line_value) * sum(nvl(ol.qty_picked,0)) as "Sku value"
, s.commodity_code as "CC"
from order_header oh
inner join order_line ol on ol.client_id = oh.client_id and ol.order_id = oh.order_id
inner join sku s on s.client_id = oh.client_id and s.sku_id = ol.sku_id
where oh.client_id = 'SE'
and oh.order_id = '&&2'
group by oh.order_id
, substr(ol.sku_id,1,7)
, oh.name
, ol.product_currency
, s.commodity_code)
;
select '--TOTALS--------------------------------------------------------------' from dual
;
select oh.order_id  as "Ord"
, null
, null
, sum(nvl(ol.qty_picked,0))   as "Qty"
, sum(nvl(ol.qty_picked,0))*0.03 || 'kg' as "Weight"
, ol.product_currency as "Curr"
, 	(
		select sum("Sku value") from (
		select oh.order_id  as "Ord"
		, oh.name           as "Name"
		, substr(ol.sku_id,1,7) as "Sku"
		, sum(nvl(ol.qty_picked,0))   as "Qty"
		, sum(nvl(ol.qty_picked,0))*0.03 || 'kg' as "Weight"
		, ol.product_currency as "Curr"
		, sum(ol.line_value) * sum(nvl(ol.qty_picked,0)) as "Sku value"
		from order_header oh
		inner join order_line ol on ol.client_id = oh.client_id and ol.order_id = oh.order_id
		where oh.client_id = 'SE'
		and oh.order_id = '&&2'
		group by oh.order_id
		, substr(ol.sku_id,1,7)
		, oh.name
		, ol.product_currency
		)
	)
from order_header oh
inner join order_line ol on ol.client_id = oh.client_id and ol.order_id = oh.order_id
where oh.client_id = 'SE'
and oh.order_id = '&&2'
group by oh.order_id
, ol.product_currency
/
--spool off