SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 90

column A heading 'User ID' format a24
column B heading 'Name' format a30
column C heading 'Picks' format 999999
column D heading 'Receipts' format 999999
column E heading 'Relocates' format 999999
column F heading 'Returns' format 999999
column G heading 'IDTs' format 999999
column HH heading 'Hour' format a4

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


--break on report

--compute sum label 'Totals' of C D E F G on report

spool picksbyhour.csv

select 'Picks by User ID by the Hour from &&2 to &&3 - for client  &&4' from DUAL;

select 'Log on,Hour,Units Picked' from DUAL;


select
au.name A,
to_char(Dstamp,'HH24') HH,
sum(update_qty) C
from inventory_transaction it,application_user au
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='&&4'
and it.update_qty<>0
and ((it.code='Pick' and it.to_loc_id='CONTAINER')
or (it.code='Pick' and it.to_loc_id<>'CONTAINER' and it.from_loc_id<>'CONTAINER'))
and it.user_id=au.user_id
group by 
au.name,
to_char(it.Dstamp,'HH24')
order by A,HH
/

spool off
