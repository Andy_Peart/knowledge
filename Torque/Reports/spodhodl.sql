SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON

select 'Order Header and Lines Data Extract - for client ID SP from &&2 to &&3 - as at  ' 
    || to_char(sysdate,'dd/mm/yy  hh24:mi:ss') from dual;

with T1 as
(
	select order_id, count(*) containers
	from (
		select order_id, container_id, count(*)
		from shipping_manifest
		where client_id = 'SP'
		and picked_dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+5
		group by order_id, container_id
		order by 1,2
	)
	group by order_id
	union all
	select order_id, count(*) containers
	from (
		select order_id, container_id, count(*)
		from shipping_manifest_archive
		where client_id = 'SP'
		and picked_dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+5
		group by order_id, container_id
		order by 1,2
	)
	group by order_id
),
tab1 as (
    select order_id, 
    case 
        when typeind = 2 then 'Mixed'
        else
            case 
                when sumisfin = 1 then 'Alteration' 
                else 'Normal' 
            end
    end
    type_of_order
    from (
        select order_id, sum(isfinished) sumisfin, count(*) typeind
        from (
            select distinct order_id, isfinished
            from (
                select  oh.order_id, case when upper(ol.user_def_note_2) like '%FINISHED%' then 1 else 0 end isfinished
                from order_header oh inner join order_line ol on oh.order_id = ol.order_id and oh.client_id = ol.client_id
                where oh.client_id = 'SP'
                and oh.creation_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
            )
        )
        group by order_id
    )
),
T_arch as (
	select oh.creation_date, oh.client_id
	||','|| oh.work_group
	||','|| oh.consignment
	||','|| to_char(oh.creation_date, 'DD/MM/YYYY')
	||','|| to_char(oh.creation_date, 'HH24:MI')
	||','|| to_char(oh.last_update_date, 'DD/MM/YYYY')  
	||','|| to_char(oh.last_update_date, 'HH24:MI') 
	||','|| to_char(oh.shipped_date, 'DD/MM/YYYY')  
	||','|| to_char(oh.shipped_date, 'HH24:MI') 
	||','|| case  when oh.client_id = 'SS' and oh.order_type = 'RETAIL' then oh.postcode 
				  when oh.client_id = 'PR' then oh.customer_id || oh.name
				  else oh.customer_id end 
	||','|| oh.order_id
	||','|| oh.order_type
	||','|| oh.country
	||','|| case when oh.client_id = 'GG' then nvl(oh.country,'GBR') else oh.dispatch_method end
	||','|| to_char(oh.priority)
	||','|| oh.ship_dock
	||','|| oh.status
	||','|| to_char(1) 
	||','|| to_char(count(ol.line_id)) 
	||','|| to_char(sum(ol.qty_ordered))
	||','|| to_char(sum(nvl(ol.qty_shipped,0)))
	||','|| 
	case 
	  when oh.status = 'Shipped' then to_char(sum(nvl(ol.qty_ordered,0)-nvl(ol.qty_shipped,0)))
	  else '0' 
	end 
	||','|| to_char(sum(nvl(ol.qty_tasked,0))) 
	||','|| nvl(T1.containers,0)
	||','|| nvl(c.tandata_id,'GBR')
	||','||case when oh.client_id = 'GG' 
				then oh.user_def_type_1||','||
				oh.user_def_type_2||','||
				oh.user_def_type_3||','||
				oh.user_def_type_4||','||
				oh.user_def_type_5||','||
				oh.user_def_type_6||','||
				oh.user_def_type_7||','||
				oh.user_def_type_8
				else '' end
	||','|| case  when oh.consignment_grouping_id like '%TROLLEY%' then 'TROLLEY' 
				  when oh.consignment_grouping_id like '%SINGLE%' then 'Single' 
				  when oh.consignment_grouping_id like '%TLY%' then 'TROLLEY' 
				  when oh.consignment like 'CL' || oh.client_id || '%' then 'Single' end
	||','|| to_char(oh.creation_date, 'HH24')
	||','|| to_char(oh.shipped_date, 'HH24') 
    ||','|| tab1.type_of_order
    AS ARCH_COLUMNS
	from order_header_archive oh inner join order_line_archive ol on oh.order_id=ol.order_id and oh.client_id=ol.client_id
	left join country c on nvl(c.iso3_id, c.iso2_id) = oh.country
	left join T1 on oh.order_id = T1.order_id
    left join tab1 on tab1.order_id = oh.order_id
	where oh.client_id = 'SP'
	and oh.creation_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
	group by oh.client_id
	, oh.work_group
	, oh.consignment
	, to_char(oh.creation_date, 'DD/MM/YYYY')
	, to_char(oh.creation_date, 'HH24:MI')
	, to_char(oh.last_update_date, 'DD/MM/YYYY')  
	, to_char(oh.last_update_date, 'HH24:MI') 
	, to_char(oh.shipped_date, 'DD/MM/YYYY')  
	, to_char(oh.shipped_date, 'HH24:MI') 
	, case  when oh.client_id = 'SS' and oh.order_type = 'RETAIL' then oh.postcode 
				  when oh.client_id = 'PR' then oh.customer_id || oh.name
				  else oh.customer_id end 
	, oh.order_id
	, oh.order_type
	, oh.country
	, oh.dispatch_method
	, to_char(oh.priority)
	, oh.ship_dock
	, oh.status
	, to_char(1)
	, T1.containers
	, oh.creation_date
	, c.tandata_id
	, case when oh.client_id = 'GG' 
				then oh.user_def_type_1||','||
				oh.user_def_type_2||','||
				oh.user_def_type_3||','||
				oh.user_def_type_4||','||
				oh.user_def_type_5||','||
				oh.user_def_type_6||','||
				oh.user_def_type_7||','||
				oh.user_def_type_8
				else '' end
	,oh.consignment_grouping_id
	, to_char(oh.shipped_date, 'HH24')
	, to_char(oh.creation_date, 'HH24')
    , tab1.type_of_order
	order by oh.creation_date
),
T_current as (
	select oh.creation_date, oh.client_id
	||','|| oh.work_group
	||','|| oh.consignment
	||','|| to_char(oh.creation_date, 'DD/MM/YYYY')
	||','|| to_char(oh.creation_date, 'HH24:MI')
	||','|| to_char(oh.last_update_date, 'DD/MM/YYYY')  
	||','|| to_char(oh.last_update_date, 'HH24:MI') 
	||','|| to_char(oh.shipped_date, 'DD/MM/YYYY')  
	||','|| to_char(oh.shipped_date, 'HH24:MI') 
	||','|| case  when oh.client_id = 'SS' and oh.order_type = 'RETAIL' then oh.postcode 
				  when oh.client_id = 'PR' then oh.customer_id || oh.name
				  else oh.customer_id end 
	||','|| oh.order_id
	||','|| oh.order_type
	||','|| oh.country
	||','|| case when oh.client_id = 'GG' then nvl(oh.country,'GBR') else oh.dispatch_method end
	||','|| to_char(oh.priority)
	||','|| oh.ship_dock
	||','|| oh.status
	||','|| to_char(1) 
	||','|| to_char(count(ol.line_id)) 
	||','|| to_char(sum(ol.qty_ordered))
	||','|| to_char(sum(nvl(ol.qty_shipped,0)))
	||','|| 
	case 
	  when oh.status = 'Shipped' then to_char(sum(nvl(ol.qty_ordered,0)-nvl(ol.qty_shipped,0)))
	  else '0' 
	end 
	||','|| to_char(sum(nvl(ol.qty_tasked,0))) 
	||','|| nvl(T1.containers,0)
	||','|| nvl(oh.signatory,'N/A')
	||','|| case when oh.client_id = 'GG' 
				then oh.user_def_type_1||','||
				oh.user_def_type_2||','||
				oh.user_def_type_3||','||
				oh.user_def_type_4||','||
				oh.user_def_type_5||','||
				oh.user_def_type_6||','||
				oh.user_def_type_7||','||
				oh.user_def_type_8
				else '' end
	||','|| case  when oh.consignment_grouping_id like '%TROLLEY%' then 'TROLLEY' 
				  when oh.consignment_grouping_id like '%SINGLE%' then 'Single' 
				  when oh.consignment_grouping_id like '%TLY%' then 'TROLLEY' 
				  when oh.consignment like 'CL' || oh.client_id || '%' then 'Single' end
	||','|| to_char(oh.creation_date, 'HH24')
	||','|| to_char(oh.shipped_date, 'HH24')  
    ||','|| tab1.type_of_order
    AS CURRENT_COLUMNS
	from order_header oh inner join order_line ol on oh.order_id=ol.order_id and oh.client_id=ol.client_id
	left join T1 on oh.order_id = T1.order_id
	left join country c on nvl(c.iso3_id, c.iso2_id) = oh.country
    left join tab1 on tab1.order_id = oh.order_id
	where oh.client_id = 'SP'
	and oh.creation_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
	group by oh.client_id
	, oh.work_group
	, oh.consignment
	, to_char(oh.creation_date, 'DD/MM/YYYY')
	, to_char(oh.creation_date, 'HH24:MI')
	, to_char(oh.last_update_date, 'DD/MM/YYYY')  
	, to_char(oh.last_update_date, 'HH24:MI') 
	, to_char(oh.shipped_date, 'DD/MM/YYYY')  
	, to_char(oh.shipped_date, 'HH24:MI') 
	, case  when oh.client_id = 'SS' and oh.order_type = 'RETAIL' then oh.postcode 
				  when oh.client_id = 'PR' then oh.customer_id || oh.name
				  else oh.customer_id end 
	, oh.order_id
	, oh.order_type
	, oh.country
	, oh.dispatch_method
	, to_char(oh.priority)
	, oh.ship_dock
	, oh.status
	, to_char(1)
	, T1.containers
	, oh.creation_date
	, nvl(oh.signatory,'N/A')
	, case when oh.client_id = 'GG' 
				then oh.user_def_type_1||','||
				oh.user_def_type_2||','||
				oh.user_def_type_3||','||
				oh.user_def_type_4||','||
				oh.user_def_type_5||','||
				oh.user_def_type_6||','||
				oh.user_def_type_7||','||
				oh.user_def_type_8
				else '' end
	, oh.consignment_grouping_id
	, to_char(oh.shipped_date, 'HH24')
	, to_char(oh.creation_date, 'HH24')
    , tab1.type_of_order
	ORDER BY oh.creation_date
)
select 
'client, work group, consignment, creation date, creation time, last update date, last update time, shipped date, shipped time, customer id, order, order type, country, dispatch method, priority, ship dock, status, orders, lines, order qty, shipped qty, short, allocated qty, containers, signatory,' || case when 'SP' = 'GG' then ',User def 1,User def 2,User def 3,User def 4,User def 5,User def 6,User def 7,User def 8' else ''||',type,Creation Hour,Shipped Hour,Type of Order' end
from dual
union all
SELECT ARCH_COLUMNS FROM T_ARCH 
union all
SELECT CURRENT_COLUMNS FROM T_CURRENT
/
