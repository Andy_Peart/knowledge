SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 200
set trimspool on
set newpage 0
set verify off
set colsep ','

--spool mwdowntime.csv

select 'User Id,Date,Started,Down,Stopped,Down,Restarted,Down,Finished,Down,Total Downtime' from DUAL;

select
J1||','||
J2||','||
J3||','||
J4||','||
K3||','||
K4||','||
L3||','||
L4||','||
M3||','||
M4||','||
N99 
from (select
J1,
J2,
J3,
J4,
K3,
K4,
L3,
L4,
M3,
M4,
J4+K4+L4+M4 N99
from (select
J1,
J2,
J3,
CASE
WHEN J3<705 THEN 0
WHEN J3>=1000 THEN J3-705-120
WHEN J3>=900 THEN J3-705-80
WHEN J3>=800 THEN J3-705-40
ELSE J3-705 END J4,
K3,
CASE
WHEN K3>1100 THEN 0
ELSE 1100-K3-40 END K4,
L3,
CASE
WHEN L3<1130 THEN 0
WHEN L3>1200 THEN L3-1130-40
ELSE L3-1130 END L4,
M3,
CASE
WHEN M3<1200 THEN 1500-M3-160
WHEN M3<1300 THEN 1500-M3-120
WHEN M3<1400 THEN 1500-M3-80
WHEN M3<1500 THEN 1500-M3-40
ELSE 0 END M4
from (select
user_id J1,
trunc(Dstamp) J2,
min(to_number(to_char(Dstamp,'HH24MI'))) J3
from inventory_transaction
where Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and client_id='MOUNTAIN'
and update_qty<>0
and code='Pick'
and elapsed_time>0
and reference_id like 'MOR%'
group by
user_id,
trunc(Dstamp)),(select
user_id K1,
trunc(Dstamp) K2,
max(to_number(to_char(Dstamp,'HH24MI'))) K3
from inventory_transaction
where Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and client_id='MOUNTAIN'
and update_qty<>0
and code='Pick'
and elapsed_time>0
and reference_id like 'MOR%'
and to_number(to_char(Dstamp,'HH24MI'))<1130
group by
user_id,
trunc(Dstamp)),(select
user_id L1,
trunc(Dstamp) L2,
min(to_number(to_char(Dstamp,'HH24MI'))) L3
from inventory_transaction
where Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and client_id='MOUNTAIN'
and update_qty<>0
and code='Pick'
and elapsed_time>0
and reference_id like 'MOR%'
and to_number(to_char(Dstamp,'HH24MI'))>1129
group by
user_id,
trunc(Dstamp)),(select
user_id M1,
trunc(Dstamp) M2,
max(to_number(to_char(Dstamp,'HH24MI'))) M3
from inventory_transaction
where Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and client_id='MOUNTAIN'
and update_qty<>0
and code='Pick'
and elapsed_time>0
and reference_id like 'MOR%'
and to_number(to_char(Dstamp,'HH24MI'))<1501
group by
user_id,
trunc(Dstamp))
where J1=K1
and J2=K2
and J1=L1
and J2=L2
and J1=M1
and J2=M2
order by J1,J2))
/


--spool off
