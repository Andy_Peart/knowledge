SET FEEDBACK OFF             
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 300
SET TRIMSPOOL ON

--spool mwLPGpicks.csv

select 'Picks from &&2 to &&3' from DUAL;
select 'LPG,Units Picked,Overall %,Total Cartons' from DUAL;

select
LPG||','||
IQTY||','||
to_char(IQTY*100/TOTQ,'999.00')||','||
to_char(Q5,'99990')
from (select
LPG,
TOTQ,
sum(IQTY) IQTY,
sum(Q5) Q5
from (select
SK,
LPG,
TOTQ,
Q4,
IQTY,
IQTY/Q4 Q5
from (select
sum(it.update_qty) TOTQ
from inventory_transaction it
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='MOUNTAIN'
and it.code like 'Pick'
and elapsed_time>0),(select
it.sku_id ISKU,
sum(it.update_qty) IQTY
from inventory_transaction it
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='MOUNTAIN'
and it.code like 'Pick'
and elapsed_time>0
group by
it.sku_id),(select
SK,
SKD,
LPG,
Q3,
to_char(Q3/CT,'9990') Q4
from (select
SK,
SKD,
LPG,
count(*) CT,
sum(to_number(Q3)) Q3
from (select
PID,
LPG,
QQ,
QU,
CASE
WHEN QQ=0 THEN '0'
ELSE to_char(QU/QQ,'9990') END Q3
from (select
PID,
LPG,
sum(QU) QU,
sum(mo.cartons) QQ
from mountainorders mo,(select
ph.pre_advice_id  PID,
sku.user_def_type_8 LPG,
--pl.sku_id SK,
sum(pl.qty_due) QU
from pre_advice_header ph,pre_advice_line pl,sku
where ph.client_id='MOUNTAIN'
and ph.pre_advice_id=pl.pre_advice_id
and pl.client_id='MOUNTAIN'
and sku.client_id = 'MOUNTAIN'
and sku.sku_id=pl.sku_id
group by
ph.pre_advice_id,
sku.user_def_type_8
union
select
ph.pre_advice_id  PID,
sku.user_def_type_8 LPG,
--pl.sku_id SK,
sum(pl.qty_due) QU
from pre_advice_header_archive ph,pre_advice_line_archive pl,sku
where ph.client_id='MOUNTAIN'
and ph.pre_advice_id=pl.pre_advice_id
and pl.client_id='MOUNTAIN'
and sku.client_id = 'MOUNTAIN'
and sku.sku_id=pl.sku_id
group by
ph.pre_advice_id,
sku.user_def_type_8)
where mo.cw_order=PID
group by
PID,
LPG)),(select
pl.pre_advice_id  PID2,
pl.sku_id SK,
sku.description SKD
from pre_advice_line pl,sku
where pl.client_id='MOUNTAIN'
and sku.client_id='MOUNTAIN'
and pl.sku_id=sku.sku_id
union
select
pl.pre_advice_id  PID2,
pl.sku_id SK,
sku.description SKD
from pre_advice_line_archive pl,sku
where pl.client_id='MOUNTAIN'
and sku.client_id='MOUNTAIN'
and pl.sku_id=sku.sku_id)
Where PID=PID2
group by
SK,
SKD,
LPG))
where ISKU=SK)
group by LPG,TOTQ
order by
LPG)
/


--spool off

