SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240


column A heading 'Order ID' format A16
column B heading 'SKU' format A18
column C heading 'Description' format A40
column D heading 'Colour' format A12
column E heading 'Size' format A10
column G heading 'Line ID' format 999999
column H heading 'Qty|Ordered' format 999999
column I heading 'Qty|Picked' format 9999999
column J heading 'Back|Order' format 9999999


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

set und '-'
set heading on
set newpage 0


select 'Order ID,SKU,Description,Colour,Size,Line ID,Ordered,Picked,Back Order Qty' from DUAL;

--ttitle  LEFT 'Back Ordered Items for client ID T as at ' report_date RIGHT 'Page:' FORMAT 999 SQL.PNO skip 1

--break on report 

--compute sum label 'Total' of J on report

--break on row skip 1

select 
ol.order_id A,
ol.sku_id B,
sku.description C,
sku.colour D,
sku.sku_size E,
ol.line_id G,
ol.qty_ordered H,
nvl(ol.qty_picked,0) I,
ol.qty_ordered-nvl(qty_picked,0) J
from order_header oh,order_line ol,sku
where oh.client_id='T'
and oh.order_id=ol.order_id
and ol.back_ordered='Y'
and oh.status<>'Shipped'
and ol.sku_id=sku.sku_id
and sku.client_id='T'
order by
ol.order_id,
ol.sku_id
/
