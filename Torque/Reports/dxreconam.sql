SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','
SET UND OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 320
SET TRIMSPOOL ON


column A1 format A18
column A format A28
column B format A40
column Z format A12
column C format 9999999
column D format 9999999
column E format 9999999
column F format 9999999
column G format 9999999
column H format 9999999
column J format 9999999
column K format 9999999
column L format 9999999
column N format 9999999
column P format 9999999
column Q format 9999999
column V format 9999999
column W format 9999999


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


COLUMN NEWSEQ NEW_VALUE SEQNO


select 'S://redprairie/apps/home/dcsdba/reports/LIVE/DXDR'||to_char(SYSDATE, '_YYYYMMDD')||'.csv' NEWSEQ from DUAL;
--select 'LIVE/DXDR'||to_char(SYSDATE, '_YYYYMMDD')||'.csv' NEWSEQ from DUAL;

--set termout off


--break on report

--compute sum label 'Total'  of C D E J K G N P Q F V W on report

set termout on

spool &SEQNO

select 'Date : '||trunc(sysdate)||',Elite W,Elite QUAR,Elite FATY,Elite DAMG,Elite QVC,Elite IDEAL,Elite Transit,Elite Total' from DUAL;

--select trunc(sysdate) from DUAL;

select
'Opening Stock' A,
CCC-QUAR-FATY-DAMG-QVC-IDEAL-SUSP-TRT C,
QUAR D,
FATY E,
DAMG F,
QVC H,
IDEAL J,
--SUSP K,
TRT L,
CCC N
from (Select
sum(qty_on_hand) CCC
from inventory
where client_id='DX'),(Select
sum(qty_on_hand) QUAR
from inventory
where client_id='DX'
and location_id in ('DXBREJ1')
and condition_id='QUAR'),(Select
sum(qty_on_hand) FATY
from inventory
where client_id='DX'
and location_id in ('DXBREJ1')
and condition_id='FATY'),(Select
NVL(sum(qty_on_hand),0) DAMG
from inventory
where client_id='DX'
and location_id in ('DXBREJ1')
and condition_id='DMGD'),(Select
nvl(sum(qty_on_hand),0) QVC
from inventory
where client_id='DX'
--and condition_id in ('QVC')),(Select
and zone_1 in ('QVC')),(Select
nvl(sum(qty_on_hand),0) IDEAL
from inventory
where client_id='DX'
--and condition_id in ('IDEAL')),(Select
and zone_1 in ('IDEAL')),(Select
nvl(sum(qty_on_hand),0) SUSP
from inventory
where client_id='DX'
and location_id='SUSPENSE'),(Select
nvl(sum(qty_on_hand),0) TRT
from inventory
where client_id='DX'
and location_id in  ('DXBRET','DXBREJ'))
/


spool off
