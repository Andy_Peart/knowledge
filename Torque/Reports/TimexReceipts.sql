SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON


column A heading 'Type' format a18
column A2 heading 'Reference' format a14
column A3 heading 'SKU' format a14
column A4 heading 'Date' format a14
column B heading 'Units' format 999999
column K heading 'Units' format 999999
column B1 heading 'Rate' format A6
column C heading 'Value' format 999999.99

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON




break on A dup skip 1 on A1 dup skip 1 on report

--compute sum label 'Sub Total' of B C on A2
compute sum label 'Summary' of B C on A1
compute sum label 'Totals' of B C on A
compute sum label 'Overall' of B C on report

spool timexreceipts.csv

select 'Type,Reference,SKU,Date Received,Units Expected,Units Received,Rate,Value' from DUAL;

select * from (select
'Receipts' A,
it.reference_id A2,
it.sku_id A3,
min(to_char(it.Dstamp,'dd/mm/yy')) A4,
nvl(pl2,0) K,
sum(it.update_qty) B,
'0.02' B1,
sum(it.update_qty)*0.02 C
from inventory_transaction it,(select
pre_advice_id as pl1,
sum(qty_due) as pl2
from pre_advice_line
where client_id='TIMEX'
group by
pre_advice_id)
where it.client_id='TIMEX'
and it.station_id not like 'Auto%'
and to_char(it.Dstamp,'mm/yyyy')=to_char(sysdate-4,'mm/yyyy')
and (it.code='Receipt' or (it.code='Adjustment' and it.reason_id in ('ADJ','RECRV','STK CHECK')))
and it.reference_id=pl1 (+)
group by
it.reference_id,
it.sku_id,
--to_char(it.Dstamp,'dd/mm/yy'),
nvl(pl2,0))
where B<>0
order by
A,A2
/

spool off