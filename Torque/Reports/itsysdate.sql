SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON


/* select 'The time sponsered by Accurist will be '||to_char(sysdate, 'DD-MON-YYYY HH24:MI') FROM DUAL; */
select 'Should show 23 OCT 2018 16:41 but shows as '||to_char(creation_date, 'DD-MON-YYYY HH24:MI') from order_header where client_id = 'HS' and order_id = '1536112';
