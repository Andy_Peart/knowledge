SET FEEDBACK OFF                 
set verify off
set wrap off
set colsep ' '

clear columns
clear breaks
clear computes

set pagesize 67
set newpage 0
set linesize 132
set trimspool on


column A heading 'Consignment' format A12
column B heading 'Order ID' format A12
column C heading 'SKU ID' format A18
column D heading 'Qty Ordered' format 99999
column E heading 'Name' format a40
column F heading 'Post Code' format a12
column Z noprint

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

--break on report 
break on A dup 

--spool mwtasks.csv

ttitle 'Consignment Check Sheet for client S2'

select oh.consignment A
	,      oh.order_id B
	,      ol.sku_id C
	,      ol.qty_ordered D
	,      oh.contact E 
	,      oh.postcode F
	from order_header  oh
	join order_line ol on ol.order_id = oh.order_id and ol.client_id = oh.client_id
	where oh.client_id = 'S2'
	and oh.consignment = '&&2'
	order by consignment
/

--spool off
