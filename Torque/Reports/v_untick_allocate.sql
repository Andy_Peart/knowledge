/* Untick the "Allocate" and "Back Ordered" flags for old orders. */
/* Script introduced by SPT563593. */

prompt Orders shipped more than 90 days ago.

update order_line
set allocate = null
where allocate = 'Y'
and (order_id, client_id) in
    (select order_id, client_id
     from order_header
     where status = 'Shipped'
     and shipped_date <= current_timestamp - numtodsinterval (90, 'DAY'));

commit;

update order_line
set back_ordered = null
where back_ordered = 'Y'
and (order_id, client_id) in
    (select order_id, client_id
     from order_header
     where status = 'Shipped'
     and shipped_date <= current_timestamp - numtodsinterval (90, 'DAY'));

commit;

prompt Orders cancelled more than 90 days ago.

update order_line
set allocate = null
where allocate = 'Y'
and (order_id, client_id) in
    (select order_id, client_id
     from order_header
     where status = 'Cancelled'
     and ship_by_date <= current_timestamp - numtodsinterval (90, 'DAY'));

commit;

update order_line
set back_ordered = null
where back_ordered = 'Y'
and (order_id, client_id) in
    (select order_id, client_id
     from order_header
     where status = 'Cancelled'
     and ship_by_date <= current_timestamp - numtodsinterval (90, 'DAY'));

commit;

exit;

