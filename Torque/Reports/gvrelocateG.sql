SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 480
SET TRIMSPOOL ON

column X heading 'Qty' format 9999999

select 'Relocate G between &&2 and &&3' from DUAL;
select 'Relocated Qty Sku G' from DUAL;

break on report

select X
from (select nvl(sum (i.update_qty),0) X
from inventory_transaction i, location l, sku s 
where i.client_id = 'GV'
and s.sku_id like ('G%')
and s.sku_id = i.sku_id (+)
and i.from_loc_id ='AQL'
and i.to_loc_id = l.location_id
and l.zone_1 ='GIVE'
and i.code ='Relocate'
and l.site_id ='GIVE'
and i.dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1)
/