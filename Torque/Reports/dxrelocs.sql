SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 200
SET TRIMSPOOL ON

break on Z dup skip 1 

compute sum label 'Day Total' of CCC on Z

--spool dxrelocs.csv

select 'DX RELOCATES FROM &&2 to &&3' from DUAL;

select 'Date,From,To,Qty' from DUAL;

Select
trunc(Dstamp) Z,
DECODE(from_loc_id,'C01A','QVC','B01A','IDEAL','DXBREJ1','REJECT','W') A,
DECODE(to_loc_id,'C01A','QVC','B01A','IDEAL','DXBREJ1','REJECT','W') B,
nvl(sum(update_qty),0) CCC
from inventory_transaction
where client_id='DX'
and Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and nvl(from_loc_id,' ') not in ('DXBRET','DXBRET1','DXIN')
and code='Relocate'
group by
trunc(Dstamp),
DECODE(from_loc_id,'C01A','QVC','B01A','IDEAL','DXBREJ1','REJECT','W'),
DECODE(to_loc_id,'C01A','QVC','B01A','IDEAL','DXBREJ1','REJECT','W') 
order by
Z,A,B
/

--spool off

