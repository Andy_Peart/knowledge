set linesize 120
set heading off
set pagesize 0
set newpage 1
set feedback off

select 
'  Date ',
to_char(sysdate,'DD-MON-YYYY'), 
'Tot Recs = ',
count(*) A
from move_task
where client_id = 'TR' and trunc(dstamp) = trunc(sysdate)
/
select 
'   Started @ ',
to_char(min(dstamp),'HH:MI:SS')
from move_task
where client_id = 'TR' and trunc(dstamp) = trunc(sysdate)
/
select 
'  Finished @ ',
to_char(max(dstamp),'HH:MI:SS') 
from move_task
where client_id = 'TR' and trunc(dstamp) = trunc(sysdate)
/
