SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 160

column A heading 'SKU' format a20
column AA heading 'Description' format a40
column B heading 'Receipt Date' format a10
column C heading 'Receipt ID' format a14
column D heading 'Supplier ID' format a20
column E heading 'Qty Received' format 999999
column F heading 'Qty On Hand' format 999999
column G heading 'Location ID' format a10
column M format a4 noprint


--set termout off
spool OPS-Stock.csv


select 'Receipt Date,Receipt ID,Supplier,SKU,Description,Qty Received,Qty still on Hand,' from DUAL;

break on report
compute sum label 'Total' of E F on report

select
MMM M,
BBB B,
BB C,
DDD D,
AA A,
IDD AA,
QQQ E,
nvl(DD,0) F,
' '
from (select
max(to_char(it.dstamp,'YYMM')) MMM,
max(to_char(it.dstamp,'DD/MM/YY')) BBB,
it.reference_id CCC,
max(it.supplier_id) DDD,
it.sku_id AAA,
sku.description DESCR,
sum(it.update_qty) QQQ
from inventory_transaction it,sku
where it.client_id='OPS'
and it.code='Receipt'
and it.update_qty<>99999
and it.sku_id=sku.sku_id
and sku.client_id='OPS'
and sku.product_group='CL'
group by
it.reference_id,
it.sku_id,
sku.description
union
select
max(to_char(it.dstamp,'YYMM')) MMM,
max(to_char(it.dstamp,'DD/MM/YY')) BBB,
it.reference_id CCC,
max(it.supplier_id) DDD,
it.sku_id AAA,
sku.description DESCR,
sum(it.update_qty) QQQ
from inventory_transaction_archive it,sku
where it.client_id='OPS'
and it.code='Receipt'
and it.update_qty<>99999
and it.sku_id=sku.sku_id
and sku.client_id='OPS'
and sku.product_group='CL'
group by
it.reference_id,
it.sku_id,
sku.description),(select
i.sku_id AA,
sku.description IDD,
i.receipt_id BB,
sum(i.qty_on_hand-nvl(i.qty_allocated,0)) DD
from inventory i,sku
where i.client_id='OPS'
and i.sku_id=sku.sku_id
and sku.client_id='OPS'
and sku.product_group='CL'
and i.lock_status<>'Locked'
and i.condition_id<>'REJECT'
and i.location_id<>'REJECT'
--and i.location_id<>'OA1-SAMPLE'
group by
i.sku_id,
sku.description,
i.receipt_id)
where AA=AAA (+)
and BB=CCC (+)
order by
A
/
