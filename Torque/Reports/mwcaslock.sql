set serveroutput on;
set linesize 250;
set verify off;
set feedback off;

declare
    pragma autonomous_transaction;
    v_Consignment varchar2(30):= '&&2';
    i number:= 0;
begin
    update ORDER_LINE
    set lock_code = 'RESV'
    where client_id = 'MOUNTAIN'
    and order_id in
    (
        select order_id from ORDER_HEADER where client_id = 'MOUNTAIN' and consignment = v_Consignment and status not in ('Shipped', 'Cancelled') and order_type = 'RETAIL1'
    );

    i:= SQL%rowcount;
    commit;

    if i > 0 then
        DBMS_OUTPUT.PUT_LINE('Update Complete...');
        DBMS_OUTPUT.PUT_LINE(i||' record/s affected.');
        DBMS_OUTPUT.PUT_LINE(v_Consignment||' now ready to deallocate and reallocate.');
    elsif i = 0 then
        DBMS_OUTPUT.PUT_LINE('Nothing to update. Ensure Consignment is correct.');
    else
        DBMS_OUTPUT.PUT_LINE('Please contact IT, something has gone wrong.');
    end if;
end;
/