SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 200
SET TRIMSPOOL ON


column WW format A16


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

break on ww dup skip 1

--compute sum of CT on A

--spool yodellabels.csv


select 'Address_ID,Town,Postcode,Label' from DUAL;


select 
ADDRESS_ID,
TOWN,
POSTCODE,
SEQUENCE
from yo_total_labels
where ADDRESS_ID like '&&4%'
and substr(SEQUENCE,21,6) between '&&2' and '&&3'
order by 1
/

--spool off
