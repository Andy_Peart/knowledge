SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ,


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 1000
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Work Group,Order Type,Customer Id,Priority,Order Id,Creation Date,Order Date,Shipped Date,Shipped Hour,Picked Date,Picked Hour,Status,Lines,Units Ordered,Units Picked,Units Shipped,Units Outstanding,Contains Ugly Units?,Fully Ugly Order?' from dual
    union all
select work_group||','||order_type||','||customer_id||','||priority||','||order_id||','||creationDate||','||orderDate||','||shippedDate||','||shippedHour||','||pickedDate
||','||pickedHour||','||status||','||num_lines||','||unitsOrdered||','||unitsPicked||','||unitsShipped||','||outstanding||','||containUg||','||fullyUg from
(
    select oh.work_group
    , oh.order_type
    , oh.customer_id
    , oh.priority
    , oh.order_id
    , case when to_char(oh.creation_date, 'HH24') >= '22' then trunc(oh.creation_date) + 1 else trunc(oh.creation_date) end creationDate
    , trunc(oh.order_date) orderDate
    , trunc(oh.shipped_date) shippedDate
    , to_char(oh.shipped_date, 'HH24') shippedHour
    , trunc(x.pick_date) pickedDate
    , to_char(x.pick_date, 'HH24') pickedHour
    , oh.status
    , oh.num_lines
    , sum(ol.qty_ordered) unitsOrdered
    , max(x.units_picked) unitsPicked
    , coalesce(sum(ol.qty_shipped),0) unitsShipped
    , sum(ol.qty_ordered) - coalesce(sum(ol.qty_picked),0) outstanding
    , case when max(coalesce(s.ugly, 'N')) = 'Y' then 'Y' else 'N' end containUg
    , case when max(coalesce(s.ugly, 'N')) = 'Y' and min(coalesce(s.ugly, 'N')) = 'Y' then 'Y' else 'N' end fullyUg
    from ORDER_HEADER oh
    inner join ORDER_LINE ol on ol.order_id = oh.order_id and ol.client_id = oh.client_id
    inner join SKU s on s.sku_id = ol.sku_id and s.client_id = ol.client_id
    left join
    (
        select it.reference_id
        , sum(it.update_qty) units_picked
        , max(it.dstamp) pick_date
        from INVENTORY_TRANSACTION it
        inner join LOCATION l on l.location_id = it.from_loc_id and l.site_id = it.site_id
        where it.client_id = 'MOUNTAIN'
        and it.code = 'Pick'
        and it.dstamp >= to_date('&&2') - 20
        and l.loc_type = 'Tag-FIFO'
        group by it.reference_id
    ) x on x.reference_id = oh.order_id
    where oh.client_id = 'MOUNTAIN'
    and oh.order_type in ('RETAIL1','WEB')
    and oh.status = 'Shipped'
    and oh.shipped_date between to_date('&&2') and to_date('&&3') + 1
    group by oh.work_group
    , oh.order_type
    , oh.customer_id
    , oh.priority
    , oh.order_id
    , case when to_char(oh.creation_date, 'HH24') >= '22' then trunc(oh.creation_date) + 1 else trunc(oh.creation_date) end
    , trunc(oh.order_date)
    , oh.status
    , oh.num_lines
    , trunc(oh.shipped_date)
    , to_char(oh.shipped_date, 'HH24')
    , trunc(x.pick_date)
    , to_char(x.pick_date, 'HH24')
        union
    select oh.work_group
    , oh.order_type
    , oh.customer_id
    , oh.priority
    , oh.order_id
    , case when to_char(oh.creation_date, 'HH24') >= '22' then trunc(oh.creation_date) + 1 else trunc(oh.creation_date) end creationDate
    , trunc(oh.order_date) orderDate
    , null shippedDate
    , null shippedHour
    , trunc(x.pick_date) pickedDate
    , to_char(x.pick_date, 'HH24') pickedHour
    , oh.status
    , oh.num_lines
    , sum(ol.qty_ordered) unitsOrdered
    , max(x.units_picked) unitsPicked
    , coalesce(sum(ol.qty_shipped),0) unitsShipped
    , sum(ol.qty_ordered) - coalesce(sum(ol.qty_picked),0) outstanding
    , case when max(coalesce(s.ugly, 'N')) = 'Y' then 'Y' else 'N' end containUg
    , case when max(coalesce(s.ugly, 'N')) = 'Y' and min(coalesce(s.ugly, 'N')) = 'Y' then 'Y' else 'N' end fullyUg
    from ORDER_HEADER oh
    inner join ORDER_LINE ol on ol.order_id = oh.order_id and ol.client_id = oh.client_id
    inner join SKU s on s.sku_id = ol.sku_id and s.client_id = ol.client_id
    left join
    (
        select it.reference_id
        , sum(it.update_qty) units_picked
        , max(it.dstamp) pick_date
        from INVENTORY_TRANSACTION it
        inner join LOCATION l on l.location_id = it.from_loc_id and l.site_id = it.site_id
        where it.client_id = 'MOUNTAIN'
        and it.code = 'Pick'
        and it.dstamp >= to_date('&&2') - 20
        and l.loc_type = 'Tag-FIFO'
        group by it.reference_id
    ) x on x.reference_id = oh.order_id
    where oh.client_id = 'MOUNTAIN'
    and oh.order_type in ('WEB')
    and oh.status in ('Allocated','Released','Hold','In Progress','Complete','Picked')
    group by oh.work_group
    , oh.order_type
    , oh.customer_id
    , oh.priority
    , oh.order_id
    , case when to_char(oh.creation_date, 'HH24') >= '22' then trunc(oh.creation_date) + 1 else trunc(oh.creation_date) end
    , trunc(oh.order_date)
    , oh.status
    , oh.num_lines
    , trunc(oh.shipped_date)
    , to_char(oh.shipped_date, 'HH24')
    , trunc(x.pick_date)
    , to_char(x.pick_date, 'HH24')
)
/
--spool off