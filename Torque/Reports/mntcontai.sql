SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON


select 'TO_LOC_ID','CONTAINER_ID','UPDATE_QTY' FROM DUAL
union all
SELECT itl.to_loc_id, container_id, TO_CHAR(SUM(itl.update_qty))
FROM inventory_transaction itl
WHERE itl.client_id = 'MOUNTAIN'
AND CODE = 'Pick'
AND TO_LOC_ID = 'CONTAINER'
AND SUBSTR(REFERENCE_ID, 1,3) = 'MOR'
and dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by itl.to_loc_id, container_id
union all
select itl.to_loc_id, container_id, TO_CHAR(sum(itl.update_qty))
FROM inventory_transaction_archive itl
WHERE itl.client_id = 'MOUNTAIN'
AND CODE = 'Pick'
AND TO_LOC_ID = 'CONTAINER'
and substr(reference_id, 1,3) = 'MOR'
and dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by itl.to_loc_id, container_id
/