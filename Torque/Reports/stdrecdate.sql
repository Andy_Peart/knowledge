SET FEEDBACK OFF           
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON


select 'Code,Client,SKU,Description,Pre Advice,Qty,Date Time,Notes,User,User Def Type 2,User Def Type 1,Product Group,Supplier' col1 from dual
/* TORQUE  - (STD) - Receipts and Reversals by Date Range */
union all
select code || ',' || client_id  || ',' || sku_id || ',' ||  des || ',' || reference_id || ',' || update_qty || ',' ||  datetime || ',' || notes || ',' || user_id || ',' || user_def_type_2 || ',' || user_def_type_1 || ',' || product_group || ',' || supplier_id as col1
from (
select IT.code, IT.client_id, IT.sku_id, libsku.getuserlangskudesc('&&2',IT.sku_id) des, IT.reference_id
, IT.update_qty,  to_char(IT.dstamp, 'DD/MM/YYYY HH24:MI') as datetime, 
IT.notes, IT.user_id, IT.user_def_type_2, IT.user_def_type_1, SkuTable.PRODUCT_GROUP, pah.supplier_id
from inventory_transaction IT
INNER JOIN Sku SkuTable ON IT.CLIENT_ID = SkuTable.CLIENT_ID AND IT.Sku_Id = SkuTable.Sku_Id
left join PRE_ADVICE_HEADER pah on pah.pre_advice_id = it.reference_id and pah.client_id = it.client_id
where IT.client_id='&&2'
and IT.code like 'Receipt%'
and IT.Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
order by 5,3
)
/

--spool off
