set pagesize 2000
set linesize 2000
set verify off
clear columns
clear breaks
clear computes

column A heading 'SKU' format a13
column B heading 'Total Qty' format 99999

select i.sku_id A,B
from inventory i inner join
	(
		select inv.sku_id C, sum(qty_on_hand) B, sum(qty_allocated) D
		from inventory inv
		where client_id in ('MOUNTAIN','AA')
		group by inv.sku_id
	)
  on i.sku_id = C 
where B < '&&2'
and D=0
group by i.sku_id, B
order by 1
/
