SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON

--spool ordertracker.csv

select 'Customer,Order Id,Order Created,Ship by Date,Order Dispatched,Turnaround,Comments' from Dual;

select
A2,
A1,
A3,
A4,
D2,
CASE
WHEN (D1>D2) THEN 0
ELSE (D2-D1) END D3
from (select
order_id  A1,
name A2,
trunc(creation_date) A3,
trunc(nvl(ship_by_date,creation_date)) A4,
DECODE (to_char(nvl(ship_by_date,creation_date),'DY'),'FRI',trunc(nvl(ship_by_date,creation_date))+2,
        'SAT',trunc(nvl(ship_by_date,creation_date))+2,
        'SUN',trunc(nvl(ship_by_date,creation_date))+1,trunc(nvl(ship_by_date,creation_date))) D1,
trunc(shipped_date) D2
from order_header
where client_id = 'TP'
and shipped_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1)
order by
A3,A2,A1
/

--spool off
