SET FEEDBACK OFF
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
set colsep ','
set heading off
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9999

column A heading 'SKU' format a20
column B heading 'EAN' format a20
column C heading 'QTY' format 9999999

select 'SKU,EAN,QTY'
from dual;

select s.sku_id A,
s.ean B,
sum(nvl(i.qty_on_hand, 0)) C
from sku s, inventory i
where s.client_id = 'PRI'
and s.sku_id = i.sku_id
and i.location_id like 'QC%'
and i.location_id <> 'WP1'
and i.location_id <> 'Z1'
and i.location_id <> 'GOLDSEALS'
group by s.sku_id, s.ean
/
