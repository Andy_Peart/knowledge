set feedback off
set pagesize 68
set linesize 240
set newpage 0
set verify off
set colsep ' '

clear columns
clear breaks
clear computes

column A heading 'Day Created' format A24
column B heading ' ' format A14
column C heading 'Orders in' format A10
column D heading 'Orders out'format 999999
column E heading 'Percentage' format 999999
column F heading 'Shipped on' format A16
column J format A12
column K format A10
column L format A16
column M noprint
column P noprint

break on A on C skip 1

--compute sum label 'Totals' of D  on A

 
ttitle LEFT 'Fulfilment Report - for TM LEWIN client &&2 for week commencing Saturday &&3 '


select
MMM M,
AAA A,
to_char(ORD) C,
'Day '||substr(to_char(FFF+100),2,3) F,
DDD D,
(DDD*100)/ORD E
from (select
to_char(ORDERED,'YYMMDD') MMM,
to_char(ORDERED,'DAY DD/MM/YY') AAA,
trunc(SHIPPED)-trunc(ORDERED)+1 FFF,
sum(ORDERS) DDD
from (select
DECODE(trunc(oh.creation_date),to_date('&&3', 'DD-MON-YYYY'),trunc(oh.creation_date)+2,
                               to_date('&&3', 'DD-MON-YYYY')+1,trunc(oh.creation_date)+1,
                               trunc(oh.creation_date)) ORDERED,
trunc(oh.shipped_date) SHIPPED,
count(*) ORDERS
from order_header oh
where oh.client_id='&&2'
and oh.status='Shipped'
and oh.order_id not like 'NP%'
and oh.creation_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+7
group by
DECODE(trunc(oh.creation_date),to_date('&&3', 'DD-MON-YYYY'),trunc(oh.creation_date)+2,
                               to_date('&&3', 'DD-MON-YYYY')+1,trunc(oh.creation_date)+1,
                               trunc(oh.creation_date)),
trunc(oh.shipped_date))
where SHIPPED between trunc(ORDERED) and trunc(ORDERED)+11
and ((to_char(ORDERED,'DAY ') like 'MON%' and (trunc(SHIPPED)-trunc(ORDERED)+1)<6)
or (to_char(ORDERED,'DAY ') like 'TUE%' and (trunc(SHIPPED)-trunc(ORDERED)+1)<5)
or (to_char(ORDERED,'DAY ') like 'WED%' and (trunc(SHIPPED)-trunc(ORDERED)+1)<4)
or (to_char(ORDERED,'DAY ') like 'THU%' and (trunc(SHIPPED)-trunc(ORDERED)+1)<3)
or (to_char(ORDERED,'DAY ') like 'FRI%' and (trunc(SHIPPED)-trunc(ORDERED)+1)<2))
group by
to_char(ORDERED,'YYMMDD'),
to_char(ORDERED,'DAY DD/MM/YY'),
trunc(SHIPPED)-trunc(ORDERED)+1),(select
to_char(XXX,'YYMMDD') XX,
sum(ORDX) ORD
from (select
DECODE(trunc(oh.creation_date),to_date('&&3', 'DD-MON-YYYY'),trunc(oh.creation_date)+2,
                               to_date('&&3', 'DD-MON-YYYY')+1,trunc(oh.creation_date)+1,
                               trunc(oh.creation_date)) XXX,
count(*) ORDX
from order_header oh
where oh.client_id='&&2'
and oh.status<>'Cancelled'
and oh.order_id not like 'NP%'
and oh.creation_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+7
group by
DECODE(trunc(oh.creation_date),to_date('&&3', 'DD-MON-YYYY'),trunc(oh.creation_date)+2,
                               to_date('&&3', 'DD-MON-YYYY')+1,trunc(oh.creation_date)+1,
                               trunc(oh.creation_date)))
group by to_char(XXX,'YYMMDD'))
where MMM=XX
union
select
MMM M,
AAA A,
to_char(ORD) C,
'Day '||substr(to_char(FFF+100),2,3) F,
DDD D,
(DDD*100)/ORD E
from (select
to_char(ORDERED,'YYMMDD') MMM,
to_char(ORDERED,'DAY DD/MM/YY') AAA,
trunc(SHIPPED)-trunc(ORDERED)-1 FFF,
sum(ORDERS) DDD
from (select
DECODE(trunc(oh.creation_date),to_date('&&3', 'DD-MON-YYYY'),trunc(oh.creation_date)+2,
                               to_date('&&3', 'DD-MON-YYYY')+1,trunc(oh.creation_date)+1,
                               trunc(oh.creation_date)) ORDERED,
trunc(oh.shipped_date) SHIPPED,
count(*) ORDERS
from order_header oh
where oh.client_id='&&2'
and oh.status='Shipped'
and oh.order_id not like 'NP%'
and oh.creation_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+7
group by
DECODE(trunc(oh.creation_date),to_date('&&3', 'DD-MON-YYYY'),trunc(oh.creation_date)+2,
                               to_date('&&3', 'DD-MON-YYYY')+1,trunc(oh.creation_date)+1,
                               trunc(oh.creation_date)),
trunc(oh.shipped_date))
where SHIPPED between trunc(ORDERED) and trunc(ORDERED)+11
and ((to_char(ORDERED,'DAY ') like 'MON%' and (trunc(SHIPPED)-trunc(ORDERED)+1)>5)
or (to_char(ORDERED,'DAY ') like 'TUE%' and (trunc(SHIPPED)-trunc(ORDERED)+1)>4)
or (to_char(ORDERED,'DAY ') like 'WED%' and (trunc(SHIPPED)-trunc(ORDERED)+1)>3)
or (to_char(ORDERED,'DAY ') like 'THU%' and (trunc(SHIPPED)-trunc(ORDERED)+1)>2)
or (to_char(ORDERED,'DAY ') like 'FRI%' and (trunc(SHIPPED)-trunc(ORDERED)+1)>1))
group by
to_char(ORDERED,'YYMMDD'),
to_char(ORDERED,'DAY DD/MM/YY'),
trunc(SHIPPED)-trunc(ORDERED)-1),(select
to_char(XXX,'YYMMDD') XX,
sum(ORDX) ORD
from (select
DECODE(trunc(oh.creation_date),to_date('&&3', 'DD-MON-YYYY'),trunc(oh.creation_date)+2,
                               to_date('&&3', 'DD-MON-YYYY')+1,trunc(oh.creation_date)+1,
                               trunc(oh.creation_date)) XXX,
count(*) ORDX
from order_header oh
where oh.client_id='&&2'
and oh.status<>'Cancelled'
and oh.order_id not like 'NP%'
and oh.creation_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+7
group by
DECODE(trunc(oh.creation_date),to_date('&&3', 'DD-MON-YYYY'),trunc(oh.creation_date)+2,
                               to_date('&&3', 'DD-MON-YYYY')+1,trunc(oh.creation_date)+1,
                               trunc(oh.creation_date)))
group by to_char(XXX,'YYMMDD'))
where MMM=XX
union
select
MMM M,
AAA A,
to_char(ORD) C,
FFF F,
DDD D,
(DDD*100)/ORD E
from (select
to_char(ORDERED,'YYMMDD') MMM,
to_char(ORDERED,'DAY DD/MM/YY') AAA,
'Total' FFF,
sum(ORDERS) DDD
from (select
DECODE(trunc(oh.creation_date),to_date('&&3', 'DD-MON-YYYY'),trunc(oh.creation_date)+2,
                               to_date('&&3', 'DD-MON-YYYY')+1,trunc(oh.creation_date)+1,
                               trunc(oh.creation_date)) ORDERED,
trunc(oh.shipped_date) SHIPPED,
count(*) ORDERS
from order_header oh
where oh.client_id='&&2'
and oh.status='Shipped'
and oh.order_id not like 'NP%'
and oh.creation_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+7
group by
DECODE(trunc(oh.creation_date),to_date('&&3', 'DD-MON-YYYY'),trunc(oh.creation_date)+2,
                               to_date('&&3', 'DD-MON-YYYY')+1,trunc(oh.creation_date)+1,
                               trunc(oh.creation_date)),
trunc(oh.shipped_date))
where SHIPPED>trunc(ORDERED-1)
group by
to_char(ORDERED,'YYMMDD'),
to_char(ORDERED,'DAY DD/MM/YY')),(select
to_char(XXX,'YYMMDD') XX,
sum(ORDX) ORD
from (select
DECODE(trunc(oh.creation_date),to_date('&&3', 'DD-MON-YYYY'),trunc(oh.creation_date)+2,
                               to_date('&&3', 'DD-MON-YYYY')+1,trunc(oh.creation_date)+1,
                               trunc(oh.creation_date)) XXX,
count(*) ORDX
from order_header oh
where oh.client_id='&&2'
and oh.status<>'Cancelled'
and oh.order_id not like 'NP%'
and oh.creation_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+7
group by
DECODE(trunc(oh.creation_date),to_date('&&3', 'DD-MON-YYYY'),trunc(oh.creation_date)+2,
                               to_date('&&3', 'DD-MON-YYYY')+1,trunc(oh.creation_date)+1,
                               trunc(oh.creation_date)))
group by to_char(XXX,'YYMMDD'))
where MMM=XX
union
select
MMM M,
AAA A,
to_char(ORD) C,
FFF F,
DDD D,
(DDD*100)/ORD E
from (select
to_char(ORDERED,'YYMMDD') MMM,
to_char(ORDERED,'DAY DD/MM/YY') AAA,
'Day 03 Total' FFF,
sum(ORDERS) DDD
from (select
DECODE(trunc(oh.creation_date),to_date('&&3', 'DD-MON-YYYY'),trunc(oh.creation_date)+2,
                               to_date('&&3', 'DD-MON-YYYY')+1,trunc(oh.creation_date)+1,
                               trunc(oh.creation_date)) ORDERED,
--trunc(oh.shipped_date) SHIPPED,
DECODE(SIGN(to_date('&&3', 'DD-MON-YYYY')+6-trunc(oh.shipped_date)),-1,trunc(oh.shipped_date)-2,trunc(oh.shipped_date)) SHIPPED,
count(*) ORDERS
from order_header oh
where oh.client_id='&&2'
and oh.status='Shipped'
and oh.order_id not like 'NP%'
and oh.creation_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+7
group by
DECODE(trunc(oh.creation_date),to_date('&&3', 'DD-MON-YYYY'),trunc(oh.creation_date)+2,
                               to_date('&&3', 'DD-MON-YYYY')+1,trunc(oh.creation_date)+1,
                               trunc(oh.creation_date)),
DECODE(SIGN(to_date('&&3', 'DD-MON-YYYY')+6-trunc(oh.shipped_date)),-1,trunc(oh.shipped_date)-2,trunc(oh.shipped_date)))
--trunc(oh.shipped_date))
where SHIPPED between trunc(ORDERED) and trunc(ORDERED)+2
group by
to_char(ORDERED,'YYMMDD'),
to_char(ORDERED,'DAY DD/MM/YY')),(select
to_char(XXX,'YYMMDD') XX,
sum(ORDX) ORD
from (select
DECODE(trunc(oh.creation_date),to_date('&&3', 'DD-MON-YYYY'),trunc(oh.creation_date)+2,
                               to_date('&&3', 'DD-MON-YYYY')+1,trunc(oh.creation_date)+1,
                               trunc(oh.creation_date)) XXX,
count(*) ORDX
from order_header oh
where oh.client_id='&&2'
and oh.status<>'Cancelled'
and oh.order_id not like 'NP%'
and oh.creation_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+7
group by
DECODE(trunc(oh.creation_date),to_date('&&3', 'DD-MON-YYYY'),trunc(oh.creation_date)+2,
                               to_date('&&3', 'DD-MON-YYYY')+1,trunc(oh.creation_date)+1,
                               trunc(oh.creation_date)))
group by to_char(XXX,'YYMMDD'))
where MMM=XX
union
select
MMM M,
AAA A,
to_char(ORD) C,
FFF F,
DDD D,
(DDD*100)/ORD E
from (select
to_char(ORDERED,'YYMMDD') MMM,
to_char(ORDERED,'DAY DD/MM/YY') AAA,
'Day 11+' FFF,
sum(ORDERS) DDD
from (select
DECODE(trunc(oh.creation_date),to_date('&&3', 'DD-MON-YYYY'),trunc(oh.creation_date)+2,
                               to_date('&&3', 'DD-MON-YYYY')+1,trunc(oh.creation_date)+1,
                               trunc(oh.creation_date)) ORDERED,
--trunc(oh.shipped_date) SHIPPED,
DECODE(SIGN(to_date('&&3', 'DD-MON-YYYY')+6-trunc(oh.shipped_date)),-1,trunc(oh.shipped_date)-2,trunc(oh.shipped_date)) SHIPPED,
count(*) ORDERS
from order_header oh
where oh.client_id='&&2'
and oh.status='Shipped'
and oh.order_id not like 'NP%'
and oh.creation_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+7
group by
DECODE(trunc(oh.creation_date),to_date('&&3', 'DD-MON-YYYY'),trunc(oh.creation_date)+2,
                               to_date('&&3', 'DD-MON-YYYY')+1,trunc(oh.creation_date)+1,
                               trunc(oh.creation_date)),
DECODE(SIGN(to_date('&&3', 'DD-MON-YYYY')+6-trunc(oh.shipped_date)),-1,trunc(oh.shipped_date)-2,trunc(oh.shipped_date)))
--trunc(oh.shipped_date))
where SHIPPED>trunc(ORDERED)+9
group by
to_char(ORDERED,'YYMMDD'),
to_char(ORDERED,'DAY DD/MM/YY')),(select
to_char(XXX,'YYMMDD') XX,
sum(ORDX) ORD
from (select
DECODE(trunc(oh.creation_date),to_date('&&3', 'DD-MON-YYYY'),trunc(oh.creation_date)+2,
                               to_date('&&3', 'DD-MON-YYYY')+1,trunc(oh.creation_date)+1,
                               trunc(oh.creation_date)) XXX,
count(*) ORDX
from order_header oh
where oh.client_id='&&2'
and oh.status<>'Cancelled'
and oh.order_id not like 'NP%'
and oh.creation_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+7
group by
DECODE(trunc(oh.creation_date),to_date('&&3', 'DD-MON-YYYY'),trunc(oh.creation_date)+2,
                               to_date('&&3', 'DD-MON-YYYY')+1,trunc(oh.creation_date)+1,
                               trunc(oh.creation_date)))
group by to_char(XXX,'YYMMDD'))
where MMM=XX
union
select
'ZZ' M,
'FULL WEEK' A,
to_char(sum(ORD)) C,
'TOTALS' F,
sum(DDD) D,
(sum(DDD)*100)/sum(ORD) E
from (select
to_char(ORDERED,'YYMMDD') MMM,
to_char(ORDERED,'DAY DD/MM/YY') AAA,
'Total' FFF,
sum(ORDERS) DDD
from (select
DECODE(trunc(oh.creation_date),to_date('&&3', 'DD-MON-YYYY'),trunc(oh.creation_date)+2,
                               to_date('&&3', 'DD-MON-YYYY')+1,trunc(oh.creation_date)+1,
                               trunc(oh.creation_date)) ORDERED,
trunc(oh.shipped_date) SHIPPED,
count(*) ORDERS
from order_header oh
where oh.client_id='&&2'
and oh.status='Shipped'
and oh.order_id not like 'NP%'
and oh.creation_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+7
group by
DECODE(trunc(oh.creation_date),to_date('&&3', 'DD-MON-YYYY'),trunc(oh.creation_date)+2,
                               to_date('&&3', 'DD-MON-YYYY')+1,trunc(oh.creation_date)+1,
                               trunc(oh.creation_date)),
trunc(oh.shipped_date))
where SHIPPED>trunc(ORDERED-1)
group by
to_char(ORDERED,'YYMMDD'),
to_char(ORDERED,'DAY DD/MM/YY')),(select
to_char(XXX,'YYMMDD') XX,
sum(ORDX) ORD
from (select
DECODE(trunc(oh.creation_date),to_date('&&3', 'DD-MON-YYYY'),trunc(oh.creation_date)+2,
                               to_date('&&3', 'DD-MON-YYYY')+1,trunc(oh.creation_date)+1,
                               trunc(oh.creation_date)) XXX,
count(*) ORDX
from order_header oh
where oh.client_id='&&2'
and oh.status<>'Cancelled'
and oh.order_id not like 'NP%'
and oh.creation_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+7
group by
DECODE(trunc(oh.creation_date),to_date('&&3', 'DD-MON-YYYY'),trunc(oh.creation_date)+2,
                               to_date('&&3', 'DD-MON-YYYY')+1,trunc(oh.creation_date)+1,
                               trunc(oh.creation_date)))
group by to_char(XXX,'YYMMDD'))
where MMM=XX
union
select
'ZY' M,
'FULL WEEK' A,
to_char(sum(ORD)) C,
'DAY 03 TOTALS' F,
sum(DDD) D,
(sum(DDD)*100)/sum(ORD) E
from (select
to_char(ORDERED,'YYMMDD') MMM,
to_char(ORDERED,'DAY DD/MM/YY') AAA,
'Day 03 Total' FFF,
sum(ORDERS) DDD
from (select
DECODE(trunc(oh.creation_date),to_date('&&3', 'DD-MON-YYYY'),trunc(oh.creation_date)+2,
                               to_date('&&3', 'DD-MON-YYYY')+1,trunc(oh.creation_date)+1,
                               trunc(oh.creation_date)) ORDERED,
--trunc(oh.shipped_date) SHIPPED,
DECODE(SIGN(to_date('&&3', 'DD-MON-YYYY')+6-trunc(oh.shipped_date)),-1,trunc(oh.shipped_date)-2,trunc(oh.shipped_date)) SHIPPED,
count(*) ORDERS
from order_header oh
where oh.client_id='&&2'
and oh.status='Shipped'
and oh.order_id not like 'NP%'
and oh.creation_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+7
group by
DECODE(trunc(oh.creation_date),to_date('&&3', 'DD-MON-YYYY'),trunc(oh.creation_date)+2,
                               to_date('&&3', 'DD-MON-YYYY')+1,trunc(oh.creation_date)+1,
                               trunc(oh.creation_date)),
DECODE(SIGN(to_date('&&3', 'DD-MON-YYYY')+6-trunc(oh.shipped_date)),-1,trunc(oh.shipped_date)-2,trunc(oh.shipped_date)))
--trunc(oh.shipped_date))
where SHIPPED between trunc(ORDERED) and trunc(ORDERED)+2
group by
to_char(ORDERED,'YYMMDD'),
to_char(ORDERED,'DAY DD/MM/YY')),(select
to_char(XXX,'YYMMDD') XX,
sum(ORDX) ORD
from (select
DECODE(trunc(oh.creation_date),to_date('&&3', 'DD-MON-YYYY'),trunc(oh.creation_date)+2,
                               to_date('&&3', 'DD-MON-YYYY')+1,trunc(oh.creation_date)+1,
                               trunc(oh.creation_date)) XXX,
count(*) ORDX
from order_header oh
where oh.client_id='&&2'
and oh.status<>'Cancelled'
and oh.order_id not like 'NP%'
and oh.creation_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+7
group by
DECODE(trunc(oh.creation_date),to_date('&&3', 'DD-MON-YYYY'),trunc(oh.creation_date)+2,
                               to_date('&&3', 'DD-MON-YYYY')+1,trunc(oh.creation_date)+1,
                               trunc(oh.creation_date)))
group by to_char(XXX,'YYMMDD'))
where MMM=XX
order by
M,F
/




