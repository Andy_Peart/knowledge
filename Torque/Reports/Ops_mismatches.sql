SET FEEDBACK OFF                 
set pagesize 66
set linesize 80
set verify off
set wrap off
set newpage 0
set colsep '   '

clear columns
clear breaks
clear computes


column A heading 'Pre Advice ID' format a14
column B heading 'Qty Due' format 9999999
column C heading 'Qty Received' format 9999999
column D heading 'Line ID' format 99999999
column E heading 'SKU ID' format a18



set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD-MON-YYYY  HH:MI:SS') curdate from DUAL;
set TERMOUT ON


TTITLE LEFT 'OPS Pre Advice Mismatches for ID &&2 ' RIGHT report_date skip 1 -
RIGHT 'Page: ' Format 999 SQL.PNO skip 2 -

 
select 
l.pre_advice_id A, 
l.line_id D, 
l.sku_id E,
l.qty_due B,
nvl(l.qty_received,0) C
from pre_advice_line l
where l.client_id ='OPS'
and nvl(l.qty_received,0) <> l.qty_due
and l.pre_advice_id like '&&2%'
order by A,D
/