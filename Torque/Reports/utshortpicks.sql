SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9999
SET TRIMSPOOL ON

column A heading 'Date' format A16
column A1 heading 'Order ID' format A24
column A2 heading 'SKU ID' format A20
column A3 heading 'Description' format A40
column B heading 'Allocated' format 999999
column C heading 'Picked' format 999999
column D heading 'Shortage' format 999999
column F heading 'Order Type' format A12
column G heading 'Inst' format A32

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



break on report
compute sum label 'Total' of B C D on report

--spool utshortpicks.csv

select 'UT Short Picks - '||to_char(SYSDATE, 'DD/MM/YY  HH24:MI:SS') from DUAL;

select 'Date,Order No,SKU,Description,Order Type,Instructions,Allocated,Picked,Shortage' from DUAL;


select
distinct
to_char(it.Dstamp,'DD/MM/YY') A,
it.reference_id A1,
it.sku_id A2,
sku.description A3,
oh.order_type F,
--DECODE(oh.order_type,'JACQUELINE',' ',oh.instructions) G,
it.original_qty B,
it.update_qty C,
it.original_qty-it.update_qty D
from inventory_transaction it,sku,order_header oh
where it.client_id='UT'
and it.code='Pick'
and it.reference_id='&&2'
--and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.original_qty>it.update_qty
and it.sku_id=sku.sku_id
and sku.client_id='UT'
and it.reference_id=oh.order_id
and oh.client_id='UT'
and it.reference_id||it.sku_id  not in (select
oh.order_id||ol.sku_id
from order_header oh,order_line ol
where oh.Client_ID='UT'
and oh.status not in ('Shipped')
and oh.order_id=ol.order_id
and ol.Client_ID='UT'
and ol.qty_ordered>0
and nvl(ol.qty_tasked,0)+nvl(ol.qty_picked,0)<ol.qty_ordered)
order by
A,A1,A2
/

--spool off
