SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Consignment,Sku,Description,Qty' from dual
union all
select consignment||',="'||sku_id||'",'||description||','||qty
from (
select it.consignment
, it.sku_id
, s.description
, sum(it.update_qty) qty
from inventory_transaction it
inner join sku s on s.client_id = it.client_id and s.sku_id = it.sku_id
where it.client_id = 'MOUNTAIN'
and it.consignment = '&&2'
and it.code = 'Consol'
and it.dstamp > trunc(sysdate)-1
and it.from_loc_id <> 'CONTAINER'
group by it.consignment
, it.sku_id
, s.description
order by s.description
)
/
--spool off