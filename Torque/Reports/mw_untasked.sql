SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 112


column A heading 'Store' format A24
column A1 heading 'WorkG' format A6
column B heading 'Order ID' format A10
column C heading 'Creation Date' format A14
column D heading 'Order Qty' format 999999
column E heading 'Lines' format 999999
column E1 heading '0 Lines' format 999999



--set termout off
--spool untasked.csv

Select '&&4 - Outstanding Order Items NOT allocated - &&2 to &&3' from DUAL;

select 'Store,Work Group,Order ID,Creation Date,Lines,OQ=0,Order Qty,,' from DUAL;


select
a.name A,
oh.work_group A1,
oh.order_id B,
to_char(oh.creation_date,'DD/MM/YYYY') C,
count(*) E,
nvl(EEE,0) E1,
sum(ol.qty_ordered) D,
' '
from Order_header oh, order_line ol,address a,
(select
oh.order_id OID,
count(*) EEE
from Order_header oh, order_line ol
where oh.client_id='&&4'
and oh.work_group<>'WWW'
and oh.creation_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and oh.order_id=ol.order_id
and ol.client_id=oh.client_id
and ol.qty_ordered=0
group by
oh.order_id)
where oh.client_id='&&4'
and oh.work_group<>'WWW'
and oh.creation_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and oh.order_id=ol.order_id
and ol.client_id=oh.client_id
and (ol.qty_tasked=0 or ol.qty_tasked is null)
and oh.order_id=OID (+)
and oh.customer_id=a.address_id
and a.client_id=oh.client_id
group by
a.name,
oh.work_group,
oh.order_id,
EEE,
oh.creation_date
order by
a.name,
oh.order_id
/


--spool off
--set termout on
