SET FEEDBACK OFF           
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 100
SET TRIMSPOOL ON

select 'SSC'  ||','||
'U'    ||','||
s.sku_id ||','||
'P1'   ||','||
'N'    ||','||
'T'  
from sku s
where s.client_id = 'T'
and s.sku_id not in (select sku_id from sku_sku_config
where client_id = 'T')
order by 1
/
