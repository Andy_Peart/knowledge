SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Creation Date,Order Type,Order id,Customer id,Customer Name,Sku id,Description,Config id,Pack Size,Qty Ordered,Difference,Unique Order' from dual
union all
select "CD" ||','|| order_type ||','|| order_id ||','|| customer_id ||','|| "Name" ||','|| sku_id ||','|| description ||','|| config_id ||','|| "Pack Size" ||','|| qty_ordered ||','|| "Difference from Pack" ||','|| "Unique Order"
from
(
    select "CD"
    , order_type
    , order_id
    , customer_id
    , "Name"
    , sku_id
    , description
    , config_id
    , "Pack Size"
    , qty_ordered
    , case when "Pack Size" > qty_ordered then "Pack Size" - qty_ordered else mod(qty_ordered, "Pack Size") end as "Difference from Pack"
    , case when "RN" = 1 then '1' else '' end as "Unique Order"
    from
(
        select trunc(oh.creation_date) as "CD"
        , oh.order_type
        , oh.order_id
        , oh.customer_id
        , oh.name as "Name"
        , ol.sku_id
        , s.description
        , ol.qty_ordered
        , ol.config_id
        , substr(ol.config_id, 1, instr(ol.config_id, 'E')-1) as "Pack Size"
        , mod(ol.qty_ordered, substr(ol.config_id, 1, instr(ol.config_id, 'E')-1)) as "Difference"
        , row_number()over(partition by oh.order_id order by ol.sku_id) as "RN"
        from order_header oh
        inner join order_line ol on ol.client_id = oh.client_id and ol.order_id = oh.order_id
        inner join sku s on s.client_id = ol.client_id and s.sku_id = ol.sku_id
        where oh.client_id = 'ZG'
        and oh.creation_date between to_date('&&2') and to_date('&&3')+1
        and oh.order_type not like '%WEB%'
		and oh.order_id not like '0%'
        and ol.config_id is not null
)
    where "Difference" <> 0
    order by "CD"
    , order_id
    , sku_id
)
/
--spool off