SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 160

column A heading 'Order' format a20

--select 'Order No,Status' from DUAL;




Select distinct 
oh.Order_id||',DESPATCH' A,
DECODE(it.to_loc_id,'UKPAR','24 hr','UKPAC','packet',it.to_loc_id) BB
from order_header oh,inventory_transaction it
where oh.client_id='MOUNTAIN'
and oh.status='Shipped'
and oh.shipped_date>sysdate-1
and oh.order_type='WEB'
and it.client_id='MOUNTAIN'
and it.code='Pick'
and it.from_loc_id='CONTAINER'
and oh.order_id=it.reference_id
union
Select
distinct oh.Order_id||','||'ON HOLD' A,
' ' BB
from order_header oh,order_line ol
where oh.client_id='MOUNTAIN'
and oh.order_id=ol.order_id
and oh.status in ('Released')
and ol.qty_ordered<>nvl(ol.qty_tasked,0)
and oh.order_type='WEB'
union
Select
distinct oh.Order_id||','||'ON HOLD' A,
' ' BB
from order_header oh,order_line ol
where oh.client_id='MOUNTAIN'
and oh.order_id=ol.order_id
and oh.status in ('Allocated')
and ol.qty_ordered<>nvl(ol.qty_tasked,0)
and oh.order_type='WEB'
union
Select
distinct oh.Order_id||','||'ON HOLD' A,
' ' BB
from order_header oh,order_line ol
where oh.client_id='MOUNTAIN'
and oh.order_id=ol.order_id
and oh.status in ('Complete')
and ol.qty_ordered=nvl(ol.qty_picked,0)
and oh.order_type='WEB'
union
Select
distinct oh.Order_id||','||'ON HOLD' A,
' ' BB
from order_header oh,order_line ol
where oh.client_id='MOUNTAIN'
and oh.order_id=ol.order_id
and oh.status in ('In Progress')
and (ol.qty_ordered<>nvl(ol.qty_tasked,0)
or ol.qty_ordered=nvl(ol.qty_picked,0)+nvl(ol.qty_tasked,0))
and oh.order_type='WEB'
order by A
/

