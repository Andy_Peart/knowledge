SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON
SET HEADING ON


select 'Shipped Dates,Order ID,TO Number,Customer ID,Name,Cartons,Quantity' from dual
union all
select "Shipped Date" || ',' || "Order ID" || ',' || "PO Number" || ',' || "Customer ID" || ',' || "Name" || ',' || "Cartons" || ',' || "Quantity" from (
SELECT TRUNC(oh.shipped_date) as "Shipped Date" 
, itl.reference_id as "Order ID"
, oh.purchase_order as "PO Number"
, oh.customer_id as "Customer ID"
, oh.name as "Name"
, COUNT( DISTINCT itl.container_id) as "Cartons"
, sum (itl.update_qty) as "Quantity"
FROM inventory_transaction itl
LEFT JOIN order_header oh
ON itl.reference_id  = oh.order_id
AND itl.client_id = oh.client_id
WHERE itl.client_id = 'SS'
and oh.SHIP_DOCK <> 'SSWEB'
AND itl.dstamp BETWEEN to_date('&2', 'DD/MON/YYYY') AND to_date('&3', 'DD/MON/YYYY')+1
AND itl.code = 'Shipment'
GROUP BY oh.shipped_date,
itl.reference_id,
oh.purchase_order,
oh.customer_id,
oh.name
ORDER BY 1, 4
)
/