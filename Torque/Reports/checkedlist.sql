SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 140
SET TRIMSPOOL ON

column A heading 'Code' format A12
column B heading 'Owner' format A10
column C heading 'Client' format A10
column D heading 'SKU' format A18
column E heading 'Site' format A10
column F heading 'Loc' format a12
column G heading 'Qty' format 99999
column H heading 'Date' format A16


select 'Code,Owner ID,Client ID,SKU,Site ID,From Location,Original Qty,Txn Date' from DUAL;
 
set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DAY, DD MONTH, YYYY') curdate from DUAL;
set TERMOUT ON


--break on report 

--compute sum label 'Total' of G on report



select
it.code A,
it.owner_id B,
it.client_id C,
it.sku_id D,
it.site_id E,
it.from_loc_id F,
it.original_qty G,
to_char(Dstamp,'DD/MM/YY  HH24:MI') H
from inventory_transaction it
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='&&4'
and it.code='Stock Check'
order by
it.from_loc_id
/
