SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

select 'Report run at ' || to_char(sysdate, 'DD-MM-YYYY HH24:MI:SS') from dual
union all
select 'Date Received,Order No,SKU,Description,Size,Units,Reason Code,Customer Name,Address Line 1,Address Line 2,Town,Postcode,Country,Phone,Email,Notes' from dual
union all
select "Date Received" || ',' || "Order No" || ',' || "SKU" || ',' || "Description" || ',' || "Size" || ',' || "Units" || ',' || "Reason Code" || ',' || "Customer Name" || ',' || "Address Line 1"
|| ',' || "Address Line 2" || ',' || "Town" || ',' || "Postcode" || ',' || "Country" || ',' || "Phone" || ',' || "Email" || ',' || "Notes" 
from
(
select trunc(it.dstamp) as "Date Received"
, it.user_def_type_2    as "Order No"
, it.sku_id             as "SKU"
, sku.description       as "Description"
, sku.sku_size          as "Size"
, it.update_qty         as "Units"
, rrc.notes             as "Reason Code"
, oh.contact            as "Customer Name"
, oh.address1           as "Address Line 1"
, oh.address2           as "Address Line 2"
, oh.town               as "Town"
, oh.postcode           as "Postcode"
, oh.country            as "Country"
, oh.contact_phone      as "Phone"
, oh.contact_email      as "Email"
, it.notes              as "Notes"
from inventory_transaction it
inner join sku on sku.sku_id = it.sku_id and sku.client_id = it.client_id
inner join order_header oh on oh.order_id = it.user_def_type_2 and oh.client_id = it.client_id
inner join return_reason rrc on substr(rrc.reason_id,3,1) = it.user_def_type_3
where it.client_id = 'HP'
and it.dstamp between to_DATE('&&2', 'DD-MON-YYYY') and to_DATE('&&3', 'DD-MON-YYYY')+1
and it.code in ('Receipt','Return')
and substr(rrc.reason_id,1,2) = 'HP'
order by 2
)
/

--spool off