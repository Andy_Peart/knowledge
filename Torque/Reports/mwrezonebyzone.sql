SET FEEDBACK OFF             
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 300
SET TRIMSPOOL ON

--spool mwrezonebyzone.csv

--select to_char(SYSDATE, 'DD/MM/YY  HH:MI:SS') curdate from DUAL;

select 'Sku Code,Qty on Hand,Description,Work Zone,Location,LPG' from dual;

select
''''||PA||''','||
QQ||','||
DD||','||
WZ||','||
LL||','||
LPG
from
(select i.sku_id PA,
i.description DD,
L.work_zone WZ,
i.location_id LL,
sku.user_def_type_8 LPG,
sum(i.qty_on_hand) QQ
from inventory i,location L,sku
where i.client_id = 'MOUNTAIN'
and i.location_id=L.location_id
and i.site_id=L.site_id
and L.work_zone='&&2'
and sku.client_id = 'MOUNTAIN'
and sku.sku_id=i.sku_id
group by i.sku_id,i.description,L.work_zone,i.location_id,sku.user_def_type_8)
order by 1
/


--spool off
