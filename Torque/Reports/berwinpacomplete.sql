SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET COLSEP ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 200
SET TRIMSPOOL ON
SET HEADING OFF 

column Z heading 'Status' format A12
column A heading 'pre_advice' format A20
column B heading 'Line' format 999999
column C heading 'SKU' format A18
column D heading 'Qty1' format 9999999
column E heading 'Qty2' format 9999999
column F heading 'Qty3' format 9999999



--spool pac.csv

--select 'Berwin Completed,Pre Advice,Detail from &&2,to &&3' from DUAL;
--select to_char(SYSDATE, 'DD/MM/YY    HH:MI:SS') from DUAL;

select 'Pre Advice ID,Line ID,SKU ID,Qty Due,Qty Received,Variance' from DUAL;


break on report
compute sum label 'Total' of E D F on report


select
pl.pre_advice_id A,
pl.line_id B,
''''||pl.sku_id||'''' C,
pl.qty_due E,
it.update_qty D,
pl.qty_due-it.update_qty F
from inventory_transaction it,pre_advice_line pl
where it.client_id='BW'
and it.station_id not like 'Auto%'
and (it.code like 'Receipt%' or (it.code='Adjustment' and it.reason_id='RECRV'))
and it.dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1
and pl.client_id like 'BW'
and it.reference_id=pl.pre_advice_id
and it.sku_id=pl.sku_id
and it.line_id=pl.line_id
order by A,B
/

--spool off

