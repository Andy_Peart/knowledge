SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

 

select 'Docket,Cartons,Units,Received,Called Off,Division,' from DUAL;

select
i.sku_id||','||
sum(i.qty_on_hand)||','||
sku.user_def_type_1||','||
to_char(i.receipt_dstamp,'dd-Mon-yy')||','||
to_char(DD,'dd-Mon-yy')||','||
i.user_def_note_1||','||
' '
from inventory i,sku,(select
sku_id SS,
oh.order_date DD
from order_line ol,order_header oh
where ol.client_id='PR'
and ol.order_id=oh.order_id
and oh.client_id='PR')
where i.client_id='PR'
and i.site_id='PROM'
and i.sku_id=sku.sku_id
and sku.client_id='PR'
and i.sku_id=SS (+)
group by
i.sku_id,
sku.user_def_type_1,
to_char(i.receipt_dstamp,'dd-Mon-yy'),
to_char(DD,'dd-Mon-yy'),
i.user_def_note_1
order by
i.sku_id
/

