SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep '|'


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 300
SET TRIMSPOOL ON
 
column XX NOPRINT


--spool mwukpacmail.csv

select
order_id  XX,
'I360143'||'|'||
to_char(Sysdate,'DDMMYYYY')||'|'||
contact||'|'||
Name||'|'||
Address1||'|'||
Address2||'|'||
Town||'|'||
County||'|||'||
PostCode||'||'||
'1'||'|'||
'1000'||'|'||
'545'||'||'||
substr(instructions,1,30)||'|'||
substr(instructions,31,30)||'||||'||
--user_def_note_1||'|'||
--user_def_note_2||'|||||'||
order_id||'|||||||||||||||'||
'Torque Anchor Works Holme Lane Bradford BD4 6NA'||'||'||
' '
from order_header
where client_id='MOUNTAIN'
and consignment='&&2'
order by XX
/

--spool off

