SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ' '



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 140


column A heading 'SKU' format A16
column AA heading 'Description' format A42
column B1 heading 'CIMS Code' format A20
column B4 heading 'Size' format A10
column C heading 'Client' format A8
column D heading 'Qty' format 9999999
column DD heading 'Qty' format A8


select 'Prominent - Call Off Check for Order &&2' from DUAL;


break on report

compute sum label 'Total' of D on report

Select ' ' from DUAL;
SELECT 'ORDER QUANTITIES' From DUAL;


select 
'SKU' A,
'Description' AA,
'     Qty' DD
from DUAL;

select
ol.sku_id A,
sku.description AA,
nvl(to_number(sku.user_def_type_1),0) D
from order_line ol,sku
where ol.client_id='PR'
and ol.order_id='&&2'
and sku.client_id='PR'
and ol.sku_id=sku.sku_id
order by
A
/

Select ' ' from DUAL;
select 'PRE ADVICE RECEIPTS' from DUAL;


select 
'SKU' A,
'Description' AA,
'CIMS Code' B1,
'Size' B4,
'Client' C,
'     Qty' DD
from DUAL;

select
pl.sku_id A,
sku.description AA,
sku.user_def_type_2||+'-'||+sku.user_def_type_1||+'-'||+sku.user_def_type_4 B1,
sku.sku_size B4,
oh.ship_dock C,
nvl(pl.qty_received,0) D
from order_header oh,pre_advice_line pl,sku
where oh.client_id='PR'
and oh.order_id='&&2'
and pl.client_id=oh.ship_dock
and pl.pre_advice_id='&&2'
and sku.client_id='TR'
and pl.sku_id=sku.sku_id
order by
sku.user_def_type_4,
B1,
A
/
