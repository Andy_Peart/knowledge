/********************************************************************************************/
/*                                                                                          */
/*                            Elite                                                         */
/*                                                                                          */
/*     FILE NAME  :   GIVEgiftnote.sql                                                      */
/*                                                                                          */
/*     DESCRIPTION:    Datastream for GIVE Gift Note                                        */
/*                                                                                          */
/*   DATE     BY   PROJ       ID         DESCRIPTION                                        */
/*   ======== ==== ======     ========   =============                                      */
/*   30/09/09 RW   GIVE                  Gift Note                                          */
/*                                                                                          */
/********************************************************************************************/

/* Input Parameters are as follows */
-- 1)  GMT
-- 2)  Order_id
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 0
SET VERIFY OFF
SET FEEDBACK OFF
SET TAB OFF

 
-- Set the line length to the maximum possible, but trim 
-- trailing spaces for both screen output and spool files
SET LINESIZE 32767
SET WRAP ON
SET TRIMOUT ON
SET TRIMSPOOL ON


/* Define the format of the output dates */
DEFINE	OutputDateFormat = 'DD-MON-YYYY HH24:MI:SS'
COLUMN	sorter	NOPRINT

SELECT Distinct '0' sorter,
       'DSTREAM_GIVE_GIFT_HDR'           ||'|'||
       rtrim('&&2')                      /* Order_id */
from dual
union all
SELECT distinct '1' sorter,
'DSTREAM_GIVE_GIFTNOTE_HDR'              	||'|'||
rtrim (oh.order_id)				||'|'||
rtrim (ol.user_def_note_2)||chr(13)||chr(13)||chr(10)
from order_header oh, order_line ol 
where oh.order_id = '&&2'
AND oh.CLIENT_ID = 'GV'
and oh.order_type in ('HOMEWEB', 'HOMEMAIL')
and oh.user_def_chk_1 = 'Y'
and oh.order_id = ol.order_id
/