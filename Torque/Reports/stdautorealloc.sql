SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON

with T1 as (
select to_char(dstamp,'DD-MON-YYYY') as DatePicked, sku_id SKU, reference_id OrderID, auto_realloc
from (
select A.dstamp, A.code, A.Sku_id, A.reference_id, A.tag_id, B.dstamp ds2, (A.dstamp-B.dstamp) as timediff,substr((A.dstamp-B.dstamp),2,18) anc,
case 
  when substr((A.dstamp-B.dstamp),2,18) < '000000000 00:00:05' then 'Yes'
  else 'No'
end auto_realloc
from
(
select dstamp, code, reference_id, sku_id, tag_id
from inventory_transaction
where client_id = '&&2'
and dstamp between to_date('&&3') and to_date('&&4')+1
and code = 'Pick'
and update_qty = 0 
) A
left join
(
select max(dstamp) dstamp, reference_id, sku_id
from inventory_transaction
where client_id = '&&2'
and dstamp between to_date('&&3') and to_date('&&4')+2
and code = 'Allocate'
group by reference_id, sku_id
) B
on A.reference_id = B.reference_id and A.sku_id = B.sku_id
where B.dstamp is not null
)
order by 4,1,3
)
select 'Date Picked,SKU,OrderID,Auto re-allocation' from dual
union all
select DatePicked || ',' ||  SKU || ',' || OrderID || ',' || Auto_realloc from T1
/
