SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 90

column A heading 'User ID' format a14
column B heading 'Name' format a30
column C heading 'Picks' format 999999
column D heading 'Receipts' format 999999
column E heading 'Relocates' format 999999
column F heading 'Returns' format 999999
column G heading 'IDTs' format 999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


break on report

compute sum label 'Totals' of C D E F G on report

spool productivity.csv

select 'Productivity by User ID from &&2 to &&3 - for client  &&5' from DUAL;

select 'Log on,Name,Picks,Receipts,Relocates,Returns,IDTs' from DUAL;


select
user_id A,
name B,
nvl(JA,0) C,
nvl(JB,0) D,
nvl(JC,0) E,
nvl(JD,0) F,
nvl(JE,0) G
from application_user,
(select
user_id J1,
sum(update_qty) JA
from inventory_transaction
where Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and site_id='&&4'
and client_id='&&5'
and user_id like '%&&6%'
and update_qty<>0
and ((code='Pick' and to_loc_id='CONTAINER')
or (code='Pick' and to_loc_id<>'CONTAINER' and from_loc_id<>'CONTAINER'))
group by user_id),
(select
user_id J2,
sum(update_qty) JB
from inventory_transaction
where Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and site_id='&&4'
and client_id='&&5'
and user_id like '%&&6%'
and update_qty<>0
and station_id not like 'Auto%'
and code='Receipt'
and elapsed_time>0
group by user_id),
(select
user_id J3,
sum(update_qty) JC
from inventory_transaction
where Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and site_id='&&4'
and client_id='&&5'
and user_id like '%&&6%'
and update_qty<>0
and code='Relocate'
and elapsed_time>0
group by user_id),
(select
user_id J4,
sum(update_qty) JD
from inventory_transaction
where Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and site_id='&&4'
and client_id='&&5'
and user_id like '%&&6%'
and update_qty<>0
and code='Adjustment'
and reason_id like 'T%'
and reason_id<>'T760'
--and elapsed_time>0
group by user_id),
(select
user_id J5,
sum(update_qty) JE
from inventory_transaction
where Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and site_id='&&4'
and client_id='&&5'
and user_id like '%&&6%'
and update_qty<>0
and code='Adjustment'
and reason_id='T760'
--and elapsed_time>0
group by user_id)
where site_id='&&4'
and user_id like '%&&6%'
and user_id=J1 (+)
and user_id=J2 (+)
and user_id=J3 (+)
and user_id=J4 (+)
and user_id=J5 (+)
and nvl(JA,0)+nvl(JB,0)+nvl(JC,0)+nvl(JD,0)+nvl(JE,0)>0
order by user_id
/

spool off
