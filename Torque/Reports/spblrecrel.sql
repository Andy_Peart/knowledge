SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 2000
SET TRIMSPOOL ON

select 'Tag ID,SKU,Description,Qty,Transaction Date,Transaction Time' A1 from dual 
union all
select tag_id || ',' || sku_id || ',' || descrip || ',' || qty || ',' || date_ || ',' || time_
from (
    select tag_id, sku_id, libsku.getuserlangskudesc(client_id,sku_id) descrip, sum(update_qty) qty, to_char(dstamp, 'DD-MM-YYYY') date_, to_char(dstamp, 'HH24:MI') time_
    from inventory_transaction
    where client_id = 'SP'
    and code = 'Receipt'
    and reference_id is null
    and dstamp between to_date('&&2') and to_date('&&3')+1
    group by tag_id, sku_id, libsku.getuserlangskudesc(client_id,sku_id), to_char(dstamp, 'DD-MM-YYYY') , to_char(dstamp, 'HH24:MI') 
    union all
    select tag_id, sku_id, libsku.getuserlangskudesc(client_id,sku_id) descrip, sum(update_qty) qty, to_char(dstamp, 'DD-MM-YYYY') date_, to_char(dstamp, 'HH24:MI') time_
    from inventory_transaction
    where client_id = 'SP'
    and code = 'Relocate'
    and (from_loc_id = 'SPGOOD' or from_loc_id = 'SPDRY')
    and dstamp between to_date('&&2') and to_date('&&3')+1
    group by tag_id, sku_id, libsku.getuserlangskudesc(client_id,sku_id), to_char(dstamp, 'DD-MM-YYYY') , to_char(dstamp, 'HH24:MI') 
)
/