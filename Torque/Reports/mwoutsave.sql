clear columns
clear breaks
clear computes

set feedback off
set pagesize 0
set linesize 200
set verify off
set trimspool on



--ttitle 'Mountain Warehouse Despatch Quantity' skip 2

column A heading 'Order' format a10
column B heading 'SKU' format a16
column C heading 'Qty' format 99999999
column D heading 'Address' format a32

--spool mwoutsave.csv

select 'Date,Order ID,SKU,Qty,Name,E-Mail Address,' from DUAL;

--break on report

--compute sum label 'Total' of B on report

select
trunc(i.Dstamp)||','||
i.reference_id||','||
i.sku_id||','||
i.update_qty||','||
oh.name||','||
--oh.address1||','||
--oh.address2||','||
oh.inv_contact_email||','||
' '
from inventory_transaction i,order_header oh
where i.client_id = 'MOUNTAIN'
and i.code = 'Shipment'
--and i.condition_id='MMAIL'
and i.dstamp>trunc(sysdate)-7
and i.reference_id=oh.order_id
--and oh.order_id not like ('MOR%')
and oh.client_id='MOUNTAIN'
and oh.work_group='WWW'
order by
i.reference_id,
i.sku_id
/

--spool off
