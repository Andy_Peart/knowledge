set feedback off
set pagesize 68
set linesize 240
set verify off

clear columns
clear breaks
clear computes

column A heading 'Order No' format A10
column B heading 'Ordered' format A10
column C heading 'Shipped' format A10
column D heading 'SKU' format A20
column E heading 'Description' format A40
column F heading 'Ordered' format 9999
column G heading 'Tasked' format 9999
column H heading 'Picked' format 9999
column I heading 'Shipped' format 9999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

ttitle  LEFT 'Shipped Orders Incomplete - for client ID '&&2' as at ' report_date skip 2

break on report
compute sum label 'Total' of F G H I on report

select 
oh.order_id A,
to_char(order_date,'DD/MM/YY') B,
to_char(shipped_date,'DD/MM/YY') C,
ol.sku_id D,
sku.description E,
ol.qty_ordered F,
nvl(ol.qty_tasked,0) G,
nvl(ol.qty_picked,0) H,
nvl(ol.qty_shipped,0) I
from order_header oh,order_line ol,sku
where oh.client_id='&&2'
and oh.status='Shipped'
and oh.order_id=ol.order_id
and ((ol.qty_shipped<>qty_ordered) or (qty_shipped is null))
and ol.sku_id=sku.sku_id (+)
order by oh.order_id
/
