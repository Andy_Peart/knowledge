SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 200
set trimspool on
set newpage 0
set verify off
set colsep ','


column C format a4


break on A dup on report
compute sum label 'WK Total' of B B2 D on A
compute sum label 'Run Total' of B B2 D on report


--spool mwyear99.csv

select 'Week,Work Group,Orders,Picks,Units,Ave Units per Order' from DUAL;



select
A,
C,
B,
B2,
D,
replace(to_char(D/B,'99999'),' ','')
from (select
trunc(to_date('&&2', 'DD-MON-YYYY'))+(trunc(((trunc(oh.shipped_date))-(trunc(to_date('&&2', 'DD-MON-YYYY'))))/7)*7) A,
oh.work_group C,
count(distinct oh.order_id) B,
count(*) B2,
sum (ol.qty_shipped) D
from order_header oh,order_line ol
where oh.client_id = 'MOUNTAIN'
and oh.order_id like 'MOR%'
and oh.consignment not like '%GRID%'
and oh.consignment not like '%GAS%'
and oh.shipped_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and ol.client_id = 'MOUNTAIN'
and ol.order_id=oh.order_id
and ol.sku_id not like '3%'
and ol.sku_id<>'5704122000690'
and ol.qty_shipped>0
group by
trunc(to_date('&&2', 'DD-MON-YYYY'))+(trunc(((trunc(oh.shipped_date))-(trunc(to_date('&&2', 'DD-MON-YYYY'))))/7)*7),
oh.work_group
union
select
trunc(to_date('&&2', 'DD-MON-YYYY'))+(trunc(((trunc(oh.shipped_date))-(trunc(to_date('&&2', 'DD-MON-YYYY'))))/7)*7) A,
oh.work_group C,
count(distinct oh.order_id) B,
count(*) B2,
sum (ol.qty_shipped) D
from order_header_archive oh,order_line_archive ol
where oh.client_id = 'MOUNTAIN'
and oh.order_id like 'MOR%'
and oh.consignment not like '%GRID%'
and oh.consignment not like '%GAS%'
and oh.shipped_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and ol.client_id = 'MOUNTAIN'
and ol.order_id=oh.order_id
and ol.sku_id not like '3%'
and ol.sku_id<>'5704122000690'
and ol.qty_shipped>0
group by
trunc(to_date('&&2', 'DD-MON-YYYY'))+(trunc(((trunc(oh.shipped_date))-(trunc(to_date('&&2', 'DD-MON-YYYY'))))/7)*7),
oh.work_group)
order by A,C
/

--spool off
