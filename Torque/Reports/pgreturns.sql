SET FEEDBACK OFF                
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 400
SET TRIMSPOOL ON


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

--spool pgreturns.csv

select 'Units Returned from &&2 to &&3 - for client PING as at '||to_char(SYSDATE, 'DD/MM/YY  HH:MI') from DUAL;

select 'Date,Qty Returned' from DUAL;

select 
D||','||
B
from (select
trunc(it.Dstamp) D,
sum(it.update_qty) as B
from inventory_transaction it
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='PG'
and it.station_id not like 'Auto%'
and it.code like 'Receipt%'
and it.from_loc_id='PGRETURN'
group by
trunc(it.Dstamp)
order by D)
/

--spool off
