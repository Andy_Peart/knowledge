SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500

column F heading 'Qty1' format 99999
column G heading 'Qty2' format 99999
column H heading 'Qty3' format 99999



--SET TERMOUT OFF
--SPOOL VDS.csv


--select 'SKU', 'MV REF', 'Description', 'Colour', 'Size', 'Product Group', 'Line', 'Collection', 'Total Quantity', 'Quantity Allocated', 'Quantity Free,' from dual;

select 'SKU,MV_REF,Description,Colour,Size,Product_Group,Line,Collection,Total Quantity,Quantity_Allocated,Quantity_Free,' from dual;

--break on report 

--compute sum label 'Total' of F on report
--compute sum label 'Total' of G on report
--compute sum label 'Total' of H on report

select
s.sku_id ||','||
s.user_def_type_5 ||','||
s.description ||','||
s.colour ||','||
s.sku_size ||','||
s.product_group ||','||
s.user_def_type_1 ||','||
s.user_def_type_2 ||','||
sum(nvl(i.qty_on_hand, 0)) ||','||
sum(nvl(i.qty_allocated, 0)) ||','||
sum(nvl(i.qty_on_hand,0) - nvl(i.qty_allocated,0)) ||','||
' '
from sku s, inventory i
where s.client_id = 'V'
and s.sku_id = i.sku_id (+)
and (nvl(i.qty_on_hand,0)<>0 or s.obsolete_product<>'Y')
group by
s.sku_id,
s.user_def_type_5,
s.description,
s.colour,
s.sku_size,
s.product_group,
s.user_def_type_1,
s.user_def_type_2
order by
s.sku_id
/



--SPOOL OFF
--SET TERMOUT ON
