set pagesize 64
set linesize 2000
set verify off
clear columns
clear breaks
clear computes

set heading on
set feedback off

column A heading 'SKU' format a20
column B heading 'Total Qty' format 999999

select i.sku_id A,
B
from inventory i,
(select inv.sku_id C,
sum(qty_on_hand) B,
sum(qty_allocated) D
from inventory inv
where inv.client_id = '&&3'
and inv.location_id not in ('CONTAINER','VDESP','HOLD','IN','IN2','IN3','IN4','IN5','PACKM','PACKSB')
group by inv.sku_id)
where client_id = '&&3'
and B < '&&2'
--and D=0
and i.sku_id = C
group by i.sku_id, B
order by 1
/


