SET FEEDBACK OFF             
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 300
SET TRIMSPOOL ON

--spool mwLPGOpicks.csv

select 'LPG Picks for Order Id &&2' from DUAL;
select 'Date,LPG,Picks,Units Picked' from DUAL;

select
A||','||
B||','||
LNS||','||
IQTY
from (select
trunc(it.Dstamp) A,
nvl(sku.user_def_type_8,'ZZZZ') B,
count(*) LNS,
sum(it.update_qty) IQTY
from inventory_transaction it,sku
where it.client_id='MOUNTAIN'
and it.reference_id like '&&2'
and it.code like 'Pick'
and elapsed_time>0
and it.sku_id=sku.sku_id
and it.client_id=sku.client_id
group by
trunc(it.Dstamp),
nvl(sku.user_def_type_8,'ZZZZ')
union
select
trunc(it.Dstamp) A,
nvl(sku.user_def_type_8,'ZZZZ') B,
count(*) LNS,
sum(it.update_qty) IQTY
from inventory_transaction_archive it,sku
where it.client_id='MOUNTAIN'
and it.reference_id like '&&2'
and it.code like 'Pick'
and elapsed_time>0
and it.sku_id=sku.sku_id
and it.client_id=sku.client_id
group by
trunc(it.Dstamp),
nvl(sku.user_def_type_8,'ZZZZ'))
order by
A,B
/


--spool off
