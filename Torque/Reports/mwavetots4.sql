SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 50
set newpage 0
set verify off
set colsep ','



column A heading 'W/C' format A12
column C heading 'Branch' format A8
column B format 999999
column D format 999999
column E format 9999.99
column F format 999 noprint
column G format 999999
column H format 9999.99
column J format 999999

break on A on report

compute sum label 'Totals' of B D J on report

--spool newtots4.csv

select 'Period &&2 to &&3' from DUAL;

select 'W/C,Store,Orders,Shipments,Units,' from DUAL;

select
to_char(trunc(to_date('&&2', 'DD-MON-YYYY'))+(AAA*7),'DD-MON-YYYY') A,
AAA F,
CCC C,
JJJ J,
BBB B,
DDD D,
' '
from (select
trunc(((trunc(Dstamp))-(trunc(to_date('&&2', 'DD-MON-YYYY'))))/7) AAA,
customer_id CCC,
count(*) BBB,
count(distinct reference_id) JJJ,
sum (update_qty) DDD
from inventory_transaction
where client_id = 'MOUNTAIN'
and code = 'Pick'
--and customer_id<>'WWW'
and dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')
group by
trunc(((trunc(Dstamp))-(trunc(to_date('&&2', 'DD-MON-YYYY'))))/7),
customer_id)
order by F,C
/


--spool off
