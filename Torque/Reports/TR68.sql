SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 160


column A heading 'PA Ref' format a12
column B heading 'Code' format a12
column C heading 'Size' format a12
column D heading 'Qty' format 999999
column E heading 'Condition' format a12
column F heading 'SKU' format a18


--break on A skip 1 dup on report

--compute sum label 'Total' of D on A
--compute sum label 'Total' of D on report


--set termout off
--spool TR068.csv

--select 'TR63 - Receipts for '||to_char(sysdate-1,'DD/MM/YYYY') from Dual;

select 'Product Group,Product Code,Sub Group,Look,ProductCode,Description,Size,SKU ID,All Stock,Other,QC Hold,Picked,Hold,Allocated,Suspense,Net Stock,' from DUAL;


select
SKU.PRODUCT_GROUP ||','|| 
SKU.USER_DEF_TYPE_1 ||','||
SKU.USER_DEF_TYPE_6 ||','||
SKU.USER_DEF_TYPE_2 ||','|| 
SKU.USER_DEF_TYPE_3 ||','||
SKU.DESCRIPTION ||','||
SKU.SKU_SIZE ||','''||
JA ||''','||
nvl(JB,0) ||','||
nvl(JB2,0) ||','||
nvl(JB3,0) ||','||
nvl(JB33,0) ||','||
nvl(JB5,0) ||','||
nvl(JC,0) ||','||
nvl(JB4,0) ||','||
--nvl(JB,0)-nvl(JB2,0)-nvl(JB3,0)-nvl(JB4,0)-nvl(JB5,0)-nvl(JC,0)
nvl(JB-nvl(JB2,0)-nvl(JB3,0)-nvl(JB33,0)-nvl(JB4,0)-nvl(JB5,0)-nvl(JC,0),0)
from
(select
i.sku_id JA,
count(*) JX,
sum(i.qty_on_hand) JB,
sum(i.qty_allocated) JC
from inventory i
where i.client_id='TR'
and i.sku_id not like '**%'
group by
i.sku_id),
(select distinct jc.sku_id JA2,sum(jc.qty_on_hand) JB2 
from inventory jc
where jc.client_id='TR'
--and jc.location_id in ('I1','I2','I3')
and jc.condition_id not in ('GOOD','HOLD','QCHOLD','Allocated')
group by jc.sku_id
order by jc.sku_id),
(select distinct jc.sku_id JA3,sum(jc.qty_on_hand) JB3 
from inventory jc
where jc.client_id='TR'
and jc.condition_id like 'QCHOLD'
group by jc.sku_id
order by jc.sku_id),
(select distinct jc.sku_id JA33,sum(jc.qty_on_hand) JB33 
from inventory jc
where jc.client_id='TR'
and (jc.location_id like 'container%' or jc.location_id like 'shipdock%')
group by jc.sku_id
order by jc.sku_id),
(select distinct jc.sku_id JA5,sum(jc.qty_on_hand) JB5 
from inventory jc
where jc.client_id='TR'
and jc.condition_id like 'HOLD'
group by jc.sku_id
order by jc.sku_id),
(select distinct jc.sku_id JA4,sum(jc.qty_on_hand) JB4 
from inventory jc
where jc.client_id='TR'
and jc.location_id='SUSPENSE'
group by jc.sku_id
order by jc.sku_id),sku
where sku.client_id='TR'
and JA=sku.sku_id
and JA=JA2 (+)
and JA=JA3 (+)
and JA=JA33 (+)
and JA=JA4 (+)
and JA=JA5 (+)
order by JA
/

--spool off
--set termout on
