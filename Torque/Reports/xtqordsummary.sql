SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Shipped Date,Cost Centre,Sku,Description,Units Shipped,Total Sale Value' from dual
union all
select shipped_date ||','|| user_def_type_2 ||','|| sku_id ||','|| description ||','|| qty_shipped ||','|| each_value
from
(
    select to_char(oh.shipped_date, 'DD-MON-YYYY') shipped_date
    , oh.user_def_type_2
    , ol.sku_id
    , s.description
    , sum(ol.qty_shipped) qty_shipped
    , sum(s.each_value) each_value
    from order_header oh
    inner join order_line ol on ol.client_id = oh.client_id and ol.order_id = oh.order_id
    inner join sku s on s.client_id = ol.client_id and s.sku_id = ol.sku_id
    where oh.client_id = 'TQ'
    and oh.status = 'Shipped'
    and oh.shipped_date between to_date('&&2') and to_date('&&3')+ 1
    group by to_char(oh.shipped_date, 'DD-MON-YYYY')
    , oh.user_def_type_2
    , ol.sku_id
    , s.description
)
/
--spool off