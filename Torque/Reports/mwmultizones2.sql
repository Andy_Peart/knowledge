SET FEEDBACK OFF                 
set verify off
set wrap off
set colsep ','

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 132
set trimspool on


select 'SKU,Description,Locations(Stock),Receipt Date, Work Zone' from DUAL
union all
select
SK||','||
LD||' ('||QQ||')'||','||
RD||','||wz
from (select 
i.SKU_ID||''','||upper(i.description) SK,
i.location_id LD,
trunc(i.receipt_dstamp) RD,
sum(i.qty_on_hand) QQ, L.work_zone wz
from inventory i,(select
i.sku_id A,
count(distinct i.location_id) B
from inventory i,location L
where i.client_id = 'MOUNTAIN'
and i.location_id not like 'Z%'
and i.location_id not in
('MAIL ORDER',
'MARSHALL',
'DESPATCH',
'STG',
'CONTAINER',
'SUSPENSE',
'UKPAC',
'UKPAR')
and i.location_id=L.location_id
and i.site_id=L.site_id
and L.work_zone in ('12A', '12B')
group by
sku_id),location L
where B > 1
and i.location_id not like 'Z%'
and i.location_id not in
('MAIL ORDER',
'MARSHALL',
'DESPATCH',
'STG',
'CONTAINER',
'SUSPENSE',
'UKPAC',
'UKPAR')
and i.sku_id = A
and i.location_id=L.location_id
and i.site_id=L.site_id
and L.work_zone in ('12A', '12B')
group by
L.work_zone,
i.SKU_ID,
i.description,
i.location_id,
trunc(i.receipt_dstamp))
/