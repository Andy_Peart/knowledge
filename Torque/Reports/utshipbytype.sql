SET FEEDBACK OFF                 
set pagesize 68
set linesize 132
set verify off
set wrap off
set newpage 0

clear columns
clear breaks
clear computes


column B heading 'Order Type' format a20
column C heading 'Orders' format 999999
column D heading 'Units' format 999999
column F heading 'Date' format a12

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



ttitle  LEFT 'Shipments by Date from &&2 to &&3 - for client ID UT as at ' report_date skip 2

break on REPORT on F skip 1

compute sum label 'Grand Total' of C D on REPORT
compute sum label 'Day Total' of C D on F 
 

select F,B,C,D
from (
select F,B,C,D 
from (
select F, B, count(*) C, sum(D) D
from (
SELECT F, B, G, C, D FROM (
select
to_char(it.Dstamp, 'DD/MM/YY') F,
DECODE(oh.order_type,
	'WEB',DECODE(oh.ship_dock,'UTNET',oh.user_def_type_1,oh.ship_dock),
	'CONCESSION',DECODE(substr(oh.customer_id,1,2),'RS','CONCESSION RETAIL','OA','CONCESSION OASIS',oh.order_type),
	oh.order_type) B,
it.reference_id G,  
count(*) C,
sum(it.update_qty) D
FROM INVENTORY_TRANSACTION IT inner join ORDER_HEADER OH on IT.REFERENCE_ID=OH.ORDER_ID
WHERE IT.DSTAMP BETWEEN TO_DATE('&&2', 'DD-MON-YYYY') AND TO_DATE('&&3', 'DD-MON-YYYY')+1
and IT.CLIENT_ID='UT'
and it.update_qty<>0
and it.code='Shipment'
and nvl(OH.CUSTOMER_ID,' ') NOT LIKE 'HF%'
group by
to_char(Dstamp, 'DD/MM/YY'),
DECODE(oh.order_type,'WEB',DECODE(oh.ship_dock,'UTNET',oh.user_def_type_1,oh.ship_dock),'CONCESSION',DECODE(substr(oh.customer_id,1,2),'RS','CONCESSION RETAIL','OA','CONCESSION OASIS',oh.order_type),oh.order_type),
reference_id
)
)
group by F, B
)
union all
select F,B,C,D
from (
select F, B, count(*) C, sum(D) D
from (
SELECT F, B, G, C, D FROM (
select
TO_CHAR(IT.DSTAMP, 'DD/MM/YY') F,
'CONCESSION_HOF' B,
it.reference_id G,  
count(*) C,
sum(it.update_qty) D
FROM INVENTORY_TRANSACTION IT, ORDER_HEADER OH
WHERE IT.DSTAMP BETWEEN TO_DATE('&&2', 'DD-MON-YYYY') AND TO_DATE('&&3', 'DD-MON-YYYY')+1
AND IT.CLIENT_ID='UT'
and oh.order_type like 'CONCESSION'
and it.update_qty<>0
and it.code='Shipment'
AND IT.REFERENCE_ID=OH.ORDER_ID
AND nvl(OH.CUSTOMER_ID,' ') LIKE 'HF%'
group by
to_char(Dstamp, 'DD/MM/YY'),
oh.order_type, it.reference_id
)
)
group by F, B
)
)
order by 1,2
/