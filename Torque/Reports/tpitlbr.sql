SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON


--spool tpitlbr.csv

select 'ITL,E,Receipt'||','
--||decode(sign(A1), 1, '+', -1, '-', '+')||','
--||A1||','
||''||','
||''||','
||decode(sign(A2), 1, '+', -1, '-', '+')||','
||A2||','
||AA||'000000,'
||'TP'||','
||SS||','
||''||','
||''||','
||''||','
||''||','
||RR||','
||''||','
||CI||','
||''||','
||RI||','
||''||','
||''||','
||''||','
||''||','
||''||','
--||SI||','
||'Bradford'||','
||''||','
||PI||','
||''||','
||OI||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||SUP||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||U4||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||'1'||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
||''||','
from (select
to_char(it.dstamp, 'YYYYMMDD') AA,
it.client_id CT,
it.sku_id SS,
it.reference_id RR,
it.condition_id CI,
it.reason_id RI,
it.site_id SI,
it.pallet_id PI,
it.owner_id OI,
--DECODE(it.reference_id,'AMAZONEU','AMAZONEU','BOOTNG90','BOOTNG90','MOTHWD24','MOTHWD24','LITTLL70','LITTLL70','JDWILM60','JDWILM60',nvl(CST,it.supplier_id)) SUP,
DECODE(substr(it.reference_id,1,2),'AM','AMAZONEU','BO','BOOTNG90','FR','FREEMAN','JD','JDWILM60','LI','LITTLL70','MO','MOTHWD24','JO','JOHN LEW',nvl(CST,it.supplier_id)) SUP,
'N' U4,
sum(it.original_qty) A1,
sum(it.update_qty) A2
from inventory_transaction it,(select
distinct
ORD,
CST
from (select
order_id ORD,
customer_id CST
from order_header
where client_id='TP'
union
select
order_id ORD,
customer_id CST
from order_header_archive@live
where client_id='TP'
union
select
order_id ORD,
customer_id CST
from order_header@live
where client_id='TP'))
where it.client_id = 'TP'
and (it.code = 'Receipt' or (it.code='Adjustment' and it.reason_id='RECRV'))
and it.tag_id like 'RET%'
and nvl(it.uploaded,'N') = 'N'
--and it.reference_id='0000035491'
--and trunc(it.Dstamp)=trunc(sysdate)-1
and it.reference_id=ORD (+)
group by
to_char(it.dstamp, 'YYYYMMDD'),
it.client_id,
it.sku_id,
it.reference_id,
it.condition_id,
it.reason_id,
it.site_id,
it.pallet_id,
it.owner_id,
--DECODE(it.reference_id,'AMAZONEU','AMAZONEU','BOOTNG90','BOOTNG90','MOTHWD24','MOTHWD24','LITTLL70','LITTLL70','JDWILM60','JDWILM60',nvl(CST,it.supplier_id)))
DECODE(substr(it.reference_id,1,2),'AM','AMAZONEU','BO','BOOTNG90','FR','FREEMAN','JD','JDWILM60','LI','LITTLL70','MO','MOTHWD24','JO','JOHN LEW',nvl(CST,it.supplier_id)))
where A2>0
order by
RR,SS
/

--spool off

SET FEEDBACK ON

update inventory_transaction
set uploaded='Y'
where client_id = 'TP'
and (code = 'Receipt' or (code='Adjustment' and reason_id='RECRV'))
and tag_id like 'RET%'
and nvl(uploaded,'N') = 'N'
--and reference_id not in (select
--pre_advice_id from pre_advice_header
--where client_id ='TP')
/




--commit;
rollback;

