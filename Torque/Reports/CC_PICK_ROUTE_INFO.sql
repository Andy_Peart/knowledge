SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ,
set trim on



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 32766
SET TRIMSPOOL ON


select odl.order_id ||','|| odl.sku_id||','|| sku.ean||','|| odl.qty_ordered||','|| odl.qty_tasked||','|| sku.description||','|| sku.product_group||','|| sku.ean
from order_line odl
inner join sku on odl.sku_id = sku.sku_id and odl.client_id = sku.client_id
where odl.client_id = 'CC'
and odl.order_id = '&&2'
/