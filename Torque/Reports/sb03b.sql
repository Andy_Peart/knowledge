SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

Select 'Status,Order Type,RP Priority,Invoice Name,Customer Name,Sales Order ID,Count of Ordered Items,Qty Ordered,Qty Picked,Qty Tasked,Qty Shipped,Variance,Web ID' from DUAL;


column A format A12
column A1 format A12
column A2 format A12
column B format A12
column C format A12
column CC format A12
column DD format A36
column D format A36
column E format A12
column EE format A12
column EEE noprint
column F format 99999
column G format 99999
column H format 99999
column I format 99999
column J format 99999
column K format 99999
column KK format 99999
column L format A16
column M format 9 noprint

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



--ttitle  LEFT 'TML Retail - Open Orders Report ' RIGHT 'Printed ' report_date skip 2


break on report

compute sum label 'Total' of F G H I J K on report

select
oh.STATUS A,
DECODE (oh.status,'In Progress','1',
		  'Allocated','2','3') M,
oh.order_type B,
oh.priority C,
oh.INV_NAME DD,
oh.NAME D,
oh.ORDER_ID E,
count(*) F,
sum(ol.QTY_ORDERED) G,
sum(nvl(ol.qty_picked,0)) H,
sum(nvl(ol.qty_tasked,0)) I,
sum(nvl(ol.qty_shipped,0)) J,
sum(ol.QTY_ORDERED)-sum(nvl(ol.qty_tasked,0))-sum(nvl(ol.qty_shipped,0)) K,
oh.USER_DEF_TYPE_7 L
from order_header oh,order_line ol
where oh.CLIENT_ID='SB'
and oh.order_ID like 'C%'
and oh.order_ID=ol.order_id
and oh.status in ('Released','Allocated','In Progress')
group by
oh.STATUS,
oh.order_type,
oh.priority,
oh.INV_NAME,
oh.NAME,
oh.order_id,
oh.USER_DEF_TYPE_7
order by
M,E
/

Select '  ' from Dual;
Select '  ' from Dual;
Select '  ' from Dual;
Select 'Explanation of STATUS codes:' from Dual;
Select 'Released - order has been received and awaits allocation to run' from Dual;
Select 'Allocated - stock has been allocated to this order but picking hasnt started yet' from Dual;
Select 'In Progress - order is still being picked' from Dual;

