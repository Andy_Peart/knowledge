SET FEEDBACK OFF           
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 300
SET TRIMSPOOL ON


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



--break on report

--compute sum label 'Totals' of Q4 B4 F4 G4 on report

--spool sbpricextract.csv

select 'Sku Id,Size,Description,ATTR,Each Value,User_def_num_4,Barcode,Group,Range,Season' from DUAL;

select 
SKU_ID||','||
SKU_SIZE||','|| 
'"'||DESCRIPTION||'",'||
COLOUR||','||
to_char(EACH_VALUE,'L99G999D99')||','|| 
to_char(USER_DEF_NUM_4,'L99G999D99')||','||
SKU_ID||','||
PRODUCT_GROUP||','||
USER_DEF_TYPE_5||','|| 
USER_DEF_TYPE_3
from sku
where client_id = 'SB'
order by sku_id
/

--spool off
