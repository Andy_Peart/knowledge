SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON


--spool tpreceipts.csv

select 'TIPPITOES receipts from &&2 to &&3' from DUAL;
select 'SKU,Qty,Condition,Reference,User Def Note 1,To Location,Transaction Date' from DUAL;

select
A||','||
B||','||
C||','''||
D||''','||
E||','||
F||','||
G
from (select
sku_id A,
update_qty B,
condition_id C,
reference_id D,
user_def_note_1 E,
to_loc_id F,
trunc(Dstamp) G
from inventory_transaction
where client_id='TP'
and code like 'Receipt%'
and Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
union
select
sku_id A,
update_qty B,
condition_id C,
reference_id D,
user_def_note_1 E,
to_loc_id F,
trunc(Dstamp) G
from inventory_transaction_archive
where client_id='TP'
and code like 'Receipt%'
and Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
order by
G,D)
/

--spool off
