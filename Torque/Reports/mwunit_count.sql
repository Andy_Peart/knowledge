/* Unit Count Report */

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 68
SET NEWPAGE 0
SET LINESIZE 132
 
/* Top Title */
--TTITLE CENTER _SITENAME SKIP 2 -
TTITLE LEFT "MOUNTAIN WAREHOUSE UNIT COUNT BY STOCK STATUS" skip 2

break on site_id skip 2 on report
compute sum label 'Total' of units on site_id

column site_id heading 'Site'
column units heading 'Units' format 99999999
column B heading 'Condition' format a12 

select site_id,
       sum(qty_on_hand) units,
       condition_id B
from inventory
where client_id = 'MOUNTAIN'
and zone_1 <> 'Z3OUT'
group by site_id, condition_id
/

