/******************************************************************************/
/*                                                                            */
/*                            Elite                                           */
/*                                                                            */
/*     FILE NAME  :   mwshortages3.sql                                        */
/*                                                                            */
/*   DATE     BY   PROJ     ID         DESCRIPTION                            */
/*   ======== ==== ======   ========   =============                          */
/*   22/01/06 BK   Mountain            Units Picked by Date Range             */
/******************************************************************************/

/* Input Parameters are as follows */
-- 1)  From Date
-- 2)  To Date


set feedback off
set pagesize 68
set linesize 240
set verify off

clear columns
clear breaks
clear computes


column AA heading 'Order Type' format A12
column A heading 'User ID' format A16
column X heading 'Zone' format A6
column B heading 'Units' format 999999
column C heading 'Picks' format 999999
column D heading 'Orders' format 999999
column E heading 'Ave Units per Order' format 99990.99


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'Units Picked from '&&2' to '&&3' - for client ID MOUNTAIN and AA as at ' report_date skip 2

break on AA dup skip 2 on A dup skip 1 
--compute sum label 'Total' of B C D E on AA
--compute sum label 'Full Total' of B C D E on report

select nvl(oh.order_type,'  ') AA, it.user_id A, '_All' X, sum(it.update_qty) B, count(*) C, count(distinct reference_id) D, sum(it.update_qty)/count(distinct reference_id) E
from inventory_transaction it inner join order_header oh on it.client_id = oh.client_id and it.reference_id = oh.order_id
where it.client_id in ('MOUNTAIN','AA')
and it.code='Pick'
and to_loc_id in ('DESPATCH','MAIL ORDER')
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by oh.order_type, it.user_id
union
select nvl(oh.order_type,'  ') AA, it.user_id A, ll.work_zone X, sum(it.update_qty) B, count(*) C, count(distinct reference_id) D, sum(it.update_qty)/count(distinct reference_id) E
from inventory_transaction it inner join order_header oh on it.reference_id = oh.order_id and it.client_id = oh.client_id
inner join location ll on ll.location_id = it.from_loc_id and it.site_id = ll.site_id
where it.client_id in ('MOUNTAIN','AA')
and it.code='Pick'
and to_loc_id in ('DESPATCH','MAIL ORDER')
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by oh.order_type, it.user_id, ll.work_zone
union
select nvl(oh.order_type,'  ') AA, '____Totals' A, ' ' X, sum(it.update_qty) B, count(*) C, count(distinct reference_id) D, sum(it.update_qty)/count(distinct reference_id) E
from inventory_transaction it inner join order_header oh on it.reference_id = oh.order_id and it.client_id = oh.client_id
where it.client_id in ('MOUNTAIN','AA')
and it.code='Pick'
and to_loc_id in ('DESPATCH','MAIL ORDER')
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by oh.order_type
order by AA,A
/
