SET FEEDBACK OFF                 
set pagesize 68
set linesize 160
set verify off

clear columns
clear breaks
clear computes

column X heading '' format A14
column C heading 'Client ID' format a10
column D heading 'Date' format a16
column A heading 'Advice ID' format a16
column B heading 'Received' format 99999999
column M format a4 noprint

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'Total Units Received from &&2 to &&3 - for client ID MM as at ' report_date skip 2

break on X skip 2 on report
compute sum label 'Totals' of B on X
compute sum label 'Grand Totals' of B on report


select
'MM ' X,
to_char(it.Dstamp, 'YYMM') M,
to_char(it.Dstamp, 'DD/MM/YY') D,
--it.user_def_type_6 A,
--it.client_id C,
sum(it.update_qty) as B
from inventory_transaction it
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='MM'
and it.station_id not like 'Auto%'
and it.code like 'Receipt%'
and (it.sku_id not like 'V%'
or it.sku_id like 'VY%')
group by
to_char(it.Dstamp, 'YYMM'),
to_char(it.Dstamp, 'DD/MM/YY')
union
select
'VH ' X,
to_char(it.Dstamp, 'YYMM') M,
to_char(it.Dstamp, 'DD/MM/YY') D,
--it.user_def_type_6 A,
--it.client_id C,
sum(it.update_qty) as B
from inventory_transaction it
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='MM'
and it.station_id not like 'Auto%'
and it.code like 'Receipt%'
and it.sku_id like 'V%'
and it.sku_id not like 'VY%'
group by
to_char(it.Dstamp, 'YYMM'),
to_char(it.Dstamp, 'DD/MM/YY')
order by
X,M,D
/

