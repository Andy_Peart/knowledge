SET FEEDBACK OFF                 
set pagesize 66
set linesize 110
set verify off
set wrap off
set newpage 0

clear columns
clear breaks
clear computes


column A heading 'Site' format a12
column B heading 'Location' format a12
column C heading 'Loc Type' format a12
column D heading 'Check String' format a16
column E heading 'Lock Status' format a12
column F heading 'In Stage' format a12
column G heading 'Out Stage' format a12
column H heading 'Pick Sequence' format 999999999



set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD-MON-YYYY  HH:MI:SS') curdate from DUAL;
set TERMOUT ON


TTITLE LEFT 'Empty Locations By Range for Zone &&4' RIGHT report_date skip 1 -
RIGHT 'Page: ' Format 999 SQL.PNO skip 2 -
LEFT 'Location Range from &&2 to &&3' skip 2

BREAK ON site_id skip page
 

select
site_id A,
location_id B,
loc_type C,
check_string D,
lock_status E,
in_stage F,
out_stage G,
pick_sequence H
from location
where current_volume=0
and nvl(alloc_volume,0)=0
and location_id between '&&2' and '&&3'
and zone_1='&&4'
order by location_id
/
