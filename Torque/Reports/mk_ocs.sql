/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :     mk_ocs.sql                              		      */
/*                                                                            */
/*     DESCRIPTION:                                                           */
/*     MK csv ocs report to excel                                             */
/*                                                                            */
/*   DATE       BY   DESCRIPTION					                          */
/*   ========== ==== ===========				                              */
/*   27/12/2012 YB   initial version   				                          */
/******************************************************************************/
SET FEEDBACK OFF           
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 350
SET TRIMSPOOL ON

break on A skip 1 dup

COLUMN NEWSEQ NEW_VALUE SEQNO


SELECT   
'"'||
B.CONTACT||'","'||
b.address1||'","'||
b.address2||'","'||
b.town||'","'||
b.county||'","'||
b.postcode||'","'||
d.TANDATA_ID||'","'||
b.country||'","'||
'0'||'","'||
b.contact_email||'","'||
b.order_id||'","'||
trunc(a.dstamp)||'","'||
e.description||'","'||
a.update_qty||'","'||
c.user_def_type_2||'","'||
c.line_value||'","'||
''||'","'||
''||'","'||
e.product_group||'","'||
F.CONTAINER_ID||'","'||
case when b.priority = '6' then 'M' else 'U' end||'"'
from inventory_transaction a
inner join order_header b on a.client_id = b.client_id and a.reference_id = b.order_id
inner join order_line c on b.client_id = c.client_id and b.order_id = c.order_id and c.line_id = a.line_id
left join country d on b.country = d.Iso3_Id
inner join sku e on b.client_id = e.client_id and c.sku_ID = e.sku_ID
inner join shipping_manifest f on c.client_id = f.client_id and c.order_id = f.order_id and c.sku_ID = f.sku_ID
where a.client_id = 'MK'
and b.client_id = 'MK'
and a.code = 'Shipment' 
AND b.priority in ( 5,6 )
and A.from_loc_id = 'MKWEB'
/* and b.dispatch_method in ('P','U','M') */
and A.dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
/


--spool off




