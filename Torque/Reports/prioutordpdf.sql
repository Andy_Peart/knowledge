/******************************************************************************/
/*                                                                            */
/*                            Elite                                           */
/*                                                                            */
/*     FILE NAME  :   prioutordpdf.sql                                        */
/*                                                                            */
/*     DESCRIPTION:    Datastream for Pringle Outstanding Orders Report (PDF) */
/*                                                                            */
/*   DATE     BY   PROJ       ID         DESCRIPTION                          */
/*   ======== ==== ======     ========   =============                        */
/*   20/11/06 LH   Pringle               Outstanding Orders Report (PDF)      */
/******************************************************************************/

/* Input Parameters are as follows */
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 0
SET VERIFY OFF
SET FEEDBACK OFF
SET TAB OFF

 
-- Set the line length to the maximum possible, but trim 
-- trailing spaces for both screen output and spool files
SET LINESIZE 32767
SET WRAP ON
SET TRIMOUT ON
SET TRIMSPOOL ON


/* Define the format of the output dates */
DEFINE	OutputDateFormat = 'DD-MON-YYYY HH24:MI:SS'
COLUMN	sorter	NOPRINT

break on oh.status

SELECT '0' sorter,
'DSTREAM_PRI_OUTORDPDF_HDR'				||'|'||
rtrim (sysdate)		
from dual			
union all
SELECT unique '0' ||oh.creation_date sorter,
'DSTREAM_PRI_OUTORDPDF_LINE'              ||'|'||
rtrim (oh.status)						||'|'||
rtrim (oh.creation_date)	||'|'||
rtrim ('order_type')		||'|'||
rtrim (case oh.priority when 1 then 5
when 2 then 4
when 3 then 3
when 4 then 2
when 5 then 1
else 0 end)		||'|'||
rtrim (oh.inv_name)			||'|'||
rtrim (oh.name)		||'|'||
rtrim (oh.order_id)		||'|'||
rtrim (sum(ol.qty_ordered))		||'|'||
rtrim (oh.user_def_note_2)
from order_header oh, order_line ol
where oh.order_id = ol.order_id
and oh.client_id = 'PRI'
and status <> 'Shipped'
and status <> 'Cancelled'
group by oh.status, oh.creation_date, oh.priority, oh.inv_name, oh.name, oh.order_id, oh.user_def_note_2;

exit;
