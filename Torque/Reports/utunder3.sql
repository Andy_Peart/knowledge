SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 200
set trimspool on
set newpage 0
set verify off
set colsep ','

--spool utunder3.csv

select 'SKU,Location,Qty,Other Locations' from DUAL;

select
B2||','||
L2||','||
C2||','||
LD
from (select
iv.sku_id B2,
iv.location_id L2,
sum(iv.qty_on_hand) C2
from inventory iv,location L
where iv.client_id='UT'
and iv.location_id=L.location_id
and iv.site_id=L.site_id
and L.loc_type='Tag-FIFO'
group by
iv.sku_id,iv.location_id
having sum(iv.qty_on_hand)<4)
,(select
SK,
LISTAGG(LD, ':') WITHIN GROUP (ORDER BY LD) LD
from (select
SK,
LD||'('||QQ||')' LD
from (select
iv.SKU_ID SK,
iv.location_id LD,
sum(iv.qty_on_hand) QQ
from inventory iv,location L
where iv.client_id='UT'
and iv.location_id=L.location_id
and iv.site_id=L.site_id
and L.loc_type='Tag-FIFO'
group by
iv.sku_id,iv.location_id
HAVING sum(iv.qty_on_hand)>3))
group by SK)
where B2=SK (+)
order by 1
/

--spool off

