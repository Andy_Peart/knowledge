SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON



select 'Preadvice ID,Supplier,Name,Creation Date,Due Date,Booked Date,Started,Completed,Qty Due,Qty Received,Difference' from dual
union all
select "ID" ||','|| "Supplier" ||','|| "Name" ||','|| "Creation Date" ||','|| "Due Date" ||','|| "Booked Date" ||','|| "Started" ||','|| "Completed" ||','|| "Qty Due" ||','|| "Qty Received" ||','|| "Diff"
from
(
select pah.pre_advice_id    as "ID"
, pah.supplier_id           as "Supplier"
, ad.name                   as "Name"
, trunc(pah.creation_date)  as "Creation Date"
, trunc(pah.due_dstamp)     as "Due Date"
, 'Booked Date'             as "Booked Date"
, trunc(pah.actual_dstamp)  as "Started"
, trunc(pah.finish_dstamp)  as "Completed"
, sum(pal.qty_due)          as "Qty Due"
, sum(pal.qty_received)     as "Qty Received"
, sum(pal.qty_due) - sum(pal.qty_received)  as "Diff"
from pre_advice_header pah
inner join pre_advice_line pal on pal.client_id = pah.client_id and pal.pre_advice_id = pah.pre_advice_id
left join address ad on ad.address_id = pah.supplier_id and ad.client_id = pah.client_id
where pah.client_id = 'PR'
and pah.status = 'Complete'
and pah.finish_dstamp between to_date('&&2') and to_date('&&3')+1
and pah.name <> 'TML'
group by pah.pre_advice_id
, pah.supplier_id
, ad.name
, trunc(pah.creation_date)
, trunc(pah.due_dstamp)
, 'Booked Date'
, trunc(pah.actual_dstamp)
, trunc(pah.finish_dstamp)
order by pah.pre_advice_id
)
/
