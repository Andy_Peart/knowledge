SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep '|'


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 300
SET TRIMSPOOL ON
 
column XX NOPRINT

update inventory_transaction
set uploaded_labor='Z'
where client_id='MOUNTAIN'
and nvl(uploaded_labor,' ')<>'Y'
and code='Pick'
and from_loc_id='CONTAINER'
and to_loc_id like '&&2%'
and dstamp>sysdate-7
and reference_id not in (select
order_id from order_header
where client_id='MOUNTAIN'
and uploaded<>'N')
/




spool mwukmail.csv

select
order_id  XX,
DECODE('&&2','UKPAC','I360143','I099979')||'|'||
to_char(Sysdate,'DDMMYYYY')||'|'||
contact||'|'||
Name||'|'||
Address1||'|'||
Address2||'|'||
Town||'|'||
County||'|||'||
PostCode||'||'||
'1'||'|'||
DECODE('&&2','UKPAC','1000','10')||'|'||
DECODE('&&2','UKPAC','545','1')||'||'||
substr(instructions,1,30)||'|'||
substr(instructions,31,30)||'||||'||
--user_def_note_1||'|'||
--user_def_note_2||'|||||'||
order_id||'|||||||||||||||'||
'Torque Anchor Works Holme Lane Bradford BD4 6NA'||'||'||
' '
from order_header,(select
distinct
reference_id AAA
from inventory_transaction
where client_id='MOUNTAIN'
and uploaded_labor='Z'
and code='Pick'
and from_loc_id='CONTAINER'
and to_loc_id like '&&2%'
and dstamp>sysdate-7)
where client_id='MOUNTAIN'
and order_id=AAA
order by XX
/

spool off

--set feedback on

update inventory_transaction
set uploaded_labor='Y'
where client_id='MOUNTAIN'
and uploaded_labor='Z'
and code='Pick'
and from_loc_id='CONTAINER'
and to_loc_id like '&&2%'
and dstamp>sysdate-7
/

commit;
--rollback;
