SET FEEDBACK OFF                
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 400
SET TRIMSPOOL ON

column A heading 'Store No.' format a12
column B heading 'Name' format a30
column C heading 'PA ID' format a12
column D heading 'Date' format a12
column E heading 'Lines' format 999999
column F heading 'Qty Due' format 999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

--break on report

--compute sum label 'Total' of C D  on report

--spool tmlsumreturns.csv

select'Store No,Store Name,Pre Advice ID,Creation Date,Lines,Qty Due' from DUAL;


select
ph.supplier_id A,
a.name B,
ph.pre_advice_id C,
to_char(ph.creation_date,'DD/MM/YYYY') D,
count(*) E,
sum(pl.qty_due) F
from pre_advice_header ph,pre_advice_line pl,address a
where ph.client_id like 'TR'
and ph.status='Released'
and ph.creation_date between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1 
and ph.pre_advice_id like '%/%'
and pl.client_id like 'TR'
and ph.pre_advice_id=pl.pre_advice_id
and a.client_id = 'TR'
and ph.supplier_id=a.address_id (+)
group by
ph.supplier_id,
a.name,
ph.pre_advice_id,
to_char(ph.creation_date,'DD/MM/YYYY')
order by 1
/

--spool off
