SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 9999
--SET NEWPAGE 0
SET LINESIZE 900
SET TRIMSPOOL ON

spool stdshipments.csv


--select 'Order Date,Creation Date,Order Id,Custommer Id,Contact,Status,Style,Colour,Size,SKU,Description,EAN,Brand,Group,Qty Ordered,Qty Shipped,Uploaded' from DUAL


SELECT
  to_char(A.ORDER_DATE,'DD/MM/YY HH24.MI.SS') ORDER_DATE,
  to_char(A.CREATION_DATE,'DD/MM/YY HH24.MI.SS') CREATION_DATE,
  A.ORDER_ID,
  A.Customer_ID,
  A.Contact,
  A.STATUS,
  C.User_Def_Type_3 STYLE,
  C.Colour COLOUR,
  C.SKU_SIZE "SIZE",
  B.SKU_ID,
  C.DESCRIPTION "DESCRIPTION",
  C.EAN ,
  C.User_def_type_1 BRAND ,
  C.Product_Group "GROUP",
  nvl(B.QTY_ORDERED,0) QTY_ORDERED,
  nvl(B.QTY_SHIPPED,0) QTY_SHIPPED,
  nvl(B.QTY_ORDERED,0)-nvl(B.QTY_SHIPPED,0) DIFFERANCE,
  A.UPLOADED
FROM ORDER_HEADER A
INNER JOIN ORDER_LINE B ON A.CLIENT_ID = B.CLIENT_ID AND A.ORDER_ID = B.ORDER_ID
INNER JOIN SKU C ON A.CLIENT_ID = C.CLIENT_ID AND B.SKU_ID = C.SKU_ID
WHERE A.CLIENT_ID = '&&2'
AND A.STATUS = 'Shipped'
--AND A.OWNER_ID = 'UTR'
and a.shipped_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
AND A.ORDER_TYPE like '&&5%'
union all
select * from (SELECT
    null ORDER_DATE,
    null CREATION_DATE,
    null ORDER_ID,
    null Customer_ID,
    null Contact,
    null STATUS,
    null STYLE,
    null COLOUR,
    null "SIZE",
    null SKU_ID,
    null "DESCRIPTION",
    null EAN ,
    null BRAND ,
    'TOTALS',
    sum(B.QTY_ORDERED) QTY_ORDERED,
    sum(B.QTY_SHIPPED) QTY_SHIPPED,
  sum(B.QTY_ORDERED)-sum(B.QTY_SHIPPED) DIFFERANCE,
    null UPLOADED
  FROM ORDER_HEADER A
  INNER JOIN ORDER_LINE B ON A.CLIENT_ID = B.CLIENT_ID AND A.ORDER_ID = B.ORDER_ID
  INNER JOIN SKU C ON A.CLIENT_ID = C.CLIENT_ID AND B.SKU_ID = C.SKU_ID
  WHERE A.CLIENT_ID = '&&2'
  AND A.STATUS = 'Shipped'
  and a.shipped_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
  AND A.ORDER_TYPE like '&&5%')
/

spool off
