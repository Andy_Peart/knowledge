/* SKU Count Report */

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 68
SET NEWPAGE 0
SET LINESIZE 132
 
/* Top Title */
--TTITLE CENTER _SITENAME SKIP 2 -
TTITLE LEFT "DAXBOURNE WAREHOUSE SKU COUNT" skip 2

column A heading 'No. Of Different|SKUs In Stock'

select count (distinct sku_id) A
from inventory
where client_id = 'DX'
/

