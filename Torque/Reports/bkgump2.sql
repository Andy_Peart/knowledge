SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','
SET UND OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 320


select 'Order ID,Client ID,Status,Creation Date,Creation Time,Instructions' from DUAL;

column A format A14
column B format A10
column C format A12
column D format A12
column E format A12
column F format A100


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



--break on report

--compute sum label 'Total'  of G  on report


Select 
Order_ID A,
Client_ID B,
Status C,
to_char(creation_date,'DD/MM/YY') D,
to_char(creation_date,'HH:MI:SS') E,
Instructions F
--user_def_note_1||' '||user_def_note_2 F
from order_header
where client_id='&&2'
and creation_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
order by
A
/
 
