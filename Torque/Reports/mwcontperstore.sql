SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

--spool mwcontperstore.csv

select 'Date,Store,Order Id,Cartons,Units' from DUAL;

select
trunc(dstamp)||','||
work_group||','||
reference_id||','||
count(distinct container_id)||','||
sum(update_qty)
from inventory_transaction
where client_id ='MOUNTAIN'
and dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1
and code ='Shipment'
and work_group <> 'WWW'
and lower(sku_id)=upper(sku_id)
group by
trunc(dstamp),
work_group,
reference_id
order by
trunc(dstamp),
work_group,
reference_id
/

--spool off

