SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

 
select det
from 
(
select 'A' tab, 'Returns - last moved' det from dual
union all
select 'A' tab, 'Client, Location, SKU, Size, Qty, Product Group, Style, Days sincde last receipt, Days since last shipment' det from dual
union all
select 'A' tab, client_id || ',' || location_id || ',' || sku_id || ',' || sku_size || ',' || qty || ',' || product_group || ',' || style || ',' || 
last_rec || ',' || last_ship det
from (
select
  inv.client_id,
  inv.location_id,
  inv.sku_id,
  sku.sku_size,
  sum(inv.qty_on_hand) qty,
  sku.product_group ,
  sku.user_def_type_2 ||'-'|| sku.user_def_type_1 ||'-'||sku.user_def_type_4 style,
  trunc(sysdate)-trunc(max(r.receipt_dstamp)) last_rec,
  DECODE(to_char(max(BB), 'DD/MM/YY'),'01/01/00','None',trunc(sysdate)-trunc(max(BB))) last_ship
from inventory inv
inner join sku on inv.sku_id = sku.sku_id and inv.client_id = sku.client_id
left join
(
  select sku_id , client_id ,
  condition_id ,
  max(trunc(receipt_dstamp)) receipt_dstamp,
  sum(qty_on_hand) QQ
  from inventory
  where client_id = 'TR'
  and (
  substr(location_id,1,5) between '01RET' and '28RET' and substr(location_id,3,3) = 'RET'
  or
  substr(location_id,1,7) between '06SEC01' and '06SEC14'
  or
  substr(location_id,1,2) between 'EA' and 'FA'  
  or
  location_id like '%TOPR%'
  )
  and location_id not like '%TOPR%T'
  group by client_id, sku_id,condition_id
)
r on sku.client_id = r.client_id and sku.sku_id = r.sku_id
left join
(
  select i.Sku_id , client_id ,
  nvl(max (i.dstamp),'01-jan-00') BB
  from inventory_transaction i
  where i.client_id = 'TR'
  and i.code = 'Shipment'
  group by i.client_id,i.sku_id
) s on sku.client_id = s.client_id and sku.sku_id = s.sku_id
where inv.client_id = 'TR'
and
(
substr(inv.location_id,1,5) between '01RET' and '28RET' and substr(inv.location_id,3,3) = 'RET'
or
substr(inv.location_id,1,7) between '06SEC01' and '06SEC14'
or
substr(location_id,1,2) between 'EA' and 'FA'  
or
location_id like '%TOPR%'
)
and location_id not like '%TOPR%T'
group by   inv.client_id, inv.location_id,
  inv.sku_id,
  sku.product_group,sku.sku_size ,inv.lock_status,sku.user_def_type_2 ||'-'|| sku.user_def_type_1 ||'-'||sku.user_def_type_4
order by 1,2
)
union all
select 'B' tab, 'Seconds - last moved' det from dual
union all
select 'B' tab, 'Client, Location, SKU, Size, Qty, Product Group, Style, Days sincde last receipt, Days since last shipment' det from dual
union all
select 'B' tab, client_id || ',' || location_id || ',' || sku_id || ',' || sku_size || ',' || qty || ',' || product_group || ',' || style || ',' || 
last_rec || ',' || last_ship det
from (
select
  inv.client_id,
  inv.location_id,
  inv.sku_id,
  sku.sku_size,
  sum(inv.qty_on_hand) qty,
  sku.product_group,
  sku.user_def_type_2 ||'-'|| sku.user_def_type_1 ||'-'||sku.user_def_type_4 style,
  trunc(sysdate)-trunc(max(r.receipt_dstamp)) last_rec,
  DECODE(to_char(max(BB), 'DD/MM/YY'),'01/01/00','None',trunc(sysdate)-trunc(max(BB))) last_ship
  from inventory inv
  inner join sku on inv.sku_id = sku.sku_id and inv.client_id = sku.client_id
  left join
  (
    select i.sku_id , i.client_id ,
    max(trunc(receipt_dstamp)) receipt_dstamp,
    sum(qty_on_hand) QQ
    from inventory i
    inner join sku on i.sku_id = sku.sku_id and i.client_id = sku.client_id
    where i.client_id = 'TR'
    and substr(i.location_id,3,3) = 'SEC'
    and (substr(i.location_id,1,7) between '01SEC01' and '05SEC59'
    or substr(i.location_id,1,7) between '06SEC15' and '06SEC59')
    group by i.client_id, i.sku_id
  ) r on inv.client_id = r.client_id and inv.sku_id = r.sku_id
  left join
  (
  select sku.Sku_id, sku.client_id ,
  nvl(max (i.dstamp),'01-jan-00') BB
  from sku
  inner join inventory_transaction i on i.sku_id = sku.sku_id and i.client_id = sku.client_id and i.code = 'Shipment'
  where sku.client_id = 'TR'
  and i.code = 'Shipment'
  group by sku.Sku_id, sku.client_id
  ) s on inv.client_id = s.client_id and inv.sku_id = s.sku_id
  where inv.client_id = 'TR'
  and substr(inv.location_id,3,3) = 'SEC'
  and (substr(inv.location_id,1,7) between '01SEC01' and '05SEC59' or
  substr(inv.location_id,1,7) between '06SEC15' and '06SEC59')
  group by   inv.client_id, inv.location_id,  inv.sku_id,  sku.product_group,sku.sku_size, inv.lock_status,sku.user_def_type_2 ||'-'|| sku.user_def_type_1 ||'-'||sku.user_def_type_4
  order by 1,2
) 
)
where tab = 
case 
when '&&2' = 'Returns' then 'A'
when '&&2' = 'Seconds' then 'B'
end
/