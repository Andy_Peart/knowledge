SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 2000
SET TRIMSPOOL ON


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

break on A dup skip 1
compute sum label 'TOTALS' of E F on A


column A format a18
column B format a24
column C format a28
column D format a40
column E format 999999
column F format 999999
column M noprint



--spool shshipments.csv

--select 'Shipments for '||to_char(SYSDATE, 'DD/MM/YYYY') from DUAL;

select 'Order_id,Customer,Notes,Description,Qty Ordered,Qty Picked' from DUAL;

select
it.reference_id A,
OH1 B,
OL2 M,
--OL3,
sku.user_def_note_1 C,
sku.description D,
nvl(OL4,0) E,
sum(nvl(it.update_qty,0)) F
from inventory_transaction it,sku,(select
ol.order_id OL1,
ol.line_id OL2,
ol.sku_id OL3,
ol.qty_ordered OL4,
oh.name OH1
from order_line ol,order_header oh
where ol.client_id='SH'
and oh.client_id='SH'
and ol.order_id=oh.order_id)
where it.client_id='SH'
and trunc(it.Dstamp)=trunc(sysdate)
and it.code like 'Shipment'
and it.reference_id=OL1
--and OL1='008-M000000810'
and it.sku_id=OL3
and it.line_id=OL2
--and from_loc_id='CONTAINER'
and it.sku_id=sku.sku_id
and sku.client_id='SH'
group by
it.reference_id,
OH1,
it.sku_id,
sku.user_def_note_1,
sku.description,
OL2,
nvl(OL4,0)
order by
it.reference_id,
OL2,
sku.user_def_note_1
/

--spool off
