SET FEEDBACK OFF             
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 900
SET TRIMSPOOL ON

--spool utocs.csv

--select 'DELIVERY CUSTOMER,DELIVERY ADDRESS1,DELIVERY ADDRESS2,TOWN,COUNTY,DELIVERY POST CODE,DELIVERY COUNTRY,ISO CODE,HARD CODE 0,DELIVERY EMAIL,RP ORDER ID,DATE SHIPPED,SKU DESCRIPTION,QTY,USER DEF TYPE 1,LINE VALUE,NOW,NOW2,SKU PRODUCT GROUP,CONTAINER ID,DISPATCH METHOD' from DUAL;

select
A||','||
B||','||
C||','||
D||','||
D1||','||
D2||','||
D3||','||
D4||','||
E||','||
F||','||
G||','||
H||','||
J||','||
J2||','||
J3||','||
J4||','||
J5||','||
J6||','||
K||','||
K1||','||
M from (SELECT 
  b.contact A,
  b.address1 B,
  b.address2 C,
  b.town D,
  b.county D1,
  b.postcode D2,
  d.TANDATA_ID D3,
  b.country D4,
  '0' E,
  b.contact_email F,
  b.order_id G,
  TRUNC(a.dstamp) H,
  e.description J,
  a.update_qty J2,
  c.user_def_type_2 J3,
  c.line_value J4,
  NULL J5,
  NULL J6,
  e.product_group K,
   'UT'||b.order_id K1,
  'M' M
from inventory_transaction a
inner join order_header b on a.client_id = b.client_id and a.reference_id = b.order_id
inner join order_line c on b.client_id = c.client_id and b.order_id = c.order_id and c.line_id = a.line_id
left join country d on b.country = d.Iso3_Id
inner join sku e on b.client_id = e.client_id and c.sku_ID = e.sku_ID
INNER JOIN SHIPPING_MANIFEST F ON C.CLIENT_ID = F.CLIENT_ID AND C.ORDER_ID = F.ORDER_ID AND C.SKU_ID = F.SKU_ID
WHERE A.CLIENT_ID = 'UT'
and b.client_id = 'UT'
AND A.CODE = 'Shipment' 
AND NVL(B.COUNTRY,'GBR') NOT IN ('GBR','GB')
AND B.ORDER_TYPE in ('WEB','WEBMAN')
AND A.dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1)
/

--spool off


