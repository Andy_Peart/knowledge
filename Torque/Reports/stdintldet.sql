SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'PO,Supplier,Name,Creation Date,Due Date,Booked Date,Started,Completed,Sku,Qty Due,Qty Received,Difference' from dual
union all
select "Id" || ',' || "Supplier" || ',' || "Description" || ',' || "Creation" || ',' || "Due" || ',' || "Booked" || ',' || "Started" || ',' || "Comp" || ',' || "Sku" || ',' ||
"QDue" || ',' || "Rec" || ',' || "Diff" from
(
select pah.pre_advice_id                                as "Id"
, pah.supplier_id                                       as "Supplier"
, s.description                                         as "Description"
, to_char(pah.creation_date, 'DD/MM/YYYY HH24:MI')      as "Creation"
, to_char(pah.due_dstamp, 'DD/MM/YYYY HH24:MI')         as "Due"
, null                                                  as "Booked"                          
, to_char(pah.actual_dstamp, 'DD/MM/YYYY HH24:MI')      as "Started"
, to_char(pah.finish_dstamp, 'DD/MM/YYYY HH24:MI')      as "Comp"
, '''' || pal.sku_id || ''''                            as "Sku"
, sum(pal.qty_due)                                      as "QDue"
, sum(pal.qty_received)                                 as "Rec"
, sum(nvl(pal.qty_received,0))-sum(pal.qty_due)         as "Diff"
from pre_advice_header pah
inner join pre_advice_line pal on pal.client_id = pah.client_id and pal.pre_advice_id = pah.pre_advice_id
inner join sku s on s.client_id = pal.client_id and s.sku_id = pal.sku_id
where pah.client_id = '&2'
and pah.status = 'Complete'
and pah.finish_dstamp between to_date('&3', 'DD-MON-YYYY') and to_date('&4', 'DD-MON-YYYY')+1
group by pah.pre_advice_id
, pah.supplier_id
, s.description
, pah.creation_date
, pah.due_dstamp
, pah.actual_dstamp
, pah.finish_dstamp
, pal.sku_id
order by 1
)
/
--spool off