SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9999
SET TRIMSPOOL ON


COLUMN NEWSEQ NEW_VALUE SEQNO
COLUMN FOLD NEW_VALUE FOLDER




select 'S://redprairie/apps/home/dcsdba/reports/LIVE/TMD'||substr(to_char(100000+next_seq),2,5)||'.csv' NEWSEQ from sequence_file
--select 'LIVE/TMD'||substr(to_char(100000+next_seq),2,5)||'.csv' NEWSEQ from sequence_file
where control_id=22
/

set termout off
spool &SEQNO


select 'ITL'||','
||'E'||','
||'Shipment'||','
||decode(sign(i.original_qty), 1, '+', -1, '-', '+')||','
||nvl(i.original_qty,0)||'.000000,'
||decode(sign(i.update_qty), 1, '+', -1, '-', '+')||','
||i.update_qty||'.000000,'
||to_char(i.dstamp,'YYYYMMDDHH24MISS')||','
||i.client_id||','
||i.sku_id||','
||i.from_loc_id||','
||i.to_loc_id||','
||i.final_loc_id||','
||i.tag_id||','
||i.reference_id||','
||i.line_id||','
||i.condition_id||','
||i.notes||','
||i.reason_id||','
||i.batch_id||','
||to_char(i.expiry_dstamp,'YYYYMMDDHH24MISS')||','
||i.user_id||','
||i.shift||','
||i.station_id||','
||i.site_id||','
||i.container_id||','
||i.pallet_id||','
||i.list_id||','
||i.owner_id||','
||i.origin_id||','
||i.work_group||','
||i.consignment||','
||to_char(i.manuf_dstamp,'YYYYMMDDHH24MISS')||','
||i.lock_status||','
||i.qc_status||','
||i.session_type||','
||i.summary_record||','
||nvl(i.elapsed_time,0)||','
||i.supplier_id||','
||i.user_def_type_1||','
||i.user_def_type_2||','
||i.user_def_type_3||','
||i.user_def_type_4||','
||i.user_def_type_5||','
||i.user_def_type_6||','
||i.user_def_type_7||','
||i.user_def_type_8||','
||i.user_def_chk_1||','
||i.user_def_chk_2||','
||i.user_def_chk_3||','
||i.user_def_chk_4||','
||to_char(i.user_def_date_1,'YYYYMMDDHH24MISS')||','
||to_char(i.user_def_date_2,'YYYYMMDDHH24MISS')||','
||to_char(i.user_def_date_3,'YYYYMMDDHH24MISS')||','
||to_char(i.user_def_date_4,'YYYYMMDDHH24MISS')||','
||i.user_def_num_1||'.000000,'
||i.user_def_num_2||'.000000,'
||i.user_def_num_3||'.000000,'
||i.user_def_num_4||'.000000,'
||i.user_def_note_1||','
||i.user_def_note_2||','
||i.from_site_id||','
||i.to_site_id||','
||'Europe/London'||','
||i.job_id||','
||i.job_unit||','
||nvl(i.manning,0)||','
||i.spec_code||','
||i.config_id||','
||nvl(i.estimated_time,0)||'.000000,'
||nvl(i.task_category,0)||','
||i.sampling_type||','
||to_char(i.complete_dstamp,'YYYYMMDDHH24MISS')||','
||nvl(i.grn,0)||','
||i.group_id||','
||'N'||','
||i.uploaded_vview||','
||i.uploaded_ab||','
||i.sap_idoc_type||','
||i.sap_tid||','
||i.ce_orig_rotation_id||','
||i.ce_rotation_id||','
||i.ce_consignment_id||','
||i.ce_receipt_type||','
||i.ce_originator||','
||i.ce_originator_reference||','
||i.ce_coo||','
||i.ce_cwc||','
||i.ce_ucr||','
||i.ce_under_bond||','
||''||','
||i.uploaded_customs||','
||i.uploaded_labor||','
||i.asn_id||','
||i.customer_id||','
||i.print_label_id||','
||i.lock_code||','
||i.ship_dock||','
||'N'||','
||'N'
from inventory_transaction i
where i.client_id = 'T'
and code = 'Shipment'
--and ((trunc(Dstamp)=trunc(sysdate) and (user_def_chk_1='N' or user_def_chk_1 is null))
--or (trunc(Dstamp)>trunc(sysdate)-11 and user_def_chk_1 is null))
and trunc(Dstamp)>trunc(sysdate)-1
and (user_def_chk_1='N' or user_def_chk_1 is null)
order by
i.dstamp
/


spool off

set feedback on

update inventory_transaction 
set user_def_chk_1='Y'
where client_id = 'T'
and code = 'Shipment'
--and ((trunc(Dstamp)=trunc(sysdate) and (user_def_chk_1='N' or user_def_chk_1 is null))
--or (trunc(Dstamp)>trunc(sysdate)-11 and user_def_chk_1 is null))
and trunc(Dstamp)>trunc(sysdate)-1
and (user_def_chk_1='N' or user_def_chk_1 is null)
/



Update sequence_file
set next_seq=next_seq+1
where control_id=22;


commit;
--rollback;

