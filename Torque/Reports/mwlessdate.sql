set pagesize 68
set linesize 120
set verify off

clear columns
clear breaks
clear computes

column A heading 'Receipt Date' format a10
column B heading 'SKU' format a15
column C heading 'Location' format a10
column D heading 'Qty' format 99999


ttitle center "Mountain Warehouse - Stock 2 weeks older than '&&2'." skip 3

select to_char (inv.receipt_dstamp, 'DD/MM/YY') A,
inv.sku_id B,
inv.location_id C,
inv.qty_on_hand D
from inventory inv
where client_id = 'MOUNTAIN'
and inv.receipt_dstamp < to_date(upper ('&&2'),'DD/MM/YY')  - 14
and inv.location_id <> 'MARSHAL'
and inv.location_id <> 'DESPATCH'
order by inv.receipt_dstamp
/
