SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 

column A heading 'Category' format A11
column B heading 'ECHO Qty' format 9999999
column C heading 'SM Qty' format 9999999
column D heading 'ALL' format 9999999

set heading off
set pagesize 100

select 'Stock Holding Report for Client ID OPS on ',to_char(SYSDATE, 'DD/MM/YY    HH:MI:SS') from DUAL;

set heading on


select
'Stock' A,
B1-B2 B,
B2 C,
B1 D
from (select
nvl(sum(i.qty_on_hand),0) B1
from inventory i
where i.location_id not in ('IN','IN2','IN3','QCHOLD','WP1','SUSPENSE')
and i.client_id='OPS'
and i.lock_status<>'Locked'),(select
nvl(sum(i.qty_on_hand),0) B2
from inventory i,sku
where i.location_id not in ('IN','IN2','IN3','QCHOLD','WP1','SUSPENSE')
and i.client_id='OPS'
and i.client_id=sku.client_id
and i.sku_id=sku.sku_id
and sku.product_group='SM'
and i.lock_status<>'Locked')
/

SET PAGESIZE 0

select
'Allocated' A,
B1-B2 B,
B2 C,
B1 D
from (select
nvl(sum(i.qty_allocated),0) B1
from inventory i
where i.location_id not in ('IN','IN2','IN3','QCHOLD','WP1','SUSPENSE')
and i.client_id='OPS'
and i.lock_status<>'Locked'),(select
nvl(sum(i.qty_allocated),0) B2
from inventory i,sku
where i.location_id not in ('IN','IN2','IN3','QCHOLD','WP1','SUSPENSE')
and i.client_id='OPS'
and i.client_id=sku.client_id
and i.sku_id=sku.sku_id
and sku.product_group='SM'
and i.lock_status<>'Locked')
/



select
'Available' A,
B1-B2 B,
B2 C,
B1 D
from (select
nvl(sum(i.qty_on_hand-i.qty_allocated),0) B1
from inventory i
where i.location_id not in ('IN','IN2','IN3','QCHOLD','WP1','SUSPENSE')
and i.client_id='OPS'
and i.lock_status<>'Locked'),(select
nvl(sum(i.qty_on_hand-i.qty_allocated),0) B2
from inventory i,sku
where i.location_id not in ('IN','IN2','IN3','QCHOLD','WP1','SUSPENSE')
and i.client_id='OPS'
and i.client_id=sku.client_id
and i.sku_id=sku.sku_id
and sku.product_group='SM'
and i.lock_status<>'Locked')
/



select 
'in IN ' A,
B1-B2 B,
B2 C,
B1 D
from (select
nvl(sum(i.qty_on_hand),0) B1
from inventory i
where i.location_id='IN'
and i.client_id='OPS'
and i.lock_status<>'Locked'),(select
nvl(sum(i.qty_on_hand),0) B2
from inventory i,sku
where i.location_id='IN'
and i.client_id='OPS'
and i.client_id=sku.client_id
and i.sku_id=sku.sku_id
and sku.product_group='SM'
and i.lock_status<>'Locked')
/


select 
'in IN2 ' A,
B1-B2 B,
B2 C,
B1 D
from (select
nvl(sum(i.qty_on_hand),0) B1
from inventory i
where i.location_id='IN2'
and i.client_id='OPS'
and i.lock_status<>'Locked'),(select
nvl(sum(i.qty_on_hand),0) B2
from inventory i,sku
where i.location_id='IN2'
and i.client_id='OPS'
and i.client_id=sku.client_id
and i.sku_id=sku.sku_id
and sku.product_group='SM'
and i.lock_status<>'Locked')
/


select 
'in IN3 ' A,
B1-B2 B,
B2 C,
B1 D
from (select
nvl(sum(i.qty_on_hand),0) B1
from inventory i
where i.location_id='IN3'
and i.client_id='OPS'
and i.lock_status<>'Locked'),(select
nvl(sum(i.qty_on_hand),0) B2
from inventory i,sku
where i.location_id='IN3'
and i.client_id='OPS'
and i.client_id=sku.client_id
and i.sku_id=sku.sku_id
and sku.product_group='SM'
and i.lock_status<>'Locked')
/


select 
'in QCHOLD ' A,
B1-B2 B,
B2 C,
B1 D
from (select
nvl(sum(i.qty_on_hand),0) B1
from inventory i
where i.location_id='QCHOLD'
and i.client_id='OPS'
and i.lock_status<>'Locked'),(select
nvl(sum(i.qty_on_hand),0) B2
from inventory i,sku
where i.location_id='QCHOLD'
and i.client_id='OPS'
and i.client_id=sku.client_id
and i.sku_id=sku.sku_id
and sku.product_group='SM'
and i.lock_status<>'Locked')
/

select 
'in WP1 ' A,
B1-B2 B,
B2 C,
B1 D
from (select
nvl(sum(i.qty_on_hand),0) B1
from inventory i
where i.location_id='WP1'
and i.client_id='OPS'
and i.lock_status<>'Locked'),(select
nvl(sum(i.qty_on_hand),0) B2
from inventory i,sku
where i.location_id='WP1'
and i.client_id='OPS'
and i.client_id=sku.client_id
and i.sku_id=sku.sku_id
and sku.product_group='SM'
and i.lock_status<>'Locked')
/

select 
'in SUSPENSE ' A,
B1-B2 B,
B2 C,
B1 D
from (select
nvl(sum(i.qty_on_hand),0) B1
from inventory i
where i.location_id='SUSPENSE'
and i.client_id='OPS'
and i.lock_status<>'Locked'),(select
nvl(sum(i.qty_on_hand),0) B2
from inventory i,sku
where i.location_id='SUSPENSE'
and i.client_id='OPS'
and i.client_id=sku.client_id
and i.sku_id=sku.sku_id
and sku.product_group='SM'
and i.lock_status<>'Locked')
/

select
'Full Total' A,
B1-B2 B,
B2 C,
B1 D
from (select
nvl(sum(i.qty_on_hand),0) B1
from inventory i
where i.client_id='OPS'
and i.lock_status<>'Locked'),(select
nvl(sum(i.qty_on_hand),0) B2
from inventory i,sku
where i.client_id='OPS'
and i.client_id=sku.client_id
and i.sku_id=sku.sku_id
and sku.product_group='SM'
and i.lock_status<>'Locked')
/
