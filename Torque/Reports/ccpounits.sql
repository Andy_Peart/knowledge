SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 200
set trimspool on
set newpage 0
set verify off
set colsep ','



break on report
compute sum label 'Totals' of Q Q2 Q3 on report


--spool eds78.csv


select 'Units per PO/Style/Colour from &&2 to &&3 for Client : '||nvl(name,client_id) from client
where client_id='&&4';

select 'PO,Style,Colour,Received Units,Shipped Units,Adjusted Units' from DUAL;


select
A,
B,
C,
sum(E) Q,
0-sum(E2) Q2,
sum(E3) Q3
from (select
it.reference_id A,
sku.user_def_type_1 B,
sku.colour C,
sum(it.update_qty) E,
0 E2,
0 E3
from inventory_transaction it,sku
where it.client_id='&&4'
and it.code like 'Receipt%'
and it.dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.sku_id=sku.sku_id
and sku.client_id='&&4'
group by
it.reference_id,
sku.user_def_type_1,
sku.colour
union
select
RR A,
sku.user_def_type_1 B,
sku.colour C,
0 E,
sum(it.update_qty) E2,
0 E3
from inventory_transaction it,(select
distinct
tag_id TT,
reference_id RR
from inventory_transaction
where client_id='&&4'
and code='Receipt'),sku
where it.client_id='&&4'
and it.code in ('Shipment','UnShipment')
and it.dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.tag_id=TT
and it.sku_id=sku.sku_id
and sku.client_id='&&4'
group by
RR,
sku.user_def_type_1,
sku.colour
union
select
RR A,
sku.user_def_type_1 B,
sku.colour C,
0 E,
0 E2,
sum(it.update_qty) E3
from inventory_transaction it,(select
distinct
tag_id TT,
reference_id RR
from inventory_transaction
where client_id='&&4'
and code='Receipt'),sku
where it.client_id='&&4'
and it.code in ('Adjustment')
and it.dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.tag_id=TT
and it.sku_id=sku.sku_id
and sku.client_id='&&4'
group by
RR,
sku.user_def_type_1,
sku.colour)
group by
A,B,C
order by A,B,C
/

--spool off
