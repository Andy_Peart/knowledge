set feedback off
set pagesize 68
set linesize 240
set newpage 0
set verify off
set colsep ' '

clear columns
clear breaks
clear computes

column A heading 'User ID' format A16
column F heading 'Client ID' format A14
column B heading 'Non Set Units' format 999999
column B2 heading 'Sets' format 999999
column B3 heading 'Set Units' format 999999
column C heading 'Lines' format 999999
column D heading 'Orders' format 999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'Units Picked from '&&2' to '&&3' - for Gorilla Glue' skip 2

break on F dup skip 1 on report
compute sum label 'Client Totals' of C D B B2 B3 on F
compute sum label 'Overall Totals' of C D B B2 B3 on report


select
A,
F,
count(distinct reference_id) D,
sum(L1) C,
sum(Q1) B2,
sum(Q2) B3,
sum(Q3) B
from (select
user_id A,
client_id F,
reference_id,
DECODE(to_loc_id,'KITTING',0,1) L1,
DECODE(from_loc_id,'KITTING',update_qty,0) Q1,
DECODE(to_loc_id,'KITTING',update_qty,0) Q2,
DECODE(from_loc_id,'KITTING',0,DECODE(to_loc_id,'KITTING',0,update_qty)) Q3
from inventory_transaction
where client_id='GG'
and code='Pick'
and Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1)
group by A,F
/
