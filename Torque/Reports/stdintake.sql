SET FEEDBACK OFF
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240
SET TRIMSPOOL ON

--SET TERMOUT OFF

--break on A skip 1


--spool stdintake.csv


select 'PO,Supplier,Name,Creation Date,Due Date,Booked Date,Started,Completed,Qty Due,Qty Received,Difference' from DUAL
union all
select * from (select
ph.pre_advice_id||','||
ph.supplier_id||','||
nvl(NMR,' ')||','||
to_char(ph.creation_date,'DD/MM/YYYY HH24:MI:SS')||','||
to_char(ph.due_dstamp,'DD/MM/YYYY HH24:MI:SS')||','||
' '||','||
to_char(ph.actual_dstamp,'DD/MM/YYYY HH24:MI:SS')||','||
to_char(ph.finish_dstamp,'DD/MM/YYYY HH24:MI:SS')||','||
sum(pl.qty_due)||','||
sum(nvl(pl.qty_received,0))||','||
sum(nvl(pl.qty_received,0)-pl.qty_due)
from pre_advice_header ph, pre_advice_line pl,(select
address_id ADR,
name NMR
from address
where client_id='&&2')
where ph.client_id = '&&2'
and ph.status='Complete'
and ph.finish_dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and ph.client_id = pl.client_id
and ph.pre_advice_id = pl.pre_advice_id
and ph.supplier_id=ADR (+)
group by
ph.pre_advice_id,
ph.supplier_id,
nvl(NMR,' '),
to_char(ph.creation_date,'DD/MM/YYYY HH24:MI:SS'),
to_char(ph.due_dstamp,'DD/MM/YYYY HH24:MI:SS'),
'Booked Date',
to_char(ph.actual_dstamp,'DD/MM/YYYY HH24:MI:SS'),
to_char(ph.finish_dstamp,'DD/MM/YYYY HH24:MI:SS')
order by 1)
union all
select * from (select
',,,,,,,Totals,'||
sum(pl.qty_due)||','||
sum(nvl(pl.qty_received,0))||','||
sum(nvl(pl.qty_received,0)-pl.qty_due)
from pre_advice_header ph, pre_advice_line pl
where ph.pre_advice_id = pl.pre_advice_id
and ph.client_id = '&&2'
and ph.status='Complete')
/

--spool off

