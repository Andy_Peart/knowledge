SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

/* Client TR doesn't use back-ordering. */
update order_line
set back_ordered = null
where back_ordered = 'Y'
and client_id = 'TR';

commit;

exit;

