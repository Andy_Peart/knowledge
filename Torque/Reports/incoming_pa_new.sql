/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :     Incoming_pa.sql					      */
/*                                                                            */
/*     DESCRIPTION:     Printout of Pre Advice details to be used by w/house  */
/*                                                                            */
/*   DATE     BY   DESCRIPTION						      */
/*   ======== ==== ===========					              */
/*   04/11/04 BS   initial version     				  	      */
/*   15/09/06 MS   modified to include ean    				      */
/*   19/02/07 BK   modified to change text                                    */ 
/*   12/03/07 BK   modified to include SKU description                        */
/******************************************************************************/
SET FEEDBACK OFF
SET VERIFY OFF
SET TAB OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Column Headings and Formats */
COLUMN AA heading "Date" FORMAT a10
COLUMN AAA heading "Time" FORMAT a10
COLUMN A heading "PO NO" FORMAT a14
COLUMN B heading "SKU" FORMAT a18
COLUMN BB heading "Description" FORMAT a40
COLUMN C heading "Line ID" FORMAT 999999
COLUMN D heading "Ordered|Qty" FORMAT 999999
COLUMN DD heading "Rec'ved|to Date" FORMAT 999999
COLUMN E heading "Scanned|Qty" FORMAT 999999
COLUMN F heading "EAN" FORMAT a16
COLUMN G heading "GRN" FORMAT a8
COLUMN H heading "NOTES" FORMAT a20
COLUMN J heading "  " FORMAT a2
COLUMN K heading "User ID" FORMAT a12
COLUMN L heading "Location" FORMAT a10
COLUMN M noprint


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


SET LINESIZE 136
SET WRAP OFF
SET PAGES 66
SET COLSEP ' '
SET NEWPAGE 0

/* Top Title */
TTITLE LEFT 'GRN Report for PO &&2 - Client ID &&3' RIGHT 'Date: ' report_date SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 1 - 

/* Set up page and line */

break on AA dup skip 1 on report

compute sum label 'Totals' of E on report


SELECT
to_char(Dstamp, 'YYMM') M,
to_char(Dstamp,'DD/MM/YY') AA,
to_char(Dstamp,'HH:MI:SS') AAA,
it.SKU_ID B,
--DDD BB,
PAL.Line_ID C,
nvl(PAL.Qty_Due,0) D,
nvl(it.update_qty,0) E,
nvl(PAL.Qty_Received,0) DD,
'  ' J,
EEE F,
it.user_id K,
it.to_loc_id L,
PAH.Notes G,
PAH.user_def_note_2 H
FROM Pre_Advice_Header PAH,Pre_Advice_Line PAL,inventory_transaction it, (select 
sku_id SSS,
description DDD,
ean EEE 
from sku where client_id='&&3')
where PAH.Pre_Advice_ID = '&&2'
and PAH.client_id = '&&3'
and PAH.Pre_Advice_ID = it.reference_ID
and PAH.Pre_Advice_ID = PAL.Pre_Advice_ID
and it.sku_id=PAL.SKU_ID
and it.code='Receipt'
AND it.SKU_ID = SSS (+)
and it.Dstamp between to_date('&&4', 'DD-MON-YYYY') and to_date('&&5', 'DD-MON-YYYY')+1
ORDER BY
M,AA,B
--PAH.user_def_type_2
/

--set Heading off
SET PAGESIZE 0
SET NEWPAGE 0

CLEAR BREAKS
CLEAR COMPUTES


select '  ' from DUAL;


SELECT distinct 'SKU DESCRIPTION:   '||S.Description 
FROM Pre_Advice_Header PAH,Pre_Advice_Line PAL,inventory_transaction it, SKU S
where PAH.Pre_Advice_ID = '&&2'
and PAH.client_id = '&&3'
and PAH.Pre_Advice_ID = it.reference_ID
and PAH.Pre_Advice_ID = PAL.Pre_Advice_ID
and it.sku_id=PAL.SKU_ID
and it.code='Receipt'
AND it.SKU_ID = S.SKU_ID (+)
and it.Dstamp between to_date('&&4', 'DD-MON-YYYY') and to_date('&&5', 'DD-MON-YYYY')+1
/

select '  ' from DUAL;
select 'NOTE  ' from DUAL;


SELECT
'Order Totals to Date   ',
'Ordered= ',
sum(nvl(PAL.Qty_Due,0)) D,
'    Received= ',
sum(nvl(PAL.Qty_Received,0)) E
--sum(nvl(it.update_qty,0)) E
FROM Pre_Advice_Header PAH,Pre_Advice_Line PAL
where PAH.Pre_Advice_ID = '&&2'
and PAH.client_id = '&&3'
and PAH.Pre_Advice_ID = PAL.Pre_Advice_ID
/
