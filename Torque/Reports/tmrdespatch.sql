/******************************************************************************/
/*                                                                            */
/*                            Elite                                           */
/*                                                                            */
/*     FILE NAME  :   pridespatch.sql                                         */
/*                                                                            */
/*     DESCRIPTION:    Datastream for Pringle Despatch Note / Pack List       */
/*                                                                            */
/*   DATE     BY   PROJ       ID         DESCRIPTION                          */
/*   ======== ==== ======     ========   =============                        */
/*   22/05/07 LH   TMR                   Despatch Note For TML Retail	      */
/*   21/10/07 LH   TMR                   Additional Line 'Product'            */
/*   19/07/11 RW   TMR                   Amend for country and address        */
/*                                                                            */
/******************************************************************************/

/* Input Parameters are as follows */
-- 1)  GMT
-- 2)  Order_id
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 0
SET VERIFY OFF
SET FEEDBACK OFF
SET TAB OFF

 
-- Set the line length to the maximum possible, but trim 
-- trailing spaces for both screen output and spool files
SET LINESIZE 32767
SET WRAP ON
SET TRIMOUT ON
SET TRIMSPOOL ON




/* Define the format of the output dates */
DEFINE	OutputDateFormat = 'DD-MON-YYYY HH24:MI:SS'
COLUMN	sorter	NOPRINT

SELECT Distinct '0' sorter,
       'DSTREAM_TMR_DESP_HDR'          ||'|'||
       rtrim('&&2')                            /* Order_id */
from dual
union all
SELECT '1' sorter,
'DSTREAM_TMR_DESP_ADD_HDR'              ||'|'||
rtrim (oh.name)						||'|'||
rtrim (oh.customer_id)				||'|'||
decode(oh.address1,null,'',oh.address1||'|')||
decode(oh.address2,null,'',oh.address2||'|')||
decode(oh.town,null,'',oh.town||'|')||
decode(oh.county,null,'',oh.county||'|')||
decode(oh.postcode,null,'',oh.postcode||'|')||
decode(t.text,null,'',t.text)          /* Country converted using workstation lookup) */
--rtrim (oh.address1)					||'|'||
--rtrim (oh.address2)					||'|'||
--rtrim (oh.town)						||'|'||
--rtrim (oh.county)					||'|'||
--rtrim (oh.country)					||'|'||
--rtrim (oh.postcode)
from order_header oh, country c, language_text t
where oh.order_id = '&&2'
and oh.client_id = 'TR'
and oh.country = c.iso3_id
and t.language = 'EN_GB'
and t.label = 'WLK'||c.iso3_id
union all
select distinct '1' sorter,
'DSTREAM_TMR_DESP_ORD_HDR'			||'|'||
rtrim (oh.order_id)					||'|'||
rtrim (to_char(sm.picked_dstamp, 'DD-MON-YYYY HH24:MI'))				||'|'||
rtrim (sm.location_id)				||'|'||
rtrim (sm.user_id)					||'|'||
rtrim (sm.site_id)					||'|'||
rtrim (oh.purchase_order)			||'|'||
rtrim (oh.user_def_type_6)			||'|'||
rtrim (oh.user_def_type_4)			||'|'||
rtrim (substr(oh.order_id, 3, 7))	||'|'||
rtrim (substr(oh.order_id, 10, 14))	||'|'||
rtrim (A)							||'|'||
rtrim (instructions)
from order_header oh, shipping_manifest sm, (select count(distinct smm.container_id) A from shipping_manifest smm where smm.order_id = '&&2')
where oh.order_id = '&&2'
and oh.order_id = sm.order_id
and oh.client_id = 'TR'
union all
SELECT '1'||sh.container_id SORTER,
'DSTREAM_TMR_DESP_LINE'              ||'|'||
rtrim (sh.container_id)       ||'|'||
rtrim (s.sku_id)				||'|'||
rtrim (s.user_def_type_2||'-'||s.user_def_type_1||'-'||s.user_def_type_4)              ||'|'||
rtrim (s.description)                ||'|'||
rtrim (s.COLOUR)					||'|'||
rtrim (s.SKU_SIZE)					||'|'||
rtrim (sh.qty_shipped)				||'|'||
rtrim (s.user_def_type_5)
from sku s, shipping_manifest sh
where sh.order_id = '&&2'
and sh.qty_picked > '0'
and sh.sku_id = s.sku_id
and s.client_id = 'TR'
order by 1,2
/