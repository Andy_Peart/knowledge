SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Shipped Date,Orders,Units' from dual
union all
select "Date" ||','|| "Orders" ||','|| "Units"
from (
select trunc(oh.shipped_date)   as "Date"
/*, oh.order_id                 as "Order #"
, ol.sku_id                     as "Sku"
, s.description                 as "Desc"*/               
, count(distinct oh.order_id)   as "Orders"
, sum(ol.qty_shipped)           as "Units"
from order_header oh
inner join order_line ol on ol.order_id = oh.order_id and ol.client_id = oh.client_id
inner join sku s on s.client_id = ol.client_id and s.sku_id = ol.sku_id
where oh.client_id = 'SD'
and s.user_Def_type_1  = 'FRAGILE'
and oh.shipped_date between to_Date('&&2') and to_Date('&&3')+1
group by trunc(oh.shipped_date)
order by trunc(shipped_date)
)
/
--spool off