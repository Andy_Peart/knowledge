SET FEEDBACK OFF                 
set pagesize 68
set linesize 160
set verify off

clear columns
clear breaks
clear computes

column C heading 'Client ID' format a10
column F heading 'Txn Type' format a16
column D heading 'Date' format a16
column A heading 'Advice ID' format a16
column B heading 'Units' format 99999999
column E heading 'Txns' format 99999999
column M format a4 noprint

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'Total Inventory Txns for Type '&&5' - from &&2 to &&3 - for client ID &&4 as at ' report_date skip 2

break on D skip 1 on report

compute sum label 'Totals' of E B on report


select
to_char(it.Dstamp, 'YYMM') M,
to_char(it.Dstamp, 'DD/MM/YY') D,
it.code F,
count(*) E,
sum(it.update_qty) as B
from inventory_transaction it
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='&&4'
and it.code like '&&5%'
group by
to_char(it.Dstamp, 'YYMM'),
to_char(it.Dstamp, 'DD/MM/YY'),
it.code
order by
to_char(it.Dstamp, 'YYMM'),
to_char(it.Dstamp, 'DD/MM/YY'),
it.code
/

