SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 999
SET TRIMSPOOL ON

/*Input SQL here*/
with t1 as
(
    select i.sku_id
    , s.product_group
    , s.user_def_type_6
    , s.user_def_type_1
    , s.description
    , s.colour
    , s.sku_size
    , l.loc_type
    , l.work_zone
    , coalesce(i.condition_id, 'GOOD') condition_id
    , i.lock_status
    , i.lock_code
    , i.location_id
    , s.user_def_num_3 web_reserve
    , sum(i.qty_on_hand) qty_on_hand
    , case when sum(i.qty_allocated) > sum(i.qty_on_hand) then sum(i.qty_on_hand) else sum(i.qty_allocated) end qty_allocated
	, max(trunc(i.receipt_dstamp)) as Last_Receipt_Date
	from INVENTORY i
    inner join SKU s on s.sku_id = i.sku_id and s.client_id = i.client_id
    inner join LOCATION l on l.location_id = i.location_id and l.site_id = i.site_id
    where i.client_id = 'MOUNTAIN'
    and i.location_id <> 'SUSPENSE'
    group by i.sku_id
    , s.product_group
    , s.user_def_type_6
    , s.user_def_type_1
    , s.description
    , s.colour
    , s.sku_size
    , l.loc_type
    , l.work_zone
    , i.condition_id
    , i.lock_status
    , i.lock_code
    , i.location_id
    , s.user_def_num_3
), t2 as
(
    select t1.sku_id
    , t1.product_group department
    , t1.user_def_type_6 type
    , t1.user_def_type_1 ref
    , t1.description
    , t1.colour
    , t1.sku_size
    , t1.web_reserve
    , max(t1.last_receipt_date) Last_Receipt_Date
    , sum(t1.qty_on_hand) total_stock
    , sum(case when t1.loc_type in ('Receive Dock') and t1.location_id not in ('Z011','Z012','Z013','NOTAG','RET001') then t1.qty_on_hand else 0 end) intake
    , sum(case when t1.lock_code is not null and t1.lock_status = 'Locked' and t1.loc_type not in ('Receive Dock') then t1.qty_on_hand else 0 end) qc_hold
    , sum(case when t1.lock_status = 'Locked' and t1.loc_type not in ('Receive Dock') and t1.lock_code is null then t1.qty_on_hand else 0 end) locked
    , sum(case when t1.loc_type in ('Stage','ShipDock') and t1.lock_status = 'UnLocked' then t1.qty_on_hand else 0 end) inProgress
    , sum(case when t1.loc_type not in ('Stage','ShipDock') and t1.lock_status = 'UnLocked' and t1.loc_type not in ('Receive Dock') then t1.qty_allocated else 0 end) allocated
    , sum(case when t1.location_id in ('Z011','Z012','Z013','NOTAG') then t1.qty_on_hand else 0 end) damaged
    , sum(case when t1.location_id in ('RET001') then t1.qty_on_hand else 0 end) returns
    , sum(case when t1.condition_id = 'CFSP' and t1.loc_type <> 'Receive Dock' then t1.qty_on_hand else 0 end) bonded
    from t1      
    group by t1.sku_id
    , t1.product_group 
    , t1.user_def_type_6 
    , t1.user_def_type_1 
    , t1.description
    , t1.colour
    , t1.sku_size
    , t1.web_reserve
), buffer_inv as
(
    select sku_id
    , sum(qty_on_hand) - sum(qty_allocated) qty_available
    from t1
    where lock_status = 'UnLocked'
    and loc_type in ('Tag-FIFO','Tag-LIFO')
    and coalesce(condition_id, 'GOOD') = 'GOOD'
    and work_zone in
    (
    '2FLA',
    '2FLB',
    '2FLC',
    'GFLA',
    'GFLB',
    'WRETA',
    'LCTN',
    'SND',
    'FOOT',
    'G',
    'BULK'
    )
    and sku_id in (select sku_id from inventory where client_id = 'MOUNTAIN' and location_id <> 'SUSPENSE' group by sku_id having sum(qty_on_hand) <= 4)
    group by sku_id    
), t3 as 
(
        select sku_id
        , order_type OrdType
        , to_char(trunc(dstamp), 'DD-MON-YY') lastDate
        from
        (
        select it.sku_id
        , oh.order_type
        , max(it.dstamp) dstamp
        , rank()over(partition by it.sku_id order by max(it.dstamp) desc) rank
        from INVENTORY_TRANSACTION it
        inner join ORDER_HEADER oh on oh.order_id = it.reference_id and oh.client_id = it.client_id
        where it.client_id = 'MOUNTAIN'
        and it.code = 'Shipment'
        and it.dstamp > sysdate - 90
        group by it.sku_id
        , oh.order_type
        )
        where rank = 1
)
select 'Sku id,Dept,Type,Ref,Description,Colour,Sku Size,Total Stock,Intake,QC Hold,Locked,In Progress,Damaged,Returns,Bonded,Web Buffer,Available,Web Reserve,Last Receipt Date,Last Shipped,Last Order Type' from dual
union all
select sku_id ||','|| department ||','|| type ||','|| ref ||',"'|| description ||'","'|| colour ||'",'|| sku_size ||','|| total_stock ||','|| intake ||','|| qc_hold ||','|| locked ||','|| inProgress ||','|| damaged ||','|| returns ||','|| bonded ||','|| web_buffer ||','|| available 
||','|| web_reserve ||','|| Last_Receipt_Date ||','|| Last_order_Date ||','|| Last_Order_Type
from 
(
    select t2.sku_id
    , department
    , type
    , ref
    , description
    , colour
    , sku_size
    , total_stock
    , intake
    , qc_hold
    , locked
    , inProgress + allocated inProgress
    , damaged
    , returns
    , bonded
    , coalesce(buffer_inv.qty_available,0) web_buffer
    , total_stock - (intake + qc_hold + locked + inProgress + allocated + damaged + returns + bonded + coalesce(buffer_inv.qty_available,0)) available
    , web_reserve
	, Last_Receipt_Date as Last_Receipt_Date
    , case when t3.lastdate is null then '> 90 Days!' else t3.lastdate end as Last_Order_Date
    , case when t3.ordtype is null then '> 90 Days!' else t3.ordtype end  as Last_Order_Type
    from t2
    left join buffer_inv on buffer_inv.sku_id = t2.sku_id
    left join t3 on t3.sku_id = t2.sku_id
)
/
--spool off
