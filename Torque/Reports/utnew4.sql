SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9999
SET TRIMSPOOL ON


--spool utnew3.csv

select 'Date,Customer,Orders,Lines,Qty' from Dual;

select
trunc(it.Dstamp) D,
address.name A,
count(distinct it.reference_id) E,
count(*),
sum(it.update_qty) as B
from inventory_transaction it,order_header oh,address
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='UT'
and it.code like 'Shipment'
and length(it.customer_id)=3
and  it.reference_id=oh.order_id
and oh.client_id='UT'
and oh.order_type='RETAIL'
and address.client_id='UT'
and address.address_id=it.customer_id
group by
trunc(it.Dstamp),
address.name
order by 1,2
/

--spool off
