SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep '|'


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 200
SET TRIMSPOOL ON
 
--spool invadj.csv



select
A||'|'||
B||'|'||
substr(B,1,5)||'|'||
R||'|'||
C||'|'||
decode(sign(H), 1, '+', -1, '', '+')||
H||'|'||
E||'|'||
D||'|'||
G
from (select
sku.user_def_type_2  A,
--itl.sku_id B,
--DECODE(sku.user_def_type_2,'TML',itl.reference_id||sku.description||'-'||sku.sku_size,itl.sku_id) B,
DECODE(sku.user_def_type_2,'TML',itl.reference_id||sku.user_def_type_8||substr(sku.colour,5,20)||'-'||sku.sku_size,itl.sku_id) B,
itl.reference_id C,
to_char(itl.dstamp,'DD/MM/YYYY') D,
itl.reason_id E,
itl.user_id G,
itl.update_qty H,
to_char(RRR,'DD/MM/YYYY') R
from inventory_transaction itl,sku,(select
sku_id SSS,
max(dstamp) RRR
from inventory_transaction
where client_id='PR'
and code='Receipt'
group by
sku_id)
where itl.client_id='PR'
and itl.code='Adjustment'
and itl.dstamp>sysdate-1
and itl.sku_id=SSS
and sku.client_id='PR'
and itl.sku_id=sku.sku_id)
order by 1
/

--spool off

