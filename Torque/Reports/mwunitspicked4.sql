set feedback off
set pagesize 68
set newpage 0
set linesize 240
set verify off

clear columns
clear breaks
clear computes

column A heading 'Location' format A16
column B heading 'SKU' format A16
column C heading 'Qty' format 999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'Units Picked for Order '&&2' - for client ID MOUNTAIN as at ' report_date skip 2

break on report
compute sum label 'Total' of C on report


select
from_loc_id A,
sku_id B,
update_qty C
from inventory_transaction
where client_id='MOUNTAIN'
and code='Pick'
--and to_loc_id='DESPATCH'
and reference_id='&&2'
order by sku_id
/

