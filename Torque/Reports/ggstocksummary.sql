SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240
SET TRIMSPOOL ON

column I format 999999
column J format 999999
column K format 999999
column L format 999999
column M format 999999
column N format 999999


select 'Stock Summary Report - for client ID &&2 - as at  ' 
    || to_char(sysdate,'dd/mm/yy  hh24:mi:ss') from dual;

--SET TERMOUT OFF
--spool ss.csv

select 'Product Code,Description,Colour,Size,Product Group,Product Allocation,Is Kit,Total Stock,Intake,QC Hold,Suspense,Locked,Picked,Allocated,Available'
from dual;

select
i.sku_id  ||','||
sku.description  ||','||
sku.colour  ||','||
sku.sku_size  ||','||
sku.product_group  ||','||
sku.user_def_type_5  ||','||
case when nvl(sku.user_def_chk_1,'X') = 'Y' then 'Kit' else '' end || ',' ||
sum(i.qty_on_hand)  ||','||
nvl(J,0)  ||','||
nvl(K,0)  ||','||
nvl(L,0)  ||','||
nvl(L2,0)  ||','||
nvl(M,0)  ||','||
nvl(sum(i.qty_allocated),0)  ||','||
nvl(sum(i.qty_on_hand-i.qty_allocated)-nvl(J,0)-nvl(K,0)-nvl(L,0)-nvl(L2,0)-nvl(M,0),0)
from inventory i,sku,
(
	select distinct jc.sku_id JA,sum(jc.qty_on_hand) J 
	from inventory jc, location lc
	where jc.client_id='GG'
	and jc.location_id=lc.location_id
	and jc.site_id=lc.site_id
	and lc.loc_type in ('Receive Dock')
	group by jc.sku_id,jc.owner_id
), 
(
	select distinct jc.sku_id JB,sum(jc.qty_on_hand) K from inventory jc
	where jc.client_id='GG'
	and jc.condition_id='QCHOLD'
	group by jc.sku_id
	order by jc.sku_id
),
(
	select sku_id JC,
	sum(qty_on_hand-qty_allocated) L
	from inventory iv,location ll
	where iv.client_id='GG'
	and iv.location_id=ll.location_id
	and iv.site_id=ll.site_id
	and ll.loc_type='Suspense'
	group by sku_id
),
(
	select sku_id JC2,
	sum(qty_on_hand-qty_allocated) L2
	from inventory iv,location ll
	where iv.client_id='GG'
	and iv.location_id=ll.location_id
	and iv.site_id=ll.site_id
	and iv.lock_status in ('Locked','OutLocked')
	group by sku_id
),
(
	select distinct jc.sku_id JD,sum(jc.qty_on_hand) M 
	from inventory jc, location lc
	where jc.client_id='GG'
	and jc.location_id=lc.location_id
	and jc.site_id=lc.site_id
	and lc.loc_type in ('ShipDock','Stage')
	group by jc.sku_id
)
where i.client_id='GG'
and sku.client_id='GG'
and i.Sku_id=sku.sku_id
and i.sku_id = JA (+)
and i.sku_id = JB (+)
and i.sku_id = JC (+)
and i.sku_id = JC2 (+)
and i.sku_id = JD (+)
group by i.sku_id,sku.description,sku.colour,sku.sku_size,sku.product_group,sku.user_def_type_5, sku.user_def_chk_1,
nvl(J,0),nvl(K,0),nvl(L,0),nvl(L2,0),nvl(M,0)
order by i.sku_id
/

--spool off
--SET TERMOUT ON


