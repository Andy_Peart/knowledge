SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9999
SET TRIMSPOOL ON



--spool mworderactivity.csv

select 'Order Shipment Activity for '||to_char(SYSDATE-1, 'DD/MM/YYYY') from Dual
union all
select ',Orders,Units'from Dual
union all
select
substr(it.reference_id,1,3)||','||
count(distinct it.reference_id)||','||
sum(it.update_qty)
from inventory_transaction it,order_header oh
where it.client_id='MOUNTAIN'
and it.code='Shipment'
and substr(it.reference_id,1,3) in ('EBY','AMZ','WWW')
and trunc(it.Dstamp)=trunc(sysdate)-1
and it.reference_id=oh.order_id
and oh.client_id='MOUNTAIN'
and nvl(oh.priority,0)<>888
group by substr(it.reference_id,1,3)
union all
select 
'NEXT DAY'||','||
count(distinct it.reference_id)||','||
sum(it.update_qty)
from inventory_transaction it,order_header oh
where it.client_id='MOUNTAIN'
and it.code='Shipment'
and trunc(it.Dstamp)=trunc(sysdate)-1
and it.reference_id=oh.order_id
and oh.client_id='MOUNTAIN'
and oh.priority=888
union all
select
'UPGRADED'||','||
count(distinct it.reference_id)||','||
sum(it.update_qty)
from inventory_transaction it,torque.yo_total_labels yo,order_header oh
where it.client_id='MOUNTAIN'
and it.code='Shipment'
and substr(it.reference_id,1,3) in ('EBY','AMZ','WWW')
and trunc(it.Dstamp)=trunc(sysdate)-1
and it.reference_id=yo.order_num
and it.reference_id=oh.order_id
and oh.client_id='MOUNTAIN'
and oh.priority<>888
/

--spool off

