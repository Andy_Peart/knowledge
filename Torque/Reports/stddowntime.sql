SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 90


column A heading 'User' format A20
column B heading 'Range' format A30
column C heading 'Minutes' format A20

break on A skip 1 on report

select 'User downtime, Client &&2, Date: &&3' from DUAL;
 
select 'User,Time,Occurences' from DUAL;

WITH start_time as (
    select itl.user_id, itl.dstamp,
    rank() over(partition by itl.user_id order by itl.Dstamp) as rank1
    from inventory_transaction itl inner join application_user au  on au.user_id = itl.user_id
    where itl.client_id = '&&2'
    and itl.elapsed_time > 0
    and itl.dstamp between to_date('&&3') and to_date('&&3')+1
    and itl.station_id like 'RDT%'
    --and itl.user_id in ('PIOTRKO','LUKASZP')
    order by 1,2
),
end_time as (
    select itl.user_id, itl.dstamp,
    rank() over(partition by itl.user_id order by itl.Dstamp)-1 as rank1
    from inventory_transaction itl inner join application_user au  on au.user_id = itl.user_id
    where itl.client_id = '&&2'
    and itl.elapsed_time > 0
    and itl.dstamp between to_date('&&3') and to_date('&&3')+1
    and itl.station_id like 'RDT%'
    --and itl.user_id in ('PIOTRKO','LUKASZP')
    order by 1,2
)
select user_id A, periods B, to_char(occurences) C
from (
    select user_id, case
        when minutes_ <=5 then '1. From 0 to 5 minutes'
        when minutes_ >5 and minutes_ <=10 then '2. From 6 to 10 minutes'
        when minutes_ >10 and minutes_ <=15 then '3. From 11 to 15 minutes'
        when minutes_ >15 and minutes_ <=20 then '4. From 16 to 20 minutes'
        when minutes_ >20 and minutes_ <=25 then '5. From 21 to 25 minutes'
        when minutes_ >25 and minutes_ <=35 then '6. From 26 to 35 minutes'
        else '7. More than 35 minutes'
    end periods, count(*) occurences
    from (
        select A.user_id, A.dstamp, B.dstamp, extract( minute from B.dstamp-A.dstamp ) minutes_
        from start_time A left join end_time B on A.user_id = B.user_id and A.rank1 = B.rank1
        where B.dstamp is not null and extract( minute from B.dstamp-A.dstamp ) > 0
    )
    group by user_id, case
        when minutes_ <=5 then '1. From 0 to 5 minutes'
        when minutes_ >5 and minutes_ <=10 then '2. From 6 to 10 minutes'
        when minutes_ >10 and minutes_ <=15 then '3. From 11 to 15 minutes'
        when minutes_ >15 and minutes_ <=20 then '4. From 16 to 20 minutes'
        when minutes_ >20 and minutes_ <=25 then '5. From 21 to 25 minutes'
        when minutes_ >25 and minutes_ <=35 then '6. From 26 to 35 minutes'
        else '7. More than 35 minutes'
    end
    order by 1,2
)
union all
select '--------------' A, 'Details:' B, '' C from dual
union all
select 'User' A, 'Time' B, 'Minutes' C from dual
union all
select user_id A, time_range B, to_char(minutes_) C
from (
    select user_id, 'From ' || to_char(ads,'HH24:MI:SS') || ' to ' || to_char(bds,'HH24:MI:SS') time_range, minutes_
    from (
        select A.user_id, A.dstamp ads, B.dstamp bds, extract( minute from B.dstamp-A.dstamp ) minutes_
        from start_time A left join end_time B on A.user_id = B.user_id and A.rank1 = B.rank1
        where B.dstamp is not null and extract( minute from B.dstamp-A.dstamp ) > 0
    )
    order by 1,2
)
/
