SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 160

column A heading 'Order' format a20

--select 'Order No,Status' from DUAL;



Select
oh.Order_id||','||'DESPATCH' A
from order_header oh
where oh.client_id='MOUNTAIN'
and oh.status='Shipped'
and oh.shipped_date>sysdate-1
and oh.customer_id ='PAR'
union
Select
distinct oh.Order_id||','||'ON HOLD' A
from order_header oh,order_line ol
where oh.client_id='MOUNTAIN'
and oh.order_id=ol.order_id
--and oh.status in ('Released','Allocated','In Progress')
and oh.status in ('Released')
and ol.qty_ordered<>nvl(ol.qty_tasked,0)
and oh.customer_id ='PAR'
union
Select
distinct oh.Order_id||','||'ON HOLD' A
from order_header oh,order_line ol
where oh.client_id='MOUNTAIN'
and oh.order_id=ol.order_id
and oh.status in ('Allocated')
and ol.qty_ordered<>nvl(ol.qty_tasked,0)
and oh.customer_id ='PAR'
union
Select
distinct oh.Order_id||','||'ON HOLD' A
from order_header oh,order_line ol
where oh.client_id='MOUNTAIN'
and oh.order_id=ol.order_id
and oh.status in ('Complete')
and ol.qty_ordered=nvl(ol.qty_picked,0)
and oh.customer_id ='PAR'
union
Select
distinct oh.Order_id||','||'ON HOLD' A
from order_header oh,order_line ol
where oh.client_id='MOUNTAIN'
and oh.order_id=ol.order_id
and oh.status in ('In Progress')
and (ol.qty_ordered<>nvl(ol.qty_tasked,0)
or ol.qty_ordered=nvl(ol.qty_picked,0)+nvl(ol.qty_tasked,0))
and oh.customer_id ='PAR'
order by A
/


