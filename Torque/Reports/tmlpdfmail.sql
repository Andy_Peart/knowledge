/******************************************************************************/
/*                                                                            */
/*                            Elite                                           */
/*                                                                            */
/*     FILE NAME  :   TMLpdfmail.sql                                          */
/*                                                                            */
/*     DESCRIPTION:   Datastream for TML e-mail in PDF format                 */
/*                                                                            */
/*   DATE     BY   PROJ       ID         DESCRIPTION                          */
/*   ======== ==== ======     ========   =============                        */
/*   14/01/10 RW   TML                   Auto e-mail re store pickup (pdf)    */
/*   14/11/11 RW   TML                   Show collection day                  */
/*                                                                            */
/******************************************************************************/

/* Input Parameters are as follows */
-- 1)  GMT
-- 2)  Order_id
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 0
SET VERIFY OFF
SET FEEDBACK OFF
SET TAB OFF
 
-- Set the line length to the maximum possible, but trim 
-- trailing spaces for both screen output and spool files
SET LINESIZE 32767
SET WRAP ON
SET TRIMOUT ON
SET TRIMSPOOL ON

/* Define the format of the output dates */
DEFINE	OutputDateFormat = 'DD-MON-YYYY HH24:MI:SS'
COLUMN	sorter	NOPRINT

SELECT Distinct '0' sorter,
       'DSTREAM_TML_PDF_HDR'     			       	||'|'||
       rtrim('&&2')                            /* Order_id */
from dual
union all
SELECT '1' sorter,
'DSTREAM_TML_PDF_ADD_HDR'					||'|'||
rtrim (oh.purchase_order)					||'|'||
rtrim (initcap(oh.contact))					||'|'||
rtrim (oh.contact_email)					||'|'||
decode(rtrim(to_char(sysdate,'DAY')),
'MONDAY','on Tuesday',
'TUESDAY','on Wednesday',
'WEDNESDAY','on Thursday',
'THURSDAY','on Friday',
'FRIDAY','on Monday',
'SATURDAY','on Monday',
'SUNDAY','on Monday','')					||'|'||
decode (oh.user_def_type_5,
'T296','after 3pm',
'T240','after 3pm',
'T268','after 3pm',
'T237','after 3pm',
'T231','after 3pm',
'T284','after 3pm',
'T283','after 3pm',
'T263','after 3pm',
'T275','after 3pm',
'T276','after 3pm',
'T264','after 3pm',
'T259','after 3pm','')						||'|'||
rtrim ('http://maps.google.co.uk/maps?daddr='||ad.postcode)	||'|'||
decode(ad.name,null,'',ad.name||'|')||
decode(ad.address1,null,'',ad.address1||'|')||
decode(ad.address2,null,'',ad.address2||'|')||
decode(ad.town,null,'',ad.town||'|')||
decode(ad.postcode,null,'',ad.postcode)	
from order_header oh, address ad
where oh.order_id = '&&2'
and oh.client_id = 'T'
--and oh.ship_dock = ad.address_id
and oh.user_def_type_5 = ad.address_id
and ad.client_id = 'T';

exit;
