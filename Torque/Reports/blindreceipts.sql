SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 999
SET TRIMSPOOL ON

Select 'Key,Code,Client_ID,SKU_ID,Description,Reference_ID,Update Qty,Date/Time,Notes,User ID,User Def Type 2,User Def Type 1,User Def Type 4,Size' from DUAL;

column A format 9999999999
column B format A12
column C format A12
column D format A20
column E format A40
column F format A16
column G format 99999
column H format A24
column I format A80
column J format A12
column K format A16
column L format A16
column M format A16
column N format A12



set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



--ttitle  LEFT 'TML Retail - Open Orders Report ' RIGHT 'Printed ' report_date skip 2


--break on AA skip 1 on report

--compute sum label 'Total' of D on report



select
it.Key A,
it.Code B,
it.Client_ID C,
it.SKU_ID D,
SKU.Description E,
it.Reference_id F,
it.Update_qty G,
to_char(it.Dstamp,'DD-MON-YYYY  HH:MI:SS') H,
it.Notes I,
it.User_ID J,
SKU.User_Def_Type_2 K,
SKU.User_Def_Type_1 L,
SKU.User_Def_Type_4 M,
SKU.SKU_Size N
from inventory_transaction it,sku
where it.client_id='&&2'
and it.Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and it.code='Receipt'
and
(
	it.reference_id not in (select pre_advice_id from pre_advice_header where client_id='&&2')
	or
	it.reference_id is null
)
and sku.client_id='&&2'
and it.sku_id=sku.sku_id
/
