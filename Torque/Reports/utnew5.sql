SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9999
SET TRIMSPOOL ON


--spool utnew5.csv

select 'Date,Customer,Order Type,Orders,Lines,Qty' from Dual;

select
D||','||
A||','||
T||','||
E||','||
C||','||
B
from (select
trunc(it.Dstamp) D,
nvl(NM,it.customer_id) A,
oh.order_type T,
count(distinct it.reference_id) E,
count(*) C,
sum(it.update_qty) as B
from inventory_transaction it,order_header oh,(select
name NM,
address_id ID
from address 
where client_id='UT')
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='UT'
and it.code like 'Shipment'
and  it.reference_id=oh.order_id
and oh.client_id='UT'
and oh.order_type not in ('WEB','WEBMAN')
and it.customer_id=ID (+)
group by
trunc(it.Dstamp),
nvl(NM,it.customer_id),
oh.order_type
union all
select
trunc(it.Dstamp) D,
nvl(NM,it.customer_id) A,
oh.order_type T,
count(distinct it.reference_id) E,
count(*) C,
sum(it.update_qty) as B
from inventory_transaction_archive it,order_header_archive oh,(select
name NM,
address_id ID
from address 
where client_id='UT')
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='UT'
and it.code like 'Shipment'
and  it.reference_id=oh.order_id
and oh.client_id='UT'
and oh.order_type not in ('WEB','WEBMAN')
and it.customer_id=ID (+)
group by
trunc(it.Dstamp),
nvl(NM,it.customer_id),
oh.order_type
order by D,A)
/

--spool off
