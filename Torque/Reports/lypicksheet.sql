SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 66
SET LINESIZE 200
SET TRIMSPOOL ON
SET HEADING OFF
SET NEWPAGE 0

--spool SHps.txt

column AA noprint
column R heading 'Order' format a15
column A heading 'SKU' format a22
column A2 heading 'KIT SKU' format a12
column B heading ' ' format a4
column C heading 'Location' format a12
column D format 999999
column D1 format 9999999999
column D2 heading 'Description' format a40
column E heading 'Picked' format A12
column F heading 'Line Id' format 99999
column M noprint
column N noprint

TTITLE  CENTER "LOVE YAWN KIT PICK SHEET for &&2" RIGHT "Page:" FORMAT 999 SQL.PNO skip 2 -
        LEFT "Order           Line Id SKU ID  Order Qty Kit Qty Actual Pick Kit Sku Id   Description                " skip 1 -
        LEFT "-----           ------- ------  --------- ------- ----------- ----------   -----------                " skip 2


break on row skip 1 on R skip page ON F ON A

select
ol.order_id R,
ol.line_id F,
ol.sku_id||' - '||
nvl(ol.qty_tasked,0) a,
kt.quantity D,
nvl(ol.qty_tasked,0)*kt.quantity D1,
'   ',
kt.sku_id A2,
CASE
WHEN oh.country like 'GB%' THEN sku.description 
ELSE
DECODE(kt.sku_id,'6044001','** '||substr(sku.description,1,21)||' ** DG NOTE REQ **',sku.description)
end d2
from order_header oh inner join order_line ol 
on oh.order_id = ol.order_id 
and oh.client_id = ol.client_id
inner join sku 
on ol.sku_id = sku.sku_id 
and ol.client_id = sku.client_id
left join kit_line kt 
on kt.kit_id=ol.sku_id 
and kt.client_id = 'LY'
where oh.client_id='LY'
and (oh.order_id='&&2'
or oh.consignment='&&2')
order by R,F
/
