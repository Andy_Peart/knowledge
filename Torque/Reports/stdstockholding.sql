SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 

column A heading 'Category' format A16
column B heading '&&2 Qty' format 9999999
column C heading 'VH Qty' format 9999999
column D heading 'ALL' format 9999999

set heading off
set pagesize 100

select 'Stock Holding Report for Client ID &&2 on ',to_char(SYSDATE, 'DD/MM/YY    HH:MI:SS') from DUAL;

set heading on




select
'Stock' A,
sum(i.qty_on_hand) B
from inventory i
where i.client_id like '&&2'
/

SET PAGESIZE 0

select
'Allocated' A,
sum(i.qty_allocated) B
from inventory i
where i.client_id like '&&2'
/
select
'Available' A,
sum(i.qty_on_hand-i.qty_allocated) B
from inventory i
where i.client_id like '&&2'
/

select 'in Receipt Dock' A,
nvl(sum(jc.qty_on_hand),0) B 
from inventory jc,location lc
where jc.client_id like '&&2'
and jc.location_id=lc.location_id
and jc.site_id=lc.site_id
and lc.loc_type='Receive Dock'
/

select 'in QCHOLD' A,
nvl(sum(jc.qty_on_hand),0) B 
from inventory jc
where jc.condition_id is not null
and jc.client_id like '&&2'
/

select 
'in SUSPENSE ' A,
nvl(sum(jc.qty_on_hand),0) B
from inventory jc
where jc.location_id='SUSPENSE'
and jc.client_id like '&&2'
/

select '' from DUAL;

select
'Full Total' A,
sum(i.qty_on_hand) B
from inventory i
where i.client_id like '&&2'
/
