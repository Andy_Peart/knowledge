SET FEEDBACK OFF                 
set verify off
set wrap off
set colsep ','

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 132
set trimspool on

--BREAK ON AAA skip 1 dup

--spool mwmultilocs.csv

select ',MOUNTAIN stock in multiple locations' from DUAL;
select 'SKU,Description,Work Zone,Location,Qty' from DUAL;

select
''''||i.SKU_ID||''','||
i.description||','||
L.work_zone||','||
''''||i.location_id||''','||
sum(i.qty_on_hand)
from inventory i,(select
sku_id A,
count(distinct location_id) B
from inventory
where client_id = 'MOUNTAIN'
and location_id not like 'Z%'
and site_id = 'BD2'
and location_id not in
('MAIL ORDER',
'MARSHALL',
'DESPATCH',
'STG',
'CONTAINER',
'SUSPENSE',
'UKPAC',
'UKPAR')
group by
sku_id),location L
where b > 1
and i.location_id not like 'Z%'
and i.location_id not in
('MAIL ORDER',
'MARSHALL',
'DESPATCH',
'STG',
'CONTAINER',
'SUSPENSE',
'UKPAC',
'UKPAR')
and i.sku_id = A
and i.location_id=L.location_id and i.site_id = l.site_id
group by
i.SKU_ID,
i.description,
L.work_zone,
i.location_id
order by 1
/


--spool off


