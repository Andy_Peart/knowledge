SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

column A heading 'Group' format a6
column B heading 'Grp2' format a8
column C heading 'Qty' format 999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

Select 'Receipts last week' from Dual;
select ',,Total' from DUAL;

break on A skip 1 on report

compute sum label 'Total' of C on A
compute sum label 'Total' of C on report

SELECT
SKU.PRODUCT_GROUP A,
SKU.USER_DEF_TYPE_1 B,
sum(it.UPDATE_QTY) C
from inventory_transaction it,sku
where it.client_id='TR'
and it.code='Receipt'
and trunc(it.dstamp)>trunc(sysdate-8)
and trunc(it.dstamp)<trunc(sysdate)
and it.sku_id=sku.sku_id
and it.client_id=sku.client_id
group by
SKU.PRODUCT_GROUP,
SKU.USER_DEF_TYPE_1
order by
SKU.USER_DEF_TYPE_1
/
