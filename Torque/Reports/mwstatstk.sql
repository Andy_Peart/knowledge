SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Sku,Description,Qty on Hand,Condition,Lock Status' from dual
union all
select "Sku" ||','|| "Desc" ||','|| "Qty" ||','|| "Cond" ||','|| "Lock"
from
(
select i.sku_id as "Sku"
, s.description as "Desc"
, sum(i.qty_on_hand) as "Qty"
, i.condition_id    as "Cond"
, i.lock_status as "Lock"
from inventory i
inner join sku s on s.client_id = i.client_id and s.sku_id = i.sku_id
inner join location l on l.site_id = i.site_id and l.location_id = i.location_id
where i.client_id = 'MOUNTAIN'
and l.work_zone in ('BD1STAT','13')
group by i.sku_id
, s.description
, i.condition_id
, i.lock_status
order by 1
)
/
--spool off