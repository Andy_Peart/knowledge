SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Date,User ID,Code,Units' from dual
union all
select "Date" ||','|| "User" ||','|| "Code" ||','|| "Qty"
from
(
select trunc(it.dstamp) as "Date"
, it.user_id            as "User"
, it.code               as "Code"
, sum(it.update_qty)    as "Qty"
from inventory_transaction it
where it.client_id = '&&2'
and it.dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and it.code in (
'Pick'
, 'Relocate'
, 'Receipt'
, 'Return'
)
and it.from_loc_id <> 'CONTAINER'
group by trunc(it.dstamp)
, it.user_id
, it.code
order by 1, 2, 3
)
/
--spool off