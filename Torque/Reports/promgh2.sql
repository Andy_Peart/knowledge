SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 200
SET TRIMSPOOL ON





column A heading 'SKU' format A20
column B heading 'Qty1' format 999999
column C heading 'Qty2' format 999999
column D heading 'Qty3' format 9999.99
column E heading 'Cust' format A12
column F heading 'Ref1' format A28
column G heading 'Ref2' format A28
column DD heading 'Date' format A12








--spool promgh2.csv

select 'PROMINENT Receipts/Shipments from &&2 to &&3' from DUAL;

select 'SKU,Received Units,Despatched Units,% Shipped,Customer Name,Receipt ID,Order Ref.,Despatch Date' from dual;


select
F A,
QQQ B,
nvl(SSS,0) C,
(nvl(SSS,0)*100)/QQQ D,
A E,
'"'||B||'"' F,
BB G,
DD 
from (select
sku.user_def_type_2  A,
itl.reference_id B,
itl.tag_id D,
itl.sku_id F,
sum(itl.update_qty) QQQ
from inventory_transaction itl,sku,location ll
where itl.client_id='PR'
and itl.station_id not like 'Auto%'
and itl.code='Receipt'
and itl.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
--and itl.dstamp>sysdate-1
and itl.to_loc_id=ll.location_id
and ll.site_id='PROM'
and ll.zone_1<>'P'
and sku.client_id='PR'
and sku.user_def_type_2 like '%&&4%'
and itl.sku_id=sku.sku_id
group by
sku.user_def_type_2,
itl.reference_id,
itl.tag_id,
itl.sku_id,
ll.zone_1),(select
trunc(itl.Dstamp) DD,
itl.reference_id BB,
itl.tag_id T2,
itl.sku_id S2,
sum(itl.update_qty) SSS
from inventory_transaction itl,sku
where itl.client_id='PR'
and itl.code='Shipment'
and itl.Dstamp>to_date('&&2', 'DD-MON-YYYY')
--and itl.dstamp>sysdate-1
and sku.client_id='PR'
and sku.user_def_type_2 like '%&&4%'
and itl.sku_id=sku.sku_id
group by
trunc(itl.Dstamp),
itl.reference_id,
itl.tag_id,
itl.sku_id)
where F=S2
and D=T2
order by A,F,B,DD
/

--spool off

