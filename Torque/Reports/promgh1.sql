SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 1500
SET TRIMSPOOL ON
 

column A format A30
column B format A40
column C format A30
column D format A30
column G1 format 9999999
column G2 format 9999999
column G3 format 9999999
column hang_box format A1

break on A dup skip 0

--spool promgh.csv

select 'PROMINENT Shipments from &&2 to &&3' from DUAL;

select 'Order H/B,Customer Name,Order reference,Shipped date,Lines shipped,Units shipped(B),Units shipped(H),IsHanging' from dual;


select
oh.user_def_type_3 C,
A,
B,
D,
count(*) G1,
sum(H-nvl(H2,0)) G2,
sum(nvl(H2,0)) G3,
hang_box
from (select
sku.user_def_type_2  A,
itl.reference_id B,
to_char(itl.dstamp,'DD/MM/YYYY') D,
itl.line_id E,
itl.sku_id F,
sum(itl.update_qty*nvl(sku.user_def_type_9,1)*DECODE(itl.code,'Shipment',1,-1)) H,
decode(sku.PRODUCT_GROUP, 'MJ', 'Y',
'WJ', 'Y',
'MP', 'Y',
'WP', 'Y',
'MW', 'Y', 'N') as hang_box 
from inventory_transaction itl,sku
where itl.client_id in ('PR','PS')
--and itl.zone_1<>'P'
and itl.code in ('UnShipment','Shipment')
--and itl.dstamp>sysdate-1
--and itl.from_loc_id='PR'
and itl.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and sku.client_id in ('PR','PS')
and itl.sku_id=sku.sku_id
group by
sku.user_def_type_2,
itl.reference_id,
to_char(itl.dstamp,'DD/MM/YYYY'),
itl.line_id,
itl.sku_id,
decode(sku.PRODUCT_GROUP, 'MJ', 'Y',
'WJ', 'Y',
'MP', 'Y',
'WP', 'Y',
'MW', 'Y', 'N')),(select
sku.user_def_type_2  A2,
itl.reference_id B2,
to_char(itl.dstamp,'DD/MM/YYYY') D2,
itl.line_id E2,
itl.sku_id F2,
sum(itl.update_qty*nvl(sku.user_def_type_9,1)*DECODE(itl.code,'Shipment',1,-1)) H2,
decode(sku.PRODUCT_GROUP, 'MJ', 'Y',
'WJ', 'Y',
'MP', 'Y',
'WP', 'Y',
'MW', 'Y', 'N') as hang_box2    
from inventory_transaction itl,sku
where itl.client_id in ('PR','PS')
--and itl.zone_1<>'P'
and itl.code in ('UnShipment','Shipment')
--and itl.dstamp>sysdate-1
--and itl.from_loc_id='PR'
and itl.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and sku.client_id in ('PR','PS')
and sku.user_def_type_1= 'H'
and itl.sku_id=sku.sku_id
group by
sku.user_def_type_2,
itl.reference_id,
to_char(itl.dstamp,'DD/MM/YYYY'),
itl.line_id,
itl.sku_id,
decode(sku.PRODUCT_GROUP, 'MJ', 'Y',
'WJ', 'Y',
'MP', 'Y',
'WP', 'Y',
'MW', 'Y', 'N')),order_header oh
where A=A2(+)
and B=B2(+)
and D=D2(+)
and E=E2(+)
and F=F2(+)
and B=oh.order_id
and oh.client_id in ('PR','PS')
group by
oh.user_def_type_3,
A,B,D,hang_box
order by 1
/

--spool off


