SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON


select 'Completion date, Client, GRN, Docket, Style, Qty Received' from dual
union all
select fin_date || ',' || client_id  || ',' || pre_advice_id  || ',' ||  docket  || ',' || styl  || ',' || qty_rec
from (
select to_char(ph.finish_dstamp, 'DD-MM-YYYY') as fin_date, ph.client_id, ph.pre_advice_id, pl.user_def_type_8 docket, pl.user_def_type_7 styl, NVL(sum(pl.qty_received),0) as qty_rec
from pre_advice_header ph inner join pre_advice_line pl on ph.pre_advice_id  = pl.pre_advice_id and ph.client_id  = pl.client_id
where ph.client_id in ('T','TR')
and pl.client_id in ('T', 'TR')
and ph.supplier_id = 'PROMIN'
and ph.status = 'Complete'
and trunc(ph.finish_dstamp) > trunc(sysdate)-'&2'
group by to_char(ph.finish_dstamp, 'DD-MM-YYYY'), ph.client_id, ph.pre_advice_id, pl.user_def_type_8, pl.user_def_type_7
order by 1,2,3
)
/