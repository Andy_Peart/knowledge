SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON

column A heading 'Client' format a12
column B heading 'Date' format a20
column C heading 'Qty ord' format 999999999
column D heading 'Qty alloc' format 999999999
column E heading 'Qty ship' format 999999999
column F heading 'A to O' format 9999999.99
column G heading 'S to A' format 9999999.99
column H heading 'S to O' format 9999999.99

break on report
compute sum label 'Totals' of C D E on report
compute avg label 'Averages' of F G H on report

select case when '&&5' = 'Y' then 'Internationl &&6 orders' else 'Non-International &&6 orders' end from dual;

select 'Client,Date Shipped, Qty Ordered, Qty Allocated, Qty Shipped, Allocated/Ordered, Shipped/Allocated, Shipped/Ordered' from dual;

select clientid A, shipped_date B, QtyOrdered C, QtyAllocated D, QtyShipped E, round(AllocatedVOrdered,2) F, round(AllocatedFulfilmentPercentage,2) G, round(OrderFulfillmentPercentage,2) H
from (
select 
            clientid,
            shipped_date,
            nvl(QtyOrderedQ1,0) QtyOrdered, 
            nvl(QtyOrderedQ1-TotalShortsQ2,0) QtyAllocated,
            nvl(QtyShippedQ3,0) QtyShipped, 
            to_char((nvl(QtyOrderedQ1-TotalShortsQ2,0)) /(nvl(QtyOrderedQ1,0)) *100,'999.000' ) AllocatedVOrdered,
            to_char((QtyShippedQ3/(QtyOrderedQ1-TotalShortsQ2))*100,'999.000') AllocatedFulfilmentPercentage,
            to_char((QtyShippedQ3/QtyOrderedQ1)*100,'999.000')  OrderFulfillmentPercentage            
  from (
                select clientid, shipped_date, OrderType, nvl(Sum(TotalShorts),0) TotalShortsQ2, sum(QtyOrderedSum) QtyOrderedQ1, sum(QtyShippedSum) QtyShippedQ3
                from  (
                                select oh.client_Id clientid, nvl(oh.order_type,' ') OrderType, oh.order_id OrderId,sum(ol.qty_ordered) QtyOrderedSum,sum(ol.qty_shipped) QtyShippedSum, to_char(shipped_date,'DD-MM-YYYY') shipped_date
                                from order_header oh
                                  inner join order_line ol on ol.client_id = oh.client_id and ol.order_id = oh.order_id
                                where oh.status='Shipped'
                                and oh.client_id = '&&2'
                                 and oh.shipped_date between to_date('&&3') and to_date('&&4')+1
                                  --international?
                                  and (
                                    ('&&5' = 'Y' and oh.customer_id like 'W%' and oh.customer_id not in ('WH1', 'WH2'))
                                    or
                                    ('&&5' = 'N' and (oh.customer_id not like 'W%'  or oh.customer_id in ('WH1','WH2')))
                                    or
                                    ('&&5' = 'ALL')		
                                  )
                                    --web or retail?
                                  and (
                                    ('&&6' = 'WEB' and oh.order_type = 'WEB')
                                    or
                                    ('&&6' = 'RETAIL' and oh.order_type = 'RETAIL')
                                    or
                                    ('&&6' = 'ALL')
                                  )
                                group by oh.client_id, oh.order_type,  oh.order_id, to_char(shipped_date,'DD-MM-YYYY')
                              ),
                              (
                                select oh.client_id clientIdShort, oh.order_id OrderIdShort, sum(gs.qty_ordered-gs.qty_tasked) TotalShorts, oh.status
                                from generation_shortage gs,order_header oh
                                where gs.task_id=oh.order_id
                                  and gs.client_id=oh.client_id
                                group by  oh.client_id, oh.order_id, status
                              )
               where clientid=clientIdShort (+)
               and OrderId=OrderIdShort  (+)
               group by OrderType, shipped_date, clientid
            )
)
ORDER BY 1,2
/
