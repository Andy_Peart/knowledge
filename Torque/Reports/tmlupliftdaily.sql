/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :     tmlupliftdaily.sql                         		      */
/*                                                                            */
/*     DESCRIPTION:                                                           */
/*     Automated uplifts daily report								          */
/*                                                                            */
/*   DATE     BY   DESCRIPTION						                          */
/*   ======== ==== ===========					                              */
/*   09/08/2013 YB   Created new report for automated uplifts				  */
/******************************************************************************/
SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','
SET WRAP OFF


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 800

/* Column Headings and Formats */
COLUMN A heading "Uplift" FORMAT A14
COLUMN B heading "Store Name" FORMAT A20
COLUMN C HEADING "Date" FORMAT A20
COLUMN D HEADING "SKU" FORMAT A15
COLUMN E heading "Received Qty" FORMAT 9999
COLUMN F heading "CIMs Code" FORMAT A20
COLUMN G heading "Size" FORMAT A10
COLUMN H HEADING "Colour" FORMAT A15


break on report

compute sum label 'Total' of E on report

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

--SPOOL TML_UPLIFT_DAILY_.CSV

/* Top Title */
select 'Uplift No, Store Name, Date Received, SKU, Received Qty, CIMs ode, Size, Colour' from DUAL;

SELECT
  CASE
    WHEN itl.reference_id IS NULL
    THEN 'T995'
    ELSE itl.reference_id
  END                 AS A,
  pah.name            AS B,
  to_char(ITL.DSTAMP, 'HH:MI:SS DD-MON-YYYY') AS C,
  itl.sku_id          AS D,
  SUM(ITL.UPDATE_QTY) AS E,
  SKU.USER_DEF_TYPE_2||'-'||SKU.USER_DEF_TYPE_1||'-'||SKU.USER_DEF_TYPE_4 AS F,
  sku.sku_size                            AS G,
  SUBSTR(SKU.COLOUR,6,LENGTH(SKU.COLOUR)) AS H
FROM inventory_transaction itl
LEFT JOIN pre_advice_header pah
ON itl.reference_id=pah.pre_advice_id
AND itl.client_id  =pah.client_id
LEFT JOIN sku
ON itl.sku_id       =sku.sku_id
AND sku.client_id   ='TR'
WHERE itl.client_id = 'TR'
AND itl.code        = 'Adjustment'
AND (ITL.REFERENCE_ID LIKE '%/%'
OR itl.reason_id = 'T995')
AND itl.dstamp BETWEEN to_date('&2', 'DD-MM-YYYY') AND to_date('&3', 'DD-MM-YYYY')+1
GROUP BY
  CASE
    WHEN itl.reference_id IS NULL
    THEN 'T995'
    ELSE itl.reference_id
  END,
  pah.name,
  to_char(ITL.DSTAMP, 'HH:MI:SS DD-MON-YYYY'),
  itl.sku_id,
  SKU.USER_DEF_TYPE_2||'-'||SKU.USER_DEF_TYPE_1||'-'||SKU.USER_DEF_TYPE_4,
  sku.sku_size,
  SUBSTR(SKU.COLOUR,6,LENGTH(SKU.COLOUR))
ORDER BY
  CASE
    WHEN itl.reference_id IS NULL
    THEN 'T995'
    ELSE itl.reference_id
  END
/
--SPOOL OFF
