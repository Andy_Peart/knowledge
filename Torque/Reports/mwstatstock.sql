SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON

break on report
compute sum of Q on report

--spool mwstatstock.csv

--select 'Stationary Stock Levels' from DUAL;
select 'Sku Id,Description,Stock on Hand' from Dual;


select
sku_id,
description,
sum(qty_on_hand) Q
from inventory
where client_id='MOUNTAIN'
and lower(sku_id)<>upper(sku_id)
and location_id not in ('DESPATCH', 'CONTAINER')
group by
sku_id,
description
order by 
sku_id
/

--spool off
