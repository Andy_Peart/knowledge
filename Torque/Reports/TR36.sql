SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 50

--set termout off
--spool TR036.csv

select 'TR36 Shipments yesterday '||to_char(sysdate-1,'DD/MM/YYYY') from Dual;


select
DG||',,'||
CID||','||
nvl(CCC,0)||','||
'End'
from
(select
CID,
DG
from
(select distinct DECODE (product_group,'MS','Mens Shirts',
    'ML','Cuff Links',
    'MA','Mens Accessories',
    'MC','Mens Casuals',
    'MT','Mens Ties',
    'WA','Womens Accessories',
    'WS','Womens Shirts',
    'WK','Womens Separates',
    'MU','Mens Tailoring',
    'WU','Womens Tailoring',user_def_type_1) DG
from sku
where client_id='TR'
and length(product_group)=2),
(SELECT
distinct sm.CUSTOMER_ID CID
from shipping_manifest sm
where sm.client_id='TR'
and trunc(sm.shipped_dstamp)=trunc(sysdate-1))),
(SELECT
sm.CUSTOMER_ID AAA,
DECODE (sku.product_group,'MS','Mens Shirts',
    'ML','Cuff Links',
    'MA','Mens Accessories',
    'MC','Mens Casuals',
    'MT','Mens Ties',
    'WA','Womens Accessories',
    'WS','Womens Shirts',
    'WK','Womens Separates',
    'MU','Mens Tailoring',
    'WU','Womens Tailoring',sku.user_def_type_1) BBB,
sum(sm.QTY_SHIPPED) CCC
from shipping_manifest sm, order_header oh, sku
where oh.client_id='TR'
and sm.client_id='TR'
and trunc(sm.shipped_dstamp)=trunc(sysdate-1)
and sm.customer_id=oh.customer_id
and sm.order_id=oh.order_id
and sm.consignment=oh.consignment
and sm.sku_id=sku.sku_id
and sku.client_id='TR'
group by
sm.CUSTOMER_ID,
DECODE (sku.product_group,'MS','Mens Shirts',
    'ML','Cuff Links',
    'MA','Mens Accessories',
    'MC','Mens Casuals',
    'MT','Mens Ties',
    'WA','Womens Accessories',
    'WS','Womens Shirts',
    'WK','Womens Separates',
    'MU','Mens Tailoring',
    'WU','Womens Tailoring',sku.user_def_type_1))
where DG=BBB (+)
and CID=AAA (+)
order by upper(DG),CID
/



select
'Total'||',,'||
CID||','||
nvl(CCC,0)||','||
'End'
from
(SELECT
distinct sm.CUSTOMER_ID CID
from shipping_manifest sm
where sm.client_id='TR'
and trunc(sm.shipped_dstamp)=trunc(sysdate-1)),
(SELECT
sm.CUSTOMER_ID AAA,
sum(sm.QTY_SHIPPED) CCC
from shipping_manifest sm, order_header oh, sku
where oh.client_id='TR'
and sm.client_id='TR'
and trunc(sm.shipped_dstamp)=trunc(sysdate-1)
and sm.customer_id=oh.customer_id
and sm.order_id=oh.order_id
and sm.consignment=oh.consignment
and sm.sku_id=sku.sku_id
and sku.client_id='TR'
group by
sm.CUSTOMER_ID)
where CID=AAA (+)
order by CID
/


--spool off
--set termout on
