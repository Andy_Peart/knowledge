SET FEEDBACK OFF             
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 400
SET TRIMSPOOL ON

select 'ITL' from DUAL;


select '"ITL'||','
||'E'||','
||'Shipment'||','
||decode(sign(i.original_qty), 1, '+', -1, '-', '+')||','
||i.original_qty||','
||decode(sign(i.update_qty), 1, '+', -1, '-', '+')||','
||i.update_qty||','
||to_char(i.dstamp, 'YYYYMMDDHH24MISS')||','
||i.client_id||','
--||i.sku_id||','
||s.ean||','
||i.from_loc_id||','
||i.to_loc_id||','
||i.tag_id||','
||i.reference_id||','
||i.line_id||','
||i.condition_id||','
||i.notes||','
||i.reason_id||','
||i.batch_id||','
||i.expiry_dstamp||','
||i.user_id||','
||i.shift||','
||i.station_id||','
||'TORQUE'||','
||i.container_id||','
||i.pallet_id||','
||i.list_id||','
||i.owner_id||','
||i.origin_id||','
||i.work_group||','
||i.consignment||','
||i.manuf_dstamp||','
||i.lock_status||','
||i.qc_status||','
||i.session_type||','
||i.summary_record||','
||i.elapsed_time||','
||i.supplier_id||','
||i.user_def_type_1||','
||i.user_def_type_2||','
||i.user_def_type_3||','
||i.user_def_type_4||','
||i.user_def_type_5||','
||i.user_def_type_6||','
||i.user_def_type_7||','
||i.user_def_type_8||','
||i.user_def_chk_1||','
||i.user_def_chk_2||','
||i.user_def_chk_3||','
||i.user_def_chk_4||','
||i.user_def_date_1||','
||i.user_def_date_2||','
||i.user_def_date_3||','
||i.user_def_date_4||','
||i.user_def_num_1||','
||i.user_def_num_2||','
||i.user_def_num_3||','
||i.user_def_num_4||','
||i.user_def_note_1||','
||i.user_def_note_2||','
||i.from_site_id||','
||i.to_site_id||','
||''||','
||i.job_id||','
||i.job_unit||','
||i.manning||','
||i.spec_code||','
||i.config_id||','
||i.estimated_time||','
||i.task_category||','
||i.sampling_type||','
||to_char(i.complete_dstamp, 'YYYYMMDDHH24MISS')||','
||i.grn||','
||i.group_id||','
||i.uploaded||','
||i.uploaded_vview||','
||i.uploaded_ab||','
||i.sap_idoc_type||','
||i.sap_tid||','
||i.ce_orig_rotation_id||','
||i.ce_rotation_id||','
||i.ce_consignment_id||','
||i.ce_receipt_type||','
||i.ce_originator||','
||i.ce_originator_reference||','
||i.ce_coo||','
||i.ce_cwc||','
||i.ce_ucr||','
||i.ce_under_bond||','
||''||','
||i.uploaded_customs||','
||i.uploaded_labor||','
||i.asn_id||','
--||i.customer_id||','
||oh.user_def_type_2||','
||i.print_label_id||','
||i.lock_code||','
||i.ship_dock||','
||''||','
||i.pallet_grouped||'"'
from inventory_transaction i, sku s,order_header oh
where i.client_id = 'OPS'
and ((i.code = 'Shipment') or (i.code = 'Pick' and i.update_qty=0))
and i.uploaded = 'N'
--and i.dstamp > sysdate-1
and s.sku_id=i.sku_id
and s.client_id='OPS'
and s.product_group ='SM'
and i.reference_id<>'030386'
and i.reference_id<>'030386/1' 
and i.reference_id in (select 
order_id from order_header
where client_id='OPS'
--and status='Shipped'
and shipped_date>sysdate-2)
and i.reference_id=oh.order_id
and oh.client_id='OPS'
/


update inventory_transaction
set uploaded='Y'
where client_id = 'OPS'
and ((code = 'Shipment') or (code = 'Pick' and update_qty=0))
and uploaded = 'N'
and sku_id in (select
sku_id from sku
where client_id='OPS'
and product_group='SM')
and reference_id in (select 
order_id from order_header
where client_id='OPS'
--and status='Shipped'
and shipped_date>sysdate-2)
/


commit;
