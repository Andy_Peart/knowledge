SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','
SET UND OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 72




column A heading 'SKU' format A18
column B heading 'Description' format A40
column C heading 'Qty' format 999999


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'dd/mm/yyyy') curdate from DUAL;
set TERMOUT ON

select 'PRINGLE ADJUSTMENTS actioned on ' 
        ||  to_char(sysdate,'dd/mm/yyyy') from dual;

select 'SKU,Description,Qty,,' from DUAL;

break on report

compute sum label 'Total'  of C  on report

select 
AA A,
BB B,
CC C
from (Select 
it.SKU_ID AA,
sku.description BB,
sum(it.Update_qty) CC
from inventory_transaction it,sku
where it.client_id='PRI'
and it.code='Adjustment'
and trunc(it.Dstamp)=trunc(sysdate)
and it.sku_id=sku.sku_id
and sku.client_id='PRI'
group by
it.SKU_ID,
sku.description)
where CC<>0
order by
A
/
 
