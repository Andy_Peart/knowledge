SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Code,Sku,Description,from Location,to Location,Zone,Qty Moved,User,Sku Type,Ugly,Error' from dual
union all
select "Code" || ',''' || "Sku" || ''',' || "Desc" || ',' || "from Location" || ',' || "to Location" || ',' || "Zone" || ',' || "Qty Moved" || ',' || "User" || ',' || "Sku Type" || ',' || "Ugly" || ',' || "Error" from
(
select * from 
(
with rp as (
select sku_id   as "Sku"
, 'Ratio Pack'  as "Sort"
from sku_sku_config
where config_id like '%E%C%'
and client_id = 'MOUNTAIN'
), pc as (
select config_id    as "Sku"
, 'Pack Config'     as "Sort"
from sku_config
where client_id = 'MOUNTAIN'
and track_level_2 = 'PACK'
)
--Non Ugly Pack Configs
select it.code                          as "Code"
, it.sku_id                             as "Sku"
, s.description                         as "Desc"
, it.from_loc_id                        as "from Location"
, it.to_loc_id                          as "to Location"
, l.work_zone                           as "Zone"
, sum(it.update_qty)                    as "Qty Moved"
, it.user_id                            as "User"
, pc."Sort"                             as "Sku Type"
, nvl(s.ugly,'N')                       as "Ugly"
, 'Should be relocated to 3* location'  as "Error"
from inventory_transaction it
inner join sku s on s.client_id = it.client_id and s.sku_id = it.sku_id
inner join pc on pc."Sku" = it.sku_id
inner join location l on l.site_id = it.site_id and l.location_id = it.to_loc_id
where it.client_id = 'MOUNTAIN'
and (it.from_loc_id like 'REP%' or it.from_loc_id like 'Z%')
and it.to_loc_id not like ('3%')
and it.dstamp between to_date('&&2','DD-MON-YYYY') and to_date('&&3','DD-MON-YYYY')+1
and it.code in ('Relocate','Putaway','Receipt','Stock Check','Adjustment')
and l.loc_type in ('Tag-FIFO','Tag-LIFO')
and l.work_zone not in ('BULK','G')
and nvl(s.ugly,'N') = 'N'
group by it.code
, it.from_loc_id
, it.to_loc_id
, l.work_zone
, it.sku_id
, s.description
, it.user_id
, pc."Sort"
, s.ugly
union all
--Ugly Pack Configs
select it.code                          as "Code"
, it.sku_id                             as "Sku"
, s.description                         as "Desc"
, it.from_loc_id                        as "from Location"
, it.to_loc_id                          as "to Location"
, l.work_zone                           as "Zone"
, sum(it.update_qty)                    as "Qty Moved"
, it.user_id                            as "User"
, pc."Sort"                             as "Sku Type"
, nvl(s.ugly,'N')                       as "Ugly"
, 'Should be relocated to W* location'  as "Error"
from inventory_transaction it
inner join sku s on s.client_id = it.client_id and s.sku_id = it.sku_id
inner join pc on pc."Sku" = it.sku_id
inner join location l on l.site_id = it.site_id and l.location_id = it.to_loc_id
where it.client_id = 'MOUNTAIN'
and (it.from_loc_id like 'REP%' or it.from_loc_id like 'Z%')
and it.to_loc_id not like ('W%')
and it.dstamp between to_date('&&2','DD-MON-YYYY') and to_date('&&3','DD-MON-YYYY')+1
and it.code in ('Relocate','Putaway','Receipt','Stock Check','Adjustment')
and l.loc_type in ('Tag-FIFO','Tag-LIFO')
and l.work_zone not in ('BULK','G')
and nvl(s.ugly,'N') = 'Y'
group by it.code
, it.from_loc_id
, it.to_loc_id
, l.work_zone
, it.sku_id
, s.description
, it.user_id
, pc."Sort"
, s.ugly
union all
--Non Ugly Ratio Packs
select it.code                          as "Code"
, it.sku_id                             as "Sku"
, s.description                         as "Desc"
, it.from_loc_id                        as "from Location"
, it.to_loc_id                          as "to Location"
, l.work_zone                           as "Zone"
, sum(it.update_qty)                    as "Qty Moved"
, it.user_id                            as "User"
, rp."Sort"                             as "Sku Type"
, nvl(s.ugly,'N')                       as "Ugly"
, 'Should be relocated to A* or 2* location'  as "Error"
from inventory_transaction it
inner join rp on rp."Sku" = it.sku_id
inner join sku s on s.client_id = it.client_id and s.sku_id = it.sku_id
inner join location l on l.site_id = it.site_id and l.location_id = it.to_loc_id
where it.client_id = 'MOUNTAIN'
and it.to_loc_id not like 'A%' 
and it.to_loc_id not like '2%'
and (it.from_loc_id like 'REP%' or it.from_loc_id like 'Z%')
and nvl(s.ugly,'N') = 'N'
and it.dstamp between to_date('&&2','DD-MON-YYYY') and to_date('&&3','DD-MON-YYYY')+1
and it.code in ('Relocate','Putaway','Receipt','Stock Check','Adjustment')
and l.loc_type in ('Tag-FIFO','Tag-LIFO')
and l.work_zone not in ('BULK','G')
group by it.code
, it.from_loc_id
, it.to_loc_id
, l.work_zone
, it.sku_id
, s.description
, it.user_id
, rp."Sort"
, s.ugly
union all
--Ugly Ratio Packs
select it.code                          as "Code"
, it.sku_id                             as "Sku"
, s.description                         as "Desc"
, it.from_loc_id                        as "from Location"
, it.to_loc_id                          as "to Loc"
, l.work_zone                           as "Zone"
, sum(it.update_qty)                    as "Qty Moved"
, it.user_id                            as "User"
, rp."Sort"                             as "Sku Type"
, nvl(s.ugly,'N')                       as "Ugly"
, 'Should be relocated to W* location'  as "Error"
from inventory_transaction it
inner join rp on rp."Sku" = it.sku_id
inner join sku s on s.client_id = it.client_id and s.sku_id = it.sku_id
inner join location l on l.site_id = it.site_id and l.location_id = it.to_loc_id
where it.client_id = 'MOUNTAIN'
and it.to_loc_id not like 'W%'
and (it.from_loc_id like '%REP%' or it.from_loc_id like 'Z%')
and nvl(s.ugly,'N') = 'Y'
and it.dstamp between to_date('&&2','DD-MON-YYYY') and to_date('&&3','DD-MON-YYYY')+1
and it.code in ('Relocate','Putaway','Receipt','Stock Check','Adjustment')
and l.loc_type in ('Tag-FIFO','Tag-LIFO')
and l.work_zone not in ('BULK','G')
group by it.code
, it.from_loc_id
, it.to_loc_id
, l.work_zone
, it.sku_id
, s.description
, it.user_id
, rp."Sort"
, s.ugly
)
order by "User"
)
/
--spool off