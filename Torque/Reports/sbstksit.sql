SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 200
SET TRIMSPOOL ON
 

column A heading 'Due Date' format A11
column B heading 'Sku' format A10
column C heading 'Description' format A40
column D heading 'Qty' format 9999999
column F heading 'Due' format 9999999
column E heading '' format A14

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


--spool kkk.csv


--select 'Sku,skuStatus,Quantity,Next_delivery_stock,Next_delivery_date' from DUAL;

select
sku.sku_id||'|1|'||
nvl(QQQ,0)||'|'||
nvl(XQQ,0)||'|'||
to_char(DDD,'DD/MM/YYYY')
from sku,(select
i.sku_id BBB,
--sum(qty_on_hand) QQQ
sum(i.qty_on_hand-i.qty_allocated) QQQ
from inventory i,location ll
where i.client_id like 'SB'
and (i.location_id like 'SB%'
or i.location_id like 'VOUCH%')
and i.location_id not like 'SBIN%'
and i.condition_id='GOOD'
and i.lock_status<>'Locked'
and i.location_id=ll.location_id
and ll.loc_type='Tag-FIFO'
group by
i.sku_id),(select
XXX,
DECODE(DDD,trunc(sysdate)-7,trunc(sysdate),
           trunc(sysdate)-6,trunc(sysdate),
           trunc(sysdate)-5,trunc(sysdate),
           trunc(sysdate)-4,trunc(sysdate),
           trunc(sysdate)-3,trunc(sysdate),
           trunc(sysdate)-2,trunc(sysdate),
           trunc(sysdate)-1,trunc(sysdate),DDD) DDD,
sum(pl.qty_due-nvl(pl.qty_received,0)) XQQ
from (select
pl.sku_id XXX,
max(ph.due_dstamp) DDD
from pre_advice_header ph,pre_advice_line pl
where ph.client_id like 'SB'
and ph.status in ('Released','In Progress')
and ph.due_dstamp>sysdate-7
and ph.pre_advice_id like 'P%'
and pl.client_id like 'SB'
and ph.pre_advice_id= pl.pre_advice_id
and pl.qty_due-nvl(pl.qty_received,0)>0
group by
pl.sku_id),pre_advice_header ph,pre_advice_line pl
where ph.client_id like 'SB'
and ph.pre_advice_id like 'P%'
and ph.pre_advice_id= pl.pre_advice_id
and ph.due_dstamp=DDD
and pl.client_id like 'SB'
and pl.sku_id=XXX
group by
XXX,
DDD)
where sku.client_id like 'SB'
and sku.sku_id=BBB (+)
and sku.sku_id=XXX (+)
--and (QQQ>0 or XQQ>0)
order by
sku.sku_id
/

--spool off
