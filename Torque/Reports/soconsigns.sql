SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 400
SET TRIMSPOOL ON


column X noprint



set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON





break on G skip 1 dup

--compute sum of QQQ on CCC

--spool soconsigns.csv

select 'Store,Store Name,Order ID,Lines,Units,Consignment,Actual,Diff,1 2,3 4,G,5,6,8,9,10,STA,TOT' from DUAL;

select
X,
A,
B,
C,
D,
--E,
nvl(F,0)+nvl(H,0),
G,
nvl(H,0),
0-nvl(F,0)
from (select
DECODE(substr(oh.consignment,5,3),'MON',1,'TUE',2,'WED',3,'THU',4,'FRI',5,0) X,
oh.customer_id A,
ADNM B,
oh.order_id C,
count(*) D,
sum(ol.qty_ordered) E,
nvl(QQQ,0) F,
oh.consignment G,
sum(ol.qty_picked) H
from order_header oh, order_line ol,(select
ad.address_id ADID, 
ad.name ADNM
from address ad
where ad.client_id='SO'),(select
task_id OOO,
sum(qty_to_move) QQQ
from move_task mt
where client_id='SO'
group by
task_id)
where oh.client_id='SO'
and ol.client_id='SO'
and oh.order_id=ol.order_id
--and oh.status in ('Released','Allocated','In Progress')
and oh.consignment like '&&2%'
--and oh.work_group<>'WWW'
and oh.work_group=ADID (+) 
and oh.order_id=OOO (+)
group by
oh.customer_id,
ADNM,
oh.order_id,
QQQ,
oh.consignment)
order by
X,G,C
/

--spool off
