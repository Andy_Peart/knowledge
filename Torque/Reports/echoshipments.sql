SET FEEDBACK OFF                 
set pagesize 68
set linesize 160
set verify off
set colsep ' '

clear columns
clear breaks
clear computes

column YY heading 'Order Type' format a12
column D heading 'Shipment Date' format a16
column A heading 'Order ID' format a16
column B heading 'Ordered' format 999999
column C heading 'Shipped' format 999999
column M noprint

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'Total Units Shipped from &&2 to &&3 - for client ID ECHO as at ' report_date skip 2

break on TT skip 2 on D skip 2 on report

compute sum label 'Period Totals' of B C on report
compute sum label 'Type Totals' of B C on TT
compute sum label 'Day Totals' of B C on D

select
oh.order_type TT,
to_char(it.Dstamp,'DD/MM/YYYY') D,
to_char(it.Dstamp,'YYYY/MM/DD') M,
it.reference_id A,
sum(ol.qty_ordered) B,
sum(it.update_qty) C
from inventory_transaction it,order_line ol,order_header oh
where it.client_id='ECHO'
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
--and it.station_id not like 'Auto%'
and it.code like 'Shipment'
--and it.reference_id not like 'RSR%'
and it.reference_id=ol.order_id
and ol.client_id='ECHO'
and it.line_id=ol.line_id
and it.reference_id=oh.order_id
and oh.client_id='ECHO'
group by
oh.order_type,
to_char(it.Dstamp,'YYYY/MM/DD'),
to_char(it.Dstamp,'DD/MM/YYYY'),
it.reference_id
order by
TT,M,D,A
/
