SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON


--spool stdcover.csv

select 'Client Id &&2' from DUAL;
select 'Sku,Description,Qty On Hand,Picks (10 weeks),Pick Average,Cover in Weeks' from Dual;

select
S||','||
Description||','||
Q||','||
Q1||','||
nvl(Q2,0)||','||
CASE
WHEN nvl(Q2,0)=0 THEN 9999
ELSE  trunc(Q/Q2) END
from (select
S,
Description,
Q,
nvl(QQ,0) Q1,
QQ/10 Q2
from (select
inventory.sku_id S,
sku.Description,
sum(qty_on_hand) Q
from inventory inner join sku on sku.client_id = inventory.client_id and sku.sku_id = inventory.sku_id
where inventory.client_id='&&2'
group by
inventory.sku_id, sku.description),(select
sku_id SS,
sum(update_qty) QQ
from inventory_transaction
where client_id='&&2'
and code='Pick'
and elapsed_time>0
and dstamp>sysdate-70
group by
sku_id)
where S=SS(+))
order by
S
/


--spool off
