SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON



clear columns
clear breaks
clear computes

set pagesize 68
set linesize 240


column D heading 'Date' format a16
column B heading 'Qty' format 999999
column C heading 'Code' 
column M noprint


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

set heading on

ttitle  LEFT 'Units Receipted from '&&2' to '&&3' - for client ID &&4 as at ' report_date skip 2

break on D skip 1 on report

compute sum label 'Day Total' of B on D
compute sum label 'Total' of B on report


select
trunc(Dstamp) D,
code C,
sum(update_qty) B
from inventory_transaction
where client_id= 'HS' 
and code = 'Return' 
and Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by
trunc(Dstamp),
code
order by 1
/
