SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240


column A heading 'Location' format A8
column B heading 'Tag ID' format A20
column C heading 'SKU' format A18
column D heading 'Description' format a40
column E heading 'Colour' format a20
column F heading 'Original Qty' format 999999
column H heading 'Update Qty' format 999999
column G heading 'Found' format a12
column J heading 'Date' format a12
column K heading 'Time' format a12
column L heading 'User ID' format a20

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



select 'Location,Tag ID,SKU,Description,Colour,Original Qty,Update Qty,Found,Date,Time,User ID' from DUAL;

--break on BB dup skip 1 on report
break on report

--compute sum label 'Total' of DD on BB
compute sum label 'Totals' of H on report


select
i.from_loc_id A,
i.tag_id  B,
''''||i.sku_id||'''' C,
sku.description D,
sku.colour E,
nvl(i.original_qty,0) F,
nvl(i.update_qty,0) H,
'  ' G,
to_char(i.Dstamp,'DD/MM/YYYY') J,
to_char(i.Dstamp,'HH24:MI:SS') K,
i.user_id L
from inventory_transaction i,sku
where i.client_id='&&2'
and i.code='Stock Check'
and trunc(i.Dstamp)=to_date('&&3', 'DD-MON-YYYY')
and nvl(i.original_qty,0)=0
and sku.client_id='&&2'
and i.sku_id=sku.sku_id
order by
A
/
