SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep '|'


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9999
SET TRIMSPOOL ON

--set feedback on


update inventory_transaction
set uploaded='X'
where client_id = 'SB'
--and trunc(Dstamp)>trunc(sysdate-7)
and code = 'Shipment'
and uploaded = 'N'
and reference_id in (select
order_id from order_header
where client_id = 'SB'
and priority='1')
/



update inventory_transaction
set uploaded='Z'
where client_id='SB'
and code = 'Receipt'
and (user_def_note_1 like '%R%'
or user_def_note_1 like '%E%')
and uploaded = 'N'
and reference_id in (select
order_id from order_header
where client_id = 'SB'
and priority='1')
/


update order_header
set uploaded='X'
where client_id='SB'
--and trunc(order_date)>trunc(sysdate-7)
and status='Cancelled'
and uploaded = 'N'
and priority='1'
/


update order_line
set user_def_chk_1='X'
where client_id='SB'
and user_def_chk_1='N'
--and trunc(order_date)>trunc(sysdate-7)
and notes like 'Cancel%'
/

commit;


--set feedback off

--spool sbitls.csv



select
distinct
'ITL'||'|'
||'E'||'|'
||decode(i.update_qty,0,decode(BAK,'Y','Shipment','Cancelled'),'Shipment')||'|'
--||decode(i.update_qty,0,'Cancelled','Shipment')||'|'
||decode(sign(OOO), 1, '+', -1, '-', '+')||'|'
||nvl(OOO,0)||'|'
||decode(sign(QQQ), 1, '+', -1, '-', '+')||'|'
||QQQ||'|'
||to_char(i.dstamp, 'DD-MM-YYYY')||'|'
||i.client_id||'|'
||SSS||'|'
||i.from_loc_id||'|'
||i.to_loc_id||'|'
||i.final_loc_id||'||'
--||i.tag_id||'|'
--||RRR||to_char(i.dstamp, 'DDHH24')||'|'
||RRR||to_char(sysdate, 'DDHH24')||'|'
--||oh.user_def_type_7||'|'
||LLL||'|'
--||DECODE(nvl(ol.user_def_num_2,0),0,i.line_id,ol.user_def_num_2)||'|'
||i.condition_id||'||'
--||i.notes||'|'
||i.reason_id||'|'
||i.batch_id||'|'
||i.expiry_dstamp||'|'
||i.user_id||'|'
||i.shift||'|'
||i.station_id||'|'
||i.site_id||'||'
--||i.container_id||'|'
||i.pallet_id||'|'
||i.list_id||'|'
||i.owner_id||'|'
||i.origin_id||'|'
||i.work_group||'|'
||i.consignment||'|'
||i.manuf_dstamp||'|'
||i.lock_status||'|'
||i.qc_status||'|'
||i.session_type||'|'
||i.summary_record||'|'
||i.elapsed_time||'||'
--||i.supplier_id||'|'
||RRR||'|'||
--||oh.user_def_type_7||'|'||
DECODE(STAT,'Shipped',decode(i.update_qty,0,decode(BAK,'Y','To Follow','Cancelled'),'Shipped'),'Part Shipped')||'|||'
--DECODE(STAT,'Shipped',decode(i.update_qty,0,'Cancelled','Shipped'),'Part Shipped')||'|||'
--||i.user_def_type_3||'|'
--||i.user_def_type_4||'|'
||i.user_def_type_5||'|'
||i.user_def_type_6||'|'
||i.user_def_type_7||'|'
||i.user_def_type_8||'|||||'
--||i.user_def_chk_1||'|'
--||i.user_def_chk_2||'|'
--||i.user_def_chk_3||'|'
--||i.user_def_chk_4||'|'
||i.user_def_date_1||'|'
||i.user_def_date_2||'|'
||i.user_def_date_3||'|'
||i.user_def_date_4||'|'
||i.user_def_num_1||'|'
||i.user_def_num_2||'|'
||i.user_def_num_3||'|'
||i.user_def_num_4||'|||'
--||i.user_def_note_1||'|'
--||i.user_def_note_2||'|'
||i.from_site_id||'|'
||i.to_site_id||'|'
||''||'|'
||i.job_id||'|'
||i.job_unit||'|'
||i.manning||'|'
||i.spec_code||'|'
||i.config_id||'|'
||i.estimated_time||'|'
||i.task_category||'|'
||i.sampling_type||'|'
||to_char(i.complete_dstamp, 'DD-MM-YYYY')||'|'
||i.grn||'|'
||i.group_id||'|'
||'N'||'|'
||i.uploaded_vview||'|'
||i.uploaded_ab||'|'
||i.sap_idoc_type||'|'
||i.sap_tid||'|||'
--||i.ce_orig_rotation_id||'|'
--||i.ce_rotation_id||'|'
||i.ce_consignment_id||'|'
||i.ce_receipt_type||'|'
||i.ce_originator||'|'
||i.ce_originator_reference||'|'
||i.ce_coo||'|'
||i.ce_cwc||'|'
||i.ce_ucr||'|'
||i.ce_under_bond||'|'
||''||'|'
||i.uploaded_customs||'|'
||i.uploaded_labor||'|'
||i.asn_id||'|'
||i.customer_id||'||'
--||i.print_label_id||'|'
||i.lock_code||'|'
||i.ship_dock||'|'
||''||'|'
||'N'||'|'
||'N'
from (select
oh.status STAT,
i.reference_id RRR,
i.sku_id SSS,
i.line_id LLL,
ol.qty_ordered OOO,
nvl(ol.back_ordered,'N') BAK,
sum(i.update_qty) as QQQ
from inventory_transaction i,order_header oh,order_line ol
where i.client_id='SB'
and i.code = 'Shipment'
and i.uploaded = 'X'
--and trunc(i.Dstamp)=trunc(sysdate-1)
and i.reference_id=oh.order_id
and oh.client_id = 'SB'
and i.reference_id=ol.order_id
and ol.client_id = 'SB'
and i.sku_id=ol.sku_id
and i.line_id=ol.line_id
group by
oh.status,
i.reference_id,
i.sku_id,
i.line_id,
ol.qty_ordered,
nvl(ol.back_ordered,'N')
),inventory_transaction i
where i.client_id='SB'
and i.code = 'Shipment'
and i.uploaded = 'X'
--and trunc(i.Dstamp)=trunc(sysdate-1)
and RRR=i.reference_id
and SSS=i.sku_id
and LLL=i.line_id
union
select 'ITL'||'|'
||'E'||'|'
||'Cancelled'||'|'
||'+|0|+|0|'
||to_char(oh.last_update_date,'DD-MM-YYYY')||'|'
||oh.client_id||'||||||'
||oh.order_id||to_char(sysdate, 'DDHH24')||'|0||||||||||||||||||||||||'
||oh.order_id||'|'
--||oh.user_def_type_7||'|'
||oh.status||'|||||||||||||||||||||||||||||||||||N||||||||||||||||||||||||N|N'
||''
from order_header oh
where oh.client_id='SB'
and oh.uploaded = 'X'
--and trunc(oh.order_date)>trunc(sysdate-7)
and oh.status='Cancelled'
union
select 'ITL'||'|'
||'E'||'|'
||'Receipt'||'|'
||decode(sign(original_qty), 1, '+', -1, '-', '+')||'|'
||nvl(original_qty,0)||'|'
||decode(sign(update_qty), 1, '+', -1, '-', '+')||'|'
||update_qty||'|'
||to_char(dstamp, 'DD-MM-YYYY')||'|'
||client_id||'|'
||SS1||'|'
||from_loc_id||'|'
||to_loc_id||'|'
||final_loc_id||'|'
||tag_id||'|'
||reference_id||to_char(sysdate, 'DDHH24')||'|'
||LLL||'|'
||condition_id||'|'
||notes||'|'
||reason_id||'|'
||batch_id||'|'
||expiry_dstamp||'|'
||user_id||'|'
||shift||'|'
||station_id||'|'
||site_id||'|'
||container_id||'|'
||pallet_id||'|'
||list_id||'|'
||owner_id||'|'
||origin_id||'|'
||work_group||'|'
||consignment||'|'
||manuf_dstamp||'|'
||lock_status||'|'
||qc_status||'|'
||session_type||'|'
||summary_record||'|'
||elapsed_time||'|'
||supplier_id||'|'
||reference_id||'|'
||'Receipt'||'|'
--||user_def_type_3||'|'
||NUM1||'|'
||user_def_type_4||'|'
||user_def_type_5||'|'
||user_def_type_6||'|'
||user_def_type_7||'|'
||user_def_type_8||'|'
||user_def_chk_1||'|'
||user_def_chk_2||'|'
||user_def_chk_3||'|'
||user_def_chk_4||'|'
||user_def_date_1||'|'
||user_def_date_2||'|'
||user_def_date_3||'|'
||user_def_date_4||'|'
||user_def_num_1||'|'
||user_def_num_2||'|'
||user_def_num_3||'|'
||user_def_num_4||'|'
||user_def_note_1||'|'
||user_def_note_2||'|'
||from_site_id||'|'
||to_site_id||'|'
||''||'|'
||job_id||'|'
||job_unit||'|'
||manning||'|'
||spec_code||'|'
||config_id||'|'
||estimated_time||'|'
||task_category||'|'
||sampling_type||'|'
||to_char(complete_dstamp, 'DD-MM-YYYY')||'|'
||grn||'|'
||group_id||'|'
||'N'||'|'
||uploaded_vview||'|'
||uploaded_ab||'|'
||sap_idoc_type||'|'
||sap_tid||'|'
||ce_orig_rotation_id||'|'
||ce_rotation_id||'|'
||ce_consignment_id||'|'
||ce_receipt_type||'|'
||ce_originator||'|'
||ce_originator_reference||'|'
||ce_coo||'|'
||ce_cwc||'|'
||ce_ucr||'|'
||ce_under_bond||'|'
||''||'|'
||uploaded_customs||'|'
||uploaded_labor||'|'
||asn_id||'|'
||customer_id||'|'
||print_label_id||'|'
||lock_code||'|'
||ship_dock||'|'
||''||'|'
||'N'||'|'
||'N'
from (select
ol.order_id OOO,
ol.sku_id SS1,
ol.line_id LLL,
rank() over(partition by ol.order_id,ol.sku_id order by ol.order_id,ol.sku_id,ol.line_id) as rank1
from order_line ol
where client_id='SB'),(select
--and order_id='C7047660'),(select
original_qty,
update_qty,
dstamp,
client_id,
sku_id SS2,
from_loc_id,
to_loc_id,
final_loc_id,
tag_id,
reference_id RRR,
condition_id,
notes,
reason_id,
batch_id,
expiry_dstamp,
user_id,
shift,
station_id,
site_id,
container_id,
pallet_id,
list_id,
owner_id,
origin_id,
work_group,
consignment,
manuf_dstamp,
lock_status,
qc_status,
session_type,
summary_record,
elapsed_time,
supplier_id,
reference_id,
user_def_type_4,
user_def_type_5,
user_def_type_6,
user_def_type_7,
user_def_type_8,
user_def_chk_1,
user_def_chk_2,
user_def_chk_3,
user_def_chk_4,
user_def_date_1,
user_def_date_2,
user_def_date_3,
user_def_date_4,
user_def_num_1,
user_def_num_2,
user_def_num_3,
user_def_num_4,
user_def_note_1,
user_def_note_2,
from_site_id,
to_site_id,
job_id,
job_unit,
manning,
spec_code,
config_id,
estimated_time,
task_category,
sampling_type,
complete_dstamp,
grn,
group_id,
uploaded_vview,
uploaded_ab,
sap_idoc_type,
sap_tid,
ce_orig_rotation_id,
ce_rotation_id,
ce_consignment_id,
ce_receipt_type,
ce_originator,
ce_originator_reference,
ce_coo,
ce_cwc,
ce_ucr,
ce_under_bond,
uploaded_customs,
uploaded_labor,
asn_id,
customer_id,
print_label_id,
lock_code,
ship_dock,
rank() over(partition by reference_id,sku_id order by reference_id,sku_id,tag_id) as rank2
from inventory_transaction
where client_id='SB'
and code='Receipt'
and uploaded = 'Z'
--and reference_id='C7047660'
--and trunc(Dstamp)=trunc(sysdate-14)
),(select
user_def_num_1 NUM1,
pre_advice_id PID
from pre_advice_header
where client_id = 'SB')
where OOO=RRR
and SS1=SS2
and rank1=rank2
and 'U'||RRR=PID(+)
/


--spool off

--set feedback on


update inventory_transaction
set uploaded='Y'
where client_id = 'SB'
--and trunc(Dstamp)>trunc(sysdate-7)
and code = 'Shipment'
and uploaded = 'X'
/

update inventory_transaction
set uploaded='Y'
where client_id = 'SB'
--and trunc(Dstamp)>trunc(sysdate-7)
and code = 'Receipt'
and uploaded = 'Z'
/

update order_header
set uploaded='Y'
where client_id='SB'
--and trunc(order_date)>trunc(sysdate-7)
and status='Cancelled'
and uploaded = 'X'
/

update order_line
set user_def_chk_1='Y'
where client_id='SB'
and user_def_chk_1='X'
--and trunc(order_date)>trunc(sysdate-7)
and notes like 'Cancel%'
/



--rollback;
commit;

 
