SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Date,900,950,958,988,805,804' from dual
union all
select "Date" || ',' || "900" || ',' || "950" || ',' ||  "958" || ',' ||  "988" || ',' || "805" || ',' || "804" from
(
with p900 as (
    select count(distinct oh.order_id)  as "Count"
    , sum(it.update_qty)                as "Sum"
    , to_char(dstamp, 'DD-MON-YYYY')    as "Date"
    from inventory_transaction it
    inner join order_header oh on oh.order_id = it.reference_id and oh.client_id = it.client_id
    where it.client_id = 'MOUNTAIN'
    and it.code = 'Pick'
    and oh.priority = '900'
    and it.dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
    and it.update_qty <> 0
    group by to_char(dstamp, 'DD-MON-YYYY')
), p950 as (
    select count(distinct oh.order_id)  as "Count"
    , sum(it.update_qty)                as "Sum"
    , to_char(dstamp, 'DD-MON-YYYY')    as "Date"
    from inventory_transaction it
    inner join order_header oh on oh.order_id = it.reference_id and oh.client_id = it.client_id
    where it.client_id = 'MOUNTAIN'
    and it.code = 'Pick'
    and oh.priority = '950'
    and it.dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
    and it.update_qty <> 0
    group by to_char(dstamp, 'DD-MON-YYYY')
), p958 as (
    select count(distinct oh.order_id)  as "Count"
    , sum(it.update_qty)                as "Sum"
    , to_char(dstamp, 'DD-MON-YYYY')    as "Date"
    from inventory_transaction it
    inner join order_header oh on oh.order_id = it.reference_id and oh.client_id = it.client_id
    where it.client_id = 'MOUNTAIN'
    and it.code = 'Pick'
    and oh.priority = '958'
    and it.dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
    and it.update_qty <> 0
    group by to_char(dstamp, 'DD-MON-YYYY')
), p988 as (
    select count(distinct oh.order_id)  as "Count"
    , sum(it.update_qty)                as "Sum"
    , to_char(dstamp, 'DD-MON-YYYY')    as "Date"
    from inventory_transaction it
    inner join order_header oh on oh.order_id = it.reference_id and oh.client_id = it.client_id
    where it.client_id = 'MOUNTAIN'
    and it.code = 'Pick'
    and oh.priority = '988'
    and it.dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
    and it.update_qty <> 0
    group by to_char(dstamp, 'DD-MON-YYYY')
), p805 as(
    select count(distinct oh.order_id)  as "Count"
    , sum(it.update_qty)                as "Sum"
    , to_char(dstamp, 'DD-MON-YYYY')    as "Date"
    from inventory_transaction it
    inner join order_header oh on oh.order_id = it.reference_id and oh.client_id = it.client_id
    where it.client_id = 'MOUNTAIN'
    and it.code = 'Pick'
    and oh.priority = '805'
    and it.dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
    and it.update_qty <> 0
    group by to_char(dstamp, 'DD-MON-YYYY')
) , p804 as (
    select count(distinct oh.order_id)  as "Count"
    , sum(it.update_qty)                as "Sum"
    , to_char(dstamp, 'DD-MON-YYYY')    as "Date"
    from inventory_transaction it
    inner join order_header oh on oh.order_id = it.reference_id and oh.client_id = it.client_id
    where it.client_id = 'MOUNTAIN'
    and it.code = 'Pick'
    and oh.priority = '804'
    and it.dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
    and it.update_qty <> 0
    group by to_char(dstamp, 'DD-MON-YYYY')
)
, dates as (
    select "Date" from
    (
        select trunc(sysdate - rownum) + 1 "Date"
        from dual 
        connect by rownum < 365 /* Pulls back last years worth of dates */
    )
    where "Date" between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
)
select dates."Date"
, nvl(p900."Count",0)/* || case when p1200."Sum" is null then '-' else '(' || p1200."Sum" || ')' end */as "900"
, nvl(p950."Count",0)/* || case when p1200."Sum" is null then '-' else '(' || p1200."Sum" || ')' end */as "950"
, nvl(p958."Count",0)/* || case when p1200."Sum" is null then '-' else '(' || p1200."Sum" || ')' end */as "958"
, nvl(p988."Count",0)/* || case when p1200."Sum" is null then '-' else '(' || p1200."Sum" || ')' end */as "988"
, nvl(p805."Count",0)/* || case when p805."Sum" is null then '-' else '(' || p805."Sum" || ')' end */as "805"
, nvl(p804."Count",0)/* || case when p804."Sum" is null then '-' else '(' || p804."Sum" || ')' end */as "804"
/*Uncomment out above for units to be included on report*/
from dates
left join p900 on p900."Date" = dates."Date"
left join p950 on p950."Date" = dates."Date"
left join p958 on p958."Date" = dates."Date"
left join p988 on p988."Date" = dates."Date"
left join p805 on p805."Date" = dates."Date"
left join p804 on p804."Date" = dates."Date"
order by dates."Date"
)
/
--spool off