SET FEEDBACK OFF           
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 300
SET TRIMSPOOL ON



column A heading 'Order' format A16
column C heading 'SKU' format A16
column C2 heading 'Line Id' format 999999
column D heading 'Qty on Hand' format 999999
column D2 heading 'Qty Allocated' format 999999
column D3 heading 'Web Reserve' format 999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


break on A dup skip 1

--spool mwretailblots.csv

select 'Retail Orders with Order Qty Zero by Order Creation Date from &&2 to &&3 - for client ID MOUNTAIN ' from DUAL;

select 'Order Id,SKU,Line,Qty on Hand,Qty Allocated,Web Reserve' from DUAL;


select
ol.order_id A,
''''||ol.sku_id||'''' C,
ol.line_id C2,
nvl(Q1,0) D,
nvl(Q2,0) D2,
sku.user_def_num_4 D3
from order_header oh,order_line ol,(select
iv.sku_id SKU,
sum(iv.qty_on_hand) Q1,
sum(iv.qty_allocated) Q2
from inventory iv
where iv.client_id='MOUNTAIN'
group by iv.sku_id),sku
where oh.client_id='MOUNTAIN'
and oh.order_id  like 'MOR%'
and oh.creation_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and ol.client_id=oh.client_id
and ol.order_id=oh.order_id
and ol.qty_ordered=0
and ol.sku_id=SKU (+)
and ol.client_id=sku.client_id
and ol.sku_id=sku.sku_id
order by A,C2
/

--spool off

