SET FEEDBACK OFF                 
set verify off
set wrap off
set colsep ','

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 132
set trimspool on


--spool mkshipments.csv

select 'Date,SKU,From Location,Reference,Update Qty,Pack Config,Packs,Singles,UDT8' from DUAL
union all
select * from (select
W||','||
X||','||
DECODE(CC,'MKWEB',0,DECODE(X,'M1',0,Y))||','||
DECODE(CC,'MKWEB',EE,DECODE(X,'M1',Y,Z))||','||
H
from (select
A||','||
B||','||
C||','||
D||','||
E W,
E EE,
C CC,
F X,
trunc(E/G) Y,
E-(trunc(E/G)*G) Z,
H
from (select
trunc(it.Dstamp) A,
it.sku_id B,
it.from_loc_id C,
it.reference_id D,
sum(it.update_qty) E,
sc.config_id F,
sc.ratio_1_to_2 G,
it.user_def_type_8 H
from inventory_transaction it,sku_config sc
where it.client_id='MK'
and it.code='Shipment'
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.config_id=sc.config_id
and sc.client_id='MK'
group by
trunc(it.Dstamp),
it.sku_id,
it.from_loc_id,
it.reference_id,
sc.config_id,
sc.ratio_1_to_2,
it.user_def_type_8
union
select
trunc(it.Dstamp) A,
it.sku_id B,
it.from_loc_id C,
it.reference_id D,
sum(it.update_qty) E,
sc.config_id F,
sc.ratio_1_to_2 G,
it.user_def_type_8 H
from inventory_transaction_archive it,sku_config sc
where it.client_id='MK'
and it.code='Shipment'
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.config_id=sc.config_id
and sc.client_id='MK'
group by
trunc(it.Dstamp),
it.sku_id,
it.from_loc_id,
it.reference_id,
sc.config_id,
sc.ratio_1_to_2,
it.user_def_type_8)
order by
A,D,B))
union all
select * from (select
A||',,'||
C||',Total,,,'||
GG||','||
HH
from (select
A,
C,
sum(G) GG,
sum(H) HH
from (select
A,
B,
C,
D,
E,
F,
--DECODE(F,'M1',0,Y) G,
--DECODE(F,'M1',Y,Z) H
DECODE(C,'MKWEB',0,DECODE(F,'M1',0,Y)) G,
DECODE(C,'MKWEB',E,DECODE(F,'M1',Y,Z)) H
from (select
A,
B,
C,
D,
E,
F,
trunc(E/G) Y,
E-(trunc(E/G)*G) Z
from (select
trunc(it.Dstamp) A,
it.sku_id B,
it.from_loc_id C,
it.reference_id D,
sum(it.update_qty) E,
sc.config_id F,
sc.ratio_1_to_2 G
from inventory_transaction it,sku_config sc
where it.client_id='MK'
and it.code='Shipment'
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.config_id=sc.config_id
and sc.client_id='MK'
group by
trunc(it.Dstamp),
it.sku_id,
it.from_loc_id,
it.reference_id,
sc.config_id,
sc.ratio_1_to_2
union
select
trunc(it.Dstamp) A,
it.sku_id B,
it.from_loc_id C,
it.reference_id D,
sum(it.update_qty) E,
sc.config_id F,
sc.ratio_1_to_2 G
from inventory_transaction_archive it,sku_config sc
where it.client_id='MK'
and it.code='Shipment'
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.config_id=sc.config_id
and sc.client_id='MK'
group by
trunc(it.Dstamp),
it.sku_id,
it.from_loc_id,
it.reference_id,
sc.config_id,
sc.ratio_1_to_2)
order by
A,D,B))
group by A,C)
order by 1)
/

--spool off
