SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240



--select 'Criminal Investigation Bureau Report - for client ID TR as at ' 
--        ||  to_char(sysdate,'dd/mm/yy  hh24:mi:ss') from dual;

select 'Client,SKU,Description,Colour,Size,Tag,Orig Qty,Update Qty,From Loc,To Loc,Date,User ID,Receipt Date,User ID,Rec Qty,Receipt'
from dual;


select 
it.client_id ||','''||
it.sku_id ||''','||
sku.description  ||','||
sku.colour ||','||
sku.sku_size ||','||
it.tag_id ||','||
it.original_qty ||','||
it.update_qty ||','||
it.from_loc_id ||','||
it.to_loc_id ||','||
to_char(it.Dstamp, 'DD/MM/YYYY HH:MI') ||','||
it.user_id ||','||
to_char(DT, 'DD/MM/YYYY HH:MI') ||','||
CULP ||','||
UQ ||','||
RI
from inventory_transaction it,
(select 
user_id CULP,
to_loc_id TL,
tag_id TG,
dstamp DT,
update_qty UQ,
reference_id RI
from inventory_transaction),sku
where it.client_id='&&2'
and it.to_loc_id='SUSPENSE'
and it.from_loc_id=TL (+)
and it.tag_id=TG (+)
and it.dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and it.sku_id=sku.sku_id
and sku.client_id='&&2'
order by it.sku_id
/
