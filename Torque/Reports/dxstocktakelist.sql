SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9999
SET TRIMSPOOL ON

--spool stocktakelist.csv


select ',Sku,Description,Qty,Location' from DUAL;


select
rownum||','||
B||','||
C||','||
D||','||
A
from (select
i.sku_id B,
Sku.description C,
i.location_id A,
sum(i.qty_on_hand) D
from inventory i,sku
where i.client_id like 'DX'
and i.sku_id=sku.sku_id
and sku.client_id='DX'
group by
i.sku_id,
Sku.description,
i.location_id
order by B)
/

--spool off


