SET FEEDBACK OFF
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON

set pagesize 68
set newpage 0
set linesize 140


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE-1, 'DD-Mon-YYYY') curdate from DUAL;
set TERMOUT ON

ttitle 'Pringle - Items Putaway on ' report_date skip 2

column A heading 'Product' format a13
column B heading 'Description' format a40
column C heading 'PA Reference' format a14
column D heading 'Colour' format a14
column E heading 'Qty' format 99999
column F heading 'Supplier' format a10
column G heading 'Date' format a9
column H heading 'Time' format a6
column M format a4 noprint


break on B skip 1 on report

compute sum label '' of E on B
compute sum label ' ' of E on report



select
substr(it.sku_id,1,11) A,
sku.description B,
PO C,
sku.colour D,
sum(it.update_qty) E,
it.supplier_id F,
to_char(it.Dstamp,'DD/MM/YY') G,
max(to_char(it.Dstamp,'HH24:MI')) H
from inventory_transaction it,sku,
(select tag_id TAG,
reference_id PO
from inventory_transaction
where client_id='PRI'
and code='Receipt')
where it.client_id='PRI'
and it.code='Putaway'
and it.sku_id=sku.sku_id
and sku.client_id='PRI'
and trunc(it.Dstamp)=trunc(sysdate-1)
and it.tag_id=TAG
group by
substr(it.sku_id,1,11),
sku.description,
PO,
sku.colour,
it.supplier_id,
to_char(it.Dstamp,'DD/MM/YY')
order by
A,B
/
