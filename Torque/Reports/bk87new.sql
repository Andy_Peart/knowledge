SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 1
SET LINESIZE 60
SET NUMFORMAT 99.99

column A heading 'Date' format A12
column B heading 'Ordered' format 99999999
column C heading 'Shipped' format 99999999
column D heading 'Variance' format 99999999
column E heading '% Shortage' format A10



select 'Date,Ordered,Shipped,Variance,% Shortage,Client &&2' from DUAL;
 
set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DAY, DD MONTH, YYYY') curdate from DUAL;
set TERMOUT ON


--break on report
--compute sum label 'Totals' of B C D E on report

select
to_char(trunc(oh.shipped_date),'DD/MM/YYYY') A,
nvl(sum(ol.qty_ordered),0) B,
nvl(sum(ol.qty_shipped),0) C,
nvl(sum(ol.qty_ordered),0)-nvl(sum(ol.qty_shipped),0) D,
to_char((nvl(sum(ol.qty_ordered),0)-nvl(sum(ol.qty_shipped),0))*100/nvl(sum(ol.qty_ordered),0),'99.99')||'%' E
from order_header oh,order_line ol
where oh.client_id='&&2'
and oh.shipped_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and oh.status in ('Shipped')
and oh.client_id=ol.client_id
and oh.order_id=ol.order_id
group by trunc(oh.shipped_date)
order by trunc(oh.shipped_date)
/

select
'Totals' A,
nvl(sum(ol.qty_ordered),0) B,
nvl(sum(ol.qty_shipped),0) C,
nvl(sum(ol.qty_ordered),0)-nvl(sum(ol.qty_shipped),0) D,
to_char((nvl(sum(ol.qty_ordered),0)-nvl(sum(ol.qty_shipped),0))*100/nvl(sum(ol.qty_ordered),0),'99.99')||'%' E
from order_header oh,order_line ol
where oh.client_id='&&2'
and oh.shipped_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and oh.status in ('Shipped')
and oh.client_id=ol.client_id
and oh.order_id=ol.order_id
/
