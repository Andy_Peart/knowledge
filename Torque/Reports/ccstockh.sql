SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 9999
set trimspool on
set newpage 0
set verify off
set colsep ','



break on report
compute sum label 'Totals' of E1 E2 E3 E4 E5 F E on report


--spool ccstockh.csv



select 'PO,SKU,Style,Colour,Size,EAN,Intake (in progress), Intake (complete), Ship-Dock, Allocated, Suspense/Locked, Free, Total' from DUAL;


select
it.receipt_id A,
sku.sku_id AA,
sku.user_def_type_1 B,
sku.colour C,
sku.sku_size C1,
sku.ean C2,
nvl(C2,0)-nvl(C3,0) E1,
nvl(C3,0) E2,
nvl(C4,0) E4,
sum(it.qty_allocated) E3,
nvl(C5,0) E5,
sum(it.qty_on_hand)-nvl(C2,0)-nvl(C4,0)-nvl(C5,0)-sum(it.qty_allocated) F,
sum(it.qty_on_hand) E
from inventory it,sku,(select
jc.receipt_id A2,
jc.sku_id B2,
nvl(sum(jc.qty_on_hand),0) C2
from inventory jc,location lc
where jc.client_id like '&&2'
and jc.location_id=lc.location_id
and jc.site_id=lc.site_id
and lc.loc_type like 'Receive Dock%'
group by
jc.receipt_id,
jc.sku_id),(select
jc.receipt_id A3,
jc.sku_id B3,
nvl(sum(jc.qty_on_hand),0) C3
from inventory jc,location lc,(select
status SS,
pre_advice_id PP
from pre_advice_header
where client_id='&&2'
and status='Complete')
where jc.client_id like '&&2'
and jc.location_id=lc.location_id
and jc.site_id=lc.site_id
and lc.loc_type like 'Receive Dock%'
and jc.receipt_id=PP
group by
jc.receipt_id,
jc.sku_id),(select
jc.receipt_id A4,
jc.sku_id B4,
nvl(sum(jc.qty_on_hand),0) C4
from inventory jc,location lc
where jc.client_id like '&&2'
and jc.location_id=lc.location_id
and jc.site_id=lc.site_id
and lc.loc_type like 'ShipDock%'
group by
jc.receipt_id,
jc.sku_id),(select
jc.receipt_id A5,
jc.sku_id B5,
nvl(sum(jc.qty_on_hand),0) C5
from inventory jc,location lc
where jc.client_id like '&&2'
and jc.location_id=lc.location_id
and jc.site_id=lc.site_id
and (jc.location_id='SUSPENSE'
or jc.lock_status='Locked'
or lc.lock_status='OutLocked')
group by
jc.receipt_id,
jc.sku_id)
where it.client_id='&&2'
and it.sku_id=sku.sku_id
and sku.client_id='&&2'
and it.receipt_id=A2 (+)
and it.receipt_id=A3 (+)
and it.receipt_id=A4 (+)
and it.receipt_id=A5 (+)
and it.sku_id=B2 (+)
and it.sku_id=B3 (+)
and it.sku_id=B4 (+)
and it.sku_id=B5 (+)
group by
it.receipt_id,
sku.sku_id,
sku.user_def_type_1,
sku.colour,
sku.sku_size,
sku.ean,
nvl(C2,0),
nvl(C3,0),
nvl(C4,0),
nvl(C5,0)
order by A,B,C
/


--spool off

