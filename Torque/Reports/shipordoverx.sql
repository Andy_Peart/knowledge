/* ************************************************************************** */
/*                                                                            */
/*     FILE NAME  :     shipordoverx.sql                         		      */
/*                                                                            */
/*     DESCRIPTION:                                                           */
/*     CSV version Used to split shipped units into over x or under x          */
/*                                                                            */
/*   DATE     BY   DESCRIPTION						                          */
/*   ======== ==== ===========					                              */
/*    21/08/2013 YB   Shipped orders over X units                            */
/* *************************************************************************** */
SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','
SET WRAP OFF


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500

/* Column Headings and Formats */
COLUMN "Client" heading "Client ID" FORMAT A9
COLUMN "Shipped Date" heading "Shipped Date" FORMAT a12
COLUMN "Order No" HEADING "Order No" FORMAT a14
COLUMN "Consignment" HEADING "Consignment" FORMAT a20
COLUMN "Units Shipped" heading "Units Shipped" FORMAT 9999
COLUMN "Over" heading "Over" FORMAT a10
COLUMN "Under" heading "Under" FORMAT a10

break on report

compute sum label 'Total' of "Units Shipped" on report

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

--SPOOL SHIPPED_ORDERS_OVER_.CSV

/* Top Title */
select 'Client, Shipped Date, Order No, Consignment, Units Shipped, Over, Under' from DUAL;

select sm.client_id as "Client", to_char(sm.shipped_dstamp, 'DD-MON-YYYY') as "Shipped Date", sm.order_id as "Order No",sm.consignment as "Consignment", sum(SM.QTY_SHIPPED) as "Units Shipped",
CASE 
         when sum(SM.QTY_SHIPPED)>=51 then 'Yes'
         when sum(SM.QTY_SHIPPED)<51 then 'No'
         Else 'not sure'
END as "Over",
CASE 
         when sum(SM.QTY_SHIPPED)<51 then 'Yes'
         when sum(SM.QTY_SHIPPED)>=51 then 'No'
         Else 'not sure'
END as "Under"
from SHIPPING_MANIFEST sm 
where sm.client_id = 'VF'
and SM.SHIPPED_DSTAMP between trunc(sysdate)-5 and trunc(sysdate)+1
group by sm.consignment, sm.order_id, to_char(sm.shipped_dstamp, 'DD-MON-YYYY'), sm.client_id
/

--SPOOL OFF
