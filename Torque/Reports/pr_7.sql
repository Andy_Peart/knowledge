SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 60


column A heading 'Docket (SKU)' format A12
column B heading 'Client' format A6
column C heading 'Date Received' format A16
column D heading 'Cartons' format 9999999
column E heading 'Units' format 9999999


select 'Docket (SKU),Client,Date Received,Cartons,Units,,' from DUAL;

break on report

compute sum label 'Totals' of D E on report

select
i.sku_id A,
i.user_def_note_1 B,
to_char(i.receipt_Dstamp,'DD-Mon-YYYY') C,
sum(i.qty_on_hand) D,
to_number(sku.user_def_type_1) E
from inventory i,sku
where i.client_id='PR'
and i.sku_id=sku.sku_id
and sku.client_id='PR'
group by
i.sku_id,
i.user_def_note_1,
to_char(receipt_Dstamp,'DD-Mon-YYYY'),
to_number(sku.user_def_type_1)
order by A
/
