SET FEEDBACK OFF             
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 300
SET TRIMSPOOL ON

--spool mwppp.csv

--select to_char(SYSDATE, 'DD/MM/YY  HH:MI:SS') curdate from DUAL;

select 'Sku,LPG,Last Receipt Date,Current Stock,Web Picked,Retail Picked,Total Picked,Percentage' from dual;

select
replace(XXX,' ','')
from (select
''''||A||''','||
LPG||','||
LRD||','||
C||','||
Q1||','||
Q2||','||
Q3||','||
to_char((Q3*100)/C,'9990.00') XXX
from (select
inv.sku_id A,
sku.user_def_type_8 LPG,
max(trunc(inv.receipt_dstamp)) LRD,
sum(inv.qty_on_hand) C,
nvl(Q1,0) Q1,
nvl(Q2,0) Q2,
nvl(Q1,0)+nvl(Q2,0) Q3
from inventory inv,location L,sku,(select
sku_id S1,
sum(DECODE(work_group,'WWW',update_qty,0)) Q1,
sum(DECODE(work_group,'WWW',0,update_qty)) Q2
from inventory_transaction
where client_id='MOUNTAIN'
and code='Pick'
and dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and elapsed_time>0
group by sku_id
order by sku_id)
where inv.client_id = 'MOUNTAIN'
and inv.location_id=L.location_id
and inv.site_id=L.site_id
and L.loc_type='Tag-FIFO'
and inv.sku_id=sku.sku_id
and sku.client_id = 'MOUNTAIN'
and sku.product_group<>'STATIONERY'
and inv.sku_id=S1 (+)
group by
inv.sku_id,
sku.user_def_type_8,
nvl(Q1,0),
nvl(Q2,0)
order by A))
/

--select to_char(SYSDATE, 'DD/MM/YY  HH:MI:SS') curdate from DUAL;

--spool off
