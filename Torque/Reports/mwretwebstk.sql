set pagesize 0
set linesize 2000
set trimspool on
set verify off
set feedback off
clear columns
clear breaks
clear computes
set colsep ','

column A heading 'SKU' format a40
column B heading 'Total Qty' format 99999

break on A

--spool mw2stk.csv

select'SKU,Qty in RETAIL,Qty in WEB,Reserve Stk,RETAIL Locations' from DUAL;


select
''''||inv.sku_id||','||
sum(qty_on_hand)||','||
nvl(W3,0)||','||
to_number(sku.user_def_num_4)||','||
inv.location_id
from inventory inv,sku,(select
sku_id W1,
sum(qty_on_hand) W3
from inventory
where client_id = 'MOUNTAIN'
and zone_1='WEB'
group by sku_id)
where inv.client_id = 'MOUNTAIN'
and inv.zone_1='RETAIL'
and inv.sku_id=sku.sku_id
and sku.client_id = 'MOUNTAIN'
and to_number(sku.user_def_num_4)>0
and inv.sku_id=W1 (+)
and W3<to_number(sku.user_def_num_4)
group by
inv.sku_id,
inv.location_id,
nvl(W3,0),
sku.user_def_num_4
order by 1
/

--spool off
