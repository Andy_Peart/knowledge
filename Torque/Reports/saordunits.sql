SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Creation Date,Order Type,Order #,Qty Ordered,More than &&4 Units?' from dual
union all
select "Creation" ||','|| "Type" ||','|| "Order" ||','|| "Qty Ord" ||','|| "More than X Units"
from
(
select trunc(oh.creation_date) as "Creation"
, oh.order_type as "Type"
, oh.order_id   as "Order"
, sum(ol.qty_ordered)   as "Qty Ord"
, case when sum(ol.qty_ordered) > '&&4' then 'Y' else 'N' end as "More than X Units"
from order_header oh
inner join order_line ol on ol.order_id = oh.order_id and ol.client_id = oh.client_id
where oh.client_id = 'SA'
and oh.order_type = 'WEB'
and oh.creation_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by trunc(oh.creation_date)
, oh.order_type
, oh.order_id
order by 4 desc
)
/
--spool off