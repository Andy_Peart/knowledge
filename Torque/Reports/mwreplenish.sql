SET FEEDBACK OFF             
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 300
SET TRIMSPOOL ON

--spool mwreplenish.csv

--select to_char(SYSDATE, 'DD/MM/YY  HH:MI:SS') curdate from DUAL;

select 'Date,Pre Advice,Description,LPG,Qty Due,Retail (Location Zone),Web (Location Zone),Bulk (Location Zone)' from dual;

select
PD||','||
PI||','||
DD||','||
LPG||','|| 
QQ||','||
LocationRetail||','||
LocationWeb||','||
LocationBulk
from (select 
trunc(ph.due_dstamp) PD,
ph.pre_advice_id PI,
--pl.sku_id SK,
sku.description DD,
sku.user_def_type_8 LPG,
sum(pl.qty_due) QQ, 
sum(case when upper(inv.zone_1) in ('RETAIL') then inv.qty_on_hand end) LocationRetail,
sum(case when upper(inv.zone_1) in ('WEB') then inv.qty_on_hand end) LocationWeb,
sum(case when upper(inv.zone_1) in ('RACK1') then inv.qty_on_hand end) LocationBulk  
from pre_advice_header ph,pre_advice_line pl,sku, inventory inv 
where ph.client_id = 'MOUNTAIN'
and ph.due_dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1 
and pl.client_id=ph.client_id
and pl.pre_advice_id=ph.pre_advice_id
and sku.client_id = 'MOUNTAIN'
and sku.sku_id=pl.sku_id and pl.client_id = sku.client_id
and inv.sku_id = sku.sku_id and inv.client_id = pl.client_id 
group by 
trunc(ph.due_dstamp),
ph.pre_advice_id,
--pl.sku_id,
sku.description,
sku.user_def_type_8 )
order by PD,PI,DD
/


--spool off
