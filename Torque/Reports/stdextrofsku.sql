SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9000
SET TRIMSPOOL ON
SET HEADING ON

select 'Client,SKU,Description,EAN,Product Group,Each value' from dual
union all
select client_id || ',' || sku_id || ',' || description || ',' || ean || ',' || product_group || ',' ||  val
from (
select client_id, sku_id, description, ean, product_group, to_char(each_value) val
from sku
where client_id = '&&2'
)
/
