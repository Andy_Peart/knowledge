SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON



set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

WITH T2 AS
  (SELECT it.Customer_ID,
    TO_CHAR(it.Dstamp, 'DD-MM-YYYY') date_shipped,
    it.Sku_id,
    it.reference_id sales_order,
    SUM(it.update_qty) qty_shipped,
    it.tag_id
  FROM inventory_transaction it
  INNER JOIN location loc
  ON it.from_loc_id  = loc.location_id
  AND it.site_id     = loc.site_id
  WHERE it.client_id = 'BL'
  AND it.site_id     = 'WO6'
  AND loc.zone_1    IN ( 'ASOS', 'LIP', 'DEB', 'URB', 'ZAL', 'BL')
  AND it.code        = 'Shipment'
  AND it.update_qty  > 0
  AND it.Dstamp BETWEEN to_date('&&2', 'DD-MON-YYYY') AND to_date('&&3', 'DD-MON-YYYY') + 1
  GROUP BY it.Customer_ID,
    TO_CHAR(it.Dstamp, 'DD-MM-YYYY'),
    it.Sku_id,
    it.reference_id,
    it.tag_id
  )
SELECT 'Date Picked, Customer ID, Location Zone, Sales Order, SKU, Original Receipt ID, Units Picked, Date Shipped, Units Shipped'
FROM dual
UNION ALL
SELECT date_picked
  || ','
  || customer_id
  || ','
  || zone_1
  || ','
  || sales_order
  || ','
  || sku_id
  || ','
  || SUM(qty_picked)
  || ','
  || date_shipped
  || ','
  || SUM(qty_shipped)
FROM
  (SELECT TabA.*,
    T2.date_shipped,
    T2.qty_shipped
  FROM
    (SELECT TO_CHAR(it.Dstamp, 'DD-MM-YYYY') date_picked,
      it.Customer_ID,
      loc.zone_1,
      it.reference_id sales_order,
      it.Sku_id,
      it.tag_id,
      SUM(it.update_qty) qty_picked
    FROM inventory_transaction it
    INNER JOIN location loc
    ON it.from_loc_id  = loc.location_id
    AND it.site_id     = loc.site_id
    WHERE it.client_id = 'BL'
    AND it.site_id     = 'WO6'
    AND loc.zone_1    IN ( 'ASOS', 'LIP', 'DEB', 'URB', 'ZAL', 'BL')
    AND it.code        = 'Pick'
    AND it.update_qty  > 0
    AND it.to_loc_id   = 'CONTAINER'
    GROUP BY it.Customer_ID,
      TO_CHAR(it.Dstamp, 'DD-MM-YYYY'),
      loc.zone_1,
      it.Sku_id,
      it.reference_id,
      it.tag_id
    ) TabA
  INNER JOIN T2
  ON TabA.customer_id  = T2. customer_id
  AND TabA.tag_id      = T2.tag_id
  AND TabA.sales_order = T2.sales_order
  AND TabA.sku_id      = T2.sku_id
  ORDER BY 1,
    2,
    3,
    4
  )
GROUP BY date_picked,
  customer_id,
  zone_1 ,
  sales_order ,
  sku_id ,
  date_shipped


/