SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON




set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

--break on A

--compute sum of CT on A

--spool yodelpicknoship.csv

select 'Picks from &&2 to &&3 not yet shipped' from DUAL;

select 'Container,Work Group,Post Code,Date Picked' from DUAL;


select
A,
B,
C,
K
from (select
distinct
trunc(it.Dstamp) K,
it.container_id A,
it.work_group B,
postcode C,
nvl(CC,'XX') D
from inventory_transaction it,address ad,(select
distinct
container_id CC,
work_group WW
from shipping_manifest
where client_id='MOUNTAIN'
and work_group<>'WWW'
and shipped_dstamp>to_date('&&2', 'DD-MON-YYYY'))
where it.client_id='MOUNTAIN'
and it.work_group<>'WWW'
and it.code='Pick'
and it.container_id like 'JJD%'
--and it.to_loc_id='CONTAINER'
and it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.container_id=CC (+)
and it.work_group=ad.address_id (+))
where D='XX'
order by
A
/


--spool off
