SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 66
SET NEWPAGE 0
SET LINESIZE 120



ttitle 'DAXBOURNE skus starting "&&2" found in multiple locations' skip 2

column AAA heading 'SKU' format A16
column BBB heading 'Location' format 99999999
column CCC heading 'Stock Qty' format 99999999


break on AAA skip 1

select
SKU_ID AAA,
location_id BBB,
sum(qty_on_hand) CCC
from inventory i, (select sku_id A, count(sku_id) B
from inventory
where client_id = 'DX'
and condition_id='GOOD'
group by sku_id) Z
where B > 1
and i.sku_id = A
and i.sku_id like '&&2%'
and i.condition_id='GOOD'
group by
SKU_ID,
location_id
order by 
AAA,BBB
/
