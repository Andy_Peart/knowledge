--	Client_ID (parameter)
--	Date (parameter)
--	Orders received (use date_created  on ODH)
--	Units shipped (use shipped on ODL)
--Ill probably add to this in time but this will do for now. Can you let me know when done
--as I need to run off some numbers for client TS. 
--You can call the report STD Key Numbers by Client / Date

SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON


column B heading 'Order Type' format a16
column C heading 'Shipments' format 999999
column C2 heading 'Orders' format 999999
column D heading 'Units' format 999999
column F heading 'Date' format a12

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON





--break on REPORT on F skip 1

--compute sum label 'Report Totals' of C C2 D on REPORT
--compute sum label 'Daily Totals' of C C2 D on F 


--spool stdkeys.csv
 
select 'Orders by Date from &&3 to &&4 - for client '||nvl(cl.name,cl.client_id)||' as at '||trunc(sysdate) 
from client cl
where client_id='&&2';



select 'Date,Orders Received,Units Shipped' from DUAL;

select
trunc(oh.creation_date),
count(distinct oh.order_id) C,
sum(ol.qty_shipped) D
from order_header oh,order_line ol
where oh.creation_date between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and oh.client_id='&&2'
and ol.client_id='&&2'
and oh.order_id=ol.order_id
group by
trunc(oh.creation_date)
order by
trunc(oh.creation_date)
/

--spool off
