SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 68
SET LINESIZE 120
SET TRIMSPOOL ON
SET HEADING ON



ttitle 'Energy Gels Pick Sheet' skip 2

column AA noprint
column A heading 'SKU' format a16
column B heading 'Description' format a40
column C heading 'Location' format a12
column D heading 'Pick Qty' format 99999
column E heading 'Packs' format 999.99

ttitle 'Mountain SKU Pick Totals for Order &&2' skip 2


select
--ol.order_id,
ol.sku_id A,
--ol.qty_ordered,
--mt.description,
ol.user_def_type_2 B,
mt.from_loc_id C,
--ol.user_def_type_1,
ol.qty_ordered*to_number(substr(ol.user_def_type_1,1,2)) D,
(ol.qty_ordered*to_number(substr(ol.user_def_type_1,1,2)))/24 E
from order_line ol,move_task mt
where ol.client_id='EN'
and ol.order_id like '&&2%'
and ol.client_id=mt.client_id
and ol.order_id=mt.task_id
and ol.sku_id=mt.sku_id
/


SET PAGESIZE 0
SET HEADING OFF

select '' from DUAL;
select 'End of Run' from DUAL;
