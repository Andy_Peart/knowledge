SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON



clear columns
clear breaks
clear computes

set pagesize 68
set linesize 240


column D heading 'Date' format a16
column C heading 'Code' 
column A heading 'Reference' format A20
column B heading 'Receipts' format 999999
column B3 heading 'Returns' format 999999
column M noprint


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

set heading on

ttitle  LEFT 'Receipts/Returns from '&&2' to '&&3' - for client ID WOL as at ' report_date skip 2

break on D skip 1 on report

compute sum label 'Day Total' of B B3 on D
compute sum label 'Total' of B B3 on report


select
D,
C,
A,
B-B3 B,
B3
from (select
trunc(Dstamp) D,
code C,
reference_id A,
sum(update_qty) B,
nvl(B2,0) B3
from inventory_transaction,(select
trunc(Dstamp) D2,
code C2,
reference_id A2,
sum(update_qty) B2
from inventory_transaction
where client_id='WOL'
and reference_id like 'R%'
and (code like 'Receipt%' or (code='Adjustment' and reason_id in ('RECRV')))
and Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by
trunc(Dstamp),
reference_id,
code)
where client_id='WOL'
and (code like 'Receipt%' or (code='Adjustment' and reason_id in ('RECRV')))
and Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and code=C2 (+)
and reference_id=A2 (+)
group by
trunc(Dstamp),
reference_id,
code,
nvl(B2,0))
order by
D,C,A
/
