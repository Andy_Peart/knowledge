SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET TRIMSPOOL ON

select 'GRN in Progress: (should be empty)' A1 from dual
union all
select 'PreAdvice Id,Status,Creation date' from dual
union all
select pre_advice_id || ',' || status || ',' || date_
from (
    select pre_advice_id, status, to_char(creation_date, 'DD-MM-YYYY') date_
    from pre_advice_header
    where client_id = 'TR'
    and pre_advice_id not like 'T%'
    and nvl(status,'X') in ('In Progress')
)
union all
select ' ' from dual
union all
select 'Orders left: (should be empty)' from dual
union all
select 'Order id,Status,Cretaion date' from dual
union all
select order_id || ',' || status || ',' || date_
from (
    select order_id, status, to_char(creation_date, 'DD-MM-YYYY') date_
    from order_header
    where client_id = 'TR'
    and status not in ('Shipped','Cancelled')
    order by 1
)
union all
select ' ' from dual
union all
select 'Locations checks: (should be empty)' from dual
union all
select 'Location,Units' from dual
union all
select location_id || ',' || qty 
from (
    select location_id, sum(qty_on_hand) qty
    from inventory
    where client_id = 'TR'
    and (
    location_id IN ('CONTAINER','MARSHALB')
    or 
    location_id IN ('TRGOOD','TRBAD','TRSEC')
    or     
    location_id like 'TRWEB%'
    or
    location_id like 'TRALT%'
    or
    location_id like 'TROUT%'
    or
    location_id like 'TRWHO%'    
    or
    location_id like 'MP%'
    or
    location_id = 'QCCUTUP'
    or
    location_id = 'SUSPENSE'
    )
    group by location_id
)
union all
select ' ' from dual
union all
select 'HOLD locations checks:' from dual
union all
select '(if not empty make sure you know why)' from dual
union all
select 'Location,Units' from dual
union all
select location_id || ',' || qty 
from (
    select location_id, sum(qty_on_hand) qty
    from inventory
    where client_id = 'TR'
    and location_id like 'HOLD%'
    group by location_id
    order by 1
)
union all
select ' ' from dual
union all
select 'Locked stock' from dual
union all
select '(make sure it is only Seconds stock)' from dual
union all
select 'Location,Units' from dual
union all
select location_id || ',' || qty 
from (
    select location_id, sum(qty_on_hand) qty
    from inventory
    where client_id = 'TR'
    and LOCK_STATUS = 'Locked'
    group by location_id
    order by 1
)
/