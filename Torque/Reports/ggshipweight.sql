SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Sku,Description,Units Shipped,Individual Weight,Total Weight' from dual
union all
select "Sku" || ',' || "Desc" || ',' || "Ship" || ',' || "Weight" || ',' || "TotWeight" from
(
select * from (
select it.sku_id    as "Sku"
, s.description     as "Desc"
, sum(it.update_qty)    as "Ship"
, s.each_weight as "Weight"
, s.each_weight * sum(it.update_qty) as "TotWeight"
from inventory_transaction it
inner join sku s on s.client_id = it.client_id and it.sku_id = s.sku_id
where it.client_id = 'GG'
and it.code = 'Shipment'
and it.dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by it.sku_id
, s.description
, s.each_weight
union all
select it.sku_id    as "Sku"
, s.description     as "Desc"
, sum(it.update_qty)    as "Ship"
, s.each_weight as "Weight"
, s.each_weight * sum(it.update_qty) as "TotWeight"
from inventory_transaction_archive it
inner join sku s on s.client_id = it.client_id and it.sku_id = s.sku_id
where it.client_id = 'GG'
and it.code = 'Shipment'
and it.dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by it.sku_id
, s.description
, s.each_weight
) order by 1
)
/
--spool off