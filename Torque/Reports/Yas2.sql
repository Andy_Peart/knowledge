set pagesize 9999
set linesize 2000
set trimspool on
set colsep ','
set feedback off

select
key,
tag_id, 
client_id, 
sku_id, 
update_qty,
Dstamp,
reason_id
FROM INVENTORY_TRANSACTION
WHERE Client_Id in ('T','TR')
and SITE_ID = 'BRADFORD'
and Code = 'Stock Check'
and update_qty <> 0
and trunc(DSTAMP) =trunc(sysdate)
/

