SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep '|'


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 3000
SET TRIMSPOOL ON

SELECT 
	'DEFAULT' 
	, odh.order_id 
	, initcap(odh.name) 
	, initcap(odh.contact) 
	, initcap(odh.address1) 
	, initcap(odh.address2) 
	, NULL 
	, upper(odh.town) 
	, upper(odh.county) 
	, upper(odh.postcode) 
	, upper(odh.country) 
	, 'US' 
	, DECODE(odh.contact_phone,NULL,odh.inv_contact_phone||'.',odh.contact_phone) 
	, 'P'
	, 1
	, '0.5' 
	, '1' 
	, 'Clothing' 
	, 1 --sum(ODL.LINE_VALUE) 
	, 0 
	, 0 
	, NULL 
	, TO_CHAR(SYSDATE,	'DD/MM/YYYY') 
	, 1 
    , 'sbtracking@torque.eu'
	, DECODE(odh.contact_email,NULL,	odh.inv_contact_email,odh.contact_email) 
FROM ORDER_HEADER ODH INNER JOIN ORDER_LINE ODL ON ODH.ORDER_ID=ODL.ORDER_ID AND ODH.CLIENT_ID=ODL.CLIENT_ID
WHERE ODH.CLIENT_ID = 'SB'
AND ODH.CONSIGNMENT = '&2'
and nvl(ODL.QTY_TASKED,0) <> 0
group by odh.order_id, 
odh.inv_contact_email, 
odh.contact_phone,
odh.inv_contact_phone, 
SYSDATE, odh.county, 
odh.contact_email, 
odh.town,
odh.postcode, 
NULL, 
odh.address2, 
odh.address1, 
odh.name, 
odh.contact,
odh.country
order by ODH.ORDER_ID
/