SET FEEDBACK OFF
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
set colsep ','
set heading off
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9999

column A heading 'EXTBARCODE' format a20
column C heading 'QTY' format 9999999

select 'EXTBARCODE,QTY'
from dual;

select distinct s.sku_id A,
sum(i.qty_on_hand) C
from sku s, inventory i
where s.client_id = 'MOUNTAIN'
and s.sku_id = i.sku_id
and location_id <> 'DESPATCH'
and location_id <> 'N001'
and condition_id = 'MMAIL'
and s.sku_id <> 'TEST'
group by s.sku_id
/
