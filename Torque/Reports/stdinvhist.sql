SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Transaction Date,Code,Sku,Availability,Stock Change,Order #,PO Number,Adjustment' from dual
union all
select to_char("Date",'DD-MON HH24:MI') || ',' || "Code" || ',' || "Sku" || ',' || "Availability" || ',' || "Stock Change" || ',' || "Order #" || ',' || "PO Number" || ',' || "Adjustment" from
(
select "Date","Code","Sku",sum("Availability") over (order by "Date" desc rows  between unbounded preceding and current row) as "Availability","Stock Change","Order #","PO Number","Adjustment" from
(
select sysdate as "Date",'Current Available' as "Code",'''' || s.sku_id || '''' as "Sku",sum(i.qty_on_hand)-sum(i.qty_allocated) as "Availability",99999 as "Order",null as "Stock Change",null as "Order #",null as "PO Number",null as "Adjustment"
from sku s 
left join inventory i on i.sku_id = s.sku_id and i.client_id = s.client_id
left join location l on l.site_id = i.site_id and l.location_id = i.location_id
where s.sku_id = '&&3'
and i.client_id = '&&4'
and l.loc_type in ('Tag-FIFO','Tag-LIFO')
and i.lock_status = 'UnLocked'
--and i.condition_id = 'GOOD'
group by s.sku_id
union all
select it.dstamp as "Date"
, it.code as "Code"
, '''' || it.sku_id || '''' as "Sku"
, case when it.code in ('Stock Check','Adjustment','DeAllocate') then -(sum(it.update_qty)) else  sum(it.update_qty)  end as "Availability"
, rank()over(partition by it.sku_id order by it.dstamp) as "Order"
, case  when it.code in ('Allocate') then '(' || -(it.update_qty) || ')' 
        when it.code in ('Adjustment','Stock Check') and it.update_qty < 0  then '(' || -(it.update_qty) || ')"' 
        else '="(+' || it.update_qty || ')"' end as "Transaction"
, oh.order_id
, oh.purchase_order
, it.extra_notes
from inventory_transaction it
inner join location l on l.site_id = it.site_id and l.location_id = it.from_loc_id
left join order_header oh on oh.order_id = it.reference_id and oh.client_id = it.client_id
where it.client_id = '&&4'
and it.code in ('Stock Check','Adjustment','Allocate','DeAllocate')
and it.sku_id = '&&3'
--and it.condition_id = 'GOOD'
and it.lock_status = 'UnLocked'
and it.update_qty <> 0
and it.dstamp between to_date('&&2','DD-MON-YYYY') and trunc(sysdate)+1
and l.loc_type in ('Tag-LIFO','Tag-FIFO')
group by it.dstamp
, it.code
, it.sku_id
, it.dstamp
, it.update_qty
, oh.order_id
, oh.purchase_order
, it.extra_notes
union all
select it.dstamp as "Date"
, it.code as "Code"
, '''' || it.sku_id || '''' as "Sku"
, case when it.code in ('Inv UnLock') then -(sum(it.update_qty)) else  sum(it.update_qty)  end as "Availability"
, rank()over(partition by it.sku_id order by it.dstamp) as "Order"
, case when it.code in ('Inv Lock') then '(' || -(it.update_qty) || ')' else '="(+' || it.update_qty || ')"' end as "Transaction"
, oh.order_id
, oh.purchase_order
, it.extra_notes
from inventory_transaction it
inner join location l on l.site_id = it.site_id and l.location_id = it.from_loc_id
left join order_header oh on oh.order_id = it.reference_id and oh.client_id = it.client_id
where it.client_id = '&&4'
and it.code in ('Inv Lock','Inv UnLock')
and it.sku_id = '&&3'
--and it.condition_id = 'GOOD'
and it.update_qty <> 0
and it.dstamp between to_date('&&2','DD-MON-YYYY') and trunc(sysdate)+1
and l.loc_type in ('Tag-LIFO','Tag-FIFO')
group by it.dstamp
, it.code
, it.sku_id
, it.dstamp
, it.update_qty
, oh.order_id
, oh.purchase_order
, it.extra_notes
union all
select it.dstamp as "Date"
, it.code as "Code"
, '''' || it.sku_id || '''' as "Sku"
, case when it.extra_notes = 'GOOD -> CFSP' then sum(it.update_qty) when it.extra_notes = 'CFSP -> GOOD' then -(sum(it.update_qty)) end as "Availability"
, rank()over(partition by it.sku_id order by it.dstamp) as "Order"
, case when it.extra_notes = 'GOOD -> CFSP' then '(' || -(it.update_qty) || ')' when it.extra_notes = 'CFSP -> GOOD' then '="(+' || it.update_qty || ')"' end  as "Transaction"
, oh.order_id
, oh.purchase_order
, it.extra_notes
from inventory_transaction it
inner join location l on l.site_id = it.site_id and l.location_id = it.from_loc_id
left join order_header oh on oh.order_id = it.reference_id and oh.client_id = it.client_id
where it.client_id = '&&4'
and it.code = 'Cond Update'
and it.sku_id = '&&3'
and it.update_qty <> 0
and it.lock_status = 'UnLocked'
and it.dstamp between to_date('&&2','DD-MON-YYYY') and trunc(sysdate)+1
and l.loc_type in ('Tag-LIFO','Tag-FIFO')
group by it.dstamp
, it.code
, it.sku_id
, it.dstamp
, it.update_qty
, it.extra_notes
, oh.order_id
, oh.purchase_order
, it.extra_notes
union all
select it.dstamp as "Date"
, it.code as "Code"
, '''' || it.sku_id || '''' as "Sku"
, sum(it.update_qty)  as "Availability"
, rank()over(partition by it.sku_id order by it.dstamp) as "Order"
, '(' || -(it.update_qty) || ')' as "Transaction"
, oh.order_id
, oh.purchase_order
, it.extra_notes
from inventory_transaction it
inner join location l on l.site_id = it.site_id and l.location_id = it.from_loc_id
inner join location lo on lo.site_id = it.site_id and lo.location_id = it.to_loc_id
left join order_header oh on oh.order_id = it.reference_id and oh.client_id = it.client_id
where it.client_id = '&&4'
and it.code = 'Relocate'
and it.sku_id = '&&3'
--and it.condition_id = 'GOOD'
and it.lock_status = 'UnLocked'
and it.update_qty <> 0
and it.dstamp between to_date('&&2','DD-MON-YYYY') and trunc(sysdate)+1
and l.loc_type in ('Tag-LIFO','Tag-FIFO')
and lo.loc_type not in ('Tag-LIFO','Tag-FIFO')
group by it.dstamp
, it.code
, it.sku_id
, it.dstamp
, it.update_qty
, oh.order_id
, oh.purchase_order
, it.extra_notes
union all
select it.dstamp as "Date"
, it.code as "Code"
, '''' || it.sku_id || '''' as "Sku"
, -(sum(it.update_qty))  as "Availability"
, rank()over(partition by it.sku_id order by it.dstamp) as "Order"
, '="(+' || it.update_qty || ')"' as "Transaction"
, oh.order_id
, oh.purchase_order
, it.extra_notes
from inventory_transaction it
inner join location l on l.site_id = it.site_id and l.location_id = it.from_loc_id
inner join location lo on lo.site_id = it.site_id and lo.location_id = it.to_loc_id
left join order_header oh on oh.order_id = it.reference_id and oh.client_id = it.client_id
where it.client_id = '&&4'
and it.code = 'Relocate'
and it.sku_id = '&&3'
--and it.condition_id = 'GOOD'
and it.update_qty <> 0
and it.dstamp between to_date('&&2','DD-MON-YYYY') and trunc(sysdate)+1
and l.loc_type not in ('Tag-LIFO','Tag-FIFO')
and lo.loc_type in ('Tag-LIFO','Tag-FIFO')
group by it.dstamp
, it.code
, it.sku_id
, it.dstamp
, it.update_qty
, oh.order_id
, oh.purchase_order
, it.extra_notes
union all
select it.dstamp as "Date"
, it.code as "Code"
, '''' || it.sku_id || '''' as "Sku"
, case when it.code in ('Receipt','Return') then -(sum(it.update_qty)) else  sum(it.update_qty)  end as "Availability"
, rank()over(partition by it.sku_id order by it.dstamp) as "Order"
, '="(+' || it.update_qty || ')"' as "Transaction"
, oh.order_id
, oh.purchase_order
, it.extra_notes
from inventory_transaction it
inner join location l on l.site_id = it.site_id and l.location_id = it.to_loc_id
left join order_header oh on oh.order_id = it.reference_id and oh.client_id = it.client_id
where it.client_id = '&&4'
and it.code in ('Receipt','Return')
and it.sku_id = '&&3'
and it.update_qty <> 0
--and it.condition_id = 'GOOD'
and it.dstamp between to_date('&&2','DD-MON-YYYY') and trunc(sysdate)+1
and l.loc_type in ('Tag-LIFO','Tag-FIFO')
group by it.dstamp
, it.code
, it.sku_id
, it.dstamp
, it.update_qty
, oh.order_id
, oh.purchase_order
, it.extra_notes
order by "Order" desc
)order by "Date")
union all
select case when nvl(sum(i.qty_on_hand)-sum(i.qty_allocated),0) = 0
            then trunc(sysdate) || ',,' || s.sku_id || '''' || ',' || 'No stock currently' || ',,,' end
            from sku s 
            left join inventory i on i.sku_id = s.sku_id and i.client_id = s.client_id
            where s.sku_id = '&&3'
            and s.client_id = '&&4'
            group by s.sku_id
/
--spool off