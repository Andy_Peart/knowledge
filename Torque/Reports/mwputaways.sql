SET FEEDBACK OFF
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET COLSEP ','

set pagesize 0
set newpage 0
set linesize 140
set trimspool on


column C heading 'PA Reference' format a14
column E heading 'Qty' format 9999999


break on report

compute sum label ' ' of E on report


--spool mwf.csv


select 'PA Reference,Qty' from DUAL;


select
it.reference_id C,
sum(it.update_qty) E
from inventory_transaction it
where it.client_id='MOUNTAIN'
and it.code='Receipt'
and trunc(it.Dstamp)=trunc(sysdate)
and it.reference_id like 'MWF%'
group by
it.reference_id 
order by
C
/

--spool off

