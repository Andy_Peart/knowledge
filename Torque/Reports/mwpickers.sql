SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 300
SET TRIMSPOOL ON

column A heading 'SKU' format A16
column B heading 'Qty' format 999999
column C heading 'Picks' format 999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

break on report
compute sum label 'Totals' of B on report

--spool mwpickers.csv

select 'Units Picked on &&2 by &&3 for store &&4 ' from DUAL;

select 'SKU,Description,From Location,Qty,User Id,Store' from DUAL;


select
''''||it.sku_id A,
sku.description,
it.from_loc_id,
it.update_qty B,
it.user_id,
it.work_group
from inventory_transaction it,sku
where it.client_id='MOUNTAIN'
and it.code='Pick'
and it.to_loc_id='CONTAINER'
and trunc(it.Dstamp)=to_date('&&2', 'DD-MON-YYYY')
and it.user_id='&&3'
and it.work_group='&&4'
and it.sku_id=sku.sku_id
and sku.client_id='MOUNTAIN'
order by
it.sku_id
/

--spool off

