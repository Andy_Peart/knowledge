SET FEEDBACK OFF             
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

column A heading 'Date' format A12
column AA heading 'Time' format A12
column B heading 'SKU' format A18
column C heading 'Description' format A40
column D heading 'Orig Qty' format 999999
column E heading 'Update Qty' format 999999
column F heading 'Condition' format A15

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

select 'Date,Time,Sku Code,Description,Original Qty,Update Qty, Condition' from dual;

--ttitle  LEFT 'Pack Config Units Picked for client ID MOUNTAIN during Period '&&1' to '&&2' as at ' report_date skip 2

break on report
compute sum label 'Total' of D E on report


select
to_char(it.Dstamp,'DD/MM/YYYY') A,
to_char(it.Dstamp,'HH24:MI:SS') AA,
''''||it.sku_id||'''' B,
sku.description C,
it.original_qty D,
it.update_qty E,
it.condition_id
from inventory_transaction it,sku
where it.client_id='MOUNTAIN'
and it.code='Adjustment'
and from_loc_id not in ('TRAILER1','TRAILER2','TRAILER3','MAIL ORDER','DESPATCH','MARSHALL')
--and trunc(it.Dstamp)=trunc(sysdate)
--and it.Dstamp between sysdate and sysdate + 1
and trunc(it.Dstamp)>trunc(sysdate-1)
and it.sku_id=sku.sku_id
order by
it.Dstamp
/

