SET FEEDBACK OFF                 
set pagesize 68
set linesize 140
set verify off

clear columns
clear breaks
clear computes

column A heading 'Store No.' format a12
column B heading 'Name' format a30
column C heading 'No. of|Returns' format 999999
column D heading 'Units|Returned' format 999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'Returns Analysis from &&2 to &&3 - for client ID TR as at ' report_date skip 2

break on report

compute sum label 'Total' of C D  on report

select it.user_def_type_1 A, nvl(a.name,'Not Found') B, count(*) C, sum (it.update_qty) D
from inventory_transaction it inner join address a on it.user_def_type_1 = a.address_id and it.client_id = a.client_id
where it.client_id = 'TR'
and it.code = 'Receipt'
and it.dstamp between to_date('&&2', 'DD/MM/YY') and to_date('&&3', 'DD/MM/YY')+1
and (it.user_def_type_1 like 'T___' or it.user_def_type_1 like 'H___' or reference_id = 'M' or reference_id = 'R')
and a.client_id = 'TR'
group by it.user_def_type_1, a.name
order by A
/
