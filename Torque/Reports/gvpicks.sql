SET FEEDBACK OFF           
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 66
SET NEWPAGE 0
SET LINESIZE 100


column AA heading 'Type' format A32
column A heading 'User ID' format A18
column B heading 'Number' format 999999
column C heading 'Qty' format 999999

ttitle  LEFT 'Picks by User ID from &&2 to &&3 - for client ID GV'

break on AA skip 1 on report

compute sum label 'Totals' of B C on AA
compute sum label 'Overall' of B C on report


select 
'Retail Pick of Merchandise' AA,
user_id A,
count(*) B,
sum(update_qty) C
from inventory_transaction
where client_id = 'GV'
and code = 'Pick'
and reference_Id like 'T%'
and sku_id like 'G%'
and Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by user_id
union
select 
'Retail Pick of Consumables' AA,
user_id A,
count(*) B,
sum(update_qty) C
from inventory_transaction
where client_id = 'GV'
and code = 'Pick'
and reference_Id like 'T%'
and sku_id not like 'G%'
and Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by user_id
union
select 
'Home Shopping Pick' AA,
user_id A,
count(*) B,
sum(update_qty) C
from inventory_transaction
where client_id = 'GV'
and code = 'Pick'
and reference_Id not like 'T%'
and Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
group by user_id
order by AA,A
/

