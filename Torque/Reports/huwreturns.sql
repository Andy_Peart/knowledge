SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9999
SET TRIMSPOOL ON

--spool huwreturns.csv

select 'Date,Order ID,Product,Description,Condition,Location,Qty' from DUAL;

select
trunc(i.Dstamp)||','||
i.reference_id||','||
i.sku_id||','||
sku.description||','||
i.condition_id||','||
i.to_loc_id||','||
i.update_qty
from inventory_transaction i,sku
where i.client_id='HU'
and i.code = 'Receipt'
and i.Dstamp>sysdate-7
and sku.client_id='HU'
and i.sku_id=sku.sku_id
and i.reference_id not in (select pre_advice_id
from pre_advice_header
where client_id='HU')
order by
i.Dstamp,
i.reference_id,
i.sku_id

/

--spool off
