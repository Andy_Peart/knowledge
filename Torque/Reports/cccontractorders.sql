SET FEEDBACK OFF                 
set verify off
set wrap off
set colsep ','

clear columns
clear breaks
clear computes

set pagesize 0
set linesize 132
set trimspool on


select 'Date,Orders Received,Orders Shipped, %,Units Allocated,Units Picked,Accuracy' from DUAL;

select
D||','||
Q||','||
QQ||','||
ltrim(to_char((QQ/Q)*100,'990.00'))||','||
Q1||','||
Q2||','||
CASE
WHEN Q1=0 THEN '0.00'
ELSE ltrim(to_char((Q2/Q1)*100,'990.00'))
END
from (select
trunc(creation_date) D,
count(distinct order_id) Q,
nvl(QQ,0) QQ,
nvl(Q1,0) Q1,
nvl(Q2,0) Q2
from order_header,(select
trunc(oh.creation_date) DD,
count(distinct oh.order_id) QQ
from order_header oh
where oh.client_id='CC'
and oh.creation_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and trunc(oh.shipped_date)=trunc(oh.creation_date)
group by
trunc(oh.creation_date)),(select
trunc(oh.creation_date) D1,
sum(DECODE(it.code,'Deallocate',(it.update_qty*-1),it.update_qty)) Q1
from order_header oh,inventory_transaction it
where oh.client_id='CC'
and oh.creation_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='CC'
and it.code in ('Allocate','Deallocate')
and it.reference_id=oh.order_id
--and trunc(it.Dstamp)=trunc(oh.creation_date)
group by
trunc(oh.creation_date)),(select
trunc(oh.creation_date) D2,
sum(it.update_qty) Q2
from order_header oh,inventory_transaction it
where oh.client_id='CC'
and oh.creation_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='CC'
and it.code='Pick'
and it.elapsed_time>0
and it.reference_id=oh.order_id
group by
trunc(oh.creation_date))
where client_id='CC'
and creation_date between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and trunc(creation_date)=DD (+)
and trunc(creation_date)=D1 (+)
and trunc(creation_date)=D2 (+)
group by
trunc(creation_date),
QQ,
Q1,
Q2
order by
trunc(creation_date))
/

