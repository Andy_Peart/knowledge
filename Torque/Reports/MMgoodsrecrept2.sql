SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET WRAP Off


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 66
SET NEWPAGE 0
SET LINESIZE 132
SET HEADING OFF

 
column A heading 'Date' format A10
column B heading 'Supplier' format A20
column D heading 'GRN' format 9999
column E heading 'Pre Advice' format A10
column F heading 'Line' format 99999
column G heading 'SKU Code' format A18
column H heading 'Description' format A32
column I heading 'Order' format 99999
column J heading 'Recvd|Adjst' format 9999999
column K heading ' File| Diff' format 99999
column M format a4 noprint

ttitle left 'Goods Received Report - for client ID &&3 and pre-advice &&2 (Selected Pre Advice or Blind)' RIGHT 'Page: ' FORMAT 999 SQL.PNO skip 2


SET PAGESIZE 66
SET HEADING ON

--BREAK ON B on D on E on REPORT
--column B new_value null
--column D new_value null
--column E new_value null

BREAK ON REPORT
compute sum label 'Total' of J  on report


select
to_char(it.Dstamp, 'YYMM') M,
to_char(it.Dstamp, 'DD/MM/YY') A,
nvl(ad.name,'NOT KNOWN') B,
it.grn D,
it.reference_id E,
it.line_id F,
it.sku_id G,
sku.description H,
nvl(ph.qty_due,0) I,
sum(it.update_qty) J,
nvl(ph.qty_due,0)-nvl(ph.qty_received,0) K
from inventory_transaction it,pre_advice_line ph,sku,address ad
where it.client_id='&&3'
and it.station_id not like 'Auto%'
and it.supplier_id=ad.address_id (+)
and it.reference_id='&&2'
and it.reference_id=ph.pre_advice_id (+) 
and it.line_id=ph.line_id (+)
and it.code like 'Receipt%'
--and (it.code='Receipt' or it.code='Adjustment')
and it.sku_id=sku.sku_id (+)
group by
to_char(it.Dstamp, 'YYMM'),
to_char(it.Dstamp, 'DD/MM/YY'),
it.supplier_id,ad.name,it.grn,it.reference_id,it.sku_id,it.line_id,sku.description,
--nvl(ph.qty_due,0),nvl(ph.qty_due,0)-nvl(ph.qty_received,0)
ph.qty_due,ph.qty_received
order by
it.reference_id,it.line_id
/
set heading off
set pagesize 0
CLEAR BREAKS
CLEAR COMPUTES

select
'ORDER SUMMARY             Ordered',sum(ph.qty_due) I,
'      Received',sum(nvl(ph.qty_received,0)) J,
'      Difference',sum(ph.qty_due-nvl(ph.qty_received,0)) K
from pre_advice_line ph
where ph.client_id='&&3'
and ph.pre_advice_id='&&2'
/
