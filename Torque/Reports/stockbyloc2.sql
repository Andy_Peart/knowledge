set feedback off
set pagesize 70
set linesize 140
set verify off


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

 

column A heading 'Location' format A11
column B heading 'SKU' format A20
column C heading 'Description' format A30
column C3 heading 'Range' format A15
column C4 heading 'Season' format A15
column C5 heading 'Style' format A15
column D heading 'Qty' format 999999
column E heading '  Moved' format A14

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

set und '-'
set heading on
set newpage 0

ttitle  LEFT 'Stock Units by Location Range from &&2 to &&3 - for client ID &&4 as at ' report_date RIGHT 'Page:' FORMAT 999 SQL.PNO skip 4

break on report

compute sum label 'Total' of D on report

break on row skip 1


select
i.sku_id B,
Sku.description C,
i.location_id A,
sku.user_def_type_5 C3,
sku.user_def_type_3 C4,
sku.user_def_type_4 C5,
sum(i.qty_on_hand) D,
'  ____________' E
from inventory i
inner join sku on i.sku_id = sku.sku_id
and i.client_id = sku.client_id
where i.client_id like '&&4'
and location_id between '&&2' and '&&3'
and substr(location_id,1,1)<>'T'
and substr(location_id,1,1)<>'V'
group by
i.sku_id,
Sku.description,
i.location_id,
sku.user_def_type_5,
sku.user_def_type_3,
sku.user_def_type_4
order by i.location_id,i.sku_id
/
ttitle  LEFT ' '
set heading off
break on row skip 0
set newpage 1

select
'Location Count =',
count (*) D
from (select distinct location_id from inventory
where client_id like '&&4'
and location_id between '&&2' and '&&3'
and substr(location_id,1,1)<>'T'
and substr(location_id,1,1)<>'V')
/


select
'     SKU Count =',
count (*) D
from (select distinct sku_id from inventory
where client_id like '&&4'
and location_id between '&&2' and '&&3'
and substr(location_id,1,1)<>'T'
and substr(location_id,1,1)<>'V')
/

select
'    Unit Count =',
nvl(sum(qty_on_hand),0) D
from inventory
where client_id like '&&4'
and location_id between '&&2' and '&&3'
and substr(location_id,1,1)<>'T'
and substr(location_id,1,1)<>'V'
/

