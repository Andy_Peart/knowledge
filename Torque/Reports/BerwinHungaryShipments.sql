SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON


column A heading 'Type' format a18
column A1 heading 'Name' format a12
column A2 heading 'Reference' format a14
column A3 heading 'SKU' format a14
column A4 heading 'Date' format a14
column B heading 'Units' format 999999
column K heading 'Units' format 999999
column B1 heading 'Rate' format A6
column C heading 'Value' format 999990.99

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON




break on A2 dup skip 1 on report


compute sum label 'Sub Total' of B C K on A2
--compute sum label 'Summary' of B C on A1
--compute sum label 'Totals' of B C on A
compute sum label 'Overall' of B C K on report

--spool berwinHung.csv

select 'Type,Name,Reference,PORD,Date Shipped,Units Rec,Units Shipped,Value' from DUAL;


select
'Shipments' A,
'Hungarian' A1,
A2,
A3,
A4,
sum(BBB) K,
sum(BB) B,
sum(BB)*0.03 C
from (select
R1 A2,
U1 A3,
D1 A4,
--S1 A5,
sum(QQQ) BBB,
sum(Q1) BB
from (select
it.reference_id R1,
UPPER(substr(ol.user_def_note_2,1,12)) U1,
to_char(it.Dstamp,'dd/mm/yy') D1,
it.sku_id S1,
it.tag_id T1,
sum(it.update_qty) Q1
from inventory_transaction it,order_line ol
where it.client_id='BW'
and to_char(it.Dstamp,'mm/yyyy')=to_char(sysdate,'mm/yyyy')
and it.code='Shipment'
--and it.reference_id='EL1102-07DC'
and ol.user_def_note_2 like '%-%'
and it.reference_id=ol.order_id
and it.line_id=ol.line_id
and it.sku_id=ol.sku_id
and ol.client_id='BW'
group by
it.reference_id,
UPPER(substr(ol.user_def_note_2,1,12)),
to_char(it.Dstamp,'dd/mm/yy'),
it.sku_id,
it.tag_id),(select
sku_id SSS,
tag_id TTT,
sum(update_qty) QQQ
from inventory_transaction
where client_id='BW'
and code='Receipt'
group by sku_id,tag_id)
where S1=SSS
and T1=TTT
group by
R1,
U1,
D1)
where ABS(BBB-BB)<6
group by
A2,
A3,
A4
union
select
'Shipments (Part)' A,
'Hungarian' A1,
A2,
A3,
A4,
sum(BBB) K,
sum(BB) B,
sum(BB)*0.18 C
from (select
R1 A2,
U1 A3,
D1 A4,
--S1 A5,
sum(QQQ) BBB,
sum(Q1) BB
from (select
it.reference_id R1,
UPPER(substr(ol.user_def_note_2,1,12)) U1,
to_char(it.Dstamp,'dd/mm/yy') D1,
it.sku_id S1,
it.tag_id T1,
sum(it.update_qty) Q1
from inventory_transaction it,order_line ol
where it.client_id='BW'
and to_char(it.Dstamp,'mm/yyyy')=to_char(sysdate,'mm/yyyy')
and it.code='Shipment'
--and it.reference_id='EL1102-07DC'
and ol.user_def_note_2 like '%-%'
and it.reference_id=ol.order_id
and it.line_id=ol.line_id
and it.sku_id=ol.sku_id
and ol.client_id='BW'
group by
it.reference_id,
UPPER(substr(ol.user_def_note_2,1,12)),
to_char(it.Dstamp,'dd/mm/yy'),
it.sku_id,
it.tag_id),(select
sku_id SSS,
tag_id TTT,
sum(update_qty) QQQ
from inventory_transaction
where client_id='BW'
and code='Receipt'
group by sku_id,tag_id)
where S1=SSS
and T1=TTT
group by
R1,
U1,
D1)
where ABS(BBB-BB)>5
group by
A2,
A3,
A4
order by
A2,A,A3
/

--spool off

