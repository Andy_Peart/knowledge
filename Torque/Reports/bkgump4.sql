SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','
SET UND OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 320


select 'Date,Client ID,Customer ID,No. of Orders,Units Shipped' from DUAL;

column A format A14
column B format A10
column C format A12
column D format 99999999
column E format 99999999
column M format a4 noprint



set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



break on A dup skip 1 on report

compute sum label 'Totals'  of D E  on report


Select 
to_char(Dstamp, 'YYMM') M,
to_char(Dstamp,'DD/MM/YY') A,
Client_ID B,
Customer_ID C,
count(distinct Reference_ID) D,
sum(update_qty) E
from inventory_transaction
where client_id='&&2'
and Dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and code='Shipment'
group by
to_char(Dstamp, 'YYMM'),
to_char(Dstamp,'DD/MM/YY'),
Client_ID,
Customer_ID
order by
M,A,B
/

