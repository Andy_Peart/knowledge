SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET COLSEP ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 200
SET TRIMSPOOL ON
SET HEADING OFF 

column Z heading 'Status' format A12
column A heading 'Order' format A20
column B heading 'Line' format 999999
column C heading 'SKU' format A18
column D heading 'Qty1' format 9999999
column E heading 'Qty2' format 9999999

select 'UT Order &&2 Shipment Detail' from DUAL;
--select to_char(SYSDATE, 'DD/MM/YY    HH:MI:SS') from DUAL;

select 'Status,Order ID,Line ID,SKU ID,Description,Qty Ordered,Qty Picked' from DUAL;


--break on report
--compute sum label 'Total' of D on report





select
oh.status Z,
oh.order_id A,
ol.line_id B,
ol.sku_id C,
sku.description,
ol.qty_ordered E,
nvl(ol.qty_picked,0) D
from order_header oh, order_line ol,sku
where oh.client_id like 'UT'
--and oh.status='Complete'
and oh.order_id like '&&2%'
and oh.client_id=ol.client_id
and oh.order_id=ol.order_id
and sku.client_id=ol.client_id
and sku.sku_id=ol.sku_id
order by B
/
