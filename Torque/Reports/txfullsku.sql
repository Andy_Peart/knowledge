SET FEEDBACK OFF
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
set colsep ','
set heading off
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 9999

column A heading 'EXTBARCODE' format a20
column D heading 'QTY' format 9999999

select 'EXTBARCODE,QTY'
from dual;

select A,
D
from (select A,
B - C "D"
from (select i.sku_id A, sum(i.qty_on_hand) B,
s.user_def_num_4 C
from inventory i, sku s
where i.client_id = 'TIMEX'
and i.sku_id = s.sku_id
group by s.user_def_num_4, i.sku_id)
order by A)
where D > 0
/
