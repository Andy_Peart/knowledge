select D from 
(select Z, greatest(nvl(sum(C-B),0),0) D from 
(select Z, s.user_def_num_3 B, nvl(sum(i.qty_on_hand - i.qty_allocated),0) C
from sku s, inventory i, 
(select X, iol.sku_id Z from interface_order_line iol,
(select min(iol.key) X from interface_order_line iol
where iol.client_id = 'MOUNTAIN'
and iol.merge_status = 'Pending'
and iol.sku_id is not null
)
where iol.key = X
)
where s.sku_id = i.sku_id (+)
and s.client_id = 'MOUNTAIN'
and Z = s.sku_id
group by s.sku_id, s.user_def_num_3, Z)
group by Z)

