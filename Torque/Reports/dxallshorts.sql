SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 66
SET NEWPAGE 1
SET LINESIZE 9999
SET TRIMSPOOL ON

column A heading 'Date' format A12
column A1 heading 'Order ID' format A22
column A2 heading 'SKU ID' format A20
column A3 heading 'Description' format A40
column F heading 'Instructions' format A24
column B heading 'Shortage' format 999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



break on report
compute sum label 'Total' of B on report

--spool dxshorts.csv

ttitle  LEFT 'All Daxbourne Shortages -  as at ' report_date skip 2

--select 'Date,Order No,Instructions,SKU,Description,Shortage' from DUAL;

select
distinct
trunc(md.Dstamp) A,
md.Task_id A1,
oh.instructions F,
md.sku_id A2,
sku.description A3,
md.old_qty_to_move-md.qty_to_move B
from move_descrepancy md,sku,order_header oh
where md.client_id='DX'
and md.reason_code='EX_NE'
and md.sku_id=sku.sku_id
and sku.client_id='DX'
and md.task_id=oh.order_id
and oh.client_id='DX'
order by
trunc(md.Dstamp),
md.Task_id
/

--spool off

