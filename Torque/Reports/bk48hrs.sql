SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep '~'


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240


column A heading 'Order Date' format A11
column B heading 'Shipped' format A11
column C heading 'Order ID' format A12
column D heading 'Name' format A24
column E heading 'Address_1' format A36
column F heading 'Post Code' format A12
column G heading 'SKU' format A18


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



--ttitle  LEFT 'TML Retail - Duplicate Order Addresses ' RIGHT 'Printed ' report_date skip 2

select ',Order Type,Order ID,Order Date,Name,Address 1,Post Code,SKU,Description,Colour,Size,Qty' from DUAL;


--break on F skip 1

select
'1 ' ||','||
oh.order_type ||','||
oh.order_id ||','||
to_char(oh.order_date, 'DD/MM/YY') ||','||
oh.name ||','||
oh.address1 ||','||
oh.postcode ||','||
ol.sku_id ||','||
sku.description ||','||
sku.colour ||','||
sku.sku_size  ||','||
ol.qty_ordered
from order_header oh, order_line ol,sku
Where oh.Client_id = 'MM'
and oh.Order_type = 'MAILORDER'
and oh.status='Released'
and oh.order_date<sysdate-2
and oh.order_id=ol.order_id
and ol.sku_id=sku.sku_id
union
select
'2 ' ||','||
oh.order_type ||','||
oh.order_id ||','||
to_char(oh.order_date, 'DD/MM/YY') ||','||
oh.name ||','||
oh.address1 ||','||
oh.postcode ||','||
ol.sku_id ||','||
sku.description ||','||
sku.colour ||','||
sku.sku_size  ||','||
ol.qty_ordered
from order_header oh, order_line ol,sku
Where oh.Client_id = 'MM'
and oh.Order_type<>'MAILORDER'
and oh.status='Released'
and oh.order_date<sysdate-5
and oh.order_id=ol.order_id
and ol.sku_id=sku.sku_id
--order by
--oh.postcode,
--oh.name
/
