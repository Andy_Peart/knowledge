set feedback off
set pagesize 0
set linesize 120
set verify off
set colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

 

column A heading 'Location' format A11
column B heading 'SKU' format A30
column C heading 'Description' format A40
column D heading 'Qty' format 999999
--column E heading '  Moved' format A14

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

set und '-'
set heading on
set newpage 0
set trimspool on


--spool BWSTOCK2.csv

--ttitle  LEFT 'Stock Levels for BERWIN as at ' report_date RIGHT 'Page:' FORMAT 999 SQL.PNO skip 4

--break on report

--compute sum label 'Total' of D on report

--break on row skip 1

select 'Location,SKU,Description,Qty' from DUAL;

select
i.location_id||','||
i.sku_id||','||
Sku.description||','||
sum(i.qty_on_hand)
from inventory i,sku
where i.client_id like 'BW'
and i.sku_id=sku.sku_id
and i.location_id<>'BEROUT'
group by
i.location_id,
i.sku_id,
Sku.description
order by 
i.location_id,
i.sku_id
/

--spool off
