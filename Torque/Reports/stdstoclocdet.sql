SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Client id,Location Zone,Receipt id,Condition,Status,Receipt Date,Receipt Time,Move Date,Move Time,Location,Tag,Sku,Qty,Qty Allocated,Description' from dual
union all
select "Client" ||','|| "Location Zone" ||','|| "Receipt id" ||','|| "Condition" ||','|| "Status" ||','|| "Receipt Date" ||','|| "Receipt Time" ||','||
"Move Date" ||','|| "Move Time" ||','|| "Location" ||','|| "Tag" ||','|| "Sku" ||','|| "Qty" ||','|| "Qty Allocated" ||','|| "Description"
from
(
select i.client_id  as "Client"
, l.zone_1          as "Location Zone"
, i.receipt_id      as "Receipt id"
, i.condition_id    as "Condition"
, i.lock_status     as "Status"
, to_char(i.receipt_dstamp,'DD/MM/YYYY')    as "Receipt Date"
, to_char(i.receipt_dstamp,'HH24:MI')       as "Receipt Time"
, to_char(i.move_dstamp,'DD/MM/YYYY')       as "Move Date"
, to_char(i.move_dstamp,'HH24:MI')          as "Move Time"
, i.location_id as "Location"
, i.tag_id      as "Tag"
, i.sku_id      as "Sku"
, sum(i.qty_on_hand)    as "Qty"
, sum(i.qty_allocated)  as "Qty Allocated"
, s.description         as "Description"
from inventory i
inner join location l on l.site_id = i.site_id and l.location_id = i.location_id
inner join sku s on s.sku_id = i.sku_id and s.client_id = i.client_id
where i.client_id = '&&2'
and i.location_id between '&&3' and '&&4'
and i.location_id <> 'SUSPENSE'
group by i.client_id
, l.zone_1
, i.receipt_id
, i.condition_id
, i.lock_status
, to_char(i.receipt_dstamp,'DD/MM/YYYY')
, to_char(i.receipt_dstamp,'HH24:MI')
, to_char(i.move_dstamp,'DD/MM/YYYY')
, to_char(i.move_dstamp,'HH24:MI')
, i.location_id
, i.tag_id
, i.sku_id
, s.description
order by i.location_id
, i.sku_id
)
/
--spool off