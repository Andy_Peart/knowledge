SET FEEDBACK OFF           
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON


select 'Code,Client,SKU,Description,Pre Advice,Qty,Date Time,Notes,User,User Def Type 2,User Def Type 1' col1 from dual
union all
select code || ',' || client_id  || ',' || sku_id || ',' ||  des || ',' || reference_id || ',' || update_qty || ',' ||  datetime || ',' || notes || ',' || user_id || ',' || user_def_type_2 || ',' || user_def_type_1 as col1
from (
select code, client_id, sku_id, libsku.getuserlangskudesc('&&2',sku_id) des, reference_id, update_qty,  to_char(dstamp, 'DD/MM/YYYY HH24:MI') as datetime, notes, user_id, user_def_type_2, user_def_type_1
from inventory_transaction
where client_id='&&2'
and code like 'Receipt%'
and reference_id = '&&3'
)
/

--spool off
