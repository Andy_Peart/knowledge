SET FEEDBACK OFF                 
set pagesize 68
set linesize 132
set verify off
set wrap off
set newpage 0

clear columns
clear breaks
clear computes


column A heading 'Task Type' format a18
column B heading 'User ID' format a18
column C heading 'Tasks' format 99999999
column D heading 'Units' format 99999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'Total Tasks by User ID from &&2 to &&3 - for client ID &&5 as at ' report_date RIGHT 'Page:'FORMAT 999 SQL.PNO skip 2 

BREAK ON A SKIP 1
COMPUTE SUM OF C D ON A;

SELECT
CODE A,
user_id B,
count(client_id) C,
sum(update_qty) D
from inventory_transaction
where Dstamp between to_date('&&2 00:00:00', 'dd/mm/yyyy HH24:MI:SS') and to_date('&&3 23:59:59', 'dd/mm/yyyy HH24:MI:SS')
and site_id='&&4'
and client_id='&&5'
and update_qty<>0
and station_id not like 'Auto%'
and ((code in ('Receipt','Putaway','Replenish','Relocate','Stock Check') and elapsed_time>0)
or (code='Pick' and to_loc_id='CONTAINER')
or (code='Pick' and (to_loc_id<>'CONTAINER' and from_loc_id<>'CONTAINER')
AND ('&&5'<>'T' AND TO_LOC_ID<>'BR04' AND FROM_LOC_ID<>'MARSHALB')))
GROUP BY CODE,USER_ID
order by CODE,USER_ID
/