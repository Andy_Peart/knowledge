set feedback on


insert into sporta_snapshot(
the_date,the_sku,the_zone,the_qty)
select
trunc(sysdate),
sku_id,
zone_1,
sum(qty_on_hand)
from inventory
where client_id='SP'
group by
sku_id,
zone_1
/

--rollback;
commit;
