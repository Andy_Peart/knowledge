SET FEEDBACK OFF                 
set pagesize 68
set linesize 160
set verify off

clear columns
clear breaks
clear computes


column A heading 'Carton' format A10
column B heading 'Store' format A10
column C heading 'Name' format A40
column D heading 'Consignment' format A12
column E heading 'Units' format 999999


set heading off

Select
address_id B,
name C
from address
where client_id='TR'
and address_id like substr('&&2',3,4)
/



--select 'Shipments for Consignment &&2' from DUAL;
 
set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DAY, DD MONTH, YYYY') curdate from DUAL;
set TERMOUT ON


set heading on


break on D skip 1 dup on report 

compute sum label 'Total' of E on report

select
it.consignment D,
it.container_id A,
sum(it.update_qty) E
from inventory_transaction it
where it.client_id='TR'
and it.final_loc_id not like 'TRWEB%'
and it.final_loc_id not like 'TRALT%'
and it.code='Shipment'
and it.consignment like '&&2%'
and it.container_id is not null
group by
it.consignment,
it.container_id
order by
it.consignment,
it.container_id
/

set heading off
set pagesize 0


select '' from DUAL;
select '                  _______' from DUAL;
select '                 |       |' from DUAL;
select '                 |       |' from DUAL;
select '    Transfers:-  |_______|' from DUAL;

select '' from DUAL;
select '                  _______' from DUAL;
select '                 |       |' from DUAL;
select '                 |       |' from DUAL;
select '    Packaging:-  |_______|' from DUAL;

select '' from DUAL;
select '' from DUAL;
select '                  ______________' from DUAL;
select '                 |              |' from DUAL;
select '                 |              |' from DUAL;
select '    Checked by:- |______________|' from DUAL;


