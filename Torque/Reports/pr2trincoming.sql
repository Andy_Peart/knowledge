/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :     pr2trincoming.sql								      */
/*                                                                            */
/*     DESCRIPTION:     Printout of Pre Advice details to be used by w/house  */
/*                                                                            */
/*   DATE     BY   DESCRIPTION						                          */
/*   ======== ==== ===========					                              */
/*   16/04/13 SY   modified to show qty_due from client PR                    */
/******************************************************************************/
SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','
SET WRAP OFF


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500



/* Column Headings and Formats */
COLUMN BA heading "Style" FORMAT a10
COLUMN BC heading "Product" FORMAT a20
COLUMN SKU_ID heading "SKU" FORMAT a20
COLUMN DESCR heading "Description" FORMAT a40
COLUMN SIZE_ heading "Size" FORMAT a10
COLUMN COLOUR_ heading "Colour" FORMAT a16
COLUMN D heading "PackList" FORMAT 9999999
COLUMN E heading "Received" FORMAT 9999999
COLUMN VAR heading "Var" FORMAT 9999999


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



/* Top Title */
select 'Report for GRN, &&2' from DUAL;
select 'Style,Product,SKU,Description,Size,Colour,Packlist,Received,Var' from DUAL;




BREAK ON BA on REPORT
column BA new_value null
compute sum label 'Totals' of D E on report

SELECT 
SKU_.Style_ as BA, 
SKU_.Product BC,
PAL.SKU_ID,
SKU_.DESCR,
SKU_.SIZE_,
SKU_.COLOUR_,
nvl(PALP.Qty_Received,0) as D,
nvl(PAL.Qty_Received,0) as E,
nvl(PALP.Qty_Received,0) - nvl(PAL.Qty_Received,0) as Var
FROM Pre_advice_header PAH, 
Pre_advice_line PAL, 
Pre_advice_line PALP,
(select 
description as Descr, 
sku_id as SSS, 
sku_size as size_,
colour as colour_,
user_def_type_4 as style_,
user_def_type_2||'-'||user_def_type_1||'-'||user_def_type_4 as product
from sku where client_id = 'TR') SKU_
WHERE PAH.Pre_advice_id = '&&2'
and PAH.Pre_advice_id = PAL.Pre_advice_id
and PALP.Pre_advice_id = PAL.User_def_type_8
and PALP.Client_id = 'PR'
and PALP.SKU_id = PAL.SKU_id
and PAL.SKU_ID = SSS
/