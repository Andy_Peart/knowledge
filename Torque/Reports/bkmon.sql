SET FEEDBACK OFF                 
set pagesize 68
set linesize 120
set verify off
set wrap off
set newpage 0

clear columns
clear breaks
clear computes


column A heading 'ROUTE' format A8
column CC heading 'DOCK' format A8
column BB heading 'ORDER' format A24
column SS heading 'STATUS' format A12
column DD heading 'CREATED' format A20
column TT heading 'TYPE' format A12
column GG heading 'CONSIGNMENT' format A12


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON



ttitle  LEFT 'TML Retail - Orders for Picking and Despatching on MONDAY ' RIGHT 'Printed ' report_date skip 2


break on A skip 2
 
select
a.user_def_type_1 A,
oh.order_id BB,
oh.customer_id CC,
oh.consignment GG,
oh.status SS,
to_char(oh.creation_date, 'DD/MM/YYYY HH:MI:SS') DD,
oh.order_type TT
from order_header oh,address a
where oh.order_type<>'REPLEN'
and oh.status not in ('Cancelled','Hold','Shipped')
and oh.client_id='TR'
and a.client_id='TR'
and oh.customer_id=a.address_id
and a.delivery_open_mon='Y'
and a.user_def_type_1 is not null
order by
a.user_def_type_1,
oh.customer_id,
oh.order_id
/
