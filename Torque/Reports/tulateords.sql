SET FEEDBACK OFF              
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 250
SET TRIMSPOOL ON

/*Input SQL here*/
select 'Client id,Status,Order Type,Order id,Number of Lines,Qty Ordered,Creation Date,Ship by Date,Name,Country' Headers from dual
union all
select client_id||','||status||','||order_type||','||order_id||','||num_lines||','||"Qty Ordered"||','||"Creation Date"||','||"Ship by Date"
||','||name||','||tandata_id
from
(
    select oh.client_id
    , oh.status
    , oh.order_type
    , oh.order_id
    , oh.num_lines
    , sum(ol.qty_ordered)   as "Qty Ordered"
    , trunc(oh.creation_date) as "Creation Date"
    , trunc(oh.ship_by_date) as "Ship by Date"
    , oh.name
    , c.tandata_id
    from order_header oh
    inner join order_line ol on ol.order_id = oh.order_id and ol.client_id = oh.client_id
    left join country c on c.iso3_id = oh.country
    where oh.client_id = 'TU'
    and oh.status not in ('Shipped','Cancelled')
    and trunc(oh.ship_by_date) <= trunc(sysdate)
    group by oh.client_id
    , oh.status
    , oh.order_type
    , oh.order_id
    , oh.num_lines
    , trunc(oh.creation_date)
    , trunc(oh.ship_by_date)
    , oh.name
    , c.tandata_id
    order by trunc(oh.ship_by_date)
)
/
--spool off