/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :     Incoming_pa.sql					      */
/*                                                                            */
/*     DESCRIPTION:     Printout of Pre Advice details to be used by w/house  */
/*                                                                            */
/*   DATE     BY   DESCRIPTION						      */
/*   ======== ==== ===========					              */
/*   04/11/04 BS   initial version     				  	      */
/*   15/09/06 MS   modified to include ean    				      */
/*   19/02/07 BK   modified to change text                                    */ 
/*   12/03/07 BK   modified to include SKU description                        */
/******************************************************************************/
SET FEEDBACK OFF
SET VERIFY OFF
SET TAB OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Column Headings and Formats */
COLUMN A heading "PO NO" FORMAT a14
COLUMN B heading "SKU" FORMAT a18
COLUMN BB heading "Description" FORMAT a40
COLUMN C heading "Line" FORMAT 9999
COLUMN D heading " Qty|Due" FORMAT 999999
COLUMN E heading " GRN|Qty" FORMAT 999999
COLUMN EE heading " Vari|-ance" FORMAT 999999
COLUMN F heading "EAN" FORMAT a16
COLUMN G heading "GRN" FORMAT a12
COLUMN H heading "NOTES" FORMAT a20
COLUMN J heading "  " FORMAT a2


set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


SET LINESIZE 124
SET WRAP OFF
SET PAGES 66
SET COLSEP ''
SET NEWPAGE 0

/* Top Title */
TTITLE CENTER 'Client: ' &&3 SKIP 1 -
LEFT 'GRN Report for PO &&2' RIGHT 'Date: ' report_date SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 1 - 

/* Set up page and line */

break on report

compute sum label 'Totals' of D E EE on report


SELECT	 PAL.SKU_ID B,
	 DDD BB,
	 PAL.Line_ID C,
         nvl(PAL.Qty_Due,0) D,
         nvl(PAL.Qty_Received,0) E,
         nvl(PAL.Qty_Due,0)-nvl(PAL.Qty_Received,0) EE,
	 '  ' J,
         SSS F,
         PAH.Notes G,
         PAH.user_def_note_2 H
FROM Pre_Advice_Header PAH, Pre_Advice_Line PAL,(select 
sku_id SSS,
description DDD,
ean EEE 
from sku where client_id='&&3')
WHERE PAH.Pre_Advice_ID = PAL.Pre_Advice_ID
AND PAL.SKU_ID = SSS (+)
AND PAH.Pre_Advice_ID = '&&2'
and PAH.client_id = '&&3'
ORDER BY PAL.user_def_type_2
/
