SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET COLSEP ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET HEADING OFF
SET PAGESIZE 0
SET LINESIZE 500
SET TRIMSPOOL ON

column A format A12
column AA format A40
column B format A12
column BB format A100
column C format A12
column D format 999999



break on A
--compute sum label 'Cust Total' of B on C
--compute sum label 'Full Total' of B on report

spool USAorders.csv

--select 'USA Shipments' from DUAL;
select to_char(SYSDATE, 'DD/MM/YY    HH:MI:SS') from DUAL;



select 'Order Ref,Name,E Mail Address,Delivery Address,Shipped On,SKU,Description,Qty' from DUAL;

select
oh.order_id A,
oh.name,
oh.inv_contact_email AA,
--'"'||inv_address1||','||inv_address2||','||inv_town||','||inv_county||','||inv_postcode||','||inv_country||'"' BB,
'"'||address1||','||address2||','||town||','||county||','||postcode||','||country||'"' BB,
trunc(oh.shipped_date) B,
ol.sku_id C,
sku.description,
nvl(ol.qty_shipped,0) D
from order_header oh,order_line ol,sku
where oh.client_id='SB'
and oh.status='Shipped'
and oh.country='USA'
and trunc(oh.shipped_date)>trunc(sysdate)-1
and oh.order_id=ol.order_id
and ol.client_id='SB'
and ol.sku_id=sku.sku_id
and sku.client_id='SB'
order by A,B,C
/

spool off
