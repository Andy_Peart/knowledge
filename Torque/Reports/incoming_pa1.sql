/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :     Incoming_pa.sql					      */
/*                                                                            */
/*     DESCRIPTION:     Printout of Pre Advice details to be used by w/house  */
/*                                                                            */
/*   DATE     BY   DESCRIPTION						      */
/*   ======== ==== ===========					              */
/*   04/11/04 BS   initial version     				  	      */
/*   15/09/06 MS   modified to include ean    				      */
/******************************************************************************/
SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Column Headings and Formats */
COLUMN "PO NO" FORMAT a14
COLUMN "SKU" FORMAT a12
COLUMN "Line" FORMAT 9999
COLUMN "QTY_DUE" FORMAT 9999999
COLUMN "QTY_RECEIVED" FORMAT 99999999999
COLUMN "SIZE" FORMAT a14 
COLUMN "EAN" FORMAT 9999999999999 
COLUMN "NOTES" FORMAT a30

/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Incoming Pre Advice Report for PO &&2' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2 - 

/* Set up page and line */
SET LINESIZE 132
SET WRAP OFF
SET PAGES 999


SELECT PAH.Pre_Advice_ID "PO NO",
	 PAL.SKU_ID "SKU",
	 PAL.Line_ID "Line",
         nvl(PAL.Qty_Due,0) "Qty_Due",   
         nvl(PAL.Qty_Received,0) "Qty_Received",
         PAL.user_def_type_2 "Size", 
         S.ean "EAN", 
         PAH.Notes "Notes"
FROM Pre_Advice_Header PAH, Pre_Advice_Line PAL, SKU S
WHERE PAH.Pre_Advice_ID = PAL.Pre_Advice_ID
AND PAL.SKU_ID = S.SKU_ID
AND PAH.Pre_Advice_ID = '&&2'
and PAH.client_id = 'TR'
and S.client_id = 'TR'
ORDER BY PAL.user_def_type_2;
