SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240


column A heading 'ROUTE' format A6
column AAA heading 'AAA' format A6 NOPRINT
column CC heading 'DOCK' format A4
column CCC heading 'CCC' format A6 NOPRINT
column D heading 'STORE NAME' format A32
column BB heading 'ORDER' format A8
column SS heading 'STATUS' format A11
column SSS heading 'M T STATUS' format A11
column DD heading 'CREATED' format A11
column DDD heading 'TIME' format A11
column TT heading 'TYPE' format A11
column GG heading 'CONSIGNMENT' format A12
column HH heading 'CONSIGN' format A14
column MM heading 'OPENS' format A18
column NN heading 'PICK' format A8
column QQQ heading 'UNITS' format 99999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON

select 'Route,Order ID,Dock,Store Name,Units,Creation Date,Creation Time,Order Type' from DUAL;

--ttitle  LEFT 'TML Retail - Orders for Picking and Despatching on &&2 ' RIGHT 'Printed ' report_date skip 2

break on report
--break on HH skip 1 on AAA skip 2 on CCC skip 1

compute sum label 'Total' of QQQ on report
--compute sum of QQQ on CCC

select
--'OK TO CONSIGN' HH,
DECODE (to_char(SYSDATE, 'DAY'),'MONDAY   ',a.user_def_type_2,
     	      'TUESDAY  ',a.user_def_type_3,
     	      'WEDNESDAY',a.user_def_type_4,
     	      'THURSDAY ',a.user_def_type_5,
     	      'FRIDAY   ',a.user_def_type_1,'N') A,
DECODE (to_char(SYSDATE, 'DAY'),'MONDAY   ',a.user_def_type_2,
     	      'TUESDAY  ',a.user_def_type_3,
     	      'WEDNESDAY',a.user_def_type_4,
     	      'THURSDAY ',a.user_def_type_5,
     	      'FRIDAY   ',a.user_def_type_1,'N') AAA,
oh.order_id BB,
oh.customer_id CC,
oh.customer_id CCC,
a.name D,
PP QQQ,
--oh.consignment GG,
--oh.status SS,
--oh.move_task_status SSS,
to_char(oh.creation_date, 'DD/MM/YYYY') DD,
to_char(oh.creation_date, 'HH24:MI:SS') DDD,
oh.order_type TT
--DECODE (oh.customer_id,'T239','S F P',
--                       'T212','S F P',
--                       'T228','S F P','Normal') NN,
--to_char(a.user_def_date_1, 'DD/MM/YYYY HH:MI') MM
from order_header oh,address a,
(select ID,OO,PP from
(select
oh.order_id  ID,
oh.order_type  TT,
sum(ol.qty_ordered) OO,
nvl(sum(ol.qty_tasked),0) PP
from order_header oh,order_line ol
where oh.client_id='TR'
and oh.order_id=ol.order_id
group by
oh.order_id,
oh.order_type))
--where OO>PP
--or TT not in ('PREALLOC-B','PREALLOC-M'))
where
--(oh.order_type not in ('REPLEN','RATIO') or oh.order_type is null)
oh.order_id=ID
and oh.status not in ('Cancelled','Hold','Shipped','Released')
and (oh.consignment is null or oh.consignment='')
and oh.client_id='TR'
and a.client_id='TR'
--and a.user_def_date_1 is null
and oh.customer_id=a.address_id
--and ((('&&2'='MONDAY') and (a.delivery_open_mon='Y') and (a.user_def_type_2 is not null))
--or (('&&2'='TUESDAY') and (a.delivery_open_tue='Y') and (a.user_def_type_3 is not null))
--or (('&&2'='WEDNESDAY') and (a.delivery_open_wed='Y') and (a.user_def_type_4 is not null))
--or (('&&2'='THURSDAY') and (a.delivery_open_thur='Y') and (a.user_def_type_5 is not null))
--or (('&&2'='FRIDAY') and (a.delivery_open_fri='Y') and (a.user_def_type_1 is not null)))
and (((to_char(SYSDATE, 'DAY')='MONDAY   ') and (a.delivery_open_mon='Y'))
or ((to_char(SYSDATE, 'DAY')='TUESDAY  ') and (a.delivery_open_tue='Y'))
or ((to_char(SYSDATE, 'DAY')='WEDNESDAY') and (a.delivery_open_wed='Y'))
or ((to_char(SYSDATE, 'DAY')='THURSDAY ') and (a.delivery_open_thur='Y'))
or ((to_char(SYSDATE, 'DAY')='FRIDAY   ') and (a.delivery_open_fri='Y')))
order by
A,
CC,
BB
/
