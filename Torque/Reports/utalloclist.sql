SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET COLSEP ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 0
SET LINESIZE 200
SET TRIMSPOOL ON
SET HEADING OFF


--spool utalloclist.csv

select upper('&&2') from DUAL;

select 'Order,Customer,Name,Qty,Picker,Carton Qty,Manifest,Courier Label' from DUAL;

select
oh.order_id,
customer_id,
name,
--sum(ol.qty_ordered) B,
sum(ol.qty_tasked) C,
' '
from order_header oh,order_line ol
where oh.client_id = 'UT'
and oh.status='Allocated'
and upper(oh.order_type) like upper('&&2%')
and oh.order_id=ol.order_id
and ol.client_id = oh.client_id
group by
oh.order_id,
customer_id,
name
order by
oh.order_id
/

--spool off

