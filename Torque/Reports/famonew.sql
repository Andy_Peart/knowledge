--Want the report to show all fully allocated mail orders 
--so any order beginning with prefix "C" 
--where quantity ordered equals quantity tasked. . . 

SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET colsep ' '


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 66
SET NEWPAGE 0
SET LINESIZE 240


column A heading 'Order Type' format A12
column AA heading 'Order' format A12
column BB heading 'Date' format A14
column CC heading 'Order|Qty' format 999999

--select 'Shop,Date,Qty' from DUAL;

break on A dup skip 1

select
DDD A,
AAA AA,
BBB BB,
OOO CC from
(select
oh.order_type  DDD,
oh.order_id  AAA,
trunc(oh.creation_date) BBB,
nvl(sum(ol.qty_ordered),0) OOO,
nvl(sum(ol.qty_tasked),0) TTT
from order_header oh, order_line ol
where oh.client_id='SB'
and oh.status='Allocated'
and oh.order_id like 'C%'
and oh.order_id=ol.order_id
and oh.order_type like '&&2%'
and UPPER(oh.Dispatch_method) like '&&3%'
group by
oh.order_type,
oh.order_id,
trunc(oh.creation_date))
where OOO=TTT
order by
A,BB,AA
/


