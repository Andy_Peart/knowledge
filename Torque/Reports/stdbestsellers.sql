SET FEEDBACK OFF                 
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','


/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 240

COLUMN A heading "Sku" FORMAT a18
COLUMN B heading "Qty" FORMAT 999999
COLUMN C heading "Description" FORMAT a40
COLUMN D heading "Prod  Group" FORMAT A18
COLUMN E heading "Alloc Group" FORMAT A10



select 'TML Best Sellers,for client &&2, for period &&3 to &&4,,,' from Dual;

select 'Sku,Qty,Description,Prod Group,Alloc Group' from DUAL;

select
''''||sm.Sku_id||'''' A,
sum(sm.Qty_shipped) B,
SKU.description C,
sku.Product_Group D,
sku.Allocation_Group E
from shipping_manifest sm,sku
where sm.client_id='&&2'
and sm.shipped_dstamp between to_date('&&3', 'DD-MON-YYYY') and to_date('&&4', 'DD-MON-YYYY')+1
and sku.client_id='&&2'
and sm.sku_id=sku.sku_id
group by
sm.Sku_id,
SKU.description,
sku.Product_Group,
sku.Allocation_Group
order by
B DESC
/

