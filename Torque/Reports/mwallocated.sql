SET FEEDBACK OFF                 
set pagesize 999
set linesize 160
set verify off

clear columns
clear breaks
clear computes

column A heading 'Shop ' format a16
column B heading 'Allocated' format 999999

set TERMOUT OFF
column curdate NEW_VALUE report_date
select to_char(SYSDATE, 'DD/MM/YY  HH:MI') curdate from DUAL;
set TERMOUT ON


ttitle  LEFT 'Units Allocated per Shop on &&2 to &&3 - for client ID MOUNTAIN as at ' report_date skip 2

break on report

compute sum label 'Totals' of B on report

select
ad.name A,
sum(it.update_qty) as B
from inventory_transaction it,address ad
where it.Dstamp between to_date('&&2', 'DD-MON-YYYY') and to_date('&&3', 'DD-MON-YYYY')+1
and it.client_id='MOUNTAIN'
and it.code='Allocate'
and it.work_group=ad.address_id (+)
group by
ad.name 
/

