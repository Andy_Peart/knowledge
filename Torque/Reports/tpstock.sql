SET FEEDBACK OFF            
SET ECHO OFF
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
SET colsep ','



/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 500
SET TRIMSPOOL ON


--spool tpstock.csv

select 'SKU,Description,Qty,Pallet,Qty/Pallet,Space per Pallet,Sku space' from DUAL;

select
A,
B,
QQQ,
C,
QP,
nvl(D,5.32),
--QP*nvl(D,5.32),
to_char(QP*nvl(D,5.32),'9999')
from (select
A,
B,
QQQ,
C,
D,
to_char((QQQ+(C/2))/C,'999') QP
from (select
it.sku_id A,
sku.description B,
sku.user_def_num_2 C,
sku.user_def_num_1 D,
sum(it.qty_on_hand) QQQ
from inventory it,sku
where it.client_id = 'TP'
and it.sku_id=sku.sku_id
and sku.client_id = 'TP'
group by it.sku_id,sku.description,sku.user_def_num_2,sku.user_def_num_1)
order by A)
/

--spool off
