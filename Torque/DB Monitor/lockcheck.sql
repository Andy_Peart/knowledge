SET SERVEROUTPUT ON
SET FEEDBACK OFF

declare
	lock_count number := 0;
begin
select count(sid)
	into
		lock_count
	from v$lock where request != 0 and ctime > 40;

	DBMS_OUTPUT.PUT_LINE(lock_count);
end;
/
