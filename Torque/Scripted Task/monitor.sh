#!/bin/sh

##################################################################################################################################################################################
# Script name: monitor.sh
# Creator: Andy Peart
# Date: 15/10/2020
# Version: 1.0
# Purpose: Automated monitoring
################################################################## Version History ###############################################################################################
# Version       Date        Initials    Project       Details
# 1.0           15/10/20    AMP         T-Internal    Initial Deployment of monitoring for Daemons and DB Locks
##################################################################################################################################################################################

# Load in the DCS profile so I can use JDA aliases

. $HOME/dcs/.dcs_profile
. $HOME/torque/.torque_profile 

#set up File Names

EXTENSION="$(date +%y%m%d%H%M%S)"
LOCKFILE="Database_Locks_${EXTENSION}.txt"
SERVER="$(uname -n)"

# Check for Database Locks

cd $TORQUE_MONITOR

sqlplus -s $DCS_SYSDBA > LOCKCHECK << EOF
@lockcheck.sql
exit
EOF


CHECKLOCK=`cat LOCKCHECK`

echo "checklock = $CHECKLOCK"
cat LOCKCHECK

#Check for Status of Run

sqlplus -s $ORACLE_USR > STATUSCHECK << EOF
SET SERVEROUTPUT ON
SET FEEDBACK OFF

declare
	key_count number;
begin
	select numeric_data
	into
		key_count
	from system_profile where profile_id = '-ROOT-_USER_MONITORING_DBLOCKS';
	
	DBMS_OUTPUT.PUT_LINE(key_count);
end;
/
exit
EOF

CHECKSTATUS=`cat STATUSCHECK`
echo "statuscheck = $CHECKSTATUS"
cat STATUSCHECK

#If there are no locks and the alert has previously been sent reset the profile

if ((CHECKLOCK == 0)); then
echo "Update Profile check lock = 0"
	if ((CHECKSTATUS != 0)); then
echo "Update System Profile to 0"		
	sqlplus -s $ORACLE_USR <<EOF

		Begin 
			Update System_profile set numeric_data = 0 where profile_id = '-ROOT-_USER_MONITORING_DBLOCKS';
			commit;
		End;
		/
		exit
EOF
	fi
fi


#echo "system Profile and lock Checks completed with checklock = $CHECKLOCK and checkstatus = $CHECKSTATUS"

if ((CHECKLOCK != 0)); then
echo " checklock greater than 0 with check status = $CHECKSTATUS"

	if ((CHECKSTATUS == 0)); then
echo "check status = 0"
echo "run locklist"
sqlplus -s $DCS_SYSDBA > $LOCKFILE << EOF
@locklist.sql
exit
EOF

sqlplus -s $ORACLE_USR <<EOF

		Begin 
			Update System_profile set numeric_data = 1 where profile_id = '-ROOT-_USER_MONITORING_DBLOCKS';
			commit;
		End;
		/
		exit
EOF

echo |cat $LOCKFILE | mailx -S smtp="mail.torque.eu" -r "noreply@torque.eu" -a $LOCKFILE -s " Database Locks Found on $SERVER : $ORACLE_TNSNAME" InterfaceError@torque.eu
echo |cat $LOCKFILE | mailx -S smtp="mail.torque.eu" -r "DBLOCKS_URGENT@torque.eu" -a $LOCKFILE -s " Database Locks Found on $SERVER : $ORACLE_TNSNAME" andrew.peart@torque.eu
rm $LOCKFILE
fi
fi
# email contents of file
#echo |cat test.txt | mailx -S smtp="mail.torque.eu" -r "noreply@torque.eu" andrew.peart@torque.eu

#echo "test checklock $CHECKLOCK checkstatus $CHECKSTATUS" | mailx -S smtp="mail.torque.eu" -r "noreply@torque.eu" andrew.peart@torque.eu
