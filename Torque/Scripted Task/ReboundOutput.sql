
DECLARE

CURSOR C_ORD_SEARCH IS
select 
OH.order_id AS DORD, 
to_char(OH.order_date,'DD/MM/YYYY') AS DORDDATE, 
OH.CONTACT AS DCONTACT, 
OH.CUSTOMER_ID AS DCOMPANY,
OH.ADDRESS1 AS DADDRESS1,
OH.ADDRESS2 AS DADDRESS2, 
OH.TOWN AS DTOWN,
OH.POSTCODE AS DPOSTCODE,
OH.COUNTY AS DCOUNTY,
CO.ISO2_ID AS DCOUNTRY,
OH.CONTACT_PHONE AS DPHONE,
OH.CONTACT_EMAIL AS DEMAIL,
OH.INV_CURRENCY AS DCURRENCY,
OH.SIGNATORY AS TRACKING,
OH.CARRIER_ID AS CARRIER,
OH.shipped_date AS SHIPPEDDATE,
ITL.SKU_ID AS SKU,
S.DESCRIPTION AS DESCRIPTION,
ITL.UPDATE_QTY AS SHIPPEDQTY,
ODL.LINE_VALUE AS VALUE,
S.EACH_WEIGHT AS WEIGHT,
S.PRODUCT_GROUP AS HSCODE,
COALESCE(ITL.USER_DEF_TYPE_1, S.CE_COO,'UNKNOWN') AS COO,
COALESCE(ITL.USER_DEF_TYPE_5,'RGR') AS BOND,
TQ.COST AS LANDED
FROM ORDER_HEADER OH
    INNER JOIN
        INVENTORY_TRANSACTION ITL ON ITL.CLIENT_ID = 'SB' AND
        OH.ORDER_ID = ITL.REFERENCE_ID AND
        ITL.CODE = 'Shipment' AND
        ITL.SITE_ID = 'CAS' AND
        ITL.DSTAMP > SYSDATE - 60 AND
        ITL.FROM_LOC_ID = 'SBWEB' 
    INNER JOIN 
        ORDER_LINE ODL ON ODL.ORDER_ID = OH.ORDER_ID AND
        ODL.CLIENT_ID = 'SB' AND
        ODL.QTY_SHIPPED IS NOT NULL AND
        ODL.LINE_ID = ITL.LINE_ID
    INNER JOIN
        SKU S ON S.SKU_ID = ODL.SKU_ID AND
        S.CLIENT_ID = 'SB'
    INNER jOIN COUNTRY CO ON
        OH.COUNTRY = CO.ISO3_ID
    INNER JOIN torque.ap_temp_landed TQ ON S.SKU_ID = TQ.SKU_ID AND ITL.SKU_ID = TQ.SKU_ID AND ODL.SKU_ID = TQ.SKU_ID
    WHERE
        OH.CLIENT_ID = 'SB' AND
        OH.SHIPPED_DATE > SYSDATE -60 AND
        OH.FROM_SITE_ID = 'CAS'
        AND OH.STATUS = 'Shipped'
        AND OH.SHIP_DOCK = 'SBWEB';
     -- and OH.order_id = 'CWEBUK02531692' ;
       
V_COUNTER NUMBER;
V_CURRENT_ORDER ORDER_HEADER.ORDER_ID%TYPE;
V_COO VARCHAR2(10);
V_COO_LENGTH NUMBER;
V_REAL_COO number;
V_SKU_COO_EXIST NUMBER;
R_ORD_SEARCH C_ORD_SEARCH%ROWTYPE;

BEGIN
	DBMS_OUTPUT.ENABLE
					  (
						  buffer_size => NULL
					  );
V_COUNTER :=0;
DBMS_OUTPUT.PUT_LINE ('"H","'||TO_CHAR(SYSDATE,'DD/MM/YYYY')||'"');
OPEN C_ORD_SEARCH;

V_CURRENT_ORDER := '1';

LOOP

FETCH C_ORD_SEARCH INTO R_ORD_SEARCH;

IF C_ORD_SEARCH%notfound THEN
			/*EXIT WHEN NO DATA FOUND*/
			EXIT;
END IF;



IF V_CURRENT_ORDER = R_ORD_SEARCH.DORD THEN

V_COO_LENGTH := LENGTH(R_ORD_SEARCH.COO);

IF V_COO_LENGTH = 2 THEN
V_COO := R_ORD_SEARCH.COO;
ELSE
select case when (select count(iso3_id) from country where ISO3_ID = R_ORD_SEARCH.COO) = 1 then 1 
                        when (select count(iso3_id) from country where ISO3_ID = R_ORD_SEARCH.COO) = 0 then 0 end into V_REAL_COO from dual;
    if V_REAL_COO = 1 then
    SELECT COALESCE(ISO2_ID,'UNKNOWN') INTO V_COO FROM COUNTRY WHERE ISO3_ID = R_ORD_SEARCH.COO;
    else 
  SELECT COUNT (ISO2_ID) INTO V_SKU_COO_EXIST  FROM COUNTRY C ,sku s where
 S.CE_COO = C.ISO3_ID AND S.SKU_ID =  R_ORD_SEARCH.SKU;  
if V_SKU_COO_EXIST = 1 THEN
SELECT
   ISO2_ID
INTO V_COO FROM COUNTRY C
INNER JOIN SKU S ON S.CE_COO = C.ISO3_ID AND S.SKU_ID =  R_ORD_SEARCH.SKU ;
else
V_COO := 'XX';
end if;
    end if;
END IF;

    DBMS_OUTPUT.PUT_LINE ('"I","'||
                                                        R_ORD_SEARCH.SKU||'","'||
                                                        R_ORD_SEARCH.DESCRIPTION||'","'||
                                                        R_ORD_SEARCH.SHIPPEDQTY||'","'||
                                                        R_ORD_SEARCH.VALUE||'","'||
                                                        R_ORD_SEARCH.WEIGHT||'","'||
                                                        'K'||'","'||
                                                        ''||'","'||
                                                        ''||'","'||
                                                        ''||'","'||
                                                        'cm'||'","'||
                                                        R_ORD_SEARCH.HSCODE||'","'||
                                                        V_COO||'","'||
                                                        ''||'","'||
                                                        'N'||'","'||
                                                        'N'||'","'||
                                                        R_ORD_SEARCH.SHIPPEDDATE||'","'||
                                                        R_ORD_SEARCH.TRACKING||'","'||
                                                        R_ORD_SEARCH.BOND||' - '||R_ORD_SEARCH.CARRIER||'","'||
                                                        ''||'","'||
                                                        R_ORD_SEARCH.TRACKING||'","'||
                                                        ''||'","'||
                                                        ''||'","'||
                                                        ''||'","'||
                                                        ''||'","'||
                                                        ''||'","'||
                                                        ''||'","'||
                                                        R_ORD_SEARCH.LANDED||'"');
    
ELSE
V_CURRENT_ORDER :=  R_ORD_SEARCH.DORD;
V_COUNTER := V_COUNTER +1;
V_COO_LENGTH := LENGTH(R_ORD_SEARCH.COO);

IF V_COO_LENGTH = 2 THEN
V_COO := R_ORD_SEARCH.COO;
ELSE
select case when (select count(iso3_id) from country where ISO3_ID = R_ORD_SEARCH.COO) = 1then 1 
                        when (select count(iso3_id) from country where ISO3_ID = R_ORD_SEARCH.COO) = 0 then 0 end into V_REAL_COO from dual;
if V_REAL_COO = 1 then
SELECT COALESCE(ISO2_ID,'UNKNOWN') INTO V_COO FROM COUNTRY WHERE ISO3_ID = R_ORD_SEARCH.COO;
else 
  SELECT COUNT (ISO2_ID) INTO V_SKU_COO_EXIST  FROM COUNTRY C ,sku s where
 S.CE_COO = C.ISO3_ID AND S.SKU_ID =  R_ORD_SEARCH.SKU;  
if V_SKU_COO_EXIST = 1 THEN
SELECT
   ISO2_ID
INTO V_COO FROM COUNTRY C
INNER JOIN SKU S ON S.CE_COO = C.ISO3_ID AND S.SKU_ID =  R_ORD_SEARCH.SKU ;
else
V_COO := 'XX';
end if;
end if;
END IF;


    DBMS_OUTPUT.PUT_LINE('"D","'||
                                                        R_ORD_SEARCH.DORD||'","'||
                                                        R_ORD_SEARCH.DORDDATE||'","'||
                                                        R_ORD_SEARCH.DCONTACT||'","'||
                                                        R_ORD_SEARCH.DCOMPANY||'","'||
                                                        R_ORD_SEARCH.DADDRESS1||'","'||
                                                        R_ORD_SEARCH.DADDRESS2||'","'||
                                                        R_ORD_SEARCH.DTOWN||'","'||
                                                        R_ORD_SEARCH.DPOSTCODE||'","'||
                                                        R_ORD_SEARCH.DCOUNTY||'","'||
                                                        R_ORD_SEARCH.DCOUNTRY||'","'||
                                                        R_ORD_SEARCH.DPHONE||'","'||
                                                        R_ORD_SEARCH.DEMAIL||'","'||
                                                        R_ORD_SEARCH.DCURRENCY||'","'||
                                                        R_ORD_SEARCH.TRACKING||'","'||
                                                        R_ORD_SEARCH.CARRIER||'","'||
                                                        ''||'","'||
                                                        ''||'","'||
                                                        R_ORD_SEARCH.SHIPPEDDATE||'","'||
                                                        'Y'||'","'||
                                                        ''||'","'||
                                                        ''||'","'||
                                                        ''||'","'||
                                                        ''||'","'||
                                                        ''||'","'||
                                                        ''||'","'||
                                                        ''||'","'||
                                                        ''||'","'||
                                                        '"');
                                                        
    DBMS_OUTPUT.PUT_LINE ('"I","'||
                                                        R_ORD_SEARCH.SKU||'","'||
                                                        R_ORD_SEARCH.DESCRIPTION||'","'||
                                                        R_ORD_SEARCH.SHIPPEDQTY||'","'||
                                                        R_ORD_SEARCH.VALUE||'","'||
                                                        R_ORD_SEARCH.WEIGHT||'","'||
                                                        'K'||'","'||
                                                        ''||'","'||
                                                        ''||'","'||
                                                        ''||'","'||
                                                        'cm'||'","'||
                                                        R_ORD_SEARCH.HSCODE||'","'||
                                                        V_COO||'","'||
                                                        ''||'","'||
                                                        'N'||'","'||
                                                        'N'||'","'||
                                                        R_ORD_SEARCH.SHIPPEDDATE||'","'||
                                                        R_ORD_SEARCH.TRACKING||'","'||
                                                        R_ORD_SEARCH.BOND||' - '||R_ORD_SEARCH.CARRIER||'","'||
                                                        ''||'","'||
                                                        R_ORD_SEARCH.TRACKING||'","'||
                                                        ''||'","'||
                                                        ''||'","'||
                                                        ''||'","'||
                                                        ''||'","'||
                                                        ''||'","'||
                                                        ''||'","'||
                                                        R_ORD_SEARCH.LANDED||'"');
                                                        
END IF;
END LOOP;
CLOSE C_ORD_SEARCH;
DBMS_OUTPUT.PUT_LINE ('"T","'||V_COUNTER||'"');
END;
/