#!/bin/sh

##################################################################################################################################################################################
# Script name: sku_convert.sh
# Creator: Andy Peart
# Date: 27/10/2020
# Version: 1.0
# Purpose: Automatic sku Conversion script
# ***************************************CHANGES**********************************************************************************************************************************
# v1.1 Andy Peart changes made as the move to update all sku's on the website
##################################################################################################################################################################################

. $HOME/dcs/.dcs_profile

PROG=$(basename $0)
DATETIMEFILE=`date +%Y%m%d%H%M%S`
DATEFILE=`date +%Y%m%d`
LOGFILE=$DCS_PACKAGELOGDIR/$PROG"_"$DATEFILE.log

sqlplus -s $ORACLE_USR  << ! >| $LOGFILE
SET SERVEROUTPUT ON;

DECLARE
/*this is used to be able to get a run sku_conversion procedure */
RETURN number := 0;

--------------------------------------------------------------------------------------

cursor inv_search
is
select key,sku_id,tag_id from inventory
  where sku_id in (select old_sku from torque.ap_sku_conversion where old_sku != new_sku)
  and client_id = 'SB'
  and location_id != 'SUSPENSE'
  --and qty_allocated > 0
  and location_id not in (select distinct(ship_dock) from order_header where client_id = 'SB');

--------------------------------------------------------------------------------------

cursor sku_search
is
select new_sku,old_sku,ean from torque.ap_sku_conversion;

--------------------------------------------------------------------------------------

cursor sm_search
is
select key,sku_id from shipping_manifest where sku_id in
	(select old_sku from torque.ap_sku_conversion where old_sku != new_sku)
	and shipped = 'Y';

--------------------------------------------------------------------------------------

cursor sku_update_search is
	select new_sku,old_sku,ean from torque.ap_sku_conversion where old_sku != new_sku;

--------------------------------------------------------------------------------------

inv_row inv_search%rowtype;
sku_row sku_search%rowtype;
sm_row  sm_search%rowtype;
sku_upd_row sku_update_search%rowtype;

--------------------------------------------------------------------------------------

new_sku1 torque.ap_sku_conversion.new_sku%type;

v_itl_key number(10);
v_tag_id  varchar2(20);
v_new_sku varchar2(20);

-------------------------------SKU CONVERT---------------------------------------------

begin

open inv_search;

dcsdba.libsession.initialisesession(
UserID          =>'AUTOMATIC',
GroupID         =>'AUTOMATIC',
StationID       =>'AUTOMATIC',
WksGroupID      =>'AUTOMATIC'
);

LOOP
	fetch inv_search into inv_row;
	IF inv_search%notfound THEN
		exit;
	END IF;
	
	select new_sku into new_sku1 from torque.ap_sku_conversion sc where inv_row.sku_id = sc.old_sku;
	
	dcsdba.libskuconversion.CONVERTSKU( RESULT => RETURN ,KEYFIELD => inv_row.key ,SKUID => new_sku1,UPDATENOTES => 'AUTOMATIC', TAGID => inv_row.tag_id);
	DBMS_OUTPUT.PUT_LINE ('Tag '||inv_row.tag_id||' Converted from '||inv_row.sku_id||' To '||new_sku1);
	commit;
	
	select nvl(max(key),1) 
	into v_itl_key 
	from inventory_transaction 
	where code = 'Adjustment'
	and client_id = 'SB'
	and original_qty = 0
	and update_qty != 0
	and sku_id = new_sku1;

	update inventory_transaction set user_def_type_5 = null where key = v_itl_key and user_def_type_5 is not null;
	Commit;
	
end loop; 

close inv_search;

------------------------------Update EAN's---------------------------------------------

open sku_search;

LOOP

	fetch sku_search into sku_row;
	IF sku_search%notfound THEN
		EXIT;
	END IF;
	
	update sku set ean = sku_row.ean where sku_id = sku_row.new_sku and client_id = 'SB';
	DBMS_OUTPUT.PUT_LINE('EAN added to SKU '||sku_row.new_sku);
	commit;
	
	
END LOOP;

close sku_search;

----------------------------------Update Shipping Manifest----------------------------------

open sm_search;

LOOP

	fetch sm_search into sm_row;
	IF sm_search%notfound THEN
		EXIT;
	END IF;
	
	select new_sku into v_new_sku
	from torque.ap_sku_conversion
	where old_sku = sm_row.sku_id;
	
	update shipping_manifest set sku_id = v_new_sku where key = sm_row.key ;
	DBMS_OUTPUT.PUT_LINE('SMKEY : '||sm_row.key|| ' Updated to SKU : '||v_new_sku);
	commit;
END LOOP;

close sm_search;

--------------------------------Make SKU's Obsolete---------------------------------------

open sku_update_search;

LOOP

	fetch sku_update_search into sku_upd_row;
	IF sku_update_search%notfound THEN
		EXIT;
	END IF;
	
	
	update SKU set obsolete_product = 'Y', ean = null, sku_id = 'X'||sku_upd_row.old_sku 
		where sku_id = sku_upd_row.old_sku;
	
	DBMS_OUTPUT.PUT_LINE ('SKU : '||sku_upd_row.old_sku ||' Updated to X'||sku_upd_row.old_sku||' and marked Obsolete');
	
	commit;
	
END LOOP;

close sku_update_search;

commit;

end;
/
!

echo ""
echo "All Updates Done"
echo ""
echo "Log file for this transaction can be found at $LOGFILE"
echo ""
echo ""
echo ""

