#!/bin/sh
while [ 1 ]
do
echo Running $0 : `date`
sqlplus $ORACLE_USR<<! >/dev/null 2>/dev/null
declare

l_pallet inventory.pallet_id%type;
l_container inventory.container_id%type;

cursor missing_pallet_header is
select distinct i.pallet_id, i.container_id, i.site_id, i.client_id
   from inventory i
  where i.location_id = 'QCDECANT'
  and not exists (select 1
  from move_task  mt
   where mt.pallet_id = i.pallet_id
and mt.task_type = 'T'
and MT.TASK_ID = 'PALLET')
and exists (select 1
 from move_task  mt2
   where mt2.pallet_id = i.pallet_id
and mt2.task_type = 'O'
and mt2.sku_id = i.sku_id);

cursor pallet_details is
select *
  from move_task
where pallet_id = l_pallet
and container_id = l_container
and rownum = 1;
l_consol_link move_task.consol_link%type;
l_taskrow pallet_details%rowtype; 
l_result integer;
begin
 for c1 in missing_pallet_header
 loop
    l_pallet := c1.pallet_id;
    l_container := c1.container_id;
   
    open pallet_details;
    fetch pallet_details into l_taskrow;
    close pallet_details;

     l_consol_link := LibConsAllocate.GetNextConsolLink;

     l_result := LibMoveTask.CreateMoveTask
                                        (
                                        LibMoveTaskID.GetMarshalTaskID(),
                                        NULL,           /* Line ID */
                                        l_taskrow.client_id, /* Client ID */
                                        LibMoveTaskID.GetMarshalTaskID(),       /* SKU ID */
                                        NULL,           /* Tag ID */
                                        l_taskrow.from_loc_id,     /* From Location */
                                        l_taskrow.final_loc_id,  /* Final Location */
                                        'Released',
                                        1,              /* Qty To Move */
                                        NULL,           /* Config ID */
                                        'Single container pallet',
                                        l_taskrow.work_group,  /* Work Group */
                                        l_taskrow.consignment, /* Consignment */
                                        'T',
                                        l_taskrow.container_id,
                                        l_taskrow.pallet_id,
                                        l_consol_link,
                                        NULL,           /* Priority */
                                        NULL,           /* Pick Face Key */
                                        NULL,           /* Allocation */
                                        l_taskrow.site_id,
                                        NULL,           /* Condition */
                                        NULL,           /* First Key */
                                        NULL,           /* Pallet Grouped */
                                        NULL,           /* Pallet Volume */
                                        NULL,           /* Owner ID */
                                        NULL,           /* Kit SKU ID */
                                        NULL,           /* Kit Line ID */
                                        NULL,           /* Kit Link */
                                        NULL,           /* Kit Ratio */
                                        NULL,           /* Origin ID */
                                        NULL,           /* Original Dstamp */
                                        NULL,             /* Pallet Config */
                                        NULL,           /* Customer ID */
                                        NULL,           /* Due Task */
                                        NULL,           /* Due Line */
                                        NULL,           /* Due Type */
                                        NULL,           /* Repack */
                                        NULL,           /* BOL ID */
                                        NULL,           /* PickFaceAutoEx */
                                        NULL,           /* PrintLabel */
                                        NULL,           /* OldTaskID */
                                        NULL,           /* RepackQCDone */
                                        NULL,           /* CatchWeight */
                                        NULL,           /* UsePickToGrid */
                                        NULL,           /* PickReallocFlag */
                                        NULL,
                                        NULL,
                                        NULL,           /* Labelling */
                                        CERotationID => null,
                                        CEAvailStatus => null,
                                        CEUnderBond => null,
                                        KitPlanID => null,
                                        ShipmentGroup => null
                                        );
   update move_task
   set to_loc_id = l_taskrow.to_loc_id, old_to_loc_id = l_taskrow.to_loc_id
   where key = l_result;

   update move_task
   set consol_link = l_consol_link
   where task_type <> 'T'
    and pallet_id = l_taskrow.pallet_id
    and container_id = l_taskrow.container_id;
 end loop;
 commit;
end;
/
!
sleep 60
done
