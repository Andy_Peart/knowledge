/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics and Industrial Systems Ltd*/
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1997             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/
 
/******************************************************************************/
/*                                                                            */
/*                                 LIS                                        */
/*                                                                            */
/*     FILE NAME  :     invtransbyuser.sql                                    */
/*                                                                            */
/*     DESCRIPTION:     List inventory transactions by user, by date range.   */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   31/10/95 RMW  N/A    N/A      initial version                            */
/*   07/08/97 MJT  N/A    N/A      Changed number format to display 0         */
/*   06/02/98 JH   DCS    PDR5915  Reports need to be multi-sited             */
/*   27/07/98 MJT  DCS    NEW3188  Third Party Warehousing                    */
/*   16/06/99 MJT  DCS    PDR7593  Some sql*plus reports are wrapping         */
/*   26/06/01 MJT  DCS    NEW5486  Client ID added                            */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   06/07/05 JH   DCS    PDR9627  Report problems                            */
/*   13/09/05 JRL  DCS    PDR9861  Zero exception picking problem	      */
/*   02/02/07 JH   DCS    DSP1547  Consolidated pick counts                   */
/******************************************************************************/
 
/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF 
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132
 
/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Productivity Measurement - Total Tasks for All Users' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2 - 
LEFT 'Date Range From &5 To &6' SKIP 3

/* Column Headings and Formats */
COLUMN Site_Client HEADING 'Site/|Client/|Owner' FORMAT A10 WORD_WRAP
COLUMN User_ID HEADING 'User' FORMAT A20  
COLUMN Receipt HEADING 'Receipt' FORMAT '999G999G990'
COLUMN Putaway HEADING 'Putaway' FORMAT '999G999G990'
COLUMN Pick HEADING 'Pick' FORMAT '999G999G990'
COLUMN Replenish HEADING 'Replenish' FORMAT '999G999G990'
COLUMN Relocate HEADING 'Relocate' FORMAT '999G999G990'
COLUMN Stock_Check HEADING 'Stock Check' FORMAT '999G999G990'
COLUMN Total HEADING 'TOTAL' FORMAT '999G999G990'

-- BREAK ON Site_Client SKIP PAGE ON Owner_ID SKIP PAGE ON User_ID SKIP 1 ON REPORT
BREAK ON Site_Client SKIP PAGE ON Owner_ID SKIP PAGE ON User_ID SKIP 1 ON REPORT

COMPUTE SUM OF Receipt Putaway Pick Replenish Relocate Stock_Check Total ON REPORT

/* SQL Select statement */
SELECT   RPAD(Site_ID, 10) || RPAD(Client_ID, 10) || Owner_ID Site_Client,
	 User_ID,
	 SUM (DECODE (Code, 'Receipt', 1, 0)) Receipt,
	 SUM (DECODE (Code, 'Putaway', 1, 0)) Putaway,
	 SUM (DECODE (Code, 'Pick', 1, 'Consol', 1, 0)) Pick,
	 SUM (DECODE (Code, 'Replenish',1, 0)) Replenish,
	 SUM (DECODE (Code, 'Relocate',1, 0)) Relocate,
	 SUM (DECODE (Code, 'Stock Check', 1, 0)) Stock_Check,
	 SUM (DECODE (Code, 'Receipt', 1,
			    'Putaway', 1,
			    'Pick', 1, 'Consol', 1, 
			    'Replenish', 1,
			    'Relocate', 1,
			    'Stock Check', 1, 0)) Total
FROM	 Inventory_Transaction
WHERE	 DStamp >= TO_DATE('&5', 'DD-MON-YYYY') 
AND 	DStamp < TO_DATE('&6', 'DD-MON-YYYY') + 1
AND (('&2' IS NULL) OR (Site_ID = '&2'))
AND (('&4' IS NULL) OR (Owner_ID = '&4'))
AND ((('&3' IS NULL) AND ('&7' IS NULL))
OR  (Client_ID = '&3')
OR  (('&3' IS NULL) AND (Client_ID IS NULL OR Client_ID IN
	(SELECT Client_ID 
	 FROM Client_Group_Clients
	 WHERE Client_Group = '&7'))))  
AND Summary_Record = 'Y'
AND ((Code = 'Pick' and Update_Qty > 0) 
OR (Code = 'Consol' and Update_Qty > 0) 
OR (Code <> 'Pick' and Code <> 'Consol'))
GROUP BY Site_ID, Client_ID, Owner_ID, User_ID
ORDER BY Site_ID, Client_ID, Owner_ID, User_ID
; 

SET TERMOUT ON










