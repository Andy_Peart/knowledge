/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics & Industrial Systems Ltd  */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1995             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/
 
/******************************************************************************/
/*                                                                            */
/*                                 LIS                                        */
/*                                                                            */
/*     FILE NAME  :     skubysku.sql                                          */
/*                                                                            */
/*     DESCRIPTION:     List the SKUs by entered SKU range.                   */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   08/02/95 RMW  DCS    N/A      initial version                            */
/*   25/04/97 MJT  DCS    PDR5924  Modified to handle european group & decimal*/
/*                                 separators.                                */
/*   16/06/99 MJT  DCS    PDR7593  Some sql*plus reports are wrapping         */
/*   28/06/01 MJT  DCS    NEW5486  Client ID added                            */
/*   03/09/01 PHH  DCS    NEW5629  Pack Config Clienting                      */
/*   16/09/02 AJR  DCS    NEW6279  Eight tracking levels on pack config       */
/*   27/08/03 DL   DCS    PDR9074  Various reporting issues                   */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   25/06/04 RMC  DCS    PDR9092  Change reports for VAS and beam 	      */
/******************************************************************************/
 
/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132
 
/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'SKUs By SKU Range' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2 - 
LEFT 'SKU Range From &3 To &4' SKIP 2
 
/* Column Headings and Formats */
COLUMN SKU_ID NOPRINT
COLUMN SKU_Description HEADING 'SKU/|Description' FORMAT A30 WORD_WRAP
COLUMN Config_Client HEADING 'Pack Config/|Client' FORMAT A15 WRAP
COLUMN Track_Level_1 HEADING 'Tracking|Level 1' FORMAT A8
COLUMN Rat_1_To_2 HEADING 'Ratio|1 to 2' FORMAT '999G999D999999'
COLUMN Track_Level_2 HEADING 'Tracking|Level 2' FORMAT A8
COLUMN Rat_2_To_3 HEADING 'Ratio|2 to 3' FORMAT '999G999'
COLUMN Track_Level_3 HEADING 'Tracking|Level 3' FORMAT A8
COLUMN Rat_3_To_4 HEADING 'Ratio|3 to 4' FORMAT '999G999'
COLUMN Track_Level_4 HEADING 'Tracking|Level 4' FORMAT A8
COLUMN Rat_4_To_5 HEADING 'Ratio|4 to 5' FORMAT '999G999'
COLUMN Track_Level_5 HEADING 'Tracking|Level 5' FORMAT A8
COLUMN Rat_5_To_6 HEADING 'Ratio|5 to 6' FORMAT '999G999'
COLUMN Track_Level_6 HEADING 'Tracking|Level 6' FORMAT A8
COLUMN Rat_6_To_7 HEADING 'Ratio|6 to 7' FORMAT '999G999'
COLUMN Track_Level_7 HEADING 'Tracking|Level 7' FORMAT A8
COLUMN Rat_7_To_8 HEADING 'Ratio|7 to 8' FORMAT '999G999'
COLUMN Track_Level_8 HEADING 'Tracking|Level 8' FORMAT A8
COLUMN Tag_Vol HEADING 'Tag|Volume' FORMAT '9G999G999D9999'
COLUMN Spec_ID HEADING 'Specification Code' FORMAT A10

BREAK ON Client_ID ON SKU_Description SKIP 1
 
/* SQL Select statement */
SELECT	A.SKU_ID,
	RPAD (A.SKU_ID, 30) || A.Description SKU_Description,
	RPAD (B.Config_ID, 15) || A.Client_ID Config_Client,
	C.Track_Level_1,
	TO_CHAR(C.Ratio_1_To_2,'999G999D999999') Rat_1_To_2,
	C.Track_Level_2,
	TO_CHAR(C.Ratio_2_To_3,'999G999') Rat_2_To_3,
	C.Track_Level_3,
	TO_CHAR(C.Ratio_3_To_4,'999G999') Rat_3_To_4,
	C.Track_Level_4,
	TO_CHAR(C.Ratio_4_To_5,'999G999') Rat_4_To_5,
	C.Track_Level_5,
	TO_CHAR(C.Ratio_5_To_6,'999G999') Rat_5_To_6,
	C.Track_Level_6,
	TO_CHAR(C.Ratio_6_To_7,'999G999') Rat_6_To_7,
	C.Track_Level_7,
	TO_CHAR(C.Ratio_7_To_8,'999G999') Rat_7_To_8,
	C.Track_Level_8,
	TO_CHAR(C.Tag_Volume,'9G999G999D9999') Tag_Vol,
	Spec_ID
FROM SKU A, SKU_SKU_Config B, SKU_Config C
WHERE A.SKU_ID = B.SKU_ID
AND A.Client_ID = B.Client_ID 
AND B.Config_ID = C.Config_ID 
AND A.SKU_ID BETWEEN '&3' AND '&4'
AND ((('&2' IS NULL) AND ('&5' IS NULL))
OR  (A.Client_ID = '&2')
OR  (('&2' IS NULL) AND A.Client_ID IN
(SELECT Client_ID 
FROM Client_Group_Clients
WHERE Client_Group = '&5'))  
AND C.rowid = LibConfig.GetConfigRowid (B.Config_ID, '&2'))
ORDER BY A.Client_ID, A.SKU_ID
; 
 
SET TERMOUT ON
 
