/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics and Industrial Systems Ltd*/
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1998             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/
 
/******************************************************************************/
/*                                                                            */
/*                                 LIS                                        */
/*                                                                            */
/*     FILE NAME  :     invpartpallet.sql                                     */
/*                                                                            */
/*     DESCRIPTION:     List all part pallets in the inventory table.         */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   27/09/98 RMW  DCS    NEW3456  Origin Functionality                       */
/*   18/04/99 JH   DCS    NEW3920  Extra/wider database columns		      */
/*   15/06/99 MJT  DCS    PDR7593  Some sql*plus reports are wrapping         */
/*   26/06/01 MJT  DCS    NEW5486  Client ID added                            */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   04/08/05 DM   DCS    NEW7858  Extend SKU to 30 characters                */
/******************************************************************************/
 
/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132

/* Top Title */
TTITLE CENTER _SITENAME SKIP 2 - 
LEFT 'Part Pallet Inventory' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -                               
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2

/* Avoid duplicate SKUs */
BREAK ON Site_Client ON Owner_Location SKIP PAGE ON SKU_Description

/* Column Headings and Formats */
COLUMN Site_Client HEADING 'Site/|Client' FORMAT A10 WORD_WRAP
COLUMN Owner_Location HEADING 'Owner/|Location' FORMAT A10 WORD_WRAP
COLUMN SKU_Description HEADING 'SKU/Description' FORMAT A30 WRAP
COLUMN Tag_Quantity HEADING 'Tag/|Qty On Hand' FORMAT A20 WRAP
COLUMN Origin_Condition HEADING 'Origin/|Condition' FORMAT A10 WORD_WRAP
COLUMN Batch_Expiry HEADING 'Batch/|Expiry Date' FORMAT A15 WRAP
COLUMN Container_Pallet_ID HEADING 'Container/|Pallet' FORMAT A20 WRAP
COLUMN Count_Lock_Code HEADING 'Count|Needed/|Lock Code' FORMAT A10 WRAP
 
/* SQL Select statement */
SELECT	RPAD (I.Site_ID, 10) || I.Client_ID Site_Client, 
	RPAD (I.Owner_ID, 10) || I.Location_ID Owner_Location,
	RPAD (I.SKU_ID, 30) || I.Description SKU_Description,
	RPAD (NVL(I.Tag_ID, ' '), 20) || 
	LPAD(TO_CHAR(I.Qty_On_Hand,'999G999G990D999999'), 20) Tag_Quantity,
	RPAD (Origin_ID, 10) || Condition_ID Origin_Condition,
	RPAD (NVL (I.Container_ID, ' '), 20) || I.Pallet_ID Container_Pallet_ID,
	RPAD (NVL (I.Batch_ID, ' '), 15) || TO_CHAR (I.Expiry_DStamp, 'DD-MON-YYYY') Batch_Expiry,
	RPAD (NVL (I.Count_Needed, 'N'), 10) || I.Lock_Code Count_Lock_Code
FROM Inventory I, Location L
WHERE I.Location_ID = L.Location_ID
AND NVL (I.Full_Pallet, 'N') = 'N'
AND L.Loc_Type <> 'Suspense'
AND ((I.Site_ID IS NULL AND L.Site_ID IS NULL) OR (I.Site_ID = L.Site_ID))
AND (('&2' IS NULL) OR (I.Site_ID = '&2'))
AND (('&4' IS NULL) OR (I.Owner_ID = '&4'))
AND ((('&3' IS NULL) AND ('&5' IS NULL))
OR  (I.Client_ID = '&3')
OR  (('&3' IS NULL) AND I.Client_ID IN
	(SELECT Client_ID 
	 FROM Client_Group_Clients
	 WHERE Client_Group = '&5')))  
ORDER BY I.Site_ID, I.Client_ID, I.Owner_ID, I.SKU_ID, I.Location_ID;
 
SET TERMOUT ON
 
