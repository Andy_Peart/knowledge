/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics and Industrial Systems Ltd*/
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1995             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*                                 LIS                                        */
/*                                                                            */
/*     FILE NAME  :     sysoption.sql                                         */
/*                                                                            */
/*     DESCRIPTION:     Dump the System Options table to report.              */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   15/02/95 RTE  DCS    N/A      initial version                            */
/*   16/06/99 MJT  DCS    PDR7593  Some sql*plus reports are wrapping         */
/*   25/01/00 LPE  DCS    NEW4632  Added Trailer report path                  */
/*   25/01/00 LPE  DCS    NEW4629  Added Pallet contents path                 */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   10/02/05 RMC  DCS    PDR9507  Miscellaneous Issues 843 Part 5 	      */
/******************************************************************************/

/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132

/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'System Options' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 3 -
LEFT 'Print Commands' SKIP 1 -
LEFT '==============' SKIP 2 -
LEFT 'Tags To        : ' cTagCommand SKIP -
LEFT 'Reports To     : ' cRptCommand SKIP -
LEFT 'Labels To      : ' cLabelCommand SKIP -
LEFT 'Pallet Lab To  : ' cPalletCommand SKIP -
LEFT 'Trailer Mani   : ' cTrailCommand SKIP -
LEFT 'Pall Cont List : ' cPalConCommand SKIP -
LEFT 'Marshal List To: ' cMarshalCommand SKIP 1

/* Column Headings and Formats */
COLUMN Order_Cancel_Ship_Days NEW_VALUE nOrderCancelShipDays NOPRINT
COLUMN Tag_Command NEW_VALUE cTagCommand NOPRINT
COLUMN Rpt_Command NEW_VALUE cRptCommand NOPRINT
COLUMN Label_Command NEW_VALUE cLabelCommand NOPRINT
COLUMN Pallet_Command NEW_VALUE cPalletCommand NOPRINT
COLUMN Trail_Command  NEW_VALUE cTrailCommand NOPRINT
COLUMN Pallist_Command NEW_VALUE cPalConCommand NOPRINT
COLUMN Marshal_Command NEW_VALUE cMarshalCommand NOPRINT

/* SQL Select statement */
SELECT	
	Order_Cancel_Ship_Days,
	Tag_Command,
	Rpt_Command,
	Label_Command,
	Pallet_Command,
	Trail_Command,
	Pallist_Command,
	Marshal_Command
FROM System_Options
;

SET TERMOUT ON

