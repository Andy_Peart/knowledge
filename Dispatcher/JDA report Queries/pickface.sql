/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics and Industrial Systems Ltd*/
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1999             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/
 
/******************************************************************************/
/*                                                                            */
/*                                  LIS                                       */
/*                                                                            */
/*     FILE NAME  :     pickface.sql                                          */
/*                                                                            */
/*     DESCRIPTION:     List all the pick faces (by site and/or owner)        */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   02/11/99 RW   dcs530 NEW4574  New Pick Face Report			      */
/*   27/06/01 MJT  DCS    NEW5486  Client ID added                            */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   04/08/05 DM   DCS    NEW7858  Increase SKU to 30 characters              */
/******************************************************************************/
 
/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132

/* Top Title */
TTITLE CENTER _SITENAME SKIP 2 - 
LEFT 'Pick Faces' RIGHT _DATESTAMP SKIP 1 -        
RIGHT _TIMESTAMP SKIP 1 -                               
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2

/* Avoid duplicate SKUs */
BREAK ON Site_Client SKIP PAGE ON sDescription SKIP 1 ON FaceType 

/* Column Headings and Formats */
COLUMN Site_Client HEADING 'Site/|Client' FORMAT A10
COLUMN Owner_Type HEADING 'Owner/|Type' FORMAT A10 WORD_WRAP
COLUMN sDescription HEADING 'SKU/|Description' FORMAT A20 WRAP
COLUMN OriginCond HEADING 'Origin/|Condition' FORMAT A10 WRAP
COLUMN LocationZone HEADING 'Location/|Zone' FORMAT A10 WRAP
COLUMN PFQuantities HEADING 'Trigger Qty/|Min Repl Qty/|Max Repl Task Qty' FORMAT A19
COLUMN Auto_Replen HEADING 'Replen|Without|Order ?' FORMAT A7 WRAP
COLUMN Max_Reld_Replen HEADING 'Maximum|Released|Replen' FORMAT A10
COLUMN Replen_Whole HEADING 'Round|To Free|Qty ?' FORMAT A7 WRAP
COLUMN PFValues HEADING 'Qty On Hand/|Qty Due/|Qty Allocated' FORMAT A19
 
/* SQL Select statement */
SELECT	RPAD (PF.Site_ID, 10) || PF.Client_ID Site_Client,
	RPAD (PF.Owner_ID, 10) || 
	DECODE (PF.Face_Type, 'F', 'Fixed', 'D', 'Dynamic') Owner_Type,
	RPAD (PF.SKU_ID, TRUNC ((LENGTH (PF.SKU_ID) - 1) / 20) * 20 + 20) || 
	S.Description sDescription,
	RPAD (PF.Origin_ID, 10) || PF.Condition_ID OriginCond,
	RPAD (PF.Location_ID, 10) || PF.Zone_1 LocationZone,
	LPAD (TO_CHAR (Trigger_Qty,'999G999G990D999999'), 19) ||
	LPAD (TO_CHAR (Min_Replen_Qty,'999G999G990D999999'), 19) ||
        TO_CHAR (Max_Task_Qty,'999G999G990D999999') PFQuantities,
        LPAD (TO_CHAR (Max_Reld_Replen,'9G999G999'), 10) Max_Reld_Replen,
	Auto_Replen,
	Replen_Whole,
	LPAD (TO_CHAR (Qty_On_Hand,'999G999G990D999999'), 19) ||
	LPAD (TO_CHAR (Qty_Due,'999G999G990D999999'), 19) ||
        TO_CHAR (Qty_Allocated,'999G999G990D999999') PFValues
FROM SKU S, Pick_Face PF
WHERE S.SKU_ID = PF.SKU_ID
AND S.Client_ID = PF.Client_ID
AND (('&2' IS NULL) OR (PF.Site_ID = '&2'))
AND (('&4' IS NULL) OR (PF.Owner_ID = '&4'))
AND ((('&3' IS NULL) AND ('&5' IS NULL))
OR  (PF.Client_ID = '&3')
OR  (('&3' IS NULL) AND PF.Client_ID IN
(SELECT Client_ID 
FROM Client_Group_Clients
WHERE Client_Group = '&5')))  
ORDER BY PF.Site_ID, PF.Client_ID, PF.Owner_ID, PF.SKU_ID, PF.Face_Type; 
 
SET TERMOUT ON
 
