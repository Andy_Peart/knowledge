/******************************************************************************/
/*                                                                            */
/*   Logistics and Internet Systems Ltd                                       */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics & Internet Systems Ltd    */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Internet   */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Internet Systems Limited, 2003               */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*                                  LIS                                       */
/*                                                                            */
/*     FILE NAME  :     lockedinvpf.sql                                       */
/*                                                                            */
/*     DESCRIPTION:     List all the locked inventory in the pick faces.      */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   20/08/03 RMW  DCS    NEW6806  Additional reports from projects           */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   04/08/05 DM   DCS    NEW7858  Extend SKU to 30 characters                */
/******************************************************************************/

/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132


/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Locked Inventory in Pick Face Report' -
RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2

/* Column Headings and Formats */
COLUMN Site_Client HEADING 'Site/|Client' FORMAT A10
COLUMN SKU_ID HEADING 'SKU' FORMAT A30
COLUMN LOCATION_ID HEADING 'Location' FORMAT A10
COLUMN QTY_ON_HAND HEADING 'Quantity' FORMAT '999G999G990D999999'
COLUMN BATCH_ID HEADING 'Batch ID' FORMAT A15
COLUMN EXPIRY_DSTAMP HEADING 'Expiry Date' FORMAT A12 WRAP
COLUMN LOCK_STATUS HEADING 'Lock Status' FORMAT A12
COLUMN LOCK_CODE HEADING 'Lock Code' FORMAT A10

BREAK ON Site_ID SKIP PAGE ON Client_ID SKIP 1

/* SQL Select statement */
SELECT	RPAD (I.Site_ID, 10) || I.Client_ID Site_Client,
	I.SKU_ID,
	I.Location_ID,
	I.Qty_On_Hand,
	I.Batch_ID,
	I.Expiry_DStamp,
	I.Lock_Status,
	I.Lock_Code
FROM Inventory I
WHERE I.Lock_Status = 'Locked'
AND (I.Site_ID, I.Location_ID) IN (SELECT PF.Site_ID, PF.Location_ID
			       FROM Pick_Face PF
			       WHERE (PF.Site_ID = '&2' OR '&2' IS NULL)
			       AND (PF.Client_ID = '&3' OR '&3' IS NULL))
AND (I.Site_ID = '&2' OR '&2' IS NULL)
AND (I.Client_ID = '&3' OR '&3' IS NULL)
ORDER BY Site_ID, Client_ID, SKU_ID, Location_ID;

SET TERMOUT ON
