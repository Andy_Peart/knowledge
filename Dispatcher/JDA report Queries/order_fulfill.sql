/******************************************************************************/
/*                                                                            */
/*   Logistics and Internet Systems Ltd                                       */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics & Internet Systems Ltd    */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Internet   */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Internet Systems Limited, 2003               */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*                                  LIS                                       */
/*                                                                            */
/*     FILE NAME  :     order_fulfill.sql                                     */
/*                                                                            */
/*     DESCRIPTION:     Summary of orders for a given ship date showing       */
/*                      Items ordered, picked, shipped and delivered as       */
/*			a qty and %                                           */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   20/08/03 RMW  DCS    NEW6806  Additional reports from projects           */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   19/10/06 JH   DCS    ENH3120  Increase cons/wg to 20 chars               */
/******************************************************************************/

/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132


BREAK ON Site_ID SKIP PAGE ON Client_ID SKIP 1

COLUMN Site_Client_Owner HEADING 'Site/|Client/|Owner' FORMAT A10 WORD_WRAP
COLUMN WG_Consign HEADING 'Work Group/|Consignment/|Order' FORMAT A20 WORD_WRAP
COLUMN G HEADING 'Lines'
COLUMN H HEADING 'Qty|Ordered'
COLUMN I HEADING 'Qty|Picked'
COLUMN J HEADING '% Picked'
COLUMN K HEADING 'Qty|Shipped'
COLUMN L HEADING '% Shipped'
COLUMN M HEADING 'Qty|Delivered'
COLUMN N HEADING '% Delivered'

BREAK ON REPORT

COMPUTE SUM AVG OF G ON REPORT
COMPUTE SUM AVG OF H ON REPORT
COMPUTE SUM AVG OF I ON REPORT
COMPUTE AVG OF J ON REPORT
COMPUTE SUM AVG OF K ON REPORT
COMPUTE AVG OF L ON REPORT
COMPUTE SUM AVG OF M ON REPORT
COMPUTE AVG OF N ON REPORT

/* Top Title */
TTITLE CENTER _SITENAME SKIP 2 -
LEFT 'Order Fullfilment for &5' -
RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2


SELECT	RPAD (OH.From_Site_ID, 10) || RPAD (OH.Client_ID, 10) || OH.Owner_ID Site_Client_Owner,
	RPAD (OH.Work_Group, 20) || RPAD(OH.Consignment, 20) || OH.Order_ID WG_Consign,
	COUNT (Line_ID) G,
	SUM (NVL (Qty_Ordered, 0)) H,
	SUM (NVL (Qty_Picked, 0)) I,
	ROUND ((SUM (NVL(Qty_Picked, 0))) / (SUM (NVL (Qty_Ordered, 0)))*100,2) J,
	SUM (NVL(qty_shipped,0)) K,
	ROUND ((SUM (NVL(Qty_Shipped, 0))) / (SUM (NVL (Qty_Ordered, 0)))*100,2) L,
	SUM (NVL(Qty_Delivered,0)) M,
	ROUND ((SUM (NVL(Qty_Delivered, 0))) / (SUM (NVL (Qty_Ordered, 0)))*100,2) N
FROM Order_Header OH, Order_Line OL
WHERE OH.Order_ID = OL.Order_ID
AND OH.Client_ID = OL.Client_ID
AND OH.Status IN ('Shipped', 'Delivered')
AND (('&2' IS NULL) OR (OH.From_Site_ID = '&2'))
AND (('&3' IS NULL) OR (OH.Client_ID = '&3'))
AND (('&4' IS NULL) OR (OH.Owner_ID = '&4'))
AND TO_CHAR (OH.Shipped_Date, 'DD-MON-YYYY') = UPPER ('&5')
GROUP BY OH.From_Site_ID,
	 OH.Owner_ID,
	 OH.Client_ID,
	 OH.Work_Group,
	 OH.Consignment,
	 OH.Order_ID;

SET TERMOUT ON
