/******************************************************************************/
/*                                                                            */
/*   Logistics & Industrial Systems Ltd                                       */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics & Industrial Systems Ltd  */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics & Industrial   */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics & Industrial Systems Limited, 1995               */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*                                 LIS                                        */
/*                                                                            */
/*     FILE NAME  :     invsumbyskudate.sql                                   */
/*                                                                            */
/*     DESCRIPTION:     List inventory by SKU with a received date between    */
/*                      the two dates passed.                                 */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   02/01/95 RMW  1580   N/A      initial version                            */
/*   7/08/97  MJT  N/A    N/A      Fixed problem with nulls in condition ID   */
/*   06/02/98 JH   DCS    PDR5915  Reports need to be multi-sited             */
/*   27/07/98 MJT  DCS    NEW3188  Third Party Warehousing                    */
/*   16/06/99 MJT  DCS    PDR7593  Some sql*plus reports are wrapping         */
/*   26/06/01 MJT  DCS    NEW5486  Client ID added                            */
/*   03/09/01 PHH  DCS    NEW5629  Pack Config Clienting                      */
/*   16/09/02 AJR  DCS    NEW6279  Eight tracking levels on pack config       */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   04/08/05 DM   DCS    NEW7858  Extend SKU to 30 characters                */
/******************************************************************************/

/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF 
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132

/* Top Title */
TTITLE CENTER _SITENAME SKIP 2 - 
LEFT 'Inventory For SKU &7 From &5 To &6' RIGHT _DATESTAMP SKIP 1 - 
RIGHT _TIMESTAMP SKIP 1 - 
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2
 
/* Column Headings and Formats */
COLUMN Site_Client HEADING 'Site/|Client' FORMAT A10 WORD_WRAP
COLUMN Owner_ID HEADING 'Owner' FORMAT A10
COLUMN cSKUID HEADING 'SKU' FORMAT A30
COLUMN cNotes HEADING 'Notes' FORMAT A19
COLUMN cConfig HEADING 'Pack|Config' FORMAT A15
COLUMN cPallets HEADING 'Pallets'
COLUMN cQuantity HEADING 'Cartons|In Stock'
COLUMN cControl HEADING 'Control|Total'
COLUMN cDiff HEADING 'Diff'

BREAK ON Site_Client SKIP PAGE ON OWNER_ID SKIP PAGE ON cSKUID SKIP 1 ON REPORT

COMPUTE SUM OF cPallets cQuantity cControl ON Site_Client

/* Now the SQL Select Statement */
SELECT	RPAD (I.Site_ID, 10) || I.Client_ID Site_Client,
	I.Owner_ID,
	I.SKU_ID cSKUID,
	CC.Notes cNotes,
	MIN (SC.Config_ID) cConfig,
	COUNT (I.SKU_ID) cPallets,
	SUM (I.Qty_On_Hand) + SUM(I.Qty_Allocated) cQuantity,
	COUNT (I.SKU_ID) * NVL (MIN (SC.Ratio_1_to_2), 1) * 
			   NVL (MIN (SC.Ratio_2_to_3), 1) * 
			   NVL (MIN (SC.Ratio_3_to_4), 1) * 
			   NVL (MIN (SC.Ratio_4_to_5), 1) * 
			   NVL (MIN (SC.Ratio_5_to_6), 1) * 
			   NVL (MIN (SC.Ratio_6_to_7), 1) * 
			   NVL (MIN (SC.Ratio_7_to_8), 1) cControl,
	COUNT (I.SKU_ID) * NVL (MIN(SC.Ratio_1_to_2), 1) * 
			   NVL (MIN (SC.Ratio_2_to_3), 1) * 
			   NVL (MIN (SC.Ratio_3_to_4), 1) * 
			   NVL (MIN (SC.Ratio_4_to_5), 1) * 
			   NVL (MIN (SC.Ratio_5_to_6), 1) * 
			   NVL (MIN (SC.Ratio_6_to_7), 1) * 
			   NVL (MIN (SC.Ratio_7_to_8), 1) - 
			   SUM (I.QTY_On_Hand) - 
			   SUM (I.QTY_Allocated) cDiff
FROM Inventory I, SKU_Config SC, Condition_code CC 
WHERE I.SKU_ID LIKE '&7'
AND I.Receipt_DStamp >= TO_DATE('&5', 'DD-MON-YYYY') 
AND I.Receipt_DStamp < TO_DATE('&6', 'DD-MON-YYYY') + 1
AND SC.Config_ID = (SELECT MIN(Config_ID)
		    FROM SKU_SKU_config
		    WHERE SKU_SKU_config.SKU_ID = I.SKU_ID
			AND ((('&3' IS NULL) AND ('&8' IS NULL))
			OR  (SKU_SKU_Config.Client_ID = '&3')
			OR  (('&3' IS NULL) AND SKU_SKU_Config.Client_ID IN
				(SELECT Client_ID 
	 			FROM Client_Group_Clients
	 			WHERE Client_Group = '&8')))  
			)
AND CC.Condition_ID(+) = I.Condition_ID
AND (('&2' IS NULL) OR (I.Site_ID = '&2'))
AND (('&4' IS NULL) OR (I.Owner_ID = '&4'))
AND ((('&3' IS NULL) AND ('&8' IS NULL))
OR  (I.Client_ID = '&3')
OR  (('&3' IS NULL) AND I.Client_ID IN
	(SELECT Client_ID 
	 FROM Client_Group_Clients
	 WHERE Client_Group = '&8')))  
AND SC.rowid = LibConfig.GetConfigRowid (SC.Config_ID, '&3')
GROUP BY I.Site_ID, I.Client_ID, I.Owner_ID, I.SKU_ID, CC.Notes
ORDER BY I.Site_ID, I.Client_ID, I.Owner_ID, I.SKU_ID, CC.Notes
;

SET TERMOUT ON
