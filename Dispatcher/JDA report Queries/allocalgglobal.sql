/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics & Industrial Systems Ltd  */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1995             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*                                 LIS                                        */
/*                                                                            */
/*     FILE NAME  :     allocalgglobal.sql                                    */
/*                                                                            */
/*     DESCRIPTION:     List the global allocation algorithms.                */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   08/06/95 RMW  DCS    N/A      initial version                            */
/*   11/11/98 JH   DCS    NEW3173  ABC/pareto analysis			      */
/*   16/06/99 MJT  DCS    PDR7593  Some sql*plus reports are wrapping         */
/*   21/02/00 RMW  DCS    NEW4070  Crossdock enhancements                     */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/******************************************************************************/

/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132

/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Global Allocation Algorithms' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2

BREAK ON Site_ID SKIP PAGE

/* Column Headings and Formats */
COLUMN Site_ID HEADING 'Site' FORMAT A10
COLUMN Priority HEADING 'Priority' FORMAT 99999999
COLUMN Algorithm HEADING 'Algorithm' FORMAT A60
COLUMN Zone_1 HEADING 'Zone' FORMAT A10
COLUMN FinalLocZone HEADING 'Final|Locn|Zone' FORMAT A5
COLUMN SplitInventory HEADING 'Split|Inv' FORMAT A5
COLUMN Min_Due_Pick_Mins HEADING 'Minimum|Pick Time' FORMAT 9999

/* SQL Select statement for the global algorithms */
SELECT	GA.Site_ID,
	GA.Priority,
	L.Text Algorithm,
	GA.Zone_1,
	NVL(GA.Final_Loc_Zone, 'N') FinalLocZone,
	NVL(GA.Split_Inventory, 'N') SplitInventory,
	GA.Min_Due_Pick_Mins
FROM Language_Text L, Global_Allocation GA
WHERE L.Label = GA.Algorithm
AND L.Language = 'EN_GB'
AND (('&2' IS NULL) OR (GA.Site_ID = '&2'))
ORDER BY 1, 2;

SET TERMOUT ON

