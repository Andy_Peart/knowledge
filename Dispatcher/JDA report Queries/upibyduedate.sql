/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics & Industrial Systems Ltd  */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1996             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*                                  LIS                                       */
/*                                                                            */
/*     FILE NAME  :     upibyduedate.sql                                      */
/*                                                                            */
/*     DESCRIPTION:     List the upi receipt data by due date.                */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   11/11/98 JH   DCS    NEW3261  UPI receiving			      */
/*   16/06/99 MJT  DCS    PDR7593  Some sql*plus reports are wrapping         */
/*   29/06/01 MJT  DCS    NEW5486  Client ID added                            */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   08/08/05 DM   DCS    NEW7858  Extend SKU to 30 characters                */
/******************************************************************************/

/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132

/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'UPI Receipts By Due Date Range' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2 -
LEFT 'Date From &5 To &6' SKIP 2

BREAK ON Site_Client SKIP PAGE ON Owner_ID SKIP PAGE ON Pallet_ID SKIP 1

/* Column Headings and Formats */
COLUMN Site_Client HEADING 'Site/|Client' FORMAT A10 WORD_WRAP
COLUMN Owner_ID HEADING 'Owner' FORMAT A10
COLUMN cTaskID HEADING 'Pallet/|Line'  FORMAT A20
COLUMN Status HEADING 'Status' FORMAT A11
COLUMN sDescription HEADING 'SKU/|Description' FORMAT A40 WRAP
COLUMN Config_ID HEADING 'Pack Config/|Cond Code' FORMAT A15
COLUMN Quant_Due HEADING 'Qty Due' FORMAT A19

SELECT	RPAD (UPH.Site_ID, 10) || UPL.Client_ID Site_Client,
	UPL.Owner_ID,
	RPAD (UPH.Pallet_ID, 20) || UPL.Line_id cTaskID,
	RPAD (UPH.Status, 11) Status,
	RPAD (UPL.SKU_ID, 40) || S.Description sDescription,
	RPAD (NVL (UPL.Config_ID, ' '), 15) || UPL.Condition_ID Config_ID,
	TO_CHAR(UPL.Qty_Due,'999G999G990D999999') Quant_Due
FROM SKU S, UPI_Receipt_Line UPL, UPI_Receipt_Header UPH
WHERE UPL.Pallet_ID = UPH.Pallet_ID
AND UPL.SKU_ID = S.SKU_ID
AND UPL.Client_ID = S.Client_ID
AND UPH.Due_DStamp >= TO_DATE('&5', 'DD-MON-YYYY') 
AND UPH.Due_DStamp < TO_DATE('&6', 'DD-MON-YYYY') + 1
AND (('&2' IS NULL) OR (UPH.Site_ID = '&2'))
AND (('&4' IS NULL) OR (UPL.Owner_ID = '&4'))
AND ((('&3' IS NULL) AND ('&7' IS NULL))
OR  (UPL.Client_ID = '&3')
OR  (('&3' IS NULL) AND UPL.Client_ID IN
(SELECT Client_ID
FROM Client_Group_Clients
WHERE Client_Group = '&7')))
ORDER BY UPH.Site_ID, UPL.Client_ID, UPL.Owner_ID, UPH.Due_DStamp, UPH.Pallet_ID, UPL.Line_ID
;
