/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics & Industrial Systems Ltd  */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1995             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/
 
/******************************************************************************/
/*                                                                            */
/*                                  LIS                                       */
/*                                                                            */
/*     FILE NAME  :     allocshortbycons.sql                                  */
/*                                                                            */
/*     DESCRIPTION:     List allocation shortages by Consignment              */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   30/10/95 RMW  DCS    N/A      initial version                            */
/*   25/04/97 MJT  N/A    PDR5924  Modified to handle european group & decimal*/
/*                                 separators.                                */
/*   07/08/97 MJT  N/A    N/A      Changed number format to display 0         */
/*   06/02/98 JH   DCS    PDR5915  Reports need to be multi-sited             */
/*   24/07/98 MJT  DCS    NEW3188  Third Party Warehousing                    */
/*   16/06/99 MJT  DCS    PDR7593  Some sql*plus reports are wrapping         */
/*   21/06/01 MJT  DCS    NEW5486  Client ID added                            */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   03/08/05 DM   DCS    NEW7858  Increase SKU to 30 charactres              */
/*   19/10/06 JH   DCS    ENH3120  Increase cons/wg to 20 chars               */
/******************************************************************************/
 
/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON

CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132

TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Allocation Shortages By Consignment' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page ' FORMAT 999 SQL.PNO SKIP 2 -
LEFT 'Consignment &5' SKIP 2

BREAK ON Site_ID SKIP PAGE ON Client_ID SKIP PAGE ON Owner_ID SKIP PAGE

COLUMN Site_ID HEADING 'Site' FORMAT A10
COLUMN CliOwn HEADING 'Client/|Owner' FORMAT A10 WRAP
COLUMN WGCons HEADING 'Work Group/|Consignment' FORMAT A20 WRAP
COLUMN Task HEADING 'Task' FORMAT A20
COLUMN LineIDent HEADING 'Line' FORMAT '999G990'
COLUMN Sku_ID heading 'SKU' FORMAT A30
COLUMN Shortage HEADING 'Shortage' FORMAT '999G999G990D999999'

SELECT	Site_ID,
	RPAD(Client_ID, 10) || Owner_ID CliOwn,
	RPAD(Work_Group, 20) || Consignment WGCons,
	(RPAD(Task_ID, 20)) Task,
	TO_CHAR(Line_ID,'999G990') LineIDent,
	SKU_ID,
	TO_CHAR(Qty_Ordered - Qty_Tasked,'999G999G990D999999') Shortage
FROM Generation_Shortage
WHERE Consignment = '&5'
AND (('&2' IS NULL) OR (Site_ID = '&2'))
AND (('&4' IS NULL) OR (Owner_ID = '&4'))
AND ((('&3' IS NULL) AND ('&6' IS NULL))
OR  (Client_ID = '&3')
OR  (('&3' IS NULL) AND Client_ID IN
	(SELECT Client_ID 
	 FROM Client_Group_Clients
	 WHERE Client_Group = '&6')))  
ORDER BY Site_ID, Client_ID, Owner_ID, Consignment, Task_ID
;

SET TERMOUT ON 



