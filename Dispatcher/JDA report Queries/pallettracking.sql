/******************************************************************************/
/*                                                                            */
/*   Logistics & Industrial Systems Ltd                                       */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics & Industrial Systems Ltd  */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics & Industrial   */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics & Industrial Systems Limited, 1995               */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*                                 LIS                                        */
/*                                                                            */
/*     FILE NAME  :     pallettracking.sql                                    */
/*                                                                            */
/*     DESCRIPTION:     Output pallet tracking details.                       */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   16/06/99 MJT  DCS    PDR7593  Some sql*plus reports are wrapping         */
/*   26/08/02 RMW  DCS    NEW6186  Multi-client addresses                     */
/*   26/02/03 RMW  DCS    NEW6000  Asset tracking changes                     */
/*   09/05/03 DGP  DCS    PDR8991  Change Count to Pallet_Count               */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/******************************************************************************/

/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132

/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Asset Tracking' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2

/* Column Headings and Formats */
COLUMN Address_ID HEADING 'Address' FORMAT A15
COLUMN Client_ID HEADING 'Client' FORMAT A10
COLUMN Address_Type HEADING 'Type' FORMAT A10
COLUMN Name HEADING 'Name' FORMAT A30
COLUMN Contact HEADING 'Contact' FORMAT A15
COLUMN Contact_Phone HEADING 'Telephone' FORMAT A15
COLUMN Config_ID HEADING 'Type' FORMAT A15
COLUMN cPalletCount HEADING 'Number Of|Pallets' FORMAT 9999999999

BREAK ON Client_ID SKIP PAGE ON Address_Type SKIP 2 ON Address_ID ON Name ON Contact ON Contact_Phone

/* SQL Select statement */
SELECT	A.Address_Type,
	A.Client_ID,
	A.Address_ID,
	A.Name,
	A.Contact,
	A.Contact_Phone,
	APCC.Config_ID,
	NVL (APCC.Pallet_Count, 0) cPalletCount
FROM Address A, Address_Pallet_Config_Cnt APCC
WHERE A.Client_ID = APCC.Client_ID
AND A.Address_ID = APCC.Address_ID
AND (('&2' IS NULL) OR (A.Client_ID = '&2'))
ORDER BY A.Client_ID, A.Address_Type, A.Address_ID, APCC.Config_ID;

SET TERMOUT ON

