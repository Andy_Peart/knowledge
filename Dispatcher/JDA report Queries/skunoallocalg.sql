/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics & Industrial Systems Ltd  */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1995             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/
 
/******************************************************************************/
/*                                                                            */
/*                                  LIS                                       */
/*                                                                            */
/*     FILE NAME  :     skunoallocalg.sql                                     */
/*                                                                            */
/*     DESCRIPTION:     List SKU_ID, Description & Product_Group for all SKUs */
/*                      that have no entry in the SKU_Allocation table, for a */
/*                      given SKU_ID range.                                   */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   03/04/97 MJT  DCS    2694     initial version                            */
/*   06/02/98 JH   DCS    PDR5915  Reports need to be multi-sited             */
/*   16/06/99 MJT  DCS    PDR7593  Some sql*plus reports are wrapping         */
/*   28/06/01 MJT  DCS    NEW5486  Client ID added                            */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   08/08/05 DM   DCS    NEW7858  Extend SKU to 30 characters                */
/******************************************************************************/
 
/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132
  
/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'SKUs With No Allocation Algorithms By SKU Range' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2 - 
LEFT 'SKU Range From &4 To &5' SKIP 2
 
/* Column Headings and Formats */
COLUMN Client_ID HEADING 'Client' FORMAT A10
COLUMN SKU_ID HEADING 'SKU' FORMAT A30
COLUMN Description HEADING 'Description' FORMAT A40
COLUMN Product_Group HEADING 'Product|Group' FORMAT A10

BREAK ON Client_ID SKIP PAGE
 
/* SQL Select statement */
SELECT	SK.Client_ID,
	SK.SKU_ID,
	SK.Description,
	SK.Product_Group
FROM SKU SK
WHERE NOT EXISTS
(SELECT * FROM SKU_Allocation SA 
WHERE SK.SKU_ID = SA.SKU_ID
AND (('&2' IS NULL) OR (SA.Site_ID = '&2'))
AND SK.Client_ID = SA.Client_ID)
AND SK.SKU_ID BETWEEN '&4' AND '&5'
AND ((('&3' IS NULL) AND ('&6' IS NULL))
OR  (SK.Client_ID = '&3')
OR  (('&3' IS NULL) AND SK.Client_ID IN
(SELECT Client_ID 
FROM Client_Group_Clients
WHERE Client_Group = '&6')))  
ORDER BY SK.Client_ID, SK.SKU_ID
; 
 
SET TERMOUT ON

