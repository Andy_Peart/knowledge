/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics & Industrial Systems Ltd  */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LEXLOGISTiX.                                 */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1995             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*                                  LIS                                       */
/*                                                                            */
/*     FILE NAME  :     stokchkbylist.sql                                     */
/*                                                                            */
/*     DESCRIPTION:     This Report lists all stock check tasks for a         */
/*                      specific List_ID in sequence order.                   */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   22/11/95 MdB  N/A    N/A      initial version                            */
/*   19/05/97 MJT  DCS    PDR5924  Modified to handle european group & decimal*/
/*                                 separators.                                */
/*   7/08/97  MJT  DCS    NEW2813  Modified to include new container id,      */
/*                                 pallet id fields in inventory              */
/*   06/02/98 JH   DCS    PDR5915  Reports need to be multi-sited             */
/*   28/07/98 MJT  DCS    NEW3188  Third Party Warehousing                    */
/*   18/04/99 JH   DCS    NEW3920  Extra/wider database columns		      */
/*   16/06/99 MJT  DCS    PDR7593  Some sql*plus reports are wrapping         */
/*   19/10/99 RMC  DCS    PDR7835  Report doesn't show empty locations	      */
/*   28/06/01 MJT  DCS    NEW5486  Client ID added                            */
/*   06/09/02 EW   DCS    PDR8822  Stock Check List report not correct        */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   08/08/05 DM   DCS    NEW7858  Extend SKU to 30 characters                */
/******************************************************************************/

/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132

/* Set up Variables */
COLUMN LIST_ID NEW_VALUE VLIST_ID NOPRINT

/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Stock Check Tasks By List' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2 -
LEFT 'List ' FORMAT A8 VLIST_ID SKIP 2

/* Column Headings and Formats */
COLUMN Site_Client HEADING 'Site/|Client' FORMAT A10 WORD_WRAP
COLUMN Owner_Location HEADING 'Owner/|Location' FORMAT A10 WORD_WRAP
COLUMN Tag_ID HEADING 'Tag' FORMAT A20
COLUMN sDescription HEADING 'SKU/|Description' FORMAT A30 WORD_WRAP
COLUMN ConditionConfig HEADING 'Condition/|Pack Config' FORMAT A15
COLUMN Container_Pallet_ID HEADING 'Container/|Pallet' FORMAT A20 WRAP
COLUMN UQTY HEADING 'Quantity' FORMAT A20

BREAK ON Site_Client SKIP PAGE ON Owner_ID SKIP PAGE ON ROW SKIP 2

/* SQL Select statement */
SELECT	RPAD (SCT.Site_ID, 10) || SCT.Client_ID Site_Client,
	RPAD (SCT.Owner_ID, 10) || SCT.Location_ID Owner_Location,
	SCT.List_ID,
	'___________________' UQTY,
	SCT.Tag_ID,
	RPAD (SCT.SKU_ID, 30) || S.Description sDescription,
        RPAD (NVL (I.Condition_ID, ' '), 15) || I.Config_ID ConditionConfig,
	RPAD (NVL (I.Container_ID, ' '), 20) || I.Pallet_ID Container_Pallet_ID
FROM Stock_Check_Tasks SCT, SKU S, Inventory I
WHERE SCT.SKU_ID = S.SKU_ID (+)
AND SCT.Client_ID = S.Client_ID (+)
AND I.SKU_ID (+) = SCT.SKU_ID
AND I.Client_ID (+) = SCT.Client_ID
AND SCT.List_ID = '&5'
AND NVL(I.Tag_ID (+), '_DUM_TAG') = NVL(SCT.Tag_ID, '_DUM_TAG')
AND I.Location_ID (+) = SCT.Location_ID
AND ((I.Site_ID IS NULL) OR (I.Site_ID = SCT.Site_ID))
AND (('&2' IS NULL) OR (SCT.Site_ID = '&2'))
AND (('&4' IS NULL) OR (SCT.Owner_ID = '&4') OR (SCT.Owner_ID IS NULL))
AND ((('&3' IS NULL) AND ('&6' IS NULL))
OR  (SCT.Client_ID = '&3')
OR  (SCT.Client_ID IS NULL)
OR  (('&3' IS NULL) AND SCT.Client_ID IN
(SELECT Client_ID
FROM Client_Group_Clients
WHERE Client_Group = '&6')))
ORDER BY SCT.Site_ID, SCT.Client_ID, SCT.Owner_ID, SCT.Sequence, SCT.Tag_ID
;

SET TERMOUT ON






