/******************************************************************************/
/*                                                                            */
/*  Copyright (c) 2004                                                        */
/*  RedPrairie Corporation                                                    */
/*  All Rights Reserved                                                       */
/*                                                                            */
/*  The information in this file contains trade secrets and confidential      */
/*  information which is the property of RedPrairie Corporation.              */
/*                                                                            */
/*  All trademarks, trade names, copyrights and other intellectual property   */
/*  rights created, developed, embodied in or arising in connection with      */
/*  this software shall remain the sole property of RedPrairie Corporation.   */
/*                                                                            */
/*  Copyright (c) RedPrairie Corporation, 2004                                */
/*  ALL RIGHTS RESERVED                                                       */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :     difotbydate.sql                                       */
/*                                                                            */
/*     DESCRIPTION:     List the suppliers that have made deliveries with a   */
/*                      percentage of the fully delivered ones.               */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   24/01/07 RMW  DCS    ENH3283  Initial version                            */
/******************************************************************************/

/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132


/* Top Title */
TTITLE CENTER _SITENAME SKIP 2 -
LEFT 'Supplier Performance - Pre-advices Delivered in Full on Time - by Date' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2 -
LEFT 'Date From &5 To &6' SKIP 2

/* Show Site and client once */
BREAK ON Site_ID SKIP 1 ON Client_ID SKIP SKIP 1

/* Column Headings and Formats */
COLUMN Client_ID HEADING 'Client' FORMAT A10
COLUMN Supplier_ID HEADING 'Supplier' FORMAT A15
COLUMN Name HEADING 'Name' FORMAT A30
COLUMN NumPreAdvice HEADING 'Number of|Pre-Advices' FORMAT '999G999G990'
COLUMN ShortPreAdvice HEADING 'Short|Pre-Advices' FORMAT '999G999G990'
COLUMN SuccessRate HEADING 'Success|Rate %' FORMAT '999D99'


/* SQL Select statement */
SELECT	PAH.Site_ID,
	PAH.Client_ID,
	PAH.Supplier_ID,
	PAH.Name,
	AC.NumPreAdvice,
	SC.ShortPreAdvice,
	(100 - (100 / (AC.NumPreAdvice / SC.ShortPreAdvice))) SuccessRate
FROM Pre_Advice_Header PAH,
(
/* Get all the short pre-advices in the date range */
SELECT	PAH1.Client_ID, PAH1.Supplier_ID,
	COUNT (PAH1.Supplier_ID) ShortPreAdvice
FROM Pre_Advice_Header PAH1
WHERE (PAH1.Pre_Advice_ID, PAH1.Client_ID) IN
	(
	/* Get all the lines that are short received */
	SELECT DISTINCT PAH2.Pre_Advice_ID, PAH2.Client_ID
	FROM Pre_Advice_Header PAH2, Pre_Advice_Line PAL2
	WHERE PAH2.Pre_Advice_ID = PAL2.Pre_Advice_ID
	AND PAH2.Client_ID = PAL2.Client_ID
	AND NVL (PAL2.Qty_Received, 0) <
		LibConfig.GetEachQuantity (PAL2.Qty_Due,
					   PAL2.Config_ID,
					   PAL2.Client_ID,
					   PAL2.SKU_ID,
					   PAL2.Tracking_Level)
	AND (PAH2.Site_ID = '&2' OR '&2' IS NULL)
	AND ((('&3' IS NULL) AND ('&7' IS NULL))
	OR  (PAH2.Client_ID = '&3')
	OR  (('&3' IS NULL) AND PAH2.Client_ID IN (SELECT Client_ID
						   FROM Client_Group_Clients
						   WHERE Client_Group = '&7')))
	AND (PAH2.Owner_ID = '&4' OR '&4' IS NULL)
	AND PAH2.Due_DStamp >= TO_DATE ('&5', 'DD-MON-YYYY')
	AND PAH2.Due_DStamp < TO_DATE ('&6', 'DD-MON-YYYY') + 1
	)
GROUP BY PAH1.Client_ID, PAH1.Supplier_ID
) SC,
(
/* Get all the pre-advices in the date range */
SELECT Client_ID, Supplier_ID, COUNT (Supplier_ID) NumPreAdvice
FROM Pre_Advice_Header
WHERE (Site_ID = '&2' OR '&2' IS NULL)
AND ((('&3' IS NULL) AND ('&7' IS NULL))
OR  (Client_ID = '&3')
OR  (('&3' IS NULL) AND Client_ID IN (SELECT Client_ID
				      FROM Client_Group_Clients
				      WHERE Client_Group = '&7')))
AND (Owner_ID = '&4' OR '&4' IS NULL)
AND Due_DStamp >= TO_DATE ('&5', 'DD-MON-YYYY')
AND Due_DStamp < TO_DATE ('&6', 'DD-MON-YYYY') + 1
GROUP BY Client_ID, Supplier_ID
) AC
WHERE PAH.Supplier_ID = SC.Supplier_ID
AND PAH.Client_ID = SC.Client_ID
AND PAH.Supplier_ID = AC.Supplier_ID
AND PAH.Client_ID = AC.Client_ID
AND (PAH.Site_ID = '&2' OR '&2' IS NULL)
AND ((('&3' IS NULL) AND ('&7' IS NULL))
OR  (PAH.Client_ID = '&3')
OR  (('&3' IS NULL) AND PAH.Client_ID IN (SELECT Client_ID
					  FROM Client_Group_Clients
					  WHERE Client_Group = '&7')))
AND (PAH.Owner_ID = '&4' OR '&4' IS NULL)
AND PAH.Due_DStamp >= TO_DATE ('&5', 'DD-MON-YYYY')
AND PAH.Due_DStamp < TO_DATE ('&6', 'DD-MON-YYYY') + 1
GROUP BY PAH.Site_ID,
	PAH.Client_ID,
	PAH.Supplier_ID,
	PAH.Name,
	AC.NumPreAdvice,
	SC.ShortPreAdvice
ORDER BY 1, 2, 7 DESC, 4
/


SET TERMOUT ON

