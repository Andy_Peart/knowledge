/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics & Industrial Systems Ltd  */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1998             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/
/******************************************************************************/
/*                                                                            */
/*                                   LIS                                      */
/*                                                                            */
/*     FILE NAME  :     function_access.sql                                   */
/*                                                                            */
/*     DESCRIPTION:     List the function_accesses.                           */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   09/07/95 RMD  3178   3415     initial version                            */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/******************************************************************************/
 
/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132
 
/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Function Access Settings' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2 -
LEFT 'For Site: &2' SKIP 2
 
BREAK ON Site_ID SKIP 3

/* Column Headings and Formats */
COLUMN Site_ID HEADING 'Site ID' FORMAT A10
COLUMN Text HEADING 'Function ID' FORMAT A90 WRAP
COLUMN Type HEADING 'Type' FORMAT A7
COLUMN Enabled HEADING 'On ?' FORMAT A4

/* SQL Select statement */
SELECT 	FA.Site_ID, LT.Text, 
	DECODE (Enabled, 'Y', 'Local', 'G', 'Global', null, 'Global', 'Local') Type, 
	DECODE (Enabled, 'Y', 'ON', 'G', 'ON', null, 'OFF', 'OFF') Enabled
FROM Function_Access FA, Language_Text LT
WHERE FA.Function_ID = LT.Label
AND FA.Site_ID = '&2'
AND LT.Language = '&3'
UNION
SELECT 	'-' Site_ID, LT.Text,
	DECODE (Enabled, 'Y', 'Local', 'G', 'Global', null, 'Global', 'Local') Type, 
	DECODE (Enabled, 'Y', 'ON', 'G', 'ON', null, 'OFF', 'OFF') Enabled
FROM Function_Access FA, Language_Text LT
WHERE FA.Function_ID = LT.Label
AND FA.Site_ID IS NULL
AND LT.Language = '&3'
ORDER BY 1 DESC, 3 ASC, 2 ASC
; 
 
SET TERMOUT ON
 
