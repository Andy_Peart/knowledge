/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics & Industrial Systems Ltd  */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1995             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*                                 LIS                                        */
/*                                                                            */
/*     FILE NAME  :     ordlatebydate.sql                                     */
/*                                                                            */
/*     DESCRIPTION:     List the Orders Shipped Late.                         */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   29/10/99 LP   DCS    N/A      initial version                            */
/*   27/06/01 MJT  DCS    NEW5486  Client ID added                            */
/*   28/08/03 DL   DCS    PDR9074  Various reporting issues                   */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/******************************************************************************/

/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132

/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Orders Shipped Late By Ship By Date Range' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2 -
LEFT 'Ship By Date Range From &5  To &6' SKIP 2

BREAK ON Site_Client ON Owner_ID SKIP PAGE ON Ship_By_Date SKIP 1 ON Order_ID

/* Column Headings and Formats */
COLUMN Site_Client HEADING 'Site/|Client' FORMAT A10 WORD_WRAP
COLUMN Owner_ID HEADING 'Owner' FORMAT A10
COLUMN cOrderID HEADING 'Order' FORMAT A20
COLUMN Order_Date HEADING 'Order Date' FORMAT A11
COLUMN Ship_By_Shipped_Date HEADING 'Ship By Date/Time|Shipped Date/Time' FORMAT A18 WRAP
COLUMN Late_By HEADING 'Late By|Days  Hours  Minutes' FORMAT A20 WRAP
COLUMN Customer HEADING 'Customer/|Name' FORMAT A25 WRAP

/* SQL Select statement */
SELECT	RPAD (OH.From_Site_ID, 10) || OH.Client_ID Site_Client,
	OH.Owner_ID,
	RPAD (OH.Order_ID, 20) cOrderID,
	TO_CHAR (OH.Order_Date, 'DD-MON-YYYY') Order_Date,
	RPAD(TO_CHAR (OH.Ship_By_Date, 'DD-MON-YYYY  HH24:MI'), 18) ||
	TO_CHAR (OH.Shipped_Date, 'DD-MON-YYYY  HH24:MI') Ship_By_Shipped_Date,
	TO_CHAR ((OH.Shipped_Date - OH.Ship_By_Date), 'DD-MON-YYYY') || '     ' || 
	TO_CHAR (CURRENT_TIMESTAMP + (OH.Shipped_Date - OH.Ship_By_Date), 'HH24"     "MI"     "') Late_By,
	RPAD (NVL (OH.Customer_ID, ' '), 25) || OH.Name Customer
FROM Order_Header OH
WHERE OH.Ship_By_Date >= TO_DATE('&5', 'DD-MON-YYYY')
AND OH.Ship_By_Date < TO_DATE('&6', 'DD-MON-YYYY') + 1
AND OH.Shipped_Date > OH.Ship_By_Date
AND (('&2' IS NULL) OR (OH.From_Site_ID = '&2'))
AND (('&4' IS NULL) OR (OH.Owner_ID = '&4'))
AND ((('&3' IS NULL) AND ('&7' IS NULL))
OR  (OH.Client_ID = '&3')
OR  (('&3' IS NULL) AND OH.Client_ID IN
(SELECT Client_ID 
FROM Client_Group_Clients
WHERE Client_Group = '&7')))  
ORDER BY OH.From_Site_ID, OH.Client_ID, OH.Owner_ID, OH.Ship_By_Date, OH.Order_ID
;

SET TERMOUT ON

