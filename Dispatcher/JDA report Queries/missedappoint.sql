/******************************************************************************/
/*                                                                            */
/*   Logistics and Internet Systems Ltd                                       */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics and Internet Systems Ltd  */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Internet   */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 2001             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*                                 LIS                                        */
/*                                                                            */
/*     FILE NAME  :     missedappoint.sql                                     */
/*                                                                            */
/*     DESCRIPTION:     List missed appointments by date range.               */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   05/03/01 DGP  DCS    NEW5300  initial version                            */
/*   29/08/02 RMW  DCS    NEW6186  Multi-client addresses                     */
/*   16/07/03 DL   DCS    PDR9048  Supplier Performance report gives SQL error*/
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/******************************************************************************/

/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132

/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Supplier Performance - Missed Appointments By Date Range' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2 -
LEFT 'Date Range From &2 To &3 Late By: &4 Hours' SKIP 2 -
LEFT 'Date: ' dDate SKIP 2

/* Column Headings and Formats */
COLUMN ClientID HEADING 'Client' FORMAT A10
COLUMN SupplierID NEW_VALUE sSupplier NOPRINT
COLUMN AddedDStamp NEW_VALUE dDate  NOPRINT
COLUMN Supplier_BookRef_Notes HEADING 'Supplier            Booking ID|Notes' FORMAT A65 WRAP
COLUMN ScheduledDateTime HEADING 'Scheduled|Date/Time' FORMAT A11 WRAP
COLUMN ActualDateTime HEADING 'Actual|Date/Time' FORMAT A11
COLUMN TrailerDriver HEADING 'Trailer ID|Driver' FORMAT A30 WRAP

BREAK ON ClientID ON AddedDStamp SKIP PAGE ON AddedDStamp SKIP 1 ON DockDoor

/* SQL Select statement */
SELECT  SP.Client_ID ClientID,
	SP.Supplier_ID SupplierID,
        SP.Added_DStamp AddedDStamp,
        RPAD(SP.Supplier_ID, 20) || RPAD(SP.BookRef_ID, 50)
		|| RPAD(Notes, 70) Supplier_BookRef_Notes,
        TO_CHAR(SP.Scheduled_DStamp, 'DD-MON-YYYY')
		|| TO_CHAR(SP.Scheduled_DStamp, 'HH24:MI') ScheduledDateTime,
        TO_CHAR(SP.Actual_DStamp, 'DD-MON-YYYY')
		|| TO_CHAR(SP.Actual_DStamp, 'HH24:MI') ActualDateTime,
        RPAD(SP.Trailer_ID, 30) || SP.Driver TrailerDriver
FROM Supplier_Performance SP
WHERE SP.Added_DStamp >= TO_DATE('&2', 'DD-MON-YYYY')
AND SP.Added_DStamp   < TO_DATE('&3', 'DD-MON-YYYY') + 1
AND SP.Actual_DStamp > SP.Scheduled_DStamp + (&4 / 24)
ORDER BY 1, 3, 2, 4
;

SET TERMOUT ON

