/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics and Industrial Systems Ltd*/
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1997             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/
 
/******************************************************************************/
/*                                                                            */
/*                                 LIS                                        */
/*                                                                            */
/*     FILE NAME  :     repackpalllbl.sql                                     */
/*                                                                            */
/*     DESCRIPTION:     Output the field(s) required for the repack pallet    */
/*                      label formatter. Note that this is only an example -  */
/*                      in reality it will probably always be customised.     */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   10/10/97 RMW  DCS    N/A      initial version                            */
/*   16/06/99 MJT  DCS    PDR7593  Some sql*plus reports are wrapping         */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/******************************************************************************/
 
/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK OFF
SET VERIFY OFF
SET TAB OFF 
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set headings off, no trailing spaces etc. */
SET PAGESIZE 0
SET NEWPAGE 0
SET HEADING OFF
SET TRIM ON
 
COLUMN Pallet_ID FOLD_AFTER

/* SQL Select statement */
SELECT DISTINCT I.Pallet_ID, OC.Pallet_Weight
FROM Inventory I, Order_Container OC
WHERE I.Pallet_ID = '&2'
AND   I.PAllet_ID = OC.Pallet_ID (+)
;

SET TERMOUT ON

