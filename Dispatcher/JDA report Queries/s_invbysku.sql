/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics and Industrial Systems Ltd*/
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LEXLOGISTiX.                                 */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1995             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/
 
/******************************************************************************/
/*                                                                            */
/*                             LEXLOGISTiX                                    */
/*                                                                            */
/*     FILE NAME  :     invbysku.sql                                          */
/*                                                                            */
/*     DESCRIPTION:     List inventory by SKU ID range.                       */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   06/03/95 DJS  DCS    N/A      initial version                            */
/*   25/04/97 MJT  DCS    PDR5924  Modified to handle european group & decimal*/
/*                                 separators.                                */
/*   6/08/97  MJT  DCS    NEW2813  Modified to include new container id,      */
/*                                 pallet id fields in inventory              */
/*   06/02/98 JH   DCS    PDR5915  Reports need to be multi-sited             */
/*   24/07/98 MJT  DCS    NEW3188  Third Party Warehousing                    */
/*   18/04/99 JH   DCS    NEW3920  Extra/wider database columns		      */
/*   16/06/99 MJT  DCS    PDR7593  Some sql*plus reports are wrapping         */
/*   25/06/01 MJT  DCS    NEW5486  Client ID added                            */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   25/06/04 RMC  DCS    PDR9092  Change reports for VAS and beam            */
/*   04/08/05 DM   DCS    NEW7858  Extend SKU to 30 characters                */
/******************************************************************************/
/* SQL Select statement */
SELECT	RPAD (I.Site_ID, 10) || I.Client_ID Site_Client,
	RPAD (I.Owner_ID, 10) || I.Location_ID Owner_Location, 
	RPAD (I.SKU_ID, 30) || I.Description sDescription,
	RPAD (NVL (I.Config_ID, ' '), 15) || I.Condition_ID Config_ID,
	TO_CHAR (I.Receipt_DStamp, 'DD-MON-YYYY') || 
	TO_CHAR (I.Expiry_DStamp, 'DD-MON-YYYY') Receipt_Expiry_DStamp,
	RPAD (NVL(I.Tag_ID, ' '), 20) || I.Batch_ID Tag_Batch,
	LPAD (TO_CHAR (Qty_On_Hand,'999G999G990D999999'), 19) ||
	TO_CHAR (Qty_Allocated,'999G999G990D999999') Quant_On_Hand_Allocated,
	I.Spec_Code
FROM Inventory I
WHERE I.SKU_ID BETWEEN '&5' AND '&6'
AND (('&2' IS NULL) OR (I.Site_ID = '&2'))
AND (('&4' IS NULL) OR (I.Owner_ID = '&4'))
AND ((('&3' IS NULL) AND ('&7' IS NULL))
OR  (I.Client_ID = '&3')
OR  (('&3' IS NULL) AND I.Client_ID IN
	(SELECT Client_ID 
	 FROM Client_Group_Clients
	 WHERE Client_Group = '&7')))  
ORDER BY I.Site_ID, I.Client_ID, I.Owner_ID, I.SKU_ID, I.Tag_ID;
