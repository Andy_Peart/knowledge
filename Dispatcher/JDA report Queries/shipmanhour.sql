/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics and Industrial Systems Ltd*/
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1999             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/
 
/******************************************************************************/
/*                                                                            */
/*                                 LIS                                        */
/*                                                                            */
/*     FILE NAME  :     shipmanperhour.sql                                    */
/*                                                                            */
/*     DESCRIPTION:     Output the shipments per hour. This will only really  */
/*                      produce sensible data if the shipments are done in a  */
/*                      timely manner (i.e. not confirmed at the end of a day */
/*                      in one large lump).                                   */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   28/06/01 MJT  DCS    NEW5486  Client ID added                            */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   28/02/08 JH   DCS    DSP1645  Order/line count problem		      */
/******************************************************************************/

/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF 

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132

/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Productivity Measurement - Shipped Order Lines By Hour' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2
 
/* Column Headings and Formats */
COLUMN OrderByShipDay NEW_VALUE nOrderByShipDay NOPRINT
COLUMN Site_ID HEADING 'Site' FORMAT A10
COLUMN Client_ID HEADING 'Client' FORMAT A10
COLUMN Owner_ID HEADING 'Owner' FORMAT A10
COLUMN Shipped_Hour HEADING 'Date          Hour' FORMAT A18
COLUMN Shipped_Lines HEADING 'Lines Shipped' FORMAT 999999999

BREAK ON Site_ID SKIP PAGE ON Client_ID ON Owner_ID ON OrderByShipDay SKIP 1

COMPUTE SUM OF Shipped_Lines ON OrderByShipDay


SELECT	Site_ID,
	Client_ID,
	Owner_ID,
	TO_CHAR (Shipped_DStamp, 'YYYYMMDD') OrderByShipDay,
	TO_CHAR (Shipped_DStamp, 'DD-MON-YYYY   HH24') Shipped_Hour,
	COUNT (DISTINCT Order_ID || 'x' || Line_ID) Shipped_Lines
FROM Shipping_Manifest 
WHERE Shipped = 'Y'
AND Shipped_DStamp >= TO_DATE('&5', 'DD-MON-YYYY')
AND Shipped_DStamp < TO_DATE('&6', 'DD-MON-YYYY') + 1
AND (('&2' IS NULL) OR (Site_ID = '&2'))
AND (('&4' IS NULL) OR (Owner_ID = '&4'))
AND ((('&3' IS NULL) AND ('&7' IS NULL))
OR  (Client_ID = '&3')
OR  (('&3' IS NULL) AND Client_ID IN
(SELECT Client_ID 
FROM Client_Group_Clients
WHERE Client_Group = '&7')))  
GROUP BY Site_ID,
	 Client_ID,
	 Owner_ID,
	 TO_CHAR (Shipped_DStamp, 'YYYYMMDD'),
	 TO_CHAR (Shipped_DStamp, 'DD-MON-YYYY   HH24')
ORDER BY Site_ID,
	 Client_ID,
	 Owner_ID,
	 TO_CHAR (Shipped_DStamp, 'YYYYMMDD') desc,
	 TO_CHAR (Shipped_DStamp, 'DD-MON-YYYY   HH24')
/

SET TERMOUT ON

