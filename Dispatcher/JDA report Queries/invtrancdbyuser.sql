/******************************************************************************/
/*                                                                            */
/*   Logistics and Internet Systems Ltd                                       */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics and Internet Systems Ltd  */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Internet   */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Internet Systems Limited, 2002               */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*                                 LIS                                        */
/*                                                                            */
/*     FILE NAME  :     invtranscdbyuser.sql                                  */
/*                                                                            */
/*     DESCRIPTION:     List inventory transactions by user, by date range    */
/*                      with total elapsed times.                             */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   21/02/03 RMW  DCS    NEW6530  Productivity Reports                       */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   06/07/05 JH   DCS    PDR9627  Report problems			      */
/*   13/09/05 JRL  DCS    PDR9861  Zero exception picking problem	      */
/******************************************************************************/

/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132

/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Productivity Measurement - Total Tasks and Elapsed Time By User' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 1 -
LEFT 'Date Range From &4 To &5' SKIP 3

/* Column Headings and Formats */
COLUMN User_ID HEADING 'User' FORMAT A20
COLUMN Notes HEADING 'Name' FORMAT A40 WRAP
COLUMN Site_WZ HEADING 'Site/|Work Zone' FORMAT A10 WORD_WRAP
COLUMN Work_Zone HEADING 'Work Zone' FORMAT A10
COLUMN Code HEADING 'Code' FORMAT A15
COLUMN TaskCount HEADING 'Number|of Tasks' FORMAT '9G999G990' JUSTIFY LEFT
COLUMN Total HEADING 'Total Time|H:MM:SS' FORMAT A12
COLUMN Average HEADING 'Average Time|H:MM:SS' FORMAT A12

BREAK ON User_ID ON Notes SKIP 1 ON Site_WZ SKIP 1 

SELECT	IT.User_ID,
	AU.Notes,
	RPAD(IT.Site_ID, 10) || L.Work_Zone Site_WZ,
	IT.Code,
	COUNT (*) TaskCount,
	LPAD (LibDate.ConvertSecsToHMS (SUM (IT.Elapsed_Time)), 12) Total,
	LPAD (LibDate.ConvertSecsToHMS (AVG (IT.ELAPSED_TIME)), 12) Average
FROM Inventory_Transaction IT, Location L, Application_User AU
WHERE (IT.DStamp BETWEEN TO_DATE('&4', 'DD-MON-YYYY')
AND TO_DATE('&5', 'DD-MON-YYYY') + 1)
AND IT.Code IN ('Consol' ,
		'Kit Build',
		'Marshal',
		'Pick',
		'Putaway',
		'Receipt',
		'Relocate',
		'Repack',
		'Replenish',
		'Shipment',
		'Stock Check')
AND (IT.Site_ID = '&2' OR '&2' IS NULL)
AND (IT.User_ID = '&3' OR '&3' IS NULL)
AND IT.User_ID <> 'Datagen'
AND IT.From_Loc_ID = L.Location_ID
AND IT.Site_ID = L.Site_ID
AND IT.User_ID = AU.User_ID
AND IT.Summary_Record = 'Y'
AND ((IT.Code = 'Pick' and IT.Update_Qty > '0') or (IT.Code <> 'Pick'))
GROUP BY IT.User_ID, AU.Notes, IT.Site_ID, L.Work_Zone, IT.Code
ORDER BY IT.User_ID, AU.Notes, IT.Site_ID, L.Work_Zone, IT.Code
/


SET TERMOUT ON
