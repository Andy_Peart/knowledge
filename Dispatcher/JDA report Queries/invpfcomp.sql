/******************************************************************************/
/*                                                                            */
/*   Logistics and Internet Systems Ltd                                       */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics & Internet Systems Ltd    */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Internet   */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Internet Systems Limited, 2003               */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*                                  LIS                                       */
/*                                                                            */
/*     FILE NAME  :     invpfcomp.sql                                         */
/*                                                                            */
/*     DESCRIPTION:     Compare pick faces and the inventory in them.         */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   20/08/03 RMW  DCS    NEW6806  Additional reports from projects           */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   04/08/05 DM   DCS    NEW7858  Update SKU to 30 charcters                 */
/******************************************************************************/

/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132


/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Inventory in wrong Pick Face Report' -
RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2

/* Column Headings and Formats */
COLUMN PF_Client_ID HEADING 'Pick Face|Client ID' FORMAT A10
COLUMN PF_SKU_ID HEADING 'Pick Face|SKU ID' FORMAT A30
COLUMN PF_LOCATION_ID HEADING 'Pick Face|Location' FORMAT A19
COLUMN I_Client_ID HEADING 'Inventory|Client ID' FORMAT A10
COLUMN I_SKU_ID HEADING 'Inventory|SKU ID' FORMAT A30
COLUMN Qty_On_Hand HEADING 'Inventory|Qty On Hand' FORMAT A19

/* SQL Select statement */
SELECT	PF.Client_ID PF_Client_ID,
	PF.SKU_ID PF_SKU_ID,
	PF.Location_ID PF_Location_ID,
	I.Client_ID I_Client_ID,
	I.SKU_ID I_SKU_ID,
	LPAD (TO_CHAR (I.Qty_On_Hand,'999G999G990D999999'), 19) Qty_On_Hand
FROM Inventory i, Pick_Face PF
WHERE (('&2' IS NULL) OR (PF.Site_ID = '&2'))
AND (('&3' IS NULL) OR (PF.Client_ID = '&3'))
AND I.Location_ID = PF.Location_ID
AND I.Site_ID = PF.Site_ID
AND (I.Client_ID != PF.Client_ID OR I.SKU_ID != PF.SKU_ID)
ORDER BY I.Location_ID;

SET TERMOUT ON
