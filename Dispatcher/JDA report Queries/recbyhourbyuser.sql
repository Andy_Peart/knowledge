/******************************************************************************/
/*                                                                            */
/*   Logistics and Internet Systems Ltd                                       */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics & Internet Systems Ltd    */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Internet   */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Internet Systems Limited, 2003               */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*                                  LIS                                       */
/*                                                                            */
/*     FILE NAME  :     recbyhourbyuser.sql                                   */
/*                                                                            */
/*     DESCRIPTION:     Number of Tasks, Tags, Pallets, Eaches and Receipts   */
/*                      by user by hour for a specified date                  */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   20/08/03 RMW  DCS    NEW6806  Additional reports from projects           */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/******************************************************************************/

/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132


BREAK ON REPORT

COLUMN Site_ID HEADING 'Site'
COLUMN Owner_ID HEADING 'Owner'
COLUMN client_ID HEADING 'Client'
COLUMN User_ID HEADING 'User'
COLUMN Day HEADING 'Date'
COLUMN Hour HEADING 'Hour' FORMAT A4
COLUMN K heading 'Transactions'
COLUMN T HEADING 'Tags'
COLUMN P HEADING 'Pallets'
COLUMN E HEADING 'Eaches'
COLUMN R HEADING 'References'

BREAK ON Site_ID SKIP PAGE ON Client_ID ON Owner_ID ON User_ID SKIP 1

COMPUTE SUM LABEL 'Total' of K on user_id
COMPUTE SUM LABEL 'Total' of T on user_id
COMPUTE SUM LABEL 'Total' of P on user_id
COMPUTE SUM LABEL 'Total' of E on user_id
COMPUTE SUM LABEL 'Total' of R on user_id

/* Top Title */
TTITLE CENTER _SITENAME SKIP 2 -
LEFT "Receipt Summary by Hour by User for &5" -
RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2


SELECT	ITL.Site_ID,
	ITL.Client_ID,
	ITL.Owner_ID,
	ITL.User_ID,
	TO_CHAR (ITL.DStamp, 'DD-MON-YYYY') day,
	TO_CHAR (ITL.DStamp, 'HH24') hour,
	COUNT (ITL.Key) K,
	COUNT (DISTINCT (ITL.Tag_id)) T,
	COUNT (DISTINCT (ITL.Pallet_id)) P,
	SUM (ITL.Update_Qty) E,
	COUNT (DISTINCT (ITL.Reference_ID)) R
FROM Inventory_Transaction ITL
WHERE TO_CHAR (ITL.DStamp, 'DD-MON-YYYY') = UPPER ('&5')
AND ITL.Station_id <> 'Automatic'
AND ITL.Code = 'Receipt'
AND (('&2' IS NULL) OR (ITL.Site_ID = '&2'))
AND (('&3' IS NULL) OR (ITL.Client_ID = '&3'))
AND (('&4' IS NULL) OR (ITL.Owner_ID = '&4'))
GROUP BY ITL.User_ID,
	 ITL.Site_ID,
	 ITL.Client_id,
	 ITL.Owner_id,
	 TO_CHAR (ITL.DStamp, 'DD-MON-YYYY'),
	 TO_CHAR (ITL.DStamp, 'HH24')
ORDER BY ITL.User_ID,
	 ITL.Site_ID,
	 ITL.Client_ID,
	 ITL.Owner_ID,
	 TO_CHAR (ITL.DStamp, 'DD-MON-YYYY'),
	 TO_CHAR (ITL.DStamp, 'HH24');

SET TERMOUT ON
