/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics & Industrial Systems Ltd  */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1996             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*                                  LIS                                       */
/*                                                                            */
/*     FILE NAME  :     preadvbyadvcid.sql                                    */
/*                                                                            */
/*     DESCRIPTION:     List the pre-advice data by pre-advice ID.            */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   28/12/95 RMW  DCS    N/A      initial version                            */
/*   25/04/97 MJT  DCS    PDR5924  Modified to handle european group & decimal*/
/*                                 separators.                                */
/*   07/08/97 MJT  N/A    N/A      Changed number format to display 0         */
/*   06/02/98 JH   DCS    PDR5915  Reports need to be multi-sited             */
/*   18/04/98 RMW  DCS    NEW3196  PeopleSoft Interface (Supplier_ID length)  */
/*   28/07/98 MJT  DCS    NEW3188  Third Party Warehousing                    */
/*   16/06/99 MJT  DCS    PDR7593  Some sql*plus reports are wrapping         */
/*   12/12/00 RMC  DCS    NEW3998  Remove Bookref_ID for Dock Scheduler       */
/*   27/06/01 MJT  DCS    NEW5486  Client ID added                            */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   04/08/05 DM   DCS    NEW7858  Increase SKU to 30 characters              */
/******************************************************************************/

/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132

/* Set up report variables */
COLUMN PreAdviceID NEW_VALUE VPRE_ADVICE_ID NOPRINT
COLUMN SupplierID NEW_VALUE VSUPPLIER_ID NOPRINT
COLUMN Status NEW_VALUE VSTATUS NOPRINT
COLUMN Actual_DStamp NEW_VALUE VACTUAL_DSTAMP NOPRINT
COLUMN Due_DStamp NEW_VALUE VDUE_DSTAMP NOPRINT
COLUMN Notes NEW_VALUE VNOTES NOPRINT

/* Top Title */
TTITLE CENTER _SITENAME SKIP 2 -
LEFT 'Pre-Advice For ' VPRE_ADVICE_ID RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2 -
LEFT 'Supplier   : ' VSUPPLIER_ID -
RIGHT 'Due Date   : ' VDUE_DSTAMP SKIP 1 -
RIGHT 'Actual Date: ' VACTUAL_DSTAMP  SKIP 2 -
LEFT 'Notes      : ' FORMAT A100 VNOTES SKIP 2

/* Column Headings and Formats */
COLUMN Site_Client HEADING 'Site/|Client' FORMAT A10 WORD_WRAP
COLUMN Owner_ID HEADING 'Owner' FORMAT A10
COLUMN Line_ID HEADING 'Line' FORMAT 999,999
COLUMN sDescription HEADING 'SKU/|Description' FORMAT A30 WORD_WRAP
COLUMN Config_ID HEADING 'Pack Config/|Cond Code' FORMAT A15
COLUMN Batch_Data HEADING 'Batch/|Expiry Date' FORMAT A15 WRAP
COLUMN cQuantities HEADING 'Qty Due/|Qty Received' FORMAT A19

BREAK ON Site_Client SKIP PAGE

/* SQL Select statement */
SELECT  RPAD (PAH.Site_ID, 10) || PAH.Client_ID Site_Client,
	PAH.Owner_ID,
	PAL.Line_ID,
	RPAD (PAL. SKU_ID, 30) || S.Description sDescription,
	RPAD (NVL (PAL.Config_ID, ' '), 15) || PAL.Condition_ID Config_ID,
	RPAD (NVL (PAL.Batch_ID, ' '), 15) || 
	TO_CHAR (PAL.Expiry_DStamp, 'DD-MON-YYYY') Batch_Data,
	RPAD (TO_CHAR(PAL.Qty_Due,'999G999G990D999999'), 19) ||
	TO_CHAR(NVL (PAL.Qty_Received, 0),'999G999G990D999999') cQuantities,
	RPAD (PAH.Pre_Advice_ID, 20) PreAdviceID,
	RTRIM (RPAD (NVL (PAH.Supplier_ID, ' '), 35), 35) SupplierID,
	RTRIM (RPAD (NVL (TO_CHAR (PAH.Due_DStamp, 'DD-MON-YYYY'), ' '), 80), 80) Due_DStamp,
	RTRIM (RPAD (NVL (TO_CHAR (PAH.Actual_DStamp, 'DD-MON-YYYY'),' '), 80), 80) Actual_DStamp,
	PAH.Status,
	PAH.Notes
FROM SKU S, Pre_Advice_Line PAL, Pre_Advice_Header PAH
WHERE PAL.Pre_Advice_ID = PAH.Pre_Advice_ID
AND PAL.Client_ID = PAH.Client_ID
AND PAL.SKU_ID = S.SKU_ID
AND PAL.Client_ID = S.Client_ID
AND PAH.Pre_Advice_ID = '&5'
AND (('&2' IS NULL) OR (PAH.Site_ID = '&2'))
AND (('&4' IS NULL) OR (PAH.Owner_ID = '&4'))
AND ((('&3' IS NULL) AND ('&6' IS NULL))
OR  (PAH.Client_ID = '&3')
OR  (('&3' IS NULL) AND PAH.Client_ID IN
(SELECT Client_ID 
FROM Client_Group_Clients
WHERE Client_Group = '&6')))  
ORDER BY PAH.Site_ID, PAH.Client_ID, PAH.Owner_ID, PAL.Line_ID
;

SET TERMOUT ON


