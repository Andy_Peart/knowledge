/******************************************************************************/
/*                                                                            */
/*   Logistics and Internet Systems Ltd                                       */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics & Internet Systems Ltd    */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Internet   */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Internet Systems Limited, 2003               */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*                                  LIS                                       */
/*                                                                            */
/*     FILE NAME  :     skurecbydate.sql                                      */
/*                                                                            */
/*     DESCRIPTION:     Skus received by work zone by date range              */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   20/08/03 RMW  DCS    NEW6806  Additional reports from projects           */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   06/07/05 JH   DCS    PDR9627  Report problems			      */
/*   08/08/05 DM   DCS    NEW7858  Extend SKU to 30 characters                */
/******************************************************************************/


/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF
SET TERMOUT ON
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132


/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Receipts in a Work Zone By Date Range' -
RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2 -
LEFT 'Date Range From &5 To &6' SKIP 3

/* Column Headings and Formats */
COLUMN Site_ID HEADING 'Site' FORMAT A10
COLUMN Client_ID HEADING 'Client' FORMAT A10
COLUMN Owner_ID HEADING 'Owner' FORMAT A10
COLUMN SKU_ID HEADING 'SKU' FORMAT A30
COLUMN Description HEADING 'Description' FORMAT A30
COLUMN Receipts HEADING 'Receipts' FORMAT 999G999G990
COLUMN Eaches HEADING 'Eaches' FORMAT 999G999G990
COLUMN Work_Zone HEADING 'Work Zone' FORMAT A10

BREAK ON Site_ID SKIP PAGE ON Client_ID SKIP 1 ON Owner_ID ON SKU_ID ON Description ON Work_Zone

SELECT	ITL.Site_ID,
	ITL.Client_ID,
	ITL.Owner_ID,
	ITL.SKU_ID,
	S.Description,
	L.Work_Zone,
	COUNT (*) Receipts,
	SUM (ITL.Update_Qty) Eaches
FROM Inventory_Transaction ITL, Location L, SKU S
WHERE (('&2' IS NULL) OR (ITL.Site_ID = '&2'))
AND (('&3' IS NULL) OR (ITL.Client_ID = '&3'))
AND (('&4' IS NULL) OR (ITL.Owner_ID = '&4'))
AND ITL.Code = 'Receipt'
AND ITL.From_Loc_ID = L.Location_ID
AND ITL.Site_ID = L.Site_ID
AND (ITL.DStamp BETWEEN TO_DATE('&5', 'DD-MON-YYYY')
AND TO_DATE('&6', 'DD-MON-YYYY') + 1)
AND ITL.SKU_ID = S.SKU_ID
AND ITL.Client_ID = S.Client_ID
GROUP BY ITL.Site_ID,
	 ITL.Client_ID,
	 ITL.Owner_ID,
	 ITL.SKU_ID,
	 S.Description,
	 L.Work_Zone
ORDER BY ITL.Site_ID,
	 ITL.Client_ID,
	 ITL.Owner_ID,
	 ITL.SKU_ID,
	 S.Description,
	 L.Work_Zone;

SET TERMOUT ON
