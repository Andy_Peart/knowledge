/******************************************************************************/
/*                                                                            */
/*   Logistics and Internet Systems Ltd                                       */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics and Internet Systems Ltd  */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Internet   */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Internet Systems Limited, 2001               */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/
 
/******************************************************************************/
/*                                                                            */
/*                                 LIS                                        */
/*                                                                            */
/*     FILE NAME  :     linefill.sql                                          */
/*                                                                            */
/*     DESCRIPTION:     List Percentage of SKU's received late.               */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   05/03/01 DGP  DCS    NEW5300  initial version                            */
/*   26/06/01 MJT  DCS    NEW5486  Client ID added                            */
/*   29/08/02 RMW  DCS    NEW6186  Multi-client addresses                     */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/******************************************************************************/

/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF 
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132
 
/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Supplier Performance - SKUs fully received at due date - by Supplier and Date Range' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2 - 
LEFT 'Date Range From &4 To &5 For Supplier:  &3' SKIP 2 -
LEFT 'Due Date: ' dDate SKIP 2

/* Column Headings and Formats */
COLUMN SupplierID NEW_VALUE sSupplier NOPRINT
COLUMN DueDate NEW_VALUE dDate NOPRINT
COLUMN ClientID HEADING 'Client' FORMAT A10
COLUMN PreAdvice HEADING 'Pre Advice ID' FORMAT A20
COLUMN Percent HEADING '% of SKUs fully Received' FORMAT 999.99 WRAP

BREAK ON ClientID SKIP PAGE ON DueDate SKIP PAGE ON DSTAMP SKIP 1 ON PreAdvice

/* SQL Select statement */

SELECT	QtyDueEachPreAdvice.ClientID ClientID,
	QtyDueEachPreAdvice.DueDStamp DueDate,
	QtyDueEachPreAdvice.PreAdviceID PreAdvice,
	((sum(QtyRecEachPreAdvice.QtyRec) / sum(QtyDueEachPreAdvice.QtyDue)) * 100) Percent
FROM
	(
	-- Qty Due for each pre advice
	SELECT	PAH.Client_ID ClientID,
		PAH.Due_DStamp DueDStamp,
		PAH.Pre_Advice_ID PreAdviceID,
		Sum(PAL.Qty_Due) QtyDue
	FROM Pre_Advice_Header PAH, Pre_Advice_Line PAL
	WHERE PAH.Due_DStamp >= TO_DATE('&4', 'DD-MON-YYYY')
	AND PAH.Due_DStamp < TO_DATE('&5', 'DD-MON-YYYY') + 1
	AND PAH.Client_ID = '&2'
	AND PAH.Supplier_ID = '&3'
	AND PAL.Pre_Advice_ID = PAH.Pre_Advice_ID
	AND PAH.Client_ID = PAL.Client_ID
	GROUP BY PAH.Client_ID, PAH.Due_DStamp, PAH.Pre_Advice_ID) QtyDueEachPreAdvice,
	(
	-- Qty Received for each pre advice (over receiving NOT allowed by sku)
	SELECT QtyDueEachSKU.ClientID, QtyDueEachSKU.DueDStamp, QtyDueEachSKU.PreAdviceID,
	sum(least (QtyDueEachSKU.QtyReceived, QtyRecEachSKU.QtyReceived)) QtyRec
	FROM
	(
		-- Qty Due for each SKU for pre advice
		SELECT	PAH.Client_ID ClientID,
			PAH.Due_DStamp DueDStamp,
			PAH.Pre_Advice_ID PreAdviceID,
			PAL.SKU_ID SKUID,
			Sum(PAL.Qty_Due) QtyReceived
		FROM Pre_Advice_Header PAH, Pre_Advice_Line PAL
		WHERE PAH.Due_DStamp >= TO_DATE('&4', 'DD-MON-YYYY')
		AND PAH.Due_DStamp <  TO_DATE('&5', 'DD-MON-YYYY') + 1
		AND PAH.Client_ID = '&2'
		AND PAH.Supplier_ID = '&3'
		AND PAL.Pre_Advice_ID = PAH.Pre_Advice_ID
		AND PAL.Client_ID = PAH.Client_ID
		GROUP BY PAH.Client_ID, PAH.Due_DStamp, PAH.Pre_Advice_ID, PAL.SKU_ID) QtyDueEachSKU,
		-- Qty Received for each SKU for pre advice
		(SELECT	PAH.Client_ID CLientID,
			PAH.Due_DStamp DueDStamp,
			PAH.Pre_Advice_ID PreAdviceID,
			PAL.SKU_ID SKUID,
			Sum(ITL.Update_Qty) QtyReceived
		FROM Pre_Advice_Header PAH, Pre_Advice_Line PAL, Inventory_Transaction ITL
		WHERE PAH.Due_DStamp >= TO_DATE('&4', 'DD-MON-YYYY')
		AND PAH.Due_DStamp < TO_DATE('&5', 'DD-MON-YYYY') + 1
		AND PAH.Client_ID = '&2'
		AND PAH.Supplier_ID = '&3'
		AND PAL.Pre_Advice_ID = PAH.Pre_Advice_ID
		AND PAL.Client_ID = PAH.Client_ID
		AND ITL.Reference_ID = PAL.Pre_Advice_ID
		AND ITL.SKU_ID = PAL.SKU_ID
		AND ITL.Client_ID = PAL.Client_ID
		AND ITL.Code = 'Receipt'
		AND ITL.DStamp <= PAH.Due_DStamp
		GROUP BY PAH.Client_ID, PAH.Due_DStamp, PAH.Pre_Advice_ID, PAL.SKU_ID) QtyRecEachSKU
	WHERE QtyDueEachSKU.PreAdviceID = QtyRecEachSKU.PreAdviceID
	AND QtyDueEachSKU.ClientID = QtyRecEachSKU.ClientID
	AND QtyDueEachSKU.SKUID = QtyRecEachSKU.SKUID
	GROUP BY QtyDueEachSKU.ClientID, QtyDueEachSKU.DueDStamp, QtyDueEachSKU.PreAdviceID) QtyRecEachPreAdvice
WHERE QtyDueEachPreAdvice.PreAdviceID = QtyRecEachPreAdvice.PreAdviceID
AND   QtyDueEachPreAdvice.ClientID = QtyRecEachPreAdvice.ClientID
GROUP BY QtyDueEachPreAdvice.ClientID, QtyDueEachPreAdvice.DueDStamp, QtyDueEachPreAdvice.PreAdviceID
ORDER BY QtyDueEachPreAdvice.ClientID, QtyDueEachPreAdvice.DueDStamp, QtyDueEachPreAdvice.PreAdviceID
;

SET TERMOUT ON
