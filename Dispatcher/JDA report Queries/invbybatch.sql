/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics and Industrial Systems Ltd*/
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1995             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/
 
/******************************************************************************/
/*                                                                            */
/*                                 LIS                                        */
/*                                                                            */
/*     FILE NAME  :     invbybatch.sql                                        */
/*                                                                            */
/*     DESCRIPTION:     List inventory by batch range..                       */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   06/03/95 DJS  DCS    N/A      initial version                            */
/*   09/04/97 MJT  DCS    PDR5909  Cosmetic change concatenate SKU &          */
/*                                 description to stop line wrapping          */
/*   25/04/97 MJT  DCS    PDR5924  Modified to handle european group & decimal*/
/*                                 separators.                                */
/*   31/07/97 MJT  DCS    NEW2813  Modified to include new container id,      */
/*                                 pallet id fields in inventory              */
/*   06/02/98 JH   DCS    PDR5915  Reports need to be multi-sited             */
/*   24/07/98 MJT  DCS    NEW3188  Third Party Warehousing                    */
/*   18/04/99 JH   DCS    NEW3920  Extra/wider database columns		      */
/*   15/06/99 MJT  DCS    PDR7593  Some sql*plus reports are wrapping         */
/*   22/06/01 MJT  DCS    NEW5486  Client ID added                            */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   03/08/05 DM   DCS    NEW7858  Increase SKU to 30 charactres              */
/******************************************************************************/
 
/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132
 
/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Inventory By Batch Range' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2 - 
LEFT 'Batch Range From &5 To &6' SKIP 2
 
/* Avoid duplicate batch's */
BREAK ON Site_Client SKIP PAGE ON Owner_Batch SKIP 1
 
/* Column Headings and Formats */
COLUMN Site_Client HEADING 'Site/|Client' FORMAT A10 WORD_WRAP
COLUMN Owner_Batch HEADING 'Owner/|Batch' FORMAT A15 WORD_WRAP
COLUMN Location_ID_Condition_ID HEADING 'Location/|Condition' FORMAT A10 WORD_WRAP
COLUMN Tag_ID_Container_Pallet_ID HEADING 'Tag/|Container/|Pallet' FORMAT A20 WORD_WRAP
COLUMN sDescription HEADING 'SKU/|Description' FORMAT A40 WORD_WRAP
COLUMN Quant_On_Hand_Allocated HEADING 'Qty On Hand/|Allocated' FORMAT A19 WRAP
COLUMN Receipt_DStamp HEADING 'Receipt/|Expiry|Dates' FORMAT A11 WORD_WRAP
 
/* SQL Select statement */
SELECT	RPAD (Site_ID, 10) || Client_ID Site_Client,
	RPAD (Owner_ID, 15) || Batch_ID Owner_Batch,
	TO_CHAR (Receipt_DStamp, 'DD-MON-YYYY') || 
	TO_CHAR (Expiry_DStamp, 'DD-MON-YYYY') Receipt_DStamp,
	RPAD(Location_ID, 10) || Condition_ID Location_ID_Condition_ID,
	RPAD (NVL (Tag_ID, ' '), 20) || RPAD (NVL (Container_ID, ' '), 20) || Pallet_ID Tag_ID_Container_Pallet_ID,
	RPAD (SKU_ID, 40) || Description sDescription,
	LPAD (TO_CHAR (Qty_On_Hand,'999G999G990D999999'), 19) ||
	TO_CHAR (Qty_Allocated,'999G999G990D999999') Quant_On_Hand_Allocated
FROM Inventory
WHERE Batch_ID BETWEEN '&5' AND '&6'
AND (('&2' IS NULL) OR (Site_ID = '&2'))
AND (('&4' IS NULL) OR (Owner_ID = '&4'))
AND ((('&3' IS NULL) AND ('&7' IS NULL))
OR  (Client_ID = '&3')
OR  (('&3' IS NULL) AND Client_ID IN
	(SELECT Client_ID 
	 FROM Client_Group_Clients
	 WHERE Client_Group = '&7')))  
ORDER BY Site_ID, Client_ID, Owner_ID, Batch_ID
; 
 
SET TERMOUT ON
 




