/******************************************************************************/
/*                                                                            */
/*   Logistics and Internet Systems Ltd                                       */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics & Internet Systems Ltd    */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Internet   */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Internet Systems Limited, 2002               */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/
/******************************************************************************/
/*                                                                            */
/*                                  LIS                                       */
/*                                                                            */
/*     FILE NAME  :     skucountbyzone.sql                                    */
/*                                                                            */
/*     DESCRIPTION:     List the SKUs and the number of locations that are    */
/*                      taken by the SKU in a specified zone.                 */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   08/08/05 DM   DCS    NEW7858  Extend SKU to 30 characters                */
/******************************************************************************/

/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF 
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132
 
/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Locations used for SKU in Site: &2 and Zone: &3' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2

BREAK ON Client_ID
 
/* Column Headings and Formats */
COLUMN Client_ID HEADING 'Client' FORMAT A10
COLUMN SKU_ID HEADING 'SKU ID' FORMAT A30
COLUMN Description HEADING 'Description' FORMAT A40
COLUMN LocCount HEADING 'Locations|Used' FORMAT 9999999999 JUSTIFY LEFT
 
/* SQL Select statement */
SELECT Client_ID, SKU_ID, Description, COUNT (Location_ID) LocCount
FROM Inventory
WHERE Site_ID = '&2'
AND Zone_1 = '&3'
GROUP BY Client_ID, SKU_ID, Description
ORDER BY 4 DESC, 1, 2;
 
SET TERMOUT ON

