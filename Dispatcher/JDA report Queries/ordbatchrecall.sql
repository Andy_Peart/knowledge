/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics & Industrial Systems Ltd  */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1998             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*                                 LIS                                        */
/*                                                                            */
/*     FILE NAME  :     ordbatchrecall.sql                                    */
/*                                                                            */
/*     DESCRIPTION:     List order details which had a specified batch        */
/*                      shipped for them.                                     */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   12/01/98 RMW  DCS    N/A      initial version                            */
/*   06/02/98 JH   DCS    PDR5915  Reports need to be multi-sited             */
/*   28/07/98 MJT  DCS    NEW3188  Third Party Warehousing                    */
/*   04/10/98 RMW  DCS    NEW3555  Add country to address data                */
/*   16/06/99 MJT  DCS    PDR7593  Some sql*plus reports are wrapping         */
/*   20/08/99 RMD  DCS    NEW3760  Tan Data carrier selection                 */
/*   27/06/01 MJT  DCS    NEW5486  Client ID added                            */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/******************************************************************************/

/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132

/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Order Batch Recall' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2 -
LEFT 'Batch: &5' SKIP 2

BREAK ON Site_Client SKIP PAGE ON Owner_ID ON Order_ID SKIP 1

/* Column Headings and Formats */
COLUMN Shipped_Date NEW_VALUE nShipByDate NOPRINT
COLUMN Site_Client HEADING 'Site/|Client' FORMAT A10 WORD_WRAP
COLUMN Owner_ID HEADING 'Owner' FORMAT A10
COLUMN Order_Purchase_Order HEADING 'Order/|Purchase Order' FORMAT A25
COLUMN cAddress HEADING 'Address' FORMAT A35 WRAP
COLUMN cStatusShip HEADING 'Status/|Shipped|Date' FORMAT A11 WRAP
COLUMN Quant_Shipped HEADING 'Qty Shipped' FORMAT '999G999G990D999999'

/* SQL Select statement */
SELECT	DISTINCT RPAD (OHA.From_Site_ID, 10) || OHA.Client_ID Site_Client,
	OHA.Owner_ID, 
	RPAD(OHA.Order_ID, 25) || OHA.Purchase_Order Order_Purchase_Order,
	OHA.Shipped_Date,
	RPAD (NVL (OHA.Status, ' '), 11) || 
	TO_CHAR (OHA.Shipped_Date, 'DD-MON-YYYY') cStatusShip,
	RPAD (NVL (OHA.Contact, ' '), 35) || 
	RPAD (NVL (OHA.Name, ' '), 35) || 
	RPAD (NVL (OHA.Address1, ' '), 35) || 
	RPAD (NVL (OHA.Address2, ' '), 35) ||
	RPAD (NVL (OHA.Town, ' '), 35) || 
	RPAD (NVL (OHA.County, ' '), 35) || 
	RPAD (NVL (OHA.Postcode, ' '), 35) ||
	RPAD (NVL (LibLanguage.GetTranslation(OHA.Country), ' '), 35) ||
	RPAD (NVL (OHA.Contact_Phone, ' '), 35) ||
	RPAD (NVL (OHA.Contact_Fax, ' '), 35) ||
	OHA.Contact_EMail cAddress,
	TO_CHAR (OLA.Qty_Shipped,'999G999G990D999999') Quant_Shipped
FROM Shipping_Manifest_Archive SMA, Order_Line_Archive OLA, 
     Order_Header_Archive OHA
WHERE SMA.Batch_ID = '&5'
AND SMA.Order_ID = OHA.Order_ID
AND SMA.Client_ID = OHA.Client_ID 
AND SMA.Order_ID = OLA.Order_ID
AND SMA.Client_ID = OLA.Client_ID
AND SMA.Line_ID = OLA.Line_ID
AND (('&2' IS NULL) OR (SMA.Site_ID = '&2'))
AND (('&4' IS NULL) OR (OHA.Owner_ID = '&4'))
AND ((('&3' IS NULL) AND ('&6' IS NULL))
OR  (OHA.Client_ID = '&3')
OR  (('&3' IS NULL) AND OHA.Client_ID IN
(SELECT Client_ID 
FROM Client_Group_Clients
WHERE Client_Group = '&6')))  
UNION
SELECT	DISTINCT RPAD(OH.From_Site_ID, 10) || OH.Client_ID Site_Client,
	OH.Owner_ID, 
	RPAD(OH.Order_ID, 25) || OH.Purchase_Order Order_Purchase_Order,
	OH.Shipped_Date,
	RPAD (NVL (OH.Status, ' '), 11) || 
	TO_CHAR (OH.Shipped_Date, 'DD-MON-YYYY') cStatusShip,
	RPAD (NVL (OH.Contact, ' '), 35) || 
	RPAD (NVL (OH.Name, ' '), 35) || 
	RPAD (NVL (OH.Address1, ' '), 35) || 
	RPAD (NVL (OH.Address2, ' '), 35) ||
	RPAD (NVL (OH.Town, ' '), 35) || 
	RPAD (NVL (OH.County, ' '), 35) || 
	RPAD (NVL (OH.Postcode, ' '), 35) ||
	RPAD (NVL (OH.Country, ' '), 35) ||
	RPAD (NVL (OH.Contact_Phone, ' '), 35) ||
	RPAD (NVL (OH.Contact_Fax, ' '), 35) ||
	OH.Contact_EMail cAddress,
	TO_CHAR (OL.Qty_Shipped,'999G999G990D999999') Quant_Shipped
FROM Shipping_Manifest SM, Order_Line OL, Order_Header OH
WHERE SM.Batch_ID = '&5'
AND SM.Order_ID = OH.Order_ID
AND SM.Client_ID = OH.Client_ID 
AND SM.Order_ID = OL.Order_ID
AND SM.Client_ID = OL.Client_ID
AND SM.Line_ID = OL.Line_ID
AND (('&2' IS NULL) OR (SM.Site_ID = '&2'))
AND (('&4' IS NULL) OR (OH.Owner_ID = '&4'))
AND ((('&3' IS NULL) AND ('&6' IS NULL))
OR  (OH.Client_ID = '&3')
OR  (('&3' IS NULL) AND OH.Client_ID IN
(SELECT Client_ID 
FROM Client_Group_Clients
WHERE Client_Group = '&6')))  
ORDER BY 1, 2, 3, 4, 5
;

SET TERMOUT ON




