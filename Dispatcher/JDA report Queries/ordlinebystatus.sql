/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics & Industrial Systems Ltd  */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LEXLOGISTiX.                                 */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1995             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*                             LEXLOGISTiX                                    */
/*                                                                            */
/*     FILE NAME  :     ordlinebystatus.sql                                   */
/*                                                                            */
/*     DESCRIPTION:     List the Orders by Status.                            */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   17/03/95 RMW  DCS    N/A      initial version                            */
/*   25/04/97 MJT  DCS    PDR5924  Modified to handle european group & decimal*/
/*                                 separators.                                */
/*   07/08/97 MJT  N/A    N/A      Changed number format to display 0         */
/*   06/02/98 JH   DCS    PDR5915  Reports need to be multi-sited             */
/*   28/07/98 MJT  DCS    NEW3188  Third Party Warehousing                    */
/*   16/06/99 MJT  DCS    PDR7593  Some sql*plus reports are wrapping         */
/*   27/06/01 MJT  DCS    NEW5486  Client ID added                            */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   04/08/05 DM   DCS    NEW7858  Extend SKU to 30 characters                */
/******************************************************************************/

/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132

/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Orders And Lines By Status' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2 -
LEFT 'Order Status &5' SKIP 2

BREAK ON Site_Client ON Owner_ID SKIP PAGE ON Order_ID SKIP 1

/* Column Headings and Formats */
COLUMN Site_Client HEADING 'Site/|Client' FORMAT A10 WORD_WRAP
COLUMN cOrderID HEADING 'Order/|Line' FORMAT A20 WRAP
COLUMN Owner_SKU HEADING 'Owner/|SKU' FORMAT A30
COLUMN Tracking_Level HEADING 'Tracking|Level' FORMAT A8
COLUMN Quant_Ordered_Shipped HEADING 'Qty Ordered/|Qty Shipped' FORMAT A19 WRAP
COLUMN Order_Date HEADING 'Order/Ship|By Dates' FORMAT A11 WRAP
COLUMN Namecon HEADING 'Name/Consignment' FORMAT A20 WRAP

/* SQL Select statement */
SELECT	RPAD (OH.From_Site_ID, 10) || OH.Client_ID Site_Client,
	RPAD (OH.Order_ID, 20) || OL.Line_ID cOrderID,
	RPAD(Owner_ID, 30) || OL.SKU_ID Owner_SKU,
	OL.Tracking_Level,
	LPAD(TO_CHAR(OL.Qty_Ordered,'999G999G990D999999'), 19) ||
	LPAD(TO_CHAR(OL.Qty_Shipped,'999G999G990D999999'), 19) Quant_Ordered_Shipped,
        TO_CHAR (OH.Order_Date, 'DD-MON-YYYY') || 
	TO_CHAR (OH.Ship_By_Date, 'DD-MON-YYYY') Order_Date,
        RPAD (NVL (OH.Name, ' '), 20) || OH.Consignment Namecon
FROM Order_Header OH, Order_Line OL
WHERE OH.Order_ID = OL.Order_ID
AND OH.Status = '&5'
AND (('&2' IS NULL) OR (OH.From_Site_ID = '&2'))
AND (('&4' IS NULL) OR (OH.Owner_ID = '&4'))
AND ((('&3' IS NULL) AND ('&6' IS NULL))
OR  (OH.Client_ID = '&3')
OR  (('&3' IS NULL) AND OH.Client_ID IN
(SELECT Client_ID 
FROM Client_Group_Clients
WHERE Client_Group = '&6')))  
ORDER BY OH.From_Site_ID, OH.Client_ID, OH.Owner_ID, OH.Order_ID, OL.Line_ID
;

SET TERMOUT ON

