/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics & Industrial Systems Ltd  */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1996             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*                                  LIS                                       */
/*                                                                            */
/*     FILE NAME  :     preadvshrcvbydate.sql                                 */
/*                                                                            */
/*     DESCRIPTION:     List the pre-advice lines not fully received within   */
/*                      a date range.                                         */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   28/10/96 RMW  DCS    N/A      initial version                            */
/*   28/07/98 MJT  DCS    NEW3188  Third Party Warehousing                    */
/*   22/03/00 RMW  DCS    PDR8189  Null quantity received not handled         */
/*   12/12/00 RMC  DCS    NEW3998  Bookref ID not used by Dock Scheduler      */
/*   27/06/01 MJT  DCS    NEW5486  Client ID added                            */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   28/02/08 JH   DCS    DSP1643  Quantity due tracking level problem        */
/******************************************************************************/

/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132

/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Pre-Advices Received Short By Date Range' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2 - 
LEFT 'Date From &5 To &6' SKIP 2
 
BREAK ON Site_Client SKIP PAGE ON Pre_Advice_ID SKIP 1
 
/* Column Headings and Formats */
COLUMN Site_Client HEADING 'Site/|Client' FORMAT A10 WORD_WRAP
COLUMN Owner_Status HEADING 'Owner/|Status' FORMAT A11 WORD_WRAP
COLUMN cTaskID HEADING 'Pre-Advice/|Line'  FORMAT A20
COLUMN sDescription HEADING 'SKU/|Description' FORMAT A32 WORD_WRAP
COLUMN Config_ID HEADING 'Pack Config/|Cond Code' FORMAT A15
COLUMN Quant_Due HEADING 'Qty Due' FORMAT A19
COLUMN cQtyReceived HEADING 'Qty Received' FORMAT A19

SELECT	RPAD (PAH.Site_ID, 10) || PAH.Client_ID Site_Client,
	RPAD (PAH.Owner_ID, 11) || PAH.Status Owner_Status,
	RPAD (PAH.Pre_Advice_ID, 20) || PAL.Line_id cTaskID,
	RPAD (PAL.SKU_ID, 32) || S.Description sDescription,
	RPAD (NVL (PAL.Config_ID, ' '), 15) || PAL.Condition_ID Config_ID,
	TO_CHAR(PAL.Qty_Due,'999G999G990D999999') Quant_Due,
	TO_CHAR(NVL (PAL.Qty_Received, 0),'999G999G990D999999') cQtyReceived
FROM SKU S, Pre_Advice_Line PAL, Pre_Advice_Header PAH
WHERE PAL.Pre_Advice_id = PAH.Pre_Advice_ID
AND PAL.Client_ID = PAH.Client_ID
AND PAL.SKU_ID = S.SKU_ID
AND PAL.Client_ID = S.Client_ID
AND NVL (PAL.Qty_Received, 0) < LibConfig.GetEachQuantity(PAL.Qty_Due, PAL.Config_ID, PAL.Client_ID, PAL.SKU_ID, PAL.Tracking_Level)
AND PAH.Status IN ('Complete', 'In Progress')
AND PAH.Due_DStamp >= TO_DATE('&5', 'DD-MON-YYYY') 
AND PAH.Due_DStamp < TO_DATE('&6', 'DD-MON-YYYY') + 1
AND (('&2' IS NULL) OR (PAH.Site_ID = '&2'))
AND (('&4' IS NULL) OR (PAH.Owner_ID = '&4'))
AND ((('&3' IS NULL) AND ('&7' IS NULL))
OR  (PAH.Client_ID = '&3')
OR  (('&3' IS NULL) AND PAH.Client_ID IN
(SELECT Client_ID 
FROM Client_Group_Clients
WHERE Client_Group = '&7')))  
ORDER BY PAH.Site_ID, PAH.Client_ID, PAH.Owner_ID, PAH.Due_DStamp,
PAH.Pre_Advice_ID, PAL.Line_ID
;
