/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics & Industrial Systems Ltd  */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LEXLOGISTiX.                                 */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1995             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/
 
/******************************************************************************/
/*                                                                            */
/*                             LEXLOGISTiX                                    */
/*                                                                            */
/*     FILE NAME  :     sernumbyordran.sql                                    */
/*                                                                            */
/*     DESCRIPTION:     List Order_ID, Line_ID, SKU_ID, Qty_Shipped,          */
/*                      Serial_Number & Name for all Serial_Numbers with a    */
/*                      given Order_ID range, where order Status='Shipped'.   */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   03/04/97 MJT  DCS    2694     initial version                            */
/*   25/04/97 MJT  DCS    PDR5924  Modified to handle european group & decimal*/
/*                                 separators.                                */
/*   07/08/97 MJT  N/A    N/A      Changed number format to display 0         */
/*   06/02/98 JH   DCS    PDR5915  Reports need to be multi-sited             */
/*   28/07/98 MJT  DCS    NEW3188  Third Party Warehousing                    */
/*   18/04/99 JH   DCS    NEW3920  Extra/wider database columns		      */
/*   16/06/99 MJT  DCS    PDR7593  Some sql*plus reports are wrapping         */
/*   26/06/99 RMW  DCS    PDR6649  Join problem outputting to much data       */
/*   28/06/01 MJT  DCS    NEW5486  Client ID added                            */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   05/08/05 DM   DCS    NEW7858  Extend SKU to 30 characters                */
/*   12/03/08 TVD  DCS    DSP1520  Extend Serialnumber to 30 characters       */
/******************************************************************************/
 
/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132
  
/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Serial Numbers By Shipped Order Range' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2 - 
LEFT 'Order Range From &5 To &6' SKIP 2
 
/* Column Headings and Formats */
COLUMN From_Site_ID HEADING 'Site' FORMAT A10 
COLUMN Client_ID HEADING 'Client' FORMAT A10
COLUMN Owner_ID HEADING 'Owner ID' FORMAT A10
COLUMN Order_ID HEADING 'Order' FORMAT A20
COLUMN SKU_ID HEADING 'SKU' FORMAT A30
COLUMN QtyShipped HEADING 'Qty Shipped' FORMAT A19
COLUMN Serial_Number HEADING 'Serial Number' FORMAT A30
 
BREAK ON From_Site_ID ON Client_ID ON Owner_ID ON Order_ID SKIP 1 ON SKU_ID SKIP 1 ON QtyShipped

/* SQL Select statement */
SELECT	OH.From_Site_ID,
	OH.Client_ID, 
	OH.Owner_ID,
	OH.Order_ID,
	OL.SKU_ID,
	TO_CHAR (SUM (OL.Qty_Shipped),'999G999G990D999999') QtyShipped,
	SN.Serial_Number
FROM Order_Header OH, Order_Line OL, Serial_Number SN
WHERE SN.Order_ID = OL.Order_ID
AND SN.Client_ID = OL.Client_ID
AND SN.SKU_ID = OL.SKU_ID
AND OL.Order_ID = OH.Order_ID
AND OL.Client_ID = OH.Client_ID
AND OH.Order_ID BETWEEN '&5' AND '&6'
AND OH.Status = 'Shipped'
AND (('&2' IS NULL) OR (OH.From_Site_ID = '&2'))
AND (('&4' IS NULL) OR (OH.Owner_ID = '&4'))
AND ((('&3' IS NULL) AND ('&7' IS NULL))
OR  (OH.Client_ID = '&3')
OR  (('&3' IS NULL) AND OH.Client_ID IN
(SELECT Client_ID 
FROM Client_Group_Clients
WHERE Client_Group = '&7')))  
GROUP BY OH.From_Site_ID, OH.Client_ID, OH.Owner_ID, OH.Order_ID, OL.SKU_ID, SN.Serial_Number
ORDER BY OH.From_Site_ID, OH.Client_ID, OH.Owner_ID, OH.Order_ID, OL.SKU_ID, SN.Serial_Number;

SET TERMOUT ON
 

