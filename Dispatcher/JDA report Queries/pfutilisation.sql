/******************************************************************************/
/*                                                                            */
/*  Copyright (c) 2004                                                        */
/*  RedPrairie Corporation                                                    */
/*  All Rights Reserved                                                       */
/*                                                                            */
/*  The information in this file contains trade secrets and confidential      */
/*  information which is the property of RedPrairie Corporation.              */
/*                                                                            */
/*  All trademarks, trade names, copyrights and other intellectual property   */
/*  rights created, developed, embodied in or arising in connection with      */
/*  this software shall remain the sole property of RedPrairie Corporation.   */
/*                                                                            */
/*  Copyright (c) RedPrairie Corporation, 2004                                */
/*  ALL RIGHTS RESERVED                                                       */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :  pfutilisation.sql                                        */
/*                                                                            */
/*     DESCRIPTION:  Historical analysis of pick face utilisation by date     */
/*                   range.                                                   */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   04/03/05 KL   DCS    NEW7763  Initial version			      */
/******************************************************************************/

/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132

/* Top Title */
TTITLE CENTER _SITENAME SKIP 2 -
LEFT 'Historical analysis of PF utilisation' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
LEFT 'Date Range From &3 To &4' RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2


/* Column Headings and Formats */
COLUMN Site_ID HEADING 'Site ID' FORMAT A10
COLUMN Location_ID HEADING 'Location' FORMAT A10
COLUMN SKU_ID HEADING 'SKU ID' FORMAT A18
COLUMN Description HEADING 'Description' FORMAT A40
COLUMN Qty_On_Hand HEADING 'Qty On Hand' FORMAT A19
COLUMN TotalPicked HEADING 'Total Picked' FORMAT A19

/* Avoid duplicate sites */
BREAK ON Site_ID SKIP PAGE


/* SQL Select statement */
SELECT	PL.Site_ID,
	PL.Location_ID,
	PL.SKU_ID,
	S.Description,
	LPAD (TO_CHAR (PL.Qty_On_Hand, '999G999G990D999999'), 19) Qty_On_Hand,
	LPAD (TO_CHAR (SUM (Update_Qty), '999G999G990D999999'), 19) TotalPicked
FROM Inventory_Transaction ITL, Pick_Face PL, SKU S
WHERE ITL.Code = 'Pick'
AND ITL.From_Loc_ID = PL.Location_ID
AND ITL.Site_ID = PL.Site_ID
AND S.Client_ID = ITL.Client_ID
AND S.SKU_ID = ITL.SKU_ID
AND ITL.DStamp >= TO_DATE('&3', 'DD-MON-YYYY')
AND ITL.DStamp < TO_DATE('&4', 'DD-MON-YYYY') + 1
AND (('&2' IS NULL) OR (PL.Site_ID = '&2'))
GROUP BY PL.Site_ID, PL.Location_ID, PL.SKU_ID, PL.Qty_On_Hand, S.Description
ORDER BY PL.Site_ID, PL.Location_ID, PL.SKU_ID, PL.Qty_On_Hand;

SET TERMOUT ON
