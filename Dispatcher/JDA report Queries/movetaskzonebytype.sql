/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics & Industrial Systems Ltd  */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1995             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/
 
/******************************************************************************/
/*                                                                            */
/*                                  LIS                                       */
/*                                                                            */
/*     FILE NAME  :     movetaskzonebytype.sql                                */
/*                                                                            */
/*     DESCRIPTION:     List allocation shortages by Work Group               */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   31/10/95 CT   DCS    N/A      initial version                            */
/*   25/04/97 MJT  DCS    PDR5924  Modified to handle european group & decimal*/
/*                                 separators.                                */
/*   7/08/97  MJT  DCS    NEW2813  Modified to include new container id,      */
/*                                 pallet id fields in inventory              */
/*   06/02/98 JH   DCS    PDR5915  Reports need to be multi-sited             */
/*   27/07/98 MJT  DCS    NEW3188  Third Party Warehousing                    */
/*   11/11/98 JH   DCS    NEW3261  UPI receiving			      */
/*   18/04/99 JH   DCS    NEW3920  Extra/wider database columns		      */
/*   16/06/99 MJT  DCS    PDR7593  Some sql*plus reports are wrapping         */
/*   27/06/01 MJT  DCS    NEW5486  Client ID added                            */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   04/10/04 DGP  DCS    PDR9452  Incorrect results - missing brackets       */
/*   04/08/05 DM   DCS    NEW7858  Extend SKU to 30 characters                */
/*   23/11/05 JH   DCS    BUG1757  Translate report arguments                 */
/*   19/10/06 JH   DCS    ENH3120  Increase cons/wg to 20 chars               */
/******************************************************************************/
 
/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF

SET TERMOUT ON

CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132

TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Move Task Within Work Zone - By Task Type' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page ' FORMAT 999 SQL.PNO SKIP 2 -
LEFT 'Task Type &5' SKIP 2

BREAK ON Site_Client ON Owner_ID SKIP PAGE ON ROW SKIP 1

COLUMN Site_Client_Owner HEADING 'Site/|Client/|Owner' FORMAT A10 WORD_WRAP
COLUMN Owner_ID HEADING 'Owner' FORMAT A10
COLUMN cTaskID HEADING 'Task/|Line' FORMAT A20 WRAP
COLUMN sDescription HEADING 'SKU/|Description' FORMAT A25 WORD_WRAP
COLUMN Qty_Tag HEADING 'Quantity/|Tag' FORMAT A20 WRAP
COLUMN Locations HEADING 'From/To|Location' FORMAT A10 WORD_WRAP
COLUMN Work_Group_Zone HEADING 'Work Group/|Consignment/|Work Zone' FORMAT A20 WRAP
COLUMN Container_Pallet_ID HEADING 'Container/|Pallet' FORMAT A20 WRAP

SELECT	RPAD (Site_ID, 10) || RPAD (Client_ID, 10) || Owner_ID Site_Client_Owner,
	RPAD (Task_ID, 20) || Line_ID cTaskID,
	RPAD (SKU_ID, 25) || Description sDescription,
	RPAD (NVL (From_Loc_ID, ' '), 10) || To_Loc_ID Locations,
	LPAD(TO_CHAR(Qty_To_Move,'999G999G990D999999'), 20) || Tag_ID Qty_Tag,
	RPAD (NVL (Work_Group, ' '), 20) || RPAD(Consignment, 20) || Work_Zone Work_Group_Zone,
	RPAD (NVL (Container_ID, ' '), 20) || Pallet_ID Container_Pallet_ID
FROM Move_Task
WHERE (Task_Type = DECODE('&5', 'PUTAWAY', 'P',
				'RELOCATE', 'M',
				'REPLENISH', 'R',
				'ORDER', 'C',
				'REPACK', 'B',
				'MARSHAL', 'T',
				'KITTING', 'K',
				'UPI', 'U',
				'DUERECEIPT', 'D',
				'VAS', 'V',
				'EMPTYPALLET', 'E')
OR Task_Type = DECODE('&5', 'ORDER' ,'O'))
AND (('&2' IS NULL) OR (Site_ID = '&2'))
AND (('&4' IS NULL) OR (Owner_ID = '&4'))
AND ((('&3' IS NULL) AND ('&6' IS NULL))
OR  (Client_ID = '&3')
OR  (('&3' IS NULL) AND Client_ID IN
(SELECT Client_ID 
FROM Client_Group_Clients
WHERE Client_Group = '&6')))  
ORDER BY Site_ID, Client_ID, Owner_ID, Work_Zone, Task_ID, Line_ID, Sequence;

SET TERMOUT ON 

