/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics and Industrial Systems Ltd*/
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LEXLOGISTiX.                                 */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1995             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/
 
/******************************************************************************/
/*                                                                            */
/*                             LEXLOGISTiX                                    */
/*                                                                            */
/*     FILE NAME  :     customsinvbyloc.sql                                   */
/*                                                                            */
/*     DESCRIPTION:     Customs & Excise - List inventory by location range.  */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   11/12/03 DGP  DCS    NEW6997  Initial Version                            */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   03/08/05 DM   DCS    NEW7858  Increase SKU to 30 charactres              */
/******************************************************************************/
 
/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132
 
/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 - 
LEFT 'Customs and Excise - Inventory By Location Range' RIGHT _DATESTAMP SKIP 1 -        
RIGHT _TIMESTAMP SKIP 1 -                               
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2 -                
LEFT 'Location Range From &5 to &6' SKIP 2 

/* Avoid duplicate locations */
BREAK ON Site_Client SKIP PAGE ON Owner_Location SKIP 1
 
/* Column Headings and Formats */
COLUMN Site_Client HEADING 'Site/|Client' FORMAT A10 WORD_WRAP
COLUMN Tag_Batch HEADING 'Tag/|Batch' FORMAT A20 WORD_WRAP
COLUMN sDescription HEADING 'SKU/|Description' FORMAT A30 WRAP
COLUMN Owner_Location HEADING 'Owner/|Location' FORMAT A10 WORD_WRAP
COLUMN Container_Pallet_ID HEADING 'Container/|Pallet' FORMAT A20 WORD_WRAP
COLUMN Quant_On_Hand_Allocated HEADING 'Qty On Hand/|Allocated' FORMAT A19 WRAP
COLUMN Condition_ID HEADING 'Condition' FORMAT A9
COLUMN Receipt_DStamp HEADING 'Receipt/|Expiry|Dates' FORMAT A11 WORD_WRAP
COLUMN Rotation_Consignment HEADING 'Rotation ID/|Consignment ID' FORMAT A12 WORD_WRAP
COLUMN Receipt_UCR HEADING 'Receipt Type/|Unique Consign Ref' FORMAT A2 WORD_WRAP
COLUMN Originator HEADING 'Originator/|Originator Ref' FORMAT A15 WORD_WRAP
COLUMN Under_Bond_Date HEADING 'Under Bond/|Document Date' FORMAT A11 WORD_WRAP
 
/* SQL Select statement */
SELECT	RPAD (Site_ID, 10) || Client_ID Site_Client,
	RPAD (Owner_ID, 10) || Location_ID Owner_Location,
	RPAD (NVL(Tag_ID, ' '), 20) || Batch_ID Tag_Batch,
	RPAD (SKU_ID, 30) || Description sDescription,
	TO_CHAR (Receipt_DStamp, 'DD-MON-YYYY') || 
	TO_CHAR (Expiry_DStamp, 'DD-MON-YYYY') Receipt_DStamp,
	Condition_ID,
	RPAD (NVL (Container_ID, ' '), 20) || Pallet_ID Container_Pallet_ID,
	LPAD (TO_CHAR (Qty_On_Hand,'999G999G990D999999'), 19) ||
	TO_CHAR (Qty_Allocated,'999G999G990D999999') Quant_On_Hand_Allocated,
	RPAD (CE_Rotation_ID, 12) || 
	CE_Consignment_ID Rotation_Consignment,
	RPAD (CE_Receipt_Type, 2) || CE_UCR Receipt_UCR,
	RPAD (NVL (CE_Originator, ' '), 15) || 
	CE_Originator_Reference Originator,
	NVL (CE_Under_Bond, ' ') || ' ' ||
	TO_CHAR (CE_Document_DStamp, 'DD-MON-YYYY') Under_Bond_Date
FROM Inventory
WHERE Location_ID BETWEEN '&5' AND '&6'
AND (('&2' IS NULL) OR (Site_ID = '&2'))
AND (('&4' IS NULL) OR (Owner_ID = '&4'))
AND ((('&3' IS NULL) AND ('&7' IS NULL))
OR  (Client_ID = '&3')
OR  (('&3' IS NULL) AND Client_ID IN
	(SELECT Client_ID 
	 FROM Client_Group_Clients
	 WHERE Client_Group = '&7')))  
ORDER BY Site_ID, Client_ID, Owner_ID, Location_ID
; 
 
SET TERMOUT ON

