/******************************************************************************/
/*                                                                            */
/*   Logistics and Internet Systems Ltd                                       */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics and Internet Systems Ltd  */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Internet   */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Internet Systems Limited, 2001               */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*                                 LIS                                        */
/*                                                                            */
/*     FILE NAME  :     suppmeasure.sql                                       */
/*                                                                            */
/*     DESCRIPTION:     List Supplier Performance by Supplier and date range. */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   05/03/01 DGP  DCS    NEW5300  initial version                            */
/*   17/04/01 RE   DCS    NEW5300c initial version                            */
/*   29/08/02 RMW  DCS    NEW6186  Multi-client addresses                     */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/******************************************************************************/

/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132

/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Supplier Performance - by Supplier and Date Range' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2 -
LEFT 'Date Range From &4 To &5 For Supplier: &3' SKIP 2 -
LEFT 'Date: ' dDate SKIP 2

/* Column Headings and Formats */
COLUMN SupplierID NEW_VALUE sSupplier NOPRINT
COLUMN ActualDStamp NEW_VALUE dDate  NOPRINT
COLUMN QuestionID NOPRINT
COLUMN Question HEADING 'Question' FORMAT A80
COLUMN MaxMarks HEADING 'MaxMarks' FORMAT 999
COLUMN Weighting HEADING 'Weight' FORMAT 999
COLUMN TotalMarks HEADING 'Total' FORMAT 999 WRAP
COLUMN AvgMarks HEADING 'Average' FORMAT 999.99 WRAP
COLUMN Score HEADING 'Score' FORMAT 999.99 WRAP

BREAK ON ActualDStamp SKIP PAGE ON DSTAMP SKIP 1 ON Code
COMPUTE SUM of Weighting on ActualDStamp
COMPUTE SUM of TotalMarks on ActualDStamp
COMPUTE SUM of AvgMarks on ActualDStamp
COMPUTE SUM of Score on ActualDStamp

/* SQL Select statement */
SELECT 	SupPermformance.SupplierID,
	SupPermformance.ActualDStamp,
	SupPermformance.QuestionID,
	SupPermformance.Question,
	SupPermformance.MaxMarks,
	SupPermformance.Weighting,
	SupPermformance.TotalMarks,
	SupPermformance.AvgMarks,
	((SupPermformance.AvgMarks * SupPermformance.Weighting) /
					TotalWeighting.SumWeighting) Score
	FROM (SELECT SPS.Supplier_ID SupplierID,
	TO_CHAR(SP.Actual_DStamp, 'DD-MON-YYYY') ActualDStamp,
	SPS.Question_ID QuestionID,
	SPS.Text Question,
	SPS.Max_Marks MaxMarks,
	SPS.Weighting Weighting,
	SUM (SPS.Score) TotalMarks,
	AVG (SPS.Score) AvgMarks,
	((SUM (SPS.Score) / SUM (Max_Marks)) * SPS.Weighting) Score
	FROM Supplier_Performance SP, Supplier_Score SPS
	WHERE SP.Actual_DStamp >= TO_DATE('&4', 'DD-MON-YYYY')
	AND SP.Actual_DStamp < TO_DATE('&5', 'DD-MON-YYYY') + 1
	AND SP.Client_ID = '&2'
	AND SP.Supplier_ID = '&3'
	AND ((SPS.Supplier_ID = SP.Supplier_ID) OR (SPS.Supplier_ID is null))
	AND SPS.BookRef_ID = SP.BookRef_ID
	GROUP BY TO_CHAR(SP.Actual_DStamp, 'DD-MON-YYYY'), SPS.Supplier_ID,
	SPS.Question_ID, SPS.Text, SPS.Max_Marks,
	SPS.Weighting) SupPermformance,
	(SELECT SUM(SPQ.weighting) SumWeighting
	FROM Supplier_Questions SPQ
	WHERE SPQ.Client_ID = '&2'
	AND SPQ.Supplier_ID = '&3'
	OR SPQ.Supplier_ID IS NULL) TotalWeighting
ORDER BY 2, 1 DESC, 3 ASC
;

SET TERMOUT ON

