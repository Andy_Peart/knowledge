/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics and Industrial Systems Ltd*/
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LEXLOGISTiX.                                 */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1998             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/
 
/******************************************************************************/
/*                                                                            */
/*                             LEXLOGISTiX                                    */
/*                                                                            */
/*     FILE NAME  :     abcinvlocmismatch.sql                                 */
/*                                                                            */
/*     DESCRIPTION:     List abc inventory which is mismatched by site/owner. */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   11/11/98 JH   DCS    NEW3173  ABC/pareto analysis			      */
/*   18/04/99 JH   DCS    NEW3920  Extra/wider database columns		      */
/*   16/06/99 MJT  DCS    PDR7593  Some sql*plus reports are wrapping         */
/*   20/06/01 MJT  DCS    NEW5486  Client ID added                            */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   03/08/05 DM   DCS    NEW7858  Increase SKU to 30 charactres              */
/******************************************************************************/
 
/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132

/* Top Title */
TTITLE CENTER _SITENAME SKIP 2 - 
LEFT 'ABC Inventory Mismatch' RIGHT _DATESTAMP SKIP 1 -        
RIGHT _TIMESTAMP SKIP 1 -                               
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2

/* Avoid duplicates */
BREAK ON Site_Client SKIP PAGE ON Owner_ID SKIP 1 ON SKU_ID

/* Column Headings and Formats */
COLUMN Site_Client HEADING 'Site/|Client' FORMAT A10 WORD_WRAP
COLUMN Owner_ID HEADING 'Owner' FORMAT A10 WRAP
COLUMN SKU_ID HEADING 'SKU' FORMAT A30 WRAP
COLUMN Tag_ID HEADING 'Tag' FORMAT A20 WRAP
COLUMN Location_ID HEADING 'Location' FORMAT A10 WRAP
COLUMN Description HEADING 'Description' FORMAT A40 WRAP
COLUMN Actual_Required_Freq HEADING 'Actual|Frequency/|Required|Frequency' FORMAT A9 WORD_WRAP

/* SQL Select statement */
SELECT RPAD(I.Site_ID, 10) || I.Client_ID Site_Client,
	I.Owner_ID,
	I.SKU_ID,
	I.Location_ID,
	I.Tag_ID,
	S.Description,
	RPAD(NVL(L.ABC_Frequency, 'null'), 9) || NVL(SR.ABC_Frequency, 'null') Actual_Required_Freq
FROM Inventory I, Location L, SKU_Ranking SR, SKU S
WHERE ((I.Site_ID IS NULL AND L.Site_ID IS NULL AND SR.Site_ID IS NULL) OR (I.Site_ID = SR.Site_ID AND I.Site_ID = L.Site_ID))
AND ((I.Owner_ID IS NULL AND SR.Owner_ID IS NULL) OR (I.Owner_ID = SR.Owner_ID))
AND (('&2' IS NULL) OR (I.Site_ID = '&2'))
AND (('&4' IS NULL) OR (I.Owner_ID = '&4'))
AND I.Location_ID = L.Location_ID
AND ((SR.ABC_Frequency IS NULL AND L.ABC_Frequency IS NOT NULL)
OR (SR.ABC_Frequency IS NOT NULL AND L.ABC_Frequency IS NULL)
OR (SR.ABC_Frequency IS NOT NULL AND L.ABC_Frequency IS NOT NULL
AND SR.ABC_Frequency <> L.ABC_Frequency))
AND NVL(S.ABC_Disable, 'N') = 'N'
AND I.SKU_ID = SR.SKU_ID
AND I.Client_ID = SR.Client_ID
AND SR.SKU_ID = S.SKU_ID
AND SR.Client_ID = S.Client_ID
AND ((('&3' IS NULL) AND ('&5' IS NULL))
OR  (I.Client_ID = '&3')
OR  (('&3' IS NULL) AND I.Client_ID IN
	(SELECT Client_ID 
	 FROM Client_Group_Clients
	 WHERE Client_Group = '&5')))  
AND L.Loc_Type IN ('Tag-Operator', 'Tag-FIFO', 'Tag-LIFO', 'Bulk', 'Bin')
ORDER BY I.Site_ID, I.Client_ID, I.Owner_ID, I.SKU_ID, I.Location_ID, I.Tag_ID;
 
SET TERMOUT ON






