/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics & Industrial Systems Ltd  */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1996             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/
 
/******************************************************************************/
/*                                                                            */
/*                                  LIS                                       */
/*                                                                            */
/*     FILE NAME  :     putalgskuall.sql                                      */
/*                                                                            */
/*     DESCRIPTION:     List the sku/group/global algorithms.                 */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   08/11/96 RMW  N/A    N/A      initial version                            */
/*   06/02/98 JH   DCS    PDR5915  Reports need to be multi-sited             */
/*   12/12/98 JH   DCS    NEW3173  ABC/pareto analysis			      */
/*   18/04/99 JH   DCS    NEW3920  Extra/wider database columns		      */
/*   16/06/99 MJT  DCS    PDR7593  Some sql*plus reports are wrapping         */
/*   27/06/01 MJT  DCS    NEW5486  Client ID added                            */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   05/08/05 DM   DCS    NEW7858  Extend SKU to 30 characters                */
/******************************************************************************/
 
/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON 
SET VERIFY OFF
SET TAB OFF
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132
 
/* Top Title - for sku algorithms */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'SKU Putaway Algorithms By SKU' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2 - 
LEFT 'SKU &4' SKIP 2
 
/* Column Headings and Formats */
COLUMN Site_Client HEADING 'Site/|Client' FORMAT A10 WORD_WRAP
COLUMN SKU_Text HEADING 'SKU/|Algorithm' FORMAT A30 WORD_WRAP
COLUMN Priority HEADING 'Pri' FORMAT 999
COLUMN OwnerMix HEADING 'Owner/Mix|Origin/Mix' FORMAT A10 WRAP
COLUMN ConditionMix HEADING 'Cond/|Mix' FORMAT A5 WRAP
COLUMN FromLocZone HEADING 'From|Loc|Zone' FORMAT A4
COLUMN Zones HEADING 'Zone/|SubZne1/|SubZne2' FORMAT A10 WRAP
COLUMN InVolume HEADING 'In|Vol' FORMAT A4
COLUMN PartPallet HEADING 'Part|Pall' FORMAT A4
COLUMN PalletSearch HEADING 'Pall|Srch' FORMAT A4
COLUMN SKUSearch HEADING 'SKU|Srch' FORMAT A4
COLUMN TopUp HEADING 'Top|Up' FORMAT A4
COLUMN SKUMix HEADING 'SKU|Mix' FORMAT A4
COLUMN BatchMix HEADING 'Btch|Mix' FORMAT A4
COLUMN Mix_Days HEADING 'Mix|Days' FORMAT 9999
COLUMN ABC_Frequency HEADING 'ABC|Freq' FORMAT A4
COLUMN ABC_Storage HEADING 'ABC|Stor' FORMAT A4
COLUMN ABCDisable HEADING 'ABC|Dis' FORMAT A3

BREAK ON Site_Client SKIP PAGE ON SKU_ID SKIP 1
 
/* SQL Select statement */
SELECT RPAD(Site_ID, 10) || Client_ID Site_Client, 
	RPAD(SKU_ID, 30) || LT.Text SKU_Text,
	Priority,
	RPAD(NVL(Owner_ID, ' '), 10) || RPAD(NVL(Owner_Mix, 'N'), 10) || RPAD(NVL(Origin_ID, ' '), 10) || NVL(Origin_Mix, 'N') OwnerMix,
	RPAD(NVL(Condition_ID, ' '), 5) || NVL(Condition_Mix, 'N') ConditionMix,
	NVL(From_Loc_Zone, 'N') FromLocZone,
	RPAD(Zone_1, 10) || RPAD(Subzone_1, 10) || Subzone_2 Zones,
	NVL(In_Volume, 'N') InVolume,
	NVL(Part_Pallet, 'N') PartPallet,
	NVL(Pallet_Search, 'N') PalletSearch,
	NVL(SKU_Search, 'N') SKUSearch,
	NVL(Top_Up, 'N') TopUP,
	NVL(SKU_Mix, 'N') SKUMix,
	NVL(Batch_Mix, 'N') BatchMix,
	Mix_Days,
	SP.ABC_Frequency,
	SP.ABC_Storage,
	NVL(SP.ABC_Disable, 'N') ABCDisable
FROM SKU_Putaway SP, Language_Text LT
WHERE SKU_ID = '&4'
AND SP.Algorithm = LT.Label
AND LT.Language = 'EN_GB'
AND (('&2' IS NULL) OR (SP.Site_ID = '&2'))
AND ((('&3' IS NULL) AND ('&5' IS NULL))
OR  (SP.Client_ID = '&3')
OR  (('&3' IS NULL) AND SP.Client_ID IN
(SELECT Client_ID 
FROM Client_Group_Clients
WHERE Client_Group = '&5')))  
ORDER BY Site_ID, Client_ID, SKU_ID, Priority;
 
/* Top Title - for group algorithms */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Group Putaway Algorithms By SKU' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2 - 
LEFT 'SKU &4' SKIP 2
 
/* Column Headings and Formats */
COLUMN Site_Client HEADING 'Site/|Client' FORMAT A10 WORD_WRAP
COLUMN Putaway_Group HEADING 'Group' FORMAT A10
COLUMN Priority HEADING 'Pri' FORMAT 999
COLUMN Text HEADING 'Algorithm' FORMAT A30 WORD_WRAP
COLUMN OwnerMix HEADING 'Owner/Mix|Origin/Mix' FORMAT A10 WRAP
COLUMN ConditionMix HEADING 'Cond/|Mix' FORMAT A5 WRAP
COLUMN FromLocZone HEADING 'From|Loc|Zone' FORMAT A4
COLUMN Zones HEADING 'Zone/|SubZne1/|SubZne2' FORMAT A10 WRAP
COLUMN InVolume HEADING 'In|Vol' FORMAT A4
COLUMN PartPallet HEADING 'Part|Pall' FORMAT A4
COLUMN PalletSearch HEADING 'Pall|Srch' FORMAT A4
COLUMN SKUSearch HEADING 'SKU|Srch' FORMAT A4
COLUMN TopUp HEADING 'Top|Up' FORMAT A4
COLUMN SKUMix HEADING 'SKU|Mix' FORMAT A4
COLUMN BatchMix HEADING 'Btch|Mix' FORMAT A4
COLUMN Mix_Days HEADING 'Mix|Days' FORMAT 9999

BREAK ON Site_Client SKIP PAGE ON Putaway_Group SKIP 1
 
/* SQL Select statement */
SELECT RPAD(GP.Site_ID, 10) || S.Client_ID Site_Client, 
	GP.Putaway_Group,
	GP.Priority,
	LT.Text,
	RPAD(NVL(Owner_ID, ' '), 10) || RPAD(NVL(Owner_Mix, 'N'), 10) || RPAD(NVL(Origin_ID, ' '), 10) || NVL(Origin_Mix, 'N') OwnerMix,
	RPAD(NVL(Condition_ID, ' '), 5) || NVL(Condition_Mix, 'N') ConditionMix,
	NVL(From_Loc_Zone, 'N') FromLocZone,
	RPAD(Zone_1, 10) || RPAD(Subzone_1, 10) || Subzone_2 Zones,
	NVL(In_Volume, 'N') InVolume,
	NVL(Part_Pallet, 'N') PartPallet,
	NVL(Pallet_Search, 'N') PalletSearch,
	NVL(SKU_Search, 'N') SKUSearch,
	NVL(Top_Up, 'N') TopUP,
	NVL(SKU_Mix, 'N') SKUMix,
	NVL(Batch_Mix, 'N') BatchMix,
	Mix_Days
FROM Group_Putaway GP, Language_Text LT, SKU S
WHERE GP.Algorithm = LT.Label
AND LT.Language = 'EN_GB'
AND (('&2' IS NULL) OR (GP.Site_ID = '&2'))
AND GP.Putaway_Group = S.Putaway_Group
AND S.SKU_ID = '&4'
AND ((('&3' IS NULL) AND ('&5' IS NULL))
OR  (S.Client_ID = '&3')
OR  (('&3' IS NULL) AND S.Client_ID IN
(SELECT Client_ID 
FROM Client_Group_Clients
WHERE Client_Group = '&5')))  
ORDER BY GP.Site_ID, S.Client_ID, GP.Putaway_Group, GP.Priority;
 
/* Top Title - for global algorithms */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Global Putaway Algorithms' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2
 
/* Column Headings and Formats */
COLUMN Site_ID HEADING 'Site' FORMAT A10
COLUMN Priority HEADING 'Pri' FORMAT 999
COLUMN Text HEADING 'Algorithm' FORMAT A40 WORD_WRAP
COLUMN OwnerMix HEADING 'Owner/Mix|Origin/Mix' FORMAT A10 WRAP
COLUMN ConditionMix HEADING 'Cond/|Mix' FORMAT A5 WRAP
COLUMN FromLocZone HEADING 'From|Loc|Zone' FORMAT A4
COLUMN Zones HEADING 'Zone/|SubZne1/|SubZne2' FORMAT A10 WRAP
COLUMN InVolume HEADING 'In|Vol' FORMAT A4
COLUMN PartPallet HEADING 'Part|Pall' FORMAT A4
COLUMN PalletSearch HEADING 'Pall|Srch' FORMAT A4
COLUMN SKUSearch HEADING 'SKU|Srch' FORMAT A4
COLUMN TopUp HEADING 'Top|Up' FORMAT A4
COLUMN SKUMix HEADING 'SKU|Mix' FORMAT A4
COLUMN BatchMix HEADING 'Btch|Mix' FORMAT A4
COLUMN Mix_Days HEADING 'Mix|Days' FORMAT 9999

BREAK ON Site_ID SKIP PAGE
 
/* SQL Select statement */
SELECT Site_ID,
	Priority,
	LT.Text,
	RPAD(NVL(Owner_ID, ' '), 10) || RPAD(NVL(Owner_Mix, 'N'), 10) || RPAD(NVL(Origin_ID, ' '), 10) || NVL(Origin_Mix, 'N') OwnerMix,
	RPAD(NVL(Condition_ID, ' '), 5) || NVL(Condition_Mix, 'N') ConditionMix,
	NVL(From_Loc_Zone, 'N') FromLocZone,
	RPAD(Zone_1, 10) || RPAD(Subzone_1, 10) || Subzone_2 Zones,
	NVL(In_Volume, 'N') InVolume,
	NVL(Part_Pallet, 'N') PartPallet,
	NVL(Pallet_Search, 'N') PalletSearch,
	NVL(SKU_Search, 'N') SKUSearch,
	NVL(Top_Up, 'N') TopUP,
	NVL(SKU_Mix, 'N') SKUMix,
	NVL(Batch_Mix, 'N') BatchMix,
	Mix_Days
FROM Global_Putaway GP, Language_Text LT
WHERE GP.Algorithm = LT.Label
AND LT.Language = 'EN_GB'
AND (('&2' IS NULL) OR (GP.Site_ID = '&2'))
ORDER BY 1, 2;
 
SET TERMOUT ON
 









