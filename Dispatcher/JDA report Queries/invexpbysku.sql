/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics and Industrial Systems Ltd*/
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1996             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/
 
/******************************************************************************/
/*                                                                            */
/*                                 LIS                                        */
/*                                                                            */
/*     FILE NAME  :     invexpbysku.sql                                       */
/*                                                                            */
/*     DESCRIPTION:     List expired inventory by SKU ID range.               */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   25/04/97 MJT  DCS    PDR5924  Modified to handle european group & decimal*/
/*                                 separators.                                */
/*   6/08/97  MJT  DCS    NEW2813  Modified to include new container id,      */
/*                                 pallet id fields in inventory              */
/*   06/02/98 JH   DCS    PDR5915  Reports need to be multi-sited             */
/*   27/07/98 MJT  DCS    NEW3188  Third Party Warehousing                    */
/*   18/04/99 JH   DCS    NEW3920  Extra/wider database columns		      */
/*   16/06/99 MJT  DCS    PDR7593  Some sql*plus reports are wrapping         */
/*   28/06/01 MJT  DCS    NEW5486  Client ID added                            */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   04/08/05 DM   DCS    NEW7858  Extend SKU to 30 characters                */
/******************************************************************************/
 
/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132

/* Top Title */
TTITLE CENTER _SITENAME SKIP 2 - 
LEFT 'Expired Inventory By SKU Range' RIGHT _DATESTAMP SKIP 1 -        
RIGHT _TIMESTAMP SKIP 1 -                               
LEFT 'SKU Range From &5 To &6' -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2

/* Avoid duplicate SKUs */
BREAK ON Site_Client SKIP PAGE ON Owner_ID ON sDescription SKIP 1 ON Location_ID

/* Column Headings and Formats */
COLUMN Site_Client HEADING 'Site/|Client' FORMAT A10 WORD_WRAP
COLUMN Owner_Location HEADING 'Owner/|Location' FORMAT A10 WORD_WRAP
COLUMN sDescription HEADING 'SKU/|Description' FORMAT A30 WRAP
COLUMN Tag_Batch HEADING 'Tag/|Batch' FORMAT A20 WORD_WRAP
COLUMN Config_ID HEADING 'Pack Config/|Cond Code' FORMAT A15 WRAP
COLUMN Receipt_DStamp HEADING 'Receipt/|Expiry|Dates' FORMAT A11 WRAP
COLUMN Qty_On_Hand HEADING 'Qty On Hand' FORMAT '999G999G990D999999'
COLUMN Lock_Status HEADING 'Status' FORMAT A8
 
/* Compute sums */
COMPUTE SUM OF Qty_On_Hand ON sDescription
 
/* SQL Select statement */
SELECT	RPAD (I.Site_ID, 10) || I.Client_ID Site_Client,
	RPAD (I.Owner_ID, 10) || I.Location_ID Owner_Location,
	RPAD (I.SKU_ID, 30) || I.Description sDescription,
	RPAD (I.Tag_ID, 20) || I.Batch_ID Tag_Batch,
	I.Qty_On_Hand,
	I.Lock_Status,
	TO_CHAR (I.Receipt_DStamp, 'DD-MON-YYYY') || 
	TO_CHAR (I.Expiry_DStamp, 'DD-MON-YYYY') Receipt_DStamp,
	RPAD (NVL (I.Config_ID, ' '), 15) || I.Condition_ID Config_ID
FROM Inventory I
WHERE I.SKU_ID BETWEEN '&5' AND '&6'
AND I.Expired = 'Y'
AND (('&2' IS NULL) OR (I.Site_ID = '&2'))
AND (('&4' IS NULL) OR (I.Owner_ID = '&4'))
AND ((('&3' IS NULL) AND ('&7' IS NULL))
OR  (I.Client_ID = '&3')
OR  (('&3' IS NULL) AND I.Client_ID IN
	(SELECT Client_ID 
	 FROM Client_Group_Clients
	 WHERE Client_Group = '&7')))  
ORDER BY I.Site_ID, I.Client_ID, I.Owner_ID, I.SKU_ID, I.Description, I.Location_ID, I.Lock_Status, I.Expiry_DStamp; 
 
SET TERMOUT ON
 



