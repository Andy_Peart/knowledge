/******************************************************************************/
/*                                                                            */
/*   Logistics and Internet Systems Ltd                                       */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics and Internet Systems Ltd  */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Internet   */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Internet Systems Limited, 2001               */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/
/******************************************************************************/
/*                                                                            */
/*                                   LIS                                      */
/*                                                                            */
/*     FILE NAME  :     language_user.sql                                     */
/*                                                                            */
/*     DESCRIPTION:     List the language text modified by the end user.      */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/******************************************************************************/
 
/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132
 
/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Language Text Modified by the End User' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2
 
BREAK ON Language SKIP 2 ON Application SKIP 1

/* Column Headings and Formats */
COLUMN Language HEADING 'Language' FORMAT A10
COLUMN Text HEADING 'Text' FORMAT A105
COLUMN Application HEADING 'Application' FORMAT A15

/* SQL Select statement */
SELECT 	Language,
	Text,
	DECODE (Application, 'I', 'IntelliBatch', 'P', 'DeliveryProof', NULL) Application
FROM Language_Text
WHERE Language LIKE '&2'
AND User_Modified = 'Y'
ORDER BY 1 ASC, 3 ASC, 2 ASC
;
 
SET TERMOUT ON

