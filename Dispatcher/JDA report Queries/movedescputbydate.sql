/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics & Industrial Systems Ltd  */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 2000             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/
 
/******************************************************************************/
/*                                                                            */
/*                                 LIS                                        */
/*                                                                            */
/*     FILE NAME  :     movedescputbydate.sql                                 */
/*                                                                            */
/*     DESCRIPTION:     List move descrepancies by date range.                */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   26/06/01 MJT  DCS    NEW5486  Client ID added                            */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   04/08/05 DM   DCS    NEW7858  Extend SKU to 30 characters                */
/******************************************************************************/
 
/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF 
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132
 
/* Set up Variables */
 
/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Move Discrepancy Report By Date Range (Putaways)' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2 - 
LEFT 'Date Range From &3 To &4' SKIP 2

/* Column Headings and Formats */
COLUMN Site_ID HEADING 'Site' FORMAT A10
COLUMN Client_ID HEADING 'Client' FORMAT A10
COLUMN DSTAMP NEW_VALUE dDate NOPRINT
COLUMN SKU_ID HEADING 'SKU/Tag' FORMAT A30
COLUMN cLOCATION HEADING 'From/|To/|Final' FORMAT A10 JUS L
COLUMN LIST_ID HEADING 'List' FORMAT A10 JUS L
COLUMN cQTY HEADING 'Quantity' FORMAT A19 JUS L
COLUMN REASON_CODE HEADING 'Reason' FORMAT A10
COLUMN cUSER HEADING 'User/|Station' FORMAT A20 JUS L

BREAK ON Site_ID SKIP PAGE ON Client_ID SKIP PAGE ON DSTAMP SKIP 1
 
/* SQL Select statement */
SELECT	Site_ID,
	Client_ID,
	DStamp,
	RPAD (SKU_ID, 30) || Tag_ID SKU_ID,
	RPAD (From_Loc_ID, 10) || RPAD (To_Loc_ID, 10) || 
	RPAD (Final_Loc_ID, 10) cLOCATION,
	List_ID,
	LPAD(TO_CHAR(Qty_To_Move,'999G999G990D999999'), 19) cQTY,
	Reason_Code,
	RPAD(User_ID, 20) || RPAD(Station_ID, 20) cUSER
FROM Move_Descrepancy
WHERE DStamp >= TO_DATE('&4', 'DD-MON-YYYY') 
AND DStamp < TO_DATE('&5', 'DD-MON-YYYY') + 1
AND (('&2' IS NULL) OR (Site_ID = '&2'))
AND Task_Type = 'P'
AND ((('&3' IS NULL) AND ('&6' IS NULL))
OR  (Client_ID = '&3')
OR  (('&3' IS NULL) AND Client_ID IN
(SELECT Client_ID 
FROM Client_Group_Clients
WHERE Client_Group = '&6')))  
ORDER BY Site_ID, Client_ID, DStamp; 

SET TERMOUT ON









