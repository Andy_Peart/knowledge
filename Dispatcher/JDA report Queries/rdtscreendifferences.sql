/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics & Industrial Systems Ltd  */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LEXLOGISTiX.                                 */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 2001             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/
 
/******************************************************************************/
/*                                                                            */
/*                                  LIS                                       */
/*                                                                            */
/*     FILE NAME  :     rdtscreendifferences.sql                              */
/*                                                                            */
/*     DESCRIPTION:     Report on all customised rdt screens with             */
/*                      a different number of fields.                         */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   10/12/01 JH   DCS    NEW5642  RDT screen editor                          */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/******************************************************************************/
 
/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF 

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132

/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'RDT Screen Differences' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2
 
/* Column Headings and Formats */
COLUMN Site_ID HEADING 'Site ID' FORMAT A10
COLUMN Language HEADING 'Language' FORMAT A10
COLUMN Name HEADING 'Name' FORMAT A20
COLUMN Num_Rows HEADING 'Rows'
COLUMN Num_Cols HEADING 'Columns'
COLUMN Type HEADING 'Type' FORMAT A15

BREAK ON Site_ID SKIP PAGE ON Language SKIP 1

/* SQL Select statement */
select distinct site_id, language, name, num_rows, num_cols, decode(type, 'New', 'Draft', type) Type
from rdt_screen_data rsd
where rsd.type in ('Current', 'New')
and (select count(*) 
     from rdt_screen_user_field rsuf
     where rsd.language = rsuf.language
     and rsd.name = rsuf.name
     and rsd.num_rows = rsuf.num_rows
     and rsd.num_cols = rsuf.num_cols
     and rsd.type = rsuf.type
     and ((rsd.site_id = rsuf.site_id)
     or (rsd.site_id is null and rsuf.site_id is null))) <>
    (select count(*) 
     from rdt_screen_field rsf
     where rsd.name = rsf.name)
order by 1, 2, 3, 4, 5, 6;

SET TERMOUT ON

