/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics & Industrial Systems Ltd  */
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1999             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*                                 LIS                                        */
/*                                                                            */
/*     FILE NAME  :     spwdescbydate.sql                                     */
/*                                                                            */
/*     DESCRIPTION:     List Suggested Putaway Overrides by date range.       */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   11/10/99 RMC  DCS    NEW4079  initial version                            */
/*   28/06/01 MJT  DCS    NEW5486  Client ID added                            */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/*   08/08/05 DM   DCS    NEW7858  Extend SKU to 30 characters                */
/*   19/10/06 JH   DCS    ENH3120  Increase cons/wg to 20 chars               */
/******************************************************************************/

/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF
SET TAB OFF

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132

/* Set up Variables */

/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Suggested Putaway Overrides By Date Range' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2 -
LEFT 'Date Range From &4 To &5' SKIP 2

/* Column Headings and Formats */
COLUMN SiteClient HEADING 'Site/|Client' FORMAT A10
COLUMN DSTAMP NEW_VALUE dDate NOPRINT
COLUMN SKU_ID HEADING 'SKU/Tag' FORMAT A20 WORD_WRAP
COLUMN cLOCATION HEADING 'From/|To/|Original' FORMAT A10 JUS L
COLUMN LIST_ID HEADING 'List' FORMAT A10 JUS L
COLUMN cQTY HEADING 'Qty Moved' FORMAT A19 JUS L
COLUMN cWORKGROUP HEADING 'Work Group' FORMAT A20 JUS L
/* Reason column is arbitrarily wrapped */
COLUMN cREASON HEADING 'Reason' FORMAT A20
COLUMN cUSER HEADING 'User/|Station' FORMAT A10 WORD_WRAP

BREAK ON Site_ID SKIP PAGE ON Client_ID SKIP PAGE ON DSTAMP SKIP 1

/* SQL Select statement */
SELECT	RPAD (M.Site_ID, 10) || M.Client_ID SiteClient,
	M.DStamp,
	RPAD (M.SKU_ID, 30) || M.Tag_ID SKU_ID,
	RPAD (M.From_Loc_ID, 10) || RPAD (M.To_Loc_ID, 10) ||
	RPAD (M.Final_Loc_ID, 10) cLOCATION,
	M.List_ID,
	LPAD(TO_CHAR(M.Qty_To_Move,'999G999G990D999999'), 19) cQTY,
	RPAD(NVL (M.Work_Group, ' '), 20) cWORKGROUP,
	decode (M.Reason_Code,  'SPW_DF', 'Doesn''t fit',
				'SPW_LF', 'Location Full',
				'SPW_OV', 'Override',
				 M.Reason_Code) cREASON,
	RPAD(M.User_ID, 20) || RPAD(M.Station_ID, 20) cUSER
FROM Move_Descrepancy M
WHERE M.DStamp >= TO_DATE('&4', 'DD-MON-YYYY')
AND M.DStamp < TO_DATE('&5', 'DD-MON-YYYY') + 1
AND (('&2' IS NULL) OR (M.Site_ID = '&2'))
AND M.Reason_Code like 'SPW%'
AND ((('&3' IS NULL) AND ('&6' IS NULL))
OR  (M.Client_ID = '&3')
OR  (('&3' IS NULL) AND M.Client_ID IN
(SELECT Client_ID
FROM Client_Group_Clients
WHERE Client_Group = '&6')))
ORDER BY Site_ID, Client_ID, DStamp;

SET TERMOUT ON

