/******************************************************************************/
/*                                                                            */
/*   Logistics and Industrial Systems Ltd                                     */
/*   Knaves House                                                             */
/*   Knaves Beech Business Centre                                             */
/*   Loudwater                                                                */
/*   High Wycombe                                                             */
/*   Bucks HP10 9QR                                                           */
/*   United Kingdom                                                           */
/*                                                                            */
/*   The information in this file contains trade secrets and confidential     */
/*   information which is the property of Logistics and Industrial Systems Ltd*/
/*                                                                            */
/*   All trademarks, trade names, copyrights and other intellectual property  */
/*   rights created, developed, embodied in or arising in connection with     */
/*   this software shall remain the sole property of Logistics and Industrial */
/*   Systems Limited, trading as LIS.                                         */
/*                                                                            */
/*   Copyright (c) Logistics and Industrial Systems Limited, 1999             */
/*   ALL RIGHTS RESERVED                                                      */
/*                                                                            */
/******************************************************************************/
 
/******************************************************************************/
/*                                                                            */
/*                                 LIS                                        */
/*                                                                            */
/*     FILE NAME  :     ordshippedshort.sql                                   */
/*                                                                            */
/*     DESCRIPTION:     List orders shipped with allocation shortages         */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   15/09/99 RMW  DCS    N/A      Initial version                            */
/*   27/06/01 MJT  DCS    NEW5486  Client ID added                            */
/*   08/03/04 JH   DCS    PDR8932  Time zone problems                         */
/******************************************************************************/

/* Setup time zone */
@logintimezonesetup.sql '&1'

SET FEEDBACK ON
SET VERIFY OFF 

/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES

/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132

/* Top Title */
TTITLE CENTER _SITENAME SKIP 1 -
LEFT 'Orders - Part Shipped having Allocation Shortages' -
RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2
 
/* Column Headings and Formats */
COLUMN Site_Client HEADING 'Site /|Client' FORMAT A10 WORD_WRAP
COLUMN Owner_Task_ID HEADING 'Owner/|Order ID' FORMAT A20 WORD_WRAP
COLUMN SKU_Description HEADING 'SKU /|Description' FORMAT A40 WRAP
COLUMN Qty_Ordered HEADING 'Ordered' FORMAT A19
COLUMN Qty_Tasked HEADING 'Tasked' FORMAT A19
COLUMN Qty_Shipped HEADING 'Shipped' FORMAT A19

BREAK ON Site_Client SKIP PAGE ON Owner_ID SKIP 1 ON Order_ID SKIP 1 

SELECT  RPAD (GS.Site_ID, 10) || GS.Client_ID Site_Client,
	RPAD (GS.Owner_ID, 20) || GS.Task_ID Owner_Task_ID, 
	RPAD (OL.SKU_ID, 40) || S.Description SKU_Description,
	LPAD (TO_CHAR (GS.Qty_Ordered,'999G999G990D999999'), 19) Qty_Ordered,
	LPAD (TO_CHAR (GS.Qty_Tasked,'999G999G990D999999'), 19) Qty_Tasked,
	LPAD (TO_CHAR (OL.Qty_Shipped,'999G999G990D999999'), 19) Qty_Shipped
FROM Generation_Shortage GS, Order_Line OL, SKU S
WHERE OL.Order_ID = GS.Task_ID
AND OL.Client_ID = GS.Client_ID
AND OL.Line_ID = GS.Line_ID
AND S.SKU_ID = OL.SKU_ID
AND S.Client_ID = OL.Client_ID
AND (('&2' IS NULL) OR (GS.Site_ID = '&2'))
AND (('&4' IS NULL) OR (GS.Owner_ID = '&4'))
AND GS.Task_ID IN (SELECT DISTINCT OL.Order_ID 
		   FROM Order_Line OL 
		   WHERE OL.Qty_Shipped > 0
		   AND OL.Order_ID = GS.Task_ID
		   AND OL.Client_ID = GS.Client_ID)
AND ((('&3' IS NULL) AND ('&5' IS NULL))
OR  (GS.Client_ID = '&3')
OR  (('&3' IS NULL) AND GS.Client_ID IN
(SELECT Client_ID 
FROM Client_Group_Clients
WHERE Client_Group = '&5')))  
ORDER BY GS.Site_ID, GS.Client_ID, GS.Owner_ID, GS.Task_ID, GS.Line_ID
/

SET TERMOUT ON



