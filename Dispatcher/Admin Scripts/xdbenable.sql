spool xdbenable1.log

set termout on
set echo on
 
--@$ORACLE_HOME/javavm/install/initjvm.sql

--@$ORACLE_HOME/rdbms/admin/catqm.sql XDB XDB TEMP NO

--@$ORACLE_HOME/rdbms/admin/catxdbj.sql

--@$ORACLE_HOME/rdbms/admin/utlrp.sql

--alter user anonymous account unlock;

grant execute on utl_smtp to dcsdba;
grant execute on utl_smtp to dcsusr;

begin
        dbms_network_acl_admin.drop_acl
                (
                acl             => 'acl_smtp.xml'
		);
end;
/

begin
        dbms_network_acl_admin.create_acl
                (
                acl             => 'acl_smtp.xml',
                description     => 'Allow smtp access',
                principal       => 'DCSDBA',
                is_grant        => true,
                privilege       => 'connect',
                start_date      => current_timestamp,
                end_date        => null
                );
end;
/

begin
        dbms_network_acl_admin.add_privilege
                (
                acl             => 'acl_smtp.xml',
                principal       => 'DCSUSR',
                is_grant        => true,
                privilege       => 'connect'
                );
end;
/

begin
        dbms_network_acl_admin.assign_acl
                (
                acl             => 'acl_smtp.xml',
                host            => '*',
                lower_port      => 25,
                upper_port      => null
                );
end;
/

commit
/

grant execute on utl_http to dcsdba;
grant execute on utl_http to dcsusr;

begin
        dbms_network_acl_admin.drop_acl
                (
                acl             => 'acl_http_post.xml'
		);
end;
/

begin
        dbms_network_acl_admin.create_acl
                (
                acl             => 'acl_http_post.xml',
                description     => 'Allow http post',
                principal       => 'DCSDBA',
                is_grant        => true,
                privilege       => 'connect',
                start_date      => current_timestamp,
                end_date        => null
                );
end;
/

begin
        dbms_network_acl_admin.add_privilege
                (
                acl             => 'acl_http_post.xml',
                principal       => 'DCSUSR',
                is_grant        => true,
                privilege       => 'connect'
                );
end;
/

begin
        dbms_network_acl_admin.assign_acl
                (
                acl             => 'acl_http_post.xml',
                host            => '*',
                lower_port      => 80,
                upper_port      => 6000 
                );
end;
/

grant execute on utl_inaddr to dcsdba;
grant execute on utl_inaddr to dcsusr;

begin
        dbms_network_acl_admin.drop_acl
                (
                acl             => 'acl_resolve_access.xml'
		);
end;
/

begin
        dbms_network_acl_admin.create_acl
                (
                acl             => 'acl_resolve_access.xml',
                description     => 'Allow network resolve',
                principal       => 'DCSDBA',
                is_grant        => true,
                privilege       => 'resolve',
                start_date      => current_timestamp,
                end_date        => null
                );
end;
/

begin
        dbms_network_acl_admin.add_privilege
                (
                acl             => 'acl_resolve_access.xml',
                principal       => 'DCSUSR',
                is_grant        => true,
                privilege       => 'resolve'
                );
end;
/

begin
        dbms_network_acl_admin.assign_acl
                (
                acl             => 'acl_resolve_access.xml',
                host            => '*',
                lower_port      => null,
                upper_port      => null
                );
end;
/

commit
/

quit
/

