/******************************************************************************/
/*                                                                            */
/*  Copyright (c) 2014 JDA Software Group, Inc.                               */
/*  All rights reserved - Company Confidential                                */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :     freespace2.sql                                        */
/*                                                                            */
/*     DESCRIPTION:     Displays free space information for all tablespaces,  */
/*			by tablespace.					      */
/*                                                                            */
/*                      In a fairly static data/indexes tablespace you        */
/*			probably want to have 15% - 25% free space.  In the   */
/*			system tablespace you probably want to have 50%       */
/*			free space.					      */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/******************************************************************************/

clear columns
clear breaks

set pagesize 66
set linesize 80

column tname format a15 heading 'Tablespace' trunc
column tbytes format 99,999,999,990 heading 'Total (B)'
column ubytes format 99,999,999,990 heading 'Used (B)'
column fbytes format 99,999,999,990 heading 'Free (B)'
column fprcnt format 999.9 heading 'Free (%)'

select tablespace_name tname, 
	total_bytes tbytes, 
	total_bytes - free_bytes ubytes,
	free_bytes fbytes,
	100 * free_bytes / total_bytes fprcnt
from (select tablespace_name, sum(nvl(bytes, 0)) total_bytes
      from dba_data_files
      group by tablespace_name),
     (select tablespace_name fs_ts_name, sum(nvl(bytes, 0)) free_bytes
      from dba_free_space
      group by tablespace_name)
where tablespace_name = fs_ts_name (+)
order by tablespace_name;
	
