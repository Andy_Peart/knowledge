/******************************************************************************/
/*                                                                            */
/*  Copyright (c) 2014 JDA Software Group, Inc.                               */
/*  All rights reserved - Company Confidential                                */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :     locklist.sql                                          */
/*                                                                            */
/*     DESCRIPTION:     Lists currently held locks in the database.           */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   20/10/99 JH   DCS    PDR7903  Changes required for Oracle 8              */
/*   27/06/11 JH   DCS    DSP4415  Add module/action to query                 */
/******************************************************************************/

clear breaks
clear columns

set pagesize 66
set linesize 132

column username format a10 heading 'Username' trunc
column program format a15 heading 'Program' trunc
column process format a9 heading 'Process'
column sid format a3 heading 'Sid'
column serial# format a10 heading 'Serial#'
column sql_hash_value format a15 heading 'Hash Value'
column lock_type format a26 heading 'Lock Type'
column mode_held format a13 heading 'Mode Held'
column mode_requested format a13 heading 'Mode Req'
column lock_id1 format a8 heading 'Id1'
column lock_id2 format a8 heading 'Id2'
column module format a48 heading 'Module'
column action format a32 heading 'Action'
column event format a40 heading 'Event'
column last_call_et heading 'LastCallET'
column seconds_in_wait heading 'SecsInWait'
column logon_time format a19 heading 'LogonTime'
break on lock_id1 skip 1 duplicate

select /*+ RULE */ b.username,
	b.program,
	b.process process,
	to_char(a.sid) sid,
	to_char(b.serial#) serial#,
	b.module,
	b.action,
	to_char(b.sql_hash_value) sql_hash_value,
	decode(a.type, 'MR', 'Media Recovery',
		       'RT', 'Redo Thread',
		       'UN', 'User Name',
		       'TX', 'Transaction',
		       'TM', 'DML',
		       'UL', 'PL/SQL User Lock',
		       'DX', 'Distributed Transaction',
		       'CF', 'Control File',
		       'IS', 'Instance State',
		       'FS', 'File Set',
		       'IR', 'Instance Recovery',
		       'ST', 'Disk Space Transaction',
		       'TS', 'Temp Segment',
		       'IV', 'Library Cache Invalidation',
		       'LS', 'Log Start or Switch',
		       'RW', 'Row Wait',
		       'SQ', 'Sequence Number',
		       'TE', 'Extend Table',
		       'TT', 'Temp Table',
		       a.type) lock_type,
	decode(a.lmode, 0, 'None',
			1, 'Null',
		 	2, 'Row-S (SS)',
			3, 'Row-X (SX)',
			4, 'Share',
			5, 'S/Row-X (SSX)',
			6, 'Exclusive',
			to_char (a.lmode)) mode_held,
	decode(a.request, 0, 'None',
	       		  1, 'Null',
			  2, 'Row-S (SS)',
		          3, 'Row-X (SX)',
			  4, 'Share',
			  5, 'S/Row-X (SSX)',
	       		  6, 'Exclusive',
			  to_char(a.request)) mode_requested,
	to_char(a.id1) lock_id1,
	to_char(a.id2) lock_id2, 
	b.event, 
	b.last_call_et, 
	b.seconds_in_wait,
	to_char (b.logon_time, 'DD-MM-YYYY HH24:MI:SS') logon_time
from v$lock a, v$session b
where (a.sid = b.sid
        and a.request != 0)
or (a.sid = b.sid
        and a.request = 0
        and a.lmode != 4
        and (a.id1, a.id2) in (select c.id1, c.id2
                               from v$lock c
                               where c.request != 0
                               and a.id1 = c.id1
                               and a.id2 = c.id2))
order by a.id1, a.id2, a.request;

