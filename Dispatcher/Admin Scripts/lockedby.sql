/******************************************************************************/
/*                                                                            */
/*  Copyright (c) 2014 JDA Software Group, Inc.                               */
/*  All rights reserved - Company Confidential                                */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :     lockedby.sql                                          */
/*                                                                            */
/*     DESCRIPTION:     Lists sql statements causing locks.                   */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   02/03/99 JH   DCS    NEW3829  New oracle admin scripts		      */
/*   20/10/99 JH   DCS    PDR7903  Changes required for Oracle 8              */
/******************************************************************************/

clear breaks
clear columns

set pagesize 66
set linesize 132

ttitle 'SQL Statements Causing Locks'

column username format a10 heading 'Username' trunc
column program format a10 heading 'Program' trunc
column process format a10 heading 'Process'
column sid format 99999999 heading 'SID'
column serial# format 99999999 heading 'Serial#'
column id1 format 99999999 heading 'ID'
column sql_text format a64 heading 'SQL' wrap

break on username on program on process on sid skip 1 on serial# on id1

select /*+ RULE */ a.username, a.program, a.process, a.sid, a.serial#, b.id1, c.sql_text
from v$session a, v$lock b, v$sqltext c
where b.id1 in (select distinct e.id1
		from v$session d, v$lock e
		where d.lockwait = e.kaddr)
and a.sid = b.sid
and c.hash_value = a.sql_hash_value
and b.request = 0
order by c.address, c.piece;

