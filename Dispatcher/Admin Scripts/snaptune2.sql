/******************************************************************************/
/*                                                                            */
/*  Copyright (c) 2014 JDA Software Group, Inc.                               */
/*  All rights reserved - Company Confidential                                */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*  FILE NAME  :  snaptune.sql                                                */
/*                                                                            */
/*  DESCRIPTION:  This SQL script carries out a series of queries and gives   */
/*                pointers to performance issues. Because it only looks at    */
/*                the system NOW and not at averages over a time period the   */
/*                results should not be acted on immediately but maybe        */
/*                investigated further.                                       */
/*                                                                            */
/*  DATE     BY   PROJ   ID       DESCRIPTION                                 */
/*  ======== ==== ====== ======== =============                               */
/*  19/02/98 JH   N/A    N/A      Initial version                             */
/******************************************************************************/

prompt
prompt 1 - Name - DB Block Efficiency 

select round((1-(pr.value/(bg.value+cg.value)))*100,2)
from v$sysstat pr, v$sysstat bg, v$sysstat cg
where pr.name = 'physical reads'
and bg.name = 'db block gets'
and cg.name = 'consistent gets';

prompt The init.ora parameter DB_BLOCK_BUFFERS controls the amount of memory 
prompt allocated for the data cache. When an application requests data, Oracle 
prompt first attempts to find it in the data cache. The more often Oracle finds
prompt requested data in memory a physical IO is avoided, and thus overall 
prompt performance is better. Under normal circumstances this ratio should be 
prompt greater than or equal to 95%. Initially set the DB_BLOCK_BUFFERS size to
prompt be 20 - 50% the size of the SGA. 
prompt 
prompt 
prompt 2 - Name - Dictionary Cache Efficiency 

select round(sum(gets)/(sum(gets)+sum(getmisses)) * 100,2) 
from v$rowcache;

prompt The init.ora parameter SHARED_POOL_SIZE controls the amount of memory 
prompt allocated for the shared buffer pool. The shared buffer pool contains 
prompt SQL and PL/SQL statements (library cache), the data dictionary cache, 
prompt and information on data base sessions. This percentage will never equal 
prompt 100 because the cache must perform an initial load when Oracle first 
prompt starts up. The percentage, therefore, should continually get closer to 
prompt 100 as the system stays "up." Ideally, the entire data dictionary would 
prompt be cached in memory. Initially set the SHARED_POOL_SIZE to be 50-100% 
prompt the size of the init.ora parameter DB_BLOCK_BUFFERS - then fine tune the
prompt  parameter. 
prompt 
prompt 
prompt 3 - Name - Disk Reads Max SQL 

select sql_text
from v$sqlarea, v$session
where address = sql_address
and username is not null
and disk_reads/executions =
        (select max(disk_reads/executions)
         from v$sqlarea, v$session
         where address = sql_address
         and username is not null);

prompt This query returns the first 1000 bytes of the SQL statement having the 
prompt highest number of disk reads per execution. This query is designed to 
prompt help determine the user generated SQL causing a large number of disk 
prompt reads per statement execution. 
prompt 
prompt 
prompt 4 - Name - Disk Reads Max User 

select username
from v$sqlarea, v$session
where address = sql_address
and username is not null
and disk_reads/executions =
        (select max(disk_reads/executions)
         from v$sqlarea, v$session
         where address = sql_address
         and username is not null);

prompt This query returns the username associated with the SQL statement having
prompt the highest number of disk reads per execution. This query is designed 
prompt to help determine the user causing a large number of disk reads per 
prompt statement execution. 
prompt 
prompt 
prompt 5 - Name - Extents Max Count 

select max(extent_id) + 1
from sys.dba_extents
where owner not in ('SYS','SYSTEM');

prompt Every table requires at least one extent. Ideally, each table should 
prompt reside in one extent, but this is sometimes not practical. However, 
prompt each table should reside in the fewest extents possible. This query 
prompt returns the largest number of extents used by any data base object, 
prompt i.e. table or index. Note These objects do not include SYS or
prompt SYSTEM owned objects. 
prompt 
prompt 
prompt 6 - Name - Extents Max Object Name 

select segment_name
from sys.dba_extents 
where owner not in ('SYS','SYSTEM')
and extent_id = (select max(extent_id)
                 from sys.dba_extents
                 where owner not in ('SYS','SYSTEM'));

prompt This query returns the name of the object - table or index - using the 
prompt maximum number of extents in any tablespace. Note - These objects do 
prompt not include SYS or SYSTEM owned objects. 
prompt 
prompt 
prompt 7 - Name - Free List Contention 

select round((sum(decode(w.class, 'free list',count, 0)) / (sum(decode(name,'db block gets', value, 0)) 
        + sum(decode(name,'consistent gets', value, 0)))) * 100,2)
from v$waitstat w, v$sysstat;

prompt The percentage that a request for data resulted in a wait for a free 
prompt block. Should be less than 1%. To reduce contention for a table's free 
prompt list the table must be recreated with a larger value in the FREELISTS 
prompt storage parameter. 
prompt 
prompt 
prompt 9 - Name - Library Cache Efficiency 

select round(sum(pinhits)/sum(pins) * 100,2) 
from v$librarycache;

prompt The percentage that a SQL statement did not need to be reloaded because 
prompt it was already in the library cache. The init.ora parameter 
prompt SHARED_POOL_SIZE controls the amount of memory allocated for the shared 
prompt buffer pool. The shared buffer pool contains SQL and PL/SQL statements 
prompt (library cache), the data dictionary cache, and information on data 
prompt base sessions. The percentage should be = 100, for maximum efficiency 
prompt requires that no SQL statement should be reloaded and reparsed. 
prompt Initially set the SHARED_POOL_SIZE to be 50-100% the size of the 
prompt init.ora parameter DB_BLOCK_BUFFERS - then fine tune the parameter. 
prompt 
prompt 
prompt 10 - Name - Recursive Calls 

select value 
from v$sysstat 
where name = 'recursive calls';

prompt The total number of recursive calls (Oracle issued SQL statements). May 
prompt show dynamic extension of tables or rollback segments. May be caused by 
prompt misses in the data dictionary cache, data base triggers, stored 
prompt procedures, functions, packages, anonymous PL/SQL blocks, DDL 
prompt statements, enforcement of referential integrity constraints. 
prompt 
prompt 
prompt 11 - Name - Redo Log Allocation Latch Contention 

select round(greatest(
        (sum(decode(ln.name, 'redo copy', misses,0))
                / greatest(sum(decode(ln.name, 'redo copy', gets,0)),1)),
        (sum(decode(ln.name, 'redo allocation', misses,0))
                / greatest(sum(decode(ln.name, 'redo allocation', gets,0)),1)),
        (sum(decode(ln.name, 'redo copy', immediate_misses,0))
                / greatest(sum(decode(ln.name, 'redo copy', immediate_gets,0))
                + sum(decode(ln.name, 'redo copy', immediate_misses,0)),1)),
        (sum(decode(ln.name, 'redo allocation', immediate_misses,0))
                / greatest(sum(decode(ln.name, 'redo allocation', immediate_gets,0))
                + sum(decode(ln.name, 'redo allocation', immediate_misses,0)),1)))
                * 100,2)
from v$latch l, v$latchname ln
where l.latch# = ln.latch#;

prompt The percentage of time that a process attempted to acquire a redo log 
prompt latch held by another process. Should be < 1%. Rare on single CPU 
prompt systems. If value > 1%, try decreasing the value of the init.ora 
prompt parameter LOG_SMALL_ENTRY_MAX_SIZE to reduce contention for the redo 
prompt allocation latch. For multiple CPU systems, increase the number of redo 
prompt copy latched by increasing the value of the init.ora parameter 
prompt LOG_SIMULTANEOUS_COPIES. It may be helpful to have up to twice as
prompt many copy latches as CPUs available to the data base instance. 
prompt Additionally, try increasing the value of the init.ora parameter 
prompt LOG_ENTRY_PREBUILD_THRESHOLD. 
prompt 
prompt 
prompt 12 - Name - Redo Log Buffer Contention 

select value 
from v$sysstat 
where name = 'redo log space waittime';

prompt The number of times a user process waited for redo log buffer space. 
prompt Should be near 0. If value increments consistently, increase the size 
prompt of the redo log buffer with the init.ora parameter LOG_BUFFER by 5%. 
prompt 
prompt 
prompt 13 - Name - Rollback Segment Contention 

select round(sum(waits)/sum(gets),2) 
from v$rollstat;

prompt The percentage that a request for data resulted in a wait for a 
prompt rollback segment. If greater than 1%, create more rollback segments. 
prompt 
prompt 
prompt 14 - Name - SGA Size 

select sum(bytes)
from v$sgastat;

prompt Returns the size of the SGA (System Global Area). The SGA is the core 
prompt memory of a computer dedicated to Oracle. Generally, the larger the SGA 
prompt the better, but the SGA must stay within core, i.e. it must not get 
prompt paged by the operating system. 
prompt 
prompt 
prompt 15 - Name - Shared Pool Free Memory 

select round((sum(decode(name, 'free memory', bytes, 0)) / sum(bytes)) * 100,2)
from v$sgastat;

prompt Percentage of free space in the SGA shared pool area. This percentage 
prompt should not drop below 5%. 
prompt 
prompt 
prompt 16 - Name - Shared Pool Reload Count 

select sum(reloads)
from v$librarycache
where namespace in ('SQL AREA','TABLE/PROCEDURE', 'BODY','TRIGGER');

prompt The number of times a SQL statement or procedure has been reparsed or 
prompt reloaded because of a lack of memory. If the number is too large, 
prompt increase the init.ora parameter SHARED_POOL_SIZE, or the init.ora 
prompt parameter OPEN_CURSORS. 
prompt 
prompt 
prompt 17 - Name - Shared Pool Reload Ratio 

select round(sum(reloads) / sum(pins) * 100,2)
from v$librarycache
where namespace in ('SQL AREA','TABLE/PROCEDURE','BODY','TRIGGER');

prompt Returns the percentage of SQL statement executions that result in a SQL 
prompt statement reload. If the percentage is more than 1%, increase the 
prompt init.ora parameter SHARED_POOL_SIZE. Although less likely, the init.ora 
prompt parameter OPEN_CURSORS may also need to increased. 
prompt 
prompt 
prompt 18 - Name - Sort Area Efficiency 

select round((sum(decode(name, 'sorts (memory)', value, 0))
        / (sum(decode(name, 'sorts (memory)', value, 0))
        + sum(decode(name, 'sorts (disk)', value, 0))))
        * 100,2)
from v$sysstat;

prompt The percentage of sorts performed in memory as opposed to sorts 
prompt performed in temporary segments on disk. 
prompt 
prompt 
prompt 19 - Name - Tablespace Max Used 

select round(max((d.bytes - sum(nvl(f.bytes,0))) / d.bytes) * 100,2)
from sys.dba_data_files d, sys.dba_free_space f
where d.file_id = f.file_id(+)
group by d.bytes;

prompt This query returns the largest percentage of used blocks by any 
prompt tablespace. For example, if a data base instance contains a tablespace 
prompt with 10% free space this query will return 90, assuming no other 
prompt tablespace has a lower percentage of free space. 
prompt 
