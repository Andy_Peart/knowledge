/******************************************************************************/
/*                                                                            */
/*  Copyright (c) 2014 JDA Software Group, Inc.                               */
/*  All rights reserved - Company Confidential                                */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :     extents.sql                                           */
/*                                                                            */
/*     DESCRIPTION:     Display current number and maximum number of extents. */
/*                                                                            */
/*			If you run it for a normal user it will show the      */
/*			the details for his tables/indexes.  If you run it    */
/*			for the sys user it will show you his tables/indexes  */
/*			clusters/rollback segments.			      */
/*                                                                            */
/*                      You are aiming to have <= 5 extents per object.       */
/*                                                                            */
/*                      If this has been exceeded consider consolidating the  */
/*                      number of extents by exporting the table/dropping the */
/*                      table/importing the table, and/or resizing the size   */
/*                      of the extents for that table.                        */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   28/05/98 JH   DCS    NEW3374  Script for backing up useful support info  */
/******************************************************************************/

clear breaks
clear columns

set pagesize 66
set linesize 80
set verify off

column tablespace_name format a15 heading 'Tablespace' trunc
column segment_type format a15 heading 'Segment Type' trunc
column segment_name format a15 heading 'Segment Name' trunc
column bytes format 999,999,990 heading 'Size (B)'
column extents format 999,990 heading 'Extents'
column max_extents format 9,999,999,990 heading 'Maximum'

select tablespace_name, 	
	segment_type,
	segment_name,
	bytes,
	extents,
	max_extents
from dba_segments 
where owner=upper('&1')
order by 1, 2, 3;

