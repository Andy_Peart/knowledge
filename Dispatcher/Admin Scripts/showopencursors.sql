set pages
select user_name || ' ' || sid || ' ' || rpad(sql_text, 60) || ' ' || count(*)
from v$open_cursor
group by user_name, sid, sql_text
order by user_name, sid, sql_text;
