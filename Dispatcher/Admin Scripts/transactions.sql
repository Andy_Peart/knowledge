/******************************************************************************/
/*                                                                            */
/*  Copyright (c) 2014 JDA Software Group, Inc.                               */
/*  All rights reserved - Company Confidential                                */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :     transactions.sql                                      */
/*                                                                            */
/*     DESCRIPTION:     Display rollback segments with active transactions    */
/*                      and users with active transactions.                   */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/******************************************************************************/

set verify off
set pagesize 23
set linesize 80
set feedback off
set showmode off
set echo off

ttitle '***** ROLLBACK SEGMENTS WITH ACTIVE TRANSACTIONS ****'
col owner           heading 'Owner'          format a6
col segment_name    heading 'RollBack|Name'  format a10
col tablespace_name heading 'TableSpace'     format a12
col EXTENTS         heading '#Of|Ext'        format 99
col SM              heading 'Size|Meg'       format 999
col IE              heading 'Init|Size|Meg'  format 99.9
col NE              heading 'Next|Size|Meg'  format 99.9
col OPT             heading "Opt|Size|Meg"   format 99.9
col HIGH            heading "High|Size|Meg"  format 999
col SHRINKS         heading "#Of|Srk"        format 99
col TRANS           heading "Cur|#Of|Trn"    format 99
col STATUS          heading 'Current|Status' format a9
select drs.owner owner,
       ds.segment_name segment_name,
       ds.tablespace_name tablespace_name,
       drs.initial_extent / 1048576 IE,
       drs.next_extent / 1048576 NE,
       ds.extents EXTENTS,
       (ds.blocks * 2) / 1024 SM,
       vr.optsize / 1048576 OPT,
       vr.hwmsize / 1048576 HIGH,
       vr.shrinks SHRINKS,
       vr.xacts TRANS,
       drs.status STATUS
from  v$rollstat vr, dba_segments ds, dba_rollback_segs drs
where ds.segment_type = 'ROLLBACK'
  and ds.segment_name = drs.segment_name
  and drs.segment_id  = vr.usn (+)
  and vr.xacts > 0
order by ds.segment_name;

ttitle '***** USERS WITH ACTIVE TRANSACTIONS ****'
col bb heading "Unix|User"             format a10
col cc heading "Oracle|User|Name"      format a10
col dd heading "Pgm|Unix|Pid"          format a6
col ee heading "Oracle|Unix|Pid"       format a6
col ef heading "Oracle|Session|ID"     format 999999
col eg heading "Serial#"               format 999999
col ff heading "TTY#"                  format a7
col gg heading "Program Name"          format a48
col hh heading "Server|Type"           format a9
col ii heading "Status"                format a9
col jj heading "Rollback|Segment"      format a10
col kk heading "Current SQL Statement" format a160
select vs.osuser bb,
       vs.username cc,
       vs.process dd,
       vp.spid ee,
       vs.sid ef,
       vs.serial# eg,
       vs.terminal ff,
       vs.program gg,
       vs.server hh,
       vs.status ii,
       vr.name jj,
       vsql.sql_text kk
  from v$rollname vr,
       v$transaction vt,
       v$sqlarea vsql,
       v$process vp,
       v$session vs
 where vs.paddr = vp.addr
   and vs.sql_address = vsql.address (+)
   and vs.sql_hash_value = vsql.hash_value (+)
   and vs.taddr          = vt.addr
   and vt.xidusn         = vr.usn;


