/******************************************************************************/
/*                                                                            */
/*  Copyright (c) 2014 JDA Software Group, Inc.                               */
/*  All rights reserved - Company Confidential                                */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :     freespace.sql                                         */
/*                                                                            */
/*     DESCRIPTION:     Displays free space information for all tablespaces,  */
/*			by datafile within tablespace.			      */
/*                                                                            */
/*                      In a fairly static data/indexes tablespace you        */
/*			probably want to have 15% - 25% free space.  In the   */
/*			system tablespace you probably want to have 50%       */
/*			free space.					      */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/******************************************************************************/

clear columns
clear breaks

set pagesize 66
set linesize 80

column tname format a15 heading 'Tablespace' trunc
column tstatus format a9 heading 'Status' trunc
column tbytes format 999,999,990 heading 'Total (B)'
column ubytes format 999,999,990 heading 'Used (B)'
column fbytes format 999,999,990 heading 'Free (B)'
column fprcnt format 999,999.9 heading 'Free (%)'

select data.tablespace_name tname,
	initcap(dbt.status) tstatus,
	data.bytes tbytes,
	data.bytes - sum(nvl(free.bytes, 0)) ubytes,
	sum(nvl(free.bytes, 0)) fbytes,
	(sum(nvl(free.bytes, 0)) / data.bytes) * 100 fprcnt
from dba_free_space free,
	dba_data_files data,
	dba_tablespaces dbt
where data.file_id = free.file_id (+)
and data.tablespace_name = dbt.tablespace_name
group by data.file_id, data.tablespace_name, data.bytes, dbt.status, data.status
order by 1;

