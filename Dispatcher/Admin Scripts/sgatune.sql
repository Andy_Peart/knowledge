/******************************************************************************/
/*                                                                            */
/*  Copyright (c) 2014 JDA Software Group, Inc.                               */
/*  All rights reserved - Company Confidential                                */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*  FILE NAME  : sgatune.sql                                                  */
/*                                                                            */
/*  DESCRIPTION: Look at the database startup parameters and offer advice     */
/*               based on current database performance. Run this as 'system'  */
/*                                                                            */
/*  DATE     BY   PROJ   ID       DESCRIPTION                                 */
/*  ======== ==== ====== ======== =============                               */
/******************************************************************************/

SET SERVEROUTPUT ON
SET FEEDBACK OFF

declare
	LibraryCache	number;
	RowCache	number;
	BufferCache	number;
	RedoLog		number;
	SharedPoolSize	number;
	BlockBuffers	number;
	LogBuffers	number;
begin
	select value 
	into RedoLog 
	from v$sysstat
	where name = 'redo log space requests';

	select round (100 * (sum(pins)- sum(reloads)) / sum(pins), 2)
	into LibraryCache
	from v$librarycache;

	select round (100 * (sum(gets) - sum(getmisses)) / sum(gets), 2)
	into RowCache
	from v$rowcache;

	select round (100 * 
		     (cur.value + con.value - phys.value) / 
		     (cur.value + con.value), 2)
	into BufferCache
	from v$sysstat cur,v$sysstat con,v$sysstat phys,
	v$statname ncu,v$statname nco,v$statname nph
	where cur.statistic# = ncu.statistic#
	and ncu.name = 'db block gets'
	and con.statistic# = nco.statistic#
	and nco.name = 'consistent gets'
	and phys.statistic# = nph.statistic#
	and nph.name = 'physical reads';

	select value 
	into SharedPoolSize
	from v$parameter 
	where name = 'shared_pool_size';

	select value 
	into BlockBuffers
	from v$parameter 
	where name = 'db_block_buffers';

	select value 
	into LogBuffers
	from v$parameter 
	where name = 'log_buffer';


	/* Output the results of the calculations */
	dbms_output.put_line(chr(13));

	dbms_output.put_line('SGA CACHE STATISTICS');
	dbms_output.put_line('====================');
	dbms_output.put_line('SQL Cache Hit rate        : '|| LibraryCache);
	dbms_output.put_line('Dictionary Cache Hit rate : '|| RowCache);
	dbms_output.put_line('Buffer Cache Hit rate     : '|| BufferCache);
	dbms_output.put_line('Redo Log space requests   : '|| RedoLog);

	dbms_output.put_line(chr(13));
	dbms_output.put_line(chr(13));

	dbms_output.put_line('INIT.ORA SETTING');
	dbms_output.put_line('================');
	dbms_output.put_line('Shared Pool Size (Bytes)  : ' || SharedPoolSize);
	dbms_output.put_line('DB Block Buffer (Blocks)  : ' || BlockBuffers);
	dbms_output.put_line('Log Buffer (Bytes)        : ' || LogBuffers);

	dbms_output.put_line(chr(13));
	dbms_output.put_line(chr(13));

	dbms_output.put_line('HINTS');
	dbms_output.put_line('=====');


	/* Determine what the advice should be */
	if LibraryCache < 99  then 
		dbms_output.put_line('Library Cache too low - increase the Shared Pool Size.'); 
	end if;

	if RowCache < 85  then 
		dbms_output.put_line('Row Cache too low - increase the Shared Pool Size.'); 
	end if;

	if BufferCache < 90  then 
		dbms_output.put_line('Buffer Cache too low - increase the DB Block Buffer value.'); 
	end if;

	if RedoLog > 100 then 
		dbms_output.put_line('Log Buffer value is rather low'); 
	end if;

	dbms_output.put_line(chr(13));
end;
.
/
