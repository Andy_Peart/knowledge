declare
	own varchar2(100);
	name varchar2(100);

	cursor pkgs is
		select owner, object_name
		from dba_objects
		where object_type = 'PACKAGE';

begin
	open pkgs;

	loop
		fetch pkgs into own, name;
		exit when pkgs%notfound;
		dbms_shared_pool.keep(own||'.'||name,'P');
	end loop;
end;
