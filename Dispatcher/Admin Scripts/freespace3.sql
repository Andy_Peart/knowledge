/******************************************************************************/
/*                                                                            */
/*  Copyright (c) 2014 JDA Software Group, Inc.                               */
/*  All rights reserved - Company Confidential                                */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :     freespace3.sql                                        */
/*                                                                            */
/*     DESCRIPTION:     Displays free space information for all tablespaces,  */
/*			by tablespace.					      */
/*                                                                            */
/*                      In a fairly static data/indexes tablespace you        */
/*			probably want to have 15% - 25% free space.  In the   */
/*			system tablespace you probably want to have 50%       */
/*			free space.					      */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   30/11/98 JH   DCS    N/A      Fragmented space monitoring                */
/*   04/05/06 JH   DCS    ENH2620  Make script work with locally managed ts's */
/******************************************************************************/

clear col
clear break
clear comp

set doc on
set echo off
set termout off
set verify off
set linesize 132
set pagesize 23
set feedback off

col tspace format a15 heading 'Tablespace' trunc
col piece format 99,990 heading 'Pieces'
col piece1 format 99,990 heading 'Pieces'
col dbsize format 99,999,999,990 heading 'Total (B)'
col dbfree format 99,999,999,990 heading 'Free (B)'
col dbused format 99,999,999,990 heading 'Used (B)'
col free format 99,999,999,990 heading 'Free (B)'
col free1 format 99,999,999,990 heading 'Free (B)'
col used format 99,999,999,990 heading 'Used (B)'
col sizeb format 99,999,999,990 heading 'Total (B)'
col biggest format 99,999,999,990 heading 'Biggest (B)'
col smallest format 99,999,999,990 heading 'Smallest (B)'
col %USED format 990.9 heading 'Used (%)'
col %FREE format 990.9 heading 'Free (%)'
col value noprint new_v b_size

select value value
from v$parameter
where name = 'db_block_size';

set termout on

select substr(ts.name, 1, 14) tspace,
	tf.blocks * &b_size sizeb,
	(tf.blocks - nvl(sum(f.length), 0)) * &b_size used,
	nvl(sum(f.length), 0) * &b_size free,
	nvl(min(f.length), 0) * &b_size smallest,
	nvl(max(f.length), 0) * &b_size biggest,
	nvl(count(*), 0) piece,
	(tf.blocks - nvl(sum(f.length), 0)) / tf.blocks * 100 "%USED",
	(100 - (tf.blocks - nvl(sum(f.length), 0)) / tf.blocks * 100) "%FREE"
from 	(select ts.ts# ts#,
		dfs.blocks length
	from dba_free_space dfs, sys.ts$ ts
	where dfs.tablespace_name = ts.name) f,
	(select ts#,
		sum(blocks) blocks
	from sys.file$
	where status$ = 2
	group by ts#) tf,
	sys.ts$ ts
where ts.ts# = f.ts# (+)
and ts.ts# = tf.ts#
group by ts.name, tf.blocks
order by ts.name, tf.blocks;

select 'Total' "     ",
	blocks1 * &b_size dbsize,
	(blocks1 - free1) * &b_size dbused,
	free1 * &b_size dbfree,
	piece1,
	(blocks1 - free1) / blocks1 * 100 "%USED",
	100 - ((blocks1 - free1) / blocks1 * 100) "%FREE"
from (select sum(blocks) blocks1
      	from sys.file$
	where status$ = 2),
	(select sum(length) free1,
		count(length) piece1
	from (select dfs.blocks length
		from dba_free_space dfs));

set doc off
set verify on
set feedback on

