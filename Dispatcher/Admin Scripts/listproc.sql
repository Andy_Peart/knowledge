/******************************************************************************/
/*                                                                            */
/*  Copyright (c) 2014 JDA Software Group, Inc.                               */
/*  All rights reserved - Company Confidential                                */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :     listproc.sql                                          */
/*                                                                            */
/*     DESCRIPTION:     Lists currently running processes and hit ratio.      */
/*                                                                            */
/*                      Hit ratio is ratio of buffer to physical block reads. */
/*                      It gives an indication of the efficiency of the query */
/*                      running.  Anything under 90% is bad.  Very low hit    */
/*                      ratios (< 10-20%) in a process can slow down the      */
/*                      whole system.                                         */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   09/02/98 JH   DCS    N/A      Initial version                            */
/******************************************************************************/

clear breaks
clear columns

set pagesize 66
set linesize 132
set verify off

column schemaname format a10 heading 'Schema'
column username format a10 heading 'Username'
column program format a30 heading 'Program'
column process format a9 heading 'PID'
column audsid format 9999999999 heading 'Audsid'
column sid format 99999 heading 'SID'
column physical_reads format 999999999 heading 'Phy Reads'
column hit_ratio format 9.90 heading 'Hit Ratio'

select s.schemaname, p.username, s.program, 
	s.process, s.audsid, s.sid, io.physical_reads,
	(io.block_gets + io.consistent_gets) / 
	(io.block_gets + io.consistent_gets + io.physical_reads) hit_ratio
from v$session s, v$process p, v$sess_io io
where s.paddr = p.addr
and s.status = 'ACTIVE'
and (io.block_gets + io.consistent_gets + io.physical_reads) > 0
and s.sid = io.sid
order by 8 desc;

