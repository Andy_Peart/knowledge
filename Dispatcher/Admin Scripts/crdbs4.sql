--#############################################################################
--                                                                              
--  Copyright (c) 2014 JDA Software Group, Inc.                                 
--  All rights reserved - Company Confidential                                  
--                                                                              
--#############################################################################

--#############################################################################
--                                                                             
--  FILE NAME  : crdbs4.sql                                                    
--                                                                             
--  DESCRIPTION: Create users for new database:			              
--								              
--		dcsdba owns all objects					      
--		dcsusr is for client access				      
--		dcsint is for the oracle interface			      
--		dcsrpt is for user written reports			      
--              dcstbl is for the synchronisetables script                     
--              dcstbl is for the oracletrace script                     
--              dcsmist is for the microstrategy report writer
--              dcsmoca is for the redprairie moca user
--              dcsems is for the redprairie ems user
--              dcsintg is for the redprairie integrator user
--                                                                             
--		Replace all X's by SID of database.		              
--									      
--  DATE     BY   PROJ   ID       DESCRIPTION                                  
--  ======== ==== ====== ======== =============                                
--  29/05/98 JH   DCS    NEW3375  Windows NT porting to Oracle 8               
--  11/06/98 JH   DCS    NEW3383  Change default oracle passwords              
--  19/08/99 JH   DCS    NEW4123  Oracle 8.1.5 port on windows nt              
--  25/05/01 JH   DCS    PDR8568  Remove utl_raw installation                  
--  11/02/02 JH   DCS    NEW5856  Porting to Oracle 9i
--  30/08/02 JH   DCS    NEW6243  Synchronise tables functionality
--  25/10/02 BA   DCS    VV6397   Added grant db link to DCSDBA for PLSQL
--  25/05/04 JH   DCS    NEW7386  Porting to oracle 10g
--  13/01/05 JH   DCS    NEW7664  Add support for Oracle RAC
--  26/01/05 RMW  DCS    NEW7668  Privileges for Oracle tracing
--  03/05/06 JH   DCS    ENH2620  Database security changes for customer
--  25/07/06 JH   DCS    ENH2846  Database creation/upgrade problems
--  13/03/07 RMD  DCS    ENH3547  Add create job to dcsdba             
--  29/05/09 SNP  DCS    DSP2398  Add grant to dba_db_links            
--  13/08/09 JH   DCS    DSP2755  Purge archiving to another db
--  30/10/12 JH   DCS    DSP3420  Table data snapshot functionality         
--#############################################################################

spool crdbs4_XXX.log

connect internal/changemenow as sysdba

--
-- Display options
--

set echo on
set termout on

--
-- Create user who owns all objects
--

create user dcsdba 
	identified by dcsabd
  	default tablespace data
  	temporary tablespace temp;

alter user dcsdba
	quota unlimited on archive
	quota unlimited on auditing
	quota unlimited on snapshot
	quota unlimited on data
	quota unlimited on indexes
	quota unlimited on media
	quota unlimited on report
	quota unlimited on logging;

ORACLE11GONalter user dcsdba
ORACLE11GON	quota unlimited on xdb;

--alter user dcsdba
--	quota unlimited on archivemedium
--	quota unlimited on archivelarge
--	quota unlimited on auditingmedium
--	quota unlimited on auditinglarge
--	quota unlimited on snapshotmedium
--	quota unlimited on snapshotlarge
--	quota unlimited on datamedium
--	quota unlimited on datalarge
--	quota unlimited on indexesmedium
--	quota unlimited on indexeslarge;

-- Allows user to connect to the database.
grant create session to dcsdba;

-- Allows user to create clusters.
grant create cluster to dcsdba;

-- Allows user to create indextypes.
grant create indextype to dcsdba;

-- Allows user to create operators.
grant create operator to dcsdba;

-- Allows user to create procedures.
grant create procedure to dcsdba;

-- Allows user to create sequences.
grant create sequence to dcsdba;

-- Allows user to create tables.
grant create table to dcsdba;

-- Allows user to create triggers.
grant create trigger to dcsdba;

-- Allows user to create types.
grant create type to dcsdba;

-- Allows user to create views.
grant create view to dcsdba;

-- Allows user to turn on the sql trace facility.
grant alter session to dcsdba;

-- Used for session handling (old way of doing it before oracle rac support).
grant select on v_$session to dcsdba;

-- Used for session handling (new way of doing it supporting oracle rac).
grant select on gv_$session to dcsdba;

-- Used for session handling (new way of doing it supporting oracle rac).
grant select on v_$mystat to dcsdba;

-- Used for scheduler functionality program/schedule/job validation
grant select on v_$reserved_words to dcsdba;

-- Used by keepsequences to make sure sequences are cached after db failure.
grant execute on dbms_shared_pool to dcsdba;

-- Allows user to use the dbms_lock package.
grant execute on dbms_lock to dcsdba;

-- Allows user to create private synonyms in any schema.
-- Used by grantaccess to create synonyms for other users.
grant create any synonym to dcsdba;

-- Allows user to create public synonyms.
-- Required for backwards compatability with how grantaccess used to work.
grant create public synonym to dcsdba;

-- Allows user to drop public synonyms.
-- Required for backwards compatability with how grantaccess used to work.
grant drop public synonym to dcsdba;

-- Allows user to create external procedure or function libraries.
-- Required when using the RedPrairie delivery proof system.
grant create library to dcsdba;

-- Allows user to create private database links.
-- Required for third party billing and purge archiving to another database.
grant create database link to dcsdba;

-- Allows user to create a trigger on database.
-- Required if using RedPrairie ems triggers on database (not currently used).
grant administer database trigger to dcsdba;

-- Allows user to create scheduler jobs, programs and schedules
grant create job to dcsdba;
grant create external job to dcsdba;

-- Allows user to see the user view for dcsdba (via dcs_scheduler_job_run_details).
grant select on dba_scheduler_job_run_details to dcsdba with grant option;

-- Allows user to connect the current session to a debugger.
--grant debug connect session to dcsdba;

-- Allows user to debug all pl/sql and java code in any database object.
--grant debug any procedure to dcsdba;

-- Allows user to see the user_jobs view for dcsdba (via dcs_user_jobs).
grant select on dba_jobs to dcsdba with grant option;

-- Allows emaildae oracle email to work (via utl_smtp).
ORACLE11GONgrant execute on utl_smtp to dcsdba;

-- Allows user to see the user_db_links view for dcsdba (via dcs_db_links).
grant select on dba_db_links to dcsdba with grant option;

-- Allows user to create directories.
grant create any directory to dcsdba;

-- Allows performance tuning tools to work.
grant select_catalog_role to dcsdba;
grant select any dictionary to dcsdba;

--
-- Add the explain plan/set autotrace table/role for dcsdba
--

@$ORACLE_HOME/sqlplus/admin/plustrce.sql

grant plustrace to dcsdba;

connect dcsdba/dcsabd

@$ORACLE_HOME/rdbms/admin/utlxplan.sql

--
-- Create user for pc client access
--

connect internal/changemenow as sysdba

create user dcsusr 
	identified by dcsrsu
  	default tablespace data
  	temporary tablespace temp;

-- Allows user to connect to the database.
grant create session to dcsusr;

-- Allows user to turn on the sql trace facility.
grant alter session to dcsusr;

-- Used for session handling (old way of doing it before oracle rac support).
grant select on v_$session to dcsusr;

-- Used for session handling (new way of doing it supporting oracle rac).
grant select on gv_$session to dcsusr;

-- Used for session handling (new way of doing it supporting oracle rac).
grant select on v_$mystat to dcsusr;

-- Used for scheduler functionality program/schedule/job validation
grant select on v_$reserved_words to dcsusr;

-- Allows user to use the dbms_lock package.
grant execute on dbms_lock to dcsusr;

-- Allows user to create database triggers in any schema.
-- Required if using the RedPrairie ems triggers functionality.
grant create any trigger to dcsusr;

-- Allows user to create private database links.
-- Required for third party billing and purge archiving to another database.
grant create database link to dcsusr;

-- Allows emaildae oracle email to work (via utl_smtp).
ORACLE11GONgrant execute on utl_smtp to dcsusr;

--
-- Create user for user written reports
--

connect internal/changemenow as sysdba

create user dcsrpt 
	identified by dcstpr
  	default tablespace data
  	temporary tablespace temp;

-- Allows user to connect to the database.
grant create session to dcsrpt;

-- Allows user to create private database links.
-- Required for third party billing and purge archiving to another database.
grant create database link to dcsrpt;

--
-- Create user for the oracle interface
--

connect internal/changemenow as sysdba

create user dcsint 
	identified by dcstni
  	default tablespace data
  	temporary tablespace temp;

-- Allows user to connect to the database.
grant create session to dcsint;

--
-- Create user who owns table synchronisation objects
--

connect internal/changemenow as sysdba

create user dcstbl 
	identified by dcslbt
        default tablespace tblsync
        temporary tablespace temp;

alter user dcstbl
	quota unlimited on tblsync;

-- Allows user to connect to the database.
grant create session to dcstbl;

-- Allows user to create clusters.
grant create cluster to dcstbl;

-- Allows user to create indextypes.
grant create indextype to dcstbl;

-- Allows user to create operators.
grant create operator to dcstbl;

-- Allows user to create procedures.
grant create procedure to dcstbl;

-- Allows user to create sequences.
grant create sequence to dcstbl;

-- Allows user to create tables.
grant create table to dcstbl;

-- Allows user to create triggers.
grant create trigger to dcstbl;

-- Allows user to create types.
grant create type to dcstbl;

-- Allows user to create views.
grant create view to dcstbl;

-- Used by keepsequences to make sure sequences are cached after db failure.
grant execute on dbms_shared_pool to dcstbl;

-- Allows user to see the user view for dcsdba (via dcs_scheduler_job_run_details).
grant select on dba_scheduler_job_run_details to dcstbl with grant option;

-- Allows user to see the user_jobs view for dcsdba (via dcs_user_jobs).
grant select on dba_jobs to dcstbl with grant option;

-- Allows user to see the user_db_links view for dcsdba (via dcs_db_links).
grant select on dba_db_links to dcstbl with grant option;

--
-- Create user who can see oracle trace objects
--

connect internal/changemenow as sysdba

create user dcstrc 
	identified by dcscrt
        default tablespace data
        temporary tablespace temp;

-- Allows user to connect to the database.
grant create session to dcstrc;

-- Allow user to access objects required by the oracle trace script
grant execute on dbms_system to dcstrc;

-- Allow user to access objects required by the oracle trace script
grant select any dictionary to dcstrc;

MISTUSERON--
MISTUSERON-- Create user who owns all MicroStrategy objects
MISTUSERON--
MISTUSERON
MISTUSERONconnect internal/changemenow as sysdba
MISTUSERON
MISTUSERONcreate user dcsmist 
MISTUSERON	identified by dcstsim
MISTUSERON  	default tablespace mist
MISTUSERON  	temporary tablespace temp;
MISTUSERON
MISTUSERONalter user dcsmist
MISTUSERON      quota unlimited on mist;
MISTUSERON
MISTUSERON-- Allows user to connect to the database.
MISTUSERONgrant create session to dcsmist;
MISTUSERON
MISTUSERON-- Allows user to create clusters.
MISTUSERONgrant create cluster to dcsmist;
MISTUSERON
MISTUSERON-- Allows user to create indextypes.
MISTUSERONgrant create indextype to dcsmist;
MISTUSERON
MISTUSERON-- Allows user to create operators.
MISTUSERONgrant create operator to dcsmist;
MISTUSERON
MISTUSERON-- Allows user to create procedures.
MISTUSERONgrant create procedure to dcsmist;
MISTUSERON
MISTUSERON-- Allows user to create sequences.
MISTUSERONgrant create sequence to dcsmist;
MISTUSERON
MISTUSERON-- Allows user to create tables.
MISTUSERONgrant create table to dcsmist;
MISTUSERON
MISTUSERON-- Allows user to create triggers.
MISTUSERONgrant create trigger to dcsmist;
MISTUSERON
MISTUSERON-- Allows user to create types.
MISTUSERONgrant create type to dcsmist;
MISTUSERON
MISTUSERON--
MISTUSERON-- Add the explain plan/set autotrace table/role for dcsmist
MISTUSERON--
MISTUSERON
MISTUSERON@$ORACLE_HOME/sqlplus/admin/plustrce.sql
MISTUSERON
MISTUSERONgrant plustrace to dcsmist;
MISTUSERON
MISTUSERONconnect dcsmist/dcstsim
MISTUSERON
MISTUSERON@$ORACLE_HOME/rdbms/admin/utlxplan.sql

MOCAUSERON--
MOCAUSERON-- Create user who owns all MOCA objects
MOCAUSERON--
MOCAUSERON
MOCAUSERONconnect internal/changemenow as sysdba
MOCAUSERON
MOCAUSERONcreate user dcsmoca identified by dcsacom
MOCAUSERON  	default tablespace moca
MOCAUSERON  	temporary tablespace temp;
MOCAUSERON
MOCAUSERONalter user dcsmoca
MOCAUSERON      quota unlimited on moca;
MOCAUSERON
MOCAUSERON-- Allows user to connect to the database.
MOCAUSERONgrant create session to dcsmoca;
MOCAUSERON
MOCAUSERON-- Allows user to create clusters.
MOCAUSERONgrant create cluster to dcsmoca;
MOCAUSERON
MOCAUSERON-- Allows user to create indextypes.
MOCAUSERONgrant create indextype to dcsmoca;
MOCAUSERON
MOCAUSERON-- Allows user to create operators.
MOCAUSERONgrant create operator to dcsmoca;
MOCAUSERON
MOCAUSERON-- Allows user to create procedures.
MOCAUSERONgrant create procedure to dcsmoca;
MOCAUSERON
MOCAUSERON-- Allows user to create sequences.
MOCAUSERONgrant create sequence to dcsmoca;
MOCAUSERON
MOCAUSERON-- Allows user to create tables.
MOCAUSERONgrant create table to dcsmoca;
MOCAUSERON
MOCAUSERON-- Allows user to create triggers.
MOCAUSERONgrant create trigger to dcsmoca;
MOCAUSERON
MOCAUSERON-- Allows user to create types.
MOCAUSERONgrant create type to dcsmoca;
MOCAUSERON
MOCAUSERON-- Allows user to create views.
MOCAUSERONgrant create view to dcsmoca;
MOCAUSERON
MOCAUSERON-- Allows user to create database triggers in any schema.
MOCAUSERONgrant create any trigger to dcsmoca;
MOCAUSERON
MOCAUSERON-- Allows user to query tables and views in any schema
MOCAUSERONgrant select any table to dcsmoca;
MOCAUSERON 
MOCAUSERON-- Allows user to query any data dictionary object in the sys schema.
MOCAUSERONgrant select any dictionary to dcsmoca;
MOCAUSERON
MOCAUSERON--
MOCAUSERON-- Add the explain plan/set autotrace table/role for dcsmoca
MOCAUSERON--
MOCAUSERON
MOCAUSERON@$ORACLE_HOME/sqlplus/admin/plustrce.sql
MOCAUSERON
MOCAUSERONgrant plustrace to dcsmoca;
MOCAUSERON
MOCAUSERONconnect dcsmoca/dcsacom
MOCAUSERON
MOCAUSERON@$ORACLE_HOME/rdbms/admin/utlxplan.sql

EMSUSERON--
EMSUSERON-- Create user who owns all EMS objects
EMSUSERON--
EMSUSERON
EMSUSERONconnect internal/changemenow as sysdba
EMSUSERON
EMSUSERONcreate user dcsems identified by dcssme
EMSUSERON  	default tablespace ems
EMSUSERON  	temporary tablespace temp;
EMSUSERON
EMSUSERONalter user dcsems
EMSUSERON       quota unlimited on ems;
EMSUSERON
EMSUSERON-- Allows user to connect to the database.
EMSUSERONgrant create session to dcsems;
EMSUSERON
EMSUSERON-- Allows user to create clusters.
EMSUSERONgrant create cluster to dcsems;
EMSUSERON
EMSUSERON-- Allows user to create indextypes.
EMSUSERONgrant create indextype to dcsems;
EMSUSERON
EMSUSERON-- Allows user to create operators.
EMSUSERONgrant create operator to dcsems;
EMSUSERON
EMSUSERON-- Allows user to create procedures.
EMSUSERONgrant create procedure to dcsems;
EMSUSERON
EMSUSERON-- Allows user to create sequences.
EMSUSERONgrant create sequence to dcsems;
EMSUSERON
EMSUSERON-- Allows user to create tables.
EMSUSERONgrant create table to dcsems;
EMSUSERON
EMSUSERON-- Allows user to create views.
EMSUSERONgrant create view to dcsems;
EMSUSERON
EMSUSERON-- Allows user to create triggers.
EMSUSERONgrant create trigger to dcsems;
EMSUSERON
EMSUSERON-- Allows user to create types.
EMSUSERONgrant create type to dcsems;
EMSUSERON
EMSUSERON--
EMSUSERON-- Add the explain plan/set autotrace table/role for dcsems
EMSUSERON--
EMSUSERON
EMSUSERON@$ORACLE_HOME/sqlplus/admin/plustrce.sql
EMSUSERON
EMSUSERONgrant plustrace to dcsems;
EMSUSERON
EMSUSERONconnect dcsems/dcssme
EMSUSERON
EMSUSERON@$ORACLE_HOME/rdbms/admin/utlxplan.sql

INTGUSERON--
INTGUSERON-- Create user who owns all Integrator objects
INTGUSERON--
INTGUSERON
INTGUSERONconnect internal/changemenow as sysdba
INTGUSERON
INTGUSERONcreate user dcsintg identified by dcsgtni
INTGUSERON  	default tablespace intg
INTGUSERON  	temporary tablespace temp;
INTGUSERON
INTGUSERONalter user dcsintg
INTGUSERON      quota unlimited on intg;
INTGUSERON
INTGUSERON-- Allows user to connect to the database.
INTGUSERONgrant create session to dcsintg;
INTGUSERON
INTGUSERON-- Allows user to create clusters.
INTGUSERONgrant create cluster to dcsintg;
INTGUSERON
INTGUSERON-- Allows user to create indextypes.
INTGUSERONgrant create indextype to dcsintg;
INTGUSERON
INTGUSERON-- Allows user to create operators.
INTGUSERONgrant create operator to dcsintg;
INTGUSERON
INTGUSERON-- Allows user to create procedures.
INTGUSERONgrant create procedure to dcsintg;
INTGUSERON
INTGUSERON-- Allows user to create sequences.
INTGUSERONgrant create sequence to dcsintg;
INTGUSERON
INTGUSERON-- Allows user to create tables.
INTGUSERONgrant create table to dcsintg;
INTGUSERON
INTGUSERON-- Allows user to create views.
INTGUSERONgrant create view to dcsintg;
INTGUSERON
INTGUSERON-- Allows user to create triggers.
INTGUSERONgrant create trigger to dcsintg;
INTGUSERON
INTGUSERON-- Allows user to create types.
INTGUSERONgrant create type to dcsintg;
INTGUSERON
INTGUSERON-- Allows user to create database triggers in any schema.
INTGUSERONgrant create any trigger to dcsintg;
INTGUSERON
INTGUSERON-- Allows user to query tables and views in any schema
INTGUSERONgrant select any table to dcsintg;
INTGUSERON 
INTGUSERON-- Allows user to query any data dictionary object in the sys schema.
INTGUSERONgrant select any dictionary to dcsintg;
INTGUSERON
INTGUSERON--
INTGUSERON-- Add the explain plan/set autotrace table/role for dcsintg
INTGUSERON--
INTGUSERON
INTGUSERON@$ORACLE_HOME/sqlplus/admin/plustrce.sql
INTGUSERON
INTGUSERONgrant plustrace to dcsintg;
INTGUSERON
INTGUSERONconnect dcsintg/dcsgtni
INTGUSERON
INTGUSERON@$ORACLE_HOME/rdbms/admin/utlxplan.sql

--
-- Change default oracle passwords and lock extra accounts (oracle 8.1.5+)
--

connect internal/changemenow as sysdba

-- Required for password encryption functionality.
grant execute on dbms_crypto to public;

alter user system identified by pleasechangeme;

ORACLE10GOFFalter user sys identified by iwanttobechanged;

alter user outln account lock;

alter user dbsnmp account lock;

alter user dip account lock;

alter user tsmsys account lock;

--ORACLE11GONalter user anonymous account unlock;

ORACLE11GONalter profile default limit password_life_time unlimited;

disconnect

--
-- End of crdbs4_XXX.sql
--

spool off

