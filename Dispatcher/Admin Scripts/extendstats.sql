
declare
	cg_name varchar2(30);
begin
	begin
		dbms_stats.drop_extended_stats(NULL,'SERIAL_NUMBER', '(tag_id, status)');
	exception when others then 
		null;
	end;
	cg_name := dbms_stats.create_extended_stats(NULL,'SERIAL_NUMBER', '(tag_id, status)');
	dbms_stats.gather_table_stats(null,'SERIAL_NUMBER',method_opt=>'for all columns size skewonly for columns (tag_id, status) size skewonly');
end;
/

declare
	cg_name varchar2(30);
begin
	begin
		dbms_stats.drop_extended_stats(NULL,'INVENTORY', '(tag_id, location_id, site_id)');
	exception when others then 
		null;
	end;
	cg_name := dbms_stats.create_extended_stats(NULL,'INVENTORY', '(tag_id, location_id, site_id)');
	dbms_stats.gather_table_stats(null,'INVENTORY',method_opt=>'for all columns size skewonly for columns (tag_id, location_id, site_id) size skewonly');
end;
/
 
set pagesize 9999
set linesize 200

select column_name, num_distinct, histogram from user_tab_col_statistics
where table_name = 'SERIAL_NUMBER'
/

select column_name, num_distinct, histogram from user_tab_col_statistics
where table_name = 'INVENTORY'
/

