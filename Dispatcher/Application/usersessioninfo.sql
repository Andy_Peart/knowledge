select ase.user_id "User", 
	ase.station_id "Station", 
        rpad(ase.session_type, 10) "Type",
	substr(to_char(vs.audsid), 1, 10) "Audsid", 
	substr(to_char(vs.sid), 1, 10) "Sid", 
	substr(to_char(vs.serial#), 1, 10) "Serial", 
	substr(to_char(vs.inst_id), 1, 10) "Instance", 
	vp.spid "PID"
from v$process vp, gv$session vs, dcsdba.application_session ase
where ase.user_id=upper('&user')
and vs.sid=ase.sid
and vs.serial#=ase.serial#
and vs.inst_id=ase.inst_id
and vp.addr=vs.paddr
/
