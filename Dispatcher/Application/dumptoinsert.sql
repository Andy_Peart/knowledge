/******************************************************************************/
/*                                                                            */
/*  Copyright (c) 2014 JDA Software Group, Inc.                               */
/*  All rights reserved - Company Confidential                                */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*  FILE NAME  : dumptoinsert.sql                                             */
/*                                                                            */
/*  DESCRIPTION: Procedure to dump data in a given table to insert statements */
/*               which can then be used to put data in another table.         */
/*                                                                            */
/*               To use this go into SQL*Plus. At the SQL prompt type:        */
/*                                                                            */
/*               @dumptoinsert                                                */
/*                                                                            */
/*               Then at the SQL prompt type something like this:             */
/*                                                                            */
/*               execute DumpToInsertSQL ('trailer', null, '/tmp', 'myfile'); */
/*                                                                            */
/*               Note that the second parameter is a where clause to reduce   */
/*               the data returned. For example replace null above with:      */
/*                                                                            */
/*               'column = ''value'''.                                        */
/*                                                                            */
/*               For tables with a large number of columns the number of      */
/*               cursors on the database may need increasing (one per         */
/*               column. The parameter is 'open_cursors'.                     */
/*                                                                            */
/*               Note that this does not support tables containing columns of */
/*               data type long.                                              */
/*                                                                            */
/*  DATE     BY   PROJ   ID       DESCRIPTION                                 */
/*  ======== ==== ====== ======== =============                               */
/******************************************************************************/

create or replace procedure DumpToInsertSQL
(
TableName	in	varchar2,
WhereClause	in	varchar2	default null,
Directory	in	varchar2	default null,
OutputFile	in	varchar2	default null
)
is
	/* Join to All_Tables so we can't get views (Inserts would fail) */
	cursor TableColumnSearch
		(
		TableName	in	varchar2
		)
	is
	select ATC.Column_Name, ATC.data_type
	from All_Tab_Columns ATC, All_Tables ATS
	where ATC.Table_Name = ATS.Table_Name
	and ATS.Table_Name = upper (TableName)
	and ATC.Data_Type in ('TIMESTAMP(6) WITH LOCAL TIME ZONE', 'NUMBER', 'VARCHAR2');

	TableColumnRow		TableColumnSearch%rowtype;

	type TableOfColumns is table of TableColumnSearch%rowtype
	index by binary_integer;

	type TableOfCursors is table of integer
	index by binary_integer;

	type TableOfValues is table of varchar2 (2000)
	index by binary_integer;


	CursorTable		TableOfCursors;
	ColumnTable		TableOfColumns;
	ValueTable		TableOfValues;

	FileHandle		utl_file.file_type;
	SelectStatement		varchar2 (2000);
	ColumnList		varchar2 (2000);
	CurrentValue		varchar2 (2000);
	TmpDirectory		varchar2 (2000) := null;
	TmpOutputFile		varchar2 (2000) := 'dumptoinsert.lst';
	CurrentRowID		rowid;
	Ignore			number;
	NumberOfColumns		number := 0;
	SelectionCursor		integer;
	i 			binary_integer := 0;

/******************************************************************************/
/*                                                                            */
/*    ROUTINE NAME: OpenFile                                                  */
/*                                                                            */
/*    DESCRIPTION:  Open a file to output the insert statments to. Note that  */
/*                  this is a local procedure to the main stored procedure.   */
/*                                                                            */
/*  RELEASE   DATE     BY  PROJ   ID       DESCRIPTION                        */
/*  ======== ======== ==== ====== ======== =============                      */
/******************************************************************************/
procedure	OpenFile
(
Directory	in	varchar2,
OutputFile	in	varchar2
)
is

begin
	if utl_file.is_open (FileHandle) then
		utl_file.fclose (FileHandle);
	end if;

	/* Open it for reading to check the permissions */
	FileHandle := utl_file.fopen (Directory, OutputFile, 'r');

	utl_file.fclose (FileHandle);

	/* Open the file to append to */
	FileHandle := utl_file.fopen (Directory, OutputFile, 'a');

exception
	when utl_file.invalid_path then
		raise_application_error (-20101, 'OpenFile: Invalid path');

	when utl_file.invalid_mode then
		raise_application_error (-20102, 'OpenFile: Invalid mode');

	when utl_file.invalid_operation then
		begin
			/* Create the file to write to */
			FileHandle := utl_file.fopen (Directory, 
						      OutputFile,
						      'w');

		exception
			when utl_file.invalid_path then
				raise_application_error (-20103,
						'OpenFile: Invalid path');

			when utl_file.invalid_mode then
				raise_application_error (-20104,
						'OpenFile: Invalid mode');

			when utl_file.invalid_operation then
				raise_application_error (-20105,
					'OpenFile: Failed to open file [' ||
					OutputFile ||
					'] in directory [' ||
					Directory ||
					']');
		end;


end OpenFile;


/******************************************************************************/
/*                                                                            */
/*    ROUTINE NAME: PrintToFile                                               */
/*                                                                            */
/*    DESCRIPTION:  Write a string to the previously opened output file. Note */
/*                  that this is a local procedure.                           */
/*                                                                            */
/*  RELEASE   DATE     BY  PROJ   ID       DESCRIPTION                        */
/*  ======== ======== ==== ====== ======== =============                      */
/******************************************************************************/
procedure	PrintToFile
(
Text	in	varchar2
)
is
	StartChar	integer := 1;
	StringLength	integer := 0;
	TmpText		varchar2 (2000);
begin
	TmpText := substr (Text, StartChar, 2000);
	StringLength := length (TmpText);

	/* Loop reading 1000 character blocks */
	loop
		utl_file.putf(FileHandle, '%s\n', TmpText);

		StartChar := StartChar + 2000;
		TmpText := substr (Text, StartChar, 2000);

		exit when StartChar > StringLength;
	end loop;

	utl_file.fflush (FileHandle);

exception
	when utl_file.invalid_operation then
		raise_application_error (-20103,
					 'PrintToFile: Invalid operation');

	when utl_file.invalid_filehandle then
		raise_application_error (-20104,
					 'PrintToFile: Invalid file handle');

	when utl_file.write_error then
		raise_application_error (-20105,
					 'PrintToFile: Write error');

end PrintToFile;


/******************************************************************************/
/*                                                                            */
/*    ROUTINE NAME: CloseFile                                                 */
/*                                                                            */
/*    DESCRIPTION:  Close the previously opened output file. Note that this   */
/*                  is a local procedure.                                     */
/*                                                                            */
/*  RELEASE   DATE     BY  PROJ   ID       DESCRIPTION                        */
/*  ======== ======== ==== ====== ======== =============                      */
/******************************************************************************/
procedure	CloseFile
is

begin
	if utl_file.is_open (FileHandle) then
		utl_file.fclose (FileHandle);
	end if;

end CloseFile;


begin
	/* Get the information to open the output file */
	TmpDirectory := Directory;

	if OutputFile is not null then
		TmpOutputFile := Outputfile;
	end if;

	OpenFile (TmpDirectory, TmpOutputFile);


	/* Fetch the columns into a PL/SQL table */
	for TableColumnRow in TableColumnSearch (TableName) loop
		i := i + 1;
		ColumnTable (i) := TableColumnRow;
	end loop;

	NumberOfColumns := i;


	for i in 1..NumberOfColumns loop
		/* Add each column name to a list */
		if ColumnList is null then
			ColumnList := ColumnTable (i).column_name;
		else
			ColumnList := ColumnList || ',' ||
				      ColumnTable (i).column_name;
		end if;

		if ColumnTable (i).data_type = 'TIMESTAMP(6) WITH LOCAL TIME ZONE' then
			/*
			All the quotes are so our strings have enough in the end
			*/
			SelectStatement := 'SELECT ''TO_DATE('''''' ||
					to_char('||
					ColumnTable (i).column_name ||
					', ''YYYYMMDDHH24MISS'') ||
					'''''', ''''YYYYMMDDHH24MISS'''')'' FROM ' ||
					TableName ||
					' WHERE rowid = :x';

		elsif ColumnTable (i).data_type = 'NUMBER' then
			SelectStatement := 'SELECT to_char('||
						ColumnTable (i).column_name ||
						')' || ' FROM ' ||
						TableName ||
						' WHERE rowid = :x';

		elsif ColumnTable (i).data_type = 'VARCHAR2' then
			SelectStatement := 'SELECT ''''||' ||
					ColumnTable (i).column_name ||
					'||'''' FROM '||
					TableName ||
					' WHERE rowid = :x';
		end if;

		CursorTable (i) := dbms_sql.open_cursor;
		dbms_sql.parse (CursorTable (i),
				SelectStatement,
				dbms_sql.native);
	end loop;


	/*
	Because dbms_sql requires us to call define_column for each
	column, we cannot have dynamic number of columns fetched
	Instead, we need to fetch each column seperately.
	*/
	SelectStatement := 'SELECT rowid'|| ' FROM '|| TableName;

	if WhereClause is not null then
		SelectStatement := SelectStatement || ' WHERE '|| WhereClause;
	end if;

	SelectionCursor := dbms_sql.open_cursor;
	dbms_sql.parse (SelectionCursor, SelectStatement, dbms_sql.native);
	dbms_sql.define_column_rowid (SelectionCursor, 1, CurrentRowID);
	Ignore := dbms_sql.execute (SelectionCursor);


	loop
		if dbms_sql.fetch_rows (SelectionCursor) > 0 then
			dbms_sql.column_value (SelectionCursor, 1, CurrentRowID);

			for i in 1..NumberOfColumns loop
				dbms_sql.define_column (CursorTable (i),
							1,
							CurrentValue,
							2000);

				dbms_sql.bind_variable (CursorTable (i),
							'x',
							CurrentRowID);

				Ignore := dbms_sql.execute (CursorTable (i));

				if dbms_sql.fetch_rows (CursorTable (i)) > 0 then
					dbms_sql.column_value (CursorTable (i),
							       1,
							       CurrentValue);
				else
					CurrentValue := null;
				end if;

				if CurrentValue IS null then
					CurrentValue := 'null';
				end if;

				ValueTable (i) := CurrentValue;
			end loop;


			/* Output this row */
			PrintToFile ('INSERT INTO '|| TableName || '(');
			PrintToFile (ColumnList);
			PrintToFile (') VALUES (');

			for I in 1..NumberOfColumns loop
				if i > 1 then
					PrintToFile (',');
				end if;

				/* 
				All the output values have quotes put round 
				them after having any quotes within them 
				mapped correctly. This means that numbers 
				have quotes around them too. Whilst this may 
				look strange the resulting insert statment 
				still works.
				*/
				if substr (ValueTable(i), 1, 7) <> 'TO_DATE'
				and ValueTable(i) <> 'null' then
					/* Add surrounding quotes */
					ValueTable(i) := '''' ||
							replace (ValueTable (i),
							'''', 
							'''''') || 
							'''';
				end if;

				PrintToFile (ValueTable(i));
			end loop output_values;

			PrintToFile (')');
			PrintToFile ('/');
		else
			exit;	/* Loop */
		end if;
	end loop;


	/* Close all the cursors */
	for i in 1..NumberOfColumns loop
		dbms_sql.close_cursor (CursorTable (i));
	end loop;

	dbms_sql.close_cursor (SelectionCursor);


	CloseFile;

end DumpToInsertSQL;
/
