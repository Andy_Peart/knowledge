/******************************************************************************/
/*                                                                            */
/*  Copyright (c) 2014 JDA Software Group, Inc.                               */
/*  All rights reserved - Company Confidential                                */
/*                                                                            */
/******************************************************************************/
 
/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :     itlsummarybydate.sql                                  */
/*                                                                            */
/*     DESCRIPTION:     List itl summary for specified date.                  */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/******************************************************************************/
 
SET FEEDBACK ON
SET VERIFY OFF 
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 80
SET SPACE 2
 
/* Top Title */
TTITLE CENTER _SITENAME SKIP 2 -
LEFT 'ITL Summary By Date' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 2 -
LEFT 'Date &1' SKIP 2

BREAK ON REPORT

COMPUTE SUM OF Count ON REPORT

/* Column Headings and Formats */
COLUMN Code HEADING 'Code' FORMAT A15
COLUMN Count HEADING 'Count' FORMAT 9999999999

/* SQL Select statement */
SELECT Code, COUNT(*) Count
FROM Inventory_Transaction
WHERE ('&1' IS NULL) OR (TRUNC(DStamp) = '&1')
GROUP BY Code;

SET TERMOUT ON

