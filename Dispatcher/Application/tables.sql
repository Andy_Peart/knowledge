/******************************************************************************/
/*                                                                            */
/*  Copyright (c) 2014 JDA Software Group, Inc.                               */
/*  All rights reserved - Company Confidential                                */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :     tables.sql                                            */
/*                                                                            */
/*     DESCRIPTION:     Lists the tables in the database.                     */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/******************************************************************************/

SET FEEDBACK ON
SET VERIFY OFF 
 
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 80
 
COLUMN Table_Name	HEADING 'Table'		FORMAT A15
COLUMN Column_Name	HEADING 'Column'	FORMAT A20
COLUMN Data_Type	HEADING 'Type'		FORMAT A9
COLUMN Char_Length	HEADING 'Char Len'	FORMAT 9999
COLUMN Nullable		HEADING 'Null?'		FORMAT A8
COLUMN Data_Precision	HEADING 'Numb Len'	FORMAT 99
COLUMN Data_Scale	HEADING 'Dec Pl'	FORMAT 9

BREAK ON Table_Name SKIP 1

SELECT Table_Name, 
	Column_Name, 
	Data_Type, 
	TO_NUMBER(DECODE(Char_Length, 0, null, Char_Length)) Char_Length, 
	Data_Precision, 
	Data_Scale, 
	DECODE(Nullable, 'N' , 'NOT NULL', '') Nullable 
FROM User_Tab_Columns
WHERE Table_Name NOT LIKE 'BIN\$%'
ORDER BY Table_Name, Column_ID;

