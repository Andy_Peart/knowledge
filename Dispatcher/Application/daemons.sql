/******************************************************************************/
/*                                                                            */
/*  Copyright (c) 2014 JDA Software Group, Inc.                               */
/*  All rights reserved - Company Confidential                                */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :     daemons.sql                                           */
/*                                                                            */
/*     DESCRIPTION:     Outputs list of daemons and their session info.       */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/******************************************************************************/

set pagesize 1000

SELECT SUBSTR(AD.Daemon_Name, 0, 12) "Name", 
       SUBSTR(TO_CHAR(AD.Start_DStamp,'DD-MON-YYYY HH12:MI AM'),0,18) "Start time",
       DECODE(AD.Status,  'T', 'Starting', 
                          'L', 'Sleeping', 
                          'R', 'Running', 
                          'A', 'Stalled', 
                          'O', 'Stopped', 
                          '??') "Status",
       SUBSTR(LibDate.IntervalMinute(CURRENT_TIMESTAMP - AD.Last_DStamp), 0, 7) "Idle m", 
       SUBSTR(AD.Sleep_Time, 0, 7) "Sleep s",
       SUBSTR(L.Text, 0, 21) "Description"
FROM Application_Daemon AD, Language_Text L 
WHERE L.Label = CONCAT('DAE', AD.Daemon_Name)
AND L.Language = 'EN_GB'
ORDER BY AD.Daemon_Name, AD.Session_ID
/
