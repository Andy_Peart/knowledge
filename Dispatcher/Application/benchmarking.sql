/******************************************************************************/
/*                                                                            */
/*  Copyright (c) 2014 JDA Software Group, Inc.                               */
/*  All rights reserved - Company Confidential                                */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :     benchmarking.sql                                      */
/*                                                                            */
/*     DESCRIPTION:     Lists the benchmarking results.                       */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/******************************************************************************/

SET FEEDBACK ON
SET VERIFY OFF 
 
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 80
 
COLUMN Station_ID	HEADING 'Station'		FORMAT A10
COLUMN User_ID		HEADING 'User'			FORMAT A10
COLUMN Bench_Type	HEADING 'Type'			FORMAT A15
COLUMN Bench_Count	HEADING 'Tasks/Lines'		FORMAT 9999
COLUMN Bench_Time	HEADING 'Time (Secs)'		FORMAT 9999
COLUMN Bench_Avge	HEADING 'Average (Secs)'	FORMAT 9999.9

SELECT Station_ID, 
	User_ID,
	Bench_Type,
	Bench_Count,
	LibDate.IntervalSecond(End_DStamp - Start_DStamp) Bench_Time,
	LibDate.IntervalSecond(End_DStamp - Start_DStamp) / Bench_Count Bench_Avge
FROM Benchmark
WHERE Bench_Count IS NOT NULL
AND Bench_Count > 0
ORDER BY Start_DStamp, Key;

