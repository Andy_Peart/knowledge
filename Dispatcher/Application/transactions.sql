/******************************************************************************/
/*                                                                            */
/*  Copyright (c) 2014 JDA Software Group, Inc.                               */
/*  All rights reserved - Company Confidential                                */
/*                                                                            */
/******************************************************************************/
 
/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :     transactions.sql                                      */
/*                                                                            */
/*     DESCRIPTION:     List transactions by date/hour.                       */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   16/01/99 JH   DCS    NEW3709  Application dependant licencing            */
/******************************************************************************/
 
SET FEEDBACK ON
SET VERIFY OFF 
 
/* Clear previous settings */
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
/* Set up page and line */
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 132
SET SPACE 2
 
/* Top Title */
TTITLE CENTER _SITENAME SKIP 2 -
LEFT 'Transactions By Date/Hour' RIGHT _DATESTAMP SKIP 1 -
RIGHT _TIMESTAMP SKIP 1 -
RIGHT 'Page: ' FORMAT 999 SQL.PNO SKIP 3 

/* Column Headings and Formats */
COLUMN Site_ID HEADING 'Site' FORMAT A10
COLUMN Owner_ID HEADING 'Owner' FORMAT A10
COLUMN TheDate HEADING 'Date' FORMAT A11
COLUMN TheHour HEADING 'Hour' FORMAT A5
COLUMN Receipt HEADING 'Receipt' FORMAT '999G990'
COLUMN Putaway HEADING 'Putaway' FORMAT '999G990'
COLUMN Relocate HEADING 'Relocate' FORMAT '999G990'
COLUMN Stock_Check HEADING 'Stock Check' FORMAT '999G990'
COLUMN Replenish HEADING 'Replenish' FORMAT '999G990'
COLUMN Pick HEADING 'Pick' FORMAT '999G990'
COLUMN Shipment HEADING 'Shipment' FORMAT '999G990'
COLUMN Total HEADING 'Total' FORMAT '999G990'

BREAK ON Site_ID SKIP PAGE ON Owner_ID SKIP PAGE ON TheDate SKIP 1 On TheTime

COMPUTE SUM OF Receipt Putaway Pick Replenish Relocate Stock_Check Shipment Total ON TheDate

/* SQL Select statement */
SELECT   Site_ID,
	 Owner_ID,
	 TO_CHAR(DStamp, 'DD-MON-YYYY') TheDate,
	 TO_CHAR(DStamp, 'HH24') TheHour,
	 SUM (DECODE (Code, 'Receipt', 1, 0)) Receipt,
	 SUM (DECODE (Code, 'Putaway', 1, 0)) Putaway,
	 SUM (DECODE (Code, 'Relocate',1, 0)) Relocate,
	 SUM (DECODE (Code, 'Stock Check', 1, 0)) Stock_Check,
	 SUM (DECODE (Code, 'Replenish',1, 0)) Replenish,
	 SUM (DECODE (Code, 'Pick', 1, 'Marshal', 1, 'Consol', 1, 0)) Pick,
	 SUM (DECODE (Code, 'Shipment',1, 0)) Shipment,
	 SUM (DECODE (Code, 'Receipt', 1,
			    'Putaway', 1,
			    'Relocate', 1,
			    'Stock Check', 1, 
			    'Replenish', 1,
			    'Pick', 1,
			    'Marshal', 1,
			    'Consol', 1,
			    'Shipment', 1, 0)) Total
FROM	 Inventory_Transaction
WHERE (('&1' IS NULL) OR (Site_ID = '&1'))
AND (('&2' IS NULL) OR (Owner_ID = '&2'))
AND Summary_Record = 'Y'
GROUP BY Site_ID, Owner_ID, TO_CHAR(DStamp, 'DD-MON-YYYY'), TO_CHAR(DStamp, 'HH24')
ORDER BY 1, 2, 3, 4
; 

/* The complex where statement is to get over the fact that Oracle stores */
/* dates and times as part of the date field. Depending on how it was stored */
/* it might have the seconds after midnight, or might be set as midnight */
/* The expression 1 will default to midnight  at the beginning of the day */
/* specified. The 2 expression adds 1 day minus 1 second, so effectively */
/* causing the between to be between day1 0 seconds after midnight and */
/* day2 1 second before midnight! */

SET TERMOUT ON

