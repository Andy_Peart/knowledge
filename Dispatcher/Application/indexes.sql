/******************************************************************************/
/*                                                                            */
/*  Copyright (c) 2014 JDA Software Group, Inc.                               */
/*  All rights reserved - Company Confidential                                */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :     indexes.sql                                           */
/*                                                                            */
/*     DESCRIPTION:     Lists the indexes in the database.                    */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/******************************************************************************/

SET FEEDBACK ON
SET VERIFY OFF 
 
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 80
 
COLUMN Tablespace_Name	HEADING 'Tablespace'	FORMAT A15
COLUMN Table_Name	HEADING 'Table'		FORMAT A30
COLUMN Index_Name	HEADING 'Index'		FORMAT A30

BREAK ON Tablespace_Name ON Table_Name SKIP 1

SELECT Tablespace_Name, Table_Name, Index_Name
FROM User_Indexes
ORDER BY Tablespace_Name, Table_name, Index_Name;
