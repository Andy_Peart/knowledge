/******************************************************************************/
/*                                                                            */
/*  Copyright (c) 2014 JDA Software Group, Inc.                               */
/*  All rights reserved - Company Confidential                                */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :     aresiteownerscorrect.sql                              */
/*                                                                            */
/*     DESCRIPTION:     Checks site and owner data configuration.             */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   28/07/99 JH   DCS    NEW4086  Multi site/owner configuration checking    */
/*   21/08/01 DGP  DCS    NEW5164  Multi-site suspense locations.             */
/******************************************************************************/

set serveroutput on

declare
	cursor sitefasearch
	is
	select enabled
	from function_access
	where function_id = 'MULTI_SITE';

	cursor ownerfasearch
	is
	select enabled
	from function_access
	where function_id = 'THIRD_PARTY';

	cursor sitecountsearch
	is 
	select count(*) cnt
	from site;

	cursor ownercountsearch
	is 
	select count(*) cnt
	from owner;

	cursor sitesuspensesearch
		(
		enabled	varchar2
		)
	is
	select count(*) cnt
	from location
	where ((enabled = 'Y' and site_id is not null)
	or (enabled = 'N' and site_id is not null))
	and loc_type = 'Suspense';

	cursor suspensefasearch
	is
	select enabled
	from function_access
	where function_id = 'SL_SITE';

	cursor sitelocationsearch
		(
		enabled	varchar2
		)
	is
	select count(*) cnt
	from location
	where ((enabled = 'Y' and site_id is null)
	or (enabled = 'N' and site_id is not null))
	and loc_type != 'Suspense';

	sitefarow	sitefasearch%rowtype;
	ownerfarow	ownerfasearch%rowtype;
	suspensefarow	suspensefasearch%rowtype;
	sitecountrow	sitecountsearch%rowtype;
	ownercountrow	ownercountsearch%rowtype;
	sitesuspenserow sitesuspensesearch%rowtype;
	sitelocationrow sitelocationsearch%rowtype;

	siteenabled	boolean;
	siteerrors	boolean := false;
	ownerenabled	boolean;
	suspenseenabled	boolean;
	ownererrors	boolean := false;
	suspenseerrors	boolean := false;
	enabled		varchar2(1);
begin
	dbms_output.put_line(':');
	dbms_output.put_line('Validating multi site data...');
	dbms_output.put_line('Checking function access record (MULTI_SITE)');

	open sitefasearch;
	fetch sitefasearch into sitefarow;

	if sitefasearch%found then
		dbms_output.put_line('Function access record found');
		if sitefarow.enabled = 'G' then
			siteenabled := true;
		elsif sitefarow.enabled is null then
			siteenabled := false;
		else
			dbms_output.put_line('Function access is not set correctly (set to ' || sitefarow.enabled || ') !');
			siteenabled := false;
			siteerrors := true;
		end if;
	else
		dbms_output.put_line('Function access record not found');
		siteenabled := false;
	end if;

	close sitefasearch;

	if siteenabled = true then
		dbms_output.put_line('Multi site is enabled');
	else
		dbms_output.put_line('Multi site is not enabled');
	end if;

	dbms_output.put_line('Checking site records');

	open sitecountsearch;
	fetch sitecountsearch into sitecountrow;

	if siteenabled = true then
		if sitecountrow.cnt = 0 then
			dbms_output.put_line('There are no site records (at least one is required) !');
			siteerrors := true;
		elsif sitecountrow.cnt = 1 then
			dbms_output.put_line('There is only one site record (consider removing M_SITE function access)');
		else
			dbms_output.put_line('There are some site records');
		end if;
	else
		if sitecountrow.cnt = 0 then
			dbms_output.put_line('There are no site records');
		else
			dbms_output.put_line('There are site records (no site records are allowed) !');
			siteerrors := true;
		end if;
	end if;

	close sitecountsearch;

	dbms_output.put_line(':');
	dbms_output.put_line('Validating multi-site suspense data...');
	dbms_output.put_line('Checking function access record (SL_SITE)');

	open suspensefasearch;
	fetch suspensefasearch into suspensefarow;

	if suspensefasearch%found then
		dbms_output.put_line('Function access record found');
		if suspensefarow.enabled = 'G' then
			suspenseenabled := true;
		elsif suspensefarow.enabled is null then
			suspenseenabled := false;
		else
			dbms_output.put_line('Function access is not set correctly (set to ' || suspensefarow.enabled || ') !');
			suspenseenabled := false;
			suspenseerrors := true;
		end if;
	else
		dbms_output.put_line('Function access record not found');
		suspenseenabled := false;
	end if;

	close suspensefasearch;

	if suspenseenabled = true then
		dbms_output.put_line('Multi-site Suspense is enabled');
	else
		dbms_output.put_line('Multi-site Suspense is not enabled');
	end if;

	dbms_output.put_line('Checking suspense records');

	if suspenseenabled = true then
		enabled := 'Y';
	else
		enabled := 'N';
	end if;

	open sitesuspensesearch(enabled);
	fetch sitesuspensesearch into sitesuspenserow;

	if sitesuspenserow.cnt > 0 and enabled = 'N' then
		dbms_output.put_line('Suspense record has a site (it should not have) !');
		siteerrors := true;
	else
		dbms_output.put_line('Suspense records are correct');
	end if;

	close sitesuspensesearch;

	dbms_output.put_line('Checking location records');

	if siteenabled = true then
		enabled := 'Y';
	else
		enabled := 'N';
	end if;

	open sitelocationsearch(enabled);
	fetch sitelocationsearch into sitelocationrow;

	if sitelocationrow.cnt > 0 then
		if siteenabled = true then
			dbms_output.put_line('Some locations do not have a site (must be set) !');
		else
			dbms_output.put_line('Some locations have a site (must not be set) !');
		end if;
		siteerrors := true;
	else
		if siteenabled = true then
	 		dbms_output.put_line('All locations have a site');
		else
			dbms_output.put_line('No locations have a site');
		end if;
	end if;

	close sitelocationsearch;

	if siteerrors = true then
		dbms_output.put_line('There were multi site errors found !!!');
	else
 		dbms_output.put_line('There were no multi site errors found');
	end if;

	dbms_output.put_line(':');
	dbms_output.put_line('Validating third party data...');
	dbms_output.put_line('Checking function access record (THIRD_PARTY)');

	open ownerfasearch;
	fetch ownerfasearch into ownerfarow;

	if ownerfasearch%found then
		dbms_output.put_line('Function access record found');
		if ownerfarow.enabled = 'G' then
			ownerenabled := true;
		elsif ownerfarow.enabled is null then
			ownerenabled := false;
		else
			dbms_output.put_line('Function access is not set correctly (set to ' || ownerfarow.enabled || ') !');
			ownerenabled := false;
			ownererrors := true;
		end if;
	else
		dbms_output.put_line('Function access record not found');
		ownerenabled := false;
	end if;

	close ownerfasearch;

	if ownerenabled = true then
		dbms_output.put_line('Third party is enabled');
	else
		dbms_output.put_line('Third party is not enabled');
	end if;

	dbms_output.put_line('Checking owner records');

	open ownercountsearch;
	fetch ownercountsearch into ownercountrow;

	if ownerenabled = true then
		if ownercountrow.cnt = 0 then
			dbms_output.put_line('There are no owner records (at least one is required) !');
			ownererrors := true;
		elsif ownercountrow.cnt = 1 then
			dbms_output.put_line('There is only one owner record (consider removing M_OWNER function access)');
		else
			dbms_output.put_line('There are some owner records');
		end if;
	else
		if ownercountrow.cnt = 0 then
			dbms_output.put_line('There are no owner records');
		else
			dbms_output.put_line('There are owner records (no owner records are allowed) !');
			ownererrors := true;
		end if;
	end if;

	close ownercountsearch;

	if ownererrors = true then
		dbms_output.put_line('There were third party errors found !!!');
	else
 		dbms_output.put_line('There were no third party errors found');
	end if;

	dbms_output.put_line(':');
end;
/
exit
