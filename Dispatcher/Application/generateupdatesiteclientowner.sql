
set output off
set feedback off
set heading off
set pages 0

select '' from dual;
select 'Creating updatesiteclientowner.sql...' from dual;
select '' from dual;

spool updatesiteclientowner.sql

select 'update '|| table_name || ' set ' || column_name || ' = ''&&site_id'';'
from user_tab_columns
where (column_name like '%SITE_ID'
and column_name not like '%TO_SITE_ID');

select 'update '|| table_name || ' set ' || column_name || ' = ''&&client_id'';'
from user_tab_columns
where (column_name like '%CLIENT_ID');

select 'update '|| table_name || ' set ' || column_name || ' = ''&&owner_id'';'
from user_tab_columns
where (column_name like '%OWNER_ID');

spool off

select '' from dual;
select 'Check updatesiteclientowner.sql before running in sql*plus' from dual;
select '' from dual;

