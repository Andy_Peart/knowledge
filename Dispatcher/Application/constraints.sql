/******************************************************************************/
/*                                                                            */
/*  Copyright (c) 2014 JDA Software Group, Inc.                               */
/*  All rights reserved - Company Confidential                                */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :     constraints.sql                                       */
/*                                                                            */
/*     DESCRIPTION:     Lists the constraints in the database.                */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/******************************************************************************/

SET FEEDBACK ON
SET VERIFY OFF 
 
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 80
 
COLUMN Table_Name HEADING 'Table' FORMAT A20
COLUMN Constraint_Name HEADING 'Constraint' FORMAT A20
COLUMN Constraint_Type HEADING 'Type' FORMAT A1
COLUMN Search_Condition HEADING 'Condition' FORMAT A30

BREAK ON Table_Name SKIP 2

SELECT 	Table_Name, Constraint_Name, Constraint_Type, Search_Condition
FROM User_Constraints
ORDER BY Table_Name;

