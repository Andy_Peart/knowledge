/******************************************************************************/
/*                                                                            */
/*  Copyright (c) 2014 JDA Software Group, Inc.                               */
/*  All rights reserved - Company Confidential                                */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*  FILE NAME  : create_beam_freespace.sql                                    */
/*                                                                            */
/*  DESCRIPTION: For every location that has a Beam ID, get the Site ID,      */
/* 		 Location ID and Beam ID.                                     */
/* 		 Next we call LibBeam.SettleBeamLocPositions() - this sets    */
/* 		 the 'From' and 'To' units on the location (and any inventory)*/
/* 		 and is called because we don't trust the values used in      */
/* 		 data take-on to be correct !                                 */
/*		 Then we call LibBeam.SettleFreeSpace() to refresh/update the */
/*		 Beam_Free_Space table using the data on the location         */
/*               (Beam Position, Beam Units etc). The location, invenotry &   */
/*		 beam data should then be straight.                           */
/*                                                                            */
/*  DATE     BY   PROJ   ID       DESCRIPTION                                 */
/*  ======== ==== ====== ======== =============                               */
/*  14/11/03 SL   DCS    PDR9142  Initial version                             */
/******************************************************************************/
SET SERVEROUTPUT ON
SET VERIFY OFF

SPOOL create_beam_freespace.log

PROMPT
PROMPT
ACCEPT show_verbose CHAR PROMPT "Verbose mode ? (Y/N, N is default): "
PROMPT
PROMPT

DECLARE

	/* 
	Find each beam
	*/
	CURSOR BeamSearch
	IS
	SELECT 	Site_ID, Beam_ID
	FROM	Beam_Details;

	/*
	Find each location on a beam 
	*/
	CURSOR	LocationSearch
	IS
	SELECT	Site_ID, Location_ID, Beam_ID
	FROM	Location
	WHERE	Site_ID IS NOT NULL
	AND	Beam_ID IS NOT NULL
	ORDER BY Site_ID, Location_ID;

BEGIN

	if ('&&show_verbose' = 'Y' or '&&show_verbose' = 'y') then
		dbms_output.put_line('Log file is create_beam_freespace.log');
	end if;

	dbms_output.put_line('create_beam_freespace.sql - starting...');

	/*
	Get each Beam and refresh the Start and End Units
	for each location on the beam
	*/

	if ('&&show_verbose' = 'Y' or '&&show_verbose' = 'y') then
		dbms_output.put_line('Checking location beam positions for:');
	end if;

	for BeamRow in BeamSearch 
	loop
		if ('&&show_verbose' = 'Y' or '&&show_verbose' = 'y') then
			dbms_output.put_line('Site : ['||BeamRow.Site_ID
						||'], Beam : ['
						||BeamRow.Beam_ID||']');
		end if;

		LibBeam.SettleBeamLocPositions
			(
			BeamRow.Site_ID,
			BeamRow.Beam_ID
			);

	end loop;

	/* 
	Get each Location on a beam, and refresh
	the beam_free_space (and the inventory start/end
	units)
	*/
	if ('&&show_verbose' = 'Y' or '&&show_verbose' = 'y') then
		dbms_output.put_line('Verifying inventory/beam data for:');
	end if;

	for LocationRow in LocationSearch 
	loop
		if ('&&show_verbose' = 'Y' or '&&show_verbose' = 'y') then
			dbms_output.put_line('Site : [' ||LocationRow.Site_ID
				||'], Beam : ['||LocationRow.Beam_ID||'], '
				||'Location: ['||LocationRow.Location_ID||']');
		end if;

		LibBeam.SettleFreeSpace
			(
			LocationRow.Site_ID,
			LocationRow.Beam_ID,
			LocationRow.Location_ID
			);

	end loop;

	dbms_output.put_line('create_beam_freespace.sql - finished.');

	commit;
end;
/
