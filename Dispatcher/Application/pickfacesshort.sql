/******************************************************************************/
/*                                                                            */
/*  Copyright (c) 2014 JDA Software Group, Inc.                               */
/*  All rights reserved - Company Confidential                                */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*  FILE NAME  : pickfacesshort.sql                                           */
/*                                                                            */
/*  DESCRIPTION: Shows pick faces without enough released replenishments.     */
/*                                                                            */
/*  DATE     BY   PROJ   ID       DESCRIPTION                                 */
/*  ======== ==== ====== ======== =============                               */
/******************************************************************************/

prompt
prompt Pick faces which do not have enough released replenishments
prompt 

select a.location_id, 
	a.trigger_qty, 
	a.qty_on_hand, 
	a.qty_allocated,
	a.qty_due,
	b.qty_to_move 
from pick_face a, move_task b
where b.final_loc_id=a.location_id
and a.key=b.face_key
and a.trigger_qty - a.qty_on_hand > (select sum(c.qty_to_move)
					from move_task c
					where c.face_key=b.face_key
					and c.status='Released'
					and task_id='REPLENISH')
and b.status='Hold'
order by 1;

