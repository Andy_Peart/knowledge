SPOOL DataAuditResults.csv
SET HEADING OFF
SET VERIFY OFF
SET LINESIZE 10000
SET FEEDBACK OFF
CLEAR BREAKS
CLEAR COLUMNS
prompt
prompt Data Audit Script,Questions,Database Value,Answer Y/N/NA in column E,Y/N/NA,Notes
prompt
prompt Q 1.1,Hidden function access for Multi Site MULTI_SITE,Database Value,Is this operationally correct?
select ',,'||decode(enabled,'G','ON','OFF') 
from function_access 
where function_id ='MULTI_SITE';
prompt
prompt Q 1.2,Hidden function access for Adding Sites SITE_CREATE,Database Value,Is this operationally correct?
select ',,'||decode(enabled,'G','ON','OFF') 
from function_access 
where function_id ='SITE_CREATE';
prompt
prompt Q 1.3 / 1.4,Summary of Sites,Database Value,Is this operationally correct?
SELECT ',,'||SITE_ID FROM SITE;
prompt
prompt Q 1.5,Intransit site on System Options,Database Value,Is this operationally correct?
select ',,'||TRANSIT_SITE_ID from system_options;
prompt
prompt Q 1.6,Hidden function access for Multi Site Suspense Locations HGFA_SL_SITE,Database Value,Is this operationally correct?
select ',,'||decode(enabled,'G','ON','OFF') 
from function_access 
where function_id ='SL_SITE';
prompt
prompt Q 1.7,Suspense location summary,Database Value,Is this operationally correct?
select ',,'||'Site - ' || 
site_id || '- Has Suspense location - '|| 
location_id 
from location 
where loc_type ='Suspense';
prompt
prompt Q 1.8,Tag Integrity in Suspense,Database Value,Is this operationally correct?
select ',,'||decode(enabled,'G','ON','OFF') 
from function_access 
where function_id ='SL_TAG';
prompt
prompt Q 1.9,Time Zones on the Site records,Database Value,Is this operationally correct?
SELECT ',,'||SITE_ID, 
time_zone_name 
FROM SITE;
prompt
prompt Q 1.10,Hidden function access for Adding Owners OWNER_CREATE,Database Value,Is this operationally correct?
select ',,'||decode(enabled,'G','ON','OFF') 
from function_access 
where function_id ='OWNER_CREATE';
prompt
prompt Q 1.12 / 1.13,Summary of Owners in the System,Database Value,Is this operationally correct?
select ',,'||owner_id 
from owner;
prompt
prompt Q 1.14,Hidden function access for Adding Clients CLIENT_CREATE,Database Value,Is this operationally correct?
select ',,'||decode(enabled,'G','ON','OFF') 
from function_access 
where function_id ='CLIENT_CREATE';
prompt
prompt Q 1.15- 1.17,Summary of Clients in the system,Database Value,Is this operationally correct?
select ',,'||'Client - ' || client_id || ' - Exists in Client Group -' ||
client_group
from client;
prompt
prompt Q 1.18,Client Group Use,Database Value,Is this operationally correct?
select ',,'||cg.client_group || ' - Controls func Acc -  ' ||
decode(cg.controls_func_acc,'Y', 'Yes','No')|| ' - Cont Data Vis -  ' ||
decode(cg.controls_data_vis,'Y', 'Yes','No')|| ' - Clients in Group -  ' ||
count(cgc.client_id) || ' - Users in Group -  ' ||
count(u.user_id)
from client_group cg, client_group_clients cgc, application_user u
where cgc.client_group (+) = cg.client_group
and u.client_vis_group (+) = cg.client_group
group by cg.client_group,
cg.controls_func_acc,
cg.controls_data_vis,
cgc.client_id;
prompt
prompt Q 1.19 - 1.21,System Options Settings,Database Value,Is this operationally correct?
select ',,'||'RU Warning = ' || rcv_warnunits ||
' - Tag Prefix = ' || tag_prefix ||
' - Default Lang = ' || default_language
from system_options;
prompt
prompt Q 1.22,System Options - Kitting and Container Locations on System Options,Database Value,Is this operationally correct?
select ',,'||'Container Location = ' || cont_stage_loc ||
'- Kitting Location = ' || kitting_loc_id from system_options;
prompt
prompt Q 1.22,Kitting and Container Locations in the Location table,Database Value,Is this operationally correct?
select ',,'||'Site = ' || site_id ||
' - Location = ' || location_id ||
' - Type = ' || loc_type 
from location 
where location_id in (select cont_stage_loc  
from system_options union select kitting_loc_id from system_options);
prompt
prompt Q 1.23,System Options - Default Container / Customer orders,Database Value,Is this operationally correct?
select ',,'||'Container Orders = ' || cont_orders ||
'- Customer Orders = ' || cont_customers from system_options;
prompt
prompt Q 1.24,System Options - Minimum / Maximum fill values,Database Value,Is this operationally correct?
select ',,'||'Minimum Fill = ' || NVL((minimum_fill),'0') ||
' - Maximum Fill = ' || NVL((maximum_fill),'0') 
from system_options;
prompt
prompt Q 1.25,System Options - Lock Codes,Database Value,Is this operationally correct?
select ',,'||'Pick = ' || pick_lock_code ||
' - Sample = ' ||sample_lock_code ||
' - Incubation = ' || incub_lock_code ||
' - Expiry = ' ||expiry_lock_code ||
' - QC = ' ||qc_lock_code 
from system_options;
prompt
prompt Q 1.26,System Options - Default tag carrier and customer label settings,Database Value,Is this operationally correct?
select ',,'||'Carrier = ' || default_carrier_label ||
' Customer = ' || default_customer_label ||
' Tag = ' || tag_label from system_options;
prompt
prompt Q 1.27,System Options - Time Zone,Database Value,Is this operationally correct?
select ',,'||time_zone_name from system_options;
prompt
prompt Q 1.28,Language Control Modifications,Database Value,Is this operationally correct?
select ',,'||'Language - ' || language ||
' - Label - ' || label || 
' - Text - ' || Text 
from language_text 
where user_modified ='Y'
and language ='EN_GB';
prompt
prompt Q1.29,User Defined Fields,Database Value,Is this operationally correct?
select ',,'||'Client Group- ' ||nvl(udf.client_group,'All Clients'),
'Table - ' ||udf.table_name,
'Column - ' ||udf.column_name,
'Label - ' ||udf.label,
'Text - ' ||lt.text,
'Tab Visible - '||decode(udt.visible,'Y','Yes','No'),
'Field Visible - ' ||decode(udf.visible,'Y','Yes','No'),
'Field Required - ' ||decode(udf.required,'Y','Yes','No')
from user_defined_field udf, user_defined_table udt, language_text lt
where udf.label =lt.label
and lt.language ='EN_GB'
and udt.table_name (+)= udf.table_name
and udt.client_group (+) = udf.client_group
and (udt.visible ='Y'
or udf.visible ='Y'
or udf.required ='Y')
order by udf.client_group,
udf.table_name;
prompt
prompt Q2,Global Function Access,Database Value,Is this operationally correct?
select (',,'||'Function Id- '||fa.function_id||' -Text- '||lt.text),
decode(fa.enabled,'Y','On','N','Off','G','On','Off')
from function_access fa, language_text lt
where lt.language ='EN_GB'
and fa.function_id = lt.label
and fa.site_id is null
and (fa.enabled = 'G' or fa.enabled is null)
order by 1;
prompt
select (',,'||'Function Id- '||fa.function_id||' -Text- '||lt.text),
decode(fa.enabled,'Y','On','N','Off','G','On','Off')
from function_access fa, language_text lt
where lt.language ='EN_GB'
and fa.function_id = lt.label
and fa.site_id is null
and fa.enabled in('Y','N')
order by 1;
prompt
prompt Q2,Site Specific Function access changes,Database Value,Is this operationally correct?
select (',,'||'Site - ' ||fa.site_id||' -Function Id- '||fa.function_id||' -Text- '||lt.text),
decode(fa.enabled,'Y','On','N','Off','G','On','Off')
from function_access fa, language_text lt
where lt.language ='EN_GB'
and fa.function_id = lt.label
and fa.site_id is not null
order by 1;
prompt
prompt Q2,Custom Function Access relies on function id starting v_,Database Value,Is this operationally correct?
select ',,'||NVL(FUNCTION_ACCESS.SITE_ID,'Blank Site ') || '/ ' || LANGUAGE_TEXT.TEXT || ' Status ' || nvl(FUNCTION_ACCESS.ENABLED,'Off')
from FUNCTION_ACCESS,
LANGUAGE_TEXT
where ((FUNCTION_ACCESS.FUNCTION_ID LIKE 'V_%')
and  (FUNCTION_ACCESS.FUNCTION_ID NOT LIKE 'VL%')
and  (FUNCTION_ACCESS.FUNCTION_ID NOT LIKE 'VC%')
and  (FUNCTION_ACCESS.FUNCTION_ID NOT LIKE 'VL%')
and  (FUNCTION_ACCESS.FUNCTION_ID NOT LIKE 'VM%')
and  (FUNCTION_ACCESS.FUNCTION_ID NOT LIKE 'VV%')
and (LANGUAGE_TEXT.LANGUAGE = 'EN_GB'))
and FUNCTION_ACCESS.FUNCTION_ID = LANGUAGE_TEXT.LABEL; 
prompt
prompt Q2,Summary of Hidden Function Access,Database Value,Is this operationally correct?
SELECT ',,'||FA.Function_ID, SUBSTR(LibLanguage.GetTranslation
('HGFA_' || FA.Function_ID, UPPER('EN_GB')), 1, 54), DECODE(Enabled, 'G', 'Enabled', 'Disabled')
FROM Function_Access FA
WHERE FA.Function_ID NOT IN (SELECT LT2.Label
FROM Language_Text LT2
WHERE LT2.Language = UPPER('EN_GB'))
AND LibLanguage.GetTranslation('HGFA_' || FA.Function_ID, UPPER('EN_GB')) != 'HGFA_' || FA.Function_ID
AND FA.Site_ID IS NULL
AND FA.Client_Group IS NULL
AND (FA.Enabled = 'G' OR FA.Enabled IS NULL)
ORDER BY 2;
prompt
prompt Q3.1,User Groups,Database Value,Is this operationally correct?
select ',,'||DCSDBA.USER_GROUP.GROUP_ID  || ' - Notes -   ' ||
DCSDBA.USER_GROUP.NOTES || ' - Users -   ' ||
COUNT (DCSDBA.APPLICATION_USER.USER_ID) C1
from DCSDBA.USER_GROUP,
DCSDBA.APPLICATION_USER
where DCSDBA.USER_GROUP.GROUP_ID (+)  = DCSDBA.APPLICATION_USER.GROUP_ID
group by DCSDBA.USER_GROUP.GROUP_ID, DCSDBA.USER_GROUP.NOTES ;
prompt
prompt Q3.2,Windows Workstation Groups,Database Value,Is this operationally correct?
select ',,'||DCSDBA.WORKSTATION_GROUP.GROUP_ID || ' - Notes- ' ||
DCSDBA.WORKSTATION_GROUP.NOTES || ' - Stations - ' ||
COUNT (DCSDBA.WORKSTATION.STATION_ID)
from DCSDBA.WORKSTATION_GROUP,DCSDBA.WORKSTATION
where DCSDBA.WORKSTATION.GROUP_ID  (+) = DCSDBA.WORKSTATION_GROUP.GROUP_ID 
and group_type = 'C'
group by DCSDBA.WORKSTATION_GROUP.GROUP_ID,
DCSDBA.WORKSTATION_GROUP.NOTES ;
prompt
prompt Q3.3,Web Workstation Groups,Database Value,Is this operationally correct?
select ',,'||DCSDBA.WORKSTATION_GROUP.GROUP_ID || ' - Notes- ' ||
DCSDBA.WORKSTATION_GROUP.NOTES || ' - Stations - ' ||
COUNT (DCSDBA.WORKSTATION.STATION_ID)
from DCSDBA.WORKSTATION_GROUP,DCSDBA.WORKSTATION
where DCSDBA.WORKSTATION.GROUP_ID  (+) = DCSDBA.WORKSTATION_GROUP.GROUP_ID 
and group_type = 'W'
group by DCSDBA.WORKSTATION_GROUP.GROUP_ID,
DCSDBA.WORKSTATION_GROUP.NOTES ;
prompt
prompt Q3.4,RDT Workstation Groups,Database Value,Is this operationally correct?
select ',,'||DCSDBA.WORKSTATION_GROUP.GROUP_ID || ' - Notes- ' ||
DCSDBA.WORKSTATION_GROUP.NOTES || ' - Stations - ' ||
COUNT (DCSDBA.WORKSTATION.STATION_ID)
from DCSDBA.WORKSTATION_GROUP,DCSDBA.WORKSTATION
where DCSDBA.WORKSTATION.GROUP_ID  (+) = DCSDBA.WORKSTATION_GROUP.GROUP_ID 
and group_type = 'R'
group by DCSDBA.WORKSTATION_GROUP.GROUP_ID,
DCSDBA.WORKSTATION_GROUP.NOTES ;
prompt
prompt Q3.5,Voice Workstation Groups,Database Value,Is this operationally correct?
select ',,'||DCSDBA.WORKSTATION_GROUP.GROUP_ID || ' - Notes- ' ||
DCSDBA.WORKSTATION_GROUP.NOTES || ' - Stations - ' ||
COUNT (DCSDBA.WORKSTATION.STATION_ID)
from DCSDBA.WORKSTATION_GROUP,DCSDBA.WORKSTATION
where DCSDBA.WORKSTATION.GROUP_ID  (+) = DCSDBA.WORKSTATION_GROUP.GROUP_ID 
and group_type = 'V'
group by DCSDBA.WORKSTATION_GROUP.GROUP_ID,
DCSDBA.WORKSTATION_GROUP.NOTES ;
prompt
prompt Q3.3,Does an enabled RedPrairie Support sign on exist (USER ID/ GROUP),Database Value,Is this operationally correct?
select ',,'||'User - ' || USER_ID || ' - Group - ' || 
GROUP_ID 
from APPLICATION_USER 
where ((USER_ID = 'SUPPORT') 
and (DISABLED = 'N')) ;
prompt
prompt Q3.5,Have Users been correctly attached to sites,Database Value,Is this operationally correct?
select ',,'||'Site - ' || site_id || ' - Count Users - ' ||
count (user_id) 
from application_user
group by site_id;
prompt
prompt Q3.6,Have Users been correctly attached to Owners,Database Value,Is this operationally correct?
select ',,'||'Owner - ' || owner_id || ' - Count Users - ' ||
count (user_id) 
from application_user
group by owner_id;
prompt
prompt Q3.7,Have Users been correctly attached to Client Visibility Groups,Database Value,Is this operationally correct?
select ',,'||'Client Group - ' || client_vis_group || ' - Count Users - ' ||
count (user_id) 
from application_user
group by client_vis_group;
prompt
prompt Q3.8 - Q3.10,Summary,Database Value,Is this operationally correct?
select ',,'||' Site_id - ' || site_id || ' - Owner - ' ||
owner_id || ' - Client Group - ' || 
client_vis_group || ' - Users - ' ||
count (user_id) 
from application_user
group by site_id, owner_id, client_vis_group;
prompt
prompt Q3.11,Have Users been correctly attached to Time Zones,Database Value,Is this operationally correct?
select ',,'||'Time Zone - ' || time_zone_name || ' - Count Users - ' ||
count (user_id) 
from application_user
group by time_zone_name;
prompt
prompt Q3.12,Have Users been correctly attached to Localities,Database Value,Is this operationally correct?
select ',,'||'Locality - ' || locality || ' - Count Users - ' ||
count (user_id) 
from application_user
group by locality;
prompt
prompt Q3.13,Summary of equipment classs,Database Value,Is this operationally correct?
select  ',,'||equipment_class||' - '||notes from equipment_class;
prompt
prompt Q3.14,Function access enabled but not assigned to a group,Database Value,Is this operationally correct?
select distinct ',,'||fa.function_id || '  ' || lt.text
from function_access fa, language_text lt
where fa.function_id = lt.label
and lt.language ='EN_GB'
and function_id in (select function_id from function_access where enabled ='Y'
minus select function_id from group_function) and lt.label =fa.function_id;
prompt
prompt Q3.15,Security Function access,Database Value,Is this operationally correct?
select ',,'||lt.text || '  ' ||  
decode(fa.enabled,'G','On','Off') Status
from function_access fa, language_text lt
where function_id IN ('RESTRICT_LOGIN1','RESTRICT_LOGIN2')
and fa.function_id = lt.label
and lt.language = 'EN_GB'
order by 1;
prompt
prompt Q3.16,System Options Security Function access,Database Value,Is this operationally correct?
select ',,'||'No of Days - ' || pwd_days || ' - No of Changes - ' ||
pwd_changes || ' - Min Length - ' ||
pwd_length || ' - Special Words - ' ||
pwd_special || ' - Previous - ' ||
pwd_previous || ' - Aplhanumeric- ' ||
pwd_mixed
from system_options;
prompt
prompt Q3.18 - 3.19,Have RDT Workstations and Emulators been set up correctly,Database Value,Is this operationally correct?
select ',,'||site_id || ' - Station - ' || STATION_ID || '- Truck - ' || TRUCK_ID || '- Port - ' || PTYSVR_PORT_NO || '- C X R - ' || SCREEN_COLUMNS || 'X' || SCREEN_ROWS  
from workstation
where rdt ='Y';
prompt
prompt Q3.20,Does a singles siteless web workstation exist,Database Value,Is this operationally correct?
select ',,'||'Site - '|| nvl(site_id,'Siteless') || '- station -' || station_id || '- group -' || group_id 
from workstation 
where station_id = 'WEB';
prompt
prompt Q3.21,Have the Voice Units been set up correctly,Database Value,Is this operationally correct?
select ',,'||site_id || ' - Station - ' || STATION_ID || ' - Unit Number - ' || unit_number
from workstation
where voice_unit = 'Y';
prompt
prompt Q3.22,RDT Workstations Restrict Aisle Setting,Database Value,Is this operationally correct?
select ',,'||nvl(count (station_id),0) || ' - RDT workstations have the functionality set to - ' ||
restrict_aisle 
from workstation
where rdt ='Y'
group by restrict_aisle;
prompt
prompt Q3.20 Part 1,Number of Workstations Set up,Database Value,Is this operationally correct?
select ',,'||'Site - ' || NVL(SITE_ID,'Siteless') || ' Stations - ' || COUNT (STATION_ID) || 'Type - ' ||
decode(nvl(rdt,'N') || nvl(voice_unit,'N'),'NN','PC/WEB','NY','Voice','YN','RDT')
from WORKSTATION
group by SITE_ID, decode(nvl(rdt,'N') || nvl(voice_unit,'N'),'NN','PC/WEB','NY','Voice','YN','RDT');
prompt
prompt Q3.23 Part 2,Licences,Database Value,Is this operationally correct?
select  ',,'||RDT_USERS || '-RDT-' || 
CLIENT_USERS || '-Client-' ||
ENQUIRER_USERS || '-Enq-' ||
WEB_USERS || '-Web-' ||
WEB_USERS || '-Voice-' ||
TRANSACTIONS || '-Trans' A
from LICENCE ;
prompt
prompt Q3.28,Have Storage Classes been set up,Database Value,Is this operationally correct?
select ',,'||DCSDBA.STORAGE_CLASS.STORAGE_CLASS, 
DCSDBA.STORAGE_CLASS.NOTES || ' Workstations with class ' ||
COUNT (DCSDBA.WORKSTATION_STORAGE_CLASS.STATION_ID) 
from DCSDBA.STORAGE_CLASS, 
DCSDBA.WORKSTATION_STORAGE_CLASS  
where DCSDBA.STORAGE_CLASS.STORAGE_CLASS (+)  = DCSDBA.WORKSTATION_STORAGE_CLASS.STORAGE_CLASS 
group by DCSDBA.STORAGE_CLASS.STORAGE_CLASS, 
DCSDBA.STORAGE_CLASS.NOTES ;
prompt
prompt Q3.28,Summary of Storage Class use on Location,Database Value,Is this operationally correct?
select ',,'||site_id || ' Class - ' || nvl(storage_class, 'No Class') || ' - ' ||
count(location_id)
from location
group by site_id, storage_class;
prompt
prompt Q3.29,Have Handling Classes been set up,Database Value,Is this operationally correct?
select ',,'||'Handling Class - ' ||DCSDBA.HANDLING_CLASS.HANDLING_CLASS || ' - ' ||  
DCSDBA.HANDLING_CLASS.NOTES ||  ' - Linked to - ' ||  
COUNT (DCSDBA.WORKSTATION_HANDLING_CLASS.STATION_ID) || ' Workstations'
from DCSDBA.HANDLING_CLASS, 
DCSDBA.WORKSTATION_HANDLING_CLASS  
where DCSDBA.HANDLING_CLASS.HANDLING_CLASS (+)  = DCSDBA.WORKSTATION_HANDLING_CLASS.HANDLING_CLASS 
group by DCSDBA.HANDLING_CLASS.HANDLING_CLASS, 
DCSDBA.HANDLING_CLASS.NOTES ;
prompt
prompt Q3.29,Handling class use on SKUs,Database Value,Is this operationally correct?
select ',,'||'Handling Class - ' || handling_class || ' - Is present on - ' ||
count(sku_id) || ' skus' 
from sku
group by handling_class;
prompt
prompt Q4.1,Location Zones,Database Value,Is this operationally correct?
select ',,'||'Site - ' || z.site_id ||' - Zone - ' ||  z.zone_1 ||' - SZone1 - ' || nvl(z.subzone_1,'BLANK')||' - SZone2 - ' || nvl(z.subzone_2,'BLANK')||' - Locs - ' || count(l.location_id)
from location l, location_zone z
where z.site_id || z.zone_1 || z.subzone_1 || z.subzone_2 = l.site_id || l.zone_1 || l.subzone_1 || l.subzone_2 
group by z.site_id, z.zone_1, z.subzone_1, z.subzone_2
order by z.site_id, z.zone_1;
prompt
prompt Q4.2,Have any work zones been created with underscores,Database Value,Is this operationally correct?
select ',,'||location_id, work_zone
from location
where work_zone like '%\_%'
escape '\';
prompt
prompt Q4.3,Work Zones,Database Value,Is this operationally correct?
select ',,'||z.site_id, z.work_zone, count(l.location_id)
from location l, work_zone z
where z.work_zone (+) = l.work_zone
and z.site_id (+) = l.site_id
group by z.site_id, z.work_zone;
prompt
prompt Q4.4,Total number of locations,Database Value,Is this operationally correct?
select ',,'||nvl(site_id,'Siteless'), 
count (location_id) 
from location group by site_id order by 1;
prompt
prompt Q4.5,Location Checkstring Summary,Database Value,Is this operationally correct?
select distinct ',,'||(site_id) || ' Site has - ' ||
nvl(q1.b,0) || ' locs without cstrings and - ' ||
nvl(q2.b,0) || ' locs with cstrings' 
from location,
(select site_id a , 
count(location_id) b 
from location 
where check_string is null 
group by site_id) q1,
(select site_id a , 
count(location_id) b 
from location 
where check_string is not null 
group by site_id) q2
where location.site_id = q1.a (+)
and location.site_id = q2.a (+);
prompt
prompt Q4.6,Summary of Location lock status,Database Value,Is this operationally correct?
select ',,'||'Site - ' || SITE_ID || ' Status - ' ||  LOCK_STATUS || ' Location Count - ' ||
COUNT (LOCATION_ID)
from LOCATION
where  LOCATION_ID NOT IN ('SUSPENSE','KITTING')
group by SITE_ID, LOCK_STATUS ;
prompt
prompt Q4.7,Incorrectly spelled Lock status,Database Value,Is this operationally correct?
select ',,'||DCSDBA.LOCATION.LOCATION_ID 
from DCSDBA.LOCATION 
where ((DCSDBA.LOCATION.LOCK_STATUS 
NOT IN ('Locked', 'UnLocked','OutLocked','InLocked'))) ;
prompt
prompt Q4.8,Summary of Location Types,Database Value,Is this operationally correct?
select ',,'||'Site - ' || SITE_ID || ' - Loc Type - ' ||  LOC_TYPE || ' - Count Locs - ' ||
COUNT (LOCATION_ID)
from LOCATION
group by SITE_ID, LOC_TYPE
ORDER BY SITE_ID;
prompt
prompt Q4.9,Locations with Instages,Database Value,Is this operationally correct?
select ',,'||site_id, count (location_id) 
from location 
where in_stage is not null
group by site_id;
prompt
prompt Q4.9,Locations with Outstages,Database Value,Is this operationally correct?
select  ',,'||site_id, count (location_id) 
from location 
where out_stage is not null
group by site_id;
prompt
prompt Q4.9,Unlinked Stages,Database Value,Is this operationally correct?
select ',,'||'Site - ' || site_id || '- Location -' ||  location_id from location where loc_type ='Stage'
minus
(select ',,'||'Site - ' || site_id || '- Location -' ||in_stage from location
union
select ',,'||'Site - ' || site_id || '- Location -' ||  out_stage from location);
prompt
prompt Q4.10,Locations with an Instage which does not exist as a location,Database Value,Is this operationally correct?
select ',,'||'Site - ' || site_id || ' - Stage - ' ||in_stage from location where in_stage is not null
minus
select ',,'||'Site - ' || site_id || ' - Stage - ' ||location_id from location;
prompt
prompt Q4.11,Locations with a location zone / subzone which does not exist in the location zone table,Database Value,Is this operationally correct?
select ',,'||site_id ||' - Location - ' || location_id || '- Zone - ' || zone_1 ||'- SZone1 - ' || subzone_1 ||'- SZone2 - ' || subzone_2
from location
where site_id || zone_1 || subzone_1 || subzone_2 NOT IN (select site_id || zone_1 || subzone_1 || subzone_2 from location_zone)
and location_id <> 'SUSPENSE';
prompt
prompt Q4.12,Locations with a work zone which does not exist in the work zone table,Database Value,Is this operationally correct?
select ',,'||'Site - ' || site_id || ' - ' || location_id from location
where  site_id || work_zone NOT IN (select site_id || work_zone from work_zone)
and location_id <> 'SUSPENSE';
prompt
prompt Q4.13,Locations with a volume of O,Database Value,Is this operationally correct?
select ',,'||DCSDBA.LOCATION.LOCATION_ID, 
DCSDBA.LOCATION.VOLUME 
from DCSDBA.LOCATION 
where ((DCSDBA.LOCATION.VOLUME = 0.000000)) ;
prompt
prompt Q4.14,Locations with a weight of O,Database Value,Is this operationally correct?
select ',,'||count (LOCATION.LOCATION_ID) || ' locations have a weight of zero'
from DCSDBA.LOCATION 
where ((DCSDBA.LOCATION.WEIGHT = 0.000000) 
and (DCSDBA.LOCATION.LOCATION_ID not in ('KITTING','SUSPENSE')) );
prompt
prompt Q4.15 - 4.16,Height Width depth and volume summary,Database Value,Is this operationally correct?
select ',,'||nvl(site_id,'Siteless') || ' - ' || ' Height - ' || height|| ' - Width - ' ||
width || ' - Depth - ' ||
depth || ' - Volume - ' || volume ||
nvl(height,0)*nvl(width,0)*nvl(depth,0)|| ' - Calculated Vol - ' ||
' - Locs - ' ||
count (location_id)
from location
group by site_id, height,
width,
depth,
volume
order by 1;
prompt
prompt Q4.16,Pack Configs with Pallet heights greater than the maximum location height,Database Value,Is this operationally correct?
select ',,'||Q1.a from
(select config_id a, 
((( ratio_1_to_2 * nvl(ratio_2_to_3,1) * nvl(ratio_3_to_4,1) * nvl(ratio_4_to_5,1) * nvl(ratio_5_to_6,1) * nvl(ratio_6_to_7,1) * nvl(ratio_7_to_8,1) )/nvl(each_per_layer,1))*nvl(Layer_height,1)) b
from sku_config) Q1 where Q1.b > (select max(height) from location);
prompt
prompt Q4.17,Location and pallet dimesnsion ranges,Database Value,Is this operationally correct?
select ',,'||Q1.a || ' and Pallet ' || Q2.a from
(select ' Location Heights range from ' || min(height) || ' to ' || 
max(height) || '  widths range from ' ||  
min(width)|| ' to ' || 
max(width)|| '  depths range from ' ||  
min(depth)|| ' to ' ||
max(depth) a
from location) Q1,
(select 'Max Height ' || max(height) || ' Max depth ' ||
max(depth)|| ' Max width ' ||
max (width)|| ' Max mixed height ' || 
max(mixed_height) a
from pallet_config) Q2;
prompt
prompt Q4.18,Location and sku weight use,Database Value,Is this operationally correct?
select ',,'||Q1.a || q2.a from
(select count(sku_id) || ' skus have each weights' a 
from sku 
where each_weight is not null) Q1,
(select ' and ' || count(location_id) || ' locations have a weight greater than 0' a
from location 
where nvl(weight,0) > 0) Q2;
prompt
prompt Q4.19,Summary of Missing Pick / Put / and Stock Check sequences,Database Value,Is this operationally correct?
select ',,'||'Site - ' || Q1.a || ' - Missing Pick -' || nvl(Q2.b,0)|| ' - Missing Put -' || nvl(Q3.b,0)|| ' - Missing Check -' || nvl(Q4.b,0) from 
(select site_id a from site) Q1,
(select site_id a, count(*) b
from location
where pick_sequence <= 0.0
and location_id <> 'SUSPENSE' 
group by site_id)Q2,
(select site_id a, count(*) b
from location
where put_sequence <= 0.0
and location_id <> 'SUSPENSE' 
group by site_id)Q3,
(select site_id a, count(*) b
from location
where check_sequence <= 0.0
and location_id <> 'SUSPENSE'
group by site_id)Q4
where Q1.a = Q2.a (+)
and Q1.a = Q3.a (+)
and Q1.a = Q4.a (+)
group by Q1.a, nvl(Q2.b,0), nvl(Q3.b,0), nvl(Q4.b,0);
prompt
prompt Q4.21,Locations with Disallow Allocation set,Database Value,Is this operationally correct?
select ',,'||'Site - ' || SITE_ID || ' - Location - '|| LOCATION_ID
from LOCATION
where DISALLOW_ALLOC = 'Y'
order by 1;
prompt
prompt Q4.22,Locations with Allow Pallet Stock Check set,Database Value,Is this operationally correct?
select ',,'||count (location_id)  || ' locations have allow pallet count set to Y'
from location 
where allow_pall_cnt ='Y';
prompt
prompt Q4.XX,Extra question - Inventory incorrectly located in pick faces,Database Value,Is this operationally correct?
select ',,'||'Site - ' || site_id ||' - Location - ' || location_id ||' - SKU - ' || sku_id || ' - Owner - '|| owner_id || ' - Client - '|| client_id || ' - Cond - '|| condition_id || ' - Origin - '|| origin_id
from inventory
where site_id || location_id
in (select site_id || location_id
from pick_face)
minus
select ',,'||'Site - ' || site_id ||' - Location - ' || location_id ||' - SKU - ' || sku_id || ' - Owner - '|| owner_id || ' - Client - '|| client_id || ' - Cond - '|| condition_id || ' - Origin - '|| origin_id
from pick_face;
prompt
prompt Q4.23,Pick faces which are not set as pick faces in the location table,Database Value,Is this operationally correct?
select ',,'||'Site - ' || site_id || ' Location - ' || location_id 
from pick_face
minus
select ',,'||'Site - ' || site_id || ' Location - ' || location_id 
from location 
where pick_face is not null;
prompt
prompt Q4.23,Locations set as pick faces which are not in the pick face table,Database Value,Is this operationally correct?
select ',,'||'Site - ' || site_id || ' Location - ' || location_id 
from location 
where pick_face is not null
minus
select ',,'||'Site - ' || site_id || ' Location - ' || location_id 
from pick_face;
prompt
prompt Q4.24,Locations flagged as containing empty Pallets,Database Value,Is this operationally correct?
select ',,'||'Site - ' || nvl(site_id,'Siteless') || ' - Empty Pallets - ' || NVL(empty_pallets,'NULL') || ' - Locs - ' || count(location_id) 
from location
where location_id <> 'SUSPENSE'
group by site_id,empty_pallets; 
prompt
prompt Q4.25,Locations flagged as preventing putaway if an empty pallet,Database Value,Is this operationally correct?
select ',,'||'Site - ' || nvl(site_id,'Siteless') || ' - No Putaway - ' || NVL(no_put_empty_pallet,'NULL') || ' - Locs - ' || count(location_id) 
from location
where location_id <> 'SUSPENSE'
group by site_id,no_put_empty_pallet;
prompt
prompt Q4.28,Locations with duplicate labour coordinates null = 0,Database Value,Is this operationally correct?
select ',,'|| b || ' - Locs - ' || a from(
select 'Site - ' || nvl(site_id,'Siteless') || 'x'|| nvl(x_position,0) || 'y' || nvl(y_position,0) || 'z'|| nvl(z_position,0) b,  count(location_id) a 
from location
group by 'Site - ' ||nvl(site_id,'Siteless') || 'x'|| nvl(x_position,0) || 'y' || nvl(y_position,0) || 'z'|| nvl(z_position,0))
where a > 1; 
prompt
prompt Q4.29,Locations without Labour Coordinates,Database Value,Is this operationally correct?
select ',,'||(nvl(site_id,'Siteless')), count(location_id) 
from location 
where nvl(x_position,0)||nvl(y_position,0)||nvl(z_position,0) = '000'
group by site_id;
prompt
prompt Q4.30,Summary of Locations with Sortation Putaway set,Database Value,Is this operationally correct?
select ',,'||(nvl(site_id,'Siteless')) || ' - Sortation Putaway - ' || nvl(sortation_putaway,'N') || ' - Locs - ' || count(location_id) 
from location 
group by site_id, sortation_putaway;
prompt
prompt Q4.30,Summary of Locations set up as block stacks,Database Value,Is this operationally correct?
select ',,'||'Site - ' || site_id || ' - zone -' || zone_1 , count(location_id) from location 
where stack = 'Y'
group by ',,'||'Site - ' || site_id || ' - zone -' || zone_1;
prompt
prompt Q4.32 - 50,Random Locations to be checked,Database Value,Is this operationally correct?
select ',,'||a from
(select site_id, location_id ,'Site - ' || site_id|| ' - Zone - ' || zone_1|| ' - SZn1 - ' || subzone_1|| ' - SZn2 - ' || subzone_2|| ' - Loc - ' || location_id || ' - CS - ' || check_string|| ' - Vol - ' || volume|| ' - h - ' || height|| ' - w - ' || width|| ' - d - ' || depth a from location
order by dbms_random.value)
where rownum <= 50
order by site_id, location_id;
prompt
prompt Q4.34,Locations with duplicate fan putaway coordinates in the same aisle,Database Value,Is this operationally correct?
select ',,'||b from(
select 'Site- ' ||site_id||'- Aisle - ' || subzone_1|| ' - x - ' || fan_x|| ' - y - ' ||fan_y|| ' - z - ' ||fan_z b, count (location_id) a
from location
where (fan_x is not null or fan_y is not null or fan_z is not null)
group by 'Site- ' ||site_id||'- Aisle - ' || subzone_1|| ' - x - ' || fan_x|| ' - y - ' ||fan_y|| ' - z - ' ||fan_z)
where a>1;
prompt
prompt Q4.35,Locations with Fan putaway coordinates but no sub zone 1,Database Value,Is this operationally correct?
select ',,'||'Site- ' ||site_id|| 'Location - ' || location_id from location 
where (fan_x is not null or fan_y is not null or fan_z is not null)
and subzone_1 is null;
prompt
prompt Q4.37,Beams with Zone information not in the location table,Database Value,Is this operationally correct?
select ',,'||'Site - ' || Site_id || ' - Zn- ' || zone_1 || ' - SZn1 - '|| subzone_1|| ' - SZn2 - '|| subzone_2 from beam_details
minus
select distinct (',,'||'Site - ' || Site_id || ' - Zn- ' || zone_1 || ' - SZn1 - '|| subzone_1|| ' - SZn2 - '|| subzone_2)  from location;
prompt
prompt Q4.38,Beams with Zone information not in the location zone table,Database Value,Is this operationally correct?
select  ',,'||'Site - ' || Site_id || ' - Zn- ' || zone_1 || ' - SZn1 - '|| subzone_1|| ' - SZn2 - '|| subzone_2 from beam_details
minus
select distinct ( ',,'||'Site - ' || Site_id || ' - Zn- ' || zone_1 || ' - SZn1 - '|| subzone_1|| ' - SZn2 - '|| subzone_2)  from location_zone;
prompt
prompt Q4.39,Beam ids with no locations,Database Value,Is this operationally correct?
select ',,'||'Site - ' || Site_id || ' - Beam- ' || beam_id from beam_details
minus
select ',,'||'Site - ' || Site_id || ' - Zn- ' || beam_id from location;
prompt
prompt Q4.42,Dock Door locations not in the location Table (Checks Loc Type),Database Value,Is this operationally correct?
select ',,'||'Site - ' || Site_id || location_id 
from dock_door
minus
select ',,'||'Site - ' || Site_id || location_id 
from location
where loc_type in ('Receive Dock','ShipDock','Auto-ShipDock');
prompt
prompt Q4.43,Dock DoorLocations which are not receipt or ship docks,Database Value,Is this operationally correct?
select ',,'||d.location_id  from dock_door d, location l
where d.location_id = l.location_id
and d.site_id = l.site_id
and l.loc_type not in ('Receive Dock','ShipDock','Auto-ShipDock');
prompt
prompt Q4.44,Pick Face Usage,Database Value,Is this operationally correct?
select ',,'||Q3.A || ' -Total SKUs-' ||
Q1.C || ' -Have Fix PFs- ' ||
Q2.C || ' -Have Dyn PFs- ' ||
ROUND(((decode(Q1.C,0,0.0001,Q1.C))/Q3.A)*100,1) || '% Fixed- ' ||
ROUND(((decode(Q2.C,0,0.0001,Q2.C))/Q3.A)*100,1) || '% Dyn- ' ||
ROUND(((Q3.A - (Q1.C+Q2.C))/Q3.A)*100,1) || '% Not with PFs'
from
(select count (sku_id) A from sku) Q3,
(select count (sku_id) C from pick_face where face_type ='F') Q1,
(select count (sku_id) C from pick_face where face_type ='D') Q2;
prompt
prompt Q4.46,Replen without order ticked,Database Value,Is this operationally correct?
select  ',,'||Q1.a || ' Fixed ' || ROUND(nvl(Q2.a,0)/q1.a*100,1) || '% have it ticked ' ||
 Q3.a || ' Dynamic ' || ROUND(greatest(Q4.a,1)/greatest(q3.a,1)*100,1) || '% have it ticked'
from
(select count (sku_id) a
from pick_face where face_type = 'F') Q1,
(select count  (sku_id) a
from pick_face
where auto_replen ='Y' and face_type = 'F') Q2,
(select count (sku_id) a
from pick_face where face_type = 'D') Q3,
(select count  (sku_id) a
from pick_face
where auto_replen ='Y' and face_type = 'D') Q4;
prompt
prompt Q4.47,Pick faces where the trigger is 0,Database Value,Is this operationally correct?
select ',,'||decode(count (sku_id),0,'None are zero',(count (sku_id))) 
from pick_face 
where trigger_qty = 0;
prompt
prompt Q4.48,Pick faces where the trigger is greater than the minimum,Database Value,Is this operationally correct?
select ',,'||' Site ' || site_id || decode((count (sku_id)),0,'No Trigger is > Minimum
', ' has ' || (count (sku_id))|| ' where trigger is > minimum')
from pick_face
where trigger_qty > min_replen_qty
and replen_whole <> 'Y'
group by site_id;
prompt
prompt Q4.50,Number of Instances where Max Taks Qty is greater than minimum,Database Value,Is this operationally correct?
select ',,'||decode((count (sku_id)),0,'No Max Taks Qty > Minimum',(count (sku_id))) 
from pick_face
where max_task_qty > min_replen_qty;
prompt
prompt Q4.51,Max Reld Replen setting,Database Value,Is this operationally correct?
select ',,'||' Site ' || site_id || ' Max Reld Replen Setting - ' ||  decode(nvl(max_reld_replen,0),0,'BLANK',max_reld_replen) || ' Count of Pick faces - ' ||  count(sku_id) 
from pick_face 
group by site_id,  max_reld_replen;
prompt
prompt Q4.52,SKUs set up in the pick face table which dont exist in the SKU table,Database Value,Is this operationally correct?
select ',,'||sku_id from pick_face
minus
select sku_id from sku;
prompt
prompt Q4.53,Do all Pick faces have a site and owner - If no Result answer is Yes or No Pick faces,Database Value,Is this operationally correct?
select ',,'||DCSDBA.PICK_FACE.LOCATION_ID, 
decode(DCSDBA.PICK_FACE.SITE_ID,'1','Site OK','Site Required'), 
decode(DCSDBA.PICK_FACE.OWNER_ID,'20','Owner OK','Owner Required') 
from DCSDBA.PICK_FACE 
where ((DCSDBA.PICK_FACE.SITE_ID IS NULL ) 
or (DCSDBA.PICK_FACE.OWNER_ID IS NULL )) ; 
prompt
prompt Q4.55,Pick faces with multiple SKUs (A Location can only Have 1 SKU),Database Value,Is this operationally correct?
select ',,'||location_id from 
(select count (sku_id) cnt, location_id, site_id from pick_face group by location_id, site_id) 
where cnt >1;
prompt
prompt Q4.56,Pick face  Round to free usage,Database Value,Is this operationally correct?
select  ',,'||Q1.a || ' Fixed Pick faces ' || ROUND(nvl(Q2.a,0)/q1.a*100,1) || '% have it ticked ' || 
Q3.a || ' Dynamic Pick faces ' || ROUND(greatest(Q4.a,1)/greatest(Q3.a,1)*100,1) || '% have it ticked'
from
(select count (sku_id) a
from pick_face
where face_type = 'F') Q1,
(select count  (sku_id) a
from pick_face
where replen_whole ='Y'
and face_type = 'F') Q2,
(select count (sku_id) a
from pick_face
where face_type = 'D') Q3,
(select count  (sku_id) a
from pick_face
where replen_whole ='Y'
and face_type = 'D') Q4;
prompt
prompt Q4.58 - 4.59,Pick faces with either Overfill quantities or Maximum Quantities but not both ?,Database Value,Is this operationally correct?
select ',,'||'Site - ' ||site_id || ' - Loc - ' || location_id from pick_face 
where OVERFILL_RELATIVE_QTY || MAXIMUM_QTY is not null
and (OVERFILL_RELATIVE_QTY is null or MAXIMUM_QTY is null);
prompt
prompt Q4.60,Locations with Overfill Quantities set to less than the Maximum Quantity,Database Value,Is this operationally correct?
select ',,'||'Site - ' ||site_id || ' - Loc - ' || location_id from pick_face 
where OVERFILL_RELATIVE_QTY <= MAXIMUM_QTY; 
prompt
prompt Q4.62,Pick face Profiles,Database Value,Is this operationally correct?
select ',,'||'Site - ' || site_id ||' - Profiles -  ' || count(fan_profile_id) 
from pick_face_profile_header
group by site_id;
prompt
prompt Q4.63,Pick face profile headers with no detail records,Database Value,Is this operationally correct?
select ',,'||'Site - ' || site_id ||' - Profile -  ' || fan_profile_id from pick_face_profile_header
minus 
select ',,'||'Site - ' || site_id ||' - Profile -  ' || fan_profile_id from pick_face_profile_detail;
prompt
prompt Q4.64,Summary of Pick face profiles,Database Value,Is this operationally correct?
select ',,'||'Site - ' || site_id ||' - Profile -  ' || fan_profile_id || ' - notes ' || notes || ' - zone -  ' use_pf_zone 
from pick_face_profile_header;
prompt
prompt Q4.67,Pallet Types,Database Value,Is this operationally correct?
select ',,'||config_id || ' - ' || notes || ' - Picking - ' || picking ||' - Receiving - ' || receiving ||' - Repacking - ' || repacking || ' Is a container - ' || is_a_container from pallet_config;
prompt
prompt Q4.68,Pallet height width depth and weight,Database Value,Is this operationally correct?
select ',,'||config_id || ' - Volume - ' || volume || ' - Height - ' || height || ' - depth - ' || depth ||' - width - ' || width || ' - weight - ' || weight || ' - mixed height - ' || mixed_height || ' - mixed weight - ' || mixed_weight 
from pallet_config;
prompt
prompt Q4.69,Global Allocation Algorithms,Database Value,Is this operationally correct?
select ',,'||GLOBAL_ALLOCATION.SITE_ID || ' - ' ||
LANGUAGE_TEXT.TEXT || ' - Prty - ' ||
GLOBAL_ALLOCATION.PRIORITY || ' - Zone - ' ||
GLOBAL_ALLOCATION.ZONE_1 || ' - Fin Zone - ' ||
GLOBAL_ALLOCATION.FINAL_LOC_ZONE || ' - Add Zone - ' ||
GLOBAL_ALLOCATION.ADDITIONAL_ZONE || ' - Alloc - ' ||
GLOBAL_ALLOCATION.ALLOC_ALGORITHM || ' - Replen - ' ||
GLOBAL_ALLOCATION.REPLEN_ALGORITHM
from GLOBAL_ALLOCATION,
LANGUAGE_TEXT
where GLOBAL_ALLOCATION.ALGORITHM = LANGUAGE_TEXT.LABEL
and LANGUAGE_TEXT.LANGUAGE = 'EN_GB'
order by GLOBAL_ALLOCATION.SITE_ID, GLOBAL_ALLOCATION.PRIORITY;
prompt
prompt Q4.70,Group Allocation Algorithms set up but not assigned to SKUs,Database Value,Is this operationally correct?
select ',,'||allocation_group from allocation_group
minus
select ',,'||allocation_group from sku;
prompt
prompt Q4.70,Group Allocation Algorithms Groups with no algorithms,Database Value,Is this operationally correct?
select ',,'||allocation_group from allocation_group
minus
select ',,'||allocation_group from group_allocation;
prompt
prompt Q4.71,Have SKU Allocation algorithms been used,Database Value,Is this operationally correct?
select ',,'||count(sku_id)|| '- SKUS in total- ' || 
Q1.A1|| '- Have Algorithms which is  ' || 
((Q1.A1/count(sku_id))*100) || ' Percent '
from sku,
(select count(distinct(sku_id)) A1 from sku_allocation) Q1
GROUP BY Q1.A1;
prompt
prompt Q4.72,Global Putaway Algorithms,Database Value,Is this operationally correct?
select ',,'||GLOBAL_PUTAWAY.SITE_ID || ' - ' || 
LANGUAGE_TEXT.TEXT || ' - Prty - ' ||
GLOBAL_PUTAWAY.PRIORITY
from GLOBAL_PUTAWAY, 
LANGUAGE_TEXT  
where GLOBAL_PUTAWAY.ALGORITHM = LANGUAGE_TEXT.LABEL 
and LANGUAGE_TEXT.LANGUAGE = 'EN_GB'
order by GLOBAL_PUTAWAY.SITE_ID, GLOBAL_PUTAWAY.PRIORITY  ;
prompt
prompt Q4.73,Group Putaway Algorithms set up but not assigned to SKUs,Database Value,Is this operationally correct?
select ',,'||putaway_group from putaway_group
minus
select ',,'||putaway_group from sku;
prompt
prompt Q4.74,Group Putaway Algorithms Groups with no algorithms,Database Value,Is this operationally correct?
select ',,'||putaway_group from putaway_group
minus
select ',,'||putaway_group from group_putaway;
prompt
prompt Q4.74,Putaway algorithms assigned to SKUs,Database Value,Is this operationally correct?
select distinct ',,'||(gp.putaway_group), nvl(Q1.A,0)
from group_putaway gp,(select putaway_group, 
count(sku_id) A from sku
group by putaway_group) Q1
where gp.putaway_group = Q1.putaway_group (+);
prompt
prompt Q4.74,Have SKU Putaway algorithms been used,Database Value,Is this operationally correct?
select ',,'||count(sku_id)|| '- SKUS in total- ' || 
Q1.A1|| '- Have Algorithms which is  ' || 
((Q1.A1/count(sku_id))*100) || ' Percent '
from sku,
(select count(distinct(sku_id)) A1 from sku_putaway) Q1
GROUP BY Q1.A1;
prompt
prompt Q5.1,Count of SKUs,Database Value,Is this operationally correct?
select ',,'||'Client - ' || client_id || ' - Skus - ' ||
count(sku_id) 
from sku
group by client_id;
prompt
prompt Q5.3,Has Each Height been used,Database Value,Is this operationally correct?
select ',,'||DECODE((count (sku_id)),0,'No',(count (sku_id))) || ' -SKUs have each height' 
from sku 
where each_height > 0;
prompt
prompt Q5.4,Has Each width been used,Database Value,Is this operationally correct?
select ',,'||DECODE((count (sku_id)),0,'No',(count (sku_id))) || ' -SKUs have each width' 
from sku 
where each_width > 0;
prompt
prompt Q5.5,Has Each DEPTH been used,Database Value,Is this operationally correct?
select ',,'||DECODE((count (sku_id)),0,'No',(count (sku_id))) || ' -SKUs have each depth' 
from sku 
where each_depth > 0;
prompt
prompt Q5.6,Has Each Weight been used,Database Value,Is this operationally correct?
select ',,'||DECODE((count (sku_id)),0,'No',(count (sku_id))) || '-SKUs have each weight' 
from sku 
where each_weight > 0;
prompt
prompt Q5.7,Has Each Volume been used,Database Value,Is this operationally correct?
select ',,'||DECODE((count (sku_id)),0,'No',(count (sku_id))) || '-SKUs have each volume' 
from sku 
where each_volume > 0;
prompt
prompt Q5.8,Have beam units been populated,Database Value,Is this operationally correct?
select ',,'||DECODE((count (sku_id)),0,'No',(count (sku_id))) || '-SKUs have beam units' 
from sku 
where beam_units > 0;
prompt
prompt Q5.10,Pick Count qty and count frequency,Database Value,Is this operationally correct?
select ',,'||'Pick Count Qty - ' || pick_count_qty || ' - Count Frequency - ' || count_frequency || '- Set for ' ||   count(sku_id) ||  ' skus'
from sku
group by pick_count_qty,count_frequency;
prompt
prompt Q5.12,SKUs with Split lowest Ticked without a split lowest pack configuration,Database Value,Is this operationally correct?
select ',,'||sku_id from sku where split_lowest = 'Y'
minus
select ',,'||sku_id from sku_sku_config where config_id IN 
(select config_id from sku_config where split_lowest ='Y');
prompt
prompt Q5.13,SKU tick box summary,Database Value,Is this operationally correct?
select ',,'||'Split Lowest - ' || nvl(Q1.a,0) 
|| ' - Cond Req - ' || nvl(Q2.a,0)
|| ' - Exp Req - ' || nvl(Q3.a,0)
|| ' - Ori Req - ' || nvl(Q4.a,0)
|| ' - Serial at Pack - ' || nvl(Q5.a,0)
|| ' - Seial at Pick - ' || nvl(Q6.a,0)
|| ' - Serial at Rec- ' || nvl(Q7.a,0)
|| ' - Kit SKU - ' || nvl(Q8.a,0)
|| ' - Kit Split - ' || nvl(Q9.a,0)
|| ' - ABC Dis - ' || nvl(Q10.a,0)
|| ' - Obs Product - ' || nvl(Q11.a,0)
|| ' - New Product - ' || nvl(Q12.a,0)
|| ' - Dis Upload - ' || nvl(Q13.a,0)
|| ' - Dis Cross Dock - ' || nvl(Q14.a,0)
|| ' - Manuf Dstamp Reqd - ' || nvl(Q15.a,0)
|| ' - Manuf Dstamp Dflt - ' || nvl(Q16.a,0)
|| ' - Hazmat - ' || nvl(Q17.a,0)
|| ' - Dis Merge Rules - ' || nvl(Q18.a,0)
|| ' - Pack DRepack - ' || nvl(Q19.a,0)
from
(select count(sku_id) a
from sku where 
split_lowest ='Y') Q1,
(select count(sku_id) a
from sku where 
condition_reqd ='Y') Q2,
(select count(sku_id) a
from sku where 
expiry_reqd ='Y') Q3,
(select count(sku_id) a
from sku where 
origin_reqd ='Y') Q4,
(select count(sku_id) a
from sku where 
serial_at_pack ='Y') Q5,
(select count(sku_id) a
from sku where 
serial_at_pick ='Y') Q6,
(select count(sku_id) a
from sku where 
serial_at_receipt ='Y') Q7,
(select count(sku_id) a
from sku where 
kit_sku ='Y') Q8,
(select count(sku_id) a
from sku where 
kit_split ='Y') Q9,
(select count(sku_id) a
from sku where 
abc_disable ='Y') Q10,
(select count(sku_id) a
from sku where 
obsolete_product ='Y') Q11,
(select count(sku_id) a
from sku where 
new_product ='Y') Q12,
(select count(sku_id) a
from sku where 
disallow_upload ='Y') Q13,
(select count(sku_id) a
from sku where 
disallow_cross_dock ='Y') Q14,
(select count(sku_id) a
from sku where 
manuf_dstamp_reqd ='Y') Q15,
(select count(sku_id) a
from sku where 
manuf_dstamp_dflt ='Y') Q16,
(select count(sku_id) a
from sku where 
hazmat ='Y') Q17,
(select count(sku_id) a
from sku where 
disallow_merge_rules ='Y') Q18,
(select count(sku_id) a
from sku where 
pack_despatch_repack ='Y') Q19;
prompt
prompt Q5.14,Function access setting for New Product functionality,Database Value,Is this operationally correct?
select ',,'||Q1.a || ' - skus have the new product flag ticked and the function access is ' || Q2.a || ' for Site ' || q2.b from
(select count (sku_id) a
from sku
where new_product = 'Y') Q1,
(select site_id b, decode(enabled,'G','ON','OFF') a
from function_access
where function_id ='NEW_PROD_DIS') Q2;
prompt
prompt Q5.15,SKUs with allocation groups not in the allocation group table,Database Value,Is this operationally correct? 
select distinct ',,'||(allocation_group) from sku
minus
select ',,'||allocation_group from allocation_group;
prompt
prompt Q5.16,Are Allocation Groups Set Up,Database Value,Is this operationally correct?
select ',,'||allocation_group, notes from allocation_group;
prompt
prompt Q5.16,How many SKUs have each allocation group,Database Value,Is this operationally correct?
select ',,'||NVL(allocation_group,'No Group') A, 
count (sku_id) 
from sku
group by allocation_group;
prompt
prompt Q5.17,Putaway groups not in the putaway group table,Database Value,Is this operationally correct?
select ',,'||putaway_group from sku
minus
select ',,'||putaway_group from putaway_group;
prompt
prompt Q5.18,Are Putaway Groups Set Up,Database Value,Is this operationally correct?
select ',,'||putaway_group, notes from putaway_group;
prompt
prompt Q5.18,How many SKUs have each putaway group,Database Value,Is this operationally correct?
select ',,'||NVL(putaway_group,'No Group') A, 
count (sku_id) 
from sku 
group by putaway_group;
prompt
prompt Q5.19,Handling classes not in the handling class table,Database Value,Is this operationally correct?
select ',,'||handling_class from sku
minus
select ',,'||handling_class from handling_class;
prompt
prompt Q5.20,Are Handling Classes Set Up,Database Value,Is this operationally correct?
select ',,'||handling_class, notes from handling_class;
prompt
prompt Q5.20,How many SKUs have each Handling Class,Database Value,Is this operationally correct?
select ',,'||NVL(handling_class,'No Class') A, 
count (sku_id) 
from sku 
group by handling_class;
prompt
prompt Q5.20,How many Work Stations have each Handling Class,Database Value,Is this operationally correct?
select ',,'||handling_class A, 
count (station_id) 
from workstation_handling_class 
group by handling_class;
prompt
prompt Q5.21,Summary of Hazmat status use,Database Value,Is this operationally correct?
select ',,'||NVL(hazmat,'N') A,
count (sku_id)
from sku
group by NVL(hazmat,'N');
prompt
prompt Q5.21,Summary of Hazmat id use,Database Value,Is this operationally correct?
select ',,'||NVL(hazmat_id,'No id') A, 
count (sku_id) 
from sku
group by hazmat_id;
prompt
prompt Q5.22,Is Hidden Expiry Date functionality Switched on,Database Value,Is this operationally correct?
select ',,'||decode(enabled,'G','Its ON','Its Off. It wont be visible in SKU Maintenance') 
from function_access 
where function_id ='EXPIRY_CONTROL';
prompt
prompt Q5.22,How has expiry Required been used on the SKUs,Database Value,Is this operationally correct?
select ',,'||count (sku_id), decode(expiry_reqd,'Y','Set to Yes','N','Set to No','Set to No')
from sku
group by decode(expiry_reqd,'Y','Set to Yes','N','Set to No','Set to No');
prompt
prompt Q5.23,Summary of incubation rule use,Database Value,Is this operationally correct?
select ',,'||NVL(INCUB_RULE,'No incubation rules') A, 
count (sku_id) 
from sku
group by INCUB_RULE;
prompt
prompt Q5.23,Summary of incubation hours,Database Value,Is this operationally correct?
select ',,'||NVL(INCUB_HOURS,'0') A, 
count (sku_id) 
from sku
group by INCUB_HOURS;
prompt
prompt Q5.24,How has Preffered Putaway Location been used on the SKUs,Database Value,Is this operationally correct?
select ',,'||NVL(Q1.a,0) || ' skus have a pref put location out of - ' || nvl(Q2.a,0) || ' this is - ' || round((NVL(Q1.a,0)/nvl(Q2.a,0))*100,2) || '%'
from
(select count (sku_id) a from putaway_location ) Q1,
(select count(sku_id) a from sku) Q2;
prompt
prompt Q5.24,SKUs sharing a preferred putaway location,Database Value,Is this operationally correct?
select ',,'||Q1.A from 
(select sku_id A, 
count (sku_id) B
from putaway_location 
group by sku_id) Q1
where Q1.B > 1;
prompt
prompt Q5.25,EAN Use,Database Value,Is this operationally correct?
select ',,'||NVL(Q1.a,0) || ' skus have a an ean out of - ' || nvl(Q2.a,0) || ' this is - ' || round((NVL(Q1.a,0)/nvl(Q2.a,0))*100,2) || '%'
from
(select count (sku_id) a from sku where ean is not null) Q1,
(select count(sku_id) a from sku) Q2;
prompt
prompt Q5.26,UPC Code use,Database Value,Is this operationally correct?
select ',,'||NVL(Q1.a,0) || ' skus have a upc code out of - ' || nvl(Q2.a,0) || ' this is - ' || round((NVL(Q1.a,0)/nvl(Q2.a,0))*100,2) || '%'
from
(select count (sku_id) a from sku where upc is not null) Q1,
(select count(sku_id) a from sku) Q2;
prompt
prompt Q5.27 5.30,Supplier SKU use,Database Value,Is this operationally correct?
select ',,'||NVL(Q1.a,0) || ' skus have a supplier sku  out of- ' || nvl(Q2.a,0) || ' this is - ' || round((NVL(Q1.a,0)/nvl(Q2.a,0))*100,2) || '%'
from
(select count (distinct(sku_id)) a from supplier_sku) Q1,
(select count (sku_id) a from sku) Q2;
prompt
prompt Q5.27,SKUs in the supplier sku table which dont exist in the sku table,Database Value,Is this operationally correct?
select distinct ',,'||(sku_id) from supplier_sku
minus
select ',,'||sku_id from sku;
prompt
prompt Q5.28,Suppliers in the supplier sku table which are not in the address table,Database Value,Is this operationally correct?
select distinct ',,'||(supplier_id) from supplier_sku
minus
select ',,'||address_id from address;
prompt
prompt Q5.51,SKUs in the sku_sku_config table which are not in the sku table,Database Value,Is this operationally correct?
select distinct ',,'||(sku_id) from sku_sku_config
minus
select ',,'||sku_id from sku;
prompt
prompt Q5.52,Configs in the sku_sku_config table which are not in the config table,Database Value,Is this operationally correct?
select distinct ',,'||(config_id) from sku_sku_config
minus
select ',,'||config_id from sku_config;
prompt
prompt Q5.54,How has Tag Volume been used on the pack Configurations (Tag Volume/Count of Configs),Database Value,Is this operationally correct?
select ',,'||'Tag Volume - ' || nvl(tag_volume,0) || ' Count of configs - ' ||count (config_id) from sku_config
group by nvl(tag_volume,0);
prompt
prompt Q5.55,How has layer height and eachs per layer been used,Database Value,Is this operationally correct?
Select ',,'||'There are ' || Q3.c || ' configs. ' || Q1.a || ' have layer heights and ' || Q2.b || ' have eachs per layer' from
(select count(config_id) a 
from sku_config 
where layer_height is not null) Q1,
(select count(config_id) b 
from sku_config 
where EACH_PER_LAYER is not null) Q2,
(select count(config_id) c
from sku_config) Q3;
prompt
prompt Q5.55,How has layer height and eachs per layer been used,Database Value,Is this operationally correct?
select ',,'||count (config_id) || ' Configs are set with ' || ' ' ||
NVL(to_char(layer_height),'No') || '-Layer Height' || ' ' ||
NVL(to_char(each_per_layer),'No ')|| '-Each Per Layer' C
from sku_config
group by layer_height, each_per_layer;
prompt
prompt Q5.56,How has Volume at each been used,Database Value,Is this operationally correct?
select ',,'||count (config_id) || ' Configs set to volume at each - ' || decode(volume_at_each,'Y','Yes','N','No','No') 
from sku_config 
group by decode(volume_at_each,'Y','Yes','N','No','No');
prompt
prompt Q5.57,How have Pack Configuration Tracking levels been used,Database Value,Is this operationally correct?
select ',,'||NVL(SKU_CONFIG.RATIO_1_TO_2,0) || ' ' || 
NVL(SKU_CONFIG.TRACK_LEVEL_1,'NULL') || ' TRACK_LEVEL_1 ' || 
NVL(SKU_CONFIG.TRACK_LEVEL_2,'NULL') || ' TRACK_LEVEL_2 ' || 
NVL(SKU_CONFIG.RATIO_2_TO_3,0) || ' ' || 
NVL(SKU_CONFIG.TRACK_LEVEL_3,'NULL') || ' TRACK_LEVEL_3 ' || 
NVL(SKU_CONFIG.RATIO_3_TO_4,0) || ' ' || 
NVL(SKU_CONFIG.TRACK_LEVEL_4,'NULL') || ' TRACK_LEVEL_4 ' || 
COUNT (SKU_CONFIG.CONFIG_ID) || ' CONFIGS ' || 
COUNT (SKU_SKU_CONFIG.SKU_ID)|| ' SKUs ' A
from SKU_CONFIG, 
SKU_SKU_CONFIG  
where SKU_SKU_CONFIG.CONFIG_ID (+) = SKU_CONFIG.CONFIG_ID 
group by SKU_CONFIG.RATIO_1_TO_2, 
SKU_CONFIG.TRACK_LEVEL_1, 
SKU_CONFIG.TRACK_LEVEL_2, 
SKU_CONFIG.RATIO_2_TO_3, 
SKU_CONFIG.TRACK_LEVEL_3, 
SKU_CONFIG.RATIO_3_TO_4, 
SKU_CONFIG.TRACK_LEVEL_4 ;
prompt
prompt Q5.58,Number of SKUs with more than 1 pack configuration,Database Value,Is this operationally correct?
select ',,'||count(Q1.A1) || ' - Skus have more than 1 package configuration'
from (select sku_id A1, count (config_id) A2 from sku_sku_config group by sku_id)Q1
where Q1.A2 >1;
prompt
prompt Q5.59,Count of Pack configs that are not in use,Database Value,Is this operationally correct?
SELECT  ',,'||COUNT(CONFIG_ID) FROM (select config_id from sku_config
minus
select config_id from sku_sku_config);
prompt
prompt Q5.59,Pack configurations that are not in use,Database Value,Is this operationally correct?
select ',,'||config_id from sku_config
minus
select ',,'||config_id from sku_sku_config;
prompt
prompt Q5.60,Location zones in the Inventory table but not in the location zone table,Database Value,Is this operationally correct?
select ',,'||zone_1 from inventory 
minus 
select ',,'||zone_1 from location_zone;
prompt
prompt Q5.61,Expired Status (Only Includes Inventory with an Expiry Date),Database Value,Is this operationally correct?
select ',,'||count (sku_id), decode(expired,'Y','Expired','Not Expired') 
from inventory 
where expiry_dstamp IS NOT NULL group by expired;
prompt
prompt Q5.62,Condition Code Use,Database Value,Is this operationally correct?
select ',,'||c.condition_id || ' - Notes: ' || 
c.notes|| ' - Count of tags: ' ||
count (i.sku_id) 
from condition_code c, inventory i
where i.condition_id (+) = c.condition_id
group by c.condition_id, c.notes;
prompt
prompt Q5.63,Lock Code Use,Database Value,Is this operationally correct?
select ',,'||c.lock_code || ' - Notes: ' || 
c.notes|| ' - Count of tags: ' ||
count (i.sku_id) 
from inventory_lock_code c, inventory i
where i.lock_code (+) = c.lock_code
group by c.lock_code, c.notes;
prompt
prompt Q5.64,Lock Status,Database Value,Is this operationally correct?
select ',,'||count (sku_id), nvl(lock_status,'No Status')
from inventory 
group by lock_status;
prompt
prompt Q5.65,QC Status,Database Value,Is this operationally correct?
select ',,'||count (sku_id), nvl(qc_status,'No Status')
from inventory 
group by qc_status;
prompt
prompt Q5.66,Pallet Types,Database Value,Is this operationally correct?
select ',,'||count (sku_id), nvl(pallet_config,'No Type')
from inventory 
group by pallet_config;
prompt
prompt Q5.67,Origin,Database Value,Is this operationally correct?
select ',,'||count (sku_id), nvl(origin_id,'No Origin')
from inventory 
group by origin_id;
prompt
prompt Q5.68,Owner,Database Value,Is this operationally correct?
select ',,'||count (sku_id), nvl(owner_id,'No Owner')
from inventory
group by owner_id;
prompt
prompt Q5.69,Client,Database Value,Is this operationally correct?
select ',,'||count (sku_id), nvl(client_id,'No Client')
from inventory
group by client_id;
prompt
prompt Q7.2,Global Purge Settings,Database Value,Is this operationally correct?
select ',,'||purge_id || ' - Days - ' || purge_days, upload from purge_setup
where purge_daysable ='Y' 
and visible ='Y'
and site_id is null;
prompt
prompt Q7.2,Site Specific Purge Settings,Database Value,Is this operationally correct?
select ',,'||'Site - ' || site_id || ' - ' ||
purge_id || '  - Days - ' ||  
purge_days || ' - Upload - '  
upload from purge_setup
where purge_daysable ='Y' 
and visible ='Y'
and site_id is not null
order by site_id;
prompt
prompt Q7.4,RDT Query Setting,Database Value,Is this operationally correct?
select ',,'||rdt_query_max from system_options;
prompt
prompt Q7.7,Has the function access for Putaway History been enabled,Database Value,Is this operationally correct?
select ',,'||decode (enabled, 'G','Yes','No')
from function_access
where function_id = 'SPW_PUTAWAY_HIS';
prompt
prompt Q7.8,Has the function access for Auditing been enabled,Database Value,Is this operationally correct?
select ',,'||decode (enabled, 'G','Yes','No')
from function_access
where function_id = 'AUDITING';
prompt
prompt Q7.9,The Audit Table settings,Database Value,Is this operationally correct?
select ',,'||table_name || ' ' || 
decode (enabled,'Y','Enabled','Disabled') 
from audit_table;
prompt
prompt Q7.10,Printer Devices,Database Value,Is this operationally correct?
select ',,'||printer_name || ' - Cmd - ' || 
print_command || ' - Site - ' || 
site_id || ' - Cont - ' || 
container_label || ' - Marsh - ' || 
marshal_list || ' - PLab - ' || 
pallet_label || ' - PLst - ' || 
pallet_list || ' - Rpt - ' || 
report || ' - Tag - ' || 
tag_label || ' - TM - ' || 
trailer_manifest || ' - Loc - ' || 
locality 
from printer_devices;
prompt
prompt Q7.14,System Options - Print Commands - Label Command,Database Value,Is this operationally correct?
select ',,'||nvl(label_command,'NO Command') from system_options;
prompt
prompt Q7.14,System Options - Print Commands - Marshall Command,Database Value,Is this operationally correct?
select ',,'||nvl(marshal_command,'NO Command') from system_options;
prompt
prompt Q7.14,System Options - Print Commands - Pallet Command,Database Value,Is this operationally correct?
select ',,'||nvl(pallet_command,'NO Command') from system_options;
prompt
prompt Q7.14,System Options - Print Commands - Pallet List Command,Database Value,Is this operationally correct?
select ',,'||nvl(pallist_command,'NO Command') from system_options;
prompt
prompt Q7.14,System Options - Print Commands - Report Command,Database Value,Is this operationally correct?
select ',,'||nvl(rpt_command,'NO Command') from system_options;
prompt
prompt Q7.14,System Options - Print Commands - Tag Command,Database Value,Is this operationally correct?
select ',,'||nvl(tag_command,'NO Command') from system_options;
prompt
prompt Q7.14,System Options - Print Commands - Trailer Command,Database Value,Is this operationally correct?
select ',,'||nvl(trail_command,'NO Command') from system_options;
prompt
prompt Q7.15,Site Print Commands - Label Command,Database Value,Is this operationally correct?
select ',,'||site_id, nvl(label_command,'NO Command') from site;
prompt
prompt Q7.15,Site Print Commands - Marshall Command,Database Value,Is this operationally correct?
select ',,'||site_id,nvl(marshal_command,'NO Command') from site;
prompt
prompt Q7.15,Site Print Commands - Pallet Command,Database Value,Is this operationally correct?
select ',,'||site_id,nvl(pallet_command,'NO Command') from site;
prompt
prompt Q7.15,Site Print Commands - Pallet List Command,Database Value,Is this operationally correct?
select ',,'||site_id,nvl(pallist_command,'NO Command') from site;
prompt
prompt Q7.15,Site Print Commands - Report Command,Database Value,Is this operationally correct?
select ',,'||site_id,nvl(rpt_command,'NO Command') from site;
prompt
prompt Q7.15,Site Print Commands - Tag Command,Database Value,Is this operationally correct?
select ',,'||site_id,nvl(tag_command,'NO Command') from site;
prompt
prompt Q7.15,Site Print Commands - Trailer Command,Database Value,Is this operationally correct?
select ',,'||site_id,nvl(trail_command,'NO Command') from site;
prompt
prompt Q7.17,Workstation Print Commands - Carrier Label,Database Value,Is this operationally correct?
select ',,'||count (station_id) ||
decode(nvl(rdt,'N')||NVL(voice_unit,'N'),'YN',' - RDTs have the command - ','NY',' - Voice Terms have the command - ','NN',' - PCs have the command - ') ||
nvl(carrier_label_command,'No command populated')
from workstation
group by nvl(rdt,'N')||NVL(voice_unit,'N'),
carrier_label_command;
prompt
prompt Q7.17,Workstation Print Commands - Customer Label,Database Value,Is this operationally correct?
select ',,'||count (station_id) ||
decode(nvl(rdt,'N')||NVL(voice_unit,'N'),'YN',' - RDTs have the command - ','NY',' - Voice Terms have the command - ','NN',' - PCs have the command - ') ||
nvl(customer_label_command,'No command populated')
from workstation
group by nvl(rdt,'N')||NVL(voice_unit,'N'),
customer_label_command;
prompt
prompt Q7.17,Workstation Print Commands - Label,Database Value,Is this operationally correct?
select ',,'||count (station_id) ||
decode(nvl(rdt,'N')||NVL(voice_unit,'N'),'YN',' - RDTs have the command - ','NY',' - Voice Terms have the command - ','NN',' - PCs have the command - ') ||
nvl(label_command,'No command populated')
from workstation
group by nvl(rdt,'N')||NVL(voice_unit,'N'),
label_command;
prompt
prompt Q7.17,Workstation Print Commands - Marshal,Database Value,Is this operationally correct?
select ',,'||count (station_id) ||
decode(nvl(rdt,'N')||NVL(voice_unit,'N'),'YN',' - RDTs have the command - ','NY',' - Voice Terms have the command - ','NN',' - PCs have the command - ') ||
nvl(marshal_command,'No command populated')
from workstation
group by nvl(rdt,'N')||NVL(voice_unit,'N'),
marshal_command;
prompt
prompt Q7.17,Workstation Print Commands - Pallet,Database Value,Is this operationally correct?
select ',,'||count (station_id) ||
decode(nvl(rdt,'N')||NVL(voice_unit,'N'),'YN',' - RDTs have the command - ','NY',' - Voice Terms have the command - ','NN',' - PCs have the command - ') ||
nvl(pallet_command,'No command populated')
from workstation
group by nvl(rdt,'N')||NVL(voice_unit,'N'),
pallet_command;
prompt
prompt Q7.17,Workstation Print Commands - Pallet List,Database Value,Is this operationally correct?
select ',,'||count (station_id) ||
decode(nvl(rdt,'N')||NVL(voice_unit,'N'),'YN',' - RDTs have the command - ','NY',' - Voice Terms have the command - ','NN',' - PCs have the command - ') ||
nvl(pallist_command,'No command populated')
from workstation
group by nvl(rdt,'N')||NVL(voice_unit,'N'),
pallist_command;
prompt
prompt Q7.17,Workstation Print Commands - Report,Database Value,Is this operationally correct?
select ',,'||count (station_id) ||
decode(nvl(rdt,'N')||NVL(voice_unit,'N'),'YN',' - RDTs have the command - ','NY',' - Voice Terms have the command - ','NN',' - PCs have the command - ') ||
nvl(rpt_command,'No command populated')
from workstation
group by nvl(rdt,'N')||NVL(voice_unit,'N'),
rpt_command;
prompt
prompt Q7.17,Workstation Print Commands - Tag,Database Value,Is this operationally correct?
select ',,'||count (station_id) ||
decode(nvl(rdt,'N')||NVL(voice_unit,'N'),'YN',' - RDTs have the command - ','NY',' - Voice Terms have the command - ','NN',' - PCs have the command - ') ||
nvl(tag_command,'No command populated')
from workstation
group by nvl(rdt,'N')||NVL(voice_unit,'N'),
tag_command;
prompt
prompt Q7.17,Workstation Print Commands - Trail,Database Value,Is this operationally correct?
select ',,'||count (station_id) ||
decode(nvl(rdt,'N')||NVL(voice_unit,'N'),'YN',' - RDTs have the command - ','NY',' - Voice Terms have the command - ','NN',' - PCs have the command - ') ||
nvl(trail_command,'No command populated')
from workstation
group by nvl(rdt,'N')||NVL(voice_unit,'N'),
trail_command;
prompt
prompt Q7.19,Hidden function access for Complier Output,Database Value,Is this operationally correct?
select ',,'||decode(enabled,'G','Its ON','Its Off') 
from function_access 
where function_id ='COMPLIER_OUTPUT';
prompt
prompt Q7.20,System Options - Data Stream Command,Database Value,Is this operationally correct?
select ',,'||nvl(datastream_command,'No Command') from system_options;
prompt
prompt Q7.21,Output Datastream settings,Database Value,Is this operationally correct?
select ',,'||'Site - ' || Site_ID || ' - Station - ' ||
Station_id || ' - DS - ' ||
output_datastream_id || ' - Dest - ' ||
destination || ' - Prog - ' || 
program_name || ' - Copies - ' ||
copies || ' - SQL+ - ' ||
sqlplus || ' - Active - ' ||
active || ' - Ssystem - ' ||
system || ' - Locality - ' ||
locality
from output_datastream;
SPOOL OFF;
