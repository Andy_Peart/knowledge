/******************************************************************************/
/*                                                                            */
/*  Copyright (c) 2014 JDA Software Group, Inc.                               */
/*  All rights reserved - Company Confidential                                */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :     menudata.sql                                          */
/*                                                                            */
/*     DESCRIPTION:     Draw the menu structure.                              */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/*   06/09/01 DGP  DCS    DWEB4311 Initial Version                            */
/******************************************************************************/

SET FEEDBACK ON
SET VERIFY OFF 
SET HEADING OFF
 
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
SET PAGESIZE 0
SET NEWPAGE 0
SET LINESIZE 80
SET FEEDBACK OFF
 
BREAK ON Level SKIP 1
COLUMN Menu_Level HEADING '' FORMAT A50

SELECT LPAD(' ', 2*(Level - 1))|| ' ' ||  LibLanguage.GetTranslation(Label) Menu_Label 
FROM Menu_Data
START WITH Parent_Label = 'menu'
AND Label = 'mt_settings'
CONNECT BY PRIOR Label = Parent_Label
AND Type = 'WV';

SELECT LPAD(' ', 2*(Level - 1))|| ' ' ||  LibLanguage.GetTranslation(Label) Menu_Label
FROM Menu_Data
START WITH Parent_Label = 'menu'
AND Label = 'mt_administration'
CONNECT BY PRIOR Label = Parent_Label
AND Type = 'WV';

SELECT LPAD(' ', 2*(Level - 1))|| ' ' ||  LibLanguage.GetTranslation(Label) Menu_Label
FROM Menu_Data
START WITH Parent_Label = 'menu'
AND Label = 'mt_operations'
CONNECT BY PRIOR Label = Parent_Label
AND Type = 'WV';

SELECT LPAD(' ', 2*(Level - 1))|| ' ' ||  LibLanguage.GetTranslation(Label) Menu_Label
FROM Menu_Data
START WITH Parent_Label = 'menu'
AND Label = 'mt_reports'
CONNECT BY PRIOR Label = Parent_Label
AND Type = 'WV';

SELECT LPAD(' ', 2*(Level - 1))|| ' ' ||  LibLanguage.GetTranslation(Label) Menu_Label
FROM Menu_Data
START WITH Parent_Label = 'menu'
AND Label = 'mt_data'
CONNECT BY PRIOR Label = Parent_Label
AND Type = 'WV';

