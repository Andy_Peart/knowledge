/******************************************************************************/
/*                                                                            */
/*  Copyright (c) 2014 JDA Software Group, Inc.                               */
/*  All rights reserved - Company Confidential                                */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*  FILE NAME  : access.sql                                                   */
/*                                                                            */
/*  DESCRIPTION: Report to show global and group/workstation access settings. */
/*                                                                            */
/*  DATE     BY   PROJ   ID       DESCRIPTION                                 */
/*  ======== ==== ====== ======== =============                               */
/******************************************************************************/

SET FEEDBACK OFF
SET HEADING OFF
SET VERIFY OFF

CLEAR BREAKS
CLEAR COLUMNS
 
SET PAGESIZE 1000
SET LINESIZE 80

PROMPT
PROMPT Global Access:

SELECT NVL(FA.Enabled, '-') || '         ', SUBSTR(LT.Text, 1, 65)
FROM Language_Text LT, Function_Access FA
WHERE LT.Language = 'EN_GB'
AND LT.Label = FA.Function_ID
AND (FA.Enabled='G' OR FA.Enabled IS NULL)
ORDER BY LT.Text;

PROMPT
PROMPT Global Group/Workstation Access:

SELECT FA.Enabled || '         ', SUBSTR(LT.Text, 1, 65)
FROM Language_Text LT, Function_Access FA
WHERE LT.Language = 'EN_GB'
AND LT.Label = FA.Function_ID
AND (FA.Enabled = 'Y' OR FA.Enabled='N')
ORDER BY LT.Text;

PROMPT
PROMPT Group Access:

BREAK ON Group_ID SKIP 1

SELECT FA.Group_ID, SUBSTR(LT.Text, 1, 65)
FROM Language_Text LT, Group_Function FA
WHERE LT.Language = 'EN_GB'
AND LT.Label = FA.Function_ID
ORDER BY 1, 2;

PROMPT
PROMPT Workstation Access:

BREAK ON Group_ID SKIP 1

SELECT FA.Group_ID, SUBSTR(LT.Text, 1, 65)
FROM Language_Text LT, Workstation_Group_Function FA
WHERE LT.Language = 'EN_GB'
AND LT.Label = FA.Function_ID
ORDER BY 1, 2;
