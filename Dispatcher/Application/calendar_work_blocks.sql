/******************************************************************************/
/*                                                                            */
/*  Copyright (c) 2014 JDA Software Group, Inc.                               */
/*  All rights reserved - Company Confidential                                */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*  NAME:           calendar_work_blocks.sql				      */
/*                                                                            */
/*  DESCRIPTION:    Script to re-generate the calendar work blocks	      */
/*                                                                            */
/*  RELEASE   DATE     BY  PROJ   ID       DESCRIPTION                        */
/*  ======== ======== ==== ====== ======== =============                      */
/*  2009.1.1 24/04/09 EW   DCS    DSP2490  Calendar time zone problem	      */
/******************************************************************************/

DECLARE
	l_Result	number;
	l_ErrMsg	varchar2(200);

	cursor	CalendarSearch
	is
	select	Calendar_ID, Time_Zone_Name
	from	Calendar_Header
	order by Calendar_ID;

BEGIN
	for CalendarRow in CalendarSearch loop
		DBMS_Output.Put_Line('Calendar_ID = [' || CalendarRow.Calendar_ID || ']');

		LibSession.SetTimeZoneProc(CalendarRow.Time_Zone_Name);

		LibCalendar.GenerateWorkBlocks(p_Result=>l_Result,
					       p_ErrMsg=>l_ErrMsg,
					       p_CalendarID=>CalendarRow.Calendar_ID);
	end loop;
END;
/
