
set serveroutput on size 99999
set linesize 255

declare
	type TabRecTyp is record
	(
		TableName	user_tab_columns.table_name%type,
		NumCols		number,
		NumErrors	number := 0
	);
	type TabTyp is table of TabRecTyp index by binary_integer;
	TableList		TabTyp;

	l_TabCount		number;
	l_SKURelErrors		number;
	l_SummaryStr		varchar2(100);
	
begin
	TableList(1).TableName := 'Global_Allocation';
	TableList(1).NumCols := 2;

	TableList(2).TableName := 'Group_Allocation';
	TableList(2).NumCols := 2;

	TableList(3).TableName := 'SKU_Allocation';
	TableList(3).NumCols := 2;

	TableList(4).TableName := 'Global_Putaway';
	TableList(4).NumCols := 4;

	TableList(5).TableName := 'Group_Putaway';
	TableList(5).NumCols := 4;

	TableList(6).TableName := 'SKU_Putaway';
	TableList(6).NumCols := 4;

	TableList(7).TableName := 'Location';
	TableList(7).NumCols := 4;

	TableList(8).TableName := 'Pick_Face';
	TableList(8).NumCols := 2;

	for l_TabCount in 1..TableList.count loop
		dbms_output.put_line('-');
		dbms_output.put_line('Testing table ' || TableList(l_TabCount).TableName
			|| ', Number of columns to check = ' || TableList(l_TabCount).NumCols);
		TableList(l_TabCount).NumErrors := LibLocationZone.CheckTable(
								TableList(l_TabCount).TableName,
								TableList(l_TabCount).NumCols);
	end loop;

	dbms_output.put_line('-');
	dbms_output.put_line('Testing table SKU_Relocation, Number of columns to check = 7');
	l_SKURelErrors := LibLocationZone.CheckSKURelocation;
	
	dbms_output.put_line('-');
	dbms_output.put_line(' ----- SUMMARY ----- ');
	dbms_output.put_line('-');

	for l_TabCount in 1..TableList.count loop
		l_SummaryStr := 'Table: ' || TableList(l_TabCount).TableName;
		if ( TableList(l_TabCount).NumErrors > 0 ) then
			dbms_output.put_line(l_SummaryStr || ': Number of errors: ' ||
						TableList(l_TabCount).NumErrors);
		else
			dbms_output.put_line(l_SummaryStr || ': OK');
		end if;
	end loop;
	if ( l_SKURelErrors > 0 ) then
		dbms_output.put_line('Table: SKU_Relocation: Number of errors: ' ||
					l_SKURelErrors );
	else
		dbms_output.put_line('Table: SKU_Relocation: OK');
	end if;
end;
/

set serveroutput off;

exit;
