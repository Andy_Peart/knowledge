/******************************************************************************/
/*                                                                            */
/*  Copyright (c) 2014 JDA Software Group, Inc.                               */
/*  All rights reserved - Company Confidential                                */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :     sequences.sql                                         */
/*                                                                            */
/*     DESCRIPTION:     Lists the sequences in the database.		      */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/******************************************************************************/

SET FEEDBACK ON
SET VERIFY OFF 
 
CLEAR COLUMNS
CLEAR BREAKS
CLEAR COMPUTES
 
SET PAGESIZE 62
SET NEWPAGE 0
SET LINESIZE 80

COLUMN Sequence_Name	HEADING 'Sequence'	FORMAT A30
COLUMN Min_Value	HEADING 'Min Value'	FORMAT 9999999999
COLUMN Max_Value	HEADING 'Max Value'	FORMAT 9999999999
COLUMN Last_Number	HEADING 'Last_Value'	FORMAT 9999999999

BREAK ON Sequence_Name SKIP 1
 
SELECT Sequence_Name, Min_Value, Max_Value, Last_Number
FROM User_Sequences
ORDER BY Sequence_Name;
