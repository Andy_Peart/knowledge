/******************************************************************************/
/*                                                                            */
/*  Copyright (c) 2014 JDA Software Group, Inc.                               */
/*  All rights reserved - Company Confidential                                */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*  FILE NAME  : pickfacegenreplen.sql                                        */
/*                                                                            */
/*  DESCRIPTION: Generate replenishments for all pick faces which are short.  */
/* 		 Intended for use after data takeon of pick face records.     */
/*                                                                            */
/*  DATE     BY   PROJ   ID       DESCRIPTION                                 */
/*  ======== ==== ====== ======== =============                               */
/*  08/12/98 JH   DCS    NEW3646  Pick face replenishment generation          */
/*  15/03/99 JH   DCS    NEW3838  Workstation group function access           */
/*  29/08/01 SL   DCS    STF5635  NO_REF_4 Add Client ID                      */
/******************************************************************************/

declare

	cursor PickFaceSearch 
	is
	select Key, Site_ID, Location_ID, Client_ID, SKU_ID, Owner_ID
	from Pick_Face;

	cursor QtyOnHandSearch
		(
		SiteID		in	Pick_Face.Site_ID%type,
		LocationID	in	Pick_Face.Location_ID%type,
		ClientID	in	Pick_Face.Client_ID%type,
		SKUID		in	Pick_Face.SKU_ID%type,
		OwnerID		in	Pick_Face.Owner_ID%type
		)
	is
	select nvl(sum(Qty_On_Hand), 0.0)
	from Inventory
	where Location_ID = LocationID
	and Client_ID = ClientID
	and SKU_ID = SKUID
	and ((OwnerID is null) or (OwnerID = Owner_ID))
	and ((SiteID is null) or (SiteID = Site_ID));

	cursor QtyAllocatedSearch
		(
		FaceKey		in	Pick_Face.Key%type,
		ClientID	in	Pick_Face.Client_ID%type,
		SKUID		in	Pick_Face.SKU_ID%type
		)
	is
	select nvl(sum(Old_Qty_To_Move), 0.0)
	from Move_Task
	where Face_Key = FaceKey
	and Client_ID = ClientID
	and SKU_ID = SKUID
	and Task_Type in ('C', 'O')
	and Status != 'Consol';

	cursor QtyDueSearch
		(
		FaceKey		in	Pick_Face.Key%type,
		ClientID	in	Pick_Face.Client_ID%type,
		SKUID		in	Pick_Face.SKU_ID%type
		)
	is
	select nvl(sum(Old_Qty_To_Move), 0.0)
	from Move_Task
	where Face_Key = FaceKey
	and Client_ID = ClientID
	and SKU_ID = SKUID
	and Task_Type in ('R', 'P', 'M');

	Result		integer;
	FaceKey		Pick_Face.Key%type;
	QtyOnHand	Pick_Face.Qty_On_Hand%type;
	QtyAllocated	Pick_Face.Qty_Allocated%type;
	QtyDue		Pick_Face.Qty_Due%type;

begin

	LibSession.InitialiseSession
			(
			'Automatic',
			null,
			'Automatic',
			null
			);

	for PickFaceRow in PickFaceSearch loop
		/*
		Work out the quantity on hand
		*/

		open QtyOnHandSearch 
				(
				PickFaceRow.Site_ID,
				PickFaceRow.Location_ID,
				PickFaceRow.Client_ID,
				PickFaceRow.SKU_ID,
				PickFaceRow.Owner_ID
				);

		fetch QtyOnHandSearch into QtyOnHand;
		close QtyOnHandSearch;

		/*
		Work out the quantity allocated
		*/

		open QtyAllocatedSearch
				(
				PickFaceRow.Key,
				PickFaceRow.Client_ID,
				PickFaceRow.SKU_ID
				);

		fetch QtyAllocatedSearch into QtyAllocated;
		close QtyAllocatedSearch;

		/*
		Work out the quantity due
		*/

		open QtyDueSearch
				(
				PickFaceRow.Key,
				PickFaceRow.Client_ID,
				PickFaceRow.SKU_ID
				);

		fetch QtyDueSearch into QtyDue;
		close QtyDueSearch;

		update Pick_Face
		set Qty_On_Hand = QtyOnHand,
		    Qty_Allocated = QtyAllocated,
		    Qty_Due = QtyDue
		where Key = PickFaceRow.Key;

		LibPickFace.CheckIfAutoReplenRequired
            	    		(
               	 		FaceKey,
                		PickFaceRow.Site_ID,
               	 		PickFaceRow.Location_ID,
                	 	PickFaceRow.Client_ID,
                	 	PickFaceRow.SKU_ID,
                		PickFaceRow.Owner_ID
       	         		);

		if FaceKey > 0 then
        		LibPickFace.GenerateReplenishmentByKey
                        		(
                        		Result,
                      		  	FaceKey
                        		);
		end if;
	end loop;

	commit;
end;
/

