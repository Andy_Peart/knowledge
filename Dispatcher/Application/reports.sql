/******************************************************************************/
/*                                                                            */
/*  Copyright (c) 2014 JDA Software Group, Inc.                               */
/*  All rights reserved - Company Confidential                                */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*  FILE NAME  : reports.sql                                                  */
/*                                                                            */
/*  DESCRIPTION: List the reports available form the report selection         */
/*		 screen.                                                      */
/*                                                                            */
/*  DATE     BY   PROJ   ID       DESCRIPTION                                 */
/*  ======== ==== ====== ======== =============                               */
/******************************************************************************/

SET FEEDBACK OFF
SET HEADING OFF
SET VERIFY OFF

CLEAR BREAKS
CLEAR COLUMNS
 
SET PAGESIZE 1000
SET LINESIZE 80

PROMPT
PROMPT Reports available from report selection screen:

SELECT LT.Text
FROM Report R, Language_Text LT
WHERE LT.Language = 'EN_GB'
AND LT.Label = R.Name
ORDER BY 1;

PROMPT
