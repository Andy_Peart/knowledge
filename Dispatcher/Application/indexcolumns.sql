/******************************************************************************/
/*                                                                            */
/*  Copyright (c) 2017 JDA Software Group, Inc.                               */
/*  All rights reserved - Company Confidential                                */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*                                 JDA                                        */
/*                                                                            */
/*     FILE NAME  :     indexcolumns.sql                                      */
/*                                                                            */
/*     DESCRIPTION:     Lists the indexes and the columns that are indexed.   */
/*                                                                            */
/*   DATE     BY   PROJ	  ID        DESCRIPTION                               */
/*   ======== ==== ====== ========  =============                             */
/*   16/01/17 NS   DCS    DSP11472  Record indexed columns so that they can be*/
/*                                  recreated in the event of accidental drop */
/******************************************************************************/

clear breaks
clear columns

set pagesize 9999
set linesize 100
set verify off

column index_name format a30 heading 'Index' trunc
column table_name format a30 heading 'Table' trunc
column column_name format a30 heading 'Column' trunc

select table_name, index_name, column_name
from user_ind_columns
order by table_name, index_name, column_position;

