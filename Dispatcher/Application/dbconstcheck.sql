/******************************************************************************/
/*                                                                            */
/*  Copyright (c) 2014 JDA Software Group, Inc.                               */
/*  All rights reserved - Company Confidential                                */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*  FILE NAME  : dbconstcheck.sql                                             */
/*                                                                            */
/*  DESCRIPTION: Report to show incorrectly specified dbconst.h values,       */
/*		 and other inconsistent data.				      */
/*                                                                            */
/*  DATE     BY   PROJ   ID       DESCRIPTION                                 */
/*  ======== ==== ====== ======== =============                               */
/*  12/05/98 JH   DCS    NEW3190  Bill of materials/kitting                   */
/*  29/08/01 SL   DCS    STF5635  NO_REF_4 Added Client ID                    */
/*  03/09/01 PHH  DCS    NEW5629  Pack Configuration Clienting                */
/*  17/11/05 DGP  DCS    PDR9705  Various issues with data                    */
/******************************************************************************/

SET FEEDBACK OFF
SET HEADING OFF
SET VERIFY OFF

CLEAR BREAKS
CLEAR COLUMNS
 
SET PAGESIZE 1000
SET LINESIZE 80

prompt
prompt Address constants incorrect:

select count(*)
from address
where address_type not in (select label
			from lookup_table
			where type = 'lookup_address_type');

prompt
prompt Global_Allocation constants incorrect:

select count(*)
from global_allocation
where algorithm not in ('fifocrtdate', 'fifocrtdatezone', 'fullpallcrtdate', 'fullpallcrtdatezone', 'wholecrtdate', 'wholecrtdatezone', 'fifoexpdate', 'fifoexpdatezone', 'fullpallexpdate', 'fullpallexpdatezone', 'wholeexpdate', 'wholeexpdatezone', 'haltallocation', 'pickfaceremainder');

prompt
prompt Inventory constants incorrect:

select count(*)
from inventory
where (qc_status is not null and qc_status not in (select label
					from lookup_table
					where type = 'lookup_qc_status'))
or lock_status not in (select label
			from lookup_table
			where type = 'lookup_inv_lock_status');

prompt
prompt Location constants incorrect:

select count(*)
from location
where loc_type not in (select label
			from lookup_table
			where type = 'lookup_loc_type')
or lock_status not in (select label
			from lookup_table
			where type = 'lookup_loc_lock_status');

prompt
prompt Location Type, Pallet Count Flag combination incorrect:

select count(*)
from location
where loc_type not in ('Tag-Operator', 'Tag-FIFO', 'Tag-LIFO')
and allow_pall_cnt = 'Y';

prompt
prompt Move_Task constants incorrect:

select count(*)
from move_task
where task_type not in (select label
			from lookup_table
			where type = 'lookup_move_task_type')
or status not in (select label
		from lookup_table
		where type = 'lookup_move_task_status');

prompt
prompt Order_Header constants incorrect:

select count(*)
from order_header
where status not in (select label
			from lookup_table
			where type = 'lookup_order_status');

prompt
prompt Pick_Face constants incorrect:

select count(*)
from pick_face
where face_type not in (select label
			from lookup_table
			where type = 'lookup_pick_face_type');

prompt
prompt Pre_Advice_Header constants incorrect:

select count(*)
from pre_advice_header
where status not in (select label
		from lookup_table
		where type = 'lookup_pre_advice_status');

prompt
prompt SKU constants incorrect:

select count(*)
from sku
where (qc_status is not null and qc_status not in (select label
					from lookup_table
					where type = 'lookup_qc_status'))

prompt
prompt SKU_Allocation constants incorrect:

select count(*)
from sku_allocation
where algorithm not in ('fifocrtdate', 'fifocrtdatezone', 'fullpallcrtdate', 'fullpallcrtdatezone', 'wholecrtdate', 'wholecrtdatezone', 'fifoexpdate', 'fifoexpdatezone', 'fullpallexpdate', 'fullpallexpdatezone', 'wholeexpdate', 'wholeexpdatezone', 'haltallocation', 'pickfaceremainder');

prompt
prompt SKU_Putaway constants incorrect:

select count(*)
from sku_putaway
where algorithm not in ('activeaislebyzone', 'preferredlocation', 'lastputlocation', 'pickfacelocation', 'searchbyzone', 'haltputawaysearch', 'beambestfitloc');

prompt
prompt System_Directed constants incorrect:

select count(*)
from system_directed
where algorithm  not in ('sysdirputaway', 'sysdirpicking', 'sysdiralternate', 'sysdirmarshal', 'sysdirrelocation', 'sysdiranything', 'sysdirstockcheck', 'sysdiranymoveentry')
or (pick_direction is not null and pick_direction not in ('C', 'N', 'P'))
or (pick_type is not null and pick_type not in ('N', 'C'));

prompt
prompt Locations without pick sequence values:

select count(*)
from location 
where pick_sequence <= 0.0;

prompt
prompt Locations without put sequence values:

select count(*)
from location 
where put_sequence <= 0.0;

prompt
prompt Locations without check sequence values:

select count(*)
from location 
where check_sequence <= 0.0;

prompt
prompt SKU allocation algorithms with no related SKUs:

select sku_id 
from sku_allocation 
minus 
select sku_id 
from sku;

prompt
prompt SKU putaway algorithms with no related SKUs:

select sku_id 
from sku_putaway 
minus
select sku_id 
from sku;

prompt
prompt SKUs with no linked Config_ID:

select sku_id 
from sku 
minus 
select sku_id 
from sku_sku_config;

prompt
prompt Inventory with missing SKU:

select sku_id 
from inventory 
minus 
select sku_id 
from sku;

prompt
prompt Inventory with missing Location:

select location_id 
from inventory 
minus 
select location_id 
from location;

prompt
prompt Inventory with missing Config_ID (SKU_Config):

select config_id 
from inventory 
minus 
select config_id 
from sku_config;

prompt
prompt Inventory with missing Config_ID (SKU_SKU_Config):

select config_id 
from inventory 
minus 
select config_id 
from sku_sku_config;

prompt
prompt Inventory with null Config_ID:

select count(*) 
from inventory 
where config_id is null;

prompt
prompt Inventory with null Zone_1:

select count(*) 
from inventory I, location L
where I.zone_1 is null
and (L.site_id = I.site_id or L.site_id is null or I.site_id is null)
and L.location_id = I.location_id
and L.loc_type not in ('Suspense', 'Trailer');

prompt
prompt Inventory with invalid QC_Status (null):

select count(*) 
from inventory I, SKU S
where I.qc_status is null 
and S.qc_status is not null
and I.Client_ID = S.Client_ID
and I.SKU_ID = S.SKU_ID;

prompt
prompt Inventory with invalid QC_Status (not null):

select count(*) 
from inventory I, SKU S
where I.qc_status is not null 
and S.qc_status is null
and I.Client_ID = S.Client_ID
and I.SKU_ID = S.SKU_ID;

prompt
prompt Locations with missing Zone_1s:

select zone_1 
from location 
minus 
select 
zone_1 from 
location_zone;

prompt
prompt Locations with missing Work_Zones:

select work_zone 
from location 
minus 
select work_zone 
from work_zone;

prompt
prompt Inventory with missing Zone_1s:

select zone_1 
from inventory 
minus 
select zone_1 
from location_zone;

prompt
prompt SKU_Putaway with missing Zone_1 (Location_Zone):

select zone_1 
from sku_putaway 
minus 
select zone_1 
from location_zone;

prompt
prompt SKU_Putaway with missing Subzone_1 (Location_Zone):

select subzone_1 
from sku_putaway 
minus 
select subzone_1 
from location_zone;

prompt
prompt SKU_Putaway with missing Subzone_2 (Location_Zone):

select subzone_2 
from sku_putaway 
minus
select subzone_2 
from location_zone;

prompt
prompt SKU_Putaway with missing Zone_1 (Location):

select zone_1 
from sku_putaway 
minus 
select zone_1 
from location;

prompt
prompt SKU_Putaway with missing Subzone_1 (Location):

select subzone_1 
from sku_putaway 
minus 
select subzone_1 
from location;

prompt
prompt SKU_Putaway with missing Subzone_2 (Location):

select subzone_2 
from sku_putaway 
minus 
select subzone_2 
from location;

prompt
prompt SKU_Allocation with missing Zone_1 (Location_Zone):

select zone_1 
from sku_allocation 
minus 
select zone_1 
from location_zone;

prompt
prompt SKU_Allocation with missing Zone_1 (Location):

select zone_1 
from sku_allocation
minus 
select zone_1 
from location;

prompt
prompt Locations with missing Storage_Class:

select storage_class 
from location 
minus 
select storage_class 
from storage_class;

prompt
prompt Workstation_Storage_Class with missing Storage_Class:

select storage_class 
from workstation_storage_class 
minus 
select storage_class 
from storage_class;

prompt
prompt Pick_Face with missing Location_ID:

select distinct(location_id) 
from pick_face 
where location_id is not null
minus 
select location_id 
from location;

prompt
prompt Pick_Face with missing SKU_ID:

select distinct(sku_id) 
from pick_face 
minus 
select sku_id 
from sku;

prompt
prompt Pick_Face (fixed) with null Location_ID:

select key 
from pick_face 
where face_type='F' 
and location_id is null;

prompt
prompt Pick_Face (fixed) with non null Zone_1:

select key
from pick_face 
where face_type = 'F' 
and zone_1 is not null;

prompt
prompt Pick_Face (dynamic) with null Zone_1:

select key 
from pick_face 
where face_type='D' 
and zone_1 is null;

prompt
prompt Pick_Face with more than one SKU:

select I.location_id, count(distinct(I.sku_id))
from inventory I, pick_face PF
where I.location_id = PF.location_id
having count(distinct(I.sku_id)) > 1
group by I.location_id 

prompt
prompt Putaway_Location with missing Location_ID (First_Try_Loc):

select first_try_loc 
from Putaway_Location 
where first_try_loc is not null
minus 
select location_id 
from location;

prompt
prompt Putaway_Location with missing Location_ID (Last_Put_Loc):

select last_put_loc 
from Putaway_Location 
where last_put_loc is not null
minus 
select location_id 
from location;

prompt
prompt Inventory with missing Condition_Code:

select condition_id 
from inventory 
minus 
select condition_id 
from condition_code;

prompt
prompt Pick_Faces with missing Zone_1:

select zone_1 
from pick_face 
minus 
select zone_1 
from location;

prompt
prompt Pick_Faces with invalid quantities:

select Client_ID, sku_id, key 
from pick_face 
where nvl(trigger_qty, 0) < 0 
or nvl(min_replen_qty,0) < 0;

prompt
prompt Locations with invalid staging locations:

select location_id 
from location 
where in_stage not in (select location_id from location)
or out_stage not in (select location_id from location);

prompt
prompt Locations with no Check_String:

select count(*) 
from location 
where check_string is null;

prompt
prompt Inventory with missing Batch:

select count(*) 
from inventory 
where batch_id is null 
and qc_status is not null;

prompt
prompt Inventory which should be expired:

select count(*) 
from inventory 
where expiry_dstamp < current_timestamp
and expiry_dstamp is not null 
and nvl (expired, 'N') <> 'Y';
 
prompt
prompt SKU and SKU_Configs with invalid Split_Lowest:

select s.Client_ID, s.sku_id, nvl(s.split_lowest, 'N') SKU, NVL(SC.Split_Lowest,'N') SKU_Config 
from SKU_Config SC, SKU S, SKU_SKU_Config SSC 
where SC.Config_ID = SSC.Config_ID 
and SSC.SKU_ID = S.SKU_ID 
and ((NVL(SC.Split_Lowest,'N') = 'Y'
and NVL(S.Split_Lowest,'N') = 'N')
and (NVL(SC.Split_Lowest,'N') = 'N'
and NVL(S.Split_Lowest,'N') = 'Y'))
and SC.rowid = LibConfig.GetConfigRowid (SSC.Config_ID, SSC.Client_ID);

prompt
prompt SKU_Configs with invalid CLient_ID:
 
select client_id
from sku_config
minus
select client_id
from Client;
 
prompt
prompt SKU_SKU_Config with invalid Config_ID:

select config_id 
from sku_sku_config
minus 
select config_id 
from sku_config;

prompt
prompt Move_Task with invalid Config_ID:

select config_id 
from move_task 
minus 
select config_id 
from sku_config;

prompt
prompt Order_Consolidation with invalid Config_ID:

select config_id 
from order_consolidation
minus 
select config_id 
from sku_config;

prompt
prompt Order_Line with invalid Config_ID:

select config_id 
from order_line
minus 
select config_id 
from sku_config;

prompt
prompt Shipping_Manifest with invalid Config_ID:

select config_id 
from shipping_manifest
minus 
select config_id 
from sku_config;

prompt
prompt Pre_Advice_Line with invalid Config_ID:

select config_id 
from pre_advice_line
minus 
select config_id 
from sku_config;

prompt
prompt Tag_Request with invalid Config_ID:

select config_id 
from tag_request
minus 
select config_id 
from sku_config;

prompt
prompt UPI_Receipt_Line with invalid Config_ID:

select config_id 
from upi_receipt_line
minus 
select config_id 
from sku_config;

prompt
