/******************************************************************************/
/*                                                                            */
/*  Copyright (c) 2014 JDA Software Group, Inc.                               */
/*  All rights reserved - Company Confidential                                */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*  FILE NAME  : adddtfks.sql                                                 */
/*                                                                            */
/*  DESCRIPTION: Add data takeon foreign keys - should remove after data      */
/*               takeon using the dropdtfks.sql script.                       */
/*                                                                            */
/*  DATE     BY   PROJ   ID       DESCRIPTION                                 */
/*  ======== ==== ====== ======== =============                               */
/******************************************************************************/

SET FEEDBACK OFF
SET HEADING OFF
SET VERIFY OFF

CLEAR BREAKS
CLEAR COLUMNS
 
SET PAGESIZE 1000
SET LINESIZE 80

alter table inventory
add constraint fk_data_takeon_site_location
foreign key (site_id, location_id)
references location (site_id, location_id);

alter table inventory
add constraint fk_data_takeon_sku
foreign key (sku_id)
references sku (sku_id);

alter table inventory
add constraint fk_data_takeon_batch
foreign key (batch_id)
references batch (batch_id);
