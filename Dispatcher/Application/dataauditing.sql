SET FEEDBACK OFF
SET HEADING OFF
SET VERIFY OFF
 
CLEAR BREAKS
CLEAR COLUMNS
SET LINESIZE 132

prompt
prompt - Data Audit Script

prompt
prompt Section 1 Custom Function Access:

select DCSDBA.LANGUAGE_TEXT.TEXT || '  ' || DCSDBA.FUNCTION_ACCESS.ENABLED
from DCSDBA.FUNCTION_ACCESS,
DCSDBA.LANGUAGE_TEXT 
where ((DCSDBA.FUNCTION_ACCESS.FUNCTION_ID LIKE 'V_%')
and (DCSDBA.FUNCTION_ACCESS.FUNCTION_ID NOT LIKE 'VL%')
and (DCSDBA.FUNCTION_ACCESS.FUNCTION_ID NOT LIKE 'VV%')
and (DCSDBA.LANGUAGE_TEXT.LANGUAGE = 'EN_GB')) 
and DCSDBA.FUNCTION_ACCESS.FUNCTION_ID = DCSDBA.LANGUAGE_TEXT.LABEL ;

prompt
prompt Section 2.1 User Groups:

select DCSDBA.USER_GROUP.GROUP_ID  || '  ' ||
DCSDBA.USER_GROUP.NOTES || '  ' || 
COUNT (DCSDBA.APPLICATION_USER.USER_ID) C1 
from DCSDBA.USER_GROUP, 
DCSDBA.APPLICATION_USER  
where DCSDBA.USER_GROUP.GROUP_ID (+)  = DCSDBA.APPLICATION_USER.GROUP_ID 
group by DCSDBA.USER_GROUP.GROUP_ID, DCSDBA.USER_GROUP.NOTES ;

prompt
prompt Section 2.2 Workstation Groups:

select DCSDBA.WORKSTATION_GROUP.GROUP_ID, 
DCSDBA.WORKSTATION_GROUP.NOTES, 
COUNT (DCSDBA.WORKSTATION.STATION_ID) 
from DCSDBA.WORKSTATION_GROUP, 
DCSDBA.WORKSTATION  
where DCSDBA.WORKSTATION.GROUP_ID = DCSDBA.WORKSTATION_GROUP.GROUP_ID 
group by DCSDBA.WORKSTATION_GROUP.GROUP_ID, 
DCSDBA.WORKSTATION_GROUP.NOTES ;


prompt
prompt Section 2.3 Does an enabled LIS Support sign on exist (USER ID/ GROUP):

select DCSDBA.APPLICATION_USER.USER_ID, 
DCSDBA.APPLICATION_USER.GROUP_ID 
from DCSDBA.APPLICATION_USER 
where ((DCSDBA.APPLICATION_USER.USER_ID = 'SUPPORT') 
and (DCSDBA.APPLICATION_USER.DISABLED = 'N')) ;

prompt
prompt Section 2.5 Function access enabled but not assigned to a group:

select distinct function_id || '  ' || text 
from function_access, language_text 
where function_id = label
and function_id in (select function_id from function_access where enabled ='Y' 
minus select function_id from group_function)and label =function_id;

prompt
prompt Section 2.6 Security Function access:

select lt.text || '  ' ||  
decode(fa.enabled,'G','On','Off') Status
from function_access fa, language_text lt
where function_id IN ('RESTRICT_LOGIN1','RESTRICT_LOGIN2')
and fa.function_id = lt.label
and lt.language = 'EN_GB'
order by 1;

prompt
prompt Section 2.10 Part 1 - Number of Workstations Set up:

select COUNT (DCSDBA.WORKSTATION.STATION_ID), 
decode(nvl(DCSDBA.WORKSTATION.RDT,'N'),'Y','RDT','PC') 
from DCSDBA.WORKSTATION 
group by DCSDBA.WORKSTATION.RDT ;

prompt
prompt Section 2.10 Part 2 - Licences:

select DCSDBA.LICENCE.RDT_USERS || '-RDT-' || 
DCSDBA.LICENCE.CLIENT_USERS || '-Client-' ||
DCSDBA.LICENCE.ENQUIRER_USERS || '-Enq-' ||
DCSDBA.LICENCE.WEB_USERS || '-Web-' ||
DCSDBA.LICENCE.VOICE_USERS || '-Voice-' ||
DCSDBA.LICENCE.TRANSACTIONS || '-Trans' A
from DCSDBA.LICENCE ;

prompt
prompt Section 2.14 Have Storage Classes been set up:

select DCSDBA.STORAGE_CLASS.STORAGE_CLASS, 
DCSDBA.STORAGE_CLASS.NOTES, 
COUNT (DCSDBA.WORKSTATION_STORAGE_CLASS.STATION_ID) 
from DCSDBA.STORAGE_CLASS, 
DCSDBA.WORKSTATION_STORAGE_CLASS  
where DCSDBA.STORAGE_CLASS.STORAGE_CLASS (+)  = DCSDBA.WORKSTATION_STORAGE_CLASS.STORAGE_CLASS 
group by DCSDBA.STORAGE_CLASS.STORAGE_CLASS, 
DCSDBA.STORAGE_CLASS.NOTES ;

prompt
prompt Section 2.15 Have Handling Classes been set up:

select DCSDBA.HANDLING_CLASS.HANDLING_CLASS, 
DCSDBA.HANDLING_CLASS.NOTES, 
COUNT (DCSDBA.WORKSTATION_HANDLING_CLASS.STATION_ID) 
from DCSDBA.HANDLING_CLASS, 
DCSDBA.WORKSTATION_HANDLING_CLASS  
where DCSDBA.HANDLING_CLASS.HANDLING_CLASS (+)  = DCSDBA.WORKSTATION_HANDLING_CLASS.HANDLING_CLASS 
group by DCSDBA.HANDLING_CLASS.HANDLING_CLASS, 
DCSDBA.HANDLING_CLASS.NOTES ;

prompt
prompt Section 2.16 Have Equipment Classes been set up:

select DCSDBA.EQUIPMENT_CLASS.EQUIPMENT_CLASS, 
DCSDBA.EQUIPMENT_CLASS.NOTES, 
COUNT (DCSDBA.USER_EQUIPMENT_CLASS.USER_ID) 
from DCSDBA.EQUIPMENT_CLASS, 
DCSDBA.USER_EQUIPMENT_CLASS  
where DCSDBA.EQUIPMENT_CLASS.EQUIPMENT_CLASS (+)  = DCSDBA.USER_EQUIPMENT_CLASS.EQUIPMENT_CLASS 
group by DCSDBA.EQUIPMENT_CLASS.EQUIPMENT_CLASS, 
DCSDBA.EQUIPMENT_CLASS.NOTES ;


prompt
prompt Section 3.6 RDT Query Setting:

select rdt_query_max from system_options;

prompt
prompt Section 3.8 Has the function access for Putaway History been enabled:

select decode (enabled, 'G','Yes','No')
from function_access
where function_id = 'SPW_PUTAWAY_HIS';

prompt
prompt Section 3.9 Has the function access for Auditing been enabled:

select decode (enabled, 'G','Yes','No')
from function_access
where function_id = 'AUDITING';

prompt
prompt Section 3.10 The Audit Table settings:

select table_name || ' ' || 
decode (enabled,'Y','Enabled','Disabled') 
from audit_table;

prompt
prompt Section 4.1 Has a Container location been set:

select cont_stage_loc from system_options;

prompt
prompt Section 4.2 Has a Kitting location been set:

select kitting_loc_id from system_options;

prompt
prompt Section 4.3 Default Container Orders / Customers settings:

select NVL((cont_orders),'0'), 
NVL((cont_customers),'0') 
from system_options;

prompt
prompt Section 4.4 Container Volumetrics settings:

select NVL((minimum_fill),'0'), 
NVL((maximum_fill),'0') 
from system_options;

prompt
prompt Section 4.7 Default Language:

select liblanguage.getsystemdefaultlanguage from dual;

prompt
prompt Section 4.8 Print Commands - Label Command:

select nvl(label_command,'NO Command') from system_options;

prompt
prompt Section 4.8 Print Commands - Marshall Command:

select nvl(marshal_command,'NO Command') from system_options;

prompt
prompt Section 4.8 Print Commands - Pallet Command:

select nvl(pallet_command,'NO Command') from system_options;

prompt
prompt Section 4.8 Print Commands - Pallet List Command:

select nvl(pallist_command,'NO Command') from system_options;

prompt
prompt Section 4.8 Print Commands - Report Command:

select nvl(rpt_command,'NO Command') from system_options;

prompt
prompt Section 4.8 Print Commands - Tag Command:

select nvl(tag_command,'NO Command') from system_options;

prompt
prompt Section 4.8 Print Commands - Trailer Command:

select nvl(trail_command,'NO Command') from system_options;

prompt
prompt Section 5.1 Location Zones:

select z.zone_1, count(l.location_id) 
from location l, location_zone z
where z.zone_1 (+) = l.zone_1
group by z.zone_1;

prompt
prompt Section 5.2 Have any work zones been created with underscores:

select location_id, work_zone
from location
where work_zone like '%\_%'
escape '\';

prompt
prompt Section 5.3 Work Zones:

select z.work_zone, count(l.location_id) 
from location l, work_zone z
where z.work_zone (+) = l.work_zone
group by z.work_zone;

prompt
prompt Section 5.4 Total number of locations:

select site_id, 
count (location_id) 
from location group by site_id;

prompt
prompt Section 5.6 Summary of Location lock status (Excludes Suspense and Kitting):

select DCSDBA.LOCATION.LOCK_STATUS, 
COUNT (DCSDBA.LOCATION.LOCATION_ID) 
from DCSDBA.LOCATION
where  DCSDBA.LOCATION.LOCATION_ID NOT IN ('SUSPENSE','KITTING')
group by DCSDBA.LOCATION.LOCK_STATUS ;

prompt
prompt Section 5.7 Incorrectly spelled Lock status:

select DCSDBA.LOCATION.LOCATION_ID 
from DCSDBA.LOCATION 
where ((DCSDBA.LOCATION.LOCK_STATUS 
NOT IN ('Locked', 'UnLocked','OutLocked','InLocked'))) ;

prompt
prompt Section 5.8 Summary of Location Types:

select DCSDBA.LOCATION.LOC_TYPE, 
COUNT (DCSDBA.LOCATION.LOCATION_ID)
from DCSDBA.LOCATION 
group by DCSDBA.LOCATION.LOC_TYPE;

prompt
prompt Section 5.10 Locations with an Instage which does not exist as a location:

select in_stage from location
minus
select location_id from location;

prompt
prompt Section 5.11 - Locations with a location zone which does not exist in the location zone table:

select location_id from location 
where zone_1 NOT IN (select zone_1 from location_zone);

prompt
prompt Section 5.12 - Locations with a work zone which does not exist in the work zone table:

select location_id from location 
where  work_zone NOT IN (select work_zone from work_zone);


prompt
prompt Section 5.13  - Locations with a volume of O:

select DCSDBA.LOCATION.LOCATION_ID, 
DCSDBA.LOCATION.VOLUME 
from DCSDBA.LOCATION 
where ((DCSDBA.LOCATION.VOLUME = 0.000000)) ;

prompt
prompt Section 5.14  - Locations with a weight of O:

select DCSDBA.LOCATION.LOCATION_ID 
from DCSDBA.LOCATION 
where ((DCSDBA.LOCATION.WEIGHT = 0.000000) 
and (DCSDBA.LOCATION.LOCATION_ID <> 'KITTING')) ;

prompt
prompt Section 5.19  - Count of Locations without pick sequence values:
 
select count(*)
from location
where pick_sequence <= 0.0
and location_id <> 'SUSPENSE';
 
prompt
prompt Section 5.19  - Count of Locations without put sequence values:
 
select count(*)
from location
where put_sequence <= 0.0
and location_id <> 'SUSPENSE';
 
prompt
prompt Section 5.19  - Count of Locations without check sequence values:
 
select count(*)
from location
where check_sequence <= 0.0
and location_id <> 'SUSPENSE';

prompt
prompt Section 5.21  - Locations with Disallow Allocation set:

select DCSDBA.LOCATION.LOCATION_ID 
from DCSDBA.LOCATION 
where DCSDBA.LOCATION.DISALLOW_ALLOC = 'Y';

prompt
prompt Section 5.22  - Locations with Allow Pallet Stock Check set:

select count (location_id) 
from location 
where allow_pall_cnt ='Y';

prompt
prompt Section 5.24  - Pick Face Usage:

select Q3.A || '-Total SKUs-' ||
Q1.C || '-Have Fix PFs-' ||
Q2.C || '-Have Dyn PFs-' ||
ROUND(((decode(Q1.C,0,0.0001,Q1.C))/Q3.A)*100,1) || '-Pct Fixed-' ||
ROUND(((decode(Q2.C,0,0.0001,Q2.C))/Q3.A)*100,1) || '-Pct Dyn-' ||
((Q3.A - (Q1.C+Q2.C))/Q3.A)*100 || '-Pct Not with PFs'
from
(select count (sku_id) A from sku) Q3,
(select count (sku_id) C from pick_face where face_type ='F') Q1,
(select count (sku_id) C from pick_face where face_type ='D') Q2;

prompt
prompt Section 5.27  - Pick faces where the trigger is 0:

select decode(count (sku_id),0,'None are zero',(count (sku_id))) 
from pick_face 
where trigger_qty = 0;

prompt
prompt Section 5.28  - Pick faces where the trigger is greater than the minimum:

select decode((count (sku_id)),0,'No Trigger is > Minimum',(count (sku_id))) 
from pick_face
where trigger_qty > min_replen_qty;

prompt
prompt Section 5.30  - Number of Instances where Max Taks Qty is greater than minimum:

select decode((count (sku_id)),0,'No Max Taks Qty > Minimum',(count (sku_id))) 
from pick_face
where max_task_qty > min_replen_qty;

prompt
prompt Section 5.32  - SKU's set up in the pick face table which dont exist in the SKU table:

select sku_id from pick_face
minus
select sku_id from sku;

prompt
prompt Section 5.33 - Do all Pick faces have a site and owner - If no Result answer is Yes or No Pick faces:

select DCSDBA.PICK_FACE.LOCATION_ID, 
decode(DCSDBA.PICK_FACE.SITE_ID,'1','Site OK','Site Required'), 
decode(DCSDBA.PICK_FACE.OWNER_ID,'20','Owner OK','Owner Required') 
from DCSDBA.PICK_FACE 
where ((DCSDBA.PICK_FACE.SITE_ID IS NULL ) 
or (DCSDBA.PICK_FACE.OWNER_ID IS NULL )) ; 

prompt
prompt EXTRA - Pick faces with multiple SKU's (A Location can only Have 1 SKU):

select location_id from 
(select count (sku_id) cnt, location_id, site_id from pick_face group by location_id, site_id) 
where cnt >1;

prompt
prompt SECTION 6.1 - Has Each Height been used:

select DECODE((count (sku_id)),0,'No',(count (sku_id))) || '-SKUs have each height' 
from sku 
where each_height > 0;

prompt
prompt SECTION 6.2 - Has Each Weight been used:

select DECODE((count (sku_id)),0,'No',(count (sku_id))) || '-SKUs have each weight' 
from sku 
where each_weight > 0;

prompt
prompt SECTION 6.3 - Has Each Volume been used:

select DECODE((count (sku_id)),0,'No',(count (sku_id))) || '-SKUs have each volume' 
from sku 
where each_volume > 0;

prompt
prompt SECTION 6.4 - SKU's with Split lowest Ticked without a split lowest pack configuration:

select sku_id from sku where split_lowest = 'Y'
minus
select sku_id from sku_sku_config where config_id IN 
(select config_id from sku_config where split_lowest ='Y');

prompt
prompt SECTION 6.6 - Count of SKU's with New Product = 'Y':

select count (sku_id) from sku where new_product = 'Y';

prompt
prompt SECTION 6.6 - Function access setting for New Product functionality:

select decode(enabled,'G','Its ON','Its Off') 
from function_access 
where function_id ='NEW_PROD_DIS';

prompt
prompt SECTION 6.7 - Are Allocation Groups Set Up:

select allocation_group, notes from allocation_group;


prompt
prompt SECTION 6.7 - How many SKU's have each allocation group:

select NVL(allocation_group,'No Group') A, 
count (sku_id) 
from sku
group by allocation_group;

prompt
prompt SECTION 6.8 - Are Putaway Groups Set Up:

select putaway_group, notes from putaway_group;

prompt
prompt SECTION 6.8 - How many SKU's have each putaway group:

select NVL(putaway_group,'No Group') A, 
count (sku_id) 
from sku 
group by putaway_group;

prompt
prompt SECTION 6.9 - Are Handling Classes Set Up:

select handling_class, notes from handling_class;

prompt
prompt SECTION 6.9 - How many SKU's have each Handling Class:

select NVL(handling_class,'No Class') A, 
count (sku_id) 
from sku 
group by handling_class;

prompt
prompt SECTION 6.9 - How many Work Stations have each Handling Class:

select handling_class A, 
count (station_id) 
from workstation_handling_class 
group by handling_class;

prompt
prompt SECTION 6.10 - Is Hidden Expiry Date functionality Switched on:

select decode(enabled,'G','Its ON','Its Off. It wont be visible in SKU Maintenance') 
from function_access 
where function_id ='EXPIRY_CONTROL';

prompt
prompt SECTION 6.10 - How has expiry Required been used on the SKU's:

select count (sku_id), decode(expiry_reqd,'Y','Set to Yes','N','Set to No')
from sku
group by decode(expiry_reqd,'Y','Set to Yes','N','Set to No');

prompt
prompt SECTION 6.11 - How has expiry Required been used on the SKU's:

select decode(Q1.A,0,'No SKUs have a Preferred Putaway Location',Q1.A || ' ' || 'SKUs Has / Have a Preffered Putaway Location') from
(select count (config_id) A from Putaway_Location 
where first_try_loc IS NOT NULL) Q1;

prompt
prompt SECTION 6.12 - How has Tag Volume been used on the pack Configurations (Tag Volume/Count of Configs):

select nvl(tag_volume,0), count (config_id) from sku_config
group by nvl(tag_volume,0);

prompt
prompt SECTION 6.13 - How has layer height and each's per layer been used:

select count (config_id) || ' Configs are set with ' || ' ' ||
NVL(to_char(layer_height),'No') || '-Layer Height' || ' ' ||
NVL(to_char(each_per_layer),'No ')|| '-Each Per Layer' C
from sku_config
group by layer_height, each_per_layer;

prompt
prompt SECTION 6.14 - How has Volume at each been used:

select count (sc.config_id) || ' Pack Configs are set with Volume at each = ' || ' ' ||
volume_at_each || ' and it applies to ' || 
nvl(count (ssc.sku_id),0) || ' SKUs'
from sku_config sc, sku_sku_config ssc
where ssc.config_id (+) = sc.config_id
group by volume_at_each;

prompt
prompt SECTION 6.15 - How have Pack Configuration Tracking levels been used:

select NVL(SKU_CONFIG.RATIO_1_TO_2,0) || ' ' || 
NVL(SKU_CONFIG.TRACK_LEVEL_1,'NULL') || ' TRACK_LEVEL_1 ' || 
NVL(SKU_CONFIG.TRACK_LEVEL_2,'NULL') || ' TRACK_LEVEL_2 ' || 
NVL(SKU_CONFIG.RATIO_2_TO_3,0) || ' ' || 
NVL(SKU_CONFIG.TRACK_LEVEL_3,'NULL') || ' TRACK_LEVEL_3 ' || 
NVL(SKU_CONFIG.RATIO_3_TO_4,0) || ' ' || 
NVL(SKU_CONFIG.TRACK_LEVEL_4,'NULL') || ' TRACK_LEVEL_4 ' || 
COUNT (SKU_CONFIG.CONFIG_ID) || ' CONFIGS ' || 
COUNT (SKU_SKU_CONFIG.SKU_ID)|| ' SKUs ' A
from SKU_CONFIG, 
SKU_SKU_CONFIG  
where SKU_SKU_CONFIG.CONFIG_ID (+) = SKU_CONFIG.CONFIG_ID 
group by SKU_CONFIG.RATIO_1_TO_2, 
SKU_CONFIG.TRACK_LEVEL_1, 
SKU_CONFIG.TRACK_LEVEL_2, 
SKU_CONFIG.RATIO_2_TO_3, 
SKU_CONFIG.TRACK_LEVEL_3, 
SKU_CONFIG.RATIO_3_TO_4, 
SKU_CONFIG.TRACK_LEVEL_4 ;

prompt
prompt SECTION 6.16 - Number of SKU's with more than 1 pack configuration:

select count(Q1.A1)
from (select sku_id A1, count(config_id) A2 from sku_sku_config group by sku_id) Q1
where Q1.A2 >1;

prompt
prompt SECTION 6.17 - Pack configurations that are not in use:

select config_id from sku_config
minus
select config_id from sku_sku_config;

prompt
prompt SECTION 7.2 - Location zones in the Inventory table but not in the location zone table:

select zone_1 from inventory 
minus 
select zone_1 from location_zone;

prompt
prompt SECTION 7.3 - Expired Status (Only Includes Inventory with an Expiry Date):

select count (tag_id), decode(expired,'Y','Expired','Not Expired') 
from inventory 
where expiry_dstamp IS NOT NULL group by expired;

prompt
prompt SECTION 7.4 - Condition Code Use:

select c.condition_id || ' - Notes: ' || 
c.notes|| ' - Count of tags: ' ||
count (i.Tag_id) 
from condition_code c, inventory i
where i.condition_id (+) = c.condition_id
group by c.condition_id, c.notes;

prompt
prompt SECTION 7.5 - Lock Code Use:

select c.lock_code || ' - Notes: ' || 
c.notes|| ' - Count of tags: ' ||
count (i.Tag_id) 
from inventory_lock_code c, inventory i
where i.lock_code (+) = c.lock_code
group by c.lock_code, c.notes;

prompt
prompt SECTION 7.6 - Lock Status:

select count (tag_id), nvl(lock_status,'No Status')
from inventory 
group by lock_status;

prompt
prompt SECTION 7.7 - QC Status:

select count (tag_id), nvl(qc_status,'No Status')
from inventory 
group by qc_status;

prompt
prompt SECTION 7.8 - Pallet Types:

select count (tag_id), nvl(pallet_config,'No Type')
from inventory 
group by pallet_config;

prompt
prompt SECTION 7.9 - Origin:

select count (tag_id), nvl(origin_id,'No Origin')
from inventory 
group by origin_id;

prompt
prompt SECTION 7.10 - Owner:

select count (tag_id), nvl(owner_id,'No Owner')
from inventory
group by owner_id;

prompt
prompt SECTION 7.11 - Client:

select count (tag_id), nvl(client_id,'No Client')
from inventory
group by client_id;

prompt
prompt SECTION 8.1 - Global Allocation Algorithms:

select GLOBAL_ALLOCATION.SITE_ID || ' - ' || 
LANGUAGE_TEXT.TEXT || ' - Prty - ' || 
GLOBAL_ALLOCATION.PRIORITY || ' - Zone - ' ||
GLOBAL_ALLOCATION.ZONE_1 || ' - Fin Zone - ' ||
GLOBAL_ALLOCATION.FINAL_LOC_ZONE || ' - Add Zone - ' ||
GLOBAL_ALLOCATION.ADDITIONAL_ZONE || ' - Alloc - ' ||
GLOBAL_ALLOCATION.ALLOC_ALGORITHM || ' - Replen - ' ||
GLOBAL_ALLOCATION.REPLEN_ALGORITHM 
from GLOBAL_ALLOCATION, 
LANGUAGE_TEXT  
where GLOBAL_ALLOCATION.ALGORITHM = LANGUAGE_TEXT.LABEL 
order by GLOBAL_ALLOCATION.PRIORITY ;

prompt
prompt SECTION 8.2 - Group Allocation Algorithms set up but not assigned to SKU's:

select allocation_group from allocation_group
minus
select allocation_group from sku;

prompt
prompt SECTION 8.2 - Group Allocation Algorithms Groups with no algorithms:

select allocation_group from allocation_group
minus
select allocation_group from group_allocation;

prompt
prompt SECTION 8.3 - Have SKU Allocation algorithms been used:

select count(sku_id)|| '- SKUS in total- ' || 
Q1.A1|| '- Have Algorithms which is  ' || 
((Q1.A1/count(sku_id))*100) || ' Percent '
from sku,
(select count(distinct(sku_id)) A1 from sku_allocation) Q1
GROUP BY Q1.A1;

prompt
prompt SECTION 8.4 - Global Putaway Algorithms:

select GLOBAL_PUTAWAY.SITE_ID || ' - ' || 
LANGUAGE_TEXT.TEXT || ' - Prty - ' ||
GLOBAL_PUTAWAY.PRIORITY
from GLOBAL_PUTAWAY, 
LANGUAGE_TEXT  
where GLOBAL_PUTAWAY.ALGORITHM = LANGUAGE_TEXT.LABEL 
order by GLOBAL_PUTAWAY.SITE_ID, GLOBAL_PUTAWAY.PRIORITY  ;

prompt
prompt SECTION 8.5 - Group Putaway Algorithms set up but not assigned to SKU's:

select putaway_group from putaway_group
minus
select putaway_group from sku;

prompt
prompt SECTION 8.6 - Group Putaway Algorithms Groups with no algorithms:

select putaway_group from putaway_group
minus
select putaway_group from group_putaway;

prompt
prompt SECTION 8.7 - Have SKU Putaway algorithms been used:

select count(sku_id)|| '- SKUS in total- ' || 
Q1.A1|| '- Have Algorithms which is  ' || 
((Q1.A1/count(sku_id))*100) || ' Percent '
from sku,
(select count(distinct(sku_id)) A1 from sku_putaway) Q1
GROUP BY Q1.A1;




























































