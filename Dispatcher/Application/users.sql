/******************************************************************************/
/*                                                                            */
/*  Copyright (c) 2014 JDA Software Group, Inc.                               */
/*  All rights reserved - Company Confidential                                */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :     users.sql                                             */
/*                                                                            */
/*     DESCRIPTION:     Outputs list of users and theirlast action info.      */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/******************************************************************************/

set pagesize 1000

SELECT  ASE.Station_ID "Station",
        ASE.User_ID "User",
        SUBSTR(AUS.Notes, 0, 25) "Notes",
        LibDate.IntervalMinute(CURRENT_TIMESTAMP - ASE.Last_DStamp) "Idle m",
        SUBSTR(ASE.Last_Action, 0, 20) "Last Action"
FROM Application_Session ASE, Application_User AUS
WHERE ASE.User_ID = AUS.User_ID (+)
ORDER BY 4 DESC
/
