
--
--create table VWSALookupData
--	(
--	Site_ID		varchar2(10),
--	Customer_ID	varchar2(15),
--	Order_Type	varchar2(10),
--	Instructions	varchar2(180),
--	Ship_Dock	varchar2(10),
--	Consignment	varchar2(10)
--	);
--
--alter   table   VWSALookupData
--                add constraint u_VWSALookupData
--                unique (Site_ID, Customer_ID, Order_Type, Instructions)
--                using index tablespace indexes;
--

create or replace package VWSALookup is

function	GetOHShipDock
(
p_SiteID	Order_Header.From_Site_ID%type,
p_CustomerID	Order_Header.Customer_ID%type,
p_OrderType	Order_Header.Order_Type%type,
p_Instructions	Order_Header.Instructions%type
)
return Order_Header.Ship_Dock%type;

pragma	restrict_references (GetOHShipDock, wnds, wnps);

function	GetOHConsignment
(
p_SiteID	Order_Header.From_Site_ID%type,
p_CustomerID	Order_Header.Customer_ID%type,
p_OrderType	Order_Header.Order_Type%type,
p_Instructions	Order_Header.Instructions%type
)
return Order_Header.Consignment%type;

pragma	restrict_references (GetOHConsignment, wnds, wnps);

end VWSALookup;
/

create or replace package body VWSALookup is

function	GetOHShipDock
(
p_SiteID	Order_Header.From_Site_ID%type,
p_CustomerID	Order_Header.Customer_ID%type,
p_OrderType	Order_Header.Order_Type%type,
p_Instructions	Order_Header.Instructions%type
)
return Order_Header.Ship_Dock%type is

	cursor ShipDockSearch
	is
	select Ship_Dock
	from VWSALookupData
	where Site_ID = p_SiteID
	and Customer_ID = p_CustomerID
	and Order_Type = p_OrderType
	and ((Instructions = p_Instructions) 
	or (Instructions is null and p_Instructions is null));

	ShipDockRow	ShipDockSearch%rowtype;

	l_ShipDock	Order_Header.Ship_Dock%type := null;

begin

	open ShipDockSearch;

	fetch ShipDockSearch into ShipDockRow;

	if ShipDockSearch%found then
		l_ShipDock := ShipDockRow.Ship_Dock;
	end if;

	close ShipDockSearch;

	return l_ShipDock;

end GetOHShipDock;

function	GetOHConsignment
(
p_SiteID	Order_Header.From_Site_ID%type,
p_CustomerID	Order_Header.Customer_ID%type,
p_OrderType	Order_Header.Order_Type%type,
p_Instructions	Order_Header.Instructions%type
)
return Order_Header.Consignment%type is

	cursor ConsignmentSearch
	is
	select Consignment
	from VWSALookupData
	where Site_ID = p_SiteID
	and Customer_ID = p_CustomerID
	and Order_Type = p_OrderType
	and ((Instructions = p_Instructions) 
	or (Instructions is null and p_Instructions is null));

	ConsignmentRow	ConsignmentSearch%rowtype;

	l_Consignment	Order_Header.Consignment%type := null;

begin

	open ConsignmentSearch;

	fetch ConsignmentSearch into ConsignmentRow;

	if ConsignmentSearch%found then
		l_Consignment := ConsignmentRow.Consignment;
	end if;

	close ConsignmentSearch;

	return l_Consignment;

end GetOHConsignment;

end VWSALookup;
/

grant execute on VWSALookup to dcsusr;
