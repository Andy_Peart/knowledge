/******************************************************************************/
/*                                                                            */
/*  Copyright (c) 2014 JDA Software Group, Inc.                               */
/*  All rights reserved - Company Confidential                                */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*  FILE NAME  : dropdtfks.sql                                                */
/*                                                                            */
/*  DESCRIPTION: Drop data takeon foreign keys - added using the              */
/*		 adddtfks.sql script.                                         */
/*                                                                            */
/*  DATE     BY   PROJ   ID       DESCRIPTION                                 */
/*  ======== ==== ====== ======== =============                               */
/******************************************************************************/

SET FEEDBACK OFF
SET HEADING OFF
SET VERIFY OFF

CLEAR BREAKS
CLEAR COLUMNS
 
SET PAGESIZE 1000
SET LINESIZE 80

alter table inventory
drop constraint fk_data_takeon_site_location;

alter table inventory
drop constraint fk_data_takeon_sku;

alter table inventory
drop constraint fk_data_takeon_batch;
