/******************************************************************************/
/*                                                                            */
/*  Copyright (c) 2014 JDA Software Group, Inc.                               */
/*  All rights reserved - Company Confidential                                */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*  FILE NAME  : language.sql                                                 */
/*                                                                            */
/*  DESCRIPTION: Highlight discrepancies in the loaded language text, i.e.    */
/*		 labels defined for one language but not another.  This query */
/*		 is quite slow.						      */
/*                                                                            */
/*  DATE     BY   PROJ   ID       DESCRIPTION                                 */
/*  ======== ==== ====== ======== =============                               */
/******************************************************************************/

SET FEEDBACK OFF
SET HEADING OFF
SET VERIFY OFF

CLEAR BREAKS
CLEAR COLUMNS
 
SET PAGESIZE 1000
SET LINESIZE 80

PROMPT
PROMPT Labels missing from nl_nl:

SELECT Label
FROM Language_Text
WHERE Language = 'EN_GB'
MINUS
SELECT Label 
FROM Language_Text
WHERE Language = 'NL_NL'
ORDER BY 1;

PROMPT
PROMPT Labels missing from es_es:

SELECT Label
FROM Language_Text
WHERE Language = 'EN_GB'
MINUS
SELECT Label 
FROM Language_Text
WHERE Language = 'ES_ES'
ORDER BY 1;

PROMPT
