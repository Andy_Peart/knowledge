/******************************************************************************/
/*                                                                            */
/*  Copyright (c) 2014 JDA Software Group, Inc.                               */
/*  All rights reserved - Company Confidential                                */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*     FILE NAME  :     excelheadings.sql                                     */
/*                                                                            */
/*     DESCRIPTION:     Outputs tables/columns suitable for excel headings.   */
/*                                                                            */
/*   DATE     BY   PROJ   ID       DESCRIPTION                                */
/*   ======== ==== ====== ======== =============                              */
/******************************************************************************/

set feedback off
set heading off
 
set serveroutput on size 99999
set linesize 255

declare

	cursor tablesearch
	is
	select table_name
	from user_tables
	where table_name not like 'BIN\$%'
	order by table_name;

	cursor columnsearch
		(
		tablename	in	varchar2
		)
	is
	select column_name
	from user_tab_columns
	where table_name = tablename
	order by column_id;

	outputline	varchar2(255);

begin

	for tablerow in tablesearch loop
		dbms_output.put_line(tablerow.table_name || chr(9));
		outputline := '';

		for columnrow in columnsearch(tablerow.table_name) loop
			if (length(outputline) + length(columnrow.column_name)) > 254 then
				dbms_output.put_line(outputline);
				outputline := '';
			end if;

			if length(outputline) > 0 then
				outputline := outputline || chr(9);
			end if;

			outputline := outputline || columnrow.column_name;
		end loop;

		if length(outputline) > 0 then
			dbms_output.put_line(outputline);
			outputline := '';
		end if;

		dbms_output.put_line(chr(9));
	end loop;

end;
/
