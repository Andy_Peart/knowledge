drop type TabTaskSummaryTimes FORCE;
create type TabTaskSummaryTimes
    as table of ObjTaskSummaryTime;
/
SHOW ERRORS
