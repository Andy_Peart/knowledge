drop type TabStagingTasks FORCE;
create type TabStagingTasks
    as table of ObjStagingTask;
/
SHOW ERRORS
