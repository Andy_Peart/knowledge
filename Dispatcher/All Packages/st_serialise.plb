drop type TabSerialData32K FORCE;
/
create type TabSerialData32K
    as table of varchar2(32767);
/
SHOW ERRORS
