create or replace package body LibSoftAllocate wrapped 
a000000
369
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
b
767a 1de8
g5rd0rWtcH7XL4MG3Eh3TpZLwPYwg80Ac8cGU/H+HoZkUM091Y8cOL06dTFdZVzLdd1tLYgF
FLdVI+l/9BfmsjdCz0d+5TPQnUihp2yT3wR17eq/xn55X3dVp6M7K78KbDkrHjxFLfoNsT/K
/10LfLWgwujrOoYwlPeHaWbihLlWnT0N5MlDU/LR4loKHupK/T3Z0+4sCFCIRTtLwb5d1Ms7
2grBVLFKpDCQpRDOyqnFBt58/M6HJH0Cn2QuI9EI5GHN0EVhRKr8VCoVRcxkc7GlOHCxFDSn
6wNVUKmVxVkWr81pQL8j4kwHZe8ycnn+Syw+NswL5k65TAV4oPSnxufhvMVBz9p2kpf2qUzv
kQWFwHZkAQLfao1Xy0/OiPZaV55AEJpIs5btphFhqUGpoU5B59yhagVNG3OoX62hzVij2WAb
ma25xxsI7nCmslBn/5HI9tV+Msf8Qy2FfkCamClD+6YSIDg0BZ9vqXM4UE4sYHkENtMzkp8v
o3P7vrsCTNsNdqNaP4FxoY6imc2zUJU17UDolwOF54ObNDaLRhV0HWdIBZDzyx59VOVIBBFN
UNfmUEkCDq6ZNU+UrCKWNnPoL9IFeWtSUzea6VtxlSC59KrCSfkpb877RxW4XTD+ppyoa2sf
o//jPompU2r5Dc2w5TULAVyyTG6Rn9oKIp2nJ93Yl6KA6KjxRfNMGCUv2DiTzkIyUsZl5a3u
R5Lko2DPjwXdm0dKKNY3zTmfK+0gKHYFzURMHQ9nO8TNDkP+OfUmb9b5K1jzayhANXLtiK0z
kGJYpiA+L5dQ5wERXaMQob9BTyte5vRAFd3+o58SfQUUS2pIBcava1W7VzwuvZxjZvZ7Vg1U
IKMnKR+IJQ27jZxjaXQefr0FVqpGTO9a+1ZjCeWJnqDP2UCc5WxIttJoULw3xA6ob2SOhI9B
rlDc535dEhlmLIZaeYk7WHQLoeZtBdU4Ji4u8GEW94N2tMt1G6DXqXV1bXAGoYn0wePl+NHj
JI4UeRTqwHg2DkTV1amV8OBpND1sYG5xJNtkOczb+qhOM8L1edbwyud77R75+A55Dygg/B0s
qcUB11bi2Q80lV6gyoCRvquWK0OrrbeKBpBaTMivMH8/9Cuz4EI1vEqj66EhujVyP9W1CIAV
qST5FrWMOT2O9LGX1qh1qalSmKKarxgg3f7LHxSamID0WZEYw/F4PdOjfFNKwbKPAGVsQglQ
UonI+j3a3iew6Vns2Y7xqn/hZnk5xgVxwYyjXtrcBcCagXZuUecckoQTywxkhMVRQ7WBSiHl
FIqD9YoMdzweAufJ1lmBe6VGKLQ4MFbM1+lJCmia3z0+uvW0aK5+lCDcaOLZ07wqYxHDAolk
WMhiiBj6V7uyJz4A6/Nf0KmelVs1piaKW7T2KEFinKk44LKQyPVWSbTyxi0rvqS7D2eziPdX
JGV1zGl4h9aE6ylVd6+/YTsr/nNQrEipk1z7gUDLqBBtsuvzDWZE6bPq9RRz1OTfkk3RBlzh
keDFpT/+/yYaANer/54WKxpxOSg7j9/7hpoakjVkbD1OIJseRe2vZyMotcU1fK9aZ9u/veiy
CT/Gzq+p8eGp4FbTuahi/DwF7aYS9SgupeTI30Ydx0F/fK2b3W3mrtHd34/sahBoq+bbD+VG
B/tXAe43wPJQ2A6/hjM43TD67cGaSkjPIFfjNUA1fYGI3mAaIESoz9BEJcPFJuZNJXbGo0XY
3hbaf4mQqz5BiMpysgEgdAEgMhg7uTsZolYn6G3cJekmM3+xO6fR9quX3HQBPhhw5h42LU1Y
0XuhH2MmQikzs8LJdCmb3gevgoc5baR9iLyKWGIWYd835Hc/2KFj4VItC3xZpOXKTEMymwUy
VykQJQVY92V2j1BxG/dgLSEOKVE7tqoCZRzRag4sW6yAJmZhUFBSlPeXYua4LUM2GSx506n7
QtT84YWmj4OfBUgSSDhony+prizL1jBDieVAc/nWKzScMh5ZybM4X7ry3igF32I3Rtu4Wrqp
f6iJO4cA8s03HtjZWUCvurbJsKJNCwepDramyzAYVBvnYlup9UYQrfp1gvgh/sGa+rOzs9qb
P4HymYxJl56+YoGRQxdZ6zREzV45CZvV1zaUmTXIbAEcV9xrS8DNc0M8zzMhky6Lgu9KNBjH
Way7pbxyUQLz1padvXyqrhd/qohEt1yR2/Gxz5OfUAuFNttnmNr1du7pGlRFBVBFx6w7bLSj
ZU/3PYLka//uxyHJuYtOMEWSvy7dDXNGjA+PapAE/NdwD6sOTGvMN6Q23XeS8IZMukqA7zVh
bXtLLsUBbyoMiWm+YGVCabc0kPxriGt4Rlmnkd8P9xHf+VdEJnpmTI/WcNZ3CHjAxoaqJq+O
M9X7Dy+MRKJkyYv2ySp4h4IeC9kqtmdUO6IzYCjbIpKAsRiFagj1+6zS8kVvMBELf/rVs/wt
q07DeR0LuPTN2i1jrqOgfMh+B6JMYWcllyhDCX4wfLaL8kOpSxFlY1NrDLjib2a74lpAGdoh
x5OgyRgP4J8/o66wZJG9S71eeFklOYMhUapXgiAHffsVLI8tRNnxB9qcEd1x7bdHWDAw30Qc
BhCuTSF9BsWQdt7HNlYH8jn9OP9DSt0ZbxX1Nm3Q/emus7d4ttNuJ4ziGiauG3hZKoyhMWLq
zGZMyFohV/MiOBff9dOogwyMgLZpBpmht16vWNeTs5vZ7F8f80KIlrzdenaW+lBD00YY4jll
6SKA1Z7a+d9BWS2J5MeBo2SxAs+hecM7owQuaj96o3olM5jRykW2caVeyuFTWMNB57s8397W
D0kKxrKettDrpvUVNHbGl9SmtO7Scp8Jw1uY4IF6WBAbmknrPjoE3uwY2ItnOStb8oJv1WsT
M093p8hxf4wCaWXH13r++tT6ZL16VuE+jtaSmjvVqEeG5x+zEd8Pqj9g4j7PWTB1TT/WxDA4
Ee+3dF4HwwXKB27doRpWtqgNoKvdRyv6SG79PxCrZGn5IWLnC9wvbhS51Maxd0BgRVIY0JwB
txpV3qCpedXn1a0Icp2Ih+rvndtPe/hqMwz+oCN9pXzGc6yDiwD+0dJieE3Ez/0W1A8ModsT
P7Ch1Q4Ll54eU1Ubg9y/DCVMPmz6f6dqr1C3OJRC5RID0cs7/Tn3L3QAl6RosGmXEJgdWcC6
YVL4STG79M3qGUZ56lYyQrcTC8VVDsQBQVgOXVeWLtwYajTolGDL+9ZNMPlisqawZz20fEhf
S1BL3lUJjNQ26J0q54rfLv6mj2D/4NRVNasXCTDgQDk4DeRbp/Pm6aWob5jdQBsyyP9S5Tcj
WaPGEUjdEeh83N9wbhp83Hhwbuh8/99wbmN8/3hwbrB8GX7dMLcVMJj7hjuss/wZi3RbmZ32
pdfPzujLqvEoqlFqT4tqCoapl9WwHXoGZAsheHAhXgRJ/XzcCQSw/Xx3P1NqP9x4tN/WOZpH
ByD8pUk6pUl75N4iNDw/lr92zv9ASrNoqa/43Z8DGi7ARJVLHegOFPbp3+xuPwlcioztR+wP
jJVuz+xkF64ym/6BwOmnsxjsrXdTs3QTrusIFyGIqhLOBxzgMXCFihfJWi18oNIJodN+XiJ/
gRFuF51fB7LeKFHpoQ6Ars9w31Kb98KFpuV5tc5ZVjZ+WzcqdO1HWTkJ5SVoREGgpuLfYRND
VWs9QN8FPEK9BWNxXAwh1meuCiWbJvJxV2X++H3UBWH0XgWK78ytrtecnyEiccKJ3FkZCYNs
uvL6t+Yvdrkq/2lFs6Y74C4GhCwjFvmUqJRcqwnwmFFDXiZRS3+w5aIhvhnfWCdeyz0rJl70
Dl5mUrVZk2mj+cmLGYhV/aHoDd84PV/o2L5zJBPZw7i5sLanywA6rbTU2+/iqHd270HECcrB
STHuJDBIX5LMwLgJXFtaUfrZ+Tq10uy1hCkqEmMlpXbsSACqLs3JbHKl2IhsbOjVsNsaxyAU
4GNo2P0qrsnZNojH/+eO73J8al3Lo4s6KO+rx8IR4t6QPAi23gd8mTNyZbMRzReOGlmsX+TT
PaMWflaJn66iMSIdbNWBRq374JWx17WsCMPzfGU8jqUJRoArzoHamOb9M7djzsT/guwr4hTI
xutARMCk2e2Io3jfucElKYZLLxFjz5r8eHLcq8YBxSkISFDLjuxurLQaPtMu6K+RhFatWFlH
6Vys+r6KUY+TOEN21Z8Laa2kXv/nPsFgiYLhlwXUt4IV+tQRwLQsSzcaCL+ShXUD6chC2QTH
9y9OPPOL4fuLNM6bn/tDR2fA7lfA7nzA7hbA7srA7gTA7i/A7ufA7qbAr85Sr6VSr31Sr49S
7rRS7lBS7oVS7t1Sr1NSr0tSr01Srw5S7nVS7shSCOMIufuG5SdXdhSeUQd4siyBaNitbxKl
fVrFQfqHX1aXXxGRh533GN4EKDT8qNd0D0wuepVpHZf9AGFahAQGuj7N/80csmPv/743aIDo
LBbdA5SCoxkv5EG12Bwh3dXAj6BnafE6OVXcORztWunTjizxStMJUUeu2rd/sXCbrHbr+XU3
dy5ALsGg8x9ZQJCQUBE9Yzp38uUSW78Rn79gA965/Sb/rbEMpKEBMMKs1xVCw2smHk1fM0E6
Bp47YtPUKHj1HxoDQQAGN+HgACJg2fXTvtSzm5GhQRvZV4zP8zs8bZPxK/p6EQJMoeI6Sr3A
QjsXKuRwgO2ysh2pu4yPQrxUr7VJDyr3aa15aW7CDrjMzA20YJOz3SZLBEkN51dNBHrZVOvT
fEJi/jcibSauNWEFcUcZkyHVt1TqaS0rWBunsi8zzetVnvJsBeIjHIa9OUUqNBTArA4trxF7
haE3GwCSucYqQnsVCeQG/+qmMexkF9VM71i6lUbiVMHDCGmSbEy/Mkvi7kYVxQdKRK4hXP2v
Izho6HK7jn9aWDgnBuYFPgp/GtaBm6zbl9ZQvNPlehHh4RE0T3seKdJJQ8pUQq2c0Hx6lTjK
p8K9VyEXXtyteHkelFNzONmYLImQoVKc1sChaslEcWki4UFqr8li1tlHQ1oB5eiihOzLFgTl
bD2ZaT6ZPnyWV1Gc6XEohAzgrhTrkIVTOhsrPUwe/2CowfsKLM+1siga9vnjMCQ9Kqqnrptq
R7hS6hwk3IXyNQHdRTaPxn4kYClOyxfP67bd7O0JVbGkqm1xdf7SWQFk7ZgIE+mlrkvPKjN+
OHPoiPxjtFaKWI40hXWxhYUKb5w4d1T6GTmWGvhiQFYWh0qViIK5ZU6edQQ3pN7S5Pb4r8cF
qgayXOBKYRI/hWKYqtfV5flf3kx/cXXm0DcGrWlqAT4mezpRicNjwIDy5SvtpEQCJU3DfVek
h2tGhkOcffq6cYBkTjHr+ZKijP6MPfTR7f0NoZ431bdG7eb3fQ7m0FqEOFCoxj5EJXwW0Atc
Uk0jj2XqF00ctE8mglCgnKz2U2POnWxh6C751W5jwJAhOl4ym12uXOXr2lw7fTnMuvq+4PK8
D8W+usW+6hgglfz+oaQBVag9TQk7A61XU09R8wJHrzG6sxHSPw5R9+F/5gbEGGoUUFIEYDhk
0zfzS5tix3s02H/DYsi+ePcS13jlSuCxIx8JKv1kCNEwEP5hsZ+ZQw065RwxujVqspoYiG+X
jZBxxKFug8/4Qn8fB1LF+GFOKyWBaB6XXAKIdPiNaHol9WbeKd2+MpeUVurFHyin4MaWLb7T
blnDfp8jHo3bdxpsmwBDaJULGAjttkYQKYNDt8mMs00bzBQX29P6OgBjVkuhgJQg9MhAXssj
Bs/bdxM74re/YBlQ0EMD58X0EAmib4ztmWX1s/lQmRVnkVQPpfWgDCPebQinEiPFqzD6HDRK
n7XtBWEkj+4CHkH2OiiMRtZ/f8s702gyKkX0KwYR2iuPQsl5PuD0VHqaRKcPyC+nX5VtZ46i
YDxn5znXpAYPJ9j81CxpOvEDAREmyGUNi7webV3F0YWhLtx1SjgYNBtCGGyBCWW3LxL0RSP6
4df7ZthhwWFuqPD9YXpJ42ojKmOb6mhN/rkdGU8wtrwU1NjLq5SKBClZtg/9cBhCayN/Xnog
VNx2S9IZB7zOMvhq3Re6ut8EhNy4F62E2ugazToFLIo8fDYp/P7NnygDtpWPiYWBUL5ECyVr
e/HiR1KCDPpDinCtlWImpPxriTakKPJ4P1N6hfXd2Rml8tU0s/IEafRalRvD6AJE8HAvfgST
KhBvTQgq4L2fZqoVoob7Pj9rccBeVOCnlTGPMPEHCg/AcWSZa4aogD+RSRMqMNX9mIvh402/
QY0tehtUa6Y9kTfgSxsvhnPe/O15YKBdK4KCVTBzg+iNJh60WWmXljl+NVzkRt0b8QiuvX4u
XjlyQFHZ6zwht5RiF02gLCy0rFPk9jYxRjJ1YoPZZN+X6Cb5cTd6pIm3DxP53jMHhR58kwQ0
8UAiYdnRZ9MQ5Ol1wC5M8F314RC1K/hy2K8jAyCvkkMBNDDXVPoNOchDzjKhK3KGlFg7sF6s
7Fv5/kYfu9gcT3+G3RDnmA6KgmHLKSOJ36pGMbfKeSXm5rKo706LJ/zo5ryxU++AezCDWIH5
fzyYpTZWdUe890OSAlaLmh6IVXKKACiJtzAMV8SE6vwq3Gw/0NiLc4hZN1PBk8C+3sUPuXJX
xX+THo46IpMA1ZLt2HE66YtPzhXgfakjzSagguxBbqP6mmH8UxovGnMoMhSBahSKcWpI5OVT
x5u2lzGuRQ43qfd9/vrADTZY766GPVPsbCBjzv/qA4Lih0YbHrfsfFi2+Kf3kQh1dCMlK/G5
pEyn2zJLyZE8cEG7S+/rBu32p7w1+0vgKrEMSvFDCzN5o1hKuOvLAl60x6YCIhDwDG5Wf8on
X6ZF24sZZifcnpXJa5EIlX4G/DEfizZ+brUkQAOzML7lTUPgMtGugBkjN1+VAWbi4lEXaqVZ
R2O0k7c07FnAHbYpzSsnQtfYpaBZnlLk/KUeY0tyzAhiUariSZlUPs0ob84M9VWJeaNHojeN
0giZZRLzAP8frgyb3rxpJlC2kj5zJl2wjnFgbH/pOMWg4Vwxs5DNPO5fDZ5DiSXz9j0ZHl/b
YvCTg+mDMLN8Cw38xFpHWW2Dn21sc0yUUeJaJfJmOrTemj+AOc4hbbaVW5xHmiKWZrgughej
jMVF+ow0eNKgbjirKjDh8KjSEFiQvmS1Bj3QRQqRsA+YUSn5mLBpJGy9w+ho3kMUF24YIDRQ
G1XP6xtx/xxuiS0swVfeX51Wc0Qim/mVgUvBoN4Yh09BGxqmcecG3VZiHRzuLNSwnVppQ/nO
CFiQcJ8bA4G7kQh6AOyW1JrQQW+GZA/QHtDr+rFkswZO4Lz5o6pusbEPWgM/hyTeHJC1wIfQ
6MGBmppB0P+u+qp2tw4kyu57ZpB9794ct2DWbpDIhng2qkFkMalby+X5vlIHJOu/sVqlRXHp
lnXG2GGsaP7W+Ctp77mSQ+/tRkPYYCMDxunS09e4HtiT77+z4PSDBuNlNEP3D/yyzr+HIHda
E/9PCNnqTXk0czrOzsGGxBpSwFJoU1ciYoK8m0JrA4nwOZJCJeLdQ51zeOgIK4Q=

/
SHOW ERRORS
