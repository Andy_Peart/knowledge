create or replace package body LibPutPrefLocation wrapped 
a000000
369
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
abcd
b
2e61 ed4
0E9Fz9VQ29WUHbkyBrenwS8hXSwwg5WjeSAFV9O8ZB4PNY9ptLx75Q/TSQXfAIVlebanbFyW
oe0PXyGBDTW3V7h2ihxLJPlAINOhzMJVzQaONqfJQYjL+ojAVwldI8e/GLMWdpb3qv6tIzHp
BYGq5NfN17lITuNaJbSq6VJ97lJvC13XS3jQ4HqvUl/GP32+Pr3WYArzhhROXqLhjsnSSZVe
dr+r0zXU8Ww+SjST9wgK9Yju3GpOvcsJy2yvlGgHCCXud+GNR4K24RTpDARZhaAaAsyZd4Uu
7Vi94KIu63WOPPaXV4zdQH+gk+HAJzfyahZtFkajdgEmEs3RpMriwk19VazPZajSHIXlzW+O
eJ3dQ8mjS+kfnnrEPOFvVVAhahGDPSU40ci0gcks60NcfReZVrN2Qxq59ZWI4Xr6zyzjBzc4
iSpuIft0XLgRX0nRgUfJUIohZnjc77H+UifyzP8YA1bA9BICY50J+tV4M8lIaJZpDBhmPosc
DtekaWNbXc6mI4Pez0dxpf71rWqRewzwy/i508bV/yUCmNhO9Us/GpZ8eAu/WHXmZ4fMRdf/
BBJ1oilXoVXpGhJTcDtXcht2h1tX0SiZ7+pfmYYx5Mj6WsElmGzGwF+M9WBEOqaFs5FdRa1T
GwsTdu+xl0PHF+ejG9xf6Azk4NQpt4IKRPhXdcAFBHUkKzdkMteByYmwAgnVyQJYZqzKLZdD
cZi26XCtocmjtHv1N0s5furzvisdRm1xBR3vDLh00Ilo6wCNwCvdo8wZ5ReCzPO5sb3+/uco
/EZbEkauHkau/udPPb1PzfMcPL0o/udPsb1Pu/O8Tba8TfMzyU7l3jkrzVYdnQ/vg2QfpHfj
y/zprvaiO06ZOCr+FX88QxYy6frf86cRj64UoaF8DUl21fph+pj5wwR2S9L7cABeLkgw3QPn
3bRbaFHYquPC+TeXOQcW/XZNitIvAbBbxdraojExN5dABxb9HE2K5C8BwVvFmFeiWCpf+iHn
C3H3hX+HMmVba6ahW3YgYCJhSQGOKvRqREaeminFKCxAkxz/rH9KgMgUnGVxNOA0lAGnAf3N
OXYmaT/15odvSThf8hfoNl7H/ecNj52FdseZPaAtHS9zFFzHGOZNbiuLuGyzpnCYNXKqFq8i
MwmOOtx25rzra7/KrysPM6Y+Kl/SiSTnahGE5OGr9WXoByEi6qSOcTpNRPWcG3StDQVgX8pB
XEQVHdzljVG3Jzu3BAte3muAFChJb6d+5uZfG1etIOZ6iyPjlUgJAXgmS7ocC/KntyfZoY1D
GhXA03VhhCzv4f2F24HOz7OHW6gyTpSHoikJdsp7IRiDFqlzFAH1Nf+UvcxcG/PmniI+vUtQ
lGdSrqTIsE/jSbCyoox4/oem9RhbBnhQgXTGdS51pfL1Pgu94NxgXBVmtm5VAriLvGILOaXp
cEOmKbvdbHs0gUVpXiGhGAd2UAay1yboIQymPUccc9fN/oGcydaj6osHltr7UDJLZxqp0ose
oc6gGRmK37pc9Dl1yKFU+tIKdYyc+Luf1FWMjSKdBc25HJ0JXpVsYm/Z/wcMubeSJ9tx8fa3
2V4iAEAcP/GCPXzlUelu/uWJpQuQJkbMlGwGpRd7l+eYbVzuNY9KpnJRLRPgjtxp3j5lb0q/
sjbM71VbPDzlTI+UpX1TCfu2zO13HaPxFa2rAmvQVabt7aK2sa4WeKGvnbRYraj7fvrfX1Ml
6Rp5sMCWeYGDjrLM5Qs9eQUYRMhXcCuQ0ZoqYDDy4X02iGIiGwt6l+jcjrfKeSrZAvLG+GG8
c9w+Hs2fc2xkD0EOwVXXn7GKDgNhPFZU8dRIyM58k2LcHiG1gbvJsDonPRppvAC2ddRRFvWh
ma3PkoGemQsO9UCvhxsmeYVQb4Eoe6JRy56J0apZA1ep2llc3BEDoPocTL+FFq+h5hef/Afv
sSjv5ctb9eHdQhMpX9uGlA1IvfYafv2NsI0yKowD9DgJ1S/AFceE21yKcbYUGNCjF4HZ0rbr
eEb7EgJ3F+lFBBTr+hnC50gmIbxC41iioKIfAo8AEUaoWX+yFACXXY8KWI4ofgG42Qj+py6U
MKUUWm4+3D7mzsdK3volMt4fFz3801w8Tdm4YQGz3H9yVogLBEfJcri+nsZdUFD+gHyKfXtf
wRayKP0qlfBOvTz9PoiXDsDfThOdBCmbb4N+USS1w3mpSVThl+Z06hxgy09+eCipifybt6xU
DNi2uJPdjmR/6N0iHfAsHKoFxdNgV02Drmlt+MyALRoLdpkRKElXRLiN00vFId9LGMzzmLQU
Rrf3do2dV7Hz2QK0XjQ6VVh+sR4K13PZauzXlCj9Vd+wV0TjOvCxrv4expjBIBgUZdnIFZSH
ubuoJPFn4k+SRjNgrAvrB0mbgVIDXLJlsdXu5vWgm3guL0VrVxQY5BoB0t3xyC02YzlEMZO6
TYJwQiNPwSqD5Wsayyd7pgiy37Vxan2LFHg0dVM/yGJgSyqtFFKt+98nIErGSyvF1T4Pe6pA
s7imSFID2kU49YDMSpIGIRWPDBiV1zM1zcEbiogv+KI+P/gc3rvZXc6IEu74KIM57b9w5dhR
I8tlHjI+Jxke9wj+BKNuvlyZNkMAJZrd5BS2m8QYZpSg8Fk/elYHoGNbm4pMws619okQZpkT
VEr4xOyVA8W8v92jpgmZ/l5vyCH03o6PLYhQwpvDPWAFhXHVX7GWLZ+gRxSNbGHN025P1bKY
TS8gupOW2Opshsq60H8/WGTAgR7gh2V9FpSsSsz2feMBnZAekW9mKirJT0B8tvHnn5CXQStM
O4xsovHHoG7xjqRiulh9u8Q8I9O0EilsbSxvwQG3LG9P+iozUwrtSRyrxukdD4lT606u3lSJ
K86+zAcQedH98mKr09wLB/aMKE0Sdye0rGlwZGih5qoRRqE0bKSw9lxkVOc66qZIFeZJBx8P
nqUS+YtPmtmelhI4OBbovbF/9Lf5yZ3JKpyadHZPacXxDRO33czIrotTFn1261T3uP9omm9I
/KUscgf71E96JP2GfwDUc2854JDTba6gl501mORw1COgBk9B+4DwAcpOe4JjCtUJoAn/N93D
mdd6V0qblgtjTMiuUL2akBujKPL2E7O9Uqv3RwaZeDX3mi30gA2MYLOsH23DDZWMkkNY6FTW
pa6FGnuMyWz6/OMYrpzbtzNc8X4Cqn4aIpQuTZpAiaFzZd5pG5CnoRcBJUtZe5a30Tb6M8Lt
7D1kybLlk7A92AMnPEvsMofXRS/57Zbztw0aZaKJgo3T8c3PcLyJCdnEOcMQlGMX9J8FRt6U
Wd6NCVk2Riuk0cXm7eaQcHNU833II8dIWMEHyEFcjid3BpY7kuUsePwLt5atvdw4CFkoTRKr
3ty+Jd7JeZc8Y9Kp+YMk3z+xzkTioW8wR0Y+3ICICMYUqWu6pXL3PqmxR5uctl9g0zNqmtrk
UQ7sbRMFIL/Q9qORNBLPOh15nVVPfmF4nU/PMRl6AIFuBfiYSVpAnUh4lq7/zdRCOkqS/1CI
0q5AncuQgXNQOobgU6tBUaozacLjaZLCYLeBF3e7a0gfkLJpu3buFOlz0WjnTfmCssJ42M3L
8vn4NLokqWJAlH0j93ESvYo6lihPsbqmbjIvQjRV4iw80Nowir9QPSP4R9UR9Sc0L3P89e4g
ffXaEc8DuiS7SBxBqQXtMOse7SlMDrmPeWT4Yb+DXYbyxlVlGs/mUyRNuTq5LLUdpn8m1Q==


/
SHOW ERRORS
