drop type IntegerArray force
/
create type IntegerArray
as
table of integer
/
SHOW ERRORS
