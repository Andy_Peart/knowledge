drop type StringArray force
/
create type StringArray
as
table of varchar2(60)
/
SHOW ERRORS
