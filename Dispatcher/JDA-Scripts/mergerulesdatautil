#!/bin/sh
################################################################################
#                                                                              #
#  Copyright (c) 2014 JDA Software Group, Inc.                                 #
#  All rights reserved - Company Confidential                                  #
#                                                                              #
################################################################################

################################################################################
#                                                                              #
#       SCRIPT NAME:    mergerulesdatautil                                     #
#                                                                              #
#       DESCRIPTION:    Script for manipulating merge rules - saving/loading.  #
#                                                                              #
#    DATE     BY   PROJ   ID       DESCRIPTION                                 #
#    ======== ==== ====== ======== =============                               #
#    18/05/04 MJH  VV251  NEW7359  Merge rules changes                         #
#    27/12/05 JH   DCS    ENH2000  Merge rules program elseif/exit support     #
#    28/03/07 JH   DCS    ENH3596  Add support for oracle data pump            #
#    05/06/07 JH   DCS    ENH3217  Merge rules by client and site              #
#    02/11/07 JH   DCS    DSP1432  Hide sqlplus passwords                      #
#    08/11/07 JH   DCS    DSP1441  Porting to oracle 11g                       #
#    08/07/09 JH   DCS    DSP1392  RDT screen merge rules                      #
#    18/10/10 RMD  DCS    DSP3379  Add report type rdt screen rules            #
################################################################################

PROG=`basename $0`
TMPFILE="$DCS_TMPDIR/tmpfile.$$"
LOADRULES="FALSE"
SAVERULES="FALSE"
ALLRULES="FALSE"
REQDRULE=""
ORDERRULE="order"
PREADVICERULE="pre_advice"
UPIRECEIPTRULE="upi_receipt"
SKURULE="sku"
SKUCONFIGRULE="sku_config"
ADDRESSRULE="address"
DELIVERYRULE="delivery"
ABTRANSRULE="ab_base_trans"
ABPROCTRANSRULE="ab_processed_trans"
ABPRICTRANSRULE="ab_priced_trans"
RDTSCREENRULE="rdt_screen"
DATAPUMP="FALSE"
SITE=""
CLIENT=""
SITECLIENT="FALSE"

if [ "$MTYPE" = "LINUX" ]
then
        alias echo="echo -e"
fi

usage()
{
	(
	echo ""
	echo "Usage: $PROG [options]"
	echo ""
	echo "[-s]         Save merge rules"
	echo "[-l]         Load merge rules"
	echo "[-z]         Process all types of merge rules"
	echo "[-t type]    Process required type of merge rule"
	echo "[-a]         All merge rules data"
	echo "[-i site     Site merge rules data"
	echo "[-c client]  Client merge rules data"
        echo "[-p]         Enable use of data pump"
	echo ""
	echo "Type & file names used:"
	echo ""

	if [ "$APPLICATION_NAME" = "WMS" ]
	then
		echo " 1  - order                    - merge_rules_order*.dmp/.dmpdp"
		echo " 2  - pre-advice               - merge_rules_pre_advice*.dmp/.dmpdp"
		echo " 3  - upi receipt              - merge_rules_upi_receipt*.dmp/.dmpdp"
		echo " 4  - sku                      - merge_rules_sku*.dmp/.dmpdp"
		echo " 5  - delivery                 - merge_rules_delivery*.dmp/.dmpdp"
		echo " 6  - rdt screen               - merge_rules_rdt_screen*.dmp/.dmpdp"
		echo " 7  - AB transaction           - merge_rules_ab_trans*.dmp/.dmpdp"
		echo " 8  - AB processed transaction - merge_rules_ab_processed_trans*.dmp/.dmpdp"
		echo " 9  - AB priced transaction    - merge_rules_ab_priced_trans*.dmp/.dmpdp"
		echo " 10 - sku config               - merge_rules_sku_config*.dmp/.dmpdp"
		echo " 11 - address                  - merge_rules_address*.dmp/.dmpdp"

	elif [ "$APPLICATION_NAME" = "VV" ]
	then
		echo " 1 - AB transaction           - merge_rules_ab_trans*.dmp/.dmpdp"
		echo " 2 - AB processed transaction - merge_rules_ab_processed_trans*.dmp/.dmpdp"
		echo " 3 - AB priced transaction    - merge_rules_ab_priced_trans*.dmp/.dmpdp"
	fi

	echo ""
	echo "This script should be used with extreme caution - make sure you"
	echo "do a backup before trying to load data into a live system !"
	echo ""
	echo "Note that this script is intended to be used to keep live and"
	echo "test system merge rules up-to-date when people are using the test"
	echo "system to change the merge rules data and test them before"
	echo "moving them to the live system."
	echo ""
	echo "The best way to use it is to use the 'Process all types of merge rules'"
	echo "option and the 'All merge rules data' option and make sure that"
	echo "live and test systems are kept totally synchronised."
	echo ""
	) | more
}

load_merge_rules()
{
	if [ "$ALLRULES" = "TRUE" ]
	then
		if [ "$APPLICATION_NAME" = "WMS" ]
		then
			load_merge_rule $ORDERRULE
			load_merge_rule $PREADVICERULE
			load_merge_rule $UPIRECEIPTRULE
			load_merge_rule $SKURULE
			load_merge_rule $DELIVERYRULE
			load_merge_rule $RDTSCREENRULE
			load_merge_rule $ABTRANSRULE
			load_merge_rule $ABPROCTRANSRULE
			load_merge_rule $ABPRICTRANSRULE
			load_merge_rule $SKUCONFIGRULE
			load_merge_rule $ADDRESSRULE

		elif [ "$APPLICATION_NAME" = "VV" ]
		then
			load_merge_rule $ABTRANSRULE
			load_merge_rule $ABPROCTRANSRULE
			load_merge_rule $ABPRICTRANSRULE
		fi
	else
		load_merge_rule $REQDRULE
	fi
}

save_merge_rules()
{
	if [ "$ALLRULES" = "TRUE" ]
	then
		if [ "$APPLICATION_NAME" = "WMS" ]
		then
			save_merge_rule $ORDERRULE
			save_merge_rule $PREADVICERULE
			save_merge_rule $UPIRECEIPTRULE
			save_merge_rule $SKURULE
			save_merge_rule $DELIVERYRULE
			save_merge_rule $RDTSCREENRULE
			save_merge_rule $ABTRANSRULE
			save_merge_rule $ABPROCTRANSRULE
			save_merge_rule $ABPRICTRANSRULE
			save_merge_rule $SKUCONFIGRULE
			save_merge_rule $ADDRESSRULE

		elif [ "$APPLICATION_NAME" = "VV" ]
		then
			save_merge_rule $ABTRANSRULE
			save_merge_rule $ABPROCTRANSRULE
			save_merge_rule $ABPRICTRANSRULE
		fi
	else
		save_merge_rule $REQDRULE
	fi
}

load_merge_rule()
{
	echo ""
	echo "Loading merge rules data for\c"

	WHERECLAUSE="1 = 1"
	FILENAME="merge_rules_$1"

	if [ "$SITECLIENT" = "TRUE" ]
	then
		echo " all sites/clients..."

		WHERECLAUSE="$WHERECLAUSE"
		FILENAME="${FILENAME}_all"

	else
		if [ "$CLIENT" != "" ]
		then
			echo " client $CLIENT\c"

			WHERECLAUSE="$WHERECLAUSE and Client_ID = '$CLIENT'"
			FILENAME="${FILENAME}_client_$CLIENT"
		fi
	
		if [ "$SITE" != "" ]
		then
			echo " site $SITE\c"

			WHERECLAUSE="$WHERECLAUSE and Site_ID = '$SITE'"
			FILENAME="${FILENAME}_site_$SITE"
		fi

		if [ "$CLIENT" = "" \
		-a "$SITE" = "" ]
		then
			echo " site null client null\c"

			WHERECLAUSE="$WHERECLAUSE and Client_ID is null and Site_ID is null"
			FILENAME="${FILENAME}_client_null_site_null"
		fi

		echo "..."
	fi

	FILENAME=`echo "$FILENAME" | tr "[ ]" "[_]"`
	FILENAME=`ls $FILENAME.* | head -1`

	if [ ! -f "$FILENAME" ]
	then
		echo ""
		echo "File $FILENAME does not exist !"
		echo ""
		exit 1
	fi

        sqlplus -s /nolog << ! > $TMPFILE
		connect $ORACLE_USR
                drop table tmp_$1_cmp $PURGE;
                drop table tmp_$1_mp $PURGE;
                drop table tmp_$1_mr $PURGE;
!

	stripsqlplusconnecteds -i $TMPFILE

        # no error checking - as tables might not exist

	if [ "$1" = "$RDTSCREENRULE" ]
	then
	        sqlplus -s /nolog << ! > $TMPFILE
			connect $ORACLE_USR
	              	drop table tmp_$1_program $PURGE;
	              	drop table tmp_$1_mr_param $PURGE;
!

		stripsqlplusconnecteds -i $TMPFILE

	        # no error checking - as tables might not exist
	fi

	sqlplus -s /nolog << ! > $TMPFILE
		connect $ORACLE_USR

                create table tmp_$1_cmp as
                select * from $1_cmp
		where rownum < 1;
                create table tmp_$1_mp as
                select * from $1_mp
		where rownum < 1;
                create table tmp_$1_mr as
                select * from $1_mr
		where rownum < 1;

		delete from $1_cmp 
		where $WHERECLAUSE;
		delete from $1_mp 
		where $WHERECLAUSE;
		delete from $1_mr 
		where $WHERECLAUSE;
!

	stripsqlplusconnecteds -i $TMPFILE

        check_for_errors "Could not clean merge rules tables for loading merge rules data !"

	if [ "$1" = "$RDTSCREENRULE" ]
	then
		sqlplus -s /nolog << ! > $TMPFILE
			connect $ORACLE_USR

	                create table tmp_$1_program as
	                select * from $1_program
			where rownum < 1;
	                create table tmp_$1_mr_param as
	                select * from $1_mr_param
			where rownum < 1;

			delete from $1_program 
			where $WHERECLAUSE;
			delete from $1_mr_param 
			where $WHERECLAUSE;
!

		stripsqlplusconnecteds -i $TMPFILE

	        check_for_errors "Could not clean merge rules tables for loading merge rules data !"
	fi

	dbload -i $FILENAME

        sqlplus -s /nolog << ! > $TMPFILE
		connect $ORACLE_USR
                insert into $1_cmp
                select * from tmp_$1_cmp;
                insert into $1_mp
                select * from tmp_$1_mp;
                insert into $1_mr
                select * from tmp_$1_mr;
!

	stripsqlplusconnecteds -i $TMPFILE

        check_for_errors "Could not move temporary table data into proper merge rules data tables !"

	if [ "$1" = "$RDTSCREENRULE" ]
	then
	        sqlplus -s /nolog << ! > $TMPFILE
			connect $ORACLE_USR
	                insert into $1_program
	                select * from tmp_$1_program;
	                insert into $1_mr_param
	                select * from tmp_$1_mr_param;
!

		stripsqlplusconnecteds -i $TMPFILE

	        check_for_errors "Could not move temporary table data into proper merge rules data tables !"
	fi

        sqlplus -s /nolog << ! > $TMPFILE
		connect $ORACLE_USR
                drop table tmp_$1_cmp $PURGE;
                drop table tmp_$1_mp $PURGE;
                drop table tmp_$1_mr $PURGE;
!

	stripsqlplusconnecteds -i $TMPFILE

        check_for_errors "Could not drop temporary tables !"

	if [ "$1" = "$RDTSCREENRULE" ]
	then
	        sqlplus -s /nolog << ! > $TMPFILE
			connect $ORACLE_USR
	                drop table tmp_$1_program $PURGE;
	                drop table tmp_$1_mr_param $PURGE;
!

		stripsqlplusconnecteds -i $TMPFILE

	        check_for_errors "Could not drop temporary tables !"
	fi

	echo ""
}

save_merge_rule()
{
        if [ "$DATAPUMP" = "TRUE" ]
        then
                DATAPUMPOPT="-p"
        else
                DATAPUMPOPT=""
        fi

	echo ""
	echo "Saving merge rules data for\c"

	WHERECLAUSE="1 = 1"
	FILENAME="merge_rules_$1"

	if [ "$SITECLIENT" = "TRUE" ]
	then
		echo " all sites/clients..."

		WHERECLAUSE="$WHERECLAUSE"
		FILENAME="${FILENAME}_all"

	else
		if [ "$CLIENT" != "" ]
		then
			echo " client $CLIENT\c"

			WHERECLAUSE="$WHERECLAUSE and Client_ID = '$CLIENT'"
			FILENAME="${FILENAME}_client_$CLIENT"
		fi
	
		if [ "$SITE" != "" ]
		then
			echo " site $SITE\c"

			WHERECLAUSE="$WHERECLAUSE and Site_ID = '$SITE'"
			FILENAME="${FILENAME}_site_$SITE"
		fi

		if [ "$CLIENT" = "" \
		-a "$SITE" = "" ]
		then
			echo " site null client null..."
			echo ""
			echo "Warning - this is not necessarily going to dump all of the records, just those  with a null client and site - to make sure you dump them all use the -a option !"

			WHERECLAUSE="$WHERECLAUSE and Client_ID is null and Site_ID is null"
			FILENAME="${FILENAME}_client_null_site_null"
		else
			echo "..."
		fi
	fi

	FILENAME=`echo "$FILENAME" | tr "[ ]" "[_]"`

        sqlplus -s /nolog << ! > $TMPFILE
		connect $ORACLE_USR
                drop table tmp_$1_cmp $PURGE;
                drop table tmp_$1_mp $PURGE;
                drop table tmp_$1_mr $PURGE;
!

	stripsqlplusconnecteds -i $TMPFILE

        # no error checking - as tables might not exist

	if [ "$1" = "$RDTSCREENRULE" ]
	then
	        sqlplus -s /nolog << ! > $TMPFILE
			connect $ORACLE_USR
	                drop table tmp_$1_program $PURGE;
	                drop table tmp_$1_mr_param $PURGE;
!

		stripsqlplusconnecteds -i $TMPFILE

	        # no error checking - as tables might not exist
	fi

        sqlplus -s /nolog << ! > $TMPFILE
		connect $ORACLE_USR
                create table tmp_$1_cmp as
                select * from $1_cmp
                where $WHERECLAUSE;
                create table tmp_$1_mp as
                select * from $1_mp
                where $WHERECLAUSE;
                create table tmp_$1_mr as
                select * from $1_mr
                where $WHERECLAUSE;
!

	stripsqlplusconnecteds -i $TMPFILE

        check_for_errors "Could not setup temporary tables for saving merge rules data !"

	if [ "$1" = "$RDTSCREENRULE" ]
	then
	        sqlplus -s /nolog << ! > $TMPFILE
			connect $ORACLE_USR
	                create table tmp_$1_program as
	                select * from $1_program
	                where $WHERECLAUSE;
	                create table tmp_$1_mr_param as
	                select * from $1_mr_param
	                where $WHERECLAUSE;
!

		stripsqlplusconnecteds -i $TMPFILE

	        check_for_errors "Could not setup temporary tables for saving merge rules data !"
	fi

	if [ "$1" = "$RDTSCREENRULE" ]
	then
		dbdump -a $DATAPUMPOPT -s $FILENAME -i "('tmp_$1_cmp','tmp_$1_mp','tmp_$1_mr','tmp_$1_program','tmp_$1_mr_param')" -r 
	else
		dbdump -a $DATAPUMPOPT -s $FILENAME -i "('tmp_$1_cmp','tmp_$1_mp','tmp_$1_mr')" -r 
	fi

        sqlplus -s /nolog << ! > $TMPFILE
		connect $ORACLE_USR
                drop table tmp_$1_cmp $PURGE;
                drop table tmp_$1_mp $PURGE;
                drop table tmp_$1_mr $PURGE;
!

	stripsqlplusconnecteds -i $TMPFILE

        check_for_errors "Could not drop temporary tables !"

	if [ "$1" = "$RDTSCREENRULE" ]
	then
	        sqlplus -s /nolog << ! > $TMPFILE
			connect $ORACLE_USR
	                drop table tmp_$1_program $PURGE;
	                drop table tmp_$1_mr_param $PURGE;
!

		stripsqlplusconnecteds -i $TMPFILE

      	  	check_for_errors "Could not drop temporary tables !"
	fi

	echo ""
}

check_for_errors()
{
        FOUND=`grep ORA- $TMPFILE`

        if [ "$FOUND" != "" ]
        then
                echo ""
                echo "$1"
                echo ""
                cat $TMPFILE
                echo ""
                exit 1
        fi
}

check_site_exists()
{
	if [ "$SITE" = "" \
	-o "$SITECLIENT" = "TRUE" ]
	then
		return
	fi

        SITE=`echo "$SITE" | tr "[a-z]" "[A-Z]"`

        sqlplus -s /nolog << ! > $TMPFILE
		connect $ORACLE_USR

                set echo on
                set feedback off
                set heading off
                set pagesize 0

                select site_id
                from site
                where site_id = '$SITE';
!

	stripsqlplusconnecteds -i $TMPFILE

        check_for_errors "Problem checking site exists !"

        FOUND=`grep "$SITE" $TMPFILE`

        if [ "$FOUND" = "" ]
        then
                echo ""
                echo "The specified site $SITE does not exist !"
                echo ""
                exit 1
        fi
}

check_client_exists()
{
	if [ "$CLIENT" = "" \
	-o "$SITECLIENT" = "TRUE" ]
	then
		return
	fi

        CLIENT=`echo "$CLIENT" | tr "[a-z]" "[A-Z]"`

        sqlplus -s /nolog << ! > $TMPFILE
		connect $ORACLE_USR

                set echo on
                set feedback off
                set heading off
                set pagesize 0

                select client_id
                from client
                where client_id = '$CLIENT';
!

	stripsqlplusconnecteds -i $TMPFILE

        check_for_errors "Problem checking client exists !"

        FOUND=`grep "$CLIENT" $TMPFILE`

        if [ "$FOUND" = "" ]
        then
                echo ""
                echo "The specified client $CLIENT does not exist !"
                echo ""
                exit 1
        fi
}

initialise()
{
	if [ "$ORACLE_SID" = "" ]
	then
		echo ""
		echo "$PROG: ERROR - \$ORACLE_SID not set to database name !"
		echo ""
		exit 1
	fi

        if [ ! -d "$ORACLE_HOME" ]
        then
                echo ""
                echo "$PROG: ERROR - \$ORACLE_HOME not set to valid directory !"
                echo ""
                exit 1
        fi

	if [ "$ORACLE_USR" = "" ]
	then
		echo ""
		echo "$PROG: ERROR - \$ORACLE_USR not set to user name !"
		echo ""
		exit 1
	fi

        if [ "$ORACLEVERSION" -ge "10" ]
        then
                PURGE="purge"
        else
                PURGE=""
        fi
}

cleanup()
{
	rm -f $TMPFILE
}

#
# Main program
#

initialise

while getopts lsat:pi:c:z? 2> /dev/null ARG
do
	case $ARG in
		l)	LOADRULES="TRUE";;

		s)	SAVERULES="TRUE";;

		z)	ALLRULES="TRUE";;

		t)	REQDRULE=$OPTARG;;

                p)      DATAPUMP="TRUE";;

		i)	SITE=$OPTARG;;

		c)	CLIENT=$OPTARG;;

		a)	SITECLIENT="TRUE";;

		?)	usage
			exit 1;;
	esac
done

if [ "$ALLRULES" != "TRUE" ]
then
	if [ "$APPLICATION_NAME" = "WMS" ]
	then
		if [ "$REQDRULE" != "1" \
		-a "$REQDRULE" != "2" \
		-a "$REQDRULE" != "3" \
		-a "$REQDRULE" != "4" \
		-a "$REQDRULE" != "5" \
		-a "$REQDRULE" != "6" \
		-a "$REQDRULE" != "7" \
		-a "$REQDRULE" != "8" \
		-a "$REQDRULE" != "9" \
		-a "$REQDRULE" != "10" \
		-a "$REQDRULE" != "11" ]
		then
			echo ""
			echo "You must specify either the -z or -t options !"
			usage
			exit 1
		fi

	elif [ "$APPLICATION_NAME" = "VV" ]
	then
		if [ "$REQDRULE" != "1" \
		-a "$REQDRULE" != "2" \
		-a "$REQDRULE" != "3" ]
		then
			echo ""
			echo "You must specify either the -z or -t options !"
			usage
			exit 1
		fi
	fi

	if [ "$APPLICATION_NAME" = "WMS" ]
	then
		if [ "$REQDRULE" = "1" ]
		then
			REQDRULE="$ORDERRULE"
		fi

		if [ "$REQDRULE" = "2" ]
		then
			REQDRULE="$PREADVICERULE"
		fi

		if [ "$REQDRULE" = "3" ]
		then
			REQDRULE="$UPIRECEIPTRULE"
		fi

		if [ "$REQDRULE" = "4" ]
		then
			REQDRULE="$SKURULE"
		fi

		if [ "$REQDRULE" = "5" ]
		then
			REQDRULE="$DELIVERYRULE"
		fi

		if [ "$REQDRULE" = "6" ]
		then
			REQDRULE="$RDTSCREENRULE"
		fi

		if [ "$REQDRULE" = "7" ]
		then
			REQDRULE="$ABTRANSRULE"
		fi
		
		if [ "$REQDRULE" = "8" ]
		then
			REQDRULE="$ABPROCTRANSRULE"
		fi
		
		if [ "$REQDRULE" = "9" ]
		then
			REQDRULE="$ABPRICTRANSRULE"
		fi

		if [ "$REQDRULE" = "10" ]
		then
			REQDRULE="$SKUCONFIGRULE"
		fi

		if [ "$REQDRULE" = "11" ]
		then
			REQDRULE="$ADDRESSRULE"
		fi

	elif [ "$APPLICATION_NAME" = "VV" ]
	then
		if [ "$REQDRULE" = "1" ]
		then
			REQDRULE="$ABTRANSRULE"
		fi

		if [ "$REQDRULE" = "2" ]
		then
			REQDRULE="$ABPROCTRANSRULE"
		fi
		
		if [ "$REQDRULE" = "3" ]
		then
			REQDRULE="$ABPRICTRANSRULE"
		fi
	fi
fi

check_site_exists

check_client_exists

if [ "$LOADRULES" = "TRUE" ]
then
	load_merge_rules

elif [ "$SAVERULES" = "TRUE" ]
then
	save_merge_rules

else
	echo ""
	echo "You must specify either the -l or -s options !"
	usage
	exit 1
fi

cleanup

exit 0

