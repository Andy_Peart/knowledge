#!/bin/sh
################################################################################
#                                                                              #
#  Copyright (c) 2014 JDA Software Group, Inc.                                 #
#  All rights reserved - Company Confidential                                  #
#                                                                              #
################################################################################

################################################################################
#                                                                              #
#       SCRIPT NAME:    schedulerdatautil                                      #
#                                                                              #
#       DESCRIPTION:    Script for manipulating scheduler data -               #
#			saving/loading.  				       #
#                                                                              #
#    DATE     BY   PROJ   ID       DESCRIPTION                                 #
#    ======== ==== ====== ======== =============                               #
################################################################################

PROG=`basename $0`
TMPFILE="$DCS_TMPDIR/tmpfile.$$"
LOADDATA="FALSE"
SAVEDATA="FALSE"
ALLDATA="TRUE"
ALLDATAFILE="scheduler_data_all"
DATAPUMP="FALSE"
SYNCHRONISE="FALSE"

if [ "$MTYPE" = "LINUX" ]
then
        alias echo="echo -e"
fi

usage()
{
	(
	echo ""
	echo "Usage: $PROG [options]"
	echo ""
	echo "[-s]        Save scheduler data"
	echo "[-l]        Load scheduler data"
        echo "[-p]        Enable use of data pump"
	echo "[-y]        Synchronise data with scheduler"
	echo ""
	echo "File names used:"
	echo ""
	echo " all - scheduler_data_all.dmp/.dmpdp"
	echo ""
	echo "This script should be used with extreme caution - make sure you"
	echo "do a backup before trying to load data into a live system !"
	echo ""
	echo "Note that this script is intended to be used to keep live and"
	echo "test system scheduler data up-to-date when people are using the test"
	echo "system to change the scheduler data and test it before"
	echo "moving it to the live system."
	echo ""
	) | more
}

load_scheduler_data()
{
	load_scheduler_data_all
}

save_scheduler_data()
{
        if [ "$DATAPUMP" = "TRUE" ]
        then
                DATAPUMPOPT="-p"
        else
                DATAPUMPOPT=""
        fi

	save_scheduler_data_all
}

check_for_errors()
{
        FOUND=`grep ORA- $TMPFILE`

        if [ "$FOUND" != "" ]
        then
                echo ""
                echo "$1"
                echo ""
                cat $TMPFILE
                echo ""
                exit 1
        fi
}

save_scheduler_data_all()
{
	echo ""
	echo "Saving scheduler data for all scheduler tables..."

	dbdump -a $DATAPUMPOPT -s $ALLDATAFILE -i "('scheduler_program','scheduler_schedule','scheduler_job')" -r
}

load_scheduler_data_all()
{
	echo ""
	echo "Loading scheduler data for all scheduler tables..."

	ALLDATAFILE=`ls $ALLDATAFILE.* | head -1`

	if [ ! -f "$ALLDATAFILE" ]
	then
		echo ""
		echo "File $ALLDATAFILE does not exist !"
		echo ""
		exit 1
	fi

	echo ""

	sqlplus -s /nolog << !
		connect $ORACLE_USR
		set serveroutput on
		declare
			cursor jobsearch
			is 
			select job_name
			from scheduler_job;
			cursor schedulesearch
			is 
			select schedule_name
			from scheduler_schedule;
			cursor programsearch
			is 
			select program_name
			from scheduler_program;
			result integer;
		begin
			for jobrow in jobsearch loop
				if (LibDbmsScheduler.JobExists(jobrow.job_name) = true) then
					dbms_output.put_line('Dropping job ' || jobrow.job_name);
					LibDBMSScheduler.DropJob(result, jobrow.job_name);
				end if;
			end loop;
			for schedulerow in schedulesearch loop
				if (LibDbmsScheduler.ScheduleExists(schedulerow.schedule_name) = true) then
					dbms_output.put_line('Dropping schedule ' || schedulerow.schedule_name);
					LibDBMSScheduler.DropSchedule(result, schedulerow.schedule_name);
				end if;
			end loop;
			for programrow in programsearch loop
				if (LibDbmsScheduler.ProgramExists(programrow.program_name) = true) then
					dbms_output.put_line('Dropping program ' || programrow.program_name);
					LibDBMSScheduler.DropProgram(result, programrow.program_name);
				end if;
			end loop;
		end;
/
!

	sqlplus -s /nolog << ! > $TMPFILE
		connect $ORACLE_USR
		alter   table   Scheduler_Job
                drop constraint fk_SJ_Program_Name;
		alter   table   Scheduler_Job
                drop constraint fk_SJ_Schedule_Name;
		delete from scheduler_job;
		delete from scheduler_schedule;
		delete from scheduler_program;
!

	stripsqlplusconnecteds -i $TMPFILE

	check_for_errors "Could not clean scheduler tables for loading all scheduler data !"

	dbload -i $ALLDATAFILE

	sqlplus -s /nolog << ! > $TMPFILE
		connect $ORACLE_USR
		alter   table   Scheduler_Job
                add constraint fk_SJ_Program_Name
                foreign key (Program_Name)
                references Scheduler_Program (Program_Name)
                on delete cascade;
		alter   table   Scheduler_Job
                add constraint fk_SJ_Schedule_Name
                foreign key (Schedule_Name)
                references Scheduler_Schedule (Schedule_Name)
                on delete cascade;
!

	stripsqlplusconnecteds -i $TMPFILE

	check_for_errors "Could not add scheduler foreign references back again !"

	if [ "$SYNCHRONISE" = "TRUE" ]
	then
		echo ""
		echo "Running schedulerdae to synchronise scheduler data with oracle dbms scheduler...\c"
	
		schedulerdae -1 -f -d
	
		echo "done"
	else
		echo ""
		echo "By default unless you use the -y option the scheduler data is not synchronised"
		echo "with the oracle dbms scheduler.  You can either run schedulerdae with the relevant"
		echo "options to do this for all the scheduler data or use the relevant maintenance"
		echo "screens to update each record of scheduler data you actually want to run."
	fi

	echo ""
}

initialise()
{
	if [ "$ORACLE_SID" = "" ]
	then
		echo ""
		echo "$PROG: ERROR - \$ORACLE_SID not set to database name !"
		echo ""
		exit 1
	fi

        if [ ! -d "$ORACLE_HOME" ]
        then
                echo ""
                echo "$PROG: ERROR - \$ORACLE_HOME not set to valid directory !"
                echo ""
                exit 1
        fi

	if [ "$ORACLE_USR" = "" ]
	then
		echo ""
		echo "$PROG: ERROR - \$ORACLE_USR not set to user name !"
		echo ""
		exit 1
	fi

	if [ "$ORACLEVERSION" -ge "10" ]
	then
		PURGE="purge"
	else
		PURGE=""
	fi
}

cleanup()
{
	rm -f $TMPFILE
}

#
# Main program
#

initialise

while getopts slpy? 2> /dev/null ARG
do
	case $ARG in
		l)	LOADDATA="TRUE";;

		s)	SAVEDATA="TRUE";;

                p)      DATAPUMP="TRUE";;

		y)	SYNCHRONISE="TRUE";;

		?)	usage
			exit 1;;
	esac
done

if [ "$LOADDATA" = "TRUE" ]
then
	load_scheduler_data

elif [ "$SAVEDATA" = "TRUE" ]
then
	save_scheduler_data

else
	echo ""
	echo "You must specify either the -l or -s options !"
	usage
	exit 1
fi

cleanup

exit 0

