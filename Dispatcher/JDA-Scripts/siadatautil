#!/bin/sh
################################################################################
#                                                                              #
#  Copyright (c) 2014 JDA Software Group, Inc.                                 #
#  All rights reserved - Company Confidential                                  #
#                                                                              #
################################################################################

################################################################################
#                                                                              #
#       SCRIPT NAME:    siadatautil                                            #
#                                                                              #
#       DESCRIPTION:    Script for manipulating sia data -                     #
#			saving/loading.  				       #
#                                                                              #
#    DATE     BY   PROJ   ID       DESCRIPTION                                 #
#    ======== ==== ====== ======== =============                               #
#    04/08/08 JH   DCS    DSP1372  Sia data in the database                    #
#    11/09/08 JH   DCS    DSP2002  Allow multiple sia's                        #
################################################################################

PROG=`basename $0`
TMPFILE="$DCS_TMPDIR/tmpfile.$$"
LOADDATA="FALSE"
SAVEDATA="FALSE"
ALLDATA="FALSE"
SIAIDDATA=""
ALLDATAFILE="sia_data_all"
SIAIDDATAFILES="sia_data_siaid"
DATAPUMP="FALSE"
DATESTAMP="FALSE"
DATESTAMPDATA=""

if [ "$MTYPE" = "LINUX" ]
then
        alias echo="echo -e"
fi

usage()
{
	(
	echo ""
	echo "Usage: $PROG [options]"
	echo ""
	echo "[-s]        Save sia data"
	echo "[-l]        Load sia data"
	echo "[-a]        Process all sia id data"
	echo "[-i siaid]  Process sia id data"
        echo "[-p]        Enable use of data pump"
	echo "[-d]        Save/load date stamp file"
	echo ""
	echo "File names used:"
	echo ""
	echo " all   - sia_data_all.dmp/.dmpdp"
	echo " siaid - sia_data_siaid_<siaid>.dmp/.dmpdp"
	echo ""
	echo "This script should be used with extreme caution - make sure you"
	echo "do a backup before trying to load data into a live system !"
	echo ""
	echo "Note that this script is intended to be used to keep live and"
	echo "test system sia data up-to-date when people are using the test"
	echo "system to change the sia data and test it before"
	echo "moving it to the live system."
	echo ""
	echo "The best way to use it is to use the 'Process all sia id"
	echo "data' option and make sure that live and test systems are kept"
	echo "totally synchronised."
	echo ""
	) | more
}

load_sia_data()
{
	if [ "$ALLDATA" = "TRUE" ]
	then
		load_sia_data_all
	else	
		load_sia_data_siaid
	fi
}

save_sia_data()
{
        if [ "$DATAPUMP" = "TRUE" ]
        then
                DATAPUMPOPT="-p"
        else
                DATAPUMPOPT=""
        fi

	if [ "$ALLDATA" = "TRUE" ]
	then
		save_sia_data_all
	else	
		save_sia_data_siaid
	fi
}

check_for_errors()
{
        FOUND=`grep ORA- $TMPFILE`

        if [ "$FOUND" != "" ]
        then
                echo ""
                echo "$1"
                echo ""
                cat $TMPFILE
                echo ""
                exit 1
        fi
}

save_sia_data_all()
{
	echo ""
	echo "Saving sia data for all sia ids..."

	dbdump -a $DATAPUMPOPT -s $ALLDATAFILE$DATESTAMPDATA -i "('sia_global_environment','sia_group','sia_server','sia_identifier','sia_process','sia_process_environment')" -r
}

save_sia_data_siaid()
{
	echo ""
	echo "Saving sia data for siaid $SIAIDDATA..."

	FILENAME="${SIAIDDATAFILES}_$SIAIDDATA$DATESTAMPDATA"

	sqlplus -s /nolog << ! > $TMPFILE
		connect $ORACLE_USR
		drop table tmp_sia_global_environment $PURGE;
		drop table tmp_sia_group $PURGE;
		drop table tmp_sia_server $PURGE;
		drop table tmp_sia_identifier $PURGE;
		drop table tmp_sia_process $PURGE;
		drop table tmp_sia_process_environment $PURGE;
!

	stripsqlplusconnecteds -i $TMPFILE

	# no error checking - as tables might not exist

	sqlplus -s /nolog << ! > $TMPFILE
		connect $ORACLE_USR
		create table tmp_sia_global_environment as
		select * from sia_global_environment
		where sia_id = '$SIAIDDATA'
		or (sia_id is null and '$SIAIDDATA' is null);
		create table tmp_sia_group as
		select * from sia_group
		where sia_id = '$SIAIDDATA'
		or (sia_id is null and '$SIAIDDATA' is null);
		create table tmp_sia_server as
		select * from sia_server
		where running_sia = '$SIAIDDATA'
		or (running_sia is null and '$SIAIDDATA' is null);
		create table tmp_sia_identifier as
		select * from sia_identifier
		where sia_id = '$SIAIDDATA'
		or (sia_id is null and '$SIAIDDATA' is null);
		create table tmp_sia_process as
		select * from sia_process
		where sia_id = '$SIAIDDATA'
		or (sia_id is null and '$SIAIDDATA' is null);
		create table tmp_sia_process_environment as
		select * from sia_process_environment
		where sia_id = '$SIAIDDATA'
		or (sia_id is null and '$SIAIDDATA' is null);
!

	stripsqlplusconnecteds -i $TMPFILE

     	check_for_errors "Could not setup temporary tables for saving sia id specific sia data !"

	dbdump -a $DATAPUMPOPT -s $FILENAME -i "('tmp_sia_global_environment','tmp_sia_group','tmp_sia_server','tmp_sia_identifier','tmp_sia_process','tmp_sia_process_environment')" -r

	sqlplus -s /nolog << ! > $TMPFILE
		connect $ORACLE_USR
		drop table tmp_sia_global_environment $PURGE;
		drop table tmp_sia_group $PURGE;
		drop table tmp_sia_server $PURGE;
		drop table tmp_sia_identifier $PURGE;
		drop table tmp_sia_process $PURGE;
		drop table tmp_sia_process_environment $PURGE;
!

	stripsqlplusconnecteds -i $TMPFILE

	check_for_errors "Could not drop temporary tables !"
}

load_sia_data_all()
{
	echo ""
	echo "Loading sia data for all sia ids..."

	ALLDATAFILE=`ls $ALLDATAFILE$DATESTAMPDATA.* | head -1`

	if [ ! -f "$ALLDATAFILE" ]
	then
		echo ""
		echo "File $ALLDATAFILE does not exist !"
		echo ""
		exit 1
	fi

	sqlplus -s /nolog << ! > $TMPFILE
		connect $ORACLE_USR
		truncate table sia_global_environment;
		truncate table sia_group;
		truncate table sia_server;
		truncate table sia_identifier;
		truncate table sia_process;
		truncate table sia_process_environment;
!

	stripsqlplusconnecteds -i $TMPFILE

	check_for_errors "Could not clean sia tables for loading all sia data !"

	dbload -i $ALLDATAFILE

	echo ""
}

load_sia_data_siaid()
{
	FILENAME="${SIAIDDATAFILES}_$SIAIDDATA$SIAIDDATAFILEE"

	FILENAME=`ls $FILENAME$DATESTAMPDATA.* | head -1`

	if [ ! -f "$FILENAME" ]
	then
		echo ""
		echo "File $FILENAME does not exist !"
		echo ""
		exit 1
	fi

	echo ""
	echo "Loading sia data for sia id $SIAIDDATA..."

	sqlplus -s /nolog << ! > $TMPFILE
		connect $ORACLE_USR
		drop table tmp_sia_global_environment $PURGE;
		drop table tmp_sia_group $PURGE;
		drop table tmp_sia_server $PURGE;
		drop table tmp_sia_identifier $PURGE;
		drop table tmp_sia_process $PURGE;
		drop table tmp_sia_process_environment $PURGE;
!

	stripsqlplusconnecteds -i $TMPFILE

	# no error checking - as tables might not exist

	sqlplus -s /nolog << ! > $TMPFILE
		connect $ORACLE_USR
		create table tmp_sia_global_environment as
		select * from sia_global_environment
		where rownum < 1;
		create table tmp_sia_group as
		select * from sia_group
		where rownum < 1;
		create table tmp_sia_server as
		select * from sia_server
		where rownum < 1;
		create table tmp_sia_identifier as
		select * from sia_identifier
		where rownum < 1;
		create table tmp_sia_process as
		select * from sia_process
		where rownum < 1;
		create table tmp_sia_process_environment as
		select * from sia_process_environment
		where rownum < 1;

		delete from sia_global_environment
		where sia_id = '$SIAIDDATA'
		or (sia_id is null and '$SIAIDDATA' is null);
		delete from sia_group
		where sia_id = '$SIAIDDATA'
		or (sia_id is null and '$SIAIDDATA' is null);
		delete from sia_server
		where running_sia = '$SIAIDDATA'
		or (running_sia is null and '$SIAIDDATA' is null);
		delete from sia_identifier
		where sia_id = '$SIAIDDATA'
		or (sia_id is null and '$SIAIDDATA' is null);
		delete from sia_process
		where sia_id = '$SIAIDDATA'
		or (sia_id is null and '$SIAIDDATA' is null);
		delete from sia_process_environment
		where sia_id = '$SIAIDDATA'
		or (sia_id is null and '$SIAIDDATA' is null);
!

	stripsqlplusconnecteds -i $TMPFILE

	check_for_errors "Could not clean sia tables for loading sia id specific sia data !"

	dbload -i $FILENAME

	echo ""
	echo "Moving data to proper tables...\c"

	sqlplus -s /nolog << ! > $TMPFILE
		connect $ORACLE_USR
		insert into sia_global_environment
		select * from tmp_sia_global_environment;
		insert into sia_group
		select * from tmp_sia_group;
		insert into sia_server
		select * from tmp_sia_server;
		insert into sia_identifier
		select * from tmp_sia_identifier;
		insert into sia_process
		select * from tmp_sia_process;
		insert into sia_process_environment
		select * from tmp_sia_process_environment;
!

	stripsqlplusconnecteds -i $TMPFILE

	check_for_errors "Could not move temporary table data into proper sia data tables !"

	echo "done"

	sqlplus -s /nolog << ! > $TMPFILE
		connect $ORACLE_USR
		drop table tmp_sia_global_environment $PURGE;
		drop table tmp_sia_group $PURGE;
		drop table tmp_sia_server $PURGE;
		drop table tmp_sia_identifier $PURGE;
		drop table tmp_sia_process $PURGE;
		drop table tmp_sia_process_environment $PURGE;
!

	stripsqlplusconnecteds -i $TMPFILE

	check_for_errors "Could not drop temporary tables !"

	echo ""
}

check_siaid_exists()
{
	SIAIDDATA=`echo $SIAIDDATA | tr "[a-z]" "[A-Z]"`

	sqlplus -s /nolog << ! > $TMPFILE
		connect $ORACLE_USR

                set echo on
                set feedback off
                set heading off
                set pagesize 0

		select sia_id
		from sia_identifier
		where sia_id = '$SIAIDDATA';
!

	stripsqlplusconnecteds -i $TMPFILE

	check_for_errors "Problem checking sia id exists !"

	FOUND=`grep $SIAIDDATA $TMPFILE`

	if [ "$FOUND" = "" ]
	then
		echo ""
		echo "The specified sia id $SIAIDDATA does not exist !"
		echo ""
		exit 1
	fi
}

initialise()
{
	if [ "$ORACLE_SID" = "" ]
	then
		echo ""
		echo "$PROG: ERROR - \$ORACLE_SID not set to database name !"
		echo ""
		exit 1
	fi

        if [ ! -d "$ORACLE_HOME" ]
        then
                echo ""
                echo "$PROG: ERROR - \$ORACLE_HOME not set to valid directory !"
                echo ""
                exit 1
        fi

	if [ "$ORACLE_USR" = "" ]
	then
		echo ""
		echo "$PROG: ERROR - \$ORACLE_USR not set to user name !"
		echo ""
		exit 1
	fi

	if [ "$ORACLEVERSION" -ge "10" ]
	then
		PURGE="purge"
	else
		PURGE=""
	fi
}

cleanup()
{
	rm -f $TMPFILE
}

#
# Main program
#

initialise

while getopts slai:pd? 2> /dev/null ARG
do
	case $ARG in
		l)	LOADDATA="TRUE";;

		s)	SAVEDATA="TRUE";;

		a)	ALLDATA="TRUE";;

		i)	SIAIDDATA=$OPTARG;;

                p)      DATAPUMP="TRUE";;

		d)	DATESTAMP="TRUE";;

		?)	usage
			exit 1;;
	esac
done

if [ "$DATESTAMP" = "TRUE" ]
then
	DATESTAMPDATA="_`date '+%d%m%y_%H%M%S'`"
fi

if [ "$SIAIDDATA" != "" ]
then
	check_siaid_exists
fi

if [ "$LOADDATA" = "TRUE" ]
then
	if [ "$SIA_ID" = "$SIAIDDATA" ]
	then
		sia -u

		if [ "$?" = "0" ]
		then
		        echo ""
		        echo "The application is currently running !"
		        echo ""
		        exit 1
		fi
	fi

	if [ "$DATESTAMP" = "TRUE" ]
	then
		echo ""
		echo "Enter the datestamp of the file you want to load \c"
		read DATESTAMPDATA
		echo ""
		DATESTAMPDATA="_$DATESTAMPDATA"
	fi

	load_sia_data

elif [ "$SAVEDATA" = "TRUE" ]
then
	save_sia_data

else
	echo ""
	echo "You must specify either the -l or -s options !"
	usage
	exit 1
fi

cleanup

exit 0

