<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="invsumbyskudate" pageWidth="842" pageHeight="595" orientation="Landscape" whenNoDataType="AllSectionsNoDetail" columnWidth="782" leftMargin="30" rightMargin="30" topMargin="20" bottomMargin="20">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="language.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<import value="oracle.sql.*"/>
	<template><![CDATA[$P{JR_TEMPLATE_DIR} + "/default_styles.jrtx"]]></template>
	<style name="Arial Unicode" isDefault="true" fontName="Arial Unicode MS" fontSize="12" isUnderline="false"/>
	<style name="Arial" fontName="Arial" fontSize="12" isUnderline="false"/>
	<parameter name="site_id" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<parameter name="site_group" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<parameter name="JR_TEMPLATE_DIR" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["Q:/gittrunkrepos/dispatcher/server/run/jrtemplates"]]></defaultValueExpression>
	</parameter>
	<parameter name="JR_LANGUAGE" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["EN_GB"]]></defaultValueExpression>
	</parameter>
	<parameter name="JR_TIME_ZONE_NAME" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["Europe/London"]]></defaultValueExpression>
	</parameter>
	<parameter name="owner_id" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<parameter name="client_id" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<parameter name="client_group" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<parameter name="sku_id" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<parameter name="fromdate" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<parameter name="todate" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT  I.Site_ID "site_id", 
	I.Client_ID "client_id",
        I.Owner_ID "owner_id",
        I.SKU_ID "sku_id",
	LibSKU.GetSKUDescription (I.Client_ID, I.SKU_ID, $P{JR_LANGUAGE}) "description", 
        CC.Notes "notes",
        MIN (SC.Config_ID) "config_id",
        COUNT (I.SKU_ID) "pallets",
        SUM (I.Qty_On_Hand) + SUM(I.Qty_Allocated) "quantity",
        COUNT (I.SKU_ID) * NVL (MIN (SC.Ratio_1_to_2), 1) *
                           NVL (MIN (SC.Ratio_2_to_3), 1) *
                           NVL (MIN (SC.Ratio_3_to_4), 1) *
                           NVL (MIN (SC.Ratio_4_to_5), 1) *
                           NVL (MIN (SC.Ratio_5_to_6), 1) *
                           NVL (MIN (SC.Ratio_6_to_7), 1) *
                           NVL (MIN (SC.Ratio_7_to_8), 1) "control",
        COUNT (I.SKU_ID) * NVL (MIN(SC.Ratio_1_to_2), 1) *
                           NVL (MIN (SC.Ratio_2_to_3), 1) *
                           NVL (MIN (SC.Ratio_3_to_4), 1) *
                           NVL (MIN (SC.Ratio_4_to_5), 1) *
                           NVL (MIN (SC.Ratio_5_to_6), 1) *
                           NVL (MIN (SC.Ratio_6_to_7), 1) *
                           NVL (MIN (SC.Ratio_7_to_8), 1) -
                           SUM (I.Qty_On_Hand) -
                           SUM (I.Qty_Allocated) "difference"
FROM Inventory I, SKU_Config SC, Condition_code CC
WHERE I.SKU_ID LIKE $P{sku_id}
AND I.Receipt_DStamp >= TO_DATE($P{fromdate}, 'DD/MM/YYYY')
AND I.Receipt_DStamp < TO_DATE($P{todate}, 'DD/MM/YYYY') + 1
AND SC.Config_ID = (SELECT MIN(Config_ID)
                    FROM SKU_SKU_config
                    WHERE SKU_SKU_config.SKU_ID = I.SKU_ID
		    AND ((($P{client_id} IS NULL) AND ('&8' IS NULL))
		    OR  (SKU_SKU_Config.Client_ID = $P{client_id})
		    OR  (($P{client_id} IS NULL) AND SKU_SKU_Config.Client_ID IN (SELECT Client_ID
										  FROM Client_Group_Clients
										  WHERE Client_Group = $P{client_group})))
		   )
AND CC.Condition_ID(+) = I.Condition_ID
AND (($P{site_id} IS NULL AND $P{site_group} IS NULL) OR  
     (I.Site_ID = $P{site_id}) OR  
     ($P{site_id} IS NULL AND I.Site_ID IN (SELECT Site_ID
					    FROM Site_Group_Sites
					    WHERE Site_Group = $P{site_group})))
AND (($P{owner_id} IS NULL) OR (I.Owner_ID = $P{owner_id}))
AND ((($P{client_id} IS NULL) AND ($P{client_group} IS NULL))
OR  (I.Client_ID = $P{client_id})
OR  (($P{client_id} IS NULL) AND I.Client_ID IN (SELECT Client_ID
						 FROM Client_Group_Clients
						 WHERE Client_Group =  $P{client_group})))
AND SC.rowid = LibConfig.GetConfigRowid (SC.Config_ID, $P{client_id})
GROUP BY I.Site_ID, I.Client_ID, I.Owner_ID, I.SKU_ID, CC.Notes
ORDER BY I.Site_ID, I.Client_ID, I.Owner_ID, I.SKU_ID, CC.Notes]]>
	</queryString>
	<field name="site_id" class="java.lang.String"/>
	<field name="client_id" class="java.lang.String"/>
	<field name="owner_id" class="java.lang.String"/>
	<field name="sku_id" class="java.lang.String"/>
	<field name="description" class="java.lang.String"/>
	<field name="notes" class="java.lang.String"/>
	<field name="config_id" class="java.lang.String"/>
	<field name="pallets" class="java.math.BigDecimal"/>
	<field name="quantity" class="java.math.BigDecimal"/>
	<field name="control" class="java.math.BigDecimal"/>
	<field name="difference" class="java.math.BigDecimal"/>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="86" splitType="Stretch">
			<textField isBlankWhenNull="false">
				<reportElement key="textField-1" style="Arial Unicode" x="110" y="34" width="558" height="24"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Bottom">
					<font fontName="Arial Unicode MS" size="12" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("invsumbyskudate")]]></textFieldExpression>
			</textField>
			<subreport isUsingCache="true">
				<reportElement key="subreport-1" style="Arial Unicode" x="0" y="0" width="782" height="33"/>
				<subreportParameter name="JR_TEMPLATE_DIR">
					<subreportParameterExpression><![CDATA[$P{JR_TEMPLATE_DIR}]]></subreportParameterExpression>
				</subreportParameter>
				<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
				<subreportExpression class="java.lang.String"><![CDATA[$P{JR_TEMPLATE_DIR} + "/titleheaderlandscape.jasper"]]></subreportExpression>
			</subreport>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-42" style="Arial Unicode" x="110" y="59" width="558" height="24"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="11" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.date($P{fromdate}) + " " + LangData.get("to") + " " + LangData.date($P{todate})]]></textFieldExpression>
			</textField>
		</band>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band height="39" splitType="Stretch">
			<rectangle>
				<reportElement key="rectangle-1" style="rptHeaderRectangle" x="0" y="0" width="781" height="39"/>
				<graphicElement>
					<pen lineWidth="0.25" lineStyle="Solid"/>
				</graphicElement>
			</rectangle>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-5" style="rptTitle" x="4" y="2" width="87" height="15"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("site_id")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-6" style="rptTitle" x="94" y="2" width="82" height="15"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("owner_id")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-10" style="rptTitle" x="179" y="2" width="170" height="15"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("sku_id")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-11" style="rptTitle" x="609" y="3" width="84" height="28"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top">
					<font fontName="Arial Unicode MS" size="10" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("control_data")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-13" style="rptTitle" x="179" y="20" width="170" height="15"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("description")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-16" style="rptTitle" x="352" y="2" width="79" height="15"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("config_id")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-17" style="rptTitle" x="352" y="20" width="79" height="15"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("notes")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-38" style="rptTitle" x="4" y="20" width="87" height="15"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("client_id")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-43" style="rptTitle" x="522" y="3" width="84" height="28"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top">
					<font fontName="Arial Unicode MS" size="10" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("qty_on_hand")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-45" style="rptTitle" x="435" y="3" width="84" height="28"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top">
					<font fontName="Arial Unicode MS" size="10" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("no_of_pallets")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-47" style="rptTitle" x="696" y="3" width="84" height="28"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top">
					<font fontName="Arial Unicode MS" size="10" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("difference")]]></textFieldExpression>
			</textField>
		</band>
	</columnHeader>
	<detail>
		<band height="40" splitType="Stretch">
			<rectangle>
				<reportElement key="rectangle-2" style="Arial Unicode" stretchType="RelativeToBandHeight" x="0" y="0" width="781" height="40" forecolor="#F5F3F0" backcolor="#EEEEEE">
					<printWhenExpression><![CDATA[new Boolean($V{REPORT_COUNT}.intValue() % 2 == 0)]]></printWhenExpression>
				</reportElement>
				<graphicElement>
					<pen lineWidth="0.25" lineStyle="Solid"/>
				</graphicElement>
			</rectangle>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-20" isPrintRepeatedValues="false" x="4" y="1" width="87" height="18"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{site_id}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-21" isPrintRepeatedValues="false" x="94" y="1" width="82" height="18"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{owner_id}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.######" isBlankWhenNull="true">
				<reportElement key="textField-24" x="609" y="0" width="71" height="18"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.number($F{control})]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="false">
				<reportElement key="textField-25" isPrintRepeatedValues="false" x="179" y="1" width="170" height="18"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{sku_id}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="false">
				<reportElement key="textField-28" isPrintRepeatedValues="false" x="179" y="20" width="170" height="18"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{description}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-31" x="352" y="1" width="79" height="18"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{config_id}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-32" x="352" y="20" width="422" height="18"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{notes}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-39" isPrintRepeatedValues="false" x="4" y="19" width="87" height="18"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{client_id}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.######" isBlankWhenNull="true">
				<reportElement key="textField-44" x="522" y="0" width="71" height="18"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.number($F{quantity})]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.######" isBlankWhenNull="true">
				<reportElement key="textField-46" x="435" y="0" width="71" height="18"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.number($F{pallets})]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.######" isBlankWhenNull="true">
				<reportElement key="textField-48" x="696" y="0" width="71" height="18"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.number($F{difference})]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="24" splitType="Stretch">
			<subreport isUsingCache="true">
				<reportElement key="subreport-2" style="Arial Unicode" x="0" y="0" width="782" height="22"/>
				<subreportParameter name="JR_TEMPLATE_DIR">
					<subreportParameterExpression><![CDATA[$P{JR_TEMPLATE_DIR}]]></subreportParameterExpression>
				</subreportParameter>
				<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
				<subreportExpression class="java.lang.String"><![CDATA[$P{JR_TEMPLATE_DIR} + "/footerlandscape.jasper"]]></subreportExpression>
			</subreport>
			<textField evaluationTime="Report" isBlankWhenNull="true">
				<reportElement key="textField-71" style="Arial Unicode" x="734" y="3" width="42" height="20"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA["" + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Page" isBlankWhenNull="true">
				<reportElement key="textField-72" style="Arial Unicode" x="531" y="3" width="202" height="20"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("page_name") + " " + $V{PAGE_NUMBER} + " " + LangData.get("page_of") + " "]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
