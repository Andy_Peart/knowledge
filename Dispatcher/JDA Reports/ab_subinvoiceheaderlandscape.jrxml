<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="ab_subinvoiceheaderlandscape" pageWidth="802" pageHeight="555" orientation="Landscape" whenNoDataType="AllSectionsNoDetail" columnWidth="802" leftMargin="0" rightMargin="0" topMargin="0" bottomMargin="0" uuid="ccd7e4c5-e901-4176-8b6a-b31de4c808c5">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<property name="ireport.zoom" value="3.0"/>
	<property name="ireport.x" value="683"/>
	<property name="ireport.y" value="0"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="language.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<import value="oracle.sql.*"/>
	<template><![CDATA[$P{JR_TEMPLATE_DIR} + "/default_styles.jrtx"]]></template>
	<parameter name="client_id" class="java.lang.String" isForPrompting="false">
		<parameterDescription><![CDATA[The required Client ID]]></parameterDescription>
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<parameter name="invoice" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<parameter name="invoice_type" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<parameter name="JR_TEMPLATE_DIR" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["Q:/gittrunkrepos/dispatcher/server/run/jrtemplates"]]></defaultValueExpression>
	</parameter>
	<parameter name="JR_LANGUAGE" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["EN_GB"]]></defaultValueExpression>
	</parameter>
	<parameter name="JR_TIME_ZONE_NAME" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["Europe/London"]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT		CLIENT_ID,
		INVOICE,
		INVOICE_DSTAMP,
		INVOICE_GROUP_ID,
		INVOICE_TYPE,
		APPROVED_BY,
		BLANKET_CONTRACT_ID,
		CONTRACT_ID,
		CURRENCY,
		STATUS,
		CUST_CONTRACT_ID,
		nvl(CONTACT_INFO,'No Contact Information for Approver:' || APPROVED_BY ) CONTACT_INFO,		I_CONTACT_NAME,
		I_NAME,
		I_ADDRESS1,
		I_ADDRESS2,
		I_TOWN,
		I_COUNTY,
		I_POSTCODE,
		I_COUNTRY,
		A_CONTACT_NAME,
		A_NAME,
		A_ADDRESS1,
		A_ADDRESS2,
		A_TOWN,
		A_COUNTY,
		A_POSTCODE,
		A_COUNTRY
from
(SELECT		1 ID,
		to_char(INVOICE_DSTAMP, 'DD/MM/YYYY') INVOICE_DSTAMP,
		ir.CLIENT_ID,
		INVOICE,
		INVOICE_GROUP_ID,
		INVOICE_TYPE,
		APPROVED_BY,
		ir.BLANKET_CONTRACT_ID,
		ir.CONTRACT_ID,
		ir.CURRENCY,
		ir.STATUS,
		nvl(cc.CUST_CONTRACT_ID, '---') CUST_CONTRACT_ID
FROM		ab_invoice_results ir, ab_contract cc, ab_client_contract abcc
WHERE		ir.client_id = $P{client_id}
AND		abcc.client_id = $P{client_id}
AND		ir.invoice = $P{invoice}
AND		ir.invoice_type = $P{invoice_type}
AND 		ir.client_id = abcc.client_id
AND		ir.blanket_contract_id = cc.blanket_contract_id
AND		ir.blanket_backup_number = cc.blanket_backup_number
AND		ir.contract_id = cc.contract_id
AND		ir.contract_backup_number = cc.contract_backup_number
AND		ir.contract_renewal_number = cc.contract_renewal_number
) query1,
(SELECT		1 ID,
		nvl(CONTACT_NAME, ' ') I_CONTACT_NAME,
		nvl(NAME, ' ') I_NAME,
		nvl(ADDRESS1, ' ') I_ADDRESS1,
		nvl(ADDRESS2, ' ') I_ADDRESS2,
		nvl(TOWN, ' ') I_TOWN,
		nvl(COUNTY, ' ') I_COUNTY,
		nvl(POSTCODE, ' ') I_POSTCODE,
		nvl(COUNTRY, ' ') I_COUNTRY
FROM		ab_address a, ab_address_contact ac, ab_invoice_results ir
WHERE		ir.client_id = ac.client_id
AND		ir.invoice_contact = ac.contact_id
AND		ac.client_id = a.client_id
AND		ac.address_id = a.address_id
AND 		ir.client_id = $P{client_id}
AND		ir.invoice = $P{invoice}
AND		ir.invoice_type = $P{invoice_type}
) query2,
(SELECT		1 ID,
		nvl(CONTACT_NAME, ' ') A_CONTACT_NAME,
		nvl(NAME, ' ') A_NAME,
		nvl(ADDRESS1, ' ') A_ADDRESS1,
		nvl(ADDRESS2, ' ') A_ADDRESS2,
		nvl(TOWN, ' ') A_TOWN,
		nvl(COUNTY, ' ') A_COUNTY,
		nvl(POSTCODE, ' ') A_POSTCODE,
		nvl(COUNTRY, ' ') A_COUNTRY
FROM		ab_address a, ab_address_contact ac, ab_invoice_results ir
WHERE		ir.client_id = ac.client_id
AND		ir.admin_contact = ac.contact_id
AND		ac.client_id = a.client_id
AND		ac.address_id = a.address_id
AND 		ir.client_id = $P{client_id}
AND		ir.invoice = $P{invoice}
AND		ir.invoice_type = $P{invoice_type}
) query3,
(SELECT		1 ID,
		CONTACT || decode( nvl(contact,'-'),'-',' ',', ') ||
		NAME || decode( nvl(NAME,'-'),'-',' ',', ') ||
		ADDRESS1 || decode( nvl(ADDRESS1,'-'),'-',' ',', ') ||
		ADDRESS2 || decode( nvl(ADDRESS2,'-'),'-',' ',', ') ||
		TOWN || decode( nvl(TOWN,'-'),'-',' ',', ') ||
		COUNTY|| decode( nvl(COUNTY,'-'),'-',' ',', ') ||
		POSTCODE || decode( nvl(POSTCODE,'-'),'-',' ',', ') ||
		COUNTRY || decode( nvl(COUNTRY,'-'),'-',' ',', ') ||
		CONTACT_PHONE || decode( nvl(CONTACT_PHONE,'-'),'-',' ',', ') ||
		CONTACT_MOBILE || decode( nvl(CONTACT_MOBILE,'-'),'-',' ',', ') ||
		CONTACT_FAX || decode( nvl(CONTACT_FAX,'-'),'-',' ',', ') ||
		CONTACT_EMAIL || decode( nvl(CONTACT_EMAIL,'-'),'-',' ',', ') ||
		u.NOTES CONTACT_INFO
FROM		application_user u, ab_invoice_results ir
WHERE		ir.approved_by = u.user_id
AND 		ir.client_id = $P{client_id}
AND		ir.invoice = $P{invoice}
AND		ir.invoice_type = $P{invoice_type}
) query4
where query1.id = query2.id
and query1.id = query3.id
and query1.id = query4.id (+)]]>
	</queryString>
	<field name="CLIENT_ID" class="java.lang.String"/>
	<field name="INVOICE" class="java.lang.String"/>
	<field name="INVOICE_DSTAMP" class="java.lang.String"/>
	<field name="INVOICE_GROUP_ID" class="java.lang.String"/>
	<field name="INVOICE_TYPE" class="java.lang.String"/>
	<field name="APPROVED_BY" class="java.lang.String"/>
	<field name="BLANKET_CONTRACT_ID" class="java.lang.String"/>
	<field name="CONTRACT_ID" class="java.lang.String"/>
	<field name="CURRENCY" class="java.lang.String"/>
	<field name="STATUS" class="java.lang.String"/>
	<field name="CUST_CONTRACT_ID" class="java.lang.String"/>
	<field name="CONTACT_INFO" class="java.lang.String"/>
	<field name="I_CONTACT_NAME" class="java.lang.String"/>
	<field name="I_NAME" class="java.lang.String"/>
	<field name="I_ADDRESS1" class="java.lang.String"/>
	<field name="I_ADDRESS2" class="java.lang.String"/>
	<field name="I_TOWN" class="java.lang.String"/>
	<field name="I_COUNTY" class="java.lang.String"/>
	<field name="I_POSTCODE" class="java.lang.String"/>
	<field name="I_COUNTRY" class="java.lang.String"/>
	<field name="A_CONTACT_NAME" class="java.lang.String"/>
	<field name="A_NAME" class="java.lang.String"/>
	<field name="A_ADDRESS1" class="java.lang.String"/>
	<field name="A_ADDRESS2" class="java.lang.String"/>
	<field name="A_TOWN" class="java.lang.String"/>
	<field name="A_COUNTY" class="java.lang.String"/>
	<field name="A_POSTCODE" class="java.lang.String"/>
	<field name="A_COUNTRY" class="java.lang.String"/>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band splitType="Stretch"/>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band height="176" splitType="Stretch">
			<rectangle radius="0">
				<reportElement uuid="590d3f30-476f-4e90-bd8e-aa16416fe1c4" key="rectangle-3" x="562" y="34" width="240" height="138" forecolor="#CCCCCC" backcolor="#CCCCCC"/>
				<graphicElement>
					<pen lineWidth="0.0" lineStyle="Solid"/>
				</graphicElement>
			</rectangle>
			<rectangle radius="0">
				<reportElement uuid="c4ba4a97-6932-4b48-bbad-93efbe3e5150" key="rectangle-2" x="346" y="34" width="196" height="138" backcolor="#CCCCCC"/>
				<graphicElement>
					<pen lineWidth="0.0" lineStyle="Solid"/>
				</graphicElement>
			</rectangle>
			<rectangle radius="6">
				<reportElement uuid="e1a02c16-c0ae-4154-ac1c-48d9df3a9b85" key="rectangle-4" style="rptHeaderRectangle" mode="Opaque" x="278" y="18" width="134" height="12"/>
				<graphicElement fill="Solid"/>
			</rectangle>
			<rectangle radius="0">
				<reportElement uuid="495e9a37-d10d-4871-b72d-435636eaeb11" key="rectangle-20" x="158" y="66" width="114" height="77" forecolor="#CCCCCC" backcolor="#CCCCCC"/>
				<graphicElement fill="Solid"/>
			</rectangle>
			<rectangle radius="6">
				<reportElement uuid="ccf61ea3-e032-4631-955f-a5d1b0154e65" key="rectangle-19" x="0" y="66" width="268" height="77" forecolor="#CCCCCC" backcolor="#CCCCCC"/>
				<graphicElement fill="Solid"/>
			</rectangle>
			<rectangle radius="6">
				<reportElement uuid="590d3f30-476f-4e90-bd8e-aa16416fe1c4" key="rectangle-3" x="550" y="34" width="228" height="138" forecolor="#CCCCCC" backcolor="#CCCCCC"/>
				<graphicElement>
					<pen lineWidth="0.0" lineStyle="Solid"/>
				</graphicElement>
			</rectangle>
			<rectangle radius="0">
				<reportElement uuid="b1853f5c-0230-486d-9614-e0e491a04e70" key="rectangle-15" style="rptHeaderRectangle" mode="Opaque" x="664" y="34" width="138" height="18"/>
				<graphicElement fill="Solid"/>
			</rectangle>
			<rectangle radius="6">
				<reportElement uuid="c4ba4a97-6932-4b48-bbad-93efbe3e5150" key="rectangle-2" x="278" y="34" width="209" height="138" backcolor="#CCCCCC"/>
				<graphicElement>
					<pen lineWidth="0.0" lineStyle="Solid"/>
				</graphicElement>
			</rectangle>
			<rectangle radius="6">
				<reportElement uuid="4b1746b8-44e6-4cfa-810b-7f9430d241da" key="rectangle-11" style="rptHeaderRectangle" mode="Opaque" x="278" y="34" width="190" height="18"/>
				<graphicElement fill="Solid"/>
			</rectangle>
			<rectangle radius="6">
				<reportElement uuid="8b1a2714-601a-4cd7-8585-33acbefd767c" key="rectangle-14" style="rptHeaderRectangle" mode="Opaque" x="550" y="34" width="240" height="18"/>
				<graphicElement fill="Solid"/>
			</rectangle>
			<rectangle radius="0">
				<reportElement uuid="7ce114f2-6ce2-4de1-b747-97b6c912a20b" key="rectangle-13" style="rptHeaderRectangle" mode="Opaque" x="345" y="34" width="197" height="18"/>
				<graphicElement fill="Solid"/>
			</rectangle>
			<rectangle radius="0">
				<reportElement uuid="a816a74d-8fad-43c0-bbca-fecd0883db5b" key="rectangle-8" style="rptHeaderRectangle" mode="Opaque" x="634" y="18" width="168" height="12"/>
				<graphicElement fill="Solid">
					<pen lineWidth="0.0" lineStyle="Solid"/>
				</graphicElement>
			</rectangle>
			<rectangle radius="6">
				<reportElement uuid="1848d370-4bbd-413e-a720-a63c299a8a1d" key="rectangle-7" style="rptHeaderRectangle" mode="Opaque" x="550" y="18" width="134" height="12"/>
				<graphicElement fill="Solid">
					<pen lineWidth="0.0" lineStyle="Solid"/>
				</graphicElement>
			</rectangle>
			<rectangle radius="0">
				<reportElement uuid="ffd2661d-6f98-4a31-8e7b-31d5bc3b597f" key="rectangle-6" style="rptHeaderRectangle" mode="Opaque" x="296" y="18" width="246" height="12"/>
				<graphicElement fill="Solid"/>
			</rectangle>
			<textField isBlankWhenNull="false">
				<reportElement uuid="d6fe4338-ed5c-4579-b70a-e3ce961d8032" key="textField" x="283" y="53" width="254" height="16" forecolor="#000000"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial Unicode MS"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{I_CONTACT_NAME}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement uuid="5dae4d61-1a95-4d1f-b6a0-a422ccd81518" key="textField" x="283" y="69" width="254" height="16" forecolor="#000000"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial Unicode MS"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{I_NAME}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement uuid="1c7df5b4-a6ce-402c-a832-671d277741a5" key="textField" x="283" y="85" width="254" height="16" forecolor="#000000"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial Unicode MS"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{I_ADDRESS1}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement uuid="94e4c8d4-a739-431f-ab45-3f1d4405ee61" key="textField" x="283" y="101" width="254" height="16" forecolor="#000000"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial Unicode MS"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{I_ADDRESS2}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement uuid="1d2527cd-6ea4-4b3a-9797-37a6761938c3" key="textField" x="283" y="117" width="254" height="16" forecolor="#000000"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial Unicode MS"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{I_TOWN}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement uuid="4941ebe7-7cef-43fa-80a7-9388de84b093" key="textField" x="283" y="133" width="254" height="16" forecolor="#000000"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial Unicode MS"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{I_COUNTY}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement uuid="53f5847b-f60b-4088-9f1c-55ece241795c" key="textField" x="283" y="149" width="254" height="16" forecolor="#000000"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial Unicode MS"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{I_POSTCODE}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement uuid="918e13ce-8655-4675-a53a-1946a8771df3" key="textField" x="555" y="56" width="167" height="16" forecolor="#000000"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial Unicode MS"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{A_CONTACT_NAME}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement uuid="32f35099-bf7f-4690-8ed5-2f9c68b39128" key="textField" x="555" y="72" width="167" height="16" forecolor="#000000"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial Unicode MS"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{A_NAME}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement uuid="475a56ac-a499-485a-8ce1-d03ab6a47a2c" key="textField" x="555" y="88" width="167" height="16" forecolor="#000000"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial Unicode MS"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{A_ADDRESS1}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement uuid="d86e480f-651f-4a1b-a4c6-d6b871499f6c" key="textField" x="555" y="104" width="167" height="16" forecolor="#000000"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial Unicode MS"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{A_ADDRESS2}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement uuid="bec493ff-b363-47ad-bbd3-727f54b6e92d" key="textField" x="555" y="120" width="167" height="16" forecolor="#000000"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial Unicode MS"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{A_TOWN}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement uuid="6eba010f-8b59-4592-b407-2eedf0390cce" key="textField" x="555" y="136" width="167" height="16" forecolor="#000000"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial Unicode MS"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{A_COUNTY}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement uuid="6a4db75f-1d71-46f2-a608-835445e9adae" key="textField" x="555" y="152" width="167" height="16" forecolor="#000000"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial Unicode MS"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{A_POSTCODE}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement uuid="7b010e0c-81d3-4093-ba77-0cd197b5ffaf" key="textField" x="670" y="4" width="124" height="14" forecolor="#000000"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{INVOICE_DSTAMP}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement uuid="60497977-c56e-4b2c-8c14-6b4073c7cd0b" key="textField" stretchType="RelativeToTallestObject" x="554" y="4" width="110" height="14" isPrintWhenDetailOverflows="true" forecolor="#000000"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{INVOICE}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement uuid="59e7e3f0-546a-474b-9111-2eb60986efd3" key="textField-1" style="rptTitle" x="554" y="18" width="110" height="12"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="7" isBold="true" isItalic="true"/>
				</textElement>
				<textFieldExpression><![CDATA[LangData.get("RPT_INVOICE_NUMBER")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement uuid="62b8ca57-153e-42f3-875f-1f21a18ea13e" key="textField-2" style="rptTitle" x="670" y="18" width="124" height="12"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="7" isBold="true" isItalic="true"/>
				</textElement>
				<textFieldExpression><![CDATA[LangData.get("RPT_INVOICE_DATE")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement uuid="f043a740-05e9-42b1-ba60-796b5da47394" key="textField-3" style="rptTitle" x="282" y="35" width="254" height="16"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10" isBold="true" isItalic="true"/>
				</textElement>
				<textFieldExpression><![CDATA[LangData.get("mqs_ab_blanket_contract_tab1")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement uuid="51ddbe16-38d0-48aa-973f-c73b9761af11" key="textField-4" style="rptTitle" x="554" y="36" width="244" height="16"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10" isBold="true" isItalic="true"/>
				</textElement>
				<textFieldExpression><![CDATA[LangData.get("mqs_ab_blanket_contract_tab2")]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement uuid="86324dba-74d0-406e-9a7d-f44ceb7f6dd5" key="staticText-1" x="282" y="0" width="166" height="20" forecolor="#000000"/>
				<textElement>
					<font fontName="Arial Unicode MS" size="14" isBold="true"/>
				</textElement>
				<text><![CDATA[JDA Software Group]]></text>
			</staticText>
			<textField isBlankWhenNull="false">
				<reportElement uuid="2baa232d-a516-43b8-8ae1-24939930a3b2" key="textField" x="6" y="146" width="134" height="14"/>
				<textElement>
					<font fontName="Arial Unicode MS"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{CLIENT_ID}]]></textFieldExpression>
			</textField>
			<rectangle radius="6">
				<reportElement uuid="fb7e781d-cef0-4c14-9e4d-de1da44171f5" key="rectangle-9" style="rptHeaderRectangle" mode="Opaque" x="2" y="160" width="134" height="12"/>
				<graphicElement fill="Solid"/>
			</rectangle>
			<rectangle radius="0">
				<reportElement uuid="e6c43dc9-c2b6-43d9-9943-687dfdeade27" key="rectangle-10" style="rptHeaderRectangle" mode="Opaque" x="105" y="160" width="167" height="12"/>
				<graphicElement fill="Solid"/>
			</rectangle>
			<textField isBlankWhenNull="false">
				<reportElement uuid="2d6456b5-6d00-4778-9432-00671c5f2cd4" key="textField-5" style="rptTitle" x="6" y="160" width="134" height="12"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="7" isBold="true" isItalic="true"/>
				</textElement>
				<textFieldExpression><![CDATA[LangData.get("client_id")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement uuid="b3f032a2-a37c-4f15-872d-58526125273c" key="textField" x="148" y="146" width="122" height="14"/>
				<textElement/>
				<textFieldExpression><![CDATA[$F{CUST_CONTRACT_ID}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement uuid="8a158a30-e7d8-4619-8656-d335c77858b1" key="textField-6" style="rptTitle" x="148" y="160" width="123" height="12"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="7" isBold="true" isItalic="true"/>
				</textElement>
				<textFieldExpression><![CDATA[LangData.get("cust_contract_id")]]></textFieldExpression>
			</textField>
			<image hAlign="Center" vAlign="Middle">
				<reportElement uuid="0710fd3c-354d-41a5-86d3-76981ce7eea4" key="image-1" x="0" y="0" width="258" height="63"/>
				<imageExpression><![CDATA[$P{JR_TEMPLATE_DIR} + "/images/ReportLogo.jpg"]]></imageExpression>
			</image>
			<textField isBlankWhenNull="false">
				<reportElement uuid="6bcceb5e-e23e-42ea-9978-967a3115d2e2" key="textField" x="6" y="83" width="259" height="58" isPrintWhenDetailOverflows="true"/>
				<textElement/>
				<textFieldExpression><![CDATA[$F{CONTACT_INFO}]]></textFieldExpression>
			</textField>
			<rectangle radius="0">
				<reportElement uuid="f632e90c-2eb4-4be1-a1a4-504e1d1db442" key="rectangle-16" style="rptHeaderRectangle" mode="Opaque" x="241" y="66" width="31" height="16"/>
				<graphicElement fill="Solid"/>
			</rectangle>
			<rectangle radius="6">
				<reportElement uuid="51534f14-0029-4842-b5f9-05bae18ff417" key="rectangle-17" style="rptHeaderRectangle" mode="Opaque" x="0" y="66" width="266" height="16"/>
				<graphicElement fill="Solid"/>
			</rectangle>
			<textField isBlankWhenNull="false">
				<reportElement uuid="0ff5e38a-e940-4d88-83ae-ea04dc69338c" key="textField-7" style="rptTitle" x="6" y="69" width="134" height="10"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="7" isBold="true" isItalic="true"/>
				</textElement>
				<textFieldExpression><![CDATA[LangData.get("r_contact")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement uuid="c3e15023-cff6-4725-919d-beccb08a3abe" key="textField-8" x="448" y="4" width="94" height="14"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{STATUS}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement uuid="83e59563-6739-444f-8995-dd9d86548333" key="textField-9" style="rptTitle" x="448" y="18" width="94" height="12"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="7" isBold="true" isItalic="true"/>
				</textElement>
				<textFieldExpression><![CDATA[LangData.get("status")]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band splitType="Stretch"/>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
