<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="dpdext_domesticzebra" pageWidth="297" pageHeight="363" orientation="Landscape" columnWidth="297" leftMargin="0" rightMargin="0" topMargin="0" bottomMargin="0" uuid="b2c3c8e1-3441-427a-a983-1f385e4f206d">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<property name="ireport.zoom" value="2.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="198"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="language.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<import value="oracle.sql.*"/>
	<template><![CDATA[$P{JR_TEMPLATE_DIR} + "/default_styles.jrtx"]]></template>
	<style name="Arial Unicode" isDefault="true" fontName="Arial Unicode MS" fontSize="12" isUnderline="false" pdfFontName="Arialuni.ttf" pdfEncoding="Identity-H" isPdfEmbedded="false"/>
	<style name="Arial" fontName="Arial" fontSize="12" isUnderline="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
	<parameter name="JR_LANGUAGE" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["EN_GB"]]></defaultValueExpression>
	</parameter>
	<parameter name="JR_TEMPLATE_DIR" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["Q:/gittrunkrepos/dispatcher/server/run/jrtemplates"]]></defaultValueExpression>
	</parameter>
	<parameter name="JR_TIME_ZONE_NAME" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["Europe/London"]]></defaultValueExpression>
	</parameter>
	<parameter name="pallet_id" class="java.lang.String"/>
	<parameter name="container_id" class="java.lang.String"/>
	<parameter name="site_id" class="java.lang.String"/>
	<parameter name="site_group" class="java.lang.String" isForPrompting="false"/>
	<queryString>
		<![CDATA[with parcels as (Select rownum "ParcelNumber", trim(to_char(rownum, '009')) "ParcelNumberPadded", a.*
       from (Select distinct OC.Dstamp, OC.Container_ID, OC.Pallet_ID, initcap(nvl(OH.Name, ' '))
       "Delivery Name", initcap(nvl(OH.Address1, ' ')) "Address1", initcap(nvl(OH.Address2, ' ')) "Address2",
       initcap(nvl(OH.Town, ' ')) "Town", initcap(nvl(OH.County, ' ')) "County", upper(nvl(OH.Postcode, ' '))
       "Postcode", upper(replace(OH.Postcode, ' ', '')) "SPostcode", nvl(OH.Contact, ' ') "Contact",
       nvl(OH.Contact_Phone, ' ') "Phone", upper(nvl(CT.ISO2_ID, ' ')) "SCountry",
       initcap(replace(CT.Tandata_ID, '_', ' ')) "Country", nvl(OH.Instructions, ' ') "Info",
       nvl(C.Service_Level, ' ') "Service Description", C.SCAC "Service Code", nvl(C.Routing_Version, ' ')
      "Routing Version", to_char(current_timestamp, 'DD/MM/YY') "Current Date", to_number(C.SCAC + 100)
       "reverseit", to_char(current_timestamp,'HH:MM') "Current Time", nvl(S.Name, ' ') "Sender Name",
       nvl(S.Address1, ' ') "SAddress1", nvl(S.Town, ' ') "STown", nvl(S.Postcode, ' ') "SendPostcode",
       nvl(C.Account_ID, ' ') "Account", nvl(S.Contact_Phone, ' ') "SenderPhone", nvl(OH.Order_ID, ' ')
       "SenderRef", substr(OC.Carrier_Container_ID, 1,4) "Depot Number",
       substr(OC.Carrier_Container_ID, 5,4) "TN1", substr(OC.Carrier_Container_ID, 9,4) "TN2",
       substr(OC.Carrier_Container_ID, 13) "TN3", substr(OC.Carrier_Container_ID, 13,2) "TN3nocheck",
       OC.Container_Weight "Weight", lpad(replace(OH.Postcode, ' ', ''), 7, '0') "BarcodePostcode",
       CT.ISO3166_ID "CountryCode",
       LibCheckStringAlg.ISO7064Mod3637(upper(lpad(replace(OH.Postcode, ' ', ''), 7, '0')) ||
       substr(OC.Carrier_Container_ID, 1, 4) || (substr(OC.Carrier_Container_ID, 5, 14)) || C.SCAC ||
       CT.ISO3166_ID) "checkdigit"
FROM Order_Header OH, Carriers C, Site S, Order_Container OC, Order_Container OC2, Country CT
WHERE (OC2.Pallet_ID = $P{pallet_id} OR $P{pallet_id} IS NULL)
AND (OC2.Container_ID = $P{container_id} OR $P{container_id} IS NULL)
AND (OC.Order_ID = OC2.Order_ID)
AND (OC.Client_ID = OC2.Client_ID)
AND (($P{site_id} IS NULL AND $P{site_group} IS NULL)
     OR (OH.From_Site_ID = $P{site_id})
     OR ($P{site_id} IS NULL AND OH.From_Site_ID IN (SELECT Site_ID
  				            FROM Site_Group_Sites
                  				  WHERE Site_Group = $P{site_group})))
AND (OC.Client_ID = OH.Client_ID)
AND (OC.Order_ID = OH.Order_ID)
AND (OH.From_Site_ID = S.Site_ID)
AND (OC.Carrier_ID = C.Carrier_ID)
AND (OC.Service_Level = C.Service_Level)
AND (OH.Country = CT.ISO3_ID)
/* sanity check to ensure that the user has specified either container OR pallet */
AND NOT ($P{pallet_id} IS NULL AND $P{container_id} IS NULL)) a
)
Select parcelcount.ParcelCount "ParcelCount", parceldetails.*
       from (Select parcels.* from parcels
       WHERE (parcels.Pallet_ID = $P{pallet_id} or $P{pallet_id} IS NULL)
       AND (parcels.Container_ID = $P{container_id} or $P{container_id} IS NULL)) parceldetails,
       (Select count(*) ParcelCount from parcels) parcelcount]]>
	</queryString>
	<field name="ParcelCount" class="java.math.BigDecimal"/>
	<field name="ParcelNumber" class="java.math.BigDecimal"/>
	<field name="ParcelNumberPadded" class="java.lang.String"/>
	<field name="DSTAMP" class="oracle.sql.TIMESTAMPLTZ"/>
	<field name="CONTAINER_ID" class="java.lang.String"/>
	<field name="PALLET_ID" class="java.lang.String"/>
	<field name="Delivery Name" class="java.lang.String"/>
	<field name="Address1" class="java.lang.String"/>
	<field name="Address2" class="java.lang.String"/>
	<field name="Town" class="java.lang.String"/>
	<field name="County" class="java.lang.String"/>
	<field name="Postcode" class="java.lang.String"/>
	<field name="SPostcode" class="java.lang.String"/>
	<field name="Contact" class="java.lang.String"/>
	<field name="Phone" class="java.lang.String"/>
	<field name="SCountry" class="java.lang.String"/>
	<field name="Country" class="java.lang.String"/>
	<field name="Info" class="java.lang.String"/>
	<field name="Service Description" class="java.lang.String"/>
	<field name="Service Code" class="java.lang.String"/>
	<field name="Routing Version" class="java.lang.String"/>
	<field name="Current Date" class="java.lang.String"/>
	<field name="reverseit" class="java.math.BigDecimal"/>
	<field name="Current Time" class="java.lang.String"/>
	<field name="Sender Name" class="java.lang.String"/>
	<field name="SAddress1" class="java.lang.String"/>
	<field name="STown" class="java.lang.String"/>
	<field name="SendPostcode" class="java.lang.String"/>
	<field name="Account" class="java.lang.String"/>
	<field name="SenderPhone" class="java.lang.String"/>
	<field name="SenderRef" class="java.lang.String"/>
	<field name="Depot Number" class="java.lang.String"/>
	<field name="TN1" class="java.lang.String"/>
	<field name="TN2" class="java.lang.String"/>
	<field name="TN3" class="java.lang.String"/>
	<field name="TN3nocheck" class="java.lang.String"/>
	<field name="Weight" class="java.math.BigDecimal"/>
	<field name="BarcodePostcode" class="java.lang.String"/>
	<field name="CountryCode" class="java.lang.String"/>
	<field name="checkdigit" class="java.lang.String"/>
	<background>
		<band splitType="Stretch"/>
	</background>
	<columnHeader>
		<band splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band height="125" splitType="Stretch">
			<frame>
				<reportElement uuid="a64572be-5d52-44fb-9265-6f28e1b564a8" x="0" y="0" width="296" height="125" backcolor="#000000"/>
				<box>
					<pen lineWidth="1.5"/>
					<topPen lineWidth="1.5"/>
					<leftPen lineWidth="1.5"/>
					<bottomPen lineWidth="1.5"/>
					<rightPen lineWidth="1.5"/>
				</box>
				<frame>
					<reportElement uuid="6cbf1f17-8e8f-4017-b4d6-455208990150" x="0" y="10" width="168" height="72"/>
					<box>
						<pen lineWidth="0.75"/>
						<topPen lineWidth="0.75"/>
						<leftPen lineWidth="0.75"/>
						<bottomPen lineWidth="0.75"/>
						<rightPen lineWidth="0.75"/>
					</box>
					<rectangle>
						<reportElement uuid="c9b2bd6b-6205-44b4-9a6d-34f86ffefe8a" x="0" y="0" width="168" height="72"/>
					</rectangle>
					<staticText>
						<reportElement uuid="7c69c635-a0e0-49d3-ad3a-12fd3fb9dd5b" x="154" y="4" width="10" height="58" forecolor="#999999"/>
						<textElement rotation="Right">
							<font size="6"/>
						</textElement>
						<text><![CDATA[Delivery Address]]></text>
					</staticText>
					<textField>
						<reportElement uuid="1ade753f-d4f5-4e55-a4c3-443205c02d60" x="6" y="3" width="119" height="11"/>
						<textElement>
							<font size="8" isBold="true"/>
						</textElement>
						<textFieldExpression><![CDATA[$F{Delivery Name}]]></textFieldExpression>
					</textField>
					<textField>
						<reportElement uuid="185ca3d5-91bb-4d30-a482-fc23e24513f2" x="6" y="14" width="119" height="11"/>
						<textElement>
							<font size="8" isBold="true"/>
						</textElement>
						<textFieldExpression><![CDATA[$F{Address1}]]></textFieldExpression>
					</textField>
					<textField>
						<reportElement uuid="d513bfe1-f4aa-4000-9c3e-13221d33bdb5" x="6" y="25" width="119" height="11"/>
						<textElement>
							<font size="8" isBold="true"/>
						</textElement>
						<textFieldExpression><![CDATA[$F{Town}]]></textFieldExpression>
					</textField>
					<textField>
						<reportElement uuid="2acc7a34-696f-4941-ab28-2682ae73de57" x="6" y="36" width="119" height="11"/>
						<textElement>
							<font size="8" isBold="true"/>
						</textElement>
						<textFieldExpression><![CDATA[$F{County}]]></textFieldExpression>
					</textField>
					<textField>
						<reportElement uuid="ff0f9d48-c0e2-4f4c-9b11-d28f1361f98c" x="6" y="47" width="119" height="11"/>
						<textElement>
							<font size="8" isBold="true"/>
						</textElement>
						<textFieldExpression><![CDATA[$F{Postcode}]]></textFieldExpression>
					</textField>
					<textField>
						<reportElement uuid="d59a5fba-3a60-4964-b56a-d6ea55cbdb72" x="6" y="58" width="119" height="11"/>
						<textElement>
							<font size="8" isBold="true"/>
						</textElement>
						<textFieldExpression><![CDATA[$F{SCountry} + " - " + $F{Country}]]></textFieldExpression>
					</textField>
				</frame>
				<frame>
					<reportElement uuid="ece1bf19-a819-4253-b813-8f7bd9a50462" x="0" y="82" width="122" height="43"/>
					<box>
						<pen lineWidth="0.75"/>
						<topPen lineWidth="0.75"/>
						<leftPen lineWidth="0.75"/>
						<bottomPen lineWidth="0.75"/>
						<rightPen lineWidth="0.75"/>
					</box>
					<rectangle>
						<reportElement uuid="e21ab082-1b77-4d73-90c8-8f79f89803e9" x="0" y="0" width="122" height="43"/>
					</rectangle>
					<staticText>
						<reportElement uuid="243c7895-76fa-4aeb-9af7-25dca85da4bb" x="3" y="0" width="25" height="9" forecolor="#999999"/>
						<textElement>
							<font size="6"/>
						</textElement>
						<text><![CDATA[Contact]]></text>
					</staticText>
					<staticText>
						<reportElement uuid="839940db-44a2-450e-a29a-3bc4a671be67" x="3" y="9" width="25" height="10" forecolor="#999999"/>
						<textElement>
							<font size="6"/>
						</textElement>
						<text><![CDATA[Phone]]></text>
					</staticText>
					<staticText>
						<reportElement uuid="9a64cc11-5fd5-4340-8272-cf353eb2fb61" x="3" y="19" width="25" height="9" forecolor="#999999"/>
						<textElement>
							<font size="6"/>
						</textElement>
						<text><![CDATA[Info]]></text>
					</staticText>
					<staticText>
						<reportElement uuid="18ecc04c-722b-4467-babc-66efd6699e8b" x="3" y="34" width="40" height="9" forecolor="#999999"/>
						<textElement>
							<font size="6"/>
						</textElement>
						<text><![CDATA[Consignment]]></text>
					</staticText>
					<textField>
						<reportElement uuid="ec5d9aa6-e755-4dfa-9ff1-6748213ed32d" x="28" y="0" width="81" height="9"/>
						<textElement>
							<font size="6" isBold="true"/>
						</textElement>
						<textFieldExpression><![CDATA[$F{Contact}]]></textFieldExpression>
					</textField>
					<textField>
						<reportElement uuid="07097cef-fa52-4d88-a05b-4bf2bd35b09d" x="28" y="9" width="81" height="10"/>
						<textElement>
							<font size="6" isBold="true"/>
						</textElement>
						<textFieldExpression><![CDATA[$F{Phone}]]></textFieldExpression>
					</textField>
					<textField>
						<reportElement uuid="55479d9b-742d-4e64-9bb4-c423d9cdf3e1" x="28" y="19" width="81" height="9"/>
						<textElement>
							<font size="6" isBold="true"/>
						</textElement>
						<textFieldExpression><![CDATA[$F{Info}]]></textFieldExpression>
					</textField>
				</frame>
				<frame>
					<reportElement uuid="9e5aa96c-541c-4e6a-bba3-c10fbb00000e" x="122" y="82" width="46" height="43"/>
					<box>
						<pen lineWidth="0.75"/>
						<topPen lineWidth="0.75"/>
						<leftPen lineWidth="0.75"/>
						<bottomPen lineWidth="0.75"/>
						<rightPen lineWidth="0.75"/>
					</box>
					<rectangle>
						<reportElement uuid="99e8353d-7d2f-46fb-96bb-d6684c4283aa" x="0" y="0" width="46" height="43"/>
					</rectangle>
					<staticText>
						<reportElement uuid="c0881d2b-0dfc-42a2-a9dd-c46098897371" x="16" y="0" width="30" height="9" forecolor="#999999"/>
						<textElement textAlignment="Center" verticalAlignment="Middle">
							<font size="6"/>
						</textElement>
						<text><![CDATA[Packages]]></text>
					</staticText>
					<staticText>
						<reportElement uuid="a73cd91a-9ca6-401b-b7e4-0d52f2c47441" x="25" y="19" width="21" height="10" forecolor="#999999"/>
						<textElement textAlignment="Center" verticalAlignment="Middle">
							<font size="6"/>
						</textElement>
						<text><![CDATA[Weight]]></text>
					</staticText>
					<textField pattern="###0.00">
						<reportElement uuid="a9f78eea-1207-4ac6-94d1-979d938ad932" x="2" y="28" width="29" height="15"/>
						<textElement textAlignment="Right" verticalAlignment="Middle">
							<font size="8" isBold="true"/>
						</textElement>
						<textFieldExpression><![CDATA[$F{Weight}]]></textFieldExpression>
					</textField>
					<textField>
						<reportElement uuid="030f59a5-e60e-40d0-91ef-80799576c479" x="16" y="8" width="30" height="11"/>
						<textElement textAlignment="Center" verticalAlignment="Middle">
							<font size="8" isBold="true"/>
						</textElement>
						<textFieldExpression><![CDATA[$F{ParcelNumber} + " of " + $F{ParcelCount}]]></textFieldExpression>
					</textField>
					<staticText>
						<reportElement uuid="ebd65393-cee7-4053-904d-774d8e8ab17e" x="31" y="28" width="15" height="15"/>
						<textElement verticalAlignment="Middle">
							<font size="8" isBold="true"/>
						</textElement>
						<text><![CDATA[ Kg]]></text>
					</staticText>
				</frame>
				<frame>
					<reportElement uuid="3b81b55c-21bc-4281-9d3a-31bd8d75efa5" x="168" y="10" width="63" height="115"/>
					<box>
						<pen lineWidth="0.75"/>
						<topPen lineWidth="0.75"/>
						<leftPen lineWidth="0.75"/>
						<bottomPen lineWidth="0.75"/>
						<rightPen lineWidth="0.75"/>
					</box>
					<rectangle>
						<reportElement uuid="6bb9d20c-2ff1-4064-8eb3-6927ca06018e" x="0" y="0" width="63" height="115"/>
					</rectangle>
					<staticText>
						<reportElement uuid="b39aead5-d979-4ed5-9b6b-d92b45fb0d84" x="53" y="3" width="10" height="21" forecolor="#999999"/>
						<textElement rotation="Right">
							<font size="6"/>
						</textElement>
						<text><![CDATA[Sender]]></text>
					</staticText>
					<staticText>
						<reportElement uuid="b11e60e5-24bc-4f05-81e3-f1e9980cd460" x="54" y="57" width="9" height="24" forecolor="#999999"/>
						<textElement rotation="Right">
							<font size="6"/>
						</textElement>
						<text><![CDATA[Account]]></text>
					</staticText>
					<staticText>
						<reportElement uuid="8f910f36-f331-49ed-9236-0a8cc6507610" x="0" y="3" width="10" height="30" forecolor="#999999"/>
						<textElement rotation="Right">
							<font size="6"/>
						</textElement>
						<text><![CDATA[Reference]]></text>
					</staticText>
					<staticText>
						<reportElement uuid="3a1cddcb-b1f4-4713-9490-0dd2b22eead5" x="8" y="3" width="9" height="30" forecolor="#999999"/>
						<textElement rotation="Right">
							<font size="6"/>
						</textElement>
						<text><![CDATA[Phone]]></text>
					</staticText>
					<textField>
						<reportElement uuid="7bb619b6-2f5d-40e9-90b7-771162ada863" x="44" y="3" width="10" height="54"/>
						<textElement rotation="Right">
							<font size="6"/>
						</textElement>
						<textFieldExpression><![CDATA[$F{Sender Name}]]></textFieldExpression>
					</textField>
					<textField>
						<reportElement uuid="ba66dd6e-725a-44b2-9c22-4e628f32ee55" x="35" y="3" width="9" height="54"/>
						<textElement rotation="Right">
							<font size="6"/>
						</textElement>
						<textFieldExpression><![CDATA[$F{SAddress1}]]></textFieldExpression>
					</textField>
					<textField>
						<reportElement uuid="f5ad7a1d-81a4-4a87-b055-a936f2fd45f1" x="26" y="3" width="9" height="54"/>
						<textElement rotation="Right">
							<font size="6"/>
						</textElement>
						<textFieldExpression><![CDATA[$F{STown}]]></textFieldExpression>
					</textField>
					<textField>
						<reportElement uuid="147fee3c-bf98-471b-b8f6-eb19af4ef80f" x="17" y="3" width="9" height="54"/>
						<textElement rotation="Right">
							<font size="6"/>
						</textElement>
						<textFieldExpression><![CDATA[$F{SendPostcode}]]></textFieldExpression>
					</textField>
					<textField>
						<reportElement uuid="c75f44b6-afda-4e53-b704-9545a91eb769" x="54" y="80" width="9" height="35"/>
						<textElement textAlignment="Center" rotation="Right">
							<font size="6" isBold="false"/>
						</textElement>
						<textFieldExpression><![CDATA[$F{Account}]]></textFieldExpression>
					</textField>
					<textField>
						<reportElement uuid="0270e608-5509-4c4c-9cba-554ac11f861c" x="8" y="36" width="9" height="55"/>
						<textElement rotation="Right">
							<font size="6"/>
						</textElement>
						<textFieldExpression><![CDATA[$F{SenderPhone}]]></textFieldExpression>
					</textField>
					<textField>
						<reportElement uuid="7bd4fefd-066d-4b46-a9cf-f1f197391ea0" x="0" y="36" width="9" height="55"/>
						<textElement rotation="Right">
							<font size="6"/>
						</textElement>
						<textFieldExpression><![CDATA[$F{SenderRef}]]></textFieldExpression>
					</textField>
				</frame>
				<frame>
					<reportElement uuid="e73eb041-7aed-4e14-a73a-59c89b587e03" x="231" y="10" width="65" height="115"/>
					<box>
						<pen lineWidth="0.75"/>
						<topPen lineWidth="0.75"/>
						<leftPen lineWidth="0.75"/>
						<bottomPen lineWidth="0.75"/>
						<rightPen lineWidth="0.75"/>
					</box>
					<rectangle>
						<reportElement uuid="b264c14d-95a2-47e6-b2de-b47fbb50bc3b" x="0" y="0" width="65" height="115"/>
					</rectangle>
					<staticText>
						<reportElement uuid="f4940745-5b24-41d3-ae57-0bc82ec8ccbd" x="30" y="39" width="23" height="38"/>
						<textElement textAlignment="Center" verticalAlignment="Middle" rotation="Right">
							<font size="16" isBold="true"/>
						</textElement>
						<text><![CDATA[DPD]]></text>
					</staticText>
					<staticText>
						<reportElement uuid="f73567c2-ed31-4a2a-ad74-1cce1bc53234" x="12" y="16" width="18" height="85"/>
						<textElement textAlignment="Center" verticalAlignment="Middle" rotation="Right">
							<font size="11" isBold="false"/>
						</textElement>
						<text><![CDATA[www.dpd.co.uk]]></text>
					</staticText>
				</frame>
			</frame>
		</band>
		<band height="75">
			<rectangle>
				<reportElement uuid="87345e2d-66d4-443e-9e82-c0d30ca03d76" x="0" y="0" width="296" height="75"/>
			</rectangle>
			<textField>
				<reportElement uuid="d7e8476c-ce2d-4669-9a45-d5a9921779c5" x="67" y="21" width="162" height="33"/>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font size="6"/>
				</textElement>
				<textFieldExpression><![CDATA["<BAR=code128>" + "%" + $F{BarcodePostcode} + $F{Depot Number} + $F{TN1} + $F{TN2} + $F{TN3nocheck} +$F{reverseit} + $F{CountryCode}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="c1d1652c-e0a5-4833-8b95-1a8a8437b756" x="76" y="58" width="144" height="13"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{BarcodePostcode} + $F{Depot Number} + $F{TN1} + $F{TN2} + $F{TN3nocheck} + $F{reverseit} + $F{CountryCode} + $F{checkdigit}]]></textFieldExpression>
			</textField>
		</band>
		<band height="163">
			<frame>
				<reportElement uuid="8094eae2-88aa-4024-a224-33bc93cf7498" x="0" y="0" width="296" height="163" backcolor="#000000"/>
				<box>
					<pen lineWidth="1.5"/>
					<topPen lineWidth="1.5"/>
					<leftPen lineWidth="1.5"/>
					<bottomPen lineWidth="1.5"/>
					<rightPen lineWidth="1.5"/>
				</box>
				<rectangle>
					<reportElement uuid="828431d2-f9ce-41c0-a14a-f0e2351d623a" x="202" y="0" width="94" height="25" backcolor="#000000"/>
				</rectangle>
				<staticText>
					<reportElement uuid="e844f57c-5292-4792-9c8a-4ddd359a7029" x="266" y="25" width="30" height="10" forecolor="#999999"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="7"/>
					</textElement>
					<text><![CDATA[Service]]></text>
				</staticText>
				<staticText>
					<reportElement uuid="4a02eb5a-7f01-42b7-9c84-4cb5d0441568" x="3" y="23" width="23" height="10" forecolor="#999999"/>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font size="7"/>
					</textElement>
					<text><![CDATA[Track]]></text>
				</staticText>
				<textField>
					<reportElement uuid="c16ec8c8-e5c1-4091-9182-aa8bfdbe9430" x="28" y="89" width="240" height="57"/>
					<textElement textAlignment="Left" verticalAlignment="Middle">
						<font size="6"/>
					</textElement>
					<textFieldExpression><![CDATA["<BAR=code128>" + "%" + $F{BarcodePostcode} + $F{Depot Number} + $F{TN1}
+ $F{TN2} + $F{TN3nocheck} + $F{Service Code} + $F{CountryCode}]]></textFieldExpression>
				</textField>
				<line>
					<reportElement uuid="7dd66092-1adc-48be-bce0-a835ad01449e" x="0" y="0" width="202" height="1"/>
					<graphicElement>
						<pen lineWidth="2.5"/>
					</graphicElement>
				</line>
				<textField>
					<reportElement uuid="f31578e9-0ce7-4c98-ba9f-57d04804744d" x="202" y="1" width="94" height="24" forecolor="#FFFFFF"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="17" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{Service Description}]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement uuid="f79b095c-1f6c-499e-aa4e-7fe2dd4c2c2e" x="84" y="37" width="46" height="27"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="20" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{SCountry} + " -"]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement uuid="c31f8eb5-da21-4468-928d-b669afc489f8" x="84" y="63" width="129" height="12"/>
					<textElement textAlignment="Center">
						<font size="7"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{Service Code} + " - " + $F{SCountry} + " - " + $F{SPostcode}]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement uuid="974ff616-5015-4cb3-ba09-a61336a78ecf" x="130" y="44" width="83" height="20"/>
					<textElement textAlignment="Right" verticalAlignment="Top">
						<font size="14" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{Postcode}]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement uuid="3d84fab0-9488-41a2-9440-6436a869e0d6" x="84" y="74" width="129" height="12"/>
					<textElement textAlignment="Center">
						<font size="6" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{Current Date} + "  " + $F{Current Time} + "  Routing Ver: " + $F{Routing Version}]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement uuid="98a8a3e8-4c3c-4dbd-badd-b33100782390" x="3" y="2" width="45" height="22"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="16" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{Depot Number}]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement uuid="5c17f022-f81d-4cc9-8686-b87f6a048874" x="48" y="2" width="120" height="22"/>
					<textElement textAlignment="Left" verticalAlignment="Middle">
						<font size="13" isBold="false"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{TN1}+" "+$F{TN2}+" "+$F{TN3}]]></textFieldExpression>
				</textField>
				<textField pattern="">
					<reportElement uuid="1113203f-0930-4359-a419-df7e53d4e55a" x="76" y="146" width="144" height="13"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="7"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{BarcodePostcode} + $F{Depot Number} + $F{TN1} + $F{TN2} + $F{TN3nocheck} + $F{Service Code} + $F{CountryCode} + $F{checkdigit}]]></textFieldExpression>
				</textField>
			</frame>
		</band>
	</detail>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
