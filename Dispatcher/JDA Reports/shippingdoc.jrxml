<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="shippingdoc" pageWidth="842" pageHeight="595" orientation="Landscape" whenNoDataType="AllSectionsNoDetail" columnWidth="782" leftMargin="30" rightMargin="30" topMargin="20" bottomMargin="20" uuid="1c06bb6b-9ff5-4764-9927-da0be40004f8">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="language.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<import value="oracle.sql.*"/>
	<template><![CDATA[$P{JR_TEMPLATE_DIR} + "/default_styles.jrtx"]]></template>
	<style name="Arial Unicode" isDefault="true" fontName="Arial Unicode MS" fontSize="12" isUnderline="false"/>
	<style name="Arial" fontName="Arial" fontSize="12" isUnderline="false"/>
	<parameter name="shipment_number" class="java.lang.String">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<parameter name="JR_TEMPLATE_DIR" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["Q:/gittrunkrepos/dispatcher/server/run/jrtemplates"]]></defaultValueExpression>
	</parameter>
	<parameter name="JR_LANGUAGE" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["EN_GB"]]></defaultValueExpression>
	</parameter>
	<parameter name="JR_TIME_ZONE_NAME" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["Europe/London"]]></defaultValueExpression>
	</parameter>
	<parameter name="order_id" class="java.lang.String"/>
	<parameter name="client_id" class="java.lang.String"/>
	<queryString>
		<![CDATA[WITH manifests AS
  (SELECT DISTINCT order_id,
    client_id
  FROM shipping_manifest
  WHERE shipment_number = $P{shipment_number}
  UNION
  SELECT DISTINCT order_id, client_id
  FROM order_header
  WHERE order_id = $P{order_id}
  and client_id = $P{client_id}
  )
SELECT DISTINCT oh.order_id "order_id",
  oh.client_id "client_id",
  TO_CHAR(ol.line_id) "line_id",
  1 "sequence",
  ol.sku_id "sku_id",
  LibSKU.GetSKUDescription(ol.Client_ID, ol.SKU_ID, $P{JR_LANGUAGE}) "description",
  TO_CHAR(ol.qty_ordered) "qty_ordered",
  ol.tracking_level "tracking_level",
  TO_CHAR(ol.qty_tasked) "qty_tasked",
  TO_CHAR(ol.qty_picked) "qty_picked",
  TO_CHAR(ol.qty_shipped) "qty_shipped",
  oh.customer_id "customer_id",
  oh.ship_dock "ship_dock",
  sic.text "special_ins",
  ol.line_id sort
FROM Order_Header oh,
  Order_Line ol,
  manifests sm,
  special_ins_link sil,
  special_ins_code sic
WHERE sm.Order_ID             = oh.Order_ID
AND sm.Client_ID              = oh.Client_ID
AND oh.Client_ID              = ol.Client_ID
AND oh.Order_ID               = ol.Order_ID
AND ol.sku_id                 = sil.sku_id
AND ol.client_id              = sil.client_id
AND sil.code                  = sic.code
AND (sil.client_id            = sic.client_id
OR sic.client_id             IS NULL)
AND sil.sku_id               IS NOT NULL
AND NVL(sic.use_dstream, 'N') = 'Y'
UNION /*---------------------------------------------------------------------*/
SELECT DISTINCT oh.order_id "order_id",
  oh.client_id "client_id",
  TO_CHAR(ol.line_id) "line_id",
  sif.sequence,
  ol.sku_id "sku_id",
  LibSKU.GetSKUDescription(ol.Client_ID, SKU_ID, $P{JR_LANGUAGE}) "description",
  TO_CHAR(ol.qty_ordered) "qty_ordered",
  ol.tracking_level "tracking_level",
  TO_CHAR(ol.qty_tasked) "qty_tasked",
  TO_CHAR(ol.qty_picked) "qty_picked",
  TO_CHAR(ol.qty_shipped) "qty_shipped",
  oh.customer_id "customer_id",
  oh.ship_dock "ship_dock",
  sif.text "special_ins",
  ol.line_id sort
FROM Order_Header oh,
  Order_Line ol,
  manifests sm,
  (SELECT isi.text,
    isi.client_id,
    isi.reference_id,
    isi.sequence,
    isi.line_id
  FROM special_ins isi,
    manifests sm
  WHERE NVL(isi.use_dstream, 'N') = 'Y'
  AND isi.type                    = 'O'
  AND isi.code                   IS NULL
  AND sm.Order_ID                 = isi.reference_id
  AND sm.Client_ID                = isi.Client_ID
  AND isi.line_id                IS NOT NULL
  UNION
  SELECT sic.text,
    isi.client_id,
    isi.reference_id,
    isi.sequence,
    isi.line_id
  FROM special_ins isi,
    special_ins_code sic,
    manifests sm
  WHERE isi.code                = sic.code
  AND NVL(sic.use_dstream, 'N') = 'Y'
  AND NVL(isi.use_dstream, 'N') = 'Y'
  AND isi.type                  = 'O'
  AND sm.Order_ID               = isi.reference_id
  AND sm.Client_ID              = isi.Client_ID
  AND isi.line_id              IS NOT NULL
  AND isi.code                 IS NOT NULL
  ) sif
WHERE sm.Order_ID = oh.Order_ID
AND sm.Client_ID  = oh.Client_ID
AND oh.Client_ID  = ol.Client_ID
AND oh.Order_ID   = ol.Order_ID
AND ol.client_id  = sif.client_id (+)
AND ol.order_id   = sif.reference_id (+)
AND ol.line_id    = sif.line_id (+)
ORDER BY 2,1,15]]>
	</queryString>
	<field name="order_id" class="java.lang.String"/>
	<field name="client_id" class="java.lang.String"/>
	<field name="line_id" class="java.lang.String"/>
	<field name="sequence" class="java.math.BigDecimal"/>
	<field name="sku_id" class="java.lang.String"/>
	<field name="description" class="java.lang.String"/>
	<field name="qty_ordered" class="java.lang.String"/>
	<field name="tracking_level" class="java.lang.String"/>
	<field name="qty_tasked" class="java.lang.String"/>
	<field name="qty_picked" class="java.lang.String"/>
	<field name="qty_shipped" class="java.lang.String"/>
	<field name="customer_id" class="java.lang.String"/>
	<field name="ship_dock" class="java.lang.String"/>
	<field name="special_ins" class="java.lang.String"/>
	<field name="SORT" class="java.math.BigDecimal"/>
	<variable name="lb_of_page_count" class="java.lang.String">
		<variableExpression><![CDATA[" " + LangData.get("page_of") + " " + $V{PAGE_NUMBER}]]></variableExpression>
	</variable>
	<variable name="lb_page_page_number" class="java.lang.String">
		<variableExpression><![CDATA[LangData.get("page_name") + " " + $V{PAGE_NUMBER}]]></variableExpression>
	</variable>
	<group name="order" isStartNewPage="true" isResetPageNumber="true">
		<groupExpression><![CDATA[$F{order_id}]]></groupExpression>
		<groupHeader>
			<band height="184" splitType="Stretch">
				<rectangle>
					<reportElement uuid="31cd65b4-00a9-4cc9-8b40-3a4c02722987" key="rectangle-2" style="rptHeaderRectangle" x="0" y="143" width="781" height="40"/>
				</rectangle>
				<textField evaluationTime="Group" evaluationGroup="order" isBlankWhenNull="false">
					<reportElement uuid="91d5b2bb-8b11-4dd8-869c-fcfc8406ead8" key="textField" x="0" y="53" width="285" height="20"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial Unicode MS" size="10"/>
					</textElement>
					<textFieldExpression><![CDATA[LangData.get("client_id") + ": " + ($F{client_id}==null?"":$F{client_id})]]></textFieldExpression>
				</textField>
				<textField evaluationTime="Group" evaluationGroup="order" isBlankWhenNull="false">
					<reportElement uuid="3e80577c-4cf2-430f-a99b-d3de954c6aef" key="textField-11" x="0" y="73" width="285" height="20"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial Unicode MS" size="10"/>
					</textElement>
					<textFieldExpression><![CDATA[LangData.get("order_id") + ": " + ($F{order_id}==null?"":$F{order_id})]]></textFieldExpression>
				</textField>
				<textField evaluationTime="Group" evaluationGroup="order" isBlankWhenNull="false">
					<reportElement uuid="8fc2c504-2850-4357-981d-581f75a0100b" key="textField-12" x="0" y="92" width="285" height="20"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial Unicode MS" size="10"/>
					</textElement>
					<textFieldExpression><![CDATA[LangData.get("customer_id") + ": " + ($F{customer_id}==null?"":$F{customer_id})]]></textFieldExpression>
				</textField>
				<textField evaluationTime="Group" evaluationGroup="order" isBlankWhenNull="false">
					<reportElement uuid="4ef1b998-f41f-4869-9546-c7ea3c98c944" key="textField-13" x="0" y="112" width="285" height="20"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial Unicode MS" size="10"/>
					</textElement>
					<textFieldExpression><![CDATA[LangData.get("ship_dock") + ": " + ($F{ship_dock}==null?"":$F{ship_dock})]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement uuid="13e29aa9-6c40-494f-a8fa-71a03d9a59e3" key="textField-14" style="rptTitle" x="688" y="143" width="92" height="20"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Left" verticalAlignment="Middle">
						<font fontName="Arial Unicode MS" size="10" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[LangData.get("qty_picked")]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement uuid="753979c0-9afe-4fc4-8950-727931a1f13c" key="textField-15" style="rptTitle" x="562" y="144" width="100" height="20"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Left" verticalAlignment="Middle">
						<font fontName="Arial Unicode MS" size="10" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[LangData.get("qty_ordered")]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement uuid="be0ec083-03f7-4d44-ac7a-3d94a8a88bef" key="textField-16" style="rptTitle" x="119" y="143" width="32" height="20"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Left" verticalAlignment="Middle">
						<font fontName="Arial Unicode MS" size="10" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[LangData.get("sku_id")]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement uuid="a9c7c25c-cd57-4d2b-9123-6537e25c2f66" key="textField-17" style="rptTitle" x="170" y="143" width="387" height="20"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Left" verticalAlignment="Middle">
						<font fontName="Arial Unicode MS" size="10" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[LangData.get("description")]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement uuid="60c416ab-ffdb-4492-8bea-c7757c911d27" key="textField-18" style="rptTitle" x="54" y="143" width="50" height="20"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Left" verticalAlignment="Middle">
						<font fontName="Arial Unicode MS" size="10" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[LangData.get("tracking_level")]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement uuid="f00aed91-e862-4965-b5e3-8e00b7db175f" key="textField-19" style="rptTitle" x="562" y="164" width="100" height="20"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Left" verticalAlignment="Middle">
						<font fontName="Arial Unicode MS" size="10" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[LangData.get("qty_tasked")]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement uuid="229aaed0-208c-4cab-b5e0-b7bbae9561f0" key="textField-20" style="rptTitle" x="688" y="164" width="92" height="20"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Left" verticalAlignment="Middle">
						<font fontName="Arial Unicode MS" size="10" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[LangData.get("qty_shipped")]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement uuid="e5be521c-9e2f-44fd-9e3b-cd078a96e94a" key="textField-21" style="rptTitle" x="3" y="144" width="40" height="20"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Left" verticalAlignment="Middle">
						<font fontName="Arial Unicode MS" size="10" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[LangData.get("line_id")]]></textFieldExpression>
				</textField>
				<subreport isUsingCache="true">
					<reportElement uuid="43fb9213-056a-4838-b5f5-797425400cf8" key="subreport-1" x="0" y="0" width="782" height="33"/>
					<subreportParameter name="JR_TEMPLATE_DIR">
						<subreportParameterExpression><![CDATA[$P{JR_TEMPLATE_DIR}]]></subreportParameterExpression>
					</subreportParameter>
					<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
					<subreportExpression><![CDATA[$P{JR_TEMPLATE_DIR} + "/titleheaderlandscape.jasper"]]></subreportExpression>
				</subreport>
				<textField isBlankWhenNull="true">
					<reportElement uuid="3b8363de-b3ee-4f78-aa90-4432cfb74a4f" key="textField" x="249" y="33" width="285" height="20" forecolor="#000000"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font fontName="Arial Unicode MS" size="12" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[LangData.get("WCM05803")]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement uuid="7ee17b7e-b6a9-48a6-9aab-86ea33337d20" key="textField-25" style="rptTitle" x="688" y="-18" width="92" height="20"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Left" verticalAlignment="Middle">
						<font fontName="Arial Unicode MS" size="10" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[LangData.get("qty_shipped")]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement uuid="9f26d86a-05bb-4c89-bec4-5b44f42ba247" key="textField-27" style="rptTitle" x="562" y="-18" width="100" height="20"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Left" verticalAlignment="Middle">
						<font fontName="Arial Unicode MS" size="10" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[LangData.get("qty_tasked")]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement uuid="7ab13fdd-870a-425b-bfe6-32e5f8378322" key="textField-73" style="rptTitle" x="3" y="163" width="554" height="20"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Left" verticalAlignment="Middle">
						<font fontName="Arial Unicode MS" size="10" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[LangData.get("Special Instructions")]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band splitType="Stretch"/>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band splitType="Stretch"/>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band height="40" splitType="Stretch">
			<printWhenExpression><![CDATA[new Boolean(($V{PAGE_NUMBER}.intValue() > 1))]]></printWhenExpression>
			<rectangle>
				<reportElement uuid="3f5f5509-993f-4001-852c-289c81254e29" key="rectangle-3" style="rptHeaderRectangle" x="0" y="0" width="781" height="40"/>
			</rectangle>
			<textField isBlankWhenNull="true">
				<reportElement uuid="26d41764-1cbb-45d1-9a06-2cdd22f3f307" key="textField-26" style="rptTitle" x="688" y="1" width="92" height="20"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left">
					<font fontName="Arial Unicode MS" size="10"/>
				</textElement>
				<textFieldExpression><![CDATA[LangData.get("qty_picked")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement uuid="8d829eb1-d5af-4037-9626-09773533ad49" key="textField-28" style="rptTitle" x="562" y="2" width="100" height="20"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left">
					<font fontName="Arial Unicode MS" size="10"/>
				</textElement>
				<textFieldExpression><![CDATA[LangData.get("qty_ordered")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement uuid="b8d748e8-e1ad-45d1-ad52-4da7e38a2698" key="textField-29" style="rptTitle" x="432" y="2" width="100" height="20"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left">
					<font fontName="Arial Unicode MS" size="10"/>
				</textElement>
				<textFieldExpression><![CDATA[LangData.get("tracking_level")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement uuid="56201753-74b1-439e-af4b-35885db1d737" key="textField-30" style="rptTitle" x="208" y="2" width="220" height="20"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left">
					<font fontName="Arial Unicode MS" size="10"/>
				</textElement>
				<textFieldExpression><![CDATA[LangData.get("description")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement uuid="1b1d352f-1885-473d-aee3-af662e9eb485" key="textField-31" style="rptTitle" x="84" y="2" width="124" height="20"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left">
					<font fontName="Arial Unicode MS" size="10"/>
				</textElement>
				<textFieldExpression><![CDATA[LangData.get("sku_id")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement uuid="8c9ec385-824c-4745-a7a4-d02a6dcf842b" key="textField-32" style="rptTitle" x="3" y="2" width="77" height="20"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left">
					<font fontName="Arial Unicode MS" size="10"/>
				</textElement>
				<textFieldExpression><![CDATA[LangData.get("line_id")]]></textFieldExpression>
			</textField>
		</band>
	</columnHeader>
	<detail>
		<band height="40" splitType="Stretch">
			<rectangle>
				<reportElement uuid="3223f56c-d34d-463f-a188-4a8db09b15b7" key="rectangle-1" stretchType="RelativeToBandHeight" x="0" y="0" width="781" height="40" forecolor="#F5F3F0" backcolor="#EEEEEE">
					<printWhenExpression><![CDATA[new Boolean($V{REPORT_COUNT}.intValue() % 2 == 0)]]></printWhenExpression>
				</reportElement>
			</rectangle>
			<textField pattern="##0" isBlankWhenNull="true">
				<reportElement uuid="917f5e85-df13-4d32-a6d9-30afdfa2d5ef" key="textField" x="688" y="0" width="92" height="20"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[($F{qty_picked} == null || $F{qty_picked}.equals("0")) ? "0.0" : $F{qty_picked}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="##0.00" isBlankWhenNull="true">
				<reportElement uuid="141d2b11-9488-4492-a41b-076cc917790b" key="textField" x="119" y="-1" width="51" height="20"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{sku_id}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement uuid="d426ace1-e5a3-4bed-8bbf-d14df76d0eca" key="textField" x="562" y="0" width="100" height="20"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[($F{qty_ordered} == null || $F{qty_ordered}.equals("0")) ? "0.0" : $F{qty_ordered}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="##0.00" isBlankWhenNull="true">
				<reportElement uuid="f910eb1c-c27c-4ec9-9d7f-82087c60ce02" key="textField-1" x="170" y="-1" width="387" height="20"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top">
					<font fontName="Arial Unicode MS" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{description}]]></textFieldExpression>
			</textField>
			<textField pattern="##0.00" isBlankWhenNull="true">
				<reportElement uuid="6ea344cf-8d71-417e-b4ee-8c7afb4c5571" key="textField-3" x="54" y="-1" width="50" height="20"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{tracking_level}]]></textFieldExpression>
			</textField>
			<textField pattern="##0.00" isBlankWhenNull="true">
				<reportElement uuid="412955d7-02ad-425b-8b16-08cf8a4ee8f7" key="textField-5" x="562" y="20" width="100" height="20"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[($F{qty_tasked} == null || $F{qty_tasked}.equals("0")) ? "0.0" : $F{qty_tasked}]]></textFieldExpression>
			</textField>
			<textField pattern="##0.00" isBlankWhenNull="true">
				<reportElement uuid="c7139cd0-31cb-4c9b-b0fc-5fb46ca41114" key="textField-7" x="688" y="20" width="92" height="20"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[($F{qty_shipped} == null || $F{qty_shipped}.equals("0")) ? "0.0" : $F{qty_shipped}]]></textFieldExpression>
			</textField>
			<textField pattern="##0" isBlankWhenNull="true">
				<reportElement uuid="6a745359-f0e4-4bc4-b975-026465bbd6ab" key="textField-9" x="3" y="0" width="40" height="20"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{line_id}]]></textFieldExpression>
			</textField>
			<textField pattern="##0.00" isBlankWhenNull="true">
				<reportElement uuid="39275163-aaf5-4a58-80d0-686b45d1eaa8" key="textField-74" x="3" y="20" width="551" height="19"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top">
					<font fontName="Arial Unicode MS" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{special_ins}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="24" splitType="Stretch">
			<subreport isUsingCache="true">
				<reportElement uuid="0675070c-436c-4c46-a602-d7672b4ae450" key="subreport-2" style="Arial Unicode" x="0" y="0" width="782" height="22"/>
				<subreportParameter name="JR_TEMPLATE_DIR">
					<subreportParameterExpression><![CDATA[$P{JR_TEMPLATE_DIR}]]></subreportParameterExpression>
				</subreportParameter>
				<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
				<subreportExpression><![CDATA[$P{JR_TEMPLATE_DIR} + "/footerlandscape.jasper"]]></subreportExpression>
			</subreport>
			<textField evaluationTime="Report" isBlankWhenNull="true">
				<reportElement uuid="12f18384-55df-4438-b1cf-9afe80c528c3" key="textField-71" style="Arial Unicode" x="734" y="3" width="42" height="20"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10"/>
				</textElement>
				<textFieldExpression><![CDATA["" + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Page" isBlankWhenNull="true">
				<reportElement uuid="5cc16c86-2300-4d4b-9fb2-7a99a638868c" key="textField-72" style="Arial Unicode" x="531" y="3" width="202" height="20"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10"/>
				</textElement>
				<textFieldExpression><![CDATA[LangData.get("page_name") + " " + $V{PAGE_NUMBER} + " " + LangData.get("page_of") + " "]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
