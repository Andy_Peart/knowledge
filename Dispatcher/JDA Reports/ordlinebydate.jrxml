<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="ordlinebydate" pageWidth="842" pageHeight="595" orientation="Landscape" whenNoDataType="AllSectionsNoDetail" columnWidth="782" leftMargin="30" rightMargin="30" topMargin="20" bottomMargin="20">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="language.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<import value="oracle.sql.*"/>
	<template><![CDATA[$P{JR_TEMPLATE_DIR} + "/default_styles.jrtx"]]></template>
	<style name="Arial Unicode" isDefault="true" fontName="Arial Unicode MS" fontSize="12" isUnderline="false"/>
	<style name="Arial" fontName="Arial" fontSize="12" isUnderline="false"/>
	<parameter name="site_id" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<parameter name="site_group" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<parameter name="JR_TEMPLATE_DIR" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["Q:/gittrunkrepos/dispatcher/server/run/jrtemplates"]]></defaultValueExpression>
	</parameter>
	<parameter name="JR_LANGUAGE" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["EN_GB"]]></defaultValueExpression>
	</parameter>
	<parameter name="JR_TIME_ZONE_NAME" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["Europe/London"]]></defaultValueExpression>
	</parameter>
	<parameter name="owner_id" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<parameter name="client_id" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<parameter name="client_group" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<parameter name="fromdate" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<parameter name="todate" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT  OH.From_Site_ID "from_site_id", 
	OH.Client_ID "client_id",
        OH.Owner_ID "owner_id", 
	OL.SKU_ID "sku_id",
	LibSKU.GetSKUDescription (OL.Client_ID, OL.SKU_ID, $P{JR_LANGUAGE}) "description",
        OH.Order_ID "order_id", 
	OL.Line_ID "line_id",
        OL.Tracking_Level "tracking_level",
        OL.Qty_Ordered "qty_ordered",
        OL.Qty_Shipped "qty_shipped",
        TO_CHAR(OH.Order_Date, 'DD/MM/YYYY HH24:MI:SS') "order_date",
        TO_CHAR(OH.Ship_By_Date, 'DD/MM/YYYY HH24:MI:SS') "ship_by_date",
        OH.Name "name",
	OH.Customer_ID "customer_id", 
	OH.Consignment "consignment"
FROM Order_Header OH, Order_Line OL
WHERE OH.Order_ID = OL.Order_ID
AND OH.Client_ID = OL.Client_ID
AND OH.Order_Date >= TO_DATE($P{fromdate}, 'DD/MM/YYYY')
AND OH.Order_Date < TO_DATE($P{todate}, 'DD/MM/YYYY') + 1
AND (($P{site_id} IS NULL AND $P{site_group} IS NULL) OR  
     (OH.From_Site_ID = $P{site_id}) OR  
     ($P{site_id} IS NULL AND OH.From_Site_ID IN (SELECT Site_ID
					          FROM Site_Group_Sites
					          WHERE Site_Group = $P{site_group})))
AND (($P{owner_id} IS NULL) OR (OH.Owner_ID = $P{owner_id}))
AND ((($P{client_id} IS NULL) AND ($P{client_group} IS NULL))
OR  (OH.Client_ID = $P{client_id})
OR  (($P{client_id} IS NULL) AND OH.Client_ID IN (SELECT Client_ID
						  FROM Client_Group_Clients
						  WHERE Client_Group = $P{client_group})))
ORDER BY OH.From_Site_ID, OH.Client_ID, OH.Owner_ID, OH.Order_Date, OH.Order_ID, OL.Line_ID]]>
	</queryString>
	<field name="from_site_id" class="java.lang.String"/>
	<field name="client_id" class="java.lang.String"/>
	<field name="owner_id" class="java.lang.String"/>
	<field name="sku_id" class="java.lang.String"/>
	<field name="description" class="java.lang.String"/>
	<field name="order_id" class="java.lang.String"/>
	<field name="line_id" class="java.math.BigDecimal"/>
	<field name="tracking_level" class="java.lang.String"/>
	<field name="qty_ordered" class="java.math.BigDecimal"/>
	<field name="qty_shipped" class="java.math.BigDecimal"/>
	<field name="order_date" class="java.lang.String"/>
	<field name="ship_by_date" class="java.lang.String"/>
	<field name="name" class="java.lang.String"/>
	<field name="customer_id" class="java.lang.String"/>
	<field name="consignment" class="java.lang.String"/>
	<group name="from_site_id" isStartNewPage="true">
		<groupExpression><![CDATA[$F{from_site_id}]]></groupExpression>
		<groupHeader>
			<band splitType="Stretch"/>
		</groupHeader>
		<groupFooter>
			<band splitType="Stretch"/>
		</groupFooter>
	</group>
	<group name="order_id">
		<groupExpression><![CDATA[$F{order_id}]]></groupExpression>
		<groupHeader>
			<band splitType="Stretch"/>
		</groupHeader>
		<groupFooter>
			<band splitType="Stretch"/>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="86" splitType="Stretch">
			<textField isBlankWhenNull="false">
				<reportElement key="textField-1" style="Arial Unicode" x="110" y="34" width="558" height="24"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Bottom">
					<font fontName="Arial Unicode MS" size="12" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("ordlinebydate")]]></textFieldExpression>
			</textField>
			<subreport isUsingCache="true">
				<reportElement key="subreport-1" style="Arial Unicode" x="0" y="0" width="782" height="33"/>
				<subreportParameter name="JR_TEMPLATE_DIR">
					<subreportParameterExpression><![CDATA[$P{JR_TEMPLATE_DIR}]]></subreportParameterExpression>
				</subreportParameter>
				<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
				<subreportExpression class="java.lang.String"><![CDATA[$P{JR_TEMPLATE_DIR} + "/titleheaderlandscape.jasper"]]></subreportExpression>
			</subreport>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-46" style="Arial Unicode" x="110" y="59" width="558" height="24"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="11" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.date($P{fromdate}) + " " + LangData.get("to") + " " + LangData.date($P{todate})]]></textFieldExpression>
			</textField>
		</band>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band height="39" splitType="Stretch">
			<rectangle>
				<reportElement key="rectangle-1" style="rptHeaderRectangle" x="0" y="0" width="781" height="39"/>
				<graphicElement>
					<pen lineWidth="0.25" lineStyle="Solid"/>
				</graphicElement>
			</rectangle>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-5" style="rptTitle" x="4" y="2" width="73" height="15"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("site_id")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-6" style="rptTitle" x="80" y="2" width="73" height="15"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("owner_id")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-7" style="rptTitle" x="80" y="20" width="73" height="15"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("order_date")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-10" style="rptTitle" x="156" y="2" width="160" height="15"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("customer_id")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-11" style="rptTitle" x="318" y="2" width="115" height="15"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("order_id")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-13" style="rptTitle" x="156" y="20" width="160" height="15"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("name")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-14" style="rptTitle" x="603" y="20" width="87" height="15"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("tracking_level")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-16" style="rptTitle" x="435" y="20" width="165" height="15"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("description")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-17" style="rptTitle" x="435" y="2" width="165" height="15"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("sku_id")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-38" style="rptTitle" x="4" y="20" width="73" height="15"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("client_id")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-43" style="rptTitle" x="603" y="2" width="87" height="15"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("qty_ordered")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-51" style="rptTitle" x="318" y="20" width="115" height="15"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("line_id")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-58" style="rptTitle" x="693" y="2" width="87" height="15"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("qty_shipped")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-62" style="rptTitle" x="693" y="20" width="87" height="15"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("ship_by_date")]]></textFieldExpression>
			</textField>
		</band>
	</columnHeader>
	<detail>
		<band height="40" splitType="Stretch">
			<rectangle>
				<reportElement key="rectangle-2" style="Arial Unicode" stretchType="RelativeToBandHeight" x="0" y="0" width="781" height="40" forecolor="#F5F3F0" backcolor="#EEEEEE">
					<printWhenExpression><![CDATA[new Boolean($V{REPORT_COUNT}.intValue() % 2 == 0)]]></printWhenExpression>
				</reportElement>
				<graphicElement>
					<pen lineWidth="0.25" lineStyle="Solid"/>
				</graphicElement>
			</rectangle>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-20" isPrintRepeatedValues="false" x="4" y="2" width="73" height="18" printWhenGroupChanges="order_id"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{from_site_id}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-21" isPrintRepeatedValues="false" x="80" y="2" width="73" height="18" printWhenGroupChanges="order_id"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{owner_id}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-25" isPrintRepeatedValues="false" x="156" y="2" width="160" height="18" printWhenGroupChanges="order_id"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{customer_id}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-28" isPrintRepeatedValues="false" x="156" y="21" width="160" height="18"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{name}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-39" isPrintRepeatedValues="false" x="4" y="21" width="73" height="18" printWhenGroupChanges="order_id"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{client_id}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-47" isPrintRepeatedValues="false" x="318" y="2" width="115" height="18"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{order_id}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-52" isPrintRepeatedValues="false" x="318" y="21" width="32" height="18"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="8"/>
				</textElement>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{line_id}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-53" isPrintRepeatedValues="false" x="80" y="21" width="73" height="18" printWhenGroupChanges="order_id"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.date($F{order_date})]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-54" isPrintRepeatedValues="false" x="435" y="2" width="165" height="18" printWhenGroupChanges="order_id"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{sku_id}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-55" isPrintRepeatedValues="false" x="435" y="21" width="165" height="18" printWhenGroupChanges="order_id"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{description}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-56" isPrintRepeatedValues="false" x="603" y="2" width="70" height="18" printWhenGroupChanges="order_id"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.number($F{qty_ordered})]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-57" isPrintRepeatedValues="false" x="603" y="21" width="87" height="18" printWhenGroupChanges="order_id"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{tracking_level}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-60" isPrintRepeatedValues="false" x="693" y="2" width="70" height="18" printWhenGroupChanges="order_id"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.number($F{qty_shipped})]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-61" isPrintRepeatedValues="false" x="693" y="21" width="87" height="18" printWhenGroupChanges="order_id"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.date($F{ship_by_date})]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="24" splitType="Stretch">
			<subreport isUsingCache="true">
				<reportElement key="subreport-2" style="Arial Unicode" x="0" y="0" width="782" height="22"/>
				<subreportParameter name="JR_TEMPLATE_DIR">
					<subreportParameterExpression><![CDATA[$P{JR_TEMPLATE_DIR}]]></subreportParameterExpression>
				</subreportParameter>
				<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
				<subreportExpression class="java.lang.String"><![CDATA[$P{JR_TEMPLATE_DIR} + "/footerlandscape.jasper"]]></subreportExpression>
			</subreport>
			<textField evaluationTime="Report" isBlankWhenNull="true">
				<reportElement key="textField-71" style="Arial Unicode" x="734" y="3" width="42" height="20"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA["" + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Page" isBlankWhenNull="true">
				<reportElement key="textField-72" style="Arial Unicode" x="531" y="3" width="202" height="20"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("page_name") + " " + $V{PAGE_NUMBER} + " " + LangData.get("page_of") + " "]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
