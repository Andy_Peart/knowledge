<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="locationusagemeter" pageWidth="425" pageHeight="283" orientation="Landscape" whenNoDataType="AllSectionsNoDetail" columnWidth="425" leftMargin="0" rightMargin="0" topMargin="0" bottomMargin="0">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="language.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<import value="oracle.sql.*"/>
	<template><![CDATA[$P{JR_TEMPLATE_DIR} + "/default_styles.jrtx"]]></template>
	<style name="Arial Unicode" isDefault="true" fontName="Arial Unicode MS" fontSize="12" isUnderline="false"/>
	<style name="Arial" isDefault="false" fontName="Arial" fontSize="12" isUnderline="false"/>
	<parameter name="JR_TEMPLATE_DIR" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["Q:/gittrunkrepos/dispatcher/server/run/jrtemplates"]]></defaultValueExpression>
	</parameter>
	<parameter name="JR_LANGUAGE" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["EN_GB"]]></defaultValueExpression>
	</parameter>
	<parameter name="JR_TIME_ZONE_NAME" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["Europe/London"]]></defaultValueExpression>
	</parameter>
	<parameter name="site_id" class="java.lang.String">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<parameter name="site_group" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT  MV.Max_Volume/1000 "max_volume",
        MV.Current_Volume/1000 "current_volume"
FROM
	(
	SELECT SUM(Volume) Max_Volume, SUM(Current_Volume) Current_Volume
	FROM Location 
	WHERE (($P{site_id} IS NULL AND $P{site_group} IS NULL) OR  
	       (Site_ID = $P{site_id}) OR  
	       ($P{site_id} IS NULL AND Site_ID IN (SELECT Site_ID
						    FROM Site_Group_Sites
						    WHERE Site_Group = $P{site_group})))
	AND Loc_Type IN ('Tag-Operator', 'Tag-FIFO', 'Tag-LIFO', 'Bulk', 'Bin')
	) MV]]>
	</queryString>
	<field name="max_volume" class="java.math.BigDecimal"/>
	<field name="current_volume" class="java.math.BigDecimal"/>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="25" splitType="Stretch">
			<textField isBlankWhenNull="false">
				<reportElement key="textField-1" style="Arial Unicode" x="0" y="0" width="419" height="24"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Bottom">
					<font fontName="Arial Unicode MS" size="14" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("locationusagemeter")]]></textFieldExpression>
			</textField>
		</band>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band height="255" splitType="Prevent">
			<meterChart>
				<chart isShowLegend="false">
					<reportElement key="element-1" style="Arial Unicode" x="0" y="0" width="419" height="248"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<chartTitle/>
					<chartSubtitle/>
					<chartLegend/>
				</chart>
				<valueDataset>
					<valueExpression><![CDATA[$F{current_volume}]]></valueExpression>
				</valueDataset>
				<meterPlot shape="chord" meterColor="#FFFFFF" needleColor="#000000" tickColor="#333333">
					<plot backcolor="#CCCCCC" backgroundAlpha="0.0"/>
					<tickLabelFont/>
					<valueDisplay color="#003366">
						<font fontName="Arial Unicode MS" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false"/>
					</valueDisplay>
					<dataRange>
						<lowExpression><![CDATA[new Double(0.0)]]></lowExpression>
						<highExpression><![CDATA[new Double($F{max_volume}.doubleValue())]]></highExpression>
					</dataRange>
					<meterInterval label="Normal" color="#BFFFBF">
						<dataRange>
							<lowExpression><![CDATA[new Double(0)]]></lowExpression>
							<highExpression><![CDATA[new Double($F{max_volume}.longValue() * 0.8)]]></highExpression>
						</dataRange>
					</meterInterval>
					<meterInterval label="High" color="#FCCB97">
						<dataRange>
							<lowExpression><![CDATA[new Double($F{max_volume}.longValue() * 0.8)]]></lowExpression>
							<highExpression><![CDATA[new Double($F{max_volume}.longValue() * 0.95)]]></highExpression>
						</dataRange>
					</meterInterval>
					<meterInterval label="Critical" color="#FF7F7F" alpha="1.0">
						<dataRange>
							<lowExpression><![CDATA[new Double($F{max_volume}.longValue() * 0.95)]]></lowExpression>
							<highExpression><![CDATA[new Double($F{max_volume}.longValue())]]></highExpression>
						</dataRange>
					</meterInterval>
				</meterPlot>
			</meterChart>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-2" style="Arial Unicode" mode="Opaque" x="109" y="218" width="200" height="30" forecolor="#000000"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="14" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[($P{site_id} == null ? "" : $P{site_id}) + " " + LangData.get("current_volume") + " : " + $F{current_volume}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band splitType="Stretch"/>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
