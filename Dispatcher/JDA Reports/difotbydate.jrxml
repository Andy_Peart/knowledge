<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="difotbydate" pageWidth="842" pageHeight="595" orientation="Landscape" whenNoDataType="AllSectionsNoDetail" columnWidth="782" leftMargin="30" rightMargin="30" topMargin="20" bottomMargin="20">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="language.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<import value="oracle.sql.*"/>
	<template><![CDATA[$P{JR_TEMPLATE_DIR} + "/default_styles.jrtx"]]></template>
	<style name="Arial Unicode" isDefault="true" fontName="Arial Unicode MS" fontSize="12" isUnderline="false"/>
	<style name="Arial" isDefault="false" fontName="Arial" fontSize="12" isUnderline="false"/>
	<parameter name="site_id" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<parameter name="JR_TEMPLATE_DIR" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["Q:/gittrunkrepos/dispatcher/server/run/jrtemplates"]]></defaultValueExpression>
	</parameter>
	<parameter name="fromdate" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<parameter name="todate" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<parameter name="owner_id" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<parameter name="client_id" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<parameter name="client_group" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<parameter name="JR_LANGUAGE" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["EN_GB"]]></defaultValueExpression>
	</parameter>
	<parameter name="JR_TIME_ZONE_NAME" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["Europe/London"]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT  PAH.Site_ID "site_id",
        PAH.Client_ID "client_id",
        PAH.Supplier_ID "supplier_id",
        PAH.Name "name",
        AC.NumPreAdvice "num_preadvices",
        SC.ShortPreAdvice "short_preadvices",
        (100 - (100 / (AC.NumPreAdvice / SC.ShortPreAdvice))) "success_rate"
FROM Pre_Advice_Header PAH,
(
/* Get all the short pre-advices in the date range */
SELECT  PAH1.Client_ID, PAH1.Supplier_ID, 
        COUNT (PAH1.Supplier_ID) ShortPreAdvice
FROM Pre_Advice_Header PAH1
WHERE (PAH1.Pre_Advice_ID, PAH1.Client_ID) IN
        (
        /* Get all the lines that are short received */
        SELECT DISTINCT PAH2.Pre_Advice_ID, PAH2.Client_ID
        FROM Pre_Advice_Header PAH2, Pre_Advice_Line PAL2
        WHERE PAH2.Pre_Advice_ID = PAL2.Pre_Advice_ID
        AND PAH2.Client_ID = PAL2.Client_ID
        AND NVL (PAL2.Qty_Received, 0) <
                LibConfig.GetEachQuantity (PAL2.Qty_Due,
                                           PAL2.Config_ID,
                                           PAL2.Client_ID,
                                           PAL2.SKU_ID,
                                           PAL2.Tracking_Level)
        AND (PAH2.Site_ID = $P{site_id} OR $P{site_id} IS NULL)
        AND ((($P{client_id} IS NULL) AND ($P{client_group} IS NULL))
        OR  (PAH2.Client_ID = $P{client_id})
        OR  (($P{client_id} IS NULL) AND PAH2.Client_ID IN (SELECT Client_ID
                                                   FROM Client_Group_Clients
                                                   WHERE Client_Group = '&7')))
        AND (PAH2.Owner_ID = $P{owner_id} OR $P{owner_id} IS NULL)
        AND PAH2.Due_DStamp >= TO_DATE ($P{fromdate}, 'DD/MM/YYYY')
        AND PAH2.Due_DStamp < TO_DATE ($P{todate}, 'DD/MM/YYYY') + 1
        )
GROUP BY PAH1.Client_ID, PAH1.Supplier_ID
) SC,
(
/* Get all the pre-advices in the date range */
SELECT Client_ID, Supplier_ID, COUNT (Supplier_ID) NumPreAdvice
FROM Pre_Advice_Header
WHERE (Site_ID = $P{site_id} OR $P{site_id} IS NULL)
AND ((($P{client_id} IS NULL) AND ($P{client_group} IS NULL))
OR  (Client_ID = $P{client_id})
OR  (($P{client_id} IS NULL) AND Client_ID IN (SELECT Client_ID
                                      FROM Client_Group_Clients
                                      WHERE Client_Group = $P{client_group})))
AND (Owner_ID = $P{owner_id} OR $P{owner_id} IS NULL)
AND Due_DStamp >= TO_DATE ($P{fromdate}, 'DD/MM/YYYY')
AND Due_DStamp < TO_DATE ($P{todate}, 'DD/MM/YYYY') + 1
GROUP BY Client_ID, Supplier_ID
) AC
WHERE PAH.Supplier_ID = SC.Supplier_ID
AND PAH.Client_ID = SC.Client_ID
AND PAH.Supplier_ID = AC.Supplier_ID
AND PAH.Client_ID = AC.Client_ID
AND (PAH.Site_ID = $P{site_id} OR $P{site_id} IS NULL)
AND ((($P{client_id} IS NULL) AND ($P{client_group} IS NULL))
OR  (PAH.Client_ID = $P{client_id})
OR  (($P{client_id} IS NULL) AND PAH.Client_ID IN (SELECT Client_ID
                                          FROM Client_Group_Clients
                                          WHERE Client_Group = $P{client_group})))
AND (PAH.Owner_ID = $P{owner_id} OR $P{owner_id} IS NULL)
AND PAH.Due_DStamp >= TO_DATE ($P{fromdate}, 'DD/MM/YYYY')
AND PAH.Due_DStamp < TO_DATE ($P{todate}, 'DD/MM/YYYY') + 1
GROUP BY PAH.Site_ID,
        PAH.Client_ID,
        PAH.Supplier_ID,
        PAH.Name,
        AC.NumPreAdvice,
        SC.ShortPreAdvice
ORDER BY 1, 2, 7 DESC, 4]]>
	</queryString>
	<field name="site_id" class="java.lang.String"/>
	<field name="client_id" class="java.lang.String"/>
	<field name="supplier_id" class="java.lang.String"/>
	<field name="name" class="java.lang.String"/>
	<field name="num_preadvices" class="java.math.BigDecimal"/>
	<field name="short_preadvices" class="java.math.BigDecimal"/>
	<field name="success_rate" class="java.math.BigDecimal"/>
	<group name="site_id" isStartNewPage="true">
		<groupExpression><![CDATA[$F{site_id}]]></groupExpression>
		<groupHeader>
			<band splitType="Stretch"/>
		</groupHeader>
		<groupFooter>
			<band splitType="Stretch"/>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="86" splitType="Stretch">
			<textField isBlankWhenNull="false">
				<reportElement key="textField-1" style="Arial Unicode" x="140" y="34" width="500" height="24"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Bottom">
					<font fontName="Arial Unicode MS" size="12" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("difotbydate")]]></textFieldExpression>
			</textField>
			<subreport isUsingCache="true">
				<reportElement key="subreport-1" style="Arial Unicode" x="0" y="0" width="782" height="33"/>
				<subreportParameter name="JR_TEMPLATE_DIR">
					<subreportParameterExpression><![CDATA[$P{JR_TEMPLATE_DIR}]]></subreportParameterExpression>
				</subreportParameter>
				<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
				<subreportExpression class="java.lang.String"><![CDATA[$P{JR_TEMPLATE_DIR} + "/titleheaderlandscape.jasper"]]></subreportExpression>
			</subreport>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-45" style="Arial Unicode" x="140" y="60" width="500" height="24"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="11" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.date($P{fromdate}) + " " + LangData.get("to") + " " + LangData.date($P{todate})]]></textFieldExpression>
			</textField>
		</band>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band height="40" splitType="Stretch">
			<rectangle>
				<reportElement key="rectangle-1" style="rptHeaderRectangle" x="0" y="0" width="781" height="40"/>
				<graphicElement>
					<pen lineWidth="0.25" lineStyle="Solid"/>
				</graphicElement>
			</rectangle>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-5" style="rptTitle" x="3" y="21" width="89" height="15"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("site_id")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-9" style="rptTitle" x="582" y="3" width="90" height="32"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Bottom">
					<font fontName="Arial Unicode MS" size="10" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("short_preadvices")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-10" style="rptTitle" x="279" y="21" width="207" height="15"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("name")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-38" style="rptTitle" x="95" y="21" width="89" height="15"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("client_id")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-43" style="rptTitle" x="684" y="3" width="90" height="32"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Bottom">
					<font fontName="Arial Unicode MS" size="10" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("success_rate")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-46" style="rptTitle" x="187" y="21" width="89" height="15"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("supplier_id")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-48" style="rptTitle" x="490" y="3" width="90" height="32"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Bottom">
					<font fontName="Arial Unicode MS" size="10" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("no_preadvices")]]></textFieldExpression>
			</textField>
		</band>
	</columnHeader>
	<detail>
		<band height="20" splitType="Stretch">
			<rectangle>
				<reportElement key="rectangle-2" style="Arial Unicode" x="0" y="0" width="781" height="20" forecolor="#F5F3F0" backcolor="#EEEEEE">
					<printWhenExpression><![CDATA[new Boolean($V{REPORT_COUNT}.intValue() % 2 == 0)]]></printWhenExpression>
				</reportElement>
				<graphicElement>
					<pen lineWidth="0.25" lineStyle="Solid"/>
				</graphicElement>
			</rectangle>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-20" isPrintRepeatedValues="false" x="3" y="1" width="89" height="18" printWhenGroupChanges="site_id"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{site_id}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0" isBlankWhenNull="true">
				<reportElement key="textField-24" x="582" y="1" width="50" height="18"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.number($F{short_preadvices})]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-25" x="279" y="1" width="207" height="18" printWhenGroupChanges="site_id"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{name}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-39" isPrintRepeatedValues="false" x="95" y="1" width="89" height="18" printWhenGroupChanges="site_id"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{client_id}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement key="textField-44" x="684" y="1" width="50" height="18"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.number($F{success_rate})]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-47" isPrintRepeatedValues="false" x="187" y="2" width="89" height="18" printWhenGroupChanges="site_id"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{supplier_id}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0" isBlankWhenNull="true">
				<reportElement key="textField-49" x="490" y="1" width="50" height="18"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.number($F{num_preadvices})]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="24" splitType="Stretch">
			<subreport isUsingCache="true">
				<reportElement key="subreport-2" style="Arial Unicode" x="0" y="0" width="782" height="22"/>
				<subreportParameter name="JR_TEMPLATE_DIR">
					<subreportParameterExpression><![CDATA[$P{JR_TEMPLATE_DIR}]]></subreportParameterExpression>
				</subreportParameter>
				<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
				<subreportExpression class="java.lang.String"><![CDATA[$P{JR_TEMPLATE_DIR} + "/footerlandscape.jasper"]]></subreportExpression>
			</subreport>
			<textField evaluationTime="Report" isBlankWhenNull="true">
				<reportElement key="textField-71" style="Arial Unicode" x="734" y="3" width="42" height="20"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA["" + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Page" isBlankWhenNull="true">
				<reportElement key="textField-72" style="Arial Unicode" x="531" y="3" width="202" height="20"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("page_name") + " " + $V{PAGE_NUMBER} + " " + LangData.get("page_of") + " "]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
