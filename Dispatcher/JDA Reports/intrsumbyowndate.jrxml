<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="intrsumbyowndate" pageWidth="842" pageHeight="595" orientation="Landscape" whenNoDataType="AllSectionsNoDetail" columnWidth="782" leftMargin="30" rightMargin="30" topMargin="20" bottomMargin="20">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="language.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<import value="oracle.sql.*"/>
	<template><![CDATA[$P{JR_TEMPLATE_DIR} + "/default_styles.jrtx"]]></template>
	<style name="Arial Unicode" isDefault="true" fontName="Arial Unicode MS" fontSize="12" isUnderline="false"/>
	<style name="Arial" isDefault="false" fontName="Arial" fontSize="12" isUnderline="false"/>
	<parameter name="JR_TEMPLATE_DIR" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["Q:/gittrunkrepos/dispatcher/server/run/jrtemplates"]]></defaultValueExpression>
	</parameter>
	<parameter name="JR_LANGUAGE" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["EN_GB"]]></defaultValueExpression>
	</parameter>
	<parameter name="JR_TIME_ZONE_NAME" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["Europe/London"]]></defaultValueExpression>
	</parameter>
	<parameter name="site_id" class="java.lang.String" isForPrompting="false"/>
	<parameter name="site_group" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<parameter name="client_id" class="java.lang.String" isForPrompting="false"/>
	<parameter name="owner_id" class="java.lang.String" isForPrompting="false"/>
	<parameter name="fromdate" class="java.lang.String" isForPrompting="false"/>
	<parameter name="todate" class="java.lang.String" isForPrompting="false"/>
	<parameter name="client_group" class="java.lang.String" isForPrompting="false"/>
	<queryString>
		<![CDATA[SELECT  Site_ID "site_id", 
	Client_ID "client_id", 
	Owner_ID "owner_id",
        SUM(Num_Receipts) "num_receipts", 
	SUM(Num_Putaways) "num_putaways",
        SUM(Num_Picks) "num_picks", 
	SUM(Num_Stock_Checks) "num_stock_checks"
FROM
(
        SELECT DISTINCT Site_ID, Client_ID, Owner_ID, 0 Num_Receipts, 0 Num_Putaways, 0 Num_Picks, 0 Num_Stock_Checks
        FROM    Inventory_Transaction
        WHERE   ($P{site_id} IS NULL OR Site_ID = $P{site_id})
	AND (($P{site_id} IS NULL AND $P{site_group} IS NULL) OR  
	    (Site_ID = $P{site_id}) OR  
	    ($P{site_id} IS NULL AND Site_ID IN (SELECT Site_ID
						 FROM Site_Group_Sites
						 WHERE Site_Group = $P{site_group})))
        AND     ($P{owner_id} IS NULL OR Owner_ID = $P{owner_id})
        AND     (
                        ($P{client_id} IS NULL AND $P{client_group} IS NULL)
                        OR      Client_ID = $P{client_id}
                        OR      (
                                $P{client_id} IS NULL AND Client_ID IN
                                        (
                                        SELECT  Client_ID
                                        FROM    Client_Group_Clients
                                        WHERE   Client_Group = $P{client_group}
                                        )
                                )
                )
        AND      Summary_Record = 'Y'
        UNION ALL
        SELECT  Site_ID, Client_ID, Owner_ID,
                SUM(CASE WHEN Code = 'Receipt' THEN 1 ELSE 0 END) Num_Receipts,
                SUM(CASE WHEN Code = 'Putaway' THEN 1 ELSE 0 END) Num_Putaways,
                SUM(CASE WHEN Code = 'Pick' THEN 1 ELSE 0 END) Num_Picks,
                SUM(CASE WHEN Code = 'Stock Check' THEN 1 ELSE 0 END) Num_Stock_Checks
        FROM    Inventory_Transaction
	WHERE (($P{site_id} IS NULL AND $P{site_group} IS NULL) OR  
	      (Site_ID = $P{site_id}) OR  
	      ($P{site_id} IS NULL AND Site_ID IN (SELECT Site_ID
						   FROM Site_Group_Sites
						   WHERE Site_Group = $P{site_group})))
        AND     ($P{owner_id} IS NULL OR Owner_ID = $P{owner_id})
        AND     (
                        ($P{client_id} IS NULL AND $P{client_group} IS NULL)
                        OR      Client_ID = $P{client_id}
                        OR      (
                                $P{client_id} IS NULL AND Client_ID IN
                                        (
                                        SELECT  Client_ID
                                        FROM    Client_Group_Clients
                                        WHERE   Client_Group = $P{client_group}
                                        )
                                )
                )
        AND     DStamp BETWEEN TO_DATE($P{fromdate}, 'DD/MM/YYYY')
                        AND TO_DATE($P{todate}, 'DD/MM/YYYY') + (86398/86399)
        AND     Code IN ('Receipt', 'Putaway', 'Pick', 'Stock Check')
        GROUP BY Site_ID, Client_ID, Owner_ID
)
GROUP BY Site_ID, Client_ID, Owner_ID
ORDER BY Site_ID, Client_ID, Owner_ID]]>
	</queryString>
	<field name="site_id" class="java.lang.String"/>
	<field name="client_id" class="java.lang.String"/>
	<field name="owner_id" class="java.lang.String"/>
	<field name="num_receipts" class="java.math.BigDecimal"/>
	<field name="num_putaways" class="java.math.BigDecimal"/>
	<field name="num_picks" class="java.math.BigDecimal"/>
	<field name="num_stock_checks" class="java.math.BigDecimal"/>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="133" splitType="Stretch">
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField-1" style="Arial Unicode" x="0" y="35" width="780" height="24"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Bottom">
					<font fontName="Arial Unicode MS" size="12" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("intrsumbyowndate")]]></textFieldExpression>
			</textField>
			<subreport isUsingCache="true">
				<reportElement key="subreport-1" style="Arial Unicode" x="0" y="0" width="782" height="33"/>
				<subreportParameter name="JR_TEMPLATE_DIR">
					<subreportParameterExpression><![CDATA[$P{JR_TEMPLATE_DIR}]]></subreportParameterExpression>
				</subreportParameter>
				<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
				<subreportExpression class="java.lang.String"><![CDATA[$P{JR_TEMPLATE_DIR} + "/titleheaderlandscape.jasper"]]></subreportExpression>
			</subreport>
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField-73" style="Arial Unicode" x="0" y="59" width="781" height="18">
					<printWhenExpression><![CDATA[new Boolean($P{site_id} != null)]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("site_id") + " : " + $P{site_id}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField-78" style="Arial Unicode" x="0" y="113" width="782" height="18"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("from") + " " + LangData.date($P{fromdate}) + " " +
LangData.get("to") + " " + LangData.date($P{todate})]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField-79" style="Arial Unicode" x="0" y="77" width="781" height="18" isRemoveLineWhenBlank="true">
					<printWhenExpression><![CDATA[new Boolean($P{client_id} != null)]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("client_id") + " : " + $P{client_id}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField-80" style="Arial Unicode" x="0" y="95" width="781" height="18" isRemoveLineWhenBlank="true">
					<printWhenExpression><![CDATA[new Boolean($P{owner_id} != null)]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("owner_id") + " : " + $P{owner_id}]]></textFieldExpression>
			</textField>
		</band>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band height="18" splitType="Stretch">
			<rectangle>
				<reportElement key="rectangle-1" style="rptHeaderRectangle" x="0" y="0" width="781" height="18"/>
				<graphicElement>
					<pen lineWidth="0.25" lineStyle="Solid"/>
				</graphicElement>
			</rectangle>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-28" style="rptTitle" x="4" y="1" width="100" height="15"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("site_id")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-44" style="rptTitle" x="111" y="0" width="100" height="15"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("client_id")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-46" style="rptTitle" x="217" y="0" width="100" height="15"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("owner_id")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-74" style="rptTitle" x="324" y="0" width="100" height="15"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("receipt_itls")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-75" style="rptTitle" x="430" y="0" width="100" height="15"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("putaway_itls")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-76" style="rptTitle" x="536" y="0" width="100" height="15"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("pick_itls")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-77" style="rptTitle" x="642" y="0" width="100" height="15"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("stock_check_itls")]]></textFieldExpression>
			</textField>
		</band>
	</columnHeader>
	<detail>
		<band height="20" splitType="Stretch">
			<printWhenExpression><![CDATA[new Boolean (($F{num_receipts}.intValue() + $F{num_putaways}.intValue() + 
$F{num_picks}.intValue() + $F{num_stock_checks}.intValue()) > 0)]]></printWhenExpression>
			<rectangle>
				<reportElement key="rectangle-2" style="Arial Unicode" x="0" y="0" width="782" height="20" forecolor="#F5F3F0" backcolor="#EEEEEE">
					<printWhenExpression><![CDATA[new Boolean($V{REPORT_COUNT}.intValue() % 2 == 0)]]></printWhenExpression>
				</reportElement>
			</rectangle>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="4" y="1" width="100" height="18"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{site_id}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="111" y="1" width="100" height="18"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{client_id}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="217" y="1" width="100" height="18"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{owner_id}]]></textFieldExpression>
			</textField>
			<textField pattern="###0" isBlankWhenNull="false">
				<reportElement key="textField" x="324" y="1" width="50" height="18"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="8"/>
				</textElement>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{num_receipts}]]></textFieldExpression>
			</textField>
			<textField pattern="###0" isBlankWhenNull="false">
				<reportElement key="textField" x="536" y="1" width="50" height="18"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="8"/>
				</textElement>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{num_picks}]]></textFieldExpression>
			</textField>
			<textField pattern="###0" isBlankWhenNull="false">
				<reportElement key="textField" x="642" y="1" width="50" height="18"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="8"/>
				</textElement>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{num_stock_checks}]]></textFieldExpression>
			</textField>
			<textField pattern="###0" isBlankWhenNull="false">
				<reportElement key="textField" x="430" y="1" width="50" height="18"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="8"/>
				</textElement>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{num_putaways}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="25" splitType="Stretch">
			<subreport isUsingCache="true">
				<reportElement key="subreport-2" style="Arial Unicode" x="0" y="1" width="782" height="22"/>
				<subreportParameter name="JR_TEMPLATE_DIR">
					<subreportParameterExpression><![CDATA[$P{JR_TEMPLATE_DIR}]]></subreportParameterExpression>
				</subreportParameter>
				<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
				<subreportExpression class="java.lang.String"><![CDATA[$P{JR_TEMPLATE_DIR} + "/footerlandscape.jasper"]]></subreportExpression>
			</subreport>
			<textField evaluationTime="Report" isBlankWhenNull="true">
				<reportElement key="textField-71" style="Arial Unicode" x="734" y="4" width="42" height="20"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA["" + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Page" isBlankWhenNull="true">
				<reportElement key="textField-72" style="Arial Unicode" x="531" y="4" width="202" height="20"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial Unicode MS" size="10"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[LangData.get("page_name") + " " + $V{PAGE_NUMBER} + " " + LangData.get("page_of") + " "]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
