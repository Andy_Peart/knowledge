create or replace package body clipper.purge is
/******************************************************************************/
/*                                                                            */
/* NAME:         Purge GFS Labels from OC and V_ORDER_COLLATABLE              */
/*                                                                            */
/* DESCRIPTION:  set GFS label to null on orders shipped over 7 days ago      */
/*                                                                            */
/*                                                                            */
/* Date       By  Proj    Ref      Description                                 /
/* ---------- --- ------- -------- -------------------------                  */
/* 13/12/2018 AMP WILKO          Clear GFS Labels                             */
/******************************************************************************/

procedure oc_clean
is
l_count			NUMBER(5)				:= 1;
v_commit_limit 	NUMBER(5)               := 100;
v_client		VARCHAR(20)				:= 'WILKINSON';
v_code  		NUMBER;
v_errm  		VARCHAR2(64);
Message 		VARCHAR2(64);
loggingName		dcsdba.package_logging_data.logging_name%type := 'OCPURGE';
currentSid		number;
/*SELECT Orders with GFS label and shipped more than 14 days ago into a cursor*/
CURSOR CUR_ORD
is
Select order_container.order_id
		from dcsdba.order_container 
			where order_container.client_id = v_client
            and order_container.V_GFS_LABEL_IMAGE is not null
            and order_container.order_id in (select oh.order_id from dcsdba.order_header oh where oh.status = 'Shipped'
                             and oh.shipped_date < (sysdate -14));

CUR_ORD_ROW CUR_ORD%ROWTYPE;

BEGIN
    OPEN CUR_ORD;
    /*Begin the update process*/
    LOOP
        FETCH CUR_ORD into CUR_ORD_ROW;
        /* If nothing to update Exit the loop and write package log*/
        IF CUR_ORD%notfound THEN
            select sys_context('USERENV','SID') into currentSid from dual;
            Message := 'NO FURTHER UPDATES TO BE MADE';
            /*INSERT A PACKAGE LOGGING RECORD */
            insert into dcsdba.package_logging_data (key, dstamp,logging_Data,logging_name,session_id) values (dcsdba.package_logging_Data_pk_seq.nextval,sysdate,message,loggingName,currentSid);
            commit;
            EXIT;
		END IF;
        
        /*Update order_container to remove the GFS Label*/
        Update dcsdba.order_container set v_GFS_label_image = null
        where order_id = cur_ord_row.order_id;
		/*Create Message for PACKAGE Logging*/
		message := (CUR_ORD_ROW.order_id||' LABELS PURGED');
		/* Get SSID */
		l_count := l_count + SQL%ROWCOUNT;
		
		select sys_context('USERENV','SID') into currentSid from dual;
		
		insert into dcsdba.package_logging_data (key, dstamp,logging_Data,logging_name,session_id) values (dcsdba.package_logging_Data_pk_seq.nextval,sysdate,message,loggingName,currentSid);
		
			/*Check how many Rows have been updated*/
		IF l_count = v_commit_limit THEN
			COMMIT;
			l_count := 1;
		END IF;
	END LOOP;
	COMMIT;
	CLOSE CUR_ORD;
	EXCEPTION  /*Error Handling to output the SQL Error Code*/
    WHEN OTHERS THEN
    v_code := SQLCODE;
    v_errm := SUBSTR(SQLERRM, 1, 64);
	message := v_code || ' ' || v_errm;
	
	insert into dcsdba.package_logging_data (key, dstamp,logging_Data,logging_name,session_id) values (dcsdba.package_logging_Data_pk_seq.nextval,sysdate,message,loggingName,currentSid);
	COMMIT;
	/* Additional Commit as Belt and Braces */
	COMMIT;
END;

/******************************************************************************/
/*                                                                            */
/* NAME:         Purge GFS Labels from V_ORDER_COLLATABLE              */
/*                                                                            */
/* DESCRIPTION:  set GFS label to null on orders shipped over 7 days ago      */
/*                                                                            */
/*                                                                            */
/* Date       By  Proj    Ref      Description                                 /
/* ---------- --- ------- -------- -------------------------                  */
/* 13/12/2018 AMP WILKO          Clear GFS Labels                             */
/******************************************************************************/

procedure label_clean
is
l_count			NUMBER(5)				:= 1;
v_commit_limit 	NUMBER(5)               := 100;
v_client		VARCHAR(20)				:= 'WILKINSON';
v_code  		NUMBER;
v_errm  		VARCHAR2(64);
Message 		VARCHAR2(64);
loggingName		dcsdba.package_logging_data.logging_name%type := 'OCPURGE';
currentSid		number;
/*SELECT Orders with GFS label and shipped more than 14 days ago into a cursor*/
CURSOR CUR_ORD is
	Select order_id 
		from dcsdba.v_order_collatable_label 
            Where GFS_LABEL_IMAGE_BASE_64 is not null
            and order_id in (select order_id from dcsdba.order_header where status = 'Shipped'
                             and shipped_date < (sysdate -14));

CUR_ORD_ROW CUR_ORD%ROWTYPE;

BEGIN
    OPEN CUR_ORD;
    /*Begin the update process*/
    LOOP
        FETCH CUR_ORD into CUR_ORD_ROW;
        /* If nothing to update Exit the loop and write package log*/
        IF CUR_ORD%notfound THEN
            select sys_context('USERENV','SID') into currentSid from dual;
            message := 'NO FURTHER UPDATES TO BE MADE';
            /*INSERT A PACKAGE LOGGING RECORD */
            insert into dcsdba.package_logging_data (key, dstamp,logging_Data,logging_name,session_id) values (dcsdba.package_logging_Data_pk_seq.nextval,sysdate,message,loggingName,currentSid);
            commit;
            EXIT;
		END IF;
        
        /*Update order_container to remove the GFS Label*/
        Update dcsdba.v_order_collatable_label set GFS_LABEL_IMAGE_BASE_64 = null
        where order_id = cur_ord_row.order_id;
		/*Create Message for PACKAGE Logging*/
		message := CUR_ORD_ROW.order_id||' LABELS PURGED';
		/* Get SSID */
		l_count := l_count + SQL%ROWCOUNT;
		
		select sys_context('USERENV','SID') into currentSid from dual;
		
		insert into dcsdba.package_logging_data (key, dstamp,logging_Data,logging_name,session_id) values (dcsdba.package_logging_Data_pk_seq.nextval,sysdate,message,loggingName,currentSid);
		
			/*Check how many Rows have been updated*/
		IF l_count = v_commit_limit THEN
			COMMIT;
			l_count := 1;
		END IF;
	END LOOP;
	COMMIT;
	CLOSE CUR_ORD;
	EXCEPTION  /*Error Handling to output the SQL Error Code*/
    WHEN OTHERS THEN
    v_code := SQLCODE;
    v_errm := SUBSTR(SQLERRM, 1, 64);
	message := v_code || ' ' || v_errm;
	
	insert into dcsdba.package_logging_data (key, dstamp,logging_Data,logging_name,session_id) values (dcsdba.package_logging_Data_pk_seq.nextval,sysdate,message,loggingName,currentSid);
	COMMIT;
	/* Additional Commit as Belt and Braces */
	COMMIT;
END;
end;