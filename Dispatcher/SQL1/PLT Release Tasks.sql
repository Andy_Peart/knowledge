/******************************************************************************/
/*                                                                            */
/* NAME:         Update Move Tasks                                            */
/*                                                                            */
/* DESCRIPTION:  Update Held Movetasks based on OH shipment date              */
/*                                                                            */
/*                                                                            */
/* Date       By  Proj    Ref      Description                                 /
/* ---------- --- ------- -------- -------------------------                  */
/* 30/09/2018 AMP PLT              Update Move Tasks based on ship by date    */
/******************************************************************************/

DECLARE
l_count			NUMBER(5)				:= 1;
v_commit_limit 	NUMBER(5)               := 200;
v_client		VARCHAR(20)				:= 'PLT';
v_code  		NUMBER;
v_errm  		VARCHAR2(64);
Message 		VARCHAR2(64);
loggingName		dcsdba.package_logging_data.logging_name%type := 'MVTUPDATE';
currentSid		number;

CURSOR CUR_TASK is
	Select Task_id 
		from move_task 
			where status = 'Hold'
			and task_id in (select order_id from order_header
							where to_char(ship_by_date,'dd/mm/rr') = to_char(sysdate,'dd/mm/rr');
							
	
CUR_TASK_ROW := CUR_TASK%rowtype
	
Begin

	open CUR_TASK;
	
	LOOP
		FETCH CUR_TASK into CUR_TASK_ROW;
		/* IF NO DATA then Exit the procedure */
		IF CUR_TASK%notfound THEN
			dbms_output.put_line('No Task to UPDATE');
			EXIT;
		END IF;
		
		select sys_context('USERENV','SID') into currentSid from dual;
		
		UPDATE MOVE_TASK set status = 'Released'
			where task_id = CUR_TASK_ROW.task_id 
			and client_id = v_client;
		message = CUR_TASK_ROW.task_id||' Updated '||SQL%ROWCOUNT||' Tasks Released';
		
		l_count = l_count + SQL%ROWCOUNT;
		
		insert into package_logging_data (key, dstamp,logging_Data,logging_name,session_id) values (package_logging_Data_pk_seq.nextval,sysdate,message,loggingName,currentSid);
		
		/*Check how many Rows have been updated*/
		IF l_count = v_commit_limit THEN
			dbms_output.put_line('commit');
			COMMIT;
			l_count = 1;
		END IF;
	END LOOP;
	COMMIT;
	/*Close open Cursor*/
	CLOSE CUR_TASK;
	
	EXCEPTION  /*Error Handling to output the SQL Error Code*/
    WHEN OTHERS THEN
    v_code := SQLCODE;
    v_errm := SUBSTR(SQLERRM, 1, 64);
	message = v_code || ' ' || v_errm;
	
	insert into package_logging_data (key, dstamp,logging_Data,logging_name,session_id) values (package_logging_Data_pk_seq.nextval,sysdate,message,loggingName,currentSid);
	COMMIT;
    DBMS_OUTPUT.PUT_LINE (v_code || ' ' || v_errm);
	/* Additional Commit as Belt and Braces */
	COMMIT;
END;
