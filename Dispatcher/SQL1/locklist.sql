-- LOCKLIST --
set linesize 200

select b.username,
	b.program,
	b.process process,
	to_char(a.sid) sid,
	to_char(b.serial#) serial#, 
	to_char(b.sql_hash_value) sql_hash_value,
	decode(a.type, 'MR', 'Media Recovery',
		       'RT', 'Redo Thread',
		       'UN', 'User Name',
		       'TX', 'Transaction',
		       'TM', 'DML',
		       'UL', 'PL/SQL User Lock',
		       'DX', 'Distributed Transaction',
		       'CF', 'Control File',
		       'IS', 'Instance State',
		       'FS', 'File Set',
		       'IR', 'Instance Recovery',
		       'ST', 'Disk Space Transaction',
		       'TS', 'Temp Segment',
		       'IV', 'Library Cache Invalidation',
		       'LS', 'Log Start or Switch',
		       'RW', 'Row Wait',
		       'SQ', 'Sequence Number',
		       'TE', 'Extend Table',
		       'TT', 'Temp Table',
		       a.type) lock_type,
	decode(a.lmode, 0, 'None',
			1, 'Null',
		 	2, 'Row-S (SS)',
			3, 'Row-X (SX)',
			4, 'Share',
			5, 'S/Row-X (SSX)',
			6, 'Exclusive',
			to_char (a.lmode)) mode_held,
	decode(a.request, 0, 'None',
	       		  1, 'Null',
			  2, 'Row-S (SS)',
		          3, 'Row-X (SX)',
			  4, 'Share',
			  5, 'S/Row-X (SSX)',
	       		  6, 'Exclusive',
			  to_char(a.request)) mode_requested,
	to_char(a.id1) lock_id1,
	to_char(a.id2) lock_id2
from v$lock a, v$session b
where (a.sid = b.sid
        and a.request != 0)
or (a.sid = b.sid
        and a.request = 0
        and a.lmode != 4
        and (a.id1, a.id2) in (select c.id1, c.id2
                               from v$lock c
                               where c.request != 0
                               and a.id1 = c.id1
                               and a.id2 = c.id2))
order by a.id1, a.id2, a.request;


-- ensure affected DAEMON is stoped --

/*
ALTER SYSTEM KILL SESSION '467,676';

*/

