/******************************************************************************/
/*                                                                            */
/* NAME:         Function to complete repack task from Reject Spur            */
/*                                                                            */
/* DESCRIPTION:  Function to complete repack task from Reject Spur            */
/*                                                                            */
/*                                                                            */
/* Date       By  Proj    Ref      Description                                 /
/* ---------- --- ------- -------- -------------------------                  */
/* 30/09/2018 AMP WILKINSON        Update EAN on SKU                          */
/******************************************************************************/

Create or replace function clipper.COMPLETE_REPACK (P_CONTAINER VARCHAR2)

is

Begin

	update move_task set status = 'Complete' where container_id = P_CONTAINER;
	commit;
	update order_container set STATUS = 'CClosed', pallet_id ='X'||pallet_id where container_id = P_CONTAINER;
	commit;
End;
