﻿SELECT "IN FULL","IN PART","TOT UN UNITS","TOT SCRATCH UNIT","CANCEL FULL UN","FULL SCRATCH","TOT UN UNITS FULL","TOT SKU FULL UN","TOT SKU SCRATCH"

FROM
/*Total orders cancelled in full	
This field shows a count of the total number of orders cancelled in full within the reported parameters set by the user. 
A report cancelled in full refers to an order cancelled as a whole with no ordered items being shipped to the customer. */

(SELECT count(order_id) "IN FULL"
	from order_header 
	WHERE 
	status = 'Cancelled'
	AND to_timestamp(to_char(UPLOADED_DSTAMP,'dd-mon-rr hh24:mi')) between
		'20-nov-18 07:30' and '21-nov-18 07:30'
	OR
	status = 'Shipped'
	AND to_timestamp(to_char(UPLOADED_DSTAMP,'dd-mon-rr hh24:mi')) between
		'20-nov-18 07:30' and '21-nov-18 07:30'
	AND order_id not in (select order_id from shipping_manifest where 
							Shipped = 'Y')), 
							
							
/*Total orders cancelled in part	
This field shows a count of the total number of orders cancelled in part within the reported parameters set by the user. 
A report cancelled in part refers to an order where by some of the ordered units or SKU’s have not been fulfilled 
and some items have been shipped to the customer. */

(SELECT count(distinct(order_id))"IN PART"
	FROM ORDER_LINE
	WHERE
	Qty_ordered != qty_shipped
	AND order_id in (select order_id
						from order_header
						where
						status = 'Shipped'
						AND to_timestamp(to_char(UPLOADED_DSTAMP,'dd-mon-rr hh24:mi')) between
						'20-nov-18 07:30' and '21-nov-18 07:30')
	AND order_id not in (SELECT order_id from order_header 
							WHERE 
							status = 'Cancelled'
							AND to_timestamp(to_char(UPLOADED_DSTAMP,'dd-mon-rr hh24:mi')) between
								'20-nov-18 07:30' and '21-nov-18 07:30'
							OR
							status = 'Shipped'
							AND to_timestamp(to_char(UPLOADED_DSTAMP,'dd-mon-rr hh24:mi')) between
								'20-nov-18 07:30' and '21-nov-18 07:30'
							AND order_id not in (select order_id from shipping_manifest where 
													Shipped = 'Y'))),
													
													
/*Total units unavailable 	
This field shows a count of the total number of units unavailable within the reported parameters set by the user. 
An unavailable unit is one that fails allocation due to insufficient stock levels. */

(SELECT NVL(SUM(GS.Qty_Ordered- (ol.Qty_Tasked+OL.Qty_Shipped)),0) "TOT UN UNITS"
	FROM Generation_Shortage GS, Order_Line OL, SKU S,Order_header OH
	WHERE 
	OL.Order_ID = GS.Task_ID
	AND OL.Order_ID = oh.order_id
	AND Oh.Order_ID = GS.Task_ID
	AND OL.Client_ID = GS.Client_ID
	AND OL.Line_ID = GS.Line_ID
	AND S.SKU_ID = OL.SKU_ID
	AND S.Client_ID = OL.Client_ID
	AND GS.Task_ID IN (SELECT DISTINCT Oh.Order_ID 
						FROM Order_Line OH 
						WHERE status = 'Shipped')
	AND to_timestamp(to_char(oh.SHIPPED_DATE,'dd-mon-rr hh24:mi')) between
	'20-nov-18 07:30' and '21-nov-18 07:30'),
	
	
/*Total units scratched	
This field shows a count of the total number of units scratched within the reported parameters set by the user. 
A scratched unit is one that has been allocated and the pick requested but at the point of picking the required unit is unavailable 
(potentially due to incorrect stock position, damage etc.)  
so the unit has not been picked and scratched from the order.*/

(SELECT NVL(SUM(OL.Qty_Ordered- (ol.Qty_Tasked+OL.Qty_Shipped)),0) "TOT SCRATCH UNIT"
	from Order_Line ol
	WHERE
	Qty_ordered != qty_shipped
	AND order_id in (select order_id
						from Order_header
						where
						status = 'Shipped'
						AND to_timestamp(to_char(UPLOADED_DSTAMP,'dd-mon-rr hh24:mi')) between
						'20-nov-18 07:30' and '21-nov-18 07:30')
	AND order_id not in (SELECT order_id from order_header 
							WHERE 
							status = 'Cancelled'
							AND to_timestamp(to_char(UPLOADED_DSTAMP,'dd-mon-rr hh24:mi')) between
								'20-nov-18 07:30' and '21-nov-18 07:30'
							OR
							status = 'Shipped'
							AND to_timestamp(to_char(UPLOADED_DSTAMP,'dd-mon-rr hh24:mi')) between
								'20-nov-18 07:30' and '21-nov-18 07:30'
							AND order_id not in (select order_id from shipping_manifest where 
													Shipped = 'Y'))
	AND ol.order_id not in (SELECT gs.task_id
							FROM Generation_Shortage GS, Order_Line OL, SKU S,Order_header OH
							WHERE 
							OL.Order_ID = GS.Task_ID
							AND OL.Order_ID = oh.order_id
							AND Oh.Order_ID = GS.Task_ID
							AND OL.Client_ID = GS.Client_ID
							AND OL.Line_ID = GS.Line_ID
							AND S.SKU_ID = OL.SKU_ID
							AND S.Client_ID = OL.Client_ID
							AND GS.Task_ID IN (SELECT DISTINCT Oh.Order_ID 
												FROM Order_Line OH 
												WHERE status = 'Shipped')
							AND to_timestamp(to_char(oh.SHIPPED_DATE,'dd-mon-rr hh24:mi')) between
							'20-nov-18 07:30' and '21-nov-18 07:30')),
							
							
/* CANCELLED IN FULL SUMMARY SECTION */
							
							
/* Total orders cancelled in full	
This field shows a count of the total number of orders cancelled in full within the reported parameters set by the user. 
A report cancelled in full refers to an order cancelled as a whole with no ordered items being shipped to the customer. 

This data has Already been Pulled */

/*Orders cancelled due to unavailable	
This field shows a count of the total number of orders cancelled in full due to unavailable units 
within the reported parameters set by the user. 
An unavailable unit is one that fails allocation due to insufficient stock levels.*/

(SELECT count(order_id) "CANCEL FULL UN"
	from order_header 
	WHERE 
	status = 'Cancelled'
	AND to_timestamp(to_char(UPLOADED_DSTAMP,'dd-mon-rr hh24:mi')) between
		'20-nov-18 07:30' and '21-nov-18 07:30'
	AND STATUS_REASON_CODE = '33'
	OR
	status = 'Shipped'
	AND to_timestamp(to_char(UPLOADED_DSTAMP,'dd-mon-rr hh24:mi')) between
		'20-nov-18 07:30' and '21-nov-18 07:30'
	AND order_id not in (select order_id from shipping_manifest where 
							Shipped = 'Y')),
							
/*Orders cancelled due to scratch	
This field shows a count of the total number of orders cancelled in full due to scratched units within the reported parameters set by the user. 
A scratched unit is one that has been allocated and the pick requested but at the point of picking the required unit is unavailable 
(potentially due to incorrect stock position, damage etc.)  So the unit has not been picked and scratched from the order.*/


(SELECT count(order_id) "FULL SCRATCH" 
	from order_header
	WHERE 
		status = 'Cancelled'
	AND to_timestamp(to_char(UPLOADED_DSTAMP,'dd-mon-rr hh24:mi')) between
		'20-nov-18 07:30' and '21-nov-18 07:30'
	AND STATUS_REASON_CODE = '29'
	OR
	ORDER_ID IN (SELECT order_id from order_line where Qty_Shipped != qty_ordered)
	AND
	Status = 'Shipped'
	AND to_timestamp(to_char(UPLOADED_DSTAMP,'dd-mon-rr hh24:mi')) between
	'20-nov-18 07:30' and '21-nov-18 07:30'
	AND order_id in ( select task_id from move_descrepancy where REASON_CODE = 'EX_NE')),
	
/* Total units unavailable 	
This field shows a count of the total number of units unavailable within the reported parameters set by the user 
relating to orders cancelled in full only. An unavailable unit is one that fails allocation due to insufficient stock levels. */

(SELECT NVL(SUM(QTY_ORDERED),0) "TOT UN UNITS FULL"
    FROM Order_line
	WHERE order_id IN (SELECT order_id from order_header 
						WHERE 
						status = 'Cancelled'
						AND to_timestamp(to_char(UPLOADED_DSTAMP,'dd-mon-rr hh24:mi')) between
							'20-nov-18 07:30' and '21-nov-18 07:30'
						OR
						status = 'Shipped'
						AND to_timestamp(to_char(UPLOADED_DSTAMP,'dd-mon-rr hh24:mi')) between
							'20-nov-18 07:30' and '21-nov-18 07:30'
						AND order_id not in (select order_id from shipping_manifest where 
												Shipped = 'Y'))),
												
												
/*Total SKU’s unavailable	
This field shows a count of the number of unique SKU’s against which an item has been recorded unavailable 
resulting in a full order cancellation. */

(SELECT count(distinct(sku_id))"TOT SKU FULL UN"
    FROM Order_line
	WHERE order_id IN (SELECT order_id from order_header 
						WHERE 
						status = 'Cancelled'
						AND to_timestamp(to_char(UPLOADED_DSTAMP,'dd-mon-rr hh24:mi')) between
							'20-nov-18 07:30' and '21-nov-18 07:30'
						OR
						status = 'Shipped'
						AND to_timestamp(to_char(UPLOADED_DSTAMP,'dd-mon-rr hh24:mi')) between
							'20-nov-18 07:30' and '21-nov-18 07:30'
						AND order_id not in (select order_id from shipping_manifest where 
												Shipped = 'Y'))),

												
/*Total units scratched	
This field shows a count of the total number of units scratched 
within the reported parameters set by the user for orders cancelled in full only. 
A scratched unit is one that has been allocated and the pick requested but at the point of picking the required unit is unavailable 
(potentially due to incorrect stock position, damage etc.)  so the unit has not been picked and scratched from the order.*/

(SELECT NVL(SUM(QTY_ORDERED),0)"TOT UNIT FULL SCRATCH"
    FROM Order_line
	WHERE order_id IN (SELECT order_id from order_header 
						WHERE 
						status = 'Cancelled'
						AND to_timestamp(to_char(UPLOADED_DSTAMP,'dd-mon-rr hh24:mi')) between
							'20-nov-18 07:30' and '21-nov-18 07:30'
						AND STATUS_REASON_CODE = '33'
						OR
						status = 'Shipped'
						AND to_timestamp(to_char(UPLOADED_DSTAMP,'dd-mon-rr hh24:mi')) between
							'20-nov-18 07:30' and '21-nov-18 07:30'
						AND order_id not in (select order_id from shipping_manifest where 
												Shipped = 'Y'))),
												
/*Total SKU’s scratched	
This field shows a count of the number of unique SKU’s against which an item has been recorded scratched 
resulting in a full order cancellation. */

(SELECT count(distinct(sku_id))"TOT SKU SCRATCH"
    FROM Order_line
	WHERE order_id IN (SELECT order_id from order_header 
						WHERE 
						status = 'Cancelled'
						AND to_timestamp(to_char(UPLOADED_DSTAMP,'dd-mon-rr hh24:mi')) between
							'20-nov-18 07:30' and '21-nov-18 07:30'
						AND STATUS_REASON_CODE = '33'
						OR
						status = 'Shipped'
						AND to_timestamp(to_char(UPLOADED_DSTAMP,'dd-mon-rr hh24:mi')) between
							'20-nov-18 07:30' and '21-nov-18 07:30'
						AND order_id not in (select order_id from shipping_manifest where 
												Shipped = 'Y'))),

												
/* CANCELLED IN PART SUMMARY SECTION */	

/*Total orders cancelled in part	
This field shows a count of the total number of orders cancelled in part within the reported parameters set by the user. 
A report cancelled in part refers to an order where by some of the ordered units or SKU’s have not been 
fulfilled and some items have been shipped to the customer.

This data has already been Pulled */

/*Orders cancelled due to unavailable	
This field shows a count of the total number of orders cancelled in part due to unavailable units 
within the reported parameters set by the user. An unavailable unit is one that fails allocation due to insufficient stock levels.*/


											