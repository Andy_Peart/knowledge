#!/bin/bash

. $HOME/dcs/.dcs_profile

cd $DCS_COMMSDIR/slgsusreport/

# compose the csv file for the extract
EXTENSION="$(date +%y%m%d%H%M%S)"
OUTFILE="Health_outbound_${EXTENSION}.csv"
INBFILE="Health_inbound_${EXTENSION}.csv"
ADJFILE="Health_adjustments_${EXTENSION}.csv"
OUTTTFILE="Health_outbound10days_${EXTENSION}.csv"
#DAYFILE="Health_outbounddaily_${EXTENSION}.csv"
OUTWORK="$DCS_COMMSDIR/slgsusreport"

find  $DCS_COMMSDIR/slgsusreport -mtime +0.5 -exec rm -fv {} \; 

# setup the filename 
date=`date +"%m%d"`


sqlplus -s $ORACLE_USR << ! >>$OUTFILE #outbound summary
        set feedback off
        set heading off
        set pagesize 999
  set linesize 500

select 
'Order ID,Trunk ID,Customer ID,Customer Name,SKU Code,SKU Description,No. of Cartons,UPC,Transaction Date and Time' from dual;

select distinct ol.order_id||','||
oh.user_def_type_1 ||','||
oh.customer_id ||','||
case when ad.USER_DEF_NOTE_1 like '%,%' then '"'||ad.USER_DEF_NOTE_1||'"' else ad.USER_DEF_NOTE_1 end||','||
ol.sku_id ||','||
case when s.description like '%,%' then '"'||s.description||'"' else s.description end||','||
ol.QTY_SHIPPED ||','||
ol.user_def_type_1 ||','||
ol.QTY_SHIPPED*ol.user_def_type_1 ||','||
to_char(oh.shipped_date,'DD-MM-YYYY-HH24MISS')
from order_header oh, order_line ol, sku s, address ad
where 
ol.client_id = 'HEALTH'
and
oh.client_id = 'HEALTH'
and
ad.client_id = 'HEALTH'
and
s.client_id = 'HEALTH'
and
oh.WORK_GROUP = 'RETAIL'
and
oh.order_id = ol.order_id
and 
ol.sku_id = s.sku_id
and
oh.customer_id = ad.ADDRESS_ID
and 
ol.QTY_SHIPPED <> 0
and
oh.shipped_date > sysdate - 1;

!





sqlplus -s $ORACLE_USR << ! >>$ADJFILE
        set feedback off
        set heading off
        set pagesize 999
		set linesize 500
select 'Tag Id,SKU Code,SKU Description,Location Code,No. of Cartons (adjusted off),Transaction Date and Time,Adjustment CODE,Adjustment Description' from dual;
select distinct it.tag_id||','||
it.sku_id||','||
case when s.description like '%,%' then '"'||s.description||'"' else s.description end||','||
it.TO_LOC_ID||','||
it.update_qty||','||
to_char(it.DSTAMP,'DD/MM/YYYY HH24:MI:SS')||','||
it.REASON_ID||','||
it.extra_notes
from inventory_transaction it, sku s
where
it.client_id = 'HEALTH'
and
it.site_id = 'SWAD003'
and
s.client_id = 'HEALTH'
and 
it.code = 'Adjustment'
and
it.sku_id = s.sku_id
and 
it.DSTAMP > sysdate - 1;
!


sqlplus -s $ORACLE_USR << ! >>$INBFILE
        set feedback off
        set heading off
        set pagesize 999
		set linesize 500
select 'Tag Id,SKU Code,SKU Description,No. of Cartons,UPC,Total Units,Transaction Date and Time' from dual;
select distinct it.tag_id||','||
it.sku_id||','||
case when s.description like '%,%' then '"'||s.description||'"' else s.description end||','||
it.update_qty||','||
it.user_def_type_1||','||
it.UPDATE_QTY*it.user_def_type_1||','||
to_char(it.DSTAMP,'DD/MM/YYYY HH24:MI:SS')
from inventory_transaction it, sku s
where
it.client_id = 'HEALTH'
and
it.site_id = 'SWAD003'
and
s.client_id = 'HEALTH'
and 
it.code = 'Receipt'
and
it.sku_id = s.sku_id
and 
it.DSTAMP > sysdate - 1;
!


sqlplus -s $ORACLE_USR << ! >>$OUTTTFILE #outbound 10 days summary
        set feedback off
        set heading off
        set pagesize 999
  set linesize 500

select 
'Order ID,Trunk ID,Customer ID,Customer Name,SKU Code,SKU Description,No. of Cartons,UPC,Transaction Date and Time' from dual;

select distinct ol.order_id||','||
oh.user_def_type_1 ||','||
oh.customer_id ||','||
case when ad.USER_DEF_NOTE_1 like '%,%' then '"'||ad.USER_DEF_NOTE_1||'"' else ad.USER_DEF_NOTE_1 end||','||
ol.sku_id ||','||
case when s.description like '%,%' then '"'||s.description||'"' else s.description end||','||
ol.QTY_SHIPPED ||','||
ol.user_def_type_1 ||','||
ol.QTY_SHIPPED*ol.user_def_type_1 ||','||
to_char(oh.shipped_date,'DD-MM-YYYY-HH24MISS')
from order_header oh, order_line ol, sku s, address ad
where 
ol.client_id = 'HEALTH'
and
oh.client_id = 'HEALTH'
and
ad.client_id = 'HEALTH'
and
s.client_id = 'HEALTH'
and
oh.WORK_GROUP = 'RETAIL'
and
oh.order_id = ol.order_id
and 
ol.sku_id = s.sku_id
and
oh.customer_id = ad.ADDRESS_ID
and 
ol.QTY_SHIPPED <> 0
and
oh.shipped_date > sysdate - 10;

!

#sqlplus -s $ORACLE_USR << ! >>$DAYFILE #outbound 1 days summary
#        set feedback off
#        set heading off
#        set pagesize 999
#  set linesize 500

#select 'Order ID,Trunk ID,Customer ID,Customer Name,SKU Code,SKU Description,Ordered,Shipped,Variance,Transaction Date and Time' from dual;
#select ol.order_id||','||
#oh.user_def_type_1 ||','||
#oh.customer_id ||','||
#case when ad.USER_DEF_NOTE_1 like '%,%' then '"'||ad.USER_DEF_NOTE_1||'"' else ad.USER_DEF_NOTE_1 end||','||
#ol.sku_id ||','||
#case when s.description like '%,%' then '"'||s.description||'"' else s.description end||','||
#ol.QTY_ordered ||','||
#ol.QTY_SHIPPED ||','||
#sum(ol.QTY_ordered-ol.qty_shipped) ||','||
#to_char(oh.shipped_date,'DD-MM-YYYY-HH24MISS')
#from order_header oh, order_line ol, sku s, address ad
#where
#ol.client_id = 'HEALTH'
#and
#oh.client_id = 'HEALTH'
#and
#ad.client_id = 'HEALTH'
#and
#s.client_id = 'HEALTH'
#and
#oh.WORK_GROUP = 'RETAIL'
#and
#oh.order_id = ol.order_id
#and
#ol.sku_id = s.sku_id
#and
#oh.customer_id = ad.ADDRESS_ID
#and
#ol.QTY_SHIPPED <> 0
#and
#oh.shipped_date > sysdate - 1
#group by ol.order_id,',',
#oh.user_def_type_1 ,
#oh.customer_id,
#ad.USER_DEF_NOTE_1,
#ol.sku_id,
#case when s.description like '%,%' then '"'||s.description||'"' else s.description end,
#ol.QTY_ordered,
#ol.QTY_SHIPPED ,
#ol.line_id,
#to_char(oh.shipped_date,'DD-MM-YYYY-HH24MISS');
#
#!

sed -i '/^$/d' $OUTFILE
sed -i '/^$/d' $INBFILE
sed -i '/^$/d' $ADJFILE
sed -i '/^$/d' $OUTTTFILE
#sed -i '/^$/d' $DAYFILE



echo "PLease see attached Daily Inbound, Outbound, Outbound10days  and Adjustments reports" | mail -a $OUTFILE -a $INBFILE -a $OUTTTFILE -a $ADJFILE -s"Daily 2am reports" nhsreports@clippergroup.co.uk;

#echo "PLease see attached Daily SHip Variance report" | mail -a $DAYFILE -s"Daily Shipment Variance reports" wmulliner@clippergroup.co.uk,lbrawn@clippergroup.co.uk,rkilduff@clippergroup.co.uk,sbistere@clippergroup.co.uk,aurwin@clippergroup.co.uk,ljones@clippergroup.co.uk;

