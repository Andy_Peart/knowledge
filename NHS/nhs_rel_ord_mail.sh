#!/bin/bash

. $HOME/dcs/.dcs_profile

cd $DCS_COMMSDIR/slgsusreport/

# compose the csv file for the extract
EXTENSION="$(date +%y%m%d%H%M%S)"
DAYFILE="Health_outbounddaily_${EXTENSION}.csv"
OUTWORK="$DCS_COMMSDIR/slgsusreport"

find  $DCS_COMMSDIR/slgsusreport -mtime +0.5 -exec rm -fv {} \; 

# setup the filename 
date=`date +"%m%d"`


sqlplus -s $ORACLE_USR << EOF >>$DAYFILE #outbound 1 days summary
        set feedback off
        set heading off
        set pagesize 999
  set linesize 500
select 'Order ID,Customer ID,Customer Name,SKU Code,SKU Description,Ordered' from dual;
select ol.order_id||','||
oh.customer_id ||','||
case when ad.USER_DEF_NOTE_1 like '%,%' then '"'||ad.USER_DEF_NOTE_1||'"' else ad.USER_DEF_NOTE_1 end||','||
ol.sku_id ||','||
case when s.description like '%,%' then '"'||s.description||'"' else s.description end||','||
ol.QTY_ordered
from order_header oh, order_line ol, sku s, address ad
where
ol.client_id = 'HEALTH'
and
oh.client_id = 'HEALTH'
and
ad.client_id = 'HEALTH'
and
s.client_id = 'HEALTH'
and
oh.WORK_GROUP = 'RETAIL'
and
oh.order_id = ol.order_id
and
ol.sku_id = s.sku_id
and
oh.customer_id = ad.ADDRESS_ID
and
oh.status = 'Released';

EOF

sed -i '/^$/d' $DAYFILE

#echo "PLease see attached Daily Ship Variance report" | mail -a $DAYFILE -s"Daily Shipment Variance reports" wmulliner@clippergroup.co.uk,lbrawn@clippergroup.co.uk,rkilduff@clippergroup.co.uk,sbistere@clippergroup.co.uk,aurwin@clippergroup.co.uk,ljones@clippergroup.co.uk;

#echo "PLease see attached Released Orders" | mail -a $DAYFILE -s"Released Order report" apeart@clippergroup.co.uk;

echo "PLease see attached Released Orders" | mail -a $DAYFILE -s"Released Order report" nhsreports@clippergroup.co.uk;