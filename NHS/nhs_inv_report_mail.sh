#!/bin/bash

. $HOME/dcs/.dcs_profile

cd $DCS_COMMSDIR/slgsusreport/

# compose the csv file for the extract
EXTENSION="$(date +%y%m%d%H%M%S)"
INVFILE="InventoryReportStockAvailable_${EXTENSION}.csv"
INAFILE="InventoryReportStockNotAvailable_${EXTENSION}.csv"
ADJFILE="Health_adjustments_${EXTENSION}.csv"
OUTWORK="$DCS_COMMSDIR/slgsusreport"

find  $DCS_COMMSDIR/slgsusreport -mtime +0.5 -exec rm -fv {} \; 

# setup the filename 
date=`date +"%m%d"`


sqlplus -s $ORACLE_USR << ! >>$INVFILE #outbound summary
        set feedback off
        set heading off
        set pagesize 999
  set linesize 500

select 
'TAG ID,SKU ID,Description,Location,Cases,UPC,Total Eaches,Reciept Date,Status' from dual;

select distinct i.tag_id||','||
i.sku_id||','||
s.DESCRIPTION||','||
i.location_id||','||
sum(i.QTY_ON_HAND)||','||
i.user_def_type_1||','||
sum(i.QTY_ON_HAND)*i.user_def_type_1||','||
to_char(i.RECEIPT_DSTAMP,'DD/MM/YYYY HH24:MI:SS')||','||
decode(i.lock_status,'UnLocked','Available')
from inventory i, sku s
where
i.client_id = 'HEALTH'
and i.site_id = 'SWAD003'
and s.client_id = 'HEALTH'
and i.sku_id = s.sku_id
and i.zone_1 like '%1%'
and i.location_id <> 'SUSPENSE'
and i.sku_id not in ('HEALTHSKU1','HEALTHSKU2')
and i.qty_allocated = '0'
and i.lock_status = 'UnLocked'
group by i.tag_id, i.sku_id, s.DESCRIPTION, i.location_id, i.QTY_ON_HAND, i.user_def_type_1, to_char(i.RECEIPT_DSTAMP,'DD/MM/YYYY HH24:MI:SS'), decode(i.lock_status,'UnLocked','Available');

!




sqlplus -s $ORACLE_USR << ! >>$INAFILE
        set feedback off
        set heading off
        set pagesize 999
		set linesize 500
select 'Tag ID,SKU ID,SKU Description,Location Code,No. of Cartons,Units Per Carton,Total Units,Received Date and Time,Stock Avalibility' from dual;
select distinct i.tag_id||','||
i.sku_id||','||
s.DESCRIPTION||','||
i.location_id||','||
sum(i.QTY_ON_HAND)||','||
i.user_def_type_1||','||
sum(i.QTY_ON_HAND)*i.user_def_type_1||','||
to_char(i.RECEIPT_DSTAMP,'DD-MM-YYYY-HH24MISS')||','||
decode(i.lock_status,'Locked','NotAvailable')
from inventory i, sku s
where
i.client_id = 'HEALTH'
and i.site_id = 'SWAD003'
and s.client_id = 'HEALTH'
and i.sku_id = s.sku_id
and (i.user_def_type_2 is null or i.user_def_type_2 = '1')
and i.location_id <> 'SUSPENSE'
and i.sku_id not in ('HEALTHSKU1','HEALTHSKU2')
and i.lock_status = 'Locked'
group by i.tag_id, i.sku_id, s.DESCRIPTION, i.location_id, i.QTY_ON_HAND, i.user_def_type_1, to_char(i.RECEIPT_DSTAMP,'DD-MM-YYYY-HH24MISS'), decode(i.lock_status,'Locked','NotAvailable');
!

sed -i '/^$/d' $INVFILE
sed -i '/^$/d' $INAFILE

echo "Please see attached Daily InventoryReportStockAvailable and InventoryReportStockNotAvailable reports" | mail -a $INVFILE -a $INAFILE -s"Please see attached Daily InventoryReportStockAvailable and InventoryReportStockNotAvailable reports" nhsreports@clippergroup.co.uk;

#echo "TEST" | mail -a $INVFILE -a $INAFILE -s"TEST" apeart@clippergroup.co.uk;
