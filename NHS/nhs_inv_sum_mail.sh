#!/bin/bash

. $HOME/dcs/.dcs_profile

cd $DCS_COMMSDIR/slgsusreport/

# compose the csv file for the extract
EXTENSION="$(date +%y%m%d%H%M%S)"
CSVFILE="Health_inv_Snapshot_${EXTENSION}.csv"
ORDFILE="Health_ord_Snapshot_${EXTENSION}.csv"
OUTWORK="$DCS_COMMSDIR/slgsusreport"

find  $DCS_COMMSDIR/slgsusreport -mtime +0.5 -exec rm -fv {} \; 

# setup the filename 
date=`date +"%m%d"`


sqlplus -s $ORACLE_USR << ! >>$CSVFILE
        set feedback off
        set heading off
        set pagesize 999
  set linesize 500

select 'GROUP,TAG,SKU,Description,Location,Cases,UPC,Units,Received date,status,zone' from dual;
select s.user_def_type_7||','||i.tag_id||','||i.sku_id||','|| s.description||','|| i.location_id||','||i.qty_on_hand||','||i.user_def_type_1||','|| sum(i.qty_on_hand * i.user_def_type_1)||','||to_char(i.receipt_dstamp,'DD/MM/YYYY HH24:MI')||','||i.lock_status||','||i.zone_1
from inventory i, sku s, location l
where
i.sku_id = s.sku_id
and
s.client_id = 'HEALTH'
and l.location_ID = I.location_id
and l.loc_type = 'Tag-FIFO'
and (i.user_def_type_2 is null or i.user_def_type_2 = '1')
group by
s.user_def_type_7,i.tag_id,i.sku_id, s.description, i.location_id,i.qty_on_hand,i.user_def_type_1,i.receipt_dstamp,i.lock_status,i.zone_1,',',i.supplier_id;

!

sqlplus -s $ORACLE_USR << ! >>$ORDFILE
        set feedback off
        set heading off
        set pagesize 999
  set linesize 500

select 'SKU,GROUP,Description,Qty Ordered' from dual;
select ol.sku_id||','|| nvl(s.user_def_type_7,'UNKNOWN')||','||s.description||','||sum(ol.qty_ordered)
from
order_line ol, order_header oh, sku s
where
s.sku_id = ol.sku_id
and ol.order_id = oh.order_id
and oh.status in ('Released')
and oh.work_group = 'RETAIL'
and oh.client_id = 'HEALTH'
group by
ol.sku_id, s.user_def_type_7, s.description;

!

sed -i '/^$/d' $CSVFILE
sed -i '/^$/d' $ORDFILE

echo "Please see attached inventory and order Snapshot" | mail -a $CSVFILE -a $ORDFILE -s"Inventory and Order Snapshot" nhsinventory@clippergroup.co.uk,rkilduff@clippergroup.co.uk;

