#!/bin/bash

. $HOME/dcs/.dcs_profile

cd $DCS_COMMSDIR/slgsusreport/

# compose the csv file for the extract
EXTENSION="$(date +%y%m%d%H%M%S)"
DAYFILE="Health_outbounddaily_${EXTENSION}.csv"
OUTWORK="$DCS_COMMSDIR/slgsusreport"

find  $DCS_COMMSDIR/slgsusreport -mtime +0.5 -exec rm -fv {} \; 

# setup the filename 
date=`date +"%m%d"`


sqlplus -s $ORACLE_USR << EOF >>$DAYFILE #outbound 1 days summary
        set feedback off
        set heading off
        set pagesize 999
  set linesize 500

select 'Order ID,Trunk ID,Customer ID,Customer Name,SKU Code,SKU Description,Ordered,Shipped,Variance,Transaction Date and Time' from dual;
select ol.order_id||','||
oh.user_def_type_1 ||','||
oh.customer_id ||','||
case when ad.USER_DEF_NOTE_1 like '%,%' then '"'||ad.USER_DEF_NOTE_1||'"' else ad.USER_DEF_NOTE_1 end||','||
ol.sku_id ||','||
case when s.description like '%,%' then '"'||s.description||'"' else s.description end||','||
ol.QTY_ordered ||','||
nvl(ol.QTY_SHIPPED,0) ||','||
sum(ol.QTY_ordered-nvl(ol.qty_shipped,0)) ||','||
to_char(coalesce(oh.shipped_date,oh.last_update_date) ,'DD-MM-YYYY-HH24MISS')
from order_header oh, order_line ol, sku s, address ad
where
ol.client_id = 'HEALTH'
and
oh.client_id = 'HEALTH'
and
ad.client_id = 'HEALTH'
and
s.client_id = 'HEALTH'
and
oh.WORK_GROUP = 'RETAIL'
and 
(ol.qty_ordered != ol.qty_shipped or ol.qty_shipped is null)
and
oh.order_id = ol.order_id
and
ol.sku_id = s.sku_id
and
oh.customer_id = ad.ADDRESS_ID
and
oh.status in ('Shipped','Cancelled')
and 
to_char(coalesce(oh.shipped_date,oh.last_update_date),'DDMMYYYY') =  to_char(sysdate - 1,'DDMMYYYY')
group by ol.order_id,',',
oh.user_def_type_1 ,
oh.customer_id,
ad.USER_DEF_NOTE_1,
ol.sku_id,
case when s.description like '%,%' then '"'||s.description||'"' else s.description end,
ol.QTY_ordered,
ol.QTY_SHIPPED ,
ol.line_id,
to_char(coalesce(oh.shipped_date,oh.last_update_date) ,'DD-MM-YYYY-HH24MISS');

EOF

sed -i '/^$/d' $DAYFILE

echo "PLease see attached Daily Ship Variance report" | mail -a $DAYFILE -s"Daily Shipment Variance reports" wmulliner@clippergroup.co.uk,kprzybysz@clippergroup.co.uk,cunderwood@clippergroup.co.uk,lbrawn@clippergroup.co.uk,rkilduff@clippergroup.co.uk,sbistere@clippergroup.co.uk,aurwin@clippergroup.co.uk,ljones@clippergroup.co.uk,mstuttard@clippergroup.co.uk;

#echo "PLease see attached Daily Ship Variance report" | mail -a $DAYFILE -s"Daily Shipment Variance reports" apeart@clippergroup.co.uk,tosik@clippergroup.co.uk;
