set feedback off
set serveroutput on
DECLARE

CURSOR CUR_INV is
 select distinct(s.user_def_type_7) "GROUP1",
i.tag_id "TAG",
i.sku_id "SKU",
case when s.description like '%,%' then '"'||s.description||'"' else s.description end "DESCRIPTION",
i.location_id "LOCATION",
i.qty_on_hand "CASES",
i.user_def_type_1 "UPC",
to_char(i.receipt_dstamp,'DD/MM/YYYY HH24:MI') "RDATE",
i.lock_status "LOCK1",
i.lock_code "CODE",    
i.zone_1 "ZONE"
from inventory i, sku s, location l
where              
i.sku_id = s.sku_id
and
s.client_id = 'HEALTH'
and l.location_ID = I.location_id
and (l.loc_type = 'Tag-FIFO' or l.location_id in ('DAV2QU', 'FLOOR2'));

INVROW CUR_INV%rowtype;

BEGIN
	DBMS_OUTPUT.ENABLE
					  (
						  buffer_size => NULL
					  );
	dbms_output.put_line('GROUP,TAG,SKU,Description,Location,Cases,UPC,Units,Received date,status,lock code,zone');
	OPEN CUR_INV;
		LOOP
		FETCH CUR_INV
		INTO
			INVROW
		;
			IF CUR_INV%notfound THEN
			/*EXIT WHEN NO DATA FOUND*/
			--dbms_output.put_line('Finished');
			EXIT;
            else
            dbms_output.put_line(invrow.group1||','||
                                 invrow.tag||','||
                                 invrow.sku||','||
                                 invrow.description||','||
                                 invrow.location||','||
                                 invrow.cases||','|| 
                                 invrow.upc||','||
                                 invrow.cases*invrow.upc||','||
                                 invrow.rdate||','||
                                 invrow.lock1||','||
                                 invrow.code||','||
                                 invrow.zone);
		END IF;
end loop;
close CUR_INV;
END;