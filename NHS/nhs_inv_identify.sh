#!/bin/bash

########################################################################################
#
# Title   : nhs_inv_identify.sh
# Creator : Andy Peart
# Date    : 9/4/2020
# Purpose : polpulate user_def_type_2 on inventory with last number on location zone
#           to identify inventory owner
######################################Version History##################################
#1.0 - 09-APR-2020  Andy Peart - Initial version
# 
#######################################################################################

. $HOME/dcs/.dcs_profile

sqlplus -s $ORACLE_USR << !
SET SERVEROUTPUT ON
SET FEEDBACK OFF
DECLARE
	L_COUNTER        NUMBER := 1;
	v_code         NUMBER;
	v_errm         VARCHAR2(64);
/* select inventory into a cursor */

CURSOR CUR_INV IS

select tag_id, sku_id, location_id from inventory 
where 
site_id = 'SWAD003' 
and substr(zone_1,-1) in ('0','1','2','3','4','5','6','7','8','9') 
and (user_def_type_2 != substr(zone_1,-1) or user_def_type_2 is null);
	
	INV_ROW CUR_INV%rowtype;
BEGIN
	DBMS_OUTPUT.ENABLE
					  (
						  buffer_size => NULL
					  );
	/* This removes Buffer size limitation, required to select all the inventory */
	OPEN CUR_INV;
	/* Loop Through each row in the cursor */
	LOOP
		FETCH CUR_INV
		INTO
			INV_ROW
		;
		
		IF CUR_INV%notfound THEN
			/*EXIT WHEN NO DATA FOUND*/
			commit;
			EXIT;
		END IF;
		/* Output a line into the ISM file */

		update inventory set user_def_type_2 = SUBSTR(zone_1,-1)
		where
		location_id = INV_ROW.location_id
		and
		TAG_ID = INV_ROW.TAG_ID
		AND
		SKU_ID = INV_ROW.SKU_ID;

		L_COUNTER := L_COUNTER+1;

		IF L_COUNTER = 100 THEN
		 COMMIT;
		 L_COUNTER := 1;
		END IF;
		
		
	END LOOP;
	close cur_INV;
	COMMIT;
EXCEPTION
	/* WHEN ERROR OCCURS OUTPUT ERROR CODE AND CYCLE THROUGH CURSORS TO CHECK THEY ARE ALL SHUT*/
WHEN OTHERS THEN
	v_code := SQLCODE;
	v_errm := SUBSTR(SQLERRM, 1, 64);
	DBMS_OUTPUT.PUT_LINE (v_code
	|| ' '
	|| v_errm);
	IF CUR_INV%isopen then
		close CUR_INV;
	END IF;
END;
/
exit
!

