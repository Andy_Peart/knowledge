#!/bin/bash

. $HOME/dcs/.dcs_profile

cd $DCS_COMMSDIR/outwork/.

# setup the filename 
date=`date +"%C%y%m%d_%H%M%S000"`
time=`date|awk '{print $4}'|sed 's/\://g'`
file=HEALTH_ORDFUL_$date.csv

# Run the ISM query output to $file in the outwork directory.
sqlplus -s $ORACLE_USR << ! >> $file
SET SERVEROUTPUT ON
SET FEEDBACK OFF
DECLARE
	v_code         NUMBER;
	v_errm         VARCHAR2(64);
/* select all orders into a cursor */
	CURSOR CUR_ORD IS
select oh.order_id , sm.user_def_note_1 , 'SHIPPED', sm.carrier_id , sm.carrier_container_id
from order_header oh, shipping_manifest sm
where 
oh.client_id = 'HEALTH'
and
oh.carrier_bags = 'N'
and
oh.Status = 'Shipped'
and
sm.order_id = oh.order_id;
	
	ORD_ROW CUR_ORD%rowtype;
BEGIN
	DBMS_OUTPUT.ENABLE
					  (
						  buffer_size => NULL
					  );
	/* This removes Buffer size limitation, required to select all the inventory */
	OPEN CUR_ORD;
	/* Loop Through each row in the cursor */
	LOOP
		FETCH CUR_ORD
		INTO
			ORD_ROW
		;
		
		IF CUR_ORD%notfound THEN
			/*EXIT WHEN NO DATA FOUND*/
			EXIT;
		END IF;
		/* Output a line into the ISM file */
		dbms_output.put_line (ORD_row.order_id
		||','
		||ord_row.user_def_note_1
		||','
		||'SHIPPED'
		||','
		||ord_row.carrier_id
		||','
		||ord_row.carrier_container_id);

		update order_header set carrier_bags = 'Y' where order_id = ord_row.order_id;
		commit;
		
	END LOOP;
	close cur_ord;
EXCEPTION
	/* WHEN ERROR OCCURS OUTPUT ERROR CODE AND CYCLE THROUGH CURSORS TO CHECK THEY ARE ALL SHUT*/
WHEN OTHERS THEN
	v_code := SQLCODE;
	v_errm := SUBSTR(SQLERRM, 1, 64);
	DBMS_OUTPUT.PUT_LINE (v_code
	|| ' '
	|| v_errm);
	IF CUR_ord%isopen then
		close CUR_ord;
	END IF;
END;
/
exit
!

# remove empty lines from the output file
sed -i '/^$/d' $file

# copy to outarchive
cp $file ../outarchive/.

# move the file to the outtray for pickup by Messageway
mv $file ../outtray/.
