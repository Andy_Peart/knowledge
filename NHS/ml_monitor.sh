#!/bin/bash

################################################################################################################
# Script name: ml_monitor.sh
# Creator: Gurmit Karyal - ModernLogic
# Date: 04/04/2020
# Purpose: Monitor system issues
# ***************************************CHANGES****************************************************************
# 1.0 - 23/01/2019 - Oliver Dearlove -   Initial version
# 1.1 - 25/01/2019 - Gurmit Karyal -     Added killBlocker and cancelPrintjob function for self healing 
# 1.2 - not recorded Gurmit Karyal -     Updated the killBlocker sql to be non dependant on a file
# 1.3 - 05/10/2019 - Gurmit Karyal -     Removed the .ml_config. Bug fixes
# 1.4 - 29/10/2019 - Gurmit Karyal -     Added sending email address
# 1.5 - 29/10/2019 - Gurmit Karyal -     Advanced Killblocker functionality
# 2.0 - 04/04/2020 - Gurmit Karyal -     Initial version - rewritten
################################################################################################################

initialiseDefaults() {
        ##  initialise the defaults in the ml_profile
	echo "$FUNCNAME"
	. $HOME/bin/.ml_profile
}

usage() {
        ##  show usage
        (
        echo ""
        echo "Usage: $PROG [options]"
        echo ""
        echo "[-h]      enable self healing"
        echo "[-t]      Test mode - send email regardless of threshold"
        echo "[-s]      Keep sending mails until resolution"
        echo "[-e str]  Email recipients for alerts"
        echo "[-l str]  Sending address (optional)"
        echo "[-d str]  SMTP server"
        echo ""
       ) | more
        exit
}

tidyupsql () {
  local FILE="$1"

  echo "$FUNCNAME $FILE"

  # tidy up the file
  sed -i '/SQL>/d' $FILE
  sed -i '/-----------/d' $FILE
  sed -i '/ORDER_ID/d' $FILE
  sed -i '/SQL/d' $FILE
  sed -i '/Copyright/d' $FILE
  sed -i '/Connected to/d' $FILE
  sed -i '/Oracle/d' $FILE
  sed -i '/rows selected/d' $FILE
  sed -i '/Last Successful/d' $FILE
  sed -i '/^$/d' $FILE

}

cleanUp() {

        ## tidy up temporary files

        echo "$FUNCNAME"

        rm OPlocks$$.csv;
#        rm OP.printers.$$;
        rm OP.movetask.$$;
        rm OP.rntasks.$$;
#        rm OP.pctfree.$$;
#        rm OP.tablespaces.$$;
        rm OP.sessions$$.csv;
}

checkSemaphore() {
  local FUNCTION="$1"
  RETURNVAL="$(grep $FUNCTION ml_monitor_semaphores|awk '{print $2}')"
  echo "$RETURNVAL"
}

updateSemaphore() {
   local FUNCTION="$1"
   SEMAPHORESTATUS="$2"

   echo "$FUNCNAME"
        
   # update the semaphore being either NA or sent
   echo $FUNCTION" to status "$SEMAPHORESTATUS
   sed -i "/$FUNCTION/c\\$FUNCTION $SEMAPHORESTATUS" ml_monitor_semaphores
}

sendMail() {
  # this function will determine weather to send a mail and which one to send
  # it will also update the semaphore to say we have sent it or to reset it

  local GOODSUBJECT="$1"
  local GOODMESSAGE="$2"
  local BADSUBJECT="$3"
  local BADMESSAGE="$4"
  local FUNCTION="$5"
  local MODE="$6"
  local FILE="$7"

  echo "$FUNCNAME"

  # check the status of the semaphore function
  SEMAPHORESTATUS="$(checkSemaphore $FUNCTION)"

  echo "$FUNCNAME - semaphore status for $FUNCTION = $SEMAPHORESTATUS"

  echo "$FUNCNAME - send mail for $FUNCTION status $SEMAPHORESTATUS mode $MODE"

  # if mode is error
  if [[ "$MODE" = "error" ]]
    then
      # if keep sending is on or we have not yet sent a mail then check if we need to attatch a file and email out
      if [[ "$KEEPSENDING" = "true" ]] || [[ "$SEMAPHORESTATUS" = "_NA_" ]]
      then
      echo "$FUNCNAME - sending mail - error" 
      if [[ ! -f "$FILE" ]]
        then
          echo "$BADMESSAGE" | mailx -s "$HOSTNAME - $BADSUBJECT" -S smtp=$SMTPSERVER $RECIPIENTS;
        else
          echo "$BADMESSAGE" | mailx -s "$HOSTNAME - $BADSUBJECT" -S smtp=$SMTPSERVER -a $FILE $RECIPIENTS;
        fi
        updateSemaphore $FUNCTION "sent"
      fi
   fi

   # if mode is good
   if [[ "$MODE" = "good" ]]
   then
     if [[ "$SEMAPHORESTATUS" = "sent" ]]
     then
       echo "$FUNCNAME - sending mail - good"
       echo "$GOODMESSAGE" | mailx -s "$HOSTNAME - $GOODSUBJECT" -S smtp=$SMTPSERVER $RECIPIENTS;
       updateSemaphore $FUNCTION "_NA_"
     fi
   fi

}

internalTest() {

  local GOODSUBJECT="$FUNCNAME - TEST Good Subject"
  local GOODMESSAGE="$FUNCNAME - TEST Good Message"
  local BADSUBJECT="$FUNCNAME - TEST Bad Subject"
  local BADMESSAGE="$FUNCNAME - TEST Bad Message"
  local FUNCTION="$FUNCNAME"
  local MODE=""  
  echo "$FUNCNAME"

#  MODE="error"
  MODE="good" 
  
  sendMail "$GOODSUBJECT" "$GOODMESSAGE" "$BADSUBJECT" "$BADMESSAGE" "$FUNCTION" "$MODE"

}

## Self Healing ##
cancelPrintJobs() {
        ## cancel print jobs
        echo "$FUNCNAME"
        PRINTERDEVICE="$1"

        if [[ "$SELFHEAL" = "true" ]]
        then
          echo "$FUNCNAME - canceling print jobs for printer "$PRINTERDEVICE;

	  TEMPFILE="printerjobs.$$"

	  lpstat -o | awk -v name="$PRINTERDEVICE" ' $0 ~ name {print $1}' > $TEMPFILE

	  while read -r LINE
	  do
            cancel $LINE
     	    echo "$FUNCNAME - cancelled print job $LINE"
          done < $TEMPFILE

          rm $TEMPFILE

        fi
}

killBlocker() {
        ##  kill blocker
        echo "$FUNCNAME"

        # kill the blocking lock if set to true
        if [[ "$SELFHEAL" = "true" ]]
        then
            echo "killing top blocking lock"
            sqlplus -s $DCS_SYSDBA  << !

            SET SERVEROUTPUT ON;
            DECLARE
              l_wait_time NUMBER  := '$BLOCKINGLOCKWAIT';
              l_sq        CHAR(1) := '''';
              CURSOR BlockingLockSearch
              IS
                SELECT DISTINCT se.sid AS sid,
                  se.SERIAL#           AS serial,
                  vsw.seconds_in_wait  AS seconds_in_wait
                FROM v\$lock lk,
                  v\$session se,
                  v\$sql vsql,
                  v\$session_wait vsw
                WHERE (lk.TYPE                     = 'TX')
                AND (lk.SID                        = se.SID)
                AND block                          = 1
                AND lockwait                      IS NULL
                AND NVL(se.sql_id, se.prev_sql_id) = vsql.sql_id
                AND vsw.sid                        = se.sid
                AND vsw.seconds_in_wait            > l_wait_time
                AND ROWNUM                         = 1
                ORDER BY vsw.seconds_in_wait DESC;
              BlockingLockRow BlockingLockSearch%rowtype;
            BEGIN
              OPEN BlockingLockSearch;
              FETCH BlockingLockSearch INTO BlockingLockRow;
              IF BlockingLockSearch%NotFound THEN
                RETURN;
              END IF;
              BEGIN
                execute immediate 'Alter System Kill Session ''' || BlockingLockRow.sid  || ',' || BlockingLockRow.serial || ''' IMMEDIATE';
              END;
              CLOSE BlockingLockSearch;
            END;
/
!

        fi
}
## Monitoring ##
checkBlockingLocks() {
  
  # function checks if there is a blocking lock and then calls the killBlocker function if error

  local GOODSUBJECT="$FUNCNAME - blocking locks resolved"
  local GOODMESSAGE="Blocking locks resolved"
  local BADSUBJECT="checkBlockingLocks - blocking locks exist"
  local BADMESSAGE="Blocking locks detected in the system"
  local MODE="good"  
  local FUNCTION="$FUNCNAME"

  echo "$FUNCNAME"

  # check the blocking locks by the blocking lock wait time
  
  sqlplus -s $DCS_SYSDBA << ! > OPlocks$$.csv
  SELECT '"'||se.sid||'","'||serial#||'","'||sql_text||'","'||se.program||'","'||vsw.event||'","'||vsw.seconds_in_wait||'"'
   FROM v\$lock lk,
        v\$session se,
        v\$sql vsql,
        v\$session_wait vsw
  WHERE (lk.TYPE = 'TX')
    AND (lk.SID = se.SID)
    and block = 1
    and lockwait is null
    and nvl(se.sql_id, se.prev_sql_id) = vsql.sql_id
    and vsw.sid = se.sid
    and vsw.seconds_in_wait > $BLOCKINGLOCKWAIT;
  exit;
!

  tidyupsql OPlocks$$.csv

  # record the lock count from the file
  BLOCKINGLOCKCOUNT="$(wc -l OPlocks$$.csv | awk '{print $1}')"
  
  echo "$FUNCNAME - blocking lock count: $BLOCKINGLOCKCOUNT"
  
  if [[ "$BLOCKINGLOCKCOUNT" -gt "0" ]] || [[ "$TESTMODE" = "true" ]]
  then
    MODE="error"
    killBlocker
  fi

  # sendmail which includes if the latest mode if error or good
  sendMail "$GOODSUBJECT" "$GOODMESSAGE" "$BADSUBJECT" "$BADMESSAGE" "$FUNCTION" "$MODE" OPlocks$$.csv

}

checkCpu() {

  # function checks the cpu usage then emails if error
  
  local GOODSUBJECT="$FUNCNAME - cpu usage resolved"
  local GOODMESSAGE="CPU usage resolved"
  local BADSUBJECT="checkCpu - cpu above $CPUTHRESHOLD"
  local BADMESSAGE="cpu running above $CPUTHRESHOLD"
  local MODE="good"
  local FUNCTION="$FUNCNAME"

  echo "$FUNCNAME"

  # check the cpu usage
  CPUIDLE=`sar|tail -1|awk '{print $8}'|awk -F . '{print $1}'`
  CPUUSAGE=`echo $(( 100-$CPUIDLE ))`;

  if [[ "$CPUUsage" -gt "$CPUTHRESHOLD" ]] || [[ "$TESTMODE" = "true" ]]
  then
    MODE="error";
  fi

  # sendmail which includes if the latest mode if error or good
  sendMail "$GOODSUBJECT" "$GOODMESSAGE" "$BADSUBJECT" "$BADMESSAGE" "$FUNCTION" "$MODE"

}

printQueues() {

  # function checks the printqueues then emails if error. it also will tidy them up if healing is on

  local GOODSUBJECT="$FUNCNAME - resolved"
  local GOODMESSAGE="$FUNCNAME - resolved"
  local BADSUBJECT="$FUNCNAME have jobs over $PRINTLIMIT."
  local BADMESSAGE="$FUNCNAME have jobs over $PRINTLIMIT. This may cause issues with overall printing if not resolved"
  local MODE="good"
  local FUNCTION="$FUNCNAME"

  echo "$FUNCNAME"

  lpstat -t|grep device|awk '{print $3}'|awk -F: '{print $1}' >| OP.printers.$$
 
  while read LINE
  do
    PRINTCOUNT=`lpstat -t|grep $LINE|wc -l`;

    echo "printercount: $PRINTCOUNT , printer:$LINE"

    if [[ "$PRINTCOUNT" -gt "$PRINTLIMIT" ]] || [[ "$TESTMODE" = "true" ]]
    then
      MODE="error";
      cancelPrintJobs $LINE
      GOODMESSAGE="$GOODMESSAGE printer: $LINE"
    fi

    # sendmail which includes if the latest mode if error or good
    # sendMail "$GOODSUBJECT" "$GOODMESSAGE" "$BADSUBJECT" "$BADMESSAGE printer: ${LINE}" "$FUNCTION" "$MODE"

  done < OP.printers.$$

  rm OP.printers.$$

  # sendmail which includes if the latest mode if error or good
  # sendMail "$GOODSUBJECT" "$GOODMESSAGE" "$BADSUBJECT" "$BADMESSAGE" "$FUNCTION" "$MODE"

}

completeMoveTasks() {

  # function checks the complete move tasks by move task daemon

  local GOODSUBJECT="$FUNCNAME - resolved"
  local GOODMESSAGE="$FUNCNAME - resolved"
  local BADSUBJECT="$FUNCNAME - Complete move tasks above $MOVETASKTASKLIMIT"
  local BADMESSAGE="Complete move tasks above $RUNTASKLIMIT"
  local MODE="good"
  local FUNCTION="$FUNCNAME"

  echo "$FUNCNAME"

  # identify the move task daemons and their where statements

  MTFILE="OP.movetask.$$"

  sqlplus -s $ORACLE_USR << ! > $MTFILE
  set feedback off
  set heading off
  set linesize 32767
  select process_name||'£'||arguments||'£'||site_id
  from sia_process
  where executable_name = 'mvtcdae'
  and active = 'Y'
  ;
!

  tidyupsql $MTFILE

  # for each line in the file we need to check the daemon and the task count

  while read LINE
  do
    # is there a where clause
    if [[ "$LINE" = *"-w"* ]]
    then
      WHERE="true"
    else
      WHERE="false"
    fi

    if [[ "$WHERE" = "true" ]]
    then
      # get the where clause
      WHERECLAUSE="$(echo $LINE | awk -F  \" '{print $2}')"
      WHERECLAUSE="1=1 and $WHERECLAUSE"
    else
      WHERECLAUSE="1=1"
    fi

    # get the site
    SITECLAUSE="$(echo $LINE | awk -F £ '{print $3}')"

    if [[ "$SITECLAUSE" = "" ]]
    then
      SITE="false"
    else
      SITE="true"
    fi

    # if site is false then run a slightly different statement

    if [[ "$SITE" = "false" ]]
    then

      # assemble the select statement
      SELECTSTATEMENT="select count(*) from move_task where $WHERECLAUSE and site_id is not null and status = 'Complete'"

    elif  [[ "$SITE" = "true" ]]
    then
 
      # assemble the select statement
      SELECTSTATEMENT="select count(*) from move_task where $WHERECLAUSE and site_id = '${SITECLAUSE}' and status = 'Complete'"
    fi

    # echo $SELECTSTATEMENT

    RESULTFILE="result.$$"

    # run the sql
    sqlplus -s $ORACLE_USR << ! > $RESULTFILE
    set feedback off
    set heading off
    $SELECTSTATEMENT
    ;
!
    tidyupsql $RESULTFILE

    DAEMON="$(echo $LINE | awk -F £ '{print $1}')"
    MTCOUNT="$(cat $RESULTFILE | sed -e 's/^[[:space:]]*//')"

    rm $RESULTFILE

    if [[ "$MTCOUNT" -gt "$MOVETASKLIMIT" ]] || [[ "$TESTMODE" = "true" ]]
    then
      MODE="error"
      BADMESSAGE="MoveTask Daemon $DAEMON has $MTCOUNT tasks at Complete. $WHERECLAUSE"
      BADSUBJECT="Complete tasks for $DAEMON above $MOVETASKLIMIT"
    fi

    # sendmail which includes if the latest mode if error or good
    sendMail "$GOODSUBJECT" "$GOODMESSAGE" "$BADSUBJECT" "$BADMESSAGE" "$FUNCTION" "$MODE"

  done < $MTFILE
}


completeRunTasks() {

  # function checks the run tasks in progress or complete 

  local GOODSUBJECT="$FUNCNAME - resolved"
  local GOODMESSAGE="$FUNCNAME - resolved"
  local BADSUBJECT="$FUNCNAME - Pending above $RUNTASKLIMIT"
  local BADMESSAGE="Pending run tasks above $RUNTASKLIMIT"
  local MODE="good"
  local FUNCTION="$FUNCNAME"

  echo "$FUNCNAME"

  # count the run tasks by daemon
  RUNTASKSQL="OP.rntasks.$$"

  sqlplus -s $ORACLE_USR << EOF >| $RUNTASKSQL
  SET LINESIZE 32000;
  SET PAGESIZE 40000;
  SET LONG 50000;
  select distinct nvl(user_id, 'jrtskdae'), COUNT(status)
  from run_task
  where not exists
  (select 1 
  from application_user 
  where user_id = run_task.user_id)
  and status in ('Pending', 'In Progress')
  group by nvl(user_id, 'jrtskdae')
  ;
  exit;
EOF

  # tidy up the sql file
  tidyupsql $RUNTASKSQL

  if [[ -s "RUNTASKSQL" ]]
  then
    # for each line in the file, record the daemon and the process count
    while read LINE
    do
  
      DAEMON="$(echo $LINE | awk '{print $1}')"
      PROCESSCOUNT="$(echo $LINE | awk '{print $2}')"
  
      if [[ "$PROCESSCOUNT" -gt "$RUNTASKLIMIT" ]]
      then
        # we have hit the process limit for the run task. set the mode to error
        MODE="error";
        BADMESSAGE="Pending or In Progress run tasks above $runTaskLimit for $daemon"
        sendMail "$GOODSUBJECT" "$GOODMESSAGE" "$BADSUBJECT" "$BADMESSAGE" "$FUNCTION" "$MODE" 
      fi
    done < $RUNTASKSQL

  fi

  # lets handle a basic test mode message anyway
  if [[ "$TESTMODE" = "TRUE" ]]
  then
    MODE="error";
    BADMESSAGE="TEST MODE Pending or In Progress run tasks above $RUNTASKLIMIT";
    sendMail "$GOODSUBJECT" "$GOODMESSAGE" "$BADSUBJECT" "$BADMESSAGE" "$FUNCTION" "$MODE"
  fi

}

databaseRunning() {

  # function checks if the database is running

  local GOODSUBJECT="$FUNCNAME - resolved"
  local GOODMESSAGE="$FUNCNAME - resolved"
  local BADSUBJECT="$FUNCNAME - Database is not running"
  local BADMESSAGE="Database is not running"
  local MODE="good"
  local FUNCTION="$FUNCNAME"

  echo "$FUNCNAME"

  COUNT=`databaseup -v|grep "The database is not running"|wc -l`;
  if [[ "$COUNT" -gt "0" ]] ||  [[ "$TESTMODE" = "true" ]]
  then
    MODE="error";
  fi

  # sendmail which includes if the latest mode if error or good
  sendMail "$GOODSUBJECT" "$GOODMESSAGE" "$BADSUBJECT" "$BADMESSAGE" "$FUNCTION" "$MODE"

}

invalidPackages() {

  # function checks if there are any invalid packages
 
  local GOODSUBJECT="$FUNCNAME - resolved"
  local GOODMESSAGE="$FUNCNAME - resolved"
  local BADSUBJECT="$FUNCNAME - Invalid packages exist"
  local BADMESSAGE="Invalid packages exist in the database"
  local MODE="good"
  local FUNCTION="$FUNCNAME"

  echo "$FUNCNAME"

  count=`pinvalid |grep -i lib|wc -l`;
  if [[ "$COUNT" -gt "0" ]] ||  [[ "$TESTMODE" = "true" ]]
  then
    MODE="error";
  fi

  # sendmail which includes if the latest mode if error or good
  sendMail "$GOODSUBJECT" "$GOODMESSAGE" "$BADSUBJECT" "$BADMESSAGE" "$FUNCTION" "$MODE"

}

processUsage() {

  local GOODSUBJECT="$FUNCNAME - resolved"
  local GOODMESSAGE="$FUNCNAME - resolved"
  local BADSUBJECT="$FUNCNAME - Process limit above 80% usage"
  local BADMESSAGE="Process limit above 80% usage"
  local MODE="good"
  local FUNCTION="$FUNCNAME"

  echo "$FUNCNAME"

  MAXUSAGE=`ulimit -u`;
  COUNT=`pscmd -f|wc -l`;
  USAGE=`echo $(( $COUNT/$MAXUSAGE*100 ))`;

  if [[ "$USAGE" -gt "80" ]] || [[ "$TESMODE" = "TRUE" ]]
  then
    MODE="error";
  fi

  # sendmail which includes if the latest mode if error or good
  sendMail "$GOODSUBJECT" "$GOODMESSAGE" "$BADSUBJECT" "$BADMESSAGE" "$FUNCTION" "$MODE"

}

tableSpaces() {

  local GOODSUBJECT="$FUNCNAME - limits resolved"
  local GOODMESSAGE="$FUNCNAME - limits resolved"
  local BADSUBJECT="$FUNCNAME - Table space warning"
  local BADMESSAGE="Following tablespaces have an issue - "
  local MODE="good"
  local FUNCTION="$FUNCNAME"

  echo "$FUNCNAME"

  sqlplus -s $ORACLE_USR << EOF >| OP.tablespaces.$$
  set feedback off
  set heading off
  set pagesize 0
  select tablespace_name from user_Tablespaces;
  exit;
EOF

  tidyupsql OP.tablespaces.$$

  while read LINE
  do
        sqlplus -s $ORACLE_USR << EOF >| OP.pctfree.$$
        set feedback off
        set heading off
        set pagesize 0
        select
           round(100 * (fs.freespace / df.totalspace)) "Pct. Free"
        from
           (select
              tablespace_name,
              round(sum(bytes) / 1048576) TotalSpace
           from
              dba_data_files
           group by
              tablespace_name
           ) df,
           (select
              tablespace_name,
              round(sum(bytes) / 1048576) FreeSpace
           from
              dba_free_space
           group by
              tablespace_name
           ) fs
        where
           df.tablespace_name = fs.tablespace_name
           and fs.tablespace_name = '$LINE';
EOF

        PCTFREE=`tail -1 OP.pctfree.$$|awk '{print $1}'`;
        echo "pctfree = $PCTFREE tablespace $LINE"

        echo "GURMIT $PCTFREE, freespacelimit: $FREESPACELIMIT"

        if [[ "$PCTFREE" = "" ]]
        then
                MODE="error";
                BADMESSAGE="${BADMESSAGE} $LINE has 0 percent free"
        elif [[ "$PCTFREE" -lt "$FREESPACELIMIT" ]] || [[ "$TESTMODE" = "true" ]]
        then
                MODE="error"
                BADMESSAGE="${BADMESSAGE} $LINE has $PCTFREE percent free"
        fi
  done < OP.tablespaces.$$

  # sendmail which includes if the latest mode if error or good
  sendMail "$GOODSUBJECT" "$GOODMESSAGE" "$BADSUBJECT" "$BADMESSAGE" "$FUNCTION" "$MODE"

}

activeSessions() {

  local GOODSUBJECT="$FUNCNAME - resolved"
  local GOODMESSAGE="$FUNCNAME - resolved"
  local BADSUBJECT="$FUNCNAME - Active db sessions above $activeDBSessionLimit"
  local BADMESSAGE="Active database sessions above set threshold"
  local MODE="good"
  local FUNCTION="$FUNCNAME"

  echo "$FUNCNAME"

  sqlplus -s $ORACLE_USR << EOF >| OP.sessions$$.csv
  set feedback off
  set heading off
  set pagesize 0
  set linesize 32767
  SELECT distinct '"'||program||'","'||se.sid||'","'||serial#||'","'||sql_text||'","'||vsw.event||'","'||vsw.seconds_in_wait||'"' 
   FROM v\$session se,
        v\$sql vsql,
        v\$session_wait vsw
  WHERE se.status = 'ACTIVE'
    and nvl(se.sql_id, se.prev_sql_id) = vsql.sql_id
    and vsw.sid = se.sid
    and user = (select username
                  from dual);
  exit;
EOF

  COUNT=`wc -l OP.sessions$$.csv|awk '{print $1}'`

  if [[ "$COUNT" -gt "$ACTIVEDBSESSIONLIMIT" ]] || [[ "$TESTMODE" = "true" ]]
  then
        MODE="error";
  fi

  # sendmail which includes if the latest mode if error or good
  sendMail "$GOODSUBJECT" "$GOODMESSAGE" "$BADSUBJECT" "$BADMESSAGE" "$FUNCTION" "$MODE" OP.sessions$$.csv
  
}

siaUp() {

  local GOODSUBJECT="$FUNCNAME - resolved"
  local GOODMESSAGE="$FUNCNAME - resolved"
  local BADSUBJECT="$FUNCNAME - SIA is not running"
  local BADMESSAGE="The SIA process is not running"
  local MODE="good"
  local FUNCTION="$FUNCNAME"

  echo "$FUNCNAME"
 
  # check if sia is running
  sia -u

  if [[ "$?" = "0" ]]
  then
    SIASTATUS="UP"
  else
    SIASTATUS="DOWN"
  fi

  if [[ "$SIASTATUS" = "DOWN" ]] || [[ "$TESTMODE" = "true" ]]
  then
        MODE="error";
  fi

  # sendmail which includes if the latest mode if error or good
  sendMail "$GOODSUBJECT" "$GOODMESSAGE" "$BADSUBJECT" "$BADMESSAGE" "$FUNCTION" "$MODE" OP.sessions$$.csv

}

###############################
# Main Program
###############################

# get command line options
while getopts htse:l:d:? 2> /dev/null OPTION
do
  case $OPTION in
    h) SELFHEAL="true" ;;
    t) TESTMODE="true" ;;
    s) KEEPSENDING="true" ;;
    e) RECIPIENTS="$OPTARG" ;;
    l) SENDERSEMAIL="$OPTARG" ;;
    d) SMTPSERVER="$OPTARG" ;;
    ?) usage ;;
  esac
done

while [ "1" -eq "1" ]
do
  initialiseDefaults
  checkBlockingLocks
  checkCpu
#  printQueues
  completeMoveTasks
  completeRunTasks
  databaseRunning
  invalidPackages
  processUsage
#  tableSpaces
  activeSessions
  siaUp
#  internalTest
  # clean up files
  cleanUp

  sleep 10




done

