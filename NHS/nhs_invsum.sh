#!/bin/sh

. $HOME/dcs/.dcs_profile

cd $DCS_COMMSDIR/senzer/outwork/.

# setup the filename
date=`date +"%C%y%m%d_%H%M%S000"`
time=`date|awk '{print $4}'|sed 's/\://g'`
file=SENZ_PO_SUM_$date.csv

sqlplus -s $ORACLE_USR << ! >> $file

  set headings on
  set feedback off
  set pagesize 50000
  with po_det as
  (
  select pl.pre_advice_id as Pre_advice ,ph.status, ph.supplier_id as supplier, pl.sku_id , s.description ,pl.qty_due as DUE_IN,nvl(pl.qty_received,0) as REC
  from
      pre_advice_header ph, pre_advice_line pl, sku s
  where
  ph.client_id = 'SENZ' and
  pl.pre_advice_id = ph.pre_advice_id and
  --itl.reference_id = ph.pre_advice_id and
  s.sku_id = pl.sku_id
  )

  select po.pre_advice "SENZER PO",',', po.status,',',po.supplier ,',',po.sku_id,',', po.description,',', po.due_in,
  ',',po.REC "Received",',', po.due_in - po.rec "OUTSTANDING",',', nvl(to_char(max(itl.dstamp),'DD/MM/RRRR'),'NO Receipt') "LAST RECEIPT" from po_det po, inventory_transaction itl
  where (po.pre_advice = itl.reference_id or po.pre_advice not in (select reference_id from inventory_transaction where code = 'Receipt' and client_id = 'SENZ'))
  group by po.pre_advice, po.status ,po.supplier ,po.sku_id, po.description, po.due_in, po.REC
  order by po.pre_advice, po.sku_id;
      
!

# remove empty lines from the output file
sed -i '/^$/d' $file

# copy to outarchive
cp $file ../outarchive/.

# move the file to the outtray
mv $file ../outtray/.
