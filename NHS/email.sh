#!/bin/bash

#/******************************************************************************/
#/*                                                                            */
#/* NAME:         INREJECT Monitor                                             */
#/*                                                                            */
#/* DESCRIPTION:  Checks inreject folder for files added in the last 24hrs     */
#/*               Creates alert email and attaches reject file and report      */
#/*                                                                            */
#/* Date       By  Proj    Ref      Description                                 /
#/* ---------- --- ------- -------- -------------------------                  */
#/* 30/08/2019 AMP CLIPPER          Run all extracts required                  */
#/******************************************************************************/

. $HOME/dcs/.dcs_profile

EXTENSION="$(date +%y%m%d%H%M%S)"
LOGFILE="REJECT_MAIL_${EXTENSION}.LOG"

cd $DCS_COMMSDIR/inreject
if [ -e filenames.txt ]
then
    echo "ok"
rm filenames.txt
else
	echo "no file"
fi
find -type f -cmin -10 -not -name filenames.txt > filenames.txt

while read i ; do 
#echo create substr
SUBSTR=$(echo $i| cut -d'.' -f 2)
SUBSTR2=$(echo $SUBSTR| cut -d'/' -f 2)
echo find file
REPORT=$(grep -F -l $SUBSTR2 ../report/infmerge*) 
echo build email
echo $SUBSTR
echo $SUBSTR2
echo $REPORT
echo $i
echo "BOOM Reject File and Report extracted in a script and emailed out" | mail -a $i -a $REPORT -s $i" failed to interface" apeart@clippergroup.co.uk,pgorny@clippergroup.co.uk,dsalyte@clippergroup.co.uk; 
done < filenames.txt > $LOGFILE

mv $LOGFILE $DCS_TMPDIR/$ORACLE_SID/$LOGFILE
