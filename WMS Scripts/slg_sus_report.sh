#!/bin/bash

. $HOME/dcs/.dcs_profile

cd $DCS_COMMSDIR/slgsusreport/

# compose the csv file for the extract
EXTENSION="$(date +%y%m%d%H%M%S)"
CSVFILE="SLG_SUSPENSE_${EXTENSION}.txt"
OUTWORK="$DCS_COMMSDIR/slgsusreport"

find  $DCS_COMMSDIR/slgsusreport -mtime +7 -exec rm -fv {} \; 

# setup the filename 
date=`date +"%m%d"`
time=`date|awk '{print $4}'|sed 's/\://g'`
file=SLG_SUSPENSE_$date$time.csv


sqlplus -s $ORACLE_USR << !
spool $OUTWORK/$CSVFILE
SET linesize 500 HEADING ON pagesize 10000
column "SKU" format a20
column "DESCRIPTION" format a70
column "BATCH" format a20
column "WAREHOUSE" format a10

        select /*csv*/ 
          i.SKU_ID "SKU"
          , s.description "DESCRIPTION"
          , i.Batch_id "BATCH"
          , i.condition_id "WAREHOUSE"
          , round(sum(i.qty_on_hand),0) "SUSPENSE Qty"
        from
          inventory i
          , sku                   s
        where
          i.client_id = 'SLG'
      and i.sku_id = s.sku_id
      and i.location_id = 'SUSPENSE'
      group by
        i.sku_id, s.description, i.batch_id, i.condition_id
        ;
spool off
!

echo "Please see attached Suspense Report for SLG on site SWAD002" | mail -a $CSVFILE -s"SLG SWAD002 Suspense Report" SLG_WMS_Alerts@clippergroup.co.uk;
