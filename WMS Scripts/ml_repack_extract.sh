#!/bin/bash

####################################################################################################################################
# Script name: ml_repack_extract.sh
# Creator: Gurmit Karyal - ModernLogic
# Date: 15/05/2019
# Purpose: Script creates csv files from repack itls
# ***************************************CHANGES************************************************************************************
# 1.0 - 15/05/2019 - Gurmit Karyal - Initial version
####################################################################################################################################

# set preextract value
PRE_UPLOADED="H"
ITLVISIBLE_UPLOADED='1'
UPLOADED="1"

# find the records that need to be extracted and set them with the pre-extract value
sqlplus -s $ORACLE_USR << !
update inventory_transaction
set uploaded_customs = '$PRE_UPLOADED'
where uploaded_customs = 'Y'
and dstamp >= current_timestamp -1
and client_id = 'PLT'
and dstamp < to_date(to_char(current_timestamp, 'yyyymmddhh24') || '00', 'yyyymmddhh24mi')
and code    = 'Repack';
commit;
exit;
!

# compose the csv file for the extract
EXTENSION="$(date +%y%m%d%H%M%S)"
CSVFILE="VITESSE_REPACK_${EXTENSION}.csv"
OUTWORK="$DCS_COMMSDIR/outwork"

sqlplus -s $ORACLE_USR << ! >> $OUTWORK/$CSVFILE
SET ECHO OFF HEADING OFF LINESIZE 32767 TRIMSPOOL ON PAGESIZE 0 FEEDBACK OFF TRIMOUT ON
SELECT code ,
  ',' ,
  list_id ,
  ',' ,
  site_id ,
  ',' ,
  from_loc_id ,
  ',' ,
  sku_id ,
  ',' ,
  user_id ,
  ',' ,
  dstamp ,
  ',' ,
  update_qty ,
  ',' ,
  original_qty ,
  ',' ,
  user_def_type_2 ,
  ',' ,
  user_def_type_3 ,
  ',' ,
  user_def_type_4 ,
  ',' ,
  user_def_type_5 ,
  ',' ,
  reference_id
FROM inventory_transaction
WHERE uploaded_customs = '$PRE_UPLOADED' 
AND client_id = 'PLT';
exit;
!

# remove empty files where needed
if [[ ! -s "$OUTWORK/$CSVFILE" ]]
then
  echo "$OUTWORK/$CSVFILE is empty. removed" 1>&2
  rm $OUTWORK/$CSVFILE
  exit 1
fi  

# set the data to be uploaded
sqlplus -s $ORACLE_USR << !
update inventory_transaction
set uploaded_customs = '$UPLOADED',
old_user_def_num_4 = '$ITLVISIBLE_UPLOADED'
where uploaded_customs = '$PRE_UPLOADED'
and client_id = 'PLT';
commit;
exit;
!

echo $OUTWORK/$CSVFILE

# move to outtray
OUTTRAY="$DCS_COMMSDIR/outtray"
echo "moving $OUTWORK/$CSVFILE $OUTTRAY"
mv $OUTWORK/$CSVFILE $OUTTRAY 


exit 0
