#!/bin/bash
####################################################################################################################################
#            Reports script - Popultate data into current DLx instance                                                             #
#                                                                                                                                  #
#DATE       BY                Change No      VERSION DESCRIPTION                                                                   #
#========== ================= ===========    ======= ==============================================================================#
#25/09/2020 Dawid Slabicki    NHS PPE        1.0     Initial version                                                               #
#                                                                                                                                  #
####################################################################################################################################

. $HOME/dcs/.dcs_profile

cd $DCS_COMMSDIR/healthusrreport/

# compose the csv file for the extract
EXTENSION="$(date +%y%m%d%H%M%S)"
RANGFILE="RANGEWARNIGFILE_${EXTENSION}.csv"
OUTWORK="$DCS_COMMSDIR/healthusrreport"

find  $DCS_COMMSDIR/healthusrreport -mtime +0.5 -exec rm -fv {} \;

# setup the filename
date=`date +"%m%d"`


sqlplus -s $ORACLE_USR > KEYSORD << EOF
SET SERVEROUTPUT ON
SET FEEDBACK OFF

declare
	key_count number;
begin
	select
		count (carrier_id)
	into
		key_count
    from
    carriers
    where
    container_warn_given = 'Y'
    and
    client_id = 'HEALTH'
    and
    site_id = 'SWAD003';

	DBMS_OUTPUT.PUT_LINE(key_count);
end;
/
exit
EOF

keycountord=`cat KEYSORD`

if ((keycountord > 0)); then

sqlplus -s $ORACLE_USR << ! >>$RANGFILE #Low Carrier Range Warning Notification File
        set feedback off
        set heading off
        set pagesize 999
  set linesize 500

Select 'Carrier Name,Service Level,Service Level Description,Tracking Range Left' from dual;
select distinct
carrier_id||','||
service_level||','||
notes||','||
(container_id_end - container_id_next)
from
carriers
where
container_warn_given = 'Y'
and
client_id = 'HEALTH'
and
site_id = 'SWAD003';
!




sed -i '/^$/d' $RANGFILE

#LINES="wc -l $RANGEFILE | cut -d '' -f 1"



#if
#$no_of_lines >1
#then
echo "Tracking Range For Royal Mail is running Low" | mail -a $RANGFILE -s"Tracking Range For Royal Mail is running Low" dslabicki@clippergroup.co.uk;
#else
#rm $RANGFILE
#fi

fi
#echo "TEST" | mail -a $INVFILE -a $INAFILE -s"TEST" dslabicki@clippergroup.co.uk;
sqlplus -s $ORACLE_USR > KEYSORD << EOF
SET SERVEROUTPUT ON
SET FEEDBACK OFF

declare
	key_count number;
begin
	select
		count (carrier_id)
	into
		key_count
    from
    carriers
    where
    container_warn_given = 'Y'
    and
    client_id = 'LOVECR'
    and
    site_id = 'LCSELBY';

	DBMS_OUTPUT.PUT_LINE(key_count);
end;
/
exit
EOF

keycountord=`cat KEYSORD`

if ((keycountord > 0)); then

sqlplus -s $ORACLE_USR << ! >>$RANGFILE #Low Carrier Range Warning Notification File
        set feedback off
        set heading off
        set pagesize 999
  set linesize 500

Select 'Carrier Name,Service Level,Service Level Description,Tracking Range Left' from dual;
select distinct
carrier_id||','||
service_level||','||
notes||','||
(container_id_end - container_id_next)
from
carriers
where
container_warn_given = 'Y'
and
client_id = 'LOVECR'
and
site_id = 'LCSELBY';
!




sed -i '/^$/d' $RANGFILE

#LINES="wc -l $RANGEFILE | cut -d '' -f 1"



#if
#$no_of_lines >1
#then
echo "Tracking Range For Royal Mail is running Low" | mail -a $RANGFILE -s"Tracking Range For Royal Mail is running Low" dslabicki@clippergroup.co.uk;
#else
#rm $RANGFILE
#fi

fi
