#!/bin/bash

#/******************************************************************************/
#/*                                                                            */
#/* NAME:         INREJECT Monitor                                             */
#/*                                                                            */
#/* DESCRIPTION:  Checks inreject folder for files added in the last 24hrs     */
#/*               Creates alert email and attaches reject file and report      */
#/*                                                                            */
#/* Date       By  Proj    Ref      Description                                 /
#/* ---------- --- ------- -------- -------------------------                  */
#/* 30/08/2019 AMP CLIPPER          Run all extracts required                  */
#/******************************************************************************/

. $HOME/dcs/.dcs_profile

EXTENSION="$(date +%y%m%d%H%M%S)"
LOGFILE="ORDERS_Cancelled_${EXTENSION}.LOG"

cd AndyP

sqlplus -s $ORACLE_USR << !
SET SERVEROUTPUT ON
SET FEEDBACK OFF
spool $LOGFILE
DECLARE 
	v_code NUMBER; 
	v_errm VARCHAR2(64);
	l_count NUMBER(5);
	cursor ORD_CURS is 
		select 
			order_id 
		from 
			order_header 
		where 
			status in ('Released','Hold')
			and order_value = 0;
ORD_ROW ORD_CURS%rowtype; 
Begin 
	l_count := 0;
	open ORD_CURS; 
	loop 
		fetch ORD_CURS 
		into 
			ORD_ROW 
		; 
		 
		IF ORD_CURS%notfound THEN
		/*EXIT WHEN NO DATA FOUND*/ 
			dbms_output.put_line('NO More Orders To Update'); 
			EXIT; 
		END IF; 
		update 
			order_header 
		set consignment = 'X'
		where order_id = ORD_ROW.order_id;
		dbms_output.put_line(ORD_ROW.order_id || ' Cancelled');
		l_count := l_count+1;
		    IF l_count = 100 THEN
      dbms_output.put_line('commit');
      commit;
	  l_count := 0;
    END IF;
	END LOOP;
  commit;
  close ord_curs;
  	EXCEPTION /* WHEN ERROR OCCURS OUTPUT ERROR CODE AND CYCLE THROUGH CURSORS TO CHECK THEY ARE ALL SHUT*/
    WHEN OTHERS THEN
    v_code := SQLCODE;
    v_errm := SUBSTR(SQLERRM, 1, 64);
    DBMS_OUTPUT.PUT_LINE (v_code || ' ' || v_errm);
	commit;
	end;
/
exit
!

mv $LOGFILE $DCS_TMPDIR/$LOGFILE