#!/bin/sh

#/******************************************************************************/
#/*                                                                            */
#/* NAME:         bounce_metapack.sh                                           */
#/*                                                                            */
#/* DESCRIPTION:  Script to automatically restart all metapack daemons         */
#/*                                                                            */
#/*                                                                            */
#/* Date       By  Proj    Ref      Description                                 /
#/* ---------- --- ------- -------- -------------------------                  */
#/* 04/09/2019 AMP PLT              Restart Metapack Daemons                  */
#/******************************************************************************/

. $HOME/dcs/.dcs_profile

#Bounce UKROYALTY Daemon
echo Bounce UKROYALTY Daemon

telsia stop pltmetapackdae1
echo STOPPED
telsia start pltmetapackdae1
echo STARTED
#Add a sleep while it processes
echo sleeping for 15 Seconds
sleep 15

#Bounce UKEXP Daemon
echo Bounce UKEXP Daemon

telsia stop pltmetapackdae2
echo STOPPED
telsia start pltmetapackdae2
echo STARTED
#Add a sleep while it processes
echo sleeping for 15 Seconds
sleep 15

#Bounce UKSAVER Daemon
echo Bounce UKSAVER Daemon

telsia stop pltmetapackdae3
echo STOPPED
telsia start pltmetapackdae3
echo STARTED
#Add a sleep while it processes
echo sleeping for 15 Seconds
sleep 15

#Bounce Remaining Order Types Daemon
echo Bounce Remaining Order Types Daemon

telsia stop pltmetapackdae4
echo STOPPED
telsia start pltmetapackdae4
echo STARTED

echo 
echo ALL DAEMONS BOUNCED