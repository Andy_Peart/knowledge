#!/bin/bash

. $HOME/dcs/.dcs_profile

cd $DCS_COMMSDIR/outwork/.

# compose the csv file for the extract
EXTENSION="$(date +%y%m%d%H%M%S)"
CSVFILE="VITESSE_USERLOGS_${EXTENSION}.csv"
OUTWORK="$DCS_COMMSDIR/outwork"


# setup the filename 
date=`date +"%m%d"`
time=`date|awk '{print $4}'|sed 's/\://g'`



# find user log activity #


sqlplus -s $ORACLE_USR << ! >> $CSVFILE
        set serveroutput on
		set feedback off
        set heading on
		set linesize 32767
select
user_id,',',station_id as work_station,',',group_id,',',work_type,',',to_char (dstamp, 'dd/MM/rr')as "DATE",',', to_char(dstamp, 'hh24:mi:ss')as "HOURS  MINS  SECONDS"
from 
user_log where work_type in('Login','Logout')and
dstamp > current_timestamp - numtodsinterval(10,'MINUTE')
order by user_id,
dstamp asc;

exit
!


echo $OUTWORK/$CSVFILE

# Copy file to out archive
echo "Copying $OUTWORK/$CSVFILE"
cp $OUTWORK/$CSVFILE $DCS_COMMSDIR/outarchive/$CSVFILE

# move to outtray
echo "moving $OUTWORK/$CSVFILE $DCS_COMMSDIR/outtray"
mv $OUTWORK/$CSVFILE $DCS_COMMSDIR/outtray
		