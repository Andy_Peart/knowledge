#!/bin/bash

. $HOME/dcs/.dcs_profile

cd $HOME/AndyP/Release

##################################################################################
echo ""
echo "Starting the Workstation Script"
echo ""
##################################################################################

workstation.sh

##################################################################################
echo ""
echo "Inserting condition Codes"
echo ""
##################################################################################

sqlplus -s $ORACLE_USR <<EOF
SET SERVEROUTPUT ON;
@Condition_code.sql
exit;
EOF

##################################################################################
echo ""
echo "Inserting Location and workzones"
echo ""
##################################################################################

sqlplus -s $ORACLE_USR <<EOF
SET SERVEROUTPUT ON;
@loc_zone_and_Work_zone.sql
exit;
EOF

##################################################################################
echo ""
echo "Inserting non Interfaced Locations"
echo ""
##################################################################################

sqlplus -s $ORACLE_USR <<EOF
SET SERVEROUTPUT ON;
@non_interfaced_location.sql 
exit;
EOF


##################################################################################
echo ""
echo "Cleaning Up"
echo ""
##################################################################################

rm slg_rdt_setup.tar.gz

tar -czvf slg_rdt_setup.tar.gz *.sql

rm *.sql

##################################################################################
echo ""
echo "Clean up completed"
echo ""
##################################################################################
