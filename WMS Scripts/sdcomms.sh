#!/bin/bash

####################################################################################################################################
# Script name: sdcomms.sh
# Creator: Gurmit Karyal - ModernLogic
# Date: 04/04/2019
# Version: 1.0
# Purpose: Script creates csv files and inserts into interface tables in the ecom schema from sports direct lob file
# ***************************************CHANGES************************************************************************************
# v1.0 - 23/01/2019 - Gurmit Karyal - Initial version
####################################################################################################################################

# Working directories and temp directories
SDCOMMSDIR="$HOME/sdcomms"
INTRAY="$SDCOMMSDIR/intray"
INWORK="$SDCOMMSDIR/inwork"
INARCHIVE="$SDCOMMSDIR/inarchive"
REPORT="$SDCOMMSDIR/report"
INREJECT="$SDCOMMSDIR/inreject"
CURRDIR="$(pwd)"

# Error codes
readonly ZEROBYTES="2"
readonly NOTCSV="3"
readonly INVALIDLINES="4"

# Oracle users
readonly ECOMUSR="ecom/ec0mdev@DEVPDB"

usage() {
  local FILE=$(basename $0)
  echo ""
  echo "Usage ${FILE}:"
  echo "[-e]       Create all interface entries" 1>&2
  echo "[-s]       Create skus only" 1>&2   
  echo "[-o]       Create order header and lines only" 1>&2
  echo "[-p]       Create preadvice header and lines only" 1>&2
  echo "[-a]       Create addresses only" 1>&2
  echo "[-m mode]  CSV or DIRECT" 1>&2
# echo "[-i] [...] Interface against a list of files" 1>&2   # for now not an option, future functionality 
  echo ""  1>&2
# echo "Note: Please only supply input files with -i. Do not use other options alongside" 1>&2
  echo "Note: using -m DIRECT does not create order data"
  echo "" 1>&2
  exit 1
}

createallhelp() {
  # Stop user going forward if options come with -e
  echo "Please supply option -i with only input interface files" 1>&2
  echo ""
  usage
}

createall() {
  # work through each file
  for FILE in $@
  do  
    echo $FILE
  done
}

reportlog() {
  # log messages to the report log
  local DATE="$(date)"
  local LOGMSG="$1"  
  echo "  reportlog $LOGMSG"

  echo "[ $DATE ] [ $LOGMSG ]" >> $REPORT/report.log
}

logmsg() {
  # log to standard output or file or off
  # NOTE: THIS FUNCTION IS UNDER DEVELOPMENT
  local ENABLED="$1"
  local MESSAGE="$2"
  
  if [[ "$ENABLED" = "true" ]]
  then
    echo "  logmsg $ENABLED"
  fi
}

deletedata() {
  # this function will delete all the records out of the interface
  local RUNVAL="$1"
  local FILE="$2"
  local SQLFILE="DEL_${FILE}_${RUNVAL}.sql"  
  
  echo "  $FUNCNAME $RUNVAL $FILE"

  # delete everything with the runval

  echo "delete from sdif_sku where runval = $RUNVAL;" >> $INWORK/$SQLFILE
  echo "delete from sdif_sku_sku_config where runval = $RUNVAL;" >> $INWORK/$SQLFILE
  echo "delete from sdif_address where runval = $RUNVAL;" >> $INWORK/$SQLFILE
  echo "delete from sdif_pre_advice_header where runval = $RUNVAL;" >> $INWORK/$SQLFILE
  echo "delete from sdif_pre_advice_line where runval = $RUNVAL;" >> $INWORK/$SQLFILE

  if [[ -f "$INWORK/$SQLFILE" ]]
  then
  
    executesql "DEL" $FILE $RUNVAL $SQLFILE 
    if [[ "$?" -ne 0 ]]
    then
      echo "  $FUNCNAME executesql failed" 1>&2
      return 1
    fi

  else
    echo "  $FUNCNAME no sql to execute"
    # might need to return an error

  fi
}

approvedata() {
  # this function will delete all the records out of the interface
  local RUNVAL="$1"
  local FILE="$2"
  local SQLFILE="APV_${FILE}_${RUNVAL}.sql"  
  
  echo "  $FUNCNAME $RUNVAL $FILE"

  # approve everything with the runval

  echo "update sdif_sku set merge_status = 'Released' where runval = $RUNVAL;" >> $INWORK/$SQLFILE
  echo "update sdif_sku_sku_config set merge_status = 'Released' where runval = $RUNVAL;" >> $INWORK/$SQLFILE
  echo "update sdif_address set merge_status = 'Released' where runval = $RUNVAL;" >> $INWORK/$SQLFILE
  echo "update sdif_pre_advice_header set merge_status = 'Released' where runval = $RUNVAL;" >> $INWORK/$SQLFILE
  echo "update sdif_pre_advice_line set merge_status = 'Released' where runval = $RUNVAL;" >> $INWORK/$SQLFILE

  if [[ -f "$INWORK/$SQLFILE" ]]
  then
  
    echo "  $FUNCNAME data has been approved" 
  
    executesql "APV" $FILE $RUNVAL $SQLFILE 
    if [[ "$?" -ne 0 ]]
    then
      echo "  $FUNCNAME executesql failed" 1>&2
      return 1
    fi
	   
    echo "  $FUNCNAME set merge_status=Released	for runval $RUNVAL"
	
  else
    echo "  $FUNCNAME no sql to execute"
    # might need to return an error

  fi
}

rejectfile() {
  # Reject the file
  local FILE="$1"
  local REPORTLOG="$2"
  local RUNVAL="$3"
  local EXTENSION="$(date +%y%m%d""%H%M%S%N)"
  local REJECTFILE="${FILE}.${EXTENSION}.rej"

  # function call
  echo "  $FUNCNAME $FILE $REPORTLOG $RUNVAL"

  # copy file to inreject
  cp -p $INTRAY/$FILE $INREJECT/$REJECTFILE
  echo "  $FUNCNAME copied $INTRAY/$FILE $INREJECT/$REJECTFILE"

  # test if something went wrong
  if [[ "$?" -ne 0 ]]
  then
    # something went wrong
    echo "  $FUNCNAME could not copy $FILE to $INREJECT" 1>&2
    return 1
  fi

  # remove the file locally if you need to
  if [[ -f "$INTRAY/$FILE" ]]
  then
    # remove the file
    rm $INTRAY/$FILE
    echo "  $FUNCNAME removed $INTRAY/$FILE"
  fi

  # remove any lockfiles if we need to
  if [[ "$RUNVAL" != "" ]]
  then
    rm $INWORK/*${RUNVAL}*
    echo "  $FUNCNAME removed lockfiles"
  fi    

  # write into the report log
  if [[ "$REPORTLOG" != "" ]]
  then
    reportlog "$FILE has been rejected $REPORTLOG"
  fi
}

lockfile() {
  # create a lockfile
  local FILETYPE="$1"
  local FILE="$2"
  local INTRAYFILE="$3"
  local RUNVAL="$4"

  local LOCKFILE="${FILETYPE}_${FILE}_${RUNVAL}.lck"

  local DCSINTRAY="$DCS_COMMSDIR/intray"

  # function call
  echo "  $FUNCNAME $FILETYPE $FILE $RUNVAL"

  # check that the file does not already exist in the target directory
  if [[ -f "$INWORK/$LOCKFILE" ]]
  then
    echo "  $FUNCNAME $LOCKFILE exists!" 1>&2

    # reject the file
    rejectfile $FILE "$LOCKFILE already exists"

    # extra comments for the log file
    reportlog "Stop sdcomms process and remove $LOCKFILE. Rename $FILE.$EXTENSION to $FILE and place back in the $INTRAY"
    reportlog "Restart comms"

    return 1     
  fi

  # check that the file does not already exist in the dcs intray either
  if [[ -f "$DCSINTRAY/$INTRAYFILE" ]]
  then
    echo "  $FUNCNAME there is an equal file in the $DCSINTRAY/$INTRAYFILE" 1>&2
    rejectfile "$FILE $DCSINTRAY/$INTRAYFILE already exists"
    
    return 1
  fi 

  # create the lock file
  touch $INWORK/$LOCKFILE
  
  echo "  $FUNCNAME $INWORK/$LOCKFILE created"

  return 0
}

dcsintray() {
  # send to dcs comms intray
  local FILE="$1"
  local RUNVAL="$2"
  local DCSINTRAY="$DCS_COMMSDIR/intray"
  # function call
  echo "  $FUNCNAME $FILE $RUNVAL"

  # move into the working directory
  cd $INWORK

  # find all the files in the intray with the same run number
  for RUNFILE in $(ls -1 *${RUNVAL}*.lck)
  do
    # copy into the dcs intray, and remove the sdcomms intray file and lockfile
    local FILETYPE="$(echo $RUNFILE | cut -c 1-3)"
    local INTRAYFILE="${FILETYPE}_$FILE"
    cp -p $INWORK/$RUNFILE $DCSINTRAY/$INTRAYFILE
    if [[ "$?" -ne 0 ]]
    then
      echo "  $FUNCNAME could not copy $INWORK/$RUNFILE $DCSINTRAY/$INTRAYFILE" 1>&2
      return 1
      exit 1  
    fi
    rm $INWORK/$RUNFILE
    echo "  $FUNCNAME copied $INWORK/$RUNFILE $DCSINTRAY/$INTRAYFILE , lockfile removed"
  done

  rm $INTRAY/$FILE
  echo "  $FUNCNAME removed $INTRAY/$FILE"
}

executesql() {
    # execute the sql
    local TYPE="$1"
    local FILE="$2"
    local RUNVAL="$3"
    local SQLFILE="$4"
    
    local OUTPUTFILE="$TYPE_${FILE}_${RUNVAL}.output"
    local ERRORLOG="$TYPE_${FILE}_${RUNVAL}.err"

    # function call
    echo "  $FUNCNAME $TYPE $FILE $RUNVAL $SQLFILE"
    
    echo "  $FUNCNAME executing $INWORK/$SQLFILE"

    sqlplus -s $ECOMUSR << ! > $INWORK/$OUTPUTFILE
    @$INWORK/$SQLFILE
    commit;
    exit
!

    # check the output file for oracle errors and throw anything into an error log
    grep "ORA" $INWORK/$OUTPUTFILE > $REPORT/$ERRORLOG

    if [[ ! -s "${REPORT}/${ERRORLOG}" ]]
    then
      # looks like we are happy
      echo "  $FUNCNAME no errors detected in $REPORT/$ERRORLOG - removing log"

      # clean up everything as we have done our job
      rm $REPORT/$ERRORLOG
      rm $INWORK/$SQLFILE
      rm $INWORK/$OUTPUTFILE

    elif [[ -s "${REPORT}/${ERRORLOG}" ]]
    then
      # we need to keep the error file and everything else for reporting later on
      echo "  $FUNCNAME errors found in $REPORT/$ERRORLOG" 1>&2
      mv $INWORK/$SQLFILE $REPORT
      mv $INWORK/$OUTPUTFILE $REPORT
      reportlog "ORACLE errors found. dumping trace data..."
      reportlog "$REPORT/$ERRORLOG"
      reportlog "$REPORT/$OUTPUTFILE"
      reportlog "$REPORT/$SQLFILE"
      return 1
   fi
}
  
order() {
  # create a lockfile and order file
  local FUNCTION="$1"
  local FILE="$2"
  local RUNVAL="$3"

  local ODHLCKFILE="ODH_${FILE}_${RUNVAL}.lck"
  local ODLLCKFILE="ODL_${FILE}_${RUNVAL}.lck"
  local INTRAYFILE="ODH_${FILE}"
  local ODTEMPFILE="OD_${FILE}_${RUNVAL}.tmp"

  if [[ "$FUNCTION" = "true" ]]
  then

    # function call
    echo "  $FUNCNAME $FUNCTION $FILE $RUNVAL"
        echo "  $FUNCNAME since this is line by line, output is only for error"

    # create a lockfile
    lockfile "ODH" $FILE $INTRAYFILE $RUNVAL

    # check return value from lockfile
    if [[ "$?" -ne 0 ]]
    then
      echo "  $FUNCNAME could not create a order header lockfile" 1>&2
      return 1
    fi

    echo "  $FUNCNAME $INWORK/$ODHLCKFILE written."

    # create a lockfile
    lockfile "ODL" $FILE $INTRAYFILE $RUNVAL

    # check return value from lockfile
    if [[ "$?" -ne 0 ]]
    then
      echo "  $FUNCNAME could not create a order line lockfile" 1>&2
      return 1
    fi

    echo "  $FUNCNAME $INWORK/$ODLLCKFILE written."

    # Create a new temp file and write only sorted data into it. Then we will use that data to create the lock file
    echo "  $FUNCNAME writing into temp file"
    grep -i -v "^Parent" $FILE | sort -t ',' -k10 | uniq >> $INWORK/$ODTEMPFILE

    # check if the order temp file was created ok
    if [[ "$?" -ne 0 ]]
    then
      echo "  $FUNCNAME could not create a temp file" 1>&2
      return 1
    fi

    LASTORDER=""
    CURRENTORDER=""
    LINENUMBER=0

    echo "  $FUNCNAME stating on order lines"

    while read LINE
    do
      CURRENTORDER="$(echo $LINE | awk -F ',' '{print $10}')"
      if [[ "$CURRENTORDER" != "$LASTORDER" ]]
      then
        if [[ "$LINENUMBER" -ne 0 ]]
        then
          echo "  $FUNCNAME $LASTORDER created $LINENUMBER lines"
        fi

        echo "  $FUNCNAME creating new order header $CURRENTORDER"

        # reset the line no
        LINENUMBER=0

        # grab the start and end string as we need to put the date in a separate field
        ODHSTART="$(echo $LINE | awk -F ',' '{print "PAH,A,"$10",,"$12",Released,,"}')"
        DATE="$(echo $LINE | awk -F ',' '{print $7}' | awk -F '/' '{print "20"$4$3$2$1}')"
        TIME="$(echo $LINE | awk -F ',' '{print $8}' | awk -F ':' '{print $1$2$3}')"
        ODHEND=$(echo ",,SDIRECT,,,,,,,,,,,,,,,,,,,,SDIRECT")

        # compose the line
        ODHFULLSTRING="${ODHSTART}${DATE}${TIME}${ODHEND}"

        echo $ODHFULLSTRING >> $INWORK/$ODHLCKFILE

        # check if the order header was created ok
        if [[ "$?" -ne 0 ]]
        then
          echo "  $FUNCNAME something went wrong writing into $INWORK/$ODHLCKFILE" 1>&2
          return 1
        fi
      fi

      # Create the order line
      ODLSTART="$(echo $LINE | awk -F ',' '{print "PAL,A,"$10","}')"
      LINENUMBER=$(echo $LINENUMBER + 1 | bc -l)
      ENDSTRING="$(echo $LINE | awk -F ',' '{print ","$17$18"-"$19"-"$20",SD1,,,CONT,,"$21",,,,,,SDIRECT,,,,,,,,,,,,,,,,,,,,,,,,,,,EACHES"}')"
      ODLFULLSTRING="${ODLSTART}${LINENUMBER}${ENDSTRING}"

      echo "$ODLFULLSTRING" >> $INWORK/$ODLLCKFILE
      if [[ "$?" -ne 0 ]]
      then
        echo "  $FUNCNAME something went wrong writing into $INWORK/$ODLLCKFILE" 1>&2
        return 1
      fi

      LASTORDER="$CURRENTORDER"

    done < $INWORK/$ODTEMPFILE

    echo "  $FUNCNAME $LASTORDER created $LINENUMBER lines"

    # remove the temp file
    rm $INWORK/$ODTEMPFILE
  fi
}

preadvice() {
  # create a lockfile and preadvice file
  local FUNCTION="$1"
  local FILE="$2"
  local RUNVAL="$3"

  local PAHLCKFILE="PAH_${FILE}_${RUNVAL}.lck"
  local PALLCKFILE="PAL_${FILE}_${RUNVAL}.lck"
  local INTRAYFILE="PAH_${FILE}"
  local PATEMPFILE="PA_${FILE}_${RUNVAL}.tmp"

  if [[ "$FUNCTION" = "true" ]]
  then

    # function call
    echo "  $FUNCNAME $FUNCTION $FILE $RUNVAL"
	echo "  $FUNCNAME since this is line by line, output is only for error"
 
    # create a lockfile
    lockfile "PAH" $FILE $INTRAYFILE $RUNVAL

    # check return value from lockfile
    if [[ "$?" -ne 0 ]]
    then
      echo "  $FUNCNAME could not create a preadvice header lockfile" 1>&2
      return 1
    fi

    echo "  $FUNCNAME $INWORK/$PAHLCKFILE written."

    # create a lockfile
    lockfile "PAL" $FILE $INTRAYFILE $RUNVAL

    # check return value from lockfile
    if [[ "$?" -ne 0 ]]
    then
      echo "  $FUNCNAME could not create a preadvice line lockfile" 1>&2
      return 1
    fi

    echo "  $FUNCNAME $INWORK/$PALLCKFILE written."

    # Create a new temp file and write only sorted data into it. Then we will use that data to create the lock file
    echo "  $FUNCNAME writing into temp file"
    grep -i -v "^Parent" $FILE | sort -t ',' -k10 | uniq >> $INWORK/$PATEMPFILE
    
    # check if the preadvice temp file was created ok	
    if [[ "$?" -ne 0 ]]
    then
      echo "  $FUNCNAME could not create a temp file" 1>&2
      return 1
    fi
    
    LASTPREADVICE=""
    CURRENTPREADVICE=""
    LINENUMBER=0

    echo "  $FUNCNAME stating on preadvice lines"
    
    while read LINE
    do
      CURRENTPREADVICE="$(echo $LINE | awk -F ',' '{print $10}')"
      if [[ "$CURRENTPREADVICE" != "$LASTPREADVICE" ]]
      then
        if [[ "$LINENUMBER" -ne 0 ]]
        then
          echo "  $FUNCNAME $LASTPREADVICE created $LINENUMBER lines"
        fi
        
        echo "  $FUNCNAME creating new preadvice header $CURRENTPREADVICE"
      
        # reset the line no
        LINENUMBER=0
		 
    	# grab the start and end string as we need to put the date in a separate field
    	PAHSTART="$(echo $LINE | awk -F ',' '{print "PAH,A,"$10",,"$12",Released,,"}')"
    	DATE="$(echo $LINE | awk -F ',' '{print $7}' | awk -F '/' '{print "20"$4$3$2$1}')"
    	TIME="$(echo $LINE | awk -F ',' '{print $8}' | awk -F ':' '{print $1$2$3}')"
        PAHEND=$(echo ",,SDIRECT,,,,,,,,,,,,,,,,,,,,SDIRECT")        
        
        # compose the line
    	PAHFULLSTRING="${PAHSTART}${DATE}${TIME}${PAHEND}"
    	
        echo $PAHFULLSTRING >> $INWORK/$PAHLCKFILE
        
    	# check if the preadvice header was created ok
        if [[ "$?" -ne 0 ]]
        then
          echo "  $FUNCNAME something went wrong writing into $INWORK/$PAHLCKFILE" 1>&2
          return 1
        fi
      fi
      
      # Create the preadvice line
      PALSTART="$(echo $LINE | awk -F ',' '{print "PAL,A,"$10","}')"
      LINENUMBER=$(echo $LINENUMBER + 1 | bc -l)
      ENDSTRING="$(echo $LINE | awk -F ',' '{print ","$17$18"-"$19"-"$20",SD1,,,CONT,,"$21",,,,,,SDIRECT,,,,,,,,,,,,,,,,,,,,,,,,,,,EACHES"}')"
      PALFULLSTRING="${PALSTART}${LINENUMBER}${ENDSTRING}"
	
      echo "$PALFULLSTRING" >> $INWORK/$PALLCKFILE
      if [[ "$?" -ne 0 ]]
      then
        echo "  $FUNCNAME something went wrong writing into $INWORK/$PALLCKFILE" 1>&2
        return 1
      fi	
    
      LASTPREADVICE="$CURRENTPREADVICE"

    done < $INWORK/$PATEMPFILE
   
    echo "  $FUNCNAME $LASTPREADVICE created $LINENUMBER lines"

    # remove the temp file
    rm $INWORK/$PATEMPFILE
  fi
}

preadvice_direct() {
  # take the data from the file and send into the sdif_address interface table
  local FUNCTION="$1"
  local FILE="$2"
  local RUNVAL="$3"

  local TEMPFILE="PA_${FILE}_${RUNVAL}.tmp"
  local H_SQLFILE="PAH_${FILE}_${RUNVAL}.sql"
  local H_OUTPUTFILE="PAH_${FILE}_${RUNVAL}.output"

  local L_SQLFILE="PAL_${FILE}_${RUNVAL}.sql"

  local CURRENTDATE="$(date +%d%m%y-%H:%M:%S)"

  if [[ "$FUNCTION" = "true" ]]
  then

    # function call
    echo "  $FUNCNAME $FUNCTION $FILE $RUNVAL"
    echo "  $FUNCNAME since this is line by line, output is only for error"

    # Create a new temp file and write only sorted data into it. Then we will use that data to create the lock file
    echo "  $FUNCNAME writing into temp file $INWORK/$TEMPFILE"

    grep -i -v "^Parent" $FILE | sort -t ',' -k10 | uniq >> $INWORK/$TEMPFILE

    # check if the temp file was created ok
    if [[ "$?" -ne 0 ]]
    then
      echo "  $FUNCNAME could not create a temp file $INWORK/$TEMPFILE" 1>&2
      return 1
    fi

    LASTPREADVICE=""
    CURRENTPREADVICE=""
    LINENUMBER=0

    echo "  $FUNCNAME starting on lines"

    while read LINE
    do
      CURRENTPREADVICE="$(echo $LINE | awk -F ',' '{print $10}')"
      if [[ "$CURRENTPREADVICE" != "$LASTPREADVICE" ]]
      then
        if [[ "$LINENUMBER" -ne 0 ]]
        then
          echo "  $FUNCNAME $LASTPREADVICE created $LINENUMBER lines"
        fi

        echo "  $FUNCNAME creating new preadvice header $CURRENTPREADVICE"

        # reset the line no
        LINENUMBER=0

        # grab the start and end string as we need to put the date in a separate field
        
        H_START="$(echo $LINE | awk -F ',' '{print "insert into sdif_pre_advice_header (pre_advice_id, supplier_id, user_def_type_6, user_def_type_7, runval, if_date) values (\x27" $10 "\x27, \x27" $12 "\x27" }')"
        DATE="$(echo $LINE | awk -F ',' '{print $7}' | awk -F '/' '{print "20"$4$3$2$1}')"
        TIME="$(echo $LINE | awk -F ',' '{print $8}' | awk -F ':' '{print $1$2$3}')"
        DATETIME="$(echo \'${DATE}${TIME}\')"
        MEMO="$(echo $LINE | awk -F ',' '{print "\x27" $6 "\x27"}')"

        # compose the line
        H_FULLSTRING="${H_START},${DATETIME},${MEMO},${RUNVAL},'${CURRENTDATE}');"

        echo $H_FULLSTRING >> $INWORK/$H_SQLFILE

        # check if the preadvice header was created ok
        if [[ "$?" -ne 0 ]]
        then
          echo "  $FUNCNAME something went wrong writing into $INWORK/$H_SQLFILE" 1>&2
          return 1
        fi
      fi

      # Create the preadvice line

      L_START="$(echo $LINE | awk -F ',' '{print "insert into sdif_pre_advice_line (pre_Advice_id, line_id, sku_id, qty_due, runval, if_date) values (\x27" $10 "\x27"}')"
      LINENUMBER=$(echo $LINENUMBER + 1 | bc -l)
      
      SKUID="$(echo $LINE | awk -F ',' '{print "\x27" $17$18"-"$19"-"$20 "\x27"}')"
      QTY="$(echo $LINE | awk -F ',' '{print $21}')"
      
      L_FULLSTRING="${L_START},${LINENUMBER},${SKUID},${QTY},${RUNVAL},'${CURRENTDATE}');"

      echo "$L_FULLSTRING" >> $INWORK/$L_SQLFILE
      if [[ "$?" -ne 0 ]]
      then
        echo "  $FUNCNAME something went wrong writing into $INWORK/$PALLCKFILE" 1>&2
        return 1
      fi

      LASTPREADVICE="$CURRENTPREADVICE"

    done < $INWORK/$TEMPFILE

    echo "  $FUNCNAME $LASTPREADVICE created $LINENUMBER lines"

    if [[ -f "$INWORK/$H_SQLFILE" ]]
    then

      # execute the sql
      executesql "PAH" $FILE $RUNVAL $H_SQLFILE
      if [[ "$?" -ne 0 ]]
      then
        echo "  $FUNCNAME executesql failed" 1>&2
        rm $INWORK/$TEMPFILE   
        return 1
      fi

      executesql "PAL" $FILE $RUNVAL $L_SQLFILE
      if [[ "$?" -ne 0 ]]
      then
        echo "  $FUNCNAME executesql failed" 1>&2
        rm $INWORK/$TEMPFILE
        return 1
      fi

    else

      echo "  $FUNCNAME no sql to execute"
      # might need to return an error

    fi


    # always tidy up the temp file
    rm $INWORK/$TEMPFILE

  fi
}


sku_sku_config() {
  # create a lockfile and sku_sku_config file
  local FUNCTION="$1"
  local FILE="$2"
  local RUNVAL="$3"

  local LOCKFILE="SSC_${FILE}_${RUNVAL}.lck"
  local INTRAYFILE="SSC_${FILE}"

  if [[ "$FUNCTION" = "true" ]]
  then

    # function call
    echo "  sku_sku_config $FUNCTION $FILE $RUNVAL"

    # create a lockfile
    lockfile "SSC" $FILE $INTRAYFILE $RUNVAL

    # check return value from lockfile
    if [[ "$?" -ne 0 ]]
    then
      echo "  sku_config could not create a sku config lockfile" 1>&2
      return 1
    fi

    echo "  sku_sku_config writing into $LOCKFILE"
    grep -i -v "^Parent" $FILE | awk -F ',' '{print "SSC,A,"$17$18"-"$19"-"$20",SD1,,SDIRECT"}' | sort -t ',' -k 3  | uniq >> $INWORK/$LOCKFILE
    if [[ "$?" -ne 0 ]]
    then
      echo "something went wrong writing into $INWORK/$LOCKFILE" 1>&2
      return 1
    fi

    # confirm the lockfile has been written to
    echo "  sku_sku_config $INWORK/$LOCKFILE written."

  fi
}

sku_sku_config_direct() {
  # take the data from the file and send into the sdif_sku_sku_config interface table
  local FUNCTION="$1"
  local FILE="$2"
  local RUNVAL="$3"

  local TEMPFILE="SSC_${FILE}_${RUNVAL}.tmp"
  local SQLFILE="SSC_${FILE}_${RUNVAL}.sql"
  local OUTPUTFILE="SSC_${FILE}_${RUNVAL}.output"
  local ERRORLOG="SSC_${FILE}_${RUNVAL}.err"
  local CURRENTDATE="$(date +%d%m%y-%H:%M:%S)"

  if [[ "$FUNCTION" = "true" ]]
  then

    # function call
    echo "  $FUNCNAME $FUNCTION $FILE $RUNVAL"

    # Create a new temp file and write only sorted data into it. Then we will use that data to create the lock file
    echo "  $FUNCNAME writing into temp file $INWORK/$TEMPFILE"

    grep -i -v "^Parent" $FILE | awk -F ',' '{print $17$18"-"$19"-"$20}' | sort -t ',' -k1 | uniq>> $INWORK/$TEMPFILE

    # check if the temp file was created ok
    if [[ "$?" -ne 0 ]]
    then
      echo "  $FUNCNAME could not create a temp file $INWORK/$TEMPFILE" 1>&2
      return 1
    fi

    # start building the sql file
    echo "  $FUNCNAME writing into sql file $INWORK/$SQLFILE"
    while read LINE
    do
      PRECHECK="$(echo $LINE | awk -F ',' '{print $1}')"
      if [[ "$PRECHECK" != "" ]]
      then
        echo "${LINE},${RUNVAL},${CURRENTDATE}" | awk -F ',' '{print "insert into sdif_sku_sku_config (sku_id, config_id, runval, if_date) values (\x27" $1 "\x27,\x27SDIRECT\x27," $2 ",\x27" $3 "\x27);"}' >> $INWORK/$SQLFILE
      fi
    done < $INWORK/$TEMPFILE

    # Check we have not had any errors
    if [[ "$?" -ne 0 ]]
    then
      echo "  $FUNCNAME something went wrong in the creation of $INWORK/$SQLFILE" 1>&2
      return 1
    fi

    if [[ -f "$INWORK/$SQLFILE" ]]
    then

      # execute the sql
      executesql "SKU" $FILE $RUNVAL $SQLFILE
      if [[ "$?" -ne 0 ]]
      then
        echo "  $FUNCNAME executesql failed" 1>&2
        rm $INWORK/$TEMPFILE
        return 1
      fi

    else

      echo "  $FUNCNAME no sql to execute"
      # might need to return an error

    fi


    # tidy up
    rm $INWORK/$TEMPFILE

  fi
}

sku() {
  # create a lockfile and skufile
  local FUNCTION="$1"
  local FILE="$2"
  local RUNVAL="$3"

  local LOCKFILE="SKU_${FILE}_${RUNVAL}.lck"
  local INTRAYFILE="SKU_${FILE}"
  
  if [[ "$FUNCTION" = "true" ]]
  then
    
    # function call
    echo "  $FUNCNAME $FUNCTION $FILE $RUNVAL"   


    # create a lockfile
    lockfile "SKU" $FILE $INTRAYFILE $RUNVAL 
  
    # check return value from lockfile
    if [[ "$?" -ne 0 ]]
    then
      echo "  $FUNCNAME could not create a sku lockfile" 1>&2
      return 1
    fi
    
    echo "  sku writing into $LOCKFILE"
    grep -i -v "^Parent" $FILE | awk -F ',' '{print "SKU,A,"$17$18"-"$19"-"$20",,"$24" "$25" "$26",,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,SDIRECT"}' | sort -t ',' -k 3 | uniq >> $INWORK/$LOCKFILE 
    if [[ "$?" -ne 0 ]]
    then
      echo "  $FUNCNAME something went wrong writing into $INWORK/$LOCKFILE" 1>&2
      return 1
    fi     
    
    # confirm the lockfile has been written to
    echo "  $FUNCNAME $INWORK/$LOCKFILE written."
    
  fi
}

sku_direct() {
  # take the data from the file and send into the sdif_sku interface table
  local FUNCTION="$1"
  local FILE="$2"
  local RUNVAL="$3"

  local TEMPFILE="SKU_${FILE}_${RUNVAL}.tmp"
  local SQLFILE="SKU_${FILE}_${RUNVAL}.sql"
  local OUTPUTFILE="SKU_${FILE}_${RUNVAL}.output"
  local ERRORLOG="SKU_${FILE}_${RUNVAL}.err"
  local CURRENTDATE="$(date +%d%m%y-%H:%M:%S)"

  if [[ "$FUNCTION" = "true" ]]
  then

    # function call
    echo "  $FUNCNAME $FUNCTION $FILE $RUNVAL"

    # Create a new temp file and write only sorted data into it. Then we will use that data to create the lock file
    echo "  $FUNCNAME writing into temp file $INWORK/$TEMPFILE"

    grep -i -v "^Parent" $FILE | awk -F ',' '{print $17$18"-"$19"-"$20","$24" "$25" "$26}' | sort -t ',' -k1 | uniq >> $INWORK/$TEMPFILE

    # check if the temp file was created ok
    if [[ "$?" -ne 0 ]]
    then
      echo "  $FUNCNAME could not create a temp file $INWORK/$TEMPFILE" 1>&2
      return 1
    fi

    # start building the sql file
    echo "  $FUNCNAME writing into sql file $INWORK/$SQLFILE"
    while read LINE
    do
      PRECHECK="$(echo $LINE | awk -F ',' '{print $1}')"
      if [[ "$PRECHECK" != "" ]]
      then
        echo "${LINE},${RUNVAL},${CURRENTDATE}" | awk -F ',' '{print "insert into sdif_sku (sku_id, description, runval, if_date) values (\x27" $1 "\x27,\x27" $2 "\x27," $3 ",\x27" $4 "\x27);"}' >> $INWORK/$SQLFILE
      fi
    done < $INWORK/$TEMPFILE

    # Check we have not had any errors
    if [[ "$?" -ne 0 ]]
    then
      echo "  $FUNCNAME something went wrong in the creation of $INWORK/$SQLFILE" 1>&2
      return 1
    fi

    if [[ -f "$INWORK/$SQLFILE" ]]
    then

      # execute the sql
      executesql "SKU" $FILE $RUNVAL $SQLFILE
      if [[ "$?" -ne 0 ]]
      then
        echo "  $FUNCNAME executesql failed" 1>&2
        rm $INWORK/$TEMPFILE
        return 1
      fi
    else

      echo "  $FUNCNAME no sql to execute"
      # might need to return an error

    fi

    # tidy up 
    rm $INWORK/$TEMPFILE

  fi
}

address() {
  # create a lockfile and address file
  local FUNCTION="$1"
  local FILE="$2"
  local RUNVAL="$3"

  local LOCKFILE="ADR_${FILE}_${RUNVAL}.lck"
  local INTRAYFILE="ADR_${FILE}"

  if [[ "$FUNCTION" = "true" ]]
  then

    # function call
    echo "  address $FUNCTION $FILE $RUNVAL"

    # create a lockfile
    lockfile "ADR" $FILE $INTRAYFILE $RUNVAL

    # check return value from lockfile
    if [[ "$?" -ne 0 ]]
    then
      echo "  address could not create an address lockfile" 1>&2
      return 1
    fi

    echo "  address writing into $LOCKFILE"
    grep -i -v "^Parent" $FILE | awk -F ',' '{print "ADR,A,"$12",Supp/Cust,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,SDIRECT"}' | sort -t ',' -k 3 | uniq >> $INWORK/$LOCKFILE
    if [[ "$?" -ne 0 ]]
    then
      echo "something went wrong writing into $INWORK/$LOCKFILE" 1>&2
      return 1
    fi

    # confirm the lockfile has been written to
    echo "  address $INWORK/$LOCKFILE written."

  fi
}

address_direct() {
  # take the data from the file and send into the sdif_address interface table
  local FUNCTION="$1"
  local FILE="$2"
  local RUNVAL="$3"

  local TEMPFILE="ADR_${FILE}_${RUNVAL}.tmp"
  local SQLFILE="ADR_${FILE}_${RUNVAL}.sql"
  local OUTPUTFILE="ADR_${FILE}_${RUNVAL}.output"
  local ERRORLOG="ADR_${FILE}_${RUNVAL}.err"
  local CURRENTDATE="$(date +%d%m%y-%H:%M:%S)"

  if [[ "$FUNCTION" = "true" ]]
  then

    # function call
    echo "  $FUNCNAME $FUNCTION $FILE $RUNVAL"

    # Create a new temp file and write only sorted data into it. Then we will use that data to create the lock file
    echo "  $FUNCNAME writing into temp file $INWORK/$TEMPFILE"

    grep -i -v "^Parent" $FILE | awk -F ',' '{print $12}' | sort -t ',' -k1 | uniq >> $INWORK/$TEMPFILE

    # check if the temp file was created ok
    if [[ "$?" -ne 0 ]]
    then
      echo "  $FUNCNAME could not create a temp file $INWORK/$TEMPFILE" 1>&2
      return 1
    fi

    # start building the sql file
    echo "  $FUNCNAME writing into sql file $INWORK/$SQLFILE"
    while read LINE
    do
      PRECHECK="$(echo $LINE | awk -F ',' '{print $1}')"
      if [[ "$PRECHECK" != "" ]]
      then 
        echo "${LINE},${RUNVAL},${CURRENTDATE}" | awk -F ',' '{print "insert into sdif_address (address_id, runval, if_date) values (\x27" $1 "\x27," $2 ",\x27" $3 "\x27);"}' >> $INWORK/$SQLFILE
      fi
    done < $INWORK/$TEMPFILE

    # Check we have not had any errors
    if [[ "$?" -ne 0 ]]
    then
      echo "  $FUNCNAME something went wrong in the creation of $INWORK/$SQLFILE" 1>&2
      return 1
    fi

    if [[ -f "$INWORK/$SQLFILE" ]]
    then 
   
      # execute the sql
      executesql "ADR" $FILE $RUNVAL $SQLFILE
      if [[ "$?" -ne 0 ]]
      then
        echo "  $FUNCNAME executesql failed" 1>&2
        rm $INWORK/$TEMPFILE
        return 1
      fi
      
    else
    
      echo "  $FUNCNAME no sql to execute"
      # might need to return an error

    fi

    # tidy up
    rm $INWORK/$TEMPFILE

  fi
}

checkcsv() {
  # check that the contents of the file are something we would be happy to work with
  local FILE="$1"

  # function call
  echo  "  $FUNCNAME $FILE"
 
  # we should be safe to remove empty lines
  echo "  $FUNCNAME removing empty lines"
  sed -i '/^$/d' $FILE

  # check 0 - does this file have a size greater than zero bytes
  if [[ ! -s "$FILE" ]]
  then   
    echo "  $FUNCNAME $FILE is zero bytes"
    rejectfile $FILE "- zero bytes"
	return $ZEROBYTES
  fi
  
  # check 1 - does this even look like a csv
  local CSVLINES="$(grep , $FILE | wc -l)"
  if [[ "$CSVLINES" -eq 0 ]]
  then
    echo "  $FUNCNAME $FILE is not a CSV File" 1>&2
    rejectfile $FILE "- not a csv"
    return $NOTCSV
  fi

  # check 2 - do we have any lines that are not CSV
  local NONCSVLINES="$(grep -v , $FILE | wc -l)"
  if [[ "$NONCSVLINES" -gt 0 ]]
  then
    echo "  $FUNCNAME $FILE contains non CSV lines" 1>&2
    rejectfile $FILE "- invalid lines"
    return $INVALIDLINES
  fi  

  # check 3 - does the word Supplier appear
  local DATACHECK="$(grep -i "Supplier" $FILE | wc -l)"
  if [[ "$DATACHECK" -eq 0 ]]
  then
    echo "  $FUNCNAME $FILE contains invalid data"
    rejectfile $FILE "- invalid data"
    return $INVALIDLINES
  fi

  # checks must have passed so return 0

  return 0
}

startsdcomms() {
  # starting the sdcomms process
  echo "  startsdcomms start"
  
  local RUNNING="$HOME/bin/running"
  
  if [[ -f "$RUNNING" ]]
  then
    echo "Cannot start comms. Comms is already running" 1>&2
    exit 1
  fi

  touch $RUNNING
}
  
stopsdcomms() {
  # stopping the sdcomms process
  echo "  stopsdcomms stop"

  local RUNNING="$HOME/bin/running"

  if [[ -f "$RUNNING" ]]
  then
    rm $RUNNING
    # check if the file was removed 
    if [[ "$?" -ne 0 ]]
    then
      echo "$RUNNING could not be removed" 1>&2
      exit 1
    fi
  fi
}

#  ***** MAIN PROGRAM *****

# Parse the options
while getopts esopai:m:? OPTION
do
  case $OPTION in
    e) CREATEALL="true" ;;
    s) SKU="true" ;;
    o) ORDER="true" ;;
    p) PREADVICE="true" ;;
    a) ADDRESS="true" ;;
    i) INPUTFILES="true" ;;
    m) MODE="$OPTARG" ;;
    ?) usage ;;
  esac
done

# check there is at least one argument
if [[ "$#" -eq 0 ]]
then
  echo "Please supply at least one argument" 1>&2
  usage
  exit 1
fi

# check the sdcommsdir is setup
if [[ ! -d "$SDCOMMSDIR" ]]
then
  echo "$SDCOMMSDIR has not been setup" 1>&2
  exit 1
fi

# check there is not aother process running
RUNNING="running"
if [[ -f "$RUNNING" ]]
then
  echo "There is a process already running" 1>&2
  exit 1
fi

#echo "Number of arguments: $#"

# if the number of arguments is 1 and inputfiles set to true then sow the user usage options
if [[ "$#" -eq 1 ]] && [[ "$INPUTFILES" = "true" ]]
then
  echo "No input files supplied" 1>&2
  echo ""
  usage
fi

# extra error validation so that -e does not have other optional arguments alongside it
if [[ "$INPUTFILES" = "true" ]] 
then
  if [[ "$CREATEALL" = "true" ]]
  then
    createallhelp
  elif [[ "$SKU" = "true" ]]
  then 
    createallhelp
  elif [[ "$ORDER" = "true" ]]
  then
    createallhelp
  elif [[ "$PREADVICE" = "true" ]]
  then
    createallhelp
  elif [[ "$ADDRESS" = "true" ]]
  then  
    createallhelp
  fi
fi

# validate the user supplied the correct argument with mode
case $MODE in
  csv)    MODE="CSV" ;;
  CSV)    MODE="CSV" ;;
  direct) MODE="DIRECT" ;;
  DIRECT) MODE="DIRECT" ;;
  *)      echo "Please supply -m option with appropriate argument" 1>&2
          echo "" 1>&2
          usage ;;
esac

# set up the report log if we need to
if [[ ! -f "$REPORT/report.log" ]]
then
  touch $REPORT/report.log
fi

# starting comms
startsdcomms

# if create all is set then set all args to true
if [[ "$CREATEALL" = "true" ]]
then
  echo "Setting all args to true"
  SKU="true"
  ORDER="true"
  PREADVICE="true"
  ADDRESS="true"
fi

# current arguments
echo ""
echo "Current Args:"
echo "INPUTFILES: $INPUTFILES"
echo "CREATEALL:  $CREATEALL"
echo "SKU:        $SKU"
echo "ORDER:      $ORDER"
echo "PREADVICE:  $PREADVICE"
echo "ADDRESS:    $ADDRESS"
echo "MODE:       $MODE"
echo ""

# remove the optional arguements for input files and work through the list
if [[ "$INPUTFILES" = "true" ]]
then
  shift
  createall $@
  
  # stopping comms
  stopsdcomms
  exit
fi

# find receive interface file into commsdir
cd $INTRAY
NUM_FILES="$(ls *.csv -1 2> /dev/null | sort | wc -l)"

# if the number of files is zero then just exit
if [[ "$NUM_FILES" -eq 0 ]]
then
  echo "No files found in $INTRAY" 1>&2
  stopsdcomms
  exit 1
fi

# find all csv files and create a working script inside temp area
FILELIST="filelist.$$"
ls -1rt *.csv > $SDCOMMSDIR/$FILELIST

# check that the file was created ok
if [[ "$?" -ne 0 ]]
then
  echo "File list could not be created" 1>&2
  exit 1
fi

# for every file in the intray go through and interface
for FILE in $(cat $SDCOMMSDIR/$FILELIST)
do
  echo ""
  echo "Looping on $FILE"

  # move into the local intray to start actioning each file 
  cd $INTRAY
 
  # archive the current file
  #EXTENSION=$(getrefnum)
  EXTENSION="$(date +%y%m%d""%H%M%S%N)"
  echo "Archiving $FILE $INARCHIVE/$FILE.$EXTENSION"
  cp -p $FILE $INARCHIVE/$FILE.$EXTENSION

  # better to only switch to a .lck file once we are sure we want to do something with this file, otherwise we can just reject it
  checkcsv $FILE
 
  if [[ "$?" -ne 0 ]]
  then
    echo "$FILE failed CSV validation" 1>&2
    
    continue    

  else
    echo "$FILE has passed initial content checks"
  fi

  echo "Starting interface on $FILE"

  # generate unique run number to pass into each of the functions when creating lock files
  #  - we will be using this as a way to track the number of files we expect to the actual number we get
  STARTVAL=$(echo $RANDOM | cut -c 1-4)
  ENDVAL=$(date +%N | cut -c 1-4)

  RUNVAL=${STARTVAL}${ENDVAL}
  echo "Generated run number: $RUNVAL" 

#createall $CREATEALL $FILE

  if [[ "$MODE" = "CSV" ]]
  then
    echo "CSV Interface"
	
	# pre validate how many .lck files are we expecting
    SKUFILES=0
    PREADVFILES=0
    ORDERFILES=0
    ADDRFILES=0

    if [[ "$SKU" = "true" ]]
    then
      SKUFILES=2      # sku, sku_sku_config
    fi

    if [[ "$PREADVICE" = "true" ]]
    then
      PREADVFILES=2   # pre_advice_header, pre_advice_line
    fi
  
    if [[ "$ORDER" = "true" ]]
    then
      ORDERFILES=2    # order_header, order_line
    fi

    if [[ "$ADDRESS" = "true" ]]
    then
      ADDRFILES=1     # address
    fi

    EXPECT="$(expr $SKUFILES + $PREADVFILES + $ORDERFILES + $ADDRFILES)"
    echo "Pre validation - expecting to create $EXPECT files"
	
    
    sku $SKU $FILE $RUNVAL
    if [[ "$?" -ne 0 ]]
    then
      echo "Something went wrong with the sku interface" 1>&2
      SKUIFFAIL="true"
    fi
  
    sku_sku_config $SKU $FILE $RUNVAL
    if [[ "$?" -ne 0 ]]
    then
      echo "Something went wrong with the sku_sku_config interface" 1>&2
      SKUIFFAIL="true"
    fi
  
    order $ORDER $FILE $RUNVAL
    if [[ "$?" -ne 0 ]]
    then
      echo "Something went wrong with the order interface" 1>&2
      ODIFFAIL="true"
    fi

    preadvice $PREADVICE $FILE $RUNVAL
    if [[ "$?" -ne 0 ]]
    then
      echo "Something went wrong with the preadvice interface" 1>&2
      PAIFFAIL="true"
    fi
  
    address $ADDRESS $FILE $RUNVAL
    if [[ "$?" -ne 0 ]]
    then
      echo "Something went wrong with the address interface" 1>&2
      ADIFFAIL="true"
    fi

    # post validation how many .lck files have we ended up with
    ACTUAL="$(ls -1 ${INWORK}/*${RUNVAL}*.lck | wc -l)"
  
    if [[ $EXPECT -ne $ACTUAL ]]
    then
      echo "The expected number of lock files do not match what we actually got" 1>&2
      echo "Expected: $EXPECT vs Actual: $ACTUAL" 1>&2
      echo "Reject" 1>&2
      REJECT="true"
    
      # reject the file and rollback the lockfiles
      ERRORLOG="- Expected lock files: $EXPECT vs Actual lock files: $ACTUAL" 
      rejectfile $FILE "$ERRORLOG" $RUNVAL

      continue  
 
    elif [[ "SKUIFFAIL" = "true" ]] || [[ $PAIFFAIL = "true" ]] 
    then
      echo "Something went wrong"
      echo "Reject"
      REJECT="true"
   
      # reject the file and rollback the lockfiles
      ERRORLOG="- Problem found in one of the interfaces" 1>&2
      reject $FILE "$ERRORLOG" $RUNVAL
    
      continue   

    else
      echo "Post validation - passed. Created $ACTUAL files"

    fi
  
      dcsintray $FILE $RUNVAL 
    
  else
    # mode is direct
    echo "DIRECT interface"
   
    sku_direct $SKU $FILE $RUNVAL
    if [[ "$?" -ne 0 ]]
    then
      echo "Something went wrong with the direct sku interface" 1>&2
      SKUIFFAIL="true"
    fi
    
    sku_sku_config_direct $SKU $FILE $RUNVAL
    if [[ "$?" -ne 0 ]]
    then
      echo "Something went wrong with the direct sku_sku_config interface" 1>&2
      SKUIFFAIL="true"
    fi

    address_direct $ADDRESS $FILE $RUNVAL
    if [[ "$?" -ne 0 ]]
    then
      echo "Something went wrong with the direct address interface" 1>&2
      ADDRESSIFFAIL="true"
    fi
    
    preadvice_direct $PREADVICE $FILE $RUNVAL
    if [[ "$?" -ne 0 ]]
    then
      echo "Something went wrong with the direct preadvice interface" 1>&2
      PREADVICEIFFAIL="true"
    fi

    if [[ "$SKUIFFAIL" = "true" ]] || [[ "$ADDRESSIFFAIL" = "true" ]] || [[ "$PREADVICEIFFAIL" = "true" ]]
    then
      echo "Something has gone wrong with the direct interface" 1>&2
      deletedata $RUNVAL $FILE
      if [[ "$?" -ne 0 ]]
      then
        echo "There was a problem with deletedata" 1>&2
      fi
      rm $INTRAY/$FILE 
	  echo "Removed intray file $INTRAY/$FILE"
    else 
      # remove the file from the intray and approve the records
      approvedata $RUNVAL $FILE
      rm $INTRAY/$FILE
	  echo "Removed intray file $INTRAY/$FILE"
    fi

  fi
done

echo ""

# cleanup files
rm $SDCOMMSDIR/$FILELIST

stopsdcomms

echo ""
echo "Tail $REPORT/reports.log : "
tail -10 $REPORT/report.log # temp

exit 0

