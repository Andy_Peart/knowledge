#!/bin/sh

##################################################################################################################################################################################
# Script name: Workstation_install.sh
# Creator: Andy Peart
# Date: 18/10/2019
# Version: 1.0
# Purpose: Automatically copy a source client into a target client
# ***************************************CHANGES**********************************************************************************************************************************
#
##################################################################################################################################################################################

. $HOME/dcs/.dcs_profile

PROG=$(basename $0)
DATETIMEFILE=`date +%Y%m%d%H%M%S`
DATEFILE=`date +%Y%m%d`
LOGFILE=$DCS_PACKAGELOGDIR/$PROG"_"$DATEFILE.log

function usage
{
        echo ""
        echo "Usage: $PROG [options]"
        echo ""
        echo "[-b]       REQUIRED - Start Port"
        echo "[-e]       REQUIRED - End Port"
        echo "[-s]       REQUIRED - Site ID"
        echo "[-r]       REQUIRED - Rows"
        echo "[-c]       REQUIRED - columns"
		echo "[-a]       REQUIRED - Daemon Active Y/N"
		echo ""
        echo "Note This scrip will create RDT Workstation records and the required Daemons"
        echo ""
}

while getopts b:e:s:r:c:a:? 2> /dev/null ARG 
do 
 case ${ARG} 
 in 
  b) STARTPORT=${OPTARG};;
  e) MAXPORT=${OPTARG};;
  s) SITEID=${OPTARG};;
  r) ROWS=${OPTARG};;
  c) COLUMNS=${OPTARG};;
  a) ACTIVE=${OPTARG};;
  ?) usage
     exit 1;;
 esac 
done 

echo ""
echo "You have entered the following data:"
echo ""
echo "Starting Port:           $STARTPORT"
echo "Last Port:               $MAXPORT"
echo "Site ID:                 $SITEID"
echo "Screen Rows:             $ROWS"
echo "Screen Columns:          $COLUMNS"
echo "Activate Ports:          $ACTIVE"
echo ""

read -p "Do you wish to continue (y/n)?" choice
case "$choice" in 
  y|Y ) echo "yes";;
  n|N ) echo "no" ; exit 0;;
  * ) echo "invalid"; exit 0;;
esac

echo ""
echo "Starting to create workstations..."
echo ""

sqlplus -s $ORACLE_USR  << ! >| $LOGFILE
SET SERVEROUTPUT ON;
DECLARE
  Logging               VARCHAR2(30) := '[WORKSTATION] - [';
  
  /* collect all script variables */
  v_start workstation.PTYSVR_PORT_NO%type 			:= $STARTPORT;
  v_end workstation.PTYSVR_PORT_NO%type 			:= $MAXPORT;
  v_site workstation.site_id%type 					:= upper('$SITEID');
  v_rows workstation.SCREEN_ROWS%type 				:= $ROWS;
  v_columns workstation.SCREEN_COLUMNS%type 		:= $COLUMNS;
  v_active varchar2(1) 								:= upper('$ACTIVE');
  l_current_port  workstation.PTYSVR_PORT_NO%type 	:= $STARTPORT;
 begin
	dbms_output.put_line(Logging ||'start '||v_start||' end '||v_end||' site '|| v_site||' rows '||v_rows||' columns '||v_columns|| ' Active '||v_active);
	while l_current_port <= v_end
		loop
			/* Workstaion Insert */
			insert into workstation values('RDT'||l_current_port,'RDT','N','',v_site,'','','','','','','Y','N','N','','','','','','','','','','','','','1','TROLLEY','','TOTE','99','Y','N','','','','','','',v_site||' RDT5'||l_current_port,l_current_port,l_current_port,'','','','','EN_GB',v_columns,v_rows,'','','','','','','','','','N','N','N','N','','','','','','','','','','','N','','','','N','','N','','','','','');
			dbms_output.put_line(Logging ||' RDT'||l_current_port|| ' workstation added]');
			insert into SIA_Process values ('rdt'||l_current_port,'emu','','N','revive','ritch','-j '||l_current_port||' -s','','','','','0',v_active,'81','','','Automatic','18-JUN-16 09.14.11.000000000','','',v_site);
			dbms_output.put_line(Logging ||' RDT'||l_current_port|| ' RDT Daemon added]');
			insert into SIA_Process values ('pty'||l_current_port,'pty','','N','revive','ptysvr','-n -t -p '||l_current_port||' crtrdt -j '||l_current_port||' -n -a -r '||v_rows||' -c '||v_columns,'','','','','0',v_active,'76','','','Automatic','18-JUN-16 09.14.03.000000000','','',v_site);
			dbms_output.put_line(Logging ||' RDT'||l_current_port|| ' PTY Daemon added]');
			commit;
			l_current_port := l_current_port +1;
		end loop;
	commit;
 end;
 /
!


echo ""
echo "All records added"

echo ""
echo "cat $LOGFILE"
cat $LOGFILE

echo ""
echo "Daemons being Refreshed"
telsia refresh

echo ""
echo "COMPLETED"

