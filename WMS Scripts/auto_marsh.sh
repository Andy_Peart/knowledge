#!/bin/sh

##################################################################################################################################################################################
# Script name: auto_marsh.sh
# Creator: Andy Peart
# Date: 22/11/2019
# Version: 1.0
# Purpose: Automatically Marshal the first container Packed when the task does not move
#
##################################################################################################################################################################################

. $HOME/dcs/.dcs_profile

PROG=$(basename $0)
DATETIMEFILE=`date +%Y%m%d%H%M%S`
DATEFILE=`date +%Y%m%d`
LOGFILE=$DCS_PACKAGELOGDIR/$PROG"_"$DATEFILE.log

sqlplus -s $ORACLE_USR  << ! >| $LOGFILE
SET SERVEROUTPUT ON;
DECLARE
  Logging               VARCHAR2(30) := '[Pallet] - [';
  l_count        NUMBER(5) := 0;
  v_commit_limit NUMBER(5) := 10;
  v_code         NUMBER;
  v_errm         VARCHAR2(64);

Cursor pall_search is
  select mt.pallet_id "pallet" 
    from move_task mt, order_container oc, order_header oh
    where oc.pallet_id = mt.pallet_id
    and oc.v_tracking_code is not null
    and mt.task_id = 'PALLET'
    and mt.status = 'Released'
    and oc.order_id = oh.order_id
    and oh.status not in ('Cancelled','Shipped')
    and mt.dstamp < sysdate -(5/1440);

pall_row pall_search%rowtype;


begin

l_count := 0;
loop
  fetch pall_search into pall_row;
  if pall_search
	----------------------------------------------------------------------------------
end;
 /
!

echo ""
echo "cat $LOGFILE"
cat $LOGFILE

echo ""
echo "Daemons being Refreshed"
telsia refresh

echo ""
echo "COMPLETED"

