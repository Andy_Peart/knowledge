#!/bin/bash
################################################################################
#                                                                              #
#                               Clipper                                        #
#                                                                              #
# SCRIPT NAME: simbaif_sku_relocation.sh - ModernLogic                         #
#                                                                              #
# DESCRIPTION: Interface into the WMS                                          #
#                                                                              #
# Date        By  Proj          Version  Description                           #
# --------    --- -----         -------- --------------------------------      #
# 18/06/2019  GSK SIMBA         1.0      Initial version                       #
################################################################################
# set up environment variables
. $HOME/dcs/.dcs_profile

# identify the file
FILE="$(basename $0)"

# Variables
LOGFILE="$DCS_TMPDIR/$ORACLE_SID/${FILE}.trc"

# Main

if [ -f $LOGFILE ]
then
        mv $LOGFILE $LOGFILE.OLD
fi

echo "$FILE started at `date`  " >>$LOGFILE

sqlplus -s  $ORACLE_USR << ! >> $LOGFILE
SET SERVEROUTPUT ON;
DECLARE
  l_client client.client_id%type  := 'SIMBA';
  l_site site.site_id%type        := 'SWAD001';
  l_owner inventory.owner_id%type := 'SIMBA';
  l_counter       NUMBER(5);
  l_counter_limit NUMBER(5) := 100;
  CURSOR SkuSearch
  IS
    SELECT DISTINCT sku_id
    FROM sku
    WHERE client_id = 'SIMBA'
    AND NOT EXISTS
      (SELECT 1
      FROM sku_relocation
      WHERE client_id = sku.client_id
      AND sku_id      = sku.sku_id
      )
  AND NOT EXISTS
    (SELECT 1
    FROM interface_sku_relocation
    WHERE client_id = sku.client_id
    AND sku_id      = sku.sku_id
    );
  SkuRow SKuSearch%rowtype;
BEGIN
  l_counter := 0;
  OPEN SkuSearch;
  LOOP
    FETCH SkuSearch INTO SkuRow;
    IF SkuSearch%NotFound THEN
      dbms_output.put_line('No records found');
      EXIT;
    END IF;
    INSERT
    INTO interface_sku_relocation
      (
        Key,
        Merge_Action,
        Merge_Status,
        Site_Id,
        Client_Id,
        Sku_Id,
        Owner_Id,
        No_Tags,
        To_Zone,
        From_Zone,
        Algorithm,
        Trigger_Qty
      )
      VALUES
      (
        if_skr_pk_seq.nextval,
        'A',
        'Pending',
        l_site,
        l_client,
        SkuRow.Sku_Id,
        l_owner,
        1,
        'PICK',
        'STORAGE',
        'FIFO by Creation Date',
        0
      );
    dbms_output.put_line('Sku inserted: ' || SkuRow.Sku_Id);
    l_counter   := l_counter + 1;
    IF l_counter = l_counter_limit THEN
      COMMIT;
      l_counter := 0;
    END IF;
  END LOOP;
  CLOSE SkuSearch;
  COMMIT;
END;
/
!
echo "$FILE finished at `date` " >>$LOGFILE
