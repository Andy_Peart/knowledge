#!/bin/bash

. $HOME/dcs/.dcs_profile

cd $DCS_COMMSDIR/outwork/.

# setup the filename 
date=`date +"%m%d"`
time=`date|awk '{print $4}'|sed 's/\://g'`
file=SLGiaaCxt_$date$time.csv

# Run the ISM query output to $file in the outwork directory.
sqlplus -s $ORACLE_USR << ! >> $file
SET SERVEROUTPUT ON
SET FEEDBACK OFF
DECLARE
	l_onhand       NUMBER(7) := 0;
	l_allocated    NUMBER(7) := 0;
	l_shipdock     NUMBER(7) := 0;
	l_not_avail    NUMBER(7) := 0;
	l_available    NUMBER(7) := 0;
	v_code         NUMBER;
	v_errm         VARCHAR2(64);
	l_lot_required VARCHAR2(1);
	l_condition inventory.condition_id%TYPE;
	l_batch inventory.batch_id%TYPE;
	l_client client.client_id%TYPE := 'SLG';
	/* select all inventory into a cursor */
	CURSOR CUR_SKU IS
		SELECT    distinct
			(sku_id)
		  , nvl(batch_id,'~!}{][')     batch_id
		  , nvl(condition_id,'~!}{][') condition_id
		  , qc_status
		  , site_id
		from
			Inventory
		where
			client_id = l_client
			and location_id not in
			(
				select
					location_id
				from
					location
				where
					loc_type in ('Receive Dock'
							   ,'Kitting')
			)
		order by
			sku_id
		;
	
	SKU_ROW CUR_SKU%rowtype;
BEGIN
	DBMS_OUTPUT.ENABLE
					  (
						  buffer_size => NULL
					  );
	/* This removes Buffer size limitation, required to select all the inventory */
	OPEN CUR_SKU;
	/* Loop Through each row in the cursor */
	LOOP
		FETCH CUR_SKU
		INTO
			SKU_ROW
		;
		
		IF CUR_SKU%notfound THEN
			/*EXIT WHEN NO DATA FOUND*/
			EXIT;
		END IF;
		select
			nvl(sum(qty_on_hand),0)
		into
			l_onhand
			/*Add the qty on hand into the variable */
		from
			inventory
		where
			sku_id                         = SKU_ROW.sku_id
			and nvl(batch_id,'~!}{][')     = SKU_ROW.batch_id
			and nvl(condition_id,'~!}{][') = SKU_ROW.condition_id
			and location_id not in
			(
				select
					location_id
				from
					location
				where
					loc_type in ('Receive Dock'
							   ,'Kitting')
			)
		;
		
		select
			nvl(sum(qty_allocated),0)
			/*Add the qty on allocated into the variable */
		into
			l_allocated
		from
			inventory
		where
			sku_id                         = SKU_ROW.sku_id
			and nvl(batch_id,'~!}{][')     = SKU_ROW.batch_id
			and nvl(condition_id,'~!}{][') = SKU_ROW.condition_id
			and location_id not in
			(
				select
					location_id
				from
					location
				where
					loc_type in ('Receive Dock'
							   ,'Kitting')
			)
		;
		
		select
			nvl(sum(qty_on_hand),0)
		into
			l_shipdock
			/* add the qty inventory on the shipdock to the variable */
		from
			inventory
		where
			sku_id                         = SKU_ROW.sku_id
			and nvl(batch_id,'~!}{][')     = SKU_ROW.batch_id
			and nvl(condition_id,'~!}{][') = SKU_ROW.condition_id
			and location_id in
			(
				select
					location_id
				from
					location
				where
					loc_type in ('ShipDock'
							   ,'Auto-ShipDock')
			)
		;
		
		Select
			case
				when sku_row.qc_status is not null
					then 'Y'
					else 'N'
			end
		into
			l_lot_required
			/* populate Y or N for lot required based in if the SKU is a batch managed product */
		from
			dual
		;
		
		if sku_row.condition_id = '~!}{][' then
			l_condition := null;
		else
			l_condition := sku_row.condition_id;
		end if;
		if sku_row.batch_id = '~!}{][' then
			l_batch := null;
		else
			l_batch := sku_row.batch_id;
		end if;
		/* do the calculation for available inventory */
		l_available := l_onhand - l_allocated - l_shipdock;
		/* Output a line into the ISM file */
		dbms_output.put_line (sku_row.sku_id
		||','
		||l_condition
		||','
		||sku_row.site_id
		||','
		||l_onhand
		||','
		||l_batch
		||','
		||l_lot_required
		||','
		||l_available);
	END LOOP;
	close cur_sku;
EXCEPTION
	/* WHEN ERROR OCCURS OUTPUT ERROR CODE AND CYCLE THROUGH CURSORS TO CHECK THEY ARE ALL SHUT*/
WHEN OTHERS THEN
	v_code := SQLCODE;
	v_errm := SUBSTR(SQLERRM, 1, 64);
	DBMS_OUTPUT.PUT_LINE (v_code
	|| ' '
	|| v_errm);
	IF CUR_SKU%isopen then
		close CUR_SKU;
	END IF;
END;
/
exit
!

# remove empty lines from the output file
sed -i '/^$/d' $file

# copy to outarchive
cp $file ../outarchive/.

# move the file to the outtray for pickup by Messageway
mv $file ../outtray/.
