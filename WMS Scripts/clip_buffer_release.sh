#!/bin/bash

. $HOME/dcs/.dcs_profile


cd $HOME/AndyP/Release

##################################################################################
echo ""
echo "Uncompress archived files to be used for Release"
echo ""
##################################################################################

tar -xzvf release.tar.gz

##################################################################################
echo ""
echo "Create RDT buffer Table and packages"
echo ""
##################################################################################

sqlplus -s clippergroup/clippergroup@$ORACLE_TNSNAME <<EOF
SET SERVEROUTPUT ON;
@create_rdt_buffer.sql
exit;
EOF

sqlplus -s clippergroup/clippergroup@$ORACLE_TNSNAME <<EOF
@rdt_buf_p.sql
exit;
EOF

sqlplus -s clippergroup/clippergroup@$ORACLE_TNSNAME <<EOF
@rdt_buf_pb.sql
exit;
EOF

##################################################################################
echo ""
echo "RDT Table and Packages Created"
echo ""
##################################################################################

##################################################################################
echo ""
echo "Granting Access to new DB object"
echo ""
##################################################################################

grant_clip_db.sh

##################################################################################
echo ""
echo "Grants Completed"
echo ""
##################################################################################

rm *.sql
rm *.sh