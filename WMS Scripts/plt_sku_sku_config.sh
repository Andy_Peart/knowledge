#!/bin/bash
################################################################################
#                                                                              #
#                               Clipper                                        #
#                                                                              #
# SCRIPT NAME: simbaif_sku_sku_config.sh - ModernLogic                         #
#                                                                              #
# DESCRIPTION: Interface into the WMS                                          #
#                                                                              #
# Date        By  Proj          Version  Description                           #
# --------    --- -----         -------- --------------------------------      #
# 18/06/2019  GSK SIMBA         1.0      Initial version                       #
################################################################################
# set up environment variables
. $HOME/dcs/.dcs_profile

# identify the file
FILE="$(basename $0)"

# Variables
LOGFILE="$DCS_TMPDIR/$ORACLE_SID/${FILE}.trc"

# Main

if [ -f $LOGFILE ]
then
        mv $LOGFILE $LOGFILE.OLD
fi

echo "$FILE started at `date`  " >>$LOGFILE

sqlplus -s  $ORACLE_USR << ! >> $LOGFILE
DECLARE
  l_config sku_sku_config.config_id%type;
  l_counter       NUMBER(5) := 0;
  l_counter_limit NUMBER(5) := 100;
  l_client        client.client_id%type := 'PLT';
  CURSOR SkuSearch
  IS
    SELECT *
    FROM sku
    WHERE client_id = l_client
    AND NOT EXISTS
      (SELECT 1
      FROM sku_sku_config
      WHERE sku_id  = sku.sku_id
      AND client_id = sku.client_id
      )
  AND NOT EXISTS
    (SELECT 1
    FROM interface_sku_sku_config
    WHERE sku_id  = sku.sku_id
    AND client_id = sku.client_id
    ) ;
  SkuRow SKuSearch%rowtype;
BEGIN
  OPEN SkuSearch;
  LOOP
    FETCH SkuSearch INTO SkuRow;
    IF SkuSearch%notfound THEN
      EXIT;
    END IF;
    SELECT DISTINCT config_id
    INTO l_config
    FROM sku_config
    WHERE client_id = SkuRow.Client_Id
    AND rownum      = 1;
    INSERT
    INTO interface_sku_sku_config
      (
        KEY,
        merge_action,
        merge_status,
        client_id,
        sku_id,
        config_id
      )
      VALUES
      (
        if_ssc_pk_seq.nextval,
        'A',
        'Pending',
        SkuRow.client_id,
        SkuRow.sku_id,
        l_config
      );
    l_counter   := l_counter + 1;
    IF l_counter = l_counter_limit THEN
      COMMIT;
      l_counter := 0;
    END IF;
  END LOOP;
  COMMIT;
END;
/
!
echo "$FILE finished at `date` " >>$LOGFILE
