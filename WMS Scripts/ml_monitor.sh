#!/bin/sh

##  Program name
PROG=ml_monitor.sh

##  Default parameters
selfHeal=off;
testMode=off;
keepSending=off;
blockThresh=300;
CPUthreshold=80;
printLimit=20;
runTaskLimit=20;
moveTaskLimit=30;
freeSpaceLimit=20;
activeDBSessionLimit=15;
recipients="oliver@modernlogic.com";
hostname=`hostname`;
smtpserver="";

##  show usage
usage()
{
        (
        echo ""
        echo "Usage: $PROG [options]"
        echo ""
        echo "[-h]      enable self healing"
        echo "[-t]      Test mode - send email regardless of threshold"
        echo "[-s]      Keep sending mails until resolution"
        echo "[-b int]  Block lock threshold (default 300 seconds)"
        echo "[-c int]  CPU Usage Threshold (default 80%)"
        echo "[-p int]  Print job limit (default 20)"
        echo "[-r int]  Pending run task limit (default 20)"
        echo "[-m int]  Complete move task limit default (30)"
        echo "[-f int]  Tablespace freespace % free limit (default 20)"
        echo "[-a int]  Active database session limit (default 15)"
        echo "[-e str]  Email recipients for alerts"
        echo "[-d str]  SMTP server"
        echo ""
        echo "Self Heal Functions (if enabled)"
        echo "--------------------------------"
        echo "Print jobs        Clear down a print queue if we breach the limit"
        echo "Blocking locks    Kill a blocking lock if the top blocker has been idle for longer that the specified time limit"
        echo ""
       ) | more
}

cleanUp() {
        ## tidy up temporary files
        rm OPlocks$$.csv;
        rm printers.$$;
        rm OP.mttasks.$$;
        rm OP.rntasks.$$;
        rm OP.pctfree.$$;
        rm OP.tablespaces.$$;
        rm OP.sessions$$.csv;
}

checkSemaphore() {
        function=$1
        echo `grep $function ml_monitor_semaphores|awk '{print $2}'`
}

updateSemaphore() {
        function=$1
        status=$2
        echo $function" to status "$status
        sed -i "/$function/c\\$function $status" ml_monitor_semaphores
}

trim() {
        ## trims spaces on either side from the argument passed in
        rtrnval=`echo $1|awk '{print $1}'`
        echo $rtrnval
}

sendmail() {
        # this function will determine weather to send a mail and which one to send
        # it will also update the semaphore to say we have sent it or to reset it
        goodsubject=$1
        goodmessage=$2
        badsubject=$3
        badmessage=$4
        function=$5
        mode=$6
	file=$7
        status=`checkSemaphore $function`;

        echo "send mail for $function status $status mode $mode"

        if [ "$mode" == "error" ]
        then
                if [ "$keepSending" == "on" ] || [ "$status" == "_NA_" ]
                then
                        echo "sending mail - error"
			if [ ! -f "$file" ]
			then
                        	echo "$badmessage" | mailx -v -s "$hostname - $badsubject" -S smtp=$smtpserver $recipients;
			else
				echo "$badmessage" | mailx -v -s "$hostname - $badsubject" -S smtp=$smtpserver -a $file $recipients;
			fi
                        updateSemaphore $function "sent"
                fi
        fi

        if [ "$mode" == "good" ]
        then
                if [ "$keepSending" == "on" ] || [ "$status" == "sent" ]
                then
                        echo "sending mail - good"
                        echo "$goodmessage" | mailx -s "$hostname - $goodsubject" -S smtp=$smtpserver $recipients;
                        updateSemaphore $function "_NA_"
                fi
        fi
}

## Monitoring ##
checkBlockingLocks()
{
goodsubject="checkBlockingLocks - blocking locks resolved";
goodmessage="Blocking locks resolved";
badsubject="checkBlockingLocks - blocking locks exist";
badmessage="Blocking locks exist in the system";
function=checkBlockingLocks
mode="good";

sqlplus -s $DCS_SYSDBA << EOF >| OPlocks$$.csv
set feedback off
set heading off
set linesize 32767
SELECT '"'||se.sid||'","'||serial#||'","'||sql_text||'","'||se.program||'","'||vsw.event||'","'||vsw.seconds_in_wait||'"'
   FROM v\$lock lk,
        v\$session se,
        v\$sql vsql,
        v\$session_wait vsw
  WHERE (lk.TYPE = 'TX')
    AND (lk.SID = se.SID)
    and block = 1
    and lockwait is null
    and nvl(se.sql_id, se.prev_sql_id) = vsql.sql_id
    and vsw.sid = se.sid;
exit;
EOF

count=`wc -l OPlocks$$.csv|awk '{print $1}'`

if [ "$count" -gt "0" -o "$testMode" = "on" ]
then
        mode="error";
fi

sendmail "$goodsubject" "$goodmessage" "$badsubject" "$badmessage" $function $mode OPlocks$$.csv

}

checkCpu() {

goodsubject="checkCpu - cpu usage resolved";
goodmessage="cpu usage resolved";
badsubject="checkCpu - cpu above $CPUthreshold";
badmessage="CPU running above $CPUthreshold";
function=checkCpu
mode="good";

cpuIDLE=`sar|tail -1|awk '{print $8}'|awk -F . '{print $1}'`
cpuUsage=`echo $(( 100-$cpuIDLE ))`;

if [ "$cpuUsage" -gt "$CPUthreshold" -o "$testMode" = "on" ]
then
       mode="error";
fi

sendmail "$goodsubject" "$goodmessage" "$badsubject" "$badmessage" $function $mode

}

printQueues() {
badsubject="printQueues - print queues have jobs over $printLimit";
badmessage="print queues have jobs over $printLimit.  This may cause issues with overall printing if not resolved";
goodsubject="printQueues - resolved";
goodmessage="printQueues - resolved";
function=printQueues
mode="good";

if [ "$testMode" = "on" ]
then
        mode="error";
fi

lpstat -t|grep device|awk '{print $3}'|awk -F: '{print $1}' >| printers.$$
while read line
do
        printCount=`lpstat -t|grep $line|wc -l`;
        if [ "$printCount" -gt "$printLimit" -o "$testMode" = "on" ]
        then
                mode="error";
                cancelPrintJobs $line;
                goodmessage=$goodmessage" printer : "$line
        fi
done < printers.$$

sendmail "$goodsubject" "$goodmessage" "$badsubject" "$badmessage" $function $mode

}

completeMoveTasks() {
badsubject="completeMoveTasks - Complete move tasks above $moveTaskLimit";
badmessage="Complete tasks above $moveTaskLimit.   This may be due to a stalled move task daemon";
goodsubject="completeMoveTasks - resolved";
goodmessage="completeMoveTasks - resolved";
function=completeMoveTasks
mode="good";

sqlplus -s $ORACLE_USR << EOF >| OP.mttasks.$$
set feedback off
set heading off
SELECT count(*) from move_task where status = 'Complete' ;
exit;
EOF

count=`wc -l OP.mttasks.$$|awk '{print $1}'`
if [ "$count" -gt "$moveTaskLimit" -o "$testMode" = "on" ]
then
        mode="error";
fi

sendmail "$goodsubject" "$goodmessage" "$badsubject" "$badmessage" $function $mode

}

completeRunTasks() {
badsubject="completeRunTasks - Pending above $runTaskLimit";
badmessage="Pending run tasks above $runTaskLimit.   This may be due to a stalled run task daemon";
goodsubject="completeRunTasks - resolved";
goodmessage="completeRunTasks - resolved";
function=completeRunTasks
mode="good";

sqlplus -s $ORACLE_USR << EOF >| OP.rntasks.$$
set feedback off
set heading off
SELECT count(*) from run_task where status = 'Pending';
exit;
EOF

count=`wc -l OP.rntasks.$$|awk '{print $1}'`
if [ "$count" -gt "$runTaskLimit" -o "$testMode" = "on" ]
then
        mode="error";
fi

sendmail "$goodsubject" "$goodmessage" "$badsubject" "$badmessage" $function $mode

}

databaseRunning() {

badsubject="databaseRunning - Database not running";
badmessage="Database is not running";
goodsubject="databaseRunning - resolved";
goodmessage="databaseRunning - resolved";

function=databaseRunning
mode="good";

count=`databaseup -v|grep "The database is not running"|wc -l`;
if [ "$count" -gt "0" -o "$testMode" = "on" ]
then
        mode="error";
fi

sendmail "$goodsubject" "$goodmessage" "$badsubject" "$badmessage" $function $mode

}

invalidPackages() {

badsubject="invalidPackages - Invalid packages exist";
badmessage="Invalid packages exist in the database";
goodsubject="invalidPackages - resolved";
goodmessage="invalidPackages - resolved";

function=invalidPackages
mode="good";

count=`pinvalid |grep -i lib|wc -l`;
if [ "$count" -gt "0" -o "$testMode" = "on" ]
then
        mode="error";
fi

sendmail "$goodsubject" "$goodmessage" "$badsubject" "$badmessage" $function $mode

}

processUsage() {

badsubject="processUsage - Process limit above 80% usage";
badmessage="Processes are above 80% used";
goodsubject="processUsage - resolved";
goodmessage="processUsage - resolved";
function=processUsage
mode="good";

maxusage=`ulimit -u`;
count=`pscmd -f|wc -l`;
usage=`echo $(( $count/$maxusage*100 ))`;

if [ "$usage" -gt "80" -o "$testMode" = "on" ]
then
        mode="error";
fi

sendmail "$goodsubject" "$goodmessage" "$badsubject" "$badmessage" $function $mode

}

tableSpaces() {

goodsubject="Tablespace limits resolved";
goodmessage="Tablespace limits resolved";
badsubject="tableSpaces - Table space warning";
badmessage="Following tablespaces have an issue - ";
function=tableSpaces
mode="good";

if [ "$testMode" = "on" ]
then
        mode="error";
fi

sqlplus -s $ORACLE_USR << EOF >| OP.tablespaces.$$
set feedback off
set heading off
set pagesize 0
select tablespace_name from user_Tablespaces;
exit;
EOF

while read line
do
        sqlplus -s $ORACLE_USR << EOF >| OP.pctfree.$$
        set feedback off
        set heading off
        set pagesize 0
        select
           round(100 * (fs.freespace / df.totalspace)) "Pct. Free"
        from
           (select
              tablespace_name,
              round(sum(bytes) / 1048576) TotalSpace
           from
              dba_data_files
           group by
              tablespace_name
           ) df,
           (select
              tablespace_name,
              round(sum(bytes) / 1048576) FreeSpace
           from
              dba_free_space
           group by
              tablespace_name
           ) fs
        where
           df.tablespace_name = fs.tablespace_name
           and fs.tablespace_name = '$line';
EOF

        pctfree=`tail -1 OP.pctfree.$$|awk '{print $1}'`;
        echo "pctfree = "$pctfree" tablespace $line"
        if [ "$pctfree" = "" ]
        then
                mode="error";
                $badmessage=$badmessage" $line has 0% free"
        elif [ "$pctfree" -lt "$freeSpaceLimit" -o "$testMode" = "on" ]
        then
                mode="error";
                $badmessage=$badmessage" $line has $pctfree % free"
        fi
done < OP.tablespaces.$$

sendmail "$goodsubject" "$goodmessage" "$badsubject" "$badmessage" $function $mode

}

activeSessions() {

goodsubject="activeSessions - resolved";
goodmessage="activeSessions - resolved";
badsubject="ActiveSessions - Active db sessions above $activeDBSessionLimit ";
badmessage="Active database sessions above set threshold";

function=activeSessions
mode="good";

sqlplus -s $ORACLE_USR << EOF >| OP.sessions$$.csv
set feedback off
set heading off
set pagesize 0
set linesize 32767
SELECT distinct '"'||program||'","'||se.sid||'","'||serial#||'","'||sql_text||'","'||vsw.event||'","'||vsw.seconds_in_wait||'"' 
   FROM v\$session se,
        v\$sql vsql,
        v\$session_wait vsw
  WHERE se.status = 'ACTIVE'
    and nvl(se.sql_id, se.prev_sql_id) = vsql.sql_id
    and vsw.sid = se.sid
    and user = (select username
                  from dual);
exit;
EOF

count=`wc -l OP.sessions$$.csv|awk '{print $1}'`

if [ "$count" -gt "$activeDBSessionLimit" -o "$testMode" = "on" ]
then
        mode="error";
fi

sendmail "$goodsubject" "$goodmessage" "$badsubject" "$badmessage" $function $mode OP.sessions$$.csv

}



########################
# Get command line options
########################

while getopts htsb:c:p:r:m:f:a:e:d:? 2> /dev/null ARG
do
        case $ARG in
                h)      selfHeal="on";;

                t)      testMode="on";;

                s)      keepSending="on";;

                b)      blockThresh=$OPTARG;;

                c)      CPUthreshold=$OPTARG;;

                p)      printLimit=$OPTARG;;

                r)      runTaskLimit=$OPTARG;;

                m)      moveTaskLimit=$OPTARG;;

                f)      freeSpaceLimit=$OPTARG;;

                a)      activeDBSessionLimit=$OPTARG;;

                e)      recipients=$OPTARG;;

                d)      smtpserver=$OPTARG;;

                ?)      usage
                        exit 1;;
        esac
done

#echo $selfHeal
#echo $blockThresh
#echo $CPUthreshold
#echo $printLimit
#echo $runTaskLimit
#echo $moveTaskLimit
#echo $freeSpaceLimit
#echo $activeDBSessionLimit
#echo $recipients

########################
# Main program
########################

while [ "1" -eq "1" ]
do

        checkBlockingLocks;
        checkCpu;
        #printQueues;
        completeMoveTasks;
        completeRunTasks;
        databaseRunning;
        invalidPackages;
        processUsage;
        #tableSpaces;
        activeSessions;

        ## clean up files
        cleanUp;

        sleep 300

done

## Self Healing ##
cancelPrintJobs() {
        ##
        if [ "$selfHeal" == "on" ]
        then
                echo "cancel print jobs for printer "$line;
        fi

}

killBlocker() {
        ##
        if [ "$selfHeal" == "on" ]
        then
                echo "kill blocker";
        fi
}
## ##

