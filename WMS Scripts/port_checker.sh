#!/bin/sh

##################################################################################################################################################################################
# Script name: Workstation_install.sh
# Creator: Andy Peart
# Date: 18/10/2019
# Version: 1.0
# Purpose: Automatically copy a source client into a target client
# ***************************************CHANGES**********************************************************************************************************************************
#
##################################################################################################################################################################################

. $HOME/dcs/.dcs_profile

PROG=$(basename $0)
DATETIMEFILE=`date +%Y%m%d%H%M%S`
DATEFILE=`date +%Y%m%d`
LOGFILE=$DCS_PACKAGELOGDIR/$PROG"_"$DATEFILE.log

function usage
{
        echo ""
        echo "Usage: $PROG [options]"
        echo ""
        echo "[-b]       REQUIRED - Start Port"
        echo "[-e]       REQUIRED - End Port"
		echo ""
        echo "This Script will scan all ports in the range and Identify any in use"
        echo ""
}

while getopts b:e:? 2> /dev/null ARG 
do 
 case ${ARG} 
 in 
  b) STARTPORT=${OPTARG};;
  e) MAXPORT=${OPTARG};;
  ?) usage
     exit 1;;
 esac 
done 

echo ""
echo "You have entered the following data:"
echo ""
echo "Starting Port:           $STARTPORT"
echo "Last Port:               $MAXPORT"
echo ""

read -p "Do you wish to continue (y/n)?" choice
case "$choice" in 
  y|Y ) echo "yes";;
  n|N ) echo "no" ; exit 0;;
  * ) echo "invalid"; exit 0;;
esac

i=$STARTPORT; while [ $i -le $MAXPORT ]; do
  echo $i
  netstat -anp | grep $i
  i=$(($i + 1))
done

