#!/bin/bash

####################################################################################################################################
# Script name: rollout.sh
# Creator: Gurmit Karyal - ModernLogic
# Date: 03/07/2019
# Purpose: Rollout
# ***************************************CHANGES************************************************************************************
# v0.1 - 03/07/2019 - Gurmit Karyal - Initial version
####################################################################################################################################

# set .dcs_profile
./$HOME/dcs/.dcs_profile 2> /dev/null

# client and versioning
CLI="slg"
VERSION="0001"

# setup local directories
RELEASEDIR="$HOME/release"
CLIENTDIR="$RELEASEDIR/$CLI"
ROLLOUTDIR="$CLIENTDIR/rollout/$VERSION"
DUMPDIR="$ROLLOUTDIR/dumpfiles"

DATE="$(date +%Y%m%d_%H:%M:%S)"
RESTOREDIR="$ROLLOUTDIR/restore_$DATE"

FILENAME="$(basename $0)"

usage() {
  echo ""
  echo "Usage $FILENAME:"
  echo "[-C]     Client id"
  echo "[-c]     Client visibility group"
  echo "[-S]     Site id"
  echo "[-s]     Site visibility group"
  echo "[-d]     dump data"
  echo "[-l]     load data"
  echo ""
  exit 1
}

stripheaderdata() {
  FILE="$1"
  
  echo " $FUNCNAME $FILE" > /dev/null 

  # strip the headers
  sed -i '/COUNT/d' $FILE
  sed -i '/----/d' $FILE
  sed -i -i '/^$/d' $FILE

  # check the headers were stripped ok
  if [[ "$?" -ne 0 ]]
  then
    echo "  Something went wrong stripping headers in $FILE"
    exit 1
  else
    echo "  Headers stripped" > /dev/null
  fi
}

checkclientdata() {

  local CLIENTID="$1"

  echo "$FUNCNAME $CLIENTID" > /dev/null

  # need to check if the client data exists
  CLIENTLOG="clientcheck.$$"

  sqlplus -s $ORACLE_USR << ! > $CLIENTLOG
  select count(*) ||'|'
  from client
  where client_id = '$CLIENTID'
  and rownum = 1;
  exit;
!

  # strip the headers
  stripheaderdata $CLIENTLOG

  # find the count of client records for the site id
  COUNT="$(cat $CLIENTLOG | cut -c 1)"

  # tidy logfiles
  rm $CLIENTLOG

  # if the client exists or not we return an appropriate status
  if [[ "$COUNT" -eq 0 ]]
  then
    return 0
  else 
    return 1
  fi

}


checksitedata() {

  local SITEID="$1"

  echo "$FUNCNAME $SITEID" > /dev/null

  # need to check if the site data exists
  SITELOG="sitecheck.$$"

  sqlplus -s $ORACLE_USR << ! > $SITELOG
  select count(*) ||'|'
  from site
  where site_id = '$SITEID'
  and rownum = 1;
  exit;
!

  # strip the headers
  stripheaderdata $SITELOG

  # find the count of site records for the site id
  COUNT="$(cat $SITELOG | cut -c 1)"

  # tidy logfiles
  rm $SITELOG

  # if the client exists or not we return an appropriate status
  if [[ "$COUNT" -eq 0 ]]
  then
    return 0
  else 
    return 1
  fi

}

# Main Program

# parse the options
while getopts C:c:S:s:dl? OPTION
do
  case $OPTION in
    C) CLIENT="$OPTARG" ;;
    c) CLIENTVISGRP="$OPTARG" ;;
    S) SITE="$OPTARG" ;;
    s) SITEVISGRP="$OPTARG" ;;
    d) DUMP="true" ;;
    l) LOAD="true" ;;
    ?) usage ;;
  esac
done

# check there is at least one argument
if [[ "$#" -lt 0 ]]
then
  usage
fi

# check that all options have an argument
if [[ "$CLIENT" = "" ]] || [[ "$CLIENTVISGRP" = "" ]] || [[ "$SITE" = "" ]] || [[ "$SITEVISGRP" = "" ]]
then
  echo "Please supply all required arguments"
  usage
fi

if [[ "$DUMP" = "" ]] && [[ "$LOAD" = "" ]]
then
  echo "Please supply a save or load option"
  usage
fi

# check the application first and foremost
sia -u
if [[ "$?" = "0" ]]
then
  # exit from the application
  echo "Application is running!" 1>&2
  echo ""
  
  read -p "Continue (y/n) " CHOICE

  # exit if the user is not happy
  case "$CHOICE" in
    y|Y ) echo "yes";;
    n|N ) echo "no" ; exit 0;;
    * ) echo "invalid"; exit 0;;
  esac
  
fi


# echo arguments
echo ""
echo " Client ID:               $CLIENT"
echo " Client Visibility Group: $CLIENTVISGRP"
echo " Site:                    $SITE"
echo " Site Visibility Group:   $SITEVISGRP"
echo " dump data:               $DUMP"
echo " load data:               $LOAD"


# we seem to be ok, so we can now process the arguments
# if the user is trying to dump the data
if [[ "$DUMP" = "true" ]]
then

  checkclientdata $CLIENT

  if [[ "$?" = "0" ]]
  then
    echo "Client does not exist!" 1>&2
    exit 1
  fi

  checksitedata $SITE

  if [[ "$?" = "0" ]]
  then
    echo "Site does not exist!" 1>&2
    exit 1
  fi

  echo ""
  echo "Saving config for client: $CLIENT and site: $SITE"
  echo ""
  echo " Please note the following data will not be copied"
  echo " users"
  echo " workstations"
  echo " scheduler programs"
  echo " advanced print mapping"
  echo " reports"
  echo ""
  read -p "Do you wish to continue (y/n)? " CHOICE

  # exit if the user is not happy
  case "$CHOICE" in 
    y|Y ) echo "yes";;
    n|N ) echo "no" ; exit 0;;
    * ) echo "invalid"; exit 0;;
  esac

  # warning
  echo ""
  echo " Before continuing please check you have updated the following:"
  echo " 1) User Groups have $CLIENT in the notes"
  echo ""
  read -p "Continue (y/n) " CHOICE

  # exit if the user is not happy
  case "$CHOICE" in
    y|Y ) echo "yes";;
    n|N ) echo "no" ; exit 0;;
    * ) echo "invalid"; exit 0;;
  esac

  cd $DUMPDIR

  DUMPLOG="dumplog.$$"

  touch $DUMPLOG

  # client data
  echo "dumping client data"
  dbdump -o client_group -w " client_group like '$CLIENT%' " >> $DUMPLOG
  dbdump -o client -w " client_id = '$CLIENT' " >> $DUMPLOG
  dbdump -o client_group_clients -w " client_id = '$CLIENT' " >> $DUMPLOG

  # owner data
  dbdump -o owner -w " client_id = '$CLIENT' " >> $DUMPLOG

  # site data
  dbdump -o site -w " site_id = '$SITE' " >> $DUMPLOG
  dbdump -o site_group -w " site_group like '$SITEVISGRP%' " >> $DUMPLOG 
  dbdump -o site_group_sites -w " site_id = '$SITE' " >> $DUMPLOG

  # suspense integrity
  dbdump -o suspense_integrity -w " site_id = '$SITE' " >> $DUMPLOG

  # user group
  dbdump -o user_group -w " notes like '%$CLIENT%' " >> $DUMPLOG

  # user_group_user_group
  dbdump -o user_group_user_group -w " group_id in (select group_id from user_group where notes like '%$CLIENT%') " >> $DUMPLOG

  # function accesses
  dbdump -o function_access -w " site_id = '$SITE' " >> $DUMPLOG 
  dbdump -o group_function -w " group_id in (select group_id from user_group where notes like '%$CLIENT%') " >> $DUMPLOG
  dbdump -o workstation_group_function >> $DUMPLOG

  # screen data
  rdtscreendatautil -s -i $SITE >> $DUMPLOG

  # mergerules data
  mergerulesdatautil -s -i $SITE -z >> $DUMPLOG

  # clustering data
  clusteringdatautil -s -i $SITE >> $DUMPLOG

  # putaway data
  dbdump -o putaway_group -w " site_id = '$SITE' " >> $DUMPLOG
  dbdump -o group_putaway -w " site_id = '$SITE' " >> $DUMPLOG
  dbdump -o global_putaway -w " site_id = '$SITE' " >> $DUMPLOG

  # allocation data
  dbdump -o allocation_group -w " site_id = '$SITE' " >> $DUMPLOG
  dbdump -o group_allocation -w " site_id = '$SITE' " >> $DUMPLOG
  dbdump -o global_allocation -w " site_id = '$SITE' " >> $DUMPLOG

  # daemon data
  dbdump -o sia_process -w " site_id = '$SITE' " >> $DUMPLOG

  # itl extract profile
  dbdump -o itl_extract_conf -w " client_id = '$CLIENT' " >> $DUMPLOG

  # adjustment reason codes
  dbdump -o adjust_reason -w " site_id = '$SITE' " >> $DUMPLOG

  # inventory lock data
  dbdump -o inventory_lock_code -w " client_id = '$CLIENT' " >> $DUMPLOG

  # condition data
  dbdump -o condition_code -w " client_id = '$CLIENT' " >> $DUMPLOG

  # order type data
  dbdump -o order_type -w " client_id = '$CLIENT' " >> $DUMPLOG

  # pallet config data
  dbdump -o pallet_config -w " client_id = '$CLIENT' " >> $DUMPLOG

  # sku config data
  dbdump -o sku_config_tracking_level -w " client_id = '$CLIENT' " >> $DUMPLOG
  dbdump -o sku_config -w " client_id = '$CLIENT' " >> $DUMPLOG

  # carrier data
  dbdump -o carriers -w " client_id = '$CLIENT' " >> $DUMPLOG

  # location zone
  dbdump -o location_zone -w " site_id = '$SITE' " >> $DUMPLOG

  # work zone
  dbdump -o work_zone -w " site_id = '$SITE' " >> $DUMPLOG

  # location
  dbdump -o location -w " site_id = '$SITE' " >> $DUMPLOG

  # user print config
  dbdump -o print_config -w " site_group = '$SITEVISGRP' " >> $DUMPLOG

elif [[ "$LOAD" = "true" ]]
then

  # warning
  echo ""
  echo "Please confirm if you wish to load data:"
  echo ""
  read -p "Continue (y/n) " CHOICE 

  # exit if the user is not happy
  case "$CHOICE" in
    y|Y ) echo "yes";;
    n|N ) echo "no" ; exit 0;;
    * ) echo "invalid"; exit 0;;
  esac 

  # pre checks

  checkclientdata $CLIENT

  # if the function returns 1 then the client already exists
  if [[ "$?" = "1" ]]
  then
    echo "" 1>&2
    echo "Client exists!" 1>&2
    echo "" 1>&2
  
    read -p "Continue (y/n) " CHOICE

    # exit if the user is not happy
    case "$CHOICE" in
      y|Y ) echo "yes";;
      n|N ) echo "no" ; exit 0;;
      * ) echo "invalid"; exit 0;;
    esac

  fi

  checksitedata $SITE

  # if the function returns 1 then the site already exists
  if [[ "$?" = "1" ]]
  then
    echo "" 1>&2
    echo "Site exists!" 1>&2
    echo "" 1>&2
    read -p "Continue (y/n) " CHOICE

    # exit if the user is not happy
    case "$CHOICE" in
      y|Y ) echo "yes";;
      n|N ) echo "no" ; exit 0;;
      * ) echo "invalid"; exit 0;;
    esac

  fi   
 
  
  # saving current data to restore later
  
  echo "Saving current restore point $RESTOREDIR"
  echo "All data will be saved"

  mkdir $RESTOREDIR
  cd $RESTOREDIR
  DUMPLOG="dumplog.$$"
  touch $DUMPLOG

  # client data
  echo "dumping client data"
  dbdump -o client_group >> $DUMPLOG
  dbdump -o client  >> $DUMPLOG
  dbdump -o client_group_clients >> $DUMPLOG

  # owner data
  dbdump -o owner >> $DUMPLOG

  # site data
  dbdump -o site >> $DUMPLOG
  dbdump -o site_group >> $DUMPLOG
  dbdump -o site_group_sites >> $DUMPLOG

  # suspense integrity
  dbdump -o suspense_integrity  >> $DUMPLOG

  # user group
  dbdump -o user_group >> $DUMPLOG

  # user_group_user_group
  dbdump -o user_group_user_group >> $DUMPLOG

  # function accesses
  dbdump -o function_access >> $DUMPLOG
  dbdump -o group_function >> $DUMPLOG
  dbdump -o workstation_group_function >> $DUMPLOG

  # screen data
  rdtscreendatautil -s -a >> $DUMPLOG

  # mergerules data
  mergerulesdatautil -s -a -z >> $DUMPLOG

  # clustering data
  clusteringdatautil -s -a >> $DUMPLOG

  # putaway data
  dbdump -o putaway_group >> $DUMPLOG
  dbdump -o group_putaway >> $DUMPLOG
  dbdump -o global_putaway >> $DUMPLOG

  # allocation data
  dbdump -o allocation_group >> $DUMPLOG
  dbdump -o group_allocation >> $DUMPLOG
  dbdump -o global_allocation >> $DUMPLOG

  # daemon data
  dbdump -o sia_process >> $DUMPLOG

  # itl extract profile
  dbdump -o itl_extract_conf >> $DUMPLOG

  # adjustment reason codes
  dbdump -o adjust_reason >> $DUMPLOG

  # inventory lock data
  dbdump -o inventory_lock_code >> $DUMPLOG

  # condition data
  dbdump -o condition_code >> $DUMPLOG

  # order type data
  dbdump -o order_type >> $DUMPLOG

  # pallet config data
  dbdump -o pallet_config >> $DUMPLOG

  # sku config data
  dbdump -o sku_config_tracking_level >> $DUMPLOG
  dbdump -o sku_config >> $DUMPLOG

  # carrier data
  dbdump -o carriers >> $DUMPLOG

  # location zone
  dbdump -o location_zone >> $DUMPLOG

  # work zone
  dbdump -o work_zone >> $DUMPLOG

  # location
  dbdump -o location >> $DUMPLOG

  # user print config
  dbdump -o print_config >> $DUMPLOG

  echo "Current restore point created: $RESTOREDIR"


  echo "loading data"

  cd $DUMPDIR
  LOADLOG="loadlog.$$"
  touch $LOADLOG

  # client data
  echo "client data"
  dbload -i client_group.dat >> $LOADLOG
  dbload -i client.dat >> $LOADLOG
  dbload -i client_group_clients.dat >> $LOADLOG

  # owner data
  dbload -i owner.dat >> $LOADLOG

  # site data
  dbload -i site.dat >> $LOADLOG
  dbload -i site_group.dat >> $LOADLOG
  dbload -i site_group_sites.dat >> $LOADLOG

  # suspense integrity
  dbload -i suspense_integrity.dat >> $LOADLOG

  # user group
  dbload -i user_group.dat >> $LOADLOG

  # user_group_user_group
  dbload -i user_group_user_group.dat >> $LOADLOG

  # function accesses
  dbload -i function_access.dat >> $LOADLOG
  dbload -i group_function.dat >> $LOADLOG
  dbload -i workstation_group_function.dat >> $LOADLOG

  # screen data
  rdtscreendatautil -l -i $SITE >> $LOADLOG

  # mergerules data
  mergerulesdatautil -l -i $SITE -z >> $LOADLOG

  # clustering data
  clusteringdatautil -l -i $SITE >> $LOADLOG

  # putaway data
  dbload -i putaway_group.dat >> $LOADLOG
  dbload -i group_putaway.dat >> $LOADLOG
  dbload -i global_putaway.dat >> $LOADLOG

  # allocation data
  dbload -i allocation_group.dat >> $LOADLOG
  dbload -i group_allocation.dat >> $LOADLOG
  dbload -i global_allocation.dat >> $LOADLOG

  # daemon data
  dbload -i sia_process.dat >> $LOADLOG

  # itl extract profile
  dbload -i itl_extract_conf.dat >> $LOADLOG

  # adjustment reason codes
  dbload -i adjust_reason.dat >> $LOADLOG

  # inventory lock data
  dbload -i inventory_lock_code.dat >> $LOADLOG

  # condition data
  dbload -i condition_code.dat >> $LOADLOG

  # order type data
  dbload -i order_type.dat >> $LOADLOG

  # pallet config data
  dbload -i pallet_config.dat >> $LOADLOG

  # sku config data
  dbload -i sku_config_tracking_level.dat >> $LOADLOG
  dbload -i sku_config.dat >> $LOADLOG

  # carrier data
  dbload -i carriers.dat >> $LOADLOG

  # location zone
  dbload -i location_zone.dat >> $LOADLOG

  # work zone
  dbload -i work_zone.dat >> $LOADLOG

  # location
  dbload -i location.dat >> $LOADLOG

  # user print config
  dbload -i print_config.dat >> $LOADLOG


fi

exit 0
