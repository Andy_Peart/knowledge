#!/bin/sh

####################################################################################################################################
# Script name: ml_zeroship.sh
# Creator: Gurmit Karyal - ModernLogic
# Date: 27/02/2019
# Purpose: Script delete order lines where user_def_chk_4 = 'Y' and write ITL
# ***************************************CHANGES************************************************************************************
# 1.0 - 27/02/2019 - Gurmit Karyal - Initial version
####################################################################################################################################

sqlplus -s $ORACLE_USR << !
DECLARE
  l_LineCount NUMBER(5);
  Result   integer;
  
  CURSOR OrderSearch
  IS
    SELECT Order_Id,
      Client_Id
    FROM Order_Header OH
    WHERE Status = 'Released'
    AND EXISTS
      (SELECT 1
      FROM Order_Line OL
      WHERE OL.Order_Id     = OH.Order_Id
      AND OL.Client_Id      = OH.Client_Id
      AND OL.User_Def_Chk_4 = 'Y'
      )
  AND NOT EXISTS
    (SELECT 1
    FROM Interface_Order_Line OL
    WHERE OL.Order_Id = OH.Order_Id
    AND OL.Client_Id  = OH.Client_Id
    );
    
  CURSOR LineSearch ( p_Client_Id Order_Header.Client_Id%type, p_Order_Id Order_Header.Order_Id%type )
  IS
    SELECT OH.From_Site_ID,
      OH.Ship_Dock,
      OH.Owner_ID,
      OH.Work_Group,
      OH.Consignment,
      OL.Line_Id,
      OL.Qty_Ordered,
      OL.SKU_ID,
      OL.Batch_ID,
      OL.Condition_ID,
      OL.Origin_ID,
      OL.Config_ID,
      OL.Lock_Code,
      OH.Customer_ID,
      OL.User_Def_Type_1 L_Type_1,
      OL.User_Def_Type_2 L_Type_2,
      OL.User_Def_Type_3 L_Type_3,
      OL.User_Def_Type_4 L_Type_4,
      OL.User_Def_Type_5 L_Type_5,
      OL.User_Def_Type_6 L_Type_6,
      OL.User_Def_Type_7 L_Type_7,
      OL.User_Def_Type_8 L_Type_8,
      OL.User_Def_Chk_1 L_Chk_1,
      OL.User_Def_Chk_2 L_Chk_2,
      OL.User_Def_Chk_3 L_Chk_3,
      OL.User_Def_Chk_4 L_Chk_4,
      OL.User_Def_Date_1 L_Date_1,
      OL.User_Def_Date_2 L_Date_2,
      OL.User_Def_Date_3 L_Date_3,
      OL.User_Def_Date_4 L_Date_4,
      OL.User_Def_Num_1 L_Num_1,
      OL.User_Def_Num_2 L_Num_2,
      OL.User_Def_Num_3 L_Num_3,
      OL.User_Def_Num_4 L_Num_4,
      OL.User_Def_Note_1 L_Note_1,
      OL.User_Def_Note_2 L_Note_2,
      OH.User_Def_Type_1 H_Type_1,
      OH.User_Def_Type_2 H_Type_2,
      OH.User_Def_Type_3 H_Type_3,
      OH.User_Def_Type_4 H_Type_4,
      OH.User_Def_Type_5 H_Type_5,
      OH.User_Def_Type_6 H_Type_6,
      OH.User_Def_Type_7 H_Type_7,
      OH.User_Def_Type_8 H_Type_8,
      OH.User_Def_Chk_1 H_Chk_1,
      OH.User_Def_Chk_2 H_Chk_2,
      OH.User_Def_Chk_3 H_Chk_3,
      OH.User_Def_Chk_4 H_Chk_4,
      OH.User_Def_Date_1 H_Date_1,
      OH.User_Def_Date_2 H_Date_2,
      OH.User_Def_Date_3 H_Date_3,
      OH.User_Def_Date_4 H_Date_4,
      OH.User_Def_Num_1 H_Num_1,
      OH.User_Def_Num_2 H_Num_2,
      OH.User_Def_Num_3 H_Num_3,
      OH.User_Def_Num_4 H_Num_4,
      OH.User_Def_Note_1 H_Note_1,
      OH.User_Def_Note_2 H_Note_2
    FROM Order_Header OH,
      Order_Line OL
    WHERE OH.Client_Id = p_Client_Id
    AND OL.Client_Id   = p_Client_Id
    AND OH.Order_Id    = p_Order_Id
    AND OL.Order_Id    = p_Order_Id;
    
  OrderRow OrderSearch%rowtype;
  LineRow LineSearch%rowtype;
  
BEGIN
  l_LineCount := 0;
  
  OPEN OrderSearch;
  LOOP
    FETCH OrderSearch INTO OrderRow;
    IF OrderSearch%NotFound THEN
      EXIT;
    END IF;
    
    FOR LineRow IN LineSearch( OrderRow.Client_Id, OrderRow.Order_Id )
    LOOP
      IF LineRow.L_Chk_4 = 'Y' THEN
        UPDATE Order_line
        SET User_Def_Chk_4='N'
        WHERE Order_Id    = OrderRow.Order_Id
        AND Line_Id       = LineRow.Line_Id;
        
        Result := LibInvTrans.CreateInvTrans (
          TransCode       => 'Shipment',
          UpdateQty       => 0,
          OriginalQty     => LineRow.Qty_Ordered,
          ClientID        => OrderRow.Client_ID,
          SKUID           => LineRow.SKU_ID,
          TagID           => null,
          BatchID         => LineRow.Batch_ID,
          ConditionID     => LineRow.Condition_ID,
          ReferenceID     => OrderRow.Order_ID,
          LineID          => LineRow.Line_ID,
          FromLocation    => 'PLTSHIP001',
          ToLocation      => null,
          StationID       => 'Automatic',
          UserID          => 'Scheduler',
          TmpNotes        => 'Zero Shipped Order Line',
          SummaryRecord   => 'Y',
          SiteID          => LineRow.From_Site_ID,
          OwnerID         => LineRow.Owner_ID,
          OriginID        => LineRow.Origin_ID,
          WorkGroup       => LineRow.Work_Group,
          Consignment     => LineRow.Consignment,
          UserDefType1    => LineRow.L_Type_1,
          UserDefType2    => LineRow.L_Type_2,
          UserDefType3    => LineRow.L_Type_3,
          UserDefType4    => LineRow.L_Type_4,
          UserDefType5    => LineRow.L_Type_5,
          UserDefType6    => LineRow.L_Type_6,
          UserDefType7    => LineRow.L_Type_7,
          UserDefType8    => LineRow.L_Type_8,
          UserDefChk1     => LineRow.L_Chk_1,
          UserDefChk2     => LineRow.L_Chk_2,
          UserDefChk3     => LineRow.L_Chk_3,
          UserDefChk4     => LineRow.L_Chk_4,
          UserDefDate1    => LineRow.L_Date_1,
          UserDefDate2    => LineRow.L_Date_2,
          UserDefDate3    => LineRow.L_Date_3,
          UserDefDate4    => LineRow.L_Date_4,
          UserDefNum1     => LineRow.L_Num_1,
          UserDefNum2     => LineRow.L_Num_2,
          UserDefNum3     => LineRow.L_Num_3,
          UserDefNum4     => LineRow.L_Num_4,
          UserDefNote1    => LineRow.L_Note_1,
          UserDefNote2    => LineRow.L_Note_2,
          ConfigID        => LineRow.Config_ID,
          UploadedCustoms => 'Y',
          LockCode        => LineRow.Lock_Code,
          CustomerID      => LineRow.Customer_ID,
          ExtraNotes      => 'Generated by Clipper Scheduler Job' );
                    
        DELETE
        FROM order_line
        WHERE order_id = OrderRow.Order_ID
        AND line_id    =LineRow.Line_ID;
        
      ELSE
        l_LineCount := l_LineCount + 1;
        
        UPDATE Order_line
        SET user_def_date_1 = CURRENT_TIMESTAMP
        WHERE Order_Id      = OrderRow.Order_Id
        AND Client_Id       = OrderRow.Client_Id
        AND Line_Id         = LineRow.Line_Id;
        
      END IF;
    END LOOP;
    
    IF l_LineCount = 0 THEN
    
      UPDATE order_header
      SET status     = 'Shipped', Num_Lines = l_LineCount
      WHERE Order_Id = OrderRow.Order_Id
      AND Client_Id  = OrderRow.Client_Id;
      
      Result := LibInvTrans.CreateInvTrans (
          TransCode       => 'Order Status',
          UpdateQty       => 0,
          ClientID        => OrderRow.Client_ID,
          TagID           => null,
          ReferenceID     => OrderRow.Order_ID,
          FromLocation    => 'PLTSHIP001',
          ToLocation      => null,
          StationID       => 'Automatic',
          UserID          => 'Scheduler',
          TmpNotes        => 'Released --> Shipped',
          SummaryRecord   => 'Y',
          SiteID          => LineRow.From_Site_ID,
          OwnerID         => LineRow.Owner_ID,
          WorkGroup       => LineRow.Work_Group,
          Consignment     => LineRow.Consignment,
          UserDefType1    => LineRow.H_Type_1,
          UserDefType2    => LineRow.H_Type_2,
          UserDefType3    => LineRow.H_Type_3,
          UserDefType4    => LineRow.H_Type_4,
          UserDefType5    => LineRow.H_Type_5,
          UserDefType6    => LineRow.H_Type_6,
          UserDefType7    => LineRow.H_Type_7,
          UserDefType8    => LineRow.H_Type_8,
          UserDefChk1     => LineRow.H_Chk_1,
          UserDefChk2     => LineRow.H_Chk_2,
          UserDefChk3     => LineRow.H_Chk_3,
          UserDefChk4     => LineRow.H_Chk_4,
          UserDefDate1    => LineRow.H_Date_1,
          UserDefDate2    => LineRow.H_Date_2,
          UserDefDate3    => LineRow.H_Date_3,
          UserDefDate4    => LineRow.H_Date_4,
          UserDefNum1     => LineRow.H_Num_1,
          UserDefNum2     => LineRow.H_Num_2,
          UserDefNum3     => LineRow.H_Num_3,
          UserDefNum4     => LineRow.H_Num_4,
          UserDefNote1    => LineRow.H_Note_1,
          UserDefNote2    => LineRow.H_Note_2,
          ConfigID        => LineRow.Config_ID,
          UploadedCustoms => 'Y',
          CustomerID      => LineRow.Customer_ID,
          FromStatus      => 'Released',
          ToStatus        => 'Shipped',
          ExtraNotes      => 'Generated by Clipper Scheduler Job' );
          
    ELSE
      UPDATE order_line SET allocate = 'Y' WHERE order_id = OrderRow.Order_Id;

      select count(*) into l_LineCount 
      from order_line
      where order_id = OrderRow.Order_Id
      AND Client_Id  = OrderRow.Client_Id;

      update order_header set num_lines = l_LineCount
      where order_id = OrderRow.Order_Id
      AND Client_Id  = OrderRow.Client_Id;

      UPDATE order_header
      SET metapack_carrier_pre = 'N'
      WHERE status = 'Released'
      AND carrier_id          IS NOT NULL
      AND service_level       IS NOT NULL
      AND mpack_consignment   IS NOT NULL
      AND mpack_pre_car_err   IS NULL
      AND order_id = OrderRow.Order_Id
      AND Client_Id  = OrderRow.Client_Id
      AND metapack_carrier_pre = 'Y';

    END IF;
    
  COMMIT;
    
  END LOOP;
  CLOSE OrderSearch;
END;
/
!
exit
