#!/bin/sh

. $HOME/dcs/.dcs_profile

cd $DCS_COMMSDIR/outwork/.

# setup the filename 
date=`date +"%m%d"`
time=`date|awk '{print $4}'|sed 's/\://g'`
file=WILKINSONifnrCxt.$date$time.txt

sqlplus -s $ORACLE_USR << ! >> $file
        set feedback off
        set heading off
        set pagesize 999
	set linesize 500

select 'ISM'||','||'E'||','||client_id||','||sku_id||',,,,,,,,,,,,,,,,,,,,,,,'
||tot_rec||','
||trim(to_char(tot_tot,999990.999999))||','
||trim(to_char(tot_unlock,999990.999999))||','
||trim(to_char(tot_lock,999990.999999))||','
||trim(to_char(tot_alloc,999990.999999))||','
||trim(to_char(unlock_alloc,999990.999999))||','
||trim(to_char(lock_alloc,999990.999999))||','
||trim(to_char(tot_unalloc,999990.999999))||','
||trim(to_char(unlock_unalloc,999990.999999))||','
||trim(to_char(lock_unalloc,999990.999999))||','
||trim(to_char(tot_prerec,999990.999999))||','
||case when tot_susp >= 0 then '+' else '-' end ||','
||trim(to_char(tot_susp,999990.999999))||','
||trim(to_char(tot_repl,999990.999999))||','
||case when description like '%,%' then '"'||description||'"' else description end||','
||SESSIONTIMEZONE||
','||'N'||','||tracking_level||','||'N'||','||''||','||''||','
||trim(to_char(tot_soft_alloc,999990.999999))||','
||trim(to_char(soft_alloc_avail,999990.999999))
from (
SELECT DISTINCT tt.client_id,tt.sku_id,null n1,null n2,null n3,null n4,null n5,null n6,null n7,null n8,null n9,null n10
,null n11,null n12,null n13,null n14,null n15,null n16,null n17,null n18,null n19,null n20,null n21,null n22
,nvl(skuprop.split_lowest,'N')split_lowest
,nvl(tt.trecords,0) tot_rec
,nvl(tt.qtyonhand,0) tot_tot
,greatest(nvl(tt.qtyonhand,0)-nvl(tlocked.qtyonhand,0),0) tot_unlock
,nvl(tlocked.qtyonhand,0) tot_lock
,greatest(nvl(tallc.qtyallocated,0)-nvl(trepl.qtytomove,0)+nvl(tpf.tpf,0)+nvl(tallcinsd.qtyonhand,0),0) tot_alloc
,least(greatest(nvl(tt.qtyonhand,0)-greatest(nvl(tallc.qtyallocated,0)-nvl(trepl.qtytomove,0)+nvl(tpf.tpf,0),0)-(nvl(tlocked.qtyonhand,0)-nvl(lockedallc.qtyallocated,0))-(nvl(tallcinsd.qtyonhand,0)-nvl(lockedallcinsd.qtyonhand,0)),0),nvl(tsoftallc.qtysoftallocated,0)) tot_soft_alloc
,greatest(nvl(tt.qtyonhand,0)-greatest(nvl(tallc.qtyallocated,0)-nvl(trepl.qtytomove,0)+nvl(tpf.tpf,0),0)-(nvl(tlocked.qtyonhand,0)-nvl(lockedallc.qtyallocated,0))-(nvl(tallcinsd.qtyonhand,0)-nvl(lockedallcinsd.qtyonhand,0)),0) soft_alloc_avail
,greatest(nvl(tallc.qtyallocated,0)-nvl(lockedallc.qtyallocated,0)-nvl(trepl.qtytomove,0)+nvl(tpf.tpf,0)+(nvl(tallcinsd.qtyonhand,0)-nvl(lockedallcinsd.qtyonhand,0)),0) unlock_alloc
,nvl(lockedallc.qtyallocated,0)+nvl(lockedallcinsd.qtyonhand,0) lock_alloc
,greatest(nvl(tt.qtyonhand,0)-greatest(nvl(tallc.qtyallocated,0)-nvl(trepl.qtytomove,0)+nvl(tpf.tpf,0),0)-nvl(lockedpf.lockedpf,0)-nvl(tallcinsd.qtyonhand,0),0) tot_unalloc
,greatest(nvl(tt.qtyonhand,0)-greatest(nvl(tallc.qtyallocated,0)-nvl(trepl.qtytomove,0)+nvl(tpf.tpf,0),0)-(nvl(tlocked.qtyonhand,0)-nvl(lockedallc.qtyallocated,0))-(nvl(tallcinsd.qtyonhand,0)-nvl(lockedallcinsd.qtyonhand,0)),0) unlock_unalloc
,greatest(nvl(tlocked.qtyonhand,0)-nvl(lockedallc.qtyallocated,0)-nvl(lockedpf.lockedpf,0)-nvl(lockedallcinsd.qtyonhand,0),0) lock_unalloc
,nvl(tprerec.qtyonhand,0) tot_prerec
,nvl(tsusp.qtyonhand,0) tot_susp
,nvl(trepl.qtytomove,0) tot_repl
,LibSKU.GetUserLangSKUDesc(skuprop.client_id,skuprop.sku_id) description
,LibConfig.GetLowestTrackingLevel(skuprop.client_id,skuprop.sku_id) tracking_level,
SESSIONTIMEZONE
FROM (
SELECT i.sku_id,sum(i.qty_on_hand/nvl(s.each_quantity,1)) qtyonhand,count(i.sku_id) trecords,i.client_id
FROM inventory i, location l, sku s
where i.location_id = l.location_id
and i.sku_id = s.sku_id
and s.client_id = i.client_id 
and (i.site_id=l.site_id or i.site_id is null)
and l.loc_type != 'Suspense'
GROUP BY i.sku_id,i.client_id
) tt, 
(
SELECT i.sku_id,sum(i.qty_allocated/nvl(s.each_quantity,1)) qtyallocated,i.client_id
FROM inventory i, location l, sku s
where i.location_id = l.location_id
and i.sku_id = s.sku_id
and s.client_id = i.client_id 
and (i.site_id=l.site_id or i.site_id is null)
and l.loc_type != 'Suspense'
and i.pick_face is null
and l.loc_type not in ('ShipDock','Trailer')
GROUP BY i.sku_id,i.client_id) tallc,
(
SELECT ol.sku_id,sum(ol.qty_ordered - nvl(ol.qty_tasked,0) - nvl(ol.qty_picked,0) - nvl(ol.qty_shipped,0)) qtysoftallocated,ol.client_id
FROM order_header oh,order_line ol, sku s
where oh.client_id=ol.client_id
and ol.sku_id = s.sku_id
and s.client_id = ol.client_id
and (oh.order_id=ol.order_id)
and oh.Status in ('Released','Hold')
GROUP BY ol.sku_id,ol.client_id) tsoftallc,
(
SELECT i.sku_id,sum(i.qty_allocated/nvl(s.each_quantity,1)) qtyallocated,i.client_id
FROM inventory i,location l, sku s
where i.location_id=l.location_id
and i.sku_id = s.sku_id
and s.client_id = i.client_id
and (i.site_id=l.site_id or i.site_id is null)
and (l.lock_status in ('Locked','OutLocked') or nvl(l.disallow_alloc, 'N') = 'Y' or nvl(i.disallow_alloc, 'N') = 'Y' or i.lock_status='Locked')
and l.loc_type!='Suspense'
and l.loc_type not in ('ShipDock','Trailer')
GROUP BY i.sku_id,i.client_id) lockedallc,
(
SELECT i.sku_id,sum(i.qty_on_hand/nvl(s.each_quantity,1)) qtyonhand,i.client_id
FROM inventory i,location l, sku s
where i.location_id=l.location_id
and i.sku_id = s.sku_id
and s.client_id = i.client_id
and (i.site_id=l.site_id or i.site_id is null)
and (l.lock_status in ('Locked','OutLocked') or nvl(l.disallow_alloc, 'N') = 'Y' or nvl(i.disallow_alloc, 'N') = 'Y' or i.lock_status='Locked')
and l.loc_type!='Suspense'
GROUP BY i.sku_id,i.client_id) tlocked,
(
SELECT i.sku_id,sum(i.qty_on_hand/nvl(s.each_quantity,1)) qtyonhand,i.client_id
FROM inventory i,location l, sku s
where i.location_id=l.location_id
and i.sku_id = s.sku_id
and s.client_id = i.client_id
and (i.site_id=l.site_id or i.site_id is null) 
and l.loc_type!='Suspense'
and l.loc_type in ('ShipDock','Trailer')
GROUP BY i.sku_id,i.client_id) tallcinsd,
(
SELECT i.sku_id,sum(i.qty_on_hand/nvl(s.each_quantity,1)) qtyonhand,i.client_id
FROM inventory i,location l, sku s
where i.location_id=l.location_id
and i.sku_id = s.sku_id
and s.client_id = i.client_id
and (i.site_id=l.site_id or i.site_id is null)
and (l.lock_status in ('Locked','OutLocked') or nvl(l.disallow_alloc, 'N') = 'Y' or nvl(i.disallow_alloc, 'N') = 'Y' or i.lock_status='Locked')
and l.loc_type!='Suspense'
and l.loc_type in ('ShipDock','Trailer')
GROUP BY i.sku_id,i.client_id) lockedallcinsd,
(
SELECT i.sku_id, sum(i.qty_on_hand/nvl(s.each_quantity,1)) qtyonhand,0 tprerec,i.client_id
FROM inventory i,location l, sku s
where i.location_id=l.location_id
and i.sku_id = s.sku_id
and (i.site_id=l.site_id or i.site_id is null)
and i.pre_received='Y'
GROUP BY i.sku_id,i.client_id) tprerec,
(
SELECT i.sku_id,sum(m.old_qty_to_move/nvl(s.each_quantity,1)) qtytomove,i.client_id
FROM inventory i,move_task m,location l, sku s
where i.location_id=l.location_id
and i.location_id=m.old_from_loc_id
and i.sku_id = s.sku_id
and s.client_id = i.client_id
and (i.site_id=l.site_id or i.site_id is null)
and (i.tag_id=m.tag_id or (m.tag_id is null and m.sku_id=i.sku_id))
and (i.pallet_id=m.pallet_id or (i.pallet_id is null and m.pallet_id is null))
and (i.container_id=m.container_id or (i.container_id is null and m.container_id is null))
and m.task_type='R' 
GROUP BY i.sku_id,i.client_id) trepl,
(
SELECT i.sku_id,sum(m.old_qty_to_move/nvl(s.each_quantity,1)) qtytomove,i.client_id
FROM inventory i,move_task m,location l, sku s
where i.location_id=l.location_id
and i.location_id=m.old_from_loc_id
and i.sku_id = s.sku_id
and s.client_id = i.client_id
and (i.site_id=l.site_id or i.site_id is null)
and (i.tag_id=m.tag_id or (m.tag_id is null and m.sku_id=i.sku_id))
and (i.pallet_id=m.pallet_id or (i.pallet_id is null and m.pallet_id is null))
and (i.container_id=m.container_id or (i.container_id is null and m.container_id is null))
and m.task_type='R'
and (l.lock_status in ('Locked','OutLocked') or nvl(l.disallow_alloc, 'N') = 'Y' or nvl(i.disallow_alloc, 'N') = 'Y' or i.lock_status='Locked')
GROUP BY i.sku_id,i.client_id) repllocked,
(
SELECT i.sku_id,sum(i.qty_on_hand/nvl(s.each_quantity,1)) qtyonhand,0 tsusp,i.client_id
FROM inventory i,location l, sku s
where i.location_id=l.location_id
and i.sku_id = s.sku_id
and s.client_id = i.client_id
and (i.site_id=l.site_id or i.site_id is null)
and l.loc_type='Suspense'
GROUP BY i.sku_id,i.client_id) tsusp,
(
SELECT p.sku_id,nvl(sum(p.qty_allocated/nvl(s.each_quantity,1)),0) tpf,p.client_id
FROM pick_face p,location l, sku s
where p.site_id=l.site_id(+)
and p.location_id=l.location_id(+)
and p.sku_id = s.sku_id
and s.sku_id = p.client_id
GROUP BY p.sku_id,p.client_id) tpf,
(
SELECT p.sku_id,nvl(sum(i.qty_on_hand*LibInventory.IsInventoryLocked(l.rowid,i.rowid)/nvl(s.each_quantity,1)),0) lockedpf,p.client_id
FROM pick_face p,location l,inventory i, sku s
where p.site_id=l.site_id(+)
and p.location_id=l.location_id(+)
and p.site_id=i.site_id(+)
and p.location_id=i.location_id(+)
and p.client_id=i.client_id(+)
and p.sku_id=i.sku_id(+)
and p.sku_id = s.sku_id
and p.client_id = s.client_id
GROUP BY p.sku_id,p.client_id) lockedpf,
(
SELECT NVL(split_lowest, 'N') split_lowest,sku_id,client_id
FROM sku
where 1=1
) skuprop
where tt.sku_id=lockedallc.sku_id(+)
and tt.sku_id=tallc.sku_id(+)
and tt.sku_id=tlocked.sku_id(+)
and tt.sku_id=tprerec.sku_id(+)
and tt.sku_id=lockedallcinsd.sku_id(+)
and tt.sku_id=tallcinsd.sku_id(+)
and tt.sku_id=trepl.sku_id(+)
and tt.sku_id=tsusp.sku_id(+)
and tt.sku_id=repllocked.sku_id(+)
and tt.sku_id=tpf.sku_id(+)
and tt.sku_id=lockedpf.sku_id(+)
and tt.sku_id=skuprop.sku_id(+)
and tt.sku_id=tsoftallc.sku_id(+)
and nvl(tt.client_id,'^')=nvl(lockedallc.client_id(+),'^')
and nvl(tt.client_id,'^')=nvl(tallc.client_id(+),'^')
and nvl(tt.client_id,'^')=nvl(tlocked.client_id(+),'^')
and nvl(tt.client_id,'^')=nvl(tprerec.client_id(+),'^')
and nvl(tt.client_id,'^')=nvl(lockedallcinsd.client_id(+),'^')
and nvl(tt.client_id,'^')=nvl(tallcinsd.client_id(+),'^')
and nvl(tt.client_id,'^')=nvl(trepl.client_id(+),'^')
and nvl(tt.client_id,'^')=nvl(tsusp.client_id(+),'^')
and nvl(tt.client_id,'^')=nvl(repllocked.client_id(+),'^')
and nvl(tt.client_id,'^')=nvl(tpf.client_id(+),'^')
and nvl(tt.client_id,'^')=nvl(lockedpf.client_id(+),'^')
and nvl(tt.client_id,'^')=nvl(skuprop.client_id(+),'^')
and nvl(tt.client_id,'^')=nvl(tsoftallc.client_id(+),'^')
UNION
SELECT DISTINCT tsusp.client_id,tsusp.sku_id,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null
,nvl(skuprop.split_lowest,'N')split_lowest 
,0 tot_rec
,0 tot_tot
,0 tot_unlock
,0 tot_lock
,0 tot_alloc
,0 tot_soft_alloc
,0 soft_alloc_avail
,0 unlock_alloc
,0 lock_alloc
,0 tot_unalloc
,0 unlock_unalloc
,0 lock_unalloc
,0 tot_prerec
,nvl(tsusp.qtyonhand,0) tot_susp
,0 tot_repl
,LibSKU.GetUserLangSKUDesc(skuprop.client_id,skuprop.sku_id) description
,LibConfig.GetLowestTrackingLevel(skuprop.client_id,skuprop.sku_id) tracking_level,
SESSIONTIMEZONE
FROM(
SELECT i.sku_id,sum(i.qty_on_hand/nvl(s.each_quantity,1)) qtyonhand,count(i.sku_id) trecords,i.client_id
FROM inventory i, location l, sku s
where i.location_id=l.location_id 
and i.sku_id = s.sku_id
and i.client_id = s.client_id
and (i.site_id=l.site_id or i.site_id is null)
and l.loc_type != 'Suspense'
GROUP BY i.sku_id,i.client_id) tt, 
(
SELECT i.sku_id,sum(i.qty_on_hand/nvl(s.each_quantity,1)) qtyonhand,0 tsusp,i.client_id
FROM inventory i,location l, sku s
where i.location_id=l.location_id
and i.sku_id = s.sku_id
and i.client_id = s.client_id
and (i.site_id=l.site_id or i.site_id is null)
and l.loc_type='Suspense'
GROUP BY i.sku_id,i.client_id) tsusp,
(
SELECT NVL(split_lowest, 'N') split_lowest,sku_id,client_id
FROM sku
where 1=1
) skuprop
where tsusp.sku_id = tt.sku_id (+) 
and tsusp.sku_id = skuprop.sku_id (+) 
and tt.qtyonhand is null
and nvl(tsusp.client_id,'^')=nvl(tt.client_id(+),'^')
ORDER BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24
)
union all
SELECT 'ISM'||','||'E'||','||client_id||','||sku_id||',,,,,,,,,,,,,,,,,,,,,,,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000'||case when description like '%,%' then '"'||description||'"' else description end||','
||SESSIONTIMEZONE||
','||'N'||','||'E'||','||'N'||','||''||','||''||',0.000000,0.000000' from sku where sku_id not in (select sku_id from inventory);

!

# remove empty lines from the output file
sed -i '/^$/d' $file

# copy to outarchive
#cp $file ../outarchive/.

# move the file to the outtray
#mv $file ../outtray/.
