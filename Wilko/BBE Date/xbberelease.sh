#!/bin/bash
. $HOME/dcs/.dcs_profile

# identify the file
FILE="$(basename $0)"

# Variables
LOGFILE="$DCS_TMPDIR/$ORACLE_SID/${FILE}.trc"


function release
{
ssh wmswcs@rhl-oraw-drapp1 << 'endextract'
	cd AndyP
	cd BBE_DATE_CONFIG
	functionaccessdatautil -a -s
	mergerulesdatautil -s -a -t 6
endextract

if [[ "$?" -ne 0 ]]
  then
    # something went wrong
    echo "  Something went wrong while Extracting Config from Test" 1>&2 >>$LOGFILE
    exit 1
  fi

	mkdir /home/wmsprd/AndyP/BBE_DATE
	mkdir /home/wmsprd/AndyP/BBE_DATE/backup
	mkdir /home/wmsprd/AndyP/BBE_DATE/release

if [[ "$?" -ne 0 ]]
  then
    # something went wrong
    echo "  Something went wrong while creating folder structure" 1>&2 >>$LOGFILE
    exit 1
  fi


	ssh wmswcs@rhl-oraw-drapp1 scp /home/wmswcs/AndyP/BBE_DATE_CONFIG/*.dmp wmsprd@rhl-oraw-dpapp1:/home/wmsprd/AndyP/BBE_DATE/release

if [[ "$?" -ne 0 ]]
  then
    # something went wrong
    echo "  Something went wrong while Collecting Files" 1>&2 >>$LOGFILE
    exit 1
  fi


	cd /home/wmsprd/AndyP/BBE_DATE/backup1

 if [[ "$?" -ne 0 ]]
  then
    # something went wrong
    echo "  Something went wrong while Backing up current config" 1>&2 >>$LOGFILE
    exit 1
  fi

	functionaccessdatautil -a -s
	mergerulesdatautil -s -a -t 6
  
 if [[ "$?" -ne 0 ]]
  then
    # something went wrong
    echo "  Something went wrong while Backing up current config" 1>&2 >>$LOGFILE
    exit 1
  fi

	cd /home/wmsprd/AndyP/BBE_DATE/release1

 if [[ "$?" -ne 0 ]]
  then
    # something went wrong
    echo "  Something went wrong while Releasing config !!! rollback required !!!!" 1>&2 >>$LOGFILE
    exit 1
  fi

	#functionaccessdatautil -a -l
	#mergerulesdatautil -l -a -t 6

	echo "this shouldn't of happened" >> $LOGFILE

 if [[ "$?" -ne 0 ]]
  then
    # something went wrong
    echo "  Something went wrong while Releasing config !!! rollback required !!!!" 1>&2 >>$LOGFILE
    exit 1
  fi

	echo "RELEASE COMPLETED" >> $LOGFILE

}


function rollback
{
	cd /home/wmsprd/AndyP/BBE_DATE/backup1

 if [[ "$?" -ne 0 ]]
  then
    # something went wrong
    echo "  Something went wrong while Rolling Back !!! rollback failed !!!!" 1>&2 >>$LOGFILE
    exit 1
  fi
   echo "this shouldn't of happened on backup" >> $LOGFILE
	#functionaccessdatautil -a -l
    #mergerulesdatautil -l -a -t 6
 
 if [[ "$?" -ne 0 ]]
  then
    # something went wrong
    echo "  Something went wrong while Rolling Back !!! rollback failed !!!!" 1>&2 >>$LOGFILE
    exit 1
  fi

    echo "ROLLBACK COMPLETED" >>$LOGFILE

    rm -rf /home/wmsprd/AndyP/BBE_DATE

     if [[ "$?" -ne 0 ]]
  then
    # something went wrong
    echo "  CLEAN UP failed !!!!" 1>&2 >>$LOGFILE
    exit 1
  fi
}



echo "$FILE started at `date`  " >>$LOGFILE

{
read -p "Do you wish to release key 1 rollback key 2 ? " choice
case "$choice" in 
  1 ) echo "Starting Release" ; release;;
  2 ) echo "Starting to Rollback" ; rollback;;
  * ) echo "invalid"; exit 0;;
esac
} >>$LOGFILE