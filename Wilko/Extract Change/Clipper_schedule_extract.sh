#!/bin/bash

#/******************************************************************************/
#/*                                                                            */
#/* NAME:         Schedule Extract                                             */
#/*                                                                            */
#/* DESCRIPTION:  Schedule extract will check if extracts are required         */
#/*               If extract is required then call appropriate exctract program*/
#/*                                                                            */
#/* Date       By  Proj    Ref      Description                                 /
#/* ---------- --- ------- -------- -------------------------                  */
#/* 30/08/2019 AMP WILKINSON        Run all extracts required                  */
#/******************************************************************************/

. $HOME/dcs/.dcs_profile

# Canord Extrtact #

sqlplus -s $ORACLE_USR > KEYSORD << EOF
SET SERVEROUTPUT ON
SET FEEDBACK OFF

declare
	key_count number;
begin
	select
		count (order_id)  
	into
		key_count
	from
			order_header 
		where
			status in ('Shipped', 'Cancelled')
			and uploaded = 'N';
	
	DBMS_OUTPUT.PUT_LINE(key_count);
end;
/
exit
EOF


keycountord=`cat KEYSORD`
            
if ((keycountord > 0)); then
	schedulerextracts.sh "extractord: -O5 -o WILKINSONordCxt -c WILKINSON"
fi

# Tailer Manifest extract #

sqlplus -s $ORACLE_USR > KEYSMAN << EOF
SET SERVEROUTPUT ON
SET FEEDBACK OFF

declare
	key_count number;
begin
	select
		count (key)  
	into
		key_count
	from
			shipping_manifest 
		where
			shipped = 'Y'
			and carrier_id = 'FLEET'
			and uploaded = 'N';
	
	DBMS_OUTPUT.PUT_LINE(key_count);
end;
/
exit
EOF


keycountman=`cat KEYSMAN`

if ((keycountman > 0)); then
	schedulerextracts.sh -s -u "extractman: -r FLEET -O3"
fi

# DESPRET and STOCKADJ extract #

sqlplus -s $ORACLE_USR > KEYSITL << EOF
SET SERVEROUTPUT ON
SET FEEDBACK OFF

declare
	key_count number;
begin
	select
		count (KEY)  
	into
		key_count
	from
			inventory_transaction 
		where
			code in ('Receipt','Adjustment')
			and uploaded = 'N';
	
	DBMS_OUTPUT.PUT_LINE(key_count);
end;
/
exit
EOF


keycountitl=`cat KEYSITL`

if ((keycountitl > 0)); then
	schedulerextracts.sh "extractitl: -O5 -o WILKINSONitlCxt -c WILKINSON -p WILKO"
fi

# Confdesp Extrtact #

sqlplus -s $ORACLE_USR > KEYSDESP << EOF
SET SERVEROUTPUT ON
SET FEEDBACK OFF

declare
	key_count number;
begin
	select
		count (order_id)  
	into
		key_count
	from
			order_header 
		where
			status in ('Shipped')
			and user_def_chk_4 = 'N';
	
	DBMS_OUTPUT.PUT_LINE(key_count);
end;
/
exit
EOF

keycountdesp=`cat KEYSDESP`

if ((keycountdesp > 0)); then
	ml_confdespdae -1 -l
fi

# EDICONF Extract #

sqlplus -s $ORACLE_USR > KEYSEDI << EOF
SET SERVEROUTPUT ON
SET FEEDBACK OFF

declare
	key_count number;
begin
	select
		count (order_id)  
	into
		key_count
	from
			order_header 
		where
			user_def_chk_1 = 'N';
	
	DBMS_OUTPUT.PUT_LINE(key_count);
end;
/
exit
EOF

keycountedi=`cat KEYSEDI`

if ((keycountedi > 0)); then
sqlplus -s $ORACLE_USR << EOF
SET SERVEROUTPUT ON
SET FEEDBACK OFF
declare
	result integer := 0;
begin
	LibPrint.AdvancedPrintDoc
							 (
								 p_Result     =>result
							   ,p_ReportName  =>'UREPEDICONF'
							   ,p_PrintCommand=>''
							   ,p_Language    =>'EN_GB'
							   ,p_TimeZoneName=>'Europe/London'
							   ,p_ExportType  =>'P'
							 );
	update
		order_header
	set user_def_chk_1 = 'Y'
	where
		user_def_chk_1   = 'N'
		and from_site_id = 'OLLE'
	;
	
	Commit;
end;
/
exit
EOF

fi

# CONFPUT Extract #

sqlplus -s $ORACLE_USR > KEYSPUT << EOF
SET SERVEROUTPUT ON
SET FEEDBACK OFF

declare
	key_count number;
begin
	select
		count (key)  
	into
		key_count
	from
			inventory_transaction
		where
			code = 'Putaway'
			and dstamp > sysdate - (10/1440);
	
	DBMS_OUTPUT.PUT_LINE(key_count);
end;
/
exit
EOF

keycountput=`cat KEYSPUT`

if ((keycountput > 0)); then
sqlplus -s $ORACLE_USR << EOF
SET SERVEROUTPUT ON
SET FEEDBACK OFF
declare
	result integer := 0;
begin
	LibPrint.AdvancedPrintDoc
							 (
								 p_Result     =>result
							   ,p_ReportName  =>'UREPCONFPUT'
							   ,p_PrintCommand=>''
							   ,p_Language    =>'EN_GB'
							   ,p_TimeZoneName=>'Europe/London'
							   ,p_ExportType  =>'P'
							 );
end;
/
exit
EOF

fi


