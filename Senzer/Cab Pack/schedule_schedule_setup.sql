BEGIN

DBMS_SCHEDULER.CREATE_SCHEDULE (
 schedule_name   => 'SENZ_IF_S',
 repeat_interval  => 'freq=minutely;interval=2',
 comments     => 'Run interface Jobs every 2 minutes');

insert into scheduler_schedule values ('SENZ_IF_S',null,systimestamp,null,'freq=minutely;interval=2','Run interface Jobs every 2 minutes','RELEASE',systimestamp,'RELEASE',systimestamp);

 DBMS_SCHEDULER.CREATE_SCHEDULE (
  schedule_name   => 'SENZ_INV_EXP_EXT_S',
  repeat_interval  => 'freq=daily;byhour=2;',
  comments     => 'Aged stock Summary daily extract');

insert into scheduler_schedule values ('SENZ_INV_EXP_EXT_S',null,systimestamp,null,'freq=daily;byhour=2;','Aged stock Summary daily extract','RELEASE',systimestamp,'RELEASE',systimestamp);

  DBMS_SCHEDULER.CREATE_SCHEDULE (
   schedule_name   => 'SENZ_INV_MOVE_EXT_S',
   repeat_interval  => 'freq=daily;byhour=2;',
   comments     => 'Aged stock movement daily extract');

insert into scheduler_schedule values ('SENZ_INV_MOVE_EXT_S',null,systimestamp,null,'freq=daily;byhour=2;','Aged stock movement daily extract','RELEASE',systimestamp,'RELEASE',systimestamp);

  DBMS_SCHEDULER.CREATE_SCHEDULE (
    schedule_name   => 'SENZ_INV_SUM_EXT_S',
    repeat_interval  => 'freq=daily;byhour=6,14;',
    comments     => 'Inventory Summary twice daily extract');

insert into scheduler_schedule values ('SENZ_INV_SUM_EXT_S',null,systimestamp,null,'freq=daily;byhour=6,14;','Inventory Summary twice daily extract','RELEASE',systimestamp,'RELEASE',systimestamp);

    DBMS_SCHEDULER.CREATE_SCHEDULE (
     schedule_name   => 'SENZ_PO_EXT_S',
     repeat_interval  => 'freq=daily;byhour=3;',
     comments     => 'Po daily extract');

insert into scheduler_schedule values ('SENZ_PO_EXT_S',null,systimestamp,null,'freq=daily;byhour=3;','Inventory Summary twice daily extract','RELEASE',systimestamp,'RELEASE',systimestamp);

  DBMS_SCHEDULER.CREATE_SCHEDULE (
    schedule_name   => 'SENZ_FILE_EXT_S',
    repeat_interval  => 'freq=daily;byhour=6,14;',
    comments     => 'File Summary twice daily extract');

insert into scheduler_schedule values ('SENZ_FILE_EXT_s',null,systimestamp,null,'freq=daily;byhour=6,14;','File Summary twice daily extract','RELEASE',systimestamp,'RELEASE',systimestamp);

END;
