#!/bin/sh

. $HOME/dcs/.dcs_profile

cd $DCS_COMMSDIR/senzer/outwork/.

# setup the filename
date=`date +"%C%y%m%d_%H%M%S000"`
time=`date|awk '{print $4}'|sed 's/\://g'`
file=SENZ_ORD_SUM_$date.csv

sqlplus -s $ORACLE_USR << ! >> $file
set serveroutput on
set feedback off
declare
cursor output is
SELECT
    order_header.order_id||','||
    order_header.status||','||
    to_char(order_header.order_date,'DD/MM/YY HH24:MI')||','||
    to_char(order_header.creation_date,'DD/MM/YY HH24:MI')||','||
    to_char(order_header.shipped_date,'DD/MM/YY HH24:MI')||','||
case when (to_number(to_char(order_date,'HH24')) <= 14
    and nvl(trunc(shipped_date),trunc(sysdate)) = trunc(order_date)
    and status != 'Shipped') then 'FORECAST ON TIME,'

    when (to_number (to_char(order_date,'HH24')) > 14
    and nvl(trunc(shipped_date),trunc(sysdate)) <= trunc(order_date +1)
    and status != 'Shipped') then 'FORECAST ON TIME,'

    when (to_number(to_char(order_date,'HH24')) <= 14
    and (shipped_date is null or shipped_date is not null)
    and nvl(trunc(shipped_date),trunc(sysdate)) != trunc(order_date)
    and status != 'Shipped'
     and order_header.order_id IN (
            SELECT
                task_id
            FROM
                move_descrepancy
            WHERE
                move_descrepancy.client_id = 'SENZ'
        )
             AND order_header.status NOT IN (
            'Shipped',
            'Complete'
        ))
    then 'FORECAST LATE,STOCK ISSUE'

    when (to_number(to_char(order_date,'HH24')) > 14
    and (shipped_date is null or shipped_date is not null)
    and nvl(trunc(shipped_date),trunc(sysdate)) >= trunc(order_date +1)
    and status != 'Shipped'
         and order_header.order_id IN (
            SELECT
                task_id
            FROM
                move_descrepancy
            WHERE
                move_descrepancy.client_id = 'SENZ'
        )
             AND order_header.status NOT IN (
            'Shipped',
            'Complete'
        ))
    then 'FORECAST LATE,STOCK ISSUE'

        when (to_number(to_char(order_date,'HH24')) <= 14
    and (shipped_date is null or shipped_date is not null)
    and nvl(trunc(shipped_date),trunc(sysdate)) != trunc(order_date)
    and status != 'Shipped'
     and order_header.order_id not IN (
            SELECT
                task_id
            FROM
                move_descrepancy
            WHERE
                move_descrepancy.client_id = 'SENZ'
        )
             AND order_header.status NOT IN (
            'Shipped',
            'Complete'
        ))
    then 'FORECAST LATE,'

    when (to_number(to_char(order_date,'HH24')) > 14
    and (shipped_date is null or shipped_date is not null)
    and nvl(trunc(shipped_date),trunc(sysdate)) >= trunc(order_date +1)
    and status != 'Shipped'
         and order_header.order_id NOT IN (
            SELECT
                task_id
            FROM
                move_descrepancy
            WHERE
                move_descrepancy.client_id = 'SENZ'
        )
             AND order_header.status NOT IN (
            'Shipped',
            'Complete'
        ))
    then 'FORECAST LATE,'

    when (to_number(to_char(order_date,'HH24')) <= 14
    and nvl(trunc(shipped_date),trunc(sysdate)) = trunc(order_date))  then 'ON TIME,'

    when (to_number(to_char(order_date,'HH24')) > 14
    and nvl(trunc(shipped_date),trunc(sysdate)) <= trunc(order_date +1))  then 'ON TIME,'

    when (to_number(to_char(order_date,'HH24')) <= 14
    and (shipped_date is null or shipped_date is not null)
    and nvl(trunc(shipped_date),trunc(sysdate)) != trunc(order_date))  then 'LATE,'

    when (to_number(to_char(order_date,'HH24')) > 14
    and (shipped_date is null or shipped_date is not null)
    and nvl(trunc(shipped_date),trunc(sysdate)) >= trunc(order_date +1))  then 'LATE,'
    END AS sla
FROM
    order_header
WHERE
    order_header.client_id = 'SENZ'
ORDER BY
    order_header.creation_date DESC;

outputrow  output%rowtype;

begin
	DBMS_OUTPUT.ENABLE
					  (
						  buffer_size => NULL
					  );
dbms_output.put_line ('Order Id,Status,Order Date,Interface Date,Shipped Date,SLA,Picking Issues');
open output;
loop
    fetch output into outputrow;
if output%notfound then
    exit;
end if;
dbms_output.put_line (outputrow.sla);
end loop;
close output;
end;
/
exit
!


# remove empty lines from the output file
sed -i '/^$/d' $file


echo "Senzer Order Well Summary" | mail -a $file -s "3,2 Senzer Order Status Report" SenzerOperationalReports@clippergroup.co.uk; 

# copy to outarchive
cp $file ../outarchive/.

# move the file to the outtray
mv $file ../outtray/.
