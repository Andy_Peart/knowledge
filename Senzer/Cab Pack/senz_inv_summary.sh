#!/bin/sh

. $HOME/dcs/.dcs_profile

cd $DCS_COMMSDIR/senzer/outwork/.

# setup the filename
date=`date +"%C%y%m%d_%H%M%S000"`
time=`date|awk '{print $4}'|sed 's/\://g'`
file=SENZ_INV_SUM_$date.csv

sqlplus -s $ORACLE_USR << !

create table tmp_senz_inv_sum as(
select distinct(sku_id), sum(qty_on_hand) "HELD" from inventory where client_id = 'SENZ' group by sku_id);

create table tmp_senz_inv_lock_sum as(
select distinct(sku_id) , sum(qty_on_hand) "HELD" from inventory where client_id = 'SENZ' and lock_status = 'Locked'group by sku_id);

create table tmp_senz_ord_sum  as(
select distinct(ol.sku_id), sum(ol.qty_ordered) "ORDERED" from order_line ol, order_header oh
where oh.client_id = 'SENZ'
and oh.status not in ('Shipped','Cancelled')
and oh.order_id = ol.order_id
group by sku_id);

exit
!

sqlplus -s $ORACLE_USR << !
spool $DCS_COMMSDIR/senzer/outwork/$file
set colsep ,
set headsep off
set pagesize 0
set trimspool on
set trimout on
set linesize 1500
set tab off
set feedback off
select 'SKU,Description,On Hand,Ordered,Available,Held' from dual;
select iv.sku_id||','||
    s.description||','||
    iv.held||','||
    os.ordered||','||
    sum(iv.held - os.ordered - il.held)||','||
    il.held
    from tmp_senz_ord_sum os, tmp_senz_inv_sum iv ,sku s, tmp_senz_inv_lock_sum il
where iv.sku_id = os.sku_id
and s.sku_id = iv.sku_id
and iv.sku_id = il.sku_id
group by
iv.sku_id||','||s.description||','|| iv.held||','|| os.ordered,il.held
union all
select iv.sku_id||','||
    s.description||','||
    iv.held||','||
    null||','||
    sum(iv.held - il.held)||','||
    il.held from tmp_senz_inv_sum iv ,sku s, tmp_senz_inv_lock_sum il
where iv.sku_id not in
(select ol.sku_id from order_line ol, order_header oh
where oh.client_id = 'SENZ'
and oh.status not in ('Shipped','Cancelled')
and oh.order_id = ol.order_id)
and s.sku_id = iv.sku_id
and iv.sku_id = il.sku_id
group by
iv.sku_id||','||s.description||','|| iv.held,il.held
union all
select iv.sku_id||','||
    s.description||','||
    iv.held||','||
    null||','||
    sum(iv.held - 0)||','||
    null from tmp_senz_inv_sum iv ,sku s
    where iv.sku_id not in
    (select sku_id from tmp_senz_inv_lock_sum)
    and iv.sku_id not in (select sku_id from tmp_senz_ord_sum)
group by
iv.sku_id||','||s.description||','|| iv.held
union all
select iv.sku_id||','||
    s.description||','||
    iv.held||','||
    os.ordered||','||
    sum(iv.held - os.ordered)||','||
    null
    from tmp_senz_ord_sum os, tmp_senz_inv_sum iv ,sku s
    where iv.sku_id = os.sku_id
    and s.sku_id = iv.sku_id
    and iv.sku_id not in
    (select sku_id from tmp_senz_inv_lock_sum)
group by
iv.sku_id||','||s.description||','|| iv.held||','|| os.ordered;
spool off
exit
!


sqlplus -s $ORACLE_USR << !

drop Table TMP_SENZ_INV_SUM;
drop Table TMP_SENZ_ORD_SUM;
drop table tmp_senz_inv_lock_sum;

exit
!

# remove empty lines from the output file
sed -i '/^$/d' $file

echo "Senzer Inventory Summary" | mail -a $file -s "2.1 Senzer Inventory Report" SenzerOperationalReports@clippergroup.co.uk;

# copy to outarchive
cp $file ../outarchive/.

# move the file to the outtray
mv $file ../outtray/.
