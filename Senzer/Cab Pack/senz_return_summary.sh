#!/bin/sh

. $HOME/dcs/.dcs_profile

cd $DCS_COMMSDIR/senzer/outwork/.

# setup the filename
date=`date +"%C%y%m%d_%H%M%S000"`
time=`date|awk '{print $4}'|sed 's/\://g'`
file=SENZ_RET_SUM_$date.csv

sqlplus -s $ORACLE_USR << !

spool $DCS_COMMSDIR/senzer/outwork/$file
set colsep ,
set headsep off
set pagesize 0
set trimspool on
set trimout on
set linesize 1500
set tab off
set feedback off
select 'Processed Date,Order,Customer,SKU,Description,QTY,Reason' from dual;
select to_char(dstamp,'DD/MM/YYYY HH24:MI')||','||
    reference_id||','||
    contact||','||
    itl.sku_id||','||
    s.description||','||
    update_qty||','||
    itl.USER_DEF_TYPE_1
    from inventory_transaction itl, order_header oh, sku s
    where itl.client_id = 'SENZ'
    and itl.USER_DEF_TYPE_1 is not null
    and code = 'Receipt'
    and reference_id = order_id
    and itl.sku_id = s.sku_id
    and s.client_id = 'SENZ'
    and oh.client_id = 'SENZ';

spool off

exit

!


# remove empty lines from the output file
sed -i '/^$/d' $file

echo "Senzer Return Processing Summary" | mail -a $file -s "4,1 Senzer Booked Returns Report" SenzerOperationalReports@clippergroup.co.uk; 

# copy to outarchive
cp $file ../outarchive/.

# move the file to the outtray
mv $file ../outtray/.
