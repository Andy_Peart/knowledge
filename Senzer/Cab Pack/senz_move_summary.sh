#!/bin/sh

. $HOME/dcs/.dcs_profile

cd $DCS_COMMSDIR/senzer/outwork/.

# setup the filename
date=`date +"%C%y%m%d_%H%M%S000"`
time=`date|awk '{print $4}'|sed 's/\://g'`
file=SENZ_ST_MOVE_SUM_$date.csv

sqlplus -s $ORACLE_USR << !
spool $DCS_COMMSDIR/senzer/outwork/$file
set colsep ,
set headsep off
set pagesize 0
set trimspool on
set trimout on
set linesize 1500
set tab off
set feedback off
select 'Expiry,Batch,SKU,Description,QTY,Movement,Movement Reason,Date' from dual;
select
    to_char(i.expiry_dstamp,'DD/MM/RRRR')||','||
    i.batch_id||','||
    i.sku_id||','||
    s.description||','||
    i.update_qty||','||
    i.code||','||
    nvl(notes,(case when i.code = 'Inv UnLock' then 'Locked (PICK) -> UnLocked ()'
    when i.code = 'Inv Lock' then 'UnLocked () -> Locked (PICK)' end))||','||
    to_char(i.dstamp,'DD/MM/RRRR')
from inventory_transaction i,sku s
    where i.code like 'Inv%Lock%'
    and i.sku_id = s.sku_id
    and i.client_id = 'SENZ'
    and s.client_id = 'SENZ'
;
spool off
exit
!


# remove empty lines from the output file
sed -i '/^$/d' $file


echo "Senzer Inventory Movement Summary" | mail -a $file -s "2.3 Senzer Stock Movement Report" SenzerOperationalReports@clippergroup.co.uk; 

# copy to outarchive
cp $file ../outarchive/.

# move the file to the outtray
mv $file ../outtray/.
