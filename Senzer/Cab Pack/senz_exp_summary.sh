#!/bin/sh

. $HOME/dcs/.dcs_profile

cd $DCS_COMMSDIR/senzer/outwork/.

# setup the filename
date=`date +"%C%y%m%d_%H%M%S000"`
time=`date|awk '{print $4}'|sed 's/\://g'`
file=SENZ_EXP_SUM_$date.csv

sqlplus -s $ORACLE_USR << !

create table tmp_senz_inv_sum1 as(
select distinct(sku_id), sum(qty_on_hand) "HELD",batch_id, expiry_dstamp from inventory where client_id = 'SENZ' group by sku_id, batch_id, expiry_dstamp);

create table tmp_senz_inv_lock_sum1 as(
select distinct(sku_id) , sum(qty_on_hand) "HELD", batch_id, expiry_dstamp from inventory where client_id = 'SENZ' and lock_status = 'Locked' group by sku_id, batch_id, expiry_dstamp);


spool $DCS_COMMSDIR/senzer/outwork/$file
set colsep ,
set headsep off
set pagesize 0
set trimspool on
set trimout on
set linesize 1500
set tab off
set feedback off
select 'Expiry,Batch,SKU,Description,On Hand,Held' from dual;
select trunc(ih.expiry_dstamp)||','||ih.batch_id||','||ih.sku_id||','||s.description||','||ih.held||','||0
from tmp_senz_inv_sum1 ih, sku s
where trunc(ih.expiry_dstamp)||','||ih.batch_id||','||ih.sku_id not in (select trunc(ih.expiry_dstamp)||','||ih.batch_id||','||ih.sku_id from tmp_senz_inv_lock_sum1 ih)
and s.sku_id = ih.sku_id
union all
select trunc(ih.expiry_dstamp)||','||ih.batch_id||','||ih.sku_id||','||s.description||','||0||','||ih.held
from tmp_senz_inv_lock_sum1 ih, sku s
where trunc(ih.expiry_dstamp)||','||ih.batch_id||','||ih.sku_id in (select trunc(ih.expiry_dstamp)||','||ih.batch_id||','||ih.sku_id from tmp_senz_inv_lock_sum1 ih)
and s.sku_id = ih.sku_id
union all
select trunc(ih.expiry_dstamp)||','||ih.batch_id||','||ih.sku_id||','||s.description||','||ih.held||','||il.held
from tmp_senz_inv_lock_sum1 il,tmp_senz_inv_sum1 ih, sku s
where s.sku_id = ih.sku_id
and trunc(ih.expiry_dstamp)||','||ih.batch_id||','||ih.sku_id = trunc(il.expiry_dstamp)||','||il.batch_id||','||il.sku_id;

drop table tmp_senz_inv_sum1;
drop table tmp_senz_inv_lock_sum1;

exit

!


# remove empty lines from the output file
sed -i '/^$/d' $file

echo "Senzer Aged Stock Summary" | mail -a $file -s "2.2 Senzer Aged Stock Summary" SenzerOperationalReports@clippergroup.co.uk; 

# copy to outarchive
cp $file ../outarchive/.

# move the file to the outtray
mv $file ../outtray/.
