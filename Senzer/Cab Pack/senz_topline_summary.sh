#!/bin/sh

. $HOME/dcs/.dcs_profile

cd $DCS_COMMSDIR/senzer/outwork/.

# setup the filename
date=`date +"%C%y%m%d_%H%M%S000"`
time=`date|awk '{print $4}'|sed 's/\://g'`
file=SENZ_ORD_TOP_SUM_$date.csv

sqlplus -s $ORACLE_USR << !

create table tmp_senz_topline as
select oh.order_id,oh.NUM_LINES,sum(ol.qty_ordered)as Order_Qty,status,
case when (to_number(to_char(order_date,'HH24'))) <= 14
    then trunc(order_date)
    when (to_number(to_char(order_date,'HH24'))) > 14
    then trunc(order_date +1)
end "SHIP_DUE",
to_date(nvl(trunc(oh.shipped_date),(sysdate + 365))) "SHIP_DATE",
trunc(order_date) "ORDER_DATE"
from order_header oh, order_line ol
where ol.order_id = oh.order_id
and oh.client_id = 'SENZ'
group by  oh.order_id,oh.NUM_LINES,status,
case when (to_number(to_char(order_date,'HH24'))) <= 14
    then trunc(order_date)
    when (to_number(to_char(order_date,'HH24'))) > 14
    then trunc(order_date +1)
end,
to_date(nvl(trunc(oh.shipped_date),(sysdate + 365))),
trunc(order_date)
;

create table tmp_senz_sla as
select order_id,status,
sum(ship_date - ship_due)"DAYS_LATE",order_date
from tmp_senz_topline
group by order_id,status, order_date;

create table tmp_senz_sla_data as
select sla.ORDER_DATE, count(sla.order_id)as orders,sum(tl.num_lines)as lines,sum(tl.order_qty) as quantity ,
case when sla.days_late  = 0 then 0
      when sla.days_late =1 then 1
      when sla.days_late =2 then 2
      when sla.days_late >2 then 3
end as Days_Late,
case when sla.days_late  = 0 then count(sla.order_id)
      when sla.days_late =1 then count(sla.order_id)
      when sla.days_late =2 then count(sla.order_id)
      when sla.days_late >2 then count(sla.order_id)
end as orders_late,sla.status
from tmp_senz_sla sla,tmp_senz_topline tl
where tl.order_date = sla.order_date
group by sla.order_date,sla.days_late,sla.status;

create table tmp_senz_sla_calc as
select order_date,sum(orders)as orders,sum(lines)as lines,sum(quantity)as units,
case when status != 'Shipped' then sum(orders)end as not_shipped,
case when days_late =1 and status = 'Shipped' then sum(orders_late) end as one_day,
case when days_late =2 and status = 'Shipped' then  sum(orders_late) end as two_day,
case when days_late =3 and status = 'Shipped' then sum(orders_late) end as more_day,
case when days_late =0 and status = 'Shipped' then sum(orders_late) end as on_time
from tmp_senz_sla_data
group by order_date,status,days_late
order by order_date;


spool $DCS_COMMSDIR/senzer/outwork/$file
set colsep ,
set headsep off
set pagesize 0
set trimspool on
set trimout on
set linesize 1500
set tab off
set feedback off
select 'Order Date,Orders,Lines,Units,In SLA,Un Shipped,Un Shipped %,Day +1,Day +2,Day >2,Day +1 %,Day +2 %,Day >2 %,OTIF %' from dual;
select order_date||','||
nvl(sum(orders),0)||','||
nvl(sum(lines),0)||','||
nvl(sum(units),0)||','||
nvl(sum(on_time),0)||','||
nvl(sum(not_shipped),0)||','||
nvl(to_char(round(sum(not_shipped)/sum(orders)*100,2)),'0')||','||
nvl(sum(one_day),0)||','||
nvl(sum(two_day),0)||','||
nvl(sum(more_day),0)||','||
nvl(to_char(round(sum(one_day)/sum(orders)*100,2)),'0')||','||
nvl(to_char(round(sum(two_day)/sum(orders)*100,2)),'0')||','||
nvl(to_char(round(sum(more_day)/sum(orders)*100,2)),'0')||','||
nvl(to_char(round(sum(on_time)/sum(orders)*100,2)),'0')
from tmp_senz_sla_calc
group by order_date||',';

spool off
drop table tmp_senz_topline;
drop table tmp_senz_sla;
drop table tmp_senz_sla_data;
drop table tmp_senz_sla_calc;

exit

!


# remove empty lines from the output file
sed -i '/^$/d' $file

echo "Senzer Topline SLA Summary" | mail -a $file -s "3,4 Senzer Fulfilemnt Report - TopLine" SenzerOperationalReports@clippergroup.co.uk; 

# copy to outarchive
cp $file ../outarchive/.

# move the file to the outtray
mv $file ../outtray/.
