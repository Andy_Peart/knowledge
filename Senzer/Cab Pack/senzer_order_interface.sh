#!/bin/bash

. $HOME/dcs/.dcs_profile

cd $HOME/comms/senzer/intray

# If there is a file containing a list of files to be processed then delete the file
if [ -e filenames.txt ]; then
	echo "ok"
	rm filenames.txt
else
	echo "no file"
fi

#Write a list of all files into a seperate file to
if ls -1 -d SENZ_ORD*.* 1>/dev/null  2>&1; then
	ls -1 -d SENZ_ORD*.* >filenames.txt
else
	echo "error out"
	exit 1
fi

#Enter a while loop reading the filenames from filenames.txt
while read i; do
	#remove the header line from the file
	echo "$(tail -n +2 $i)" >$i
	#sed 's/"//g' $i
	#Enter a while loop to read all the fields in the file IFS defines the field seperator
	while IFS="|" read f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 f29 f30 f31 f32 f33 f34 f35 f36 f37 f38 f39 f40 f41 f42 f43 f44 f45 f46 f47 f48 f49 f50 f51 f52 f53 f54 f55 f56 f57 f58 f59 f60 f61 f62 f63 f64 f65 f66 f67 f68 f69 f70 f71 f72 f73 f74 f75 f76 f77 f78 f79 f80 f81 f82 f83 f84 f85 f86 f87 f88 f89 f90	f91	f92	f93	f94	f95	f96	f97	f98	f99	f100	f101	f102	f103	f104	f105	f106	f107	f108	f109	f110	f111	f112	f113	f114	f115	f116	f117	f118	f119; do
		#Drop into SQL to start inserting orders into the interface
		sqlplus -s $ORACLE_USR <<EOF

		set serveroutput on

		declare
		--Populate the variables with the values read from the file

		C_ORDER_ID interface_order_header.ORDER_ID%type := upper('$f11');
		C_ORDER_TYPE interface_order_header.ORDER_TYPE%type := upper('$f4');
		C_WORK_GROUP interface_order_header.WORK_GROUP%type := upper('$f4');
		C_CUSTOMER_ID interface_order_header.CUSTOMER_ID%type := upper('$f8');
		C_ORDER_DATE interface_order_header.ORDER_DATE%type := to_timestamp(to_char( TO_timestamp('$f9','YYYYMMDDHH24MISS'),'DD-MON-RR HH.MI.SSXFF AM'),'DD-MON-RR HH.MI.SSXFF AM'); --interface file in 24hr this converts to 12hr
		C_PURCHASE_ORDER interface_order_header.PURCHASE_ORDER%type := upper('$f3');
		C_CONTACT interface_order_header.CONTACT%type := upper('$f12');
		C_CONTACT_PHONE interface_order_header.CONTACT_PHONE%type := upper('$f13');
		C_CONTACT_MOBILE interface_order_header.CONTACT_MOBILE%type := upper('$f13');
		C_CONTACT_FAX interface_order_header.CONTACT_FAX%type := upper('$f14');
		C_CONTACT_EMAIL interface_order_header.CONTACT_EMAIL%type := upper('$f15');
		C_NAME interface_order_header.NAME%type := upper('$f16');
		C_ADDRESS1 interface_order_header.ADDRESS1%type := upper('$f17');
		C_ADDRESS2 interface_order_header.ADDRESS2%type := upper('$f18');
		C_TOWN interface_order_header.TOWN%type := upper('$f19');
		C_COUNTY interface_order_header.COUNTY%type := upper('$f20');
		C_POSTCODE interface_order_header.POSTCODE%type := upper('$f21');
		C_COUNTRY interface_order_header.COUNTRY%type := upper('$f22');
		C_CARRIER_ID interface_order_header.CARRIER_ID%type := upper('$f25');
		C_SERVICE_LEVEL interface_order_header.SERVICE_LEVEL%type := upper('$f26');
		C_INV_ADDRESS_ID interface_order_header.INV_ADDRESS_ID%type := upper('$f27');
		C_INV_CONTACT interface_order_header.INV_CONTACT%type := upper('$f28');
		C_INV_CONTACT_PHONE interface_order_header.INV_CONTACT_PHONE%type := upper('$f29');
		C_INV_CONTACT_MOBILE interface_order_header.INV_CONTACT_MOBILE%type := upper('$f30');
		C_INV_CONTACT_FAX interface_order_header.INV_CONTACT_FAX%type := upper('$f31');
		C_INV_CONTACT_EMAIL interface_order_header.INV_CONTACT_EMAIL%type := upper('$f32');
		C_INV_NAME interface_order_header.INV_NAME%type := upper('$f33');
		C_INV_ADDRESS1 interface_order_header.INV_ADDRESS1%type := upper('$f34');
		C_INV_ADDRESS2 interface_order_header.INV_ADDRESS2%type := upper('$f35');
		C_INV_TOWN interface_order_header.INV_TOWN%type := upper('$f36');
		C_INV_COUNTY interface_order_header.INV_COUNTY%type := upper('$f37');
		C_INV_POSTCODE interface_order_header.INV_POSTCODE%type := upper('$f38');
		C_EXPORT interface_order_header.EXPORT%type := upper('$f41');
		C_INSTRUCTIONS interface_order_header.instructions%type := upper('$i');
		C_HEADER_EXIST number ;
		--C_PRODUCT_CURRENCY interface_order_line.product_currency%type := upper ('$f87');
		C_QTY_ORDERED interface_order_line.qty_ordered%type := to_number('$f81');
		C_KIT_SKU interface_order_line.sku_id%type := upper('$f75');
		C_LINE_ID interface_order_line.line_id%type := 1;
		-- cursor used to read the kit so the lines can be built
		cursor C_ORDER_LINE is
			select sku_id,quantity
			from kit_line
			where client_id = 'SENZ'
			and kit_id = C_KIT_SKU;

		R_ORDER_LINE C_ORDER_LINE%rowtype;

		begin
		C_HEADER_EXIST := 0;
		select greatest(nvl((select max(line_id) from interface_order_line where order_id = C_ORDER_ID),0),nvl((select max(line_id) from order_line where order_id = C_ORDER_ID),0)) into C_LINE_ID from dual;
		C_LINE_ID := C_LINE_ID +1;
		--Check if header already exists in the interface
		--select 1 into C_HEADER_EXIST from interface_order_header i, order_header h, dual where (i.order_id = C_ORDER_ID or h.order_id = C_ORDER_ID);
		if
			C_HEADER_EXIST = 0
		then
		-- build the order header interface using the variables defined above

			insert into interface_order_header
			(KEY,CLIENT_ID,ORDER_ID,ORDER_TYPE,WORK_ORDER_TYPE,STATUS,MOVE_TASK_STATUS,PRIORITY,SHIP_DOCK,WORK_GROUP,CONSIGNMENT,DELIVERY_POINT,LOAD_SEQUENCE,FROM_SITE_ID,TO_SITE_ID,OWNER_ID,CUSTOMER_ID,ORDER_DATE,SHIP_BY_DATE,DELIVER_BY_DATE,PURCHASE_ORDER,CONTACT,CONTACT_PHONE,CONTACT_MOBILE,CONTACT_FAX,CONTACT_EMAIL,NAME,ADDRESS1,ADDRESS2,TOWN,COUNTY,POSTCODE,COUNTRY,INSTRUCTIONS,REPACK,CARRIER_ID,DISPATCH_METHOD,SERVICE_LEVEL,FASTEST_CARRIER,CHEAPEST_CARRIER,INV_ADDRESS_ID,INV_CONTACT,INV_CONTACT_PHONE,INV_CONTACT_MOBILE,INV_CONTACT_FAX,INV_CONTACT_EMAIL,INV_NAME,INV_ADDRESS1,INV_ADDRESS2,INV_TOWN,INV_COUNTY,INV_POSTCODE,INV_COUNTRY,PSFT_DMND_SRCE,PSFT_ORDER_ID,SITE_REPLEN,CID_NUMBER,SID_NUMBER,LOCATION_NUMBER,FREIGHT_CHARGES,DISALLOW_MERGE_RULES,EXPORT,SOH_ID,REPACK_LOC_ID,USER_DEF_TYPE_1,USER_DEF_TYPE_2,USER_DEF_TYPE_3,USER_DEF_TYPE_4,USER_DEF_TYPE_5,USER_DEF_TYPE_6,USER_DEF_TYPE_7,USER_DEF_TYPE_8,USER_DEF_CHK_1,USER_DEF_CHK_2,USER_DEF_CHK_3,USER_DEF_CHK_4,USER_DEF_DATE_1,USER_DEF_DATE_2,USER_DEF_DATE_3,USER_DEF_DATE_4,USER_DEF_NUM_1,USER_DEF_NUM_2,USER_DEF_NUM_3,USER_DEF_NUM_4,USER_DEF_NOTE_1,USER_DEF_NOTE_2,CE_REASON_CODE,CE_REASON_NOTES,CE_ORDER_TYPE,CE_CUSTOMS_CUSTOMER,CE_EXCISE_CUSTOMER,CE_INSTRUCTIONS,DELIVERED_DSTAMP,SIGNATORY,ROUTE_ID,CROSS_DOCK_TO_SITE,WEB_SERVICE_ALLOC_IMMED,WEB_SERVICE_ALLOC_CLEAN,DISALLOW_SHORT_SHIP,HUB_ADDRESS_ID,HUB_CONTACT,HUB_CONTACT_PHONE,HUB_CONTACT_MOBILE,HUB_CONTACT_FAX,HUB_CONTACT_EMAIL,HUB_NAME,HUB_ADDRESS1,HUB_ADDRESS2,HUB_TOWN,HUB_COUNTY,HUB_POSTCODE,HUB_COUNTRY,HUB_CARRIER_ID,HUB_SERVICE_LEVEL,STATUS_REASON_CODE,STAGE_ROUTE_ID,SINGLE_ORDER_SORTATION,FORCE_SINGLE_CARRIER,EXPECTED_VOLUME,EXPECTED_WEIGHT,EXPECTED_VALUE,TOD,TOD_PLACE,LANGUAGE,SELLER_NAME,SELLER_PHONE,DOCUMENTATION_TEXT_1,DOCUMENTATION_TEXT_2,DOCUMENTATION_TEXT_3,COD,COD_VALUE,COD_CURRENCY,COD_TYPE,VAT_NUMBER,INV_VAT_NUMBER,HUB_VAT_NUMBER,PRINT_INVOICE,INV_REFERENCE,INV_DSTAMP,INV_CURRENCY,LETTER_OF_CREDIT,PAYMENT_TERMS,SUBTOTAL_1,SUBTOTAL_2,SUBTOTAL_3,SUBTOTAL_4,FREIGHT_COST,FREIGHT_TERMS,INSURANCE_COST,MISC_CHARGES,DISCOUNT,OTHER_FEE,INV_TOTAL_1,INV_TOTAL_2,INV_TOTAL_3,INV_TOTAL_4,TAX_RATE_1,TAX_BASIS_1,TAX_AMOUNT_1,TAX_RATE_2,TAX_BASIS_2,TAX_AMOUNT_2,TAX_RATE_3,TAX_BASIS_3,TAX_AMOUNT_3,TAX_RATE_4,TAX_BASIS_4,TAX_AMOUNT_4,TAX_RATE_5,TAX_BASIS_5,TAX_AMOUNT_5,ORDER_REFERENCE,COLLECTIVE_MODE,COLLECTIVE_SEQUENCE,START_BY_DATE,METAPACK_CARRIER_PRE,SHIPMENT_GROUP,FREIGHT_CURRENCY,NCTS,GLN,HUB_GLN,INV_GLN,ALLOW_PALLET_PICK,SPLIT_SHIPPING_UNITS,VOL_PCK_SSCC_LABEL,ALLOCATION_PRIORITY,TRAX_USE_HUB_ADDR,MPACK_CONSIGNMENT,MPACK_NOMINATED_DSTAMP,DIRECT_TO_STORE,VOL_CTR_LABEL_FORMAT,RETAILER_ID,CARRIER_BAGS,SESSION_TIME_ZONE_NAME,TIME_ZONE_NAME,NLS_CALENDAR,CLIENT_GROUP,MERGE_ACTION,MERGE_STATUS,MERGE_ERROR,MERGE_DSTAMP)
			values
				(if_oh_pk_seq.nextval,'SENZ',C_ORDER_ID,C_ORDER_TYPE,null,'Released','Released','50','ASSEMBLY',C_ORDER_TYPE,null,null,null,'ROTH001',null,'SENZ',C_CUSTOMER_ID,C_ORDER_DATE,null,null,C_PURCHASE_ORDER,
					C_CONTACT,C_CONTACT_PHONE,C_CONTACT_MOBILE,C_CONTACT_FAX,C_CONTACT_EMAIL,C_NAME,C_ADDRESS1,C_ADDRESS2,C_TOWN,C_COUNTY,C_POSTCODE,'GBR',
					C_INSTRUCTIONS,'N',C_CARRIER_ID,null,C_SERVICE_LEVEL,'N','N',C_INV_ADDRESS_ID,C_INV_CONTACT,C_INV_CONTACT_PHONE,C_INV_CONTACT_MOBILE,C_INV_CONTACT_FAX,C_INV_CONTACT_EMAIL,C_INV_NAME,
					C_INV_ADDRESS1,C_INV_ADDRESS2,C_INV_TOWN,C_INV_COUNTY,C_INV_POSTCODE,null,null,null,null,null,null,null,null,null,C_EXPORT
					,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null
					,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null
					,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null
					,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
					null,null,null,null,'Y',null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,'A','Pending',null,to_timestamp(to_char(systimestamp,'DD-MON-RR HH.MI.SSXFF AM')));
			-- set the starting line id

			-- Open the order line Cursor and loop through inserting all the kit sku's into the order line interface
			OPEN C_ORDER_LINE;
			LOOP
				FETCH C_ORDER_LINE into R_ORDER_LINE;
				IF C_ORDER_LINE%notfound
				THEN
					EXIT;
				END IF;
				Insert into interface_order_line
				(KEY,CLIENT_ID,ORDER_ID,LINE_ID,SKU_ID,CONFIG_ID,TRACKING_LEVEL,BATCH_MIXING,SHELF_LIFE_DAYS,QTY_ORDERED,PRODUCT_CURRENCY,OWNER_ID,COLLECTIVE_MODE,CLIENT_GROUP,MERGE_ACTION,MERGE_STATUS,HOST_ORDER_ID,HOST_LINE_ID )
				VALUES
				(if_ol_pk_seq.nextval, 'SENZ', C_ORDER_ID,C_LINE_ID, R_ORDER_LINE.SKU_ID ,'SM1' ,'EACHES','Y' ,'180' ,C_QTY_ORDERED*R_ORDER_LINE.QUANTITY
					,null ,'SENZ', 'Y' ,null, 'A', 'Pending',null,null);
				-- increment the line id
				C_LINE_ID := C_LINE_ID +1;
			END LOOP;
			CLOSE C_ORDER_LINE;
			COMMIT;
		end if;
		end;
		/
		exit

EOF
	done <$i
	echo "NEW FILE"
	# Once the file is done rename it with a .old so it is not processed again
	mv $i $HOME/comms/senzer/inarchive/$i.old
	# Loop through all the files listed in the file
done <filenames.txt
