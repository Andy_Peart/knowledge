#!/bin/sh

##################################################################################################################################################################################
# Script name: auto_ship.sh
# Creator: Andy Peart
# Date: 10/09/2020
# Version: 1.0
# Purpose: Automatically Ship orders on a ship dock
# ***************************************CHANGES**********************************************************************************************************************************
#
##################################################################################################################################################################################

. $HOME/dcs/.dcs_profile

PROG=$(basename $0)
DATETIMEFILE=`date +%Y%m%d%H%M%S`
DATEFILE=`date +%Y%m%d`
LOGFILE=$DCS_PACKAGELOGDIR/$PROG"_"$DATEFILE.log

function usage
{
        echo ""
        echo "Usage: $PROG [options]"
        echo ""
        echo "[-c]       CLIENT"
		echo "[-s]       SITE_ID"
		echo ""
        echo "Note This scrip will Ship all orders that can be shipped"
        echo "from the shipdock on the defined client"
		echo ""
}		
		
		
		
while getopts s:c:? 2> /dev/null ARG 
do 
 case ${ARG} 
 in 
  c) CLIENT=${OPTARG};;
  s) SITE=${OPTARG};;
  ?) usage
     exit 1;;
 esac 
done 

sqlplus -s $ORACLE_USR  << ! >| $LOGFILE
SET SERVEROUTPUT ON;

declare
	l_client order_header.client_id%type := upper('$CLIENT');
	l_site location.site_id%type := upper('$SITE');
	l_return integer := 0;
	run_needed integer      := 0;
	v_code         NUMBER;
	v_errm         VARCHAR2(64);
	
/* Find all orders That need Shipping */
	
	cursor OrderSearch is
	  select distinct order_header.order_id,order_header.client_id
	  from dcsdba.order_header, dcsdba.shipping_manifest sm1, dcsdba.location
	  where order_header.status = 'Complete'
	  and sm1.order_id = order_header.order_id
	  and nvl(sm1.shipped,'N') != 'Y'
	  and location.loc_type = 'ShipDock'
	  and location.location_id = sm1.location_id
	  and order_header.client_id = l_client
	  and location.user_def_chk_2 = 'Y'
	  and location.site_id = l_site
	  and not exists 
		(select 1 from dcsdba.shipping_manifest sm2 where sm2.key = sm1.key 
		and picked_dstamp > sysdate  -2);

	OrderRow OrderSearch%rowtype;
	
	begin
	
	
	  dcsdba.libsession.initialisesession(
	UserID          =>'AUTOSHIP',
	GroupID         =>'AUTOSHIP',
	StationID       =>'AUTOSHIP',
	WksGroupID      =>'AUTOSHIP'
	);

/* Firstly check if any ship docks for the site have a flag to ship */
	
	select count(location_id) into run_needed from location
		where loc_type = 'ShipDock'
		and user_def_chk_2 = 'Y'
		and site_id = l_site;
	
	IF run_needed != 0 THEN
	
/* If there is something to ship then start the process of shipping */	

		OPEN OrderSearch;
		/* Loop Through each row in the cursor */
		LOOP
			FETCH OrderSearch
			INTO
				OrderRow
			;
			
/* Once the Cursor is empty then exit the Loop */
			
			IF OrderSearch%notfound THEN
				/*EXIT WHEN NO DATA FOUND*/
				EXIT;
			END IF;
			
/* Run the package to ship the order */

			dcsdba.LibShipment.ShipOrder(result => l_return,
							  clientid => orderRow.client_id,
							  orderid => orderRow.order_id
							  );
							  
/* Run the package to mark it as Ready to manifest for CMS */							  
							  
			dcsdba.LibMetaPack.MarkOrderConsAsRTM(
						   orderRow.client_id,
						  orderRow.order_id
						  ); 
						  
/* Output The details To the log file */ 						  
						  
			dbms_output.put_line ('order = '||orderRow.order_id ||' Shipped');
			
		END LOOP;
		
		CLOSE OrderSearch;

/* remove the flags from the locations so they will not be considered again */

update location 
		set user_def_chk_2 = 'N' 		
		where loc_type = 'ShipDock'
		and user_def_chk_2 = 'Y'
		and site_id = l_site;
	
/* commit the Update */
		
		commit;
		
/* Error Reporting to the log file */
	END IF;
		EXCEPTION
			/* WHEN ERROR OCCURS OUTPUT ERROR CODE AND CYCLE THROUGH CURSORS TO CHECK THEY ARE ALL SHUT*/
		WHEN OTHERS THEN
			v_code := SQLCODE;
			v_errm := SUBSTR(SQLERRM, 1, 64);
			DBMS_OUTPUT.PUT_LINE (v_code
			|| ' '
			|| v_errm);
			IF OrderSearch%isopen then
				close OrderSearch;
			END IF;

END;
/
!

echo ""
echo "All Orders Shipped"

echo ""
echo "cat $LOGFILE"
cat $LOGFILE