#!/bin/sh

. $HOME/dcs/.dcs_profile

cd $DCS_COMMSDIR/senzer/outwork/.

# setup the filename
date=`date +"%C%y%m%d_%H%M%S000"`
time=`date|awk '{print $4}'|sed 's/\://g'`
file=SENZ_RET_SUM_$date.csv

sqlplus -s $ORACLE_USR << !

spool $DCS_COMMSDIR/senzer/outwork/$file
set colsep ,
set headsep off
set pagesize 0
set trimspool on
set trimout on
set linesize 1500
set tab off
set feedback off
select 'Filename,interface Date,Orders,Min Order, Max Order' from dual;
select distinct(instructions)||','||to_char(creation_date,'DD/MM/RRRR HH24:MI')||','||count(order_id)||','||min(order_id)||','||max(order_id)
from order_header
where client_id = 'SENZ'
and instructions is not null
group by instructions||','||to_char(creation_date,'DD/MM/RRRR HH24:MI')
order by 1 desc;

spool off

exit

!


# remove empty lines from the output file
sed -i '/^$/d' $file

echo "Senzer Interface File Summary" | mail -a $file -s "3.1 Senzer Order files Received and Processed" SenzerOperationalReports@clippergroup.co.uk; 

# copy to outarchive
cp $file ../outarchive/.

# move the file to the outtray
mv $file ../outtray/.
