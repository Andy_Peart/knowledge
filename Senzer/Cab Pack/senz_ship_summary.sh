#!/bin/sh

. $HOME/dcs/.dcs_profile

cd $DCS_COMMSDIR/senzer/outwork/.

# setup the filename
date=`date +"%C%y%m%d_%H%M%S000"`
time=`date|awk '{print $4}'|sed 's/\://g'`
file=SENZ_SHIP_SUM_$date.csv

sqlplus -s $ORACLE_USR << !
spool $DCS_COMMSDIR/senzer/outwork/$file
set colsep ,
set headsep off
set pagesize 0
set trimspool on
set trimout on
set linesize 1500
set tab off
set feedback off
select 'Order,Order Date,Shipped Date, Fulfilment,SKU,Description,Qty' from dual;
select oh.order_id||','||
    to_char(oh.creation_date,'dd/mm/rrrr hh24:mi')||','||
    to_char(oh.shipped_date,'dd/mm/rrrr hh24:mi')||','||
    '100%'||','||
    ol.sku_id||','||
    s.description||','||
    ol.qty_shipped
from order_header oh, order_line ol, sku s
where ol.sku_id = s.sku_id
and oh.order_id = ol.order_id
and oh.client_id = 'SENZ'
and s.client_id = 'SENZ'
and oh.status = 'Shipped'
order by oh.creation_date, oh.order_id;
spool off
!


# remove empty lines from the output file
sed -i '/^$/d' $file

# copy to outarchive
cp $file ../outarchive/.

# move the file to the outtray
mv $file ../outtray/.
