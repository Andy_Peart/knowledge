#!/bin/sh

. $HOME/dcs/.dcs_profile

cd $DCS_COMMSDIR/senzer/outwork/.

# setup the filename
date=`date +"%C%y%m%d_%H%M%S000"`
time=`date|awk '{print $4}'|sed 's/\://g'`
file=SENZ_HOLD_SUM_$date.csv


sqlplus -s $ORACLE_USR << !
spool $DCS_COMMSDIR/senzer/outwork/$file
set colsep ,
set headsep off
set pagesize 0
set trimspool on
set trimout on
set linesize 1500
set tab off
set feedback off
select 'SKU,Description,QTY,Held Reason' from dual;
select i.sku_id||','||
    s.description||','||
    sum(i.qty_on_Hand)||','||
    nvl(i.lock_code,'PICK')
    from inventory i, Sku s
where i.sku_id = s.sku_id
and i.lock_status = 'Locked'
and i.owner_id = 'SENZ'
and s.client_id = 'SENZ'
group by
i.sku_id||','||
    s.description,nvl(i.lock_code,'PICK');

spool off

exit
!


# remove empty lines from the output file
sed -i '/^$/d' $file

echo "Senzer Held Inventory Summary" | mail -a $file -s "2.4 Senzer Held Stock Report" SenzerOperationalReports@clippergroup.co.uk; 

# copy to outarchive
cp $file ../outarchive/.

# move the file to the outtray
mv $file ../outtray/.
