BEGIN
    DBMS_SCHEDULER.create_program(
        program_name => 'SENZ_EXP_EXT_P',
        program_action => 'begin LibRunTask.CreateSchedulerRunTask(p_Command => ''scheduler.sh senz_exp_summary.sh ''); end;',
        program_type => 'PLSQL_BLOCK',
        number_of_arguments => 0,
        comments => 'Senzer Batch Expiry Extract',
        enabled => FALSE);
    DBMS_SCHEDULER.ENABLE(name=>'SENZ_EXP_EXT_P');

    insert into scheduler_program values ('SENZ_EXP_EXT_P','PLSQL_BLOCK', 'begin LibRunTask.CreateSchedulerRunTask(p_Command => ''scheduler.sh senz_exp_summary.sh ''); end;',null,'Y','Senzer Expiry Extract','RELEASE',systimestamp,'RELEASE',systimestamp);

    DBMS_SCHEDULER.create_program(
        program_name => 'SENZ_INV_MOVE_EXT_P',
        program_action => 'begin LibRunTask.CreateSchedulerRunTask(p_Command => ''scheduler.sh senz_mov_summary.sh ''); end;',
        program_type => 'PLSQL_BLOCK',
        number_of_arguments => 0,
        comments => 'Inventory Movement Summary',
        enabled => FALSE);
    DBMS_SCHEDULER.ENABLE(name=>'SENZ_INV_MOVE_EXT_P');

    insert into scheduler_program values ('SENZ_INV_MOVE_EXT_P','PLSQL_BLOCK', 'begin LibRunTask.CreateSchedulerRunTask(p_Command => ''scheduler.sh senz_mov_summary.sh ''); end;',null,'Y','Senzer Movement Extract','RELEASE',systimestamp,'RELEASE',systimestamp);

    DBMS_SCHEDULER.create_program(
        program_name => 'SENZ_INV_SUM_EXT_P',
        program_action => 'begin LibRunTask.CreateSchedulerRunTask(p_Command => ''scheduler.sh senz_inv_summary.sh ''); end;',
        program_type => 'PLSQL_BLOCK',
        number_of_arguments => 0,
        comments => 'Inventory Summary',
        enabled => FALSE);
    DBMS_SCHEDULER.ENABLE(name=>'SENZ_INV_SUM_EXT_P');

insert into scheduler_program values ('SENZ_INV_SUM_EXT_P','PLSQL_BLOCK', 'begin LibRunTask.CreateSchedulerRunTask(p_Command => ''scheduler.sh senz_inv_summary.sh ''); end;',null,'Y','Senzer Inventory Extract','RELEASE',systimestamp,'RELEASE',systimestamp);

    DBMS_SCHEDULER.create_program(
        program_name => 'SENZ_PO_EXT_P',
        program_action => 'begin LibRunTask.CreateSchedulerRunTask(p_Command => ''scheduler.sh senz_po_summary.sh ''); end;',
        program_type => 'PLSQL_BLOCK',
        number_of_arguments => 0,
        comments => 'PO Summary',
        enabled => FALSE);
    DBMS_SCHEDULER.ENABLE(name=>'SENZ_PO_EXT_P');

insert into scheduler_program values ('SENZ_PO_EXT_P','PLSQL_BLOCK', 'begin LibRunTask.CreateSchedulerRunTask(p_Command => ''scheduler.sh senz_po_summary.sh ''); end;',null,'Y','Senzer pre advice Extract','RELEASE',systimestamp,'RELEASE',systimestamp);

    DBMS_SCHEDULER.create_program(
        program_name => 'SENZ_ORDER_IF_P',
        program_action => 'begin LibRunTask.CreateSchedulerRunTask(p_Command => ''scheduler.sh senzer_order_interface.sh ''); end;',
        program_type => 'PLSQL_BLOCK',
        number_of_arguments => 0,
        comments => 'Order Interface',
        enabled => FALSE);
    DBMS_SCHEDULER.ENABLE(name=>'SENZ_ORDER_IF_P');

insert into scheduler_program values ('SENZ_ORDER_IF_P','PLSQL_BLOCK', 'begin LibRunTask.CreateSchedulerRunTask(p_Command => ''scheduler.sh senz_order_interface.sh ''); end;',null,'Y','Senzer Order Interface','RELEASE',systimestamp,'RELEASE',systimestamp);

    DBMS_SCHEDULER.create_program(
        program_name => 'SENZ_PO_IF_P',
        program_action => 'begin LibRunTask.CreateSchedulerRunTask(p_Command => ''scheduler.sh senzer_po_interface.sh ''); end;',
        program_type => 'PLSQL_BLOCK',
        number_of_arguments => 0,
        comments => 'Inventory Summary',
        enabled => FALSE);
    DBMS_SCHEDULER.ENABLE(name=>'SENZ_PO_IF_P');

  insert into scheduler_program values ('SENZ_ORDER_IF_P','PLSQL_BLOCK', 'begin LibRunTask.CreateSchedulerRunTask(p_Command => ''scheduler.sh senz_po_interface.sh ''); end;',null,'Y','Senzer Pre Advice Interface','RELEASE',systimestamp,'RELEASE',systimestamp);

    DBMS_SCHEDULER.create_program(
        program_name => 'SENZ_FILE_EXT_P',
        program_action => 'begin LibRunTask.CreateSchedulerRunTask(p_Command => ''scheduler.sh senzer_file_summary.sh ''); end;',
        program_type => 'PLSQL_BLOCK',
        number_of_arguments => 0,
        comments => 'File Summary',
        enabled => FALSE);
    DBMS_SCHEDULER.ENABLE(name=>'SENZ_FILE_EXT_P');

  insert into scheduler_program values ('SENZ_FILE_EXT_P','PLSQL_BLOCK', 'begin LibRunTask.CreateSchedulerRunTask(p_Command => ''scheduler.sh senz_file_summary.sh ''); end;',null,'Y','Senzer Pre Advice Interface','RELEASE',systimestamp,'RELEASE',systimestamp);
commit;

END;
/
