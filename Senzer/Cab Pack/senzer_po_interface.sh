#!/bin/bash

. $HOME/dcs/.dcs_profile

cd $HOME/comms/bmd

# If there is a file containing a list of files to be processed then delete the file
if [ -e filenames.txt ]; then
	echo "ok"
	rm filenames.txt
else
	echo "no file"
fi

#Write a list of all files into a seperate file to  this will need updating once file names are confirmed
if ls -1 -d SENZ_PO*.* 1>/dev/null  2>&1; then
	ls -1 -d SENZ_PO*.* >filenames1.txt
else
	echo "error out"
	exit 1
fi

#Enter a while loop reading the filenames from filenames.txt
while read i; do

# this section removes headers if the filr has no headers then leave commented

	#remove the header line from the file
#	echo "$(tail -n +2 $i)" >$i
# sed 's/"//g' $i


	#Enter a while loop to read all the fields in the file IFS defines the field seperator
	while IFS="," read f1 f2 f3 f4; do
		#Drop into SQL to start inserting orders into the interface
		sqlplus -s $ORACLE_USR <<EOF

		set serveroutput on

		declare
		--Populate the variables with the values read from the file

		C_PALLET_ID INVENTORY.PALLET_ID%type := upper('$f1');
		C_TAG_ID interface_INVENTORY.TAG_ID%type := upper('$f2');
		C_SEND_TO INVENTORY.USER_DEF_TYPE_4%type := to_number('$f3');
		C_SUPPLIER_ID interface_pre_advice_header.supplier_ID%type := upper('$f4');
		C_DEL_DATE interface_pre_advice_header.due_dstamp%type := to_timestamp(to_char( TO_timestamp('$f9','YYYYMMDDHH24MISS'),'DD-MON-RR HH.MI.SSXFF AM'),'DD-MON-RR HH.MI.SSXFF AM'); --interface file in 24hr this converts to 12hr
		C_LINE_ID interface_order_line.line_id%type := 1;
		C_HEADER_EXIST number ;

		begin
		C_HEADER_EXIST := 0;
		select greatest(nvl((select max(line_id) from interface_pre_advice_line where pre_advice_id = C_PRE_ADVICE_ID),0),nvl((select max(line_id) from pre_advice_line where pre_advice_id = C_PRE_ADVICE_ID),0)) into C_LINE_ID from dual;
		C_LINE_ID := C_LINE_ID +1;
		--Check if header already exists in the interface
		--select 1 into C_HEADER_EXIST from interface_order_header i, order_header h, dual where (i.order_id = C_ORDER_ID or h.order_id = C_ORDER_ID);
		if
			C_HEADER_EXIST = 0
		then
		-- build the PreAdvice header interface using the variables defined above

			Insert into interface_pre_advice_header
			(KEY, CLIENT_ID,PRE_ADVICE_ID,PRE_ADVICE_TYPE,SITE_ID,OWNER_ID,SUPPLIER_ID,STATUS,DUE_DSTAMP,MERGE_ACTION,MERGE_STATUS,MERGE_DSTAMP)
			values
				(if_pah_pk_seq.nextval,'SENZ',C_PRE_ADVICE_ID,null,'ROTH001','SENZ',C_SUPPLIER_ID,'Hold',C_DEL_DATE,'A','Pending',to_timestamp(to_char(systimestamp,'DD-MON-RR HH.MI.SSXFF AM')));
			-- set the starting line id

			Insert into interface_pre_advice_line
				(KEY,CLIENT_ID,PRE_ADVICE_ID,LINE_ID,SKU_ID,CONFIG_ID,tracking_level,QTY_DUE,MERGE_ACTION,MERGE_STATUS,MERGE_DSTAMP,owner_id)
				VALUES
				(if_pal_pk_seq.nextval, 'SENZ', C_PRE_ADVICE_ID,C_LINE_ID, C_SKU_ID ,'SM1' ,'EACHES',C_QTY, 'A', 'Pending',to_timestamp(to_char(systimestamp,'DD-MON-RR HH.MI.SSXFF AM')),'SENZ');
			COMMIT;

		end if;
		end;
		/
		exit

EOF
	done <$i
	echo "NEW FILE"
	# Once the file is done rename it with a .old so it is not processed again
	mv $i $HOME/comms/senzer/inarchive/$i.old
	# Loop through all the files listed in the file
done <filenames1.txt
