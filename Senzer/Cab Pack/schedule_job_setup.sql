BEGIN
DBMS_SCHEDULER.CREATE_JOB (
  job_name     => 'SENZ_EXP_EXT_J',
  program_name   => 'SENZ_EXP_EXT_P',
  schedule_name   => 'SENZ_INV_EXP_EXT_S');
  DBMS_SCHEDULER.ENABLE(name=>'SENZ_EXP_EXT_J');
insert into scheduler_job values ('SENZ_EXP_EXT_J',null,'SENZ_EXP_EXT_P','Y','N',null,'SENZ_INV_EXP_EXT_S',null,null,null,null,null,'Senzer Expiry Extract','RELEASE',sysdate,'RELEASE',sysdate);

  DBMS_SCHEDULER.CREATE_JOB (
    job_name     => 'SENZ_INV_MOVE_EXT_J',
    program_name   => 'SENZ_INV_MOVE_EXT_P',
    schedule_name   => 'SENZ_INV_MOVE_EXT_S');
    DBMS_SCHEDULER.ENABLE(name=>'SENZ_INV_MOVE_EXT_J');
insert into scheduler_job values ('SENZ_INV_MOVE_EXT_J',null,'SENZ_INV_MOVE_EXT_P','Y','N',null,'SENZ_INV_MOVE_EXT_S',null,null,null,null,null,'Senzer Movement Extract','RELEASE',sysdate,'RELEASE',sysdate);

 DBMS_SCHEDULER.CREATE_JOB (
      job_name     => 'SENZ_INV_SUM_EXT_J',
      program_name   => 'SENZ_INV_SUM_EXT_P',
      schedule_name   => 'SENZ_INV_SUM_EXT_S');
      DBMS_SCHEDULER.ENABLE(name=>'SENZ_INV_SUM_EXT_J');

insert into scheduler_job values ('SENZ_INV_SUM_EXT_J',null,'SENZ_INV_SUM_EXT_P','Y','N',null,'SENZ_INV_SUM_EXT_S',null,null,null,null,null,'Senzer Inventory Extract','RELEASE',sysdate,'RELEASE',sysdate);

      DBMS_SCHEDULER.CREATE_JOB (
        job_name     => 'SENZ_PO_EXT_J',
        program_name   => 'SENZ_PO_EXT_P',
        schedule_name   => 'SENZ_PO_EXT_S');
        DBMS_SCHEDULER.ENABLE(name=>'SENZ_PO_EXT_J');

insert into scheduler_job values ('SENZ_PO_EXT_J',null,'SENZ_PO_EXT_P','Y','N',null,'SENZ_PO_EXT_S',null,null,null,null,null,'Senzer PO Extract','RELEASE',sysdate,'RELEASE',sysdate);

        DBMS_SCHEDULER.CREATE_JOB (
          job_name     => 'SENZ_ORDER_IF_J',
          program_name   => 'SENZ_ORDER_IF_P',
          schedule_name   => 'SENZ_IF_S');
          DBMS_SCHEDULER.ENABLE(name=>'SENZ_ORDER_IF_J');

insert into scheduler_job values ('SENZ_ORDER_IF_J',null,'SENZ_ORDER_IF_P','Y','N',null,'SENZ_IF_S',null,null,null,null,null,'Senzer Order Interface','RELEASE',sysdate,'RELEASE',sysdate);

          DBMS_SCHEDULER.CREATE_JOB (
            job_name     => 'SENZ_PO_IF_J',
            program_name   => 'SENZ_PO_IF_P',
            schedule_name   => 'SENZ_IF_S');
            DBMS_SCHEDULER.ENABLE(name=>'SENZ_PO_IF_J');

insert into scheduler_job values ('SENZ_PO_IF_J',null,'SENZ_PO_IF_P','Y','N',null,'SENZ_IF_S',null,null,null,null,null,'Senzer PO Interface','RELEASE',sysdate,'RELEASE',sysdate);

  DBMS_SCHEDULER.CREATE_JOB (
    job_name     => 'SENZ_FILE_EXT_J',
    program_name   => 'SENZ_FILE_EXT_P',
    schedule_name   => 'SENZ_FILE_EXT_S');
    DBMS_SCHEDULER.ENABLE(name=>'SENZ_FILE_EXT_J');

insert into scheduler_job values ('SENZ_FILE_EXT_J',null,'SENZ_FILE_EXT_P','Y','N',null,'SENZ_FILE_EXT_S',null,null,null,null,null,'Senzer PO Interface','RELEASE',sysdate,'RELEASE',sysdate);

commit;

END;
