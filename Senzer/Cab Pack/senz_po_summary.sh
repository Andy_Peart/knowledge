#!/bin/sh

. $HOME/dcs/.dcs_profile

cd $DCS_COMMSDIR/senzer/outwork/.

# setup the filename
date=`date +"%C%y%m%d_%H%M%S000"`
time=`date|awk '{print $4}'|sed 's/\://g'`
file=SENZ_PO_SUM_$date.csv

sqlplus -s $ORACLE_USR << !

create table tmp_senz_po_det as
select pl.pre_advice_id as Pre_advice ,ph.status, ph.supplier_id as supplier, pl.sku_id , s.description ,pl.qty_due as DUE_IN,nvl(pl.qty_received,0) as REC,to_char(ph.due_dstamp,'DD/MM/YYYY') as due_dstamp
from
    pre_advice_header ph, pre_advice_line pl, sku s
where
ph.client_id = 'SENZ' and
pl.pre_advice_id = ph.pre_advice_id and
s.sku_id = pl.sku_id
;
exit
!

sqlplus -s $ORACLE_USR << !
spool $DCS_COMMSDIR/senzer/outwork/$file
set colsep ,
set headsep off
set pagesize 0
set trimspool on
set trimout on
set linesize 1500
set tab off
set feedback off
select 'PO,Status,Supplier,SKU,Description,Qty Due,Received,Outstanding,Due Date,Last Receipt Date' from dual;
select
 po.pre_advice||','||case when po.status = 'Hold' then 'Outstanding' else po.status end||','||po.supplier ||','||po.sku_id||','|| po.description||','||po.due_in
||','||po.REC||','|| greatest(po.due_in - po.rec,0)||','||po.due_dstamp||','||nvl(to_char(max(itl.dstamp),'DD/MM/RRRR'),'NO Receipt')
from tmp_senz_po_det po, inventory_transaction itl
where (po.pre_advice = itl.reference_id or po.pre_advice not in (select reference_id from inventory_transaction where code = 'Receipt' and client_id = 'SENZ'))
group by  po.pre_advice||','||case when po.status = 'Hold' then 'Outstanding' else po.status end||','||po.supplier ||','||po.sku_id||','|| po.description||','||po.due_in
||','||po.REC||','|| greatest(po.due_in - po.rec,0)||','||po.due_dstamp;
spool off
exit
!


sqlplus -s $ORACLE_USR << !
drop table tmp_senz_po_det;
exit
!

# remove empty lines from the output file
sed -i '/^$/d' $file

echo "Senzer Pre Advice Summary" | mail -a $file -s "1,1 Senzer PO Report" SenzerOperationalReports@clippergroup.co.uk; 

# copy to outarchive
cp $file ../outarchive/.

# move the file to the outtray
mv $file ../outtray/.
