#!/bin/sh

. $HOME/dcs/.dcs_profile

cd $DCS_COMMSDIR/senzer/outwork/.

# setup the filename
date=`date +"%C%y%m%d_%H%M%S000"`
time=`date|awk '{print $4}'|sed 's/\://g'`
file=SENZ_ORD__SUM_$date.csv

sqlplus -s $ORACLE_USR << !
spool $DCS_COMMSDIR/senzer/outwork/$file
set colsep ,
set headsep off
set pagesize 0
set trimspool on
set trimout on
set linesize 1500
set tab off
set feedback off
SELECT
    order_header.order_id||','||
    order_header.status||','||
    to_char(order_header.order_date,'DD/MM/YY HH24:MI')||','||
    to_char(order_header.creation_date,'DD/MM/YY HH24:MI')||','||
    to_char(order_header.shipped_date,'DD/MM/YY HH24:MI')||','||
case
    when to_number (to_char(order_date,'HH24')) <= 14
    and nvl(trunc(shipped_date),trunc(sysdate)) = trunc(order_date)
    and status != 'Shipped' then 'FORECAST ON TIME,'

    when to_number (to_char(order_date,'HH24')) > 14
    and nvl(trunc(shipped_date),trunc(sysdate)) <= trunc(order_date +1)
    and status != 'Shipped' then 'FORECAST ON TIME,'

    when to_number (to_char(order_date,'HH24')) <= 14
    and (shipped_date is null or shipped_date is not null)
    and nvl(trunc(shipped_date),trunc(sysdate)) != trunc(order_date)
    and status != 'Shipped'
     and order_header.order_id IN (
            SELECT
                task_id
            FROM
                move_descrepancy
            WHERE
                move_descrepancy.client_id = 'SENZ'
        )
             AND order_header.status NOT IN (
            'Shipped',
            'Complete'
        )
    then 'FORECAST LATE,STOCK ISSUE'

    when to_number (to_char(order_date,'HH24')) > 14
    and (shipped_date is null or shipped_date is not null)
    and nvl(trunc(shipped_date),trunc(sysdate)) >= trunc(order_date +1)
    and status != 'Shipped'
         and order_header.order_id IN (
            SELECT
                task_id
            FROM
                move_descrepancy
            WHERE
                move_descrepancy.client_id = 'SENZ'
        )
             AND order_header.status NOT IN (
            'Shipped',
            'Complete'
        )
    then 'FORECAST LATE,STOCK ISSUE'

        when to_number (to_char(order_date,'HH24')) <= 14
    and (shipped_date is null or shipped_date is not null)
    and nvl(trunc(shipped_date),trunc(sysdate)) != trunc(order_date)
    and status != 'Shipped'
     and order_header.order_id not IN (
            SELECT
                task_id
            FROM
                move_descrepancy
            WHERE
                move_descrepancy.client_id = 'SENZ'
        )
             AND order_header.status NOT IN (
            'Shipped',
            'Complete'
        )
    then 'FORECAST LATE,'

    when to_number (to_char(order_date,'HH24')) > 14
    and (shipped_date is null or shipped_date is not null)
    and nvl(trunc(shipped_date),trunc(sysdate)) >= trunc(order_date +1)
    and status != 'Shipped'
         and order_header.order_id NOT IN (
            SELECT
                task_id
            FROM
                move_descrepancy
            WHERE
                move_descrepancy.client_id = 'SENZ'
        )
             AND order_header.status NOT IN (
            'Shipped',
            'Complete'
        )
    then 'FORECAST LATE,'

    when to_number (to_char(order_date,'HH24')) <= 14
    and nvl(trunc(shipped_date),trunc(sysdate)) = trunc(order_date)  then 'ON TIME,'

    when to_number (to_char(order_date,'HH24')) > 14
    and nvl(trunc(shipped_date),trunc(sysdate)) <= trunc(order_date +1)  then 'ON TIME,'

    when to_number (to_char(order_date,'HH24')) <= 14
    and (shipped_date is null or shipped_date is not null)
    and nvl(trunc(shipped_date),trunc(sysdate)) != trunc(order_date)  then 'LATE,'

    when to_number (to_char(order_date,'HH24')) > 14
    and (shipped_date is null or shipped_date is not null)
    and nvl(trunc(shipped_date),trunc(sysdate)) >= trunc(order_date +1)  then 'LATE,'
    END AS sla
FROM
    order_header
WHERE
    order_header.client_id = 'SENZ'
ORDER BY
    order_header.creation_date DESC
;
spool off
!


# remove empty lines from the output file
sed -i '/^$/d' $file

# copy to outarchive
cp $file ../outarchive/.

# move the file to the outtray
mv $file ../outtray/.
