#!/bin/sh

. $HOME/dcs/.dcs_profile

cd $DCS_COMMSDIR/senzer/outwork/.

# setup the filename
date=`date +"%C%y%m%d_%H%M%S000"`
time=`date|awk '{print $4}'|sed 's/\://g'`
file=SENZ_INV_SUM_$date.csv

sqlplus -s $ORACLE_USR << !

create table tmp_senz_inv_sum as(
select distinct(sku_id) , lock_status, nvl(lock_code,' ') "LOCK_CODE", sum(qty_on_hand) "HELD" from inventory where client_id = 'SENZ' group by sku_id,lock_status, lock_code);

create table tmp_senz_ord_sum  as(
select distinct(ol.sku_id), sum(ol.qty_ordered) "ORDERED" from order_line ol, order_header oh
where oh.client_id = 'SENZ'
and oh.status not in ('Shipped','Cancelled')
and oh.order_id = ol.order_id
group by sku_id);

!

sqlplus -s $ORACLE_USR << !
spool $DCS_COMMSDIR/senzer/outwork/$file
set colsep ,
set headsep off
set pagesize 0
set trimspool on
set trimout on
set linesize 1500
set tab off
set feedback off
select iv.sku_id||','||iv.lock_status||','|| iv.lock_code||','|| iv.held||','|| os.ordered||','|| sum(iv.held - os.ordered) from tmp_senz_ord_sum os, tmp_senz_inv_sum iv
where iv.lock_status = 'UnLocked'
and iv.sku_id = os.sku_id
group by
iv.sku_id||','||iv.lock_status||','|| iv.lock_code||','|| iv.held||','|| os.ordered||','
union all
select iv.sku_id ||','||iv.lock_status||','|| iv.lock_code||','|| iv.held||','|| null||','|| iv.held from tmp_senz_inv_sum iv
where iv.lock_status = 'UnLocked'
and iv.sku_id not in
(select ol.sku_id from order_line ol, order_header oh
where oh.client_id = 'SENZ'
and oh.status not in ('Shipped','Cancelled')
and oh.order_id = ol.order_id)
union all
select iv.sku_id ||','||iv.lock_status||','|| iv.lock_code||','|| iv.held||','|| 0||','|| 0 from tmp_senz_inv_sum iv
where iv.lock_status = 'Locked';
spool off
!


sqlplus -s $ORACLE_USR << !

drop Table TMP_SENZ_INV_SUM;
drop Table TMP_SENZ_ORD_SUM;

exit
!

# remove empty lines from the output file
sed -i '/^$/d' $file

# copy to outarchive
cp $file ../outarchive/.

# move the file to the outtray
mv $file ../outtray/.
