create table tmp_senz_inv_sum1 as(
select distinct(sku_id), sum(qty_on_hand) "HELD",batch_id, expiry_dstamp from inventory where client_id = 'SENZ' group by sku_id, batch_id, expiry_dstamp);

create table tmp_senz_inv_lock_sum1 as(
select distinct(sku_id) , sum(qty_on_hand) "HELD", batch_id, expiry_dstamp from inventory where client_id = 'SENZ' and lock_status = 'Locked' group by sku_id, batch_id, expiry_dstamp);

select trunc(ih.expiry_dstamp)||','||ih.batch_id||','||ih.sku_id||','||s.description||','||ih.held||','||0 
from tmp_senz_inv_sum1 ih, sku s
where trunc(ih.expiry_dstamp)||','||ih.batch_id||','||ih.sku_id not in (select trunc(ih.expiry_dstamp)||','||ih.batch_id||','||ih.sku_id from tmp_senz_inv_lock_sum1 ih)
and s.sku_id = ih.sku_id
union all
select trunc(ih.expiry_dstamp)||','||ih.batch_id||','||ih.sku_id||','||s.description||','||0||','||ih.held 
from tmp_senz_inv_lock_sum1 ih, sku s
where trunc(ih.expiry_dstamp)||','||ih.batch_id||','||ih.sku_id in (select trunc(ih.expiry_dstamp)||','||ih.batch_id||','||ih.sku_id from tmp_senz_inv_lock_sum1 ih)
and s.sku_id = ih.sku_id
union all
select trunc(ih.expiry_dstamp)||','||ih.batch_id||','||ih.sku_id||','||s.description||','||ih.held||','||il.held 
from tmp_senz_inv_lock_sum1 il,tmp_senz_inv_sum1 ih, sku s
where s.sku_id = ih.sku_id
and trunc(ih.expiry_dstamp)||','||ih.batch_id||','||ih.sku_id = trunc(il.expiry_dstamp)||','||il.batch_id||','||il.sku_id;

drop table tmp_senz_inv_sum1;
drop table tmp_senz_inv_lock_sum1;


