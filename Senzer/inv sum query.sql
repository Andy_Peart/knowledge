with
inv_sum as(
select distinct(sku_id) , lock_status, nvl(lock_code,' ') "LOCK_CODE", sum(qty_on_hand) "HELD" from inventory where client_id = 'SENZ' group by sku_id,lock_status, lock_code),
ord_sum as(
select distinct(ol.sku_id), sum(ol.qty_ordered) "ORDERED" from order_line ol, order_header oh
where oh.client_id = 'SENZ'
and oh.status not in ('Shipped','Cancelled')
and oh.order_id = ol.order_id
group by sku_id)

select iv.sku_id ,iv.lock_status, iv.lock_code, iv.held, os.ordered, sum(iv.held - os.ordered) from ord_sum os, inv_sum iv
where iv.lock_status = 'UnLocked'
and iv.sku_id = os.sku_id
group by
iv.sku_id , iv.lock_status, iv.lock_code, iv.held,os.ordered,os.sku_id
union all
select iv.sku_id ,iv.lock_status, iv.lock_code, iv.held, null, iv.held from inv_sum iv
where iv.lock_status = 'UnLocked'
and iv.sku_id not in
(select ol.sku_id from order_line ol, order_header oh
where oh.client_id = 'SENZ'
and oh.status not in ('Shipped','Cancelled')
and oh.order_id = ol.order_id)
union all
select iv.sku_id ,iv.lock_status, iv.lock_code, iv.held, 0, 0 from inv_sum iv
where iv.lock_status = 'Locked'
