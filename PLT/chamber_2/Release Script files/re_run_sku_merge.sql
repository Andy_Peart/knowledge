DECLARE
	l_count        NUMBER(5) := 0;
	v_commit_limit NUMBER(5) := 500;
	v_code         NUMBER;
	v_errm         VARCHAR2(64);
	l_sleep		   NUMBER := 10;
	RESULT         NUMBER;
	client		   client.client_id%type := 'PLT'

	CURSOR CUR_SKU IS
		SELECT
			SKU_ID
		from
			SKU
		;
	
	SKU_ROW CUR_SKU%rowtype;
BEGIN
	DBMS_OUTPUT.ENABLE
					  (
						  buffer_size => NULL
					  );
	l_count := 1;
	OPEN CUR_SKU;
	LOOP
		FETCH CUR_SKU
		INTO
			SKU_ROW
		;
		
		IF CUR_SKU%notfound THEN
			/*EXIT WHEN NO DATA FOUND*/
			dbms_output.put_line('All SKUS Updated');
			EXIT;
		END IF;
		dbms_output.put_line('SKU: '
		||SKU_ROW.SKU_ID );
		
		LibSkuMergeRule.RUNHEADERPROGRAMPROC (RESULT,client,SKU_ROW.SKU_ID);
		
		L_COUNT := L_COUNT +1;
		IF l_count = v_commit_limit THEN
			/*COMMIT COUNTER*/
			dbms_output.put_line('commit');
			l_count := 1;

			COMMIT;
			DBMS_LOCK.SLEEP (l_sleep);
		END IF;
	END LOOP;
	CLOSE CUR_SKU;
	COMMIT;
EXCEPTION
	/* WHEN ERROR OCCURS OUTPUT ERROR CODE AND CYCLE THROUGH CURSORS TO CHECK THEY ARE ALL SHUT*/
WHEN OTHERS THEN
	v_code := SQLCODE;
	v_errm := SUBSTR(SQLERRM, 1, 64);
	DBMS_OUTPUT.PUT_LINE (v_code
	|| ' '
	|| v_errm);
	IF CUR_SKU%isopen then
		close CUR_SKU;
	END IF;
	COMMIT;
	/*FINAL COMMIT TO ENSURE NO DB LOCKS*/
END;
-- set serveroutput on