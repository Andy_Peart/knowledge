#!/bin/bash

#Variable definitions

#host need to be defined in ~/.ssh/config with key file location

DATEFILE=`date +%Y%m%d%H%M%S`
LOGFILE="$HOME/Documents/PLT/chamber_2/logfiles/release."$DATEFILE".trc"

LOCALSAVESOURCE="$HOME/Documents/PLT/chamber_2/source"
LOCALSAVEBACKUP="$HOME/Documents/PLT/chamber_2/target_backup"

REMOTESOURCE="/home/wmsprd/AndyP/chamber_2/"
SOURCEHOST="clipper@clippltlive"
SOURCEUSER="wmsprd"

REMOTETARGET="/home/wmstst/AndyP/chamber_2/"
TARGETHOST="clipper@clipplttest"
TARGETUSER="wmstst"


################################################################################################
#function to extract Source Config

function extract_config
{
	echo "" >>$LOGFILE
	echo "EXTRACTING SOURCE CONFIG" >>$LOGFILE

#Log onto TST environment and run functionaccessdatautil

ssh $SOURCEHOST << FATSTEXTRACT
sudo -i -u $SOURCEUSER bash 
echo "In" 
whoami
cd $REMOTESOURCE/backup
pwd

functionaccessdatautil -a -s

FATSTEXTRACT


#if error exit the script and write error to logfile else write success to logfile

if [[ "$?" -ne 0 ]]
  then
    # something went wrong
    echo "  Something went wrong while running Functionaccess datautil from $SOURCEUSER" 
    exit 1
else
	echo " functionaccessdatautil extracted successfully"
fi >>$LOGFILE

#Log onto TST environment and run mergerulesdatautil


ssh $SOURCEHOST << MERGETSTEXTRACT
sudo -i -u $SOURCEUSER bash 
echo "In" 
whoami
cd $REMOTESOURCE/backup
pwd

mergerulesdatautil -s -a -z

MERGETSTEXTRACT

#if error exit the script and write error to logfile else write success to logfile

if [[ "$?" -ne 0 ]]
  then
    # something went wrong
    echo "  Something went wrong while running mergerulesdatautil from $SOURCEUSER" 1>&2
    exit 1
else
	echo " mergerulesdatautil extracted successfully"
fi >>$LOGFILE

ssh $SOURCEHOST << CLUSTERTSTEXTRACT
sudo -i -u $SOURCEUSER bash 
echo "In" 
whoami
cd $REMOTESOURCE/backup
pwd

clusteringdatautil -s -a

CLUSTERTSTEXTRACT

#if error exit the script and write error to logfile else write success to logfile

if [[ "$?" -ne 0 ]]
  then
    # something went wrong
    echo "  Something went wrong while running clusteringdatautil  from $SOURCEUSER" 1>&2
    exit 1
else
	echo " clusteringdatautil  extracted successfully"
fi >>$LOGFILE

ssh $SOURCEHOST << PUTAWAYGROUPEXTRACT
sudo -i -u $SOURCEUSER bash 
echo "In" 
whoami
cd $REMOTESOURCE/backup
pwd

dbdump -o putaway_group

PUTAWAYGROUPEXTRACT

#if error exit the script and write error to logfile else write success to logfile

if [[ "$?" -ne 0 ]]
  then
    # something went wrong
    echo "  Something went wrong while running dbdump from $SOURCEUSER" 1>&2
    exit 1
else
	echo " dbdump  extracted successfully"
fi >>$LOGFILE

ssh $SOURCEHOST << GROUPPUTAWAYEXTRACT
sudo -i -u $SOURCEUSER bash 
echo "In" 
whoami
cd $REMOTESOURCE/backup
pwd

dbdump -o group_putaway

GROUPPUTAWAYEXTRACT

#if error exit the script and write error to logfile else write success to logfile


if [[ "$?" -ne 0 ]]
  then
    # something went wrong
    echo "  Something went wrong while running dbdump from $SOURCEUSER" 1>&2
    exit 1
else
	echo " dbdump  extracted successfully"
fi >>$LOGFILE

}

############################################################################################

#Function to extract Source Config

function collect_source
{
	echo "" >>$LOGFILE
	echo "COLLECTING SOURCE CONFIG" >>$LOGFILE

cd $LOCALSAVESOURCE

 if [[ "$?" -ne 0 ]]
  then
    # something went wrong
    echo "" >>$LOGFILE
    echo "  Failed to access Local Directory" 1>&2 >>$LOGFILE
    exit 1
  fi


sftp $SOURCEHOST <<COLLECT_FILES
cd $REMOTESOURCE/backup
get *
bye
COLLECT_FILES

if [[ "$?" -ne 0 ]]
  then
    # something went wrong
    echo "" >>$LOGFILE
    echo "  Something went wrong collecting source files" 1>&2
    exit 1
else
	echo "" >>$LOGFILE
	echo " Source files collected and saved to $LOCALSAVESOURCE"
fi >>$LOGFILE
}

###########################################################################################


function backup_target
{

	echo "" >>$LOGFILE
	echo "BACKING UP TARGET CONFIG" >>$LOGFILE

#Log onto TST environment and run functionaccessdatautil

ssh $TARGETHOST << FATSTEXTRACT
sudo -i -u $TARGETUSER bash 
echo "In" 
whoami
cd $REMOTETARGET/backup
pwd

functionaccessdatautil -a -s

FATSTEXTRACT


#if error exit the script and write error to logfile else write success to logfile

if [[ "$?" -ne 0 ]]
  then
    # something went wrong
    echo "  Something went wrong while running Functionaccess datautil from $TARGETUSER" 
    exit 1
else
	echo " functionaccessdatautil extracted successfully"
fi >>$LOGFILE

#Log onto TST environment and run mergerulesdatautil


ssh $TARGETHOST << MERGETSTEXTRACT
sudo -i -u $TARGETUSER bash 
echo "In" 
whoami
cd $REMOTETARGET/backup
pwd

mergerulesdatautil -s -a -z

MERGETSTEXTRACT

#if error exit the script and write error to logfile else write success to logfile

if [[ "$?" -ne 0 ]]
  then
    # something went wrong
    echo "  Something went wrong while running mergerulesdatautil from $TARGETUSER" 1>&2
    exit 1
else
	echo " mergerulesdatautil extracted successfully"
fi >>$LOGFILE

ssh $TARGETHOST << CLUSTERTSTEXTRACT
sudo -i -u $TARGETUSER bash 
echo "In" 
whoami
cd $REMOTETARGET/backup
pwd

clusteringdatautil -s -a

CLUSTERTSTEXTRACT

#if error exit the script and write error to logfile else write success to logfile

if [[ "$?" -ne 0 ]]
  then
    # something went wrong
    echo "  Something went wrong while running clusteringdatautil  from $SOURCEUSER" 1>&2
    exit 1
else
	echo " clusteringdatautil  extracted successfully"
fi >>$LOGFILE

ssh $TARGETHOST << PUTAWAYGROUPEXTRACT
sudo -i -u $TARGETUSER bash 
echo "In" 
whoami
cd $REMOTETARGET/backup
pwd

dbdump -o putaway_group

PUTAWAYGROUPEXTRACT

#if error exit the script and write error to logfile else write success to logfile

if [[ "$?" -ne 0 ]]
  then
    # something went wrong
    echo "  Something went wrong while running dbdump from $SOURCEUSER" 1>&2
    exit 1
else
	echo " dbdump  extracted successfully"
fi >>$LOGFILE

ssh $TARGETHOST << GROUPPUTAWAYEXTRACT
sudo -i -u $TARGETUSER bash 
echo "In" 
whoami
cd $REMOTETARGET/backup
pwd

dbdump -o group_putaway

GROUPPUTAWAYEXTRACT

#if error exit the script and write error to logfile else write success to logfile

if [[ "$?" -ne 0 ]]
  then
    # something went wrong
    echo "  Something went wrong while running dbdump from $SOURCEUSER" 1>&2
    exit 1
else
	echo " dbdump  extracted successfully"
fi >>$LOGFILE

}	

##########################################################################################

function collect_target_backup
{
	echo "" >>$LOGFILE
	echo "COLLECTING TARGET BACKUP" >>$LOGFILE

cd $LOCALSAVEBACKUP

 if [[ "$?" -ne 0 ]]
  then
    # something went wrong
    echo "" >>$LOGFILE
    echo "  Failed to access Local Directory" 1>&2 >>$LOGFILE
    exit 1
  fi

sftp $TARGETHOST <<COLLECT_FILES
cd $REMOTETARGET/backup
get *
bye
COLLECT_FILES

if [[ "$?" -ne 0 ]]
  then
    # something went wrong
    echo "" >>$LOGFILE
    echo "  Something went wrong collecting source files" 1>&2
    exit 1
else
	echo "" >>$LOGFILE
	echo " Source files collected and saved to $LOCALSAVEBACKUP"
fi >>$LOGFILE

}

##########################################################################################

function push_source_files
{
	echo "" >>$LOGFILE
	echo "PUTTING NEW CODE TO TARGET" >>$LOGFILE

cd $LOCALSAVEBACKUP

 if [[ "$?" -ne 0 ]]
  then
    # something went wrong
    echo "" >>$LOGFILE
    echo "  Failed to access Local Directory" 1>&2 >>$LOGFILE
    exit 1
  fi

#Ensure the folder is empty of .DMP files

sftp $TARGETHOST <<COLLECT_FILES
cd $REMOTETARGET/release
lcd $LOCALSAVESOURCE
put *
bye
COLLECT_FILES

if [[ "$?" -ne 0 ]]
  then
    # something went wrong
    echo "" >>$LOGFILE
    echo "  Something went wrong putting source files" 1>&2
    exit 1
else
	echo "" >>$LOGFILE
	echo " Source files put to cd $REMOTETARGET/release"
fi >>$LOGFILE
}


##########################################################################################


function load_config
{

	echo "" >>$LOGFILE
	echo "Loading New CONFIG" >>$LOGFILE

#Log onto TST environment and run functionaccessdatautil

ssh $TARGETHOST << FATSTEXTRACT
sudo -i -u $TARGETUSER bash 
echo "In" 
whoami
cd $REMOTETARGET/release
pwd

functionaccessdatautil -a -l

FATSTEXTRACT


#if error exit the script and write error to logfile else write success to logfile

if [[ "$?" -ne 0 ]]
  then
    # something went wrong
    echo "  Something went wrong while running Functionaccess datautil from $TARGETUSER" 
    exit 1
else
	echo " functionaccessdatautil loaded successfully"
fi >>$LOGFILE

#Log onto TST environment and run mergerulesdatautil


ssh $TARGETHOST << MERGETSTEXTRACT
sudo -i -u $TARGETUSER bash 
echo "In" 
whoami
cd $REMOTETARGET/release
pwd

mergerulesdatautil -l -a -z

MERGETSTEXTRACT

#if error exit the script and write error to logfile else write success to logfile

if [[ "$?" -ne 0 ]]
  then
    # something went wrong
    echo "  Something went wrong while running mergerulesdatautil from $TARGETUSER" 1>&2
    exit 1
else
	echo " mergerulesdatautil loading successfully"
fi >>$LOGFILE

ssh $TARGETHOST << CLUSTERTSTEXTRACT
sudo -i -u $TARGETUSER bash 
echo "In" 
whoami
cd $REMOTETARGET/release
pwd

clusteringdatautil -l -a

CLUSTERTSTEXTRACT

#if error exit the script and write error to logfile else write success to logfile

if [[ "$?" -ne 0 ]]
  then
    # something went wrong
    echo "  Something went wrong while running clusteringdatautil  from $SOURCEUSER" 1>&2
    exit 1
else
	echo " clusteringdatautil  extracted successfully"
fi >>$LOGFILE

ssh $TARGETHOST << DBLOAD
sudo -i -u $TARGETUSER bash 
echo "In" 
whoami
cd $REMOTETARGET/release
pwd
sudo chown wmsprd *

dbload -r -a

DBLOAD


#if error exit the script and write error to logfile else write success to logfile

if [[ "$?" -ne 0 ]]
  then
    # something went wrong
    echo "  Something went wrong while running dbload from $SOURCEUSER" 1>&2
    exit 1
else
	echo " dbload  extracted successfully"
fi >>$LOGFILE


}

################################## MAIN PROGRAM ##########################################

echo "$FILE started at `date`  " >>$LOGFILE
echo "" >>$LOGFILE

{
read -p "Do you wish to release key 1 rollback key 2 ? " choice
case "$choice" in 
  1 ) echo "Starting Release" ; extract_config;collect_source;backup_target;collect_target_backup;
								push_source_files;load_config; datacheck -m2 -f;;
  2 ) echo "Starting to Rollback" ; rollback;;
  * ) echo "invalid"; exit 0;;
esac
} >>$LOGFILE

echo "$FILE completed at `date`  " >>$LOGFILE