#!/bin/bash

. $HOME/dcs/.dcs_profile

cd $DCS_COMMSDIR/outwork/.

# compose the csv file for the extract
EXTENSION="$(date +%y%m%d%H%M%S)"
CSVFILE="EDICONF_${EXTENSION}.csv"
OUTWORK="$DCS_COMMSDIR/outwork"

# set preextract value
PRE_UPLOADED="A"
UPLOADED="Y"


# setup the filename 
date=`date +"%m%d"`
time=`date|awk '{print $4}'|sed 's/\://g'`
file=EDICONF_$date$time.csv


# find orders that need uploading #


sqlplus -s $ORACLE_USR << ! >> $CSVFILE
        set serveroutput on
		set feedback off
        set heading on
		set linesize 32767
select
order_id,',',to_char(order_date, 'dd/MM/rr hh24:mi'),',',to_char(creation_date,'dd/MM/rr hh24:mi')
from 
order_header where 
creation_date > to_timestamp(to_char(current_timestamp -(1/24), 'yyyymmddhh24') || '00', 'yyyymmddhh24mi');

exit
!


echo $OUTWORK/$CSVFILE

# Copy file to out archive
echo "Copying $OUTWORK/$CSVFILE"
cp $OUTWORK/$CSVFILE $DCS_COMMSDIR/outarchive/$CSVFILE

# move to outtray
echo "moving $OUTWORK/$CSVFILE $DCS_COMMSDIR/outtray"
mv $OUTWORK/$CSVFILE $DCS_COMMSDIR/outtray
