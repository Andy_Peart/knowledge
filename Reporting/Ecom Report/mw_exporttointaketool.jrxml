<?xml version="1.0" encoding="UTF-8"?>
<!-- Created with Jaspersoft Studio version 6.0.0.final using JasperReports Library version 6.0.0  -->
<!-- 2019-02-14T18:16:24 -->
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="lk_replenreport" pageWidth="1190" pageHeight="842" orientation="Landscape" columnWidth="1150" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" uuid="b69a7ba2-1866-4d8e-a809-2b1445170a2e">
	<property name="com.jaspersoft.studio.unit." value="pixel"/>
	<property name="net.sf.jasperreports.export.xls.exclude.origin.band.2" value="pageFooter"/>
	<property name="net.sf.jasperreports.export.xls.exclude.origin.band.1" value="pageHeader"/>
	<property name="net.sf.jasperreports.export.xls.exclude.origin.band.title" value="title"/>
	<property name="net.sf.jasperreports.export.csv.exclude.origin.band.2" value="pageFooter"/>
	<property name="net.sf.jasperreports.export.csv.exclude.origin.band.1" value="pageHeader"/>
	<property name="net.sf.jasperreports.export.csv.exclude.origin.band.title" value="title"/>
	<property name="net.sf.jasperreports.export.xls.exclude.origin.keep.first.band.1" value="columnHeader"/>
	<property name="net.sf.jasperreports.export.csv.exclude.origin.keep.first.band.1" value="columnHeader"/>
	<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
	<property name="net.sf.jasperreports.export.xls.wrap.text" value="false"/>
	<property name="net.sf.jasperreports.export.csv.wrap.text" value="false"/>
	<property name="com.jaspersoft.studio.unit.pageHeight" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.pageWidth" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.topMargin" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.bottomMargin" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.leftMargin" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.rightMargin" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.columnWidth" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.columnSpacing" value="pixel"/>
	<property name="com.jaspersoft.studio.data.defaultdataadapter" value="MW Clipper DEV"/>
	<parameter name="client_id" class="java.lang.String">
		<defaultValueExpression><![CDATA["MOUNTAIN"]]></defaultValueExpression>
	</parameter>
	<queryString language="SQL">
		<![CDATA[WITH
  inventory_view AS
  (
    SELECT
      regexp_replace(inventory.zone_1,'[^[:alpha:]]') AS zone_1
    , DENSE_RANK() OVER
      (
        PARTITION BY
          regexp_replace(inventory.zone_1,'[^[:alpha:]]')
        , sku_id
        , nvl(user_def_type_1,'NULL')
        ORDER BY
          SUM(qty_on_hand - qty_allocated) DESC
        , location_id
      ) AS location_rank
    , location_id
    , sku_id
    , nvl(user_def_type_1,'NULL') AS dye_lot
    , LISTAGG(';'||tag_id) WITHIN GROUP (ORDER BY tag_id) AS tag_id
    , SUM(qty_on_hand - qty_allocated) AS qty_available
    FROM inventory
    WHERE regexp_replace(inventory.zone_1,'[^[:alpha:]]') IN ('REPLEN','PICK')
          and site_id = 'PLPZ001'
    GROUP BY
      regexp_replace(inventory.zone_1,'[^[:alpha:]]')
    , location_id
    , sku_id
    , nvl(user_def_type_1,'NULL')
  )
, inventory_group AS
  (
    SELECT
      zone_1
    , sku_id
    , dye_lot
    , SUM(qty_available) AS qty_available
    FROM inventory_view
    GROUP BY
      zone_1
    , sku_id
    , dye_lot
  )
, order_line_view AS
  (
    SELECT
      order_header.user_def_type_6 AS delivery_type
    , qty_ordered - ( nvl(qty_tasked,0) + nvl(qty_picked,0) ) AS qty_outstanding
    , nvl(order_line.user_def_type_1,'NULL') AS dye_lot
    , order_line.*
    FROM order_line
    JOIN order_header
      ON order_header.order_id = order_line.order_id AND order_header.client_id = order_line.client_id
    WHERE order_header.client_id = $P{client_id}
      AND order_header.status NOT IN ('Cancelled','Shipped')
  )
, order_line_group AS
  (
    SELECT
      sku_id
    , dye_lot
    , SUM(decode(delivery_type,'STANDARD',qty_outstanding,0)) AS qty_outstanding_standard
    , SUM(decode(delivery_type,'NEXTDAY' ,qty_outstanding,0)) AS  qty_outstanding_nextday
    , SUM(qty_outstanding) AS qty_outstanding
    FROM order_line_view
    GROUP BY
      sku_id
    , dye_lot
  )
, replen_view AS
  (
    SELECT
      order_line_group.sku_id
    , order_line_group.dye_lot
    , order_line_group.qty_outstanding_standard
    , order_line_group.qty_outstanding_nextday
    , order_line_group.qty_outstanding
    , nvl(zone_group.qty_available,0) AS qty_in_zone
    , order_line_group.qty_outstanding - nvl(zone_group.qty_available,0) AS replen_qty
    FROM order_line_group
    LEFT JOIN inventory_group zone_group
           ON zone_group.sku_id = order_line_group.sku_id
          AND zone_group.dye_lot = order_line_group.dye_lot
          AND zone_group.zone_1 = 'PICK'
    LEFT JOIN inventory_group bulk_group
           ON bulk_group.sku_id = order_line_group.sku_id
          AND bulk_group.dye_lot = order_line_group.dye_lot
          AND bulk_group.zone_1 = 'REPLEN'
    WHERE order_line_group.qty_outstanding > nvl(zone_group.qty_available,0)
  )
SELECT
  replen_view.sku_id
 , (select ean from sku where sku.sku_id = replen_view.sku_id) EAN
, replen_view.dye_lot
, replen_view.qty_outstanding_standard
, replen_view.qty_outstanding_nextday
, replen_view.qty_outstanding
, replen_view.qty_in_zone
, replen_view.replen_qty
, nullif(LISTAGG(';'||bulk_view.location_id) WITHIN GROUP (ORDER BY bulk_view.location_rank DESC),';') AS from_location
, LISTAGG(bulk_view.tag_id) WITHIN GROUP (ORDER BY bulk_view.location_rank DESC) AS tag_id
, nvl(zone_view.location_id,'') AS to_location
FROM replen_view
LEFT JOIN inventory_view bulk_view
       ON bulk_view.sku_id = replen_view.sku_id
      AND bulk_view.dye_lot = replen_view.dye_lot
      AND bulk_view.zone_1 = 'REPLEN'
LEFT JOIN inventory_view zone_view
       ON zone_view.sku_id = replen_view.sku_id
      AND zone_view.dye_lot = replen_view.dye_lot
      AND zone_view.zone_1 = 'PICK'
      AND nvl(zone_view.location_rank,1) = 1
GROUP BY
  replen_view.sku_id
, replen_view.dye_lot
, replen_view.qty_outstanding_standard
, replen_view.qty_outstanding_nextday
, replen_view.qty_outstanding
, replen_view.qty_in_zone
, replen_view.replen_qty
, nvl(zone_view.location_id,'')]]>
	</queryString>
	<field name="SKU_ID" class="java.lang.String"/>
	<field name="DYE_LOT" class="java.lang.String"/>
	<field name="QTY_OUTSTANDING_STANDARD" class="java.math.BigDecimal"/>
	<field name="QTY_OUTSTANDING_NEXTDAY" class="java.math.BigDecimal"/>
	<field name="QTY_OUTSTANDING" class="java.math.BigDecimal"/>
	<field name="QTY_IN_ZONE" class="java.math.BigDecimal"/>
	<field name="FROM_LOCATION" class="java.lang.String"/>
	<field name="TAG_ID" class="java.lang.String"/>
	<field name="TO_LOCATION" class="java.lang.String"/>
	<field name="REPLEN_QTY" class="java.math.BigDecimal"/>
	<field name="EAN" class="java.lang.String"/>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="40" splitType="Stretch">
			<property name="local_mesure_unitheight" value="pixel"/>
			<property name="com.jaspersoft.studio.unit.height" value="px"/>
			<textField>
				<reportElement x="580" y="0" width="570" height="30" uuid="83c52866-5436-4704-9138-3b8f232f82ef">
					<property name="com.jaspersoft.studio.unit.y" value="px"/>
				</reportElement>
				<box padding="2">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="1.2" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="14"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[DATEFORMAT(TODAY(),"dd/MM/yyyy HH:mm")]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="0" y="0" width="580" height="30" uuid="9edae87d-c683-466f-837d-9890f5036a24">
					<property name="local_mesure_unity" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="px"/>
				</reportElement>
				<box padding="2">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="1.2" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font size="14"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA["MW Replen Report"]]></textFieldExpression>
			</textField>
			<frame>
				<reportElement x="437" y="7" width="299" height="20" uuid="c0daecc1-81e0-40ea-9474-9b3a7be87c69"/>
			</frame>
		</band>
	</title>
	<columnHeader>
		<band height="20" splitType="Stretch">
			<property name="com.jaspersoft.studio.layout" value="com.jaspersoft.studio.editor.layout.HorizontalRowLayout"/>
			<property name="local_mesure_unitheight" value="pixel"/>
			<property name="com.jaspersoft.studio.unit.height" value="px"/>
			<staticText>
				<reportElement mode="Opaque" x="0" y="0" width="149" height="20" backcolor="#D1CFCF" uuid="16f7413f-7a57-405e-bcd9-467b070bfcc9"/>
				<box>
					<topPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[SKU]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="149" y="0" width="143" height="20" backcolor="#D1CFCF" uuid="d80cdde2-d036-4c49-9b70-f00fea6d8a58"/>
				<box>
					<topPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[EAN]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="292" y="0" width="99" height="20" backcolor="#D1CFCF" uuid="faf6b2a9-7780-449b-9b95-6db7b070ed3e"/>
				<box>
					<topPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Ordered qty]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="391" y="0" width="99" height="20" backcolor="#D1CFCF" uuid="060db1fd-8f36-4302-be97-e74bfdb2d610"/>
				<box>
					<topPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Qty in PICK zone]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="490" y="0" width="143" height="20" backcolor="#D1CFCF" uuid="4a0fabf4-6b30-40c8-a507-461a51dff3fd"/>
				<box>
					<topPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Location in PICK zone]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="633" y="0" width="99" height="20" backcolor="#D1CFCF" uuid="6c6b5e26-c4cd-4af3-9a4e-7abbc67758f5"/>
				<box>
					<topPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Qty to replenish]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="732" y="0" width="207" height="20" backcolor="#D1CFCF" uuid="b1995fee-b913-4597-b12f-2ed0fb29871f"/>
				<box>
					<topPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Locations in REPLEN zone]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="939" y="0" width="207" height="20" backcolor="#D1CFCF" uuid="0bdbb72a-7ac2-4ad7-a104-2bb84be014f9"/>
				<box>
					<topPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Tag Ids in REPLEN zone]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="20" splitType="Stretch">
			<property name="com.jaspersoft.studio.layout" value="com.jaspersoft.studio.editor.layout.HorizontalRowLayout"/>
			<textField isBlankWhenNull="true">
				<reportElement x="0" y="0" width="149" height="20" uuid="cb0c8e51-8f2d-40df-8149-07c78c5553af"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<paragraph leftIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{SKU_ID}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="149" y="0" width="143" height="20" uuid="7deda02f-2ed4-4a0d-ba06-523a988caf71"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<paragraph leftIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{EAN}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="292" y="0" width="99" height="20" uuid="0153044e-4aec-461b-aca4-7cfb863e9908"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<paragraph leftIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{QTY_OUTSTANDING}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="391" y="0" width="99" height="20" uuid="bf29a69e-58bf-4ddd-a403-4fb997159f76"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<paragraph leftIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{QTY_IN_ZONE}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="490" y="0" width="143" height="20" uuid="b05f9690-ba4c-41d3-93b6-f538eea6105b"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<paragraph leftIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{TO_LOCATION}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="633" y="0" width="99" height="20" uuid="23a5923c-bf8b-4a7c-af9b-354c034ec6ac"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<paragraph leftIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{REPLEN_QTY}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="732" y="0" width="207" height="20" uuid="b99e48fd-5956-4fb6-b71a-82ff4774e056"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<paragraph leftIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{FROM_LOCATION}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="939" y="0" width="207" height="20" uuid="475fd581-485b-4835-a2d2-dd5b074b8cdc"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<paragraph leftIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{TAG_ID}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<pageFooter>
		<band height="19">
			<textField evaluationTime="Master">
				<reportElement x="0" y="0" width="1150" height="19" uuid="904b3cb7-780f-4904-ba9b-7075c2af3fc5"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[msg ("Page {0} of {1}",$V{MASTER_CURRENT_PAGE},$V{PAGE_NUMBER})]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
</jasperReport>
